package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContextListener;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaandroid;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaw3producao;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.UsuarioDAO;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SHA512Util;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.atividadetipo.AtividadetipoRESTModel;
import br.com.linkcom.sined.util.rest.android.banco.BancoRESTModel;
import br.com.linkcom.sined.util.rest.android.categoria.CategoriaRESTModel;
import br.com.linkcom.sined.util.rest.android.cliente.ClienteRESTModel;
import br.com.linkcom.sined.util.rest.android.clientedocumentotipo.ClientedocumentotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.clientehistorico.ClientehistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.clienteprazopagamento.ClienteprazopagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.clientevendedor.ClientevendedorRESTModel;
import br.com.linkcom.sined.util.rest.android.colaborador.ColaboradorRESTModel;
import br.com.linkcom.sined.util.rest.android.conta.ContaRESTModel;
import br.com.linkcom.sined.util.rest.android.contaempresa.ContaempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.contato.ContatoRESTModel;
import br.com.linkcom.sined.util.rest.android.contatotipo.ContatotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.documento.DocumentoRESTModel;
import br.com.linkcom.sined.util.rest.android.documentohistorico.DocumentohistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.documentotipo.DocumentotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.downloadbean.DownloadBeanRESTModel;
import br.com.linkcom.sined.util.rest.android.downloadbean.DownloadBeanRESTWSBean;
import br.com.linkcom.sined.util.rest.android.empresa.EmpresaRESTModel;
import br.com.linkcom.sined.util.rest.android.endereco.EnderecoRESTModel;
import br.com.linkcom.sined.util.rest.android.enderecotipo.EnderecotipoRESTModel;
import br.com.linkcom.sined.util.rest.android.formapagamento.FormapagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.fornecedor.FornecedorRESTModel;
import br.com.linkcom.sined.util.rest.android.indicecorrecao.IndicecorrecaoRESTModel;
import br.com.linkcom.sined.util.rest.android.localarmazenagem.LocalarmazenagemRESTModel;
import br.com.linkcom.sined.util.rest.android.localarmazenagemempresa.LocalarmazenagemempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.material.MaterialRESTModel;
import br.com.linkcom.sined.util.rest.android.materialEmpresa.MaterialEmpresaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialformulavalorvenda.MaterialformulavalorvendaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialgrupo.MaterialgrupoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialproducao.MaterialproducaoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialrelacionado.MaterialrelacionadoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialsimilar.MaterialsimilarRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelapreco.MaterialtabelaprecoRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecocliente.MaterialtabelaprecoclienteRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoempresa.MaterialtabelaprecoempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoitem.MaterialtabelaprecoitemRESTModel;
import br.com.linkcom.sined.util.rest.android.materialunidademedida.MaterialunidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.meiocontato.MeiocontatoRESTModel;
import br.com.linkcom.sined.util.rest.android.municipio.MunicipioRESTModel;
import br.com.linkcom.sined.util.rest.android.parametrogeral.ParametrogeralRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovendahistorico.PedidovendahistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovendatipo.PedidovendatipoRESTModel;
import br.com.linkcom.sined.util.rest.android.pneu.PneuRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumarca.PneumarcaRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumedida.PneumedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.pneumodelo.PneumodeloRESTModel;
import br.com.linkcom.sined.util.rest.android.prazopagamento.PrazopagamentoRESTModel;
import br.com.linkcom.sined.util.rest.android.prazopagamentoitem.PrazopagamentoitemRESTModel;
import br.com.linkcom.sined.util.rest.android.projeto.ProjetoRESTModel;
import br.com.linkcom.sined.util.rest.android.responsavelfrete.ResponsavelFreteRESTModel;
import br.com.linkcom.sined.util.rest.android.situacaohistorico.SituacaohistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.tabelavalor.TabelavalorRESTModel;
import br.com.linkcom.sined.util.rest.android.uf.UfRESTModel;
import br.com.linkcom.sined.util.rest.android.unidademedida.UnidademedidaRESTModel;
import br.com.linkcom.sined.util.rest.android.usuario.UsuarioRESTModel;
import br.com.linkcom.sined.util.rest.android.usuarioempresa.UsuarioempresaRESTModel;
import br.com.linkcom.sined.util.rest.android.valecompra.ValecompraRESTModel;
import br.com.linkcom.sined.util.rest.android.vwultimascompras.VwultimascomprasandroidRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.cliente.ClienteW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.clienteVendedor.ClienteVendedorW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean.DownloadBeanW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.downloadw3pneubean.DownloadBeanW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.empresa.EmpresaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipo.GarantiaTipoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipopercentual.GarantiaTipoPercentualW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.material.MaterialW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialgrupo.MaterialGrupoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialpneumodelo.MaterialPneuModeloW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialproducao.MaterialProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialsimilar.MaterialSimilarW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialunidademedida.MaterialUnidademedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.motivodevolucao.MotivoDevolucaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.patrimonioitem.PatrimonioItemW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pedidovenda.PedidoVendaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneu.PneuW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumarca.PneuMarcaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumedida.PneuMedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneumodelo.PneuModeloW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.pneuqualificacao.PneuQualificacaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapa.ProducaoEtapaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapaitem.ProducaoEtapaItemW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanome.ProducaoEtapaNomeW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanomecampo.ProducaoEtapaNomeCampoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.unidademedida.UnidadeMedidaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.usuario.UsuarioW3producaoRESTModel;


public class UsuarioService extends GenericService<Usuario> {
	
	private UsuarioDAO usuarioDAO;
	private PapelService papelService;
	private EmpresaService empresaService;
	private EnvioemailService envioemailService;
	private OrdemcompraService ordemcompraService;
	private ContapagarService contapagarService;
	private ArquivoService arquivoService;
	private CategoriaService categoriaService;
	private ParametrogeralService parametrogeralService;
	private EnderecotipoService enderecotipoService;
	private MaterialgrupoService materialgrupoService;
	private UnidademedidaService unidademedidaService;
	private UfService ufService;
	private MunicipioService municipioService;
	private ContatotipoService contatotipoService;
	private ColaboradorService colaboradorService;
	private FornecedorService fornecedorService;
	private ClienteService clienteService;
	private EnderecoService enderecoService;
	private ContatoService contatoService;
	private ResponsavelFreteService responsavelFreteService;
	private BancoService bancoService;
	private ContaService contaService;
	private DocumentotipoService documentotipoService;
	private FormapagamentoService formapagamentoService;
	private IndicecorrecaoService indicecorrecaoService;
	private PedidovendatipoService pedidovendatipoService;
	private PrazopagamentoService prazopagamentoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ProjetoService projetoService;
	private MaterialService materialService;
	private MaterialrelacionadoService materialrelacionadoService;
	private MaterialsimilarService materialsimilarService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService; 
	private MaterialtabelaprecoclienteService materialtabelaprecoclienteService;
	private ValecompraService valecompraService;
	private SincronizacaotabelaandroidService sincronizacaotabelaandroidService;
	private Sincronizacaotabelaw3producaoService sincronizacaotabelaw3producaoService;
	private UsuarioempresaService usuarioempresaService;
	private ClientevendedorService clientevendedorService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private MaterialtabelaprecoempresaService materialtabelaprecoempresaService;
	private VwultimascomprasandroidService vwultimascomprasandroidService;
	private MaterialproducaoService materialproducaoService;
	private PatrimonioitemService patrimonioitemService;
	private ProducaoetapaService producaoetapaService;
	private ProducaoetapaitemService producaoetapaitemService;
	private MotivodevolucaoService motivodevolucaoService;
	private PneuService pneuService;
	private PneumarcaService pneumarcaService;
	private PneumodeloService pneumodeloService;
	private PneumedidaService pneumedidaService;
	private ProducaoetapanomeService producaoetapanomeService;
	private ProducaoetapanomecampoService producaoetapanomecampoService;
	private AtividadetipoService atividadetipoService;
	private ClienteDocumentoTipoService clienteDocumentoTipoService; 
	private ClientehistoricoService clientehistoricoService;
	private ClientePrazoPagamentoService clientePrazoPagamentoService;
	private DocumentoService documentoService;
	private DocumentohistoricoService documentohistoricoService;
	private LocalarmazenagemService localarmazenagemService;
	private MeiocontatoService meiocontatoService;
	private SituacaohistoricoService situacaohistoricoService;
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private MaterialformulavalorvendaService materialformulavalorvendaService;
	private TabelavalorService tabelavalorService;
	private PedidovendaService pedidovendaService;
	private MaterialpneumodeloService materialpneumodeloService;
	private ContaempresaService contaempresaService;
	private PneuqualificacaoService pneuqualificacaoService;
	private GarantiatipoService garantiatipoService;
	private GarantiatipopercentualService garantiatipopercentualService;
	private MaterialempresaService materialEmpresaService;
	
	
	public void setMaterialEmpresaService(
			MaterialempresaService materialEmpresaService) {
		this.materialEmpresaService = materialEmpresaService;
	}
	
	public void setMaterialpneumodeloService(
			MaterialpneumodeloService materialpneumodeloService) {
		this.materialpneumodeloService = materialpneumodeloService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setProducaoetapanomecampoService(
			ProducaoetapanomecampoService producaoetapanomecampoService) {
		this.producaoetapanomecampoService = producaoetapanomecampoService;
	}
	public void setProducaoetapanomeService(
			ProducaoetapanomeService producaoetapanomeService) {
		this.producaoetapanomeService = producaoetapanomeService;
	}
	public void setMaterialtabelaprecoempresaService(
			MaterialtabelaprecoempresaService materialtabelaprecoempresaService) {
		this.materialtabelaprecoempresaService = materialtabelaprecoempresaService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	
	public void setClientevendedorService(
			ClientevendedorService clientevendedorService) {
		this.clientevendedorService = clientevendedorService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setEnderecotipoService(EnderecotipoService enderecotipoService) {
		this.enderecotipoService = enderecotipoService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setContatotipoService(ContatotipoService contatotipoService) {
		this.contatotipoService = contatotipoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setResponsavelFreteService(
			ResponsavelFreteService responsavelFreteService) {
		this.responsavelFreteService = responsavelFreteService;
	}
	public void setBancoService(BancoService bancoService) {
		this.bancoService = bancoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {
		this.formapagamentoService = formapagamentoService;
	}
	public void setIndicecorrecaoService(IndicecorrecaoService indicecorrecaoService) {
		this.indicecorrecaoService = indicecorrecaoService;
	}
	public void setPedidovendatipoService(
			PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setPrazopagamentoitemService(
			PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialrelacionadoService(
			MaterialrelacionadoService materialrelacionadoService) {
		this.materialrelacionadoService = materialrelacionadoService;
	}
	public void setMaterialsimilarService(
			MaterialsimilarService materialsimilarService) {
		this.materialsimilarService = materialsimilarService;
	}
	public void setMaterialtabelaprecoService(
			MaterialtabelaprecoService materialtabelaprecoService) {
		this.materialtabelaprecoService = materialtabelaprecoService;
	}
	public void setMaterialtabelaprecoitemService(
			MaterialtabelaprecoitemService materialtabelaprecoitemService) {
		this.materialtabelaprecoitemService = materialtabelaprecoitemService;
	}
	public void setMaterialtabelaprecoclienteService(
			MaterialtabelaprecoclienteService materialtabelaprecoclienteService) {
		this.materialtabelaprecoclienteService = materialtabelaprecoclienteService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setMaterialunidademedidaService(
			MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setSincronizacaotabelaandroidService(
			SincronizacaotabelaandroidService sincronizacaotabelaandroidService) {
		this.sincronizacaotabelaandroidService = sincronizacaotabelaandroidService;
	}
	public void setSincronizacaotabelaw3peuService(
			Sincronizacaotabelaw3producaoService sincronizacaotabelaw3producaoService) {
		this.sincronizacaotabelaw3producaoService = sincronizacaotabelaw3producaoService;
	}
	public void setUsuarioempresaService(UsuarioempresaService usuarioempresaService) {
		this.usuarioempresaService = usuarioempresaService;
	}
	public void setVwultimascomprasandroidService(VwultimascomprasandroidService vwultimascomprasandroidService) {
		this.vwultimascomprasandroidService = vwultimascomprasandroidService;
	}
	
	public void setMaterialproducaoService(
			MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setProducaoetapaService(
			ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setProducaoetapaitemService(
			ProducaoetapaitemService producaoetapaitemService) {
		this.producaoetapaitemService = producaoetapaitemService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setPneumarcaService(PneumarcaService pneumarcaService) {
		this.pneumarcaService = pneumarcaService;
	}
	public void setPneumodeloService(PneumodeloService pneumodeloService) {
		this.pneumodeloService = pneumodeloService;
	}
	public void setPneumedidaService(PneumedidaService pneumedidaService) {
		this.pneumedidaService = pneumedidaService;
	}
	public void setMotivodevolucaoService(MotivodevolucaoService motivodevolucaoService) {
		this.motivodevolucaoService = motivodevolucaoService;
	}
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	public void setClienteDocumentoTipoService(
			ClienteDocumentoTipoService clienteDocumentoTipoService) {
		this.clienteDocumentoTipoService = clienteDocumentoTipoService;
	}
	public void setClientehistoricoService(
			ClientehistoricoService clientehistoricoService) {
		this.clientehistoricoService = clientehistoricoService;
	}
	public void setClientePrazoPagamentoService(
			ClientePrazoPagamentoService clientePrazoPagamentoService) {
		this.clientePrazoPagamentoService = clientePrazoPagamentoService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setMeiocontatoService(MeiocontatoService meiocontatoService) {
		this.meiocontatoService = meiocontatoService;
	}
	public void setSituacaohistoricoService(
			SituacaohistoricoService situacaohistoricoService) {
		this.situacaohistoricoService = situacaohistoricoService;
	}
	public void setLocalarmazenagemempresaService(LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {
		this.materialformulavalorvendaService = materialformulavalorvendaService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	public void setContaempresaService(ContaempresaService contaempresaService) {
		this.contaempresaService = contaempresaService;
	}
	public void setPneuqualificacaoService(PneuqualificacaoService pneuqualificacaoService) {
		this.pneuqualificacaoService = pneuqualificacaoService;
	}
	public void setGarantiatipoService(GarantiatipoService garantiatipoService) {
		this.garantiatipoService = garantiatipoService;
	}
	public void setGarantiatipopercentualService(GarantiatipopercentualService garantiatipopercentualService) {
		this.garantiatipopercentualService = garantiatipopercentualService;
	}
	
	/**
	 * <p>Gera uma lista de UsuarioPapel e seta no par�metro <i>bean</i>.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.service.PapelService#createListaUsuariopapel(List, Usuario)
	 * @param bean
	 * @author Hugo Ferreira
	 */
	public void criarListaUsuarioPapel(Usuario bean) {
		List<Papel> listaPapel = bean.getListaPapel();
		List<Usuariopapel> listaUsuariopapel = papelService.createListaUsuariopapel(listaPapel, bean);	
		bean.setListaUsuariopapel(listaUsuariopapel);
	}
	
	/**
	 * Gera uma lista de UsuarioEmpresa e seta no par�metro <i>bean</i>.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#createListaUsuarioempresa
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void criarListaUsuarioEmpresa(Usuario bean) {
		List<Empresa> listaEmpresa = bean.getListaEmpresa();
		List<Usuarioempresa> listaUsuarioempresa = empresaService.createListaUsuarioempresa(listaEmpresa,bean);
		bean.setListaUsuarioempresa(listaUsuarioempresa);		
	}
	
	/**
	 * M�todo respons�vel por criptografar a senha do usuario.
	 * Criptografa somente quando ela � alterada.
	 * Orestes 05/03/2008: ou se � registro novo
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#load(Usuario)
	 * @see #encryptPassword(String)
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	private void encryptPassword(Usuario bean){
		Usuario load = usuarioDAO.load(bean);
		if(load == null || !load.getSenha().equals(bean.getSenha())){
			bean.setSenha(this.encryptPassword(bean.getSenha()));
		}
	}
	
	/**
	 * M�todo respons�vel por criptografar a senha do usuario.
	 * @param String
	 * @author Orestes
	 */
	public String encryptPassword(String senha){
		senha = Util.crypto.makeHashMd5(senha);
		return senha;
	}
	
	
	/**
	 * M�todo de refer�ncia ao dao
	 * @param email
	 * @param login
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregarUsuarioByEmail(String)
	 * @author Jo�o Paulo Zica
	 */
	public Usuario carregarUsuarioByEmail(String email) {
		return usuarioDAO.carregarUsuarioByEmail(email);
	}
	/**
	 * M�todo de refer�ncia ao dao
	 * 
	 * @param login
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregarUsuarioByLogin(String)
	 * @author Fl�vio Tavares
	 */
	public Usuario carregarUsuarioByLogin(String login) {
		return usuarioDAO.carregarUsuarioByLogin(login);
	}

	/**
	 * M�todo para carregar as listaUsuariopapel de um usuario.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregaUsuario(Usuario)
	 * @param usuario
	 * @author Fl�vio Tavares
	 */
	public Usuario carregaUsuario(Usuario usuario){
		return usuarioDAO.carregaUsuario(usuario);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregaUsuarioWithEmpresa
	 * @param usuario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Usuario carregaUsuarioWithEmpresa(Usuario usuario){
		return usuarioDAO.carregaUsuarioWithEmpresa(usuario);
	}
	
	/**M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#findUsuariocolaborador(Integer)
	 * @param cdpessoa
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Usuario findUsuariocolaborador(Integer cdpessoa) {
		return usuarioDAO.findUsuariocolaborador(cdpessoa);
	}

	/**
	 * Envia um e-mail para a pessoa e usa o template da mensagem que est� no arquivo
	 * EnvioEmail.tpl localizado dentro do pacote "br.com.daltec.sined.template".
	 *
	 * obs: O objeto pessoa deve estar carregado com as informa��es e-mail, bloqueado e senha
	 *      dispon�veis
	 *	<p>� gerada uma nova senha aleat�ria de 6 caracteres alfanum�ricos para o usu�rio.</p>
	 *
	 * @param usuario
	 * @param remetente
	 * @see br.com.daltec.sined.util.TemplateManager
	 * @see br.com.daltec.sined.util.EmailManager
	 * @see #gerarNovaSenhaUsuario(Usuario)
	 * @author Jo�o Paulo Silva Zica
	 * 			Fl�vio Tavares
	 */
	public void enviaEmailRecuperarSenha(Usuario usuario)throws Exception {
		if(usuario == null) throw new SinedException("O objeto pessoa n�o deve ser null");
		else if(usuario.getEmail() == null) throw new SinedException("O campo E-mail n�o pode ser null");
		
		this.gerarNovaSenhaUsuario(usuario);
		
		Empresa empresa = empresaService.loadPrincipal();
		
		String template = null;
		try{
			template = new TemplateManager("/WEB-INF/template/templateEnvioEmail.tpl")
				.assign("nome", usuario.getNome())
				.assign("email", usuario.getEmail())
				.assign("senha", usuario.getSenha())
				.assign("link", SinedUtil.getUrlWithContext())
				.getTemplate();
		} catch (IOException e) {
			throw new SinedException("Erro no processamento do template.",e);
		}

		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			try {
				
				String assunto = "Envio de senha " + (SinedUtil.isSined() ? "SINED" : "W3ERP");

				Envioemail envioemail = envioemailService.registrarEnvio(empresa.getEmail(), assunto, template, Envioemailtipo.RECUPERACAO_SENHA, 
						usuario, usuario.getEmail(), usuario.getNome(), email);
				
				email
					.setFrom(empresa.getEmail())
					.setSubject(assunto)
					.setTo(usuario.getEmail())
					.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, usuario.getEmail()))
					.sendMessage();
				super.log.info("E-mail de recupera��o de senha enviado para <"+usuario.getEmail()+">");
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail.",e);
			}
	}

	/**
	 * M�todo para gerar uma nova senha para o usu�rio.
	 * 
	 * @see org.apache.commons.lang.RandomStringUtils#randomAlphanumeric(int)
	 * @param usuario
	 * @author Fl�vio Tavares
	 */
	public void gerarNovaSenhaUsuario(Usuario usuario){
		String senha = RandomStringUtils.randomAlphanumeric(10);
		usuario.setSenha(senha);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregarUsuarioByPK(Integer)
	 * @param usuario
	 */
	public Usuario carregarUsuarioByPK(Integer cdusuario){
		return usuarioDAO.carregarUsuarioByPK(cdusuario);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Dependendo do par�metro passado em encrypt, criptografa a senha antes de salvar.
	 *
	 * @see #encryptPassword(Usuario)
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#alterarSenhaUsuario
	 * @param usuario
	 * @param encrypt
	 * @author Fl�vio Tavares
	 */
	public void alterarSenhaUsuario(Usuario usuario, boolean encrypt){
		if(encrypt) this.encryptPassword(usuario);
		usuarioDAO.alterarSenhaUsuario(usuario);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#salvaUsuarioColaborador(Usuario)
	 * @param usuario
	 * @author Jo�o Paulo Zica
	 */
	public void salvaUsuarioColaborador(Usuario usuario){
		usuarioDAO.salvaUsuarioColaborador(usuario);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#deleteUsuario(Usuario)
	 * @param usuario
	 * @author Jo�o Paulo Zica
	 */
	public void deleteUsuario(Usuario usuario){
		usuarioDAO.deleteUsuario(usuario);
	}

	/**
	 * <p>M�todo de refer�ncia ao DAO.</p>
	 * <p>Fornece a senha de um usu�rio.</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregarSenha(Usuario)
	 * @param usuario
	 * @return
	 * @author Hugo Ferreira
	 */
	public String carregarSenha(Usuario usuario) {
		return usuarioDAO.carregarSenha(usuario);
	}
	/* singleton */
	private static UsuarioService instance;
	public static UsuarioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuarioService.class);
		}
		return instance;
	}
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Fornece uma lista de usu�rios do sistema.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#findAll(orderBy)
	 * @param orderBy
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Usuario> findAll(String orderBy) {
		return usuarioDAO.findAll(orderBy);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#saveOrUpdateNoUseTransaction(Usuario, boolean)
	 * @param bean
	 * @param updateSaveOrUpdate - par�metro informado para indicar a necessidade de se chamar 
	 * 			o m�todo {@link br.com.linkcom.sined.geral.dao.UsuarioDAO#updateSaveOrUpdate(br.com.linkcom.neo.persistence.SaveOrUpdateStrategy)}
	 * 			ao salvar ou atualizar um Usu�rio.
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateNoUseTransaction(Usuario bean, boolean updateSaveOrUpdate) {
		usuarioDAO.saveOrUpdateNoUseTransaction(bean, updateSaveOrUpdate);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#exiteLoginUsuario
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean exiteLoginUsuario(Fornecedor bean) {
		return usuarioDAO.exiteLoginUsuario(bean);
	}
	
	/**
	 * Verifica se o email j� est� cadastrado para um usu�rio diferente do usu�rio
	 * que est� sendo salvo.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#existeEmailCadastrado(Integer, String)
	 * @param cdUsuario
	 * 		Se n�o for recebido (null), verifica apenas se o e-mail existe no sistema.
	 * 		Caso seja recebido, verifica se o e-mail existe para um usu�rio de c�digo
	 * 		diferente do informado.
	 * @param email
	 * @return
	 * @author Hugo Ferreira
	 */
	public boolean existeEmailCadastrado(Integer cdUsuario, String email) {
		return usuarioDAO.existeEmailCadastrado(cdUsuario, email);
	}
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#findForGerenciaPlanejamento
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Usuario> findForGerenciaPlanejamento(String whereIn) {
		return usuarioDAO.findForGerenciaPlanejamento(whereIn);
	}
	
	/**
	 * Retorna usu�rio com e-mail carregado
	 * @param usuario
	 * @return
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#loadWithEmail(Usuario)
	 * @author C�ntia Nogueira
	 */
	public Usuario loadWithEmail(Usuario usuario){
		return usuarioDAO.loadWithEmail(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param usuario
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Usuario> findUsuariosDesbloqueados(Usuario usuario) {
		return usuarioDAO.findUsuariosDesbloqueados(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/03/2016
	 */
	public List<Usuario> findForAutocompleteNotBloqueados(String q){
		return usuarioDAO.findForAutocompleteNotBloqueados(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param usuario
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Usuario carregaRubricaUsuario(Usuario usuario) {
		return usuarioDAO.carregaRubricaUsuario(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#carregaImagemassinaturaemailUsuario(Usuario usuario)
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Usuario carregaImagemassinaturaemailUsuario(Usuario usuario) {
		return usuarioDAO.carregaImagemassinaturaemailUsuario(usuario);
	}
	
	public Usuario carregaUsuarioWithProjeto(Usuario usuario) {
		return usuarioDAO.carregaUsuarioWithProjeto(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Usuario> findUsuariosForBuscaGeral(String busca) {
		return usuarioDAO.findUsuariosForBuscaGeral(busca);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 21/03/2012
	 * @param usuario
	 * @return
	 */
	public Usuario getUsuarioComPapel(Usuario usuario){
		return usuarioDAO.getUsuarioComPapel(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#getLimitevaloraprovacaoautorizacao(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getLimitevaloraprovacaoautorizacao(Usuario usuario) {
		return usuarioDAO.getLimitevaloraprovacaoautorizacao(usuario);
	}
	
	/**
	 * M�todo que verifica se o valor da ordem de compra ou da conta a pagar � maior do que o limite para
	 * autoriza��o e/ou aprova��o
	 *
	 * @param whereIn
	 * @param tipo
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isValorMaiorLimitevaloraprovacaoautorizacao(String whereIn, String tipo) {
		if(tipo != null){
			Money limitevalor = this.getLimitevaloraprovacaoautorizacao(SinedUtil.getUsuarioLogado());
			Double valortotal;
			if(limitevalor != null && limitevalor.getValue().doubleValue() > 0){
				if("ordemcompra".equals(tipo)){
					List<Ordemcompra> lista = ordemcompraService.findOrdensParaAutorizar(whereIn);					
					if(lista != null && !lista.isEmpty()){
						for(Ordemcompra oc : lista){
							valortotal = oc.getValorTotalOrdemCompra();
							if(valortotal != null && limitevalor.getValue().doubleValue() < valortotal)
								return Boolean.TRUE;
						}
					}				
				}else if("contapagar".equals(tipo)){
					List<Documento> lista = contapagarService.findContas(whereIn);					
					if(lista != null && !lista.isEmpty() ){
						for(Documento d : lista){
							valortotal = d.getValorDouble();
							if(valortotal != null && limitevalor.getValue().doubleValue() < valortotal)
								return Boolean.TRUE;
						}
					}
				}
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isRestricaoClienteVendedor(Usuario usuario)
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isRestricaoClienteVendedor(Usuario usuario){
		return usuarioDAO.isRestricaoClienteVendedor(usuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#findForVerificarRestricaoClientevendedor(Usuario usuario)
	 *
	 * @param usuarioLogado
	 * @return
	 * @author Luiz Fernando
	 */
	public Usuario findForVerificarRestricaoClientevendedor(Usuario usuarioLogado) {
		return usuarioDAO.findForVerificarRestricaoClientevendedor(usuarioLogado);
	}
	
	public void updateBloqueado(Integer cdpessoa){
		usuarioDAO.updateBloqueado(cdpessoa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#findForEnvioemailcompra(String keyacao, Empresa empresa, String whereInProjeto, String whereInCdusuario)
	 *
	 * @param keyacao
	 * @param empresa
	 * @param whereInProjeto
	 * @param whereInCdusuario
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Usuario> findForEnvioemailcompra(String keyacao, Empresa empresa, String whereInProjeto, String whereInCdusuario, String notWhereInCdusuario) {
		return usuarioDAO.findForEnvioemailcompra(keyacao, empresa, whereInProjeto, whereInCdusuario, notWhereInCdusuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isRestricaoVendaVendedor(Usuario usuario)
	 *
	 * @param usuario
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isRestricaoVendaVendedor(Usuario usuario) {
		return usuarioDAO.isRestricaoVendaVendedor(usuario);
	}
	
	/**
	 * M�todo que adiciona a imagem de assinatura do usu�rio e retorna o texto a ser adicionado no corpo do email
	 *
	 * @param usuario
	 * @param emailManager
	 * @return
	 * @author Luiz Fernando
	 */
	public String addImagemassinaturaemailUsuario(Usuario usuario, EmailManager emailManager){
		boolean sucesso = false;
		String textimg = "";
		if(usuario != null && usuario.getCdpessoa() != null && emailManager != null){
			try {
				Usuario user = this.carregaImagemassinaturaemailUsuario(usuario);
				if(user != null && user.getImagemassinaturaemail() != null) {
					arquivoService.fillWithContents(user.getImagemassinaturaemail());
					String fileAttachment = ArquivoDAO.getInstance().getCaminhoArquivoCompleto(user.getImagemassinaturaemail());
					textimg = "<br><br><img src='cid:" +  user.getImagemassinaturaemail().getCdarquivo() + "'>";
					emailManager.attachFile(fileAttachment, user.getImagemassinaturaemail().getNome(), user.getImagemassinaturaemail().getContenttype(), user.getImagemassinaturaemail().getCdarquivo().toString());
					sucesso = true;
				}
			} catch (Exception e) {e.printStackTrace();}
		}
		
		return (sucesso ? textimg : "");
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#permiteAcao(Usuario usuario, String acao)
	 *
	 * @param usuario
	 * @param acao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean permiteAcao(Usuario usuario, String acao){
		return usuarioDAO.permiteAcao(usuario, acao);
	}
	
	/**
	 * M�todo que verifica se a base est� ativada para utilizar o offline
	 *
	 * @param urlWithContext
	 * @return
	 * @author Luiz Fernando
	 * @since 20/01/2014
	 */
	public Boolean existeDatasourceOffline(String urlWithContext) {
		try{
			InputStream inputstream = ServletContextListener.class.getResourceAsStream("/WEB-INF/classes/pedido_venda_offline.properties");
			
			Properties properties = new Properties();
			properties.load(inputstream);
			for (Object key : properties.keySet()){
				String banco = (String) key;
				
				if(urlWithContext.contains(banco.substring(0, banco.indexOf('.'))))
					return true;
			}
		} catch (Exception e) {}
		
		return false;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isSemprelogaroffline(Usuario usuarioLogado)
	 *
	 * @param usuarioLogado
	 * @return
	 * @author Luiz Fernando
	 * @since 20/01/2014
	 */
	public Boolean isSemprelogaroffline(Usuario usuarioLogado) {
		return usuarioDAO.isSemprelogaroffline(usuarioLogado);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isSemprelogaroffline(String login)
	 *
	 * @param login
	 * @return
	 * @author Luiz Fernando
	 * @since 22/01/2014
	 */
	public boolean isSemprelogaroffline(String login) {
		return usuarioDAO.isSemprelogaroffline(login);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isRestricaoFornecedorColaborador(Usuario usuario)
	 *
	 * @param usuario
	 * @return
	 * @author Rafael Salvio
	 */
	public Boolean isRestricaoFornecedorColaborador(Usuario usuario){
		return usuarioDAO.isRestricaoFornecedorColaborador(usuario);
	}
	
	/**
	 * Verifica se o usu�rio possui perfil de administrador.
	 * 
	 * @param cdpessoa
	 * @return
	 */
	public boolean possuiPerfilAdministrador(Integer cdpessoa){
		
		Usuario usuario = usuarioDAO.carregarUsuarioByPK(cdpessoa);
		
		if (usuario == null)
			return false;
		
		for (Usuariopapel up : usuario.getListaUsuariopapel()){
			if (up.getPapel().getAdministrador())
				return true;
		}
		
		return false;
	}
	
	/**
	* Retorno o usuario se o hash de autentica��o do android for compativel
	*/
	public Usuario loadUsuarioByHashAndroid(Integer cdpessoa, String token) throws NoSuchAlgorithmException {
		if(cdpessoa==null || StringUtils.isBlank(token))
			return null;
		
		Usuario usuario = usuarioDAO.load(new Usuario(cdpessoa));
		String hash = SHA512Util.encrypt(usuario.getLogin() + "|#$" + usuario.getPassword());
		if(hash.equals(token)){
			return usuario;
		}
		return null;
	}
	
	public List<UsuarioRESTModel> findForAndroid(String whereInUsuario) {
		List<UsuarioRESTModel> lista = new ArrayList<UsuarioRESTModel>();
		if(StringUtils.isNotEmpty(whereInUsuario)){
			for(Usuario bean : usuarioDAO.findForAndroid(whereInUsuario))
				lista.add(new UsuarioRESTModel(bean));
		}
		return lista;
	}
	
	public DownloadBeanRESTModel getDownloadBeanForAndroid(DownloadBeanRESTWSBean command) {
		DownloadBeanRESTModel model = new DownloadBeanRESTModel();
		
		model.setDataUltimaSincronizacao(System.currentTimeMillis());
		HashMap<Tabelaandroid, String> mapTabelaWhereIn = sincronizacaotabelaandroidService.getMaptabela(command, model, false);
		HashMap<Tabelaandroid, String> mapTabelaExcluidoWhereIn = sincronizacaotabelaandroidService.getMaptabela(command, null, true);
		model.setDataUltimaSincronizacao(model.getDataUltimaSincronizacao() + 1);
		
		boolean considerarSomenteTabelapreco = command.getConsiderarSomenteTabelapreco() != null && command.getConsiderarSomenteTabelapreco();
		boolean considerarSomenteTabelaclientevendedor = command.getConsiderarSomenteTabelaclientevendedor() != null && command.getConsiderarSomenteTabelaclientevendedor();
		boolean considerarSomenteUltimascompras = command.getConsiderarSomenteUltimascompras() != null && command.getConsiderarSomenteUltimascompras();
		boolean sincronizacaoInicial = false;
		
		List<CategoriaRESTModel> listaCategoria = categoriaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CATEGORIA), sincronizacaoInicial);
		model.setCategoriaRESTModelArray(listaCategoria != null ? listaCategoria.toArray(new CategoriaRESTModel[listaCategoria.size()]) : null);
		model.setWhereInExcluidoCategoria(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CATEGORIA));		
		
		List<ParametrogeralRESTModel> listaParametrogeral = parametrogeralService.findForAndroid();
		model.setParametrogeralRESTModelArray(listaParametrogeral != null ? listaParametrogeral.toArray(new ParametrogeralRESTModel[listaParametrogeral.size()]) : null);
		
		List<EnderecotipoRESTModel> listaEnderecotipoRESTModel = enderecotipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.ENDERECOTIPO), sincronizacaoInicial);
		model.setEnderecotipoRESTModelArray(listaEnderecotipoRESTModel != null ? listaEnderecotipoRESTModel.toArray(new EnderecotipoRESTModel[listaEnderecotipoRESTModel.size()]) : null);
		model.setWhereInExcluidoEnderecotipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.ENDERECOTIPO));
		
		List<MaterialgrupoRESTModel> listaMaterialgrupoRESTModel = materialgrupoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALGRUPO), sincronizacaoInicial);
		model.setMaterialgrupoRESTModelArray(listaMaterialgrupoRESTModel != null ? listaMaterialgrupoRESTModel.toArray(new MaterialgrupoRESTModel[listaMaterialgrupoRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialgrupo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALGRUPO));
		
		List<UnidademedidaRESTModel> listaUnidademedidaRESTModel = unidademedidaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.UNIDADEMEDIDA), sincronizacaoInicial);
		model.setUnidademedidaRESTModelArray(listaUnidademedidaRESTModel != null ? listaUnidademedidaRESTModel.toArray(new UnidademedidaRESTModel[listaUnidademedidaRESTModel.size()]) : null);
		model.setWhereInExcluidoUnidademedida(mapTabelaExcluidoWhereIn.get(Tabelaandroid.UNIDADEMEDIDA));
		
		List<UfRESTModel> listaUfRESTModel = ufService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.UF), sincronizacaoInicial);
		model.setUfRESTModelArray(listaUfRESTModel != null ? listaUfRESTModel.toArray(new UfRESTModel[listaUfRESTModel.size()]) : null);
		model.setWhereInExcluidoUf(mapTabelaExcluidoWhereIn.get(Tabelaandroid.UF));
		
		List<MunicipioRESTModel> listaMunicipioRESTModel = municipioService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MUNICIPIO), sincronizacaoInicial);
		model.setMunicipioRESTModelArray(listaMunicipioRESTModel != null ? listaMunicipioRESTModel.toArray(new MunicipioRESTModel[listaMunicipioRESTModel.size()]) : null);
		model.setWhereInExcluidoMunicipio(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MUNICIPIO));
		
		List<ContatotipoRESTModel> listaContatotipoRESTModel = contatotipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CONTATOTIPO), sincronizacaoInicial);
		model.setContatotipoRESTModelArray(listaContatotipoRESTModel != null ? listaContatotipoRESTModel.toArray(new ContatotipoRESTModel[listaContatotipoRESTModel.size()]) : null);
		model.setWhereInExcluidoContatotipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CONTATOTIPO));
		
		List<ColaboradorRESTModel> listaColaboradorRESTModel = colaboradorService.findForAndroid(sincronizacaoInicial  || considerarSomenteTabelaclientevendedor ? null : mapTabelaWhereIn.get(Tabelaandroid.COLABORADOR), sincronizacaoInicial || considerarSomenteTabelaclientevendedor);
		model.setColaboradorRESTModelArray(listaColaboradorRESTModel != null ? listaColaboradorRESTModel.toArray(new ColaboradorRESTModel[listaColaboradorRESTModel.size()]) : null);
		model.setWhereInExcluidoColaborador(mapTabelaExcluidoWhereIn.get(Tabelaandroid.COLABORADOR));
		
		List<FornecedorRESTModel> listaFornecedorRESTModel = fornecedorService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.FORNECEDOR), sincronizacaoInicial);
		model.setFornecedorRESTModelArray(listaFornecedorRESTModel != null ? listaFornecedorRESTModel.toArray(new FornecedorRESTModel[listaFornecedorRESTModel.size()]) : null);
		model.setWhereInExcluidoFornecedor(mapTabelaExcluidoWhereIn.get(Tabelaandroid.FORNECEDOR));
		
		List<EmpresaRESTModel> listaEmpresaRESTModel = empresaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.EMPRESA), sincronizacaoInicial);
		model.setEmpresaRESTModelArray(listaEmpresaRESTModel != null ? listaEmpresaRESTModel.toArray(new EmpresaRESTModel[listaEmpresaRESTModel.size()]) : null);
		model.setWhereInExcluidoEmpresa(mapTabelaExcluidoWhereIn.get(Tabelaandroid.EMPRESA));
		
		List<ContaempresaRESTModel> listaContaempresaRESTModel = contaempresaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CONTAEMPRESA), sincronizacaoInicial);
		model.setContaempresaRESTModelArray(listaContaempresaRESTModel != null ? listaContaempresaRESTModel.toArray(new ContaempresaRESTModel[listaContaempresaRESTModel.size()]) : null);
		model.setWhereInExcluidoConta(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CONTAEMPRESA));
		
		List<DocumentoRESTModel> listaDocumentoRESTModel = documentoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.DOCUMENTO), sincronizacaoInicial);
		model.setDocumentoRESTModelArray(listaDocumentoRESTModel != null ? listaDocumentoRESTModel.toArray(new DocumentoRESTModel[listaDocumentoRESTModel.size()]) : null);
		model.setWhereInExcluidoDocumento(mapTabelaExcluidoWhereIn.get(Tabelaandroid.DOCUMENTO));
		
		String whereInCdpessoa = null;
		if(SinedUtil.isListNotEmpty(listaDocumentoRESTModel)){
			whereInCdpessoa = CollectionsUtil.listAndConcatenate(listaDocumentoRESTModel, "cdpessoa", ",");
			if(StringUtils.isNotBlank(whereInCdpessoa)){
				String whereIn = mapTabelaWhereIn.get(Tabelaandroid.CLIENTE);
				if(StringUtils.isNotBlank(whereIn)){
					whereInCdpessoa += "," + whereIn;
				}
				mapTabelaWhereIn.put(Tabelaandroid.CLIENTE, whereInCdpessoa);
			}
		}
		
		List<ClienteRESTModel> listaClienteRESTModel = clienteService.findForAndroid(sincronizacaoInicial || considerarSomenteTabelaclientevendedor ? null : mapTabelaWhereIn.get(Tabelaandroid.CLIENTE), sincronizacaoInicial || considerarSomenteTabelaclientevendedor );
		model.setClienteRESTModelArray(listaClienteRESTModel != null ? listaClienteRESTModel.toArray(new ClienteRESTModel[listaClienteRESTModel.size()]) : null);
		model.setWhereInExcluidoCliente(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CLIENTE));
		
		List<EnderecoRESTModel> listaEnderecoRESTModel = enderecoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.ENDERECO), sincronizacaoInicial);
		model.setEnderecoRESTModelArray(listaEnderecoRESTModel != null ? listaEnderecoRESTModel.toArray(new EnderecoRESTModel[listaEnderecoRESTModel.size()]) : null);
		model.setWhereInExcluidoEndereco(mapTabelaExcluidoWhereIn.get(Tabelaandroid.ENDERECO));
		
		List<ContatoRESTModel> listaContatoRESTModel = contatoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CONTATO), sincronizacaoInicial);
		model.setContatoRESTModelArray(listaContatoRESTModel != null ? listaContatoRESTModel.toArray(new ContatoRESTModel[listaContatoRESTModel.size()]) : null);
		model.setWhereInExcluidoContato(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CONTATO));
		
		List<ResponsavelFreteRESTModel> listaResponsavelFreteRESTModel = responsavelFreteService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.RESPONSAVELFRETE), sincronizacaoInicial);
		model.setResponsavelFreteRESTModelArray(listaResponsavelFreteRESTModel != null ? listaResponsavelFreteRESTModel.toArray(new ResponsavelFreteRESTModel[listaResponsavelFreteRESTModel.size()]) : null);
		model.setWhereInExcluidoResponsavelFrete(mapTabelaExcluidoWhereIn.get(Tabelaandroid.RESPONSAVELFRETE));
		
		List<BancoRESTModel> listaBancoRESTModel = bancoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.BANCO), sincronizacaoInicial);
		model.setBancoRESTModelArray(listaBancoRESTModel != null ? listaBancoRESTModel.toArray(new BancoRESTModel[listaBancoRESTModel.size()]) : null);
		model.setWhereInExcluidoBanco(mapTabelaExcluidoWhereIn.get(Tabelaandroid.BANCO));
		
		List<ContaRESTModel> listaContaRESTModel = contaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CONTA), sincronizacaoInicial);
		model.setContaRESTModelArray(listaContaRESTModel != null ? listaContaRESTModel.toArray(new ContaRESTModel[listaContaRESTModel.size()]) : null);
		model.setWhereInExcluidoConta(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CONTA));
		
		List<DocumentotipoRESTModel> listaDocumentotipoRESTModel = documentotipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.DOCUMENTOTIPO), sincronizacaoInicial);
		model.setDocumentotipoRESTModelArray(listaDocumentotipoRESTModel != null ? listaDocumentotipoRESTModel.toArray(new DocumentotipoRESTModel[listaDocumentotipoRESTModel.size()]) : null);
		model.setWhereInExcluidoDocumentotipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.DOCUMENTOTIPO));
		
		List<FormapagamentoRESTModel> listaFormapagamentoRESTModel = formapagamentoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.FORMAPAGAMENTO), sincronizacaoInicial);
		model.setFormapagamentoRESTModelArray(listaFormapagamentoRESTModel != null ? listaFormapagamentoRESTModel.toArray(new FormapagamentoRESTModel[listaFormapagamentoRESTModel.size()]) : null);
		model.setWhereInExcluidoFormapagamento(mapTabelaExcluidoWhereIn.get(Tabelaandroid.FORMAPAGAMENTO));
		
		List<IndicecorrecaoRESTModel> listaIndicecorrecaoRESTModel = indicecorrecaoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.INDICECORRECAO), sincronizacaoInicial);
		model.setIndicecorrecaoRESTModelArray(listaIndicecorrecaoRESTModel != null ? listaIndicecorrecaoRESTModel.toArray(new IndicecorrecaoRESTModel[listaIndicecorrecaoRESTModel.size()]) : null);
		model.setWhereInExcluidoIndicecorrecao(mapTabelaExcluidoWhereIn.get(Tabelaandroid.INDICECORRECAO));
		
		List<ProjetoRESTModel> listaProjetoRESTModel = projetoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PROJETO), sincronizacaoInicial);
		model.setProjetoRESTModelArray(listaProjetoRESTModel != null ? listaProjetoRESTModel.toArray(new ProjetoRESTModel[listaProjetoRESTModel.size()]) : null);
		model.setWhereInExcluidoProjeto(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PROJETO));
		
		List<PedidovendatipoRESTModel> listaPedidovendatipoRESTModel = pedidovendatipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PEDIDOVENDATIPO), sincronizacaoInicial);
		model.setPedidovendatipoRESTModelArray(listaPedidovendatipoRESTModel != null ? listaPedidovendatipoRESTModel.toArray(new PedidovendatipoRESTModel[listaPedidovendatipoRESTModel.size()]) : null);
		model.setWhereInExcluidoPedidovendatipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PEDIDOVENDATIPO));
		
		List<PrazopagamentoRESTModel> listaPrazopagamentoRESTModel = prazopagamentoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PRAZOPAGAMENTO), sincronizacaoInicial);
		model.setPrazopagamentoRESTModelArray(listaPrazopagamentoRESTModel != null ? listaPrazopagamentoRESTModel.toArray(new PrazopagamentoRESTModel[listaPrazopagamentoRESTModel.size()]) : null);
		model.setWhereInExcluidoPrazopagamento(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PRAZOPAGAMENTO));
		
		List<PrazopagamentoitemRESTModel> listaPrazopagamentoitemRESTModel = prazopagamentoitemService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PRAZOPAGAMENTOITEM), sincronizacaoInicial);
		model.setPrazopagamentoitemRESTModelArray(listaPrazopagamentoitemRESTModel != null ? listaPrazopagamentoitemRESTModel.toArray(new PrazopagamentoitemRESTModel[listaPrazopagamentoitemRESTModel.size()]) : null);
		model.setWhereInExcluidoPrazopagamentoitem(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PRAZOPAGAMENTOITEM));
		
		List<MaterialRESTModel> listaMaterialRESTModel = materialService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIAL), sincronizacaoInicial);
		model.setMaterialRESTModelArray(listaMaterialRESTModel != null ? listaMaterialRESTModel.toArray(new MaterialRESTModel[listaMaterialRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterial(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIAL));
		
		List<MaterialproducaoRESTModel> listaMaterialproducaoRESTModel = materialproducaoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALPRODUCAO), sincronizacaoInicial);
		model.setMaterialproducaoRESTModelArray(listaMaterialproducaoRESTModel != null ? listaMaterialproducaoRESTModel.toArray(new MaterialproducaoRESTModel[listaMaterialproducaoRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialproducao(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALPRODUCAO));
		
		List<MaterialrelacionadoRESTModel> listaMaterialrelacionadoRESTModel = materialrelacionadoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALRELACIONADO), sincronizacaoInicial);
		model.setMaterialrelacionadoRESTModelArray(listaMaterialrelacionadoRESTModel != null ? listaMaterialrelacionadoRESTModel.toArray(new MaterialrelacionadoRESTModel[listaMaterialrelacionadoRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialrelacionado(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALRELACIONADO));
		
		List<MaterialsimilarRESTModel> listaMaterialsimilarRESTModel = materialsimilarService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALSIMILAR), sincronizacaoInicial);
		model.setMaterialsimilarRESTModelArray(listaMaterialsimilarRESTModel != null ? listaMaterialsimilarRESTModel.toArray(new MaterialsimilarRESTModel[listaMaterialsimilarRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialsimilar(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALSIMILAR));
		
		List<MaterialunidademedidaRESTModel> listaMaterialunidademedidaRESTModel = materialunidademedidaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALUNIDADEMEDIDA), sincronizacaoInicial);
		model.setMaterialunidademedidaRESTModelArray(listaMaterialunidademedidaRESTModel != null ? listaMaterialunidademedidaRESTModel.toArray(new MaterialunidademedidaRESTModel[listaMaterialunidademedidaRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialunidademedida(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALUNIDADEMEDIDA));
		
		List<MaterialtabelaprecoRESTModel> listaMaterialtabelaprecoRESTModel = materialtabelaprecoService.findForAndroid(sincronizacaoInicial || considerarSomenteTabelapreco ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALTABELAPRECO), sincronizacaoInicial || considerarSomenteTabelapreco);
		model.setMaterialtabelaprecoRESTModelArray(listaMaterialtabelaprecoRESTModel != null ? listaMaterialtabelaprecoRESTModel.toArray(new MaterialtabelaprecoRESTModel[listaMaterialtabelaprecoRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialtabelapreco(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALTABELAPRECO));
		
		List<MaterialtabelaprecoitemRESTModel> listaMaterialtabelaprecoitemRESTModel = materialtabelaprecoitemService.findForAndroid(sincronizacaoInicial || considerarSomenteTabelapreco ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOITEM), sincronizacaoInicial || considerarSomenteTabelapreco);
		model.setMaterialtabelaprecoitemRESTModelArray(listaMaterialtabelaprecoitemRESTModel != null ? listaMaterialtabelaprecoitemRESTModel.toArray(new MaterialtabelaprecoitemRESTModel[listaMaterialtabelaprecoitemRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialtabelaprecoitem(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOITEM));
		
		List<MaterialtabelaprecoclienteRESTModel> listaMaterialtabelaprecoclienteRESTModel = materialtabelaprecoclienteService.findForAndroid(sincronizacaoInicial || considerarSomenteTabelapreco ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOCLIENTE), sincronizacaoInicial || considerarSomenteTabelapreco);
		model.setMaterialtabelaprecoclienteRESTModelArray(listaMaterialtabelaprecoclienteRESTModel != null ? listaMaterialtabelaprecoclienteRESTModel.toArray(new MaterialtabelaprecoclienteRESTModel[listaMaterialtabelaprecoclienteRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialtabelaprecocliente(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOCLIENTE));
		
		List<MaterialtabelaprecoempresaRESTModel> listaMaterialtabelaprecoempresaRESTModel = materialtabelaprecoempresaService.findForAndroid(sincronizacaoInicial || considerarSomenteTabelapreco ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOEMPRESA), sincronizacaoInicial || considerarSomenteTabelapreco);
		model.setMaterialtabelaprecoempresaRESTModelArray(listaMaterialtabelaprecoempresaRESTModel != null ? listaMaterialtabelaprecoempresaRESTModel.toArray(new MaterialtabelaprecoempresaRESTModel[listaMaterialtabelaprecoempresaRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialtabelaprecoempresa(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALTABELAPRECOEMPRESA));
		
		List<MaterialformulavalorvendaRESTModel> listaMaterialformulavalorvendaRESTModel = materialformulavalorvendaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALFORMULAVALORVENDA), sincronizacaoInicial);
		model.setMaterialformulavalorvendaRESTModelArray(listaMaterialformulavalorvendaRESTModel != null ? listaMaterialformulavalorvendaRESTModel.toArray(new MaterialformulavalorvendaRESTModel[listaMaterialformulavalorvendaRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialformulavalorvenda(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALFORMULAVALORVENDA));
		
		List<ValecompraRESTModel> listaValecompraRESTModel = valecompraService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.VALECOMPRA), sincronizacaoInicial);
		model.setValecompraRESTModelArray(listaValecompraRESTModel != null ? listaValecompraRESTModel.toArray(new ValecompraRESTModel[listaValecompraRESTModel.size()]) : null);
		model.setWhereInExcluidoValecompra(mapTabelaExcluidoWhereIn.get(Tabelaandroid.VALECOMPRA));
		
		List<UsuarioRESTModel> listaUsuarioRESTModel = findForAndroid(command.getWhereInUsuarioAndroid());
		model.setUsuarioRESTModelArray(listaUsuarioRESTModel != null ? listaUsuarioRESTModel.toArray(new UsuarioRESTModel[listaUsuarioRESTModel.size()]) : null);
		
		List<UsuarioempresaRESTModel> listaUsuarioempresaRESTModel = usuarioempresaService.findForAndroid(mapTabelaWhereIn.get(Tabelaandroid.USUARIOEMPRESA), false, command.getWhereInUsuarioAndroid());
		model.setUsuarioempresaRESTModelArray(listaUsuarioempresaRESTModel != null ? listaUsuarioempresaRESTModel.toArray(new UsuarioempresaRESTModel[listaUsuarioempresaRESTModel.size()]) : null);
		model.setWhereInExcluidoUsuarioempresa(mapTabelaExcluidoWhereIn.get(Tabelaandroid.USUARIOEMPRESA));
		
		List<ClientevendedorRESTModel> listaClientevendedorRESTModel = clientevendedorService.findForAndroid(sincronizacaoInicial  || considerarSomenteTabelaclientevendedor ? null : mapTabelaWhereIn.get(Tabelaandroid.CLIENTEVENDEDOR), sincronizacaoInicial || considerarSomenteTabelaclientevendedor );
		model.setClientevendedorRESTModelArray(listaClientevendedorRESTModel != null ? listaClientevendedorRESTModel.toArray(new ClientevendedorRESTModel[listaClientevendedorRESTModel.size()]) : null);
		model.setWhereInExcluidoClientevendedor(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CLIENTEVENDEDOR));
		
		List<PedidovendahistoricoRESTModel> listaPedidovendahistoricoRESTModel = pedidovendahistoricoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PEDIDOVENDAHISTORICO), sincronizacaoInicial, command.getWhereInPedidovendaAndroid());
		model.setPedidovendahistoricoRESTModelArray(listaPedidovendahistoricoRESTModel != null ? listaPedidovendahistoricoRESTModel.toArray(new PedidovendahistoricoRESTModel[listaPedidovendahistoricoRESTModel.size()]) : null);
		model.setWhereInExcluidoPedidovendahistorico(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PEDIDOVENDAHISTORICO));
		
		List<VwultimascomprasandroidRESTModel> listaVwultimascomprasandroidRESTModel = vwultimascomprasandroidService.findForAndroid(command.getDataUltimaAtualizacaoMenosUmaHora() != null && !considerarSomenteUltimascompras ? new Timestamp(command.getDataUltimaAtualizacaoMenosUmaHora()) : null);
		model.setVwultimascomprasandroidRESTModelArray(listaVwultimascomprasandroidRESTModel != null ? listaVwultimascomprasandroidRESTModel.toArray(new VwultimascomprasandroidRESTModel[listaVwultimascomprasandroidRESTModel.size()]) : null);
		
		List<AtividadetipoRESTModel> listaAtividadetipoRESTModel = atividadetipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.ATIVIDADETIPO), sincronizacaoInicial);
		model.setAtividadetipoRESTModelArray(listaAtividadetipoRESTModel != null ? listaAtividadetipoRESTModel.toArray(new AtividadetipoRESTModel[listaAtividadetipoRESTModel.size()]) : null);
		model.setWhereInExcluidoAtividadetipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.ATIVIDADETIPO));

		List<ClientedocumentotipoRESTModel> listaClientedocumentotipoRESTModel = clienteDocumentoTipoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CLIENTEDOCUMENTOTIPO), sincronizacaoInicial);
		model.setClientedocumentotipoRESTModelArray(listaClientedocumentotipoRESTModel != null ? listaClientedocumentotipoRESTModel.toArray(new ClientedocumentotipoRESTModel[listaClientedocumentotipoRESTModel.size()]) : null);
		model.setWhereInExcluidoClientedocumentotipo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CLIENTEDOCUMENTOTIPO));

		List<ClientehistoricoRESTModel> listaClientehistoricoRESTModel = clientehistoricoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CLIENTEHISTORICO), sincronizacaoInicial);
		model.setClientehistoricoRESTModelArray(listaClientehistoricoRESTModel != null ? listaClientehistoricoRESTModel.toArray(new ClientehistoricoRESTModel[listaClientehistoricoRESTModel.size()]) : null);
		model.setWhereInExcluidoClientehistorico(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CLIENTEHISTORICO));

		List<ClienteprazopagamentoRESTModel> listaClienteprazopagamentoRESTModel = clientePrazoPagamentoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.CLIENTEPRAZOPAGAMENTO), sincronizacaoInicial);
		model.setClienteprazopagamentoRESTModelArray(listaClienteprazopagamentoRESTModel != null ? listaClienteprazopagamentoRESTModel.toArray(new ClienteprazopagamentoRESTModel[listaClienteprazopagamentoRESTModel.size()]) : null);
		model.setWhereInExcluidoClienteprazopagamento(mapTabelaExcluidoWhereIn.get(Tabelaandroid.CLIENTEPRAZOPAGAMENTO));

		List<DocumentohistoricoRESTModel> listaDocumentohistoricoRESTModel = documentohistoricoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.DOCUMENTOHISTORICO), sincronizacaoInicial);
		model.setDocumentohistoricoRESTModelArray(listaDocumentohistoricoRESTModel != null ? listaDocumentohistoricoRESTModel.toArray(new DocumentohistoricoRESTModel[listaDocumentohistoricoRESTModel.size()]) : null);
		model.setWhereInExcluidoDocumentohistorico(mapTabelaExcluidoWhereIn.get(Tabelaandroid.DOCUMENTOHISTORICO));

		List<LocalarmazenagemRESTModel> listaLocalarmazenagemRESTModel = localarmazenagemService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.LOCALARMAZENAGEM), sincronizacaoInicial);
		model.setLocalarmazenagemRESTModelArray(listaLocalarmazenagemRESTModel != null ? listaLocalarmazenagemRESTModel.toArray(new LocalarmazenagemRESTModel[listaLocalarmazenagemRESTModel.size()]) : null);
		model.setWhereInExcluidoLocalarmazenagem(mapTabelaExcluidoWhereIn.get(Tabelaandroid.LOCALARMAZENAGEM));

		List<MeiocontatoRESTModel> listaMeiocontatoRESTModel = meiocontatoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MEIOCONTATO), sincronizacaoInicial);
		model.setMeiocontatoRESTModelArray(listaMeiocontatoRESTModel != null ? listaMeiocontatoRESTModel.toArray(new MeiocontatoRESTModel[listaMeiocontatoRESTModel.size()]) : null);
		model.setWhereInExcluidoMeiocontato(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MEIOCONTATO));

		List<PneuRESTModel> listaPneuRESTModel = pneuService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PNEU), sincronizacaoInicial);
		model.setPneuRESTModelArray(listaPneuRESTModel != null ? listaPneuRESTModel.toArray(new PneuRESTModel[listaPneuRESTModel.size()]) : null);
		model.setWhereInExcluidoPneu(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PNEU));

		List<PneumarcaRESTModel> listaPneumarcaRESTModel = pneumarcaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PNEUMARCA), sincronizacaoInicial);
		model.setPneumarcaRESTModelArray(listaPneumarcaRESTModel != null ? listaPneumarcaRESTModel.toArray(new PneumarcaRESTModel[listaPneumarcaRESTModel.size()]) : null);
		model.setWhereInExcluidoPneumarca(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PNEUMARCA));

		List<PneumedidaRESTModel> listaPneumedidaRESTModel = pneumedidaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PNEUMEDIDA), sincronizacaoInicial);
		model.setPneumedidaRESTModelArray(listaPneumedidaRESTModel != null ? listaPneumedidaRESTModel.toArray(new PneumedidaRESTModel[listaPneumedidaRESTModel.size()]) : null);
		model.setWhereInExcluidoPneumedida(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PNEUMEDIDA));

		List<PneumodeloRESTModel> listaPneumodeloRESTModel = pneumodeloService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.PNEUMODELO), sincronizacaoInicial);
		model.setPneumodeloRESTModelArray(listaPneumodeloRESTModel != null ? listaPneumodeloRESTModel.toArray(new PneumodeloRESTModel[listaPneumodeloRESTModel.size()]) : null);
		model.setWhereInExcluidoPneumodelo(mapTabelaExcluidoWhereIn.get(Tabelaandroid.PNEUMODELO));

		List<TabelavalorRESTModel> listaTabelavalorRESTModel = tabelavalorService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.TABELAVALOR), sincronizacaoInicial);
		model.setTabelavalorRESTModelArray(listaTabelavalorRESTModel != null ? listaTabelavalorRESTModel.toArray(new TabelavalorRESTModel[listaTabelavalorRESTModel.size()]) : null);
		model.setWhereInExcluidoTabelavalor(mapTabelaExcluidoWhereIn.get(Tabelaandroid.TABELAVALOR));
		
		List<SituacaohistoricoRESTModel> listaSituacaohistoricoRESTModel = situacaohistoricoService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.SITUACAOHISTORICO), sincronizacaoInicial);
		model.setSituacaohistoricoRESTModelArray(listaSituacaohistoricoRESTModel != null ? listaSituacaohistoricoRESTModel.toArray(new SituacaohistoricoRESTModel[listaSituacaohistoricoRESTModel.size()]) : null);
		model.setWhereInExcluidoSituacaohistorico(mapTabelaExcluidoWhereIn.get(Tabelaandroid.SITUACAOHISTORICO));
		
		List<LocalarmazenagemempresaRESTModel> listaLocalarmazenagemempresaRESTModel = localarmazenagemempresaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.LOCALARMAZENAGEMEMPRESA), sincronizacaoInicial);
		model.setLocalarmazenagemempresaRESTModelArray(listaLocalarmazenagemempresaRESTModel != null ? listaLocalarmazenagemempresaRESTModel.toArray(new LocalarmazenagemempresaRESTModel[listaLocalarmazenagemempresaRESTModel.size()]) : null);
		model.setWhereInExcluidoLocalarmazenagemempresa(mapTabelaExcluidoWhereIn.get(Tabelaandroid.LOCALARMAZENAGEMEMPRESA));
		
		List<MaterialEmpresaRESTModel> listaMaterialEmpresaRESTModel = materialEmpresaService.findForAndroid(sincronizacaoInicial ? null : mapTabelaWhereIn.get(Tabelaandroid.MATERIALEMPRESA), sincronizacaoInicial);
		model.setMaterialEmpresaRESTModel( listaMaterialEmpresaRESTModel != null ? listaMaterialEmpresaRESTModel.toArray(new MaterialEmpresaRESTModel[listaMaterialEmpresaRESTModel.size()]) : null);
		model.setWhereInExcluidoMaterialEmpresa(mapTabelaExcluidoWhereIn.get(Tabelaandroid.MATERIALEMPRESA));
		
		boolean haveDados = false;
		for (Field field : model.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				if(!field.getName().equals("materialEmpresaRESTModel") 
						&& !field.getName().equals("parametrogeralRESTModelArray") 
						&& !field.getName().equals("vwultimascomprasandroidRESTModelArray") 
						&& !field.getName().equals("usuarioRESTModelArray") 
					){
					Object object = field.get(model);
					if (object != null && field.getType().isArray()) {
						Object[] array = (Object[]) object;
						if (array.length > 0)
							haveDados = true;
					}
					if (object != null && field.getType().getCanonicalName().equals("java.lang.String")) {
						if (StringUtils.isNotBlank(object.toString())) {
							haveDados = true;
						}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		model.setHaveDados(haveDados);
		return model;
	}
	
	public DownloadBeanW3producaoRESTModel getDownloadBeanForW3producao(DownloadBeanW3producaoRESTWSBean command) {
		DownloadBeanW3producaoRESTModel model = new DownloadBeanW3producaoRESTModel();
		
		model.setDataUltimaSincronizacao(System.currentTimeMillis());
		
		HashMap<Tabelaw3producao, String> mapTabelaWhereIn = sincronizacaotabelaw3producaoService.getMaptabela(command, false);
		HashMap<Tabelaw3producao, String> mapTabelaExcluidoWhereIn = sincronizacaotabelaw3producaoService.getMaptabela(command, true);
		
		List<UsuarioW3producaoRESTModel> listaUsuarioRESTModel = findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.USUARIO), false);
		model.setUsuarioRESTModelArray(listaUsuarioRESTModel !=null ?listaUsuarioRESTModel.toArray(new UsuarioW3producaoRESTModel[listaUsuarioRESTModel.size()]):null);
		model.setWhereInExcluidoUsuario(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.USUARIO));
		
		List<EmpresaW3producaoRESTModel> listaEmpresaRESTModel = empresaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.EMPRESA), false);
		model.setEmpresaRESTModelArray(listaEmpresaRESTModel !=null ?listaEmpresaRESTModel.toArray(new EmpresaW3producaoRESTModel[listaEmpresaRESTModel.size()]):null);
		model.setWhereInExcluidoEmpresa(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.EMPRESA));
		
		List<ClienteW3producaoRESTModel> listaClienteRESTModel = clienteService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.CLIENTE), false);
		model.setClienteRESTModelArray(listaClienteRESTModel !=null ?listaClienteRESTModel.toArray(new ClienteW3producaoRESTModel[listaClienteRESTModel.size()]):null);
		model.setWhereInExcluidoCliente(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.CLIENTE));
		
		List<MaterialW3producaoRESTModel> listaMaterialRESTModel = materialService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIAL), false);
		model.setMaterialRESTModelArray(listaMaterialRESTModel !=null ?listaMaterialRESTModel.toArray(new MaterialW3producaoRESTModel[listaMaterialRESTModel.size()]):null);
		model.setWhereInExcluidoMaterial(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIAL));
		
		List<MaterialUnidademedidaW3producaoRESTModel> listamaterialunidademedidaRESTModel = materialunidademedidaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIALUNIDADEMEDIDA), false);
		model.setMaterialUnidademedidaRESTModelArray(listamaterialunidademedidaRESTModel !=null ?listamaterialunidademedidaRESTModel.toArray(new MaterialUnidademedidaW3producaoRESTModel[listamaterialunidademedidaRESTModel.size()]):null);
		model.setWhereInExcluidoMaterialUnidademedida(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIALUNIDADEMEDIDA));
		
		List<MaterialSimilarW3producaoRESTModel> listamaterialsimilarRESTModel = materialsimilarService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIALSIMILAR), false);
		model.setMaterialSimilarRESTModelArray(listamaterialsimilarRESTModel !=null ?listamaterialsimilarRESTModel.toArray(new MaterialSimilarW3producaoRESTModel[listamaterialsimilarRESTModel.size()]):null);
		model.setWhereInExcluidoMaterialSimilar(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIALSIMILAR));
		
		List<MaterialProducaoW3producaoRESTModel> listaMaterialProducaoRESTModel = materialproducaoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIALPRODUCAO), false);
		model.setMaterialProducaoRESTModelArray(listaMaterialProducaoRESTModel !=null ?listaMaterialProducaoRESTModel.toArray(new MaterialProducaoW3producaoRESTModel[listaMaterialProducaoRESTModel.size()]):null);
		model.setWhereInExcluidoMaterialProducao(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIALPRODUCAO));
		
		List<MaterialGrupoW3producaoRESTModel> listaMaterialGrupoRESTModel = materialgrupoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIALGRUPO), false);
		model.setMaterialGrupoRESTModelArray(listaMaterialGrupoRESTModel !=null ?listaMaterialGrupoRESTModel.toArray(new MaterialGrupoW3producaoRESTModel[listaMaterialGrupoRESTModel.size()]):null);
		model.setWhereInExcluidoMaterialGrupo(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIALGRUPO));
				
		List<PatrimonioItemW3producaoRESTModel> listaPatrimonioItemRESTModel = patrimonioitemService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PATRIMONIOITEM), false);
		model.setPatrimonioItemRESTModelArray(listaPatrimonioItemRESTModel !=null ?listaPatrimonioItemRESTModel.toArray(new PatrimonioItemW3producaoRESTModel[listaPatrimonioItemRESTModel.size()]):null);
		model.setWhereInExcluidoPatrimonioItem(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PATRIMONIOITEM));
		
		List<UnidadeMedidaW3producaoRESTModel> listaUnidadeMedidaRESTModel = unidademedidaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.UNIDADEMEDIDA), false);
		model.setUnidadeMedidaRESTModelArray(listaUnidadeMedidaRESTModel !=null ?listaUnidadeMedidaRESTModel.toArray(new UnidadeMedidaW3producaoRESTModel[listaUnidadeMedidaRESTModel.size()]):null);
		model.setWhereInExcluidoUnidadeMedida(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.UNIDADEMEDIDA));
		
		List<ProducaoEtapaW3producaoRESTModel> listaProducaoEtapaRESTModel = producaoetapaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PRODUCAOETAPA), false);
		model.setProducaoEtapaRESTModelArray(listaProducaoEtapaRESTModel !=null ?listaProducaoEtapaRESTModel.toArray(new ProducaoEtapaW3producaoRESTModel[listaProducaoEtapaRESTModel.size()]):null);
		model.setWhereInExcluidoProducaoEtapa(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PRODUCAOETAPA));
		
		List<ProducaoEtapaItemW3producaoRESTModel> listaProducaoEtapaItemRESTModel = producaoetapaitemService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PRODUCAOETAPAITEM), false);
		model.setProducaoEtapaItemRESTModelArray(listaProducaoEtapaItemRESTModel !=null ?listaProducaoEtapaItemRESTModel.toArray(new ProducaoEtapaItemW3producaoRESTModel[listaProducaoEtapaItemRESTModel.size()]):null);
		model.setWhereInExcluidoProducaoEtapaItem(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PRODUCAOETAPAITEM));
		
		List<MotivoDevolucaoW3producaoRESTModel> listaMotivoDevolucaoRESTModel = motivodevolucaoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MOTIVODEVOLUCAO), false);
		model.setMotivoDevolucaoRESTModelArray(listaMotivoDevolucaoRESTModel !=null ?listaMotivoDevolucaoRESTModel.toArray(new MotivoDevolucaoW3producaoRESTModel[listaMotivoDevolucaoRESTModel.size()]):null);
		model.setWhereInExcluidoMotivoDevolucao(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MOTIVODEVOLUCAO));
		
		List<PneuW3producaoRESTModel> listaPneuRESTModel = pneuService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PNEU), false);
		model.setPneuRESTModelArray(listaPneuRESTModel !=null ?listaPneuRESTModel.toArray(new PneuW3producaoRESTModel[listaPneuRESTModel.size()]):null);
		model.setWhereInExcluidoPneu(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PNEU));
		
		List<PneuMarcaW3producaoRESTModel> listaPneuMarcaRESTModel = pneumarcaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PNEUMARCA), false);
		model.setPneuMarcaRESTModelArray(listaPneuMarcaRESTModel !=null ?listaPneuMarcaRESTModel.toArray(new PneuMarcaW3producaoRESTModel[listaPneuMarcaRESTModel.size()]):null);
		model.setWhereInExcluidoPneuMarca(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PNEUMARCA));
		
		List<PneuModeloW3producaoRESTModel> listaPneuModeloRESTModel = pneumodeloService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PNEUMODELO), false);
		model.setPneuModeloRESTModelArray(listaPneuModeloRESTModel !=null ?listaPneuModeloRESTModel.toArray(new PneuModeloW3producaoRESTModel[listaPneuModeloRESTModel.size()]):null);
		model.setWhereInExcluidoPneuModelo(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PNEUMODELO));
		
		List<PneuMedidaW3producaoRESTModel> listaPneuMedidaRESTModel = pneumedidaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PNEUMEDIDA), false);
		model.setPneuMedidaRESTModelArray(listaPneuMedidaRESTModel !=null ?listaPneuMedidaRESTModel.toArray(new PneuMedidaW3producaoRESTModel[listaPneuMedidaRESTModel.size()]):null);
		model.setWhereInExcluidoPneuMedida(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PNEUMEDIDA));
		
		List<ProducaoEtapaNomeW3producaoRESTModel> listaProducaoEtapaNomeRESTModel = producaoetapanomeService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PRODUCAOETAPANOME), false);
		model.setProducaoEtapaNomeRESTModelArray(listaProducaoEtapaNomeRESTModel !=null ?listaProducaoEtapaNomeRESTModel.toArray(new ProducaoEtapaNomeW3producaoRESTModel[listaProducaoEtapaNomeRESTModel.size()]):null);
		model.setWhereInExcluidoProducaoEtapaNome(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PRODUCAOETAPANOME));
		
		List<ProducaoEtapaNomeCampoW3producaoRESTModel> listaProducaoEtapaNomeCampoRESTModel = producaoetapanomecampoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PRODUCAOETAPANOMECAMPO), false);
		model.setProducaoEtapaNomeCampoRESTModelArray(listaProducaoEtapaNomeCampoRESTModel !=null ?listaProducaoEtapaNomeCampoRESTModel.toArray(new ProducaoEtapaNomeCampoW3producaoRESTModel[listaProducaoEtapaNomeCampoRESTModel.size()]):null);
		model.setWhereInExcluidoProducaoEtapaNomeCampo(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PRODUCAOETAPANOMECAMPO));
		
		List<PedidoVendaW3producaoRESTModel> listaPedidoVendaRESTModel = pedidovendaService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PEDIDOVENDA), false);
		model.setPedidoVendaRESTModelArray(listaPedidoVendaRESTModel !=null ?listaPedidoVendaRESTModel.toArray(new PedidoVendaW3producaoRESTModel[listaPedidoVendaRESTModel.size()]):null);
		model.setWhereInExcluidoPedidoVenda(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PEDIDOVENDA));
		
		List<MaterialPneuModeloW3producaoRESTModel> listaMaterialPneuModeloRESTModel = materialpneumodeloService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.MATERIALPNEUMODELO), false);
		model.setMaterialPneuModeloRESTModelArray(listaMaterialPneuModeloRESTModel !=null ?listaMaterialPneuModeloRESTModel.toArray(new MaterialPneuModeloW3producaoRESTModel[listaMaterialPneuModeloRESTModel.size()]):null);
		model.setWhereInExcluidoMaterialPneuModelo(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.MATERIALPNEUMODELO));
		
		List<PneuQualificacaoW3producaoRESTModel> listaPneuQualificacaoRESTModel = pneuqualificacaoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.PNEUQUALIFICACAO), false);
		model.setPneuQualificacaoRESTModelArray(listaPneuQualificacaoRESTModel !=null ? listaPneuQualificacaoRESTModel.toArray(new PneuQualificacaoW3producaoRESTModel[listaPneuQualificacaoRESTModel.size()]):null);
		model.setWhereInExcluidoPneuQualificacao(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.PNEUQUALIFICACAO));
		
		List<GarantiaTipoW3producaoRESTModel> listaGarantiaTipoRESTModel = garantiatipoService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.GARANTIATIPO), false);
		model.setGarantiaTipoRESTModelArray(listaGarantiaTipoRESTModel !=null ? listaGarantiaTipoRESTModel.toArray(new GarantiaTipoW3producaoRESTModel[listaGarantiaTipoRESTModel.size()]):null);
		model.setWhereInExcluidoGarantiaTipo(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.GARANTIATIPO));
		
		List<GarantiaTipoPercentualW3producaoRESTModel> listaGarantiaTipoPercentualRESTModel = garantiatipopercentualService.findForW3Producao(mapTabelaWhereIn.get(Tabelaw3producao.GARANTIATIPOPERCENTUAL), false);
		model.setGarantiaTipoPercentualRESTModelArray(listaGarantiaTipoPercentualRESTModel !=null ? listaGarantiaTipoPercentualRESTModel.toArray(new GarantiaTipoPercentualW3producaoRESTModel[listaGarantiaTipoPercentualRESTModel.size()]):null);
		model.setWhereInExcluidoGarantiaTipoPercentual(mapTabelaExcluidoWhereIn.get(Tabelaw3producao.GARANTIATIPOPERCENTUAL));
		String whereIn = mapTabelaWhereIn.get(Tabelaw3producao.CLIENTEVENDEDOR);
		List<ClienteVendedorW3producaoRESTModel> listaClienteVendedorRESTModel = clientevendedorService.findForW3Producao(whereIn, false);
		model.setClienteVendedorRESTModelArray(listaClienteVendedorRESTModel !=null ?listaClienteVendedorRESTModel.toArray(new ClienteVendedorW3producaoRESTModel[listaClienteVendedorRESTModel.size()]):null);
		
		return model;
	}
	
	/**
	 * Verifica se o usu�rio logado possui permiss�o em uma determinada empresa
	 * @param empresa
	 * @return
	 */
	public boolean possuiPermissaoNaEmpresa(Empresa empresa) {
		return usuarioDAO.possuiPermissaoNaEmpresa(empresa);
	}
	
	public List<UsuarioW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<UsuarioW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<UsuarioW3producaoRESTModel> lista = new ArrayList<UsuarioW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Usuario u : usuarioDAO.findForW3Producao(whereIn))
				lista.add(new UsuarioW3producaoRESTModel(u));
		}
		
		return lista;
	}
	public Usuario findById(Integer idExterno) {
		return usuarioDAO.findById(idExterno);
	}
	
	public List<Usuario> findForAviso(String whereInProjeto, String whereInEmpresa) {
		return usuarioDAO.findForAviso(whereInProjeto, whereInEmpresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UsuarioDAO#isVisualizaOutrasOportunidades(Usuario usuario)
	 *
	 * @param usuario
	 * @return
	 * @author Mairon Cezar
	 */
	public Boolean isVisualizaOutrasOportunidades(Usuario usuario){
		return usuarioDAO.isVisualizaOutrasOportunidades(usuario);
	}
	
	public boolean existeSenhaTerminal(Usuario usuario, String senhaterminal) {
		return usuarioDAO.existeSenhaTerminal(usuario, senhaterminal);
	}
	public Usuario findByCdpessoa(Integer cdpessoa) {
		return usuarioDAO.findByCdpessoa(cdpessoa);
	}
}
