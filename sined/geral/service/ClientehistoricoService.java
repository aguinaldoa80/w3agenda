package br.com.linkcom.sined.geral.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Meiocontato;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.AtividadetipoSituacaoEnum;
import br.com.linkcom.sined.geral.dao.ClientehistoricoDAO;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.ClientehistoricoVO;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.clientehistorico.ClientehistoricoRESTModel;
import br.com.linkcom.sined.util.rest.android.clientehistorico.ClientehistoricoRESTWSBean;

public class ClientehistoricoService extends GenericService<Clientehistorico>{
	private ClientehistoricoDAO clientehistoricoDAO;
	
	public void setClientehistoricoDAO(ClientehistoricoDAO clientehistoricoDAO) {
		this.clientehistoricoDAO = clientehistoricoDAO;
	}
	 
	public List<Clientehistorico> findByCliente(Cliente cliente){
		return clientehistoricoDAO.findByCliente(cliente, null, null);
	}
	
	public List<Clientehistorico> findByCliente(Cliente cliente, Atividadetipo atividadetipo){
		return clientehistoricoDAO.findByCliente(cliente, atividadetipo, null);
	}
	
	public List<Clientehistorico> findByCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa){
		return clientehistoricoDAO.findByCliente(cliente, atividadetipo, empresa);
	}
	
	/* singleton */
	private static ClientehistoricoService instance;
	public static ClientehistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClientehistoricoService.class);
		}
		return instance;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ClientehistoricoDAO#findForRelatorioHistoricoCliente(Cliente cliente, Atividadetipo atividadetipo)
	 *
	 * @param cliente
	 * @param atividadetipo
	 * @return
	 * @author Luiz Fernando
	 * @param dtfim 
	 * @param dtinicio 
	 */
	public List<Clientehistorico> findForRelatorioHistoricoCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa, Date dtinicio, Date dtfim, AtividadetipoSituacaoEnum situacao) {
		return clientehistoricoDAO.findForRelatorioHistoricoCliente(cliente, atividadetipo, empresa, dtinicio, dtfim, situacao);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.ClientehistoricoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClientehistoricoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClientehistoricoDAO#findForAndroid(String whereIn)

	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClientehistoricoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClientehistoricoRESTModel> lista = new ArrayList<ClientehistoricoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Clientehistorico bean : clientehistoricoDAO.findForAndroid(whereIn))
				lista.add(new ClientehistoricoRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	* M�todo que salva a intera��o com o cliente realizada no app
	*
	* @param command
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public ClientehistoricoRESTModel salvarClientehistoricoAndroid(ClientehistoricoRESTWSBean command) {
		ClientehistoricoRESTModel clientehistoricoRESTModel = new ClientehistoricoRESTModel();
		try {
			
			Clientehistorico bean = createClientehistoricoByAndroid(command);
			if(bean.getCdclientehistorico() ==null && bean.getCliente() != null && bean.getCliente().getCdpessoa() != null){
				saveOrUpdateWithoutLog(bean);
				
				clientehistoricoRESTModel.setCdclientehistorico(bean.getCdclientehistorico());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			String nomeMaquina = "N�o encontrado.";
			try {  
	            InetAddress localaddr = InetAddress.getLocalHost();  
	            nomeMaquina = localaddr.getHostName();  
	        } catch (UnknownHostException e3) {}  

			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			enviarEmailErro("Erro ao salvar cliente (app android). (M�quina: " + nomeMaquina + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
			
			clientehistoricoRESTModel.setErroSincronizacao(SinedUtil.FORMATADOR_DATA_HORA.format(new Timestamp(System.currentTimeMillis())) + " - " + e.getMessage());
		}
		return clientehistoricoRESTModel;
	}
	
	/**
	* M�todo que cria o hist�rico do cliente de acordo com a intera��o realizada no app
	*
	* @param command
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	private Clientehistorico createClientehistoricoByAndroid(ClientehistoricoRESTWSBean command) {
		Clientehistorico clientehistorico = new Clientehistorico();
		
		clientehistorico.setCliente(new Cliente(command.getCdcliente()));
		clientehistorico.setObservacao(command.getObservacao());
		clientehistorico.setCdusuarioaltera(command.getCdusuarioaltera() != null ? command.getCdusuarioaltera().intValue() : null);
		clientehistorico.setDtaltera(command.getDtalteracao() != null ? new Timestamp(command.getDtalteracao()) : null);

		if(command.getCdmeiocontato() != null) clientehistorico.setMeiocontato(new Meiocontato(command.getCdmeiocontato()));
		if(command.getCdatividadetipo() != null) clientehistorico.setAtividadetipo(new Atividadetipo(command.getCdatividadetipo()));
		if(command.getCdsituacaohistorico() != null) clientehistorico.setSituacaohistorico(new Situacaohistorico(command.getCdsituacaohistorico()));
		
		return clientehistorico;
	}
	
	private void enviarEmailErro(String mensagem) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject("[W3ERP] Erro ao sincronizar intera��o de cliente.")
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
	
	public List<ClientehistoricoVO> getClientehistoricoVO(List<Clientehistorico> listaCliente) {
		List<ClientehistoricoVO> lista = new ArrayList<ClientehistoricoVO>();
		if(SinedUtil.isListNotEmpty(listaCliente)){
			for(Clientehistorico ch : listaCliente){
				lista.add(new ClientehistoricoVO(ch));
			}
		}
		return lista;
	}
}
