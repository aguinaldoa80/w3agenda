package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteSegmento;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendavalorcampoextra;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.IntegracaoVipalFiltro;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.CampoAdicionalReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ClienteReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.PedidoVendaMaterialReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.PedidoVendaReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ProducaoAgendaMaterialReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ProducaoPneuReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendamaterialService extends GenericService<Producaoagendamaterial> {
	
	private static final String SEPARADOR_VIPAL = ";";

	private ProducaoagendamaterialDAO producaoagendamaterialDAO;
	private ProducaoordemService producaoordemService;
	private ReportTemplateService reportTemplateService;
	private ProducaoetapaService producaoEtapaService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private PedidovendamaterialService pedidovendamaterialService;
	
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setProducaoEtapaService(ProducaoetapaService producaoEtapaService) {this.producaoEtapaService = producaoEtapaService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setProducaoordemService(ProducaoordemService producaoordemService) {this.producaoordemService = producaoordemService;}
	public void setProducaoagendamaterialDAO(ProducaoagendamaterialDAO producaoagendamaterialDAO) {this.producaoagendamaterialDAO = producaoagendamaterialDAO;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO#findByProducaoagenda(String whereIn)
	 *
	 * @param whereInProducaoagenda
	 * @param whereInProducaoagendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagendamaterial> findByProducaoagenda(String whereInProducaoagenda, String whereInProducaoagendamaterial) {
		return producaoagendamaterialDAO.findByProducaoagenda(whereInProducaoagenda, whereInProducaoagendamaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO#findByProducaoagenda(Producaoagenda producaoagenda)
	 *
	 * @param producaoagenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Producaoagendamaterial> findByProducaoagenda(Producaoagenda producaoagenda) {
		return producaoagendamaterialDAO.findByProducaoagenda(producaoagenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO#findForDelete(Producaoagenda producaoagenda, String whereIn)
	*
	* @param producaoagenda
	* @param whereIn
	* @return
	* @since 10/03/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterial> findForDelete(Producaoagenda producaoagenda, String whereIn) {
		return producaoagendamaterialDAO.findForDelete(producaoagenda, whereIn);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO#findByPedidovendamaterial(String whereInPedidovendamaterial)
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public List<Producaoagendamaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		return producaoagendamaterialDAO.findByPedidovendamaterial(whereInPedidovendamaterial);
	}
	
	/**
	* M�todo que verifica se existe item do pedido de venda sem registro de coleta
	*
	* @see br.com.linkcom.sined.geral.service.ColetaMaterialService#findByPedidovendamaterial(String whereInPedidovendamaterial)
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public boolean existeItemSemProducao(String whereInPedidovendamaterial) {
		boolean itemSemProducao = false;
		if(StringUtils.isNotEmpty(whereInPedidovendamaterial)){
			List<Producaoagendamaterial> listaProducaoagendamaterial = findByPedidovendamaterial(whereInPedidovendamaterial);
			if(SinedUtil.isListNotEmpty(listaProducaoagendamaterial)){
				for(String id : whereInPedidovendamaterial.split(",")){
					boolean achou = false;
					for(Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial){
						if(producaoagendamaterial.getPedidovendamaterial() != null && 
								producaoagendamaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null &&
								id.equals(producaoagendamaterial.getPedidovendamaterial().getCdpedidovendamaterial().toString())){
							achou = true;
							break;
						}
					}
					if(!achou){
						itemSemProducao = true;
						break;
					}
				}
			}else {
				itemSemProducao = true;
			}
		}
		return itemSemProducao;
	}
	public LinkedList<ReportTemplateBean> criarDadosTemplateRelatorio(LinkedList<ReportTemplateBean> lista,String whereInAgenda,String whereInPneu) {
			
		List<Producaoagendamaterial> listaItens =  this.findByAgendaAndPneu(whereInAgenda,whereInPneu);
		
		for (Producaoagendamaterial prod : listaItens) {
			ReportTemplateBean template = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU);
			LinkedHashMap<String, Object> dataSource = new LinkedHashMap<String, Object>();
			ProducaoAgendaMaterialReportBean bean = new ProducaoAgendaMaterialReportBean();
			
			if(prod.getPneu() != null && prod.getCdproducaoagendamaterial() != null){
				Producaoordem ordem = producaoordemService.findByPneuAndProducaoagendamaterial(prod.getPneu(),prod);	
				bean.setSituacaoProducaoOrdem(ordem != null && ordem.getProducaoordemsituacao() != null ? ordem.getProducaoordemsituacao().ordinal() : null);
			}
			
			bean.setPneuReportBean(this.criarPneuReportBean(prod));
			bean.setListaProducaoPneuReportBean(this.criarProducaoPneuReportBean(prod));
			bean.setClienteReportBean(this.criarClienteReportBean(prod));
			bean.setEmpresaReportBean(this.criarEmpresaReportBean(prod));
			bean.setPedidoVendaReportBean(this.criarPedidoVendaReportBean(prod));
			bean.setPedidoVendaMaterialReportBean(this.criarPedidoVendaMaterialReportBean(prod));
			
			dataSource.put("bean",bean);	
			template.setDatasource(dataSource);
			lista.add(template);
		}
		return lista;	
	}
	private PedidoVendaMaterialReportBean criarPedidoVendaMaterialReportBean(Producaoagendamaterial prod) {
		PedidoVendaMaterialReportBean bean = new PedidoVendaMaterialReportBean();
		if(prod.getPedidovendamaterial() != null ){
			if(prod.getPedidovendamaterial().getMaterial() != null){
				bean.setCodigo(prod.getPedidovendamaterial().getMaterial().getIdentificacao());
				bean.setNome(prod.getPedidovendamaterial().getMaterial().getNome());
				bean.setItem(prod.getPedidovendamaterial().getOrdem());
			}
			List<Pedidovendamaterial> listaItens = pedidovendamaterialService.findByPedidovenda(prod.getPedidovendamaterial().getPedidovenda());
			if(SinedUtil.isListNotEmpty(listaItens)){
				bean.setQuantidadeItensPedido(listaItens.size());
			}
			bean.setPrazoEntrega(prod.getPedidovendamaterial().getDtprazoentrega());
			
		}
		if(prod.getProducaoetapa() !=null){
			bean.setCicloProducao(prod.getProducaoetapa().getNome());
		}
		return bean;
	}
	private PedidoVendaReportBean criarPedidoVendaReportBean(Producaoagendamaterial prod) {
		PedidoVendaReportBean bean = new PedidoVendaReportBean();
		Pedidovenda pedido = prod.getProducaoagenda().getPedidovenda();
		if(pedido != null){
			bean.setCodigo(pedido.getCdpedidovenda());
			bean.setIdExterno(pedido.getIdentificacaoexterna());
			if(SinedUtil.isListNotEmpty(pedido.getListaPedidovendavalorcampoextra())){
				LinkedList<CampoAdicionalReportBean> listaCampoAdicional = new LinkedList<CampoAdicionalReportBean>();
				for (Pedidovendavalorcampoextra extra: pedido.getListaPedidovendavalorcampoextra()) {
					CampoAdicionalReportBean campo = new CampoAdicionalReportBean();
					if(extra.getCampoextrapedidovendatipo() != null) 
						campo.setOrdem(extra.getCampoextrapedidovendatipo().getOrdem());
					campo.setCampoAdicional(extra.getValor());
					listaCampoAdicional.add(campo);
				}
				bean.setListaCampoAdicional(listaCampoAdicional);
			}
			bean.setDataPedido(pedido.getDtpedidovenda().toString());
			if(pedido.getEndereco() != null){
				Endereco end = pedido.getEndereco();
				bean.setLogradouroEnderecoCliente(end.getLogradouro());
				bean.setNumeroEnderecoCliente(end.getNumero());
				bean.setBairroEnderecoCliente(end.getBairro());
				bean.setComplementoEnderecoCliente(end.getComplemento());
				bean.setCepEnderecoCliente(end.getCep().toString());
				if(end.getMunicipio()!=null){
					bean.setCidadeEnderecoCliente(end.getMunicipio().getNome());
					if(end.getMunicipio().getUf()!=null){
						bean.setUfEnderecoCliente(end.getMunicipio().getUf().getSigla());
					}
				}
				bean.setEnderecoCliente(end.getLogradouroCompletoComBairro());
			}
			if(pedido.getColaborador() != null){
				bean.setResponsavel(pedido.getColaborador().getNome());
			}
		}
		return bean;
	}
	@SuppressWarnings("unchecked")
	private ClienteReportBean criarClienteReportBean(Producaoagendamaterial prod) {
		ClienteReportBean bean = new ClienteReportBean();
		Cliente cliente = prod.getProducaoagenda().getCliente();
		if(cliente!=null){
			bean.setCodigoCliente(cliente.getCdpessoa());
			bean.setIdentificadorCliente(cliente.getIdentificador());
			bean.setNomeCliente(cliente.getNome());
			bean.setRazaoSocialCliente(cliente.getRazaosocial());
			bean.setCpfCnpjCliente(cliente.getCpfcnpj());
			bean.setInscricaoEstadualCliente(cliente.getInscricaoestadual());
			if(cliente.getUfinscricaoestadual()!=null){
				bean.setUfInscricaoEstadulaCliente(cliente.getUfinscricaoestadual().getSigla());
			}
			if(SinedUtil.isListNotEmpty(cliente.getListaEndereco())){
				Endereco end = cliente.getEnderecoWithPrioridade(Enderecotipo.ENTREGA);
				bean.setLogradouroEnderecoCliente(end.getLogradouro());
				bean.setNumeroEnderecoCliente(end.getNumero());
				bean.setBairroEnderecoCliente(end.getBairro());
				bean.setComplementoEnderecoCliente(end.getComplemento());
				bean.setCepEnderecoCliente(end.getCep().toString());
				if(end.getMunicipio()!=null){
					bean.setCidadeEnderecoCliente(end.getMunicipio().getNome());
					if(end.getMunicipio().getUf()!=null){
						bean.setUfEnderecoCliente(end.getMunicipio().getUf().getSigla());
					}
				}
			}
			bean.setEmailCliente(cliente.getEmail());
			cliente.setListaTelefone(SinedUtil.listToSet(telefoneService.carregarListaTelefone(cliente), Telefone.class));
			bean.setTelefoneCliente(cliente.getTelefones());
		}
		return bean;
	}
	@SuppressWarnings("unchecked")
	private EmpresaReportBean criarEmpresaReportBean(Producaoagendamaterial prod) {
		EmpresaReportBean bean = new EmpresaReportBean();
		Empresa e = prod.getProducaoagenda().getEmpresa();
		if(e !=null){
			bean.setRazaosocial(e.getRazaosocial());
			bean.setNomefantasia(e.getNomefantasia());
			bean.setLogo(e.getLogomarca());
			Endereco endereco = enderecoService.carregaEnderecoEmpresa(e);
			bean.setEndereco(endereco.getLogradouroCompleto());
			e.setListaTelefone(SinedUtil.listToSet(telefoneService.carregarListaTelefone(e), Telefone.class));
			bean.setTelefone(e.getTelefones());
		}
		return bean;
	}
	private LinkedList<ProducaoPneuReportBean> criarProducaoPneuReportBean(Producaoagendamaterial prod) {
		LinkedList<ProducaoPneuReportBean> listaBean = new LinkedList<ProducaoPneuReportBean>();
		Producaoordem ordem = producaoordemService.findByPneuAndEtapa(prod.getPneu(),producaoEtapaService.getUltimaEtapa(prod.getProducaoetapa()));	
		if(SinedUtil.isListNotEmpty(ordem.getListaProducaoordemconstatacao())){
			for(Producaoordemconstatacao constatacao :ordem.getListaProducaoordemconstatacao()){
				ProducaoPneuReportBean bean = new ProducaoPneuReportBean();
				if(constatacao.getMotivodevolucao() !=null){
					if(Boolean.TRUE.equals(constatacao.getMotivodevolucao().getMotivodevolucao())){
						bean.setMotivoRecusa("Sim");
					}else{
						bean.setMotivoRecusa("N�o");
					}
					bean.setObservacaoRecusa(constatacao.getMotivodevolucao().getDescricao());
				}
				bean.setConstatacao(""+constatacao.getCdproducaoordemconstatacao());
				bean.setObservacaoConstatacao(constatacao.getObservacao());
				listaBean.add(bean);
			}
		}
		return listaBean;
	}
	private PneuReportBean criarPneuReportBean(Producaoagendamaterial prod) {
		Pneu p = prod.getPneu();
		PneuReportBean bean = new PneuReportBean();
		if(prod.getPneu() != null){
			if(p.getMaterialbanda() != null){
				bean.setBanda(p.getMaterialbanda().getNome());
			}
			if(p.getPneumarca() !=null){
				bean.setMarca(p.getPneumarca().getNome());
			}
			if(p.getPneumodelo() !=null){
				bean.setModelo(p.getPneumodelo().getNome());
			}
			if(p.getPneumedida() !=null){
				bean.setMedida(p.getPneumedida().getNome());
			}
			bean.setSeriefogo(p.getSerie());
			bean.setDot(p.getDot());
			bean.setNumeroreforma(""+p.getNumeroreforma());
			bean.setId(""+p.getCdpneu());
			if(p.getPneuqualificacao() != null){
				bean.setQualificacao(p.getPneuqualificacao().getNome());
			}
		}
		return bean;
	}
	public List<Producaoagendamaterial> findByAgendaAndPneu(String whereInAgenda,String whereInPneu) {
		return producaoagendamaterialDAO.findByAgendaAndPneu(whereInAgenda,whereInPneu);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	public List<Producaoagendamaterial> findForIntegracaoVipal(IntegracaoVipalFiltro filtro) {
		return producaoagendamaterialDAO.findForIntegracaoVipal(filtro);
	}
	
	/**
	 * Gera o arquivo de cliente a partir de uma agenda de produ��o
	 *
	 * @param producaoagendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	public Arquivo gerarArquivoVipalCliente(Producaoagendamaterial producaoagendamaterial) {
		StringBuilder s = gerarStringVipalCliente(producaoagendamaterial);
		return this.criaArquivoForVipal(producaoagendamaterial, s, "cliente");
	}
	
	public Arquivo gerarArquivoVipalCliente(String s) {
		return this.criaArquivoForVipal(null, s, "cliente");
	}
	
	public StringBuilder gerarStringVipalCliente(Producaoagendamaterial producaoagendamaterial) {
		//15301 � o c�digo da �ltima vers�o no svn antes da altera��o de layoput
		StringBuilder arquivo = new StringBuilder();
		
		Producaoagenda producaoagendaBean = producaoagendamaterial.getProducaoagenda();
		Empresa empresaBean = producaoagendaBean != null ? producaoagendaBean.getEmpresa() : null;
		Cliente clienteBean = producaoagendaBean != null ? producaoagendaBean.getCliente() : null;
		Endereco enderecoBean = clienteBean != null ? clienteBean.getEndereco() : null;
		Telefone telefoneFaxBean = clienteBean != null ? clienteBean.getTelefoneFax() : null;
		Telefone telefoneBean = clienteBean != null ? clienteBean.getTelefone() : null;
		Contato contatoBean = clienteBean != null ? clienteBean.getContatoResponsavel() : null;
		Municipio municipioBean = enderecoBean != null ? enderecoBean.getMunicipio() : null;
		Segmento segmento = null;
		if(clienteBean.getListaClienteSegmento()!=null){
			for(ClienteSegmento clienteSegmento : clienteBean.getListaClienteSegmento()){
				if(clienteSegmento!=null && clienteSegmento.isPrincipal() && clienteSegmento.getSegmento()!=null){
					segmento = clienteSegmento.getSegmento();
					break;
				}
			}
		} 
		
		String reformador = empresaBean != null ? empresaBean.getNome() : "";
		String cliente = clienteBean != null && clienteBean.getCdpessoa() != null ? clienteBean.getCdpessoa().toString() : "";
		String ramoatividade = segmento != null && segmento.getCdsegmento() != null ? segmento.getCdsegmento().toString() : null;
		String cpf_cnpj = clienteBean != null ? clienteBean.getCpfcnpj() : "";
		String nome_usual = clienteBean != null ? clienteBean.getNome() : "";
		String razao_social = clienteBean != null ? clienteBean.getRazaosocial() : "";
		String tipo_pessoa = clienteBean != null && clienteBean.getTipopessoa() != null ? ""+clienteBean.getTipopessoa().ordinal() : "";
		String endereco = enderecoBean != null ? enderecoBean.getLogradouroNumero() : "";
		String cidade = municipioBean != null ? municipioBean.getNome() : "";
		String uf = municipioBean != null && municipioBean.getUf() != null ? municipioBean.getUf().getSigla() : "";
		String bairro = enderecoBean != null ? enderecoBean.getBairro() : "";
		String cep = enderecoBean != null && enderecoBean.getCep() != null ? enderecoBean.getCep().toString() : "";
		String complemento = enderecoBean != null ? enderecoBean.getComplemento() : "";
		String ie_rg = clienteBean != null ? (clienteBean.getInscricaoestadual() != null ? clienteBean.getInscricaoestadual() : clienteBean.getRg())  : "";
		String numero_fax = telefoneFaxBean != null ? telefoneFaxBean.getTelefone() : "";
		String numero_fone = telefoneBean != null ? telefoneBean.getTelefone() : "";
		String email = clienteBean != null ? clienteBean.getEmail() : "";
		String contato = contatoBean != null ? contatoBean.getNome() : "";
		String obs = producaoagendamaterial.getObservacao();
		
		arquivo.append(this.getStringCampoVipal(cliente, 9));
		arquivo.append(this.getStringCampoVipal(tipo_pessoa, 1));
		arquivo.append(this.getStringCampoVipal(ie_rg, 20));
		arquivo.append(this.getStringCampoVipal(cpf_cnpj, 18));
		arquivo.append(this.getStringCampoVipal(razao_social, 50));
		arquivo.append(this.getStringCampoVipal(nome_usual, 20));
		arquivo.append(this.getStringCampoVipal(ramoatividade, 8));
		arquivo.append(this.getStringCampoVipal(email, 100));
		arquivo.append(this.getStringCampoVipal(contato, 30));
		arquivo.append(this.getStringCampoVipal(reformador, 12));
		arquivo.append(this.getStringCampoVipal(endereco, 50));
		arquivo.append(this.getStringCampoVipal(bairro, 50));
		arquivo.append(this.getStringCampoVipal(cep, 10));
		arquivo.append(this.getStringCampoVipal(cidade, 25));
		arquivo.append(this.getStringCampoVipal(uf, 2));
		arquivo.append(this.getStringCampoVipal(numero_fone, 15));
		arquivo.append(this.getStringCampoVipal(numero_fax, 15));
		arquivo.append(this.getStringCampoVipal(complemento, 50));
		arquivo.append(this.getObservacaoVipalNovo(obs));
		
		return arquivo;
	}

	/**
	 * Retorna a string para preenchimento do campo do arquivo da Vipal
	 *
	 * @param obs
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
//	private String getObservacaoVipal(String obs) {
//		return "//IN�CIO DA OBSERVA��O\n" + (obs != null ? obs : "") + "\n//FIM DA OBSERVA��O\n";
//	}
	
	private String getObservacaoVipalNovo(String obs) {
		return (obs != null ? obs : "") + SEPARADOR_VIPAL;
	}
	
	/**
	 * Retorna a string para preenchimento do campo do arquivo da Vipal
	 *
	 * @param date
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	private String getDateCampoVipal(Date date) {
		if(date == null) return SEPARADOR_VIPAL;
		return SinedDateUtils.toString(date) + SEPARADOR_VIPAL;
	}

	/**
	 * Retorna a string para preenchimento do campo do arquivo da Vipal
	 *
	 * @param valor
	 * @param tamanho
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	private String getStringCampoVipal(String valor, int tamanho) {
		return this.truncateVipal(valor, tamanho).trim() + SEPARADOR_VIPAL;
	}
	
	/**
	 * Trunca o valor para o arquivo da Vipal
	 *
	 * @param texto
	 * @param size
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	private String truncateVipal(String texto,Integer size) {
		if(texto == null) return "";
		
		if(texto != null && texto.length() >= size)
			texto = texto.substring(0, size);
		
		return texto;
	}
	
	/**
	 * Gera o arquivo de planilha a partir de uma agenda de produ��o
	 *
	 * @param producaoagendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	public Arquivo gerarArquivoVipalPlanilha(Producaoagendamaterial producaoagendamaterial) {
		StringBuilder s = gerarStringVipalPlanilha(producaoagendamaterial);
		return this.criaArquivoForVipal(producaoagendamaterial, s, "pneu");
	}
	
	public Arquivo gerarArquivoVipalPlanilha(String s) {
		return this.criaArquivoForVipal(null, s, "pneu");
	}
		
	public StringBuilder gerarStringVipalPlanilha(Producaoagendamaterial producaoagendamaterial) {
		//15301 � o c�digo da �ltima vers�o no svn antes da altera��o de layoput
		StringBuilder arquivo = new StringBuilder();
		
		Pedidovendamaterial pedidovendamaterialBean = producaoagendamaterial.getPedidovendamaterial();
		Producaoagenda producaoagendaBean = producaoagendamaterial.getProducaoagenda();
		Pedidovenda pedidovendaBean = producaoagendaBean != null ? producaoagendaBean.getPedidovenda() : null;
		Empresa empresaBean = producaoagendaBean != null ? producaoagendaBean.getEmpresa() : null;
		Cliente clienteBean = producaoagendaBean != null ? producaoagendaBean.getCliente() : null;
		Pneu pneuBean = producaoagendamaterial.getPneu();
		
		Date dt_producao = producaoordemService.getMaxDateProducaoByProducaoagenda(producaoagendaBean);
		
		String reformador = empresaBean != null ? empresaBean.getNome() : "";
		String cpf_cnpj = clienteBean != null ? clienteBean.getCpfOuCnpjValue() : "";
		String os = pedidovendaBean != null && pedidovendaBean.getCdpedidovenda() != null ? pedidovendaBean.getCdpedidovenda().toString() : "";
		String item = pedidovendamaterialBean != null && pedidovendamaterialBean.getCdpedidovendamaterial() != null ? pedidovendamaterialBean.getCdpedidovendamaterial().toString() : "";
		Date dt_entrada = pedidovendaBean != null ?  pedidovendaBean.getDtpedidovenda() : null;
		String servico = "0"; //"PR�-MOLDADO"
		String nf = pedidovendaBean != null && pedidovendaBean.getCdpedidovenda() != null ? pedidovendaBean.getCdpedidovenda().toString() : "";
		String codigopneu = pneuBean != null && pneuBean.getCdpneu() != null ? pneuBean.getCdpneu().toString() : "0";
		String serie = pneuBean != null ? pneuBean.getSerie() : "NE";
		String fogo = "";
		String conserto = "No";
		String dot = pneuBean != null ? pneuBean.getDot() : "";
		String vlr_reforma = pedidovendamaterialBean != null && pedidovendamaterialBean.getValorcustomaterial() != null ? new Money(pedidovendamaterialBean.getValorcustomaterial()).toString().replaceAll("\\.", "") + "" : "0";
		String modelo = pneuBean != null && pneuBean.getPneumodelo() != null ? pneuBean.getPneumodelo().getCodigointegracao() : "";
		String marca = pneuBean != null && pneuBean.getPneumarca() != null ? pneuBean.getPneumarca().getCodigointegracao() : "";
		String medida = pneuBean != null && pneuBean.getPneumedida() != null ? pneuBean.getPneumedida().getCodigointegracao() : "";
		String nref = pneuBean != null && pneuBean.getNumeroreforma() != null ? ""+pneuBean.getNumeroreforma().ordinal() : "0";
		String refanterior = "";
		String reclam = "No";
		String dt_reclam = ""; // Linha em branco
		String produto = ""; // Linha em branco
		String matriz = ""; // Linha em branco
		String possuilaudo = "No";
		
		arquivo.append(this.getStringCampoVipal(codigopneu, 9));
		arquivo.append(this.getDateCampoVipal(dt_entrada));
		arquivo.append(this.getStringCampoVipal(reformador, 12));
		arquivo.append(this.getStringCampoVipal(os, 20));
		arquivo.append(this.getStringCampoVipal(nf, 20));
		arquivo.append(this.getDateCampoVipal(dt_producao));
		arquivo.append(this.getStringCampoVipal(medida, 7));
		arquivo.append(this.getStringCampoVipal(marca, 7));
		arquivo.append(this.getStringCampoVipal(modelo, 7));
		arquivo.append(this.getStringCampoVipal(conserto, 2));
		arquivo.append(this.getStringCampoVipal(nref, 1));
		arquivo.append(this.getStringCampoVipal(refanterior, 50));
		arquivo.append(this.getStringCampoVipal(serie, 20));
		arquivo.append(this.getStringCampoVipal(fogo, 10));
		arquivo.append(this.getStringCampoVipal(dot, 4));
		arquivo.append(this.getStringCampoVipal(servico, 1));
		arquivo.append(this.getStringCampoVipal(produto, 16));
		arquivo.append(this.getStringCampoVipal(matriz, 9));
		arquivo.append(this.getStringCampoVipal(vlr_reforma, 12));
		arquivo.append(this.getStringCampoVipal(possuilaudo, 2));
		arquivo.append(this.getStringCampoVipal(reclam, 2));
		arquivo.append(this.getStringCampoVipal(dt_reclam, 10));
		arquivo.append(this.getStringCampoVipal(cpf_cnpj, 14));
		arquivo.append(this.getStringCampoVipal(item, 999));
		
		return arquivo;
	}
	
	/**
	 * Cria o arquivo para integra��o da Vipal
	 *
	 * @param producaoagendamaterial
	 * @param arquivo
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2015
	 */
	private Arquivo criaArquivoForVipal(Producaoagendamaterial producaoagendamaterial, StringBuilder arquivo, String nome) {
		return criaArquivoForVipal(producaoagendamaterial, arquivo.toString(), nome);
	}
	
	private Arquivo criaArquivoForVipal(Producaoagendamaterial producaoagendamaterial, String arquivo, String nome) {
		return new Arquivo(arquivo.toUpperCase().getBytes(), nome + (producaoagendamaterial != null ? "_" + producaoagendamaterial.getCdproducaoagendamaterial() : "") + ".txt", "text/plain");
	}
	
	/**
	* M�todo que retorno o item da agenda de produ��o vinculado ao item da ordem de produ��o
	*
	* @param producaoordemmaterial
	* @return
	* @since 11/04/2016
	* @author Luiz Fernando
	*/
	public Producaoagendamaterial getProducaoagendamaterial(Producaoordemmaterial producaoordemmaterial) {
		if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialorigem())){
			for (Producaoordemmaterialorigem producaoordemmaterialorigem : producaoordemmaterial.getListaProducaoordemmaterialorigem()) {
				if(producaoordemmaterialorigem.getProducaoagendamaterial() != null){
					return producaoordemmaterialorigem.getProducaoagendamaterial();
				}
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoagendamaterialDAO#loadForRegistroproducao(Producaoagendamaterial producaoagendamaterial)
	*
	* @param producaoagendamaterial
	* @return
	* @since 17/08/2016
	* @author Luiz Fernando
	*/
	public Producaoagendamaterial loadForRegistroproducao(Producaoagendamaterial producaoagendamaterial){
		return producaoagendamaterialDAO.loadForRegistroproducao(producaoagendamaterial);
	}
	
	/**
	 * Atualiza o material no item da agenda de produ��o
	 *
	 * @param producaoagendamaterial
	 * @param materialChaofabrica
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void atualizaMaterial(Producaoagendamaterial producaoagendamaterial, Material materialChaofabrica) {
		this.updateMaterial(producaoagendamaterial, materialChaofabrica);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param producaoagendamaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void updateMaterial(Producaoagendamaterial producaoagendamaterial, Material material) {
		producaoagendamaterialDAO.updateMaterial(producaoagendamaterial, material);
	}
	
	public void updateBandaenviada(Producaoagendamaterial producaoagendamaterial, Boolean bandaenviada) {
		producaoagendamaterialDAO.updateBandaenviada(producaoagendamaterial, bandaenviada);
	}
	
	public ReportTemplateBean getTemplateByRaw(WebRequestContext request) {
		if("true".equalsIgnoreCase(request.getParameter("rawTemplate"))){
			return reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU_MATRICIAL);
		} else {
			return reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU);
		}
	}
	
	public LinkedList<ReportTemplateBean> carregaDadosEtiquetaPneuReport(WebRequestContext request) {
		LinkedList<ReportTemplateBean> lista = new LinkedList<ReportTemplateBean>();
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.equals("")){
			SinedUtil.fechaPopUp(request);
			throw new SinedException("Nenhum item selecionado.");
		}
		String wherinPneu = montarWhereInPneu(whereIn.split(","));
		String wherinAgenda = montarWhereInAgenda(whereIn.split(","));
		return this.criarDadosTemplateRelatorio(lista, wherinAgenda, wherinPneu);
	}
	
	private String montarWhereInPneu(String[] strIds) {
		StringBuilder retorno = new StringBuilder(); 
		for (String str : strIds) {
			String[] idSeparado =  str.split("\\|");
			retorno.append(idSeparado[1]+",");
		}
		return retorno.toString().substring(0, retorno.toString().length() -1);
	}
	
	private String montarWhereInAgenda(String[] strIds) {
		StringBuilder retorno = new StringBuilder(); 
		for (String str : strIds) {
			String[] idSeparado =  str.split("\\|");
			retorno.append(idSeparado[0]+",");
		}
		return retorno.toString().substring(0, retorno.toString().length() -1);
	}
	
}
