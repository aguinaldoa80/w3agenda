package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcomissao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.dao.ColaboradorcomissaoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorcomissaoService extends GenericService<Colaboradorcomissao>{
	
	private DocumentocomissaoService documentocomissaoService;
	private ColaboradorcomissaoDAO colaboradorcomissaoDAO;
	
	public void setColaboradorcomissaoDAO(
			ColaboradorcomissaoDAO colaboradorcomissaoDAO) {
		this.colaboradorcomissaoDAO = colaboradorcomissaoDAO;
	}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {
		this.documentocomissaoService = documentocomissaoService;
	}
	
	public void salvarComissao(Documento documento){
		Colaboradorcomissao colaboradorcomissao;
		if(documento.getListaDocumentocomissao() != null && documento.getListaDocumentocomissao().size() > 0){
			colaboradorcomissao = new Colaboradorcomissao();
			colaboradorcomissao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			colaboradorcomissao.setDtaltera(new Timestamp(System.currentTimeMillis()));
			colaboradorcomissao.setColaborador(documento.getColaborador());
			colaboradorcomissao.setDocumento(documento);
			if(documento.getFromPagamentoComissaoDesempenho() != null && documento.getFromPagamentoComissaoDesempenho())
				colaboradorcomissao.setDesempenho(Boolean.TRUE);
			this.saveOrUpdate(colaboradorcomissao);
			for (Documentocomissao item : documento.getListaDocumentocomissao()) {
				documentocomissaoService.baixarComissao(item, colaboradorcomissao);
			}
		}
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcomissaoDAO#getTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada)
	 *
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @since 10/02/2012
	 * @author Rodrigo Freitas
	 */
	public Money getTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada) {
		return colaboradorcomissaoDAO.getTotalComissaoColaboradorMes(colaborador, dataGerada);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorcomissaoDAO#buscaTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada)
	 *
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public Money buscaTotalComissaoColaboradorMes(Colaborador colaborador, Date dataGerada) {
		return colaboradorcomissaoDAO.buscaTotalComissaoColaboradorMes(colaborador, dataGerada);
	}
}
