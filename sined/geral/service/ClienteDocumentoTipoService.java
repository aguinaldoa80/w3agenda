package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ClienteDocumentoTipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.clientedocumentotipo.ClientedocumentotipoRESTModel;

public class ClienteDocumentoTipoService extends GenericService<ClienteDocumentoTipo> {

	private ClienteDocumentoTipoDAO clienteDocumentoTipoDAO;
	public void setClienteDocumentoTipoDAO(ClienteDocumentoTipoDAO clienteDocumentoTipoDAO) { this.clienteDocumentoTipoDAO = clienteDocumentoTipoDAO;	}

	/* singleton */
	private static ClienteDocumentoTipoService instance;
	public static ClienteDocumentoTipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClienteDocumentoTipoService.class);
		}
		return instance;
	}
	
	/**
	 * Lista todos os Documento Tipo para um determinado Cliente
	 * @author Marcos Lisboa
	 */
	public List<ClienteDocumentoTipo> findByCliente(Cliente cliente){
		return clienteDocumentoTipoDAO.findByCliente(cliente);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDocumentoTipoDAO#findByClienteUsuario(Cliente cliente)
	*
	* @param cliente
	* @return
	* @since 13/05/2015
	* @author Luiz Fernando
	*/
	public List<ClienteDocumentoTipo> findByClienteUsuario(Cliente cliente, Usuario usuario){
		return clienteDocumentoTipoDAO.findByClienteUsuario(cliente, usuario);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.ClienteDocumentoTipoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClientedocumentotipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClienteDocumentoTipoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClientedocumentotipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClientedocumentotipoRESTModel> lista = new ArrayList<ClientedocumentotipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(ClienteDocumentoTipo bean : clienteDocumentoTipoDAO.findForAndroid(whereIn))
				lista.add(new ClientedocumentotipoRESTModel(bean));
		}
		
		return lista;
	}
}
