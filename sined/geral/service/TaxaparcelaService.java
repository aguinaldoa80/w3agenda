package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Taxaparcela;
import br.com.linkcom.sined.geral.dao.TaxaparcelaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TaxaparcelaService extends GenericService<Taxaparcela> {
	
	private TaxaparcelaDAO taxaparcelaDAO;
	
	/* singleton */
	private static TaxaparcelaService instance;
	
	public void setTaxaparcelaDAO(TaxaparcelaDAO taxaparcelaDAO) {
		this.taxaparcelaDAO = taxaparcelaDAO;
	}
	
	public boolean isTaxaParcelaCadastrada(Taxaparcela taxaparcela){
		return taxaparcelaDAO.isTaxaParcelaCadastrada(taxaparcela);
	}
	
	public Taxaparcela findTaxaParcelaByParcela(Integer numParcela){
		return taxaparcelaDAO.findTaxaParcelaByParcela(numParcela);
	}
	
	public static TaxaparcelaService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(TaxaparcelaService.class);
		}
		return instance;
	}
	
}
