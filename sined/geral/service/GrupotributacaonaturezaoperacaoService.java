package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaonaturezaoperacao;
import br.com.linkcom.sined.geral.dao.GrupotributacaonaturezaoperacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaonaturezaoperacaoService extends GenericService<Grupotributacaonaturezaoperacao>{

	private GrupotributacaonaturezaoperacaoDAO grupotributacaonaturezaoperacaoDAO;
	
	public void setGrupotributacaonaturezaoperacaoDAO(GrupotributacaonaturezaoperacaoDAO grupotributacaonaturezaoperacaoDAO) {
		this.grupotributacaonaturezaoperacaoDAO = grupotributacaonaturezaoperacaoDAO;
	}

	public List<Grupotributacaonaturezaoperacao> findByGrupotributacao(Grupotributacao grupotributacao){
		return grupotributacaonaturezaoperacaoDAO.findByGrupotributacao(grupotributacao);
	}
}
