package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ParametrogeralDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.parametrogeral.ParametrogeralRESTModel;

public class ParametrogeralService extends GenericService<Parametrogeral> {

	private ParametrogeralDAO parametrogeralDAO;
	
	public void setParametrogeralDAO(ParametrogeralDAO parametrogeralDAO) {
		this.parametrogeralDAO = parametrogeralDAO;
	}
	
	public Parametrogeral findByNome(String nome){
		return parametrogeralDAO.findByNome(nome);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * <p>Fornece o valor de um Par�metro Geral do Sistema a partir de seu nome</p>
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ParametrogeralDAO#getValorPorNome(String)
	 * @param nome
	 * @return String valor do Par�metro Geral
	 * @author Hugo Ferreira
	 */
	public String getValorPorNome(String nome) {
		return parametrogeralDAO.getValorPorNome(nome);
	}
	
	
	/**
	 * <p>Fornece o valor de um par�metro do sistema do tipo <code>Integer</code>, validando
	 * e tratando exce��es poss�vel na convers�o para este tipo.</p>
	 * 
	 * @param nomeParametro
	 * @return
	 * @throws SinedException
	 * @author Hugo Ferreira
	 */
	public Integer getInteger(String nomeParametro) {
		Integer valor = null;
		
		try {
			valor = Integer.parseInt(this.getValorPorNome(nomeParametro));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return valor;
	}
	
	public Integer getIntegerNullable(String nome){
		Integer valor = null;
		
		try {
			String valorStr = this.getValorPorNome(nome);
			if(valorStr != null && !valorStr.trim().equals("")){
				valor = Integer.parseInt(valorStr);
			}
		} catch (Exception e) {}
		
		return valor;
	}
	
	/**
	 * <p>Fornece o valor de um par�metro do sistema do tipo <code>Integer</code> positivo, validando
	 * e tratando exce��es poss�vel na convers�o para este tipo.</p>
	 * 
	 * @param nomeParametro
	 * @return
	 * @throws SinedException
	 * @author Hugo Ferreira
	 */
	public Integer getPositiveInteger(String nomeParametro) {
		Integer valor = this.getInteger(nomeParametro);
		
		if (valor < 0) {
			throw new SinedException("O valor do par�metro '" + nomeParametro + "' n�o pode ser negativo. " +
			"Altere seu valor na tela 'Par�metro' do m�dulo 'Sistema'.");
		}
		
		return valor;
	}
	
	/* singleton */
	private static ParametrogeralService instance;
	public static ParametrogeralService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ParametrogeralService.class);
		}
		return instance;
	}

	public void updateValorPorNome(String nome, String valor) {
		parametrogeralDAO.updateValorPorNome(nome, valor);
	}

	/**
	 * Recupera um par�metro pelo nome (M�todo criado para fazer a consulta sem exigir o contexto de request)
	 * 
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 * @param string
	 * @return
	 */
	public Parametrogeral findForArquivoGerencial(String nome) {
		return parametrogeralDAO.findForArquivoGerencial(nome);
	}
	
	/**
	 * M�todo com refer�ncia no DAO 
	 * Obs: busca o valor do par�metro de acordo com o nome, mas se o valor for null o m�todo retorno o null
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ParametrogeralDAO#buscaValorPorNome(String nome)
	 *
	 * @param nome
	 * @return
	 * @author Luiz Fernando
	 */
	public String buscaValorPorNome(String nome){
		return parametrogeralDAO.buscaValorPorNome(nome);
	}

	/**
	 * Busca o valor de um parametro do tipo Long
	 * @param nomeParametro
	 * @return
	 */
	public Long getLong(String nomeParametro) {
		String l = parametrogeralDAO.buscaValorPorNome(nomeParametro);
		return l==null ? null : new Long(l);
	}

	public boolean getBoolean(String nomeParametro) {
		String param = parametrogeralDAO.buscaValorPorNome(nomeParametro);
		return param==null ? false : Boolean.parseBoolean(param);
	}
	
	/**
	 * M�todo que retorna o nome do campo (altura,largura ou comprimento), definido por par�metro
	 *
	 * @param posicao
	 * @return
	 * @author Luiz Fernando
	 */
	public String getDimensaovenda(int posicao) {
		String valor = parametrogeralDAO.getValorPorNome(Parametrogeral.ORDEM_DIMENSAOVENDA);
		if(valor != null){
			String campos[] = valor.split(",");
			if(campos.length-1 < posicao) return "";
			else return campos[posicao].trim();
		}		
		return "";
	}

	/**
	 * Busca os par�metros para o android
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/10/2014
	 */
	public List<ParametrogeralRESTModel> findForAndroid() {
		List<ParametrogeralRESTModel> lista = new ArrayList<ParametrogeralRESTModel>();
		List<Parametrogeral> listaParametrogeral = parametrogeralDAO.findForAndroid(
			Parametrogeral.VENDASALDOPRODUTO,
			Parametrogeral.VENDASALDOPORMINMAX,
			Parametrogeral.VENDASALDOPRODUTONEGATIVO,
			Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO,
			Parametrogeral.LOCAL_VENDA_OBRIGATORIO,
			Parametrogeral.RECAPAGEM,
			Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE,
			Parametrogeral.OBRIGAR_CAMPOS_PNEU,
			Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO,
			Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO,
			Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA,
			Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR,
			Parametrogeral.BLOQUEAR_CRUD_CLIENTE_APP,
			Parametrogeral.TIPO_EXIBICAO_QTDDISPONIVEL_RESERVADO
		);
		for (Parametrogeral parametrogeral : listaParametrogeral) {
			lista.add(new ParametrogeralRESTModel(parametrogeral));
		}
		return lista;
	}
	
	/**
	 * M�todo que retorna o valor do par�metro para o flex
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/03/2015
	 */
	public String getCalculoHistogramaSemanal(){
		return this.getValorPorNome(Parametrogeral.CALCULO_HISTOGRAMA_SEMANAL);
	}
	
	/**
	 * M�todo que retorna o valor do par�metro para o flex
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/03/2015
	 */
	public String getPrecoCustoProjeto(){
		return this.getValorPorNome(Parametrogeral.PRECOCUSTO_MATERIAL_PROJETO);
	}
	
	/**
	* M�todo que retorna um double de acordo com o valor do parametro 
	*
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public Double getDouble(String parametro) {
		String l = parametrogeralDAO.buscaValorPorNome(parametro);
		Double valor = null;
		
		if(StringUtils.isNotEmpty(l)){
			try {
				valor = Double.parseDouble(l.replace(".", "").replace(",", "."));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return valor;
	}
	
	/**
	 * Valida��o de Usu�rio, se � admin ou n�o
	 * @author C�sar
	 */
	public boolean isUsuarioAdmin() {
		String url = SinedUtil.getUrlWithoutContext();
		
		List<String> urlsAdmin = new ArrayList<String>();
		urlsAdmin.add("linkcom.w3erp.com.br");
		urlsAdmin.add("izap.w3erp.com.br");
		urlsAdmin.add("linkcom.teste1:8080");
		urlsAdmin.add("izap.teste1:8080");
		
		urlsAdmin.add("linkcom.w3erp.local:8080");
		urlsAdmin.add("linkcom.piedade.local:8080");
		urlsAdmin.add("izap.w3erp.local:8080");
		urlsAdmin.add("izap.piedade.local:8080");
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		
		if(usuario.getLogin().equals("igor.costa")) return true;
		if(usuario.getLogin().equals("salvio")) return true;
		if(urlsAdmin.contains(url)){
			return SinedUtil.isUsuarioLogadoAdministrador();
		} else {
			return usuario.getCdpessoa()==1;
		}
	}
	/**
	 * Metodo com refer�ncia no DAO
	 * @author C�sar
	 */
	public Parametrogeral findForParametroInterno(Parametrogeral parametrogeral){
		return parametrogeralDAO.findForParametroInterno(parametrogeral);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 02/04/2019
	 * @author Rodrigo Freitas
	 */
	public String getNomeBancoDeDados() {
		return parametrogeralDAO.getNomeBancoDeDados();
	}
}