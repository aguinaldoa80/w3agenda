package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventos;
import br.com.linkcom.sined.geral.dao.TipoEventosDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoEventosService extends GenericService<TipoEventos>{

	private TipoEventosDAO tipoEventosDAO;
	
	private static TipoEventosService instance;
	
	public static TipoEventosService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipoEventosService.class);
		}
		return instance;
	}
	
	public void setTipoEventosDAO(TipoEventosDAO tipoEventosDAO) {
		this.tipoEventosDAO = tipoEventosDAO;
	}
	
	public TipoEventos loadEventoFinal(EventosCorreios eventoCorreios){
		return tipoEventosDAO.loadEventoFinal(eventoCorreios);
	}
	
	public TipoEventos loadWithEventosCorreios(TipoEventos tipoEventos){
		return tipoEventosDAO.loadWithEventosCorreios(tipoEventos);
	}
	
	public boolean isEventoFinal(TipoEventos tipoEvento){
		TipoEventos bean = this.loadWithEventosCorreios(tipoEvento);
		return bean != null && bean.getEventosCorreios() != null &&  Boolean.TRUE.equals(bean.getEventosCorreios().getEventoFinal());
	}
	
	public boolean isEventoFinal(EventosCorreios eventosCorreios){
		TipoEventos bean = this.loadEventoFinal(eventosCorreios);
		return bean != null && bean.getEventosCorreios() != null &&  Boolean.TRUE.equals(bean.getEventosCorreios().getEventoFinal());
	}
	
	public List<TipoEventos> findByEventosFinaisCorreios(EventosFinaisCorreios eventosFinaisCorreios){
		return tipoEventosDAO.loadByEventosFinaisCorreios(eventosFinaisCorreios);
	}
}