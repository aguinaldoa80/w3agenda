package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Contratomaterialpessoa;
import br.com.linkcom.sined.geral.dao.ContratomaterialpessoaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratomaterialpessoaService extends GenericService<Contratomaterialpessoa> {

	private ContratomaterialpessoaDAO contratomaterialpessoaDAO;
	
	public void setContratomaterialpessoaDAO(
			ContratomaterialpessoaDAO contratomaterialpessoaDAO) {
		this.contratomaterialpessoaDAO = contratomaterialpessoaDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratomaterialpessoaDAO#findByContratomaterial(Contratomaterial contratomaterial)
	 *
	 * @param contratomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/01/2013
	 */
	public List<Contratomaterialpessoa> findByContratomaterial(Contratomaterial contratomaterial) {
		return contratomaterialpessoaDAO.findByContratomaterial(contratomaterial);
	}

	
}
