package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Campolistagemusuario;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.CampolistagemusuarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericDAO;

public class CampolistagemusuarioService extends GenericDAO<Campolistagemusuario> {

	private CampolistagemusuarioDAO campolistagemusuarioDAO;
		
	public void setCampolistagemusuarioDAO(CampolistagemusuarioDAO campolistagemusuarioDAO) {this.campolistagemusuarioDAO = campolistagemusuarioDAO;}

	/* singleton */
	private static CampolistagemusuarioService instance;
	public static CampolistagemusuarioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CampolistagemusuarioService.class);
		}
		return instance;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* atualiza a ordem dos campos da listagem e as colunas exibir/ocultar
	* 
	* @see br.com.linkcom.sined.geral.dao.CampolistagemusuarioDAO#atualizaCamposOrdemUsuario(Campolistagemusuario campolistagemusuario)
	*
	* @param campolistagemusuario
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public void atualizaCamposOrdemUsuario(Campolistagemusuario campolistagemusuario){
		campolistagemusuarioDAO.atualizaCamposOrdemUsuario(campolistagemusuario);
	}
	
	
	/**
	* M�todo com refer�ncia no DAO
	* busca a ordem e os campos a serem exibidosna listagem definidos pelo usu�rio
	* 
	* @see	br.com.linkcom.sined.geral.dao.CampolistagemusuarioDAO.findByTelaUsuario(Tela tela, Usuario usuario)
	*
	* @param tela
	* @param usuario
	* @return
	* @since Oct 28, 2011
	* @author Luiz Fernando F Silva
	*/
	public Campolistagemusuario findByTelaUsuario(Tela tela, Usuario usuario){
		return campolistagemusuarioDAO.findByTelaUsuario(tela, usuario);
	}
	
	
}
