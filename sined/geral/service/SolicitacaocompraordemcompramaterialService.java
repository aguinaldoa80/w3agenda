package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraordemcompramaterial;
import br.com.linkcom.sined.geral.dao.SolicitacaocompraordemcompramaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaocompraordemcompramaterialService extends GenericService<Solicitacaocompraordemcompramaterial>{
	
	private SolicitacaocompraordemcompramaterialDAO solicitacaocompraordemcompramaterialDAO;
	
	public void setSolicitacaocompraordemcompramaterialDAO(SolicitacaocompraordemcompramaterialDAO solicitacaocompraordemcompramaterialDAO) {
		this.solicitacaocompraordemcompramaterialDAO = solicitacaocompraordemcompramaterialDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraordemcompramaterialDAO#findByOrdemcompramaterial(String whereIn)
	*
	* @param ordemcompramaterial
	* @return
	* @since 21/10/2016
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompraordemcompramaterial> findByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		if(ordemcompramaterial == null || ordemcompramaterial.getCdordemcompramaterial() == null)
			return new ArrayList<Solicitacaocompraordemcompramaterial>();
		
		return solicitacaocompraordemcompramaterialDAO.findByOrdemcompramaterial(ordemcompramaterial.getCdordemcompramaterial().toString());
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraordemcompramaterialDAO#findByOrdemcompramaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 21/10/2016
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompraordemcompramaterial> findByOrdemcompramaterial(String whereIn) {
		return solicitacaocompraordemcompramaterialDAO.findByOrdemcompramaterial(whereIn);
	}
}
