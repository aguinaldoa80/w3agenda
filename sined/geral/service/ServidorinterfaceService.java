package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.Servidorinterface;
import br.com.linkcom.sined.geral.dao.ServidorinterfaceDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServidorinterfaceService extends GenericService<Servidorinterface>{
	
	private ServidorinterfaceDAO servidorinterfaceDAO;
	
	public void setServidorinterfaceDAO(ServidorinterfaceDAO servidorinterfaceDAO) {
		this.servidorinterfaceDAO = servidorinterfaceDAO;
	}
	
	/**
	 * Retorna lista de servidorinterface de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidorinterface> findByServidor(Servidor servidor){
		return servidorinterfaceDAO.findByServidor(servidor);
	}
}
