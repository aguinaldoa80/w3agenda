package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Servicoservidor;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.dao.ServicoservidorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicoservidorService extends GenericService<Servicoservidor>{

	private ServicoservidorDAO servicoservidorDAO;
	
	public void setServicoservidorDAO(ServicoservidorDAO servicoservidorDAO) {
		this.servicoservidorDAO = servicoservidorDAO;
	}
	
	/**
	 * Retorna lista de itens de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> find(Servidor servidor){
		return servicoservidorDAO.find(servidor);
	}
	
	/**
	 * Retorna lista de servi�os do tipo SMTP de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findSmtp(Servidor servidor){
		return servicoservidorDAO.findSmtp(servidor);
	}

	/**
	 * Retorna lista de servi�os do tipo FTP de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findFtp(Servidor servidor){
		return servicoservidorDAO.findFtp(servidor);
	}

	/**
	 * Retorna lista de servi�os do tipo Banco de dados de acordo com o servidor
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidor> findBancodados(Servidor servidor){
		return servicoservidorDAO.findBancodados(servidor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param servidor
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Servicoservidor findByServidorAndServicoServidorTipoWeb(Servidor servidor) {
		return servicoservidorDAO.findByServidorAndServicoServidorTipoWeb(servidor);
	}
}
