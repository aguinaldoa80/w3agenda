package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargodepartamento;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.dao.CargodepartamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargodepartamentoService extends GenericService<Cargodepartamento> {	
	
	private CargodepartamentoDAO cargodepartamentoDAO;
	
	public void setCargodepartamentoDAO(
			CargodepartamentoDAO cargodepartamentoDAO) {
		this.cargodepartamentoDAO = cargodepartamentoDAO;
	}
	
	public boolean haveCargoDepartamento(Cargo cargo, Departamento departamento) {
		return cargodepartamentoDAO.haveCargoDepartamento(cargo, departamento);
	}
	
	/* singleton */
	private static CargodepartamentoService instance;
	public static CargodepartamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargodepartamentoService.class);
		}
		return instance;
	}
	
}
