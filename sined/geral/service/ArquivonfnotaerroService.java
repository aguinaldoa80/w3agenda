package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerro;
import br.com.linkcom.sined.geral.dao.ArquivonfnotaerroDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivonfnotaerroService extends GenericService<Arquivonfnotaerro> {
	
	private ArquivonfnotaerroDAO arquivonfnotaerroDAO;
	
	public void setArquivonfnotaerroDAO(
			ArquivonfnotaerroDAO arquivonfnotaerroDAO) {
		this.arquivonfnotaerroDAO = arquivonfnotaerroDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaerroDAO#findByArquivonf
	 *
	 * @param arquivonf
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnotaerro> findByArquivonf(Arquivonf arquivonf) {
		return arquivonfnotaerroDAO.findByArquivonf(arquivonf);
	}

	public List<Arquivonfnotaerro> findByArquivonfnota(Arquivonfnota arquivonfnotaFound) {
		return arquivonfnotaerroDAO.findByArquivonfnota(arquivonfnotaFound);
	}

	public void deleteByArquivonf(Arquivonf arquivonf) {
		arquivonfnotaerroDAO.deleteByArquivonf(arquivonf); 
	}
}
