package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.geral.dao.QuestionarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class QuestionarioService extends GenericService<Questionario>{
	
	private QuestionarioDAO questionarioDAO;
	
	public void setQuestionarioDAO(QuestionarioDAO questionarioDAO) {
		this.questionarioDAO = questionarioDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 05/03/2012
	 * @return
	 */
	public List<Questionario> carregaListaQuestionario(){
		return questionarioDAO.carregarListaQuestionario();
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 05/03/2012
	 * @param tipoPessoa
	 * @return
	 */
	public Questionario carregaQuestionarioPorTipoPessoa(Tipopessoaquestionario tipopessoaquestionario){
		return questionarioDAO.carregaQuestionarioPorTipoPessoa(tipopessoaquestionario);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public Questionario carregarQuestionarioPorId(Questionario bean){
		return questionarioDAO.carregarQuestionarioPorId(bean);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Jo�o Vitor 
	 *@date 02/02/2015
	 * @param tipoPessoa
	 * @return
	 */
	public Questionario carregaQuestionarioPorTipoPessoaForOrdemCompra(Tipopessoaquestionario tipopessoaquestionario){
		return questionarioDAO.carregaQuestionarioPorTipoPessoaForOrdemCompra(tipopessoaquestionario); 
	}
}
