package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao.ContaBancariaVO;
import br.com.linkcom.sined.geral.dao.ContaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.ContaOfflineJSON;
import br.com.linkcom.sined.util.rest.android.conta.ContaRESTModel;

public class ContaService extends GenericService<Conta> {

	
	private ContaDAO contaDAO;
	private EmpresaService empresaService;
	
	public void setContaDAO(ContaDAO contaDAO) {
		this.contaDAO = contaDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}


	/**
	 * Fun��o para obter o valor atual de uma conta, com formata��o decimal.
	 * 
	 * @see #obterValorMaximo(Conta)
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public String valorMaximoFormat(Conta conta){
		Money valor = this.obterValorMaximo(conta);
		String format = new DecimalFormat("R$#,##0.00").format(Math.abs(valor.getValue().doubleValue()));
		return format;
	}
	
	/**
	 * M�todo para obter o valor m�ximo de uma conta.
	 * 
	 * @see #calculaSaldoAtual(Conta)
	 * @see #findLimiteConta(Conta)
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money obterValorMaximo(Conta conta){
		Money saldo = this.calculaSaldoAtual(conta);
		Money limite = this.findLimiteConta(conta);
		
		Money valorMaximo = saldo.add(limite);
		
		if(valorMaximo.getValue().doubleValue() < 0){
			valorMaximo = new Money();
		}
		
		return valorMaximo;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o limite de uma conta.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findLimiteConta(Conta)
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteConta(Conta conta){
		return contaDAO.findLimiteConta(conta);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o limite de v�rias contas.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findLimiteContas(String)
	 * @param listaContas
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteContas(Collection<Conta> listaContas){
		return contaDAO.findLimiteContas(CollectionsUtil.listAndConcatenate(listaContas, "cdconta", ","));
	}
	
	public Money findLimiteContas(Collection<Conta> listaContas, Empresa... empresas){
		return contaDAO.findLimiteContas(CollectionsUtil.listAndConcatenate(listaContas, "cdconta", ","));
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o limite de um tipo de conta
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findLimiteContatipo(Contatipo)
	 * @param contatipo
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money findLimiteContatipo(Contatipo contatipo){
		return contaDAO.findLimiteContatipo(contatipo);
	}
	
	public Money findLimiteContatipo(Contatipo contatipo, Empresa... empresas){
		return contaDAO.findLimiteContatipo(contatipo, empresas);
	}
	
	/**
	 * M�todo para calcular o saldo atual de todas as contas de um determinado tipo, tendo como refer�ncia uma data.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#calculaSaldoAtual(Contatipo, Date)
	 * @param contatipo
	 * @param dataReferencia
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Contatipo contatipo, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		return contaDAO.calculaSaldoAtual(contatipo, dataReferencia, mostrarContasInativadas, whereInNatureza, empresas);
	}
	
	public Money calculaSaldoAtual(Contatipo contatipo, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		return contaDAO.calculaSaldoAtual(contatipo, dataReferencia, mostrarContasInativadas, whereInNatureza, byFluxoCaixa, empresas);
	}

	/**
	 * M�todo para calcular o saldo atual de uma lista de contas, tendo como refer�ncia uma data.
	 * Os ID's das contas s�o concatenados e separados por virgula para ser utilizado em um where In. 
	 * Se a lista de contas for null ou estiver vazia o m�todo retorna o saldo tendo como base somente a data refer�ncia.
	 * 
	 * @see CollectionsUtil#listAndConcatenate(java.util.Collection, String, String)
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#calculaSaldoAtual(String, Date)
	 * @see #calculaSaldoAtual(Date)
	 * @param listaContas
	 * @param dataReferencia
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(List<Conta> listaContas, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		if(listaContas != null && listaContas.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(listaContas, "cdconta", ",");
			return contaDAO.calculaSaldoAtual(whereIn, dataReferencia, mostrarContasInativadas, whereInNatureza);
		}
		return this.calculaSaldoAtual(dataReferencia, mostrarContasInativadas, whereInNatureza, empresas);
	}
	
	/**
	 * M�todo respons�vel por calcular o saldo atual, tendo como base a data atual.
	 * 
	 * @see #calculaSaldoAtual(Conta, Date)
	 * @see SinedDateUtils#currentDate()
	 * @param conta
	 * @return
	 * @author Flavio Tavares
	 */
	public Money calculaSaldoAtual(Conta conta, Empresa... empresas){		
		return this.calculaSaldoAtual(conta, SinedDateUtils.currentDate(), empresas);
	}
	
	/**
	 * M�todo respons�vel por calcular o saldo atual.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#calculaSaldoAtual(Conta, Date)
	 * @param conta
	 * @return
	 * @author Flavio Tavares
	 */
	public Money calculaSaldoAtual(Conta conta, Date date, Empresa... empresas){		
		return contaDAO.calculaSaldoAtual(conta, date, empresas);
	}
	
	public Money calculaSaldoAtualNormal(Conta conta, Date date, Empresa... empresas){		
		return contaDAO.calculaSaldoAtualNormal(conta, date, empresas);
	}
	
	public Money calculaSaldoAtualConciliada(Conta conta, Date date, Empresa... empresas){		
		return contaDAO.calculaSaldoAtualConciliada(conta, date, empresas);
	}
	
	/**
	 * M�todo para calcular o saldo de todas as contas, tendo como refer�ncia uma data.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#calculaSaldoAtual(Date)
	 * @param dataReferencia
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Empresa... empresas){
		return contaDAO.calculaSaldoAtual(dataReferencia, mostrarContasInativadas, whereInNatureza, empresas);
	}
	
	public Money calculaSaldoAtual(Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		return contaDAO.calculaSaldoAtual(dataReferencia, mostrarContasInativadas, whereInNatureza, byFluxoCaixa, empresas);
	}
	
	public Money calculaSaldoAtual(Date dataReferencia, Boolean mostrarContasInativadas, Empresa... empresas){
		return contaDAO.calculaSaldoAtual(dataReferencia, mostrarContasInativadas, null, empresas);
	}
	
	/**
	 * M�todo para calcular o saldo de todas as contas, tendo como refer�ncia uma lista de tipos de conta e uma data.
	 * O c�lculo � feito todos os saldos das contas da lista.
	 * 
	 * @param dataReferencia
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Money calculaSaldoAtual(Collection<Contatipo> listaTipo, Date dtRefer�ncia, Empresa... empresas){
		if(listaTipo != null && listaTipo.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(listaTipo, "cdcontatipo", ",");
			return contaDAO.calculaSaldoAtualByContatipo(whereIn, dtRefer�ncia, empresas);
		}
		return contaDAO.calculaSaldoAtual(dtRefer�ncia, true, null, empresas);
	}
	
	/**
	 * M�todo carregar determinados campos de uma conta especificados por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#load(Conta, String)
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Conta load(Conta bean,String campos){
		return contaDAO.load(bean, campos);
	}
	

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de contas por tipo e por empresa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findByTipo(Contatipo, Empresa)
	 * @param contatipo
	 * @param empresa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Conta> findByTipo(Contatipo contatipo, Empresa empresa){
		return contaDAO.findByTipo(contatipo,empresa, false);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m lista de contas por tipo, de todas as empresas.
	 * 
	 * @see #findByTipo(Contatipo, Empresa)
	 * @param contatipo
	 * @param empresa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Conta> findByTipo(Contatipo contatipo){
		return this.findByTipo(contatipo, null);
	}
	
	public List<Conta> findByTipo(Contatipo contatipo, boolean hasContaDigital){
		return contaDAO.findByTipo(contatipo, null, hasContaDigital);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#carregaConta
	 * @param vinculo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Conta carregaConta(Conta vinculo) {
		return contaDAO.carregaConta(vinculo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#carregaConta
	 * @param vinculo
	 * @return
	 * @author Marden Silva
	 */
	public Conta carregaConta(Conta vinculo, Contacarteira contacarteira) {
		return contaDAO.carregaConta(vinculo, contacarteira);
	}
	
	/**
	 * M�todo que carrega a conta. Caso a conta n�o tenha empresa, seta a conta com a empresa passada por par�metro
	 *
	 * @see br.com.linkcom.sined.geral.service.ContaService#carregaConta(Conta vinculo, Contacarteira 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#carregaEmpresa(Empresa empresa)
	 *
	 * @param conta
	 * @param contacarteira
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta carregaContaForRemessa(Conta conta, Contacarteira contacarteira, Empresa empresa) {
		Conta c = this.carregaConta(conta, contacarteira);
		if((c.getListaContaempresa() == null || c.getListaContaempresa().size() == 0) && empresa != null && empresa.getCdpessoa() != null){ 
			Set<Contaempresa> listaContaempresa = new ListSet<Contaempresa>(Contaempresa.class);
			listaContaempresa.add(new Contaempresa(empresaService.carregaEmpresa(empresa)));
			c.setListaContaempresa(listaContaempresa);
		}
		return c;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO.findByBanco
	 * @param banco
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Conta> findByBanco(Banco banco){
		return contaDAO.findByBanco(banco);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findAllContas
	 * @return
	 * @author Rodrigo Freitas
	 * @param codigobanco 
	 */
	public List<Conta> findAllContas(Integer codigobanco) {
		return contaDAO.findAllContas(codigobanco);
	}
	
	/**
	 * M�todo para fazer combo reload na copia de cheque.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContaService#findBy
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Conta> findContaBancaria(Empresa empresa){
		return this.findByTipo(new Contatipo(Contatipo.CONTA_BANCARIA), empresa);
	}
	
	public List<Conta> findContaBancariaAlterarEmLote(){
		return this.findByTipo(new Contatipo(Contatipo.CONTA_BANCARIA), null);
	}
	
	/**
	 * Renorna uma lista de Caixa contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Conta> findDescricao(String whereIn) {
		return contaDAO.findDescricao(whereIn);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findForGerarMovimentacao(Conta)
	 * @param conta
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Conta findForGerarMovimentacao(Conta conta){
		return contaDAO.findForGerarMovimentacao(conta);
	}
	
	/**
	 * Retorna os nomes das Contas, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 * 
	 * @see #findDescricao(String)
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeContas(List<ItemDetalhe> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(lista, "conta.cdconta", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);
		
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 *<p>M�todo que atualiza o sequencial da conta </p>
	 * 
	 * @param conta
	 * @author Jo�o Paulo Zica	 
	 */
	public void setProximoSequencial(Conta conta) {
		contaDAO.setProximoSequencial(conta);
	}
	
	/**
	 * M�todo que atualiza o sequencialpagamento da conta
	 * 
	 * @param conta
	 * @since 06/07/2017
	 * @author Rafael Salvio
	 */
	public void setProximoSequencialpagamento(Conta conta) {
		contaDAO.setProximoSequencialpagamento(conta);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param conta
	 * @author Rodrigo Freitas
	 * @since 18/07/2017
	 */
	public void setProximoSequencialcheque(Conta conta) {
		contaDAO.setProximoSequencialcheque(conta);
	}
	
	

	public boolean isContaBradesco(Conta vinculo) {
		return contaDAO.isContaBradesco(vinculo);
	}
	
	public List<Conta> findByTipoAndEmpresa(String whereInEmpresas, Contatipo... contatipo){
		return contaDAO.findByTipoAndEmpresa(whereInEmpresas, contatipo);
	}
	
	public List<Conta> findByTipoAndEmpresa(Contatipo contatipo, String whereInEmpresas){
		return this.findByTipoAndEmpresa(whereInEmpresas, contatipo);
	}
	
	/* singleton */
	private static ContaService instance;
	public static ContaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContaService.class);
		}
		return instance;
	}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 * 
	 * @author Taidson Santos
	 * @since 20/05/2010	 
	 */
	public Conta verificaBanco (Conta conta, Contacarteira contacarteira){
		return contaDAO.verificaBanco(conta, contacarteira);
	}
	
	/**
	 * Faz refer�ncia a DAO.
	 * @param conta
	 * @return
	 * @author Taidson
	 * @since 10/08/2010
	 */
	public Conta loadConta(Conta conta){
		return contaDAO.loadConta(conta);
	}
	
	/**
	 * M�todo que retorna uma lista contendo as duas carteiras da conta
	 * 
	 * @param conta
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Contacarteira> getCarteirasConta(Conta conta){
		conta = contaDAO.carregaConta(conta);
		return conta.getListaContacarteira();
	}

	/**M�todo para buscar o sequencial do registro da conta.
	 * @author Thiago Augusto
	 * @param bean
	 * @return
	 */
	public Integer findSequencialConta(Conta bean){
		return contaDAO.findSequencialConta(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO 
	 * busca o c�digo sispag da empresa
	 *
	 * @see	br.com.linkcom.sined.geral.dao.ContaDAO#findForCodigoEmpresaSispagByEmpresa(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findForCodigoEmpresaSispagByEmpresa(Empresa empresa){
		return contaDAO.findForCodigoEmpresaSispagByEmpresa(empresa);
	}

	public List<ContaOfflineJSON> findForPVOffline() {
		List<ContaOfflineJSON> lista = new ArrayList<ContaOfflineJSON>();
		for(Conta c : contaDAO.findForPVOffline())
			lista.add(new ContaOfflineJSON(c));
		
		return lista;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findContasAtivasComPermissao()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissao() {
		return contaDAO.findContasAtivasComPermissao();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findContasAtivasComPermissao(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissao(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			String cdempresa = "";
			if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
				cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
				if(cdempresa != null && !"".equals(cdempresa)){
					empresa = new Empresa(Integer.parseInt(cdempresa));
				}
			}
		}
		return contaDAO.findContasAtivasComPermissao(empresa);
	}
	
	public List<Conta> findContasAtivasComPermissao(Empresa empresa, Boolean desconsideraContaQueNaoassociacontareceber){
		return contaDAO.findContasAtivasComPermissao(empresa, desconsideraContaQueNaoassociacontareceber);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findContasAtivasComPermissao(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContasAtivasComPermissaoByEmpresa(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			String cdempresa = "";
			if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
				cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
				if(cdempresa != null && !"".equals(cdempresa)){
					empresa = new Empresa(Integer.parseInt(cdempresa));
				}
			}
		}
		return contaDAO.findContasAtivasComPermissao(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#findContasAtivasComPermissao(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Marden Silva
	 */
	public List<Conta> findContasBoletoAtivasComPermissao(Empresa empresa) {
		if(empresa == null || empresa.getCdpessoa() == null){
			String cdempresa = "";
			if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS") != null){
				cdempresa = (String) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_MATERIAIS");
				if(cdempresa != null && !"".equals(cdempresa)){
					empresa = new Empresa(Integer.parseInt(cdempresa));
				}
			}
		}
		return contaDAO.findContasBoletoAtivasComPermissao(empresa);
	}	

	public Conta carregaContaComMensagem(Conta conta, Contacarteira contacarteira) {
		return contaDAO.carregaContaComMensagem(conta, contacarteira);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#carregaContaForSaldoBaixar(Conta conta)
	 *
	 * @param conta
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public Conta carregaContaForSaldoBaixar(Conta conta) {
		return contaDAO.carregaContaForSaldoBaixar(conta);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#isUsuarioPermissaoConta(Conta conta)
	 *
	 * @param conta
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isUsuarioPermissaoConta(Conta conta) {
		return contaDAO.isUsuarioPermissaoConta(conta);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name loadTaxaBoletoByEmpresa
	 * @param empresa
	 * @return
	 * @return Conta
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public Conta loadTaxaBoletoByEmpresa(Empresa empresa){
		return contaDAO.loadTaxaBoletoByEmpresa(empresa);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name loadTaxaBoletoByConta
	 * @param conta
	 * @return
	 * @return Conta
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public Conta loadTaxaBoletoByConta(Conta conta){
		return contaDAO.loadTaxaBoletoByConta(conta);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findContaBoletoByEmpresa
	 * @param empresa
	 * @return
	 * @return List<Conta>
	 * @author Marden Silva
	 *
	 */
	public List<Conta> findContaBoletoByEmpresa(Empresa empresa){
		return contaDAO.findContaBoletoByEmpresa(empresa, null);
	}
	
	/**
	 *  M�todo com refer�ncia no DAO.
	 * 
	 * @param empresa
	 * @return
	 * @author Luiz Romario Filho
	 */
	public List<Conta> findVinculosByEmpresa(Empresa empresa){
		return contaDAO.findVinculosByEmpresa(empresa, false);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @param empresa
	* @return
	* @since 12/08/2016
	* @author Luiz Fernando
	*/
	public List<Conta> findVinculosAtivosByEmpresa(Empresa empresa){
		return contaDAO.findVinculosByEmpresa(empresa, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @return
	* @since 12/08/2016
	* @author Luiz Fernando
	*/
	public List<Conta> findVinculos(){
		return findVinculosByEmpresa(null);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findByEmpresaForRemessaPagamento
	 * @param empresa
	 * @return
	 * @return List<Conta>
	 * @author Rafael Salvio
	 *
	 */
	public List<Conta> findByEmpresaForRemessaPagamento(Empresa empresa){
		return contaDAO.findByEmpresaForRemessaPagamento(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.ContaDAO#findContaBoletoByEmpresaContrato(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Conta> findContaBoletoByEmpresaContrato(Empresa empresa){
		return contaDAO.findContaBoletoByEmpresaContrato(empresa);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findContaBoleto
	 * @return
	 * @return List<Conta>
	 * @author Marden Silva
	 *
	 */
	public List<Conta> findContaBoleto(){
		return contaDAO.findContaBoletoByEmpresa(null, null);
	}
	
	public List<Conta> findContaBoletoFiltro(){
		return contaDAO.findContaBoletoByEmpresa(null, Boolean.TRUE);
	}
		
	public ContaBancariaVO getContaForRemessaConfiguravel(Conta conta, Contacarteira contacarteira){
		ContaBancariaVO contaVO = new ContaBancariaVO();
		conta = contaDAO.carregaConta(conta, contacarteira);
		contacarteira = conta.getContacarteira();
		
		contaVO.setAgencia(conta.getAgencia());
		contaVO.setDvagencia(conta.getDvagencia());
		contaVO.setConta(conta.getNumero());
		contaVO.setDvconta(conta.getDvnumero());
		contaVO.setTitular(conta.getTitular());
		contaVO.setSequencial(conta.getSequencial() != null ? conta.getSequencial().toString() : null);
		contaVO.setLocalpagamento(conta.getLocalpagamento());
		contaVO.setCdempresatitular(conta.getEmpresatitular() != null ? conta.getEmpresatitular().getCdpessoa() : null);
		contaVO.setCodigoempresasispag(conta.getCodigoempresasispag());
		
		if (contacarteira!=null){
			contaVO.setConvenio(contacarteira.getConvenio());
			contaVO.setConveniolider(contacarteira.getConveniolider());
			contaVO.setCodigocarteira(contacarteira.getCodigocarteira());
			contaVO.setCarteira(contacarteira.getCarteira());
			contaVO.setMsgboleto1(contacarteira.getMsgboleto1());
			contaVO.setMsgboleto2(contacarteira.getMsgboleto2());
			contaVO.setInstrucao1(contacarteira.getInstrucao1());
			contaVO.setInstrucao2(contacarteira.getInstrucao2());
			contaVO.setTipocobranca(contacarteira.getTipocobranca());
			contaVO.setEspeciedocumento(contacarteira.getEspeciedocumento());
			contaVO.setQtdediasdevolucao(contacarteira.getQtdediasdevolucao());
			contaVO.setQtdediasprotesto(contacarteira.getQtdedias());
			contaVO.setCodigotransmissao(contacarteira.getCodigotransmissao());
			contaVO.setAceito(contacarteira.getAceito());
			contaVO.setIdemissaoboleto(contacarteira.getIdemissaoboleto());
			contaVO.setIddistribuicao(contacarteira.getIddistribuicao());
			contaVO.setQtdediasdistribuicao(contacarteira.getQtdediasdistribuicao());
			contaVO.setEspeciedocumentoremessa(contacarteira.getEspeciedocumentoremessa());
			contaVO.setVariacaocarteira(contacarteira.getVariacaocarteira());
		}
		
		return contaVO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#loadWithContagerencial(Conta conta)
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadWithContagerencial(Conta conta) {
		return contaDAO.loadWithContagerencial(conta);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#loadWithTaxa(Conta conta)
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadWithTaxa(Conta conta) {
		return contaDAO.loadWithTaxa(conta);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContaDAO#loadContaForRemessa(Conta conta)
	 *
	 * @param conta
	 * @return
	 * @author Luiz Fernando
	 */
	public Conta loadContaForRemessa(Conta conta){
		return contaDAO.loadContaForRemessa(conta);
	}

	public List<ContaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ContaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ContaRESTModel> lista = new ArrayList<ContaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Conta bean : contaDAO.findForAndroid(whereIn))
				lista.add(new ContaRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContaDAO#findContaBoletoByEmpresaUsuario()
	*
	* @return
	* @since 17/02/2016
	* @author Luiz Fernando
	*/
	public List<Conta> findContaBoletoByEmpresaUsuario(){
		return contaDAO.findContaBoletoByEmpresaUsuario();
	}
	
	public List<Conta> findContaBoletoDigitalByEmpresaUsuario(){
		return contaDAO.findContaBoletoDigitalByEmpresaUsuario();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContaDAO#findContaByContacarteira(Contacarteira contacarteira)
	*
	* @return
	* @since 14/11/2016
	* @author Mairon Cezar
	*/
	public Conta findContaByContacarteira(Contacarteira contacarteira){
		return contaDAO.findContaByContacarteira(contacarteira);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ContaDAO#loadContaWithEmpresa(Conta conta)
	*
	* @param conta
	* @return
	* @since 05/05/2017
	* @author Luiz Fernando
	*/
	public Conta loadContaWithEmpresa(Conta conta){
		return contaDAO.loadContaWithEmpresa(conta);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ContaService#isPermissaoConta(Conta conta, Empresa empresa)
	*
	* @param conta
	* @param empresa
	* @return
	* @since 05/05/2017
	* @author Luiz Fernando
	*/
	public boolean isPermissaoConta(Conta conta, Empresa empresa){
		return contaDAO.isPermissaoConta(conta, empresa);
	}
	
	public Conta loadForTransferenciaInterna(Conta conta){
		return contaDAO.loadForTransferenciaInterna(conta);
	}

	public Money calculaSaldoAtual(List<Conta> listaContas, Date dataReferencia, Boolean mostrarContasInativadas, String whereInNatureza, Boolean byFluxoCaixa, Empresa... empresas){
		if(listaContas != null && listaContas.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(listaContas, "cdconta", ",");
			return contaDAO.calculaSaldoAtual(whereIn, dataReferencia, mostrarContasInativadas, whereInNatureza, byFluxoCaixa);
		}
		return this.calculaSaldoAtual(dataReferencia, mostrarContasInativadas, whereInNatureza, byFluxoCaixa, empresas);
	}
	
	public List<Conta> findByContatipo(Contatipo c, Empresa e) {
		return contaDAO.findByContatipo(c, e);
	}
	
	public List<Conta> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return contaDAO.loadForLcdpr(filtro);
	}

	public List<GenericBean> findContasAtivasComPermissaoWSVenda(Empresa empresa){
		return ObjectUtils.translateEntityListToGenericBeanList(this.findContasAtivasComPermissao(empresa, true));
	}
	
}
