package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletohistorico;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoenvioboletohistoricoService extends GenericService<Documentoenvioboletohistorico>{

	private DocumentoenvioboletohistoricoDAO documentoenvioboletohistoricoDAO;
	public void setDocumentoenvioboletohistoricoDAO(
			DocumentoenvioboletohistoricoDAO documentoenvioboletohistoricoDAO) {
		this.documentoenvioboletohistoricoDAO = documentoenvioboletohistoricoDAO;
	}
	
	public List<Documentoenvioboletohistorico> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto){
		return documentoenvioboletohistoricoDAO.findByDocumentoenvioboleto(documentoenvioboleto);
	}
}
