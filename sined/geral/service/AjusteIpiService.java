package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.AjusteIpi;
import br.com.linkcom.sined.geral.dao.AjusteIpiDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AjusteIpiService extends GenericService<AjusteIpi> {

	private AjusteIpiDAO ajusteIpiDAO;
	
	public void setAjusteIpiDAO(AjusteIpiDAO ajusteIpiDAO) {
		this.ajusteIpiDAO = ajusteIpiDAO;
	}
	
	public List<AjusteIpi> findForApuracaoIpi(SpedarquivoFiltro filtro) {
		return ajusteIpiDAO.findForApuracaoIpi(filtro);
	}

}
