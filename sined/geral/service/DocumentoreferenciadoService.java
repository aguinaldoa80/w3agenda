package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoreferenciado;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Documentoreferenciadoimposto;
import br.com.linkcom.sined.geral.dao.DocumentoreferenciadoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoreferenciadoService extends GenericService<Documentoreferenciado> {

	private DocumentoreferenciadoDAO documentoreferenciadoDAO;
	
	public void setDocumentoreferenciadoDAO(DocumentoreferenciadoDAO documentoreferenciadoDAO) {this.documentoreferenciadoDAO = documentoreferenciadoDAO;}

	public List<Documentoreferenciado> findByImposto(List<Documentoreferenciadoimposto> listaDocumentoreferenciadoimposto, Empresa empresa) {
		return documentoreferenciadoDAO.findByImposto(listaDocumentoreferenciadoimposto, empresa);
	}

	public boolean existeEmpresaImposto(Documentoreferenciado documentoreferenciado) {
		return documentoreferenciadoDAO.existeEmpresaImposto(documentoreferenciado);
	}
}
