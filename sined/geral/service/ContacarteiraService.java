package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.dao.ContacarteiraDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacarteiraService extends GenericService<Contacarteira> {
	
	private ContacarteiraDAO contacarteiraDAO;

	public void setContacarteiraDAO(ContacarteiraDAO contacarteiraDAO) {this.contacarteiraDAO = contacarteiraDAO;}
	
	/**
	 * M�todo que as carteiras de determinada conta
	 * 
	 * @param conta
	 * @author Marden Marden
	 */
	public List<Contacarteira> findByConta(Conta conta){
		return contacarteiraDAO.findByConta(conta);
	}
	
	
	
	/**
	 * M�todo que retorna as carteiras sem aprovacao para producao de determinada conta
	 * 
	 * @param conta
	 * @author Jo�o Vitor
	 */
	public List<Contacarteira> findByContaSemAprovacao(Conta conta){
		return contacarteiraDAO.findByContaSemAprovacao(conta);
	}
	

	public void aprovaBoletoProducao(Contacarteira contacarteira){
		contacarteiraDAO.saveOrUpdate(contacarteira);
	}
	
	public List<Contacarteira> findByConta(Conta conta, Boolean ignoraCarteiraQueNaoGeraBoleto, Boolean ignoraCarteiraQueNaoAssociaContaReceber){
		return contacarteiraDAO.findByConta(conta, ignoraCarteiraQueNaoGeraBoleto, ignoraCarteiraQueNaoAssociaContaReceber);
	}

	public Contacarteira loadPadraoBoleto(Conta contaboleto) {
		if(contaboleto == null || contaboleto.getCdconta() == null)
			return null;
			
		List<Contacarteira> lista = findByConta(contaboleto, true, true);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Contacarteira contacarteira : lista){
				if(contacarteira.isPadrao()){
					return contacarteira;
				}
			}
		}
		return null;
	}

	public Contacarteira isNossoNumeroIndividual(Integer cdcontacarteira) {
		return contacarteiraDAO.isNossoNumeroIndividual(cdcontacarteira);
	}
}
