package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupocomissaovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.enumeration.Materialgrupocomissaopara;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialgrupocomissaovendaService extends GenericService<Materialgrupocomissaovenda> {

	private MaterialService materialService;
	private PessoacategoriaService pessoacategoriaService;
	
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {this.pessoacategoriaService = pessoacategoriaService;}

	public Materialgrupocomissaovenda getComissaoAgencia(Material material,	Fornecedor fornecedor, Fornecedor agencia, Documentotipo documentotipo,	Pedidovendatipo pedidovendatipo) {
		Materialgrupocomissaovenda mgcv = null;
		Materialgrupocomissaovenda mgcvSemCategoria = null;
		Materialgrupocomissaovenda mgcvSemFornecedor = null;
		Material material_aux = materialService.loadForComissaoAgencia(material);
		List<Categoria> listaCategoria = null;
		if(agencia != null){
			List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByFornecedor(agencia);
			if(SinedUtil.isListNotEmpty(listaPessoacategoria)){
				for(Pessoacategoria pessoacategoria : listaPessoacategoria){
					if(pessoacategoria.getCategoria() != null && pessoacategoria.getCategoria().getCdcategoria() != null){
						if(listaCategoria == null) listaCategoria = new ArrayList<Categoria>();
						listaCategoria.add(pessoacategoria.getCategoria());
					}
				}
			}
			if(material_aux != null && material_aux.getMaterialgrupo() != null && 
					SinedUtil.isListNotEmpty(material_aux.getMaterialgrupo().getListaMaterialgrupocomissaovenda())){
				for(Materialgrupocomissaovenda materialgrupocomissaovenda : material_aux.getMaterialgrupo().getListaMaterialgrupocomissaovenda()){
					if(materialgrupocomissaovenda.getComissaopara() != null && Materialgrupocomissaopara.AGENCIA.equals(materialgrupocomissaovenda.getComissaopara()) &&
						(materialgrupocomissaovenda.getFornecedor() == null || (materialgrupocomissaovenda.getFornecedor() != null && materialgrupocomissaovenda.getFornecedor().equals(fornecedor)))){
						if((pedidovendatipo != null && materialgrupocomissaovenda.getPedidovendatipo() != null && 
								pedidovendatipo.equals(materialgrupocomissaovenda.getPedidovendatipo())) || 
								(materialgrupocomissaovenda.getPedidovendatipo() == null)){
							if((documentotipo != null && materialgrupocomissaovenda.getDocumentotipo() != null && 
									documentotipo.equals(materialgrupocomissaovenda.getDocumentotipo())) || 
									(materialgrupocomissaovenda.getDocumentotipo() == null)){
								if(SinedUtil.isListNotEmpty(listaCategoria) && materialgrupocomissaovenda.getCategoria() != null && 
										materialgrupocomissaovenda.getCategoria().getCdcategoria() != null){
									for(Categoria categoria : listaCategoria){
										if(materialgrupocomissaovenda.getCategoria().getCdcategoria().equals(categoria.getCdcategoria())){
											if(materialgrupocomissaovenda.getFornecedor() != null){
												mgcv = materialgrupocomissaovenda;
											}else {
												mgcvSemFornecedor = materialgrupocomissaovenda;
											}
											break;
										}
									}
									if(mgcv != null) break;
								}else if(SinedUtil.isListEmpty(listaCategoria) && materialgrupocomissaovenda.getCategoria() == null){
									if(materialgrupocomissaovenda.getFornecedor() != null){
										mgcv = materialgrupocomissaovenda;
									}else {
										mgcvSemFornecedor = materialgrupocomissaovenda;
									}
								}
								if(materialgrupocomissaovenda.getCategoria() == null){
									mgcvSemCategoria = materialgrupocomissaovenda;
								}
								if(materialgrupocomissaovenda.getFornecedor() == null && mgcvSemFornecedor == null){
									mgcvSemFornecedor = materialgrupocomissaovenda;
								}
							}
						}
					}
				}
				if(mgcv == null){
					if(mgcvSemFornecedor != null && (mgcvSemCategoria == null || mgcvSemCategoria.getFornecedor() == null)){
						mgcv = mgcvSemFornecedor;
					}else {
						mgcv = mgcvSemCategoria;
					}
				}
			}
		}
		return mgcv;
	}

	
}
