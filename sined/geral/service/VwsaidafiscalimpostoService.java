package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscalimposto;
import br.com.linkcom.sined.geral.dao.VwsaidafiscalimpostoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwsaidafiscalimpostoService extends GenericService<Vwsaidafiscalimposto>{

	private VwsaidafiscalimpostoDAO vwsaidafiscalimpostoDAO;

	public void setVwsaidafiscalimpostoDAO(
			VwsaidafiscalimpostoDAO vwsaidafiscalimpostoDAO) {
		this.vwsaidafiscalimpostoDAO = vwsaidafiscalimpostoDAO;
	}

	public List<Vwsaidafiscalimposto> getListaImpsoto(Integer cdnota) {
		if (cdnota!=null){
			return findVwsaidafiscalimpostoByCdnota(cdnota.toString());
		}else {
			return new ArrayList<Vwsaidafiscalimposto>();
		}
	}

	public List<Vwsaidafiscalimposto> findVwsaidafiscalimpostoByCdnota(String whereInCdnota) {
		return vwsaidafiscalimpostoDAO.findVwsaidafiscalimpostoByCdnota(whereInCdnota);
	}	
}