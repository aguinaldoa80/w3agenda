package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Requisicaoprioridade;

public class RequisicaoprioridadeService extends GenericService<Requisicaoprioridade>{
	
	
	/*singleton*/
	public static RequisicaoprioridadeService instance;
	
	public static RequisicaoprioridadeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RequisicaoprioridadeService.class);
		}
		return instance;
	}

}
