package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoTipoPagamento;
import br.com.linkcom.sined.geral.dao.BancotipopagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancotipopagamentoService extends GenericService<BancoTipoPagamento>{
	
	protected BancotipopagamentoDAO bancotipopagamentoDAO;
	
	public void setBancotipopagamentoDAO(
			BancotipopagamentoDAO bancotipopagamentoDAO) {
		this.bancotipopagamentoDAO = bancotipopagamentoDAO;
	}
	
	public List<BancoTipoPagamento> findByBanco(Banco banco){
		return bancotipopagamentoDAO.findByBanco(banco); 
	}
	
	public BancoTipoPagamento findByCdTipoPagamento(int id){
		return bancotipopagamentoDAO.findByCdTipoPagamento(id);
	}

}
