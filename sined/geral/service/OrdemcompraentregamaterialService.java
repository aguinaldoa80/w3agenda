package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.dao.OrdemcompraentregamaterialDAO;

public class OrdemcompraentregamaterialService extends GenericService<Ordemcompraentregamaterial>{
	
	private OrdemcompraentregamaterialDAO ordemcompraentregamaterialDAO;
	
	public void setOrdemcompraentregamaterialDAO(
			OrdemcompraentregamaterialDAO ordemcompraentregamaterialDAO) {
		this.ordemcompraentregamaterialDAO = ordemcompraentregamaterialDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraentregamaterialDAO#findByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial)
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public List<Ordemcompraentregamaterial> findByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		return ordemcompraentregamaterialDAO.findByOrdemcompramaterial(ordemcompramaterial);
	}

}
