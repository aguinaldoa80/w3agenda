package br.com.linkcom.sined.geral.service;


import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialmestre;
import br.com.linkcom.sined.geral.dao.PedidovendamaterialmestreDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaMaterialMestreWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendamaterialmestreService extends GenericService<Pedidovendamaterialmestre>{

	private PedidovendamaterialmestreDAO pedidovendamaterialmestreDAO;
	
	public void setPedidovendamaterialmestreDAO(PedidovendamaterialmestreDAO pedidovendamaterialmestreDAO) {
		this.pedidovendamaterialmestreDAO = pedidovendamaterialmestreDAO;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialmestreDAO#findByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 27/03/2014
	 */
	public List<Pedidovendamaterialmestre> findByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendamaterialmestreDAO.findByPedidovenda(pedidovenda);
	}
	
	public List<PedidoVendaMaterialMestreWSBean> toWSList(List<Pedidovendamaterialmestre> listaPedidoVendaMaterialMestre){
		List<PedidoVendaMaterialMestreWSBean> lista = new ArrayList<PedidoVendaMaterialMestreWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidoVendaMaterialMestre)){
			
			for(Pedidovendamaterialmestre pvmm: listaPedidoVendaMaterialMestre){
				PedidoVendaMaterialMestreWSBean bean = new PedidoVendaMaterialMestreWSBean();
				bean.setAliquotaReaisIpi(pvmm.getAliquotaReaisIpi());
				bean.setAltura(pvmm.getAltura());
				bean.setCdpedidovendamaterialmestre(pvmm.getCdpedidovendamaterialmestre());
				bean.setComprimento(pvmm.getComprimento());
				bean.setDesconto(pvmm.getDesconto());
				bean.setDtprazoentrega(pvmm.getDtprazoentrega());
				bean.setExibiritenskitflexivel(pvmm.getExibiritenskitflexivel());
				bean.setGrupoTributacao(ObjectUtils.translateEntityToGenericBean(pvmm.getGrupotributacao()));
				bean.setIdentificadorespecifico(pvmm.getIdentificadorespecifico());
				bean.setIdentificadorinterno(pvmm.getIdentificadorinterno());
				bean.setIpi(pvmm.getIpi());
				bean.setLargura(pvmm.getLargura());
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(pvmm.getMaterial()));
				bean.setNomealternativo(pvmm.getNomealternativo());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvmm.getPedidovenda()));
				bean.setPeso(pvmm.getPeso());
				bean.setPreco(pvmm.getPreco());
				bean.setQtde(pvmm.getQtde());
				bean.setSaldo(pvmm.getSaldo());
				bean.setTipoCalculoIpi(EnumUtils.translateEnum(pvmm.getTipoCalculoIpi()));
				bean.setTipoCobrancaIpi(EnumUtils.translateEnum(pvmm.getTipocobrancaipi()));
				bean.setUnidademedida(ObjectUtils.translateEntityToGenericBean(pvmm.getUnidademedida()));
				bean.setValorIpi(pvmm.getValoripi());
				bean.setSaldo(pvmm.getSaldo());
				
				lista.add(bean);
			}
			return lista;
		}
		return lista;
	}
}
