package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.dao.ProducaoordemconstatacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class ProducaoordemconstatacaoService extends GenericService<Producaoordemconstatacao> {
	
	private ProducaoordemconstatacaoDAO producaoordemconstatacaoDAO;
	
	public void setProducaoordemconstatacaoDAO(ProducaoordemconstatacaoDAO producaoordemconstatacaoDAO) {
		this.producaoordemconstatacaoDAO = producaoordemconstatacaoDAO;
	}
	
	private static ProducaoordemconstatacaoService instance;
	public static ProducaoordemconstatacaoService getInstance(){
		if(instance==null){
			instance = Neo.getObject(ProducaoordemconstatacaoService.class);
		}
		return instance;
	}
	
	public List<Producaoordemconstatacao> findByProducaoordem(Producaoordem producaoordem) {
		return producaoordemconstatacaoDAO.findByProducaoordem(producaoordem);
	}
	
	public List<Producaoordemconstatacao> findForDelete(Producaoordem producaoordem, String whereIn) {
		return producaoordemconstatacaoDAO.findForDelete(producaoordem, whereIn);
	}
}