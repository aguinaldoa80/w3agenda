package br.com.linkcom.sined.geral.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Cemcomponente;
import br.com.linkcom.sined.geral.bean.Cemcomponenteacumulado;
import br.com.linkcom.sined.geral.bean.Cemobra;
import br.com.linkcom.sined.geral.bean.Cempainel;
import br.com.linkcom.sined.geral.bean.Cemperfil;
import br.com.linkcom.sined.geral.bean.Cemperfilacumulado;
import br.com.linkcom.sined.geral.bean.Cemtipologia;
import br.com.linkcom.sined.geral.bean.Cemvidro;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcor;
import br.com.linkcom.sined.geral.bean.Materialformulapeso;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterialBean;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterialProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.process.filter.ImportarproducaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CemobraService extends GenericService<Cemobra>{

	private CemtipologiaService cemtipologiaService;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private MaterialgrupoService materialgrupoService;
	private ProducaoetapaService producaoetapaService;
	private ProducaoagendaService producaoagendaService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ProducaoagendamaterialitemService producaoagendamaterialitemService;
	private MaterialformulapesoService materialformulapesoService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialtipoService materialtipoService;
	private MaterialcorService materialcorService;
	private ContratoService contratoService;
	
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setMaterialcorService(MaterialcorService materialcorService) {
		this.materialcorService = materialcorService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setMaterialunidademedidaService(
			MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setMaterialformulapesoService(
			MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setProducaoagendamaterialitemService(
			ProducaoagendamaterialitemService producaoagendamaterialitemService) {
		this.producaoagendamaterialitemService = producaoagendamaterialitemService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setProducaoetapaService(
			ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCemtipologiaService(CemtipologiaService cemtipologiaService) {
		this.cemtipologiaService = cemtipologiaService;
	}
	
	public void saveFromImportacao(Cemobra cemobra) {
		Set<Cemtipologia> listaCemtipologia = cemobra.getListaCemtipologia();
		this.saveOrUpdate(cemobra);
		
		for (Cemtipologia cemtipologia : listaCemtipologia) {
			cemtipologia.setCemobra(cemobra);
			cemtipologiaService.saveOrUpdate(cemtipologia);
		}
	}

	@SuppressWarnings("unchecked")
	public void saveProducaoFromImportacao(Cemobra cemobra, ImportarproducaoFiltro filtro) {
		cemobra = this.loadForEntrada(cemobra);
		cemobra.setListaCemtipologia(SinedUtil.listToSet(cemtipologiaService.findByCemobra(cemobra), Cemtipologia.class));
		
		final int POSICAO_QTDE = 0;
		final int POSICAO_VOLUME = 1;
		
		Set<Cemcomponenteacumulado> listaCemcomponenteacumulado = cemobra.getListaCemcomponenteacumulado();
		Set<Cemperfilacumulado> listaCemperfilacumulado = cemobra.getListaCemperfilacumulado();
		Set<Cemtipologia> listaCemtipologia = cemobra.getListaCemtipologia();
		
		Map<String, ImportacaoproducaoMaterialBean> mapMateriaisNormais = new HashMap<String, ImportacaoproducaoMaterialBean>();
		Map<String, ImportacaoproducaoMaterialProducaoBean> mapMateriaisProducao = new HashMap<String, ImportacaoproducaoMaterialProducaoBean>();
		Map<Material, Double[]> mapAgendaProducaoCompra = new HashMap<Material, Double[]>(); 
		
		for (Cemcomponenteacumulado cemcomponenteacumulado : listaCemcomponenteacumulado) {
			String nome = this.getNome(cemcomponenteacumulado);
			if(!mapMateriaisNormais.containsKey(nome)){
				mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cemcomponenteacumulado));
			}
		}
		
		for (Cemperfilacumulado cemperfilacumulado : listaCemperfilacumulado) {
			String nome = this.getNome(cemperfilacumulado);
			if(!mapMateriaisNormais.containsKey(nome)){
				mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cemperfilacumulado));
			}
		}
		
		for (Cemtipologia cemtipologia : listaCemtipologia) {
			Set<Cemcomponente> listaCemcomponente = cemtipologia.getListaCemcomponente();
			Set<Cempainel> listaCempainel = cemtipologia.getListaCempainel();
			Set<Cemperfil> listaCemperfil = cemtipologia.getListaCemperfil();
			Set<Cemvidro> listaCemvidro = cemtipologia.getListaCemvidro();
			
			for (Cemcomponente cemcomponente : listaCemcomponente) {
				String nome = this.getNome(cemcomponente);
				if(!mapMateriaisNormais.containsKey(nome)){
					mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cemcomponente));
				}
			}
			
			for (Cempainel cempainel : listaCempainel) {
				String nome = this.getNome(cempainel);
				if(!mapMateriaisNormais.containsKey(nome)){
					mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cempainel));
				}
			}
			
			for (Cemperfil cemperfil : listaCemperfil) {
				String nome = this.getNome(cemperfil);
				if(!mapMateriaisNormais.containsKey(nome)){
					mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cemperfil));
				}
			}
			
			for (Cemvidro cemvidro : listaCemvidro) {
				String nome = this.getNome(cemvidro);
				if(!mapMateriaisNormais.containsKey(nome)){
					mapMateriaisNormais.put(nome, new ImportacaoproducaoMaterialBean(cemvidro));
				}
			}
		}
		
		Map<String, Unidademedida> mapUnidademedida = new HashMap<String, Unidademedida>();
		Map<String, Materialgrupo> mapMaterialgrupo = new HashMap<String, Materialgrupo>();
		Map<String, Materialcor> mapMaterialcor = new HashMap<String, Materialcor>();
		Map<String, Materialtipo> mapMaterialtipo = new HashMap<String, Materialtipo>();
		
		Unidademedida unidademedidaKG = this.getUnidadeMedida(mapUnidademedida, "KG");
		Unidademedida unidademedidaMM = this.getUnidadeMedida(mapUnidademedida, "MM");
		
		Set<Entry<String, ImportacaoproducaoMaterialBean>> entrySetMateriais = mapMateriaisNormais.entrySet();
		for (Entry<String, ImportacaoproducaoMaterialBean> entry : entrySetMateriais) {
			String nome = entry.getKey();
			ImportacaoproducaoMaterialBean bean = entry.getValue();
			ImportacaoproducaoMaterial importacaoproducaoMaterial = bean.getImportacaoproducaoMaterial();
			
			if(importacaoproducaoMaterial != null){
				Material material = materialService.findByNome(nome);
				if(material == null){
					material = new Material();
					
					material.setNome(nome);
					material.setIdentificacao(importacaoproducaoMaterial.getCodigo());
					material.setValorcusto(importacaoproducaoMaterial.getCustoMaterial());
					material.setUnidademedida(this.getUnidadeMedida(mapUnidademedida, importacaoproducaoMaterial.getUnidademedidaMaterial()));
					material.setContagerencial(filtro.getContagerencial());
					material.setMaterialgrupo(this.getMaterialgrupo(mapMaterialgrupo, importacaoproducaoMaterial.getGrupoMaterial()));
					material.setMaterialcor(this.getMaterialcor(mapMaterialcor, importacaoproducaoMaterial.getCorMaterial()));
					material.setMaterialtipo(this.getMaterialtipo(mapMaterialtipo, importacaoproducaoMaterial.getTipoMaterial()));
					material.setPesobruto(importacaoproducaoMaterial.getPesoMaterial());
					material.setProduto_altura(importacaoproducaoMaterial.getAlturaMaterial());
					material.setProduto_largura(importacaoproducaoMaterial.getLarguraMaterial());
					material.setProduto_comprimento(importacaoproducaoMaterial.getComprimentoMaterial());
					material.setPatrimonio(Boolean.FALSE);
					material.setEpi(Boolean.FALSE);
					material.setServico(Boolean.FALSE);
					material.setProduto(Boolean.TRUE);
					material.setAtivo(Boolean.TRUE);
					
					materialService.saveOrUpdate(material);
					
					if(importacaoproducaoMaterial instanceof Cemperfil || importacaoproducaoMaterial instanceof Cemperfilacumulado){
						Materialformulapeso materialformulapeso = new Materialformulapeso();
						materialformulapeso.setFormula("(material.qtdvolume > 0 ? material.qtdvolume : 1) * material.pesobruto");
						materialformulapeso.setIdentificador("A");
						materialformulapeso.setOrdem(1);
						materialformulapeso.setMaterial(material);
						
						materialformulapesoService.saveOrUpdate(materialformulapeso);
						
						if(unidademedidaKG != null && 
								importacaoproducaoMaterial.getPesoMaterial() != null && 
								importacaoproducaoMaterial.getPesoMaterial() > 0){
							Materialunidademedida materialunidademedida = new Materialunidademedida();
							materialunidademedida.setMaterial(material);
							materialunidademedida.setFracao(importacaoproducaoMaterial.getPesoMaterial());
							materialunidademedida.setQtdereferencia(1.0);
							materialunidademedida.setValorunitario(0d);
							materialunidademedida.setUnidademedida(unidademedidaKG);
							materialunidademedida.setPrioridadecompra(Boolean.TRUE);
							
							materialunidademedidaService.saveOrUpdate(materialunidademedida);
						}
						
						if(unidademedidaMM != null && 
								importacaoproducaoMaterial.getComprimentoMaterial() != null && 
								importacaoproducaoMaterial.getComprimentoMaterial() > 0){
							Materialunidademedida materialunidademedida = new Materialunidademedida();
							materialunidademedida.setMaterial(material);
							materialunidademedida.setFracao(importacaoproducaoMaterial.getComprimentoMaterial());
							materialunidademedida.setQtdereferencia(1.0);
							materialunidademedida.setValorunitario(0d);
							materialunidademedida.setUnidademedida(unidademedidaMM);
							
							materialunidademedidaService.saveOrUpdate(materialunidademedida);
						}
					}
				}
				
				bean.setMaterial(material);
				mapMateriaisNormais.put(nome, bean);
			}
		}
		
		// FAZER O LEVANTAMENTO DOS MATERIAIS DE PRODU��O (CEMTIPOLOGIA)
		for (Cemtipologia cemtipologia : listaCemtipologia) {
			String nome = this.getNome(cemtipologia);
			if(!mapMateriaisProducao.containsKey(nome)){
				mapMateriaisProducao.put(nome, new ImportacaoproducaoMaterialProducaoBean(cemtipologia));
			}
		}
		
		Producaoetapa producaoetapa = filtro.getProducaoetapa();
		producaoetapa = producaoetapaService.loadForEntrada(producaoetapa);
		
		Set<Entry<String, ImportacaoproducaoMaterialProducaoBean>> entrySetMateriasProducao = mapMateriaisProducao.entrySet();
		for (Entry<String, ImportacaoproducaoMaterialProducaoBean> entry : entrySetMateriasProducao) {
			String nome = entry.getKey();
			ImportacaoproducaoMaterialProducaoBean bean = entry.getValue();
			
			Material material = materialService.findByNome(nome);
			if(material == null){
				ImportacaoproducaoMaterial importacaoproducaoMaterial = bean.getImportacaoproducaoMaterial();
				
				if(importacaoproducaoMaterial != null){
					material = new Material();
					
					material.setNome(nome);
					material.setIdentificacao(importacaoproducaoMaterial.getCodigo());
					material.setValorcusto(importacaoproducaoMaterial.getCustoMaterial());
					material.setUnidademedida(this.getUnidadeMedida(mapUnidademedida, importacaoproducaoMaterial.getUnidademedidaMaterial()));
					material.setContagerencial(filtro.getContagerencial());
					material.setMaterialgrupo(this.getMaterialgrupo(mapMaterialgrupo, importacaoproducaoMaterial.getGrupoMaterial()));
					material.setMaterialcor(this.getMaterialcor(mapMaterialcor, importacaoproducaoMaterial.getCorMaterial()));
					material.setMaterialtipo(this.getMaterialtipo(mapMaterialtipo, importacaoproducaoMaterial.getTipoMaterial()));
					material.setPesobruto(importacaoproducaoMaterial.getPesoMaterial());
					material.setProduto_altura(importacaoproducaoMaterial.getAlturaMaterial());
					material.setProduto_largura(importacaoproducaoMaterial.getLarguraMaterial());
					material.setProduto_comprimento(importacaoproducaoMaterial.getComprimentoMaterial());
					material.setPatrimonio(Boolean.FALSE);
					material.setEpi(Boolean.FALSE);
					material.setServico(Boolean.FALSE);
					material.setProduto(Boolean.TRUE);
					material.setAtivo(Boolean.TRUE);
					material.setProducao(Boolean.TRUE);
					material.setProducaoetapa(producaoetapa);
					
//					Cemtipologia cemtipologia = bean.getCemtipologia();
//					List<Materialproducao> listaMaterialproducao = new ArrayList<Materialproducao>();
//					List<ImportacaoproducaoMaterial> listaImportacaoproducaoMaterial = cemtipologia.getListaImportacaoproducaoMaterial();
//					
//					for (ImportacaoproducaoMaterial it : listaImportacaoproducaoMaterial) {
//						String nomeIt = this.getNome(it);
//						ImportacaoproducaoMaterialBean beanIt = mapMateriaisNormais.get(nomeIt);
//						if(beanIt != null && beanIt.getMaterial() != null){
//							Materialproducao materialproducao = new Materialproducao();
//							
//							materialproducao.setAltura(it.getAlturaMaterial());
//							materialproducao.setLargura(it.getLarguraMaterial());
//							materialproducao.setMaterial(beanIt.getMaterial());
//							materialproducao.setConsumo(it.getQuantidade());
//							materialproducao.setPreco(it.getCustoMaterial());
//							materialproducao.setValorconsumo(it.getQuantidade() * it.getCustoMaterial());
//							
//							listaMaterialproducao.add(materialproducao);
//						}
//					}
//					material.setListaProducao(SinedUtil.listToSet(listaMaterialproducao, Materialproducao.class));
					
					materialService.saveOrUpdate(material);
				}
			}
			
			bean.setMaterial(material);
			mapMateriaisProducao.put(nome, bean);
		}
		
		// MONTAR A AGENDA DE PRODU��O
		Contrato contrato = filtro.getContrato();
		contrato = contratoService.loadContratoWithEmpresa(contrato);
		
		Producaoagenda producaoagenda = new Producaoagenda();
		producaoagenda.setEmpresa(contrato.getEmpresa());
		producaoagenda.setCliente(filtro.getCliente());
		producaoagenda.setContrato(contrato);
		producaoagenda.setDtentrega(filtro.getDtentrega());
		producaoagenda.setProducaoagendasituacao(Producaoagendasituacao.EM_ESPERA);
		producaoagenda.setCemobra(cemobra);
		producaoagenda.setProjeto(filtro.getProjeto());
		
		Set<Producaoagendamaterial> listaProducaoagendamaterial = new ListSet<Producaoagendamaterial>(Producaoagendamaterial.class);
		for (Cemtipologia cemtipologia : listaCemtipologia) {
			String nome = this.getNome(cemtipologia);
			ImportacaoproducaoMaterialProducaoBean bean = mapMateriaisProducao.get(nome);
			
			Producaoagendamaterial producaoagendamaterial = new Producaoagendamaterial();
			producaoagendamaterial.setMaterial(bean.getMaterial());
			producaoagendamaterial.setQtde(cemtipologia.getQtde());
			producaoagendamaterial.setAltura(cemtipologia.getAltura());
			producaoagendamaterial.setLargura(cemtipologia.getLargura());
			listaProducaoagendamaterial.add(producaoagendamaterial);
		}
		producaoagenda.setListaProducaoagendamaterial(listaProducaoagendamaterial);
		
		producaoagendaService.saveOrUpdate(producaoagenda);
		
		Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
		producaoagendahistorico.setProducaoagenda(producaoagenda);
		producaoagendahistorico.setObservacao("Criado a partir da importa��o do CEM");
		producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		
		
		// MONTAR OS ITENS	ACUMULADOS PARA A COMPRA DA AGENDA DE PRODU��O
		for (Cemcomponenteacumulado cemcomponenteacumulado : listaCemcomponenteacumulado) {
			String nome = this.getNome(cemcomponenteacumulado);
			ImportacaoproducaoMaterialBean bean = mapMateriaisNormais.get(nome);
			Double qtde = cemcomponenteacumulado.getQuantidade();
			Double volume = cemcomponenteacumulado.getVolume();
			
			if(bean != null && bean.getMaterial() != null){
				Double qtdeMem = 0d;
				Double volumeMem = 0d;
				if(mapAgendaProducaoCompra.containsKey(bean.getMaterial())){
					qtdeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_QTDE];
					volumeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_VOLUME];
				} 
				qtdeMem += qtde != null ? qtde : 0d;
				volumeMem += volume != null ? volume : 0d;
				mapAgendaProducaoCompra.put(bean.getMaterial(), new Double[]{qtdeMem, volumeMem});
			}
		}
		
		for (Cemperfilacumulado cemperfilacumulado : listaCemperfilacumulado) {
			String nome = this.getNome(cemperfilacumulado);
			ImportacaoproducaoMaterialBean bean = mapMateriaisNormais.get(nome);
			Double qtde = cemperfilacumulado.getQuantidade();
			Double volume = cemperfilacumulado.getVolume();
			
			if(bean != null && bean.getMaterial() != null){
				Double qtdeMem = 0d;
				Double volumeMem = 0d;
				if(mapAgendaProducaoCompra.containsKey(bean.getMaterial())){
					qtdeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_QTDE];
					volumeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_VOLUME];
				} 
				qtdeMem += qtde != null ? qtde : 0d;
				volumeMem += volume != null ? volume : 0d;
				mapAgendaProducaoCompra.put(bean.getMaterial(), new Double[]{qtdeMem, volumeMem});
			}
		}
		
		for (Cemtipologia cemtipologia : listaCemtipologia) {
			List<ImportacaoproducaoMaterial> listaImportacaoproducaoMaterial = cemtipologia.getListaImportacaoproducaoMaterial();
			for (ImportacaoproducaoMaterial it : listaImportacaoproducaoMaterial) {
				String nome = this.getNome(it);
				ImportacaoproducaoMaterialBean bean = mapMateriaisNormais.get(nome);
				
				if(bean != null && bean.getMaterial() != null){
					if(it instanceof Cemvidro ||  it instanceof Cempainel){
						Double qtdeMem = 0d;
						Double volumeMem = 0d;
						if(mapAgendaProducaoCompra.containsKey(bean.getMaterial())){
							qtdeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_QTDE];
							volumeMem = mapAgendaProducaoCompra.get(bean.getMaterial())[POSICAO_VOLUME];
						} 
						qtdeMem += it.getQuantidade() != null ? it.getQuantidade() : 0d;
						volumeMem += it.getVolume() != null ? it.getVolume() : 0d;
						
						mapAgendaProducaoCompra.put(bean.getMaterial(), new Double[]{qtdeMem, volumeMem});
					}
				}
			}
		}
		
		Set<Entry<Material, Double[]>> entrySetCompra = mapAgendaProducaoCompra.entrySet();
		for (Entry<Material, Double[]> entry : entrySetCompra) {
			Producaoagendamaterialitem producaoagendamaterialitem = new Producaoagendamaterialitem();
			producaoagendamaterialitem.setMaterial(entry.getKey());
			producaoagendamaterialitem.setProducaoagenda(producaoagenda);
			producaoagendamaterialitem.setQtde(entry.getValue()[POSICAO_QTDE]);
//			producaoagendamaterialitem.setVolume(entry.getValue()[POSICAO_VOLUME]);
			
			producaoagendamaterialitemService.saveOrUpdate(producaoagendamaterialitem);
		}
	}
	
	private Materialtipo getMaterialtipo(Map<String, Materialtipo> mapMaterialtipo, String tipo) {
		if(tipo == null || tipo.equals("")) return null;
		
		if(mapMaterialtipo.containsKey(tipo)){
			return mapMaterialtipo.get(tipo);
		}
		
		Materialtipo materialtipo = materialtipoService.findByNomeForImportacao(tipo);
		if(materialtipo == null){
			materialtipo = new Materialtipo();
			materialtipo.setAtivo(Boolean.TRUE);
			materialtipo.setVeiculo(Boolean.FALSE);
			materialtipo.setNome(tipo);
			
			materialtipoService.saveOrUpdate(materialtipo);
		}
		
		mapMaterialtipo.put(tipo, materialtipo);
		return materialtipo;
	}
	
	private Materialcor getMaterialcor(Map<String, Materialcor> mapMaterialcor, String cor) {
		if(cor == null || cor.equals("")) return null;
		
		if(mapMaterialcor.containsKey(cor)){
			return mapMaterialcor.get(cor);
		}
		
		Materialcor materialcor = materialcorService.findByNomeForImportacao(cor);
		if(materialcor == null){
			materialcor = new Materialcor();
			materialcor.setNome(cor);
			
			materialcorService.saveOrUpdate(materialcor);
		}
		
		mapMaterialcor.put(cor, materialcor);
		return materialcor;
	}
	
	private Materialgrupo getMaterialgrupo(Map<String, Materialgrupo> mapMaterialgrupo, String grupo) {
		if(grupo == null || grupo.equals("")) grupo = "DIVERSOS";
		
		if(mapMaterialgrupo.containsKey(grupo)){
			return mapMaterialgrupo.get(grupo);
		}
		
		Materialgrupo materialgrupo = materialgrupoService.findByNomeForImportacao(grupo);
		if(materialgrupo == null){
			materialgrupo = new Materialgrupo();
			materialgrupo.setAtivo(Boolean.TRUE);
			materialgrupo.setLocacao(Boolean.FALSE);
			materialgrupo.setNome(grupo);
			
			materialgrupoService.saveOrUpdate(materialgrupo);
		}
		
		mapMaterialgrupo.put(grupo, materialgrupo);
		return materialgrupo;
	}
	
	private Unidademedida getUnidadeMedida(Map<String, Unidademedida> mapUnidademedida, String unid) {
		if(mapUnidademedida.containsKey(unid)){
			return mapUnidademedida.get(unid);
		}
		
		Unidademedida unidademedida = unidademedidaService.findBySimbolo(unid);
		if(unidademedida == null){
			unidademedida = new Unidademedida();
			unidademedida.setAtivo(Boolean.TRUE);
			unidademedida.setNome(unid);
			unidademedida.setSimbolo(unid);
			
			unidademedidaService.saveOrUpdate(unidademedida);
		}
		
		mapUnidademedida.put(unid, unidademedida);
		return unidademedida;
	}
	
	private String getNome(ImportacaoproducaoMaterial bean) {
		return bean.getNomeMaterial();
	}

}
