package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Questionarioresposta;
import br.com.linkcom.sined.geral.dao.QuestionariorespostaDAO;

public class QuestionariorespostaService extends GenericService<Questionarioresposta>{
	
	private QuestionariorespostaDAO questionariorespostaDAO;
	
	public void setQuestionariorespostaDAO(
			QuestionariorespostaDAO questionariorespostaDAO) {
		this.questionariorespostaDAO = questionariorespostaDAO;
	}

	public Questionarioresposta loadPontuacaoByResposta (String resposta){
		return questionariorespostaDAO.loadPontuacaoByResposta(resposta);
	}

}
