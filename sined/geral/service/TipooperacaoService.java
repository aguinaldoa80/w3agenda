package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.dao.TipooperacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipooperacaoService extends GenericService<Tipooperacao>{

	private TipooperacaoDAO tipooperacaoDAO;
	
	public void setTipooperacaoDAO(TipooperacaoDAO tipooperacaoDAO) {
		this.tipooperacaoDAO = tipooperacaoDAO;
	}
	
	/* singleton */
	private static TipooperacaoService instance;
	public static TipooperacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipooperacaoService.class);
		}
		return instance;
	}
	
	public List<Tipooperacao> find(String whereIn){
		return tipooperacaoDAO.find(whereIn);
	}
	
	public List<Tipooperacao> findDebitoCredito(){
		return tipooperacaoDAO.find("1, 2");
	}
}
