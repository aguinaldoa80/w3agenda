package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ambienterede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servidor;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.ServidorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServidorService extends GenericService<Servidor>{

	private ServidorDAO servidorDAO;
	
	public void setServidorDAO(ServidorDAO servidorDAO) {
		this.servidorDAO = servidorDAO;
	}
	
	/**
	 * Retorna lista de servidores que possuam servi�os do tipo SMTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoSmtp(){
		return servidorDAO.findByServicoSmtp();
	}

	/**
	 * Retorna lista de servidores que possuam servi�os do tipo FTP
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoFTP(){
		return servidorDAO.findByServicoFtp();
	}	
	
	/**
	 * Retorna lista de servidores que possuam servi�os do tipo Banco de dados
	 * @param servidor
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoBancodados(){
		return servidorDAO.findByServicoBancodados();
	}

	/**
	 * Retorna lista de servidores de acordo com o tipo de servico e o ambiente de rede
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servidor> findByServicoAmbiente(Servicoservidortipo tipo, Ambienterede ambienterede){
		return servidorDAO.findByServicoAmbiente(tipo, ambienterede);
	}
	
	/**
	 * Retorna o servidor marcado como BriefCase
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Servidor findBriefcase(){
		return servidorDAO.findBriefcase();
	}
	
	@Override
	public void saveOrUpdate(Servidor bean) {
		boolean novoRegistro = false;
		if(bean.getCdservidor() == null)
			novoRegistro = true;
		
		super.saveOrUpdate(bean);
		
		SinedUtil.executaBriefCase(Servicoservidortipo.SERVICO_SERVIDOR, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCdservidor());
	}
	
	@Override
	public void delete(Servidor bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.SERVICO_SERVIDOR, AcaoBriefCase.REMOVER, bean.getCdservidor());
		super.delete(bean);
	}

	/**
	 * M�todo que sinaliza altera��es nos servidores
	 * 
	 * @param listaServidores
	 * @param servico
	 * @param acaoServico
	 * @author Tom�s Rabelo
	 */
	public void atualizaServicos(String[] listaServidores, String servico, String acaoServico) {
		for (String cdServico : listaServidores) 
			SinedUtil.executaBriefCase(Integer.valueOf(servico), Integer.valueOf(acaoServico) == 4 ? AcaoBriefCase.ATUALIZAR : AcaoBriefCase.REINICIAR, Integer.valueOf(cdServico));
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public List<Servidor> findForComboServicoWeb() {
		return servidorDAO.findForComboServicoWeb();
	}

	public Servidor findByIp(String ip) {
		return servidorDAO.findByIp(ip);
	}
	
}
