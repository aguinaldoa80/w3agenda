package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.dao.NaturezaoperacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NaturezaoperacaoService extends GenericService<Naturezaoperacao> {
	
	private NaturezaoperacaoDAO naturezaoperacaoDAO;
	private static NaturezaoperacaoService instance;
	
	public static NaturezaoperacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(NaturezaoperacaoService.class);
		}
		return instance;
	}
	
	public void setNaturezaoperacaoDAO(NaturezaoperacaoDAO naturezaoperacaoDAO) {
		this.naturezaoperacaoDAO = naturezaoperacaoDAO;
	}
	
	/**
	 * 
	 * @param cdnaturezaoperacaoExcecao
	 * @author Thiago Clemente
	 * @param notaTipo 
	 * 
	 */
	public Long getTotalNaturezaoperacaoPadrao(Integer cdnaturezaoperacaoExcecao, NotaTipo notaTipo){
		return naturezaoperacaoDAO.getTotalNaturezaoperacaoPadrao(cdnaturezaoperacaoExcecao, notaTipo);
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public Naturezaoperacao loadPadrao(){
		return loadPadrao(null);
	}
	
	public Naturezaoperacao loadPadrao(NotaTipo notaTipo){
		Naturezaoperacao naturezaoperacao = naturezaoperacaoDAO.loadPadrao(notaTipo);
		if(naturezaoperacao != null){
			if(!Hibernate.isInitialized(naturezaoperacao.getListaNaturezaoperacaocfop())){
				naturezaoperacao.setListaNaturezaoperacaocfop(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getListaNaturezaoperacaocst())){
				naturezaoperacao.setListaNaturezaoperacaocst(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getNotaTipo())){
				naturezaoperacao.setNotaTipo(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getNotaTipo())){
				naturezaoperacao.setNotaTipo(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getOperacaocontabilavista())){
				naturezaoperacao.setOperacaocontabilavista(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getOperacaocontabilaprazo())){
				naturezaoperacao.setOperacaocontabilaprazo(null);
			}
			if(!Hibernate.isInitialized(naturezaoperacao.getOperacaocontabilpagamento())){
				naturezaoperacao.setOperacaocontabilpagamento(null);
			}
		}
		return naturezaoperacao;
	}

	/**
	 * M�todo que retorna a natureza da opera��o pelo c�digo
	 *
	 * @param regimeEspecialTributacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Naturezaoperacao getNaturezaoperacaoForImportacaoxml(String codigonaturezaoperacao) {
		Naturezaoperacao naturezaoperacao = null;
		List<Naturezaoperacao> listaNaturezaoperacao = this.findByNaturezaoperacao(codigonaturezaoperacao);
		if(listaNaturezaoperacao != null && listaNaturezaoperacao.size() > 0)
			naturezaoperacao = listaNaturezaoperacao.get(0);
		return naturezaoperacao;
	}
	
	public List<Naturezaoperacao> findByNaturezaoperacao(String codigonaturezaoperacao) {
		return naturezaoperacaoDAO.findByNaturezaoperacao(codigonaturezaoperacao);
	}
	
	/**
	 * 
	 * @param naturezaoperacao
	 * @param notaTipo
	 * @param ativo
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacao> find(Naturezaoperacao naturezaoperacao, NotaTipo notaTipo, Boolean ativo){
		return naturezaoperacaoDAO.find(naturezaoperacao, notaTipo, ativo);
	}
	
	/**
	 * 
	 * @param q
	 * @author Thiago Clemente
	 * 
	 */
	public List<Naturezaoperacao> findForAutocompleteNotaFiscalProduto(String q){
		return naturezaoperacaoDAO.findForAutocompleteNota(q, NotaTipo.NOTA_FISCAL_PRODUTO);
	}
	
	public List<Naturezaoperacao> findForAutocompleteNotaFiscalServico(String q){
		return naturezaoperacaoDAO.findForAutocompleteNota(q, NotaTipo.NOTA_FISCAL_SERVICO);
	}
	
	public List<Naturezaoperacao> findForAutocompleteNota(String q){
		return naturezaoperacaoDAO.findForAutocompleteNota(q, null);
	}
	
	public List<Naturezaoperacao> findNomes(){
		return naturezaoperacaoDAO.findNomes();
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NaturezaoperacaoDAO#loadForValidaLancamentoImpostoNota(Naturezaoperacao naturezaoperacao)
	*
	* @param naturezaoperacao
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public Naturezaoperacao loadForValidaLancamentoImpostoNota(Naturezaoperacao naturezaoperacao) {
		return naturezaoperacaoDAO.loadForValidaLancamentoImpostoNota(naturezaoperacao);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NaturezaoperacaoDAO#findForSagefiscal(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Naturezaoperacao> findForSagefiscal(String whereIn) {
		return naturezaoperacaoDAO.findForSagefiscal(whereIn);
	}
	
	public ReportTemplateBean getReportTemplateBeanInfoAdicionalProduto(Naturezaoperacao naturezaoperacao){
		if(naturezaoperacao == null || naturezaoperacao.getCdnaturezaoperacao() == null) return null;
		
		Naturezaoperacao bean = load(naturezaoperacao, "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.templateinfproduto");
		return bean != null ? bean.getTemplateinfproduto() : null;
	}
	
	public List<Naturezaoperacao> findByCfops(String whereinCfops) {
		return naturezaoperacaoDAO.findByCfops(whereinCfops);
	}

	public Naturezaoperacao loadForSped(Naturezaoperacao naturezaoperacao) {
		return naturezaoperacaoDAO.loadForSped(naturezaoperacao);
	}
	
	public List<Naturezaoperacao> findByAtivoEntradaFiscalLikeNome(String whereIn) {
		return naturezaoperacaoDAO.findByAtivoEntradaFiscalLikeNome(whereIn);
	}

	public Naturezaoperacao findNaturezaoperacaoConferenciaDeDados(String descricao) {
		return naturezaoperacaoDAO.findNaturezaoperacaoConferenciaDeDados(descricao);
	}
	
	public List<Naturezaoperacao> findByNotaTipoNome (String nome){
		return naturezaoperacaoDAO.findByNotaTipoNome(nome);
	}
	
	public Naturezaoperacao createIfNotExistsByNome(String nome){
		Naturezaoperacao bean = naturezaoperacaoDAO.findByNome(nome);
		if(bean == null){
			bean = new Naturezaoperacao();
			bean.setNome(nome);
			bean.setDescricao(nome);
			bean.setOperacaonfe(Operacaonfe.CONSUMIDOR_FINAL);
			this.saveOrUpdate(bean);
		}
		return bean;
	}
}