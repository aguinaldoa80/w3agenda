package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.dao.ColaboradordespesamotivoDAO;

public class ColaboradordespesamotivoService extends GenericService<Colaboradordespesamotivo>{

	private static ColaboradordespesamotivoService instance;
	private ColaboradordespesamotivoDAO colaboradordespesamotivoDAO;
	
	public void setColaboradordespesamotivoDAO(ColaboradordespesamotivoDAO colaboradordespesamotivoDAO){this.colaboradordespesamotivoDAO = colaboradordespesamotivoDAO;}
	
	public static ColaboradordespesamotivoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordespesamotivoService.class);
		}
		return instance;
	}
	
	public Colaboradordespesamotivo motivoHolerite(){
		Colaboradordespesamotivo colaboradordespesamotivo = colaboradordespesamotivoDAO.motivoHolerite();
		if(colaboradordespesamotivo==null){			
			colaboradordespesamotivo = new Colaboradordespesamotivo();
			colaboradordespesamotivo.setDescricao("holerite");
			colaboradordespesamotivoDAO.saveOrUpdate(colaboradordespesamotivo);
			colaboradordespesamotivo = colaboradordespesamotivoDAO.motivoHolerite();
			return colaboradordespesamotivo;
		}else{
			return colaboradordespesamotivo;
		}
	}
}
