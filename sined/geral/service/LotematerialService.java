package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.dao.LotematerialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LotematerialService extends GenericService<Lotematerial> {
	
	private LotematerialDAO lotematerialDAO;

	public void setLotematerialDAO(LotematerialDAO lotematerialDAO) {this.lotematerialDAO = lotematerialDAO;}
	
	/* singleton */
	private static LotematerialService instance;
	public static LotematerialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LotematerialService.class);
		}
		return instance;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param loteestoque
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 * @since 22/07/2015
	 */
	public List<Lotematerial> findAllByLoteestoque(Loteestoque loteestoque) {
		return lotematerialDAO.findAllByLoteestoqueMaterial(loteestoque, null);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param loteestoque
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/07/2015
	 */
	public List<Lotematerial> findAllByLoteestoqueMaterial(Loteestoque loteestoque, Material material) {
		return lotematerialDAO.findAllByLoteestoqueMaterial(loteestoque, material);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @return
	 * @author Rafael Salvio
	 */
	public boolean existeLotematerial(Material material){
		return lotematerialDAO.existeLotematerial(material);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LotematerialDAO#existeMaterialNoLote(Loteestoque loteestoque, Material material)
	*
	* @param material
	* @param loteestoque
	* @return
	* @since 23/09/2016
	* @author Luiz Fernando
	*/
	public boolean existeMaterialNoLote(Material material, Loteestoque loteestoque) {
		return lotematerialDAO.existeMaterialNoLote(loteestoque, material);
	}

	public boolean existeNumeroFornecedorMaterial(Loteestoque loteestoque, Material material) {
		return lotematerialDAO.existeNumeroFornecedorMaterial(loteestoque, material);
	}
}
