package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.bean.Restricaocontratomaterial;
import br.com.linkcom.sined.geral.dao.RestricaocontratomaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RestricaocontratomaterialService extends GenericService<Restricaocontratomaterial>{
	
	private RestricaocontratomaterialDAO restricaocontratomaterialDAO;
	
	public void setRestricaocontratomaterialDAO(RestricaocontratomaterialDAO restricaocontratomaterialDAO) {this.restricaocontratomaterialDAO = restricaocontratomaterialDAO;}

	/**
	 * Faz referÍncia ao DAO.
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public Restricaocontratomaterial verificaDesbloqueio(Contratomaterial contratomaterial){
		return restricaocontratomaterialDAO.verificaDesbloqueio(contratomaterial);
	}
	
	public Restricaocontratomaterial loadServicoBloqueadoDesbloqueado(Contratomaterial contratomaterial, Restricao restricao){
		return restricaocontratomaterialDAO.loadServicoBloqueadoDesbloqueado(contratomaterial, restricao);
	}
	
	public Restricaocontratomaterial loadServicoBloqueadoDesbloqueado(Integer contratomaterial, Integer cdrestricaocontratomaterial){
		return restricaocontratomaterialDAO.loadServicoBloqueadoDesbloqueado(contratomaterial, cdrestricaocontratomaterial);
	}
	
	public List<Restricaocontratomaterial> restricoesContratoMaterialDesbloquear (Restricao restricao){
		return restricaocontratomaterialDAO.restricoesContratoMaterialDesbloquear(restricao);
	}
	
}
