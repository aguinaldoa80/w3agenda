package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Configuracaoecomerro;
import br.com.linkcom.sined.geral.dao.ConfiguracaoecomerroDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaoecomerroService extends GenericService<Configuracaoecomerro>{
	
	private ConfiguracaoecomerroDAO configuracaoecomerroDAO;
	
	public void setConfiguracaoecomerroDAO(
			ConfiguracaoecomerroDAO configuracaoecomerroDAO) {
		this.configuracaoecomerroDAO = configuracaoecomerroDAO;
	}
	
	/* singleton */
	private static ConfiguracaoecomerroService instance;
	public static ConfiguracaoecomerroService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ConfiguracaoecomerroService.class);
		}
		return instance;
	}
	
	public Configuracaoecomerro getErroByConfiguracaoecom(Configuracaoecom configuracaoecom, String mensagem) {
		return configuracaoecomerroDAO.getErroByConfiguracaoecom(configuracaoecom, mensagem);
	}

	public void addContadorErro(Configuracaoecomerro configuracaoecomerro) {
		configuracaoecomerroDAO.addContadorErro(configuracaoecomerro);
	}
	
}
