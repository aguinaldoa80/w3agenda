package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.dao.ColetaMaterialPedidovendamaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColetaMaterialPedidovendamaterialService extends GenericService<ColetaMaterialPedidovendamaterial> {

	private ColetaMaterialPedidovendamaterialDAO coletaMaterialPedidovendamaterialDAO;
	
	public void setColetaMaterialPedidovendamaterialDAO(ColetaMaterialPedidovendamaterialDAO coletaMaterialPedidovendamaterialDAO) {
		this.coletaMaterialPedidovendamaterialDAO = coletaMaterialPedidovendamaterialDAO;
	}
	
	public List<ColetaMaterialPedidovendamaterial> findNaoRecusados(ColetaMaterial coletaMaterial){
		return coletaMaterialPedidovendamaterialDAO.findNaoRecusados(coletaMaterial);
	}
	
	public void updateRecusado(ColetaMaterial coletaMaterial, Pedidovendamaterial pedidoVendaMaterial){
		coletaMaterialPedidovendamaterialDAO.updateRecusado(coletaMaterial, pedidoVendaMaterial);
	}
}
