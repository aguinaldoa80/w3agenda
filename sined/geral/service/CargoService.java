package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargodepartamento;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.dao.CargoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.CargoReportBean;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.CargoSubReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargoService extends GenericService<Cargo> {
	
	private CargoDAO cargoDAO;
	
	public void setCargoDAO(CargoDAO cargoDAO) {
		this.cargoDAO = cargoDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findCargo(Departamento, String)
	 * @param departamento
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Cargo> findCargo(Departamento departamento){
		return cargoDAO.findCargo(departamento, "");
	}
	/**
	 * M�todo de refer�ncia ao DAO sem o pr�metro String passando String vazia
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findCargo(Departamento, String)
	 * @param departamento
	 * @return
	 * @author Orestes
	 */
	public List<Cargo> findCargo(Departamento departamento, String campos){
		return cargoDAO.findCargo(departamento, campos);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m todos os cargos.
	 * 
	 * @param departamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findAllCargos(Departamento departamento){
		return cargoDAO.findAllCargos(departamento);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findByTarefa(Tarefa) 
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findByTarefa(Tarefa tarefa){
		return cargoDAO.findByTarefa(tarefa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cargo> findAllForFlex(){
		return cargoDAO.findAllForFlex();
	}
	
	
	/**
	 * Carrega a lista de cargo do tipo M�o-de-Obra Direta para ser exibida na parte em flex.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findAllMODForFlex()
	 *
	 * @return List<Cargo>
	 * @author Rodrigo Alvarenga
	 */
	public List<Cargo> findAllMODForFlex(){
		return cargoDAO.findAllMODForFlex();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findByPlanejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Cargo> findByPlanejamento(Planejamento planejamento){
		return cargoDAO.findByPlanejamento(planejamento);
	}
	
	/* singleton */
	private static CargoService instance;
	public static CargoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoService.class);
		}
		return instance;
	}

	public List<Cargo> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return cargoDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findByPlanejamentoTarefa(Planejamento, Tarefa)
	 * @param planejamento
	 * @param tarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findByPlanejamentoTarefa(Planejamento planejamento, Tarefa tarefa){
		return cargoDAO.findByPlanejamentoTarefa(planejamento, tarefa);
	}
	
	/**
	 * Obt�m lista de Colaborador de acordo com a inst�ncia do par�metro.
	 * 
	 * @see #findByPlanejamentoTarefa(Planejamento, Tarefa)
	 * @param object
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Cargo> findByApontamentoFiltro(ApontamentoFiltro filtro){
		
		Planejamento planejamento = null;
		Tarefa tarefa = filtro.getTarefa();
		if(tarefa == null)
			planejamento = filtro.getPlanejamento();
		
		return this.findByPlanejamentoTarefa(planejamento, tarefa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @return
	 * @author Taidson
	 * @since 22/06/2010
	 */
	public List<Cargo> carregaCargos(){
		return cargoDAO.carregaCargos();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o cargo de acordo com a matr�cula.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findByMatricula(Integer matricula)
	 *
	 * @param matricula
	 * @return
	 * @since Jun 13, 2011
	 * @author Rodrigo Freitas
	 */
	public Cargo findByMatricula(Integer matricula) {
		return cargoDAO.findByMatricula(matricula);
	}

	/**
	 * Busca o cargo pelo seu c�digo folha. 
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargoDAO#findByCodigofolha(String codigofolha)
	 *
	 * @param codigofolha
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Cargo findByCodigofolha(Integer codigofolha) {
		return cargoDAO.findByCodigofolha(codigofolha);
	}
	
	/**
	 * 
	 * M�todo que gera o relat�rio de cargo
	 *
	 * @name gerarRelatorio
	 * @param filtro
	 * @return
	 * @return Report
	 * @author Thiago Augusto
	 * @date 07/05/2012
	 *
	 */
	public Report gerarRelatorio(CargoFiltro filtro){
		Report report = new Report("/rh/cargo");
		Report subReport = new Report("/rh/cargo_subreport");
		List<CargoReportBean> listaCargos = this.findForReport(filtro);
		report.addSubReport("CARGO_SUBREPORT", subReport);
		report.setDataSource(listaCargos);
		return report;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Cargo>
	 * @author Thiago Augusto
	 * @date 07/05/2012
	 *
	 */
	public List<CargoReportBean> findForReport(CargoFiltro filtro){
		List<Cargo> listaCargos = cargoDAO.findForReport(filtro);
		List<CargoReportBean> listaCargoReportBean = new ArrayList<CargoReportBean>();
		CargoReportBean bean;
		for (Cargo cargo : listaCargos) {
			bean = new CargoReportBean();
			bean.setNome(cargo.getNome());
			bean.setCbo(cargo.getCbo() != null ? cargo.getCbo().getNome() : "");
			bean.setCodigoFolha(cargo.getCodigoFolha() != null ? cargo.getCodigoFolha().toString() : "");
			if (cargo.getAtivo() != null && cargo.getAtivo()){
				bean.setAtivo("Sim");
			} else {
				bean.setAtivo("N�o");
			}
			List<CargoSubReportBean> listaDepartamento = new ArrayList<CargoSubReportBean>();
			CargoSubReportBean subReportBean;
			for (Cargodepartamento cd : cargo.getListaCargodepartamento()) {
				if (cd.getDepartamento() != null){
					subReportBean = new CargoSubReportBean();
					subReportBean.setDepartamento(cd.getDepartamento().getNome());
					listaDepartamento.add(subReportBean);
				}
			}
			bean.setListaDepartamento(listaDepartamento);
			listaCargoReportBean.add(bean);
		}
		return listaCargoReportBean;
	}
}
