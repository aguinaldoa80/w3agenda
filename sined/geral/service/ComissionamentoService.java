package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.ComissionamentoFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Comissionamentometa;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialcolaborador;
import br.com.linkcom.sined.geral.bean.Materialgrupocomissaovenda;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentodesempenhoconsiderar;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Documentocomissaotipoperiodo;
import br.com.linkcom.sined.geral.bean.view.Vwdocumentocomissaodesempenho;
import br.com.linkcom.sined.geral.dao.ComissionamentoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ComissaoMaterialVenda;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwdocumentocomissaodesempenhoFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentodesempenhoReportBean;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ComissionamentovendaReportBean;
import br.com.linkcom.sined.util.SinedUtil;

public class ComissionamentoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Comissionamento> {

	private ComissionamentoDAO comissionamentoDAO;
	private VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService;
	private DocumentocomissaovendaService documentocomissaovendaService;
	private DocumentoService documentoService;	
	private VendaService vendaService;
	private PedidovendaService pedidovendaService;
	private ComissionamentofaixadescontoService comissionamentofaixadescontoService;
	private ComissionamentoService comissionamentoService;
	private ParametrogeralService parametrogeralService;
	
	public void setComissionamentoDAO(ComissionamentoDAO comissionamentoDAO) {this.comissionamentoDAO = comissionamentoDAO;}
	public void setVwdocumentocomissaodesempenhoService(VwdocumentocomissaodesempenhoService vwdocumentocomissaodesempenhoService) {this.vwdocumentocomissaodesempenhoService = vwdocumentocomissaodesempenhoService;}
	public void setDocumentocomissaovendaService(DocumentocomissaovendaService documentocomissaovendaService) {this.documentocomissaovendaService = documentocomissaovendaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setComissionamentofaixadescontoService(ComissionamentofaixadescontoService comissionamentofaixadescontoService) {this.comissionamentofaixadescontoService = comissionamentofaixadescontoService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	/**
	* M�todo com refer�ncia no DAO
	* busca o comissionamento com o campo deduzirvalor
	* 
	* @see	br.com.linkcom.sined.geral.dao.ComissionamentoDAO#verificaDeducao(Integer cdcomissionamento)
	*
	* @param cdcomissionamento
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public Comissionamento verificaDeducao(Integer cdcomissionamento){
		return comissionamentoDAO.verificaDeducao(cdcomissionamento);
	}
	
//	/**
//	* M�todo para gerar o relat�rio do comissionamento
//	*
//	* @param filtro
//	* @return
//	* @since Oct 11, 2011
//	* @author Luiz Fernando F Silva
//	*/
//	public IReport gerarRelatorioComissionamentoColaborador(DocumentocomissaoFiltro filtro) {
//		
//		Report report = new Report("");
//		
//		if(filtro.getVisaorelatorio() != null){
//			if(filtro.getVisaorelatorio().equals(Visaorelatorio.COLABORADOR))
//				report = new Report("/rh/comissionamentoColaborador");
//			else if(filtro.getVisaorelatorio().equals(Visaorelatorio.CONTRATO))
//				report = new Report("/rh/comissionamentoContrato");
//		}
//		
//		Integer cdcontratoanterior = 0;
//		Money valorliquido = new Money(0);
//		StringBuilder whereIn = new StringBuilder("");
//		List<Contrato> listaContrato = new ArrayList<Contrato>();
//		List<ComissionamentoReportBean> listaCompleta = this.findForReportComissionamento(filtro);
//		List<ComissionamentoReportBean> lista = new ListSet<ComissionamentoReportBean>(ComissionamentoReportBean.class);
//
//		if(listaCompleta != null && !listaCompleta.isEmpty()){		
//			List<ComissionamentoReportBean> listaColaborador = new ListSet<ComissionamentoReportBean>(ComissionamentoReportBean.class);
//			Date ultimoVencimento = null;
//			
//			for(ComissionamentoReportBean bean : listaCompleta){
//				if(bean.getCdcontrato() != null && !bean.getCdcontrato().equals(cdcontratoanterior) && !bean.getCdcontrato().equals(0)){	
//					if(listaColaborador!=null && !listaColaborador.isEmpty()){
//						lista.addAll(listaColaborador);
//						listaColaborador = new ListSet<ComissionamentoReportBean>(ComissionamentoReportBean.class);
//					}
//					ultimoVencimento = bean.getVencimento();
//					listaColaborador.add(bean);
//					
//					if(whereIn.toString().equals("")){
//						whereIn.append(bean.getCdcontrato());
//						cdcontratoanterior = bean.getCdcontrato();
//					}else{
//						whereIn.append(",").append(bean.getCdcontrato());
//						cdcontratoanterior = bean.getCdcontrato();
//					}
//				} 
//				else if(ultimoVencimento != null && filtro.getVisaorelatorio().equals(Visaorelatorio.CONTRATO)){
//					if(bean.getVencimento().after(ultimoVencimento)){
//						ultimoVencimento = bean.getVencimento();
//						listaColaborador = new ListSet<ComissionamentoReportBean>(ComissionamentoReportBean.class);
//						listaColaborador.add(bean);
//					}
//					else if(bean.getVencimento().equals(ultimoVencimento)){
//						listaColaborador.add(bean);
//					}
//				} 
//				else{
//					listaColaborador.add(bean);
//				}
//			}
//			lista.addAll(listaColaborador);
//			
//			if(!whereIn.toString().equals("")){
//				listaContrato = contratoService.findContratoParaCalculoValorcontrato(whereIn.toString());
//				
//				if(listaContrato != null && !listaContrato.isEmpty()){
//					for(Contrato contrato : listaContrato){
//						if(contrato.getCdcontrato() != null){
//							for(ComissionamentoReportBean bean : lista){
//								if( bean.getValordocumento() != null && !filtro.getVisaorelatorio().equals(Visaorelatorio.CONTRATO)){
//									//bean.setValorliquidocontrato(bean.getValorbrutocontrato());
//									bean.setValorliquidocontrato(bean.getValordocumento());
//								}else if(bean.getCdcontrato() != null && bean.getCdcontrato().equals(contrato.getCdcontrato())){
//									valorliquido = contrato.getValorContrato();
//									if(valorliquido.getValue().doubleValue() > 0){
//										bean.setValorliquidocontrato(valorliquido);
//										valorliquido = new Money(0);
//									}else
//										bean.setValorliquidocontrato(bean.getValorbrutocontrato());
//										
//								}									
//							}
//						}
//					}
//					
//					if (filtro.getVisaorelatorio().equals(Visaorelatorio.CONTRATO)){
//						Money valorcomissao = null;
//						for(ComissionamentoReportBean bean : lista){
//							
//							if(bean.getCdcontrato() != null && bean.getCdpessoacomissao() != null){
//								for(Contrato c : listaContrato){
//									if(c.getCdcontrato() != null && bean.getCdcontrato().equals(c.getCdcontrato())){
//										if(c.getVendedor() != null && c.getVendedor().getCdpessoa() != null &&
//												bean.getCdpessoacomissao() != null && bean.getCdpessoacomissao().equals(c.getVendedor().getCdpessoa()) && bean.getPercentual() != null ){
//											bean.setValorcomissao(new Money(bean.getValorliquidocontrato().getValue().doubleValue()*bean.getPercentual()/100));
//											bean.setOrdemcomissao(null);
//										}else{
//											valorcomissao = contapagarService.calculaComissao(c.getListaContratocolaborador(), new Pessoa(bean.getCdpessoacomissao()), bean.getValorliquidocontrato(), c);
//											if(valorcomissao != null && valorcomissao.getValue().doubleValue() == 0)
//												valorcomissao = null;
//										}
//										
//										if(valorcomissao != null){
//											bean.setValorcomissao(valorcomissao);
//											valorcomissao = null;
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}			
//		}
//		
//		if(lista != null && !lista.isEmpty())
//			report.setDataSource(lista);			
//		
//		return report;
//	}
//	
//	public List<ComissionamentoReportBean> findForReportComissionamento(DocumentocomissaoFiltro filtro){
//		return comissionamentoDAO.findForReportComissionamento(filtro);
//	}
	
	
	/**
	 * M�todo que gera o relat�rio de comissionamento de venda
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarRelatorioComissionamentovenda(DocumentocomissaovendaFiltro filtro) {
		Report report = new Report("");
		report = new Report("/rh/comissionamentoVenda");
		
		List<Documentocomissao> lista = documentocomissaovendaService.findForPDF(filtro);
		if(filtro.getDocumentocomissaotipoperiodo() != null && 
				Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
			StringBuilder whereIn = new StringBuilder();
			for (Documentocomissao dc : lista) {
				if(dc.getDocumento() != null && dc.getDocumento().getCddocumento() != null){
					whereIn.append(dc.getDocumento().getCddocumento()).append(",");
				}
			}
			if(!whereIn.toString().equals("")){
				List<Documento> listaDocumento = documentoService.findWithDatapagamento(whereIn.substring(0, whereIn.length()-1));
				if(SinedUtil.isListNotEmpty(listaDocumento)){
					for(Documento documento : listaDocumento){
						if(documento.getDatapagamento() != null){
							for (Documentocomissao dc : lista) {
								if(dc.getDocumento() != null && dc.getDocumento().getCddocumento() != null && 
										dc.getDocumento().getCddocumento().equals(documento.getCddocumento())){
									dc.getDocumento().setDatapagamento(documento.getDatapagamento());
								}
							}
						}
					}
				}
			}
		}
		List<ComissionamentovendaReportBean> listaComissao = new ArrayList<ComissionamentovendaReportBean>();
		for (Documentocomissao dc : lista) {
			ComissionamentovendaReportBean bean = new ComissionamentovendaReportBean();
			
			if (dc.getVenda() != null) {
				bean.setCdvenda(dc.getVenda().getCdvenda());
				bean.setNomecliente(dc.getVenda().getCliente().getNome());
				bean.setNomevendedor(dc.getVenda().getColaborador().getNome());
			}
			
			if (dc.getPedidovenda() != null) {
				bean.setCdpedidovenda(dc.getPedidovenda().getCdpedidovenda());
				bean.setPvnomecliente(dc.getPedidovenda().getCliente().getNome());
				bean.setNomevendedor(dc.getPedidovenda().getColaborador().getNome());
			}
			
			bean.setValorcomissao(dc.getValorcomissao());
//			bean.setValorvenda(dc.getVenda().getValor());
			if(dc.getDocumento() != null){
				bean.setVencimento(dc.getDocumento().getDtvencimento());
				if(filtro.getDocumentocomissaotipoperiodo() != null){
					if(Documentocomissaotipoperiodo.EMISSAO.equals(filtro.getDocumentocomissaotipoperiodo())){
						bean.setVencimento(dc.getDocumento().getDtemissao());
					}else if(Documentocomissaotipoperiodo.PAGAMENTO.equals(filtro.getDocumentocomissaotipoperiodo())){
						bean.setVencimento(dc.getDocumento().getDatapagamento());
					}
				}
			}
			listaComissao.add(bean);
		}
		
		Money totalComissao = new Money();
		for (ComissionamentovendaReportBean crv : listaComissao) {
			if (crv.getValorcomissao() != null)
				totalComissao = totalComissao.add(crv.getValorcomissao());
		}
		
		if(listaComissao != null && !listaComissao.isEmpty())
			report.setDataSource(listaComissao);			
		report.addParameter("totalComissao", totalComissao.toString());
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComissionamentoDAO#findForReportComissionamentovenda(DocumentocomissaovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<ComissionamentovendaReportBean> findForReportComissionamentovenda(DocumentocomissaovendaFiltro filtro) {
		return comissionamentoDAO.findForReportComissionamentovenda(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComissionamentoDAO#findForComissionamentodesempenho(Comissionamento bean)
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Comissionamento> findForComissionamentodesempenho(Comissionamento bean) {
		return comissionamentoDAO.findForComissionamentodesempenho(bean);
	}
	
	public Comissionamento findForCalcularmeta(Comissionamento Comissionamento) {
		return comissionamentoDAO.findForCalcularmeta(Comissionamento);
	}
	
	/**
	 * M�todo que gera o relat�rio de comissionamento de desempenho
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarRelatorioComissionamentodesempenho(VwdocumentocomissaodesempenhoFiltro filtro) {
		Report report = new Report("");
		report = new Report("/rh/comissionamentoDesempenho");
		
		Money totalvenda = new Money();
		Money totalarepassar = new Money();
		Money totalrepassado = new Money();
		List<Vwdocumentocomissaodesempenho> listaVw = vwdocumentocomissaodesempenhoService.findForReportDesempenho(filtro);
		
		String labelvalor = "";
		List<ComissionamentodesempenhoReportBean> lista = new ArrayList<ComissionamentodesempenhoReportBean>();
		if(filtro.getConsiderar() != null && filtro.getConsiderar().equals(Comissionamentodesempenhoconsiderar.TOTAL_VENDA)){
			labelvalor = "Valor de Venda";
			vwdocumentocomissaodesempenhoService.addVendaForTotal(filtro, listaVw);
			totalvenda = filtro.getTotalvenda();
			totalarepassar = filtro.getTotalarepassar();
			totalrepassado = filtro.getTotalrepassado();
			
			for(Vwdocumentocomissaodesempenho dcd : listaVw){
				if(dcd.getVenda() != null){
					ComissionamentodesempenhoReportBean bean = new ComissionamentodesempenhoReportBean();
					bean.setCdcomissionamento(dcd.getCdcomissionamento());
					bean.setNomevendedor(dcd.getNomecolaborador());
					bean.setRepassado(dcd.getCdcolaboradorcomissao() != null && dcd.getCdcolaboradorcomissao() != 0 ? "Sim" : "N�o");
					bean.setValor(dcd.getVenda().getTotalvenda());
					bean.setTipo("Venda");
					lista.add(bean);
				}
			}			
		}else {			
			labelvalor = "Valor do Documento";
			for (Vwdocumentocomissaodesempenho dcd : listaVw) {
				ComissionamentodesempenhoReportBean bean = new ComissionamentodesempenhoReportBean();
				bean.setCdcomissionamento(dcd.getCdcomissionamento());
				bean.setNomevendedor(dcd.getNomecolaborador());
				bean.setRepassado(dcd.getCdcolaboradorcomissao() != null && dcd.getCdcolaboradorcomissao() != 0 ? "Sim" : "N�o");
				bean.setValor(dcd.getValordocumento());
				bean.setTipo(dcd.getCdvenda() != null && dcd.getCdvenda() != 0 ? "Venda" : "Contrato");
				lista.add(bean);
			}
		}
		
		if(lista != null && !lista.isEmpty() && filtro.getColaborador() != null && filtro.getColaborador().getCdpessoa() != null){
			Comissionamento comissionamento = null;	
			if(filtro.getConsiderar() == null || !filtro.getConsiderar().equals(Comissionamentodesempenhoconsiderar.TOTAL_VENDA)){
				totalvenda = vwdocumentocomissaodesempenhoService.findForTotalcomissionamentodesempenho(filtro);
				totalarepassar = vwdocumentocomissaodesempenhoService.findForTotalarepassarcomissionamentodesempenho(filtro);
				totalrepassado = vwdocumentocomissaodesempenhoService.findForTotalrepassadocomissionamentodesempenho(filtro);
			}
			
			Double percentualcomissao = 0.0;
			Double valorcomissao = 0.0;
			Double valorcomissao_aux = 0d;
			Double totalmetavenda_aux  = 0d;
			if(lista.get(0).getCdcomissionamento() != null)
				comissionamento = this.findForCalcularmeta(new Comissionamento(lista.get(0).getCdcomissionamento()));
			
			if(comissionamento != null && comissionamento.getListaComissionamentometa() != null &&
					!comissionamento.getListaComissionamentometa().isEmpty()){
				Boolean acumulativo = comissionamento.getDesempenhocumulativo();
				Boolean diferencaentremetas = comissionamento.getConsiderardiferencaentremetas();
				
				if(acumulativo != null && acumulativo && diferencaentremetas != null && diferencaentremetas){
					HashMap<Integer, Comissionamentometa> mapPosicaoValor = new HashMap<Integer, Comissionamentometa>();
					//ordem deve ser por maior valor
					Collections.sort(comissionamento.getListaComissionamentometa(),new Comparator<Comissionamentometa>(){
						public int compare(Comissionamentometa o1, Comissionamentometa o2) {
							try {
								return o1.getMetavenda().compareTo(o2.getMetavenda());
							}catch (Exception e) {return 0;}
						}
					});
					
					int posicao = 0;
					for(int i = 0; i < comissionamento.getListaComissionamentometa().size(); i++){
						Comissionamentometa cm = comissionamento.getListaComissionamentometa().get(i);
						if(totalvenda != null && cm.getMetavenda() != null &&
								totalvenda.getValue().doubleValue() >= (cm.getMetavenda().getValue().doubleValue() + totalmetavenda_aux)){
							totalmetavenda_aux += cm.getMetavenda().getValue().doubleValue();
							mapPosicaoValor.put(posicao, cm);
							posicao++;
						}
					}
					if(mapPosicaoValor.size() > 0){
						totalmetavenda_aux = totalvenda.getValue().doubleValue();
						for(int i = mapPosicaoValor.size()-1; i >= 0; i--){
							Comissionamentometa cm = mapPosicaoValor.get(i);
							if(cm != null && cm.getMetavenda() != null){
								if(i == 0){
									valorcomissao_aux += totalmetavenda_aux * cm.getComissao();
								}else {
									valorcomissao_aux += cm.getMetavenda().getValue().doubleValue() * cm.getComissao();
									totalmetavenda_aux -= cm.getMetavenda().getValue().doubleValue();
								}
							}
						}
						
						if(valorcomissao_aux != null && valorcomissao_aux != 0){
							valorcomissao = valorcomissao_aux;
						}
					}
				}else {
					for(Comissionamentometa cm : comissionamento.getListaComissionamentometa()){
						if(totalvenda != null && cm.getMetavenda() != null &&
							totalvenda.getValue().doubleValue() >= cm.getMetavenda().getValue().doubleValue()){
								if(acumulativo != null && acumulativo){
									percentualcomissao += cm.getComissao();
								} else {
									percentualcomissao = cm.getComissao();
								}
						}
					}
					if(percentualcomissao != null && percentualcomissao > 0){
						valorcomissao = totalarepassar.getValue().doubleValue() * percentualcomissao;
					}
				}
				
				if(valorcomissao != null && valorcomissao != 0){
					valorcomissao = valorcomissao/100d;
					filtro.setValorcomissao(new Money(valorcomissao));
				}
				
			}			
						
			report.addParameter("labelvalor", labelvalor);
			report.addParameter("nomecolaborador", filtro.getColaborador().getNome());
			report.addParameter("dtinicio", filtro.getDtinicio());
			report.addParameter("dtfim", filtro.getDtfim());
			report.addParameter("totalrepassado", totalrepassado);
			report.addParameter("totalarepassar", totalarepassar);
			report.addParameter("valorcomissao", new Money(valorcomissao));
			report.addParameter("percentualcomissao", percentualcomissao);
			report.addParameter("totalvenda", totalvenda);
		
			report.setDataSource(lista);
		
		}
		
		return report;
	}
	/**
	 * M�todo com refer�ncia ao DAO
	 * @return
	 * @author Thiers Euller
	 */
	public List<Comissionamento> findForContrato(){
		return comissionamentoDAO.findForContrato();
	}
	
	public List<Comissionamento> findForTabelapreco(Comissionamento comissionamento){
		return comissionamentoDAO.findForTabelapreco(comissionamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComissionamentoDAO#findComissionamentoByColaborador(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Comissionamento> findComissionamentoByColaborador(Colaborador colaborador) {
		return comissionamentoDAO.findComissionamentoByColaborador(colaborador);
	}
	
	public boolean existeComissaoMategrial(ComissaoMaterialVenda comissaoMaterialVenda, Venda venda, Vendamaterial vm, Material material, Vendapagamento vendapagamento, Colaboradorcargo colaboradorcargo, 
			Pedidovendatipo pedidovendatipo, Integer qtdeParcelas, Money valorDescontoJuros, boolean validacao, Double valorTotalVenda){
		Double percentualComissaoFaixaDesconto = null;
		boolean existcomissaogrupomaterial = false;
		boolean existcomissaotabelapreco = false;
		boolean existeComissaoMaterial = false;
		boolean achou = false;
		Documentocomissao documentocomissaoByMaterial = null;
		
		Double valortotalmaterial = 0.0;
		Double qtdeMaterial = 0.0;
		comissaoMaterialVenda.setAchou(achou);
		
		boolean CALCULAR_COMISSAO_PROPORCIONAL_VENDA = parametrogeralService.getBoolean(Parametrogeral.CALCULAR_COMISSAO_PROPORCIONAL_VENDA); 
		Double proporcaoParcelaTotal = 1d / qtdeParcelas;
		if(CALCULAR_COMISSAO_PROPORCIONAL_VENDA && vendapagamento.getValororiginal() != null && valorTotalVenda != null && valorTotalVenda > 0) {
			proporcaoParcelaTotal = vendapagamento.getValororiginal().doubleValue() / valorTotalVenda;
		}
		
		if(Hibernate.isInitialized(vm.getComissionamento()) && vm.getComissionamento() != null && vm.getComissionamento().getCdcomissionamento() != null){
			Comissionamento comissionamento = comissionamentoService.loadForEntrada(vm.getComissionamento());
			if (comissionamento != null && comissionamento.getCriteriocomissionamento() != null) {
				if (Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL.equals(comissionamento.getCriteriocomissionamento() != null)) {
					comissaoMaterialVenda.setDividirprincipal(true);
					comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
					existcomissaotabelapreco = true;

					if (comissionamento.getPercentualdivisao() != null && comissionamento.getPercentualdivisao() > 0) comissaoMaterialVenda.setPercentualcomissaovendedorprincipal(comissionamento.getPercentualdivisao());
					else if (comissionamento.getValordivisao() != null && comissionamento.getValordivisao() > 0) comissaoMaterialVenda.setValorcomissionamentogrupovendedorprincipal(comissionamento.getValordivisao());

				} else {
					if (Criteriocomissionamento.SEM_CRITERIO.equals(comissionamento.getCriteriocomissionamento())) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.IGUAL_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && vendaService.isIgualValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.ABAIXO_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && vendaService.isAbaixoValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.ACIMA_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && vendaService.isAcimaValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE.equals(comissionamento.getCriteriocomissionamento()) && vendaService.isPrimeiraVendaCliente(venda.getCliente(), venda.getEmpresa(), venda)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE.equals(comissionamento.getCriteriocomissionamento()) && vendaService.isPrimeiraVendaGrupoCliente(venda.getCliente(), material.getMaterialgrupo(), venda.getEmpresa(), venda)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.FAIXA_DESCONTO.equals(comissionamento.getCriteriocomissionamento())){
						Double percentualItem = (vm.getPercentualdesconto() != null ? vm.getPercentualdesconto() : 0d) + (venda.getPercentualdesconto() != null ? venda.getPercentualdesconto() : 0d);
						percentualComissaoFaixaDesconto = comissionamentofaixadescontoService.getPercentualComissao(comissionamento, percentualItem, vm.getPreco(), vm.getValorvendamaterial());
						if(percentualComissaoFaixaDesconto != null){
							comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
							existcomissaotabelapreco = true;
						}
					}
				}

				if (existcomissaotabelapreco) {
					existeComissaoMaterial = true;
					if(!validacao){
						if(percentualComissaoFaixaDesconto != null) comissaoMaterialVenda.addPercentualcomissao(percentualComissaoFaixaDesconto);
						else if (comissionamento.getPercentual() != null && comissionamento.getPercentual() > 0) comissaoMaterialVenda.addPercentualcomissao(comissionamento.getPercentual());
						else if (comissionamento.getValor() != null && comissionamento.getValor() > 0) comissaoMaterialVenda.setValorcomissionamentogrupo(comissionamento.getValor());
					}
				}

				existcomissaotabelapreco = false;
				percentualComissaoFaixaDesconto = null;
			}
		}else {
		
			if (vm.getMaterial().getListaMaterialcolaborador() == null || vm.getMaterial().getListaMaterialcolaborador().isEmpty()) {
				if (material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getListaMaterialgrupocomissaovenda() != null && !material.getMaterialgrupo().getListaMaterialgrupocomissaovenda().isEmpty()) {
	
					Fornecedor fornecedor = null;
					if (vm.getFornecedor() != null && vm.getFornecedor().getCdpessoa() != null) {
						fornecedor = vm.getFornecedor();
					}
	
					for (Materialgrupocomissaovenda materialgrupocomissaovenda: material.getMaterialgrupo().getListaMaterialgrupocomissaovenda()) {
						if (vendaService.existCargoComissaoDocumentotipo(materialgrupocomissaovenda, vendapagamento.getDocumentotipo(), colaboradorcargo, pedidovendatipo, fornecedor)) {
							percentualComissaoFaixaDesconto = null;
							if (materialgrupocomissaovenda.getComissionamento() != null && materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento() != null) {
								if (Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())) {
									comissaoMaterialVenda.setDividirprincipal(true);
									comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
									existcomissaogrupomaterial = true;
	
									if (materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() != null && materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() > 0) comissaoMaterialVenda.setPercentualcomissaovendedorprincipal(materialgrupocomissaovenda.getComissionamento().getPercentualdivisao());
									else if (materialgrupocomissaovenda.getComissionamento().getValordivisao() != null && materialgrupocomissaovenda.getComissionamento().getValordivisao() > 0) comissaoMaterialVenda.setValorcomissionamentogrupovendedorprincipal(materialgrupocomissaovenda.getComissionamento().getValordivisao());
	
								} else {
									if (Criteriocomissionamento.SEM_CRITERIO.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.IGUAL_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && vendaService.isIgualValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.ABAIXO_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && vendaService.isAbaixoValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.ACIMA_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && vendaService.isAcimaValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && vendaService.isPrimeiraVendaCliente(venda.getCliente(), venda.getEmpresa(), venda)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && vendaService.isPrimeiraVendaGrupoCliente(venda.getCliente(), material.getMaterialgrupo(), venda.getEmpresa(), venda)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.FAIXA_DESCONTO.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())){
										Double percentualItem = (vm.getPercentualdesconto() != null ? vm.getPercentualdesconto() : 0d) + (venda.getPercentualdesconto() != null ? venda.getPercentualdesconto() : 0d);
										percentualComissaoFaixaDesconto = comissionamentofaixadescontoService.getPercentualComissao(materialgrupocomissaovenda.getComissionamento(), percentualItem, vm.getPreco(), vm.getValorvendamaterial());
										if(percentualComissaoFaixaDesconto != null){
											comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
											existcomissaogrupomaterial = true;
										}
									}
								}
	
								if (existcomissaogrupomaterial) {
									existeComissaoMaterial = true;
									if(!validacao){
										if(percentualComissaoFaixaDesconto != null) comissaoMaterialVenda.addPercentualcomissao(percentualComissaoFaixaDesconto);
										else if (materialgrupocomissaovenda.getComissionamento().getPercentual() != null && materialgrupocomissaovenda.getComissionamento().getPercentual() > 0) comissaoMaterialVenda.addPercentualcomissao(materialgrupocomissaovenda.getComissionamento().getPercentual());
										else if (materialgrupocomissaovenda.getComissionamento().getValor() != null && materialgrupocomissaovenda.getComissionamento().getValor() > 0) comissaoMaterialVenda.setValorcomissionamentogrupo(materialgrupocomissaovenda.getComissionamento().getValor());
									}
								}
	
								existcomissaogrupomaterial = false;
								percentualComissaoFaixaDesconto = null;
							}
						}
					}
				}
			}
			
			if (vm.getMaterial() != null && vm.getMaterial().getListaMaterialcolaborador() != null && !vm.getMaterial().getListaMaterialcolaborador().isEmpty() && vm.getPreco() != null) {
				for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
					if (mc.getColaborador() != null && mc.getColaborador().equals(venda.getColaborador()) && vendapagamento.getDocumentotipo() != null && mc.getDocumentotipo() != null && vendapagamento.getDocumentotipo().equals(mc.getDocumentotipo())) {
						if(!validacao){
							if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
								comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
								documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
							}
							documentocomissaoByMaterial.setColaborador(venda.getColaborador());
							if (mc.getPercentual() != null) {
								qtdeMaterial = vm.getQuantidade();
								if (vm.getMultiplicador() != null) {
									qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
								}
								valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
								if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
									valortotalmaterial = valortotalmaterial - vendaService.valorDescontoProporcional(
									venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListavendamaterial());
								}
								
								valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
								// valortotalmaterial = valortotalmaterial / qtdeParcelas;
								
								if (valorDescontoJuros != null) {
									valortotalmaterial += vendaService.valorDescontojurosProporcional(
									valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListavendamaterial());
								}
								//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
								valortotalmaterial = valortotalmaterial * mc.getPercentual();
								comissaoMaterialVenda.addValor(valortotalmaterial);
							} else if (mc.getValor() != null) {
								//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
								comissaoMaterialVenda.setValor(mc.getValor().getValue().doubleValue() * 100);
							}
							comissaoMaterialVenda.setComissaogrupomaterial(false);
						}
						achou = true;
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() == null && vendapagamento.getDocumentotipo() != null && mc.getDocumentotipo() != null && vendapagamento.getDocumentotipo().equals(mc.getDocumentotipo())) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - vendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListavendamaterial());
									}
									
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
									// valortotalmaterial = valortotalmaterial / qtdeParcelas;
									
									if (valorDescontoJuros != null) {
										valortotalmaterial += vendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListavendamaterial());
									}
									//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
									//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
								
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() != null && mc.getDocumentotipo() == null && mc.getColaborador().equals(venda.getColaborador())) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - vendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListavendamaterial());
									}
									
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
									// valortotalmaterial = valortotalmaterial / qtdeParcelas;
									
									if (valorDescontoJuros != null) {
										valortotalmaterial += vendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListavendamaterial());
									}
									//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
								
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() == null && mc.getDocumentotipo() == null) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - vendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListavendamaterial());
									}
									
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
									// valortotalmaterial = valortotalmaterial / qtdeParcelas;
									
									if (valorDescontoJuros != null) {
										valortotalmaterial += vendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListavendamaterial());
									}
									//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
									//a divis�o por 100 � feita no m�todo verificaComissionamento no vendaService
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
		
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
				
				if(achou){
					existeComissaoMaterial = true;
					comissaoMaterialVenda.setAchou(achou);
				}
	
				achou = false;
			}
		}
		
		if(existeComissaoMaterial){
			return true;
		}
		
		return false;
	}
	
	public boolean existeComissaoMategrial(ComissaoMaterialVenda comissaoMaterialVenda, Pedidovenda venda, Pedidovendamaterial vm, Material material, Pedidovendapagamento vendapagamento, Colaboradorcargo colaboradorcargo, 
			Pedidovendatipo pedidovendatipo, Integer qtdeParcelas, Money valorDescontoJuros, boolean validacao, Double valorTotalVenda){
		Double percentualComissaoFaixaDesconto = null;
		boolean existcomissaogrupomaterial = false;
		boolean existcomissaotabelapreco = false;
		boolean existeComissaoMaterial = false;
		boolean achou = false;
		Documentocomissao documentocomissaoByMaterial = null;
		
		Double valortotalmaterial = 0.0;
		Double qtdeMaterial = 0.0;
		
		boolean CALCULAR_COMISSAO_PROPORCIONAL_VENDA = parametrogeralService.getBoolean(Parametrogeral.CALCULAR_COMISSAO_PROPORCIONAL_VENDA); 
		Double proporcaoParcelaTotal = 1d / qtdeParcelas;
		if(CALCULAR_COMISSAO_PROPORCIONAL_VENDA && vendapagamento.getValororiginal() != null && valorTotalVenda != null && valorTotalVenda > 0) {
			proporcaoParcelaTotal = vendapagamento.getValororiginal().doubleValue() / valorTotalVenda;
		}
		
		if(Hibernate.isInitialized(vm.getComissionamento()) && vm.getComissionamento() != null){
			Comissionamento comissionamento = comissionamentoService.loadForEntrada(vm.getComissionamento());
			if (comissionamento != null && comissionamento.getCriteriocomissionamento() != null) {
				if (Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL.equals(comissionamento.getCriteriocomissionamento() != null)) {
					comissaoMaterialVenda.setDividirprincipal(true);
					comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
					existcomissaotabelapreco = true;

					if (comissionamento.getPercentualdivisao() != null && comissionamento.getPercentualdivisao() > 0) comissaoMaterialVenda.setPercentualcomissaovendedorprincipal(comissionamento.getPercentualdivisao());
					else if (comissionamento.getValordivisao() != null && comissionamento.getValordivisao() > 0) comissaoMaterialVenda.setValorcomissionamentogrupovendedorprincipal(comissionamento.getValordivisao());

				} else {
					if (Criteriocomissionamento.SEM_CRITERIO.equals(comissionamento.getCriteriocomissionamento())) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.IGUAL_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && pedidovendaService.isIgualValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.ABAIXO_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && pedidovendaService.isAbaixoValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.ACIMA_VALOR_VENDA.equals(comissionamento.getCriteriocomissionamento()) && pedidovendaService.isAcimaValorvenda(material, venda, vm)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE.equals(comissionamento.getCriteriocomissionamento()) && pedidovendaService.isPrimeiraVendaCliente(venda.getCliente(), venda.getEmpresa(), venda)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE.equals(comissionamento.getCriteriocomissionamento()) && pedidovendaService.isPrimeiraVendaGrupoCliente(venda.getCliente(), material.getMaterialgrupo(), venda.getEmpresa(), venda)) {
						comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
						existcomissaotabelapreco = true;
					} else if (Criteriocomissionamento.FAIXA_DESCONTO.equals(comissionamento.getCriteriocomissionamento())){
						Double percentualItem = (vm.getPercentualdesconto() != null ? vm.getPercentualdesconto() : 0d) + (venda.getPercentualdesconto() != null ? venda.getPercentualdesconto() : 0d);
						percentualComissaoFaixaDesconto = comissionamentofaixadescontoService.getPercentualComissao(comissionamento, percentualItem, vm.getPreco(), vm.getValorvendamaterial());
						if(percentualComissaoFaixaDesconto != null){
							comissaoMaterialVenda.setComissaotabelapreco(Boolean.TRUE);
							existcomissaotabelapreco = true;
						}
					}
				}

				if (existcomissaotabelapreco) {
					existeComissaoMaterial = true;
					if(!validacao){
						if(percentualComissaoFaixaDesconto != null) comissaoMaterialVenda.addPercentualcomissao(percentualComissaoFaixaDesconto);
						else if (comissionamento.getPercentual() != null && comissionamento.getPercentual() > 0) comissaoMaterialVenda.addPercentualcomissao(comissionamento.getPercentual());
						else if (comissionamento.getValor() != null && comissionamento.getValor() > 0) comissaoMaterialVenda.setValorcomissionamentogrupo(comissionamento.getValor());
					}
				}

				existcomissaotabelapreco = false;
				percentualComissaoFaixaDesconto = null;
			}
		}else {
			if (vm.getMaterial().getListaMaterialcolaborador() == null || vm.getMaterial().getListaMaterialcolaborador().isEmpty()) {
				if (material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getListaMaterialgrupocomissaovenda() != null && !material.getMaterialgrupo().getListaMaterialgrupocomissaovenda().isEmpty()) {
	
					Fornecedor fornecedor = null;
					if (vm.getFornecedor() != null && vm.getFornecedor().getCdpessoa() != null) {
						fornecedor = vm.getFornecedor();
					}
	
					for (Materialgrupocomissaovenda materialgrupocomissaovenda: material.getMaterialgrupo().getListaMaterialgrupocomissaovenda()) {
						if (vendaService.existCargoComissaoDocumentotipo(materialgrupocomissaovenda, vendapagamento.getDocumentotipo(), colaboradorcargo, pedidovendatipo, fornecedor)) {
							percentualComissaoFaixaDesconto = null;
							if (materialgrupocomissaovenda.getComissionamento() != null && materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento() != null) {
								if (Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())) {
									comissaoMaterialVenda.setDividirprincipal(true);
									comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
									existcomissaogrupomaterial = true;
	
									if (materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() != null && materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() > 0) comissaoMaterialVenda.setPercentualcomissaovendedorprincipal(materialgrupocomissaovenda.getComissionamento().getPercentualdivisao());
									else if (materialgrupocomissaovenda.getComissionamento().getValordivisao() != null && materialgrupocomissaovenda.getComissionamento().getValordivisao() > 0) comissaoMaterialVenda.setValorcomissionamentogrupovendedorprincipal(materialgrupocomissaovenda.getComissionamento().getValordivisao());
	
								} else {
									if (Criteriocomissionamento.SEM_CRITERIO.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.IGUAL_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && pedidovendaService.isIgualValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.ABAIXO_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && pedidovendaService.isAbaixoValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.ACIMA_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && pedidovendaService.isAcimaValorvenda(material, venda, vm)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && pedidovendaService.isPrimeiraVendaCliente(venda.getCliente(), venda.getEmpresa(), venda)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && pedidovendaService.isPrimeiraVendaGrupoCliente(venda.getCliente(), material.getMaterialgrupo(), venda.getEmpresa(), venda)) {
										comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
										existcomissaogrupomaterial = true;
									} else if (Criteriocomissionamento.FAIXA_DESCONTO.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())){
										Double percentualItem = (vm.getPercentualdesconto() != null ? vm.getPercentualdesconto() : 0d) + (venda.getPercentualdesconto() != null ? venda.getPercentualdesconto() : 0d);
										percentualComissaoFaixaDesconto = comissionamentofaixadescontoService.getPercentualComissao(materialgrupocomissaovenda.getComissionamento(), percentualItem, vm.getPreco(), vm.getValorvendamaterial());
										if(percentualComissaoFaixaDesconto != null){
											comissaoMaterialVenda.setComissaogrupomaterial(Boolean.TRUE);
											existcomissaogrupomaterial = true;
										}
									}
								}
	
								if (existcomissaogrupomaterial) {
									existeComissaoMaterial = true;
									if(!validacao){
										if(percentualComissaoFaixaDesconto != null) comissaoMaterialVenda.addPercentualcomissao(percentualComissaoFaixaDesconto);
										else if (materialgrupocomissaovenda.getComissionamento().getPercentual() != null && materialgrupocomissaovenda.getComissionamento().getPercentual() > 0) comissaoMaterialVenda.addPercentualcomissao(materialgrupocomissaovenda.getComissionamento().getPercentual());
										else if (materialgrupocomissaovenda.getComissionamento().getValor() != null && materialgrupocomissaovenda.getComissionamento().getValor() > 0) comissaoMaterialVenda.setValorcomissionamentogrupo(materialgrupocomissaovenda.getComissionamento().getValor());
									}
								}
	
								existcomissaogrupomaterial = false;
								percentualComissaoFaixaDesconto = null;
							}
						}
					}
				}
			}
			
			if (vm.getMaterial() != null && vm.getMaterial().getListaMaterialcolaborador() != null && !vm.getMaterial().getListaMaterialcolaborador().isEmpty() && vm.getPreco() != null) {
				for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
					if (mc.getColaborador() != null && mc.getColaborador().equals(venda.getColaborador()) && vendapagamento.getDocumentotipo() != null && mc.getDocumentotipo() != null && vendapagamento.getDocumentotipo().equals(mc.getDocumentotipo())) {
						if(!validacao){
							if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
								comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
								documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
							}
							documentocomissaoByMaterial.setColaborador(venda.getColaborador());
							if (mc.getPercentual() != null) {
								qtdeMaterial = vm.getQuantidade();
								if (vm.getMultiplicador() != null) {
									qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
								}
								valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
								if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
									valortotalmaterial = valortotalmaterial - pedidovendaService.valorDescontoProporcional(
									venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListaPedidovendamaterial());
								}
								
								valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
//								valortotalmaterial = valortotalmaterial / qtdeParcelas;
								
								if (valorDescontoJuros != null) {
									valortotalmaterial += pedidovendaService.valorDescontojurosProporcional(
									valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListaPedidovendamaterial());
								}
								valortotalmaterial = valortotalmaterial * mc.getPercentual();
								comissaoMaterialVenda.addValor(valortotalmaterial);
							} else if (mc.getValor() != null) {
								comissaoMaterialVenda.setValor(mc.getValor().getValue().doubleValue() * 100);
							}
							comissaoMaterialVenda.setComissaogrupomaterial(false);
						}
						achou = true;
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() == null && vendapagamento.getDocumentotipo() != null && mc.getDocumentotipo() != null && vendapagamento.getDocumentotipo().equals(mc.getDocumentotipo())) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - pedidovendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListaPedidovendamaterial());
									}
									
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
//									valortotalmaterial = valortotalmaterial / qtdeParcelas;
									
									if (valorDescontoJuros != null) {
										valortotalmaterial += pedidovendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListaPedidovendamaterial());
									}
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
								
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() != null && mc.getDocumentotipo() == null && mc.getColaborador().equals(venda.getColaborador())) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - pedidovendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListaPedidovendamaterial());
									}
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
//									valortotalmaterial = valortotalmaterial / qtdeParcelas;
									if (valorDescontoJuros != null) {
										valortotalmaterial += pedidovendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListaPedidovendamaterial());
									}
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
								
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
	
				if (!achou) {
					for (Materialcolaborador mc: vm.getMaterial().getListaMaterialcolaborador()) {
						if (mc.getColaborador() == null && mc.getDocumentotipo() == null) {
							if(!validacao){
								if (comissaoMaterialVenda.getDocumentocomissaoByMaterial() == null) {
									comissaoMaterialVenda.setDocumentocomissaoByMaterial(new Documentocomissao());
									documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
								}
								documentocomissaoByMaterial.setColaborador(venda.getColaborador());
								if (mc.getPercentual() != null) {
									qtdeMaterial = vm.getQuantidade();
									if (vm.getMultiplicador() != null) {
										qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
									}
									valortotalmaterial = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if (venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0) {
										valortotalmaterial = valortotalmaterial - pedidovendaService.valorDescontoProporcional(
										venda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, venda.getListaPedidovendamaterial());
									}
									valortotalmaterial = valortotalmaterial * proporcaoParcelaTotal;
//									valortotalmaterial = valortotalmaterial / qtdeParcelas;
									if (valorDescontoJuros != null) {
										valortotalmaterial += pedidovendaService.valorDescontojurosProporcional(
										valorDescontoJuros, vm.getQuantidade() * (vm.getMultiplicador() != null && vm.getMultiplicador() > 0 ? vm.getMultiplicador() : 1d), vm.getPreco(), venda.getListaPedidovendamaterial());
									}
									valortotalmaterial = valortotalmaterial * mc.getPercentual();
									comissaoMaterialVenda.addValor(valortotalmaterial);
								} else if (mc.getValor() != null) comissaoMaterialVenda.addValor(mc.getValor().getValue().doubleValue() * 100);
		
								comissaoMaterialVenda.setComissaogrupomaterial(false);
							}
							achou = true;
						}
					}
				}
				
				if(achou){
					existeComissaoMaterial = true;
					comissaoMaterialVenda.setAchou(achou);
				}
	
				achou = false;
			}
		}
		
		if(existeComissaoMaterial){
			return true;
		}
		
		return false;
	}

	public Comissionamento findByCriterio(Criteriocomissionamento criteriocomissionamento) {
		return comissionamentoDAO.findByCriterio(criteriocomissionamento);
	}
	
	public Boolean possuiFaixasMarkupRepetidas(List<ComissionamentoFaixaMarkup> lista) {
		if(SinedUtil.isListNotEmpty(lista) && lista.size() > 1) {
			for(int i = 0; i < lista.size(); i++) {
				for(int j = 0; j < lista.size(); j++) {
					if(i != j && lista.get(i).getFaixa() != null && lista.get(i).getFaixa().equals(lista.get(j).getFaixa())) {
						return true;
					}
				}
			}
		}
		
		return false;
	}

	public Comissionamento findByNome(String nome) {
		if (nome != null && !nome.isEmpty()) {
			return comissionamentoDAO.findByNome(nome);
		} else {
			return null;
		}
	}
}