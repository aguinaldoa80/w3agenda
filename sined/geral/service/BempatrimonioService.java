package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Bempatrimonio;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.dao.BempatrimonioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BempatrimonioService extends GenericService<Bempatrimonio> {

	private BempatrimonioDAO bempatrimonioDAO;
	
	public void setBempatrimonioDAO(BempatrimonioDAO bempatrimonioDAO) {
		this.bempatrimonioDAO = bempatrimonioDAO;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#deletePatrimonio(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deletePatrimonio(Material material) {
		bempatrimonioDAO.deletePatrimonio(material);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#insertPatrimonio(Bempatrimonio)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void insertPatrimonio(Bempatrimonio bem) {
		bempatrimonioDAO.insertPatrimonio(bem);		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#carregaPatrimonio(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Bempatrimonio carregaPatrimonio(Material material){
		return bempatrimonioDAO.carregaPatrimonio(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#updatePatrimonio(Bempatrimonio)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updatePatrimonio(Bempatrimonio bem){
		bempatrimonioDAO.updatePatrimonio(bem);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#countPatrimonio(Material)
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Long countPatrimonio(Material material){
		return bempatrimonioDAO.countPatrimonio(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.BempatrimonioDAO#verificaPatrimonio
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaPatrimonio(Material bean){
		return bempatrimonioDAO.verificaPatrimonio(bean);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @param materialtipo
	 * @param materialgrupo
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Bempatrimonio> findByTipoGrupo(Materialgrupo materialgrupo, Materialtipo materialtipo){
		return bempatrimonioDAO.findByTipoGrupo(materialgrupo, materialtipo);
	}
	
	public List<Bempatrimonio> findByTipoGrupo(String queyr, 
			Materialgrupo materialgrupo, Materialtipo materialtipo){
		return bempatrimonioDAO.findByTipoGrupo(materialgrupo, materialtipo);
	}
	
	/**
	 * 
	 * M�todo que verifica se existe patrim�nio na hora de baixar a entrega, 
	 * caso n�o exista e o material seja patrim�nio ele insere o patrim�nio para que n�o ocorra a baixa.
	 *
	 * @name verificaBemPatrimonio
	 * @param bean
	 * @return void
	 * @author Thiago Augusto
	 * @date 09/05/2012
	 *
	 */
	public boolean verificaBemPatrimonio(Bempatrimonio bean){
		boolean retorno = false;
		Bempatrimonio bempatrimonio = this.carregaPatrimonio(new Material(bean.getCdmaterial()));
		if (bempatrimonio == null){
			retorno = true;
		} 
		return retorno;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public List<Bempatrimonio> findForAutocompltePatrimonio(String q){
		return bempatrimonioDAO.findForAutocompltePatrimonio(q);
	}

	public List<Bempatrimonio> findBemPatrimonioAutoComplete(String query){
		Materialgrupo grupo = null;
		Materialtipo tipo = null;
		try{
			grupo = new Materialgrupo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("materialgrupo")));
		}catch(Exception e1){}
		try{
			tipo = new Materialtipo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("materialtipo")));
		}catch(Exception e2){}
		return bempatrimonioDAO.findBemPatrimonioAutoComplete(query, tipo, grupo);
	}
}
