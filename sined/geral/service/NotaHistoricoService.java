package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.dao.NotaHistoricoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaHistoricoService extends GenericService<NotaHistorico>{

	private NotaHistoricoDAO notaHistoricoDAO;
	private NotaService notaService;

	public void setNotaHistoricoDAO(NotaHistoricoDAO notaHistoricoDAO) {this.notaHistoricoDAO = notaHistoricoDAO;}
	public void setNotaService(NotaService notaService) {this.notaService = notaService;}


	/**
	 * Fornece o hist�rico completo de uma nota, ordenado de forma com que as �ltimas
	 * a��es apare�am primeiro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaHistoricoDAO#carregarListaHistorico(Nota)
	 * @param nota
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<NotaHistorico> carregarListaHistorico(Nota nota) {
		return notaHistoricoDAO.carregarListaHistorico(nota);
	}
	
	/**
	 * Cria uma entrada de hist�rico com status EMITIDA e associa
	 * � nota.
	 * 
	 * @see br.com.linkcom.sined.geral.bean.NotaHistorico
	 * @param nota
	 * @author Hugo Ferreira
	 */
	public void adicionaHistoricoEmitida(Nota nota) {
		this.adicionaHistoricoEmitidaObservacao(nota, null);
	}
	
	public void adicionaHistoricoEmitidaObservacao(Nota nota, String obs) {
		NotaHistorico notaHistorico = new NotaHistorico(null, nota, NotaStatus.EMITIDA, obs,
				SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
	
		List<NotaHistorico> listaNotaHistorico = new ArrayList<NotaHistorico>();
		listaNotaHistorico.add(notaHistorico);
		nota.setListaNotaHistorico(listaNotaHistorico);
	}
	
	/**
	 * M�todo que adiciona um hist�rico quando a nota � alterada
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaHistoricoDAO#carregarListaHistorico(Nota nota)
	 *
	 * @param nota
	 * @author Luiz Fernando
	 */
	public void adicionaHistoricoAlterada(Nota nota) {
		if(nota != null && nota.getCdNota() != null){
			NotaHistorico notaHistorico = new NotaHistorico(null, nota, NotaStatus.ALTERADA, "",
						SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
			
			Nota nf = notaService.load(nota, "nota.cdNota, nota.numero, nota.notaTipo");
			if(nf != null && nf.getNotaTipo() != null && (NotaTipo.NOTA_FISCAL_SERVICO.equals(nf.getNotaTipo()) || NotaTipo.NOTA_FISCAL_PRODUTO.equals(nf.getNotaTipo()))){
				String obs = "";
				if(nota.getNumero() == null && (nf.getNumero() != null && !"".equals(nf.getNumero()))){
					obs = "N�mero da nota alterado. N�mero anterior: " + nf.getNumero();
				}else if(nf.getNumero() == null && (nota.getNumero() != null && !"".equals(nota.getNumero()))){
					obs = "N�mero da nota alterado. N�mero anterior n�o estava preenchido.";
				}else if(nf.getNumero() != null && nota.getNumero() != null && !nf.getNumero().equals(nota.getNumero())){
					obs = "N�mero da nota alterado. N�mero anterior: " + nf.getNumero();
				}
				if(!"".equals(obs)){
					notaHistorico.setObservacao(obs);
				}
			}
			
			if(nota.getDiferencaLancamentoConfirmado() != null && nota.getDiferencaLancamentoConfirmado()){
				notaHistorico.setObservacao("Inconsist�ncia nos lan�amentos tribut�veis. " + 
						(StringUtils.isNotEmpty(notaHistorico.getObservacao()) ? notaHistorico.getObservacao() : ""));
			}
			
			List<NotaHistorico> listaNotaHistorico = notaHistoricoDAO.carregarListaHistorico(nota);
			if(listaNotaHistorico == null)
				listaNotaHistorico = new ArrayList<NotaHistorico>();
			
			listaNotaHistorico.add(notaHistorico);
			nota.setListaNotaHistorico(listaNotaHistorico);
		}
	}
	
	/**
	 * Cria e retorna uma inst�ncia de DocumentoHistorico com as informa��es
	 * necess�rias para a liquida��o de uma nota.
	 *
	 * @param nota : obrigat�rio
	 * @param list : obrigat�rio
	 * 
	 * @throws SinedException : caso algum par�metro recebido seja null
	 * 
	 * @return NotaHistorico
	 * @author Hugo Ferreira
	 */
	public NotaHistorico criarHistoricoLiquidacao(Nota nota, List<NotaDocumento> list) {
		if (nota == null || nota.getCdNota() == null) throw new SinedException("O par�metro nota na fun��o criarHistoricoLiquidacao" +
				" n�o foi informado e n�o pode ser nulo.");
		if (list == null) throw new SinedException("O par�metro cdPrimeiraParcela na fun��o criarHistoricoLiquidacao" +
				" n�o foi informado e n�o pode ser nulo.");
		
		StringBuilder sb = new StringBuilder();
		sb.append("Foi gerada a(s) conta(s) a receber ");
		
		for (int i = 0; i < list.size(); i++) {
			if(i > 0) sb.append(", ");
			sb.append("<a href=\"javascript:visualizarContaReceber(");
			sb.append(list.get(i).getDocumento().getCddocumento());
			sb.append(");\">");
			sb.append(list.get(i).getDocumento().getCddocumento());
			sb.append("</a>");
		}
		sb.append(".");
		
		NotaHistorico notaHistorico = new NotaHistorico(null, nota, NotaStatus.LIQUIDADA, sb.toString(),
				SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
		
		return notaHistorico;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 05/01/2012
	 * @param notaHistorico
	 * @return
	 */
	public List<NotaHistorico> carregarNotaHistorico(String whereIn){
		return notaHistoricoDAO.carregarNotaHistorico(whereIn);
	}

	/**
	 * Retorna a primeira ocorr�ncia da lista de historico das situa��es da nota passada por array
	 *
	 * @param nota
	 * @param notaStatus
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public NotaStatus getNotaStatusLast(Nota nota, NotaStatus... notaStatus) {
		List<NotaHistorico> listaHistorico = this.carregarListaHistorico(nota);
		
		for (NotaHistorico hist : listaHistorico) {
			for (int i = 0; i < notaStatus.length; i++) {
				if(hist.getNotaStatus() != null && hist.getNotaStatus().equals(notaStatus[i])){
					return hist.getNotaStatus();
				}
			}
		}
		
		return null;
	}

	/**
	 * M�todo que cria hist�rico na nota ao enviar arquivo para o cliente
	 *
	 * @param notafiscalproduto
	 * @param envioemailtipo
	 * @param observacao
	 * @author Luiz Fernando
	 */
	public void criaNotaHistoricoEnvioEmail(Notafiscalproduto notafiscalproduto, Envioemail envioemail, String observacao) {
		if(notafiscalproduto != null && notafiscalproduto.getCdNota() != null){
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setNota(notafiscalproduto);
			notaHistorico.setNotaStatus(NotaStatus.ENVIO_EMAIL);
			
			String obs = "";
			if(envioemail != null && envioemail.getCdenvioemail() != null && envioemail.getEnvioemailtipo() != null){
				if(Envioemailtipo.ENVIO_DANFE_CLIENTE.equals(envioemail.getEnvioemailtipo())){
					obs = "Envio de DANFE para o cliente. " +
						"<a href=\"javascript:visualizarEnvioemail("+envioemail.getCdenvioemail()+","+ envioemail.getEnvioemailtipo().getCdenvioemailtipo()+")\"> Clique aqui </a> para visualizar o registro de envio do cliente. ";
				}else if(Envioemailtipo.ENVIO_NFE_CLIENTE.equals(envioemail.getEnvioemailtipo())){
					obs = "Envio de NFE para o cliente. " +
						"<a href=\"javascript:visualizarEnvioemail("+envioemail.getCdenvioemail()+","+ envioemail.getEnvioemailtipo().getCdenvioemailtipo()+")\"> Clique aqui </a> para visualizar o registro de envio do cliente. ";
				}
			}
			if(observacao != null){
				obs += observacao;
			}
			notaHistorico.setObservacao(obs);
			this.saveOrUpdate(notaHistorico);
		}
	}
	
	/**
	 * 
	 * @param idnota
	 */
	public void adicionaHistoricoAtualizar(final String idnota) {
		notaHistoricoDAO.getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				String[] notas = idnota.split(",");
				for (String nota : notas) {
					Integer cdnota = Integer.parseInt(nota);
					Nota nf = new Nota(cdnota);
					NotaStatus notaStatus = notaService.getNotaStatus(nf);
					String observacao = "Atualiza��o de Dados. \n* NF Servi�o: CST Pis e CST Cofins\n* NF Produto: Local de Armazenagem ";
					NotaHistorico notaHistorico = new NotaHistorico(null,nf,notaStatus,observacao,SinedUtil.getUsuarioLogado().getCdpessoa(),new Timestamp(System.currentTimeMillis()));
					saveOrUpdate(notaHistorico);
				}
				return null;
			}
		});
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/07/2014
	 */
	public List<NotaHistorico> findForResumo(String whereInNota) {
		return notaHistoricoDAO.findForResumo(whereInNota);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaHistoricoDAO#carregarUltimoHistorico(Nota nota)
	*
	* @param nota
	* @return
	* @since 22/03/2017
	* @author Luiz Fernando
	*/
	public NotaHistorico carregarUltimoHistorico(Nota nota, NotaStatus notaStatus) {
		return notaHistoricoDAO.carregarUltimoHistorico(nota, notaStatus);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/12/2017
	 */
	public NotaHistorico carregarUltimoHistorico(Nota nota) {
		return notaHistoricoDAO.carregarUltimoHistorico(nota, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaHistoricoDAO#updateObservacao(NotaHistorico notaHistorico, String observacao)
	*
	* @param notaHistorico
	* @param observacao
	* @since 22/03/2017
	* @author Luiz Fernando
	*/
	public void updateObservacao(NotaHistorico notaHistorico, String observacao) {
		notaHistoricoDAO.updateObservacao(notaHistorico, observacao);
	}
	
	/**
	* M�todo para salvar historico da nf mostrando a ordem de servico que a gerou
	*
	* @param Notafiscalproduto
	* @param whereInRequisicao
	* @since 16/03/18
	* @author Fabricio Berg
	*/
	public void salvarHistoricoNotaRequisicao(Nota bean, String whereInRequisicao) {
		StringBuilder observacao = new StringBuilder();
		String observacaoString = "";
		
		observacao.append("Gerada a partir da ordem de servi�o: ");
		
		NotaHistorico notaHistorico = new NotaHistorico();
		for (String s : whereInRequisicao.split(",")) {
			observacao.append("<a href=\"/w3erp/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao=");
			observacao.append(s + "\">" + s);
			observacao.append("</a>,");
		}
		
		observacaoString = observacao.toString();
		observacaoString = observacaoString.substring(0, observacao.length() - 1);
		
		notaHistorico.setNota(bean);
		notaHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
		notaHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		notaHistorico.setObservacao(observacaoString);
		notaHistorico.setNotaStatus(bean.getNotaStatus());
		
		notaHistoricoDAO.saveOrUpdate(notaHistorico);
	}
}