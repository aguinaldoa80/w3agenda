package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.ContaContabilSaldoInicial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContaCompensacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.dao.ContacontabilDAO;
import br.com.linkcom.sined.modulo.contabil.controller.crud.filter.ContacontabilFiltro;
import br.com.linkcom.sined.modulo.contabil.controller.report.bean.EmitirBalancoPatrimonialBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.AnaliseReceitaDespesaReportFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacontabilService extends GenericService<ContaContabil> {

	public static final String FILTRO_SESSION = "FILTRO_ANALISE_RECEITAS_DESPESAS";
	private ContacontabilDAO contacontabilDAO;
	private ParametrogeralService parametrogeralService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;

	public void setContacontabilDAO(ContacontabilDAO contacontabilDAO) {
		this.contacontabilDAO = contacontabilDAO;
	}

	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	private static ContacontabilService instance;

	public static ContacontabilService getInstance() {
		if (instance == null) {
			instance = Neo.getObject(ContacontabilService.class);
		}
		return instance;
	}
	public void setMaterialCustoEmpresaService(
			MaterialCustoEmpresaService materialCustoEmpresaService) {
		this.materialCustoEmpresaService = materialCustoEmpresaService;
	}

	@Override
	public void saveOrUpdate(ContaContabil bean) {
		super.saveOrUpdate(bean);
		if (BooleanUtils.isTrue(bean.getAnaliticacomregistros())) {
			contacontabilDAO.updateRelacionamento(bean);
		}
	}

	public ContaContabil carregaConta(ContaContabil contaContabil) {
		return contacontabilDAO.carregaConta(contaContabil);
	}

	public List<ContaContabil> findFilhos(ContaContabil pai) {
		return contacontabilDAO.findFilhos(pai);
	}

	public List<ContaContabil> findRaiz() {
		return contacontabilDAO.findRaiz();
	}

	public Boolean isAnalitica(ContaContabil contacontabil) {
		return contacontabilDAO.haveRegister(contacontabil);
	}

	public List<ContaContabil> findAnaliticas(Tipooperacao tipooperacao) {
		return contacontabilDAO.findAnaliticas(tipooperacao);
	}

	public List<ContaContabil> findAnaliticasByNatureza(
			Tipooperacao tipooperacao, String whereInNatureza) {
		return contacontabilDAO.findAnaliticasByNatureza(tipooperacao,
				whereInNatureza);
	}

	public List<ContaContabil> findByNatureza(String whereInNatureza) {
		return contacontabilDAO.findByNatureza(whereInNatureza);
	}

	public List<ContaContabil> findAnaliticasDebito(String whereIn) {
		return findAnaliticas(whereIn, Tipooperacao.TIPO_DEBITO);
	}

	public List<ContaContabil> findAnaliticasCredito(String whereIn) {
		return findAnaliticas(whereIn, Tipooperacao.TIPO_CREDITO);
	}

	public List<ContaContabil> findAnaliticas(String whereIn,
			Tipooperacao tipooperacao) {
		return contacontabilDAO.findAnaliticas(whereIn, tipooperacao);
	}

	public void preparaBeanParaSalvar(ContaContabil bean) {
		if (bean.getPai() != null) {
			bean.setContaContabilPai(bean.getPai());
			bean.setTipooperacao(this.carregaConta(bean.getContaContabilPai())
					.getTipooperacao());
		}

		if (bean.getItem() == null) {
			// setar o campo item para gera��o do identificador
			if (bean.getContaContabilPai() != null
					&& bean.getContaContabilPai().getCdcontacontabil() != null) {
				List<ContaContabil> listaFilhos = findFilhos(bean
						.getContaContabilPai());
				if (listaFilhos.size() > 0) {
					bean.setItem(listaFilhos.get(0).getItem() + 1);
					bean.setFormato(listaFilhos.get(0).getFormato());
				} else {
					bean.setItem(1);
				}
			} else {
				List<ContaContabil> listaPais = findRaiz();
				if (listaPais.size() > 0) {
					if(listaPais.get(0).getItem()!=null){
						bean.setItem(listaPais.get(0).getItem() + 1);						
					}
					if(listaPais.get(0).getFormato()!=null){
						bean.setFormato(listaPais.get(0).getFormato());						
					}
				} else {
					bean.setItem(1);
				}
			}
		}

		if (bean.getTipooperacao() == null) {
			bean.setTipooperacao(bean.getTipooperacao2());
		}
	}

	public List<ContaContabil> findByTipooperacao(Tipooperacao tipooperacao) {
		return contacontabilDAO.findByTipooperacao(tipooperacao);
	}

	public List<ContaContabil> findByTipooperacaoNatureza(Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza, Integer cdEmpresa) {
		return contacontabilDAO.findByTipooperacaoNatureza(tipooperacao, whereInCdcontagerencial, whereInNatureza, whereNotInNatureza, cdEmpresa);
	}

	public List<ContaContabil> findForError(String listaContaContabil) {
		return contacontabilDAO.findForError(listaContaContabil);
	}

	public void calcularValores(List<AnaliseRecDespBean> listaCompleta,
			Boolean contascomlancamentos) {
		List<AnaliseRecDespBean> listaTrabalho = null;
		List<AnaliseRecDespBean> listaIrmas = null;
		List<AnaliseRecDespBean> listaZerados = new ArrayList<AnaliseRecDespBean>();
		AnaliseRecDespBean filho = null;
		AnaliseRecDespBean pai = null;

		int nivel = this.encontrarMaiorNivel(listaCompleta);

		while (nivel > 1) {
			listaTrabalho = this.buscarTodasNivel(listaCompleta, nivel);
			while (!listaTrabalho.isEmpty()) {
				filho = listaTrabalho.get(0);
				listaIrmas = this.buscarIrmas(listaTrabalho, filho);
				pai = this.buscarPai(listaCompleta, filho);
				if (pai != null) {
					pai.setValor(this.somarValores(listaIrmas));

					for (AnaliseRecDespBean irmas : listaIrmas) {
						if (irmas.getItemIncluidoControleOrcamento() != null
								&& irmas.getItemIncluidoControleOrcamento()) {
							pai.setItemIncluidoControleOrcamento(irmas
									.getItemIncluidoControleOrcamento());
						}
					}
				}
				listaTrabalho.removeAll(listaIrmas);
			}
			nivel -= 1;
		}

		if (contascomlancamentos)
			for (AnaliseRecDespBean analiseRecDespBean : listaCompleta)
				if (analiseRecDespBean.getValor().getValue().doubleValue() == 0.0
						&& (analiseRecDespBean
								.getItemIncluidoControleOrcamento() == null || !analiseRecDespBean
								.getItemIncluidoControleOrcamento()))
					listaZerados.add(analiseRecDespBean);

		if (listaZerados != null && !listaZerados.isEmpty())
			listaCompleta.removeAll(listaZerados);

	}

	private List<AnaliseRecDespBean> buscarTodasNivel(
			List<AnaliseRecDespBean> listaCompleta, int nivel) {
		List<AnaliseRecDespBean> listaMesmoNivel = new ArrayList<AnaliseRecDespBean>();

		for (AnaliseRecDespBean bean : listaCompleta) {
			if (bean.getIdentificador().split("\\.").length == nivel
					&& ((bean.getAtivo() != null && bean.getAtivo()) || bean
							.getAtivo() == null))
				listaMesmoNivel.add(bean);
		}

		return listaMesmoNivel;
	}

	private int encontrarMaiorNivel(List<AnaliseRecDespBean> listaCompleta) {
		if (listaCompleta == null || listaCompleta.isEmpty())
			return -1;

		int tamMax = -1;
		for (AnaliseRecDespBean bean : listaCompleta) {
			if (bean.getIdentificador().split("\\.").length > tamMax)
				tamMax = bean.getIdentificador().split("\\.").length;
		}
		return tamMax;
	}

	private List<AnaliseRecDespBean> buscarIrmas(
			List<AnaliseRecDespBean> lista, AnaliseRecDespBean contaContabil) {
		String id = contaContabil.getIdentificador();
		String prefixo = id.split("\\.").length > 1 ? StringUtils.substring(id,
				0, (id.length() - (id.split("\\.")[id.split("\\.").length - 1]
						.length() + 1))) : "";
		if (StringUtils.isEmpty(prefixo))
			return null;

		AnaliseRecDespBean irma = null;
		List<AnaliseRecDespBean> listaIrmas = new ArrayList<AnaliseRecDespBean>();

		int index = lista.indexOf(contaContabil);
		listaIrmas.add(lista.get(index));
		index++;

		while (lista.size() > index) {
			irma = lista.get(index);
			if (irma.getIdentificador().startsWith(prefixo)) {
				listaIrmas.add(irma);
			} else {
				break;
			}

			index++;
		}

		return listaIrmas;
	}

	private Money somarValores(List<AnaliseRecDespBean> lista) {
		Money somatorio = new Money();
		for (AnaliseRecDespBean recDespBean : lista) {
			somatorio = somatorio.add(recDespBean.getValor());
		}
		return somatorio;
	}

	public void inserirFolhasNaListaCompleta(
			List<AnaliseRecDespBean> listaCompleta,
			List<AnaliseRecDespBean> listaFolhas) {
		for (AnaliseRecDespBean folha : listaFolhas) {
			if (listaCompleta.contains(folha)) {
				int index = listaCompleta.indexOf(folha);
				listaCompleta.set(index, folha);
			}
		}
	}

	private AnaliseRecDespBean buscarPai(List<AnaliseRecDespBean> lista,
			AnaliseRecDespBean filho) {
		int indexPai = Collections.binarySearch(lista, filho,
				AnaliseRecDespBean.PAI_COMPARATOR);

		if (indexPai < 0) {
			return null;
		}
		return lista.get(indexPai);
	}

	private double calcularPercentual(AnaliseRecDespBean filho,
			AnaliseRecDespBean pai) {
		if (pai == null) { // N�vel 0
			return 1D;
		}

		double valorPai = pai.getValor().getValue().doubleValue();
		double valorFilho = filho.getValor().getValue().doubleValue();

		return valorFilho / valorPai;
	}

	private void edentar(AnaliseRecDespBean nodo, int numEspacos) {
		String id = nodo.getIdentificador();

		int tamId = id.length();
		int nivel = id.split("\\.").length * 2;
		int tamTotal = tamId + nivel * numEspacos;

		id = br.com.linkcom.neo.util.StringUtils.stringCheia(id, " ", tamTotal,
				false);
		nodo.setIdentificador(id);
	}

	private void edentarNome(AnaliseRecDespBean nodo, int numEspacos) {
		String nome = nodo.getNome();
		String id = nodo.getIdentificador();

		int nivel = id.split("\\.").length * 2;
		int tamTotal = nodo.getNome().length() + nivel * numEspacos;

		nome = br.com.linkcom.neo.util.StringUtils.stringCheia(nome, " ",
				tamTotal, false);
		nodo.setNome(nome);
	}

	public Map<String, Money> totalizar(List<AnaliseRecDespBean> listaCompleta,
			Boolean flex) {
		AnaliseRecDespBean pai = null;
		Money totalCredito = new Money();
		Money totalDebito = new Money();

		for (AnaliseRecDespBean nodoAtual : listaCompleta) {
			// C�lculo do percentual
			pai = this.buscarPai(listaCompleta, nodoAtual);
			nodoAtual.setPercentual(this.calcularPercentual(nodoAtual, pai));

			if (!flex) {
				this.edentar(nodoAtual, 2);
			} else {
				this.edentarNome(nodoAtual, 2);
			}
			if (pai == null) {
				if (nodoAtual.getOperacao().equals("C")) {
					totalCredito = totalCredito.add(nodoAtual.getValor());
				} else {
					totalDebito = totalDebito.add(nodoAtual.getValor());
				}
			}
		}

		Map<String, Money> map = new HashMap<String, Money>();
		map.put("totalCredito", totalCredito);
		map.put("totalDebito", totalDebito);
		return map;
	}

	public List<ContaContabil> findContasContabeis(String whereIn) {
		return contacontabilDAO.findContasContabeis(whereIn);
	}

	public ContaContabil findContaContabilVeiculo(Veiculo veiculo) {
		return contacontabilDAO.findContaContabilVeiculo(veiculo);
	}

	public ContaContabil loadWithIdentificador(ContaContabil contacontabil) {
		return contacontabilDAO.loadWithIdentificador(contacontabil);
	}

	public List<ContaContabil> findAllFilhos(ContaContabil cg) {
		return contacontabilDAO.findAllFilhos(cg);
	}

	public ContaContabil findPaiNivel1(ContaContabil contacontabil) {
		return contacontabilDAO.findPaiNivel1(contacontabil);
	}

	public ContaContabil findByIdentificador(String identificador) {
		return contacontabilDAO.findByIdentificador(identificador);
	}

	public List<ContaContabil> findAnaliticasDebitoForFlex() {
		return this.findAnaliticas(Tipooperacao.TIPO_DEBITO);
	}

	public List<ContaContabil> findDebitoForFlex() {
		return this.findByTipooperacao(Tipooperacao.TIPO_DEBITO);
	}

	public List<ContaContabil> findForReport(ContacontabilFiltro filtro) {
		return contacontabilDAO.findForReport(filtro);
	}

	public IReport createRelatorioContaContabil(
			ContacontabilFiltro contacontabilfiltro) {
		Report report = new Report("/financeiro/contacontabil");
		List<ContaContabil> lista = this.findForReport(contacontabilfiltro);

		report.setDataSource(lista);
		return report;
	}

	public void ajustaListasAnaliseReceitaDespesaFiltro(
			AnaliseReceitaDespesaReportFiltro filtro) {
		List<ItemDetalhe> listaFornecedor = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaCliente = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaColaborador = new ArrayList<ItemDetalhe>();
		List<ItemDetalhe> listaOutrospagamento = new ArrayList<ItemDetalhe>();

		List<ItemDetalhe> listaPagamentoa = filtro.getListaPagamentoa();
		List<ItemDetalhe> listaRecebimentode = filtro.getListaRecebimentode();

		if (SinedUtil.isListNotEmpty(listaPagamentoa)) {
			for (ItemDetalhe id : listaPagamentoa) {
				id.setRecebidode(false);
				switch (id.getTipopagamento()) {
				case CLIENTE:
					listaCliente.add(id);
					break;
				case COLABORADOR:
					listaColaborador.add(id);
					break;
				case FORNECEDOR:
					listaFornecedor.add(id);
					break;
				case OUTROS:
					listaOutrospagamento.add(id);
					break;
				}
			}
		}

		if (SinedUtil.isListNotEmpty(listaRecebimentode)) {
			for (ItemDetalhe id : listaRecebimentode) {
				id.setRecebidode(true);
				switch (id.getTipopagamento()) {
				case CLIENTE:
					listaCliente.add(id);
					break;
				case COLABORADOR:
					listaColaborador.add(id);
					break;
				case FORNECEDOR:
					listaFornecedor.add(id);
					break;
				case OUTROS:
					listaOutrospagamento.add(id);
					break;
				}
			}
		}

		filtro.setListaFornecedor(listaFornecedor);
		filtro.setListaCliente(listaCliente);
		filtro.setListaColaborador(listaColaborador);
		filtro.setListaOutrospagamento(listaOutrospagamento);
	}

	public List<ContaContabil> findAutocomplete(String q, Tipooperacao tipooperacao, String whereInNatureza, String whereInCdcontagerencial, String nomeCampoEmpresa, String whereInEmpresa, Boolean selectPai) {
		return contacontabilDAO.findAutocomplete(q, tipooperacao, whereInNatureza, whereInCdcontagerencial, nomeCampoEmpresa, whereInEmpresa, selectPai);
	}

	public List<ContaContabil> findTreeView(Tipooperacao tipooperacao, String whereInCdcontagerencial, String whereInNatureza, String whereNotInNatureza, Integer cdEmpresa) {
		List<ContaContabil> lista = this.findByTipooperacaoNatureza(tipooperacao, whereInCdcontagerencial, whereInNatureza, whereNotInNatureza, cdEmpresa);

		List<ContaContabil> raiz = new ArrayList<ContaContabil>();
		for (ContaContabil cg : lista) {
			if (cg.getContaContabilPai() == null || cg.getContaContabilPai().getCdcontacontabil() == null) {
				raiz.add(cg);
			}
		}
		for (ContaContabil pai : raiz) {
			pai.setFilhos(this.somenteFilhos(pai, lista));
		}

		return raiz;
	}

	private List<ContaContabil> somenteFilhos(ContaContabil pai,
			List<ContaContabil> lista) {
		List<ContaContabil> filhos = new ArrayList<ContaContabil>();
		for (ContaContabil cg : lista) {
			if (cg.getContaContabilPai() != null
					&& cg.getContaContabilPai().getCdcontacontabil() != null
					&& cg.getContaContabilPai().getCdcontacontabil()
							.equals(pai.getCdcontacontabil())) {
				cg.setFilhos(somenteFilhos(cg, lista));
				filhos.add(cg);
			}
		}
		return filhos;
	}

	public List<ContaContabil> findInTarefaOrcamento(Orcamento orcamento) {
		return this.findInTarefaOrcamento(orcamento, null);
	}

	public List<ContaContabil> findInTarefaOrcamento(Orcamento orcamento,
			Tarefaorcamento tarefaorcamento) {
		return contacontabilDAO.findInTarefaOrcamento(orcamento,
				tarefaorcamento);
	}

	public List<ContaContabil> findInComposicao(Orcamento orcamento) {
		return this.findInComposicao(orcamento, null);
	}

	public List<ContaContabil> findInComposicao(Orcamento orcamento,
			Composicaoorcamento composicaoorcamento) {
		return contacontabilDAO
				.findInComposicao(orcamento, composicaoorcamento);
	}

	public ContaContabil findByProjetoOrcamentoMOD(Projeto projeto) {
		return contacontabilDAO.findByProjetoOrcamentoMOD(projeto);
	}

	public List<ContaContabil> findForFluxoCaixaMensal(
			List<NaturezaContagerencial> listaNaturezaContagerencial) {
		return contacontabilDAO
				.findForFluxoCaixaMensal(listaNaturezaContagerencial);
	}

	public Iterator<Object[]> findForArquivoGerencial() {
		return contacontabilDAO.findForArquivoGerencial();
	}

	public List<ContaContabil> findAutocomplete(String q) {
		return contacontabilDAO.findAutocomplete(q);
	}

	public List<ContaContabil> loadForSpedPiscofinsReg0500(Set<String> listaIdentificador, String whereInEmpresas) {
		return contacontabilDAO.loadForSpedPiscofinsReg0500(listaIdentificador, whereInEmpresas);
	}

	public Boolean isPai(ContaContabil pai) {
		return contacontabilDAO.isPai(pai);
	}

	public String getWhereInNaturezaResultadoCompensacaoOutras() {
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial
				.add(NaturezaContagerencial.CONTAS_RESULTADO);
		listaNaturezaContagerencial
				.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContagerencial.add(NaturezaContagerencial.OUTRAS);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial,
				"value", ",");
	}

	public List<ContaContabil> findForGerarMovimentacao(
			Tipooperacao tipooperacao) {
		return contacontabilDAO.findForGerarMovimentacao(tipooperacao);
	}

	/**
	 * 
	 * @param contagerencial
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarItem(ContaContabil contacontabil) {
		return contacontabilDAO.verificarItem(contacontabil);
	}

	/**
	 * 
	 * @return
	 * @author Thiago Clemente
	 */
	public String getWhereInNaturezaContaGerencial() {
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial,
				"value", ",");
	}

	/**
	 * 
	 * @return
	 * @author Thiago Clemente
	 */
	public String getWhereInNaturezaCompensacaoContaGerencialOutras() {
		List<NaturezaContagerencial> listaNaturezaContagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezaContagerencial
				.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		listaNaturezaContagerencial.add(NaturezaContagerencial.CONTA_GERENCIAL);
		listaNaturezaContagerencial.add(NaturezaContagerencial.OUTRAS);
		return CollectionsUtil.listAndConcatenate(listaNaturezaContagerencial,
				"value", ",");
	}

	public ContaContabil getContagerencialIndenizacaoByParametro() {
		ContaContabil contacontabil = null;
		try {
			String idContaContabil = parametrogeralService
					.getValorPorNome(Parametrogeral.INDENIZACAO_CONTAGERENCIAL);
			contacontabil = load(
					new ContaContabil(Integer.parseInt(idContaContabil)),
					"contagerencial.cdcontagerencial, contagerencial.nome");
		} catch (Exception e) {
		}
		return contacontabil;
	}

	public List<ContaContabil> findListaCompleta() {
		return contacontabilDAO.findListaCompleta();
	}

	public void updateUsadocalculocustooperacional(String whereInContagerencial) {
		contacontabilDAO
				.updateUsadocalculocustooperacional(whereInContagerencial);
	}

	public void updateUsadocalculocustocomercial(String whereInContagerencial) {
		contacontabilDAO
				.updateUsadocalculocustocomercial(whereInContagerencial);
	}

	public List<ContaContabil> getContagerencialUsadaCustoOperacional() {
		return contacontabilDAO.getContagerencialUsadaCustoOperacional();
	}

	public List<ContaContabil> getContagerencialUsadaCustoComercial() {
		return contacontabilDAO.getContagerencialUsadaCustoComercial();
	}

	public ContaContabil criarContaContabil(Integer id, String nome,
			String nomeParametro) {
		ContaContabil contacontabilfilha = null;
		Integer cdcontacontabilpai = null;
		try {
			cdcontacontabilpai = Integer.valueOf(parametrogeralService
					.buscaValorPorNome(nomeParametro));
		} catch (Exception e) {
		}
		if (cdcontacontabilpai != null) {
			ContaContabil contacontabilpai = this
					.loadForEntrada(new ContaContabil(cdcontacontabilpai));
			if (contacontabilpai != null) {
				contacontabilfilha = new ContaContabil();
				contacontabilfilha.setPai(contacontabilpai);
				contacontabilfilha.setItem(null);
				contacontabilfilha.setFormato(contacontabilpai.getFormato());
				contacontabilfilha.setNome(nome);
				contacontabilfilha.setTipooperacao(contacontabilpai
						.getTipooperacao());
				contacontabilfilha.setNatureza(contacontabilpai.getNatureza());
				contacontabilfilha.setAtivo(Boolean.TRUE);

				this.preparaBeanParaSalvar(contacontabilfilha);

				try {
					this.saveOrUpdate(contacontabilfilha);
				} catch (Exception e) {
					// Caso j� tenha uma CG cadastrada com este
					// PAI/NOME/OPERACAO, seto o ID ap�s o nome.
					if (e instanceof DataIntegrityViolationException
							&& DatabaseError.isKeyPresent(
									(DataIntegrityViolationException) e,
									"idx_contagerencial_nome")) {
						contacontabilfilha.setCdcontacontabil(null);
						contacontabilfilha.setNome(contacontabilfilha.getNome()
								.concat(" - " + id));
						this.saveOrUpdate(contacontabilfilha);
					} else {
						throw new SinedException(e);
					}
				}
			}
		}

		return contacontabilfilha;
	}

	public void ajaxNivel(WebRequestContext request, ContaContabil bean) {
		request.getServletResponse().setContentType("text/html");
		if (bean == null || bean.getContaContabilPai() == null) {
			View.getCurrent().eval("");
		} else {
			bean = carregaConta(bean.getContaContabilPai());
			int proximoNivel = (bean.getVcontacontabil().getNivel() + 1);
			View.getCurrent().eval(String.valueOf(proximoNivel));
		}
	}

	public void ajaxVerificaPaiRetificador(WebRequestContext request, ContaContabil bean) {
		if (bean == null || bean.getContaContabilPai() == null) {
			throw new SinedException("Contagerencialpai nao pode ser nula.");
		} else {
			request.getServletResponse().setContentType("text/html");
			bean = carregaConta(bean.getContaContabilPai());
			View.getCurrent().eval("var paiRetificador = " + Boolean.TRUE.equals(bean.getContaretificadora()) + ";");
		}
	}

	public void ajaxAnalitica(WebRequestContext request, ContaContabil bean) {
		if (bean == null || bean.getContaContabilPai() == null) {
			throw new SinedException("Contagerencialpai nao pode ser nula.");
		} else {
			if (isAnalitica(bean.getContaContabilPai())) {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().eval("true");
			} else
				View.getCurrent().eval("false");
		}
	}

	public void ajaxItem(WebRequestContext request, ContaContabil bean) {

		Integer item = null;

		if (bean.getContaContabilPai() != null
				&& bean.getContaContabilPai().getCdcontacontabil() != null) {
			List<ContaContabil> listaFilhos = findFilhos(bean
					.getContaContabilPai());
			if (listaFilhos.size() > 0) {
				item = listaFilhos.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		} else {
			List<ContaContabil> listaPais = findRaiz();
			if (listaPais.size() > 0) {
				item = listaPais.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		}
		
		View.getCurrent().println("var item = " + item + ";");
		if(bean.getContaContabilPai() != null && bean.getContaContabilPai().getCdcontacontabil() != null){
			ContaContabil contaContabil = this.carregaConta(bean.getContaContabilPai());
			if(contaContabil != null){
				if(contaContabil.getNatureza() != null){
					View.getCurrent().println("var natureza = '"+contaContabil.getNatureza().name()+"';");
				}
				if(contaContabil.getNaturezaContaCompensacao() != null){
					View.getCurrent().println("var naturezaContaCompensacao = '"+contaContabil.getNaturezaContaCompensacao().name()+"';");
				}
			}
		}
	}

	public List<NaturezaContagerencial> getListaNaturezaContabil() {
		List<NaturezaContagerencial> listaNaturezacontagerencial = new ArrayList<NaturezaContagerencial>();
		listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_ATIVO);
		listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_PASSIVO);
		listaNaturezacontagerencial.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
		listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_RESULTADO);
		listaNaturezacontagerencial.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		return listaNaturezacontagerencial;
	}

	public List<NaturezaContaCompensacaoEnum> getListaNaturezaContaCompensacao(){
		List<NaturezaContaCompensacaoEnum> listaNaturezaContaCompensacao = new ArrayList<NaturezaContaCompensacaoEnum>();
		listaNaturezaContaCompensacao.add(NaturezaContaCompensacaoEnum.CREDORA);
		listaNaturezaContaCompensacao.add(NaturezaContaCompensacaoEnum.DEVEDORA);
		return listaNaturezaContaCompensacao;
	}
	
	public String getOptionComboNaturezaContagerencial(List<NaturezaContagerencial> lista, Integer cdnaturezaselecionada) {

		StringBuilder sb = new StringBuilder();

		for (NaturezaContagerencial natureza : lista) {
			sb.append("<option ");

			if (cdnaturezaselecionada != null
					&& cdnaturezaselecionada.equals(natureza.getValue())) {
				sb.append("selected");
			}

			sb.append(" value=\"" + natureza.name()).append("\">")
					.append(natureza.getNome()).append("</option>");
		}

		return sb.toString();
	}

	public void ajaxBuscaNaturezaContaContabil(WebRequestContext request,
			ContaContabil contacontabil) {
		List<NaturezaContagerencial> listaNaturezacontagerencial = new ArrayList<NaturezaContagerencial>();
		if (Tipooperacao.TIPO_CREDITO.equals(contacontabil.getTipooperacao())
				|| Tipooperacao.TIPO_DEBITO.equals(contacontabil
						.getTipooperacao())) {
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.CONTA_GERENCIAL);
			listaNaturezacontagerencial.add(NaturezaContagerencial.OUTRAS);
		} else if (Tipooperacao.TIPO_CONTABIL.equals(contacontabil
				.getTipooperacao())) {
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.CONTAS_ATIVO);
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.CONTAS_PASSIVO);
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.PATRIMONIO_LIQUIDO);
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.CONTAS_RESULTADO);
			listaNaturezacontagerencial
					.add(NaturezaContagerencial.CONTAS_COMPENSACAO);
		}
		View.getCurrent().println(
				getOptionComboNaturezaContagerencial(
						listaNaturezacontagerencial, contacontabil
								.getNatureza() != null ? contacontabil
								.getNatureza().getValue() : null));
	}
	
	public ContaContabil loadForImportacaoSaldoInicial(Integer codigoAlternativo, Integer cdContaContabil, String identificador, Empresa empresa) {
		return contacontabilDAO.loadForImportacaoSaldoInicial(codigoAlternativo, cdContaContabil, identificador, empresa);
	}
	
	public ContaContabil loadWithEmpresa(ContaContabil contaContabil) {
		return contacontabilDAO.loadWithEmpresa(contaContabil);
	}

	public void saveSaldoInicial(ContaContabilSaldoInicial bean){
		contacontabilDAO.saveSaldoInicial(bean);
	}
	
	public Money calculaSaldoAtual(ContaContabil conta, Date date, Empresa... empresas) {
		return contacontabilDAO.calculaSaldoAtual(conta, date, empresas);
	}
	
	public List<EmitirBalancoPatrimonialBean> buscarArvoreContaGerencialBalancoPatrimonial() {
		return contacontabilDAO.buscarTudoBalancoPatrimonial();
	}

	public void alteraFilhosRetificadora(String identificador, Integer cdempresa) {
		contacontabilDAO.alteraFilhosRetificadora(identificador, cdempresa);
		
	}
	
	public ContaContabil loadForLancamentoContabil(Material material, Empresa empresa) {
		MaterialCustoEmpresa materialCustoEmpresa = materialCustoEmpresaService.loadForLancamentoContabil(material, empresa);
		if(Util.objects.isPersistent(materialCustoEmpresa)){
			return materialCustoEmpresa.getContaContabil();
		}
		return null;
	}
}