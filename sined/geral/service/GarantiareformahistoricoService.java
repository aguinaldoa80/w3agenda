package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Garantiareformahistorico;
import br.com.linkcom.sined.geral.dao.GarantiareformahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GarantiareformahistoricoService extends GenericService<Garantiareformahistorico>{

	protected GarantiareformahistoricoDAO garantiareformahistoricoDAO;
	
	public void setGarantiareformahistoricoDAO(
			GarantiareformahistoricoDAO garantiareformahistoricoDAO) {
		this.garantiareformahistoricoDAO = garantiareformahistoricoDAO;
	}
	
	public void saveHistorico(List<Garantiareformahistorico> lista){
		for(Garantiareformahistorico bean: lista){
			this.saveOrUpdate(bean);
		}
	}
}
