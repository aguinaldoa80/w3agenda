package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.dao.LeadtelefoneDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadtelefoneService extends GenericService<Leadtelefone>{
	
	protected LeadtelefoneDAO leadtelefoneDAO;
	
	public void setLeadtelefoneDAO(LeadtelefoneDAO leadtelefoneDAO) {
		this.leadtelefoneDAO = leadtelefoneDAO;
	}
	
	
	public String createListaTelefone(Collection<Leadtelefone> listaTelefones, String separador){
		StringBuilder sb = new StringBuilder();
		
		for (Leadtelefone tel : listaTelefones) {
			sb.append(tel.getTelefone());
			if(tel.getTelefonetipo() != null && tel.getTelefonetipo().getNome() != null){
				sb.append(" " + tel.getTelefonetipo().getNome());
			}
			
			if(separador != null) sb.append(separador);
		}
		
		if(!sb.toString().equals("")){
			sb.append(sb.substring(0, (sb.length()-separador.length())));
		}
		
		return sb.toString();
	}
	
	public List<Leadtelefone> findByLead(Lead lead) {
		return leadtelefoneDAO.findByLead(lead);
	}

}
