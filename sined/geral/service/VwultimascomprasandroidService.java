package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.view.Vwultimascomprasandroid;
import br.com.linkcom.sined.geral.dao.VwultimascomprasandroidDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.vwultimascompras.VwultimascomprasandroidRESTModel;

public class VwultimascomprasandroidService extends GenericService<Vwultimascomprasandroid> {

	private VwultimascomprasandroidDAO vwultimascomprasandroidDAO;
	
	public void setVwultimascomprasandroidDAO(VwultimascomprasandroidDAO vwultimascomprasandroidDAO) {
		this.vwultimascomprasandroidDAO = vwultimascomprasandroidDAO;
	}
	
	/* singleton */
	private static VwultimascomprasandroidService instance;
	
	public static VwultimascomprasandroidService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VwultimascomprasandroidService.class);
		}
		return instance;
	}
	
	/**
	* M�todo que busca as ultimas compra para o android
	*
	* @return
	* @since 28/10/2015
	* @author Luiz Fernando
	*/
	public List<VwultimascomprasandroidRESTModel> findForAndroid() {
		return findForAndroid(null);
	}

	/**
	* M�todo que busca as ultimas compra para o android de acordo com a �ltima atualiza��o
	*
	* @param dataUltimaAtualizacao
	* @return
	* @since 28/10/2015
	* @author Luiz Fernando
	*/
	public List<VwultimascomprasandroidRESTModel> findForAndroid(Timestamp dataUltimaAtualizacao) {
		List<VwultimascomprasandroidRESTModel> lista = new ArrayList<VwultimascomprasandroidRESTModel>();
		for(Vwultimascomprasandroid bean : vwultimascomprasandroidDAO.findForAndroid(dataUltimaAtualizacao)){
			lista.add(new VwultimascomprasandroidRESTModel(bean));
		}
		return lista;
	}
}