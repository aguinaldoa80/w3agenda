package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletocliente;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletoclienteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoenvioboletoclienteService extends GenericService<Documentoenvioboletocliente>{
	
	private DocumentoenvioboletoclienteDAO documentoenvioboletoclienteDAO;
	
	public void setDocumentoenvioboletoclienteDAO(
			DocumentoenvioboletoclienteDAO documentoenvioboletoclienteDAO) {
		this.documentoenvioboletoclienteDAO = documentoenvioboletoclienteDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletocliente> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		return documentoenvioboletoclienteDAO.findByDocumentoenvioboleto(documentoenvioboleto);
	}
}
