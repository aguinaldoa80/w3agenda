package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoetapanomecampo;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.bean.Producaoordemequipamento;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemitemadicional;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialcampo;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialproduzido;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.dao.ProducaochaofabricaDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.downloadproducaobean.DownloadProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.marcarproducao.MarcarProducaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.producaochaofabrica.ProducaoChaoFabricaW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoProducaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoServicoProducaoW3producaoRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.retorno.RetornoServicoProducaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoCampoadicionalW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoConstatacaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoEquipamentoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoEtapaW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoHistoricoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoItemadicionalW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoMateriaprimaW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.retorno.bean.RetornoProducaoPneuW3producaoRESWSTBean;

public class ProducaochaofabricaService extends GenericService<Producaochaofabrica>{
	
	private ProducaoagendaService producaoagendaService;
	private ProducaochaofabricaDAO producaochaofabricaDAO;
	private ParametrogeralService parametrogeralService;
	private ProducaoordemService producaoordemService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private PneuService pneuService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private PedidovendamaterialService pedidovendamaterialService;
	private ColetaMaterialService coletaMaterialService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	
	public void setColetaMaterialService(
			ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	public void setProducaoordemService(
			ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setProducaochaofabricaDAO(
			ProducaochaofabricaDAO producaochaofabricaDAO) {
		this.producaochaofabricaDAO = producaochaofabricaDAO;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}


	/* singleton */
	private static ProducaochaofabricaService instance;
	public static ProducaochaofabricaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProducaochaofabricaService.class);
		}
		return instance;
	}

	/**
	 * M�todo que cria os registro para envio para o ch�o de f�brica
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 04/03/2016
	 */
	public void createChaofabricaByProducaoagenda(final String whereIn) {
		if(StringUtils.isBlank(whereIn)){
			throw new SinedException("Nenhuma agenda de produ��o selecionada.");
		}
		
		if(producaoagendaService.haveProducaoagendaNotInSituacao(whereIn, Producaoagendasituacao.EM_ESPERA)){
			throw new SinedException("O envio de agenda de produ��o para o ch�o de f�brica somente pode ser na situa��o 'EM ESPERA'.");
		}
		
		final List<Producaoagenda> listaProducaoagenda = producaoagendaService.findForEnvioChaofabrica(whereIn);
		if(listaProducaoagenda != null && listaProducaoagenda.size() > 0){
			
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				Set<Producaoagendamaterial> listaProducaoagendamaterial = producaoagenda.getListaProducaoagendamaterial();
				if(listaProducaoagendamaterial != null && listaProducaoagendamaterial.size() > 0){
					for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
						if(producaoagendamaterial.getPedidovendamaterial() != null && producaoagendamaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							if(!coletaMaterialService.haveColetaMaterialByPedidovendamaterial(producaoagendamaterial.getPedidovendamaterial()) &&
									!coletaMaterialService.haveColetaMaterialByPedidovendamaterialOtr(producaoagendamaterial.getPedidovendamaterial())){
								throw new SinedException("Para o envio da agenda de produ��o relacionada ao pedido de venda � preciso realizar a a��o de \"Gerar coleta para produ��o\" na tela de pedido de venda.");
							}
						}
					}
				}
			}
			
			
			
			final ProducaochaofabricaService producaochaofabricaService = this;
			
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
				@Override
				public Object doInTransaction(TransactionStatus status) {
					for (Producaoagenda producaoagenda : listaProducaoagenda) {
						Set<Producaoagendamaterial> listaProducaoagendamaterial = producaoagenda.getListaProducaoagendamaterial();
						if(listaProducaoagendamaterial != null && listaProducaoagendamaterial.size() > 0){
							for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
								if(producaoagendamaterial.getPneu() != null){
									Producaochaofabrica producaochaofabrica = new Producaochaofabrica();
									producaochaofabrica.setProducaoagendamaterial(producaoagendamaterial);
									producaochaofabrica.setProducaochaofabricasituacao(Producaochaofabricasituacao.EM_ESPERA);
									producaochaofabricaService.saveOrUpdateNoUseTransaction(producaochaofabrica);
								} else {
									throw new SinedException("Pneu n�o encontrado para o item da agenda de produ��o " + producaoagenda.getCdproducaoagenda() + ".");
								}
								
								if (producaoagendamaterial.getProducaoetapa() == null){
									throw new SinedException("O ciclo de produ��o deve ser preenchido para todos os materiais.");
								}
							}
							
							try {
								Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
								producaoagendahistorico.setProducaoagenda(producaoagenda);
								producaoagendahistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
								producaoagendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
								producaoagendahistorico.setObservacao("Enviado para o ch�o de f�brica.");
								producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							throw new SinedException("Nenhum item encontrado para a agenda de produ��o " + producaoagenda.getCdproducaoagenda() + ".");
						}
					}
					
					producaoagendaService.updateSituacao(whereIn, Producaoagendasituacao.CHAO_DE_FABRICA);
					return null;
				}
			});
		} else {
			throw new SinedException("Nenhuma agenda de produ��o encontrada na base de dados.");
		}
	}

	/**
	 * Retorno o Model para uso no W3Producao
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param whereInEmpresa 
	 * @since 07/03/2016
	 */
	public DownloadProducaoW3producaoRESTModel getDownloadProducaoForW3producao(String whereInEmpresa) {
		List<ProducaoChaoFabricaW3producaoRESTModel> lista = this.findForW3producao(whereInEmpresa);
		ProducaoChaoFabricaW3producaoRESTModel[] array = lista.toArray(new ProducaoChaoFabricaW3producaoRESTModel[lista.size()]);
		
		DownloadProducaoW3producaoRESTModel model = new DownloadProducaoW3producaoRESTModel();
		model.setProducaoChaoFabricaRESTModelArray(array);
		return model;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param whereInEmpresa 
	 * @since 07/03/2016
	 */
	private List<ProducaoChaoFabricaW3producaoRESTModel> findForW3producao(String whereInEmpresa) {
		return producaochaofabricaDAO.findForW3producao(whereInEmpresa);
	}
	
	/**
	 * Atualiza a situa��o da produ��o para libera��o para o ch�o de f�brica.
	 *
	 * @param command
	 * @author Rodrigo Freitas
	 * @since 07/03/2016
	 */
	public void updateProducaoForW3producao(MarcarProducaoW3producaoRESTWSBean command) {
		this.updateSituacao(new Producaochaofabrica(command.getCdproducaochaofabrica()), Producaochaofabricasituacao.EM_ANDAMENTO);
		try {
			Producaochaofabrica producaochaofabrica = loadForRetorno(new Producaochaofabrica(command.getCdproducaochaofabrica()));
			
			Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
			producaoagendahistorico.setProducaoagenda(producaochaofabrica.getProducaoagendamaterial().getProducaoagenda());
			producaoagendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			producaoagendahistorico.setObservacao("Liberado para o ch�o de f�brica.");
			producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @param producaochaofabrica
	 * @param producaochaofabricasituacao
	 * @author Rodrigo Freitas
	 * @since 04/07/2016
	 */
	public void updateSituacao(Producaochaofabrica producaochaofabrica, Producaochaofabricasituacao producaochaofabricasituacao){
		producaochaofabricaDAO.updateSituacao(producaochaofabrica, producaochaofabricasituacao);
	}
	
	public RetornoProducaoW3producaoRESTModel retornoProducaoForW3producao(RetornoProducaoW3producaoRESTWSBean command) {
		RetornoProducaoW3producaoRESTModel model = new RetornoProducaoW3producaoRESTModel();
		Integer cdlocalarmazenagem = parametrogeralService.getIntegerNullable(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA);
		if(cdlocalarmazenagem == null){
			throw new SinedException("N�o foi encontrado o local de armazenagem.");
		}
		Localarmazenagem localarmazenagem_padrao = new Localarmazenagem(cdlocalarmazenagem);
		
		try {
			if(command.getCdproducaochaofabrica() == null){
				model.setSincronizado(Boolean.TRUE);
				return model;
			}
			
			Producaochaofabrica producaochaofabrica = this.loadForRetorno(new Producaochaofabrica(command.getCdproducaochaofabrica()));
			Producaoagendamaterial producaoagendamaterial = producaochaofabrica.getProducaoagendamaterial();
			Producaoagenda producaoagenda = producaoagendamaterial.getProducaoagenda();
			Empresa empresa = producaoagenda.getEmpresa();
			Localarmazenagem localarmazenagem = empresa != null ? empresa.getLocalarmazenagemmateriaprima() : null;
			
			RetornoProducaoHistoricoW3producaoRESTWSBean[] historicos = command.getHistoricos();
			RetornoProducaoCampoadicionalW3producaoRESTWSBean[] camposadicionais = command.getCamposadicionais();
			RetornoProducaoEquipamentoW3producaoRESTWSBean[] equipamentos = command.getEquipamentos();
			RetornoProducaoItemadicionalW3producaoRESTWSBean[] itensadicionais = command.getItensadicionais();
			RetornoProducaoMateriaprimaW3producaoRESTWSBean[] materiasprimas = command.getMateriasprimas();
			RetornoProducaoConstatacaoW3producaoRESTWSBean[] constatacoes = command.getConstatacoes();
			RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean[] motivosdevolucoes = command.getMotivosdevolucoes();
			
			RetornoProducaoPneuW3producaoRESWSTBean pneuBean = command.getPneu();
			Integer cdpneu = pneuBean.getCdpneu();
			Integer cdmaterialbanda = pneuBean.getCdmaterialbanda();
			
			producaoagendamaterial.setMaterialChaofabrica(new Material(command.getCdmaterial()));
			
			Producaoagendamaterial pam = producaoagendamaterialService.load(producaoagendamaterial, "producaoagendamaterial.cdproducaoagendamaterial, producaoagendamaterial.bandaenviada");
			if(pam != null){
				producaoagendamaterial.setBandaenviada(pam.getBandaenviada());
			}
			
			Pedidovendamaterial pedidovendamaterial = producaoagendamaterial.getPedidovendamaterial();
			Pneu pneu = new Pneu(cdpneu);
			pneu.setCdmaterialbanda(cdmaterialbanda);
			
			Date currentDate = SinedDateUtils.currentDate();
			List<Producaoordem> listaProducaoordem = new ArrayList<Producaoordem>();
			List<Motivodevolucao> listaMotivodevolucao = new ArrayList<Motivodevolucao>();
			
			RetornoProducaoEtapaW3producaoRESTWSBean[] etapas = command.getEtapas();
			for (RetornoProducaoEtapaW3producaoRESTWSBean etapaBean : etapas) {
				Integer etapa_id = etapaBean.getEtapa_id();
				Integer etapaanterior_id = etapaBean.getEtapaanterior_id();
				Integer cdproducaoetapa = etapaBean.getCdproducaoetapa();
				Integer cdproducaoetapaitem = etapaBean.getCdproducaoetapaitem();
				String observacao = etapaBean.getObservacao();
				Boolean concluida = etapaBean.getConcluida() != null && etapaBean.getConcluida();
				
				List<RetornoProducaoMateriaprimaW3producaoRESTWSBean> listaMateriaprimaEtapa = new ArrayList<RetornoProducaoMateriaprimaW3producaoRESTWSBean>();
				for (RetornoProducaoMateriaprimaW3producaoRESTWSBean materiaprimaBean : materiasprimas) {
					if(materiaprimaBean != null && 
							materiaprimaBean.getEtapa_id() != null &&
							materiaprimaBean.getEtapa_id().equals(etapa_id)){
						listaMateriaprimaEtapa.add(materiaprimaBean);
					}
				}
				
				Set<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima = new ListSet<Producaoordemmaterialmateriaprima>(Producaoordemmaterialmateriaprima.class);
				for (RetornoProducaoMateriaprimaW3producaoRESTWSBean materiaprimaBean : listaMateriaprimaEtapa) {
					Producaoordemmaterialmateriaprima producaoordemmaterialmateriaprima = new Producaoordemmaterialmateriaprima();
					producaoordemmaterialmateriaprima.setMaterial(new Material(materiaprimaBean.getCdmaterial()));
					producaoordemmaterialmateriaprima.setQtdeprevista(materiaprimaBean.getQuantidade());
					producaoordemmaterialmateriaprima.setUnidademedida(new Unidademedida(materiaprimaBean.getCdunidademedida()));
					producaoordemmaterialmateriaprima.setBanda(materiaprimaBean.getBanda());
					producaoordemmaterialmateriaprima.setAcompanhabanda(materiaprimaBean.getAcompanhabanda());
					listaProducaoordemmaterialmateriaprima.add(producaoordemmaterialmateriaprima);
				}
				
				List<RetornoProducaoItemadicionalW3producaoRESTWSBean> listaItemadicionalEtapa = new ArrayList<RetornoProducaoItemadicionalW3producaoRESTWSBean>();
				for (RetornoProducaoItemadicionalW3producaoRESTWSBean itemadicionalBean : itensadicionais) {
					if(itemadicionalBean != null && 
							itemadicionalBean.getEtapa_id() != null &&
							itemadicionalBean.getEtapa_id().equals(etapa_id)){
						listaItemadicionalEtapa.add(itemadicionalBean);
					}
				}
				
				Set<Producaoordemitemadicional> listaProducaoordemitemadicional = new ListSet<Producaoordemitemadicional>(Producaoordemitemadicional.class);
				for (RetornoProducaoItemadicionalW3producaoRESTWSBean itemadicionalBean : listaItemadicionalEtapa) {
					Producaoordemitemadicional producaoordemitemadicional = new Producaoordemitemadicional();
					producaoordemitemadicional.setMaterial(new Material(itemadicionalBean.getCdmaterial()));
					producaoordemitemadicional.setQuantidade(itemadicionalBean.getQuantidade());
					producaoordemitemadicional.setLocalarmazenagem(localarmazenagem != null ? localarmazenagem : localarmazenagem_padrao);
					producaoordemitemadicional.setPneu(new Pneu(itemadicionalBean.getCdpneu()));
					listaProducaoordemitemadicional.add(producaoordemitemadicional);
				}
				
				List<RetornoProducaoEquipamentoW3producaoRESTWSBean> listaEquipamentoEtapa = new ArrayList<RetornoProducaoEquipamentoW3producaoRESTWSBean>();
				for (RetornoProducaoEquipamentoW3producaoRESTWSBean equipamentoBean : equipamentos) {
					if(equipamentoBean != null && 
							equipamentoBean.getEtapa_id() != null &&
							equipamentoBean.getEtapa_id().equals(etapa_id)){
						listaEquipamentoEtapa.add(equipamentoBean);
					}
				}
				
				Set<Producaoordemequipamento> listaProducaoordemequipamento = new ListSet<Producaoordemequipamento>(Producaoordemequipamento.class);
				for (RetornoProducaoEquipamentoW3producaoRESTWSBean equipamentoBean : listaEquipamentoEtapa) {
					if(equipamentoBean.getCdmaterial() != null && equipamentoBean.getCdpatrimonioitem() != null){
						Producaoordemequipamento producaoordemequipamento = new Producaoordemequipamento();
						producaoordemequipamento.setMaterial(new Material(equipamentoBean.getCdmaterial()));
						producaoordemequipamento.setPatrimonioitem(new Patrimonioitem(equipamentoBean.getCdpatrimonioitem()));
						listaProducaoordemequipamento.add(producaoordemequipamento);
					}
				}
				
				List<RetornoProducaoHistoricoW3producaoRESTWSBean> listaHistoricoEtapa = new ArrayList<RetornoProducaoHistoricoW3producaoRESTWSBean>();
				for (RetornoProducaoHistoricoW3producaoRESTWSBean historicoBean : historicos) {
					if(historicoBean != null && 
							historicoBean.getEtapa_id() != null &&
							historicoBean.getEtapa_id().equals(etapa_id)){
						listaHistoricoEtapa.add(historicoBean);
					}
				}
				
				List<Producaoordemhistorico> listaProducaoordemhistorico = new ListSet<Producaoordemhistorico>(Producaoordemhistorico.class);
				List<Producaoagendahistorico> listaProducaoagendahistorico = new ListSet<Producaoagendahistorico>(Producaoagendahistorico.class);
				Long maxDataHistorico = null;
				for (RetornoProducaoHistoricoW3producaoRESTWSBean historicoBean : listaHistoricoEtapa) {
					String observacaoHistorico = "Ch�o de F�brica. ";
					switch (historicoBean.getAcao_id()) {
						case 0:
							observacaoHistorico += "Produ��o confirmada.";
							break;
						case 1:
							observacaoHistorico += "Produ��o recusada.";
							break;
						case 2:
							observacaoHistorico += "Produ��o reprocessada.";
							break;
						case 3:
							observacaoHistorico += "Produ��o n�o executada, usu�rio pulou a etapa.";
							break;
						case 4:
							observacaoHistorico += "Produ��o retrabalhada.";
							break;
					}
					
					if(maxDataHistorico == null || maxDataHistorico < historicoBean.getDatahora()){
						maxDataHistorico = historicoBean.getDatahora();
					}
					
					Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
					producaoordemhistorico.setCdusuarioaltera(historicoBean.getCdusuario());
					producaoordemhistorico.setDtaltera(new Timestamp(historicoBean.getDatahora()));
					producaoordemhistorico.setObservacao(observacaoHistorico);
					listaProducaoordemhistorico.add(producaoordemhistorico);
					
					Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
					producaoagendahistorico.setCdusuarioaltera(historicoBean.getCdusuario());
					producaoagendahistorico.setDtaltera(new Timestamp(historicoBean.getDatahora()));
					producaoagendahistorico.setObservacao(observacaoHistorico);
					listaProducaoagendahistorico.add(producaoagendahistorico);
				}
				
				List<RetornoProducaoCampoadicionalW3producaoRESTWSBean> listaCampoadicionalEtapa = new ArrayList<RetornoProducaoCampoadicionalW3producaoRESTWSBean>();
				for (RetornoProducaoCampoadicionalW3producaoRESTWSBean campoadicionalBean : camposadicionais) {
					if(campoadicionalBean != null && 
							campoadicionalBean.getEtapa_id() != null &&
									campoadicionalBean.getEtapa_id().equals(etapa_id)){
						listaCampoadicionalEtapa.add(campoadicionalBean);
					}
				}
				
				Set<Producaoordemmaterialcampo> listaProducaoordemmaterialcampo = new ListSet<Producaoordemmaterialcampo>(Producaoordemmaterialcampo.class);
				for (RetornoProducaoCampoadicionalW3producaoRESTWSBean campoadicionalBean : listaCampoadicionalEtapa) {
					Producaoordemmaterialcampo producaoordemmaterialcampo = new Producaoordemmaterialcampo();
					producaoordemmaterialcampo.setProducaoetapanomecampo(new Producaoetapanomecampo(campoadicionalBean.getCdproducaoetapanomecampo()));
					if(campoadicionalBean.getValorcaracter() != null){
						if(campoadicionalBean.getValorcaracter().length() > 50){
							producaoordemmaterialcampo.setValor(campoadicionalBean.getValorcaracter().substring(0, 49));
						}else {
							producaoordemmaterialcampo.setValor(campoadicionalBean.getValorcaracter());
						}
						
					}
					producaoordemmaterialcampo.setValorboleano(campoadicionalBean.getValorboleano());
					listaProducaoordemmaterialcampo.add(producaoordemmaterialcampo);
				}
				
				List<RetornoProducaoConstatacaoW3producaoRESTWSBean> listaConstatacaoEtapa = new ArrayList<RetornoProducaoConstatacaoW3producaoRESTWSBean>();
				if (constatacoes!=null){
					for (RetornoProducaoConstatacaoW3producaoRESTWSBean constatacaoBean : constatacoes) {
						if(constatacaoBean != null && 
								constatacaoBean.getEtapa_id() != null &&
								constatacaoBean.getEtapa_id().equals(etapa_id)){
							listaConstatacaoEtapa.add(constatacaoBean);
						}
					}
				}
				
				Set<Producaoordemconstatacao> listaProducaoordemconstatacao = new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class);
				for (RetornoProducaoConstatacaoW3producaoRESTWSBean constatacaoBean: listaConstatacaoEtapa){
					Producaoordemconstatacao producaoordemconstatacao = new Producaoordemconstatacao();
					producaoordemconstatacao.setMotivodevolucao(new Motivodevolucao(constatacaoBean.getCdmotivodevolucao())); 
					producaoordemconstatacao.setObservacao(constatacaoBean.getObservacao());
					listaProducaoordemconstatacao.add(producaoordemconstatacao);
				}
				
				List<RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean> listaMotivoDevolucaoEtapa = new ArrayList<RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean>();
				if (motivosdevolucoes!=null){
					for (RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean motivoDevolucaoBean : motivosdevolucoes) {
						if(motivoDevolucaoBean != null &&
								motivoDevolucaoBean.getEtapa_id() != null &&
								motivoDevolucaoBean.getEtapa_id().equals(etapa_id)){
							listaMotivoDevolucaoEtapa.add(motivoDevolucaoBean);
						}
					}
				}
				
				for (RetornoProducaoMotivoDevolucaoW3producaoRESTWSBean motivoDevolucaoBean: listaMotivoDevolucaoEtapa){
					listaMotivodevolucao.add(new Motivodevolucao(motivoDevolucaoBean.getCdmotivodevolucao()));
				}
				
				Producaoordemmaterialorigem producaoordemmaterialorigem = new Producaoordemmaterialorigem();
				producaoordemmaterialorigem.setProducaoagenda(producaoagenda);
				producaoordemmaterialorigem.setProducaoagendamaterial(producaoagendamaterial);
				
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = new ListSet<Producaoordemmaterialorigem>(Producaoordemmaterialorigem.class);
				listaProducaoordemmaterialorigem.add(producaoordemmaterialorigem);
				
				Producaoordemmaterial producaoordemmaterial = new Producaoordemmaterial();
				producaoordemmaterial.setMaterial(producaoagendamaterial.getMaterialChaofabrica());
				producaoordemmaterial.setQtde(producaoagendamaterial.getQtde());
				producaoordemmaterial.setQtdeperdadescarte(concluida ? 0d : producaoagendamaterial.getQtde());
				producaoordemmaterial.setQtdeproduzido(concluida ? producaoagendamaterial.getQtde() : 0d);
				producaoordemmaterial.setProducaoetapa(new Producaoetapa(cdproducaoetapa));
				producaoordemmaterial.setProducaoetapaitem(new Producaoetapaitem(cdproducaoetapaitem));
				producaoordemmaterial.setPedidovendamaterial(pedidovendamaterial);
				producaoordemmaterial.setObservacao(observacao);
				producaoordemmaterial.setUnidademedida(producaoagendamaterial.getUnidademedida());
				producaoordemmaterial.setPneu(pneu);
				producaoordemmaterial.setListaProducaoordemmaterialmateriaprima(listaProducaoordemmaterialmateriaprima);
				producaoordemmaterial.setListaProducaoordemmaterialorigem(listaProducaoordemmaterialorigem);
				producaoordemmaterial.setListaCampo(listaProducaoordemmaterialcampo);
				producaoordemmaterial.setResiduobanda(command.getResiduobanda());
				producaoordemmaterial.setGarantiatipo(command.getCdgarantiatipo()!=null ? new Garantiatipo(command.getCdgarantiatipo()) : null);				
				producaoordemmaterial.setGarantiatipopercentual(command.getCdgarantiatipopercentual()!=null ? new Garantiatipopercentual(command.getCdgarantiatipopercentual()) : null);
				
				Date dataProducao = null;
				if(maxDataHistorico != null){
					dataProducao = new Date(maxDataHistorico);
				}
				if(concluida){
					Producaoordemmaterialproduzido producaoordemmaterialproduzido = new Producaoordemmaterialproduzido();
					producaoordemmaterialproduzido.setData(dataProducao);
					producaoordemmaterialproduzido.setQuantidade(producaoagendamaterial.getQtde());
					
					Set<Producaoordemmaterialproduzido> listaProducaoordemmaterialproduzido = new ListSet<Producaoordemmaterialproduzido>(Producaoordemmaterialproduzido.class);
					listaProducaoordemmaterialproduzido.add(producaoordemmaterialproduzido);
					producaoordemmaterial.setListaProducaoordemmaterialproduzido(listaProducaoordemmaterialproduzido);
				}
				
				Set<Producaoordemmaterial> listaProducaoordemmaterial = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
				listaProducaoordemmaterial.add(producaoordemmaterial);
				
				Producaoordem producaoordem = new Producaoordem();
				producaoordem.setEtapa_id(etapa_id);
				producaoordem.setEtapaanterior_id(etapaanterior_id);
				producaoordem.setDtprevisaoentrega(pedidovendamaterial != null && pedidovendamaterial.getDtprazoentrega() != null ? pedidovendamaterial.getDtprazoentrega() : currentDate);
				producaoordem.setDtproducaoordem(dataProducao != null ? dataProducao : currentDate);
				producaoordem.setEmpresa(empresa);
				producaoordem.setListaProducaoordemmaterial(listaProducaoordemmaterial);
				producaoordem.setListaProducaoordemhistorico(listaProducaoordemhistorico);
				producaoordem.setListaProducaoagendahistorico(listaProducaoagendahistorico);
				producaoordem.setListaProducaoordemequipamento(listaProducaoordemequipamento);
				producaoordem.setListaProducaoordemitemadicional(listaProducaoordemitemadicional);
				producaoordem.setListaProducaoordemconstatacao(listaProducaoordemconstatacao);
				producaoordem.setProducaoordemsituacao(concluida ? Producaoordemsituacao.CONCLUIDA : Producaoordemsituacao.CANCELADA);
				
				listaProducaoordem.add(producaoordem);
			}
			
			Integer cdmotivodevolucao = command.getCdmotivodevolucao();
			String observacaorecusa = command.getObservacaorecusa();
			producaoagenda.setCdmotivodevolucao(cdmotivodevolucao);
			producaoagenda.setObservacaorecusa(observacaorecusa);
			producaoagenda.setListaMotivodevolucao(listaMotivodevolucao);
			
			producaoordemService.saveListaProducaoordemForChaofabrica(producaochaofabrica, producaoagenda, producaoagendamaterial, pedidovendamaterial, listaProducaoordem);
			
			pneuService.updatePneuqualificacao(pneu, pneuBean.getCdpneuqualificacao());
			
			model.setSincronizado(Boolean.TRUE);
		} catch (Exception e) {
			e.printStackTrace();
			model.setSincronizado(Boolean.FALSE);
		}
		
		return model;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaochaofabrica
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/07/2016
	 */
	public Producaochaofabrica loadForRetorno(Producaochaofabrica producaochaofabrica) {
		return producaochaofabricaDAO.loadForRetorno(producaochaofabrica);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public List<Producaochaofabrica> findByProducaoagenda(Producaoagenda producaoagenda, boolean desmarkAsReader) {
		return producaochaofabricaDAO.findByProducaoagenda(producaoagenda, desmarkAsReader);
	}
	
	public RetornoServicoProducaoW3producaoRESTModel retornoServicoProducaoForW3producao(RetornoServicoProducaoW3producaoRESTWSBean command) {
		RetornoServicoProducaoW3producaoRESTModel model = new RetornoServicoProducaoW3producaoRESTModel();
		
		try {
			if (command.getCdproducaochaofabrica()==null){
				model.setSincronizado(Boolean.TRUE);
				return model;
			}
			
			Producaochaofabrica producaochaofabrica = this.loadForRetorno(new Producaochaofabrica(command.getCdproducaochaofabrica()));
			Producaoagendamaterial producaoagendamaterial = producaochaofabrica.getProducaoagendamaterial();
			Pedidovendamaterial pedidovendamaterial = producaoagendamaterial.getPedidovendamaterial();
			Material servicoNovo = new Material(command.getCdmaterial());
			
			if (producaoagendamaterial.getMaterial()!=null && !servicoNovo.equals(producaoagendamaterial.getMaterial())){
				producaoagendamaterialService.atualizaMaterial(producaoagendamaterial, servicoNovo);
				if (pedidovendamaterial != null) {
					pedidovendamaterialService.atualizaMaterial(pedidovendamaterial, servicoNovo);
					pedidovendamaterialService.updateServicoAlterado(pedidovendamaterial.getCdpedidovendamaterial() + "");
					pedidovendahistoricoService.registrarAlteracaoServico(pedidovendamaterial, producaoagendamaterial.getMaterial(), servicoNovo);
				}
			}
			
			model.setSincronizado(Boolean.TRUE);
		} catch(Exception e){
			model.setSincronizado(Boolean.FALSE);
		}
		
		return model;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaochaofabrica
	 * @param flag
	 * @since 01/10/2019
	 * @author Rodrigo Freitas
	 */
	public void updateEmitirEtiquetaAutomatico(Producaochaofabrica producaochaofabrica, Boolean flag) {
		producaochaofabricaDAO.updateEmitirEtiquetaAutomatico(producaochaofabrica, flag);
	}
	
	public void updateEmitirEtiquetaAutomatico(String whereIn, Boolean flag) {
		producaochaofabricaDAO.updateEmitirEtiquetaAutomatico(whereIn, flag);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 01/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Producaochaofabrica> findForEtiquetaAutomatica() {
		return producaochaofabricaDAO.findForEtiquetaAutomatica();
	}
}
