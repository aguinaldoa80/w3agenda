package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.PedidoVendaTipoHistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.enumeration.PedidoVendaTipoEnum;
import br.com.linkcom.sined.geral.dao.PedidoVendaTipoHistoricoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidoVendaTipoHistoricoService extends GenericService<PedidoVendaTipoHistorico>{
	
	PedidoVendaTipoHistoricoDAO pedidoVendaTipoHistoricoDAO;
	
	public void setPedidoVendaTipoHistoricoDAO(PedidoVendaTipoHistoricoDAO pedidoVendaTipoHistoricoDAO) {
		this.pedidoVendaTipoHistoricoDAO = pedidoVendaTipoHistoricoDAO;
	}
	
	private static PedidoVendaTipoHistoricoService instance;
	public static PedidoVendaTipoHistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PedidoVendaTipoHistoricoService.class);
		}
		return instance;
	}
	
	public void preencherHistorico(Pedidovendatipo pedidovendatipo, PedidoVendaTipoEnum acao, String observacao, Boolean useTransaction) {
		PedidoVendaTipoHistorico pedidoVendaTipoHistorico = new PedidoVendaTipoHistorico();
		try {
			pedidoVendaTipoHistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		} catch (Exception e) {}
		pedidoVendaTipoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		pedidoVendaTipoHistorico.setPedidovendatipo(pedidovendatipo);
		pedidoVendaTipoHistorico.setPedidoVendaTipoAcao(acao);
		pedidoVendaTipoHistorico.setObservacao(observacao);
		if(Boolean.TRUE.equals(useTransaction)){
			saveOrUpdate(pedidoVendaTipoHistorico);			
		} else {
			saveOrUpdateNoUseTransaction(pedidoVendaTipoHistorico);
		}
	}
	
	public List<PedidoVendaTipoHistorico> findByPedidoVendaTipo(Pedidovendatipo pedidovendatipo) {
		return pedidoVendaTipoHistoricoDAO.findByPedidoVendaTipo(pedidovendatipo);
	}

}
