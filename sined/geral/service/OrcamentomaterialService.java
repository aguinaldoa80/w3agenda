package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Orcamentomaterialitem;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentomaterialFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportarListaMaterialBean;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrcamentomaterialService extends GenericService<Orcamentomaterial> {

	private OrcamentomaterialDAO orcamentomaterialDAO;
	private SolicitacaocompraService solicitacaocompraService;
	private EmpresaService empresaService;
	private UnidademedidaService unidademedidaService;
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setOrcamentomaterialDAO(
			OrcamentomaterialDAO orcamentomaterialDAO) {
		this.orcamentomaterialDAO = orcamentomaterialDAO;
	}
	
	/* 
	 * Deixar sobrescrito pois � usado na parte em flex da aplica��o.
	 */
	@Override
	public void saveOrUpdate(Orcamentomaterial bean) {
		
		if(bean.getListaOrcamentomaterialitem() != null){
			for (Orcamentomaterialitem om : bean.getListaOrcamentomaterialitem()) {
				if (om.getCdorcamentomaterialitem() != null && om.getCdorcamentomaterialitem().equals(0)) {
					om.setCdorcamentomaterialitem(null);
				}
				if (om.getCdmaterial() != null && om.getCdmaterial().equals(0)){
					om.setCdmaterial(null);
				}
			}
		}
		
		if(bean.getCdorcamentomaterial() != null && bean.getCdorcamentomaterial().equals(0)){
			bean.setCdorcamentomaterial(null);
		}
		
		super.saveOrUpdate(bean);
		
		this.callProcedureAtualizaOrcamentomaterial(bean);
	}
	
	/* 
	 * Deixar sobrescrito pois � usado na parte em flex da aplica��o.
	 */
	@Override
	public Orcamentomaterial loadForEntrada(Orcamentomaterial bean) {
		return super.loadForEntrada(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findByOrcamentoFlex(Orcamento orcamento){
		return this.findByOrcamentoFlex(new OrcamentomaterialFiltro(), orcamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#findByOrcamentoFlex
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findByOrcamentoFlex(OrcamentomaterialFiltro filtro, Orcamento orcamento){
		return orcamentomaterialDAO.findByOrcamentoFlex(filtro, orcamento);
	}
	
	/**
	 * Deleta a lista de material.
	 *
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteFlex(Orcamentomaterial orcamentomaterial){
		if (orcamentomaterial == null || orcamentomaterial.getCdorcamentomaterial() == null) {
			throw new SinedException("Erro na passagem de par�metro.");
		}
		delete(orcamentomaterial);
	}
	
	/**
	 * Salvam as solicita��es de compra que vem da lista de material do or�amento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#criaSolicitacao
	 * @param solicitacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean salvaSolicitacoesFlex(Solicitacaocompra solicitacao){
		try{
			Orcamentomaterial om = this.loadForEntrada(solicitacao.getOrcamentomaterial());
			List<Orcamentomaterialitem> listaOrcamentomaterialitem = om.getListaOrcamentomaterialitem();
			
			if(listaOrcamentomaterialitem != null && listaOrcamentomaterialitem.size() > 0){
				
				List<Solicitacaocompra> lista = new ArrayList<Solicitacaocompra>();
				
				for (Orcamentomaterialitem item : listaOrcamentomaterialitem) {
					lista.add(this.criaSolicitacao(item,solicitacao));
				}
				
				for (Solicitacaocompra bean : lista) {
					solicitacaocompraService.saveOrUpdate(bean);
				}
				
			} else 
				throw new SinedException("Nenhum material encontrado na lista de material.");
			
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Cria a solicita��o de compra que veio da tela de lista de material no or�amento.
	 *
	 * @param item
	 * @param solicitacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Solicitacaocompra criaSolicitacao(Orcamentomaterialitem item, Solicitacaocompra solicitacao) {
		Solicitacaocompra sol = new Solicitacaocompra();
		
////		Composi��o da descri��o: '[LM] <Descri��o da lista> + idenficador / <2 �ltimos n�meros do ano>'
//		sol.setDescricao("[LM] " + 
//				solicitacao.getOrcamentomaterial().getDescricao() + " " + 
//				solicitacao.getIdentificador() + "/" + 
//				new SimpleDateFormat("yy").format(new Date(System.currentTimeMillis())));
		
		sol.setDescricao(solicitacao.getDescricao());
		sol.setIdentificador(solicitacao.getIdentificador());
		sol.setCentrocusto(solicitacao.getCentrocusto());
		sol.setColaborador(solicitacao.getColaborador());
		sol.setDepartamento(solicitacao.getDepartamento());
		sol.setDtlimite(solicitacao.getDtlimite());
		sol.setEmpresa(solicitacao.getEmpresa());
		sol.setLocalarmazenagem(solicitacao.getLocalarmazenagem());
		sol.setProjeto(solicitacao.getProjeto());
		
		sol.setMaterial(new Material(item.getCdmaterial()));
		sol.setQtde(new Double(item.getQtde()));
		sol.setMaterialclasse(item.getMaterialclasse());
		
		sol.setSolicitacaocompraorigem(new Solicitacaocompraorigem(solicitacao.getOrcamentomaterial()));
		
		//Soliita��o j� autorizada
		sol.setDtautorizacao(new Date(System.currentTimeMillis()));
		sol.setFaturamentocliente(false);
		
		sol.setFrequencia(new Frequencia(Frequencia.UNICA));
		sol.setQtdefrequencia(new Double(1));
		
		return sol;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#callProcedureAtualizaOrcamentomaterial
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaOrcamentomaterial(Orcamentomaterial orcamentomaterial){
		orcamentomaterialDAO.callProcedureAtualizaOrcamentomaterial(orcamentomaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#doCancelarFlex
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#callProcedureAtualizaOrcamentomaterial
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doCancelarFlex(Orcamentomaterial orcamentomaterial){
		orcamentomaterialDAO.doCancelarFlex(orcamentomaterial);
		
//		ATUALIZA A TABELA AUXILIAR DO ORCAMENTOMATERIAL
		this.callProcedureAtualizaOrcamentomaterial(orcamentomaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#doBaixarFlex
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#callProcedureAtualizaOrcamentomaterial
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doBaixarFlex(Orcamentomaterial orcamentomaterial){
		orcamentomaterialDAO.doBaixarFlex(orcamentomaterial);
		
//		ATUALIZA A TABELA AUXILIAR DO ORCAMENTOMATERIAL
		this.callProcedureAtualizaOrcamentomaterial(orcamentomaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#doBaixarFlex
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#callProcedureAtualizaOrcamentomaterial
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doEstornarFlex(Orcamentomaterial orcamentomaterial){
		orcamentomaterialDAO.doEstornarFlex(orcamentomaterial);
		
//		ATUALIZA A TABELA AUXILIAR DO ORCAMENTOMATERIAL
		this.callProcedureAtualizaOrcamentomaterial(orcamentomaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#doEstornarProcessoCompraFlex
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#callProcedureAtualizaOrcamentomaterial
	 * @param orcamentomaterial
	 * @author Rodrigo Freitas
	 */
	public void doEstornarProcessoCompraFlex(Orcamentomaterial orcamentomaterial){
		
		if(solicitacaocompraService.haveSolicitacao(orcamentomaterial)){
			throw new SinedException("N�o � poss�vel estornar uma lista de material que tenha alguma solicita��o de compra que n�o esteja em abert ou autorizada.");
		}
		
		orcamentomaterialDAO.doEstornarProcessoCompraFlex(orcamentomaterial);
		
//		ATUALIZA A TABELA AUXILIAR DO ORCAMENTOMATERIAL
		this.callProcedureAtualizaOrcamentomaterial(orcamentomaterial);
	}
	
	/**
	 * Cria o arquivo em formato excel do relat�rio de lista de material de aplica��o
	 * 
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#findForExcel
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#createRowTotal
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#createHeaderDataGrid
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#createHeaderDataGrid2
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#createDetalheDataGrid
	 * @see br.com.linkcom.sined.geral.service.OrcamentomaterialService#createFooterDataGrid
	 * @param request
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoExcelFormatado(WebRequestContext request) throws IOException {
		
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Orcamentomaterial> listaMaterial = this.findForExcel(whereIn);
		
		SinedExcel e = new SinedExcel();
		
		Integer linhasAbaixo = 6;
		
		HSSFSheet p = e.createSheet("Lista de Material");

		HSSFRow row = null;

		p.addMergedRegion(new Region(0, (short) 0, 0,(short)  11));
		p.addMergedRegion(new Region(1, (short) 0, 1,(short)  11));
		
		p.setDisplayGridlines(false);
		
		row = p.createRow(0);
		row.setHeightInPoints(45);
		
		this.createRowTotal(row, SinedExcel.STYLE_HEADER_RELATORIO, empresaService.loadPrincipal().getRazaosocialOuNome());
		
		row = p.createRow(1);
		row.setHeightInPoints(45);
		
		this.createRowTotal(row, SinedExcel.STYLE_HEADER_RELATORIO, "LISTA DE MATERIAL DE APLICA��O");
		
		p.addMergedRegion(new Region(3, (short) 0, 3,(short)  11));
		p.addMergedRegion(new Region(4, (short) 0, 4,(short)  11));
		
		row = p.createRow(3);
		row.setHeightInPoints(30);
		
		this.createRowTotal(row, SinedExcel.STYLE_FILTRO, "Or�amento: " + listaMaterial.get(0).getOrcamento().getNome());
		
		row = p.createRow(4);
		row.setHeightInPoints(30);
		
		this.createRowTotal(row, SinedExcel.STYLE_FILTRO, "Lista(s) de Material: " + CollectionsUtil.listAndConcatenate(listaMaterial, "descricao", ", "));
		
//		SETANDO OS TAMANHOS DAS COLUNAS
		p.setColumnWidth((short) 0, (short) (51*35));
		p.setColumnWidth((short) 1, (short) (253*35));
		p.setColumnWidth((short) 2, (short) (100*35));
		p.setColumnWidth((short) 3, (short) (82*35));
		p.setColumnWidth((short) 4, (short) (82*35));
		p.setColumnWidth((short) 5, (short) (107*35));
		p.setColumnWidth((short) 6, (short) (82*35));
		p.setColumnWidth((short) 7, (short) (93*35));
		p.setColumnWidth((short) 8, (short) (93*35));
		p.setColumnWidth((short) 9, (short) (107*35));
		p.setColumnWidth((short) 10, (short) (93*35));
		p.setColumnWidth((short) 11, (short) (93*35));
		
//		MERGE DAS COLUNAS DO CABE�ALHO
		p.addMergedRegion(new Region(linhasAbaixo + 0, (short) 0, linhasAbaixo + 0,(short)  3));
		p.addMergedRegion(new Region(linhasAbaixo + 0, (short) 4, linhasAbaixo + 0,(short)  8));
		p.addMergedRegion(new Region(linhasAbaixo + 0, (short) 9, linhasAbaixo + 0,(short)  11));
		
//		CRIA��O DA PRIMEIRA LINHA
		row = p.createRow(linhasAbaixo + 0);
		row.setHeightInPoints(30);

		this.createHeaderDataGrid(row);
		
//		CRIA��O DA SEGUNDA LINHA
		row = p.createRow(linhasAbaixo + 1);
		row.setHeightInPoints(30);
		
		this.createHeaderDataGrid2(row);
				
		List<Orcamentomaterialitem> listaItens = new ArrayList<Orcamentomaterialitem>();
		for (Orcamentomaterial o : listaMaterial) {
			listaItens.addAll(o.getListaOrcamentomaterialitem());
		}
		
		Collections.sort(listaItens,new Comparator<Orcamentomaterialitem>(){
			public int compare(Orcamentomaterialitem o1, Orcamentomaterialitem o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		
		Orcamentomaterialitem item;
		int i;
		for (i = 0; i < listaItens.size(); i++) {
			item = listaItens.get(i);
			
			row = p.createRow(linhasAbaixo + i+2);
			row.setHeightInPoints(21);
			
			this.createDetalheDataGrid(row, item, i);
		}
		
		p.addMergedRegion(new Region(linhasAbaixo + i+2, (short) 4, linhasAbaixo + i+2,(short)  6));
		p.addMergedRegion(new Region(linhasAbaixo + i+2, (short) 7, linhasAbaixo + i+2,(short)  8));
		p.addMergedRegion(new Region(linhasAbaixo + i+2, (short) 10, linhasAbaixo + i+2,(short)  11));
		
		row = p.createRow(linhasAbaixo + i+2);
		row.setHeightInPoints(21);
		
		this.createFooterDataGrid(linhasAbaixo, row, i);
		
		return e.getWorkBookResource("listamaterialaplicacao_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * Cria o footer do datagrid.
	 *
	 * @param linhasAbaixo
	 * @param row
	 * @param i
	 * @author Rodrigo Freitas
	 */
	private void createFooterDataGrid(Integer linhasAbaixo, HSSFRow row, int i) {
		HSSFCell cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("Total (Compra)");
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		
		cell = row.createCell((short)6);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		
		cell = row.createCell((short)7);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM(I3:I"+(linhasAbaixo + i+2)+")");
		
		cell = row.createCell((short)8);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		
		cell = row.createCell((short)9);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
		cell.setCellValue("Total (Venda)");
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
		cell.setCellFormula("SUM(L3:L"+(linhasAbaixo + i+2)+")");
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_TOTAL_RIGHT_WITHOUT_BORDERLEFT);
	}
	
	/**
	 * Cria uma linha do detalhe do datagrid.
	 *
	 * @param row
	 * @param item
	 * @param i
	 * @author Rodrigo Freitas
	 */
	private void createDetalheDataGrid(HSSFRow row, Orcamentomaterialitem item, int i) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(i+1);
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
		cell.setCellValue(item.getDescricao());
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(item.getUnidademedida().getSimbolo());
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
		cell.setCellValue(item.getQtde());
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		cell.setCellValue(item.getIpi());
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(item.getIpiincluso() ? "Sim" : "N�o");
		
		cell = row.createCell((short)6);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_PERCENT);
		cell.setCellValue(item.getIcms());
		
		cell = row.createCell((short)7);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
		cell.setCellValue(item.getValorunitariocompra().getValue().doubleValue());
		
		cell = row.createCell((short)8);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
		cell.setCellValue(item.getTotalcompra().getValue().doubleValue());
		
		cell = row.createCell((short)9);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
		cell.setCellValue(item.getFaturamentodireto() ? "Sim" : "N�o");
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
		cell.setCellValue(item.getValorunitariovenda().getValue().doubleValue());
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
		cell.setCellValue(item.getTotalvenda().getValue().doubleValue());
	}
	
	/**
	 * Cria a segunda parte do header do datagrid.
	 *
	 * @param row
	 * @author Rodrigo Freitas
	 */
	private void createHeaderDataGrid2(HSSFRow row) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Item");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Descri��o");
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Unidade");
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Qtde.");
		
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("IPI (%)");
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("IPI Incluso?");
		
		cell = row.createCell((short)6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("ICMS (%)");
		
		cell = row.createCell((short)7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor Unit. (R$)");
		
		cell = row.createCell((short)8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Total (R$)");
		
		cell = row.createCell((short)9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Fat. Dir.?");
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor Unit. (R$)");
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Total (R$)");
	}
	/**
	 * Cria o primeiro header do datagrid do relat�rio.
	 *
	 * @param row
	 * @author Rodrigo Freitas
	 */
	private void createHeaderDataGrid(HSSFRow row) {
//		CRIA��O DA PRIMEIRA C�LULA DO CABE�ALHO
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Material");
		
		cell = row.createCell((short)1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
//		CRIA��O DA SEGUNDA C�LULA DO CABE�ALHO
		cell = row.createCell((short)4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Compra");
		
		cell = row.createCell((short)5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
//		CRIA��O DA SEGUNDA C�LULA DO CABE�ALHO
		cell = row.createCell((short)9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Venda");
		
		cell = row.createCell((short)10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		cell = row.createCell((short)11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
	}
	
	/**
	 * Cria uma linha setando o style em todas as colunas da linha.
	 *
	 * OBS: S�o 11 colunas
	 * 
	 * @param row
	 * @param styleTop
	 * @param texto
	 * @author Rodrigo Freitas
	 */
	private void createRowTotal(HSSFRow row, HSSFCellStyle styleTop, String texto) {
		HSSFCell cell = row.createCell((short)0);
		cell.setCellStyle(styleTop);
		cell.setCellValue(texto);
		
		cell = row.createCell((short)1);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)2);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)3);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)4);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)5);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)6);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)7);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)8);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)9);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)10);
		cell.setCellStyle(styleTop);
		
		cell = row.createCell((short)11);
		cell.setCellStyle(styleTop);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#findForExcel
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Orcamentomaterial> findForExcel(String whereIn) {
		return orcamentomaterialDAO.findForExcel(whereIn);
	}
	
	/**
	 * Processa o arquivo xls de lista de material para uma lista de <code>ImportarListaMaterialBean</code>
	 *
	 * @param content
	 * @return
	 * @throws IOException
	 * @author Rodrigo Freitas
	 */
	public List<ImportarListaMaterialBean> processarArquivoImportacao(byte[] content) throws IOException {
		
		short colunaDescricao = 0;
		short colunaUnidade = 1;
		short colunaQtde = 2;
		short colunaCodigoSined = 3;
		
		POIFSFileSystem fs = new POIFSFileSystem(new ByteArrayInputStream(content));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		
		HSSFSheet p = wb.getSheetAt(0);
		
		List<ImportarListaMaterialBean> lista = new ArrayList<ImportarListaMaterialBean>();
		
		HSSFRow row = null;
		ImportarListaMaterialBean bean;
		int index = 1;
		
		while(p.getRow(index) != null && 
				p.getRow(index).getCell(colunaDescricao) != null &&
				p.getRow(index).getCell(colunaUnidade) != null &&
				p.getRow(index).getCell(colunaQtde) != null){
			row = p.getRow(index);
			
			bean = new ImportarListaMaterialBean();
			
			bean.setDescricao(row.getCell(colunaDescricao).getStringCellValue());
			
			bean.setUnidade(row.getCell(colunaUnidade).getStringCellValue());
			
			bean.setQtde(row.getCell(colunaQtde).getCellType() == HSSFCell.CELL_TYPE_NUMERIC ?
					row.getCell(colunaQtde).getNumericCellValue() : Double.parseDouble(row.getCell(colunaQtde).getStringCellValue()));
			
			if(row.getCell(colunaCodigoSined) != null)
				if(row.getCell(colunaCodigoSined).getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
					bean.setCdmaterial(row.getCell(colunaCodigoSined).getNumericCellValue());
				} else if (!row.getCell(colunaCodigoSined).getStringCellValue().equals("")){
					bean.setCdmaterial(Double.parseDouble(row.getCell(colunaCodigoSined).getStringCellValue()));
				}
			
			
			lista.add(bean);
			
			index++;
		}
		
		return lista;
	}
	
	/**
	 * Processa a lista de material que foi importada de um arquivo xls.
	 * TRansforma uma <code>List<ImportarListaMaterialBean></code> em <code>List<Orcamentomaterialitem></code>.
	 * 
	 * @see br.com.linkcom.sined.geral.service.UnidademedidaService#findBySimbolo
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterialitem> processaListaMateriais(List<ImportarListaMaterialBean> lista) {
		
		List<Orcamentomaterialitem> listaItens = new ArrayList<Orcamentomaterialitem>();
		Orcamentomaterialitem item;
		Unidademedida unidademedida;
		Material material;
		
		for (ImportarListaMaterialBean bean : lista) {
			item = new Orcamentomaterialitem();
			
			item.setDescricao(bean.getDescricao());
			
			unidademedida = unidademedidaService.findBySimbolo(bean.getUnidade());
			if(unidademedida == null || unidademedida.getCdunidademedida() == null)
				throw new SinedException("N�o encontrada a unidade de medida: " + bean.getUnidade());
			
			item.setUnidademedida(unidademedida);
			
			item.setQtde(Integer.parseInt(bean.getQtde().toString().split("\\.")[0]));
			
			if(bean.getCdmaterial() != null){
				material = materialService.load(new Material(Integer.parseInt(bean.getCdmaterial().toString().split("\\.")[0])), "material.cdmaterial, material.nome, material.produto, material.servico, material.epi, material.patrimonio");
				if(material == null || material.getCdmaterial() == null)
					throw new SinedException("N�o encontrada o material com c�digo: " + bean.getCdmaterial());
				
				List<Materialclasse> listaClasse = materialService.findClassesWithoutLoad(material);
				if(listaClasse != null && listaClasse.size() > 0){
					item.setMaterialclasse(listaClasse.get(0));
				}
				
				item.setCdmaterial(material.getCdmaterial());
			}
			
			listaItens.add(item);
		}
		
		return listaItens;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrcamentomaterialDAO#findForCopiaOrcamento
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Orcamentomaterial> findForCopiaOrcamento(String whereIn) {
		return orcamentomaterialDAO.findForCopiaOrcamento(whereIn);
	}
	
}
