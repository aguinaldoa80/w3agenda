package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.dao.DadobancarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DadobancarioService extends GenericService<Dadobancario> {	
	
	private DadobancarioDAO dadobancarioDAO;
	
	public void setDadobancarioDAO(DadobancarioDAO dadobancarioDAO) {
		this.dadobancarioDAO = dadobancarioDAO;
	}
	
	/* singleton */
	private static DadobancarioService instance;
	public static DadobancarioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DadobancarioService.class);
		}
		return instance;
	}
	
	public void deletaColaborador(Colaborador colaborador) {
		dadobancarioDAO.deletaColaborador(colaborador);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name getDadoBancarioByPessoa
	 * @param pessoa
	 * @return
	 * @return Dadobancario
	 * @author Thiago Augusto
	 * @date 17/05/2012
	 *
	 */
	public Dadobancario findDadoBancarioByPessoa(Pessoa pessoa){
		return dadobancarioDAO.findDadoBancarioByPessoa(pessoa);
	}
}
