package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Arquivonfnotaerrocancelamento;
import br.com.linkcom.sined.geral.dao.ArquivonfnotaerrocancelamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivonfnotaerrocancelamentoService extends GenericService<Arquivonfnotaerrocancelamento> {

	private ArquivonfnotaerrocancelamentoDAO arquivonfnotaerrocancelamentoDAO;
	
	public void setArquivonfnotaerrocancelamentoDAO(
			ArquivonfnotaerrocancelamentoDAO arquivonfnotaerrocancelamentoDAO) {
		this.arquivonfnotaerrocancelamentoDAO = arquivonfnotaerrocancelamentoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivonfnotaerrocancelamentoDAO#findByArquivonfnota(Arquivonfnota arquivonfnota)
	 * 
	 * @param arquivonfnota
	 * @return
	 * @since 05/03/2012
	 * @author Rodrigo Freitas
	 */
	public List<Arquivonfnotaerrocancelamento> findByArquivonfnota(Arquivonfnota arquivonfnota) {
		return arquivonfnotaerrocancelamentoDAO.findByArquivonfnota(arquivonfnota);
	}
	
}
