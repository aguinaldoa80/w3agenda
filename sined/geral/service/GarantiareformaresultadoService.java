package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Garantiareformaresultado;
import br.com.linkcom.sined.geral.dao.GarantiareformaresultadoDAO;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GarantiareformaresultadoService extends GenericService<Garantiareformaresultado>{

	private GarantiareformaresultadoDAO garantiareformaresultadoDAO;
	public static final String VARIAVEL_PERCENTUAL = "$GARANTIA_PERCENTUAL";
	
	public void setGarantiareformaresultadoDAO(
			GarantiareformaresultadoDAO garantiareformaresultadoDAO) {
		this.garantiareformaresultadoDAO = garantiareformaresultadoDAO;
	}
	
	public List<GenericBean> findVariaveis(){
		List<GenericBean> retorno = new ArrayList<GenericBean>();
		
		GenericBean var = new GenericBean(VARIAVEL_PERCENTUAL, "Percentual de garantia do servi�o (" + VARIAVEL_PERCENTUAL + ")");

		retorno.add(var);
		
		return retorno;
	}
}
