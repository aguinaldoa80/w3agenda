package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoItemHistorico;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.dao.MaterialTabelaPrecoItemHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialTabelaPrecoItemHistoricoService extends GenericService<MaterialTabelaPrecoItemHistorico>{

	private MaterialTabelaPrecoItemHistoricoDAO materialTabelaPrecoItemHistoricoDAO;
	
	public void setMaterialTabelaPrecoItemHistoricoDAO(
			MaterialTabelaPrecoItemHistoricoDAO materialTabelaPrecoItemHistoricoDAO) {
		this.materialTabelaPrecoItemHistoricoDAO = materialTabelaPrecoItemHistoricoDAO;
	}
	
	public MaterialTabelaPrecoItemHistorico loadUltimoHistoricoComVendaNaoCancelada(Materialtabelaprecoitem bean){
		return materialTabelaPrecoItemHistoricoDAO.loadUltimoHistoricoComVendaNaoCancelada(bean);
	}
	
	public void createHistorico(Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem, Tipocalculo tipoCalculo){
		for(Materialtabelaprecoitem item: listaMaterialtabelaprecoitem){
			MaterialTabelaPrecoItemHistorico historico =  new MaterialTabelaPrecoItemHistorico();
			historico.setMaterialTabelaPrecoItem(item);
			historico.setPedidoVendaMaterial(item.getPedidoVendaMaterial());
			historico.setVendaMaterial(item.getVendaMaterial());
			historico.setVendaOrcamentoMaterial(item.getVendaOrcamentoMaterial());
			if(Tipocalculo.EM_VALOR.equals(tipoCalculo)){
				historico.setValor(item.getValor());
			}else if(Tipocalculo.PERCENTUAL.equals(tipoCalculo)){
				historico.setPercentualDesconto(item.getPercentualdesconto());
			}

			this.saveOrUpdate(historico);
		}
	}
}
