package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.dao.MaterialcategoriaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.suprimento.MaterialCategoriaTreeFiltro;

public class MaterialcategoriaService extends GenericService<Materialcategoria>{
	
	private MaterialcategoriaDAO materialcategoriaDAO;
	private static MaterialcategoriaService instance;
	
	public void setMaterialcategoriaDAO(MaterialcategoriaDAO materialcategoriaDAO) {
		this.materialcategoriaDAO = materialcategoriaDAO;
	}
	
	public static MaterialcategoriaService getInstance() {
		if (instance==null){
			instance = Neo.getObject(MaterialcategoriaService.class);
		}
		
		return instance;
	}
	
	public void preparaParaSalvar(Materialcategoria bean){

		if (bean.getItem() == null) {
			//setar o campo item para gera��o do identificador
			if (bean.getMaterialcategoriapai() != null && bean.getMaterialcategoriapai().getCdmaterialcategoria() != null) {
				List<Materialcategoria> listaFilhos = materialcategoriaDAO.findFilhos(bean.getMaterialcategoriapai());
				if (listaFilhos.size() > 0) {
					bean.setItem(listaFilhos.get(0).getItem() + 1);
				} else {
					bean.setItem(1);
				}
			} else {
				List<Materialcategoria> listaPais = materialcategoriaDAO.findRaiz();
				if (listaPais.size() > 0) {
					if (listaPais.get(0).getItem() != null)
						bean.setItem(listaPais.get(0).getItem() + 1);
					else
						bean.setItem(listaPais.size() + 1);
					
					if(bean.getItem() != null){
						Integer tamanhoFormato = bean.getItem().toString().length();
						if(tamanhoFormato > 2){
							StringBuilder formato = new StringBuilder();
							for(int i = 0; i < tamanhoFormato; i++)
								formato.append("0");
							bean.setFormato(formato.toString());
						}
					}
				} else {
					bean.setItem(1);
				}
			}
		}
	}
	
	/**
	* M�todo que busca as categorias ativas do material
	*
	* @param q
	* @return
	* @since 05/08/2016
	* @author Luiz Fernando
	*/
	public List<Materialcategoria> findAutocompleteAtivos(String q) {
		return materialcategoriaDAO.findAutocomplete(q, true);
	}
	
	/**
	* M�todo autocomplete da categoria do material
	*
	* @param q
	* @return
	* @since 05/08/2016
	* @author Luiz Fernando
	*/
	public List<Materialcategoria> findAutocomplete(String q) {
		return materialcategoriaDAO.findAutocomplete(q, false);
	}
	
	public List<Materialcategoria> findAutocompleteTreeView(MaterialCategoriaTreeFiltro filtro) {
		return materialcategoriaDAO.findAutocompleteTreeView(filtro);
	}		
	
	public List<Materialcategoria> findForTreeView(MaterialCategoriaTreeFiltro filtro) {
		List<Materialcategoria> listaMaterialcategoria = materialcategoriaDAO.findForTreeView(filtro);
		List<Materialcategoria> listaRaiz = new ArrayList<Materialcategoria>();
		
		for (Materialcategoria materialcategoria: listaMaterialcategoria) {
			if (materialcategoria.getMaterialcategoriapai()==null || materialcategoria.getMaterialcategoriapai().getCdmaterialcategoria()==null){
				listaRaiz.add(materialcategoria);
			}
		}
		
		for (Materialcategoria raiz: listaRaiz) {
			raiz.setFilhos(this.somenteFilhos(raiz, listaMaterialcategoria));
		}
		
		return listaRaiz;
	}
	
	private List<Materialcategoria> somenteFilhos(Materialcategoria materialcategoriapai, List<Materialcategoria> lista) {
		List<Materialcategoria> listaFilhos = new ArrayList<Materialcategoria>();
		
		for (Materialcategoria materialcategoria: lista) {
			if (materialcategoria.getMaterialcategoriapai()!=null && 
					materialcategoria.getMaterialcategoriapai().getCdmaterialcategoria() != null &&
					materialcategoria.getMaterialcategoriapai().getCdmaterialcategoria().equals(materialcategoriapai.getCdmaterialcategoria())){
				materialcategoria.setFilhos(somenteFilhos(materialcategoria, lista));
				listaFilhos.add(materialcategoria);
			}
		}
		
		return listaFilhos;
	}
	
	/**
	 * Este m�todo existe pois a pesquisa de Categoria de Material n�o � feita pelo sei ID e sim pelo campo IDENTIFICADOR.
	 * 
	 * @param materialcategoria
	 * @author Thiago Clemente
	 * 
	 */
	public void loadIdentificador(Materialcategoria materialcategoria){
		if (materialcategoria!=null && materialcategoria.getCdmaterialcategoria()!=null && (materialcategoria.getIdentificador()==null || materialcategoria.getIdentificador().trim().equals(""))){
			materialcategoria.setIdentificador(load(materialcategoria, "materialcategoria.vmaterialcategoria").getIdentificador());
		}
	}
	
	/**
	 * M�todo criado para facilitar o filtro de Categoria do material. Pois em alguns filtros, a query estava ficando muito lenta
	 * 
	 * @param identificador
	 * @author Thiago Clemente
	 * 
	 */
	public List<Materialcategoria> findForFitlro(String identificador) {
		return materialcategoriaDAO.findForFitlro(identificador);
	}
}