package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AcaoService extends GenericService<Acao> {

	/* singleton */
	private static AcaoService instance;
	
	public static AcaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(AcaoService.class);
		}
		return instance;
	}
}
