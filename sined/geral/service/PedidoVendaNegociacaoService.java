package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.PedidoVendaNegociacao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.dao.PedidoVendaNegociacaoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaNegociacaoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidoVendaNegociacaoService extends GenericService<PedidoVendaNegociacao> {
	
	private PedidoVendaNegociacaoDAO pedidoVendaNegociacaoDAO;
	
	public void setPedidoVendaNegociacaoDAO(
			PedidoVendaNegociacaoDAO PedidoVendaNegociacaoDAO) {
		this.pedidoVendaNegociacaoDAO = PedidoVendaNegociacaoDAO;
	}

	public List<PedidoVendaNegociacao> findByPedidovenda(Pedidovenda pedidovenda,boolean isBuscarCancelados) {
		return pedidoVendaNegociacaoDAO.findByPedidovenda(pedidovenda,isBuscarCancelados);
	}
	
	public List<PedidoVendaNegociacaoWSBean> toWSList(List<PedidoVendaNegociacao> listaPedidoVendaNegociacao){
		List<PedidoVendaNegociacaoWSBean> lista = new ArrayList<PedidoVendaNegociacaoWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidoVendaNegociacao)){
			for(PedidoVendaNegociacao pvn: listaPedidoVendaNegociacao){
				PedidoVendaNegociacaoWSBean bean = new PedidoVendaNegociacaoWSBean();
				bean.setAgencia(pvn.getAgencia());
				bean.setBanco(pvn.getBanco());
				bean.setCdPedidoVendaNegociacao(pvn.getCdPedidoVendaNegociacao());
				bean.setCheque(ObjectUtils.translateEntityToGenericBean(pvn.getCheque()));
				bean.setConta(pvn.getConta());
				bean.setCpfcnpj(pvn.getCpfcnpj());
				bean.setDataParcela(pvn.getDataParcela());
				bean.setDocumento(ObjectUtils.translateEntityToGenericBean(pvn.getDocumento()));
				bean.setDocumentoAntecipacao(ObjectUtils.translateEntityToGenericBean(pvn.getDocumentoAntecipacao()));
				bean.setDocumentoTipo(ObjectUtils.translateEntityToGenericBean(pvn.getDocumentoTipo()));
				bean.setEmitente(pvn.getEmitente());
				bean.setNumero(pvn.getNumero());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvn.getPedidovenda()));
				bean.setValorJuros(pvn.getValorJuros());
				bean.setValorOriginal(pvn.getValorOriginal());
				
				lista.add(bean);
			}
		}
		return lista;
	}
}
