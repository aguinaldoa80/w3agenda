package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.dao.OtrClassificacaoCodigoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrClassificacaoCodigoService extends GenericService<OtrClassificacaoCodigo>{
	OtrClassificacaoCodigoDAO otrClassificacaoCodigoDao;

	public OtrClassificacaoCodigoDAO getOtrClassificacaoCodigoDao() {
		return otrClassificacaoCodigoDao;
	}

	public void setOtrClassificacaoCodigoDao(
			OtrClassificacaoCodigoDAO otrClassificacaoCodigoDao) {
		this.otrClassificacaoCodigoDao = otrClassificacaoCodigoDao;
	}
	
	
	public Boolean validaCodigo(String codigo, OtrClassificacaoCodigo bean){	
		return otrClassificacaoCodigoDao.validaCodigo(codigo, bean);
	}
	
	public List<OtrClassificacaoCodigo> findAutocomplete(String codigo){
		return otrClassificacaoCodigoDao.findAutocomplete(codigo);
	}
	
}
