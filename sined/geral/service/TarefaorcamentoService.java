package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayInputStream;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.BdiForm;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Indicerecursogeral;
import br.com.linkcom.sined.geral.bean.Indicerecursohumano;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.geral.bean.Tarefaorcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorg;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorh;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.auxiliar.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.OrcamentoTarefaVO;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporecursogeral;
import br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoGeralFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.OrcamentoRecursoHumanoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.ImportarTarefaBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.RecursosBean;
import br.com.linkcom.sined.modulo.projeto.controller.process.filter.TarefaorcamentodetalhadoCSVFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.TarefaOrcamentoReportFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefaorcamentoService extends GenericService<Tarefaorcamento>{

	public static final String WHERENOTIN_TAREFA_SESSION = "wherenotin_tarefaorcamento_session";
	public static final String SESSION_FILTRO_RELATORIO_TAREFA = "SESSION_FILTRO_RELATORIO_TAREFA";
	public static final String SESSION_FILTRO_RELATORIO_ORCAMENTO_DETALHADO = "SESSION_FILTRO_RELATORIO_ORCAMENTO_DETALHADO";
	
	private IndiceService indiceService;
	private TarefaorcamentoDAO tarefaorcamentoDAO;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private UnidademedidaService unidademedidaService;
	private BdiService bdiService;
	private RecursocomposicaoService recursocomposicaoService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private ContagerencialService contagerencialService;
	private AreaService areaService;
	private MaterialService materialService;
	private CargoService cargoService;
	private CalendarioService calendarioService;
	private ComposicaomaoobraService composicaomaoobraService;
	
	public void setComposicaomaoobraService(ComposicaomaoobraService composicaomaoobraService) {this.composicaomaoobraService = composicaomaoobraService;}
	public void setOrcamentorecursohumanoService(OrcamentorecursohumanoService orcamentorecursohumanoService) {this.orcamentorecursohumanoService = orcamentorecursohumanoService;}
	public void setRecursocomposicaoService(RecursocomposicaoService recursocomposicaoService) {this.recursocomposicaoService = recursocomposicaoService;}
	public void setBdiService(BdiService bdiService) {this.bdiService = bdiService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setTarefaorcamentoDAO(TarefaorcamentoDAO tarefaorcamentoDAO) {this.tarefaorcamentoDAO = tarefaorcamentoDAO;}	
	public void setIndiceService(IndiceService indiceService) {this.indiceService = indiceService;}
	public void setPeriodoorcamentocargoService(PeriodoorcamentocargoService periodoorcamentocargoService) {this.periodoorcamentocargoService = periodoorcamentocargoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setAreaService(AreaService areaService) {this.areaService = areaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setCargoService(CargoService cargoService) {this.cargoService = cargoService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	
	public OrcamentoTarefaVO getListasForFlex(Orcamento orcamento){
		OrcamentoTarefaVO vo = new OrcamentoTarefaVO();
		
		vo.setListaContagerencial(contagerencialService.findDebitoForFlex());
		vo.setListaIndice(indiceService.findAllForFlex());
		vo.setListaArea(areaService.findAllForFlex());
		
		List<Material> listaMaterial = materialService.findAllForFlex();
		List<MaterialVO> listaMaterialVO = new ArrayList<MaterialVO>();
		MaterialVO materialVO;
		for (Material material : listaMaterial) {
			materialVO = new MaterialVO();
			materialVO.setCdmaterial(material.getCdmaterial());
			materialVO.setNome(material.getNome());
			materialVO.setValorcusto(material.getValorcusto());
			materialVO.setValorvenda(material.getValorvenda());
			listaMaterialVO.add(materialVO);
		}		
		vo.setListaMaterialVO(listaMaterialVO);
		
		vo.setListaCargo(cargoService.findAllMODForFlex());
		vo.setListaUnidademedida(unidademedidaService.findAllForFlex());
		vo.setCalendario(calendarioService.getByOrcamento(orcamento));
		vo.setListaRecursocomposicao(recursocomposicaoService.findRecursoDiretoByOrcamento(orcamento));
		vo.setListaOrcamentoRecursohumano(orcamentorecursohumanoService.findByCargoOrcamento(orcamento));
		vo.setListaComposicaomaoobra(composicaomaoobraService.findAllForFlex());
		
		return vo;
	}
	
	/**
	 * M�todo para organizar os ids das tarefas da tela em flex.
	 *
	 * @param listaTarefa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> atualizaIdsFlex(List<Tarefaorcamento> listaTarefa){
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		atualizaIdListaFlex(listaTarefa, 0, map);
		modificaPredecessora(listaTarefa, map);
		return listaTarefa;
	}
	
	/**
	 * Modifica os idds da predecessora que teve seu id modificado.
	 *
	 * @param listaTarefa
	 * @param map
	 * @author Rodrigo Freitas
	 */
	private void modificaPredecessora(List<Tarefaorcamento> listaTarefa, Map<Integer, Integer> map) {
		Integer result = null;
		for (Tarefaorcamento tarefa : listaTarefa) {
			if (tarefa.getPredecessoras() != null && !tarefa.getPredecessoras().equals("")) {
				String[] array = tarefa.getPredecessoras().split(";");
				for (int i = 0; i < array.length; i++) {
					result = map.get(Integer.parseInt(array[i]));
					if (result != null) {
						array[i] = result.toString();
					}
				}
				tarefa.setPredecessoras(joinArray(array, ";"));
			}
			if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
				modificaPredecessora(tarefa.getListaTarefaFilha(), map);
			}
		}		
	}
	
	/**
	 * Junta um array de string em apenas uma string com o separador.
	 *
	 * @param array
	 * @param separator
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String joinArray(String[] array, String separator){
		String string = "";
		for (int i = 0; i < array.length; i++) {
			string += array[i] + separator;
		}
		return string.substring(0,string.length() - separator.length());
	}

	/**
	 * Atualiza o id da lista na tela em flex.
	 *
	 * @param listaTarefa
	 * @param id
	 * @param map
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Integer atualizaIdListaFlex(List<Tarefaorcamento> listaTarefa, Integer id, Map<Integer, Integer> map){
		for (Tarefaorcamento tarefa : listaTarefa) {
			map.put(tarefa.getId(), id);
			tarefa.setId(id);
			id++;
			if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
				id = atualizaIdListaFlex(tarefa.getListaTarefaFilha(), id, map);
			}
		}
		return id;
	}
	
	/**
	 * Carrega as listas de indices e transforma em tarefa para uso na tela em flex.
	 *
	 * @param indice2
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Tarefaorcamento carregaListasIndiceFlex(Indice indice2){
		Indice indice = indiceService.findWithListasFlex(indice2);
		
		double num = indice2.getQtde()/indice.getQtde();
		Tarefaorcamento tarefa = new Tarefaorcamento();
		transformIndiceTarefa(indice, num, tarefa);
		
		return tarefa;
	}
	
	/**
	 * Transforma as lista de RG e RH de �ndice para uma Tarefa.
	 *
	 * @param indice
	 * @param num
	 * @return
	 * @author Rodrigo Freitas
	 */
	private void transformIndiceTarefa(Indice indice, double num, Tarefaorcamento tarefa) {
		List<Tarefaorcamentorg> listaTarefaGeral = new ArrayList<Tarefaorcamentorg>();
		Tarefaorcamentorg tarefarecursogeral = null;
		if(indice.getListaIndicerecursogeral() != null){
			for (Indicerecursogeral ig : indice.getListaIndicerecursogeral()) {
				tarefarecursogeral = new Tarefaorcamentorg();
				if(ig.getMaterial() != null){
					tarefarecursogeral.setTiporecursogeral(Tiporecursogeral.MATERIAL);
					tarefarecursogeral.setMaterial(ig.getMaterial());
				} else {
					tarefarecursogeral.setTiporecursogeral(Tiporecursogeral.OUTRO);
					tarefarecursogeral.setUnidademedida(ig.getUnidademedida());
					tarefarecursogeral.setOutro(ig.getOutro());
				}
				tarefarecursogeral.setQuantidade(new Double(num*ig.getQtde()));
				listaTarefaGeral.add(tarefarecursogeral);
			}
		}
		
		List<Tarefaorcamentorh> listaTarefaHumano = new ArrayList<Tarefaorcamentorh>();
		Tarefaorcamentorh tarefarecursohumano = null;
		if (indice.getListaIndicerecursohumano() != null) {
			for (Indicerecursohumano ih : indice.getListaIndicerecursohumano() ) {
				tarefarecursohumano = new Tarefaorcamentorh();
				tarefarecursohumano.setCargo(ih.getCargo());
				tarefarecursohumano.setQuantidade(new Double(num*ih.getQtde()));
				listaTarefaHumano.add(tarefarecursohumano);
			}
		}
		
		tarefa.setUnidademedida(indice.getUnidademedida());
		tarefa.setListaTarefaorcamentorg(listaTarefaGeral);
		tarefa.setListaTarefaorcamentorh(listaTarefaHumano);
	}
	
	/**
	 * Salvar a lista da tela em flex no BD, e retorna a lista atualizada.
	 *
	 * @param listaTarefa
	 * @param sair
	 * @param orcamento
	 * @param recalcularHistograma
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> sincronizaListaFlex(final List<Tarefaorcamento> listaTarefa, Boolean sair, final Orcamento orcamento, final Boolean recalcularHistograma) {
		
		NeoWeb.getRequestContext().getSession().removeAttribute(WHERENOTIN_TAREFA_SESSION);
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				if(listaTarefa.get(0).getListaTarefaFilha() != null && listaTarefa.get(0).getListaTarefaFilha().size() > 0){
					salvaListaFilha(listaTarefa.get(0).getListaTarefaFilha(), null, orcamento);
				} else {
//					DELETAR TODAS AS TAREFAS QUE EXISTEM DO OR�AMENTO
					deleteWhereNotIn("0", orcamento);
				}
				
				if (recalcularHistograma) {
					if(orcamento.getCalcularhistograma()){
						periodoorcamentocargoService.atualizaValoresHistograma(orcamento);
					} else {
						periodoorcamentocargoService.deleteByOrcamento(orcamento);
						orcamentorecursohumanoService.atualizaOrcamentoRecursoHumanoSemHistogramaFlex(orcamento);
						recursocomposicaoService.atualizaOrcamentoRecursoGeralSemHistogramaFlex(orcamento);
					}
				}
				
				return null;
			}
		});		
		
		String string = NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION) != null ? NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION).toString() : null;
		if(string != null && !string.equals("")){
			this.deleteWhereNotIn(string, orcamento);
		}
		if(sair != null && sair){
			return null;
		} else {
			return getListaTarefaFlex(orcamento);
		}
		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#deleteWhereNotIn
	 * @param string
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	private void deleteWhereNotIn(String string, Orcamento orcamento) {
		tarefaorcamentoDAO.deleteWhereNotIn(string, orcamento);
	}
	
	/**
	 * Salva a lista de tarefas filhas para a tela em flex.
	 *
	 * @param listaTarefaFilha
	 * @param pai
	 * @param planejamento
	 * @author Rodrigo Freitas
	 */
	private void salvaListaFilha(List<Tarefaorcamento> listaTarefaFilha, Tarefaorcamento pai, Orcamento orcamento) {
		if(listaTarefaFilha != null){
			String string = null;
			for (Tarefaorcamento tarefa : listaTarefaFilha) {
				if (tarefa.getCdtarefaorcamento().equals(-1)) {
					tarefa.setCdtarefaorcamento(null);
				}
				tarefa.setOrcamento(orcamento);
				tarefa.setTarefapai(pai);
				if(tarefa.getListaTarefaorcamentorg() != null){
					for (Tarefaorcamentorg geral : tarefa.getListaTarefaorcamentorg()) {
						if (geral.getCdtarefaorcamentorg() != null && geral.getCdtarefaorcamentorg().equals(0)) {
							geral.setCdtarefaorcamentorg(null);
						}
					}
				}
				if(tarefa.getListaTarefaorcamentorh() != null){
					for (Tarefaorcamentorh humano : tarefa.getListaTarefaorcamentorh()) {
						if (humano.getCdtarefaorcamentorh() != null && humano.getCdtarefaorcamentorh().equals(0)) {
							humano.setCdtarefaorcamentorh(null);
						}
					}
				}
				
				saveOrUpdateNoUseTransaction(tarefa);
				string = NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION) != null ? NeoWeb.getRequestContext().getSession().getAttribute(WHERENOTIN_TAREFA_SESSION).toString() : null;
				if (string == null) {
					string = tarefa.getCdtarefaorcamento().toString();
				} else {
					string += ","+tarefa.getCdtarefaorcamento().toString();
				}
				NeoWeb.getRequestContext().getSession().setAttribute(WHERENOTIN_TAREFA_SESSION, string);
				if (tarefa.getListaTarefaFilha() != null && tarefa.getListaTarefaFilha().size() > 0) {
					salvaListaFilha(tarefa.getListaTarefaFilha(), tarefa, orcamento);
				}
				
			}
		}
	}
	
	/**
	 * Retorna a lista j� formatada para a tela em flex.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefaService#getListaTarefaFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> getListaDPFlex(Orcamento orcamento){
		return getListaTarefaFlex(orcamento);
	}
	
	/**
	 * Retorna a lista de tarefas preenchidas.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#preencheListaPlanejamentoRecGeral
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#preencheListaPlanejamentoRecHumano
	 * @see br.com.linkcom.sined.geral.service.TarefaService#getListaFilha
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Tarefaorcamento> getListaTarefaFlex(Orcamento orcamento) {
		Tarefaorcamento tarefa = new Tarefaorcamento();
		tarefa.setCdtarefaorcamento(0);
		tarefa.setId(0);
		tarefa.setDescricao(orcamento.getNome());
		tarefa.setDtinicio(null);
		tarefa.setDtfim(null);
		tarefa.setDuracao(0);
		
		List<Tarefaorcamento> lista = findTarefasByOrcamento(orcamento);
		List<Tarefaorcamento> listaHierarquizada = new ArrayList<Tarefaorcamento>();
		montaHierarquiaTarefas(lista, listaHierarquizada, tarefa, orcamento);
		tarefa.setListaTarefaFilha(listaHierarquizada);
		
		Date dataInicio = null;
		Date dataFim = null;
		
		for (Tarefaorcamento t : tarefa.getListaTarefaFilha()) {
			t.setTarefapai(tarefa);
			
			if(dataInicio == null || (t.getDtinicio() != null && SinedDateUtils.beforeIgnoreHour(t.getDtinicio(), dataInicio))){
				dataInicio = t.getDtinicio();
			}
			if(dataFim == null || (t.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(dataFim, t.getDtfim()))){
				dataFim = t.getDtfim();
			}
		}
		tarefa.setDtinicio(dataInicio);
		tarefa.setDtfim(dataFim);
		
		tarefa.setDuracao(SinedDateUtils.diferencaDias(dataInicio, dataFim, null) + 1);
		
		List<Tarefaorcamento> listaDP = new ArrayList<Tarefaorcamento>();
		listaDP.add(tarefa);
		
		return atualizaIdsFlex(listaDP);
	}
	
//	/**
//	 * Carrega a lista de tarefas filhas.
//	 *
//	 * @see br.com.linkcom.sined.geral.service.TarefaService#listaWithPai
//	 * @param tarefa
//	 * @param planejamento
//	 * @param plan
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	private List<Tarefaorcamento> getListaFilha(Tarefaorcamento tarefa, Orcamento orcamento, Boolean plan) {
//		List<Tarefaorcamento> lista = null;
//		if(plan) lista = this.listaWithPai(null, orcamento);
//		else lista = this.listaWithPai(tarefa, orcamento);
//		for (Tarefaorcamento t2 : lista) {
//			t2.setTarefapai(tarefa);
//			t2.setListaTarefaFilha(getListaFilha(t2, orcamento, false));
//		}
//		return lista;
//	}
	
//	/**
//	 * Faz refer�ncia ao DAO.
//	 *
//	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#listaWithPai
//	 * @param tarefa
//	 * @param planejamento
//	 * @return
//	 * @author Rodrigo Freitas
//	 */
//	private List<Tarefaorcamento> listaWithPai(Tarefaorcamento tarefa, Orcamento orcamento) {
//		return tarefaorcamentoDAO.listaWithPai(tarefa, orcamento);
//	}
	
	private void montaHierarquiaTarefas(List<Tarefaorcamento> lista, List<Tarefaorcamento> listaHierarquizada, Tarefaorcamento tarefaPai, Orcamento orcamento) {
		for (Tarefaorcamento tarefaFilha : lista) {
			if (tarefaPai.getId() != null && tarefaPai.getId().intValue() == 0 && tarefaFilha.getTarefapai() == null) {
				tarefaFilha.setTarefapai(tarefaPai);
				listaHierarquizada.add(tarefaFilha);
				montaHierarquiaTarefas(lista, listaHierarquizada, tarefaFilha, orcamento);
			}
			else if (tarefaPai.equals(tarefaFilha.getTarefapai())) {
				tarefaFilha.setTarefapai(tarefaPai);
				if (tarefaPai.getListaTarefaFilha() == null) {
					tarefaPai.setListaTarefaFilha(new ArrayList<Tarefaorcamento>());
				}
				tarefaPai.getListaTarefaFilha().add(tarefaFilha);
				montaHierarquiaTarefas(lista, tarefaPai.getListaTarefaFilha(), tarefaFilha, orcamento);
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TarefaDAO#findTarefasByOrcamento
	 * @param orcamento
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	private List<Tarefaorcamento> findTarefasByOrcamento(Orcamento orcamento) {
		return tarefaorcamentoDAO.findTarefasByOrcamento(orcamento);
	}
	
	/**
	 * Captura todas as linhas do arquivo xls, e transforma para <code>ImportarTarefaBean</code>.
	 *
	 * @param content
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public List<ImportarTarefaBean> processarArquivoImportacao(byte[] content) throws Exception {
		 
		
		short colunaId = 0;
		short colunaIdPai = 1;
		short colunaDescricao = 2;
		short colunaUnidade = 3;
		short colunaQtde = 4;
		short colunaIndice = 5;
		
		POIFSFileSystem fs = new POIFSFileSystem(new ByteArrayInputStream(content));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		
		HSSFSheet p = wb.getSheetAt(0);
		
		List<ImportarTarefaBean> lista = new ArrayList<ImportarTarefaBean>();
		
		HSSFRow row = null;
		ImportarTarefaBean bean;
		int index = 1;
		
		row = p.getRow(index);
		
		Double idPrimeiro = row.getCell((short)0).getCellType() == HSSFCell.CELL_TYPE_NUMERIC ? 
				row.getCell((short)0).getNumericCellValue() :
				Double.parseDouble(row.getCell((short)0).getStringCellValue());
		
		if(idPrimeiro == null || idPrimeiro.intValue() != 1){
			throw new SinedException("Id da primeira tarefa tem que ser 1.");
		}
		
		while(p.getRow(index) != null && p.getRow(index).getCell(colunaId) != null){
			row = p.getRow(index);
			
			bean = new ImportarTarefaBean();
			
			if(row.getCell(colunaId).getCellType() != HSSFCell.CELL_TYPE_NUMERIC && 
					row.getCell(colunaId).getStringCellValue() != null && 
					row.getCell(colunaId).getStringCellValue().equals("")){
				break;
			}
			
			bean.setId(row.getCell(colunaId).getCellType() == HSSFCell.CELL_TYPE_NUMERIC ?
					row.getCell(colunaId).getNumericCellValue() : Double.parseDouble(row.getCell(colunaId).getStringCellValue().trim()));
			
			if(index != bean.getId().intValue()){
				throw new SinedException("O Id tem que ser incremental.");
			}
			
			bean.setDescricao(row.getCell(colunaDescricao).getStringCellValue().trim());
			
			HSSFCell unidadeCell = row.getCell(colunaUnidade);
			if(unidadeCell != null){
				String unidadeStr = unidadeCell.getStringCellValue();
				if(unidadeStr != null)
					bean.setUnidade(unidadeStr.trim());
			}
			
			HSSFCell qtdeCell = row.getCell(colunaQtde);
			if(qtdeCell != null){
				bean.setQtde(qtdeCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC ?
						qtdeCell.getNumericCellValue() : 
							(qtdeCell.getStringCellValue() != null && !qtdeCell.getStringCellValue().trim().equals("") ? Double.valueOf(qtdeCell.getStringCellValue().trim()) : 0.0));
			}
			
			if(row.getCell(colunaIndice) != null)
				if(row.getCell(colunaIndice).getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
					bean.setIndice(String.valueOf(new Double(row.getCell(colunaIndice).getNumericCellValue()).intValue()));
				} else if (!row.getCell(colunaIndice).getStringCellValue().trim().equals("")){
					bean.setIndice(row.getCell(colunaIndice).getStringCellValue().trim());
				}
			
			if(row.getCell(colunaIdPai) != null)
				if(row.getCell(colunaIdPai).getCellType() == HSSFCell.CELL_TYPE_NUMERIC){
					bean.setIdtarefapai(row.getCell(colunaIdPai).getNumericCellValue());
				} else if (!row.getCell(colunaIdPai).getStringCellValue().trim().equals("")){
					bean.setIdtarefapai(Double.parseDouble(row.getCell(colunaIdPai).getStringCellValue().trim()));
				}

			if(bean.getId() != null && 
					bean.getIdtarefapai() != null && 
					bean.getId().intValue() == bean.getIdtarefapai().intValue()){
				throw new SinedException("O Id do pai da tarefa n�o pode ser o pr�prio Id da tarefa.");
			}
			
			lista.add(bean);
			
			index++;
		}
		
		return lista;
	}
	
	/**
	 * Processa a lista de tarefas vindas de um arquivo xls.
	 * 
	 * @see br.com.linkcom.sined.geral.service.UnidademedidaService#findBySimbolo
	 * @see br.com.linkcom.sined.geral.service.IndiceService#findWithListasFlex
	 * @param lista
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> processaListaTarefas(List<ImportarTarefaBean> lista, Orcamento orcamento) {
		List<Tarefaorcamento> listaTarefa = new ArrayList<Tarefaorcamento>();
		
		Tarefaorcamento taf = null;
		Indice indice;
		
		for (ImportarTarefaBean b : lista){
			taf = new Tarefaorcamento();
			
			taf.setId(Integer.parseInt(b.getId().toString().split("\\.")[0]));
			taf.setDescricao(b.getDescricao());
			taf.setQtde(b.getQtde());
			
			if(b.getIndice() != null){
				indice = null;
				indice = indiceService.findForImportacaoTarefa(b.getIndice());
				if(indice == null){
					throw new SinedException("N�o encontrado a composi��o: " + b.getIndice());
				}
				taf.setIndice(indice);
				transformIndiceTarefa(indice, taf.getQtde()/indice.getQtde(), taf);
			}
			
			taf.setOrcamento(orcamento);
			taf.setIdtarefapai(b.getIdtarefapai());
			
			listaTarefa.add(taf);
		}
		
		
		for (Tarefaorcamento to : listaTarefa) {
			if(to.getIdtarefapai() != null)
				to.setTarefapai(listaTarefa.get(to.getIdtarefapai().intValue() - 1));
		}
		
		return listaTarefa;
	}
	
	
	/**
	 * Salva a lista de tarefa de um or�amento.
	 *
	 * @param listaTarefa
	 * @author Rodrigo Freitas
	 */
	public void saveListaTarefa(final List<Tarefaorcamento> listaTarefa) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Tarefaorcamento tarefaorcamento : listaTarefa) {					
					salvaTarefa(tarefaorcamento);
				}
				
				return null;
			}
		});
	}
	
	/**
	 * Salva a tarefa, mas antes verifica de existe uma tarefa pai n�o salva.
	 *
	 * @param tarefaorcamento
	 * @author Rodrigo Freitas
	 */
	private void salvaTarefa(Tarefaorcamento tarefaorcamento){
		if(tarefaorcamento.getTarefapai() != null && tarefaorcamento.getTarefapai().getCdtarefaorcamento() == null){
			salvaTarefa(tarefaorcamento.getTarefapai());
		}
		saveOrUpdateNoUseTransaction(tarefaorcamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO#haveWithOrcamento
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveWithOrcamento(Orcamento orcamento) {
		return tarefaorcamentoDAO.haveWithOrcamento(orcamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO#deleteByOrcamento
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	public void deleteByOrcamento(Orcamento orcamento) {
		tarefaorcamentoDAO.deleteByOrcamento(orcamento);	
	}
	
	/**
	 * Valida o arquivo para ver se tem relacionamento de pai recursivo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#verificaPai
	 * @param lista
	 * @author Rodrigo Freitas
	 */
	public void validaArquivo(List<ImportarTarefaBean> lista) {
		
		for (ImportarTarefaBean bean : lista) {
			if(bean.getIdtarefapai() != null && verificaPai(bean.getId().intValue(), bean.getIdtarefapai().intValue(), lista)){
				throw new SinedException("Relacionamento de tarefa pai recursivo.");
			}
		}
	}
	
	/**
	 * Verifica se a tarefa � de um relacionamento recursivo.
	 *
	 * @param id
	 * @param index
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Boolean verificaPai(int id, int index, List<ImportarTarefaBean> lista){
		ImportarTarefaBean bean = lista.get(index-1);
		
		if(bean.getId().intValue() == id) {
			return true;
		} else if(bean.getIdtarefapai() != null){
			return verificaPai(id, bean.getIdtarefapai().intValue(), lista);
		} else {
			return false;
		}
	}
	
	/**
	 * Gera o relat�rio de tarefas do or�amento.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorioTarefaOrcamento(TarefaOrcamentoReportFiltro filtro) {
		
		if(filtro == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		
		boolean precocusto = filtro.getPrecocusto() != null && filtro.getPrecocusto();
		boolean precovenda = filtro.getPrecovenda() != null && filtro.getPrecovenda();
		
		Report report;
		if(precocusto && precovenda){
			report = new Report("/projeto/tarefaOrcamento_comCusto");
		} else if(precocusto){
			report = new Report("/projeto/tarefaOrcamento_semVenda");
		} else if(precovenda){
			report = new Report("/projeto/tarefaOrcamento_semCusto");
		} else {
			report = new Report("/projeto/tarefaOrcamento");
		}
		
		List<Tarefaorcamento> listaTarefa = this.findForReport(filtro);
		
		this.organizaListaTarefa(listaTarefa);
		
		Double totalCusto = this.calculaTotal(listaTarefa, 3);
		Double totalMat = this.calculaTotal(listaTarefa, 1);
		Double totalMo = this.calculaTotal(listaTarefa, 2);
		Double totalAcrescido = this.calculaTotal(listaTarefa, 5);
		
		this.calculoValorVenda(listaTarefa, totalCusto, filtro.getOrcamento());
		
		Double totalVenda = this.calculaTotal(listaTarefa, 4);
		
		Tarefaorcamento tarefa;
		for (Tarefaorcamento tarefaorcamento : listaTarefa) {
			tarefa = new Tarefaorcamento(tarefaorcamento.getCdtarefaorcamento());
			tarefa = this.loadTarefa(tarefa);
			tarefaorcamento.setQtde(tarefa != null ? tarefa.getQtde() : 0);
		}
		
		this.incluiIdentificador(listaTarefa);
		
		report.setDataSource(listaTarefa);
		
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		report.addParameter("TOTALCUSTO", "R$ " + decimalFormat.format(totalCusto));
		report.addParameter("TOTALVENDA", "R$ " + decimalFormat.format(totalVenda));
		report.addParameter("TOTALMAT", "R$ " + decimalFormat.format(totalMat));
		report.addParameter("TOTALMO", "R$ " + decimalFormat.format(totalMo));
		report.addParameter("TOTALACRESCIDO", "R$ " + decimalFormat.format(totalAcrescido));
		
		report.addParameter("ORCAMENTO", filtro.getOrcamento().getNome());
		
		return report;
	}
	
	private Tarefaorcamento loadTarefa(Tarefaorcamento tarefa) {
		return tarefaorcamentoDAO.loadTarefa(tarefa);
	}
	
	private void incluiIdentificador(List<Tarefaorcamento> listaTarefa) {
		
		Map<Integer, String> mapId = new HashMap<Integer, String>();
		Map<Integer, Integer> mapPxId = new HashMap<Integer, Integer>();
		
		String idPai;
		Integer pxIdPai;
		int idxRaiz = 1;
		for(Tarefaorcamento to : listaTarefa){
			if(to.getTarefapai() == null || to.getTarefapai().getCdtarefaorcamento() == null ||  to.getTarefapai().getCdtarefaorcamento().equals(0)){
				to.setIdentificador(idxRaiz + "");
				idxRaiz++;
			} else {
				idPai = null;
				pxIdPai = null;
				idPai = mapId.get(to.getTarefapai().getCdtarefaorcamento());
				pxIdPai = mapPxId.get(to.getTarefapai().getCdtarefaorcamento());
				
				to.setIdentificador(idPai + "." + pxIdPai);
				
				pxIdPai++;
				mapPxId.put(to.getTarefapai().getCdtarefaorcamento(), pxIdPai);
			}
			
			mapId.put(to.getCdtarefaorcamento(), to.getIdentificador());
			mapPxId.put(to.getCdtarefaorcamento(), 1);
		}
		
		mapId = null;
		mapPxId = null;
	}
	
	/**
	 * Calcula o valor de venda das tarefa.
	 * 
	 * fator = Valor final do BDI / Valor total das tarefas;
	 * valor de venda = valor de custo * fator;
	 * 
	 * @see br.com.linkcom.sined.geral.service.BdiService#getByOrcamentoFlex
	 * @param listaTarefa
	 * @param totalCusto
	 * @param orcamento
	 * @author Rodrigo Freitas
	 */
	private void calculoValorVenda(List<Tarefaorcamento> listaTarefa, Double totalCusto, Orcamento orcamento) {
		BdiForm bdi = bdiService.getByOrcamentoFlex(orcamento);
		
		Double fator = totalCusto > 0 ? bdi.getPrecoComFolga().getValue().doubleValue() / totalCusto : 1;
		
		for (Tarefaorcamento to : listaTarefa) {
			to.setPrecovenda(new Money(to.getPrecocusto().getValue().doubleValue() * fator));
		}
	}
	
	/**
	 * Calcula o total do pre�o total ou do pre�o de venda
	 * 
	 * true - Valor de custo
	 * false - Valor de venda
	 *
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Double calculaTotal(List<Tarefaorcamento> lista, int comportamento) {
		
		Double total = 0.0;
		
		for (Tarefaorcamento tarefaorcamento : lista) {
			if(tarefaorcamento.getTarefapai() == null || tarefaorcamento.getTarefapai().getCdtarefaorcamento() == null || tarefaorcamento.getTarefapai().getCdtarefaorcamento().equals(0)){
				switch (comportamento) {
					case 1:
						if(tarefaorcamento.getPrecomat() != null) total += tarefaorcamento.getPrecomat().getValue().doubleValue();
						break;
					case 2:
						if(tarefaorcamento.getPrecomo() != null) total += tarefaorcamento.getPrecomo().getValue().doubleValue();
						break;
					case 3:
						if(tarefaorcamento.getPrecocusto() != null) total += tarefaorcamento.getPrecocusto().getValue().doubleValue();
						break;
					case 4:
						if(tarefaorcamento.getPrecovenda() != null) total += tarefaorcamento.getPrecovenda().getValue().doubleValue();
						break;
					case 5:
						if(tarefaorcamento.getPrecoacrescido() != null) total += tarefaorcamento.getPrecoacrescido().getValue().doubleValue();
						break;
				}
			}
		}

		return total;
	}
	
	
	/**
	 * Organiza o data grid, mudando os valores totais das tarefas pais e a descri��o das tarefas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TarefaorcamentoService#getValoresFilhos
	 * @param listaTarefa
	 * @author Rodrigo Freitas
	 */
	private void organizaListaTarefa(List<Tarefaorcamento> listaTarefa) {
		
		Tarefaorcamento to;
		
		for (int i = 0; i < listaTarefa.size(); i++) {
			to = listaTarefa.get(i);
			if(to.getTarefapai() == null || to.getTarefapai().getCdtarefaorcamento() == null ||  to.getTarefapai().getCdtarefaorcamento().equals(0)){
				to.setPrecomat(getValoresFilhosMAT(to, listaTarefa));
				to.setPrecomo(getValoresFilhosMO(to, listaTarefa));
				to.setPrecoacrescido(getValoresFilhosAcrescido(to, listaTarefa));
				to.setPrecocusto(getValoresFilhos(to,listaTarefa,1));
			}
		}
	}
	
	private Money getValoresFilhosAcrescido(Tarefaorcamento to, List<Tarefaorcamento> listaTarefa) {
		
		Money total = new Money();
		Boolean filhos = false;
		Tarefaorcamento tor;
		
		for (int i = 0; i < listaTarefa.size(); i++) {
			tor = listaTarefa.get(i);
			if(tor.getTarefapai().getCdtarefaorcamento().equals(to.getCdtarefaorcamento())){
				tor.setPrecoacrescido(getValoresFilhosAcrescido(tor, listaTarefa));
				total = total.add(tor.getPrecoacrescido());
				filhos = true;
			}
		}
		
		if(filhos)
			return total;
		else
			return to.getPrecoacrescido();
	}
	
	private Money getValoresFilhosMO(Tarefaorcamento to, List<Tarefaorcamento> listaTarefa) {
		
		Money total = new Money();
		Boolean filhos = false;
		Tarefaorcamento tor;
		
		for (int i = 0; i < listaTarefa.size(); i++) {
			tor = listaTarefa.get(i);
			if(tor.getTarefapai().getCdtarefaorcamento().equals(to.getCdtarefaorcamento())){
				tor.setPrecomo(getValoresFilhosMO(tor, listaTarefa));
				total = total.add(tor.getPrecomo());
				filhos = true;
			}
		}
		
		if(filhos)
			return total;
		else
			return to.getPrecomo();
	}
	
	private Money getValoresFilhosMAT(Tarefaorcamento to, List<Tarefaorcamento> listaTarefa) {
		
		Money total = new Money();
		Boolean filhos = false;
		Tarefaorcamento tor;
		
		for (int i = 0; i < listaTarefa.size(); i++) {
			tor = listaTarefa.get(i);
			if(tor.getTarefapai().getCdtarefaorcamento().equals(to.getCdtarefaorcamento())){
				tor.setPrecomat(getValoresFilhosMAT(tor, listaTarefa));
				total = total.add(tor.getPrecomat());
				filhos = true;
			}
		}
		
		if(filhos)
			return total;
		else
			return to.getPrecomat();
	}
	
	/**
	 * Soma os valores dos filhos e retorna o total.
	 *
	 * @param to
	 * @param listaTarefa
	 * @param numEspaco
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Money getValoresFilhos(Tarefaorcamento to, List<Tarefaorcamento> listaTarefa, int numEspaco) {
		
		Money total = new Money();
		Boolean filhos = false;
		Tarefaorcamento tor;
		
		for (int i = 0; i < listaTarefa.size(); i++) {
			tor = listaTarefa.get(i);
			if(tor.getTarefapai().getCdtarefaorcamento().equals(to.getCdtarefaorcamento())){
//				tor.setDescricao(StringUtils.geraStringVazia(numEspaco * 5) + tor.getDescricao());
				tor.setPrecocusto(getValoresFilhos(tor, listaTarefa,  numEspaco+1));
				tor.setEspacoVazio(br.com.linkcom.utils.StringUtils.geraStringVazia(numEspaco * 5));
				total = total.add(tor.getPrecocusto());
				filhos = true;
			}
		}
		
		if(filhos)
			return total;
		else
			return to.getPrecocusto();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO#findForReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Tarefaorcamento> findForReport(TarefaOrcamentoReportFiltro filtro) {
		return tarefaorcamentoDAO.findForReport(filtro);
	}
	
	public List<RecursosBean> findForReportRecursos(Tarefaorcamento to) {
		return tarefaorcamentoDAO.findForReportRecursos(to);
	}
	
	
	public void putFiltroSessionRelatorioCsv(TarefaOrcamentoReportFiltro filtro){
		this.putFiltroSessionRelatorio(filtro);
	}

	public void putFiltroSessionRelatorioPdf(TarefaOrcamentoReportFiltro filtro){
		this.putFiltroSessionRelatorio(filtro);
	}
	
	/**
	 * Coloca o filtro na sess�o para a gera��o do relat�rio.
	 *
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void putFiltroSessionRelatorio(TarefaOrcamentoReportFiltro filtro){
		NeoWeb.getRequestContext().getSession().setAttribute(SESSION_FILTRO_RELATORIO_TAREFA, filtro);
	}
	
	
	

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO#findByOrcamento
	 * @param orcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Tarefaorcamento> findByOrcamento(Orcamento orcamento) {
		return tarefaorcamentoDAO.findByOrcamento(orcamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param orcamento
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean orcamentoPossuiTarefa(Orcamento orcamento) {
		return tarefaorcamentoDAO.orcamentoPossuiTarefa(orcamento);
	}
	
	public Double getTotalRecursosGeraisDireto(Orcamento orcamento) {
		return tarefaorcamentoDAO.getTotalRecursosGeraisDireto(orcamento);
	}
	public Double getTotalOutrasTarefas(Orcamento orcamento) {
		return tarefaorcamentoDAO.getTotalOutrasTarefas(orcamento);
	}
	
	public Double calculaTotalTarefa(Orcamento orcamento, List<Tarefaorcamentorh> listaRH, List<Tarefaorcamentorg> listaRG){
		Double total = 0d;
		
		List<Recursocomposicao> listaRecursoGeral = recursocomposicaoService.findRecursoDiretoByOrcamento(orcamento);
		List<Orcamentorecursohumano> listaRecursoHumano = orcamentorecursohumanoService.findByCargoOrcamento(orcamento);
		List<Composicaomaoobra> listaComposicaomaoobra = composicaomaoobraService.findAllForFlex();
		
		String paramPrecocustoProjeto = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.PRECOCUSTO_MATERIAL_PROJETO);
		
		Double valor;
		boolean find;
		
		for (Tarefaorcamentorg trg : listaRG) {
			if(trg.getMaterial() != null){
				valor = 0d;
				find = false;
				for (Recursocomposicao rc : listaRecursoGeral) {
					if(rc.getMaterial() != null && rc.getMaterial().equals(trg.getMaterial()) && rc.getCustounitario() != null && rc.getCustounitario().getValue().doubleValue() > 0){
						find = true;
						valor = rc.getCustounitario().getValue().doubleValue();
						break;
					}
				}
				
				if(!find) {
					valor = trg.getMaterial().getValorcusto() != null ? trg.getMaterial().getValorcusto() : 0d;
					if(paramPrecocustoProjeto != null && paramPrecocustoProjeto.trim().toUpperCase().equals("FALSE")){
						valor = trg.getMaterial().getValorvenda() != null ? trg.getMaterial().getValorvenda() : 0d;
					}
				}
				
				total += valor * trg.getQuantidade();
			} else {
				valor = 0d;
				find = false;
				for (Recursocomposicao rc : listaRecursoGeral) {
					if(rc.getNome() != null && trg.getOutro() != null && rc.getNome().trim().toUpperCase().equals(trg.getOutro().trim().toUpperCase()) && rc.getCustounitario() != null && rc.getCustounitario().getValue().doubleValue() > 0){
						find = true;
						valor = rc.getCustounitario().getValue().doubleValue();
						break;
					}
				}
				
				total += valor * trg.getQuantidade();
			}
		}
		
		for (Tarefaorcamentorh trh : listaRH) {
			if(trh.getCargo() != null){
				valor = 0d;
				find = false;
				for (Orcamentorecursohumano orh : listaRecursoHumano) {
					if(orh.getCargo() != null && orh.getCargo().equals(trh.getCargo()) && orh.getCustohora() != null && orh.getCustohora().getValue().doubleValue() > 0){
						if(orh.getFatormdo() != null && orh.getFatormdo().getTotal() != null){
							valor = orh.getCustohora().getValue().doubleValue() * orh.getFatormdo().getTotal();
						} else {
							valor = orh.getCustohora().getValue().doubleValue();
						}
						find = true;
						break;
					}
				}
				
				if(!find){
					for (Composicaomaoobra composicaomaoobra : listaComposicaomaoobra) {
						if(composicaomaoobra.getCargo() != null && composicaomaoobra.getCargo().equals(trh.getCargo()) && composicaomaoobra.getCustoporhora() != null && composicaomaoobra.getCustoporhora().getValue().doubleValue() > 0){
							valor = composicaomaoobra.getCustoporhora().getValue().doubleValue();
							find = true;
							break;
						}
					}
				}
				
				if(!find && trh.getCargo().getCustohora() != null) {
					valor = trh.getCargo().getCustohora().getValue().doubleValue();
				}
				
				total += valor * trh.getQuantidade();
			}
		}
		
		return total;
	}
	
	public List<Tarefaorcamento> recalcularTarefas(Orcamento orcamento){
		List<Tarefaorcamento> listaTarefa = this.getListaTarefaFlex(orcamento);
		
		if(listaTarefa != null && 
				listaTarefa.size() > 0 && 
				listaTarefa.get(0) != null && 
				listaTarefa.get(0).getListaTarefaFilha() != null && 
				listaTarefa.get(0).getListaTarefaFilha().size() > 0){
			calculaValorTarefa(listaTarefa.get(0).getListaTarefaFilha(), orcamento);
		}
		
		return this.sincronizaListaFlex(listaTarefa, false, orcamento, true);
	}
	
	private Double calculaValorTarefa(List<Tarefaorcamento> listaTarefaFilha, Orcamento orcamento) {
		Double valor = 0d;
		
		for (Tarefaorcamento tarefaorcamento : listaTarefaFilha) {
			if(tarefaorcamento.getListaTarefaFilha() != null && tarefaorcamento.getListaTarefaFilha().size() > 0){
				tarefaorcamento.setCustototal(calculaValorTarefa(tarefaorcamento.getListaTarefaFilha(), orcamento));
				tarefaorcamento.setPercentualacrescimo(null);
				tarefaorcamento.setCustoacrescido(null);
			} else {
				tarefaorcamento.setCustototal(calculaTarefa(tarefaorcamento, orcamento));
				if(tarefaorcamento.getCustototal() != null && tarefaorcamento.getCustototal() > 0){
					double custo = tarefaorcamento.getCustototal();
					if(tarefaorcamento.getPercentualacrescimo() != null && tarefaorcamento.getPercentualacrescimo() > 0){
						custo = custo * ((tarefaorcamento.getPercentualacrescimo()/100d) + 1d);
					}
					tarefaorcamento.setCustoacrescido(custo);
				} else tarefaorcamento.setCustoacrescido(null);
			}
			
			valor += (tarefaorcamento.getCustototal() != null ? tarefaorcamento.getCustototal() : 0d);
		}
		
		return valor;
	}
	
	private Double calculaTarefa(Tarefaorcamento tarefaorcamento, Orcamento orcamento) {
		Double custototal = tarefaorcamento.getCustototal();
		
		if((tarefaorcamento.getListaTarefaorcamentorg() != null && tarefaorcamento.getListaTarefaorcamentorg().size() > 0) || (tarefaorcamento.getListaTarefaorcamentorh() != null && tarefaorcamento.getListaTarefaorcamentorh().size() > 0)){
			custototal = calculaTotalTarefa(orcamento, tarefaorcamento.getListaTarefaorcamentorh(), tarefaorcamento.getListaTarefaorcamentorg());
		}
		
		return custototal;
	}
	
	public void putFiltroSessionRelatorioTarefaorcamentodetalhadoCsv(TarefaorcamentodetalhadoCSVFiltro filtro){
		NeoWeb.getRequestContext().getSession().setAttribute(SESSION_FILTRO_RELATORIO_ORCAMENTO_DETALHADO, filtro);
	}
	
	public Resource preparaArquivoTarefaorcamentodetalhadoCSV(TarefaorcamentodetalhadoCSVFiltro filtro) {
		if(filtro == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		
		TarefaOrcamentoReportFiltro _filtro = new TarefaOrcamentoReportFiltro();
		_filtro.setOrcamento(filtro.getOrcamento());
		_filtro.setIncluirrg(Boolean.TRUE);
		_filtro.setIncluirrh(Boolean.TRUE);
		
		List<Tarefaorcamento> listaTarefa = this.findForReport(_filtro);
		
		this.organizaListaTarefa(listaTarefa);
		
		Tarefaorcamento tarefa;
		for (Tarefaorcamento tarefaorcamento : listaTarefa) {
			tarefa = new Tarefaorcamento(tarefaorcamento.getCdtarefaorcamento());
			tarefa = this.loadTarefa(tarefa);
			tarefaorcamento.setQtde(tarefa != null ? tarefa.getQtde() : 0);
		}
		
		this.incluiIdentificador(listaTarefa);
		
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		
		StringBuilder csv = new StringBuilder();
		Double custounitario;
		
		csv.append("Id;Tarefas;Pre�o Custo;Qtd.;Pre�o Total; U.M.\n");
		for (Tarefaorcamento to : listaTarefa) {
			csv.append(to.getIdentificador()).append(";");
			csv.append(to.getDescricao()).append(";");
			
			try{
				custounitario = to.getPrecocusto().getValue().doubleValue() / to.getQtde();
			} catch (Exception e) {
				custounitario = null;
			}
			if(custounitario != null && to.getQtde() > 0)
				csv.append(decimalFormat.format(custounitario));
			csv.append(";");
			
			if(to.getQtde() > 0)
				csv.append(to.getQtde());
			csv.append(";");
			
			csv.append(to.getPrecocusto().toString()).append(";");
			
			csv.append("\n");
			
			if(filtro.getExibirrecursosdiretos()){
				List<RecursosBean> lista = this.findForReportRecursos(to);
				this.putRecursosListagem(decimalFormat, csv, lista);
			}
		}
		
		if(filtro.getExibirrecursosdiretos()){
			Double totalCusto = this.calculaTotal(listaTarefa, 3);
			double valoracrescimo = this.getTotalAcrescimo(filtro.getOrcamento());
			
			csv.append("TOTAL;;;;").append(decimalFormat.format(totalCusto)).append("\n");
			csv.append("ACR�SCIMO;;;;").append(decimalFormat.format(valoracrescimo)).append("\n");
			csv.append("TOTAL DIRETO;;;;").append(decimalFormat.format(totalCusto + valoracrescimo)).append("\n");
		}
		
		if(filtro.getExibirrecursosindiretos()){
			csv.append("RECURSOS INDIRETOS\n");
			
			List<RecursosBean> listaInd = new ArrayList<RecursosBean>();
			RecursosBean beanInd;
			
			OrcamentoRecursoGeralFiltro geralFiltro = new OrcamentoRecursoGeralFiltro();
			geralFiltro.setOrcamento(filtro.getOrcamento());
			geralFiltro.setRecursoDireto(Boolean.FALSE);
			geralFiltro.setRecursoComposicao(Boolean.TRUE);
			geralFiltro.setComposicaoOrcamento(null);
			geralFiltro.setNomeRecurso(null);
			
			List<Recursocomposicao> listaRGInd = recursocomposicaoService.findForListagemFlex(geralFiltro);
			for (Recursocomposicao rc : listaRGInd) {
				beanInd = new RecursosBean();
				
				beanInd.setNome(rc.getNome());
				beanInd.setUnidademedida(rc.getUnidademedida().getSimbolo());
				beanInd.setQtde(rc.getQuantidadecalculada() * rc.getNumocorrencia());
				beanInd.setValorunitario(rc.getCustounitario().getValue().doubleValue());
				beanInd.setValortotal(rc.getQuantidadecalculada() * rc.getNumocorrencia() * rc.getCustounitario().getValue().doubleValue());
				
				listaInd.add(beanInd);
			}
			
			OrcamentoRecursoHumanoFiltro humanoFiltro = new OrcamentoRecursoHumanoFiltro();
			humanoFiltro.setOrcamento(filtro.getOrcamento());
			humanoFiltro.setTipoCargo(Tipocargo.MOI);
			
			Double unitario;
			List<Orcamentorecursohumano> listaRHInd = orcamentorecursohumanoService.findForListagemFlex(humanoFiltro);
			for (Orcamentorecursohumano orh : listaRHInd) {
				beanInd = new RecursosBean();
				
				beanInd.setNome(orh.getCargo().getNome());
				beanInd.setUnidademedida("H/H");
				beanInd.setQtde(orh.getTotalhoras());
				
				unitario = orh.getCustohora().getValue().doubleValue() * (orh.getFatormdo() != null && orh.getFatormdo().getTotal() != null ? orh.getFatormdo().getTotal() : 1);
				beanInd.setValorunitario(unitario);
				beanInd.setValortotal(unitario * orh.getTotalhoras());
				
				listaInd.add(beanInd);
			}
			
			
			Collections.sort(listaInd,new Comparator<RecursosBean>(){
				public int compare(RecursosBean o1, RecursosBean o2) {
					return o1.getNome().compareTo(o2.getNome());
				}
			});
			
			this.putRecursosListagem(decimalFormat, csv, listaInd);
			
			Double totalInd = 0d;
			for (RecursosBean b : listaInd) {
				totalInd += b.getValortotal();
			}
			
			csv.append("TOTAL INDIRETO;;;;").append(decimalFormat.format(totalInd));
		}
		
		return new Resource("text/csv", "tarefaorcamentodetalhado_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
	}
	
	private void putRecursosListagem(DecimalFormat decimalFormat, StringBuilder csv, List<RecursosBean> lista) {
		for (RecursosBean bean : lista) {
			csv.append(";");
			csv.append(bean.getNome()).append(";");
			csv.append(decimalFormat.format(bean.getValorunitario())).append(";");
			csv.append(decimalFormat.format(bean.getQtde())).append(";");
			csv.append(decimalFormat.format(bean.getValortotal())).append(";");
			csv.append(bean.getUnidademedida()!= null ? bean.getUnidademedida() : "" ).append(";");
			
			csv.append("\n");
		}
	}
	
	/**
	 * Prepara o arquivo CSV de tarefa do or�amento
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Resource preparaArquivoTarefaOrcamentoCSV(TarefaOrcamentoReportFiltro filtro) {
		
		if(filtro == null){
			throw new SinedException("Erro na passagem de par�metro.");
		}
		
		boolean precocusto = filtro.getPrecocusto() != null && filtro.getPrecocusto();
		boolean precovenda = filtro.getPrecovenda() != null && filtro.getPrecovenda();
		
		List<Tarefaorcamento> listaTarefa = this.findForReport(filtro);
		
		this.organizaListaTarefa(listaTarefa);
		
		Double totalCusto = this.calculaTotal(listaTarefa, 3);
		Double totalMat = this.calculaTotal(listaTarefa, 1);
		Double totalMo = this.calculaTotal(listaTarefa, 2);
		Double totalAcrescido = this.calculaTotal(listaTarefa, 5);
		
		this.calculoValorVenda(listaTarefa, totalCusto, filtro.getOrcamento());
		
		Double totalVenda = this.calculaTotal(listaTarefa, 4);
		
		Tarefaorcamento tarefa;
		for (Tarefaorcamento tarefaorcamento : listaTarefa) {
			tarefa = new Tarefaorcamento(tarefaorcamento.getCdtarefaorcamento());
			tarefa = this.loadTarefa(tarefa);
			tarefaorcamento.setQtde(tarefa != null ? tarefa.getQtde() : 0);
		}
		
		this.incluiIdentificador(listaTarefa);
		
		DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
		 
		String acrescidoStr = decimalFormat.format(totalAcrescido);
		String custoStr = decimalFormat.format(totalCusto);
		String vendaStr = decimalFormat.format(totalVenda);
		String customat = decimalFormat.format(totalMat);
		String customo = decimalFormat.format(totalMo);
		
		StringBuilder csv = new StringBuilder();
		csv.append("Identificador;Tarefa");
		
		if(precocusto || precovenda) csv.append(";Qtde.");
		if(precovenda) csv.append(";Valor de venda");
		if(precocusto) csv.append(";Valor de materiais;Valor de m�o-de-obra;Valor de custo;Valor total com acr�scimo"); 
		
		csv.append("\n"); 
		for (Tarefaorcamento to : listaTarefa) {
			csv.append(to.getIdentificador());
			csv.append(";");
			csv.append(to.getDescricao().replaceAll(";", ","));
			
			if(precocusto || precovenda){
				csv.append(";");
				csv.append(decimalFormat.format(to.getQtde()));
			}
			if(precovenda){
				csv.append(";");
				csv.append(to.getPrecovenda().toString());
			}
			if(precocusto){
				csv.append(";");
				csv.append(to.getPrecomat().toString());
				csv.append(";");
				csv.append(to.getPrecomo().toString());
				csv.append(";");
				csv.append(to.getPrecocusto().toString());
				csv.append(";");
				csv.append(to.getPrecoacrescido().toString());
			}
			csv.append("\n");
		}
		
		if(precovenda && precocusto){
			csv.append(";;;");
			csv.append(vendaStr);
			csv.append(";");
			csv.append(customat);
			csv.append(";");
			csv.append(customo);
			csv.append(";");
			csv.append(custoStr);
			csv.append(";");
			csv.append(acrescidoStr);
		} else if(precovenda){
			csv.append(";;;");
			csv.append(vendaStr);
		} else if(precocusto){
			csv.append(";;;");
			csv.append(customat);
			csv.append(";");
			csv.append(customo);
			csv.append(";");
			csv.append(custoStr);
			csv.append(";");
			csv.append(acrescidoStr);
		}
		
		return new Resource("text/csv", "tarefaorcamento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
	}
	
	public List<Tarefaorcamento> findByOrcamentoSemRecursos(Orcamento orcamento) {
		return tarefaorcamentoDAO.findByOrcamentoSemRecursos(orcamento);
	}
	
	public Double getTotalAcrescimo(Orcamento orcamento) {
		return tarefaorcamentoDAO.getTotalAcrescimo(orcamento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TarefaorcamentoDAO#atualizarPrazoOrcamento(Orcamento orcamento)
	*
	* @param orcamento
	* @since 06/03/2015
	* @author Luiz Fernando
	*/
	public void atualizarPrazoOrcamento(Orcamento orcamento) {
		tarefaorcamentoDAO.atualizarPrazoOrcamento(orcamento);
	}
}
