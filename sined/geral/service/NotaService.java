package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Cumulativotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoNotafiscalservicoReport;
import br.com.linkcom.sined.geral.bean.enumeration.TipoEnvioNFCliente;
import br.com.linkcom.sined.geral.dao.NotaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailNotaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailPessoaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EnvioEmailResultadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GeraReceita;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NumeroNfeNaoUtilizadoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImpostoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EvolucaoFaturamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EvolucaoFaturamentoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConsultarSituacaoNotaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarSituacaoNotaWSBean;
import br.com.linkcom.sined.util.Grafico;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaService extends GenericService<Nota> {
	
	private NotaDAO notaDAO;
	private NotafiscalprodutoService notafiscalprodutoService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaHistoricoService notaHistoricoService;
	private DocumentoService documentoService;
	private ContareceberService contareceberService;
	private NotaDocumentoService notaDocumentoService;
	private ContratoService contratoService;
	private DocumentocomissaoService documentocomissaoService;
	private VendaService vendaService;
	private ArquivonfnotaService arquivonfnotaService;
	private ArquivonfService arquivonfService;
	private NotaContratoService notaContratoService;
	private ParametrogeralService parametrogeralService;
	private ContratofaturalocacaoService contratofaturalocacaoService;
	private EnderecoService enderecoService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private PrazopagamentoService prazopagamentoService;
	private NotafiscalservicoduplicataService notafiscalservicoduplicataService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setNotaContratoService(NotaContratoService notaContratoService) {this.notaContratoService = notaContratoService;}	
	public void setArquivonfService(ArquivonfService arquivonfService) {this.arquivonfService = arquivonfService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setNotaDAO(NotaDAO notaDAO) {this.notaDAO = notaDAO;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {this.notaHistoricoService = notaHistoricoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setContratofaturalocacaoService(ContratofaturalocacaoService contratofaturalocacaoService) {this.contratofaturalocacaoService = contratofaturalocacaoService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setNotafiscalprodutoduplicataService(NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;}
	public void setNotafiscalservicoduplicataService(NotafiscalservicoduplicataService notafiscalservicoduplicataService) {this.notafiscalservicoduplicataService = notafiscalservicoduplicataService;}
	
	@Override
	public ListagemResult<Nota> findForListagem(FiltroListagem filtro) {
		ListagemResult<Nota> listagemResult = super.findForListagem(filtro);
		List<Nota> lista = listagemResult.list();
		
		lista = this.carregarInfoParaCalcularTotal(lista);
		listagemResult.list().clear();
		listagemResult.list().addAll(lista);
		
		return listagemResult;
	}
	
	/**
	 * <p>Recebe uma lista de notas e carrega as informa��es necess�rias para o
	 * c�lculo do valor total de cada uma.
	 * <p>Gra�as � heran�a, n�o � poss�vel pegar, a partir da nota, estes dados.
	 * Portanto � necess�rio passar por este m�todo para peg�-los.
	 * 
	 * @see #separarNotaPorTipo(List)
	 * @see br.com.linkcom.sined.geral.service.NotafiscalprodutoService#carregarInfoParaCalcularTotal(List)
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#carregarInfoParaCalcularTotal(List)
	 * @see #merge(List, Map)
	 * 
	 * @param lista
	 * 
	 * @return a mesma lista, com mais atributos carregados.
	 * 
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarInfoParaCalcularTotal(List<Nota> lista) {
		Map<NotaTipo, List<Nota>> map = this.separarNotaPorTipo(lista);
		List<Nota> listaNfProduto = map.get(NotaTipo.NOTA_FISCAL_PRODUTO);
		List<Nota> listaNfServico = map.get(NotaTipo.NOTA_FISCAL_SERVICO);
		
		listaNfProduto = notafiscalprodutoService.carregarInfoParaCalcularTotal(listaNfProduto);
		listaNfServico = notaFiscalServicoService.carregarInfoParaCalcularTotal(listaNfServico);
		
		map.put(NotaTipo.NOTA_FISCAL_PRODUTO, listaNfProduto);
		map.put(NotaTipo.NOTA_FISCAL_SERVICO, listaNfServico);
		
		lista = this.merge(lista, map);
		return lista;
	}
	
	
	/**
	 * Recebe uma nota e carrega as informa��es necess�rias para o
	 * c�lculo do seu valor total.
	 * 
	 * @see #carregarInfoParaCalcularTotal(List)

	 * @param nota
	 * @return a mesma nota, com mais atributos carregados.
	 * 
	 * @author Hugo Ferreira
	 */
	public Nota carregarInfoParaCalculartotal(Nota nota) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("O par�metro da fun��o carregarInfoParaCalcularTotal n�o pode ser nulo.");
		}
		
		List<Nota> lista = new ArrayList<Nota>(1);
		lista.add(nota);
		lista = this.carregarInfoParaCalcularTotal(lista);
		
		if (lista == null || lista.isEmpty()) {
			throw new SinedException("A nota " + nota.getCdNota() + " n�o existe.");
		}
		
		return lista.get(0);
	}

	/**
	 * Separa as notas pelo tipo.
	 * 
	 * @param lista
	 * @return um Map que cont�m como chave um NotaTipo e como valor uma
	 * 			lista de Nota, do tipo correspondente ao NotaTipo.
	 * 
	 * @author Hugo Ferreira
	 */
	public Map<NotaTipo, List<Nota>> separarNotaPorTipo(List<Nota> lista) {
		List<Nota> listaNfProduto = new ArrayList<Nota>();
		List<Nota> listaNfServico = new ArrayList<Nota>();
		for (Nota nota : lista) {
			if (nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)) {
				listaNfProduto.add(nota);
			} else if (nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)) {
				listaNfServico.add(nota);
			}
		}
		
		Map<NotaTipo, List<Nota>> map = new HashMap<NotaTipo, List<Nota>>();
		map.put(NotaTipo.NOTA_FISCAL_PRODUTO, listaNfProduto);
		map.put(NotaTipo.NOTA_FISCAL_SERVICO, listaNfServico);
		
		return map;
	}
	
	/**
	 * Cria uma nova lista contendo os objetos das listas obtidas de dentro do 
	 * "map" na mesma ordem dos objetos da "lista".
	 * 
	 * @param lista
	 * @param mapListas
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Nota> merge(List<Nota> lista, Map<NotaTipo, List<Nota>> mapListas) {
		List<Nota> listaNfProduto = mapListas.get(NotaTipo.NOTA_FISCAL_PRODUTO);
		List<Nota> listaNfServico = mapListas.get(NotaTipo.NOTA_FISCAL_SERVICO);
		List<Nota> listaResultado = new ArrayList<Nota>();
		Nota notaAux = null;
		
		for (Nota nota : lista) {
			if (nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)) {
				int index = listaNfProduto.indexOf(nota);
				if (index >= 0) {
					notaAux = new Notafiscalproduto(nota);
					notaAux.copiarItensDeValor(listaNfProduto.get(index));

					Timestamp dtsaida = ((Notafiscalproduto) listaNfProduto.get(index)).getDatahorasaidaentrada();
					notaAux.setDataSaida(dtsaida != null ? new Date(dtsaida.getTime()) : null);
					
					listaResultado.add(notaAux);
				}
			} else if (nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)) {
				int index = listaNfServico.indexOf(nota);
				if (index >= 0) {
					notaAux = new NotaFiscalServico(nota);
					notaAux.copiarItensDeValor(listaNfServico.get(index));
					listaResultado.add(notaAux);
				}
			}
		}
		
		return listaResultado;
	}
	
	/**
	 * M�todo que gera o relat�rio resumo de faturamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaService#buscarParaRelatorioResumoFaturamento(NotaFiltro)
	 * @see #carregarInfoParaCalcularTotal(List)
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 * @author Hugo Ferreira
	 */
	public Report gerarRelarorioResumoFaturamento(NotaFiltro filtro) {
		Report report = new Report("/faturamento/resumoFaturamento");
		
		if(filtro.getListaNotaTipo().contains(NotaTipo.NOTA_FISCAL_SERVICO)){
			report = new Report("/faturamento/resumoFaturamentoServico");
		}
		
//		List<Nota> lista = this.buscarParaRelatorioResumoFaturamento(filtro);
//		lista = this.carregarInfoParaCalcularTotal(lista);
		List<Nota> lista = this.carregarInfoParaCalcularTotalNew(filtro);
		
		if(filtro.getListaNotaTipo().contains(NotaTipo.NOTA_FISCAL_SERVICO)){
			this.preencheNumeroNfse(lista);
			this.preencheObservacaoCancelamento(lista);

			if(filtro.getOrdenacaoNotafiscalservicoReport() != null){
				if(filtro.getOrdenacaoNotafiscalservicoReport().equals(OrdenacaoNotafiscalservicoReport.NUMERO_NOTA)){
					Collections.sort(lista, new Comparator<Nota>(){
						public int compare(Nota o1, Nota o2) {
							if(o1.getNumero() == null || o1.getNumero().trim().equals("")) return 1;
							if(o2.getNumero() == null || o2.getNumero().trim().equals("")) return -1;
							Long o1Num = Long.parseLong(o1.getNumero());
							Long o2Num = Long.parseLong(o2.getNumero());
							
							return o2Num.compareTo(o1Num);
						}
					});
				} else if(filtro.getOrdenacaoNotafiscalservicoReport().equals(OrdenacaoNotafiscalservicoReport.NUMERO_NFSE)){
					Collections.sort(lista, new Comparator<Nota>(){
						public int compare(Nota o1, Nota o2) {
							if(o1.getNumeroNfseOrder() == null) return 1;
							if(o2.getNumeroNfseOrder() == null) return -1;
							return o2.getNumeroNfseOrder().compareTo(o1.getNumeroNfseOrder());
						}
					});
				}
			}
		} else {
			Collections.sort(lista, new Comparator<Nota>(){
				public int compare(Nota o1, Nota o2) {
					if(o1.getNumero() == null || o1.getNumero().trim().equals("")) return 1;
					if(o2.getNumero() == null || o2.getNumero().trim().equals("")) return -1;
					Long o1Num = Long.parseLong(o1.getNumero());
					Long o2Num = Long.parseLong(o2.getNumero());
					
					return o2Num.compareTo(o1Num);
				}
			});
		}
		this.putTotalizadores(report, lista);
        report.setDataSource(lista);
        
		return report;
	}
	
	/**
	 * M�todo que preeche a observa��o do cancelamento
	 *
	 * @param lista
	 * @author Rodrigo Freitas
	 * @since 01/07/2014
	 */
	private void preencheObservacaoCancelamento(List<Nota> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			List<NotaHistorico> listaNotaHistorico = notaHistoricoService.findForResumo(CollectionsUtil.listAndConcatenate(lista, "cdNota", ","));
			if(listaNotaHistorico != null && !listaNotaHistorico.isEmpty() && lista != null && !lista.isEmpty()){
				for(Nota nota : lista){
					boolean achou = false;
					
					for(NotaHistorico notaHistorico : listaNotaHistorico){
						if(nota.getCdNota() != null &&
								notaHistorico.getNota() != null && 
								notaHistorico.getNotaStatus() != null && 
								notaHistorico.getObservacao() != null && 
								notaHistorico.getNotaStatus().equals(NotaStatus.NFSE_CANCELANDO) && 
								nota.getCdNota().equals(notaHistorico.getNota().getCdNota()) &&
								!"".equals(notaHistorico.getObservacao())){
							achou = true;
							nota.setObservacaoCancelamento("Obseva��o cancelamento: " + notaHistorico.getObservacao());
							break;
						}
					}
					
					if(!achou){
						for(NotaHistorico notaHistorico : listaNotaHistorico){
							if(nota.getCdNota() != null &&
									notaHistorico.getNota() != null && 
									notaHistorico.getNotaStatus() != null && 
									notaHistorico.getObservacao() != null && 
									notaHistorico.getNotaStatus().equals(NotaStatus.CANCELADA) && 
									nota.getCdNota().equals(notaHistorico.getNota().getCdNota()) &&
									!"".equals(notaHistorico.getObservacao())){
								nota.setObservacaoCancelamento("Obseva��o cancelamento: " + notaHistorico.getObservacao());
								break;
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Criado m�todo novo, pois o antigo estava dando erro pelo fato do whereIn ficar muito grande
	 * O c�digo anterior foi mantido com o mesmo nome do m�todo sem o "NEW"
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Nota> carregarInfoParaCalcularTotalNew(NotaFiltro filtro) {
		List<Nota> listaNova = new ArrayList<Nota>();
		List<Notafiscalproduto> listaNotaFiscalProduto = notafiscalprodutoService.carregarInfoParaCalcularTotalNew(filtro);
		List<NotaFiscalServico> listaNfServico = notaFiscalServicoService.carregarInfoParaCalcularTotalNew(filtro);
		
		if(listaNotaFiscalProduto != null && !listaNotaFiscalProduto.isEmpty())
			listaNova.addAll(listaNotaFiscalProduto);
		if(listaNfServico != null && !listaNfServico.isEmpty())
			listaNova.addAll(listaNfServico);
		return listaNova;
	}
	private void putTotalizadores(Report report, List<Nota> lista) {
		Money totalValorBruto = new Money();
		Money totalBaseCalculo = new Money();
		Money totalValorIss = new Money();
		Money totalOutros = new Money();
		Money totalLiquido = new Money();
		
		Money valorBruto;
		Money valorLiquido;
		
		for (Nota nota : lista) {
			valorBruto = nota.getValorBruto();
			valorLiquido = nota.getValorNota();
			if(valorBruto != null) totalValorBruto = totalValorBruto.add(valorBruto);
			if(valorLiquido != null) totalLiquido = totalLiquido.add(valorLiquido);
			if(nota instanceof NotaFiscalServico) {
				NotaFiscalServico nf = (NotaFiscalServico) nota;
				if(nf.getBasecalculoiss() != null)
					totalBaseCalculo = totalBaseCalculo.add(nf.getBasecalculoiss());
				else if(nf.getBasecalculo() != null)
					totalBaseCalculo = totalBaseCalculo.add(nf.getBasecalculo());
				else
					totalBaseCalculo = totalBaseCalculo.add(valorBruto);
			} else {
				totalBaseCalculo = totalBaseCalculo.add(valorBruto != null ? valorBruto : new Money());
			}
				
			Money valorIss = nota.getValoriss() != null ? nota.getValoriss() : new Money(0);
			if(nota.getIncideiss() != null && nota.getIncideiss()){
				totalValorIss = totalValorIss.add(valorIss);
			} else {
				totalOutros = totalOutros.add(valorIss);
			}
		}
		
		report.addParameter("TOTAL_VALORBRUTO", totalValorBruto.getValue().doubleValue());
		report.addParameter("TOTAL_BASECALCULO", totalBaseCalculo.getValue().doubleValue());
		report.addParameter("TOTAL_VALORISS", totalValorIss.getValue().doubleValue());
		report.addParameter("TOTAL_OUTROS", totalOutros.getValue().doubleValue());
		report.addParameter("TOTAL_VALORLIQUIDO", totalLiquido.getValue().doubleValue());
	}
	
	/**
	 * Recebe uma String contendo IDs de Notas e, para cada nota, executa o cancelamento.
	 * Retorna o n�mero de notas canceladas com sucesso.
	 * 
	 * @see #carregarItensParaAcao(String)
	 * @see br.com.linkcom.sined.geral.bean.state.nota.NotaState
	 * @see br.com.linkcom.sined.geral.bean.state.nota.Cancelada
	 * 
	 * @param idNotas : String contendo IDs de Notas
	 * @param observacao : justificativa a ser inserida no Hist�rico das Notas.
	 * @return : o n�mero de notas canceladas com sucesso
	 * 
	 * @author Hugo Ferreira
	 * @param request 
	 */
	public int cancelar(WebRequestContext request, String idNotas, String observacao, MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		List<Nota> listaNota = this.carregarItensParaAcao(idNotas);
		int contadorSucesso = 0;
		
		for (Nota nota : listaNota) {
			nota.setNotaStatus(NotaStatus.CANCELADA);
			if(this.alterarStatusAcao(nota, observacao, motivoCancelamentoFaturamento)){
				contadorSucesso++;
				if(request != null){
					notaDocumentoService.adicionaMensagensCancelamentoNotaWebserviceTrue(request, nota.getCdNota() + "", false);
				}
			}
		}
		return contadorSucesso;
	}
	
	/**
	 * Recebe uma String contendo IDs de Notas e, para cada nota, executa o estorno.
	 * Retorna o n�mero de notas canceladas com sucesso.
	 * 
	 * @see #carregarItensParaAcao(String)
	 * @see br.com.linkcom.sined.geral.bean.state.nota.NotaState
	 * @see br.com.linkcom.sined.geral.bean.state.nota.Emitida
	 * 
	 * @param idNotas : String contendo IDs de Notas
	 * @param observacao : justificativa a ser inserida no Hist�rico das Notas.
	 * @return : o n�mero de notas canceladas com sucesso

	 * @author Hugo Ferreira
	 */
	public int estornar(String idNotas, String observacao) {
		List<Nota> listaNota = this.carregarItensParaAcao(idNotas);
		int contadorSucesso = 0;
		
		NotaStatus estadoAnterior = null;
		for (Nota nota : listaNota) {
			estadoAnterior = nota.getNotaStatus();
			
			nota.setAcaoestornar(Boolean.TRUE);
			
			nota.setNotaStatus(NotaStatus.EMITIDA);
			if(estadoAnterior.equals(NotaStatus.CANCELADA)) {
				if(!vendaService.vendaSituacaoCancelada(nota) && this.alterarStatusAcao(nota, observacao, null))
					contadorSucesso++;
			} else if(estadoAnterior.equals(NotaStatus.NFSE_SOLICITADA) || estadoAnterior.equals(NotaStatus.NFE_SOLICITADA)) {
				if(this.alterarStatusAcao(nota, observacao, null)){
					arquivonfService.naoEnviarArquivo_NotaEstornada(nota, observacao);
					contadorSucesso++;
				}
			} else if(estadoAnterior.equals(NotaStatus.NFSE_CANCELANDO)){
				nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
				if(this.alterarStatusAcao(nota, observacao, null)){
					arquivonfnotaService.removerCancelamento(nota);
					contadorSucesso++;
				}
			} else if(estadoAnterior.equals(NotaStatus.NFE_CANCELANDO)){
				nota.setNotaStatus(NotaStatus.NFE_EMITIDA);
				if(this.alterarStatusAcao(nota, observacao, null)){
					arquivonfnotaService.removerCancelamento(nota);
					contadorSucesso++;
				}
			} else if(estadoAnterior.equals(NotaStatus.LIQUIDADA)){
				nota.setNotaStatus(NotaStatus.EMITIDA);
				if(this.alterarStatusAcao(nota, observacao, null)){
					contadorSucesso++;
				}
			} else if(estadoAnterior.equals(NotaStatus.NFE_LIQUIDADA)){
				nota.setNotaStatus(NotaStatus.NFE_EMITIDA);
				if(this.alterarStatusAcao(nota, observacao, null)){
					contadorSucesso++;
				}
			} else if(estadoAnterior.equals(NotaStatus.NFSE_LIQUIDADA)){
				nota.setNotaStatus(NotaStatus.NFSE_EMITIDA);
				if(this.alterarStatusAcao(nota, observacao, null)){
					contadorSucesso++;
				}
			}
			
		}
		return contadorSucesso;
	}
	
	/**
	 * M�todo que executa e manda persistir os dados de uma nota na a��o
	 * LIQUIDAR (Gerar Receita).
	 *
	 * @see #carregarItensParaAcao(String)
	 * @see br.com.linkcom.sined.geral.bean.state.nota.NotaState
	 * @see br.com.linkcom.sined.geral.bean.state.nota.Liquidada
	 * @see #salvarNotaLiquidada(Nota, Documento)
	 *
	 * @param cdNota : o ID de uma nota. Obrigat�rio.
	 * @param numeroNota : o n�mero da nota. Obrigat�rio.
	 * @param contaReceber : uma conta a receber gerada na receita. Pode haver parcelas. Obrigat�rio.
	 * 
	 * @return 1, se a a��o for executada com sucesso, ou 0, se a nota n�o puder ser liquidada.
	 * 
	 * @throws SinedException : se um dos par�metros obrigat�rios n�o forem passados;
	 *
	 * @author Hugo Ferreira
	 */
	public int liquidar(Integer cdNota, String numeroNota, Documento contaReceber) {
		if (cdNota == null) throw new SinedException("O par�metro cdNota da fun��o liquidar n�o foi informado.");
		if (contaReceber == null) throw new SinedException("O par�metro contaReceber da fun��o liquidar n�o foi informado.");
		
		List<Nota> lista = this.carregarItensParaAcao(cdNota.toString());
		if (lista == null || lista.isEmpty()) throw new SinedException("A nota de ID " + cdNota + " n�o p�de ser localizada.");
		
		Nota nota = lista.get(0);
		nota.setNumero(numeroNota);
		if (this.verificaLiquidar(nota)) {
			this.salvarNotaLiquidada(nota, contaReceber);
			return 1;
		}
		
		return 0;
	}
	
	public int liquidar(String whereIn, Documento contaReceber) {
		if (StringUtils.isBlank(whereIn)) throw new SinedException("O par�metro cdNota da fun��o liquidar n�o foi informado.");
		if (contaReceber == null) throw new SinedException("O par�metro contaReceber da fun��o liquidar n�o foi informado.");
		
		List<Nota> lista = this.carregarItensParaAcao(whereIn);
		if (lista == null || lista.isEmpty()) throw new SinedException("A nota de ID " + whereIn + " n�o p�de ser localizada.");
		
		for(Nota nota : lista){
			if (!this.verificaLiquidar(nota)) {
				return 0;
			}
		}
		
		this.salvarNotaLiquidada(lista, contaReceber);
		
		return 1;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#existOrigem(Nota nota)
	 *
	 * @param nota
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existOrigem(Nota nota) {
		return notaDAO.existOrigem(nota);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/07/2013
	 */
	public Boolean isOrigemVenda(String whereIn) {
		return notaDAO.isOrigemVenda(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#existOrigem(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existOrigem(String whereIn) {
		return notaDAO.existOrigem(whereIn);
	}
	
	public Boolean existOrigemVendaGeracaoContaAposEmissaoNota(String whereIn, boolean reserva) {
		return notaDAO.existOrigemVendaGeracaoContaAposEmissaoNota(whereIn, reserva);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#existOrigemForBaixaNota(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/11/2013
	 */
	public Boolean existOrigemForBaixaNota(String whereIn) {
		return notaDAO.existOrigemForBaixaNota(whereIn);
	}
	
//	/**
//	 * M�todo com refer�ncia no DAO
//	 * 
//	 * @param cdNota
//	 * @return
//	 * @author Tom�s Rabelo
//	 */
//	private Nota loadForReceita(Integer cdNota) {
//		return notaDAO.loadForReceita(cdNota);
//	}
	
	/**
	 * Verifica se h� comissionamento quando da gera��o de receita.
	 * @param contrato
	 * @param documento
	 * @author Taidson
	 * @since 11/11/2010
	 */
	public void verificaComissionamentoReceita(Contrato contrato, Documento documento){		
		Documentocomissao documentocomissao;
		Comissionamento vendedorComissionamento, colaboradorComissionamento;
		Pessoa vendedor;
		Colaborador colaborador;
		
		Contrato aux_contrato = contratoService.findForComissaoByContrato(contrato);
		
		if(aux_contrato.getVendedor() != null && aux_contrato.getComissionamento() != null){
			vendedor = aux_contrato.getVendedor();
			vendedorComissionamento = aux_contrato.getComissionamento();			
			
			if(vendedorComissionamento != null){
				documentocomissao = new Documentocomissao();
				
				documentocomissao.setDocumento(documento);				
				documentocomissao.setPessoa(vendedor);
				documentocomissao.setComissionamento(vendedorComissionamento);
				documentocomissao.setContrato(contrato);
				documentocomissao.setVendedor(true);
				
				documentocomissaoService.saveOrUpdate(documentocomissao);
			}
		}
		
		if(aux_contrato.getListaContratocolaborador() != null && aux_contrato.getListaContratocolaborador().size() > 0){
			for (Contratocolaborador cc : aux_contrato.getListaContratocolaborador()) {
				colaborador = cc.getColaborador();
				colaboradorComissionamento = cc.getComissionamento();				
				
				if(colaboradorComissionamento != null){
					documentocomissao = new Documentocomissao();
					
					documentocomissao.setDocumento(documento);					
					documentocomissao.setPessoa(colaborador);
					documentocomissao.setComissionamento(colaboradorComissionamento);
					documentocomissao.setContrato(contrato);
					documentocomissao.setVendedor(false);
					
					documentocomissaoService.saveOrUpdate(documentocomissao);
				}
			}
		}
		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#verificaLiquidar
	 * 
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean verificaLiquidar(Nota nota) {
		return notaDAO.verificaLiquidar(nota);
	}
	/**
	 * M�todo de refer�ncia ao DAO.<br>
	 * Persiste as altera��es de estado de uma nota GARANTINDO a TRANSA��O.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#alterarStatusAcao(Nota, NotaHistorico, boolean)
	 * 
	 * @param nota
	 * @param observacao
	 * @param cancelarDocumentos : informa se � para cancelar os documentos gerados pela
	 * 			nota. Note que n�o h� valida��o para cancelamento de documento. Este passo
	 * 			deve ser garantido antes de se invocar este m�todo
	 * 
	 * @author Hugo Ferreira
	 */
	public boolean alterarStatusAcao(Nota nota, String observacao, MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		return notaDAO.alterarStatusAcao(nota, observacao, motivoCancelamentoFaturamento);
	}
	
	/**
	 * Persiste as altera��es de estado de uma nota SEM GARANTIR a transa��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#cancelarSemTransacao(String, String)
	 * @see #atualizaNotaStatus(Nota, NotaStatus)
	 * @see br.com.linkcom.sined.geral.service.NotaHistoricoService#saveOrUpdateNoUseTransaction(NotaHistorico)
	 * 
	 * @param nota
	 * @param notaHistorico 
	 * @param cancelarDocumentos: informa se � para cancelar os documentos gerados pela
	 * 			nota. Note que n�o h� valida��o para cancelamento de documento. Este passo
	 * 			deve ser garantido antes de se invocar este m�todo
	 * 
	 * @author Hugo Ferreira
	 * @param observacao 
	 */
	public boolean alterarStatusAcaoSemTransacao(Nota nota, String observacao, MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		if (nota == null || nota.getCdNota() == null) {
			throw new SinedException("A Nota n�o pode ser nula.");
		}
		if (nota.getNotaStatus() == null || nota.getNotaStatus().getCdNotaStatus() == null) {
			throw new SinedException("O Status da nota n�o pode ser nulo.");
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		NotaStatus notaStatus = nota.getNotaStatus();
		if(nota.getAcaoestornar() != null && nota.getAcaoestornar()){
			notaStatus = NotaStatus.ESTORNADA;
		}
		NotaHistorico notaHistorico = new NotaHistorico(null, nota, notaStatus,
				observacao, motivoCancelamentoFaturamento, (usuarioLogado != null ? usuarioLogado.getCdpessoa() : null), new Timestamp(System.currentTimeMillis()));

//		if(nota.getListaNotaDocumento() != null){
//			List<Documento> listaDocumento = (List<Documento>) CollectionsUtil.getListProperty(nota.getListaNotaDocumento(), "documento");
//			
//			if (cancelarDocumentos && listaDocumento != null && listaDocumento.size() > 0) {
//				String strCdDocumento = CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ",");
//				
//				try {
//					documentoService.cancelarSemTransacao(strCdDocumento, notaHistorico.getObservacao());
//				} catch (AlterarEstadoException e) {
//					throw new SinedException(e.getMessage());
//				}
//			}
//		}

		this.atualizaNotaStatus(nota, nota.getNotaStatus());
		notaHistoricoService.saveOrUpdateNoUseTransaction(notaHistorico);

		return true;
	}
	
	/**
	 * Invoca o m�todo de persist�ncia para as informa��es inerentes � liquida��o
	 * de uma nota fiscal GARANTINDO a TRANSA��O.
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#salvarNotaLiquidada(Nota)
	 * 
	 * @param nota
	 * @param contaReceber
	 * 
	 * @return true, se todas as opera��es forem executadas com sucesso;
	 * 		   false, caso alguma opera��o falhe.
	 * 
	 * @author Hugo Ferreira
	 */
	public boolean salvarNotaLiquidada(Nota nota, Documento contaReceber) {
		List<Nota> lista = new ArrayList<Nota>();
		lista.add(nota);
		return notaDAO.salvarNotaLiquidada(lista, contaReceber);
	}
	
	public boolean salvarNotaLiquidada(List<Nota> lista, Documento contaReceber) {
		return notaDAO.salvarNotaLiquidada(lista, contaReceber);
	}
	
	/**
	 * Persiste as informa��es inerentes � liquida��o de uma nota fiscal
	 * SEM GARANTIR a TRANSA��O.
	 *
	 * @see 
	 *
	 * @param nota
	 * @param contaReceber
	 *  
	 * @return true, se todas as opera��es forem executadas com sucesso;
	 * 		   false, caso alguma opera��o falhe.
	 *  
	 * @author Hugo Ferreira
	 */
	public boolean salvarNotaLiquidadaSemTransacao(List<Nota> lista, Documento contaReceber) {
		boolean financiado = contaReceber.getFinanciamento() != null 
						&& contaReceber.getListaDocumento() != null 
						&& contaReceber.getListaDocumento().size() != 0;
		
		if (financiado) {
			contaReceber.setListaNotaTransient(lista);
			documentoService.salvaDocumentoFinanciadoSemTransacao(contaReceber);
			for(Nota nota : lista){
				nota.setListaNotaDocumento(this.montarListaNotaDocumentoFinanciado(nota, contaReceber));
			}
			if (contaReceber.getContrato()!=null && contaReceber.getContrato().getCdcontrato()!=null){
				contratoService.updateReajuste(contaReceber.getContrato(), Boolean.FALSE, null);
			}
		} else {
			StringBuilder observacaoNota = new StringBuilder(); 
			for(Nota nota : lista){
				observacaoNota.append(SinedUtil.makeLinkNota(nota)).append(",");
			}
			contaReceber.setObservacaoHistorico("Receita da nota " + observacaoNota.substring(0, observacaoNota.length()-1) + ".");
			documentoService.atualizaHistoricoDocumento(contaReceber, Boolean.FALSE);
			
			contareceberService.saveOrUpdateNoUseTransaction(contaReceber);
			if (contaReceber.getContrato()!=null && contaReceber.getContrato().getCdcontrato()!=null){
				contratoService.updateReajuste(contaReceber.getContrato(), Boolean.FALSE, null);
			}
			for(Nota nota : lista){
				nota.setListaNotaDocumento(this.montarListaNotaDocumentoUnico(nota, contaReceber));
			}
		}
		for(Nota nota : lista){
			notaDAO.atualizaStatus(nota);
			
			List<Documento> listaContaReceber = new ArrayList<Documento>();
			List<NotaDocumento> listaNotaDocumento = nota.getListaNotaDocumento();
			for (NotaDocumento notaDocumento : listaNotaDocumento) {
				notaDocumentoService.saveOrUpdateNoUseTransaction(notaDocumento);
				listaContaReceber.add(notaDocumento.getDocumento());
			}
			
			NotaHistorico notaHistorico = notaHistoricoService.criarHistoricoLiquidacao(nota, listaNotaDocumento);
			notaHistoricoService.saveOrUpdateNoUseTransaction(notaHistorico);
			
			NotaFiscalServico notaFiscalServico = null;
			Nota notafiscalproduto = null;
			if(nota.getNotaTipo() != null){
				if(nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
					notaFiscalServico = notaFiscalServicoService.carregarInfoCalcularTotal(new NotaFiscalServico(nota.getCdNota()));
				} else {
					notafiscalproduto = notafiscalprodutoService.carregarInfoCalcularTotal(nota);
				}
			}
			
			List<NotaContrato> listaNotaContrato = notaContratoService.findByNota(nota);
			for (NotaContrato notaContrato : listaNotaContrato) {
				for (Documento documento : listaContaReceber) {
					documento.setContrato(notaContrato.getContrato());
					
					if(notaFiscalServico != null){
						documento.setNota(notaFiscalServico);
					} else if(notafiscalproduto != null){
						documento.setNota(notafiscalproduto);
					} else {
						documento.setNota(nota);
					}
				}
				contratoService.verificaComissionamentoDocumento(listaContaReceber, true);
			}
		}
		
		
		return true;
	}
	
	/**
	 * Faz um update no campo notaStatus de uma Nota.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#atualizaNotaStatus(Nota, NotaStatus)
	 * @param nota
	 * @param novoStatus
	 * @author Hugo Ferreira
	 */
	public void atualizaNotaStatus(Nota nota, NotaStatus novoStatus) {
		notaDAO.atualizaNotaStatus(nota, novoStatus);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Retorna uma lista de Notas, cada uma com sua lista de NotaDocumento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#carregarItensParaAcao(String)
	 * 
	 * @param itensSelecionados: Deve ser uma string de id's separados por v�rgula
	 * @return List
	 * @throws SinedException: Se o par�metro itensSelecionados for nulo.
	 * 
	 * @author Hugo Ferreira
	 */
	public List<Nota> carregarItensParaAcao(String itensSelecionados) {
		return notaDAO.carregarItensParaAcao(itensSelecionados);
	}
	
	public Nota carregarItensParaAcao(Nota nota) {
		return this.carregarItensParaAcao(nota.getCdNota().toString()).get(0);
	}
	
	/**
	 *
	 * @see
	 *
	 * @param nota
	 * @param contaReceber
	 * @return
	 *
	 * @author Hugo Ferreira
	 */
	private List<NotaDocumento> montarListaNotaDocumentoUnico(Nota nota, Documento contaReceber) {
		NotaDocumento notaDocumento = new NotaDocumento();
		notaDocumento.setNota(nota);
		notaDocumento.setDocumento(contaReceber);
		notaDocumento.setOrdem(1);
		
		List<NotaDocumento> listaNotaDocumento = new ArrayList<NotaDocumento>();
		listaNotaDocumento.add(notaDocumento);
		return listaNotaDocumento;
	}
	
	/**
	 *
	 * @see
	 *
	 * @param nota
	 * @param contaReceber
	 * @return
	 *
	 * @author Hugo Ferreira
	 */
	private ArrayList<NotaDocumento> montarListaNotaDocumentoFinanciado(Nota nota, Documento contaReceber) {
		ArrayList<NotaDocumento> listaNotaDocumento = new ArrayList<NotaDocumento>();
		int ordem = 1;
		for (Documento parcela : contaReceber.getListaDocumento()) {
			NotaDocumento notaDocumento = new NotaDocumento();
			notaDocumento.setNota(nota);
			notaDocumento.setDocumento(parcela);
			notaDocumento.setOrdem(ordem);
			listaNotaDocumento.add(notaDocumento);
			
			ordem++;
		}
		return listaNotaDocumento;
	}

	/**
	 * Recebe uma nota com o cdNota preenchido;
	 * Carrega a nota com os dados preenchidos:
	 * 	  - cdNota;
	 *    - numero
	 *    - valor* (itens para calcular o valor total da nota - varia de acordo com o tipo)
	 * 
	 * Instancia e inicializa um objeto GeraReceita com as informa��es
	 * da nota e com valores padr�o.
	 *
	 * @see #carregarInfoParaCalculartotal(Nota)
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findByNota(Nota)
	 *
	 * @param nota
	 * @return
	 *
	 * @author Hugo Ferreira
	 * @author Fl�vio Tavares
	 */
	public GeraReceita criarGeraReceita(Nota nota) {
		Endereco endereco = null;
		if(nota.getListaNotaContrato() != null && nota.getListaNotaContrato().size() > 0)
			endereco = nota.getListaNotaContrato().get(0).getContrato().getEnderecocobranca();
		if(endereco == null)
			endereco = nota.getCliente().getEnderecoWithPrioridade(Enderecotipo.UNICO, Enderecotipo.FATURAMENTO);
		
		nota = this.load(nota, "nota.cdNota, nota.numero, nota.notaTipo, nota.dtEmissao");
		nota = this.carregarInfoParaCalculartotal(nota);
		
		GeraReceita receita = new GeraReceita();
		receita.setNota(nota);
		NotaFiscalServico nfs =  notaFiscalServicoService.findDescontosByNota(nota);
		Money valorDesconto = new Money();
		if (nfs != null){
			if (nfs.getDescontocondicionado() != null){
				valorDesconto = valorDesconto.add(nfs.getDescontocondicionado());
			} if (nfs.getDescontoincondicionado() != null){
				valorDesconto = valorDesconto.add(nfs.getDescontoincondicionado());
//			} if (nfs.getDeducao() != null){ //o valor da dedu��o n�o deduz no valor da nota
//				valorDesconto = valorDesconto.add(nfs.getDeducao());
			} if (nfs.getOutrasretencoes() != null){
				valorDesconto = valorDesconto.add(nfs.getOutrasretencoes());
			}
		}
		receita.setDescontoContratualMny(valorDesconto);
		Money prct = nota.getValorNota() != null && nota.getValorNota().getValue().doubleValue() > 0 ? valorDesconto.divide(nota.getValorNota()) : new Money();
		receita.setDescontoContratualPrct(prct);
		receita.setPrazoPagamento(nota.getPrazopagamentofatura() != null ? nota.getPrazopagamentofatura() : Prazopagamento.UNICA);
		receita.setQtdeParcelas(Integer.valueOf(1));
		receita.setValorUmaParcela(nota.getValorNota());
		receita.setEndereco(endereco);
		receita.setDtemissao(nota.getDtEmissao());
		
		if(nota instanceof Notafiscalproduto) {
			if(nota.getDataSaida() != null) {
				receita.setDtsaida(new Date(nota.getDataSaida().getTime()));
			} else if(nota.getDtEmissao() != null) {
				receita.setDtsaida(new Date(nota.getDtEmissao().getTime()));
			}
		} else {
			receita.setDtsaida(nota.getDtEmissao());
		}
		
		List<Documento> listaDocumento = documentoService.findByNota(nota);
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			for (Documento d : listaDocumento) {
				if(StringUtils.isBlank(d.getOutrospagamento()) && d.getPessoa() != null){
					d.setOutrospagamento(d.getPessoa().getNome());
				}
			}
		}
		receita.setListaDocumento(listaDocumento);
		
		if(nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)){
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaNotCancelada(nota);
			if(arquivonfnota != null){
				if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
					receita.setNumero(notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()));
				}
			}
		}
		
		return receita;
	}
	
	/**
	 * M�todo para criar o relat�rio de Nota Fiscal.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaFiscalServicoService#findForReport(NotaFiltro)
	 * @see #ajustaListaNotaRelatorio(List)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public IReport gerarRelatorioNotaFiscal(NotaFiltro filtro){
		
		String modelo = ParametrogeralService.getInstance().getValorPorNome("ModeloNotaFiscal");
		
		if(modelo == null || modelo.equals("")){
			throw new SinedException("Favor incluir o par�metro de configura��o do modelo de nota fiscal.");
		}
		
		Report report = new Report("faturamento/notaFiscal_" + modelo);
		Report subreport = new Report("faturamento/sub_notafiscal_" + modelo);
		report.addSubReport("NOTA_SUBREPORT", subreport);
		
		List<NotaFiscalServico> listaNotaServico = notaFiscalServicoService.findForReport(filtro);
		
		for (NotaFiscalServico nfs : listaNotaServico) {
			for (NotaFiscalServicoItem itens : nfs.getListaItens()) {
				if (itens.getDescricao() == null || itens.getDescricao().equals("")) {
					if (itens.getMaterial() != null && itens.getMaterial().getNome() != null && !itens.getMaterial().getNome().equals("")) {
						itens.setDescricao(itens.getMaterial().getNome());
					}
				} else {
					itens.setDescricao(itens.getDescricao());
				}
			}
		}
		
		this.ajustaListaNotaRelatorio(listaNotaServico);
		
		if(modelo.equals("exagro")){
			Report subreport2 = new Report("faturamento/sub_notafiscal_" + modelo + "_2");
			report.addSubReport("NOTA_SUBREPORT_2", subreport2);
			this.adicionaListaImpostos(listaNotaServico);
		}
		
		Collections.sort(listaNotaServico,new Comparator<NotaFiscalServico>(){
			public int compare(NotaFiscalServico o1, NotaFiscalServico o2) {
				
				try{
					Double o1num = Double.parseDouble(o1.getNumero());
					Double o2num = Double.parseDouble(o2.getNumero());
					return o1num.compareTo(o2num);
				} catch (Exception e) {
					if(o1.getNumero() != null && o2.getNumero() != null){
						return o1.getNumero().compareTo(o2.getNumero());
					} else if(o1.getNumero() == null){
						return "".compareTo(o2.getNumero());
					} else if(o2.getNumero() == null){
						return o1.getNumero().compareTo("");
					} else return 0;
					
				}
				
			}
		});
		
		report.setDataSource(listaNotaServico);
		
		return report;
	}
	
	private void adicionaListaImpostos(List<NotaFiscalServico> listaNotaServico) {
		
		List<ImpostoBean> lista;
		Money valorInssLocal,valorIrLocal,valorIssLocal,valorIcmsLocal,valorCofinsLocal,valorPisLocal,valorCsllLocal;
		ImpostoBean bean;
		for (NotaFiscalServico nf : listaNotaServico) {
			lista = new ArrayList<ImpostoBean>();
			
			valorInssLocal = nf.getValorInss();
			valorIrLocal = nf.getValorIr();
			valorIssLocal = nf.getValorIss();
			valorIcmsLocal = nf.getValorIcms();
			valorCofinsLocal = nf.getValorCofins();
			valorPisLocal = nf.getValorPis();
			valorCsllLocal = nf.getValorCsll();
			
			if (valorInssLocal != null && nf.getIncideinss() != null && nf.getIncideinss() && valorInssLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("INSS");
				bean.setImposto("(" + nf.getInss().toString() + "%)");
				bean.setValor(valorInssLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorIrLocal != null && nf.getIncideir() != null && nf.getIncideir() && valorIrLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("IRRF");
				bean.setImposto("(" + nf.getIr().toString() + "%)");
				bean.setValor(valorIrLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorIssLocal != null && nf.getIncideiss() != null && nf.getIncideiss() && valorIssLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("ISS");
				bean.setImposto("(" + nf.getIss().toString() + "%)");
				bean.setValor(valorIssLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorIcmsLocal != null && nf.getIncideicms() != null && nf.getIncideicms() && valorIcmsLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("ICMS");
				bean.setImposto("(" + nf.getIcms().toString() + "%)");
				bean.setValor(valorIcmsLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorCofinsLocal != null && nf.getIncidecofins() != null && nf.getIncidecofins() && valorCofinsLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("COFINS");
				bean.setImposto("(" + nf.getCofins().toString() + "%)");
				bean.setValor(valorCofinsLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorPisLocal != null && nf.getIncidepis() != null && nf.getIncidepis() && valorPisLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("PIS");
				bean.setImposto("(" + nf.getPis().toString() + "%)");
				bean.setValor(valorPisLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			if (valorCsllLocal != null && nf.getIncidecsll() != null && nf.getIncidecsll() && valorCsllLocal.getValue().doubleValue() > 0d){
				bean = new ImpostoBean();
				bean.setNome("CSLL");
				bean.setImposto("(" + nf.getCsll().toString() + "%)");
				bean.setValor(valorCsllLocal.getValue().doubleValue());
				lista.add(bean);
			}
			
			nf.setListaImpostos(lista);
		}
	}
	/**
	 * Ajustes para a lista do relat�rio de Nota fiscal.
	 * 
	 * @param listaNotaServico
	 * @author Fl�vio Tavares
	 */
	private void ajustaListaNotaRelatorio(List<NotaFiscalServico> listaNotaServico){
		for (NotaFiscalServico nfs : listaNotaServico) {
			
			Calendar cal = Calendar.getInstance();  
			int day = cal.get(Calendar.DATE);
			int month = cal.get(Calendar.MONTH);  
			int year = cal.get(Calendar.YEAR);  
			
			String diaEmissao;
			String anoEmissao;
			
			diaEmissao = Integer.toString(day);
			nfs.setDiaEmissao(diaEmissao);
			
			String mesEmissao = "";
		
			if(month == 0) 	mesEmissao = "Janeiro";
			if(month == 1) 	mesEmissao = "Fevereiro";
			if(month == 2) 	mesEmissao = "Mar�o";
			if(month == 3) 	mesEmissao = "Abril";
			if(month == 4) 	mesEmissao = "Maio";
			if(month == 5) 	mesEmissao = "Junho";
			if(month == 6) 	mesEmissao = "Julho";
			if(month == 7) 	mesEmissao = "Agosto";
			if(month == 8) 	mesEmissao = "Setembro";
			if(month == 9) 	mesEmissao = "Outubro";
			if(month == 10) mesEmissao = "Novembro";
			if(month == 11) mesEmissao = "Dezembro";
			
			nfs.setMesEmissao(mesEmissao);

			anoEmissao = Integer.toString(year);
			nfs.setAnoEmissao(anoEmissao);
			
			List<NotaFiscalServicoItem> listaItens = nfs.getListaItens();
			for (NotaFiscalServicoItem nfsi : listaItens) {
				if(nfsi.getQtde() == null){
					nfsi.setQtde(1.0);
				}
				nfsi.setPrecoTotal(nfsi.getTotalParcial());
			}
			if(nfs.getValorNota() != null) nfs.setExtenso(new Extenso(nfs.getValorNota()).toString());
			if(nfs.getCliente().getCnpj() !=null) {
				nfs.getCliente().setCpfcnpj(nfs.getCliente().getCnpj().toString());
			}else if (nfs.getCliente().getCpf()!=null) {
				nfs.getCliente().setCpfcnpj(nfs.getCliente().getCpf().toString());
			}
			//Este m�todo foi colocado separado, pois a query fora do for j� carrega uma lista. Se possuir 2 vai carrega incorretamente.
			Contrato contrato = contratoService.findInscricaoEstadualContratoByNota(nfs);
			String inscricaoestadual = nfs.getCliente().getInscricaoestadual();
			String nomeNF = nfs.getCliente().getRazaosocial() != null && !nfs.getCliente().getRazaosocial().trim().equals("") ? nfs.getCliente().getRazaosocial() : nfs.getCliente().getNome();
			if(contrato != null){
				if(contrato.getInscricaoestadual() != null && !contrato.getInscricaoestadual().trim().equals("")){
					inscricaoestadual = contrato.getInscricaoestadual();
				}
				
				if(contrato.getNomefantasia() != null && !contrato.getNomefantasia().trim().equals("")){
					nomeNF = contrato.getNomefantasia();
				}
			} 
			
			nfs.setInscricaoestadual(inscricaoestadual);
			nfs.setNomeNF(nomeNF);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#haveNFProduto
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveNFProduto(String whereIn) {
		return notaDAO.haveNFProduto(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#findForReceita
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Nota> findForReceita(String whereIn) {
		return notaDAO.findForReceita(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#liquidarNota
	 * 
	 * @param nota
	 * @author Rodrigo Freitas
	 */
	public void liquidarNota(Nota nota){
		notaDAO.liquidarNota(nota);
	}
	
	public IReport gerarRelarorioEvolucaoFaturamento(EvolucaoFaturamentoFiltro filtro) throws IOException {
		
		Report report = new Report("/faturamento/evolucaoFaturamento");
		
		List<Contratofaturalocacao> listaContratofaturalocacao = contratofaturalocacaoService.findForEvolucao(filtro);
		List<Nota> listaNota = this.findForEvolucao(filtro);
		listaNota = this.carregarInfoParaCalcularTotal(listaNota);
		
		List<EvolucaoFaturamentoBean> listaBean = this.agrupaNota(listaNota, listaContratofaturalocacao);
		
		report.addParameter("GRAFICO", Grafico.geraGraficoEvolucaoFaturamento(listaBean));
		report.setDataSource(listaBean);
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#findForEvolucao 
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Nota> findForEvolucao(EvolucaoFaturamentoFiltro filtro) {
		return notaDAO.findForEvolucao(filtro);
	}
	
	private List<EvolucaoFaturamentoBean> agrupaNota(List<Nota> listaNota, List<Contratofaturalocacao> listaContratofaturalocacao) {
		
		if(listaContratofaturalocacao != null && !listaContratofaturalocacao.isEmpty()){
			for (Contratofaturalocacao contratofaturalocacao : listaContratofaturalocacao) {
				if(contratofaturalocacao.getDtemissao() != null && contratofaturalocacao.getValortotal() != null){
					Nota nota = new Nota();
					nota.setValorLocacao(contratofaturalocacao.getValortotal());
					nota.setDtEmissao(contratofaturalocacao.getDtemissao());
					listaNota.add(nota);
				}
			}
		}
		
		Collections.sort(listaNota,new Comparator<Nota>(){
			public int compare(Nota o1, Nota o2) {
				return o1.getDtEmissao().compareTo(o2.getDtEmissao());
			}
		});
		
		List<EvolucaoFaturamentoBean> listaBean = new ArrayList<EvolucaoFaturamentoBean>();
		EvolucaoFaturamentoBean bean = null;
		
		Notafiscalproduto nfp;
		NotaFiscalServico nfs;
		
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		
		for (Nota nota : listaNota) {
			calendar1.setTimeInMillis(nota.getDtEmissao().getTime());
			
			if(bean != null){
				calendar2.setTimeInMillis(bean.getData().getTime());
				
				if(calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH) && 
						calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)){
					
					if (nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)) {
						
						nfp = (Notafiscalproduto) nota;
						bean.addValor(nfp.getValorNota());
						
					} else if (nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)) {
						
						nfs = (NotaFiscalServico) nota;
						bean.addValor(nfs.getValorNotaTransient());
			
					}else if(nota.getValorLocacao() != null){
						
						bean.addValor(nota.getValorLocacao());
						
					}else {
						throw new SinedException("Erro no tipo da nota.");
					}

					
					continue;
				} else {
					listaBean.add(bean);
				}
			}
			
			bean = new EvolucaoFaturamentoBean();
			
			if (nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_PRODUTO)) {
				
				nfp = (Notafiscalproduto) nota;
				bean.setValor(nfp.getValorNota().getValue().doubleValue());
				
			} else if (nota.getNotaTipo() != null && nota.getNotaTipo().equals(NotaTipo.NOTA_FISCAL_SERVICO)) {
				
				nfs = (NotaFiscalServico) nota;
				bean.setValor(nfs.getValorNota().getValue().doubleValue());
	
			} else if(nota.getValorLocacao() != null){
				
				bean.addValor(nota.getValorLocacao());
				
			} else {
				throw new SinedException("Erro no tipo da nota.");
			}
			
			bean.setData(nota.getDtEmissao());			
		}
		
		if(bean != null){
			listaBean.add(bean);
		}
		
		
		return listaBean;
	}
	
	public Money getValorCumulativoContrato(Contrato contrato, Faixaimpostocontrole controle) {
		return notaDAO.getValorCumulativoContrato(contrato, controle);
	}
	
	public Money getValorCumulativoMesCorrente(Faixaimpostocontrole controle, NotaTipo notatipo, Cliente cliente, Cumulativotipo cumulativotipo, Codigotributacao codigotributacao) {
		return this.getValorCumulativoMesCorrente(controle, notatipo, cliente, cumulativotipo, codigotributacao, null);
	}
	
	public Money getValorCumulativoMesCorrente(Faixaimpostocontrole controle, NotaTipo notatipo, Cliente cliente, Cumulativotipo cumulativotipo, Codigotributacao codigotributacao, Nota bean) {
		return notaDAO.getValorCumulativoMesCorrente(controle, notatipo, cliente, cumulativotipo, codigotributacao, bean);
	}
	
	public boolean isValorCumulativoCobrado(Faixaimpostocontrole controle, NotaTipo notatipo, Cliente cliente, Cumulativotipo cumulativotipo, Nota nota){
		return notaDAO.isValorCumulativoCobrado(controle, notatipo, cliente, cumulativotipo, nota);
	}
	
	public Nota findForReceitaIndividual(Nota nota) {
		return notaDAO.findForReceitaIndividual(nota);
	}
	
	public boolean haveNFDiferenteStatus(String whereIn, NotaStatus... status) {
		return notaDAO.haveNFDiferenteStatus(whereIn, status);
	}
	
	public boolean haveNFWithStatus(String whereIn, NotaStatus... status) {
		return notaDAO.haveNFWithStatus(whereIn, status);
	}
	
	public List<Nota> findNotas(String whereIn) {
		return notaDAO.findNotas(whereIn);
	}
	
	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosNotas(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Integer totalEmitidas = getQtdeNotasEmitidas();
		Integer totalLiquidadas = getQtdeNotasLiquidadas();
		Integer total = getQtdeTotalNotas();
		return new DadosEstatisticosBean(DadosEstatisticosBean.NOTA, ultimoCadastro, totalEmitidas, totalLiquidadas, total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeTotalNotas() {
		return notaDAO.getQtdeTotalNotas();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeNotasLiquidadas() {
		return notaDAO.getQtdeNotasLiquidadas();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getQtdeNotasEmitidas() {
		return notaDAO.getQtdeNotasEmitidas();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return notaDAO.getDtUltimoCadastro();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param nota
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Nota carregaDadosNota(Nota nota) {
		return notaDAO.carregaDadosNota(nota);
	}
	
	public void updateNumeroNota(Nota nota, Integer proxNum) {
		notaDAO.updateNumeroNota(nota, proxNum);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param venda
	 * @return
	 * @author Taidson
	 * @since 13/01/2011
	 */
	public List<Nota> notaSituacaoCanceladaDenegada(Venda venda){
		return notaDAO.notaSituacaoCanceladaDenegada(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param nota
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isNotaPossuiReceita(Nota nota) {
		return notaDAO.isNotaPossuiReceita(nota, false);
	}
	
	public boolean isNotaPossuiReceita(Nota nota, boolean considerarNotaDocumento) {
		return notaDAO.isNotaPossuiReceita(nota, considerarNotaDocumento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#isSituacaoNE(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param situacoes 
	 */
	public Boolean isSituacaoNE(String whereIn, NotaStatus[] situacoes){
		return notaDAO.isSituacaoNE(whereIn, situacoes);
	}
	/**
	 * M�todo que preenche o campo numeronfse de uma lista de notas.
	 * @param lista
	 */
	public void preencheNumeroNfse(List<Nota> lista){
		String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdNota", ",");
		
		List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNota(whereIn);
		for (Arquivonfnota arquivonfnota : listaArquivonfnota) {
			for (Nota nf : lista) {
				if(nf.getCdNota().equals(arquivonfnota.getNota().getCdNota())){
					if((arquivonfnota.getDtcancelamento() == null && 
							arquivonfnota.getArquivonf() != null && 
							arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
							arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO)) ||
							arquivonfnota.getDtcancelamento() != null && NotaStatus.CANCELADA.equals(nf.getNotaStatus())){
						try{
							nf.setNumeroNfseOrder(Long.parseLong(arquivonfnota.getNumeronfse()));
						} catch (Exception e) {}
						nf.setNumeroNfse(notaFiscalServicoService.formataNumNfse(arquivonfnota.getNumeronfse()) + " / " + arquivonfnota.getCodigoverificacao());
					}
					break;
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaDAO#atualizaDataEmissao(Nota nota, Date dtemissao)
	 *
	 * @param nota
	 * @param dtemissao
	 * @author Rodrigo Freitas
	 * @since 24/10/2012
	 */
	public void atualizaDataEmissao(Nota nota, Date dtemissao) {
		notaDAO.atualizaDataEmissao(nota, dtemissao);
	}
	
	/**
	 * M�todo que verifica se a nota pode ser editada. Quando a nota estiver em alguma situacao do processo de NFE, n�o pode editar
	 *
	 * @param nota
	 * @param produto
	 * @param servico
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean podeEditar(NotaStatus notaStatus, Boolean produto, Boolean servico, Boolean notaimportadaxml){
		if(notaimportadaxml != null && notaimportadaxml){
			return true;
		}
		
		Boolean editar = true;
		if(produto){
			if(NotaStatus.LIQUIDADA.equals(notaStatus) || NotaStatus.NFE_CANCELANDO.equals(notaStatus) ||
				NotaStatus.NFE_EMITIDA.equals(notaStatus) || NotaStatus.NFE_ENVIADA.equals(notaStatus) ||
				NotaStatus.NFE_LIQUIDADA.equals(notaStatus) || NotaStatus.NFE_SOLICITADA.equals(notaStatus)){
				editar = false;		
			}
		}else if(servico){
			if(NotaStatus.LIQUIDADA.equals(notaStatus) || NotaStatus.NFSE_CANCELANDO.equals(notaStatus) ||
					NotaStatus.NFSE_EMITIDA.equals(notaStatus) || NotaStatus.NFSE_ENVIADA.equals(notaStatus) ||
					NotaStatus.NFSE_LIQUIDADA.equals(notaStatus) || NotaStatus.NFSE_SOLICITADA.equals(notaStatus)){
					editar = false;		
				}
		}		
		return editar;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nota
	 * @param cancelarvenda
	 * @author Rodrigo Freitas
	 * @since 16/07/2013
	 */
	public void updateCancelamentoVenda(Nota nota, Boolean cancelarvenda) {
		notaDAO.updateCancelamentoVenda(nota, cancelarvenda);
	}
	
	/**
	 * M�todo que referencia o DAO 
	 * 
	 * @param nfproduto
	 * @return
	 */
	public NotaStatus getNotaStatus(Nota nota) {
		nota = notaDAO.getNotaStatus(nota);
		return nota.getNotaStatus();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/02/2014
	 */
	public List<Nota> findForEnvioNFCliente(String whereIn) {
		return notaDAO.findForEnvioNFCliente(whereIn);
	}
	
	public List<Nota> findForNotaRemessaLocacao (Contrato contrato){
		return notaDAO.findForNotaRemessaLocacao(contrato);
	}
	
	public ModelAndView preparePopupEnvioEmail(WebRequestContext request, String whereIn, NotaTipo notaTipo, Boolean isNfe) {
		List<Nota> listaNota = this.findForEnvioNFCliente(whereIn);
		if(listaNota == null || listaNota.size() == 0){
			request.addError("Falha no carregamento da nota para o envio.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(notafiscalprodutoService.haveNotaCriadasViaIntegracaoECF(whereIn)){
			request.addError("Nota importada via integra��o com ECF. Favor efeturar o envio do e-mail no local de origem.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Boolean mesmoCliente = Boolean.TRUE;
		Integer cdpessoa_aux = null;
		
		for (Nota nota : listaNota) {
//			if(Boolean.TRUE.equals(nota.getNotaimportadaxml())){
//				request.addError("N�o � poss�vel o envio de e-mail de notas que tenham sido importadas.");
//				SinedUtil.fechaPopUp(request);
//				return null;
//			}
			
			if(cdpessoa_aux == null){
				cdpessoa_aux = nota.getCliente().getCdpessoa();
			} else {
				if(!nota.getCliente().getCdpessoa().equals(cdpessoa_aux)){
					mesmoCliente = Boolean.FALSE;
					break;
				}
			}
		}
		
		EnvioEmailNotaBean bean = new EnvioEmailNotaBean();
		bean.setWhereInNota(whereIn);
		bean.setMesmoCliente(mesmoCliente);
		bean.setNotaTipo(notaTipo);
		bean.setNfe(isNfe);
		
		if(mesmoCliente){
			Nota nota = listaNota.get(0);
			Cliente cliente = nota.getCliente();
			List<PessoaContato> listaPessoaContato = cliente.getListaContato();
			
			List<EnvioEmailPessoaBean> listaPessoaEnvio = new ArrayList<EnvioEmailPessoaBean>();
			
			// INCLUIR E-MAIL PRINCIPAL DO CLIENTE
			if(cliente != null && 
					cliente.getEmail() != null && 
					!cliente.getEmail().trim().equals("")){
				listaPessoaEnvio.add(new EnvioEmailPessoaBean(true, cliente.getCdpessoa(), cliente.getNome(), cliente.getEmail(), true));
			}
			
			// INCLUIR OS CONTATOS
			if(listaPessoaContato != null && listaPessoaContato.size() > 0){
				for (PessoaContato pessoaContato : listaPessoaContato) {
					Contato contato = pessoaContato.getContato();
					
					if(contato.getAtivo() != null && 
							contato.getAtivo() && 
							Boolean.TRUE.equals(contato.getRecebernf()) &&
							contato.getEmailcontato() != null && //
							!contato.getEmailcontato().trim().equals("")){
						listaPessoaEnvio.add(new EnvioEmailPessoaBean(Boolean.TRUE.equals(contato.getRecebernf()), 
								contato.getCdpessoa(), 
								contato.getNome(), 
								contato.getEmailcontato(),
								Boolean.TRUE.equals(contato.getReceberboleto())));
					}
				}
			}
			
			bean.setListaDestinatario(listaPessoaEnvio);
		}
		
		return new ModelAndView("direct:/crud/popup/popupEmailNota", "bean", bean);
	}
	
	public void savePopupEnvioEmailNota(WebRequestContext request, EnvioEmailNotaBean bean, NotaTipo notaTipo, Boolean isNfe) {
		boolean enviarPdf = bean.getEnviarPdf() != null && bean.getEnviarPdf();
		boolean enviarXml = bean.getEnviarXml() != null && bean.getEnviarXml();
		
		TipoEnvioNFCliente[] tipos = null;
		
		TipoEnvioNFCliente tipoPdf = notaTipo.equals(NotaTipo.NOTA_FISCAL_PRODUTO) ? TipoEnvioNFCliente.PRODUTO_PDF : TipoEnvioNFCliente.SERVICO_PDF;
		TipoEnvioNFCliente tipoXml = notaTipo.equals(NotaTipo.NOTA_FISCAL_PRODUTO) ? TipoEnvioNFCliente.PRODUTO_XML : TipoEnvioNFCliente.SERVICO_XML;
		
		if(enviarPdf && enviarXml) tipos = new TipoEnvioNFCliente[]{tipoPdf, tipoXml};
		if(!enviarPdf && enviarXml) tipos = new TipoEnvioNFCliente[]{tipoXml};
		if(enviarPdf && !enviarXml) tipos = new TipoEnvioNFCliente[]{tipoPdf};
		
		List<EnvioEmailPessoaBean> listaEnvioEmailPessoaBean = bean.getListaDestinatario();
		List<EnvioEmailPessoaBean> listaEnvioEmailPessoaBeanNova = new ArrayList<EnvioEmailPessoaBean>();
		for (EnvioEmailPessoaBean envioEmailPessoaBean : listaEnvioEmailPessoaBean) {
			if(envioEmailPessoaBean.getMarcado() != null && envioEmailPessoaBean.getMarcado()){
				listaEnvioEmailPessoaBeanNova.add(envioEmailPessoaBean);
			}
		}
		
		EnvioEmailResultadoBean resultado = arquivonfService.enviarNFCliente(bean.getWhereInNota(), listaEnvioEmailPessoaBeanNova, bean.getEnviarTransportador(), isNfe, tipos); 
		
		String descricaoNfe = "NF-e";
		if(isNfe){
			descricaoNfe = "NFC-e";
		}
		if(notaTipo.equals(NotaTipo.NOTA_FISCAL_SERVICO)){
			descricaoNfe = "NFS-e";
		}
		if(resultado.getSucesso() > 0){
			request.addMessage(resultado.getSucesso() + " " + descricaoNfe + "(s) enviada(s) para o(s) cliente(s) com sucesso.");
		}
		if(resultado.getErro() > 0){
			request.addError(resultado.getErro() + " " + descricaoNfe + "(s) n�o foi(ram) enviada(s) para o(s) cliente(s).");
		}
		
		if(resultado.getListaErro() != null){
			for (String erro : resultado.getListaErro()) {
				request.addError(erro);
			}
		}
		
		SinedUtil.fechaPopUp(request);
	}
	
	public List<Nota> findForNotaNfeNaoUtilizado(NumeroNfeNaoUtilizadoFiltro filtro){
		return notaDAO.findForNotaNumeroNfeNaoUtilizado(filtro);
	}
	
	public List<Nota> findForNotaNfeNaoUtilizadoByNumero(String whereIn, NumeroNfeNaoUtilizadoFiltro filtro){
		return notaDAO.findForNotaNumeroNfeNaoUtilizadoByNumero(whereIn, filtro);
	}
	
	/**
	 * Retorna a inscri��o estadual baseado no endere�o e cliente da nota.
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/07/2015
	 */
	public String getInscricaoEstadualClienteEndereco(Nota nota) {
		String inscricaoestadual = null;
		if(nota.getEnderecoCliente() != null){
			Endereco endereco = enderecoService.load(nota.getEnderecoCliente(), "endereco.cdendereco, endereco.inscricaoestadual");
			if(endereco != null && 
					endereco.getInscricaoestadual() != null &&
					!endereco.getInscricaoestadual().equals("")){
				inscricaoestadual = endereco.getInscricaoestadual();
			}
		}
		if(inscricaoestadual == null && nota.getCliente() != null){
			Cliente cliente = clienteService.load(nota.getCliente(), "cliente.cdpessoa, cliente.inscricaoestadual");
			if(cliente != null &&
					cliente.getInscricaoestadual() != null &&
					!cliente.getInscricaoestadual().equals("")){
				inscricaoestadual = cliente.getInscricaoestadual();
			}
		}
		return inscricaoestadual;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaDAO#loadWithCliente(Nota nota)
	*
	* @param nota
	* @return
	* @since 22/10/2015
	* @author Luiz Fernando
	*/
	public Nota loadWithCliente(Nota nota) {
		return notaDAO.loadWithCliente(nota);
	}
	
	/**
	 * M�todo que faz a consulta da situa��o de nota para o WS SOAP.
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2016
	 */
	public List<ConsultarSituacaoNotaWSBean> consultarSituacaoNotaWSSOAP(ConsultarSituacaoNotaBean bean) {
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		return this.findNotasForWSSOAP(empresa, bean.getDataReferencia());
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param dataReferencia
	 * @return
	 * @author Rodrigo Freitas
	 * @since 23/09/2016
	 */
	private List<ConsultarSituacaoNotaWSBean> findNotasForWSSOAP(Empresa empresa, Timestamp dataReferencia) {
		return notaDAO.findNotasForWSSOAP(empresa, dataReferencia);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Documentotipo getDocumentotipo(List lista) {
		Documentotipo documentotipo = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Nota nota : (List<Nota>) lista){
				if(nota.getDocumentotipo() != null){
					if(documentotipo == null){
						documentotipo = nota.getDocumentotipo();
					}else if(!documentotipo.equals(nota.getDocumentotipo())){
						return null;
					}
				}
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotaDAO#findLiquidadaComDocumentoCancelado(String whereInDocumento)
	*
	* @param whereInDocumento
	* @return
	* @since 13/03/2017
	* @author Luiz Fernando
	*/
	public List<Nota> findLiquidadaComDocumentoCancelado(String whereInDocumento) {
		return notaDAO.findLiquidadaComDocumentoCancelado(whereInDocumento);
	}
	
	/**
	* M�todo que valida se as notas passadas como par�metro na request possuem total diferente do total de parcelas.
	*
	*
	* @param request
	* @return
	* @since 15/09/2017
	* @author Mairon Cezar
	*/
	public String validaDivergenciaValoresParcela(WebRequestContext request){
		boolean valorNotaParcelaDivergente = parametrogeralService.getBoolean(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE);
		if(!valorNotaParcelaDivergente){
			return null;
		}
		
		String whereIn = request.getParameter("selectedItens");
		String tipoNota = (String)request.getAttribute("tipoNota");
		boolean listagem = "true".equals(request.getParameter("listagem"));
		
		List<? extends Nota> listaNota = null;
		if("servico".equals(tipoNota)){
			listaNota = notaFiscalServicoService.findByWhereInForCalculototal(whereIn);
		}else{
			listaNota = notafiscalprodutoService.findByWhereIn(whereIn);
		}
		String msg = "";
		List<Nota> listTotalparcelasDifValornota = new ArrayList<Nota>();
		Money valorduplicatas = null;
		Money valordiferencaduplicataTotalNota = null;
		for(Nota bean: listaNota){
			boolean parcelasiguaisjuros = false;

			Boolean cadastrarCobranca = "servico".equals(tipoNota)? ((NotaFiscalServico)bean).getCadastrarcobranca(): ((Notafiscalproduto)bean).getCadastrarcobranca();
			if(Boolean.TRUE.equals(cadastrarCobranca)){
				if (bean.getPrazopagamentofatura()!=null){
					parcelasiguaisjuros = Boolean.TRUE.equals(prazopagamentoService.load(bean.getPrazopagamentofatura()).getParcelasiguaisjuros());
				}
				if(!parcelasiguaisjuros){
					valorduplicatas = new Money();
					valordiferencaduplicataTotalNota = new Money();
					
					if("servico".equals(tipoNota)){
						NotaFiscalServico nota = (NotaFiscalServico)bean;
						for(Notafiscalservicoduplicata nfpd: notafiscalservicoduplicataService.findByNotafiscalservico((NotaFiscalServico)bean)){
							valorduplicatas = valorduplicatas.add(nfpd.getValor());	
						}
						
						valordiferencaduplicataTotalNota = nota.getValorNota().compareTo(valorduplicatas) > 0? valordiferencaduplicataTotalNota.add(nota.getValorNota().subtract(valorduplicatas)):
																											valordiferencaduplicataTotalNota.add(valorduplicatas.subtract(nota.getValorNota()));
					}else{
						Notafiscalproduto nota = (Notafiscalproduto)bean;
						for(Notafiscalprodutoduplicata nfpd: notafiscalprodutoduplicataService.findByNotafiscalproduto(nota)){
							valorduplicatas = valorduplicatas.add(nfpd.getValor());	
						}

						valordiferencaduplicataTotalNota = nota.getValor().compareTo(valorduplicatas) > 0? valordiferencaduplicataTotalNota.add(nota.getValor().subtract(valorduplicatas)):
																										valordiferencaduplicataTotalNota.add(valorduplicatas.subtract(nota.getValor()));
					}
					
					if(valorNotaParcelaDivergente && valordiferencaduplicataTotalNota.getValue().compareTo(BigDecimal.ZERO)!=0){
						listTotalparcelasDifValornota.add(bean);
					}

				}
			}
		}
		
		if(listTotalparcelasDifValornota.size() > 0){
			String notas = montaStringNotas(listTotalparcelasDifValornota, listagem);

			if(listTotalparcelasDifValornota.size() == 1){
				msg += "O valor total das parcelas na nota "+ notas+" est� diferente do valor l�quido da nota. Favor verificar o valor original da aba cobran�a e gerar as parcelas novamente.\n";				
			}else{
				msg += "O valor total das parcelas nas notas "+ notas+" est�o diferentes do valor l�quido da nota. Favor verificar o valor original da aba cobran�a e gerar as parcelas novamente.\n";				
			}
		}

		return msg;
		
	}
	
	private String montaStringNotas(List<Nota> listCdnotas, boolean criaLinks){
		String notas = "";
		int count = 0;
		if(criaLinks){
			for(Nota nota: listCdnotas){
				count++;
				String cdnota = nota.getCdNota().toString();
				String numeronota = nota.getNumeroFormatado() != null? nota.getNumeroFormatado(): nota.getCdNota().toString();
				notas += "<a href=\"javascript:visualizarNota("+ cdnota+");\">"+ numeronota +"</a>";
				if(count == (listCdnotas.size() - 1)){
					notas += " e ";
				}else if(count < listCdnotas.size()){
					notas += ", ";
				}
			}
		}else{
			for(Nota nota: listCdnotas){
				count++;
				String cdnota = nota.getCdNota().toString();
				notas += (" "+cdnota);
				if(count == (listCdnotas.size() - 1)){
					notas += " e ";
				}else if(count < listCdnotas.size()){
					notas += ",";
				}
			}
		}
		return notas;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param cdNota
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public boolean haveRetornoCanhoto(Integer cdNota) {
		return notaDAO.haveRetornoCanhoto(cdNota);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param cdNota
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public void updateRetornoCanhoto(Integer cdNota) {
		notaDAO.updateRetornoCanhoto(cdNota);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param cdNota
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public void updateRetornoCanhotoFalse(Integer cdNota) {
		notaDAO.updateRetornoCanhotoFalse(cdNota);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/10/2017
	 */
	public List<Nota> findForEstornoCanhoto(String whereIn) {
		return notaDAO.findForEstornoCanhoto(whereIn);
	}
	
	public void updateSerieNfe(Integer cdnotafiscalproduto, Integer serienfe){
		notaDAO.updateSerieNfe(cdnotafiscalproduto, serienfe);
	}
	
	public void updateSerieNfce(Integer cdnotafiscalproduto, Integer serieNfce){
		notaDAO.updateSerieNfce(cdnotafiscalproduto, serieNfce);
	}
}
