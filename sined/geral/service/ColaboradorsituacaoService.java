package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorsituacaoService extends GenericService<Colaboradorsituacao> {	
	
	/* singleton */
	private static ColaboradorsituacaoService instance;
	public static ColaboradorsituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorsituacaoService.class);
		}
		return instance;
	}
	
}
