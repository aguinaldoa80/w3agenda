package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Rede;
import br.com.linkcom.sined.geral.bean.Redeip;
import br.com.linkcom.sined.geral.dao.RedeipDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RedeipService extends GenericService<Redeip>{
	
	private RedeipDAO redeipDAO;
	
	public void setRedeipDAO(RedeipDAO redeipDAO) {
		this.redeipDAO = redeipDAO;
	}
	
	public List<Redeip> findByRede(Rede rede){
		return this.findByRedeContratomaterialrede(rede, null);
	}
	
	public List<Redeip> findByRedeContratomaterialrede(Rede rede, Integer cdcontratomaterialrede){
		return redeipDAO.findByRedeContratomaterialrede(rede, cdcontratomaterialrede);
	}
	
	public List<Redeip> findByContratoMaterialRede(Integer cdcontratomaterialrede){
		return redeipDAO.findByContratoMaterialRede(cdcontratomaterialrede);
	}
}
