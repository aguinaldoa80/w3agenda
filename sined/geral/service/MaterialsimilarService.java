package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialsimilar;
import br.com.linkcom.sined.geral.dao.MaterialsimilarDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MaterialSimilarTrocaWSBean;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialsimilar.MaterialsimilarRESTModel;
import br.com.linkcom.sined.util.rest.w3producao.materialsimilar.MaterialSimilarW3producaoRESTModel;

public class MaterialsimilarService extends GenericService<Materialsimilar> {

	private MaterialsimilarDAO materialsimilarDAO;
	private MaterialService materialService;
	
	public void setMaterialsimilarDAO(MaterialsimilarDAO materialsimilarDAO) {
		this.materialsimilarDAO = materialsimilarDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	/* singleton */
	private static MaterialsimilarService instance;
	public static MaterialsimilarService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialsimilarService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialsimilarDAO#existMaterialsimilar(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMaterialsimilar(Material material){
		return materialsimilarDAO.existMaterialsimilar(material);
	}
	
	public List<MaterialsimilarRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialsimilarRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialsimilarRESTModel> lista = new ArrayList<MaterialsimilarRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialsimilar bean : materialsimilarDAO.findForAndroid(whereIn))
				lista.add(new MaterialsimilarRESTModel(bean));
		}
		
		return lista;
	}
	
	public List<MaterialSimilarW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * M�todo que monta a lista de Materialsimilar a ser sincronizada com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<MaterialSimilarW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialSimilarW3producaoRESTModel> lista = new ArrayList<MaterialSimilarW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialsimilar ms : materialsimilarDAO.findForW3Producao(whereIn))
				lista.add(new MaterialSimilarW3producaoRESTModel(ms));
		}
		
		return lista;
	}
	
	public MaterialSimilarTrocaWSBean findForWSVenda(Material material){
		if(!Util.objects.isPersistent(material)){
			return null;
		}
		return new MaterialSimilarTrocaWSBean(materialService.findForTrocaMaterialsimilar(material));
	}
}
