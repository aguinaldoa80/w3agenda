package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Turmaparticipante;
import br.com.linkcom.sined.geral.dao.TurmaDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.TurmaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TurmaService extends GenericService<Turma> {

	private TurmaDAO turmaDAO;
	private DepartamentoService departamentoService;
	private ColaboradorService colaboradorService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setDepartamentoService(DepartamentoService departamentoService) {
		this.departamentoService = departamentoService;
	}
	public void setTurmaDAO(TurmaDAO turmaDAO) {
		this.turmaDAO = turmaDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaDAO#findTurmaConflitante
	 * @param colaborador
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long findTurmaConflitante(Colaborador colaborador, Turma bean) {
		return turmaDAO.findTurmaConflitante(colaborador, bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaDAO#findTurmaConflitante
	 * @param instrutor
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long findTurmaConflitante(Instrutor instrutor, Turma bean) {
		return turmaDAO.findTurmaConflitante(instrutor, bean);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaDAO#findForListagemColaborador()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Turma> findForListagemColaborador(){
		return turmaDAO.findForListagemColaborador();
	}
		
	/* singleton */
	private static TurmaService instance;
	public static TurmaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TurmaService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * Carrega a lista de turmas de um treinamento para confec��o do relat�rio.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TurmaDAO#findForReport(TurmaFiltro)
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Turma> findForReport(TurmaFiltro filtro) {
		return turmaDAO.findForReport(filtro);
	}
	
	/**
	 * Cria��o do relat�rio de treinamento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TurmaService#findForReport(TurmaFiltro)
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createRelatorioTreinamento(TurmaFiltro filtro) {
		Report report = new Report("rh/treinamento");
		List<Turma> lista = this.findForReport(filtro);
		
		report.setDataSource(lista);

		report.addParameter("DEPARTAMENTO", filtro.getDepartamento() != null ? departamentoService.load(filtro.getDepartamento(), "departamento.nome") : null);
		report.addParameter("PARTICIPANTE", filtro.getColaborador() != null ? colaboradorService.load(filtro.getColaborador(), "colaborador.nome") : null);
		
		
		return report;
	}
	public IReport createRelatorioTreinamentoListapresenca(Integer cdturma) {
		Report report = new Report("rh/treinamento_listaPresenca");
		Turma turma = this.findTurmaForListaPresenca(cdturma);
		
		report.addParameter("TREINAMENTO", turma.getTreinamento() != null ? turma.getTreinamento().getNome() : null);
		report.addParameter("TURMA", turma.getDescription() != null ? turma.getDescription() : null);
		report.addParameter("PERIODO", turma.getPeriodo() != null ? turma.getPeriodo() : null);
		report.addParameter("INSTRUTOR", turma.getInstrutor() != null ? turma.getInstrutor().getNome() : null);
		
		
		Set<Turmaparticipante> lista = turma.getListaTurmaparticipante();
		
		report.setDataSource(lista);
		
		return report;
	}
	private Turma findTurmaForListaPresenca(Integer cdturma) {
		return turmaDAO.findTurmaForListaPresenca(cdturma);
	}
	
}
