package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.dao.ServicoservidortipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicoservidortipoService extends GenericService<Servicoservidortipo>{

	private ServicoservidortipoDAO servicoservidortipoDAO;
	
	public void setServicoservidortipoDAO(ServicoservidortipoDAO servicoservidortipoDAO) {
		this.servicoservidortipoDAO = servicoservidortipoDAO;
	}
	
	/**
	 * Retorna o tipo de servi�o de acordo com o tipo de material
	 * @param materialtipo
	 * @return
	 * @author Thiago Gon�alves
	 */
	public Servicoservidortipo findByMaterialtipo(Materialtipo materialtipo){
		return servicoservidortipoDAO.findByMaterialtipo(materialtipo);
	}
	
	/**
	 * Retorna os tipos de servi�o que est�o relacionados � rede
	 * @return
	 * @author Thiago Gon�alves
	 */
	public List<Servicoservidortipo> findByServicorede(){
		return servicoservidortipoDAO.findByServicorede();
	}
}
