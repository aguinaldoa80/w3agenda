package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.linkcom.lkbanco.LkBanco;
import br.com.linkcom.lkbanco.bank.BankEnum;
import br.com.linkcom.lkbanco.filereturn.FilereturnImpl;
import br.com.linkcom.lkbanco.filereturn.FilereturnVO;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessa240Bean;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaBean;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauCT;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauDOC;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauHeader;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauHeaderLote;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauLT;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauNF;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauTrailer;
import br.com.linkcom.lkbanco.filesend.bean.ArquivoRemessaItauTrailerLote;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivobancarioDAO;
import br.com.linkcom.sined.geral.dao.ArquivobancariodocumentoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.GerarArquivoRemessaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.Modulos;
import br.com.linkcom.utils.StringUtils;

public class ArquivobancarioService extends GenericService<Arquivobancario> {

	private ArquivobancarioDAO arquivobancarioDAO;
	private ArquivobancariodocumentoDAO arquivobancariodocumentoDAO;
	private DocumentoService documentoService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private MovimentacaoService movimentacaoService;
	private TaxaitemService taxaitemService;
	private ClienteService clienteService;
	private ContacorrenteService contacorrenteService;

	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setMovimentacaoorigemService(
			MovimentacaoorigemService movimentacaoorigemService) {
		this.movimentacaoorigemService = movimentacaoorigemService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setArquivobancarioDAO(ArquivobancarioDAO arquivobancarioDAO) {
		this.arquivobancarioDAO = arquivobancarioDAO;
	}
	public void setTaxaitemService(TaxaitemService taxaitemService) {
		this.taxaitemService = taxaitemService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {
		this.contacorrenteService = contacorrenteService;
	}
	public void setArquivobancariodocumentoDAO(
			ArquivobancariodocumentoDAO arquivobancariodocumentoDAO) {
		this.arquivobancariodocumentoDAO = arquivobancariodocumentoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivobancarioDAO#findArquivosBancarios
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas 
	 */
	public List<Arquivobancario> findArquivosBancarios(String whereIn) {
		return arquivobancarioDAO.findArquivosBancarios(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivobancarioDAO#updateSituacao
	 * @param situacao
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Arquivobancariosituacao situacao, String whereIn) {
		arquivobancarioDAO.updateSituacao(situacao,whereIn);
	}

	/**
	 * Processa o arquivo retorno.
	 * 
	 * - O LkBanco retorna uma lista de FilereturnVO, com todas as contas a pagar do arquivo.
	 * - O m�todo verifica se as contas a pagar existem em outros arquivos banc�rios.
	 * - Um arquivo banc�rio n�o pode ter situa��o diferente de GERADO e n�o pode ter um arquivo correspondente.
	 * - Salva o arquivo banc�rio com seu arquivo correspondete e situa��o igual a PROCESSADO.
	 * - Atualiza a situa��o dos documento n�o aprovados para a �ltima acao antes da baixada.
	 * - Atualiza a situa��o do arquivo banc�rio correspondente.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivobancarioService#findArquivoDocumentos
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#carregaDocumentos
	 * @see br.com.linkcom.sined.geral.service.ArquivobancarioService#updateArquivoCorrespondenteSituacao
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#retornaDocumentosAcaoAnt
	 * @param bean
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void processaArquivoRetorno(ArquivoRetornoBean bean) throws Exception {
		
		FilereturnImpl readFileReturn = LkBanco.readFileReturn(BankEnum.ITAU.getNumber(), bean.getArquivo().getContent());
		
		List<FilereturnVO> lista = readFileReturn.getLista();
		
		List<FilereturnVO> listaAprovados = new ArrayList<FilereturnVO>();
		List<FilereturnVO> listaReprovados = new ArrayList<FilereturnVO>();	
		
		List<Arquivobancario> listaArquivo = findArquivoDocumentos(CollectionsUtil.listAndConcatenate(lista, "seunumero", ","));
		if (listaArquivo == null || listaArquivo.size() != 1) {
			throw new SinedException("Documentos relacionados no arquivo presentes em mais de um arquivo remessa.");
		}
		
		if (lista.size() != listaArquivo.get(0).getListaArqBancarioDoc().size()) {
			throw new SinedException("Arquivo de retorno n�o confere com nenhum arquivo remessa.");
		}
		
		for (FilereturnVO filereturnVO : lista) {
			if (filereturnVO.getAprovado()) {
				listaAprovados.add(filereturnVO);
			} else {
				listaReprovados.add(filereturnVO);
			}
		}
		
		List<Documento> documentos;
		if(listaAprovados.size() > 0){
			documentos = documentoService.carregaDocumentos(CollectionsUtil.listAndConcatenate(listaAprovados, "seunumero", ","));
			bean.setListaDocumentoAprovado(documentos);
			
			Money total = new Money();
			for (Documento documento : documentos) {
				total = total.add(documento.getValor());
			}
			bean.setValortotal(total);
			
		} else {
			bean.setListaDocumentoAprovado(new ArrayList<Documento>());
		}
		
		if (listaReprovados.size() > 0) {
			documentos = documentoService.carregaDocumentos(CollectionsUtil.listAndConcatenate(listaReprovados, "seunumero", ","));
			bean.setListaDocumentoReprovado(documentos);
			bean.setMovimentacao(movimentacaoService.findByDocumento(bean.getListaDocumentoReprovado().get(0)));
			
			for (Documento documento : documentos) {
				// DELETA A MOVIMENTA��O ORIGEM DO DOCUMENTO
				movimentacaoorigemService.deleteFromDocumento(documento);
				// RETORNA O DOCUMENTO PARA A �LTIMA A��O ANTES DA BAIXA.
				documentoService.retornaDocumentosAcaoAnt(documento,Documentoacao.ALTERADA,"Pagamento reprovado", false);
			}
		} else {
			bean.setListaDocumentoReprovado(new ArrayList<Documento>());
		}
		
		
		Arquivo arquivo = bean.getArquivo();
		Arquivobancario arquivobancario = new Arquivobancario(bean.getArquivo().getNome(),Arquivobancariotipo.RETORNO,bean.getArquivo(),new Date(System.currentTimeMillis()),Arquivobancariosituacao.PROCESSADO,listaArquivo.get(0));
		
		arquivoDAO.saveFile(arquivobancario, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		saveOrUpdate(arquivobancario);
		
		updateArquivoCorrespondenteSituacao(listaArquivo.get(0), arquivobancario, Arquivobancariosituacao.PROCESSADO);
	}
	
	
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivobancarioDAO#findArquivoDocumentos
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Arquivobancario> findArquivoDocumentos(String whereIn) {
		return arquivobancarioDAO.findArquivoDocumentos(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivobancarioDAO#updateArquivoCorrespondenteSituacao
	 * @param arquivobancario
	 * @param arquivobancariocorrespondente
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoCorrespondenteSituacao(Arquivobancario arquivobancario, 
			Arquivobancario arquivobancariocorrespondente, Arquivobancariosituacao situacao){
		arquivobancarioDAO.updateArquivoCorrespondenteSituacao(arquivobancario, arquivobancariocorrespondente, situacao);
	}
	
	public ArquivoRemessaBean setHeaderTraillerRemessa400(Conta conta, Integer sequencialRegistro){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String empresaNome = conta.getEmpresa().getRazaosocialOuNome().toUpperCase(); 
		String codigoBanco = "237";
		String nomeBanco = "BRADESCO";
		Integer sequencial = conta.getSequencial() != null ? conta.getSequencial() : 1;
		String convenioBanco = conta.getContacarteira().getConvenio();
		
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRANCA");
		ht.setCdEmpresa(convenioBanco);
		ht.setNomeEmpresa(empresaNome);
		ht.setCdBanco(codigoBanco);
		ht.setNomeBanco(nomeBanco);
		ht.setDtMovimento(data); 							// Data do Movimento
		ht.setIdSistema("MX");
		ht.setNumSequencialArquivo(sequencial.toString());			// N�mero seq�encial do arquivo
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		sequencialRegistro++;
		ht.setIdRegistroTrailler("9");															
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		return ht;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Real
	 * Registros do tipo HEADER e TRAILLER
	 * @since 22/05/2010
	 * @author Taidson
	 */
	public ArquivoRemessaBean setHeaderTraillerRemessa400BancoReal(Conta conta, Integer sequencialRegistro, Long vrTotal){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String codigoBanco = "356";
		String nomeBanco = "Banco Real";
		String agenciaCedente = conta.getAgencia();
		String contaCedente = conta.getNumero();
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
		
		ht.setIdRegistro("0");
		ht.setIdRemessa("1REMESSA01COBRANCA");
		ht.setAgenciaCedente(agenciaCedente);
		ht.setContaCedente(contaCedente);
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco(codigoBanco);
		ht.setNomeBanco(nomeBanco);
		ht.setDtMovimento(data); 							// Data do Movimento
		ht.setIdSistema("01600BPI");
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		ht.setIdRegistroTrailler("9");
		ht.setQtdeTitulos(sequencialRegistro.toString());
		sequencialRegistro=(sequencialRegistro*2)+2;
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		ht.setVrTotal(vrTotal.toString());
		
		return ht;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco ITAU
	 * Registros do tipo HEADER e TRAILLER
	 * @author Tom�s
	 */
	public ArquivoRemessaBean setHeaderTraillerRemessa400BancoItau(Conta conta, Integer sequencialRegistro){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String codigoBanco = "341";
		String nomeBanco = "BANCO ITAU SA";
		String agenciaCedente = conta.getAgencia();
		String contaCedente = conta.getNumero();
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
		
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRANCA");
		ht.setAgenciaCedente(agenciaCedente);
		ht.setContaCedente(contaCedente);
		ht.setDacCedente(conta.getDvnumero().substring(conta.getDvnumero().length()-1, conta.getDvnumero().length())); //Verificar se est� certo
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco(codigoBanco);
		ht.setNomeBanco(nomeBanco);
		ht.setDtMovimento(data); 							// Data do Movimento
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
//			ht.setIdRegistroTrailler("9");
//			ht.setQtdeTitulos(sequencialRegistro.toString());
//			sequencialRegistro=(sequencialRegistro*2)+2;
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
//			ht.setVrTotal(vrTotal.toString());
		
		return ht;
	}
	
	public List<ArquivoRemessaBean> setBodyRemessa400(Conta conta, List<Documento> listaDocumento){
		Contacarteira contacarteira = conta.getContacarteira();
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		DecimalFormat df = new DecimalFormat("####.00"); 
		String codigoBanco = "237";
		String carteira = contacarteira.getCarteira();
		String agencia = conta.getAgencia();
		String contacorrente = conta.getNumero();
		String dgcontacorrente = conta.getDvnumero();
//		Double valorTotal = new Double(0.0);
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;
//			Double valorDebito = new Double(0.0);
//			valorDebito+=d.getAux_documento().getValoratual().getValue().doubleValue();
//			valorTotal += valorDebito;

			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("1");
			bean.setCarteiraBody(carteira);
			bean.setAgenciaBody(agencia);
			bean.setContaBody(contacorrente);
			bean.setDigitoContaBody(dgcontacorrente);
//			bean.setIdEmpresaBody("0"+carteira+agencia+contacorrente+dgcontacorrente);
			bean.setControleParticipanteBody(d.getCddocumento().toString());
			bean.setCdBancoBody(codigoBanco);
			
			if(contacarteira.getEnviarmultaremessa() != null && contacarteira.getEnviarmultaremessa()){
				Double percentualMulta = taxaitemService.getValorPercentualTipotaxa(d, Tipotaxa.MULTA, true);
				if(percentualMulta > 0){
					bean.setCampoMultaBody("2");
					bean.setPercentualMultaBody(StringUtils.stringCheia(StringUtils.soNumero(df.format(percentualMulta)), "0", 4, false));
				} else {
					bean.setCampoMultaBody("0");
					bean.setPercentualMultaBody("0000");
				}
			} else {
				bean.setCampoMultaBody("0");
				bean.setPercentualMultaBody("0000");
			}
			
			if(contacarteira.getBancogeranossonumero() != null && contacarteira.getBancogeranossonumero()){
				bean.setIdTituloBancoBody("00000000000");
				bean.setDigitoConfNossoNumeroBody("0");
			} else {
				String idtitulo = StringUtils.stringCheia(d.getCddocumento().toString(), "0", 11, false);
				bean.setIdTituloBancoBody(idtitulo);
				bean.setDigitoConfNossoNumeroBody(this.Modulo11Bradesco(carteira + idtitulo));
			}
			
			bean.setDescontoBonificacaoBody("0000000000");
			
			if(contacarteira.getIdemissaoboleto() != null && !contacarteira.getIdemissaoboleto().equals("")){
				bean.setCondicaoEmissaoBody(contacarteira.getIdemissaoboleto());
			} else {
				bean.setCondicaoEmissaoBody("2");
			}
			
			bean.setEmitePapeletaCobrancaBody("N");
			bean.setIdRateioCreditoBody(" ");
			bean.setEnderecamentoDebitoBody("2");
			bean.setIdOcorrenciaBody("01");
			bean.setNumeroDocumentoBody(d.getNumero()!=null ? SinedUtil.getNumeroDocumentoRemessa(d.getNumero()) : d.getCddocumento().toString());
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
//			bean.setValorBody(valorDebito.toString().replace(".", ""));
			bean.setValorBody(new Long(d.getValor().toLong()).toString());
			bean.setBancoCobrancaBody(codigoBanco);
			bean.setAgenciaDepositariaBody("");
			
			if(contacarteira.getEspeciedocumentoremessa() != null && !contacarteira.getEspeciedocumentoremessa().equals("")){
				bean.setEspecieTituloBody(contacarteira.getEspeciedocumentoremessa());
			} else {
				bean.setEspecieTituloBody("04");
			}
			
			if(contacarteira.getAceito() != null && !contacarteira.getAceito().equals("")){
				bean.setIdBody(contacarteira.getAceito());
			} else {
				bean.setIdBody("A");
			}
			
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			
			if(contacarteira.getProtestoautomaticoremessa() != null && contacarteira.getProtestoautomaticoremessa()){
				bean.setInstrucao1Body("06"); // INSTRU��O DE PROTESTO AUTOM�TICO
				
				if(contacarteira.getQtdedias() != null && contacarteira.getQtdedias() > 5 && contacarteira.getQtdedias() < 100){
					bean.setInstrucao2Body(contacarteira.getQtdedias().toString());
				} else {
					bean.setInstrucao2Body("05");
				}
			} else {
				if(contacarteira.getInstrucao1() != null && !contacarteira.getInstrucao1().equals("")){
					bean.setInstrucao1Body(contacarteira.getInstrucao1());
				} else {
					bean.setInstrucao1Body("08");
				}
				
				if(contacarteira.getInstrucao2() != null && !contacarteira.getInstrucao2().equals("")){
					bean.setInstrucao2Body(contacarteira.getInstrucao2());
				} else {
					bean.setInstrucao2Body("09");
				}
			}
			
			bean.setValorIOFBody("0");
			bean.setValorAbatimentoBody("0");
			
			if(contacarteira.getEnviarmoraremessa() != null && contacarteira.getEnviarmoraremessa()){
				Double valorMora = taxaitemService.getValorPercentualTipotaxa(d, Tipotaxa.JUROS, false);
				bean.setValorAtrasoBody(StringUtils.stringCheia(StringUtils.soNumero(df.format(valorMora)), "0", 13, false));
			} else {
				bean.setValorAtrasoBody("0");
			}
			
			bean.setDtLimiteDescontoBody("");
			if(d.getTaxa() != null){
				Taxaitem taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if(taxaitemDesconto != null && taxaitemDesconto.getDtlimite() != null){
					bean.setDtLimiteDescontoBody(format.format(taxaitemDesconto.getDtlimite()));
				}
			}
			if(contacarteira.getEnviardescontoremessa() != null && contacarteira.getEnviardescontoremessa()){
				Double valorDesconto = taxaitemService.getValorPercentualTipotaxa(d, Tipotaxa.DESCONTO, false);
				bean.setValorDescontoBody(StringUtils.stringCheia(StringUtils.soNumero(df.format(valorDesconto)), "0", 13, false));
			} else {
				bean.setValorDescontoBody("0");
			}
			
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", ""));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
//			bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			bean.setSequencialRegistroBody(sequencialregistro.toString());	
			
			Endereco endereco = d.getEndereco();
			if(endereco == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " n�o tem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(StringUtils.limpaNull(endereco.getLogradouro())+", "+
					StringUtils.limpaNull(endereco.getNumero())+", "+
					StringUtils.limpaNull(endereco.getComplemento())+", "+
					StringUtils.limpaNull(endereco.getBairro()));
			bean.setCepBody(endereco.getCep() != null ? endereco.getCep().toString().replace("-", "") : "");
			
			
			if(contacarteira.getIdemissaoboleto() != null && contacarteira.getIdemissaoboleto().equals("1")){
				sequencialregistro++;
				
				bean.setIdRegistroMensagem("2");
				bean.setMensagem1(d.getMensagem1() != null ? d.getMensagem1() : "");
				bean.setMensagem2(d.getMensagem4() != null ? d.getMensagem4() : "");
				bean.setMensagem3(d.getMensagem5() != null ? d.getMensagem5() : "");
				bean.setMensagem4(d.getMensagem6() != null ? d.getMensagem6() : "");
				bean.setSequenciaRegMensagem(sequencialregistro.toString());
			}
			
			listaRegistros.add(bean);
		}
		
		return listaRegistros;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Real
	 * Registros do tipo DETALHE e MENSAGEM
	 * @since 22/05/2010
	 * @author Taidson
	 */
	public List<ArquivoRemessaBean> setBodyMensagemRemessa400BancoReal(Conta conta, List<Documento> listaDocumento){
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		String codigoBanco = "356";
	//	String carteira = conta.getCarteira();
		String carteira = "00";
		String agenciaCedente = conta.getAgencia();
		String agenciaBody = "00000"; // ag�ncia cobradora
		String contacorrente = conta.getNumero();
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;

			bean = new ArquivoRemessaBean();	
			
			/*BODY*/
			bean.setIdRegistroBody("1");
			String cnpjCedenteAux = "";
			if(conta.getEmpresa().getCnpj()!=null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
				cnpjCedenteAux = conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", "");
				bean.setFilialCedenteBody(cnpjCedenteAux.substring(8, 12));
				bean.setControleCedenteBody(cnpjCedenteAux.substring(12));
			}
			bean.setAgenciaCedente(conta.getAgencia());
			bean.setContaCedente(conta.getNumero());
			bean.setNumeroDocumentoBody(d.getCddocumento().toString());
			bean.setIncidenciaMultaBody("0");
			bean.setNrDiasMultaBody("00");

			bean.setContratoBody(conta.getContacarteira().getConvenio());
			bean.setCarteiraBody(carteira);
			bean.setBancoCobrancaBody("5");
			bean.setIdOcorrenciaBody("01");
			bean.setNumeroDocumentoBody(d.getCddocumento().toString()); //N�mero t�tulo (Cedente)
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
			bean.setValorBody(new Long(d.getAux_documento().getValoratual().toLong()).toString());
			bean.setCdBancoBody(codigoBanco);
			bean.setAgenciaBody(agenciaBody);
			
			if(conta.getBancoemiteboleto().equals(true)){
				bean.setEspecieTituloBody("06");
			}else{
				bean.setEspecieTituloBody("07");
			}
			
			bean.setAceiteBody("N");
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			bean.setCodigoProtestoBody("99");
			Taxaitem taxaitemJuros = new Taxaitem();
			if(d.getTaxa() != null){
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					if(taxaitemJuros.isPercentual()){
						bean.setTipoJurosBody("1");
					}
					else{
						bean.setTipoJurosBody("0");
					}
					bean.setTaxaValorJurosBody(new Long(taxaitemJuros.getValor().toLong()).toString());
				}else{
					bean.setTipoJurosBody("1");
					bean.setTaxaValorJurosBody("0");
				}
			}else{
				bean.setTipoJurosBody("1");
				bean.setTaxaValorJurosBody("0");
			}
			bean.setDtLimiteDescontoBody(bean.getDtvencimentoBody());
			bean.setValorDescontoBody("0");
			bean.setValorAbatimentoBody("0");
			bean.setIdRegistroBody("1");
			String cpfAux = "";
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				cpfAux = d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", "");
				bean.setNumeroInscricaoSacadoBody("000" + cpfAux.substring(0, 11));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			Endereco endereco = d.getEndereco();
			if(endereco == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " est� sem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(StringUtils.limpaNull(endereco.getLogradouro())+", "+
					StringUtils.limpaNull(endereco.getNumero())+", "+
					StringUtils.limpaNull(endereco.getComplemento()));
			bean.setBairroBody(StringUtils.limpaNull(endereco.getBairro()));
			bean.setCepBody(endereco.getCep() != null ? endereco.getCep().toString().replace("-", "") : "");
			bean.setCidadeBody(StringUtils.limpaNull(endereco.getMunicipio().getNome()));
			bean.setUfBody(StringUtils.limpaNull(endereco.getMunicipio().getUf().getSigla()));
			
			bean.setNomeEmpresa(conta.getEmpresa().getRazaosocialOuNome());
			bean.setValorMoedaBody("0");
			bean.setTipoMoedaBody("07");

			/*Mensagens*/
			bean.setIdRegistroMensagem("8");
			bean.setSequenciaRegMensagem("1");
			bean.setCodLocalMensagem1("3");
			bean.setCodLocalMensagem2("3");
			bean.setCodLocalMensagem3("3");
			bean.setCodLocalMensagem4("3");
			bean.setCodLocalMensagem5("3");
			bean.setAgenciaCedente(agenciaCedente); 								//Ag�ncia cedente
			bean.setContaBody(contacorrente); 							// Conta cedente
			bean.setNumeroDocumentoBody(d.getCddocumento().toString()); //N�mero t�tulo (Cedente)
			bean.setMensagem1(d.getMensagem1()); 
			bean.setMensagem2(d.getMensagem2());
			bean.setMensagem3(d.getMensagem3());
			bean.setMensagem4(d.getMensagem4());
			bean.setMensagem5(d.getMensagem5());
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			sequencialregistro++;
			bean.setNumSequencialRegistroMensagemBody(sequencialregistro.toString());
			listaRegistros.add(bean);
		}
		
		return listaRegistros;
	}
	
	public String Modulo11Bradesco(String numero) {
		int indexador;
		int soma      = 0;
		int mult      = 0;
		String result = "";		
		int tamanho = numero.length();
	
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 6) + 2; //Sequ�ncia de 2 a 7
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = String.valueOf(11 - (soma % 11));
		if (result.equals("10")) result = "P";
		else if (result.equals("11")) result = "0";			
		return result;
	} //Modulo11	
	
	@SuppressWarnings("unchecked")
	public List<ArquivoRemessaBean> setBodyRegistroBaixa400(Conta conta, List<Documento> listaDocumentoRemessa) {
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		String codigoBanco = "237";
		String carteira = conta.getContacarteira().getCarteira();
		String agencia = conta.getAgencia();
		String contacorrente = conta.getNumero();
		String dgcontacorrente = conta.getDvnumero();
//		Double valorTotal = new Double(0.0);
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		List<Endereco> arrayEndereco;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumentoRemessa) {
			sequencialregistro++;
//			Double valorDebito = new Double(0.0);
//			valorDebito+=d.getAux_documento().getValoratual().getValue().doubleValue();
			
//			valorTotal += valorDebito;

			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("1");
			bean.setCarteiraBody(carteira);
			bean.setAgenciaBody(agencia);
			bean.setContaBody(contacorrente);
			bean.setDigitoContaBody(dgcontacorrente);
//			bean.setIdEmpresaBody("0"+carteira+agencia+contacorrente+dgcontacorrente);
			bean.setControleParticipanteBody(d.getCddocumento().toString());
			bean.setCdBancoBody(codigoBanco);
			bean.setCampoMultaBody("0");
			bean.setPercentualMultaBody("0000");
			String idtitulo = StringUtils.stringCheia(d.getCddocumento().toString(), "0", 11, false);
			bean.setIdTituloBancoBody(idtitulo);
			bean.setDigitoConfNossoNumeroBody(this.Modulo11Bradesco(carteira + idtitulo));
			bean.setDescontoBonificacaoBody("0000000000");
			bean.setCondicaoEmissaoBody("2");
			bean.setEmitePapeletaCobrancaBody("N");
			bean.setIdRateioCreditoBody(" ");
			bean.setEnderecamentoDebitoBody("2");
			bean.setIdOcorrenciaBody("02");
			bean.setNumeroDocumentoBody(d.getCddocumento().toString());
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
//			bean.setValorBody(valorDebito.toString().replace(".", ""));
			bean.setValorBody(new Long(d.getAux_documento().getValoratual().toLong()).toString());
			bean.setBancoCobrancaBody(codigoBanco);
			bean.setAgenciaDepositariaBody(agencia);
			bean.setEspecieTituloBody("04");
			bean.setIdBody("A");
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			bean.setInstrucao1Body("08");
			bean.setInstrucao2Body("09");
			bean.setDtLimiteDescontoBody(bean.getDtvencimentoBody());
			
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", ""));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
			bean.setNomeSacadoBody(d.getPessoa().getNome());
			bean.setSequencialRegistroBody(sequencialregistro.toString());	
			arrayEndereco = (List<Endereco>) d.getPessoa().getListaEndereco();
			if(arrayEndereco != null && arrayEndereco.size() > 0){
				bean.setEnderecoSacadoBody(StringUtils.limpaNull(arrayEndereco.get(0).getLogradouro())+", "+
						StringUtils.limpaNull(arrayEndereco.get(0).getNumero())+", "+
						StringUtils.limpaNull(arrayEndereco.get(0).getComplemento())+", "+
						StringUtils.limpaNull(arrayEndereco.get(0).getBairro()));
				bean.setCepBody(arrayEndereco.get(0).getCep().toString().replace("-", ""));
			}
			listaRegistros.add(bean);
		}
		
		return listaRegistros;
	}

	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco ITAU
	 * Registros do tipo DETALHE e MENSAGEM
	 * 
	 * @author Tom�s Rabelo
	 * @param filtro 
	 */	
	public List<ArquivoRemessaBean> setBodyMensagemRemessa400BancoItau(Conta conta, List<Documento> listaDocumento, GerarArquivoRemessaBean filtro) throws Exception{
		Contacarteira contacarteira = conta.getContacarteira();
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		String codigoBanco = "341";
		String carteira = contacarteira.getCarteira();
		String agencia = conta.getAgencia();
		String contacorrente = conta.getNumero();
		String dgcontacorrente = conta.getDvnumero();
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;
	
			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("1");
			if(conta.getEmpresa() != null && conta.getEmpresa().getCnpj()!=null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			bean.setAgenciaBody(agencia);
			bean.setContaBody(contacorrente);
			bean.setDacEmpresaBody(dgcontacorrente); //Verificar se est� certo
			//C�digo de alega��o vai com 0 9(4)
			//Uso da empresa vai em branco x(25)
			bean.setUsoEmpresaBody(d.getCddocumento().toString());
			bean.setNumeroDocumentoBody(d.getNumero()!=null ? SinedUtil.getNumeroDocumentoRemessa(d.getNumero()) : d.getCddocumento().toString());
			
			if(contacarteira.getBancogeranossonumero() != null && contacarteira.getBancogeranossonumero()){
				bean.setNossoNumeroBody("");
			} else {
				if (conta.getNossonumerointervalo() != null && conta.getNossonumerointervalo()) {
					if (d.getNossonumero() == null)
						throw new SinedException("O documento " + d.getCddocumento() + " n�o possui nosso n�mero gerado.");
					bean.setNossoNumeroBody(d.getNossonumero());
				} else
					bean.setNossoNumeroBody(d.getCddocumento().toString());
			}
			//Qtde moeda vari�vel 9(13)
			bean.setCarteiraBody(carteira);
			//Uso do banco identifica��o da opera��o
			bean.setCodigoCarteiraBody(contacarteira.getCodigocarteira() != null ? contacarteira.getCodigocarteira() : " ");//Carteira 109 e 112
			bean.setIdOcorrenciaBody("01");
			//N� do documento � o mesmo do nosso n�
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
			bean.setValorBody(new Long(d.getValor().toLong()).toString());
			bean.setCdBancoBody(codigoBanco);
			//Ag�ncia cobradora vai com 0 na remessa
			bean.setEspecieTituloBody("01"); //Duplicata mercantil???
			bean.setAceiteBody("N"); //O que � isso?!
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			bean.setInstrucao1Body(contacarteira.getInstrucao1() != null ? contacarteira.getInstrucao1() : "");//Confirmar isso
			bean.setInstrucao2Body(contacarteira.getInstrucao2() != null ? contacarteira.getInstrucao2() : "");//Confirmar isso
			bean.setTaxaValorJurosBody("");
			bean.setDtCobrancaMulta("");
			
			if(d.getTaxa() != null){
				Taxaitem taxaitemJuros = new Taxaitem();
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					Money valorJuros = new Money(0); 
					
					if(taxaitemJuros.isPercentual()) valorJuros = new Money(d.getValor().multiply(taxaitemJuros.getValor().divide(new Money(100))));
					else valorJuros = new Money(taxaitemJuros.getValor());
					
					bean.setTaxaValorJurosBody(new Long(valorJuros.toLong()).toString());
					bean.setDtCobrancaMulta(padronizaData(taxaitemJuros.getDtlimite()));
				}
			}
			
			bean.setDtLimiteDescontoBody("");
			bean.setValorDescontoBody("0");
			if(d.getTaxa() != null){
				Taxaitem taxaitemDesconto = new Taxaitem();
				taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if(taxaitemDesconto != null && taxaitemDesconto.getTipotaxa() != null && taxaitemDesconto.getTipotaxa().getCdtipotaxa() != null){
					Money valorDesconto = new Money(0); 
					if(taxaitemDesconto.isPercentual())
						valorDesconto = new Money(d.getValor().multiply(taxaitemDesconto.getValor().divide(new Money(100))));
					else
						valorDesconto = new Money(taxaitemDesconto.getValor());
					bean.setValorDescontoBody(new Long(valorDesconto.toLong()).toString());
					if (taxaitemDesconto.getDtlimite() != null)
						bean.setDtLimiteDescontoBody(format.format(taxaitemDesconto.getDtlimite()));
				}
			}
			
			//Valor IOF 0 
			bean.setValorAbatimentoBody("0");
			String cpfAux = "";
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				cpfAux = d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", "");
				bean.setNumeroInscricaoSacadoBody("000" + cpfAux.substring(0, 11));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			Endereco endereco = d.getEndereco();
			if(endereco == null || endereco.getCdendereco() == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " est� sem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(StringUtils.limpaNull(endereco.getLogradouro())+", "+
					StringUtils.limpaNull(endereco.getNumero())+", "+
					StringUtils.limpaNull(endereco.getComplemento()));
			bean.setBairroBody(StringUtils.limpaNull(endereco.getBairro()));
			bean.setCepBody(endereco.getCep() != null ? endereco.getCep().toString().replace("-", "") : "");
			bean.setCidadeBody(StringUtils.limpaNull(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : ""));
			bean.setUfBody(StringUtils.limpaNull(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : ""));
			
			if ((contacarteira.getInstrucao1() != null && contacarteira.getInstrucao1().equals("93")) || (contacarteira.getInstrucao2() != null && contacarteira.getInstrucao2().equals("93")))
				bean.setNomeSacadorAvalista(d.getMensagem1() != null ? d.getMensagem1().substring(0,d.getMensagem1().length() > 30 ? 30 : d.getMensagem1().length()) : "");
			
			if ((contacarteira.getInstrucao1() != null && contacarteira.getInstrucao1().equals("94")) || (contacarteira.getInstrucao2() != null && contacarteira.getInstrucao2().equals("94")))
				bean.setNomeSacadorAvalista(d.getMensagem1() != null ? d.getMensagem1().substring(0,d.getMensagem1().length() > 40 ? 40 : d.getMensagem1().length()) : "");
			
			bean.setPrazoBody(contacarteira.getQtdedias() != null ? contacarteira.getQtdedias().toString() : "00");
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			listaRegistros.add(bean);
		}
		return listaRegistros;
	}

	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 240 registrada Banco do Brasil
	 * Registros do tipo HEADER 
	 * @author Marden Silva
	 */
	public List<ArquivoRemessa240Bean> setHeaderRemessa240BancoBrasil(Conta conta, GerarArquivoRemessaBean filtro){
		List<ArquivoRemessa240Bean> listaHeader = new ArrayList<ArquivoRemessa240Bean>();		
		SimpleDateFormat formatData = new SimpleDateFormat("ddMMyyyy");
		SimpleDateFormat formatHora = new SimpleDateFormat("hhmmss");
		Date date = new Date(System.currentTimeMillis());
		String data = formatData.format(date);
		String hora = formatHora.format(date);
		String codigoBanco = "001";
		String nomeBanco = "BANCO DO BRASIL S.A.";
		String tipoInscEmpresa = (conta.getEmpresa().getCpf() != null ? "1" : "2");
		ArquivoRemessa240Bean bean = null;
		
		//Header Arquivo
		bean = new ArquivoRemessa240Bean();
		bean.setCdBanco(codigoBanco);
		bean.setLoteServico("0000");
		bean.setTipoRegistro("0");
		bean.setTipoInscEmpresa(tipoInscEmpresa);
		bean.setNumInscEmpresa(conta.getEmpresa().getCpfOuCnpj().replaceAll("[^0-9]", ""));
		bean.setCdConvenioBanco(conta.getContacarteira().getConvenio());
		bean.setCdCarteiraBanco(filtro.getContacarteira().getCarteira());
		bean.setVariacaoCarteiraBanco(conta.getContacarteira().getVariacaocarteira());
		bean.setNumagenciaCedente(conta.getAgencia());
		bean.setDvagenciaCedente(conta.getDvagencia());
		bean.setNumContaCedente(conta.getNumero());
		bean.setDvContaCedente(conta.getDvnumero());
		bean.setNomeCedente(contacorrenteService.getNomeCedente(conta));
		bean.setNomeBanco(nomeBanco);
		bean.setCdRemessa("1");
		bean.setDtGeracao(data);
		bean.setHoraGeracao(hora);
		bean.setNumSeqArquivo(conta.getSequencial().toString());
		bean.setVersaoLayoutArquivo("080");
		listaHeader.add(bean);
		
		//Header Lote
		bean = new ArquivoRemessa240Bean();
		bean.setCdBanco(codigoBanco);
		bean.setLoteServico("0001");
		bean.setTipoRegistro("1");
		bean.setTipoOperacao("R");
		bean.setTipoServico("01");
		bean.setVersaoLayoutLote("042");
		bean.setTipoInscEmpresa(tipoInscEmpresa);
		bean.setNumInscEmpresa(conta.getEmpresa().getCpfOuCnpj().replaceAll("[^0-9]", ""));
		bean.setCdConvenioBanco(conta.getContacarteira().getConvenio());
		bean.setCdCarteiraBanco(conta.getContacarteira().getCarteira());
		bean.setVariacaoCarteiraBanco(conta.getContacarteira().getVariacaocarteira());
		bean.setNumagenciaCedente(conta.getAgencia());
		bean.setDvagenciaCedente(conta.getDvagencia());
		bean.setNumContaCedente(conta.getNumero());
		bean.setDvContaCedente(conta.getDvnumero());
		bean.setNomeCedente(conta.getEmpresa().getRazaosocialOuNome().toUpperCase());
		bean.setDtGravacao(data);		
		listaHeader.add(bean);		
		
		return listaHeader;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 240 registrada Banco do Brasil
	 * Registros do tipo DETALHE
	 * 
	 * @author Marden Silva
	 * @param conta, listaDocumento, filtro 
	 */	
	public List<ArquivoRemessa240Bean> setBodyRemessa240BancoBrasil(Conta conta, List<Documento> listaDocumento, GerarArquivoRemessaBean filtro) throws Exception{
		Contacarteira contacarteira = conta.getContacarteira();
		String convenio = contacarteira.getConvenio();
		
		// Data atual do sistema
		SimpleDateFormat formatData = new SimpleDateFormat("ddMMyyyy");
		String codigoBanco = "001";
				
		List<ArquivoRemessa240Bean> listaBody = new ArrayList<ArquivoRemessa240Bean>();
		ArquivoRemessa240Bean bean = null;
		Integer sequencialregistro = 0;
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "pessoa.cdpessoa", ",");
		List<Cliente> listaCliente = clienteService.findByWherein(whereIn);
		
		
		for (Documento d : listaDocumento) {
			for(Cliente c : listaCliente){
				if(d.getPessoa().getCdpessoa().equals(c.getCdpessoa())){
					d.setCliente(c);
					break;
				}
			}
			//Segmento P
			sequencialregistro++;			
			bean = new ArquivoRemessa240Bean();
			bean.setCdBanco(codigoBanco);
			bean.setLoteServico("0001");
			bean.setTipoRegistro("3");
			bean.setNumSequencial(sequencialregistro.toString());
			bean.setCdSegmento("P");
			bean.setCdMovimentoRemessa("01");
			bean.setNumagenciaCedente(conta.getAgencia());
			bean.setDvagenciaCedente(conta.getDvagencia());
			bean.setNumContaCedente(conta.getNumero());
			bean.setDvContaCedente(conta.getDvnumero());
			bean.setCdConvenioBanco(convenio);
			bean.setIdTituloBanco(d.getCddocumento().toString());
			if (contacarteira.getBancogeranossonumero())
				bean.setIdTituloBanco("00000000000000000000");
			else {
				if(convenio != null && convenio.length() == 6){
					bean.setIdTituloBanco(bean.getNossonumeroConv6());
				} else {
					bean.setIdTituloBanco(bean.getNossonumeroConv7());
				}
			}
			bean.setCdCarteira(contacarteira.getCodigocarteira()); //C�digo da carteira
			bean.setFormaCadastroTitulo("1");
			bean.setTipoDocumento("1");
			bean.setIdEmissaoBloqueto(contacarteira.getIdemissaoboleto());
			bean.setIdDistribuicao(contacarteira.getIddistribuicao());		
			if(conta.getConsiderarnumerodocumentoremessa() != null && conta.getConsiderarnumerodocumentoremessa() && 
					org.apache.commons.lang.StringUtils.isNotEmpty(d.getNumero())){
				bean.setNumDocumentoCobranca(SinedUtil.getNumeroDocumentoRemessa(d.getNumero().toString())); //Campo n�mero do documento do boleto
			}else {
				bean.setNumDocumentoCobranca(d.getCddocumento().toString()); //Campo n�mero do documento do boleto
			}
			bean.setDtVencimento(formatData.format(d.getDtvencimento()));
			bean.setVrNominal(new Long(d.getValor().toLong()).toString());
			bean.setEspecieTitulo("02"); //Qual a esp�cie do t�tulo? DUPLICATA MERCANTIL
			bean.setIdTituloAceite("N"); //Identifica��o de t�tulo aceite ou n�o
			bean.setDataEmissao(formatData.format(d.getDtemissao()));
			
			bean.setCdJurosMora("1");
			bean.setDtJurosMora(formatData.format(d.getDtvencimento()));
			bean.setTaxaJurosMora("0");
			
			if(d.getTaxa() != null){
				Taxaitem taxaitemJuros = new Taxaitem();
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					if(taxaitemJuros.isPercentual())
						bean.setCdJurosMora("2");
					else
						bean.setCdJurosMora("1");
					bean.setTaxaJurosMora(new Long(taxaitemJuros.getValor().toLong()).toString());
					if (taxaitemJuros.getDtlimite() != null)
						bean.setDtJurosMora(formatData.format(taxaitemJuros.getDtlimite()));
				}
				
			}
			
			bean.setCdDesconto("0");
			bean.setDtDesconto("0");
			bean.setVrDesconto("0");
			
			if(d.getTaxa() != null){
				Taxaitem taxaitemDesconto = new Taxaitem();
				taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if(taxaitemDesconto != null && taxaitemDesconto.getTipotaxa() != null && taxaitemDesconto.getTipotaxa().getCdtipotaxa() != null){
					if(taxaitemDesconto.isPercentual())
						bean.setCdDesconto("2");
					else
						bean.setCdDesconto("1");					
					bean.setVrDesconto(new Long(taxaitemDesconto.getValor().toLong()).toString());
					if (taxaitemDesconto.getDtlimite() != null)
						bean.setDtDesconto(formatData.format(taxaitemDesconto.getDtlimite()));
				}
			}
						
			bean.setVrIOF("0");
			bean.setVrAbatimento("0");
			if (contacarteira.getQtdedias() != null && contacarteira.getQtdedias() > 0){
				bean.setCdProtesto("2");
				bean.setNumDiasProtesto(contacarteira.getQtdedias().toString());
			} else {
				bean.setCdProtesto("3");
				bean.setNumDiasProtesto("0");
			}
			bean.setCdBaixa("0");
			bean.setCdMoeda("09");	
			bean.setIdTituloEmpresa(d.getCddocumento().toString());
			bean.setNumContratoCobranca("0");
			listaBody.add(bean);
			
			//Segmento Q
			sequencialregistro++;
			bean = new ArquivoRemessa240Bean();
			bean.setCdBanco(codigoBanco);
			bean.setLoteServico("0001");
			bean.setTipoRegistro("3");
			bean.setNumSequencial(sequencialregistro.toString());
			bean.setCdSegmento("Q");
			bean.setCdMovimentoRemessa("01");	
			if (d.getPessoa() != null) {
				bean.setTipoInscSacado(d.getPessoa().getCpf() != null ? "1" : "2");
				bean.setNumInscSacado(d.getPessoa().getCpfOuCnpj() != null ? d.getPessoa().getCpfOuCnpj().toString().replaceAll("[^0-9]", "") : "0");
				
				String nomeFantasia = null;
				String razaoSocial = null;
				if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
					if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
						Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
						if(cliente != null){
							nomeFantasia = cliente.getNome();
							razaoSocial = cliente.getRazaosocial();
						}
					}else {
						nomeFantasia = d.getPessoa().getRazaoSocial();
					}
				}
				
				if(razaoSocial != null && !"".equals(razaoSocial)){
					bean.setNomeSacado(razaoSocial);
				}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
					bean.setNomeSacado(nomeFantasia);
				}else {
					bean.setNomeSacado(d.getPessoa().getRazaoSocial());
				}
			}
			if (d.getEndereco() != null) {
				bean.setEnderecoSacado(d.getEndereco().getLogradouroCompleto());
				bean.setBairroSacado(d.getEndereco().getBairro() != null ? d.getEndereco().getBairro() : "");
				bean.setCepSacado(d.getEndereco().getCep() != null ? d.getEndereco().getCep().toString().replace("-", "") : "");
				bean.setCidadeSacado(d.getEndereco().getMunicipio() != null ? d.getEndereco().getMunicipio().getNome() : "");
				bean.setUfSacado(d.getEndereco().getMunicipio() != null && d.getEndereco().getMunicipio().getUf() != null &&
						         d.getEndereco().getMunicipio().getUf().getSigla() != null ? d.getEndereco().getMunicipio().getUf().getSigla() : "");
			}			
			bean.setTipoInscAvalista("0");
			bean.setNumInscAvalista("0");
			listaBody.add(bean);
			
			//Segmento R
			sequencialregistro++;
			bean = new ArquivoRemessa240Bean();
			bean.setCdBanco(codigoBanco);
			bean.setLoteServico("0001");
			bean.setTipoRegistro("3");
			bean.setNumSequencial(sequencialregistro.toString());
			bean.setCdSegmento("R");
			bean.setCdMovimentoRemessa("01");
			
			bean.setCdmulta("0");
			bean.setDatamulta("0");
			bean.setValormulta("0");
			
			if(d.getTaxa() != null){
				Taxaitem taxaitemMulta = new Taxaitem();
				taxaitemMulta = taxaitemService.findByTaxaMulta(d.getTaxa());
				if(taxaitemMulta != null && taxaitemMulta.getTipotaxa() != null && taxaitemMulta.getTipotaxa().getCdtipotaxa() != null){
					if(taxaitemMulta.isPercentual())
						bean.setCdmulta("2");
					else
						bean.setCdmulta("1");					
					bean.setValormulta(new Long(taxaitemMulta.getValor().toLong()).toString());
					if (taxaitemMulta.getDtlimite() != null)
						bean.setDatamulta(formatData.format(taxaitemMulta.getDtlimite()));
					else 
						bean.setDatamulta(formatData.format(d.getDtvencimento()));
				}
			}
			
			bean.setMensagem3(d.getMensagem1() != null ? d.getMensagem1() : "");
			listaBody.add(bean);
		}
		return listaBody;
	}	
	
	public List<ArquivoRemessa240Bean> setTrillerRemessa240BancoBrasil(Integer totalLinhas) {
		String codigoBanco = "001";
		List<ArquivoRemessa240Bean> listaTriller = new ArrayList<ArquivoRemessa240Bean>();
		ArquivoRemessa240Bean bean = null;
		
		//Triller de lote
		bean = new ArquivoRemessa240Bean();
		bean.setCdBanco(codigoBanco);
		bean.setLoteServico("0001");
		bean.setTipoRegistro("5");
		bean.setTotalLinhaLote(totalLinhas.toString());
		listaTriller.add(bean);
		
		//Triller de arquivo
		bean = new ArquivoRemessa240Bean();
		bean.setCdBanco(codigoBanco);
		bean.setLoteServico("9999");
		bean.setTipoRegistro("9");
		bean.setTotalLoteArquivo("1");
		totalLinhas++; totalLinhas++;
		bean.setTotalLinhaArquivo(totalLinhas.toString());
		listaTriller.add(bean);
		
		return listaTriller;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 Banco Brasil
	 * Registros do tipo DETALHE
	 * 
	 * @author Marden Silva
	 * @param filtro 
	 */	
	public List<ArquivoRemessaBean> setBodyRemessa400BancoBrasil(Conta conta, List<Documento> listaDocumento, GerarArquivoRemessaBean filtro) throws Exception{
		Contacarteira contacarteira = conta.getContacarteira();
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		String codigoBanco = "001";
		String carteira = contacarteira.getCarteira();
		String agencia = conta.getAgencia();
		String dvAgencia = conta.getDvagencia();
		String contacorrente = conta.getNumero();
		String dgcontacorrente = conta.getDvnumero();
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;
	
			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("7");
			if(conta.getEmpresa().getCnpj()!=null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			bean.setAgenciaBody(agencia);
			bean.setDvAgenciaBody(dvAgencia);
			bean.setContaBody(contacorrente);
			bean.setDacEmpresaBody(dgcontacorrente); 
			bean.setNumConvenioBody(contacarteira.getConvenio());
			if(conta.getConsiderarnumerodocumentoremessa() != null && conta.getConsiderarnumerodocumentoremessa() && 
					org.apache.commons.lang.StringUtils.isNotEmpty(d.getNumero())){
				bean.setNumeroDocumentoBody(SinedUtil.getNumeroDocumentoRemessa(d.getNumero().toString()));
			}else {
				bean.setNumeroDocumentoBody(d.getCddocumento().toString()); 
			}
			
			if (!contacarteira.getBancogeranossonumero())
				bean.setNossoNumeroBody(bean.getNossonumeroConv7(d.getCddocumento().toString()));
			else
				bean.setNossoNumeroBody("00000000000000000");		
			bean.setVariacaoCarteiraBody(contacarteira.getVariacaocarteira() /*"019"*/); //Informa��o fornecida pelo banco
			bean.setTipoCobrancaBody(contacarteira.getTipocobranca()); 
			bean.setCarteiraBody(carteira);
			bean.setComandoBody("01"); //01 - Registro de t�tulos			
			
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
			bean.setValorBody(d.getValor().toString().replace(".", "").replace(",", ""));
			bean.setCdBancoBody(codigoBanco);
			bean.setEspecieTituloBody("01");//SEMPRE SER� DM 
			bean.setAceiteBody(contacarteira.getAceito() != null ? contacarteira.getAceito() : "N"); 
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			bean.setInstrucao1Body(contacarteira.getInstrucao1());
			bean.setInstrucao2Body(contacarteira.getInstrucao2());
			bean.setTaxaValorJurosBody("0");// Inicializa Juros como zero
			bean.setValorDescontoBody("0");// Inicializa desconto com zero 
			Taxaitem taxaitemJuros = new Taxaitem();
			if(d.getTaxa() != null){
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					if (taxaitemJuros.isPercentual()){
						bean.setTaxaValorJurosBody(SinedUtil.calculaValorPorcentagem(d.getValor(), taxaitemJuros.getValor()));
					} else {
						bean.setTaxaValorJurosBody(new Long(taxaitemJuros.getValor().toLong()).toString().replace(".", "").replace(",", ""));
					}
				}
				if(bean.getTaxaValorJurosBody() == null || bean.getTaxaValorJurosBody().equals("")){
					bean.setTaxaValorJurosBody("0");
				}
			}
			Taxaitem taxaItemDesconto = new Taxaitem();
			if (d.getTaxa() != null){
				taxaItemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if (taxaItemDesconto != null){
					bean.setDtLimiteDescontoBody(taxaItemDesconto != null ? format.format(taxaItemDesconto.getDtlimite()): "");
					if (taxaItemDesconto.isPercentual()){
						bean.setValorDescontoBody(SinedUtil.calculaValorPorcentagem(d.getValor(), taxaItemDesconto.getValor()) != null ? SinedUtil.calculaValorPorcentagem(d.getValor(), taxaItemDesconto.getValor()) : "0" );
					} else {
						bean.setValorDescontoBody(taxaItemDesconto != null ? taxaItemDesconto.getValor().toString().replace(".", "").replace(",", "") : "0");
					}
					if(bean.getValorDescontoBody() == null || bean.getValorDescontoBody().equals("")){
						bean.setValorDescontoBody("0");
					}
				}
			}
			bean.setValorIOFBody("0");
			bean.setValorAbatimentoBody("0");
			String cpfAux = "";
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				cpfAux = d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", "");
				bean.setNumeroInscricaoSacadoBody("000" + cpfAux.substring(0, 11));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			} else{
				bean.setIdTipoInscricaoBody("00");
				bean.setNumeroInscricaoSacadoBody("0");
			}
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			Endereco endereco = d.getEndereco();
			if(endereco == null || endereco.getCdendereco() == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " est� sem endere�o de cobran�a cadastrado.");
			String logradouro = "";
			if (endereco.getLogradouro() != null && endereco.getLogradouro().length() > 32){
				logradouro += endereco.getLogradouro().substring(0, 32);
			} else if (endereco.getLogradouro() != null && endereco.getLogradouro().length() <= 32){
				logradouro += endereco.getLogradouro();
			}
			bean.setEnderecoSacadoBody(
					((!logradouro.equals("")? logradouro.replace(",", "") + " "  : "") +
					 (endereco.getNumero() != null && !endereco.getNumero().equals("") ? endereco.getNumero() + " " : "") +
					 (endereco.getComplemento() != null && !endereco.getComplemento().equals("") ? endereco.getComplemento() + " " : "")));
			bean.setBairroBody(endereco.getBairro() != null ? endereco.getBairro() : "");
			bean.setCepBody(endereco.getCep() != null ? endereco.getCep().toString().replace("-", "") : "");
			bean.setCidadeBody(StringUtils.limpaNull(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : ""));
			bean.setUfBody(StringUtils.limpaNull(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : ""));
			bean.setMensagem1Body(d.getMensagem1());
			bean.setNumDiasProtesto(contacarteira.getQtdedias() != null ? contacarteira.getQtdedias().toString() : "");
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			listaRegistros.add(bean);
		}
		return listaRegistros;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 Banco Brasil
	 * Registros do tipo HEADER e TRAILLER
	 * @author Marden Silva
	 */
	public ArquivoRemessaBean setHeaderTraillerRemessa400BancoBrasil(Conta conta, Integer sequencialRegistro){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeBanco = "001BANCODOBRASIL";
		String agenciaCedente = conta.getAgencia();
		String dvAgenciaCedente = conta.getDvagencia();
		String contaCedente = conta.getNumero();
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
		
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRANCA");
		ht.setAgenciaCedente(agenciaCedente);
		ht.setDvAgenciaCedente(dvAgenciaCedente);
		ht.setContaCedente(contaCedente);
		ht.setDacCedente(conta.getDvnumero().substring(conta.getDvnumero().length()-1, conta.getDvnumero().length())); //Verificar se est� certo
		ht.setNomeCedente(nomeCedente);
		ht.setNomeBanco(nomeBanco);
		ht.setNumConvenio(conta.getContacarteira().getConveniolider());	
		ht.setDtMovimento(data); 
		ht.setNumSequencialArquivo("000001");
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		ht.setIdRegistroTrailler("9");
		sequencialRegistro = sequencialRegistro+2;
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		
		return ht;
	}	
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Santander
	 * Registros do tipo HEADER e TRAILLER
	 * @since 15/07/2011
	 * @author Marden Silva
	 */
	public ArquivoRemessaBean setHeaderTraillerRemessa400BancoSantander(Conta conta, Integer sequencialRegistro, Long vrTotal){
		Contacarteira contacarteira = conta.getContacarteira();
		
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeBanco = "SANTANDER";
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
				
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRAN�A");	
		ht.setCodigoTransmissao(contacarteira.getCodigotransmissao());
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco(conta.getBanco().getNumero().toString());
		ht.setNomeBanco(nomeBanco);
		ht.setDtMovimento(data);
		ht.setMensagem1(contacarteira.getMsgboleto1());
		ht.setMensagem2(contacarteira.getMsgboleto2());
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		ht.setIdRegistroTrailler("9");
		sequencialRegistro=(sequencialRegistro*2)+2;
		ht.setQtdeTitulos(sequencialRegistro.toString());
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		ht.setVrTotal(vrTotal.toString());
		
		return ht;
	}	
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Santander
	 * Registros do tipo DETALHE
	 * @since 15/07/2011
	 * @author Marden Silva
	 */	
	public List<ArquivoRemessaBean> setBodyRemessa400BancoSantander(Conta conta, List<Documento> listaDocumento){
		Contacarteira contacarteira = conta.getContacarteira();
		
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;

			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("1");
			
			if(conta.getEmpresa().getCnpj()!=null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}			
			
			bean.setCodigoTransmissao(contacarteira.getCodigotransmissao());			
			bean.setControleParticipanteBody(d.getCddocumento().toString());
			
			if (contacarteira.getBancogeranossonumero() != null && contacarteira.getBancogeranossonumero()){ 
				bean.setNossoNumeroBody("00000000");
			}else{
				bean.setNossoNumeroBody(d.getCddocumento().toString() + Modulos.modulo11(d.getCddocumento().toString(), 6));
				//bean.setNossoNumeroBody(d.getCddocumento().toString() + modulo11Santander(d.getCddocumento().toString()));
			}
			bean.setCodigoCarteiraBody(contacarteira.getCodigocarteira());
			bean.setIdOcorrenciaBody("01"); //Entrada de titulo
			bean.setNumeroDocumentoBody(d.getCddocumento().toString());
			bean.setDtvencimentoBody(format.format(d.getDtvencimento()));
			bean.setValorBody(new Long(d.getValor().toLong()).toString());
			bean.setCdBancoBody(conta.getBanco().getNumero().toString());
			if (contacarteira.getCodigocarteira() != null && contacarteira.getCodigocarteira().equals("5")) {
				if (conta.getAgencia().length() < 4)
					bean.setAgenciaBody(conta.getAgencia() + conta.getDvagencia());
				else
					bean.setAgenciaBody(conta.getAgencia().substring(0,4));			
			} else
				bean.setAgenciaBody("00000");
			bean.setEspecieTituloBody("01");
			bean.setAceiteBody("N");
			bean.setDtemissaoBody(format.format(d.getDtemissao()));
			bean.setInstrucao1Body(contacarteira.getInstrucao1() != null ? contacarteira.getInstrucao1() : "00" );
			bean.setInstrucao2Body(contacarteira.getInstrucao2() != null ? contacarteira.getInstrucao2() : "00" );
			
			bean.setDtLimiteDescontoBody("0");
			bean.setValorDescontoBody("0");
			if(d.getTaxa() != null){
				Taxaitem taxaitemDesconto = new Taxaitem();
				taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if(taxaitemDesconto != null && taxaitemDesconto.getTipotaxa() != null && taxaitemDesconto.getTipotaxa().getCdtipotaxa() != null){
					Money valorDesconto = new Money(0); 
					if(taxaitemDesconto.isPercentual())
						valorDesconto = new Money(d.getValor().multiply(taxaitemDesconto.getValor().divide(new Money(100))));
					else
						valorDesconto = new Money(taxaitemDesconto.getValor());
					bean.setValorDescontoBody(new Long(valorDesconto.toLong()).toString());
					if (taxaitemDesconto.getDtlimite() != null)
						bean.setDtLimiteDescontoBody(format.format(taxaitemDesconto.getDtlimite()));
				}
			}
			
			bean.setTipoMultaBody("0");
			bean.setPercentualMultaBody("0");
			bean.setDtCobrancaMulta("0");
			if(d.getTaxa() != null){
				Taxaitem taxaitemMulta = new Taxaitem();
				taxaitemMulta = taxaitemService.findByTaxaMulta(d.getTaxa());
				if(taxaitemMulta != null && taxaitemMulta.getTipotaxa() != null && taxaitemMulta.getTipotaxa().getCdtipotaxa() != null){
					Money valorMulta = new Money(0); 
					if(taxaitemMulta.isPercentual())
						valorMulta = new Money(taxaitemMulta.getValor());
					else
						valorMulta = new Money(taxaitemMulta.getValor().divide(d.getValor()).multiply(new Money(100)));
					bean.setTipoMultaBody("4");
					bean.setPercentualMultaBody(new Long(valorMulta.toLong()).toString());
					if (taxaitemMulta.getDtlimite() != null && !taxaitemMulta.getDtlimite().equals(d.getDtvencimento()))
						bean.setDtCobrancaMulta(format.format(taxaitemMulta.getDtlimite()));
				}
			}
			
			bean.setTaxaValorJurosBody("0");
			if(d.getTaxa() != null){
				Taxaitem taxaitemJuros = new Taxaitem();
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					Money valorJuros = new Money(0); 
					if(taxaitemJuros.isPercentual())
						valorJuros = new Money(d.getValor().multiply(taxaitemJuros.getValor().divide(new Money(100))));
					else
						valorJuros = new Money(taxaitemJuros.getValor());
					bean.setTaxaValorJurosBody(new Long(valorJuros.toLong()).toString());
				}
			}
			
			bean.setValorAtrasoBody("0");
			bean.setValorIOFBody("0");
			bean.setValorAbatimentoBody("0");
					
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", ""));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
			bean.setNomeSacadoBody(d.getPessoa().getNome());				
			Endereco endereco = d.getEndereco();
			if(endereco == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " n�o tem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(
					((endereco.getLogradouro() != null ? endereco.getLogradouro() + ", "  : "") +
					 (endereco.getNumero() != null ? endereco.getNumero() + ", " : "") +
					 (endereco.getComplemento() != null ? endereco.getComplemento() + ", " : "")));
			bean.setBairroBody(endereco.getBairro() != null ? endereco.getBairro() : "");
			bean.setCepBody(endereco.getCep() != null ? endereco.getCep().toString().replace("-", "") : "");
			bean.setCidadeBody(endereco.getMunicipio().getNome());
			bean.setUfBody(endereco.getMunicipio().getUf().getSigla());

			if (conta.getNumero().length() < 8) {
				bean.setIdComplemento(" ");
				bean.setComplemento("  ");
			} else {
				bean.setIdComplemento("I");
				bean.setComplemento(conta.getNumero().substring(conta.getNumero().length()-1,conta.getNumero().length()) + conta.getDvnumero());
			}			
			
			bean.setNumDiasProtesto(contacarteira.getQtdedias().toString());
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			
			//Mensagem
			sequencialregistro++;
			bean.setIdRegistroMensagem("2");
			bean.setMensagem1(d.getMensagem1());
			bean.setSequenciaRegMensagem(sequencialregistro.toString());
			listaRegistros.add(bean);			
		}
		
		return listaRegistros;
	}	
	
	/**M�todo que retorna o detalhe do HSBC preenchido
	 * @author Thiago Augusto
	 * @param conta
	 * @param listaDocumento
	 * @return
	 */
	public List<ArquivoRemessaBean> setBodyRemessa400BancoHSBC(Conta conta, List<Documento> listaDocumento){
		Contacarteira contacarteira = conta.getContacarteira();
		
		List<ArquivoRemessaBean> listaRemessa = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencial = 1;
		for (Documento d : listaDocumento) {
			bean = new ArquivoRemessaBean();
			
			bean.setIdRegistroBody("1");
			if(conta.getEmpresa().getCnpj()!=null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			} else if (conta.getEmpresa().getCpf() != null){
				bean.setCodigoInscricaoCedenteBody("01");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCpf().toString().replace("0", "").replace("-", ""));
			}
			bean.setAgenciaBody(conta.getAgencia());
			bean.setSubConta("55");
			bean.setContaCorrenteBody(conta.getAgencia() + conta.getNumero() + conta.getDvnumero());
			bean.setOrigemPagamento("");
			bean.setControleParticipanteBody(d.getCddocumento().toString());
			
			if(contacarteira.getBancogeranossonumero() != null && contacarteira.getBancogeranossonumero()){
				bean.setNossoNumeroBody("");	
			} else {
				String nossoNum = StringUtils.stringCheia(contacarteira.getConvenio(), "0", 5, false);
				nossoNum += StringUtils.stringCheia(d.getCddocumento().toString(), "0" ,5, false);
				nossoNum += Modulos.calculoDvNossoNumeroHSBC(nossoNum);
				bean.setNossoNumeroBody(nossoNum);
			}
			
			bean.setSegundaDtLimiteDesconto("");
			bean.setSegundoValorDesconto("");
			bean.setTerceiraDtLimiteDesconto("");
			bean.setTerceiroValorDesconto("");
			bean.setCarteiraBody("1");
			bean.setCodOcorrencia("1");
			bean.setSeuNumero(d.getCddocumento().toString());
			bean.setDtvencimentoBody(padronizaData(d.getDtvencimento()));
			bean.setValorTitulo(getValorEditado(d.getValor()));
			bean.setCdBanco("399");
			bean.setAgenciaDepositariaBody("00000");
			bean.setEspecieTituloBody(d.getContacarteira() != null && d.getContacarteira().getEspeciedocumentoremessa() != null ? d.getContacarteira().getEspeciedocumentoremessa() : "98");
			bean.setAceiteBody(contacarteira.getAceito());
			bean.setDtemissaoBody(padronizaData(d.getDtemissao()));
			bean.setInstrucao1Body(contacarteira.getInstrucao1());
			bean.setInstrucao2Body(contacarteira.getInstrucao2());
			bean.setJurosMoura(getJuros(d));
			
			List<String> multa = getMulta(d); // 0 - valor multa; 1 - taxa multa; 2 - dtcobrancamulta; 3 - nrDiasMulta;
			
			if(multa != null && multa.size() > 0){
				bean.setValorMulta(multa.get(0));
				bean.setTaxaMulta(multa.get(1));
				bean.setDtCobrancaMulta(multa.get(2));
				bean.setNrDiasMultaBody(multa.get(3));
			}
			
			bean.setDtLimiteDescontoBody(getDataDesconto(d.getTaxa()));
			bean.setValorDescontoBody(getValorDesconto(d.getValor(), d.getTaxa()));
			bean.setValorAbatimentoBody("");
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			if(d.getCliente() != null){
				if(d.getCliente().getCnpj()!=null) {
					bean.setCodigoInscricaoClienteBody("02");
					bean.setCnpjCliente(d.getCliente().getCnpj().getValue());
				} else if (d.getCliente().getCpf() != null){
					bean.setCodigoInscricaoClienteBody("01");
					bean.setCnpjCliente(d.getCliente().getCpf().getValue());
				}
				bean.setEnderecoSacadoBody(d.getCliente().getEndereco().getLogradouro() + " " + d.getCliente().getEndereco().getNumero() + " " + d.getCliente().getEndereco().getComplemento());
				bean.setBairroBody(d.getCliente().getEndereco().getBairro());
				if (d.getCliente().getEndereco().getCep() != null){
					bean.setCepBody(d.getCliente().getEndereco().getCep().toString().substring(0, 5));
					bean.setSufixoCepBody(d.getCliente().getEndereco().getCep().toString().substring(6, 9));
				}
				bean.setCidadeBody(d.getCliente().getEndereco().getMunicipio().getNome());
				bean.setUfBody(d.getCliente().getEndereco().getMunicipio() != null && d.getCliente().getEndereco().getMunicipio().getUf() != null ? d.getCliente().getEndereco().getMunicipio().getUf().getSigla() : "");
			}
			if(contacarteira.getInstrucao1().equals("71") || contacarteira.getInstrucao1().equals("72") || contacarteira.getInstrucao2().equals("71") || contacarteira.getInstrucao2().equals("72")){
				bean.setInstrucaoNaoRecebimento(contacarteira.getQtdedias().toString() != null ? contacarteira.getQtdedias().toString() : "0");
			}
			String avalista = "";
			//if (conta.getEmpresa() != null)
				//avalista = conta.getEmpresa().getNome().toUpperCase();
			bean.setAvalista(avalista);
			bean.setValorIOFBody("0");
			bean.setTipoBloqueto("S");
			bean.setPrazoBody(contacarteira.getQtdedias() != null ? contacarteira.getQtdedias().toString() : "0");
			bean.setTipoMoedaBody("9");
			sequencial ++;
			bean.setNumSequencialRegistro(sequencial.toString());
			
			listaRemessa.add(bean);
		}
		return listaRemessa;
	}
	
	/**M�todo que retorna o Header e Trailer do Banco HSBC
	 * @author Thiago Augusto
	 * @param conta
	 * @param sequencialRegistro
	 * @param vrTotal
	 * @return
	 */
	public ArquivoRemessaBean setHeaderTraillerRemessa400BancoHSBC(Conta conta, Integer sequencialRegistro, Long vrTotal){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRANCA");	
		ht.setAgenciaCedente(conta.getAgencia());
		ht.setSubConta("55");
		ht.setContaCedente(conta.getAgencia() + conta.getNumero() + conta.getDvnumero());
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco("399");
		ht.setNomeBanco("HSBC");
		ht.setDtMovimento(padronizaData(new Date(System.currentTimeMillis())));
		ht.setDensidade("01600");
		ht.setLiteralDensidade("BPI");
		ht.setSiglaLayout("LANCV08");
		ht.setSequencial("1");
		ht.setCodigoServico("1");
		ht.setQtdeTitulos("");
		ht.setVrTotal("000");
		ht.setIdRegistroTrailler("9");
		ht.setSequencial("000001");
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		return ht;
	}
	
	public String modulo11Santander(String numero) {
		int indexador;
		int soma    = 0;
		int mult    = 0;
		int result  = 0;		
		int tamanho = numero.length();
		/*
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = 9 - (indexador % 8); // De 9 a 2.
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
			
		}*/
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 8) + 2; //Sequ�ncia de 2 a 9
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = soma % 11;
		
		if (result == 0 || result == 1) {
			return "0"; 
		} else if (result == 10) { 
			return "1";
		} else {
			return Integer.toString(11-result);
		}
	}
	
	/**M�todo que padroniza as datas (ddMMyyyy) para a gera��o do arquivo
	 * @author Thiago Augusto
	 * @param data
	 * @return
	 */
	public String padronizaData(Date data){
		if (data != null){
			SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
			return format.format(data);
		} else 
			return "";
	}
	
	public String getValorEditado(Money valor){
		String retorno = "000";
		if(valor != null)
			retorno = valor.toString().replace(".", "").replace(",", "");
		return retorno;
	}
	
	/**M�todo que busca a data limite do desconto
	 * @author Thiago Augusto
	 * @param lista
	 * @return
	 */
	public String getDataDesconto(Taxa taxa){
		String data = "";
		if(taxa != null){
			for (Taxaitem taxaitem : taxa.getListaTaxaitem()) {
				if(taxaitem.getTipotaxa().getCdtipotaxa().equals(Tipotaxa.DESCONTO.getCdtipotaxa())){
					data = padronizaData(taxaitem.getDtlimite());
				}
			}
		}
		return data;
	}
	
	/**M�todo que busca o valor do desconto
	 * @author Thiago Augusto
	 * @param lista
	 * @return
	 */
	public String getValorDesconto(Money valorConta, Taxa taxa){
		String valor = "000";
		if (taxa != null){
			for (Taxaitem taxaitem : taxa.getListaTaxaitem()) {
				if(taxaitem.getTipotaxa().getCdtipotaxa().equals(Tipotaxa.DESCONTO.getCdtipotaxa())){
					if(taxaitem.isPercentual()){
						valor = SinedUtil.calculaValorPorcentagem(valorConta, taxaitem.getValor());
					} else {
						valor = getValorEditado(taxaitem.getValor());
					}
				}
			}
		}
		return valor.replace(".", "").replace(",", "");
	}
	
	/**M�todo para fazer o c�lculo de juros
	 * @author Thiago Augusto
	 * @param bean
	 * @return
	 */
	public String getAcrescimos(Documento bean){
		String retorno = "000";
		Date atual = new Date(System.currentTimeMillis());
		
		if(bean.getTaxa() != null){
			List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
			listaTaxaItem = taxaitemService.findTaxasItens(bean.getTaxa());
			Money valorAux = new Money();;	
			int difdias;
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				for (Taxaitem item : listaTaxaItem) {
					
					//if(item.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
					if(item.getTipotaxa().equals(Tipotaxa.MULTA)){
						//if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
						//}
					}
					
					//if(item.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
					if(item.getTipotaxa().equals(Tipotaxa.JUROS)){
						//if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
							
							difdias = SinedDateUtils.diferencaDias(atual,item.getDtlimite());
							valorAux = valorAux.multiply(new Money(new Double(difdias)));
						//}
					}
				}
				retorno = valorAux.toString().replace(".", "").replace(",", "");
			}
		}
		return retorno;
	}
	
	/**M�todo pra pegar o valor real do pagamento (com abatimento)
	 * @author Thiago Augusto
	 * @param doc
	 * @return
	 */
	public String getValorAbatimento(Documento doc){
		String retorno = "000";
		Money valor = doc.getValor().subtract(new Money(getValorDesconto(doc.getValor(), doc.getTaxa())));
		retorno = (doc.getValor().subtract(valor)).toString().replace(".", "").replace(",", "");
		return retorno;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Santander
	 * Registros do tipo DETALHE
	 * @since 08/02/2012
	 * @author Thiago Augusto
	 */	
	public List<ArquivoRemessaBean> setBodyNewRemessa400BancoSantander(Conta conta, List<Documento> listaDocumento){
		Contacarteira contacarteira = conta.getContacarteira();
		// Data atual do sistema
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		for (Documento d : listaDocumento) {
			sequencialregistro++;

			bean = new ArquivoRemessaBean();	
			bean.setIdRegistroBody("1");
			
			if (conta.getEmpresa().getCpf() != null){
				bean.setCodigoInscricaoCedenteBody("01");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCpf().getValue().replace("0", "").replace("-", ""));
			}
			else if(conta.getEmpresa().getCnpj()!= null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}	
			bean.setCodigoTransmissao(contacarteira.getCodigotransmissao() != null ? contacarteira.getCodigotransmissao() : "");
			if (contacarteira.getCodigocarteira() != null && contacarteira.getCodigocarteira().equals("5")) {
				if (conta.getAgencia().length() < 5)
					bean.setAgenciaBody(conta.getAgencia() + conta.getDvagencia());
				else
					bean.setAgenciaBody(conta.getAgencia().substring(0,5));			
			} else {
				bean.setAgenciaBody("00000");
			}
			bean.setContaCorrenteBody(conta.getNumero().toString());
			bean.setControleParticipanteBody(d.getCddocumento().toString());
			if (contacarteira.getBancogeranossonumero() != null && contacarteira.getBancogeranossonumero()) 
				bean.setNossoNumeroBody("00000000");
			else
				bean.setNossoNumeroBody(d.getCddocumento().toString() + modulo11Santander(d.getCddocumento().toString()));
			bean.setTipoMultaBody("0");
			
			if (d.getTaxa() != null){
				
//				MULTA
				Taxaitem taxaitemMulta = new Taxaitem();
				taxaitemMulta = taxaitemService.findByTaxaMulta(d.getTaxa());				
				if(taxaitemMulta != null && taxaitemMulta.getTipotaxa() != null && taxaitemMulta.getTipotaxa().getCdtipotaxa() != null){
					
					if(taxaitemMulta.isPercentual()){
						bean.setPercentualMultaBody(new Long(taxaitemMulta.getValor().toLong()).toString());
					}else{
						if(taxaitemMulta.getValor() != null && d.getValor() != null){
							Money percentual = taxaitemMulta.getValor().multiply(new Money(100)).divide(d.getValor());
							bean.setPercentualMultaBody(percentual.toString().replace(".", "").replace(",", ""));
						}
					}
					bean.setTipoMultaBody("4");
					if (taxaitemMulta.getDtlimite() != null && !taxaitemMulta.getDtlimite().equals(d.getDtvencimento())){
						bean.setDtCobrancaMulta(format.format(taxaitemMulta.getDtlimite()));
					} else if (taxaitemMulta.getDtlimite() != null && taxaitemMulta.getDtlimite().equals(d.getDtvencimento())){
						bean.setDtCobrancaMulta("000000");
					}
				}else{
					bean.setPercentualMultaBody("0");
					bean.setDtCobrancaMulta("0");
				}

//				DESCONTO				
				Taxaitem taxaitemDesconto = new Taxaitem();
				taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());

				if(taxaitemDesconto != null && taxaitemDesconto.getTipotaxa() != null && taxaitemDesconto.getTipotaxa().getCdtipotaxa() != null){
					Money valorDesconto = new Money(0); 
					if(taxaitemDesconto.isPercentual())
						valorDesconto = new Money(d.getValor().multiply(taxaitemDesconto.getValor().divide(new Money(100))));
					else
						valorDesconto = new Money(taxaitemDesconto.getValor());
					bean.setValorDescontoBody(new Long(valorDesconto.toLong()).toString());
					if (taxaitemDesconto.getDtlimite() != null && taxaitemDesconto.getValor() != null )
						bean.setDtLimiteDescontoBody(padronizaData(taxaitemDesconto.getDtlimite()));
				}else{
					bean.setValorDescontoBody("0");
					bean.setDtLimiteDescontoBody("0");
				}
				
//				JUROS
				Taxaitem taxaitemJuros = new Taxaitem();
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null && taxaitemJuros.getTipotaxa() != null && taxaitemJuros.getTipotaxa().getCdtipotaxa() != null){
					Money valorDesconto = new Money(0); 
					if(taxaitemJuros.isPercentual())
						valorDesconto = new Money(d.getValor().multiply(taxaitemJuros.getValor().divide(new Money(100))));
					else
						valorDesconto = new Money(taxaitemJuros.getValor());
					bean.setTaxaValorJurosBody(new Long(valorDesconto.toLong()).toString());
				}else{
					bean.setTaxaValorJurosBody("0");
				}
				
			} else {
				bean.setPercentualMultaBody("0");
				bean.setTaxaValorJurosBody("0");
				bean.setDtLimiteDescontoBody("0");
				bean.setValorDescontoBody("0");
				bean.setPercentualMultaBody("0");
				bean.setDtCobrancaMulta("0");
			}
			
			bean.setUnidadeMoeda("00");
			bean.setCodigoCarteiraBody(contacarteira.getCodigocarteira());
			bean.setIdOcorrenciaBody("01"); //Entrada de titulo
			bean.setSeuNumero(d.getCddocumento().toString());
			bean.setDtvencimentoBody(padronizaData(d.getDtvencimento()));
			bean.setValorBody(new Long(d.getValor().toLong()).toString());
			bean.setCdBancoBody(conta.getBanco().getNumero().toString());
			if (contacarteira.getCodigocarteira().equals("05") || contacarteira.getCodigocarteira().equals("5")){
				bean.setAgenciaCobradora(conta.getAgencia());
			} else {
				bean.setAgenciaCobradora("00000");
			}
			bean.setEspecieTituloBody("01");
			bean.setAceiteBody("N");
			bean.setDtemissaoBody(padronizaData(d.getDtemissao()));
			bean.setInstrucao1Body(contacarteira.getInstrucao1());
			bean.setInstrucao2Body(contacarteira.getInstrucao2());
			
			bean.setValorIOFBody("0");
			bean.setValorAbatimentoBody("0");
			if (d.getPessoa().getCpf()!=null) {				
				bean.setIdTipoInscricaoBody("01");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCpf().toString().replaceAll("\\.","").replaceAll("-", ""));
			}else if(d.getPessoa().getCnpj()!=null) {
				bean.setIdTipoInscricaoBody("02");
				bean.setNumeroInscricaoSacadoBody(d.getPessoa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			Endereco endereco = d.getEndereco();
			if(endereco == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " n�o tem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(
					((endereco.getLogradouro() != null ? endereco.getLogradouro() + ", "  : "") +
					 (endereco.getNumero() != null ? endereco.getNumero() + ", " : "") +
					 (endereco.getComplemento() != null ? endereco.getComplemento() + ", " : "")));
			bean.setBairroBody(endereco.getBairro() != null ? endereco.getBairro() : "");
			
			if(endereco.getCep() != null){
				String [] cep = endereco.getCep().toString().split("-");
				bean.setCepBody(cep[0] != null ? cep[0] : "");
				bean.setComplementoCep(cep[1] != null ? cep[1] : "");
			}
			if (endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null)
				bean.setCidadeBody(endereco.getMunicipio().getNome());
			if (endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null)
				bean.setUfBody(endereco.getMunicipio().getUf().getSigla());
			bean.setSacador("");
			if (conta.getNumero().length() < 8) {
				bean.setIdComplemento(" ");
				bean.setComplemento("  ");
			} else {
				bean.setIdComplemento("I");
				bean.setComplemento(conta.getNumero().substring(conta.getNumero().length() - 2, conta.getNumero().length()));
			}
			if ("06".equals(contacarteira.getInstrucao1()) || "6".equals(contacarteira.getInstrucao1()) || 
					"06".equals(contacarteira.getInstrucao2()) || "6".equals(contacarteira.getInstrucao2())){
				bean.setNumDiasProtesto(contacarteira.getQtdedias().toString());
			} else {
				bean.setNumDiasProtesto("00");
			}
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			
			listaRegistros.add(bean);
		}
		
		return listaRegistros;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Santander
	 * Registros do tipo HEADER e TRAILLER
	 * @since 08/02/2012
	 * @author Thiago Augusto
	 */
	public ArquivoRemessaBean setNewHeaderTraillerRemessa400BancoSantander(Conta conta,Integer totalRegistros, Integer sequencialRegistro, String vrTotal){
		Contacarteira contacarteira = conta.getContacarteira();
		
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
		Date date = new Date(System.currentTimeMillis());
		String data = format.format(date);
		String nomeBanco = "SANTANDER";
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
				
		ht.setIdRegistro("0");
		ht.setIdRemessa("1");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRAN�A");	
		ht.setCodigoTransmissao(contacarteira.getCodigotransmissao() != null ? contacarteira.getCodigotransmissao() : "");
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco(conta.getBanco().getNumero().toString());
		ht.setNomeBanco(nomeBanco);
		ht.setDtMovimento(data);
		ht.setMensagem1("");
		ht.setMensagem2("");
		ht.setMensagem3("");
		ht.setMensagem4("");
		ht.setMensagem5("");
		ht.setMensagem6("");
		ht.setNumeroVersao("");
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		ht.setIdRegistroTrailler("9");
		totalRegistros = totalRegistros + 2;
		ht.setQtdeTitulos(totalRegistros.toString());
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		ht.setVrTotal(vrTotal.replace(".", "").replace(",", ""));
		
		return ht;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Rural
	 * Registros do tipo HEADER e TRAILLER
	 * @since 27/04/2012
	 * @author Thiers Euller
	 */
	public ArquivoRemessaBean setNewHeaderTraillerRemessa400BancoRural(Conta conta, Integer sequencialRegistro){
		ArquivoRemessaBean ht = new ArquivoRemessaBean();
		String nomeBanco = "RURAL";
		String nomeCedente = contacorrenteService.getNomeCedente(conta);
				
		ht.setIdRegistro("0");
		ht.setIdRemessa("9");
		ht.setLiteralRemessa("REMESSA");
		ht.setCdServico("01");
		ht.setLiteralServico("COBRANCA");	
		ht.setCodigoTransmissao(conta.getAgencia() + conta.getNumero() + conta.getDvnumero());
		ht.setNomeCedente(nomeCedente);
		ht.setCdBanco(conta.getBanco().getNumero().toString());
		ht.setNomeBanco(nomeBanco);
		ht.setCodigoVersao("CD0V01");
		ht.setNumSequencialRegistro("000001");
		
		//Trailler
		ht.setIdRegistroTrailler("9");
		ht.setNumSequencialRegistroTrailler(sequencialRegistro.toString());
		
		return ht;
	}
	
	/**
	 * M�todo respons�vel por setar os valores necess�rios para popular o arquivo de cobran�a 400 registrada Banco Rural
	 * Registros do tipo DETALHE
	 * @since 03/05/2012
	 * @author Thiers Euller
	 */	
	public List<ArquivoRemessaBean> setBodyNewRemessa400BancoRural(Conta conta, List<Documento> listaDocumento){
		Contacarteira contacarteira = conta.getContacarteira();
		
		List<ArquivoRemessaBean> listaRegistros = new ArrayList<ArquivoRemessaBean>();
		ArquivoRemessaBean bean;
		Integer sequencialregistro = 1;
		
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumento, "pessoa.cdpessoa", ",");
		List<Cliente> listaCliente = clienteService.findByWherein(whereIn);
		
		
		for (Documento d : listaDocumento) {
			for(Cliente c : listaCliente){
				if(d.getPessoa().getCdpessoa().equals(c.getCdpessoa())){
					d.setCliente(c);
					break;
				}
			}
			sequencialregistro++;
			

			bean = new ArquivoRemessaBean();	
			
			bean.setSequencialRegistroBody(sequencialregistro.toString());
			
			bean.setIdRegistroBody("1"); 																				//c�d do registro
			bean.setCdEmpresa(conta.getAgencia() + conta.getNumero() + conta.getDvnumero());		//identificacao da empresa no banco
			bean.setNumDiasProtesto(contacarteira.getQtdedias() != null ? contacarteira.getQtdedias().toString() : "");													//quantidade de dias para protesto
			bean.setCodigoMoeda("00");
			
			bean.setTaxaValorJurosBody("");//Verificar campo TIPO MORA
			
			bean.setMensagem1(d.getMensagem1() != null ? d.getMensagem1() : "");
			bean.setMensagem2(d.getMensagem2() != null ? d.getMensagem2() : "");
			bean.setMensagem3(d.getMensagem3() != null ? d.getMensagem3() : "");
			bean.setMensagem4(d.getMensagem4() != null ? d.getMensagem4() : "");
			bean.setMensagem5(d.getMensagem5() != null ? d.getMensagem5() : "");
			bean.setMensagem6(d.getMensagem6() != null ? d.getMensagem6() : "");
			
			if(!bean.getMensagem1().equals("") || !bean.getMensagem2().equals("") || !bean.getMensagem3().equals("") || !bean.getMensagem4().equals("") || !bean.getMensagem5().equals("") || !bean.getMensagem6().equals("")){
				bean.setIdRegistroMensagem("4");
				sequencialregistro++;
				bean.setSequencialMensagem(sequencialregistro.toString());
			}else{
				bean.setIdRegistroMensagem("");
				bean.setSequencialMensagem("");
			}
			
			//verificar campo - Identifica��o informada pela empresa
			bean.setInformacaoEmpresa(d.getCddocumento().toString());
			bean.setNossoNumeroBody(d.getCddocumento().toString());
			//Verificar nota 04
			String dvnossosumero = modulo10Rural(conta.getAgencia(), conta.getNumero(), conta.getDvnumero(), d.getCddocumento().toString());
			bean.setDigitoConfNossoNumeroBody(dvnossosumero);
			
			bean.setContratoBody(contacarteira.getConvenio());
			if (conta.getEmpresa().getCpf() != null){
				bean.setCodigoInscricaoCedenteBody("01");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCpf().getValue().replace("0", "").replace("-", ""));
			}
			else if(conta.getEmpresa().getCnpj()!= null) {
				bean.setCodigoInscricaoCedenteBody("02");
				bean.setCnpjCedenteBody(conta.getEmpresa().getCnpj().toString().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}else{
				bean.setCodigoInscricaoCedenteBody("");
				bean.setCnpjCedenteBody("");
			}
			bean.setCarteiraBody(contacarteira.getCarteira() != null ? contacarteira.getCarteira() : "");
			bean.setCodigoServico("1");
			bean.setNumeroDocumentoBody(d.getCddocumento() != null ? d.getCddocumento().toString() : "");
			bean.setDtvencimentoBody(padronizaData(d.getDtvencimento()));
			bean.setValorBody(new Long(d.getValor().toLong()).toString());
			bean.setAgenciaCobradora("0000000");
			bean.setEspecieTituloBody("01");
			bean.setAceiteBody("B");
			bean.setDtemissaoBody(padronizaData(d.getDtemissao()));
			bean.setInstrucao1Body("0000");
			
			bean.setTipoMora("");
			bean.setMoraUmDia("");
			bean.setValorDescontoBody("");
			bean.setDtLimiteDescontoBody("");
			
			if (d.getTaxa() != null){
				//JUROS
				Taxaitem taxaitemJuros = new Taxaitem();
				taxaitemJuros = taxaitemService.findByTaxaJuros(d.getTaxa());
				if(taxaitemJuros != null){
					if(taxaitemJuros.isPercentual()){
						bean.setTipoMora("1");
					}else{
						bean.setTipoMora("2");
					}
					bean.setMoraUmDia(taxaitemJuros.getValor().toString().replace(".", "").replace(",", ""));
				}
				
				//DESCONTO
				Taxaitem taxaitemDesconto = new Taxaitem();
				taxaitemDesconto = taxaitemService.findByTaxaDesconto(d.getTaxa());
				if(taxaitemDesconto != null && taxaitemDesconto.getTipotaxa() != null && taxaitemDesconto.getTipotaxa().getCdtipotaxa() != null){
					Money valorDesconto = new Money(0); 
					if(taxaitemDesconto.isPercentual())
						valorDesconto = new Money(d.getValor().multiply(taxaitemDesconto.getValor().divide(new Money(100))));
					else
						valorDesconto = new Money(taxaitemDesconto.getValor());
					bean.setValorDescontoBody(new Long(valorDesconto.toLong()).toString().replace(".", "").replace(",", ""));
					if (taxaitemDesconto.getDtlimite() != null)
						bean.setDtLimiteDescontoBody(padronizaData(taxaitemDesconto.getDtlimite()));
				}
			}
				
			//VALOR IOC
			//Abatimento
			if (d.getCliente() != null && d.getCliente().getCpf() != null){
				bean.setTipoInscricaoSacado("01");
				bean.setNumeroInscricaoSacadoBody(d.getCliente().getCpf().getValue().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}
			else if(d.getCliente() != null && d.getCliente().getCnpj()!= null) {
				bean.setTipoInscricaoSacado("02");
				bean.setNumeroInscricaoSacadoBody(d.getCliente().getCnpj().getValue().replaceAll("\\.","").replaceAll("-", "").replaceAll("/", ""));
			}else{
				bean.setTipoInscricaoSacado("");
				bean.setNumeroInscricaoSacadoBody("");
			}
			
			String nomeFantasia = null;
			String razaoSocial = null;
			if(d.getTipopagamento() != null && d.getPessoa().getCdpessoa() != null){
				if(Tipopagamento.CLIENTE.equals(d.getTipopagamento())){
					Cliente cliente = clienteService.carregarDadosCliente(new Cliente(d.getPessoa().getCdpessoa()));
					if(cliente != null){
						nomeFantasia = cliente.getNome();
						razaoSocial = cliente.getRazaosocial();
					}
				}else {
					nomeFantasia = d.getPessoa().getRazaoSocial();
				}
			}
			
			if(razaoSocial != null && !"".equals(razaoSocial)){
				bean.setNomeSacadoBody(razaoSocial);
			}else if(nomeFantasia != null && !"".equals(nomeFantasia)) {
				bean.setNomeSacadoBody(nomeFantasia);
			}else {
				bean.setNomeSacadoBody(d.getPessoa().getRazaoSocial());
			}
			
			Endereco endereco = d.getEndereco();
			if(endereco == null) throw new SinedException("A conta a receber " + d.getCddocumento() + " n�o tem endere�o de cobran�a cadastrado.");
			bean.setEnderecoSacadoBody(
					((endereco.getLogradouro() != null ? endereco.getLogradouro() + ", "  : "") +
					 (endereco.getNumero() != null ? endereco.getNumero() + ", " : "")));
			bean.setComplemento(endereco.getComplemento() != null ? endereco.getComplemento() : "");
			bean.setBairroBody(endereco.getBairro() != null ? endereco.getBairro() : "");
			//bean.setComplemento(bean.getBairroBody());
			String [] cep = endereco.getCep().toString().split("-");
			bean.setCepBody(cep[0] != null ? cep[0] : "");
			bean.setComplementoCep(cep[1] != null ? cep[1] : "");
			bean.setCidadeBody(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome(): "");
			bean.setUfBody(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : "");
			bean.setIndicadorSacador("1");
			bean.setNomeSacador("");
			
			
			listaRegistros.add(bean);
		}
		
		return listaRegistros;
	}
	
	public List<String> getMulta(Documento bean){
		List<String> retorno = new ArrayList<String>();
		String taxaMulta = "000";
		String valorMulta = "000";
		String dtCobrancaMulta = "00000000";
		String nrDiasMultaBody = "0";
		
		
		if(bean.getTaxa() != null){
			List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
			listaTaxaItem = taxaitemService.findTaxasItens(bean.getTaxa());
			Money valorAux = new Money();;	
			Money taxaAux = new Money();
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				for (Taxaitem item : listaTaxaItem) {
					if(item.getTipotaxa().equals(Tipotaxa.MULTA)){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor()).divide(new Money(100d));
								taxaAux = item.getValor();
							} else {
								valorAux = item.getValor();
								taxaAux = item.getValor().multiply(new Money(100d)).divide(bean.getValor());
							}
							dtCobrancaMulta = padronizaData(item.getDtlimite());
							nrDiasMultaBody = SinedDateUtils.calculaDiferencaDias(item.getDtlimite(), SinedDateUtils.currentDate()).toString();
					}
				}
				valorMulta = valorAux.toString().replace(".", "").replace(",", "");
				taxaMulta = taxaAux.toString().replace(".", "").replace(",", "");
				
				retorno.add(valorMulta);
				retorno.add(taxaMulta);
				retorno.add(dtCobrancaMulta);
				retorno.add(nrDiasMultaBody);
			}
		}
		return retorno;
	}
	
	public String getJuros(Documento bean){
		String retorno = "000";
		if(bean.getTaxa() != null){
			List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
			listaTaxaItem = taxaitemService.findTaxasItens(bean.getTaxa());
			Money valorAux = new Money();;	
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				for (Taxaitem item : listaTaxaItem) {
					if(item.getTipotaxa().equals(Tipotaxa.JUROS)){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
							//difdias = SinedDateUtils.diferencaDias(atual,item.getDtlimite());
							//valorAux = valorAux.multiply(new Money(new Double(difdias)));
					}
				}
				retorno = valorAux.toString().replace(".", "").replace(",", "");
			}
		}
		return retorno;
	}
	
	public String modulo10Rural(String agencia, String numeroconta, String dgconta, String nossonumero) {
		int soma    = 0;
		int result  = 0;	
		
		//String dgverificador = "0";
		
		//Calculo agencia
		if(agencia != null && !agencia.equals("")){
			int somaagencia = 0;
		
			somaagencia += Integer.parseInt(agencia.substring(0, 1)) * 0;
			somaagencia += Integer.parseInt(agencia.substring(1, 2)) * 1;
			somaagencia += Integer.parseInt(agencia.substring(2, 3)) * 9;
			somaagencia += Integer.parseInt(agencia.substring(3, 4)) * 7;
			
			soma += somaagencia;
		}
		
		//Calculo numero conta + dgconta
		if(numeroconta != null && !numeroconta.equals("")){
			if(numeroconta.length() != 9){
				throw new SinedException("N�mero da conta banc�ria deve ter exatamente 9 digitos.");
			}
			int somaconta = 0;
			
			somaconta += Integer.parseInt(numeroconta.substring(0, 1)) * 3;
			somaconta += Integer.parseInt(numeroconta.substring(1, 2)) * 1;
			
			somaconta += Integer.parseInt(numeroconta.substring(2, 3)) * 9;
			somaconta += Integer.parseInt(numeroconta.substring(3, 4)) * 7;
			somaconta += Integer.parseInt(numeroconta.substring(4, 5)) * 3;
			somaconta += Integer.parseInt(numeroconta.substring(5, 6)) * 1;
			somaconta += Integer.parseInt(numeroconta.substring(6, 7)) * 9;
			somaconta += Integer.parseInt(numeroconta.substring(7, 8)) * 7;
			somaconta += Integer.parseInt(numeroconta.substring(8, 9)) * 3;
			
			somaconta += Integer.parseInt(dgconta) * 1;
			
			soma += somaconta;
		}
		
		//Calculo nosso numero
		if(nossonumero != null && !nossonumero.equals("")){
			int somanossonumero = 0;
			
			nossonumero = StringUtils.stringCheia(nossonumero, "0", 7, false);
			
			somanossonumero += Integer.parseInt(nossonumero.substring(0, 1)) * 9;
			somanossonumero += Integer.parseInt(nossonumero.substring(1, 2)) * 7;
			somanossonumero += Integer.parseInt(nossonumero.substring(2, 3)) * 3;
			somanossonumero += Integer.parseInt(nossonumero.substring(3, 4)) * 1;
			somanossonumero += Integer.parseInt(nossonumero.substring(4, 5)) * 9;
			somanossonumero += Integer.parseInt(nossonumero.substring(5, 6)) * 7;
			somanossonumero += Integer.parseInt(nossonumero.substring(6, 7)) * 3;
			
			soma += somanossonumero;
		}
		
		result = soma % 10;
		
		if (result == 0 || result == 10) {
			return "0"; 
		} else { 
			Integer dg = 10 - result;
			return dg.toString();
		} 
	}
	
	/**M�todo para preencher o bean ArquivoRemessaItauHeader para a gera��o do arquivo
	 * @author Thiago Augusto
	 * @param baixarContaBean
	 * @return
	 */
	public ArquivoRemessaItauHeader setHeaderSispagItau(BaixarContaBean baixarContaBean){
		ArquivoRemessaItauHeader header = new ArquivoRemessaItauHeader();
		header.setCdBanco("341");
		header.setCodLote("0000");
		header.setTipoRegistro("0");
		header.setVersaoLote("080");
		if (baixarContaBean.getVinculo().getEmpresa().getCnpj() == null)
			header.setTipoInscricaoEmpresa("1");
		else
			header.setTipoInscricaoEmpresa("2");
		header.setCnpjEmpresa(baixarContaBean.getVinculo().getEmpresa().getCpfCnpj().replace(".", "").replace("/", "").replace("-", ""));
		header.setAgencia(baixarContaBean.getVinculo().getAgencia());
		header.setConta(baixarContaBean.getVinculo().getNumero());
		header.setDac(baixarContaBean.getVinculo().getDvnumero());
		header.setNomeEmpresa(StringUtils.tiraAcento(baixarContaBean.getVinculo().getEmpresa().getRazaosocialOuNome().toUpperCase()));
		header.setNomeBanco(StringUtils.tiraAcento(baixarContaBean.getVinculo().getBanco().getNome().toUpperCase().replace(".", "")));
		header.setArquivoCodigo("1");
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		header.setDataGeracaoArquivo(format.format(calendar.getTime()).replace("/", ""));
		Integer hora = calendar.get(Calendar.HOUR_OF_DAY);
		String horaAux = "0";
		if (hora > 9){
			horaAux = hora.toString();
		} else {
			horaAux += hora.toString();
		}
		Integer minutos = calendar.get(Calendar.MINUTE);
		String minutoAux = "0";
		if (minutos > 9){
			minutoAux = minutos.toString();
		} else {
			minutoAux += minutos.toString();
		}
		Integer segundos = calendar.get(Calendar.SECOND);
		String segundoAux = "0";
		if (segundos > 9){
			segundoAux = segundos.toString();
		} else {
			segundoAux += segundos.toString();
		}
		header.setHoraGeracaoArquivo(horaAux + minutoAux + segundoAux);
		header.setUnidadeDensidade("0000");
		return header;
	}
	
	/**M�todo para preencher uma lista do bean ArquivoRemessaItauHeaderLote para a gera��o do arquivo
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @return
	 */
	public ArquivoRemessaItauHeaderLote setHeaderLoteSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		ArquivoRemessaItauHeaderLote lote;
		lote = new ArquivoRemessaItauHeaderLote();
		
		lote.setCdbanco("341");
		lote.setCodLote("1");
		lote.setTipoRegistro("1");
		lote.setTipoOperacao("C");
		if (bean.getBancotipopagamento() == null || bean.getBancoformapagamento() == null)//CHEQUE E OP DEVE PERMITIR
			throw new SinedException("� necess�rio preencher a Forma de Pagamento e o Tipo de Pagamento.");
		lote.setTipoPagamento(bean.getBancotipopagamento().getIdentificador().toString());
		lote.setFormaPagamento(bean.getBancoformapagamento().getIdentificador().toString());
		lote.setLayoutLote("040");
		if (bean.getVinculo().getEmpresa().getCnpj() == null){
			lote.setTipoEmpresa("1");
			lote.setInscricaoNumero(bean.getVinculo().getEmpresa().getCpf().toString().replace(".", "").replace("/", "").replace("-", ""));
		}
		else { 
			lote.setTipoEmpresa("2");
			lote.setInscricaoNumero(bean.getVinculo().getEmpresa().getCnpj().toString().replace(".", "").replace("/", "").replace("-", ""));
		}
		lote.setIdentificacaoLancamento("1707");
		lote.setAgencia(bean.getVinculo().getAgencia());
		lote.setConta(bean.getVinculo().getNumero());
		lote.setDac(bean.getVinculo().getDvnumero());
		lote.setNomeEmpresa(StringUtils.tiraAcento(bean.getVinculo().getEmpresa().getRazaosocialOuNome().toUpperCase()));
		lote.setFinalidadeLote("");
		lote.setHistoricoCC("");
		lote.setEnderecoEmpresa(StringUtils.tiraAcento(bean.getVinculo().getEmpresa().getEnderecoAux().getLogradouro().toUpperCase()));
		lote.setNumero(bean.getVinculo().getEmpresa().getEnderecoAux().getNumero());
		lote.setComplemento(StringUtils.tiraAcento(bean.getVinculo().getEmpresa().getEnderecoAux().getComplemento().toUpperCase()));
		lote.setCidade(StringUtils.tiraAcento(bean.getVinculo().getEmpresa().getEnderecoAux().getMunicipio().getNome().toUpperCase()));
		lote.setCep(bean.getVinculo().getEmpresa().getEnderecoAux().getCep().getValue());
		if (bean.getVinculo().getEmpresa().getEnderecoAux().getMunicipio() != null && bean.getVinculo().getEmpresa().getEnderecoAux().getMunicipio().getUf() != null)
			lote.setEstado(bean.getVinculo().getEmpresa().getEnderecoAux().getMunicipio().getUf().getSigla());
		return lote;
	}
	
	/**M�todo que preenche o detalhe de DOC (Segmento A)
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @return
	 */
	public List<ArquivoRemessaItauDOC> setDetalheDOCSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean, int identificador){
		List<ArquivoRemessaItauDOC> listaDOC = new ArrayList<ArquivoRemessaItauDOC>();
		ArquivoRemessaItauDOC itauDoc;
		Integer numeroRegistro = 0;
		Banco banco = null;
		String agencia = "";
//		String dvAgencia = "";
		String conta = "";
		String dvConta = "";
		for (Documento documento : listaDocumentoRemessa) {
			numeroRegistro ++;
//			FINALIZAR O M�TODO
			itauDoc = new ArquivoRemessaItauDOC();
			
			itauDoc.setCdBanco("341");
			itauDoc.setCodLote("1");
			itauDoc.setTipoRegistro("3");
			itauDoc.setNumeroRegistro(numeroRegistro.toString());
			itauDoc.setSegmento("A");
			itauDoc.setTipoMovimento("000");
			
//			Preenche Ag�ncia/Conta
			if (documento.getPessoa() != null){				
				Dadobancario dadoBancario = documento.getPessoa().getDadobancario();
				
				if (dadoBancario != null){
					banco = new Banco();
					banco = dadoBancario.getBanco();
					agencia = dadoBancario.getAgencia();
					conta = dadoBancario.getConta();
					dvConta = dadoBancario.getDvconta();
				} else if (dadoBancario == null && identificador !=  2 && identificador != 10){
					throw new SinedException("� necess�rio preencher os dados banc�rios do fornecedor para gerar o arquivo SISPAG.");
				}
				
				itauDoc.setBancoFavorecido(banco != null ? banco.getNumero().toString() : "");
				itauDoc.setAgencia(agencia);
				itauDoc.setConta(conta);
				if (identificador ==  2 || identificador == 10)
					itauDoc.setDac("0");
				else 
					itauDoc.setDac(dvConta);
			}
//			Preenche Ag�ncia/Conta
			
			itauDoc.setNomeFavorecido(StringUtils.tiraAcento(documento.getPessoa().getNome().toUpperCase()));
			itauDoc.setSeuNumero(documento.getCddocumento().toString());
			itauDoc.setDataPagamento(padronizaData(bean.getDtpagamento()));
			itauDoc.setMoeda("REA");
			itauDoc.setValorPagamento(documento.getValor().toString().replace(".", "").replace(",", ""));
			itauDoc.setNossoNumero("");
			itauDoc.setDataEfetiva(padronizaData(documento.getDataparcela()));
			itauDoc.setValorEfetivo(documento.getValor().toString().replace(".", "").replace(",", ""));
			itauDoc.setFinalidadeDetalhe(bean.getHistorico());
			itauDoc.setNumeroDocumento(documento.getCddocumento().toString());
			if (bean.getVinculo().getEmpresa().getCnpj() != null){
				itauDoc.setNumeroInscricao(documento.getPessoa().getCpfCnpj().toString());
			}
			else{ 
				itauDoc.setNumeroInscricao(documento.getPessoa().getCpf().toString());
			}
			itauDoc.setFinalidade(bean.getFinalidadedoc() != null ? bean.getFinalidadedoc().getCodigo() : "");
			itauDoc.setFinalidadeTED(bean.getFinalidadeted() != null ? bean.getFinalidadeted().getCodigo() : "00010");
			itauDoc.setAviso("");
			itauDoc.setOcorrencia("");
			
			listaDOC.add(itauDoc);
		}
		return listaDOC;
	}
	
	/**M�todo que preenche o detalhe de Nota Fiscal (Segmento A)
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @return
	 */
	public List<ArquivoRemessaItauNF> setDetalheNFSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		List<ArquivoRemessaItauNF> listaNF = new ArrayList<ArquivoRemessaItauNF>();
		ArquivoRemessaItauNF itauNF;
		Integer numeroRegistro = 0;
		for (Documento documento : listaDocumentoRemessa) {
			numeroRegistro ++;
			itauNF = new ArquivoRemessaItauNF();
			Fornecedor fornecedor = null;
			Cliente cliente = null;
			String nomeFavorecido = "";
			String bancoFavorecido = "";
			String agenciaFavorecido = "";
			String contaFavorecido = "";
			String dacFavorecido = "";
			if (documento.getTipopagamento().equals(Tipopagamento.FORNECEDOR)){
				fornecedor = documento.getFornecedor();
				nomeFavorecido = fornecedor.getRazaosocial();
				if (fornecedor.getListaDadobancario() != null && fornecedor.getListaDadobancario().size() > 0 && !fornecedor.getListaDadobancario().isEmpty()){
					bancoFavorecido = fornecedor.getListaDadobancario().iterator().next().getBanco().getNumeroString();
					agenciaFavorecido = fornecedor.getListaDadobancario().iterator().next().getAgencia();
					contaFavorecido = fornecedor.getListaDadobancario().iterator().next().getConta();
					dacFavorecido = fornecedor.getListaDadobancario().iterator().next().getDvconta();
					itauNF.setNumeroInscricao(fornecedor.getCnpj() != null ? fornecedor.getCnpj().toString() : fornecedor.getCpf().toString());
				} else {
					throw new SinedException("S�o necess�rios os dados banc�rios para gerar o arquivo de remessa.");
				}
			} else if (documento.getTipopagamento().equals(Tipopagamento.CLIENTE)){
				cliente = clienteService.carregarDadosCliente(new Cliente(documento.getPessoa().getCdpessoa()));
				nomeFavorecido = cliente.getRazaosocial();
				if (cliente.getListaDadobancario() != null && cliente.getListaDadobancario().size() > 0 && !cliente.getListaDadobancario().isEmpty()){
					bancoFavorecido = cliente.getListaDadobancario().iterator().next().getBanco().getNumeroString();
					agenciaFavorecido = cliente.getListaDadobancario().iterator().next().getAgencia();
					contaFavorecido = cliente.getListaDadobancario().iterator().next().getConta();
					dacFavorecido = cliente.getListaDadobancario().iterator().next().getDvconta();
					itauNF.setNumeroInscricao(cliente.getCnpj() != null ? cliente.getCnpj().toString() : cliente.getCpf().toString());
				}
				else {
					throw new SinedException("S�o necess�rios os dados banc�rios para gerar o arquivo de remessa.");
				}
			} else if (documento.getTipopagamento().equals(Tipopagamento.OUTROS)){
				nomeFavorecido = documento.getOutrospagamento();
				bancoFavorecido = bean.getVinculo().getBanco().getNumeroString();
				agenciaFavorecido = bean.getVinculo().getAgencia();
				contaFavorecido = bean.getVinculo().getNumero();
				dacFavorecido = bean.getVinculo().getDvnumero();
//				itauNF.setNumeroInscricao(bean.getCnpj() != null ? fornecedor.getCnpj().toString() : fornecedor.getCpf().toString());
			}
			
			
//			PREENCHE O BEAN
			itauNF.setCdBanco("341");
			itauNF.setCodLote(numeroRegistro.toString());
			itauNF.setTipoRegistro("3");
			itauNF.setNumeroRegistro(numeroRegistro.toString());
			itauNF.setSegmento("A");
			itauNF.setTipoMovimento("000");
			itauNF.setBancoFavorecido(bancoFavorecido);
//			Preenche Ag�ncia/Conta
			itauNF.setAgencia(agenciaFavorecido);
			itauNF.setConta(contaFavorecido);
			itauNF.setDac(dacFavorecido);
//			Preenche Ag�ncia/Conta
			itauNF.setNomeFavorecido(StringUtils.tiraAcento(nomeFavorecido));
			itauNF.setSeuNumero(documento.getCddocumento().toString());
			itauNF.setDataPagamento(padronizaData(bean.getDtpagamento()));
			itauNF.setMoeda("REA");
			itauNF.setValorPagamento(documento.getValor().toString().replace(".", "").replace(",", ""));
			itauNF.setNossoNumero("");
			itauNF.setDataEfetiva(padronizaData(bean.getDtpagamento()));
			itauNF.setValorEfetivo(documento.getValor().toString().replace(".", "").replace(",", ""));
			
			if (!org.apache.commons.lang.math.NumberUtils.isNumber(documento.getNumero()))
				throw new SinedException("� obrigat�rio o preenchimento do n�mero da NF no campo 'Documento' do contas a pagar " + documento.getCddocumento() + ".");
			
			itauNF.setNumeroNotaFiscal(documento.getNumero());
			itauNF.setNumeroDocumento(documento.getCddocumento().toString());
//			if (bean.getVinculo().getEmpresa().getCpf() != null){
//				itauNF.setNumeroInscricao(bean.getVinculo().getEmpresa().getCpf().toString());
//			} else if (bean.getVinculo().getEmpresa().getCnpj() != null){
//				itauNF.setNumeroInscricao(bean.getVinculo().getEmpresa().getCnpj().toString());
//			}
			itauNF.setTipoIdentificacao("2");
			itauNF.setAviso("");
			itauNF.setOcorrencia("");
			
			listaNF.add(itauNF);
		}
		return listaNF;
	}
	
	/**M�todo que preenche o detalhe de Liquida��o de T�tulos (Segmento J)
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @return
	 */
	public List<ArquivoRemessaItauLT> setDetalheLTSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		List<ArquivoRemessaItauLT> listaLT = new ArrayList<ArquivoRemessaItauLT>();
		ArquivoRemessaItauLT itauLT;
		Integer numeroRegistro = 0;
		for (Documento documento : listaDocumentoRemessa) {
			numeroRegistro++;
			itauLT = new ArquivoRemessaItauLT();
//			PREENCHE O BEAN
			itauLT.setCdBanco("341");
			itauLT.setCodLote("1");
			itauLT.setTipoRegistro("3");
			itauLT.setNumeroRegistro(numeroRegistro.toString());
			itauLT.setSegmento("J");
			itauLT.setTipoMovimento("000");

			if (documento.getCodigobarras() == null || ("".equals(documento.getCodigobarras())))
				throw new SinedException("� necess�rio informar o c�digo de barras do documento " + documento.getCddocumento() + ".");
			
			ArquivoRemessaItauLT retorno = getCodigoBarras(documento.getCodigobarras());
//			C�DIGO DE BARRAS
			itauLT.setBancoFavorecido(retorno.getBancoFavorecido());
			itauLT.setMoeda(retorno.getMoeda());
			itauLT.setDv(retorno.getDv());
			if (Integer.parseInt(retorno.getVencimento()) >= 1000){
				itauLT.setVencimento(retorno.getVencimento());
				itauLT.setValorTitulo(retorno.getValorPagamento());
			}
			else {
				itauLT.setVencimento("");
				itauLT.setValorTitulo(retorno.getVencimento());
			}
			itauLT.setValor(retorno.getValor());
			itauLT.setCampoLivre(retorno.getCampoLivre());
			itauLT.setNomeFavorecido(documento.getPessoa() != null ? documento.getPessoa().getNome() : documento.getOutrospagamento());
			itauLT.setDataVencimento(padronizaData(documento.getDtvencimento()));
			itauLT.setDescontos(getDescontoForSispag(documento));
			itauLT.setAcrescimos(getAcrescimosForSispag(documento).replace(".", "").replace(",", ""));
			itauLT.setDataPagamento(padronizaData(bean.getDtpagamento()));
			itauLT.setValorPagamento(getValorLiquidoForSispag(itauLT, documento.getValor()));
			itauLT.setSeuNumero(documento.getCddocumento().toString());
			itauLT.setNossoNumero("");
			itauLT.setOcorrencia("");
			
			listaLT.add(itauLT);
		}
		return listaLT;
	}
	
	/**M�todo que preenche o detalhe de pagamento de contas de Concession�rias e Tributos com c�digo de barra (Segmento O)
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @return
	 */
	public List<ArquivoRemessaItauCT> setDetalheCTSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		List<ArquivoRemessaItauCT> listaCT = new ArrayList<ArquivoRemessaItauCT>();
		ArquivoRemessaItauCT itauCT;
		Integer numeroRegistro = 0;
		for (Documento documento : listaDocumentoRemessa) {
			numeroRegistro ++;
			itauCT = new ArquivoRemessaItauCT();
//			PREENCHE O BEAN
			itauCT.setCdBanco("341");
			itauCT.setCodLote("1");
			itauCT.setTipoRegistro("3");
			itauCT.setNumeroRegistro(numeroRegistro.toString());
			itauCT.setSegmento("O");
			itauCT.setTipoMovimento("000");
			itauCT.setCodbarras(documento.getCodigobarras());
			itauCT.setNome(StringUtils.tiraAcento(documento.getPessoa().getNome().toUpperCase()));
			itauCT.setDtVencimento(padronizaData(documento.getDtvencimento()));
			itauCT.setMoeda("REA");
			itauCT.setQtdeMoeda(documento.getValor().toString().replace(".", "").replace(",", ""));
			itauCT.setValorPagamento(documento.getValor().toString().replace(".", "").replace(",", ""));
			itauCT.setDataPagamento(padronizaData(bean.getDtpagamento()));
			itauCT.setValorPago("");
			itauCT.setNumeroNF("");
			itauCT.setSeuNumero(documento.getCddocumento().toString());
			itauCT.setNossoNumero("");
			itauCT.setOcorrencia("");
			
			listaCT.add(itauCT);
		}
		return listaCT;
	}
	
	/**M�todo que preenche o Trailer do Lote
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @param bean
	 * @return
	 */
	public ArquivoRemessaItauTrailerLote setTrailerLoteSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		Money valorPagamento = new Money();
		Integer tamanhoLista = 0;
		for (Documento documento : listaDocumentoRemessa) {
			valorPagamento = valorPagamento.add(documento.getValor());
			tamanhoLista ++;
		}
		ArquivoRemessaItauTrailerLote itauTrailerLote = new ArquivoRemessaItauTrailerLote();
		itauTrailerLote.setCdbanco("341");
		itauTrailerLote.setCodLote("1");
		itauTrailerLote.setTipoRegistro("5");
		itauTrailerLote.setQtdeRegistro(tamanhoLista.toString());
		itauTrailerLote.setValorPagamento(valorPagamento.toString().replace(".", "").replace(",", ""));
		itauTrailerLote.setOcorrencia("");
		return itauTrailerLote;
	}
	
	/**M�todo que preenche o Trailer do Arquivo
	 * @author Thiago Augusto
	 * @param listaDocumentoRemessa
	 * @param bean
	 * @return
	 */
	public ArquivoRemessaItauTrailer setTrailerSispagItau(List<Documento> listaDocumentoRemessa, BaixarContaBean bean){
		ArquivoRemessaItauTrailer itauTrailer = new ArquivoRemessaItauTrailer();
		itauTrailer.setCdBanco("341");
		itauTrailer.setCodLote("9999");
		itauTrailer.setTipoRegistro("9");
		itauTrailer.setTotalLotes("1");
		Integer quantidade = listaDocumentoRemessa.size();
		itauTrailer.setTotalRegistros(quantidade.toString());
		Money valorTotal = new Money();
		for (Documento documento : listaDocumentoRemessa) {
			valorTotal = valorTotal.add(documento.getValor());
		}
		itauTrailer.setValorTotal(valorTotal.toString());
		return itauTrailer;
	}
	
	/**M�todo para separar o c�digo de barras para o preenchimento do bean ArquivoRemessaItauLT
	 * @author Thiago Augusto
	 * @param codigo
	 * @return
	 */
	public ArquivoRemessaItauLT getCodigoBarras(String codigo){
		ArquivoRemessaItauLT retorno = new ArquivoRemessaItauLT();
		retorno.setBancoFavorecido(codigo.substring(0, 3));
		retorno.setMoeda(codigo.substring(3, 4));
		retorno.setCampoLivre(codigo.substring(4, 9) + codigo.substring(10, 20) + codigo.substring(21, 31));
		retorno.setDv(codigo.substring(32, 33));
		retorno.setVencimento(codigo.substring(33, 37));
		retorno.setValorTitulo(codigo.substring(37, codigo.length()));
		return retorno;
	}
	
	/**M�todo para fazer o c�lculo de acrescimos para o Segmento J
	 * @author Thiago Augusto
	 * @param bean
	 * @return
	 */
	public String getAcrescimosForSispag(Documento bean){
		String retorno = "";
		Date atual = new Date(System.currentTimeMillis());
		
		if(bean.getTaxa() != null){
			List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
			listaTaxaItem = taxaitemService.findTaxasItens(bean.getTaxa());
			Money valorAux = new Money();;	
			int difdias;
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				for (Taxaitem item : listaTaxaItem) {
					
					if(item.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = valorAux.add(bean.getValor().multiply(item.getValor().divide(new Money(100d))));
							} else {
								valorAux = valorAux.add(item.getValor());
							}
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = valorAux.add(bean.getValor().multiply(item.getValor().divide(new Money(100d))));
							} else {
								valorAux = valorAux.add(item.getValor());
							}
							
							difdias = SinedDateUtils.diferencaDias(atual,item.getDtlimite());
							valorAux = valorAux.multiply(new Money(new Double(difdias)));
						}
					}
				}
				retorno = valorAux.toString().replace(".", "").replace(",", "");
			}
		}
		return retorno;
	}
	
	/**M�todo para fazer o c�lculo de descontos para o Segmento J
	 * @author Thiago Ausguto
	 * @param bean
	 * @return
	 */
	public String getDescontoForSispag(Documento bean){
		String retorno = "";
		List<Taxaitem> listaTaxaItem = new ListSet<Taxaitem>(Taxaitem.class);
		if (bean.getTaxa() != null){
			listaTaxaItem = taxaitemService.findTaxasItens(bean.getTaxa());
			Money valorAux = new Money();;	
			Date atual = new Date(System.currentTimeMillis());
			int difdias;
			
			if(listaTaxaItem != null && listaTaxaItem.size() > 0){
				for (Taxaitem item : listaTaxaItem) {
					if(item.getTipotaxa().equals(Tipotaxa.DESAGIO) && SinedDateUtils.afterIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
							
							difdias = SinedDateUtils.diferencaDias(item.getDtlimite(), atual);
							valorAux = valorAux.multiply(new Money(new Double(difdias)));
						}
					}
					
					if(item.getTipotaxa().equals(Tipotaxa.DESCONTO) && SinedDateUtils.afterIgnoreHour(item.getDtlimite(), atual)){
						if(item.getGerarmensagem() == null || !item.getGerarmensagem()){
							if(item.isPercentual()){
								valorAux = bean.getValor().multiply(item.getValor().divide(new Money(100d)));
							} else {
								valorAux = item.getValor();
							}
						}
					}
				}
				retorno = valorAux.toString().replace(".", "").replace(",", "");
			}
		}
		return retorno;
	}
	
	/**M�todo que retorna o valor a ser pago no documento
	 * @author Thiago Augusto
	 * @param bean
	 * @return
	 */
	public String getValorPagoForSispag(Documento bean){
		String retorno = "";
		String desconto = getDescontoForSispag(bean);
		String acrescimo = getAcrescimos(bean);
		Money valor = bean.getValor();
		if (desconto != null && !desconto.equals(""))
			valor = valor.subtract(new Money(desconto));
		if (acrescimo != null && !acrescimo.equals(""))
			valor = valor.add(new Money(acrescimo));
		retorno = valor.toString().replace(".", "").replace(",", "");
		return retorno;
	}
	
	/**
	 * 
	 * M�todo que busca o valor l�quido pago do documento + acr�scimos - descontos.
	 *
	 * @name getValorLiquido
	 * @param itauLT
	 * @param valorDocumento
	 * @return
	 * @return String
	 * @author Thiago Augusto
	 * @date 03/09/2012
	 *
	 */
	public String getValorLiquidoForSispag(ArquivoRemessaItauLT itauLT, Money valorDocumento){
		String retorno = "000";
		Money valorDesconto;
		if (itauLT.getDescontos() != null && !itauLT.getDescontos().equals(""))
			valorDesconto = new Money(Long.parseLong(itauLT.getDescontos()), true);
		else 
			valorDesconto = new Money();
		
		Money valorAcrescimo;
		if (itauLT.getAcrescimos() != null && !itauLT.getAcrescimos().equals(""))
			valorAcrescimo = new Money(Long.parseLong(itauLT.getAcrescimos()), true);
		else 
			valorAcrescimo = new Money();
		
		valorDocumento = valorDocumento.add(valorAcrescimo).subtract(valorDesconto);
		retorno = valorDocumento.toString().replace(".", "").replace(",", "");
		return retorno;
	}
	
	public List<Arquivobancario> findByNomeArquivo(String nome){
		return arquivobancarioDAO.findByNomeArquivo(nome);
	}

	@Override
	protected ListagemResult<Arquivobancario> findForExportacao(
			FiltroListagem filtro) {
		ListagemResult<Arquivobancario> listagemResult = super.findForExportacao(filtro);
		List<Arquivobancario> lista = listagemResult.list();
		
		for (Arquivobancario arquivobancario : lista) {
			List<Arquivobancariodocumento> listaArquivoDocumento = arquivobancariodocumentoDAO.findListaForArquivoBancario(arquivobancario);
			
			int contadorClientes = 0;
			
			for (Arquivobancariodocumento abd : listaArquivoDocumento) {
				if(abd.getDocumento() != null && abd.getDocumento().getPessoa() != null){
					contadorClientes ++;
				}
			}
			
			if (contadorClientes <= 5){
				arquivobancario.setClientes(SinedUtil.listAndConcatenate(listaArquivoDocumento, "documento.pessoa.nome", ", "));
			} else {
				arquivobancario.setClientes("Diversos");
			}
			
		}
		
		return listagemResult;
	}
}
