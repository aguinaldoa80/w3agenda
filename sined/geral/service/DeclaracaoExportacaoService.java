package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.DeclaracaoExportacaoDAO;

public class DeclaracaoExportacaoService extends GenericService<DeclaracaoExportacao> {

	private DeclaracaoExportacaoDAO declaracaoExportacaoDAO;
	public void setDeclaracaoExportacaoDAO(DeclaracaoExportacaoDAO declaracaoExportacaoDAO) {this.declaracaoExportacaoDAO = declaracaoExportacaoDAO;}
	public List<DeclaracaoExportacao> findByEmpresa(Empresa empresa) {
		return declaracaoExportacaoDAO.findByEmpresa(empresa);
	}
	public List<DeclaracaoExportacao> findForSped(String whereIn) {
		return declaracaoExportacaoDAO.findForSped(whereIn);
	}
	
}
