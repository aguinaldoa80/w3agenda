package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.Arquivotefhistorico;
import br.com.linkcom.sined.geral.bean.Arquivotefitem;
import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Configuracaotefadquirente;
import br.com.linkcom.sined.geral.bean.Configuracaotefterminal;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Vendahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TefParcelamentoAdmin;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ArquivotefDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFAdquirenteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.IntegracaoTefCallbackBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.util.rest.TLSSocketConnectionFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ArquivotefService extends GenericService<Arquivotef> {
	
	private ArquivoDAO arquivoDAO;
	private ArquivotefDAO arquivotefDAO;
	private ArquivotefhistoricoService arquivotefhistoricoService;
	private ConfiguracaotefService configuracaotefService;
	private ConfiguracaotefterminalService configuracaotefterminalService;
	private NotafiscalprodutoduplicataService notafiscalprodutoduplicataService;
	private NotaHistoricoService notaHistoricoService;
	private VendapagamentoService vendapagamentoService;
	private VendahistoricoService vendahistoricoService;
	private ArquivoService arquivoService;
	private VendaService vendaService;
	private ReportTemplateService reportTemplateService;
	private EmpresaService empresaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setReportTemplateService(
			ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setVendahistoricoService(
			VendahistoricoService vendahistoricoService) {
		this.vendahistoricoService = vendahistoricoService;
	}
	public void setVendapagamentoService(
			VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setNotafiscalprodutoduplicataService(
			NotafiscalprodutoduplicataService notafiscalprodutoduplicataService) {
		this.notafiscalprodutoduplicataService = notafiscalprodutoduplicataService;
	}
	public void setConfiguracaotefterminalService(
			ConfiguracaotefterminalService configuracaotefterminalService) {
		this.configuracaotefterminalService = configuracaotefterminalService;
	}
	public void setConfiguracaotefService(
			ConfiguracaotefService configuracaotefService) {
		this.configuracaotefService = configuracaotefService;
	}
	public void setArquivotefhistoricoService(
			ArquivotefhistoricoService arquivotefhistoricoService) {
		this.arquivotefhistoricoService = arquivotefhistoricoService;
	}
	public void setArquivotefDAO(ArquivotefDAO arquivotefDAO) {
		this.arquivotefDAO = arquivotefDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	/* singleton */
	private static ArquivotefService instance;
	public static ArquivotefService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivotefService.class);
		}
		return instance;
	}
	
	public static String getURLIntegracao(){
		if(SinedUtil.isAmbienteDesenvolvimento()){
			return "https://sandbox.controlpay.com.br/webapi";
		} else {
			return "https://api.controlpay.com.br";
		}
	}

	@SuppressWarnings("unchecked")
	public void saveAndSendControlPay(Arquivotef arquivotef) throws Exception {
		
		Configuracaotef configuracaotef = arquivotef.getConfiguracaotef();
		Configuracaotefadquirente configuracaotefadquirente = arquivotef.getConfiguracaotefadquirente();
		configuracaotef = configuracaotefService.loadForEntrada(configuracaotef);
		
		Set<Configuracaotefadquirente> listaConfiguracaotefadquirente = configuracaotef.getListaConfiguracaotefadquirente();
		for (Configuracaotefadquirente it : listaConfiguracaotefadquirente) {
			if(it.equals(configuracaotefadquirente)){
				configuracaotefadquirente = it;
				break;
			}
		}
		
		TefParcelamentoAdmin parcelamentoadmin = configuracaotef.getParcelamentoadmin();
		Configuracaotefterminal configuracaotefterminal = configuracaotefterminalService.load(arquivotef.getConfiguracaotefterminal());
		
		List<Arquivotefitem> listaArquivotefitemFinal = new ArrayList<Arquivotefitem>(); 
		List<Arquivotefitem> listaArquivotefitem = arquivotef.getListaArquivotefitem();
		for (Arquivotefitem arquivotefitem : listaArquivotefitem) {
			if(arquivotefitem.getMarcado() != null && arquivotefitem.getMarcado()){
				listaArquivotefitemFinal.add(arquivotefitem);
			}
		}
		arquivotef.setListaArquivotefitem(listaArquivotefitemFinal);
		arquivotef.setSituacao(Arquivotefsituacao.GERADO);
		arquivotef.setDtenvio(SinedDateUtils.currentTimestamp());
		this.saveOrUpdate(arquivotef);
		this.createAndSaveHistorico(arquivotef, "Criado", null);
		
		String jsonRetorno = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("formaPagamentoId", arquivotef.getFormaPagamentoTef().getValue());
			map.put("terminalId", configuracaotefterminal.getIdentificador());
			map.put("referencia", arquivotef.getCdarquivotef() + "");
			map.put("aguardarTefIniciarTransacao", true);
			if(parcelamentoadmin != null && parcelamentoadmin.equals(TefParcelamentoAdmin.ADMINISTRADORA)){
				map.put("parcelamentoAdmin", true);
			} else if(parcelamentoadmin != null && parcelamentoadmin.equals(TefParcelamentoAdmin.LOJISTA)){
				map.put("parcelamentoAdmin", false);
			} else {
				map.put("parcelamentoAdmin", null);
			}
			map.put("quantidadeParcelas", arquivotef.getQuantidadeparcelas());
			map.put("adquirente", configuracaotefadquirente != null ? configuracaotefadquirente.getAdquirente() : "");
			map.put("valorTotalVendido", arquivotef.getValortotal().doubleValue());
			
			String url = getURLIntegracao() + "/Venda/Vender/?key=" + configuracaotef.getChaveintegracao();
			String json = new GsonBuilder().serializeNulls().create().toJson(map);
			
			Arquivo arquivoenvio = new Arquivo(json.getBytes(), "envio_" + SinedUtil.datePatternForReport() + ".json", "application/json");
			arquivotef.setArquivoenvio(arquivoenvio);
			arquivoDAO.saveFile(arquivotef, "arquivoenvio");
			this.updateArquivoenvio(arquivotef);
			
			SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(new StringEntity(json));
			
	        HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpPost);
	        HttpEntity entityResponse = response.getEntity();
	        
	        jsonRetorno = EntityUtils.toString(entityResponse, "UTF-8");
	        
	        Arquivo arquivoretorno = new Arquivo(jsonRetorno.getBytes(), "retorno_" + SinedUtil.datePatternForReport() + ".json", "application/json");
			arquivotef.setArquivoretorno(arquivoretorno);
			arquivoDAO.saveFile(arquivotef, "arquivoretorno");
			this.updateArquivoretorno(arquivotef);
			
			if(response.getStatusLine().getStatusCode() == 200){
				this.createAndSaveHistorico(arquivotef, "Enviado", null);
				this.updateSituacao(arquivotef, Arquivotefsituacao.ENVIADO);
			} else {
				Map<String, Object> mapRetorno = new Gson().fromJson(jsonRetorno, Map.class);
				throw new SinedException((String) mapRetorno.get("message"));
			}
		} catch (Exception e) {
			this.createAndSaveHistorico(arquivotef, "Erro no envio", e.getMessage());
			this.updateSituacao(arquivotef, Arquivotefsituacao.NAO_ENVIADO);
			throw new SinedException("Erro no envio do pagamento", e);
		}
		
		try {
	        Map<String, Object> mapRetorno = new Gson().fromJson(jsonRetorno, Map.class);
	        Map<String, Object> mapIntencaoVenda = (Map<String, Object>) mapRetorno.get("intencaoVenda");
	        Map<String, Object> mapIntencaoVendaStatus = (Map<String, Object>) mapIntencaoVenda.get("intencaoVendaStatus");
	        
	        Double intencaoVendaStatusId = (Double) mapIntencaoVendaStatus.get("id");
	        if(intencaoVendaStatusId.equals(15.0)){
	        	throw new SinedException("Venda expirada");
	        }
	        
	        if(intencaoVendaStatusId.equals(6.0)){
	        	this.createAndSaveHistorico(arquivotef, "Pagamento em andamento", null);
	        } else {
	        	throw new SinedException("Status desconhecido");
	        }
		} catch (Exception e) {
			this.createAndSaveHistorico(arquivotef, "Erro no processamento do retorno", e.getMessage());
			this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_ERRO);
			throw new SinedException("Erro no processamento do envio do pagamento: " + e.getMessage(), e);
		}
        
	}
	
	public void updateSituacao(Arquivotef arquivotef, Arquivotefsituacao situacao) {
		arquivotefDAO.updateSituacao(arquivotef, situacao);
	}
	
	public void createAndSaveHistorico(Arquivotef arquivotef, String acao, String observacao) {
		Arquivotefhistorico arquivotefhistorico = new Arquivotefhistorico();
		arquivotefhistorico.setAcao(acao);
		arquivotefhistorico.setArquivotef(arquivotef);
		arquivotefhistorico.setObservacao(observacao);
		arquivotefhistoricoService.saveOrUpdate(arquivotefhistorico);
	}

	private void updateArquivoretorno(Arquivotef arquivotef) {
		arquivotefDAO.updateArquivoretorno(arquivotef);
	}
	
	private void updateArquivoretornopagamento(Arquivotef arquivotef) {
		arquivotefDAO.updateArquivoretornopagamento(arquivotef);
	}

	private void updateArquivoenvio(Arquivotef arquivotef) {
		arquivotefDAO.updateArquivoenvio(arquivotef);
	}
	
	@SuppressWarnings("unchecked")
	public void processCallback(IntegracaoTefCallbackBean bean) throws ParseException, IOException {
		Integer cdarquivotef = Integer.parseInt(bean.getIntencaoVendaReferencia());
		Arquivotef arquivotef = this.loadForEntrada(new Arquivotef(cdarquivotef));
		if(arquivotef == null){
			throw new SinedException("N�o encontrado o pagamento " + bean.getIntencaoVendaReferencia() + " com id " + bean.getIntencaoVendaId());
		}
		
		Configuracaotef configuracaotef = arquivotef.getConfiguracaotef();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("intencaoVendaId", bean.getIntencaoVendaId());
		map.put("referencia", bean.getIntencaoVendaReferencia());
		
		String url = getURLIntegracao() + "/IntencaoVenda/GetByFiltros?key=" + configuracaotef.getChaveintegracao();
		String json = new GsonBuilder().serializeNulls().create().toJson(map);
		
		SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setEntity(new StringEntity(json));
		
        HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpPost);
        HttpEntity entityResponse = response.getEntity();
        
        String jsonRetorno = EntityUtils.toString(entityResponse, "UTF-8");
        
        Arquivo arquivoretornopagamento = new Arquivo(jsonRetorno.getBytes(), "retorno_pagamento_" + SinedUtil.datePatternForReport() + ".json", "application/json");
		arquivotef.setArquivoretornopagamento(arquivoretornopagamento);
		arquivoDAO.saveFile(arquivotef, "arquivoretornopagamento");
		this.updateArquivoretornopagamento(arquivotef);
        
		boolean sucesso = false;
        Map<String, Object> mapRetorno = new GsonBuilder().create().fromJson(jsonRetorno, Map.class);
        List<Map<String, Object>> mapListIntencoesVendas = (List<Map<String, Object>>) mapRetorno.get("intencoesVendas");
        if(mapListIntencoesVendas != null && mapListIntencoesVendas.size() == 1){
        	Map<String, Object> mapIntencaoVenda = mapListIntencoesVendas.get(0);
        	Map<String, Object> mapIntencaoVendaStatus = (Map<String, Object>) mapIntencaoVenda.get("intencaoVendaStatus");
        	Double idStatus = (Double) mapIntencaoVendaStatus.get("id");
        	if(idStatus.equals(10.0)){
        		List<Map<String, Object>> mapListPagamentosExternos = (List<Map<String, Object>>) mapIntencaoVenda.get("pagamentosExternos");
        		if(mapListPagamentosExternos != null && mapListPagamentosExternos.size() == 1){
		        	Map<String, Object> mapPagamentosExternos = mapListPagamentosExternos.get(0);
		        	
		        	String autorizacao = mapPagamentosExternos.containsKey("autorizacao") ? mapPagamentosExternos.get("autorizacao").toString() : null;
		        	String bandeira = mapPagamentosExternos.containsKey("bandeira") ? mapPagamentosExternos.get("bandeira").toString() : null;
		        	String adquirente = mapPagamentosExternos.containsKey("adquirente") ? mapPagamentosExternos.get("adquirente").toString() : null;
		        	if(autorizacao != null && bandeira != null && adquirente != null){
		        		this.updateProcessamentoSucessoArquivotef(arquivotef, autorizacao, bandeira, adquirente);
		        		this.createAndSaveHistorico(arquivotef, "Processamento do retorno de pagamento", "Autoriza��o = " + autorizacao + "<BR>Bandeira = " + bandeira + "<BR>Adquirente = " + adquirente);
		        		this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_SUCESSO);
		        		sucesso = true;
		        	} else {
		        		this.createAndSaveHistorico(arquivotef, "Erro no processamento do retorno de pagamento", "N�o foi encontrada as informa��es de autoriza��o, bandeira e/ou adquirente.");
		    			this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_ERRO);
		        	}
        		} else {
        			this.createAndSaveHistorico(arquivotef, "Erro no processamento do retorno de pagamento", "N�o foi encontrado a lista de pagamentos externos no arquivo de retorno de pagamento.");
        			this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_ERRO);
        		}
        	} else {
        		this.createAndSaveHistorico(arquivotef, "Erro no processamento do retorno de pagamento", "Pagamento com status " + mapIntencaoVendaStatus.get("nome"));
    			this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_ERRO);
        	}
        } else {
        	this.createAndSaveHistorico(arquivotef, "Erro no processamento do retorno de pagamento", "N�o foi encontrado a lista de inten��es de venda no arquivo de retorno de pagamento.");
			this.updateSituacao(arquivotef, Arquivotefsituacao.PROCESSADO_COM_ERRO);
        }
        
        if(sucesso && configuracaotef.getImpressaoautomatica() != null && configuracaotef.getImpressaoautomatica()){
        	String whereIn = "";
        	String tipo = "";
        	
        	arquivotef = this.loadForEntrada(arquivotef);
    		if(arquivotef.getListaArquivotefitem() != null && arquivotef.getListaArquivotefitem().size() > 0){
    			for (Arquivotefitem arquivotefitem : arquivotef.getListaArquivotefitem()) {
    				if(arquivotefitem.getNotafiscalprodutoduplicata() != null && arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto() != null){
    					if(whereIn != null)
    					whereIn = arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto().getCdNota() + "";
    					tipo = "nfe";
    					if(arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto().getModeloDocumentoFiscalEnum() != null &&
    							arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto().getModeloDocumentoFiscalEnum().equals(ModeloDocumentoFiscalEnum.NFCE)){
    						tipo = "nfce";
    					}
    				}
    				if(arquivotefitem.getVendapagamento() != null){
    					whereIn = arquivotefitem.getVendapagamento().getVenda().getCdvenda() + "";
    					tipo = "venda";
    				}
    			}
    		}
        	
        	
        	this.imprimirComprovante(null, whereIn, tipo);
        }
	}
	
	public void updateProcessamentoSucessoArquivotef(Arquivotef arquivotef, String autorizacao, String bandeira, String adquirente) {
		arquivotef = this.loadForEntrada(arquivotef);
		if(arquivotef.getListaArquivotefitem() != null && arquivotef.getListaArquivotefitem().size() > 0){
			for (Arquivotefitem arquivotefitem : arquivotef.getListaArquivotefitem()) {
				if(arquivotefitem.getNotafiscalprodutoduplicata() != null){
					notafiscalprodutoduplicataService.updateInfoCartao(arquivotefitem.getNotafiscalprodutoduplicata(), autorizacao, bandeira, adquirente);
					
					NotaHistorico notaHistorico = new NotaHistorico(arquivotefitem.getNotafiscalprodutoduplicata().getNotafiscalproduto(), "Duplicata: " + arquivotefitem.getNotafiscalprodutoduplicata().getNumero(), NotaStatus.PAGAMENTO_TEF_REALIZADO);
					notaHistoricoService.saveOrUpdate(notaHistorico);
				}
				if(arquivotefitem.getVendapagamento() != null){
					vendapagamentoService.updateInfoCartao(arquivotefitem.getVendapagamento(), autorizacao, bandeira, adquirente);
					
					Vendahistorico vendahistorico = new Vendahistorico();
					vendahistorico.setAcao("Pagamento com TEF Realizado");
					vendahistorico.setVenda(arquivotefitem.getVendapagamento().getVenda());
					vendahistoricoService.saveOrUpdate(vendahistorico);
				}
			}
		}
	}
	
	public List<Arquivotef> findByVenda(String whereInVenda) {
		return arquivotefDAO.findByVenda(whereInVenda);
	}
	
	public List<Arquivotef> findByNota(String whereInNota) {
		return arquivotefDAO.findByNota(whereInNota);
	}
	
	@SuppressWarnings("unchecked")
	public void preencheAdquirenteComprovante(EmitirComprovanteTEFBean bean, List<Arquivotef> listaArquivotef) {
		if(listaArquivotef != null && listaArquivotef.size() > 0){
			for (Arquivotef arquivotef : listaArquivotef) {
				Arquivo arquivoretornopagamento = arquivoService.loadWithContents(arquivotef.getArquivoretornopagamento());
				
				Map<String, Object> mapJson = new Gson().fromJson(new String(arquivoretornopagamento.getContent()), Map.class);
				List<Map<String, Object>> mapListIntencoesVendas = (List<Map<String, Object>>) mapJson.get("intencoesVendas");
		        if(mapListIntencoesVendas != null && mapListIntencoesVendas.size() == 1){
		        	Map<String, Object> mapIntencaoVenda = mapListIntencoesVendas.get(0);
		        	Map<String, Object> mapIntencaoVendaStatus = (Map<String, Object>) mapIntencaoVenda.get("intencaoVendaStatus");
		        	Double idStatus = (Double) mapIntencaoVendaStatus.get("id");
		        	if(idStatus.equals(10.0)){
		        		List<Map<String, Object>> mapListPagamentosExternos = (List<Map<String, Object>>) mapIntencaoVenda.get("pagamentosExternos");
		        		if(mapListPagamentosExternos != null && mapListPagamentosExternos.size() == 1){
				        	Map<String, Object> mapPagamentosExternos = mapListPagamentosExternos.get(0);
				        	
				        	String autorizacao = mapPagamentosExternos.containsKey("autorizacao") ? mapPagamentosExternos.get("autorizacao").toString() : null;
				        	String bandeira = mapPagamentosExternos.containsKey("bandeira") ? mapPagamentosExternos.get("bandeira").toString() : null;
				        	String adquirente = mapPagamentosExternos.containsKey("adquirente") ? mapPagamentosExternos.get("adquirente").toString() : null;
				        	String nsuTid = mapPagamentosExternos.containsKey("nsuTid") ? mapPagamentosExternos.get("nsuTid").toString() : null;
				        	String comprovanteAdquirente = mapPagamentosExternos.containsKey("comprovanteAdquirente") ? mapPagamentosExternos.get("comprovanteAdquirente").toString() : null;
				        	String dataAdquirente = mapPagamentosExternos.containsKey("dataAdquirente") ? mapPagamentosExternos.get("dataAdquirente").toString() : null;
				        	
				        	EmitirComprovanteTEFAdquirenteBean adBean = new EmitirComprovanteTEFAdquirenteBean();
				        	adBean.setAdquirente(adquirente);
				        	adBean.setAutorizacao(autorizacao);
				        	adBean.setBandeira(bandeira);
				        	adBean.setComprovante(comprovanteAdquirente);
				        	adBean.setData(dataAdquirente);
				        	adBean.setNsutid(nsuTid);
				        	bean.addAdquirente(adBean);
		        		}
		        	}
		        }
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void imprimirComprovante(WebRequestContext request, String whereIn, String tipo) throws UnsupportedEncodingException, IOException, ClientProtocolException {
		EnumCategoriaReportTemplate categoria = null;
		List<EmitirComprovanteTEFBean> lista = null;
		Empresa empresa = null;
		
		if(StringUtils.equalsIgnoreCase(tipo, "venda")){
			categoria = EnumCategoriaReportTemplate.COMPROVANTE_VENDA_TEF;
			lista = vendaService.getListaComprovanteTEFBean(whereIn);
			empresa = empresaService.getEmpresaByVendas(whereIn);
		} else if(StringUtils.equalsIgnoreCase(tipo, "nfe")){
			categoria = EnumCategoriaReportTemplate.COMPROVANTE_NFE_TEF;
			lista = notafiscalprodutoService.getListaComprovanteTEFBean(whereIn, true);
			empresa = empresaService.getEmpresaForTEFNotas(whereIn);
		} else if(StringUtils.equalsIgnoreCase(tipo, "nfce")){
			categoria = EnumCategoriaReportTemplate.COMPROVANTE_NFCE_TEF;
			lista = notafiscalprodutoService.getListaComprovanteTEFBean(whereIn, false);
			empresa = empresaService.getEmpresaForTEFNotas(whereIn);
		}
		
		Configuracaotef configuracaotef = configuracaotefService.loadByEmpresa(empresa);
		Set<Configuracaotefterminal> listaConfiguracaotefterminal = configuracaotef.getListaConfiguracaotefterminal();
		Integer codigoImpressora = null;
		for (Configuracaotefterminal configuracaotefterminal : listaConfiguracaotefterminal) {
			if(configuracaotefterminal.getImpressora() != null && configuracaotefterminal.getImpressora()){
				codigoImpressora = configuracaotefterminal.getCodigo();
			}
		}
		
		if(codigoImpressora == null) throw new SinedException("N�o foi configurada a impressora nos terminais.");
		
		ReportTemplateBean reportTemplateBean = reportTemplateService.loadTemplateByCategoria(categoria);
		if(reportTemplateBean == null) throw new SinedException("N�o foi configurada o template para impress�o.");
		
		if(lista != null && lista.size() > 0){
			StringBuilder retorno = new StringBuilder();
			for (EmitirComprovanteTEFBean bean : lista) {
				ITemplateEngine engine = new GroovyTemplateEngine().build(reportTemplateBean.getLeiaute());
				Map<String,Object> datasource = new HashMap<String, Object>();
				datasource.put("bean", bean);
				retorno.append(engine.make(datasource));
			}
			
			String referencia = "" + System.currentTimeMillis() + "#" + (Math.random() * 100);
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("referencia", referencia);
			map.put("conteudo", new br.com.linkcom.neo.util.StringUtils().tiraAcento(retorno.toString()));
			map.put("terminalId", codigoImpressora);
			map.put("aguardarClienteIniciarImpressao", true);
			
			String url = ArquivotefService.getURLIntegracao() + "/IntencaoImpressao/Insert?key=" + configuracaotef.getChaveintegracao();
			String json = new GsonBuilder().serializeNulls().create().toJson(map);
			SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(new TLSSocketConnectionFactory(), new String[] { "TLSv1.2" }, null, SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setEntity(new StringEntity(json));
			
		    HttpResponse response = HttpClientBuilder.create().setSSLSocketFactory(sf).build().execute(httpPost);
		    HttpEntity entityResponse = response.getEntity();
		    
		    String jsonRetorno = EntityUtils.toString(entityResponse, "UTF-8");
			
			boolean sucesso = false;
			Map<String, Object> mapRetorno = new Gson().fromJson(jsonRetorno, Map.class);
			if(mapRetorno.containsKey("intencaoImpressao")){
				Map<String, Object> intencaoImpressao = (Map<String, Object>) mapRetorno.get("intencaoImpressao");
				Map<String, Object> intencaoImpressaoStatus = (Map<String, Object>) intencaoImpressao.get("intencaoImpressaoStatus");
				
				Double intencaoImpressaoStatusId = (Double) intencaoImpressaoStatus.get("id");
				if(intencaoImpressaoStatusId.equals(15.0) || intencaoImpressaoStatusId.equals(10.0)){
					sucesso = true;
				}
					
				if(request != null){
					if(sucesso){
						request.addMessage("Impress�o solicitada com sucesso.");
					} else {
						request.addError("Erro na impress�o. ID: " + intencaoImpressaoStatusId.intValue());
					}
				}
			} else {
				if(request != null){
					if(mapRetorno.containsKey("message")){
						request.addError("Erro na impress�o: " + mapRetorno.get("message"));
					} else {
						request.addError("Erro na impress�o.");
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getComprovanteTEF(List<Arquivotef> listaArquivotef) {
		String comprovante = "";
		if(listaArquivotef != null && listaArquivotef.size() > 0){
			for (Arquivotef arquivotef : listaArquivotef) {
				if(arquivotef.getArquivoretornopagamento() != null && arquivotef.getArquivoretornopagamento().getCdarquivo() != null){
					Arquivo arquivoretornopagamento = arquivoService.loadWithContents(arquivotef.getArquivoretornopagamento());
					
					Map<String, Object> mapJson = new Gson().fromJson(new String(arquivoretornopagamento.getContent()), Map.class);
					List<Map<String, Object>> mapListIntencoesVendas = (List<Map<String, Object>>) mapJson.get("intencoesVendas");
			        if(mapListIntencoesVendas != null && mapListIntencoesVendas.size() == 1){
			        	Map<String, Object> mapIntencaoVenda = mapListIntencoesVendas.get(0);
			        	Map<String, Object> mapIntencaoVendaStatus = (Map<String, Object>) mapIntencaoVenda.get("intencaoVendaStatus");
			        	Double idStatus = (Double) mapIntencaoVendaStatus.get("id");
			        	if(idStatus.equals(10.0)){
			        		List<Map<String, Object>> mapListPagamentosExternos = (List<Map<String, Object>>) mapIntencaoVenda.get("pagamentosExternos");
			        		if(mapListPagamentosExternos != null && mapListPagamentosExternos.size() == 1){
					        	Map<String, Object> mapPagamentosExternos = mapListPagamentosExternos.get(0);
					        	
					        	String comprovanteAdquirente = mapPagamentosExternos.containsKey("comprovanteAdquirente") ? mapPagamentosExternos.get("comprovanteAdquirente").toString() : null;
					        	comprovante += "\n\n\n\n" + comprovanteAdquirente + "\n";
			        		}
			        	}
			        }
				}
			}
		}
		return comprovante;
	}
	
	@SuppressWarnings("unchecked")
	public String getDadosTEFForObservacao(List<Arquivotef> listaArquivotef) {
		String dadostef = "";
		if(listaArquivotef != null && listaArquivotef.size() > 0){
			for (Arquivotef arquivotef : listaArquivotef) {
				if(arquivotef.getArquivoretornopagamento() != null && arquivotef.getArquivoretornopagamento().getCdarquivo() != null){
					Arquivo arquivoretornopagamento = arquivoService.loadWithContents(arquivotef.getArquivoretornopagamento());
					
					Map<String, Object> mapJson = new Gson().fromJson(new String(arquivoretornopagamento.getContent()), Map.class);
					List<Map<String, Object>> mapListIntencoesVendas = (List<Map<String, Object>>) mapJson.get("intencoesVendas");
			        if(mapListIntencoesVendas != null && mapListIntencoesVendas.size() == 1){
			        	Map<String, Object> mapIntencaoVenda = mapListIntencoesVendas.get(0);
			        	Map<String, Object> mapIntencaoVendaStatus = (Map<String, Object>) mapIntencaoVenda.get("intencaoVendaStatus");
			        	Double idStatus = (Double) mapIntencaoVendaStatus.get("id");
			        	if(idStatus.equals(10.0)){
			        		List<Map<String, Object>> mapListPagamentosExternos = (List<Map<String, Object>>) mapIntencaoVenda.get("pagamentosExternos");
			        		if(mapListPagamentosExternos != null && mapListPagamentosExternos.size() == 1){
					        	Map<String, Object> mapPagamentosExternos = mapListPagamentosExternos.get(0);
					        	
					        	String autorizacao = mapPagamentosExternos.containsKey("autorizacao") ? mapPagamentosExternos.get("autorizacao").toString() : null;
					        	String bandeira = mapPagamentosExternos.containsKey("bandeira") ? mapPagamentosExternos.get("bandeira").toString() : null;
					        	String adquirente = mapPagamentosExternos.containsKey("adquirente") ? mapPagamentosExternos.get("adquirente").toString() : null;
					        	String nsuTid = mapPagamentosExternos.containsKey("nsuTid") ? mapPagamentosExternos.get("nsuTid").toString() : null;
					        	String dataAdquirente = mapPagamentosExternos.containsKey("dataAdquirente") ? mapPagamentosExternos.get("dataAdquirente").toString() : null;
					        	
					        	dadostef += "Pagamento com cart�o: Autoriza��o " + autorizacao + " / Bandeira " + bandeira + " / Adquirente " + adquirente + " / NSU " + nsuTid + " / Data " + dataAdquirente + "\n";
			        		}
			        	}
			        }
				}
			}
		}
		return dadostef;
	}
	
}
