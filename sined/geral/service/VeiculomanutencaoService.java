package br.com.linkcom.sined.geral.service;

import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.enumeration.EnumSituacaomanutencao;
import br.com.linkcom.sined.geral.dao.VeiculomanutencaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculomanutencaoService extends GenericService<Veiculoordemservico>{
	
	private VeiculomanutencaoDAO veiculomanutencaoDAO;
	
	public void setVeiculomanutencaoDAO(
			VeiculomanutencaoDAO veiculomanutencaoDAO) {
		this.veiculomanutencaoDAO = veiculomanutencaoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeManutencaoData(Veiculoordemservico bean) {
		return veiculomanutencaoDAO.existeManutencaoData(bean);
	}
	
	public void processarListagem(ListagemResult<Veiculoordemservico> listagemResult) {
		List<Veiculoordemservico> lista = listagemResult.list();
		for (Veiculoordemservico ordemservico : lista) {
			ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao();
			if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(1)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.PENDENTE.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(2)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.EM_ANDAMENTO.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(3)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.REALIZADA.getDesricao());
			}else if (ordemservico.getVwsituacaomanutencao().getCdsituacaomanutencao().equals(4)) {
				ordemservico.setManutencao(EnumSituacaomanutencao.CANCELADA.getDesricao());
			}
			Date dataatual = new Date(System.currentTimeMillis());
			if (ordemservico.getDtrealizada().before(dataatual)) {
				ordemservico.setVencida(Boolean.TRUE);
			}else if (ordemservico.getDtrealizada().after(dataatual) || ordemservico.getManutencao().equals("Realizada")){
				ordemservico.setVencida(Boolean.FALSE);
			}
		}
	}
	
	@Override
	protected ListagemResult<Veiculoordemservico> findForExportacao(
			FiltroListagem filtro) {
		ListagemResult<Veiculoordemservico> listagemResult = veiculomanutencaoDAO.findForExportacao(filtro);
		processarListagem(listagemResult);
		
		return listagemResult;
	}
}
