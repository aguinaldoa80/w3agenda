package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.geral.dao.PedidovendasituacaoecommerceDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendasituacaoecommerceService extends GenericService<Pedidovendasituacaoecommerce> {
	
	private PedidovendasituacaoecommerceDAO pedidovendasituacaoecommerceDAO;
	
	public void setPedidovendasituacaoecommerceDAO(
			PedidovendasituacaoecommerceDAO pedidovendasituacaoecommerceDAO) {
		this.pedidovendasituacaoecommerceDAO = pedidovendasituacaoecommerceDAO;
	}

	public List<Pedidovendasituacaoecommerce> loadForFiltro(String whereIn) {
		return pedidovendasituacaoecommerceDAO.loadForFiltro(whereIn);
	}
	public List<Pedidovendasituacaoecommerce> findByIdsecommerce(String idsecommerce) {
		return pedidovendasituacaoecommerceDAO.findByIdsecommerce(idsecommerce);
	}
	
}
