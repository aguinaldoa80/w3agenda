package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaPagamentoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendapagamentoService extends GenericService<Pedidovendapagamento> {
	
	private PedidovendapagamentoDAO pedidovendapagamentoDAO;
	
	public void setPedidovendapagamentoDAO(
			PedidovendapagamentoDAO pedidovendapagamentoDAO) {
		this.pedidovendapagamentoDAO = pedidovendapagamentoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO#findByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendapagamento> findByPedidovenda(Pedidovenda pedidovenda){
		return pedidovendapagamentoDAO.findByPedidovenda(pedidovenda);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO#existeDocumento(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 03/12/2014
	* @author Luiz Fernando
	*/
	public boolean existeDocumento(Pedidovenda pedidovenda){
		return pedidovendapagamentoDAO.existeDocumento(pedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO#existItemSemDocumento(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public boolean existItemSemDocumento(Pedidovenda pedidovenda){
		return pedidovendapagamentoDAO.existItemSemDocumento(pedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO#updateReferenciaCheque(String whereIn)
	 *
	 * @param whereIn
	 * @author Mairon Cezar
	 * @since 18/11/2016
	 */
	public void updateReferenciaCheque(String whereIn) {
		pedidovendapagamentoDAO.updateReferenciaCheque(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendapagamentoDAO#findForChequeByDocumento(Documento documento)
	 *
	 * @param documento
	 * @author Mairon Cezar
	 * @since 18/11/2016
	 */
	public Pedidovendapagamento findForChequeByDocumento(Documento documento) {
		return pedidovendapagamentoDAO.findForChequeByDocumento(documento);
	}

	public List<Pedidovendapagamento> buscarPorVendaDoClienteSemContaReceber(
			Cliente cliente) {
		return pedidovendapagamentoDAO.buscarPorVendaDoClienteSemContaReceber(cliente);
	}

	public List<Pedidovendapagamento> findPedidovendaPagamentoByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendapagamentoDAO.findPedidovendaPagamentoByPedidovenda(pedidovenda);
	}
	
	public List<PedidoVendaPagamentoWSBean> toWSList(List<Pedidovendapagamento> listaPedidoVendaPagamento){
		List<PedidoVendaPagamentoWSBean> lista = new ArrayList<PedidoVendaPagamentoWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidoVendaPagamento)){
			for(Pedidovendapagamento pvp: listaPedidoVendaPagamento){
				PedidoVendaPagamentoWSBean bean = new PedidoVendaPagamentoWSBean();
				bean.setCdpedidovendapagamento(pvp.getCdpedidovendapagamento());
				bean.setAgencia(pvp.getAgencia());
				bean.setBanco(pvp.getBanco());
				bean.setCheque(ObjectUtils.translateEntityToGenericBean(pvp.getCheque()));
				bean.setConta(pvp.getConta());
				bean.setCpfcnpj(pvp.getCpfcnpj());
				bean.setDataparcela(pvp.getDataparcela());
				bean.setDocumento(ObjectUtils.translateEntityToGenericBean(pvp.getDocumento()));
				bean.setDocumentoantecipacao(ObjectUtils.translateEntityToGenericBean(pvp.getDocumentoantecipacao()));
				bean.setDocumentotipo(ObjectUtils.translateEntityToGenericBean(pvp.getDocumentotipo()));
				bean.setEmitente(pvp.getEmitente());
				bean.setNumero(pvp.getNumero());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvp.getPedidovenda()));
				bean.setValorjuros(pvp.getValorjuros());
				bean.setValororiginal(pvp.getValororiginal());
				
				lista.add(bean);
			}
		}
		return lista;
	}
}
