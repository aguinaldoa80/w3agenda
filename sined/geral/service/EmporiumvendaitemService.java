package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.EmporiumvendaitemDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumvendaitemService extends GenericService<Emporiumvendaitem>{
	
	private EmporiumvendaitemDAO emporiumvendaitemDAO;
	
	public void setEmporiumvendaitemDAO(
			EmporiumvendaitemDAO emporiumvendaitemDAO) {
		this.emporiumvendaitemDAO = emporiumvendaitemDAO;
	}

	public List<Emporiumvendaitem> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return emporiumvendaitemDAO.findByEmporiumvenda(emporiumvenda);
	}
	
	public List<Emporiumvendaitem> getEmporiumvendaitemByDataMovimento(Date datamovimento, List<Emporiumvendaitem> listaEmporiumvendaitem, Empresa empresa) {
		List<Emporiumvendaitem> listaFinal = new ArrayList<Emporiumvendaitem>();
		
		for (Emporiumvendaitem emporiumvendaitem : listaEmporiumvendaitem) {
			if(emporiumvendaitem.getEmporiumvenda() != null &&
					emporiumvendaitem.getEmporiumvenda().getLoja().equals(empresa.getIntegracaopdv()) &&
					emporiumvendaitem.getEmporiumvenda().getData_hora() != null &&
					SinedDateUtils.equalsIgnoreHour(SinedDateUtils.timestampToDate(emporiumvendaitem.getEmporiumvenda().getData_hora()), datamovimento)){
				listaFinal.add(emporiumvendaitem);
			}
		}
		
		return listaFinal;
	}

}
