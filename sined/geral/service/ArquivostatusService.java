package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivostatus;
import br.com.linkcom.sined.geral.dao.ArquivostatusDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivostatusService extends GenericService<Arquivostatus> {	
	
	private ArquivostatusDAO arquivostatusDAO;
	
	public void setArquivostatusDAO(ArquivostatusDAO arquivostatusDAO) {
		this.arquivostatusDAO = arquivostatusDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivostatusDAO#findArquivoStatusAguardandoResposta()
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Arquivostatus findArquivoStatusAguardandoResposta(){
		return arquivostatusDAO.findArquivoStatusAguardandoResposta();
	}
	
	/* singleton */
	private static ArquivostatusService instance;
	public static ArquivostatusService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivostatusService.class);
		}
		return instance;
	}
	
}
