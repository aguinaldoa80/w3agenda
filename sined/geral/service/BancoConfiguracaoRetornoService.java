package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.lkutil.csv.CSVWriter;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaAgrupamento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoIdentificador;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoOcorrencia;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoRejeicao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentoconfirmacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoCriarMovimentacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoDataMovimentacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoOperacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoSituacaoProtestoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRetornoDAO;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRetornoSegmentoDetalheDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.ArquivoW3erpUtil;

public class BancoConfiguracaoRetornoService extends GenericService<BancoConfiguracaoRetorno>{

	private BancoConfiguracaoRetornoDAO bancoConfiguracaoRetornoDAO;
	private ContaService contaService;
	private DocumentoService documentoService;
	private ContareceberService contareceberService;
	private ContapagarService contapagarService;
	private RateioService rateioService;
	private NotaDocumentoService notaDocumentoService;
	private MovimentacaoService movimentacaoService;
	private ValecompraService valecompraService;
	private FechamentofinanceiroService fechamentofinanceiroService;
	private DocumentohistoricoService documentohistoricoService;
	private ArquivobancarioService arquivobancarioService;
	private DocumentoconfirmacaoService documentoconfirmacaoService;
	private BancoConfiguracaoRemessaAgrupamentoService bancoConfiguracaoRemessaAgrupamentoService;
	private TaxaService taxaService;
	private TaxaitemService taxaitemService;
	private BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	private ContratoService contratoService;
	private ParametrogeralService parametrogeralService;
	private BancoConfiguracaoRetornoSegmentoDetalheService bancoConfiguracaoRetornoSegmentoDetalheService;
	private BancoConfiguracaoRetornoCampoService bancoConfiguracaoRetornoCampoService;
	private BancoConfiguracaoRetornoOcorrenciaService bancoConfiguracaoRetornoOcorrenciaService;
	private BancoConfiguracaoRetornoRejeicaoService bancoConfiguracaoRetornoRejeicaoService;
	private BancoConfiguracaoRetornoIdentificadorService bancoConfiguracaoRetornoIdentificadorService;
	
	
	public void setBancoConfiguracaoRetornoIdentificadorService(BancoConfiguracaoRetornoIdentificadorService bancoConfiguracaoRetornoIdentificadorService) {this.bancoConfiguracaoRetornoIdentificadorService = bancoConfiguracaoRetornoIdentificadorService;}
	public void setBancoConfiguracaoRetornoRejeicaoService(BancoConfiguracaoRetornoRejeicaoService bancoConfiguracaoRetornoRejeicaoService) {this.bancoConfiguracaoRetornoRejeicaoService = bancoConfiguracaoRetornoRejeicaoService;}
	public void setBancoConfiguracaoRetornoOcorrenciaService(BancoConfiguracaoRetornoOcorrenciaService bancoConfiguracaoRetornoOcorrenciaService) {this.bancoConfiguracaoRetornoOcorrenciaService = bancoConfiguracaoRetornoOcorrenciaService;}
	public void setBancoConfiguracaoRetornoCampoService(BancoConfiguracaoRetornoCampoService bancoConfiguracaoRetornoCampoService) {this.bancoConfiguracaoRetornoCampoService = bancoConfiguracaoRetornoCampoService;}
	public void setBancoConfiguracaoRetornoSegmentoDetalheService(BancoConfiguracaoRetornoSegmentoDetalheService bancoConfiguracaoRetornoSegmentoDetalheService) {this.bancoConfiguracaoRetornoSegmentoDetalheService = bancoConfiguracaoRetornoSegmentoDetalheService;}
	public void setBancoConfiguracaoRemessaAgrupamentoService(BancoConfiguracaoRemessaAgrupamentoService bancoConfiguracaoRemessaAgrupamentoService) {this.bancoConfiguracaoRemessaAgrupamentoService = bancoConfiguracaoRemessaAgrupamentoService;}
	public void setBancoConfiguracaoRetornoDAO(BancoConfiguracaoRetornoDAO bancoConfiguracaoRetornoDAO) {this.bancoConfiguracaoRetornoDAO = bancoConfiguracaoRetornoDAO;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}	
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setContapagarService(ContapagarService contapagarService) {this.contapagarService = contapagarService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {this.notaDocumentoService = notaDocumentoService;}	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}	
	public void setFechamentofinanceiroService(FechamentofinanceiroService fechamentofinanceiroService) {this.fechamentofinanceiroService = fechamentofinanceiroService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {this.arquivobancarioService = arquivobancarioService;}
	public void setDocumentoconfirmacaoService(DocumentoconfirmacaoService documentoconfirmacaoService) {this.documentoconfirmacaoService = documentoconfirmacaoService;}
	public void setTaxaService(TaxaService taxaService) {this.taxaService = taxaService;}
	public void setTaxaitemService(TaxaitemService taxaitemService) {this.taxaitemService = taxaitemService;}
	public void setBancoConfiguracaoRetornoSegmentoService(BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService) {this.bancoConfiguracaoRetornoSegmentoService = bancoConfiguracaoRetornoSegmentoService;}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {this.arquivobancariodocumentoService = arquivobancariodocumentoService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	public List<BancoConfiguracaoRetorno> findByBancoAndTamanhoAndTipo(Banco banco, Integer tamanho, BancoConfiguracaoRemessaTipoEnum tipo){
		return bancoConfiguracaoRetornoDAO.findByBancoAndTamanhoAndTipo(banco, tamanho, tipo);
	}
	
	public ArquivoConfiguravelRetornoBean processarArquivoRetorno(ArquivoConfiguravelRetornoBean bean) throws Exception {
		
		if (bean.getConta() == null || bean.getContacarteira() == null || bean.getTipo() == null || bean.getArquivo() == null)
			throw new SinedException("� obrigat�rio informar a conta, carteira, tipo e arquivo.");
		
		String stringArquivo = new String(bean.getArquivo().getContent());
		String[] linhas = stringArquivo.split("\\r?\\n");
		
		if (linhas.length == 1 && ArquivoW3erpUtil.getTotalLinhas(new StringBuilder(stringArquivo)) > 1){
			linhas = stringArquivo.split("\r");
		}
			
		System.out.println(ArquivoW3erpUtil.getTotalLinhas(new StringBuilder(stringArquivo)));
		if (linhas.length == 0)
			throw new SinedException("Arquivo de retorno n�o possui informa��o.");			
			
		Conta conta = contaService.carregaConta(bean.getConta(), bean.getContacarteira());		
		BancoConfiguracaoRetorno conf = null;
		
		if (BancoConfiguracaoRemessaTipoEnum.COBRANCA.equals(bean.getTipo())){
			if (conta == null || conta.getContacarteira() == null || 
					conta.getContacarteira().getBancoConfiguracaoRetorno() == null ||
						conta.getContacarteira().getBancoConfiguracaoRetorno().getCdbancoconfiguracaoretorno() == null){
				throw new SinedException("Arquivo de retorno n�o definido na conta banc�ria selecionada.");
			} else{
				List<BancoConfiguracaoRetornoSegmentoDetalhe> listaAux = new ListSet<BancoConfiguracaoRetornoSegmentoDetalhe>(BancoConfiguracaoRetornoSegmentoDetalhe.class);;
				conf = this.loadForEntrada(conta.getContacarteira().getBancoConfiguracaoRetorno());
				if(SinedUtil.isListNotEmpty(conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe())){
					for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
						bcrsd.setListaBancoConfiguracaoRetornoCampo(new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class,bancoConfiguracaoRetornoCampoService.findBySegmento(bcrsd)));
						listaAux.add(bcrsd);
					}
					conf.setListaBancoConfiguracaoRetornoSegmentoDetalhe(new ListSet<BancoConfiguracaoRetornoSegmentoDetalhe>(BancoConfiguracaoRetornoSegmentoDetalhe.class,listaAux));
				}
			}
		} else if (BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo())){ 
			if (conta == null || conta.getContacarteira() == null || 
					conta.getContacarteira().getBancoConfiguracaoRetornoPagamento() == null ||
						conta.getContacarteira().getBancoConfiguracaoRetornoPagamento().getCdbancoconfiguracaoretorno() == null){
				throw new SinedException("Arquivo de retorno n�o definido na conta banc�ria selecionada.");
			} else{
				conf = this.loadForEntrada(conta.getContacarteira().getBancoConfiguracaoRetornoPagamento());
				if(SinedUtil.isListNotEmpty(conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe())){
					for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : conf.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
						bcrsd.setListaBancoConfiguracaoRetornoCampo(new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class,bancoConfiguracaoRetornoCampoService.findBySegmento(bcrsd)));
					}
				}
			}
		}			
	
		String identificador;
		if(parametrogeralService.getBoolean(Parametrogeral.CONFIGURACAO_RETORNO_VARIOS_SEGMENTOS)&& bean.getTipo().equals(BancoConfiguracaoRemessaTipoEnum.PAGAMENTO)){
			if (conf != null){
				
				String agenciacedente = null;
				String contacedente = null;
				String linhaHeader = null;	
					

				BancoConfiguracaoRetornoIdentificador campoIdentificadorHeader = conf.getCampo2(BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER);
				List<String> listaIdentificadorHeader = conf.getIdentificadoresByTipo2(BancoConfiguracaoTipoSegmentoEnum.HEADER);
				
				if (campoIdentificadorHeader != null){			
					for (int i=0; i<linhas.length; i++){
						identificador = campoIdentificadorHeader.getValorString(linhas[i]); 
						if (listaIdentificadorHeader.contains(identificador)){
							linhaHeader = linhas[i];
							break;
						}
					}
				}

				//Busca a linha do Header e faz a verifica��o de agencia e conta
				if (linhaHeader != null){
					agenciacedente = conf.getValorCampoLinha2(linhaHeader, BancoConfiguracaoCampoEnum.AGENCIA_CEDENTE);
					contacedente = conf.getValorCampoLinha2(linhaHeader, BancoConfiguracaoCampoEnum.CONTA_CEDENTE);
					if (agenciacedente != null && contacedente != null){					
						Boolean contaAgenciaValidado = Boolean.FALSE;
						
						if(contacedente.equals(conta.getNumero()) && agenciacedente.equals(conta.getAgencia())){							
							contaAgenciaValidado = Boolean.TRUE;
						} else if(conta.getContacarteira() != null){
							if(contacedente.equals(conta.getContacarteira().getConvenio()))
								contaAgenciaValidado = Boolean.TRUE;						
						}
						
						if (!contaAgenciaValidado.booleanValue())
							throw new SinedException("A agencia/conta do arquivo de retorno n�o corresponde a conta banc�ria selecionada. Ag: " + agenciacedente + " Cc: " + contacedente);
					}
				}
			}
			
			if (conf == null)
				throw new SinedException("Arquivo inv�lido para a conta banc�ria selecionada.");
			
			//Verifica se o arquivo j� foi processado. Busca pelo nome do arquivo
			List<Arquivobancario> listaArquivobancario = arquivobancarioService.findByNomeArquivo(bean.getArquivo().getNome());
			if (listaArquivobancario != null && !listaArquivobancario.isEmpty())
				bean.setProcessado(true);
			
			bean.setBancoConfiguracaoRetorno(conf);
			
			BancoConfiguracaoRetornoIdentificador campoIdentificadorDetalhe = conf.getCampo2(BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE);
			if (campoIdentificadorDetalhe == null)
				throw new SinedException("� obrigat�rio definir as posi��es do campo 'Identificador (Detalhe)' nas configura��es do retorno.");
			
			ArquivoConfiguravelRetornoDocumentoBean retornoDocumento;
			List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento = new ArrayList<ArquivoConfiguravelRetornoDocumentoBean>();
			
			List<String> listaIdentificadorDetalhe = conf.getIdentificadoresByTipo2(BancoConfiguracaoTipoSegmentoEnum.DETALHE);		
			Integer qtdeLinhaDetalhe = listaIdentificadorDetalhe.size();
				
			//Processa cada linha do detalhe para formar o bean do arquivo de retorno
			for (int i=0; i<linhas.length; i++){
				identificador = campoIdentificadorDetalhe.getValorString(linhas[i]); 
				if (listaIdentificadorDetalhe.contains(identificador)){
					retornoDocumento = new ArquivoConfiguravelRetornoDocumentoBean();				
					if (qtdeLinhaDetalhe > 1){
						for (int j = 0; j<qtdeLinhaDetalhe; j++){
							retornoDocumento.preparaMultisegmento(linhas[i], conf);
							if (j<qtdeLinhaDetalhe-1)
								i++;
						}
					} else
						retornoDocumento.preparaMultisegmento(linhas[i], conf);
					
					retornoDocumento.setSequencial(i);
					listaDocumento.add(retornoDocumento);
				}			
			}	
			bean.setListaDocumento(listaDocumento);
			bean.setMsgEstrutura(conf.getMsgEstrutura());
		}else{
			if (conf != null){
				ListSet<BancoConfiguracaoRetornoCampo> listaAux; 
				listaAux = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class, conf.getListaBancoConfiguracaoRetornoCampo());
				Collections.sort(listaAux, new Comparator<BancoConfiguracaoRetornoCampo>() {
					public int compare(BancoConfiguracaoRetornoCampo o1, BancoConfiguracaoRetornoCampo o2) {
						return o1.getCampo().getOrdem().compareTo(o2.getCampo().getOrdem());
					}
				});
				conf.setListaBancoConfiguracaoRetornoCampo(listaAux);
				
				String agenciacedente = null;
				String contacedente = null;
				String linhaHeader = null;	
					
				BancoConfiguracaoRetornoCampo campoIdentificadorHeader = conf.getCampo(BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER);
				List<String> listaIdentificadorHeader = conf.getIdentificadoresByTipo(BancoConfiguracaoTipoSegmentoEnum.HEADER);
				
				if (campoIdentificadorHeader != null){			
					for (int i=0; i<linhas.length; i++){
						identificador = campoIdentificadorHeader.getValorString(linhas[i]); 
						if (listaIdentificadorHeader.contains(identificador)){
							linhaHeader = linhas[i];
							break;
						}
					}
				}
	
				//Busca a linha do Header e faz a verifica��o de agencia e conta
				if (linhaHeader != null){
					agenciacedente = conf.getValorCampoLinha(linhaHeader, BancoConfiguracaoCampoEnum.AGENCIA_CEDENTE);
					contacedente = conf.getValorCampoLinha(linhaHeader, BancoConfiguracaoCampoEnum.CONTA_CEDENTE);
					if (agenciacedente != null && contacedente != null){					
						Boolean contaAgenciaValidado = Boolean.FALSE;
						
						if(contacedente.equals(conta.getNumero()) && agenciacedente.equals(conta.getAgencia())){							
							contaAgenciaValidado = Boolean.TRUE;
						} else if(conta.getContacarteira() != null){
							if(contacedente.equals(conta.getContacarteira().getConvenio()))
								contaAgenciaValidado = Boolean.TRUE;						
						}
						
						if (!contaAgenciaValidado.booleanValue())
							throw new SinedException("A agencia/conta do arquivo de retorno n�o corresponde a conta banc�ria selecionada. Ag: " + agenciacedente + " Cc: " + contacedente);
					}
				}
			}
			
			if (conf == null)
				throw new SinedException("Arquivo inv�lido para a conta banc�ria selecionada.");
			
			//Verifica se o arquivo j� foi processado. Busca pelo nome do arquivo
			List<Arquivobancario> listaArquivobancario = arquivobancarioService.findByNomeArquivo(bean.getArquivo().getNome());
			if (listaArquivobancario != null && !listaArquivobancario.isEmpty())
				bean.setProcessado(true);
			
			bean.setBancoConfiguracaoRetorno(conf);
			
			BancoConfiguracaoRetornoCampo campoIdentificadorDetalhe = conf.getCampo(BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE);
			if (campoIdentificadorDetalhe == null)
				throw new SinedException("� obrigat�rio definir as posi��es do campo 'Identificador (Detalhe)' nas configura��es do retorno.");
			
			ArquivoConfiguravelRetornoDocumentoBean retornoDocumento;
			List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumento = new ArrayList<ArquivoConfiguravelRetornoDocumentoBean>();
			
			List<String> listaIdentificadorDetalhe = conf.getIdentificadoresByTipo(BancoConfiguracaoTipoSegmentoEnum.DETALHE);		
			Integer qtdeLinhaDetalhe = listaIdentificadorDetalhe.size();
				
			//Processa cada linha do detalhe para formar o bean do arquivo de retorno
			for (int i=0; i<linhas.length; i++){
				identificador = campoIdentificadorDetalhe.getValorString(linhas[i]); 
				if (listaIdentificadorDetalhe.contains(identificador)){
					retornoDocumento = new ArquivoConfiguravelRetornoDocumentoBean();				
					if (qtdeLinhaDetalhe > 1){
						for (int j = 0; j<qtdeLinhaDetalhe; j++){
							retornoDocumento.processaLinha(linhas[i], conf);
							if (j<qtdeLinhaDetalhe-1)
								i++;
						}
					} else
						retornoDocumento.processaLinha(linhas[i], conf);
					
					retornoDocumento.setSequencial(i);
					listaDocumento.add(retornoDocumento);
				}			
			}	
			bean.setListaDocumento(listaDocumento);
			bean.setMsgEstrutura(conf.getMsgEstrutura());
		}
		
		//Busca e identifica os documentos relacionados no arquivo de retorno pelo nosso n�mero/uso empresa
		this.identificarDocumentosArquivoRetorno(bean);		
		
		for (ArquivoConfiguravelRetornoDocumentoBean it : bean.getListaDocumento()) {
			if(BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(it.getAcao()) || BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(it.getAcao())){
				Date dtreferencia = new Date(System.currentTimeMillis());
				if(it.getDtpagamento() != null){ 
					dtreferencia = new Date(it.getDtpagamento().getTime());
				} else if(it.getDtocorrencia() != null) {
					dtreferencia = new Date(it.getDtocorrencia().getTime());
				}
				
				Money valorDoc = null;
				if(it.getDocumento() != null && it.getDocumento().getCddocumento() != null){
					valorDoc = documentoService.getValorAtual(it.getDocumento().getCddocumento(), dtreferencia);
					if(valorDoc == null){
						valorDoc =  it.getDocumento().getAux_documento() != null ? 
									it.getDocumento().getAux_documento().getValoratual() : null;
					}else if(it.getDocumento().getAux_documento() != null){
						it.getDocumento().getAux_documento().setValoratual(valorDoc);
					}
					
				}
											
				Money valorPago = it.getValorpago();
				
				if(valorDoc != null && valorPago != null){
					Double valorPagoRound = SinedUtil.round(valorPago.getValue().doubleValue(), 2);
					Double valorDocRound = SinedUtil.round(valorDoc.getValue().doubleValue(), 2);
					if(valorDocRound.compareTo(valorPagoRound) != 0){
						it.setValordiferente(true);
					}
					if(valorDocRound > valorPagoRound){
						it.setExibirBotaoGerarContaReceber(true);
					}
				}
			}
		}
		
		return bean;
	}
	
	private void identificarDocumentosArquivoRetorno(ArquivoConfiguravelRetornoBean bean) throws Exception {
		if (bean.getListaDocumento() == null || bean.getListaDocumento().isEmpty())
			throw new SinedException("Nenhum documento encontrado no arquivo de retorno informado.");
		
		boolean tipoPagamento = BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo());
		
		Documentoclasse documentoclasse;
		if (tipoPagamento){
			documentoclasse = Documentoclasse.OBJ_PAGAR;
		} else {
			documentoclasse = Documentoclasse.OBJ_RECEBER;
		}
		
		if(tipoPagamento){
			List<BancoConfiguracaoRemessaAgrupamento> listaAgrupamento = bancoConfiguracaoRemessaAgrupamentoService.findForRetornoConfiguravel(bean.getListaDocumento());
			List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumentoAntigo = bean.getListaDocumento();
			List<ArquivoConfiguravelRetornoDocumentoBean> listaDocumentoNovo = new ArrayList<ArquivoConfiguravelRetornoDocumentoBean>();
			
			for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : listaDocumentoAntigo){
				List<Documento> listaDocumentoVinculado = new ArrayList<Documento>();
				for (BancoConfiguracaoRemessaAgrupamento bancoConfiguracaoRemessaAgrupamento : listaAgrupamento) {
					Long identificador = bancoConfiguracaoRemessaAgrupamento.getIdentificador().longValue();
					if((retornoBean.getNossonumeroLong() != null && retornoBean.getNossonumeroLong().equals(identificador)) ||
						(retornoBean.getUsoempresaLong() != null && retornoBean.getUsoempresaLong().equals(identificador))){
						listaDocumentoVinculado.add(bancoConfiguracaoRemessaAgrupamento.getDocumento());
						retornoBean.setAgrupado(Boolean.TRUE);
					}
				}
				retornoBean.setListaDocumento(listaDocumentoVinculado);
			}
			
			for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : listaDocumentoAntigo) {
				if(retornoBean.getAgrupado() != null && retornoBean.getAgrupado()){
					List<Documento> listaDocumentoVinculado = retornoBean.getListaDocumento();
					for (Documento documento : listaDocumentoVinculado) {
						ArquivoConfiguravelRetornoDocumentoBean retornoBeanNovo = new ArquivoConfiguravelRetornoDocumentoBean(retornoBean);
						retornoBeanNovo.getListaDocumento().add(documento);
						listaDocumentoNovo.add(retornoBeanNovo);
					}
				} else {
					listaDocumentoNovo.add(retornoBean);
				}
			}
			bean.setListaDocumento(listaDocumentoNovo);
		}
		//Se for processamento de arquivo de contas a receber, s� devem ser localizados os documentos referentes � conta selecionada no filtro
		Conta contaConsiderar = bean.getBancoConfiguracaoRetorno().getTipo().equals(BancoConfiguracaoRemessaTipoEnum.COBRANCA)? bean.getConta(): null;
		List<Documento> listaDocumento = documentoService.findForRetornoConfiguravel(bean.getListaDocumento(), contaConsiderar, documentoclasse); 
		
		//Faz a vincula��o dos documentos
		for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){
			if(retornoBean.getAgrupado() != null && retornoBean.getAgrupado()) continue;
			List<Documento> listaDocumentoVinculado = new ArrayList<Documento>();
			for (Documento doc : listaDocumento){
				Long cddocumento = doc.getCddocumento().longValue();
				if ((retornoBean.getNossonumero() != null && retornoBean.getNossonumero().equals(doc.getNossonumero()))){
					listaDocumentoVinculado.add(doc);					
				} else if(doc.getSomentenossonumero() == null || !doc.getSomentenossonumero()){
					if((retornoBean.getNossonumeroLong() != null && retornoBean.getNossonumeroLong().equals(cddocumento)) ||
							(retornoBean.getUsoempresaLong() != null && retornoBean.getUsoempresaLong().equals(cddocumento))){
						listaDocumentoVinculado.add(doc);
					}
				}	
			}	
			
			if(listaDocumentoVinculado != null && listaDocumentoVinculado.size() > 1 && (retornoBean.getAgrupado() == null || !retornoBean.getAgrupado())){
				List<Documento> listaNova = new ArrayList<Documento>();
				for(Documento documento : listaDocumentoVinculado){
					if(documento.getDocumentoacao() == null 
							|| !Documentoacao.BAIXADA.equals(documento.getDocumentoacao()) 
							|| !arquivobancariodocumentoService.existeProcessamentoArquivoRetorno(documento)){
						listaNova.add(documento);
					}
				}
				retornoBean.setListaDocumento(listaNova);
			}else {
				retornoBean.setListaDocumento(listaDocumentoVinculado);
			}
		}
		
		//Faz o tratamento dos documentos vinculados
		for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){			
			
			if (retornoBean.getListaDocumento() != null && retornoBean.getListaDocumento().size() > 0){
				if (retornoBean.getListaDocumento().size() == 1 || (retornoBean.getAgrupado() != null && retornoBean.getAgrupado())){
					retornoBean.setDocumento(retornoBean.getListaDocumento().get(0));
					if (Documentoacao.BAIXADA.equals(retornoBean.getDocumento().getDocumentoacao()))
						retornoBean.setOperacao(BancoConfiguracaoRetornoOperacaoEnum.REGISTRAR);
					else
						retornoBean.setOperacao(BancoConfiguracaoRetornoOperacaoEnum.PROCESSAR);
				} else {
					retornoBean.setNomesacado("Registro n�o identificado, encontrado mais de um registro com o mesmo nosso n�mero.");
					retornoBean.setOperacao(BancoConfiguracaoRetornoOperacaoEnum.IDENTIFICAR);
				}					
			} else {
				retornoBean.setNomesacado("Nosso n�mero n�o encontrado.");
				retornoBean.setOperacao(BancoConfiguracaoRetornoOperacaoEnum.REGISTRAR);
			}
			
			if(retornoBean.getDocumento() != null && Documentoacao.BAIXADA.equals(retornoBean.getDocumento().getDocumentoacao())){
				retornoBean.setAcaoOriginal(retornoBean.getAcao());
				retornoBean.setAcao(null);
			}
			
			if (retornoBean.getAcao() != null && retornoBean.getDocumento() != null){
				retornoBean.setMarcado(true);
			}else {
				retornoBean.setMarcado(false);
			}
		}
		
	}
	
	public void processarDocumentosArquivoRetorno(ArquivoConfiguravelRetornoBean bean, Arquivobancario arquivobancario) throws Exception {
				
		if (bean != null && bean.getListaDocumento() != null){			
		
			if (bean.getBancoConfiguracaoRetorno() == null)
				throw new SinedException("Configura��o do arquivo de retorno n�o encontrada.");
			else
				bean.setBancoConfiguracaoRetorno(this.loadForEntrada(bean.getBancoConfiguracaoRetorno()));			

			List<Documento> listaDocumentoValecompra = new ArrayList<Documento>();
			List<Documento> listaDocumentoForComissionamento = new ArrayList<Documento>();
			List<Movimentacao> listaMovimentacao = new ArrayList<Movimentacao>();	
			HashMap<String, Money> mapTarifas = new HashMap<String, Money>();
			HashMap<String, List<ArquivoConfiguravelRetornoDocumentoBean>> mapRetornoAgrupado = new HashMap<String, List<ArquivoConfiguravelRetornoDocumentoBean>>();
			StringBuilder idsBaixado = new StringBuilder();
			
			for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()){
				
				if(retornoBean.getMarcado() == null || !retornoBean.getMarcado()){
					continue;
				}
				
				if (retornoBean.getDocumento() == null){
					retornoBean.addMensagem("Nenhum documento vinculado para ser processado.");
					continue;
				}
				
				//Verifica��o de data limite do ultimo fechamento.
				Documento aux = new Documento();
				aux.setWhereIn(retornoBean.getDocumento().getCddocumento().toString());
				if(fechamentofinanceiroService.verificaListaFechamento(aux)){
					retornoBean.addMensagem("A data de vencimento deste documento refere-se a um per�odo j� fechado.");
					continue;
				}

				documentoService.updateAssociadomanualmente(retornoBean.getDocumento());
				
				if (BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo())) 
					retornoBean.setDocumento(contapagarService.loadForEntradaWtihDadosPessoa(retornoBean.getDocumento()));
				else
					retornoBean.setDocumento(contareceberService.loadForEntradaWithDadosPessoa(retornoBean.getDocumento()));
				
				//Grava o nosso n�mero na tabela documento
				if (retornoBean.getDocumento() != null && retornoBean.getNossonumero() != null){
					Documento documento = retornoBean.getDocumento();					
					documento.setNossonumero(retornoBean.getNossonumero());
					documentoService.updateNossoNumero(documento);
				}		
				
				if (BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(retornoBean.getAcao()) || 
						BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO.equals(retornoBean.getAcao())){
					if (BancoConfiguracaoRetornoCriarMovimentacaoEnum.AGRUPADA.equals(bean.getBancoConfiguracaoRetorno().getCriarMovimentacao())){
						String identificador;
						SimpleDateFormat formatadorData = new SimpleDateFormat("ddMMyyyy");
						
						if (BancoConfiguracaoRetornoDataMovimentacaoEnum.DATA_ATUAL.equals(bean.getBancoConfiguracaoRetorno().getConsiderardatamovimentacao())){
							identificador = formatadorData.format(SinedDateUtils.currentDate());
						}else if (BancoConfiguracaoRetornoDataMovimentacaoEnum.DATA_CREDITO.equals(bean.getBancoConfiguracaoRetorno().getConsiderardatamovimentacao())){
							identificador = retornoBean.getDtcredito()!=null ? formatadorData.format(retornoBean.getDtcredito()) : formatadorData.format(SinedDateUtils.currentDate());
						}else if (BancoConfiguracaoRetornoDataMovimentacaoEnum.DATA_PAGAMENTO.equals(bean.getBancoConfiguracaoRetorno().getConsiderardatamovimentacao())){
							identificador = retornoBean.getDtpagamento()!=null ? formatadorData.format(retornoBean.getDtpagamento()) : formatadorData.format(SinedDateUtils.currentDate());
						}else {
							continue;
						}
						
						List<ArquivoConfiguravelRetornoDocumentoBean> listaRetornoBean = mapRetornoAgrupado.get(identificador);
						if (listaRetornoBean==null){
							listaRetornoBean = new ArrayList<ArquivoConfiguravelRetornoDocumentoBean>();
						}
						listaRetornoBean.add(retornoBean);
						mapRetornoAgrupado.put(identificador, listaRetornoBean);
					}else if(retornoBean.getAgrupado() != null && retornoBean.getAgrupado()){
						String identificador = retornoBean.getNossonumero() + "|" + retornoBean.getUsoempresa();
						if(mapRetornoAgrupado.containsKey(identificador)){
							List<ArquivoConfiguravelRetornoDocumentoBean> listaRetornoBean = mapRetornoAgrupado.get(identificador);
							listaRetornoBean.add(retornoBean);
							mapRetornoAgrupado.put(identificador, listaRetornoBean);
						} else {
							List<ArquivoConfiguravelRetornoDocumentoBean> listaRetornoBean = new ArrayList<ArquivoConfiguravelRetornoDocumentoBean>();
							listaRetornoBean.add(retornoBean);
							mapRetornoAgrupado.put(identificador, listaRetornoBean);
						}
					} else {
						Movimentacao movimentacao = this.doBaixar(bean, retornoBean, listaDocumentoValecompra, mapTarifas, arquivobancario);
						if (movimentacao != null){
							listaMovimentacao.add(movimentacao);
							if (idsBaixado.length() > 0)
								idsBaixado.append(",");
							idsBaixado.append(retornoBean.getDocumento().getCddocumento());
							listaDocumentoForComissionamento.add(new Documento(retornoBean.getDocumento().getCddocumento()));
						}		
					}
					
					taxaService.processaBancoConfiguracaoRetorno(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.CANCELAR.equals(retornoBean.getAcao())){
					this.doCancelar(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.PROTESTAR.equals(retornoBean.getAcao())){
					this.doProtestar(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.CANCELAR_PROTESTO.equals(retornoBean.getAcao())){
					this.doCancelarProtesto(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.ESTORNAR_PREVISTA.equals(retornoBean.getAcao())){
					this.doEstornar(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.CONFIRMACAO.equals(retornoBean.getAcao())){
					this.doConfirmar(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.CONFIRMACAO_REGISTRO.equals(retornoBean.getAcao())){
					this.doConfirmarRegistro(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.CONFIRMACAO_AGENDAMENTO.equals(retornoBean.getAcao())){
					this.doConfirmarAgendamento(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.LANCAMENTO_HISTORICO.equals(retornoBean.getAcao())){
					this.doLancarHistorico(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.REJEITADO.equals(retornoBean.getAcao())){
					this.doRejeitar(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO.equals(retornoBean.getAcao())){
					this.doAtualizarVencimento(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO_DATALIMITE_JUROS_MULTA.equals(retornoBean.getAcao())){
					this.doAtualizarVencimentoDataLimiteJurosMulta(bean, retornoBean);
				} else if (BancoConfiguracaoRetornoAcaoEnum.BLOQUEAR_EMISSAO_BOLETO.equals(retornoBean.getAcao())){
					this.doBloquearEmissaoBoleto(bean, retornoBean);
				}else {
					retornoBean.setMensagem("A��o n�o identificada.");
				}				
			}
			
			Set<Entry<String, List<ArquivoConfiguravelRetornoDocumentoBean>>> entrySetRetornoAgrupado = mapRetornoAgrupado.entrySet();
			for (Entry<String, List<ArquivoConfiguravelRetornoDocumentoBean>> entry : entrySetRetornoAgrupado) {
				List<ArquivoConfiguravelRetornoDocumentoBean> listaRetornoBean = entry.getValue();
				ArquivoConfiguravelRetornoDocumentoBean retornoBean = new ArquivoConfiguravelRetornoDocumentoBean(listaRetornoBean.get(0));
				
				Money valortitulo = new Money();
				Money valordesconto = new Money();
				Money valoroutrodesconto = new Money();
				Money valoracrecimo = new Money();
				Money valorjuros = new Money();
				Money valormulta = new Money();
				Money valorpago = new Money();
				Money valortarifa = new Money();
				
				for (ArquivoConfiguravelRetornoDocumentoBean arquivoConfiguravelRetornoDocumentoBean : listaRetornoBean) {
					Documento documento = arquivoConfiguravelRetornoDocumentoBean.getDocumento();
					
					//Se o processamento tiver configurado para AGRUPAR movimenta��es e n�o tiver marcado para incluir juros/multas/desconto,
					//ocorre um problema na cria��o do vale compra.
					//Pois, por exemplo, se a conta tiver uma MULTA/JUROS em um pagamento de boleto, o valor da movimenta��o � criado com esta MULTA/JUROS,
					//s� que a conta n�o � adicionada com esta TAXA (pois o check INCLUIR JUROS, MULTA E DESCONTO est� desmarcado), assim o sistema "entende" que o usu�rio pagou a mais.
//					if (BancoConfiguracaoRetornoCriarMovimentacaoEnum.AGRUPADA.equals(bean.getBancoConfiguracaoRetorno().getCriarMovimentacao())
//							&& !Boolean.TRUE.equals(bean.getBancoConfiguracaoRetorno().getIncluirJurosMultaDescontoTaxasDocumento())){
//						documento.setValorJurosMultaValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValorjurosmulta());
//						documento.setValorDescontoOutroDescontoValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValorDescontoOutroDesconto());
//					}
					
					retornoBean.getListaDocumento().add(documento);
					
					documento.setValorTituloValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValor());
					documento.setValorPagoValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValorpago());
					documento.setValorJurosMultaValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValorjurosmulta());
					documento.setValorDescontoOutroDescontoValeCompra(arquivoConfiguravelRetornoDocumentoBean.getValorDescontoOutroDesconto());
					
					valortitulo = valortitulo.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValor()));
					valordesconto = valordesconto.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValordesconto()));
					valoroutrodesconto = valoroutrodesconto.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValoroutrodesconto()));
					valoracrecimo = valoracrecimo.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValoracrecimo()));
					valorjuros = valorjuros.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValorjuros()));
					valormulta = valormulta.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValormulta()));
					valorpago = valorpago.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValorpago()));
					valortarifa = valortarifa.add(SinedUtil.zeroIfNull(arquivoConfiguravelRetornoDocumentoBean.getValortarifa()));
				}
				
				retornoBean.setValor(valortitulo);
				retornoBean.setValordesconto(valordesconto);
				retornoBean.setValoroutrodesconto(valoroutrodesconto);
				retornoBean.setValoracrecimo(valoracrecimo);
				retornoBean.setValorjuros(valorjuros);
				retornoBean.setValormulta(valormulta);
				retornoBean.setValorpago(valorpago);
				retornoBean.setValortarifa(valortarifa);
				
				if (BancoConfiguracaoRetornoCriarMovimentacaoEnum.AGRUPADA.equals(bean.getBancoConfiguracaoRetorno().getCriarMovimentacao())){
					retornoBean.setDtpagamentoAgrupado(SinedDateUtils.stringToDate(entry.getKey(), "ddMMyyyy"));
				}
				
				Movimentacao movimentacao = this.doBaixar(bean, retornoBean, listaDocumentoValecompra, mapTarifas, arquivobancario);
				if (movimentacao != null){
					listaMovimentacao.add(movimentacao);
					if (idsBaixado.length() > 0)
						idsBaixado.append(",");
					idsBaixado.append(CollectionsUtil.listAndConcatenate(retornoBean.getListaDocumento(), "cddocumento", ","));
				}
				
				for (ArquivoConfiguravelRetornoDocumentoBean arquivoConfiguravelRetornoDocumentoBean : listaRetornoBean) {
					arquivoConfiguravelRetornoDocumentoBean.setMensagem(retornoBean.getMensagem());
				}
			}
			
			//Preenche no bean principal os ids das contas que foram baixadas
			bean.setIdsBaixado(idsBaixado.toString());
			
			//Atualiza os rateios dos documentos
			if (Boolean.TRUE.equals(bean.getBancoConfiguracaoRetorno().getAtualizarjurosmulta())){
				for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()) {
					if(retornoBean.getDocumento()!=null 
							&& retornoBean.getDocumento().getCddocumento()!=null 
							&& Boolean.TRUE.equals(retornoBean.getMarcado())
							&& !retornoBean.isAddJurosMulta()){
						if (retornoBean.getValorjuros()!=null && retornoBean.getValorjuros().toLong()>0){
							boolean possuiJurosMulta = false;
							Taxa taxa = taxaService.findByDocumento(retornoBean.getDocumento());
							
							if (taxa!=null && taxa.getListaTaxaitem()!=null){
								for (Taxaitem taxaitem: taxa.getListaTaxaitem()){
									if (Tipotaxa.JUROS.equals(taxaitem.getTipotaxa()) || Tipotaxa.MULTA.equals(taxaitem.getTipotaxa())){
										possuiJurosMulta = true;
										break;
									}
								}
								
								if (possuiJurosMulta){
									taxaitemService.deleteFromArquivoretornoAtualizarJurosMulta(taxa);
									
									Taxaitem taxaitem = new Taxaitem();
									taxaitem.setTaxa(taxa);
									taxaitem.setTipotaxa(Tipotaxa.MULTA);
									taxaitem.setPercentual(false);
									taxaitem.setValor(retornoBean.getValorjuros());
									taxaitem.setDtlimite(retornoBean.getDtvencimento()!=null ? new Date(retornoBean.getDtvencimento().getTime()) : retornoBean.getDocumento().getDtvencimento());
									taxaitem.setMotivo("Atualizado conforme arquivo de retorno");
									taxaitemService.saveOrUpdate(taxaitem);
								}
							}							
						}
					}
				}
			}
			
			//Atualiza os documentos processados
			for (ArquivoConfiguravelRetornoDocumentoBean retornoBean : bean.getListaDocumento()) {
				if(retornoBean.getDocumento() != null && retornoBean.getDocumento().getCddocumento() != null && 
						retornoBean.getMarcado() != null && retornoBean.getMarcado()){
					documentoService.callProcedureAtualizaDocumento(retornoBean.getDocumento());
				}
			}
			
			//Atualiza as movimenta��es processadas			
			for (Movimentacao movimentacao2 : listaMovimentacao) {
				if(movimentacao2 != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao2);
					movimentacaoService.updateHistoricoWithDescricao(movimentacao2);
				}
			}
			
			//Cria movimenta��o das tarifas
			try {
				if (!mapTarifas.isEmpty() && listaMovimentacao != null && !listaMovimentacao.isEmpty()){
					//Pega a primeira movimenta��o para servir de base para cria��o da movimenta��o de tarifas
					Movimentacao movimentacao = listaMovimentacao.get(0);
					Money valorTarifa;
					for (String motivoTarifa : mapTarifas.keySet()){
						valorTarifa = mapTarifas.get(motivoTarifa);
						movimentacao.setValorTarifa(valorTarifa);
						movimentacao.setMotivoTarifa(motivoTarifa);
						movimentacaoService.gerarMovimentacaoTarifa(movimentacao);
					}
				}
			} catch (Exception e) {
				bean.addErro("Erro ao criar movimenta��o de tarifas: " + e.getMessage());
			}	
			
			if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.GERAR_VALE_COMPRA_ARQUIVO_RETORNO))){
				valecompraService.createValecompraArquivoRetorno(bean, listaDocumentoValecompra, listaMovimentacao);
			}
			
			try {
				if(SinedUtil.isListNotEmpty(listaDocumentoForComissionamento)){
					contratoService.recalcularComissaoContaBaixada(listaDocumentoForComissionamento);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void doConfirmarAgendamento(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		try {
			Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
			documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
			documentohistorico.setObservacao("Confirma��o de agendamento via arquivo de retorno");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
			documentoconfirmacao.setDocumento(retornoBean.getDocumento());
			documentoconfirmacao.setData(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));
			documentoconfirmacao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			documentoconfirmacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
			documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
			
			retornoBean.addMensagem("Confirma��o de agendamento efetuada");
		} catch (Exception e) {
			retornoBean.addMensagem("Confirma��o de agendamento n�o efetuada: " + e.getMessage());
		}
	}
	
	private void doConfirmarRegistro(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		try {
			Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
			documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
			documentohistorico.setObservacao("Confirma��o de registro via arquivo de retorno");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			Documentoconfirmacao documentoconfirmacao = new Documentoconfirmacao();
			documentoconfirmacao.setDocumento(retornoBean.getDocumento());
			documentoconfirmacao.setData(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));
			documentoconfirmacao.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			documentoconfirmacao.setDtaltera(new Timestamp(System.currentTimeMillis()));
			documentoconfirmacaoService.saveOrUpdate(documentoconfirmacao);
			
			retornoBean.addMensagem("Confirma��o efetuada");
		} catch (Exception e) {
			retornoBean.addMensagem("Confirma��o n�o efetuada: " + e.getMessage());
		}		
	}
	
	private void doAtualizarVencimento(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO, retornoBean.getCodocorrencia());
		documentohistorico.setObservacao((ocorrencia != null && ocorrencia.getHistorico() != null && !"".equals(ocorrencia.getHistorico().trim()) ? ocorrencia.getHistorico() : retornoBean.getMensagem() != null ? retornoBean.getMensagem() : "Data de vencimento atualizada via arquivo de retorno."));
		documentohistoricoService.saveOrUpdate(documentohistorico);
		
		Documento documento = retornoBean.getDocumento();
		
		if(documento != null && documento.getCddocumento() != null){
			List<Documento> listadocumento = documentoService.findDtVencimentoByWhereIn(documento.getCddocumento().toString());
			if(SinedUtil.isListNotEmpty(listadocumento)){
				for (Documento doc : listadocumento) {
					if(doc != null && doc.getCddocumento() != null && doc.getDtvencimento() != null && 
							!SinedDateUtils.equalsIgnoreHour(doc.getDtvencimento(), SinedDateUtils.castUtilDateForSqlDate(retornoBean.getDtvencimento()))){
						doc.setDtvencimentoantiga(doc.getDtvencimento());
						documentoService.updateDtVencimentoAntiga(doc);
					}
				}
			}
		}
		documento.setDtvencimento(SinedDateUtils.castUtilDateForSqlDate(retornoBean.getDtvencimento()));
		documentoService.updateVencimento(documento);
		
		retornoBean.addMensagem("Data de vencimento atualizada.");
	}
	
	/**
	* M�todo que atualiza a data de vencimento do documento e a data limte da taxa de juros e multa
	*
	* @param bean
	* @param retornoBean
	* @since 07/12/2015
	* @author Luiz Fernando
	*/
	private void doAtualizarVencimentoDataLimiteJurosMulta(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.ATUALIZAR_VENCIMENTO_DATALIMITE_JUROS_MULTA, retornoBean.getCodocorrencia());
		documentohistorico.setObservacao((ocorrencia != null && ocorrencia.getHistorico() != null && !"".equals(ocorrencia.getHistorico().trim()) ? ocorrencia.getHistorico() : retornoBean.getMensagem() != null ? retornoBean.getMensagem() : "Data de vencimento e data limte de Juros/Multa atualizada via arquivo de retorno."));
		documentohistoricoService.saveOrUpdate(documentohistorico);
		
		Documento documento = retornoBean.getDocumento();
		Date dtvencimento = SinedDateUtils.castUtilDateForSqlDate(retornoBean.getDtvencimento());
		documento.setDtvencimento(dtvencimento);
		documentoService.updateVencimento(documento);
		
		Taxa taxa = taxaService.findForArquivoretornoByDocumento(documento);
		if(taxa != null && SinedUtil.isListNotEmpty(taxa.getListaTaxaitem())){
			Taxaitem taxaitemJuros = null;
			Taxaitem taxaitemMulta = null;
			boolean taxaitemJurosAtualizada = false;
			boolean taxaitemMultaAtualizada = false;
			
			for(Taxaitem txitem : taxa.getListaTaxaitem()){
				if(Tipotaxa.JUROS.equals(txitem.getTipotaxa())){
					taxaitemJuros = txitem;
				}else if(Tipotaxa.MULTA.equals(txitem.getTipotaxa())){
					taxaitemMulta = txitem;
				}
			}
			if(taxaitemJuros != null || taxaitemMulta != null){
				Conta conta = contaService.loadWithTaxa(documento.getConta());
				if(conta != null && conta.getTaxa() != null && SinedUtil.isListNotEmpty(conta.getTaxa().getListaTaxaitem())){
					for(Taxaitem txitem : conta.getTaxa().getListaTaxaitem()){
						if(Tipotaxa.JUROS.equals(txitem.getTipotaxa()) || Tipotaxa.MULTA.equals(txitem.getTipotaxa())){
							Date dtlimite = SinedDateUtils.addDiasData(dtvencimento, txitem.getDias() != null ? txitem.getDias() : 0);
							if(Tipotaxa.JUROS.equals(txitem.getTipotaxa()) && !taxaitemJurosAtualizada){
								taxaitemJuros.setDtlimite(dtlimite);
								taxaitemService.updateDtlimite(taxaitemJuros, taxaitemJuros.getDtlimite());
								taxaitemJurosAtualizada = true;
							}else if(Tipotaxa.MULTA.equals(txitem.getTipotaxa()) && !taxaitemMultaAtualizada){
								taxaitemMulta.setDtlimite(dtlimite);
								taxaitemService.updateDtlimite(taxaitemMulta, taxaitemMulta.getDtlimite());
								taxaitemMultaAtualizada = true;
							} 
						}
					}
				}
				if(taxaitemJuros != null && !taxaitemJurosAtualizada){
					taxaitemJuros.setDtlimite(dtvencimento);
					taxaitemService.updateDtlimite(taxaitemJuros, taxaitemJuros.getDtlimite());
					taxaitemJurosAtualizada = true;
				}
				if(taxaitemMulta != null && !taxaitemMultaAtualizada){
					taxaitemMulta.setDtlimite(dtvencimento);
					taxaitemService.updateDtlimite(taxaitemMulta, taxaitemMulta.getDtlimite());
					taxaitemMultaAtualizada = true;
				}
			}
		}
		retornoBean.addMensagem("Data de vencimento e data limte de juros/multa atualizada.");
	}
	
	private void doRejeitar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
		documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
		documentohistorico.setObservacao(retornoBean.getMotivorejeicao());
		documentohistoricoService.saveOrUpdate(documentohistorico);
		
		Documento documento = retornoBean.getDocumento();
		documento.setRejeitadobanco(Boolean.TRUE);
		documentoService.updateRejeitado(documento);
		
		retornoBean.addMensagem(retornoBean.getMotivorejeicao());
	}
	
	private void doLancarHistorico(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
		documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.LANCAMENTO_HISTORICO, retornoBean.getCodocorrencia());
		documentohistorico.setObservacao((ocorrencia != null && ocorrencia.getHistorico() != null && !"".equals(ocorrencia.getHistorico().trim()) ? ocorrencia.getHistorico() : retornoBean.getMensagem() != null ? retornoBean.getMensagem() : "Lan�amento de hist�rico (Arquivo retorno)"));
		documentohistoricoService.saveOrUpdate(documentohistorico);
		retornoBean.addMensagem("Lan�amento de hist�rico efetuado");
	}
	
	private void doConfirmar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
		documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.CONFIRMACAO, retornoBean.getCodocorrencia());
		documentohistorico.setObservacao((ocorrencia != null && ocorrencia.getHistorico() != null ? ocorrencia.getHistorico() : "Confirma��o de altera��o de dados via arquivo de retorno"));
		documentohistoricoService.saveOrUpdate(documentohistorico);
		retornoBean.addMensagem("Confirma��o efetuada");
	}
	
	private void doEstornar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documento documento = retornoBean.getDocumento();
		
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setIds(documento.getCddocumento().toString());
		documentohistorico.setDocumentoacao(Documentoacao.ESTORNADA);
		documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.ESTORNAR_PREVISTA, retornoBean.getCodocorrencia());
		documentohistorico.setObservacao((ocorrencia != null ? ocorrencia.getHistorico() : null));
		
		if (!Documentoacao.BAIXADA.equals(retornoBean.getDocumento().getDocumentoacao())){			
			List<String> listaErros = null;			
			try {
				listaErros = documentoService.doEstornar(documentohistorico);				
				if (listaErros == null || listaErros.isEmpty()) {
					retornoBean.addMensagem("Documento estornado com sucesso.");
				} else {
					for (String string : listaErros) {
						retornoBean.addMensagem(string);
					}
				}				
			} catch (Exception e) {
				retornoBean.addMensagem("Documento n�o estornado: " + e.getMessage());
			}			
		} else {
			retornoBean.addMensagem("Documento n�o estornado, est� com situa��o 'Baixada'.");
		}
		
	}
	
	private void doCancelarProtesto(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documento documento = retornoBean.getDocumento();
		
		if (Documentoacao.PROTESTADA.equals(documento.getDocumentoacao())){
			try{
				
				Documentohistorico documentohistorico = new Documentohistorico();
				documentohistorico.setIds(retornoBean.getDocumento().getCddocumento().toString());
				documentohistorico.setDocumentoacao(Documentoacao.RETIRADA_PROTESTO);
				documentohistorico.setDtreferencia(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));
				
				BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.CANCELAR_PROTESTO, retornoBean.getCodocorrencia());
				documentohistorico.setObservacao((ocorrencia != null ? ocorrencia.getHistorico() : null));
				
				String nomes = documentoService.retirarProtesto(documentohistorico);
				if (!"".equals(nomes)) {
					nomes = nomes.substring(0, nomes.length()-2);
					retornoBean.addMensagem("Protesto n�o retirado.");
				} else {
					retornoBean.addMensagem("Protesto retirado com sucesso.");
				}

			} catch (Exception e) {
				retornoBean.addMensagem("Protesto n�o retirado: " + e.getMessage());
			}
			
		} else {
			retornoBean.addMensagem("Situa��o diferente de 'Protestada'");
		}
		
	}
	
	private void doProtestar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		Documento documento = retornoBean.getDocumento();
		
		if (Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao())){					
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setIds(retornoBean.getDocumento().getCddocumento().toString());
//			documentohistorico.setDocumentoacao(Documentoacao.PROTESTADA);
			documentohistorico.setDtcartorio(retornoBean.getDtocorrencia() != null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));
			
			BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.PROTESTAR, retornoBean.getCodocorrencia());
			documentohistorico.setObservacao((ocorrencia != null ? ocorrencia.getHistorico() : null));
			
			BancoConfiguracaoRetornoOcorrencia ocorrenciaSituacaoProtesto = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.PROTESTAR, retornoBean.getCodocorrencia());
			if (ocorrenciaSituacaoProtesto!=null){
				BancoConfiguracaoRetornoSituacaoProtestoEnum situacaoProtesto = ocorrenciaSituacaoProtesto.getSituacaoProtesto();
				if (BancoConfiguracaoRetornoSituacaoProtestoEnum.ENVIADO_CARTORIO.equals(situacaoProtesto)){
					documentohistorico.setDocumentoacao(Documentoacao.ENVIADO_CARTORIO);
				}else if (BancoConfiguracaoRetornoSituacaoProtestoEnum.ENVIADO_JURIDICO.equals(situacaoProtesto)){
					documentohistorico.setDocumentoacao(Documentoacao.ENVIADO_JURIDICO);
				}else if (situacaoProtesto==null || BancoConfiguracaoRetornoSituacaoProtestoEnum.PROTESTADA.equals(situacaoProtesto)){
					documentohistorico.setDocumentoacao(Documentoacao.PROTESTADA);
				}
			}
			
			try{			
				String nomes = documentoService.doEmCartorio(documentohistorico);
				if (!"".equals(nomes)) {
					nomes = nomes.substring(0, nomes.length()-2);
					retornoBean.addMensagem("Documento j� protestado.");
				} else {
					retornoBean.addMensagem("Documento protestado com sucesso.");
				}

			} catch (Exception e) {
				retornoBean.addMensagem("Documento n�o protestado: " + e.getMessage());
			}
			
		} else {
			retornoBean.addMensagem("Documento n�o protestado, situa��o diferente de 'Prevista' ou 'Definitiva'");
		}
		
	}
	
	private void doCancelar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		String obs;
		BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.CANCELAR, retornoBean.getCodocorrencia());
		
		if (ocorrencia != null && ocorrencia.getHistorico() != null)
			obs = ocorrencia.getHistorico();
		else
			obs = "Cancelamento via arquivo de retorno, opera��o: " + (ocorrencia != null ? ocorrencia.getDescricao() : "Desconhecida");
			
		try {
			documentoService.cancelarSemTransacao(retornoBean.getDocumento().getCddocumento().toString(), obs);
			retornoBean.addMensagem("Cancelamento efetuado");
		} catch (Exception e) {
			retornoBean.addMensagem("Cancelamento n�o efetuado: " + e.getMessage());
		}	
	}
	
	private Movimentacao doBaixar(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean, List<Documento> listaDocumentoValecompra, HashMap<String, Money> mapTarifas, Arquivobancario arquivobancario){
		Movimentacao movimentacao = null;
		
		try {
			Rateio rateio = null;
			List<Documento> listaDocumento = new ArrayList<Documento>();
			Documento documento = retornoBean.getDocumento();
			boolean agrupada = BancoConfiguracaoRetornoCriarMovimentacaoEnum.AGRUPADA.equals(bean.getBancoConfiguracaoRetorno().getCriarMovimentacao());
			
			if(retornoBean.getAgrupado() != null && retornoBean.getAgrupado() || agrupada){
				List<Documento> listaDocumentoVinculo = retornoBean.getListaDocumento();
				for (Documento documentoVinculo : listaDocumentoVinculo) {
					if (Documentoacao.PREVISTA.equals(documentoVinculo.getDocumentoacao()) ||
							Documentoacao.DEFINITIVA.equals(documentoVinculo.getDocumentoacao()) ||
									Documentoacao.AUTORIZADA.equals(documentoVinculo.getDocumentoacao())){
						if (BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO.equals(retornoBean.getAcao())){
							BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO, retornoBean.getCodocorrencia());
							if (ocorrencia != null && ocorrencia.getHistorico() != null)
								documentoVinculo.setHistoricoBaixaRetorno(ocorrencia.getHistorico());
						}
						
						documentoVinculo.setRateio(rateioService.findByDocumento(documentoVinculo));
						
						listaDocumento.add(documentoVinculo);
						listaDocumentoValecompra.add(documentoVinculo);
					}
				}
				
				if(listaDocumento.size() == 0){
					retornoBean.addMensagem("Baixa n�o realizada. Conta com situa��o diferente de 'Prevista', 'Definitiva' ou 'Autorizada'.");
					return null;
				}
				
				rateio = contapagarService.agruparItensRateio(listaDocumento, true, null);
				rateioService.atualizaValorRateio(rateio, retornoBean.getValor());
				documento = listaDocumento.get(listaDocumento.size()-1);
			} else {
				if (Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) ||
						Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao()) ||
								Documentoacao.AUTORIZADA.equals(documento.getDocumentoacao())){
				
					if(documento.getValor().getValue().doubleValue() == 0d){
						rateio = contareceberService.atualizaValorRateio(documento, retornoBean.getValor());
						documento = contareceberService.loadForEntradaWithDadosPessoa(retornoBean.getDocumento());
					} else {
						rateio = rateioService.findByDocumento(documento);
						rateioService.atualizaValorRateio(rateio, retornoBean.getValor());
					}							
		
					if (BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO.equals(retornoBean.getAcao())){
						BancoConfiguracaoRetornoOcorrencia ocorrencia = bean.getBancoConfiguracaoRetorno().getOcorrencia(BancoConfiguracaoRetornoAcaoEnum.BAIXA_HISTORICO, retornoBean.getCodocorrencia());
						if (ocorrencia != null && ocorrencia.getHistorico() != null)
							documento.setHistoricoBaixaRetorno(ocorrencia.getHistorico());
					}							
					
					documento.setValoratual(retornoBean.getValor());
					
					listaDocumento.add(documento);
					listaDocumentoValecompra.add(documento);
				}
				
				if(listaDocumento.size() == 0){
					retornoBean.addMensagem("Baixa n�o realizada. Conta com situa��o diferente de 'Prevista', 'Definitiva' ou 'Autorizada'.");
					return null;
				}
			}
			
			Money vlrPago = retornoBean.getValorpago() != null ? retornoBean.getValorpago() : retornoBean.getValor();
			
			rateioService.atualizaRateioWithDescontoAndMulta(rateio, retornoBean.getValordesconto(), retornoBean.getValoroutrodesconto(), retornoBean.getValorjurosmulta(), retornoBean.getValor(), vlrPago, BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo()) ? Tipooperacao.TIPO_DEBITO : Tipooperacao.TIPO_CREDITO);	
			
			BaixarContaBean baixarContaBean = new BaixarContaBean();
			baixarContaBean.setListaDocumento(listaDocumento);
			baixarContaBean.setRateio(rateio);
			baixarContaBean.setDtpagamento(!agrupada ? (new Date(retornoBean.getDtpagamento() != null ? retornoBean.getDtpagamento().getTime() : retornoBean.getDtcredito().getTime())) : new Date(retornoBean.getDtpagamentoAgrupado().getTime()));
			baixarContaBean.setDtcredito(!agrupada ? (new Date(retornoBean.getDtcredito() != null ? retornoBean.getDtcredito().getTime() : (retornoBean.getDtpagamento() != null ? retornoBean.getDtpagamento().getTime() : null))) : SinedDateUtils.currentDate());
			baixarContaBean.setFormapagamento(BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo()) ? Formapagamento.DEBITOCONTACORRENTE : Formapagamento.CREDITOCONTACORRENTE);
			baixarContaBean.setContatipo(Contatipo.TIPO_CONTA_BANCARIA);
			baixarContaBean.setVinculo(bean.getConta());
			baixarContaBean.setDocumentoclasse(BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(bean.getTipo()) ? Documentoclasse.OBJ_PAGAR : Documentoclasse.OBJ_RECEBER);
			baixarContaBean.setChecknum(!agrupada ? retornoBean.getNossonumero() : null);
			baixarContaBean.setFromRetorno(true);
			baixarContaBean.setValorTotal(retornoBean.getValorpago() != null ? retornoBean.getValorpago().getValue().doubleValue() : retornoBean.getValor().getValue().doubleValue());
			baixarContaBean.setMensagem(retornoBean.getMensagem());
			
			movimentacao = documentoService.doBaixarConta(null, baixarContaBean);						
							
			if (BancoConfiguracaoRemessaTipoEnum.COBRANCA.equals(bean.getTipo())){	
				notaDocumentoService.liberaNotaDocumentoWebservice(NeoWeb.getRequestContext(), baixarContaBean.getListaDocumento(), baixarContaBean.getDtpagamento());
//				contareceberService.incrementaNumDocumento(documento);
				documentoService.criaDescontoAutomaticoContratoPagamento(baixarContaBean);	
			}
			
			Money valorTarifa;
			if(retornoBean.getValortarifa() != null && retornoBean.getValortarifa().toLong() > 0){							
				if(retornoBean.getMotivotarifa() != null && !"".equals(retornoBean.getMotivotarifa())){								
					if (mapTarifas.containsKey(retornoBean.getMotivotarifa())){									
						valorTarifa = mapTarifas.get(retornoBean.getMotivotarifa());
						valorTarifa = valorTarifa.add(retornoBean.getValortarifa());
						mapTarifas.put(retornoBean.getMotivotarifa(), valorTarifa);
					} else
						mapTarifas.put(retornoBean.getMotivotarifa(), retornoBean.getValortarifa());								
				} else {
					if (mapTarifas.containsKey("99")){									
						valorTarifa = mapTarifas.get("99");
						valorTarifa = valorTarifa.add(retornoBean.getValortarifa());
						mapTarifas.put("99", valorTarifa);
					} else
						mapTarifas.put("99", retornoBean.getValortarifa());								
				}
			}
			
			for (Documento documento2: listaDocumento){
				arquivobancariodocumentoService.saveProcessamentoArquivoRetorno(arquivobancario, documento2);
			}
			
			if (movimentacao == null) {
				retornoBean.addMensagem("Baixa n�o efetuada");
			} else {
				retornoBean.addMensagem("Baixa efetuada");
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			retornoBean.addMensagem("Baixa n�o efetuada: " + e.getMessage());
		}
			
		return movimentacao;
	}

	private void doBloquearEmissaoBoleto(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		try {
			Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(retornoBean.getDocumento());
			documentohistorico.setDtreferencia(retornoBean.getDtocorrencia()!=null ? new Date(retornoBean.getDtocorrencia().getTime()) : new Date(System.currentTimeMillis()));		
			documentohistorico.setObservacao("Bloqueio de emiss�o de boleto via arquivo de retorno");
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			documentoService.updateEmissaoboletobloqueado(retornoBean.getDocumento());
			
			retornoBean.addMensagem("Bloqueio de emiss�o de boleto efetuado");
		} catch (Exception e) {
			retornoBean.addMensagem("Bloqueio de emiss�o de boleto n�o efetuado: " + e.getMessage());
		}		
	}
	
	/**
	 * M�todo respons�vel por processar exporta��o do arquivo
	 * @param request
	 * @param filtro
	 * @throws IOException
	 * @author C�sar
	 * @since 07/06/2016
	 */
	public void processaExportar(WebRequestContext request, BancoConfiguracaoRetornoFiltro filtro, ByteArrayOutputStream conteudoZip) throws IOException{
		List<BancoConfiguracaoRetorno> listaBancoConfiguracaoRetorno = new ArrayList<BancoConfiguracaoRetorno>();		
		ZipOutputStream zip = new ZipOutputStream(conteudoZip);
		zip.setLevel(Deflater.BEST_COMPRESSION);
		
		//Recebendo os ids
		String whereIn = filtro.getSelecteditens();
		//Carregando os campos necess�rios para exportar
		listaBancoConfiguracaoRetorno =  this.findforWhereIn(whereIn);
		//Preenchendo arquivo para exportar
		if(listaBancoConfiguracaoRetorno != null && !listaBancoConfiguracaoRetorno.isEmpty()){				
			for (BancoConfiguracaoRetorno retorno : listaBancoConfiguracaoRetorno) {
				
				CSVWriter csv = new CSVWriter();
				csv.setDelimiter("@");
							
				//preenchendo dados do segmento
				csv.add(retorno.getNome());
				csv.add(retorno.getBanco().getCdbanco());
				csv.add(retorno.getTamanho());
				csv.add(retorno.getTipo().name());
				csv.newLine();
				//Marca��o para importa��o
				csv.add("###");
				csv.newLine();
				if(retorno.getListaBancoConfiguracaoRetornoCampo() != null && SinedUtil.isListNotEmpty(retorno.getListaBancoConfiguracaoRetornoCampo())){
					for (BancoConfiguracaoRetornoCampo campo : retorno.getListaBancoConfiguracaoRetornoCampo()){
						if(campo.getBancoConfiguracaoRetornoSegmento() != null){
							csv.add(campo.getBancoConfiguracaoRetornoSegmento().getNome());
							csv.add(campo.getBancoConfiguracaoRetornoSegmento().getTipo().name());
							csv.add(campo.getBancoConfiguracaoRetornoSegmento().getBanco().getCdbanco());
							csv.add(campo.getBancoConfiguracaoRetornoSegmento().getIdentificador());
							csv.newLine();
						}
					}
					//Marca��o para importa��o
					csv.add("###");
					csv.newLine();
					for (BancoConfiguracaoRetornoCampo campo : retorno.getListaBancoConfiguracaoRetornoCampo()){
						csv.add(campo.getCampo().name());
						csv.add(campo.getBancoConfiguracaoRetornoSegmento() != null &&
								campo.getBancoConfiguracaoRetornoSegmento().getCdbancoconfiguracaoretornosegmento() != null ? 
								campo.getBancoConfiguracaoRetornoSegmento().getNome() : "");
						csv.add(campo.getPosIni());
						csv.add(campo.getTamanho());
						csv.add(campo.getQtdeDecimal());
						csv.add(campo.getFormatoData());
						csv.add(campo.isCalculado());
						csv.newLine();
					}
					//Marca��o para importa��o
					csv.add("###");
					csv.newLine();
				}
				if(retorno.getListaBancoConfiguracaoRetornoOcorrencia() != null && SinedUtil.isListNotEmpty(retorno.getListaBancoConfiguracaoRetornoOcorrencia())){
					for(BancoConfiguracaoRetornoOcorrencia ocorrencia : retorno.getListaBancoConfiguracaoRetornoOcorrencia()){
						csv.add(ocorrencia.getCodigo());
						csv.add(ocorrencia.getDescricao());
						csv.add(ocorrencia.getAcao().name());
						csv.add(ocorrencia.getHistorico());
						csv.newLine();
					}
				}
				//Marca��o para importa��o
				csv.add("###");
				csv.newLine();
				if(retorno.getListaBancoConfiguracaoRetornoRejeicao() != null && SinedUtil.isListNotEmpty(retorno.getListaBancoConfiguracaoRetornoRejeicao())){
					for(BancoConfiguracaoRetornoRejeicao rejeicao : retorno.getListaBancoConfiguracaoRetornoRejeicao()){
						csv.add(rejeicao.getCodigo());
						csv.add(rejeicao.getDescricao());
						csv.newLine();
					}
				}
				try{
					zip.putNextEntry(new ZipEntry("planilha_banco_config_retorno_" + retorno.getNome().replace(" ", "_")  + ".csv"));
				}
				catch (Exception e) {
					throw new SinedException("N�o � permitido exportar arquivos com configura��o duplicada");
				}
				
				zip.write(csv.toString().getBytes());
				zip.closeEntry();
			}
		}
		zip.close();		
	}
	/**
	 * M�todo com refer�ncias no DAO
	 * @param ids
	 * @return
	 * @author C�sar
	 * @since 08/06/2016
	 */
	public List<BancoConfiguracaoRetorno> findforWhereIn(String whereIn){
		return  bancoConfiguracaoRetornoDAO.findforWhereIn(whereIn);
	}
	/**
	 * M�todo respons�vel pelo recebimento e salvar os arquivos para importa��o
	 * @param arquivos
	 * @return
	 * @author C�sar
	 * @throws IOException 
	 * @since 08/06/2016
	 */
	public String importacaoDados (List<Arquivo> arquivos, WebRequestContext request) throws IOException{
		String mensagemErro = "";
		if(arquivos != null){
			for (Arquivo arquivo : arquivos) {				
				if(arquivo.getArquivo() != null){			
					BancoConfiguracaoRetorno bancoConfiguracaoRetorno = this.processaArquivo(arquivo.getArquivo(), request);								
					if(bancoConfiguracaoRetorno != null)
						this.saveOrUpdate(bancoConfiguracaoRetorno);
					else
						mensagemErro += "A planilha " + arquivo.getArquivo().getNome() + " n�o foi importada por inconsist�ncias. <br>";
				}				
			}
		}
		return mensagemErro;
	}	
	/**
	 * M�todo respons�vel por processar os CSV importados
	 * @param arquivo
	 * @param bancoConfiguracaoSegmento
	 * @author C�sar
	 * @throws Exception 
	 * @since 08/06/2016
	 */
	private BancoConfiguracaoRetorno processaArquivo(Arquivo arquivo, WebRequestContext request) throws IOException {
		BufferedReader br = null;
		InputStream csv = new ByteArrayInputStream(arquivo.getContent());
		BancoConfiguracaoRetorno retorno = new BancoConfiguracaoRetorno();	
		Set<BancoConfiguracaoRetornoCampo> listacampos = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
		Set<BancoConfiguracaoRetornoOcorrencia> listaOcorrencia = new ListSet<BancoConfiguracaoRetornoOcorrencia>(BancoConfiguracaoRetornoOcorrencia.class);
		Set<BancoConfiguracaoRetornoRejeicao> listaRejeicao = new ListSet<BancoConfiguracaoRetornoRejeicao>(BancoConfiguracaoRetornoRejeicao.class);
		Map<String, BancoConfiguracaoRetornoSegmento> mapSegmentos = new HashMap<String, BancoConfiguracaoRetornoSegmento>();
		
		//Carregando segmentos que j� existem
		List<BancoConfiguracaoRetornoSegmento> listSegmentos = bancoConfiguracaoRetornoSegmentoService.findForImportacao();
		
		String linha;
		Integer cont=0;
		String[] dados;
		
		try {
			br = new BufferedReader(new InputStreamReader(csv));
			while ((linha = br.readLine()) != null) {			
				dados = linha.split("@");			
				if("###".equals(dados[0].replace("\"", ""))){
					cont ++;
				}else if(cont == 0){//Cria��o BancoConfiguracaoRetorno	
					retorno.setNome(dados[0].replace("\"", ""));
					if(!dados[1].replace("\"", "").isEmpty()){
						retorno.setBanco(new Banco(Integer.parseInt(dados[1].replace("\"", ""))));				
					}
					if(!dados[2].replace("\"", "").isEmpty()){
						retorno.setTamanho(Integer.parseInt(dados[2].replace("\"", "")));
					}
					retorno.setTipo(BancoConfiguracaoRemessaTipoEnum.valueOf(dados[3].replace("\"", "")));
				}else if(cont == 1){//Cria��o BancoConfiguracaoRetornoSegmento
					if(!mapSegmentos.containsKey(dados[0].replace("\"", ""))){
						BancoConfiguracaoRetornoSegmento segmento = new BancoConfiguracaoRetornoSegmento();
						segmento.setNome(dados[0].replace("\"", ""));
						segmento.setTipo(BancoConfiguracaoTipoSegmentoEnum.valueOf(dados[1].replace("\"", "")));
						if(!dados[2].replace("\"", "").isEmpty()){
							segmento.setBanco(new Banco(Integer.parseInt(dados[2].replace("\"", ""))));						
						}
						segmento.setIdentificador(dados[3].replace("\"", ""));
						
						if(SinedUtil.isListNotEmpty(listacampos)){
							for(BancoConfiguracaoRetornoSegmento bancoSegmento : listSegmentos){					
								if(!bancoSegmento.getNome().equals(segmento.getNome()) && 
								   !(bancoSegmento.getBanco().getCdbanco() == segmento.getBanco().getCdbanco()) &&
								   !bancoSegmento.getIdentificador().equals(segmento.getIdentificador()) &&
								   !bancoSegmento.getTipo().name().equals(segmento.getTipo().name())){
									
									bancoConfiguracaoRetornoSegmentoService.saveOrUpdate(segmento);
									listSegmentos.add(segmento);
									//Map utilizado para otimizar a importa��o
									mapSegmentos.put(segmento.getNome(), segmento);
								}else{
									listSegmentos.add(segmento);
									//Map utilizado para otimizar a importa��o
									mapSegmentos.put(segmento.getNome(), segmento);
								}
							}
						}else{
							bancoConfiguracaoRetornoSegmentoService.saveOrUpdate(segmento);
							listSegmentos.add(segmento);
							//Map utilizado para otimizar a importa��o
							mapSegmentos.put(segmento.getNome(), segmento);
						}
					}
				}else if(cont == 2){//Cria��o BancoConfiguracaoRetornoCampo
					BancoConfiguracaoRetornoCampo campo = new BancoConfiguracaoRetornoCampo();
					campo.setCampo(BancoConfiguracaoCampoEnum.valueOf(dados[0].replace("\"", "")));
					if(!dados[1].replace("\"", "").isEmpty()){
						campo.setBancoConfiguracaoRetornoSegmento(mapSegmentos.get(dados[1].replace("\"", "")));
					}
					if(!dados[2].replace("\"", "").isEmpty()){
						campo.setPosIni(Integer.parseInt(dados[2].replace("\"", "")));
					}
					if(!dados[3].replace("\"", "").isEmpty()){
						campo.setTamanho(Integer.parseInt(dados[3].replace("\"", "")));						
					}
					if(!dados[4].replace("\"", "").isEmpty()){
						campo.setQtdeDecimal(Integer.parseInt(dados[4].replace("\"", "")));
					}
					campo.setFormatoData(dados[5].replace("\"", ""));
					campo.setCalculado(Boolean.parseBoolean(dados[6].replace("\"", "")));
					listacampos.add(campo);
				}else if(cont == 3){// Cria��o BancoConfiguracaoRetornoOcorrencia
					BancoConfiguracaoRetornoOcorrencia ocorrencia = new BancoConfiguracaoRetornoOcorrencia();
					ocorrencia.setCodigo(dados[0].replace("\"", ""));
					ocorrencia.setDescricao(dados[1].replace("\"", ""));
					ocorrencia.setAcao(BancoConfiguracaoRetornoAcaoEnum.valueOf(dados[2].replace("\"", "")));
					ocorrencia.setHistorico(dados[3].replace("\"", ""));
					listaOcorrencia.add(ocorrencia);			
				}else{//Cria��o BancoConfiguracaoRetornoRejeicao
					BancoConfiguracaoRetornoRejeicao rejeicao = new BancoConfiguracaoRetornoRejeicao();
					rejeicao.setCodigo(dados[0].replace("\"", ""));
					rejeicao.setDescricao(dados[1].replace("\"", ""));
					listaRejeicao.add(rejeicao);
				}
				linha = "";
			}
		} catch (Exception e) {		
			e.printStackTrace();
			retorno = null;
		} finally {
			if(retorno != null){
				retorno.setListaBancoConfiguracaoRetornoCampo(listacampos);
				retorno.setListaBancoConfiguracaoRetornoOcorrencia(listaOcorrencia);
				retorno.setListaBancoConfiguracaoRetornoRejeicao(listaRejeicao);
			}
			if (br != null) {
				br.close();		
			}
		}
		return retorno;
	}	
	
	@Override
	public void saveOrUpdate(final BancoConfiguracaoRetorno bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				boolean isFirstSave = bean.getCdbancoconfiguracaoretorno() == null ? true : false;
				
				 Set<BancoConfiguracaoRetornoSegmentoDetalhe> listaBancoConfiguracaoRetornoSegmentoDetalhe = bean.getListaBancoConfiguracaoRetornoSegmentoDetalhe();
				 Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = bean.getListaBancoConfiguracaoRetornoCampo();
				 Set<BancoConfiguracaoRetornoOcorrencia> listaBancoConfiguracaoRetornoOcorrencia = bean.getListaBancoConfiguracaoRetornoOcorrencia();
				 Set<BancoConfiguracaoRetornoRejeicao> listaBancoConfiguracaoRetornoRejeicao = bean.getListaBancoConfiguracaoRetornoRejeicao();
				 Set<BancoConfiguracaoRetornoIdentificador> listaBancoConfiguracaoRetornoIdentificador = bean.getListaBancoConfiguracaoRetornoIdentificador();
				
				if(!isFirstSave){ 
					List<BancoConfiguracaoRetornoSegmentoDetalhe> listaBCRCAntiga = bancoConfiguracaoRetornoSegmentoDetalheService.findByBancoConfiguracaoRetorno(bean);
					if(listaBCRCAntiga !=null && !listaBCRCAntiga.isEmpty()){
						for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsdAntigo : listaBCRCAntiga){
							boolean delete = true;
							for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : listaBancoConfiguracaoRetornoSegmentoDetalhe){
								if(bcrsd.equals(bcrsdAntigo)){
									delete = false;
									break;
								}
							}
							if(delete){
								bancoConfiguracaoRetornoSegmentoDetalheService.delete(bcrsdAntigo);
							}
						}
					}
				}
				saveOrUpdateNoUseTransaction(bean);
				List<BancoConfiguracaoRetornoCampo> listaBCRC = new ArrayList<BancoConfiguracaoRetornoCampo>();
				if(listaBancoConfiguracaoRetornoSegmentoDetalhe !=null && !listaBancoConfiguracaoRetornoSegmentoDetalhe.isEmpty()){
					for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : listaBancoConfiguracaoRetornoSegmentoDetalhe){
						bcrsd.setBancoConfiguracaoRetorno(bean);
						bancoConfiguracaoRetornoSegmentoDetalheService.saveOrUpdateNoUseTransaction(bcrsd);
						if(bcrsd.getListaBancoConfiguracaoRetornoCampo()!=null && !bcrsd.getListaBancoConfiguracaoRetornoCampo().isEmpty()){
							for(BancoConfiguracaoRetornoCampo bcrc: bcrsd.getListaBancoConfiguracaoRetornoCampo()){
								bcrc.setBancoConfiguracaoRetornoSegmentoDetalhe(bcrsd);
								bancoConfiguracaoRetornoCampoService.saveOrUpdateNoUseTransaction(bcrc);
								listaBCRC.add(bcrc);
							}
						}
					}
				}
				
				if(listaBancoConfiguracaoRetornoCampo !=null && !listaBancoConfiguracaoRetornoCampo.isEmpty()){
					for(BancoConfiguracaoRetornoCampo bcrc : listaBancoConfiguracaoRetornoCampo){
						bcrc.setBancoConfiguracaoRetorno(bean);
						bancoConfiguracaoRetornoCampoService.saveOrUpdateNoUseTransaction(bcrc);
					}
				}
				
				if(listaBancoConfiguracaoRetornoOcorrencia !=null && !listaBancoConfiguracaoRetornoOcorrencia.isEmpty()){
					for(BancoConfiguracaoRetornoOcorrencia bcro : listaBancoConfiguracaoRetornoOcorrencia){
						bcro.setBancoConfiguracaoRetorno(bean);
						bancoConfiguracaoRetornoOcorrenciaService.saveOrUpdateNoUseTransaction(bcro);
					}
				}
				
				if(listaBancoConfiguracaoRetornoRejeicao !=null && !listaBancoConfiguracaoRetornoRejeicao.isEmpty()){
					for(BancoConfiguracaoRetornoRejeicao bcrr : listaBancoConfiguracaoRetornoRejeicao){
						bcrr.setBancoConfiguracaoRetorno(bean);
						bancoConfiguracaoRetornoRejeicaoService.saveOrUpdateNoUseTransaction(bcrr);
					}
				}
				
				if(listaBancoConfiguracaoRetornoIdentificador !=null && !listaBancoConfiguracaoRetornoIdentificador.isEmpty()){
					for(BancoConfiguracaoRetornoIdentificador bcri : listaBancoConfiguracaoRetornoIdentificador){
						bcri.setBancoConfiguracaoRetorno(bean);
						bancoConfiguracaoRetornoIdentificadorService.saveOrUpdateNoUseTransaction(bcri);
					}
				}
				
				return status;
				
			}
		});
	}
}
