package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecimentocontrato;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcomprafornecimentocontrato;
import br.com.linkcom.sined.geral.dao.OrdemcomprafornecimentocontratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcomprafornecimentocontratoService extends GenericService<Ordemcomprafornecimentocontrato>{

	private OrdemcomprafornecimentocontratoDAO ordemcomprafornecimentocontratoDAO;	
	
	public void setOrdemcomprafornecimentocontratoDAO(OrdemcomprafornecimentocontratoDAO ordemcomprafornecimentocontratoDAO) {
		this.ordemcomprafornecimentocontratoDAO = ordemcomprafornecimentocontratoDAO;
	}

	/**
	 * M�todo que salva a referencia do fornecimentocontrato da ordem de compra
	 *
	 * @param listaOrdemcompra
	 * @param contrato
	 * @author Luiz Fernando
	 */
	public void salvaOrdemcomprafornecimentocontrato(List<Ordemcompra> listaOrdemcompra, Fornecimentocontrato fornecimentocontrato){	
		if(fornecimentocontrato != null && listaOrdemcompra != null && !listaOrdemcompra.isEmpty()){
			for (Ordemcompra ordemcompra : listaOrdemcompra) {
					Ordemcomprafornecimentocontrato ordemcomprafornecimentocontrato = new Ordemcomprafornecimentocontrato();
					ordemcomprafornecimentocontrato.setOrdemcompra(ordemcompra);
					ordemcomprafornecimentocontrato.setFornecimentocontrato(fornecimentocontrato);
					this.saveOrUpdate(ordemcomprafornecimentocontrato);
			}
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcomprafornecimentocontratoDAO#findByFornecimentocontrato(Fornecimentocontrato fornecimentocontrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcomprafornecimentocontrato> findByFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		return ordemcomprafornecimentocontratoDAO.findByFornecimentocontrato(fornecimentocontrato);
	}

}
