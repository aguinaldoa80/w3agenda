package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contatotipo;
import br.com.linkcom.sined.geral.dao.ContatotipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.contatotipo.ContatotipoRESTModel;

public class ContatotipoService extends GenericService<Contatotipo> {

	private ContatotipoDAO contatotipoDAO;
	
	public void setContatotipoDAO(ContatotipoDAO contatotipoDAO) {
		this.contatotipoDAO = contatotipoDAO;
	}

	/* singleton */
	private static ContatotipoService instance;
	public static ContatotipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContatotipoService.class);
		}
		return instance;
	}
	
	public List<ContatotipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ContatotipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ContatotipoRESTModel> lista = new ArrayList<ContatotipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Contatotipo bean : contatotipoDAO.findForAndroid(whereIn))
				lista.add(new ContatotipoRESTModel(bean));
		}
		
		return lista;
	}
}
