package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargoatividade;
import br.com.linkcom.sined.geral.dao.CargoatividadeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargoatividadeService extends GenericService<Cargoatividade> {	
	
	private CargoatividadeDAO cargoatividadeDAO;
	
	public void setCargoatividadeDAO(CargoatividadeDAO cargoatividadeDAO) {
		this.cargoatividadeDAO = cargoatividadeDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cargo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cargoatividade> getAtividadesByCargo(Cargo cargo) {
		return cargoatividadeDAO.getAtividadesByCargo(cargo);
	}
	
}
