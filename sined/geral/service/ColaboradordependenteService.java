package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradordependente;
import br.com.linkcom.sined.geral.dao.ColaboradordependenteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradordependenteService extends GenericService<Colaboradordependente> {	
	
	private ColaboradordependenteDAO colaboradordependenteDAO;
	
	public void setColaboradordependenteDAO(ColaboradordependenteDAO colaboradordependenteDAO) {
		this.colaboradordependenteDAO = colaboradordependenteDAO;
	}

	/**
	 * M�todo para salvar cada dependente da lista cadastrada 
	 * 
	 * @param listaColaboradordependente
	 * @author Jo�o Paulo Zica
	 */
	public void saveDependente(List<Colaboradordependente> listaColaboradordependente){
		if (listaColaboradordependente!=null){
			for (Colaboradordependente colaboradordependente : listaColaboradordependente) {
				saveOrUpdate(colaboradordependente);
			}
		}
	}
	
	/* singleton */
	private static ColaboradordependenteService instance;
	public static ColaboradordependenteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordependenteService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradordependenteDAO#getQtdeDependentes(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getQtdeDependentes(Colaborador colaborador) {
		return colaboradordependenteDAO.getQtdeDependentes(colaborador);
	}
	
}
