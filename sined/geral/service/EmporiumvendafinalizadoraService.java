package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendafinalizadora;
import br.com.linkcom.sined.geral.dao.EmporiumvendafinalizadoraDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumvendafinalizadoraService extends GenericService<Emporiumvendafinalizadora>{

	private EmporiumvendafinalizadoraDAO emporiumvendafinalizadoraDAO;
	
	public void setEmporiumvendafinalizadoraDAO(
			EmporiumvendafinalizadoraDAO emporiumvendafinalizadoraDAO) {
		this.emporiumvendafinalizadoraDAO = emporiumvendafinalizadoraDAO;
	}
	
	public List<Emporiumvendafinalizadora> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return emporiumvendafinalizadoraDAO.findByEmporiumvenda(emporiumvenda);
	}
	

}
