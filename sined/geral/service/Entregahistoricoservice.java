package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Entregaacao;
import br.com.linkcom.sined.geral.dao.EntregahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class Entregahistoricoservice extends GenericService<Entregahistorico> {

	private EntregahistoricoDAO entregahistoricoDAO;

	public void setEntregahistoricoDAO(EntregahistoricoDAO entregahistoricoDAO) {
		this.entregahistoricoDAO = entregahistoricoDAO;
	}
	
	public void saveHistoricos(List<Entrega> listaentregas, Entregaacao entregaacao){
		this.entregahistoricoDAO.criaHistoricoEntregas(listaentregas, entregaacao);
	}
	
	public void criaHistoricoEntrega(Entrega entrega, Entregaacao entregaacao){
		this.entregahistoricoDAO.criaHistoricoEntrega(entrega, entregaacao);
	}
	
	public static Entregahistoricoservice instance;
	
	public static Entregahistoricoservice getInstance(){
		return instance==null? Neo.getObject(Entregahistoricoservice.class): instance;
	}
}
