package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Tipoidentidade;

public class TipoidentidadeService extends  GenericService<Tipoidentidade>{
	
	private static TipoidentidadeService instance;
	public static TipoidentidadeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipoidentidadeService.class);
		}
		return instance;
	}

}
