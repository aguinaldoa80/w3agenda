package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clientehistorico;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Contacrmsegmento;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Interacao;
import br.com.linkcom.sined.geral.bean.Interacaoitem;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Prospeccao;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.OrdenacaoControleInteracao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.geral.bean.enumeration.VisualizarControleInteracao;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InteracaoService extends GenericService<Interacao>{
	
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private InteracaoitemService interacaoitemService;
	private ProspeccaoService prospeccaoService;
	private PropostaService propostaService;
	private ClienteService clienteService;
	private ContacrmService contacrmService;
	
	public void setProspeccaoService(ProspeccaoService prospeccaoService) {
		this.prospeccaoService = prospeccaoService;
	}
	public void setInteracaoitemService(
			InteracaoitemService interacaoitemService) {
		this.interacaoitemService = interacaoitemService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setContacrmService(ContacrmService contacrmService) {
		this.contacrmService = contacrmService;
	}
	
	/**
	 * Salva o arquivo no disco e no banco.
	 * 
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	public void salvaArquivo(Interacaoitem interacaoitem) {
		if (interacaoitem.getArquivo() != null && interacaoitem.getArquivo().getNome() != null && !interacaoitem.getArquivo().getNome().equals("")) {
			if (interacaoitem.getIndexarquivo() == null || interacaoitem.getIndexarquivo().equals(new Long(-1))) {
				arquivoDAO.saveFile(interacaoitem, "arquivo");
				arquivoService.saveOrUpdate(interacaoitem.getArquivo());
			}
			
		} else {
			if (interacaoitem.getArquivo() == null || interacaoitem.getArquivo().getCdarquivo() == null) {
				interacaoitem.setArquivo(null);
			}
		}
	}
	
	/**
	 * Verifica o arquivo e faz corre��es para seja salvo ou atualizado.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadWithContents
	 * @param interacaoitem
	 * @author Rodrigo Freitas
	 */
	public void verificacaoArquivo(Interacaoitem interacaoitem) {
//		VERIFICA SE ALGUM ARQUIVO FOI CARREGADO NO ANEXO
		if (interacaoitem.getAnexo() != null && interacaoitem.getAnexo().getNome() != null && !interacaoitem.getAnexo().getNome().equals("")) {
			interacaoitem.setArquivo(interacaoitem.getAnexo());
		}
		
//		VERIFICA SE ALGUM ARQUIVO J� TINHA SALVO ANTES
//		A VARI�VEL INDEXARQUIVO RECEBE O CDARQUIVO DO ARQUIVO ANTERIOR
		if (interacaoitem.getIndexarquivo() != null && !interacaoitem.getIndexarquivo().equals(new Long(-1))) {
			Arquivo arquivo = new Arquivo();
			arquivo.setCdarquivo(interacaoitem.getIndexarquivo());
			Arquivo loadWithContents = arquivoService.loadWithContents(arquivo);
//			VERIFICA SE O ARQUIVO ANTIGO � IGUAL AO CARREGADO
			if (interacaoitem.getAnexo() != null && (interacaoitem.getAnexo().getNome().equals("") || interacaoitem.getAnexo().getNome().equals(loadWithContents.getNome()))) {
				interacaoitem.setArquivo(loadWithContents);
			} else {
				interacaoitem.setIndexarquivo(null);
			}
		}
	}
	
	/**
	 * Salva a intera��o no escopo de sess�o.
	 * 
	 * @param interacaoitem
	 * @param interacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Interacao salvaInteracaoSessao(Interacaoitem interacaoitem,
			Interacao interacao) {
		if(interacao == null){
			interacao = new Interacao();			
		}
		List<Interacaoitem> lista = interacao.getListaInteracaoitem();
		if (lista == null) {
			lista = new ArrayList<Interacaoitem>();
		}
//			SALVA O ARQUIVO PARA QUE POSSA FAZER O DOWNLOAD NA LISTAGEM
		salvaArquivo(interacaoitem);
		
		if(interacaoitem.getIndexlista() != null){
			lista.set(interacaoitem.getIndexlista(), interacaoitem);
		} else {
			lista.add(interacaoitem);
		}
		interacao.setListaInteracaoitem(lista);
		return interacao;
	}
	
	/**
	 * Salva uma intera��o que a partir da listagem de prospec��o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ProspeccaoService#updateInteracao
	 * @param interacaoitem
	 * @param prospeccao
	 * @author Rodrigo Freitas
	 */
	public void interacaoNova(Interacaoitem interacaoitem,
			Prospeccao prospeccao, Proposta proposta) {
//			CAI NESTA PARTE SE FOR NA LISTAGEM DE PROSPEC��O
			salvaArquivo(interacaoitem);
			if (interacaoitem.getInteracao() != null && interacaoitem.getInteracao().getCdinteracao() != null) {
				interacaoitemService.saveOrUpdate(interacaoitem);
			} else {
//				CRIA UMA NOVA INTERA��O
				Interacao interacao = new Interacao();
				List<Interacaoitem> lista = new ArrayList<Interacaoitem>();
				lista.add(interacaoitem);
				interacao.setListaInteracaoitem(lista);
//				SALVA A INTERA��O NOVA
				saveOrUpdate(interacao);
				if (prospeccao != null && prospeccao.getCdprospeccao() != null) {
					prospeccao.setInteracao(interacao);
//					FAZ UPDATE NA PROSPEC��O
					prospeccaoService.updateInteracao(prospeccao);
				} else {
					proposta.setInteracao(interacao);
//					FAZ UPDATE NA PROSPEC��O
					propostaService.updateInteracao(proposta);
				}

			}
	}
	
	/**
	 * Carrega ou somente remove o item da lista de interacaoitem.
	 * 
	 * @param request
	 * @param comportamento 
	 * 			true - retorna o item a ser exclu�do.
	 * 			false - retorna null.
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Interacaoitem carregaIndexLista(WebRequestContext request, boolean comportamento) {
		
		Interacaoitem interacaoitem = null;
		String parameter = request.getParameter("posicaoLista");
		Integer index = null;
		if (!comportamento) {
			parameter = parameter.substring(parameter.indexOf("indice=")+7, parameter.length()-1);
			index = Integer.parseInt(parameter)-1;
		} else {
			index = Integer.parseInt(parameter);
		}
		
		

		
		Interacao interacao = (Interacao)request.getSession().getAttribute("interacao");
		if(interacao == null){
			throw new SinedException();
		}
		List<Interacaoitem> lista = new ArrayList<Interacaoitem>();
		lista.addAll(interacao.getListaInteracaoitem());
		if (comportamento) {
			interacaoitem = lista.get(index.intValue());
			interacaoitem.setIndexlista(index);
		} else {
			lista.remove(index.intValue());
		}
		
		
		interacao.setListaInteracaoitem(lista);
		request.getSession().setAttribute("interacao", interacao);
		
		return interacaoitem;
	}
	
	public List<ControleInteracaoBean> getListaControleInteracao(WebRequestContext request,ControleInteracaoBean filtro){
		List<ControleInteracaoBean> listaControleInteracao = new ArrayList<ControleInteracaoBean>();
		if(filtro.getListaSituacaohistorico() == null){
			filtro.setListaSituacaohistorico(new ArrayList<Situacaohistorico>());
			if("true".equals(request.getParameter("venda"))){
				filtro.getListaSituacaohistorico().add(Situacaohistorico.VENDA);
			}
			if("true".equals(request.getParameter("rotina"))){
				filtro.getListaSituacaohistorico().add(Situacaohistorico.ROTINA);
			}
			if("true".equals(request.getParameter("perdida"))){
				filtro.getListaSituacaohistorico().add(Situacaohistorico.PERDIDA);
			}
			if("true".equals(request.getParameter("sem_contato"))){
				filtro.getListaSituacaohistorico().add(Situacaohistorico.SEM_CONTATO);
			}
		}
		Date data2 = null;
		Date data3 = null;
		Date data4 = null;
		if(filtro.getVisualizarpor().equals(VisualizarControleInteracao.SEMANAL)){
			data2 = SinedDateUtils.incrementDateFrequencia(filtro.getDatade(), new Frequencia(Frequencia.SEMANAL), 1);
			data3 = SinedDateUtils.incrementDateFrequencia(data2, new Frequencia(Frequencia.SEMANAL), 1);
			data4 = SinedDateUtils.incrementDateFrequencia(data3, new Frequencia(Frequencia.SEMANAL), 1);
		} else if(filtro.getVisualizarpor().equals(VisualizarControleInteracao.QUINZENAL)){
			data2 = SinedDateUtils.incrementDateFrequencia(filtro.getDatade(), new Frequencia(Frequencia.QUINZENAL), 1);
			data3 = SinedDateUtils.incrementDateFrequencia(data2, new Frequencia(Frequencia.QUINZENAL), 1);
			data4 = SinedDateUtils.incrementDateFrequencia(data3, new Frequencia(Frequencia.QUINZENAL), 1);
		} else if(filtro.getVisualizarpor().equals(VisualizarControleInteracao.MENSAL)){
			data2 = SinedDateUtils.incrementDateFrequencia(filtro.getDatade(), new Frequencia(Frequencia.MENSAL), 1);
			data3 = SinedDateUtils.incrementDateFrequencia(data2, new Frequencia(Frequencia.MENSAL), 1);
			data4 = SinedDateUtils.incrementDateFrequencia(data3, new Frequencia(Frequencia.MENSAL), 1);
		}
		
		if(!Tipoagendainteracao.CONTA.equals(filtro.getTipoagendainteracao())){
			List<Cliente> listaClientes = clienteService.findForControleInteracao(filtro);
			for(Cliente item : listaClientes){
				ControleInteracaoBean controle = new ControleInteracaoBean();
				controle.setCliente(item);
				controle.setTipoagendainteracao(Tipoagendainteracao.CLIENTE);
				controle.setTelefones(item.getTelefonesHtml().replace("<BR>", "\n"));
				controle.setContato(item);
				
				controle.setMunicipionome(item.getEnderecoCidade());
				controle.setUfsigla(item.getEnderecoSiglaEstado());
				if(StringUtils.isNotBlank(controle.getMunicipionome())){
					controle.setCidadeuf(controle.getMunicipionome() + " / " + controle.getUfsigla());
				}
				controle.setCategorias(item.getCategorias());
				
				if(filtro.getDatade() != null && filtro.getDataate() != null && item != null){
					List<Clientehistorico> listaHistorico = new ArrayList<Clientehistorico>();
					
					listaHistorico.add(clienteService.buscaMaiorDataHistoricoCliente(item.getListClientehistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(filtro.getDatade()), 
							SinedDateUtils.dateToTimestampInicioDia(data2)));
					listaHistorico.add(clienteService.buscaMaiorDataHistoricoCliente(item.getListClientehistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data2), 
							SinedDateUtils.dateToTimestampInicioDia(data3)));
					listaHistorico.add(clienteService.buscaMaiorDataHistoricoCliente(item.getListClientehistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data3), 
							SinedDateUtils.dateToTimestampInicioDia(data4)));
					listaHistorico.add(clienteService.buscaMaiorDataHistoricoCliente(item.getListClientehistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data4), 
							SinedDateUtils.dateToTimestampInicioDia(SinedDateUtils.addDiasData(filtro.getDataate(),1))));
					
					controle.setListaHistoricoCliente(listaHistorico);
				}
				
				listaControleInteracao.add(controle);
			}
		}
		if(!Tipoagendainteracao.CLIENTE.equals(filtro.getTipoagendainteracao())){
			List<Contacrm> listaContas = contacrmService.findForControleInteracao(filtro);
			for(Contacrm item : listaContas){
				ControleInteracaoBean controle = new ControleInteracaoBean();
				controle.setContacrm(item);
				controle.setTipoagendainteracao(Tipoagendainteracao.CONTA);

				String telefones = "";
				for(Contacrmcontato contato : item.getListcontacrmcontato()){
					telefones += contato.getTelefoneComTipoQuebraDeLinha();
				}
				controle.setTelefones(telefones);
				controle.setContato(item.getContatos());
				
				if(item.getListcontacrmcontato() != null && item.getListcontacrmcontato().size() > 0){
					Contacrmcontato ccc = item.getListcontacrmcontato().get(0);
					if(ccc.getMunicipio() != null){
						controle.setMunicipionome(ccc.getMunicipio().getNome());
						controle.setUfsigla(ccc.getMunicipio().getUf().getSigla());
						if(StringUtils.isNotBlank(controle.getMunicipionome())){
							controle.setCidadeuf(controle.getMunicipionome() + " / " + controle.getUfsigla());
						}
					}
				}
				
				if(item.getListcontacrmsegmento() != null && item.getListcontacrmsegmento().size() > 0){
					String segmentos = "";
					for(Contacrmsegmento crmsegmento : item.getListcontacrmsegmento()){
						if(crmsegmento.getSegmento() != null){
							segmentos += crmsegmento.getSegmento().getNome() + ", ";
						}
					}
					controle.setSegmentos(segmentos != "" ? segmentos.substring(0, segmentos.length() - 2) : segmentos);
				}
				
				if(filtro.getDatade() != null && filtro.getDataate() != null && item != null){
					List<Contacrmhistorico> listaHistorico = new ArrayList<Contacrmhistorico>();
					
					
//						if(item.getListoportunidade() != null && !item.getListoportunidade().isEmpty()){
//							for(Oportunidade o : item.getListoportunidade()){
//								if(o.getListoportunidadehistorico() != null && !o.getListoportunidadehistorico().isEmpty()){
//									for(Oportunidadehistorico oh : o.getListoportunidadehistorico()){
//										Contacrmhistorico ch = new Contacrmhistorico();
//										ch.setDtaltera(oh.getDtaltera());
//										item.getListcontacrmhistorico().add(ch);
//									}
//								}
//							}
//						}
					
					listaHistorico.add(contacrmService.buscaMaiorDataHistoricoContacrm(item.getListcontacrmhistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(filtro.getDatade()), 
							SinedDateUtils.dateToTimestampInicioDia(data2)));
					listaHistorico.add(contacrmService.buscaMaiorDataHistoricoContacrm(item.getListcontacrmhistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data2), 
							SinedDateUtils.dateToTimestampInicioDia(data3)));
					listaHistorico.add(contacrmService.buscaMaiorDataHistoricoContacrm(item.getListcontacrmhistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data3), 
							SinedDateUtils.dateToTimestampInicioDia(data4)));
					listaHistorico.add(contacrmService.buscaMaiorDataHistoricoContacrm(item.getListcontacrmhistorico(), 
							SinedDateUtils.dateToTimestampInicioDia(data4), 
							SinedDateUtils.dateToTimestampInicioDia(SinedDateUtils.addDiasData(filtro.getDataate(),1))));
					
					controle.setListaHistoricoContacrm(listaHistorico);
				}
				
				listaControleInteracao.add(controle);
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaControleInteracao) && (OrdenacaoControleInteracao.CIDADE.equals(filtro.getOrdenacao()) ||
				OrdenacaoControleInteracao.UF.equals(filtro.getOrdenacao()) || 
				OrdenacaoControleInteracao.MUNICIPIOUF.equals(filtro.getOrdenacao()))){
			
			if(OrdenacaoControleInteracao.CIDADE.equals(filtro.getOrdenacao())){
				Collections.sort(listaControleInteracao,new Comparator<ControleInteracaoBean>(){
					public int compare(ControleInteracaoBean c1, ControleInteracaoBean c2) {
						if(c1.getMunicipionome() == null) c1.setMunicipionome("");
						if(c2.getMunicipionome() == null) c2.setMunicipionome("");
						return c1.getMunicipionome().compareTo(c2.getMunicipionome());
					}
				});
			}else if(OrdenacaoControleInteracao.UF.equals(filtro.getOrdenacao())){
				Collections.sort(listaControleInteracao,new Comparator<ControleInteracaoBean>(){
					public int compare(ControleInteracaoBean c1, ControleInteracaoBean c2) {
						if(c1.getUfsigla() == null) c1.setUfsigla("");
						if(c2.getUfsigla() == null) c2.setUfsigla("");
						return c1.getUfsigla().compareTo(c2.getUfsigla());
					}
				});
			}else {
				Collections.sort(listaControleInteracao,new Comparator<ControleInteracaoBean>(){
					public int compare(ControleInteracaoBean c1, ControleInteracaoBean c2) {
						if(c1.getCidadeuf() == null) c1.setCidadeuf("");
						if(c2.getCidadeuf() == null) c2.setCidadeuf("");
						return c1.getCidadeuf().compareTo(c2.getCidadeuf());
					}
				});
			}
		}
		
		return listaControleInteracao;
	}
}
