package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulavalorvenda;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.FormulatabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.OportunidadeVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.TabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.VendaVO;
import br.com.linkcom.sined.geral.dao.MaterialformulavalorvendaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialformulavalorvenda.MaterialformulavalorvendaRESTModel;

public class MaterialformulavalorvendaService extends GenericService<Materialformulavalorvenda>{
	
	private MaterialformulavalorvendaDAO materialformulavalorvendaDAO;
	private MaterialService materialService;
	private TabelavalorService tabelavalorService;
	
	public void setMaterialformulavalorvendaDAO(MaterialformulavalorvendaDAO materialformulavalorvendaDAO) {
		this.materialformulavalorvendaDAO = materialformulavalorvendaDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}

	/* singleton */
	private static MaterialformulavalorvendaService instance;
	public static MaterialformulavalorvendaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialformulavalorvendaService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo calcula o valor de venda
	 *
	 * @param material
	 * @return
	 * @throws ScriptException
	 * @author Luiz Fernando
	 * @since 20/05/2014
	 */
	public Double calculaValorvendaMaterial(Material material) throws ScriptException {
		Double valorvenda = 0d;
		
		if(material != null){
			if(material.getListaMaterialformulavalorvenda() != null && !material.getListaMaterialformulavalorvenda().isEmpty()){
				List<Materialformulavalorvenda> listaMaterialformulavalorvenda = material.getListaMaterialformulavalorvenda();
				
				Pattern pattern_material = Pattern.compile("material\\.[a-zA-Z]+\\(([0-9]+)\\)");
				List<String> idsMateriais = new ArrayList<String>();
				idsMateriais.add(material.getCdmaterial().toString());
				if(material.getBanda() != null && material.getBanda().getCdmaterial() != null){
					idsMateriais.add(material.getBanda().getCdmaterial().toString());
				}
				
				Pattern pattern_tabelavalor = Pattern.compile("tabelavalor\\.[a-zA-Z]+\\((['a-zA-Z0-9_']+)\\)");
				List<String> identificadoresTabelavalor = new ArrayList<String>();
				
				for (Materialformulavalorvenda materialformulavalorvenda : listaMaterialformulavalorvenda) {
					if(materialformulavalorvenda.getOrdem() == null ||
							materialformulavalorvenda.getIdentificador() == null ||
							materialformulavalorvenda.getFormula() == null){
						return 0d;
					}else {
						Matcher m_material = pattern_material.matcher(materialformulavalorvenda.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
						
						Matcher m_tabelavalor = pattern_tabelavalor.matcher(materialformulavalorvenda.getFormula());
						while(m_tabelavalor.find()){
							identificadoresTabelavalor.add(m_tabelavalor.group(1));
						}
					}
				}
				
				FormulaVO formulaVO = new FormulaVO();
				
				if(idsMateriais.size() > 0){
					List<Material> listaMaterial = materialService.findForCalcularValorvenda(CollectionsUtil.concatenate(idsMateriais, ","));
					for (Material m : listaMaterial) {
						MaterialVO materialVO = new MaterialVO();
						materialVO.setCdmaterial(m.getCdmaterial());
						materialVO.setPesobruto(m.getPesobruto() != null ? m.getPesobruto() : 0.0);
						materialVO.setFrete(m.getValorfrete() != null ? m.getValorfrete().getValue().doubleValue() : 0.0);
						if(m.equals(material) && material.getValorcustoByCalculoProducao() != null){
							materialVO.setValorcusto(material.getValorcustoByCalculoProducao());
						}else {
							materialVO.setValorcusto(m.getValorcusto() != null ? m.getValorcusto() : 0.0);
						}
						materialVO.setValorvenda(m.getValorvenda() != null ? m.getValorvenda() : 0.0);
						if(m.getMaterialproduto() != null){
							materialVO.setAltura(m.getMaterialproduto().getAltura() != null ? m.getMaterialproduto().getAltura() : 0.0);
							materialVO.setLargura(m.getMaterialproduto().getLargura() != null ? m.getMaterialproduto().getLargura() : 0.0);
							materialVO.setComprimento(m.getMaterialproduto().getComprimento() != null ? m.getMaterialproduto().getComprimento() : 0.0);
						}
						if(m.equals(material) && material.getBanda() != null){
							materialVO.setCdmaterialbanda(material.getBanda().getCdmaterial());
						}
						formulaVO.addMaterial(materialVO);
						
						if(m.getCdmaterial().equals(material.getCdmaterial())){
							formulaVO.setAux_materialVO(materialVO);
							if(m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
								formulaVO.setCdgrupomaterial(m.getMaterialgrupo().getCdmaterialgrupo());
							}
							if(m.getOrigemproduto() != null){
								formulaVO.setOrigem(m.getOrigemproduto().ordinal());
							}
						}
					}
				}
				
				VendaVO vendaVO = new VendaVO();
				if(material.getVendaaltura() != null){
					vendaVO.setAltura(material.getVendaaltura());
				}
				if(material.getVendalargura() != null){
					vendaVO.setLargura(material.getVendalargura());
				}
				if(material.getVendacomprimento() != null){
					vendaVO.setComprimento(material.getVendacomprimento());
				}
				if(material.getVendafrete() != null){
					vendaVO.setFrete(material.getVendafrete());
				}
				
				FormulatabelavalorVO formulatabelavalorVO = new FormulatabelavalorVO();
				if(identificadoresTabelavalor.size() > 0){
					List<Tabelavalor> listaTabelavalor = tabelavalorService.findForCalcularValorvenda(CollectionsUtil.concatenate(identificadoresTabelavalor, ","));
					for (Tabelavalor tabelavalor : listaTabelavalor) {
						formulatabelavalorVO.addTabelavalor(new TabelavalorVO(tabelavalor));
					}
				}
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				OportunidadeVO oportunidadeVO = new OportunidadeVO();
				oportunidadeVO.setRevendedor(material.getRevendedor() != null ? material.getRevendedor() : false);
				oportunidadeVO.setQuantidade(material.getQuantidadeOportunidade() != null ? material.getQuantidadeOportunidade() : 0d);
				
				engine.put("material", formulaVO);
				engine.put("venda", vendaVO);
				engine.put("tabelavalor", formulatabelavalorVO);
				engine.put("oportunidade", oportunidadeVO);
				
				Collections.sort(listaMaterialformulavalorvenda, new Comparator<Materialformulavalorvenda>(){
					public int compare(Materialformulavalorvenda o1, Materialformulavalorvenda o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Materialformulavalorvenda materialformulavalorvenda : listaMaterialformulavalorvenda) {
					Object obj = engine.eval(materialformulavalorvenda.getFormula());
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(materialformulavalorvenda.getIdentificador(), resultado);
					valorvenda = SinedUtil.roundByParametro(resultado);
				}
				
				material.setIsValorvendaFormula(true);
			}  else if(material.getValorvenda() != null){
				valorvenda = material.getValorvenda();
			}
		}
		
		return valorvenda;
	}
	
	public boolean setValorvendaByFormula(Material material, Double altura, Double largura, Double comprimento, Double frete){
		return this.setValorvendaByFormula(material, altura, largura, comprimento, frete, null, true);
	}
	
	public boolean setValorvendaByFormula(Material material, Double altura, Double largura, Double comprimento, Double frete, Double valorcustoProducao, boolean carregarMaterial){
		boolean isValorvendaFormula = false; 
		if(material != null && material.getCdmaterial() != null){
			try {
				Material bean = carregarMaterial ? materialService.loadForCalcularValorvenda(material) : material;
				
				if(SinedUtil.isRecapagem() && material.getBanda() == null){
					Material mateiral_aux = carregarMaterial ? bean : materialService.loadForCalcularValorvenda(material);
					if(mateiral_aux != null && SinedUtil.isListNotEmpty(mateiral_aux.getListaProducao())){
						for (Materialproducao materialproducao : mateiral_aux.getListaProducao()){
							if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
								bean.setBanda(materialproducao.getMaterial());
								break;
							}
						}
					}
				}else {
					bean.setBanda(material.getBanda());
				}
				
				bean.setVendaaltura(altura);
				bean.setVendalargura(largura);
				bean.setVendacomprimento(comprimento);
				bean.setVendafrete(frete);
				bean.setValorcustoByCalculoProducao(valorcustoProducao);
				if(material.getValorvenda() != null && material.getConsiderarValorvendaCalculado() != null && 
						material.getConsiderarValorvendaCalculado()){
					bean.setValorvenda(material.getValorvenda());
				}
				Double valorvendaCalculado = calculaValorvendaMaterial(bean);
				if(valorvendaCalculado != null && valorvendaCalculado > 0d){
					material.setValorvenda(valorvendaCalculado);
				}
				isValorvendaFormula = bean.getIsValorvendaFormula() != null ? bean.getIsValorvendaFormula() : false; 
			} catch (Exception e) {e.printStackTrace();}
		}
		return isValorvendaFormula;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialformulavalorvendaDAO#findByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @since 20/05/2014
	 */
	public List<Materialformulavalorvenda> findByMaterial(Material material) {
		return materialformulavalorvendaDAO.findByMaterial(material);
	}

	public void saveReplicarFormulavalorvenda(List<Materialformulavalorvenda> listaMaterialformulavalorvenda, Material material) {
		if(material != null && material.getCdmaterial() != null && listaMaterialformulavalorvenda != null && 
				!listaMaterialformulavalorvenda.isEmpty()){
			if(material.getListaMaterialformulavalorvenda() != null && !material.getListaMaterialformulavalorvenda().isEmpty()){
				deleteFormulavalorvendaByMaterial(material);
			}
			for(Materialformulavalorvenda materialformulavalorvenda : listaMaterialformulavalorvenda){
				materialformulavalorvenda.setCdmaterialformulavalorvenda(null);
				materialformulavalorvenda.setMaterial(material);
				saveOrUpdate(materialformulavalorvenda);
			}
		}
		
	}

	private void deleteFormulavalorvendaByMaterial(Material material) {
		materialformulavalorvendaDAO.deleteFormulavalorvendaByMaterial(material);
	}
	
	/**
	* M�todo que retorna as formulas de venda dos materiais para sincronizar com o app
	*
	* @return
	* @since 29/07/2016
	* @author Luiz Fernando
	*/
	public List<MaterialformulavalorvendaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialformulavalorvendaDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 29/07/2016
	* @author Luiz Fernando
	*/
	public List<MaterialformulavalorvendaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialformulavalorvendaRESTModel> lista = new ArrayList<MaterialformulavalorvendaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialformulavalorvenda m : materialformulavalorvendaDAO.findForAndroid(whereIn))
				lista.add(new MaterialformulavalorvendaRESTModel(m));
		}
		
		return lista;
	}
}
