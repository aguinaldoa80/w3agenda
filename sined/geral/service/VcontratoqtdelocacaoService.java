package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.view.Vcontratoqtdelocacao;
import br.com.linkcom.sined.geral.dao.VcontratoqtdelocacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VcontratoqtdelocacaoService extends GenericService<Vcontratoqtdelocacao> {

	private VcontratoqtdelocacaoDAO vcontratoqtdelocacaoDAO;
	
	public void setVcontratoqtdelocacaoDAO(
			VcontratoqtdelocacaoDAO vcontratoqtdelocacaoDAO) {
		this.vcontratoqtdelocacaoDAO = vcontratoqtdelocacaoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VcontratoqtdelocacaoDAO#findForAtualizacao
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/05/2014
	 */
	public List<Vcontratoqtdelocacao> findForAtualizacao() {
		return vcontratoqtdelocacaoDAO.findForAtualizacao();
	}

	public boolean isHabilitadoParaRenovacao(Contrato contrato) {
		return vcontratoqtdelocacaoDAO.isHabilitadoParaRenovacao(contrato);
	}

}
