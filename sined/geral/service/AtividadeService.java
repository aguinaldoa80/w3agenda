package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.dao.AtividadeDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.AtividadeFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AtividadeService extends GenericService<Atividade>{
	
	private AtividadeDAO atividadeDAO;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	public void setAtividadeDAO(AtividadeDAO atividadeDAO) {
		this.atividadeDAO = atividadeDAO;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	
	/**
	 * Obt�m lista de atividade para o relat�rio de atividades.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AtividadeDAO#findForReport(AtividadeFiltro)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Atividade> findForReport(AtividadeFiltro filtro){
		return atividadeDAO.findForReport(filtro);
	}
	
	/**
	 * Gera o relat�rio de atividade.
	 * 
	 * @see #findForReport(AtividadeFiltro)
	 * @see br.com.linkcom.sined.geral.service.ProjetoService#load(Projeto, String)
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#load(Planejamento, String)
	 * @param filtro
	 * @return
	 * @author Fl�vio Tavares
	 */
	public IReport geraReportAtividade(AtividadeFiltro filtro){
		Report report = new Report("/projeto/atividade");
		List<Atividade> listaAtividade = this.findForReport(filtro);
		report.setDataSource(listaAtividade);
		
		Projeto projeto = null;
		if(filtro.getProjeto() != null){
			projeto = projetoService.load(filtro.getProjeto(), "projeto.cdprojeto, projeto.nome");
		}
		Planejamento planejamento = null;
		if(filtro.getPlanejamento() != null){
			planejamento = planejamentoService.load(filtro.getPlanejamento(), "planejamento.cdplanejamento, planejamento.descricao");
		}
		
		report.addParameter("PROJETO", projeto);
		report.addParameter("PLANEJAMENTO", planejamento);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AtividadeDAO#findByData
	 * @param dtdia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Atividade> findByData(Date dtdia, Projeto projeto) {
		return atividadeDAO.findByData(dtdia, projeto);
	}
}
