package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoamaterial;
import br.com.linkcom.sined.geral.dao.PessoamaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoamaterialService extends GenericService<Pessoamaterial>{

	private PessoamaterialDAO pessoamaterialDAO;
	
	public List<Pessoamaterial> findByPessoa(Pessoa pessoa){
		return this.pessoamaterialDAO.findByPessoa(pessoa);
	}
	
	public void setPessoamaterialDAO(PessoamaterialDAO pessoamaterialDAO) {
		this.pessoamaterialDAO = pessoamaterialDAO;
	}
}
