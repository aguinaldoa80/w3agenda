package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.dao.CentrocustoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CentrocustoService extends GenericService<Centrocusto> {

	private CentrocustoDAO centrocustoDAO;
	private ParametrogeralService parametrogeralService;
	private DocumentoService documentoService;
	private MovimentacaoestoqueService movimentacaoEstoqueService;
	private MovimentacaoService movimentacaoService;
	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setMovimentacaoEstoqueService(MovimentacaoestoqueService movimentacaoEstoqueService) {this.movimentacaoEstoqueService = movimentacaoEstoqueService;}
	public void setCentrocustoDAO(CentrocustoDAO centrocustoDAO) {
		this.centrocustoDAO = centrocustoDAO;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	public List<Centrocusto> findAtivosFluxoCaixa(String whereIn) {
		return centrocustoDAO.findAtivosFluxoCaixa(whereIn);
	}
	
	public List<Centrocusto> findForError(String listaCentrocusto) {
		return centrocustoDAO.findForError(listaCentrocusto);
	}
	
	/**
	 * Renorna uma lista de centro de custo contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CentrocustoDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Centrocusto> findDescricao(String whereIn) {
		return centrocustoDAO.findDescricao(whereIn);
	}
	
	/**
	 * Retorna os nomes dos Centros de Custo, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 * 
	 * @see #findDescricao(String)
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeCentroCustos(List<ItemDetalhe> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(lista, "centrocusto.cdcentrocusto", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);
		
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/**
	 * Lista os centro de custo para o combo do flex
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CentrocustoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Centrocusto> findAllForFlex(){
		return centrocustoDAO.findAllForFlex();
	}
	
	/* singleton */
	private static CentrocustoService instance;
	public static CentrocustoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CentrocustoService.class);
		}
		return instance;
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Centrocusto> findForArquivoGerencial() {
		return centrocustoDAO.findForArquivoGerencial();
	}
	
	public Centrocusto getCentrocustoIndenizacaoByParametro(){
		Centrocusto centrocusto = null;
		try {
			String idCentrocusto = parametrogeralService.getValorPorNome(Parametrogeral.INDENIZACAO_CENTROCUSTO);
			centrocusto = load(new Centrocusto(Integer.parseInt(idCentrocusto)), "centrocusto.cdcentrocusto, centrocusto.nome");
		} catch (Exception e) {}
		return centrocusto;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param id
	 * @return
	 * @since 04/02/2016
	 * @author C�sar
	 */
	public List<GenericBean> ocorrenciaCentrocusto(Integer id){
		return centrocustoDAO.ocorrenciaCentrocusto(id);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param centrocusto
	 * @param datereferencia
	 * @param empresas
	 * @return
	 * @since 28/11/2016
	 * @author C�sar
	 */
	public Money calculaSaldoAtualCentroCusto(Centrocusto centrocusto, Date datereferencia, Integer cdcontatipo, Integer cdconta, Empresa... empresas){
		return centrocustoDAO.calculaSaldoAtualCentroCusto(centrocusto, datereferencia, cdcontatipo, cdconta ,empresas);
	}
	

	public List<Centrocusto> findByListaCodigoalternativo(List<Integer> listaCodigoalternativo){
		if (listaCodigoalternativo==null || listaCodigoalternativo.isEmpty()){
			return new ArrayList<Centrocusto>();
		}
		return centrocustoDAO.findByListaCodigoalternativo(listaCodigoalternativo);
	}
	public List<Centrocusto> buscarParaCusteio(boolean csv, Date dtInicio, Date dtFim, Empresa empresa) {
		List<Centrocusto> listaFinal = new ArrayList<Centrocusto>();
		List<Centrocusto> listaCentroCusto = centrocustoDAO.buscarParaCusteio();
		if(SinedUtil.isListNotEmpty(listaCentroCusto)){
				for (Centrocusto centroCusto : listaCentroCusto) {
					List<Documento> listaDoc = documentoService.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
					if(SinedUtil.isListNotEmpty(listaDoc)){
						listaFinal.add(centroCusto);
						continue;
					}
					List<Movimentacao> listaMov = movimentacaoService.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
					if(SinedUtil.isListNotEmpty(listaMov)){
						listaFinal.add(centroCusto);
						continue;
					}
					List<Movimentacaoestoque> listaEstoque = movimentacaoEstoqueService.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
					if(SinedUtil.isListNotEmpty(listaEstoque)){
						listaFinal.add(centroCusto);
						continue;
					}
				}
		}
		return listaFinal;
	}
	public Centrocusto buscarParaProcessarCusteio(Integer id) {
		return centrocustoDAO.buscarParaProcessarCusteio(id);
	}
	
	
	
	
	
	
	
	
	
	
	
}
