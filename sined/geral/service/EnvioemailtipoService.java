package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EnvioemailtipoService extends GenericService<Envioemailtipo>{
		
	//private EnvioemailtipoDAO envioemailtipoDAO;
	//public void setEnvioemailtipoDAO(EnvioemailtipoDAO envioemailtipoDAO) {
	//	this.envioemailtipoDAO = envioemailtipoDAO;
	//}	
	
	private static EnvioemailtipoService instance;
	public static EnvioemailtipoService getInstance(){
		if (instance==null){
			instance = Neo.getObject(EnvioemailtipoService.class);
		}
		return instance;
	}		
}