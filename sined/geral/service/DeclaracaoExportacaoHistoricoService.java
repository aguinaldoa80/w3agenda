package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoHistorico;
import br.com.linkcom.sined.geral.dao.DeclaracaoExportacaoHistoricoDAO;

public class DeclaracaoExportacaoHistoricoService extends GenericService<DeclaracaoExportacaoHistorico> {

	private DeclaracaoExportacaoHistoricoDAO declaracaoExportacaoHistoricoDAO;	
	public void setDeclaracaoExportacaoHistoricoDAO(DeclaracaoExportacaoHistoricoDAO declaracaoExportacaoHistoricoDAO) {this.declaracaoExportacaoHistoricoDAO = declaracaoExportacaoHistoricoDAO;}
	
	
}
