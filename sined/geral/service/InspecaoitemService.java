package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.geral.dao.InspecaoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InspecaoitemService extends GenericService<Inspecaoitem> {

	private InspecaoitemDAO inspecaoitemDAO;
	
	public void setInspecaoitemDAO(InspecaoitemDAO inspecaoitemDAO) {
		this.inspecaoitemDAO = inspecaoitemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param itens
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Inspecaoitem> findItens(List<Inspecaoitem> itens) {
		return inspecaoitemDAO.findItens(itens);
	}

}
