package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.documentohistorico.DocumentohistoricoRESTModel;

public class DocumentohistoricoService extends GenericService<Documentohistorico>{

	
	private DocumentohistoricoDAO documentohistoricoDAO;
	public void setDocumentohistoricoDAO(DocumentohistoricoDAO documentohistoricoDAO) {
		this.documentohistoricoDAO = documentohistoricoDAO;
	}
	
	/* singleton */
	private static DocumentohistoricoService instance;
	public static DocumentohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentohistoricoService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m um documentohistorico para visualiza��o.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO#findForVisualizacao(Documentohistorico)
	 * @param documentohistorico
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documentohistorico findForVisualizacao(Documentohistorico documentohistorico){
		return documentohistoricoDAO.findForVisualizacao(documentohistorico);
	}
	
	/**
	 *	M�todo que prepara o historico do documento. 
	 * 
	 * @param documentohistorico
	 * @param acao
	 * @param documento
	 * @return 
	 * @author Rodrigo Freitas
	 * @author Hugo Ferreira - revis�o das transactions
	 */
	public Documentohistorico gerarHistoricoDocumento(Documentohistorico documentohistorico, Documentoacao acao, Documento documento) {
		if (documentohistorico == null) {
			documentohistorico = new Documentohistorico();
		}
		documentohistorico.setValor(documento.getValor());
		documentohistorico.setDocumento(documento);
		documentohistorico.setDocumentoacao(acao);
		documentohistorico.setDocumentotipo(documento.getDocumentotipo());
		documentohistorico.setDtemissao(documento.getDtemissao());
		documentohistorico.setDtvencimento(documento.getDtvencimento());
		documentohistorico.setTipopagamento(documento.getTipopagamento());
		documentohistorico.setOutrospagamento(documento.getOutrospagamento());
		documentohistorico.setNumero(documento.getNumero());
		documentohistorico.setDescricao(documento.getDescricao());
		documentohistorico.setPessoa(documento.getPessoa());
		
		if(documento.getFromRetorno() != null && documento.getFromRetorno()){
			if (documento.getHistoricoBaixaRetorno() != null && !"".equals(documento.getHistoricoBaixaRetorno())){
				documentohistorico.setObservacao(documento.getHistoricoBaixaRetorno());
			} else
				documentohistorico.setObservacao("Baixa feita via arquivo de retorno");
		}
		if(documento.getFromWebservice() != null && documento.getFromWebservice()){
			documentohistorico.setObservacao("Baixa feita via webservice");
		}
		if(documento.getFromBaixaautomatica() != null && documento.getFromBaixaautomatica()){
			documentohistorico.setObservacao("Baixa feita autom�tica");
		}
		
		if(documento.getFromAcaoProtestar() != null && documento.getFromAcaoProtestar()){
			String obsAcao = "";
			if(acao.equals(Documentoacao.PROTESTADA)) obsAcao = Documentoacao.PROTESTADA.getNome();
			else if(acao.equals(Documentoacao.ENVIADO_JURIDICO)) obsAcao = Documentoacao.ENVIADO_JURIDICO.getNome();
			else if(acao.equals(Documentoacao.ENVIADO_CARTORIO)) obsAcao = Documentoacao.ENVIADO_CARTORIO.getNome();
			else if(acao.equals(Documentoacao.ARQUIVO_MORTO)) obsAcao = Documentoacao.ARQUIVO_MORTO.getNome();
			
			if(documentohistorico.getObservacao() != null && !documentohistorico.getObservacao().equals("")){
				documentohistorico.setObservacao(obsAcao + ": " + documentohistorico.getObservacao());
			}
		}
		return documentohistorico;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. Carrega o historico de um documento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO#carregarHistorico(Documento)
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentohistorico> carregaHistorico(Documento documento){
		return documentohistoricoDAO.carregarHistorico(documento);
	}

	/**
	 * M�todo para gerar um bean de Documentohistorico com as propriedades de Documento.
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documentohistorico geraHistoricoDocumento(Documento documento){
		Documentohistorico documentohistorico = new Documentohistorico();
		documentohistorico.setValor(documento.getValor());
		documentohistorico.setDocumento(documento);
		documentohistorico.setDocumentoacao(documento.getDocumentoacao());
		documentohistorico.setDocumentotipo(documento.getDocumentotipo());
		documentohistorico.setDtemissao(documento.getDtemissao());
		documentohistorico.setDtvencimento(documento.getDtvencimento());
		documentohistorico.setTipopagamento(documento.getTipopagamento());
		documentohistorico.setOutrospagamento(documento.getOutrospagamento());
		documentohistorico.setNumero(documento.getNumero());
		documentohistorico.setDescricao(documento.getDescricao());
		documentohistorico.setPessoa(documento.getPessoa());
		return documentohistorico;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO#findPenultimaAcao
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documentoacao findPenultimaAcao(Documento documento) {
		Documentohistorico dh = documentohistoricoDAO.findPenultimaAcao(documento);
		if(dh != null){
			return dh.getDocumentoacao();
		}else{
			return null;
		}
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO#carregarHistorico(Documento)
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Boolean possuiBoletoEnviado (Documento documento) {
		return documentohistoricoDAO.possuiBoletoEnviado (documento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param bean
	 * @param observacaoHistorico
	 */
	public void updateObservacaoDocumento(Documento bean, String observacaoHistorico) {
		documentohistoricoDAO.updateObservacaoDocumento(bean, observacaoHistorico);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.DocumentohistoricoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<DocumentohistoricoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentohistoricoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<DocumentohistoricoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<DocumentohistoricoRESTModel> lista = new ArrayList<DocumentohistoricoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Documentohistorico bean : documentohistoricoDAO.findForAndroid(whereIn))
				lista.add(new DocumentohistoricoRESTModel(bean));
		}
		
		return lista;
	}
	
	public Documentohistorico loadUltimoHistorico(Documento documento) {
		return documentohistoricoDAO.loadUltimoHistorico(documento);
	}
	
	public void updateObservacaoDocumento(Documentohistorico documentohistorico, String observacaoHistorico) {
		documentohistoricoDAO.updateObservacaoDocumento(documentohistorico, observacaoHistorico);
	}
}
