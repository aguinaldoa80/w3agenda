package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.dao.MovpatrimonioorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovpatrimonioorigemService extends GenericService<Movpatrimonioorigem> {

	private MovpatrimonioorigemDAO movpatrimonioorigemDAO;
	
	public void setMovpatrimonioorigemDAO(
			MovpatrimonioorigemDAO movpatrimonioorigemDAO) {
		this.movpatrimonioorigemDAO = movpatrimonioorigemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeMovPatrimonioComOrigemEntrega(Entrega entrega) {
		return movpatrimonioorigemDAO.existeMovPatrimonioComOrigemEntrega(entrega);
	}


}
