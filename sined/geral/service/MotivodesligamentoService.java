package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Motivodesligamento;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MotivodesligamentoService extends GenericService<Motivodesligamento> {	
	
	/* singleton */
	private static MotivodesligamentoService instance;
	public static MotivodesligamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MotivodesligamentoService.class);
		}
		return instance;
	}
	
}
