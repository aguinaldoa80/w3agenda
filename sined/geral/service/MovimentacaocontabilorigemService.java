package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabilorigem;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.dao.MovimentacaocontabilorigemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaocontabilorigemService extends GenericService<Movimentacaocontabilorigem>{
	
	private MovimentacaocontabilorigemDAO movimentacaocontabilorigemDAO;
	private static MovimentacaocontabilorigemService instance;
	
	public void setMovimentacaocontabilorigemDAO(MovimentacaocontabilorigemDAO movimentacaocontabilorigemDAO) {
		this.movimentacaocontabilorigemDAO = movimentacaocontabilorigemDAO;
	}
	
	public static MovimentacaocontabilorigemService getInstance() {
		if (instance==null){
			instance = Neo.getObject(MovimentacaocontabilorigemService.class);
		}
		return instance;
	}
	
	public List<Movimentacaocontabilorigem> findByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil){
		return movimentacaocontabilorigemDAO.findByMovimentacaocontabil(movimentacaocontabil);
	}
	
	public String getLinksOrigem(List<Movimentacaocontabilorigem> listaMovimentacaocontabilorigem, boolean listagem){
		String links = "";
		
		if (listaMovimentacaocontabilorigem!=null){
			for (Movimentacaocontabilorigem movimentacaocontabilorigem: listaMovimentacaocontabilorigem){
				String path = null;
				String descricao = null;
				
				if (movimentacaocontabilorigem.getNota()!=null){
					if (NotaTipo.NOTA_FISCAL_PRODUTO.equals(movimentacaocontabilorigem.getNota().getNotaTipo())){
						path = "/faturamento/crud/Notafiscalproduto";
						descricao = listagem ? "NF Produto" : "Nota fiscal de produto";
					}else if (NotaTipo.NOTA_FISCAL_SERVICO.equals(movimentacaocontabilorigem.getNota().getNotaTipo())){
						path = "/faturamento/crud/NotaFiscalServico";
						descricao = listagem ? "NF Serviço" : "Nota fiscal de serviço";
					}
					path += getParametrosLinkOrigem(path, "cdNota", movimentacaocontabilorigem.getNota().getCdNota());
				}else if (movimentacaocontabilorigem.getEntregadocumento()!=null){
					path = "/fiscal/crud/Entradafiscal" + getParametrosLinkOrigem(path, "cdentregadocumento", movimentacaocontabilorigem.getEntregadocumento().getCdentregadocumento());
					descricao = listagem ? "Entrada fiscal" : "Entrada fiscal";
				}else if (movimentacaocontabilorigem.getDocumento()!=null){
					if (Documentoclasse.OBJ_PAGAR.equals(movimentacaocontabilorigem.getDocumento().getDocumentoclasse())){
						path = "/financeiro/crud/Contapagar";
						descricao = listagem ? "Conta a pagar" : "Conta a pagar";
					}else if (Documentoclasse.OBJ_RECEBER.equals(movimentacaocontabilorigem.getDocumento().getDocumentoclasse())){
						path = "/financeiro/crud/Contareceber";
						descricao = listagem ? "Conta a receber" : "Conta a receber";
					}
					path += getParametrosLinkOrigem(path, "cddocumento", movimentacaocontabilorigem.getDocumento().getCddocumento());
				}else if (movimentacaocontabilorigem.getMovimentacao()!=null){
					path = "/financeiro/crud/Movimentacao" + getParametrosLinkOrigem(path, "cdmovimentacao", movimentacaocontabilorigem.getMovimentacao().getCdmovimentacao());
					descricao = listagem ? "Movimentação" : "Movimentação";
				}else if (movimentacaocontabilorigem.getVenda()!=null){
					path = "/faturamento/crud/Venda" + getParametrosLinkOrigem(path, "cdvenda", movimentacaocontabilorigem.getVenda().getCdvenda());
					descricao = listagem ? "Venda" : "Venda";
				}else if (movimentacaocontabilorigem.getMovimentacaoestoque()!=null) {
					path = "/suprimento/crud/Movimentacaoestoque" + getParametrosLinkOrigem(path, "cdmovimentacaoestoque", movimentacaocontabilorigem.getMovimentacaoestoque().getCdmovimentacaoestoque());
					descricao = listagem ? "Movimentação de estoque" : "Movimentação de estoque";
				}
				
				if (path!=null){
					links += "<a href='/" + NeoWeb.getApplicationName() + path + "' target='_blank'>" + descricao + "</a>";
					links += listagem ? "<br/>" : "&nbsp;&nbsp;&nbsp;";					
				}
			}
		}
		
		return links;
	}
	
	private String getParametrosLinkOrigem(String path, String campoId, Integer valorId){
		String parametros = "?ACAO=consultar&<CAMPO_ID>=<VALOR_ID>";
		return parametros.replace("<CAMPO_ID>", campoId).replace("<VALOR_ID>", valorId.toString());
	}
	
	public Boolean existeLancamentoContabilMovimentacao(Movimentacao movimentacao, MovimentacaocontabilTipoLancamentoEnum tipoLancamentoEnum){
		return movimentacaocontabilorigemDAO.existeLancamentoContabilMovimentacao(movimentacao, tipoLancamentoEnum);
	}
}