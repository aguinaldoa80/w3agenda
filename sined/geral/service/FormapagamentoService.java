package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.dao.FormapagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.formapagamento.FormapagamentoRESTModel;

public class FormapagamentoService extends GenericService<Formapagamento> {

	private FormapagamentoDAO formapagamentoDAO;
	
	public void setFormapagamentoDAO(FormapagamentoDAO formapagamentoDAO) {
		this.formapagamentoDAO = formapagamentoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FormapagamentoDAO#findCredito
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Formapagamento> findCredito(){
		return formapagamentoDAO.findCredito();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FormapagamentoDAO#findDebito
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Formapagamento> findDebito(){
		return formapagamentoDAO.findDebito();
	}
	
	
	/* singleton */
	private static FormapagamentoService instance;
	public static FormapagamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FormapagamentoService.class);
		}
		return instance;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param formapagamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/07/2014
	 */
	public boolean isConciliacaoAuto(Formapagamento formapagamento) {
		return formapagamentoDAO.isConciliacaoAuto(formapagamento);
	}

	public List<FormapagamentoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<FormapagamentoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<FormapagamentoRESTModel> lista = new ArrayList<FormapagamentoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Formapagamento bean : formapagamentoDAO.findForAndroid(whereIn))
				lista.add(new FormapagamentoRESTModel(bean));
		}
		
		return lista;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.FormapagamentoDAO#findCreditoAndDebito
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Formapagamento> findCreditoAndDebito(){
		return formapagamentoDAO.findCreditoAndDebito();
	}
}
