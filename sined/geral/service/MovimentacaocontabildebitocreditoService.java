package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacaocontabil;
import br.com.linkcom.sined.geral.bean.Movimentacaocontabildebitocredito;
import br.com.linkcom.sined.geral.dao.MovimentacaocontabildebitocreditoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaocontabildebitocreditoService extends GenericService<Movimentacaocontabildebitocredito>{
	
	private MovimentacaocontabildebitocreditoDAO movimentacaocontabildebitocreditoDAO;
	
	public void setMovimentacaocontabildebitocreditoDAO(MovimentacaocontabildebitocreditoDAO movimentacaocontabildebitocreditoDAO) {
		this.movimentacaocontabildebitocreditoDAO = movimentacaocontabildebitocreditoDAO;
	}
	
	/**
	 * 
	 * @param movimentacaocontabil
	 * @param propertyMovimentacaocontabil
	 * @author Thiago Clemente
	 * 
	 */
	public List<Movimentacaocontabildebitocredito> findByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil, String propertyMovimentacaocontabil){
		return movimentacaocontabildebitocreditoDAO.findByMovimentacaocontabil(movimentacaocontabil, propertyMovimentacaocontabil);
	}
	
	public List<Movimentacaocontabildebitocredito> findWithIdentificadorByMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil, String propertyMovimentacaocontabil){
		return movimentacaocontabildebitocreditoDAO.findWithIdentificadorByMovimentacaocontabil(movimentacaocontabil, propertyMovimentacaocontabil);
	}
}
	


