package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.dao.ExameclinicaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExameclinicaService extends GenericService<Exameclinica> {	
	
	private ExameclinicaDAO exameclinicaDAO;
	public void setExameclinicaDAO(ExameclinicaDAO exameclinicaDAO) {
		this.exameclinicaDAO = exameclinicaDAO;
	}
	
	/* singleton */
	private static ExameclinicaService instance;
	public static ExameclinicaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExameclinicaService.class);
		}
		return instance;
	}

	public List<Exameclinica> findAll(String campos, String orderBy) {
		return exameclinicaDAO.findAll(campos, orderBy);
	}


	
	
}
