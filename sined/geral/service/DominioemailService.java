package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Dominio;
import br.com.linkcom.sined.geral.bean.Dominioemail;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.DominioemailDAO;
import br.com.linkcom.sined.util.briefcase.BriefCase;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominioemailService extends GenericService<Dominioemail>{

	private DominioemailDAO dominioemailDAO;
	
	public void setDominioemailDAO(DominioemailDAO dominioemailDAO) {
		this.dominioemailDAO = dominioemailDAO;
	}
	
	/**
	 * M�todo que verifica se eixtem DominioEmail do dom�nio
	 * 
	 * @param dominio
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeDominioEmail(Dominio dominio) {
		return dominioemailDAO.existeDominioEmail(dominio);
	}
	
	@Override
	public void saveOrUpdate(Dominioemail bean) {
		boolean novoRegistro = false;
		if(bean.getCddominioemail() == null)
			novoRegistro = true;
		
		super.saveOrUpdate(bean);
		
		BriefCase.executar(Servicoservidortipo.SERVICO_SERVIDOR, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCddominioemail());
	}
	
	@Override
	public void delete(Dominioemail bean) {
		BriefCase.executar(Servicoservidortipo.SERVICO_SERVIDOR, AcaoBriefCase.REMOVER, bean.getCddominioemail());
		super.delete(bean);
	}
	
}
