package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagendamaterialitem;
import br.com.linkcom.sined.geral.dao.ProducaoagendamaterialitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendamaterialitemService extends GenericService<Producaoagendamaterialitem>{

	private ProducaoagendamaterialitemDAO producaoagendamaterialitemDAO;
	
	public void setProducaoagendamaterialitemDAO(
			ProducaoagendamaterialitemDAO producaoagendamaterialitemDAO) {
		this.producaoagendamaterialitemDAO = producaoagendamaterialitemDAO;
	}
	
	public List<Producaoagendamaterialitem> findByProducaoagenda(String whereIn) {
		return producaoagendamaterialitemDAO.findByProducaoagenda(whereIn);
	}
	

}
