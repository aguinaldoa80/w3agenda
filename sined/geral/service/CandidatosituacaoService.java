package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Candidatosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CandidatosituacaoService extends GenericService<Candidatosituacao>{
	
	/**
	 * Cria uma lista de Candidatosituacao para montar um checklist
	 * 
	 * @return
	 * @author M�rio Caixeta
	 */
	public List<Candidatosituacao> createListaForChecklist(){
		List<Candidatosituacao> lista = new ArrayList<Candidatosituacao>();
		lista.add(Candidatosituacao.EM_ABERTO);
		lista.add(Candidatosituacao.CONTRATADO);
		lista.add(Candidatosituacao.ELIMINADO);
		return lista;
	}
	
	/* singleton */
	private static CandidatosituacaoService instance;
	public static CandidatosituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CandidatosituacaoService.class);
		}
		return instance;
	}

}
