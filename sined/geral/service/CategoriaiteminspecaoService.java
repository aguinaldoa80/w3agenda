package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.dao.CategoriaiteminspecaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CategoriaiteminspecaoService extends GenericService<Categoriaiteminspecao> {

	
	private CategoriaiteminspecaoDAO categoriaiteminspecaoDAO;
	
	public void setCategoriaiteminspecaoDAO(
			CategoriaiteminspecaoDAO categoriaiteminspecaoDAO) {
		this.categoriaiteminspecaoDAO = categoriaiteminspecaoDAO;
	}
	
	/**
	 * <b>M�todo respons�vel em carregar uma lista de categoria ItemInspe��o a partir de um<br>
	 * modelo</b>
	 * @author Jo�o Paulo
	 * @see br.com.linkcom.w3auto.geral.dao.CategoriaiteminspecaoDAO#loadCategoriaItemInspecao(Modelo modelo)
	 * @param categoria
	 * @return
	 */
	public List<Categoriaiteminspecao> loadCategoriaItemInspecao(Veiculomodelo modelo, Boolean visual){
		return categoriaiteminspecaoDAO.loadCategoriaItemInspecao(modelo,visual);
	}
	
	/**
	 * <b>M�todo respons�vel em carregar uma lista de categoria ItemInspe��o a partir de um<br>
	 * modelo</b>
	 * @author Biharck
	 * @see br.com.linkcom.w3auto.geral.dao.CategoriaiteminspecaoDAO#loadCategoriaItemInspecao(Modelo modelo)
	 * @param categoria
	 * @return
	 */
	public List<Categoriaiteminspecao> loadCategoriaItemInspecao(Veiculomodelo modelo, String orderBy, Boolean asc){
		return categoriaiteminspecaoDAO.loadCategoriaItemInspecao(modelo, orderBy, asc);
	}
	
}
