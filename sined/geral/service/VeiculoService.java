package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.Veiculohorimetro;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.bean.Veiculoordemservico;
import br.com.linkcom.sined.geral.bean.Veiculoordemservicoitem;
import br.com.linkcom.sined.geral.bean.Veiculouso;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.bean.view.Vwrelatorioinspecao;
import br.com.linkcom.sined.geral.bean.view.Vwveiculoseminspecaovencido3;
import br.com.linkcom.sined.geral.bean.view.Vwveiculosseminspecao3;
import br.com.linkcom.sined.geral.bean.view.Vwveiculosvencidos;
import br.com.linkcom.sined.geral.dao.VeiculoDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.AgendarVeiculoInspecaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.process.filter.BuscarVeiculosSemInspecaoFiltro;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FormularioInspecaoReportBean;
import br.com.linkcom.sined.modulo.veiculo.controller.report.bean.FormularioInspecaoReportBeanItem;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculoService extends GenericService<Veiculo> {

	private VeiculokmService veiculokmService;
	private VeiculoDAO veiculoDAO;
	private VwrelatorioinspecaoService vwrelatorioinspecaoService;
	private VeiculoordemservicoService veiculoordemservicoServico;
	private VeiculousoService veiculousoService;
	
	public void setVwrelatorioinspecaoService(
			VwrelatorioinspecaoService vwrelatorioinspecaoService) {
		this.vwrelatorioinspecaoService = vwrelatorioinspecaoService;
	}
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
	public void setVeiculoDAO(VeiculoDAO veiculoDAO) {
		this.veiculoDAO = veiculoDAO;
	}
	public void setVeiculoordemservicoitemServico(
			VeiculoordemservicoService veiculoordemservicoServico) {
		this.veiculoordemservicoServico = veiculoordemservicoServico;
	}
	public void setVeiculousoService(VeiculousoService veiculousoService) {
		this.veiculousoService = veiculousoService;
	}
	
	@Override
	public void saveOrUpdate(final Veiculo bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				boolean isFirstTime = false;
				if(bean.getCdveiculo() == null){
					isFirstTime = true;
				}
				
				saveOrUpdateNoUseTransaction(bean);
				
				
				
				if(isFirstTime){
					if(bean.getKminicial() == null){
						bean.setKminicial(0);
					}
					veiculokmService.saveOrUpdateNoUseTransaction(new Veiculokm(bean, new Date(), bean.getKminicial(), Tipokmajuste.INCIAL));
				}
				
				return status;
			}
		});
	}
	
	public void atualizaKmInicial(Veiculo veiculo){
		veiculoDAO.atualizaKmInicial(veiculo);
	}
	
	/**
	 * M�todo que procura ve�culo pela sua placa
	 * 
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Veiculo
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findByPlaca(Veiculo)
	 */
	public Veiculo findByPlaca(Veiculo veiculo){
		return veiculoDAO.findByPlaca(veiculo);
	}
	
	/**
	 * M�todo que procura ve�culo pela sua placa
	 * 
	 * @author Rafael Odon
	 * @param veiculo
	 * @return Veiculo
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findVeiculo(Veiculo)
	 */
	public Veiculo findVeiculo(Veiculo veiculo){
		return veiculoDAO.findVeiculo(veiculo);
	}
	
	/**
	 * Lista os ve�culos com itens vencidos
	 * 
	 * @return Veiculo
	 * @author Rafael Odon
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findVencidos()
	 */
	public List<Vwveiculosvencidos> findVencidos(){	
		return veiculoDAO.findVencidos();
	}

	/**
	 * M�todo que gera relat�rio padr�o da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(VeiculoFiltro filtro) {
		Report report = new Report("/veiculo/veiculo");
		
		ListagemResult<Veiculo> veiculos = veiculoDAO.findForListagem(filtro);
		if(veiculos != null && veiculos.list().size() > 0)
			report.setDataSource(veiculos.list());
		return report;
	}
	
	/**
	 * M�todo que gerar o CSV de ve�culo
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Resource gerarRelatorioCSV(VeiculoFiltro filtro) {
		
		List<Veiculo> listaVeiculos = this.findForCSV(filtro);
		StringBuilder csv = new StringBuilder();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(listaVeiculos != null && !listaVeiculos.isEmpty()){
			csv
				.append("Patrim�nio;Km Inicial;Data Entrada;Placa;Modelo;Combust�vel;Renavam;Cor;Prefixo;Ano Fabrica��o;Ano Modelo;CHASSI;")
				.append("Prazo de Inspe��o Preventiva (DIAS);Respons�vel;Empresa;Escala;Fornecedor;Anota��es;\n");
			for(Veiculo veiculo : listaVeiculos){
				csv
					.append(veiculo.getPatrimonioitem() != null ? veiculo.getPatrimonioitem().getDescricao() : "").append(";")
					.append(veiculo.getKminicial() != null ? veiculo.getKminicial() : "").append(";")
					.append(veiculo.getDtentrada() != null ? format.format(veiculo.getDtentrada()) : "").append(";")
					.append(veiculo.getPlaca() != null ? veiculo.getPlaca() : "").append(";")
					.append(veiculo.getVeiculomodelo() != null ? veiculo.getVeiculomodelo().getNome() : "").append(";")
					.append(veiculo.getVeiculocombustivel() != null ? veiculo.getVeiculocombustivel().getDescricao() : "").append(";")
					.append(veiculo.getRenavam() != null ? ("'" + veiculo.getRenavam() + "'") : "").append(";")
					.append(veiculo.getVeiculocor() != null ? veiculo.getVeiculocor().getDescricao() : "").append(";")
					.append(veiculo.getPrefixo() != null ? veiculo.getPrefixo() : "").append(";")
					.append(veiculo.getAnofabricacao() != null ? veiculo.getAnofabricacao() : "").append(";")
					.append(veiculo.getAnomodelo() != null ? veiculo.getAnomodelo() : "").append(";")
					.append(veiculo.getChassi() != null ? ("'" + veiculo.getChassi() + "'") : "").append(";")
					.append(veiculo.getPrazopreventiva() != null ? veiculo.getPrazopreventiva() : "").append(";")
					.append(veiculo.getColaborador() != null ? veiculo.getColaborador().getNome() : "").append(";")
					.append(veiculo.getEmpresa() != null ? veiculo.getEmpresa().getRazaosocialOuNome() : "").append(";")
					.append(veiculo.getEscala() != null ? veiculo.getEscala().getNome() : "").append(";")
					.append(veiculo.getFornecedor() != null ? veiculo.getFornecedor().getNome() : "").append(";")
					.append(veiculo.getAnotacoes() != null ? ("\"" + veiculo.getAnotacoes() + "\"") : "");
				
				csv.append("\n");
			}
		}			
		
		Resource resource = new Resource("text/csv", "veiculo" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.VeiculoDAO#findForCSV(VeiculoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Veiculo> findForCSV(VeiculoFiltro filtro) {
		return veiculoDAO.findForCSV(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VeiculoDAO#updateAux
	 * 
	 * @author Rodrigo Freitas
	 */
	public void updateAux() {
		veiculoDAO.updateAux();
	}
	
	/**
	 * Carrega o ve�culo com os dados da view Vwveiculo
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @author Rafael Odon
	 */
	public Veiculo loadWithVwveiculo(Veiculo veiculo){
		return veiculoDAO.loadWithVwveiculo(veiculo);		
	}
	
	/**
	 * Carrega o ve�culo com os dados para o agendamento
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @author Rafael Odon
	 */
	public Veiculo loadForAgendamento(Veiculo veiculo){
		return veiculoDAO.loadForAgendamento(veiculo);
	}
	
	/**
	 * M�todo para gerar formul�rio de inspe��o 
	 * @author Ramon Brazil, Fernando Boldrini
	 * @param FormularioinspecaoFiltro 
	 * @return report
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findForVeiculo(filtro, order)
	 * @see br.com.linkcom.w3auto.geral.service.VwrelatorioinspecaoService#FindForRelatorio(veiculo, filtro)
	 */
	public Report createReportFormularioInspecao(FormularioinspecaoFiltro filtro) {
		
		String order;
		if(filtro.getOrdenacao().equals("cooperado")){
			order= "vwrelatorioinspecao.nomepessoa"; //order="colaborador.nome"; 
		}else if  (filtro.getOrdenacao().equals("tipo")){
			order = "vwrelatorioinspecao.nometipo"; //order="veiculotipo.descricao";
		}else if (filtro.getOrdenacao().equals("modelo")) {
			order = "vwrelatorioinspecao.nomemodelo";//order="veiculomodelo.nome";
		}else{
			order = "vwrelatorioinspecao.placa";//order ="veiculo.placa";
		}
		order += ",veiculo.cdveiculo"; //Para manter os itens agrupador por veiculo
				
		if(!filtro.getVisual()){
			filtro.setVisual(null);
		}
		
		Integer pagina = 0;
		List<FormularioInspecaoReportBean> listaFormulario = new ArrayList<FormularioInspecaoReportBean>();//Lista com resultado final do relat�rio
		//List<Veiculo> listaveiculo = this.findForVeiculo(filtro, order);
		List<Vwrelatorioinspecao> listVeiculoParaInspecao = vwrelatorioinspecaoService.findForVeiculoParaInspecao(filtro, order);//Armazena os veiculos que devem ter inspecao
		//List<Vwrelatorioinspecao> listItensParaInspecao = null;//Armazena os itens dos veiculos selecionados pelo listVeiculo
		FormularioInspecaoReportBean beanFormulario = null;
		FormularioInspecaoReportBeanItem beanItem = null;
		List<FormularioInspecaoReportBeanItem> listaItem = null;
		
		Integer aux_cdVeiculo = -1;
		
		for(Vwrelatorioinspecao vriBean : listVeiculoParaInspecao) {
			if(vriBean.getVeiculo() != null && !vriBean.getVeiculo().getCdveiculo().equals(aux_cdVeiculo)){ 
				aux_cdVeiculo = vriBean.getVeiculo().getCdveiculo();
			
				beanFormulario = new FormularioInspecaoReportBean();
				listaItem = new ArrayList<FormularioInspecaoReportBeanItem>();	
			
				beanFormulario.setCooperado(vriBean.getNomepessoa() == null ? "" : vriBean.getNomepessoa());
				beanFormulario.setKm(vriBean.getCalculakmatual());
				beanFormulario.setHorimetro(vriBean.getCalculahorimetroatual());
				beanFormulario.setPlaca(vriBean.getPlaca());
				beanFormulario.setPrefixo(vriBean.getPrefixo());
				beanFormulario.setTipocontroleveiculo(vriBean.getVeiculo() != null ? vriBean.getVeiculo().getTipocontroleveiculo() : null);
				
				beanFormulario.setListaItem(listaItem);
				listaFormulario.add(beanFormulario);
			}
			if(vriBean.getNomeiteminspecao() != null) {
				beanItem = new FormularioInspecaoReportBeanItem();
				beanItem.setDescricaoitens(vriBean.getNomeiteminspecao());
				beanItem.setDtmanutencao(vriBean.getDtmanutencao());
				beanItem.setKminspecao(vriBean.getKminspecao());
				beanItem.setHorimetroinspecao(vriBean.getHorimetroinspecao());
				beanItem.setTipoitens(vriBean.getNometipoiteminspecao());
				beanFormulario.setTipocontroleveiculo(vriBean.getVeiculo() != null ? vriBean.getVeiculo().getTipocontroleveiculo() : null);
				
				listaItem.add(beanItem);			
			}
		}
		
		/*
		//List<Vwrelatorioinspecao> relatorioInspecao= null;
		//FormularioInspecaoReportBean beanFormulario = null;
		//FormularioInspecaoReportBeanItem beanItem = null;
		
		
		if(!listVeiculo.isEmpty()) {
			whereIn.deleteCharAt(whereIn.length() - 1);//Retira a ultima virgula
			listItensParaInspecao = vwrelatorioinspecaoService.findItensParaInspecao(new String(whereIn));
		}
		
		
		//List<FormularioInspecaoReportBeanItem> listaItem =null;
		
		for (Veiculo veiculo:listaveiculo) {
			beanFormulario= new FormularioInspecaoReportBean();
			beanFormulario.setCooperado(veiculo.getColaborador() != null ? veiculo.getColaborador().getNome() : "");
			beanFormulario.setPlaca(veiculo.getPlaca());
			beanFormulario.setPrefixo(veiculo.getPrefixo());
			beanFormulario.setKm(veiculo.getVwveiculo().getKmatual());
			listaItem = new ArrayList<FormularioInspecaoReportBeanItem>();	
			relatorioInspecao = vwrelatorioinspecaoService.findForRelatorio(veiculo, filtro);
			
			for (Vwrelatorioinspecao vwrelatorioinspecao:relatorioInspecao) {
				beanItem = new FormularioInspecaoReportBeanItem();
				beanItem.setTipoitens(vwrelatorioinspecao.getNometipoiteminspecao());
				beanItem.setDescricaoitens(vwrelatorioinspecao.getNomeiteminspecao());
				beanItem.setKminspecao(vwrelatorioinspecao.getKminspecao());
				beanItem.setDtmanutencao(vwrelatorioinspecao.getDtmanutencao());
				listaItem.add(beanItem);	
			}
			if(relatorioInspecao != null && !relatorioInspecao.isEmpty()) {
				beanFormulario.setListaItem(listaItem);	
				listaFormulario.add(beanFormulario);
			}
		}
		*/
		if(filtro.getVisual()==null){
			filtro.setVisual(Boolean.FALSE);
		}
		
		
		Report report= new Report("veiculo/relInspecaoVeicular");
		report.addParameter("VISUAL",filtro.getVisual());
		report.addParameter("PREVENTIVA", filtro.getVisual() ? false : true );
		report.addParameter("PAGINA", pagina);
		report.setDataSource(listaFormulario);
		report.addSubReport("SUBFOMULARIOINSPECAO", new Report("veiculo/relInspecaoVeicularSub"));	 
		
		try {
			Image image = null;
			URL arqImagem = new URL(SinedUtil.getUrlWithContext() + "/imagens/veiculo/relatorio/inspecao_visual.gif");
			image = ImageIO.read(arqImagem.openStream());
			report.addParameter("IMAGEMINSPECAOVISUAL", image);
		} catch (Exception e) {}
			
		return report;
	}
	
	/**
	 * M�todo que lista todos os veiculos de acordo com filtro
	 * 
	 * @author Ramon Brazil
	 * @param filtro, order
	 * @return List <Veiculo>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findForVeiculo(filtro, order)
	 */
	public List <Veiculo> findForVeiculo(FormularioinspecaoFiltro filtro,String order){
		return veiculoDAO.findForVeiculo(filtro, order);
	}
	
	/**
	 * <b>M�todo de refer�ncia ao DAO</b>
	 * 
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findVeiculosAgendados(BuscarVeiculosSemInspecaoFiltro)
	 * @return List<Vwveiculosseminspecao2>
	 * @param  BuscarVeiculosSemInspecaoFiltro
	 * @author Jo�o Paulo Zica
	 */
	public List<Vwveiculosseminspecao3> findVeiculosAgendados(BuscarVeiculosSemInspecaoFiltro filtro){
		return veiculoDAO.findVeiculosAgendados(filtro);									
	}
	
	
	/**
	 * faz referencia ao DAO
	 * @author Rafael Odon
	 * @param filtro
	 * @return
	 */
	public List<Vwveiculoseminspecaovencido3> findForAgendamento(AgendarVeiculoInspecaoFiltro filtro){
		return veiculoDAO.findForAgendamento(filtro);
	}
	
	/**
	 * Carrega o ve�culo a lista dos seus �tens vencidos
	 * 
	 * @param veiculo
	 * @return Veiculo
	 * @author Rafael Odon
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findComItensVencidos(veiculo)

	 */
	public Veiculo findComItensVecidos(Veiculo veiculo){						
		return veiculoDAO.findComItensVencidos(veiculo);
	}

	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Veiculo getEscalaVeiculo(Veiculo veiculo) {
		return veiculoDAO.getEscalaVeiculo(veiculo);
	}
	
	/**
	 * M�todo que retorna lista de todos itens para o FLEX
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculo> findAllForFlex(){
		return veiculoDAO.findForComboFlex("");
	}
	
	/* singleton */
	private static VeiculoService instance;
	public static VeiculoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VeiculoService.class);
		}
		return instance;
	}
	
	public List<Veiculo>findForAutocomplete(String q){
		return veiculoDAO.findForAutocomplete(q);
	}
	
	/**
	 * M�todo para validar a situa��o dos itens de inspe��o do ve�culo.
	 * 
	 * @param veiculo
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Integer validaVeiculoItemInspecaoSituacao(Veiculo veiculo, HashMap<Integer, List<Categoriaiteminspecao>> itemInspecaoCategoriaMap, List<Categoriaveiculo> listaItemInspecaoCategoria) {
		StringBuilder whereIn = null;
		HashMap<Integer, Integer> mapaItemInspecaoOrdemManutencao = new HashMap<Integer, Integer>();
		Veiculoordemservico ultimaOrdemInspecao = veiculo != null ? veiculoordemservicoServico.findByVeiculo(veiculo) : null;
		List<Veiculoordemservico> listaUltimaOrdemServico = null;
		
		Integer tipoVencimento = 0;

		List<Categoriaiteminspecao> listaCategoriaItemInspecao = null;
		
		if (veiculo != null && veiculo.getVeiculomodelo() != null && veiculo.getVeiculomodelo().getCategoriaveiculo() != null && veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo() != null) {
			listaCategoriaItemInspecao = itemInspecaoCategoriaMap.get(veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo());
		}
		
		if (SinedUtil.isListNotEmpty(listaCategoriaItemInspecao)) {
			whereIn = new StringBuilder();
			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
				if (categoriaiteminspecao != null && categoriaiteminspecao.getInspecaoitem() != null && categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem() != null) {
					whereIn.append(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem().toString()+ ",");
				}
			}
		}
		
		
		if (whereIn != null && whereIn.length() > 0) {
			 listaUltimaOrdemServico = veiculoordemservicoServico.findUltimaManutencaoByVeiculoAndItemInspecao(veiculo, whereIn.deleteCharAt(whereIn.length() - 1).toString());
		}

		if (SinedUtil.isListNotEmpty(listaUltimaOrdemServico)) {
			for (Veiculoordemservico veiculoordemservico : listaUltimaOrdemServico) {
				if (veiculoordemservico != null && veiculoordemservico.getListaVeiculoordemservicoitem() != null) {
					for (Veiculoordemservicoitem veiculoordemservicoitem : veiculoordemservico.getListaVeiculoordemservicoitem()) {
						mapaItemInspecaoOrdemManutencao.put(veiculoordemservicoitem.getInspecaoitem().getCdinspecaoitem(),listaUltimaOrdemServico.indexOf(veiculoordemservico));
					}
				}
			}
		}

		if (SinedUtil.isListNotEmpty(listaCategoriaItemInspecao)) {

			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
				
				
				
				if(SinedUtil.isListNotEmpty(listaUltimaOrdemServico) 
						&& mapaItemInspecaoOrdemManutencao != null 
						&& !mapaItemInspecaoOrdemManutencao.isEmpty() 
						&& mapaItemInspecaoOrdemManutencao.get(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem()) != null){
					
					Integer indiceListaUltimaOrdemServico = mapaItemInspecaoOrdemManutencao.get(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem());
					
					if ((Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && 
							this.validaItemInspecaoManutencaoOdometro(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																	  veiculo, 
																	  listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																	  categoriaiteminspecao, 
																	  veiculo.getListaVeiculoKm().get(0))) ||
						(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && 
							this.validaItemInspecaoManutencaoOdometro(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																	  veiculo, 
																	  listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																	  categoriaiteminspecao, 
																	  veiculo.getListaVeiculohorimetro().get(0)))
						) {
						tipoVencimento = 3;
					}  
					if(tipoVencimento < 1 && (
							(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && 
									this.validaItemInspecaoManutencaoMargemAndVencimento(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																						 veiculo, 
																						 listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																						 categoriaiteminspecao, 
																						 veiculo.getListaVeiculoKm().get(0))) ||
						 (Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && 
									this.validaItemInspecaoManutencaoMargemAndVencimento(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																						 veiculo, 
																						 listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																						 categoriaiteminspecao, 
																						 veiculo.getListaVeiculohorimetro().get(0)))
							)){
						tipoVencimento = 1;
					} 
					if(tipoVencimento < 2 && (
							(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && 
									this.validaItemInspecaoManutencaoMaiorIgualVencimento(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																						  veiculo, 
																						  listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																						  categoriaiteminspecao, 
																						  veiculo.getListaVeiculoKm().get(0))) ||
							(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && 
									this.validaItemInspecaoManutencaoMaiorIgualVencimento(listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico).getListaVeiculoordemservicoitem().get(0), 
																						  veiculo, 
																						  listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico), 
																						  categoriaiteminspecao, 
																						  veiculo.getListaVeiculohorimetro().get(0)))
							)){
						tipoVencimento = 2;
					}
				} else if (ultimaOrdemInspecao != null){
					if(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculoKm())){
						if (this.validaItemInspecaoOdometro(veiculo, ultimaOrdemInspecao, veiculo.getListaVeiculoKm().get(0))) {
							tipoVencimento = 3;
						}  
						if(tipoVencimento < 1 && this.validaItemInspecaoMargemAndVencimento(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculoKm().get(0))){
							tipoVencimento = 1;
						} 
						if(tipoVencimento < 2 && this.validaItemInspecaoMaiorIgualVencimento(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculoKm().get(0))){
							tipoVencimento = 2;
						}						
					}else if(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculohorimetro())){
						if (this.validaItemInspecaoOdometro(veiculo, ultimaOrdemInspecao, veiculo.getListaVeiculohorimetro().get(0))) {
							tipoVencimento = 3;
						}  
						if(tipoVencimento < 1 && this.validaItemInspecaoMargemAndVencimento(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculohorimetro().get(0))){
							tipoVencimento = 1;
						} 
						if(tipoVencimento < 2 && this.validaItemInspecaoMaiorIgualVencimento(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculohorimetro().get(0))){
							tipoVencimento = 2;
						}						
					} 
				}
			}
		}
		return tipoVencimento;
	}
	
	/**
	 * M�todo para validar se algum item de inspe��o do ve�culo est� vencido
	 * 
	 * @param veiculo
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	
	public Boolean validaVeiculoItemInspecaoVencido(
			Veiculo veiculo,
			HashMap<Integer, List<Categoriaiteminspecao>> itemInspecaoCategoriaMap,
			List<Categoriaveiculo> listaItemInspecaoCategoria) {
		Boolean isInspecaoVencida = false;
		List<Categoriaiteminspecao> listaCategoriaItemInspecao = null;
		StringBuilder whereIn = null;
		HashMap<Integer, Integer> mapaItemInspecaoOrdemManutencao = new HashMap<Integer, Integer>();
		Veiculoordemservico ultimaOrdemInspecao = veiculo != null ? veiculoordemservicoServico.findByVeiculo(veiculo) : null;
		List<Veiculoordemservico> listaUltimaOrdemServico = null;

		if (veiculo != null
				&& veiculo.getVeiculomodelo() != null
				&& veiculo.getVeiculomodelo().getCategoriaveiculo() != null
				&& veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo() != null) {
			
			listaCategoriaItemInspecao = itemInspecaoCategoriaMap.get(veiculo.getVeiculomodelo().getCategoriaveiculo().getCdcategoriaveiculo());
		}

		if (SinedUtil.isListNotEmpty(listaCategoriaItemInspecao)) {
			whereIn = new StringBuilder();
			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {
				if (categoriaiteminspecao != null
						&& categoriaiteminspecao.getInspecaoitem() != null
						&& categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem() != null) {
					whereIn.append(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem().toString()+ ",");
				}
			}
		}

		if (whereIn != null && whereIn.length() > 0) {
			listaUltimaOrdemServico = veiculoordemservicoServico.findUltimaManutencaoByVeiculoAndItemInspecao(veiculo, whereIn.deleteCharAt(whereIn.length() - 1).toString());
		}
		

		if (SinedUtil.isListNotEmpty(listaUltimaOrdemServico)) {
			for (Veiculoordemservico veiculoordemservico : listaUltimaOrdemServico) {
				if (veiculoordemservico != null && veiculoordemservico.getListaVeiculoordemservicoitem() != null) {
					for (Veiculoordemservicoitem veiculoordemservicoitem : veiculoordemservico.getListaVeiculoordemservicoitem()) {
						mapaItemInspecaoOrdemManutencao.put(veiculoordemservicoitem.getInspecaoitem().getCdinspecaoitem(),listaUltimaOrdemServico.indexOf(veiculoordemservico));
					}
				}
			}
		}

		if (SinedUtil.isListNotEmpty(listaCategoriaItemInspecao) && veiculo != null) {

			for (Categoriaiteminspecao categoriaiteminspecao : listaCategoriaItemInspecao) {

				if(SinedUtil.isListNotEmpty(listaUltimaOrdemServico) 
						&& mapaItemInspecaoOrdemManutencao != null 
						&& !mapaItemInspecaoOrdemManutencao.isEmpty() 
						&& mapaItemInspecaoOrdemManutencao.get(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem()) != null){
					
					Integer indiceListaUltimaOrdemServico = mapaItemInspecaoOrdemManutencao.get(categoriaiteminspecao.getInspecaoitem().getCdinspecaoitem());
					if(indiceListaUltimaOrdemServico != null){
						Veiculoordemservico veiculoordemservico = listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico);
						if(veiculoordemservico != null && SinedUtil.isListNotEmpty(veiculoordemservico.getListaVeiculoordemservicoitem()) &&
								(
									(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculoKm())) ||
									(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculohorimetro()))
								)
								){
							if(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo())){
								isInspecaoVencida = validaItemInspecaoManutencao(
										veiculoordemservico.getListaVeiculoordemservicoitem().get(0),
										veiculo, 
										listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico),
										categoriaiteminspecao, 
										veiculo.getListaVeiculoKm().get(0));
							}else if(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo())){
								isInspecaoVencida = validaItemInspecaoManutencao(
										veiculoordemservico.getListaVeiculoordemservicoitem().get(0),
										veiculo, 
										listaUltimaOrdemServico.get(indiceListaUltimaOrdemServico),
										categoriaiteminspecao, 
										veiculo.getListaVeiculohorimetro().get(0));
							} 
						}
					}
				} else if(ultimaOrdemInspecao != null && 
						(
							(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculoKm())) ||
							(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo()) && SinedUtil.isListNotEmpty(veiculo.getListaVeiculohorimetro()))
						)
						){
					if(Tipocontroleveiculo.HODOMETRO.equals(veiculo.getTipocontroleveiculo())){
						isInspecaoVencida = validaItemInspecao(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculoKm().get(0));
					}else if(Tipocontroleveiculo.HORIMETRO.equals(veiculo.getTipocontroleveiculo())){
						isInspecaoVencida = validaItemInspecao(veiculo, categoriaiteminspecao, ultimaOrdemInspecao, veiculo.getListaVeiculohorimetro().get(0));
					} 
				}
	
				if (isInspecaoVencida) {
					return isInspecaoVencida;
				}
			}
		}
		return isInspecaoVencida;
	}

	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de manuten��o est� com odometro menor que o registro de manutencao.
	 * 
	 * @param veiculoOrdemServicoManutencaoItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoManutencao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoManutencaoOdometro(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoOdometroOk = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null && (ultimoKmVeiculo < ultimaOrdemServicoManutencao.getKm())){
				isItemInspecaoOdometroOk = true;
			}
		return isItemInspecaoOdometroOk;
	}
	
	public Boolean validaItemInspecaoManutencaoOdometro(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoOdometroOk = false;

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null && (ultimoHorimetroVeiculo < ultimaOrdemServicoManutencao.getHorimetro())){
				isItemInspecaoOdometroOk = true;
			}
		return isItemInspecaoOdometroOk;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de inspecao est� com odometro menor que o registro de inspecao.
	 * 
	 * @see PatrimonioitemService#findPatrimonioItemByPlaqueta(String)
	 * @param listaCategoriaItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoInspecao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoOdometro(Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoOdometroOk = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
	
			if (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null && (ultimoKmVeiculo < ultimaOrdemServicoInspecao.getKm())){
				isItemInspecaoOdometroOk = true;
			}
		return isItemInspecaoOdometroOk;
	}
	
	public Boolean validaItemInspecaoOdometro(Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculohorimetro veiculohorimetro){
		Boolean isItemInspecaoOdometroOk = false;
		Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;

		if (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null && (ultimoHorimetroVeiculo < ultimaOrdemServicoInspecao.getHorimetro())){
			isItemInspecaoOdometroOk = true;
		}
		return isItemInspecaoOdometroOk;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de manuten��o est� entre a margem e vencimento.
	 * 
	 * @param veiculoOrdemServicoManutencaoItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoManutencao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoManutencaoMargemAndVencimento(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoEntreMargemVencimento = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0;
			}

			if (
					(	
						(categoriaiteminspecao != null && categoriaiteminspecao.getMargeminspecao() != null && kmAtual >= categoriaiteminspecao.getMargeminspecao()) 
						&& 
						(categoriaiteminspecao != null && categoriaiteminspecao.getVenckm() != null && kmAtual < categoriaiteminspecao.getVenckm())
					)
					|| 
					(
						(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao != null && categoriaiteminspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) < categoriaiteminspecao.getVencperiodo())
						&&
						(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao != null && categoriaiteminspecao.getMargeminspecaodias() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias())
					)

			) {
				isItemInspecaoEntreMargemVencimento = true;
			}
		return isItemInspecaoEntreMargemVencimento;
	}
	
	public Boolean validaItemInspecaoManutencaoMargemAndVencimento(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoEntreMargemVencimento = false;

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoHorimetroVeiculo > 0) {
				horimetroAtual = ultimoHorimetroVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0d);
			} else {
				horimetroAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0;
			}

			if (
					(	
						(categoriaiteminspecao != null && categoriaiteminspecao.getMargeminspecaohorimetro() != null && horimetroAtual >= categoriaiteminspecao.getMargeminspecaohorimetro()) 
						&& 
						(categoriaiteminspecao != null && categoriaiteminspecao.getVenchorimetro() != null && horimetroAtual < categoriaiteminspecao.getVenchorimetro())
					)
					|| 
					(
						(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao != null && categoriaiteminspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) < categoriaiteminspecao.getVencperiodo())
						&&
						(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao != null && categoriaiteminspecao.getMargeminspecaodias() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias())
					)

			) {
				isItemInspecaoEntreMargemVencimento = true;
			}
		return isItemInspecaoEntreMargemVencimento;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de inspecao est� entre a margem e vencimento.
	 * 
	 * @param listaCategoriaItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoInspecao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoMargemAndVencimento(Veiculo veiculo, Categoriaiteminspecao categoriaItemInspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoEntreMargemVencimento = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0;
			}
		
			if (
					
					(		
						(categoriaItemInspecao != null && categoriaItemInspecao.getMargeminspecao() != null && kmAtual >= categoriaItemInspecao.getMargeminspecao())
						&& 
						(categoriaItemInspecao != null && categoriaItemInspecao.getVenckm() != null && kmAtual < categoriaItemInspecao.getVenckm())
					)
					|| 
					(
						(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) < categoriaItemInspecao.getVencperiodo())
						&&
						(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getMargeminspecaodias() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaItemInspecao.getMargeminspecaodias())
					)
					
				){
				
				isItemInspecaoEntreMargemVencimento = true;
			}
			
		return isItemInspecaoEntreMargemVencimento;
	}
	
	public Boolean validaItemInspecaoMargemAndVencimento(Veiculo veiculo, Categoriaiteminspecao categoriaItemInspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoEntreMargemVencimento = false;

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoHorimetroVeiculo > 0) {
				horimetroAtual = ultimoHorimetroVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0);
			} else {
				horimetroAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0;
			}
		
			if (
					
					(		
						(categoriaItemInspecao != null && categoriaItemInspecao.getMargeminspecaohorimetro() != null && horimetroAtual >= categoriaItemInspecao.getMargeminspecaohorimetro())
						&& 
						(categoriaItemInspecao != null && categoriaItemInspecao.getVenchorimetro() != null && horimetroAtual < categoriaItemInspecao.getVenchorimetro())
					)
					|| 
					(
						(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) < categoriaItemInspecao.getVencperiodo())
						&&
						(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getMargeminspecaodias() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaItemInspecao.getMargeminspecaodias())
					)
					
				){
				
				isItemInspecaoEntreMargemVencimento = true;
			}
			
		return isItemInspecaoEntreMargemVencimento;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de manuten��o est� vencido
	 * 
	 * @param veiculoOrdemServicoManutencaoItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoManutencao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoManutencao(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoVencida = false;

		if (!veiculoOrdemServicoManutencaoItemInspecao.getStatus()) {
			isItemInspecaoVencida = true;

		} else {

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0;
			}

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao !=null && categoriaiteminspecao.getVenckm() != null && (kmAtual > categoriaiteminspecao.getVenckm())) 
					|| 
					(categoriaiteminspecao !=null && categoriaiteminspecao.getMargeminspecao() != null && (kmAtual >= categoriaiteminspecao.getMargeminspecao()))
					||
					((ultimaOrdemServicoManutencao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && categoriaiteminspecao.getVencperiodo() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) > categoriaiteminspecao.getVencperiodo()))
					||
					((ultimaOrdemServicoManutencao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecaodias() != null && categoriaiteminspecao.getMargeminspecaodias() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias()))
			){
				isItemInspecaoVencida = true;
			}
		}
		return isItemInspecaoVencida;
	}
	
	public Boolean validaItemInspecaoManutencao(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoVencida = false;

		if (!veiculoOrdemServicoManutencaoItemInspecao.getStatus()) {
			isItemInspecaoVencida = true;

		} else {

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoHorimetroVeiculo > 0) {
				horimetroAtual = ultimoHorimetroVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0d);
			} else {
				horimetroAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0d;
			}

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao !=null && categoriaiteminspecao.getVenchorimetro() != null && (horimetroAtual > categoriaiteminspecao.getVenchorimetro())) 
					|| 
					(categoriaiteminspecao !=null && categoriaiteminspecao.getMargeminspecaohorimetro() != null && (horimetroAtual >= categoriaiteminspecao.getMargeminspecaohorimetro()))
					||
					((ultimaOrdemServicoManutencao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && categoriaiteminspecao.getVencperiodo() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) > categoriaiteminspecao.getVencperiodo()))
					||
					((ultimaOrdemServicoManutencao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecaodias() != null && categoriaiteminspecao.getMargeminspecaodias() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias()))
			){
				isItemInspecaoVencida = true;
			}
		}
		return isItemInspecaoVencida;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de inspecao est� vencido
	 * 
	 * @param listaCategoriaItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoInspecao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecao(Veiculo veiculo, Categoriaiteminspecao categoriaiteminspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoVencida = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0;
			}
			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao!=null && categoriaiteminspecao.getVenckm() != null && (kmAtual > categoriaiteminspecao.getVenckm())) 
					|| 
					(categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecao() != null && (kmAtual >= categoriaiteminspecao.getMargeminspecao()))
					||
					((ultimaOrdemServicoInspecao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && categoriaiteminspecao.getVencperiodo() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) > categoriaiteminspecao.getVencperiodo()))
					||
					((ultimaOrdemServicoInspecao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecaodias() != null && categoriaiteminspecao.getMargeminspecaodias() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias()))
			){
				isItemInspecaoVencida = true;
			}
		return isItemInspecaoVencida;
	}
	
	public Boolean validaItemInspecao(Veiculo veiculo, Categoriaiteminspecao categoriaiteminspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoVencida = false;

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoHorimetroVeiculo > 0) {
				horimetroAtual = ultimoHorimetroVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0d);
			} else {
				horimetroAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0d;
			}
			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao!=null && categoriaiteminspecao.getVenchorimetro() != null && (horimetroAtual > categoriaiteminspecao.getVenchorimetro())) 
					|| 
					(categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecaohorimetro() != null && (horimetroAtual >= categoriaiteminspecao.getMargeminspecaohorimetro()))
					||
					((ultimaOrdemServicoInspecao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && categoriaiteminspecao.getVencperiodo() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) > categoriaiteminspecao.getVencperiodo()))
					||
					((ultimaOrdemServicoInspecao != null && categoriaiteminspecao!=null && categoriaiteminspecao.getMargeminspecaodias() != null && categoriaiteminspecao.getMargeminspecaodias() > 0) && (SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaiteminspecao.getMargeminspecaodias()))
			){
				isItemInspecaoVencida = true;
			}
		return isItemInspecaoVencida;
	}

	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de manuten��o est� maior ou igual ao vencimento.
	 * 
	 * @param veiculoOrdemServicoManutencaoItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoManutencao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoManutencaoMaiorIgualVencimento(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoMaiorIgualVencimento = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getKm() != null ? ultimaOrdemServicoManutencao.getKm() : 0;
			}

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao!=null && categoriaiteminspecao.getVenckm() != null && kmAtual >= categoriaiteminspecao.getVenckm()) 
					|| 
					(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getVencperiodo())
					
			){
				isItemInspecaoMaiorIgualVencimento = true;
			}

		return isItemInspecaoMaiorIgualVencimento;
	}
	
	public Boolean validaItemInspecaoManutencaoMaiorIgualVencimento(Veiculoordemservicoitem veiculoOrdemServicoManutencaoItemInspecao, 
			Veiculo veiculo, Veiculoordemservico ultimaOrdemServicoManutencao, Categoriaiteminspecao categoriaiteminspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoMaiorIgualVencimento = false;

			Double ultimoHorimetroVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoHorimetroVeiculo > 0) {
				horimetroAtual = ultimoHorimetroVeiculo - (ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0d);
			} else {
				horimetroAtual =  ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getHorimetro() != null ? ultimaOrdemServicoManutencao.getHorimetro() : 0d;
			}

			//Valida se o ve�culo tem algum item de inspecao vencido
			if (
					(categoriaiteminspecao!=null && categoriaiteminspecao.getVenchorimetro() != null && horimetroAtual >= categoriaiteminspecao.getVenchorimetro()) 
					|| 
					(ultimaOrdemServicoManutencao != null && ultimaOrdemServicoManutencao.getDtrealizada() != null && categoriaiteminspecao!=null && categoriaiteminspecao.getVencperiodo() != null && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoManutencao.getDtrealizada()) >= categoriaiteminspecao.getVencperiodo())
					
			){
				isItemInspecaoMaiorIgualVencimento = true;
			}

		return isItemInspecaoMaiorIgualVencimento;
	}
	
	/**
	 * M�todo para validar se o item de inspe��o de uma O.S. de inspecao est� maior ou igual ao vencimento.
	 * 
	 * @param listaCategoriaItemInspecao
	 * @param veiculo
	 * @param ultimaOrdemServicoInspecao
	 * @return Boolean
	 * @author Jo�o Vitor
	 */		
	public Boolean validaItemInspecaoMaiorIgualVencimento(Veiculo veiculo, Categoriaiteminspecao categoriaItemInspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculokm veiculokm){

		Boolean isItemInspecaoMaiorIgualVencimento = false;

			Long ultimoKmVeiculo = veiculokm != null && veiculokm.getKmnovo() != null ? veiculokm.getKmnovo().longValue() : 0;
			Long kmAtual;

			if (ultimoKmVeiculo > 0) {
				kmAtual = ultimoKmVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0);
			} else {
				kmAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getKm() != null ? ultimaOrdemServicoInspecao.getKm() : 0;
			}
		
			if (
					(categoriaItemInspecao != null && categoriaItemInspecao.getVenckm() != null && kmAtual >= categoriaItemInspecao.getVenckm()) 
					|| 
					(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getVencperiodo() != null  && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaItemInspecao.getVencperiodo())
					
			){
				isItemInspecaoMaiorIgualVencimento = true;
			}
		
		return isItemInspecaoMaiorIgualVencimento;
	}
	
	public Boolean validaItemInspecaoMaiorIgualVencimento(Veiculo veiculo, Categoriaiteminspecao categoriaItemInspecao, Veiculoordemservico ultimaOrdemServicoInspecao, Veiculohorimetro veiculohorimetro){

		Boolean isItemInspecaoMaiorIgualVencimento = false;

			Double ultimoKmVeiculo = veiculohorimetro != null && veiculohorimetro.getHorimetronovo() != null ? veiculohorimetro.getHorimetronovo() : 0d;
			Double horimetroAtual;

			if (ultimoKmVeiculo > 0) {
				horimetroAtual = ultimoKmVeiculo - (ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0d);
			} else {
				horimetroAtual =  ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getHorimetro() != null ? ultimaOrdemServicoInspecao.getHorimetro() : 0d;
			}
		
			if (
					(categoriaItemInspecao != null && categoriaItemInspecao.getVenchorimetro() != null && horimetroAtual >= categoriaItemInspecao.getVenchorimetro()) 
					|| 
					(ultimaOrdemServicoInspecao != null && ultimaOrdemServicoInspecao.getDtrealizada() != null && categoriaItemInspecao != null && categoriaItemInspecao.getVencperiodo() != null  && SinedDateUtils.diferencaDias(new Date(), ultimaOrdemServicoInspecao.getDtrealizada()) >= categoriaItemInspecao.getVencperiodo())
					
			){
				isItemInspecaoMaiorIgualVencimento = true;
			}
		
		return isItemInspecaoMaiorIgualVencimento;
	}
	
	/**
	 * M�todo que retorna lista de ve�culos para valida��o de vencimento dos itens de categoria
	 * 
	 * @author Jo�o Vitor
	 * @return List<Veiculo>
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findVeiculoForValidaItemCategoriaVencido()
	 */
	public List<Veiculo> findVeiculoListForValidaItemCategoriaVencido(){
		return veiculoDAO.findVeiculoListForValidaItemCategoriaVencido();
	}
	
	/**
	 * M�todo que retorna ve�culo para valida��o de vencimento dos itens de categoria
	 * 
	 * @param veiculo
	 * @author Jo�o Vitor
	 * @return Veiculo
	 * @see br.com.linkcom.w3auto.geral.dao.VeiculoDAO#findVeiculoForValidaItemCategoriaVencido(Veiculo veiculo)
	 */
	public Veiculo findVeiculoForValidaItemCategoriaVencido(Veiculo veiculo){
		return veiculoDAO.findVeiculoForValidaItemCategoriaVencido(veiculo);
	}

	/**
	 * M�todo que busca os implementos para o autocomplete no detalhe de veiculoimplemento na tela de entrada de veiculouso
	 * 
	 * @param param
	 * @return
	 * @since 30/06/2017
	 * @author Rafael Salvio
	 */
	public List<Veiculo> findImplementosForAutocomplete(String param){
		return veiculoDAO.findImplementosForAutocomplete(param);
	}

	public void processarListagem(ListagemResult<Veiculo> listagemResult) {
		List<Veiculo> listaVeiculo = listagemResult.list();
		
		for (Veiculo veiculo: listaVeiculo){
			Veiculouso ultimoUso = veiculousoService.getUltimoUso(veiculo);
			if (ultimoUso != null){
				veiculo.setUltimoProjeto(ultimoUso.getProjeto());
			}
		}
	}
	
	@Override
	protected ListagemResult<Veiculo> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Veiculo> listagemResult = veiculoDAO.findForExportacao(filtro);
		processarListagem(listagemResult);
		
		return listagemResult;
	}
}
