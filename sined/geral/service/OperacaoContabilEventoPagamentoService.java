package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OperacaoContabilEventoPagamento;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.dao.OperacaoContabilEventoPagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OperacaoContabilEventoPagamentoService extends GenericService<OperacaoContabilEventoPagamento>{

	private OperacaoContabilEventoPagamentoDAO operacaoContabilEventoPagamentoDAO;
	
	public void setOperacaoContabilEventoPagamentoDAO(
			OperacaoContabilEventoPagamentoDAO operacaoContabilEventoPagamentoDAO) {
		this.operacaoContabilEventoPagamentoDAO = operacaoContabilEventoPagamentoDAO;
	}
	
	public List<OperacaoContabilEventoPagamento> findByOperacaoContabil(Operacaocontabil operacaoContabil){
		return operacaoContabilEventoPagamentoDAO.findByOperacaoContabil(operacaoContabil);
	}
}
