package br.com.linkcom.sined.geral.service;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.dao.ModelodocumentofiscalDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ModelodocumentofiscalService extends GenericService<Modelodocumentofiscal> {

	private ModelodocumentofiscalDAO modelodocumentofiscalDAO;
	
	public void setModelodocumentofiscalDAO(
			ModelodocumentofiscalDAO modelodocumentofiscalDAO) {
		this.modelodocumentofiscalDAO = modelodocumentofiscalDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param codigo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadByCodigo(String codigo) {
		return modelodocumentofiscalDAO.loadByCodigo(codigo);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadNotafiscalservicoeletronica() {
		return modelodocumentofiscalDAO.loadNotafiscalservicoeletronica();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/11/2014
	 */
	public Modelodocumentofiscal loadNotafiscalprodutoeletronica() {
		return modelodocumentofiscalDAO.loadNotafiscalprodutoeletronica();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rafael Salvio
	 */
	public Modelodocumentofiscal loadConhecimentotransporteeletronico() {
		return modelodocumentofiscalDAO.loadConhecimentotransporteeletronico();
	}

	public ModelAndView buscaInformacoesModelo(Modelodocumentofiscal modelodocumentofiscal) {
		if (modelodocumentofiscal != null && modelodocumentofiscal.getCdmodelodocumentofiscal() != null){
			modelodocumentofiscal = load(modelodocumentofiscal, "modelodocumentofiscal.cdmodelodocumentofiscal, modelodocumentofiscal.codigo");
		}
		return new JsonModelAndView().addObject("modelodocumentofiscal", modelodocumentofiscal);
	}
}