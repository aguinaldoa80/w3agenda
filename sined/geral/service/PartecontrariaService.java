package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Partecontraria;
import br.com.linkcom.sined.geral.dao.PartecontrariaDAO;
import br.com.linkcom.sined.modulo.juridico.controller.crud.filter.PartecontrariaFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PartecontrariaService extends GenericService<Partecontraria> {
	
	private PartecontrariaDAO partecontrariaDAO;
	
	public void setPartecontrariaDAO(PartecontrariaDAO partecontrariaDAO) {
		this.partecontrariaDAO = partecontrariaDAO;
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem de acordo com o filtro
	 * 
	 * @param filtro
	 * @auhtor Thiago Clemente
	 * 
	 */
	public IReport gerarRelatorioListagem(PartecontrariaFiltro filtro) {
		
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Partecontraria> listaListagemResult = partecontrariaDAO.findForListagem(filtro);
		List<Partecontraria> listaPartecontraria = listaListagemResult.list();
		
		Report report = new Report("/juridico/partecontraria");
		report.setDataSource(listaPartecontraria);
		
		return report;
	}	
}