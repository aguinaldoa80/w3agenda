package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.CampoExtraSegmento;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.dao.CampoExtraSegmentoDAO;

public class CampoExtraSegmentoService extends GenericService<CampoExtraSegmento> {
	
	private CampoExtraSegmentoDAO campoExtraSegmentoDAO;
	
	public void setCampoExtraSegmentoDAO(CampoExtraSegmentoDAO campoExtraSegmentoDAO) {
		this.campoExtraSegmentoDAO = campoExtraSegmentoDAO;
	}

	public List<CampoExtraSegmento> findBySegmento(Segmento segmento) {
		return campoExtraSegmentoDAO.findBySegmento(segmento);
	}

}
