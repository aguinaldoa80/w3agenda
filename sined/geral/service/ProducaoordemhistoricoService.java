package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.dao.ProducaoordemhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemhistoricoService extends GenericService<Producaoordemhistorico> {

	private ProducaoordemhistoricoDAO producaoordemhistoricoDAO;
	
	public void setProducaoordemhistoricoDAO(
			ProducaoordemhistoricoDAO producaoordemhistoricoDAO) {
		this.producaoordemhistoricoDAO = producaoordemhistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemhistoricoDAO#findByProducaoordem(Producaoordem producaoordem)
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordemhistorico> findByProducaoordem(Producaoordem producaoordem) {
		return producaoordemhistoricoDAO.findByProducaoordem(producaoordem);
	}

}
