package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.FornecimentoDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.FornecimentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

public class FornecimentoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Fornecimento> {

	private FornecimentoDAO fornecimentoDAO;
	private DocumentoorigemService documentoorigemService;
	private FornecimentocontratoService fornecimentocontratoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	private ContapagarService contapagarService;
	private EmpresaService empresaService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setFornecimentoDAO(FornecimentoDAO fornecimentoDAO) {
		this.fornecimentoDAO = fornecimentoDAO;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setFornecimentocontratoService(
			FornecimentocontratoService fornecimentocontratoService) {
		this.fornecimentocontratoService = fornecimentocontratoService;
	}
	public void setPrazopagamentoitemService(
			PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecimentoDAO#findForFaturar
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecimento> findForFaturar(String whereIn) {
		return fornecimentoDAO.findForFaturar(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecimentoDAO#findForRegistrarEntradafiscal(Fornecimento fornecimento)
	 *
	 * @param fornecimento
	 * @return
	 * @author Luiz Fernando
	 */
	public Fornecimento findForRegistrarEntradafiscal(Fornecimento fornecimento) {
		return fornecimentoDAO.findForRegistrarEntradafiscal(fornecimento);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.service.FornecimentoService#updateBaixado
	 * @param fornecimento
	 * @author Rodrigo Freitas
	 */
	public void updateBaixado(Fornecimento fornecimento) {
		if(fornecimento != null && fornecimento.getCdfornecimento() != null){
			updateBaixado(fornecimento.getCdfornecimento().toString(), true);
		}
	}
	
	public void updateBaixado(String whereIn, Boolean baixado) {
		fornecimentoDAO.updateBaixado(whereIn, baixado);
	}
	
	/**
	 * Monta as contas a pagar geradas pelo fornecimento
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaDestino(Fornecimento form) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		OrigemsuprimentosBean origemsuprimentosBean = null;
		
		List<Documentoorigem> listaDocumento = documentoorigemService.getListaDocumentosDoFornecimento(form);
		form.setListaDocumentoorigem(listaDocumento);
		
		if(form.getListaDocumentoorigem() != null && form.getListaDocumentoorigem().size() > 0)
			for (Documentoorigem documentoorigem : form.getListaDocumentoorigem()){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.DOCUMENTO, "CONTA A PAGAR", documentoorigem.getDocumento().getCddocumento().toString(), null);
				listaBean.add(origemsuprimentosBean);
			}
		
		return listaBean;
	}
 
	
	@Override
	public void saveOrUpdate(Fornecimento bean) {
		if(bean.getFromFornecimentocontrato()){
			calculaPrazo(bean);
		}
		super.saveOrUpdate(bean);
	}

	protected void calculaPrazo(Fornecimento bean){

		Integer numParcela = fornecimentoDAO.getQtdeFornecimento(bean.getFornecimentocontrato());
		bean.setFornecimentocontrato(fornecimentocontratoService.loadForEntrada(bean.getFornecimentocontrato()));
		
		Prazopagamentoitem prazopagamentoitem = new Prazopagamentoitem();
		prazopagamentoitem = prazopagamentoitemService.obtemDadosParcela(bean.getFornecimentocontrato().getPrazopagamento(),numParcela);
		
		if(prazopagamentoitem != null){
			Prazopagamento prazopagamento = new Prazopagamento();
			prazopagamento = prazopagamentoitem.getPrazopagamento();
			
			List<Prazopagamentoitem> listaPrazopagamentoitem = new ListSet<Prazopagamentoitem>(Prazopagamentoitem.class);
			listaPrazopagamentoitem.add(prazopagamentoitem);
			prazopagamento.setListaPagamentoItem(listaPrazopagamentoitem);
			
			if(prazopagamento != null){
				bean.getFornecimentocontrato().setPrazopagamento(prazopagamento);
				fornecimentocontratoService.atualizaProximoVencimento(bean.getFornecimentocontrato(), numParcela);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.FornecimentoDAO#updateCdentregadocumento(String idFornecimento, Integer cdentregadocumento)
	 *
	 * @param idFornecimento
	 * @param cdentregadocumento
	 * @author Luiz Fernando
	 */
	public void updateCdentregadocumento(String idFornecimento, Integer cdentregadocumento) {
		fornecimentoDAO.updateCdentregadocumento(idFornecimento, cdentregadocumento);
	}
	
	public void verificaFornecimentoAposCancelamentoDocumento(String whereInDocumento) {
		List<Documento> listaDocumento = contapagarService.findForUpdateBaixaByDocumentoCancelado(whereInDocumento);
		if(listaDocumento != null && !listaDocumento.isEmpty()){
			List<Fornecimento> listaFornecimento = new ArrayList<Fornecimento>();
			for(Documento documento : listaDocumento){
				if(documento.getListaDocumentoOrigem() != null && !documento.getListaDocumentoOrigem().isEmpty()){
					for(Documentoorigem documentoorigem : documento.getListaDocumentoOrigem()){
						if(documentoorigem.getFornecimento() != null){
							if(!listaFornecimento.contains(documentoorigem.getFornecimento())){
								listaFornecimento.add(documentoorigem.getFornecimento());
							}
						}
					}
				}
			}
			if(listaFornecimento != null && !listaFornecimento.isEmpty()){
				StringBuilder whereIn = new StringBuilder();
				for(Fornecimento fornecimento : listaFornecimento){
					if(!existDocumentoNotCancelado(fornecimento)){
						if(!whereIn.toString().equals("")) whereIn.append(",");
						whereIn.append(fornecimento.getCdfornecimento());
					}
				}
				if(!whereIn.toString().equals("")){
					updateBaixado(whereIn.toString(), false);
				}
			}
		}
	}
	
	public Boolean existDocumentoNotCancelado(Fornecimento fornecimento) {
		return fornecimentoDAO.existDocumentoNotCancelado(fornecimento);
	}
	
	public void criarAvisoFornecimentoVencido(Motivoaviso m, Date data, Date dateToSearch) {
		List<Fornecimento> fornecimentoList = fornecimentoDAO.findByVencido(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Fornecimento f : fornecimentoList) {
			Aviso aviso = new Aviso("Contrato de fornecimento expirado", "C�digo do contrato de fornecimento: " + f.getCdfornecimento(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), f.getCdfornecimento(), empresa, SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, f.getCdfornecimento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, f.getCdfornecimento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public Resource generateCsvReport(FornecimentoFiltro filtro, String nomeArquivo) {
		List<Fornecimento> lista = fornecimentoDAO.findForReport(filtro);
		
		StringBuilder csv = new StringBuilder("\"Hist�rico\";\"Contrato\";\"Data de fornecimento\";\"Valor\";\"Baixado\";\n");
		String [] fields = {"historico", "fornecimentocontrato.descricao", "dtcontrato", "valor", "baixado"};
		
		for(Fornecimento bean : lista) {
			beanToCSV(fields, csv, bean);
		}
		
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
}
