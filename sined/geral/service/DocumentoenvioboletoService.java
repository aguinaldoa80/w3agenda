package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletoDAO;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletoclienteDAO;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletodiascobrancaDAO;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletosituacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoenvioboletoService extends GenericService<Documentoenvioboleto>{

	private DocumentoenvioboletoDAO documentoenvioboletoDAO;
	private DocumentoenvioboletoclienteDAO documentoenvioboletoclienteDAO;
	private DocumentoenvioboletodiascobrancaDAO documentoenvioboletodiascobrancaDAO;
	private DocumentoenvioboletosituacaoDAO documentoenvioboletosituacaoDAO;
	/* singleton */
	private static DocumentoenvioboletoService instance;
	
	public static DocumentoenvioboletoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentoenvioboletoService.class);
		}
		return instance;
	}
	public void setDocumentoenvioboletoDAO(
			DocumentoenvioboletoDAO documentoenvioboletoDAO) {
		this.documentoenvioboletoDAO = documentoenvioboletoDAO;
	}
	
	public void setDocumentoenvioboletoclienteDAO(
			DocumentoenvioboletoclienteDAO documentoenvioboletoclienteDAO) {
		this.documentoenvioboletoclienteDAO = documentoenvioboletoclienteDAO;
	}
	
	public void setDocumentoenvioboletosituacaoDAO(
			DocumentoenvioboletosituacaoDAO documentoenvioboletosituacaoDAO) {
		this.documentoenvioboletosituacaoDAO = documentoenvioboletosituacaoDAO;
	}
	
	public void setDocumentoenvioboletodiascobrancaDAO(
			DocumentoenvioboletodiascobrancaDAO documentoenvioboletodiascobrancaDAO) {
		this.documentoenvioboletodiascobrancaDAO = documentoenvioboletodiascobrancaDAO;
	}
	
	public void cancelarAgendamento(String whereIn){
		documentoenvioboletoDAO.cancelarAgendamento(whereIn);
	}
	
	public List<Documentoenvioboleto> findAtivos(String whereIn){
		return documentoenvioboletoDAO.findAtivos(whereIn);
	}
	
	public List<Documentoenvioboleto> findForEnvioautomatico(){
		return documentoenvioboletoDAO.findForEnvioautomatico();
	}
	
	public void concluiAgendamentosVencidos(){
		documentoenvioboletoDAO.concluiAgendamentosVencidos();
	}
	public boolean existeAgedamentoCancelado(String whereIn) {
		return documentoenvioboletoDAO.existeAgedamentoCancelado(whereIn);
	}

	@Override
	protected ListagemResult<Documentoenvioboleto> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Documentoenvioboleto> listagemResult = documentoenvioboletoDAO.findForExportacao(filtro);
		List<Documentoenvioboleto> list = listagemResult.list();
		for (Documentoenvioboleto documentoenvioboleto : list) {
			documentoenvioboleto.setListaCliente(documentoenvioboletoclienteDAO.findByDocumentoenvioboleto(documentoenvioboleto));
			documentoenvioboleto.setListaSituacao(documentoenvioboletosituacaoDAO.findByDocumentoenvioboleto(documentoenvioboleto));
			documentoenvioboleto.setListaDiascobranca(documentoenvioboletodiascobrancaDAO.findByDocumentoenvioboleto(documentoenvioboleto));
		}
		return listagemResult;
	}
}
