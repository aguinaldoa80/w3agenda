package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipocarteiratrabalho;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ClassificacaocarteiraService extends GenericService<Tipocarteiratrabalho> {	
	
	/* singleton */
	private static ClassificacaocarteiraService instance;
	public static ClassificacaocarteiraService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClassificacaocarteiraService.class);
		}
		return instance;
	}
	
}
