package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Pessoaconfiguracao;
import br.com.linkcom.sined.geral.bean.enumeration.Pessoaconfiguracaotipo;
import br.com.linkcom.sined.geral.dao.PessoaconfiguracaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PessoaconfiguracaoService extends GenericService<Pessoaconfiguracao> {

	private PessoaconfiguracaoDAO pessoaconfiguracaoDAO;
	
	public void setPessoaconfiguracaoDAO(PessoaconfiguracaoDAO pessoaconfiguracaoDAO) {this.pessoaconfiguracaoDAO = pessoaconfiguracaoDAO;}

	public Pessoaconfiguracao loadPessoaconfiguracao(Pessoaconfiguracao pessoaconfiguracao) {
		return pessoaconfiguracaoDAO.loadPessoaconfiguracao(pessoaconfiguracao);
	}
	
	public Pessoaconfiguracao loadPessoaconfiguracaoByPessoa(Pessoa pessoa, Pessoaconfiguracaotipo tipo) {
		return pessoaconfiguracaoDAO.loadPessoaconfiguracaoByPessoa(pessoa, tipo);
	}
	
	public Pessoaconfiguracao preenchePessoaconfiguracao(Empresa bean) {
		Pessoaconfiguracao pessoaconfiguracao = null; 
		
		if(bean.getPessoaconfiguracao() != null && bean.getPessoaconfiguracao().getCdpessoaconfiguracao() != null){
			pessoaconfiguracao = loadPessoaconfiguracao(bean.getPessoaconfiguracao());
		}else if(bean != null && bean.getCdpessoa() != null){ 
			pessoaconfiguracao = loadPessoaconfiguracaoByPessoa(bean, Pessoaconfiguracaotipo.EMPRESA);
		}
		
		if(pessoaconfiguracao == null){ 
			pessoaconfiguracao = new Pessoaconfiguracao(bean, Pessoaconfiguracaotipo.EMPRESA);
		}
		pessoaconfiguracao.setTransportador(bean.getPessoaconfiguracao_transportador());
		
		return pessoaconfiguracao;
	}

}
