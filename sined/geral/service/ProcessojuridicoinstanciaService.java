package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Processojuridicoinstancia;
import br.com.linkcom.sined.geral.dao.ProcessojuridicoinstanciaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessojuridicoinstanciaService extends GenericService<Processojuridicoinstancia>{

	ProcessojuridicoinstanciaDAO processojuridicoinstanciaDAO;
	
	public void setProcessojuridicoinstanciaDAO(ProcessojuridicoinstanciaDAO processojuridicoinstanciaDAO) {
		this.processojuridicoinstanciaDAO = processojuridicoinstanciaDAO;
	}
	
	public Processojuridicoinstancia carregaInstancia(Integer cdprocessojuridicoinstancia) {
		return processojuridicoinstanciaDAO.carregaInstancia(cdprocessojuridicoinstancia);
	}

	public List<Processojuridicoinstancia> listaDeInstancias(Integer cdprocessojuridico) {
		return processojuridicoinstanciaDAO.listaDeInstancias(cdprocessojuridico);
	}
	
	public List<Processojuridicoinstancia> listaAtualizarandamento() {
		return processojuridicoinstanciaDAO.listaAtualizarandamento();
	}
}
