package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.dao.ManifestoDfeHistoricoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;

public class ManifestoDfeHistoricoService extends GenericService<ManifestoDfeHistorico> {

	private ManifestoDfeHistoricoDAO manifestoDfeHistoricoDAO;
	
	public void setManifestoDfeHistoricoDAO(ManifestoDfeHistoricoDAO manifestoDfeHistoricoDAO) {
		this.manifestoDfeHistoricoDAO = manifestoDfeHistoricoDAO;
	}

	public List<ManifestoDfeHistorico> procurarPorManifestoDfe(ManifestoDfe manifestoDfe) {
		return manifestoDfeHistoricoDAO.procurarPorManifestoDfe(manifestoDfe);
	}
}
