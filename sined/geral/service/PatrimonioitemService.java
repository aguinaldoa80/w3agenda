package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialnumeroserie;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Patrimonioitemsituacao;
import br.com.linkcom.sined.geral.dao.PatrimonioitemDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PatrimonioitemFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.patrimonioitem.PatrimonioItemW3producaoRESTModel;

public class PatrimonioitemService extends GenericService<Patrimonioitem> {

	private PatrimonioitemDAO patrimonioitemDAO;
	private ParametrogeralService parametrogeralService;
	private MaterialnumeroserieService materialnumeroserieService;
	private static PatrimonioitemService instance;
	
	public void setPatrimonioitemDAO(PatrimonioitemDAO patrimonioitemDAO) {
		this.patrimonioitemDAO = patrimonioitemDAO;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setMaterialnumeroserieService(
			MaterialnumeroserieService materialnumeroserieService) {
		this.materialnumeroserieService = materialnumeroserieService;
	}
	
	public static PatrimonioitemService getInstance() {
		if (instance==null){
			instance = Neo.getObject(PatrimonioitemService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#loadByPlaquetaPatrimonio(Patrimonioitem)
	 * @param patrimonioitem
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Patrimonioitem loadByPlaquetaPatrimonio(Patrimonioitem patrimonioitem){
		return patrimonioitemDAO.loadByPlaquetaPatrimonio(patrimonioitem);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#loadWithPlaqueta
	 * @param patrimonioitem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Patrimonioitem loadWithPlaqueta(Patrimonioitem patrimonioitem) {
		return patrimonioitemDAO.loadWithPlaqueta(patrimonioitem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findWithoutPlaqueta
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findWithoutPlaqueta(String whereIn) {
		return patrimonioitemDAO.findWithoutPlaqueta(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#findByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Patrimonioitem> findByEntrega(Entrega entrega) {
		return patrimonioitemDAO.findByEntrega(entrega);
	}

	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#updatePlaqueta
	 * @param patrimonioitem
	 * @author Rodrigo Freitas
	 */
	public void updatePlaqueta(Patrimonioitem patrimonioitem) {
		patrimonioitemDAO.updatePlaqueta(patrimonioitem);		
	}
	
	/**
	 * Faz update em uma lista de patrimonios itens.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#updatePlaqueta
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void updateListaPlaquetas(final List<Patrimonioitem> lista) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				for (Patrimonioitem patrimonioitem : lista) {
					try {
						updatePlaqueta(patrimonioitem);
					} catch (DataIntegrityViolationException e) {
						if (DatabaseError.isKeyPresent(e, "IDX_PATRIMONIOITEM_PLAQUETA")) {
							throw new SinedException("Plaqueta "+patrimonioitem.getPlaqueta()+" j� cadastrada no sistema.");
						}
					}
				}
				
				return null;
			}
		});
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findForReport
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Patrimonioitem> findForReport(PatrimonioitemFiltro filtro){
		return patrimonioitemDAO.findForReport(filtro);
	}

	/**
	 * M�todo para a gera��o do relat�rio de itens de patrim�nio.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorio(PatrimonioitemFiltro filtro) {
		Report report = new Report("/suprimento/patrimonioitem");
		
		List<Patrimonioitem> lista = this.findForReport(filtro);
		
		report.setDataSource(lista);
		report.addParameter("TEXTO_PLAQUETA_SERIE", getTextoPlaquetaSerie());
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findForReportWithUltimolocal(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Patrimonioitem> findForReportWithUltimolocal(String whereIn) {
		return patrimonioitemDAO.findForReportWithUltimolocal(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param patrimonioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Boolean isPlaquetaValida(Patrimonioitem patrimonioitem) {
		return patrimonioitemDAO.isPlaquetaValida(patrimonioitem);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param plaqueta
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Patrimonioitem> findPatrimonioItemByPlaqueta(String plaqueta) {
		return patrimonioitemDAO.findPatrimonioItemByPlaqueta(plaqueta);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findPatrimonioItemAtivosByPlaqueta
	 * 
	 * @param plaqueta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findPatrimonioItemAtivosByPlaqueta(String plaqueta) {
		return patrimonioitemDAO.findPatrimonioItemAtivosByPlaqueta(plaqueta);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param material
	 * @return
	 * @author Tom�s Rabelo
	 * @param localarmazenagem 
	 */
	public Long getQtdDisponivelPatrimonio(Material material, Localarmazenagem localarmazenagem) {
		if(localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() == null){
			localarmazenagem = null;
		}
		return patrimonioitemDAO.getQtdDisponivelPatrimonio(material, localarmazenagem);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findByEntregaWithoutPlaqueta
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Patrimonioitem> findByEntregaWithoutPlaqueta(String whereIn) {
		return patrimonioitemDAO.findByEntregaWithoutPlaqueta(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findMaterialWithoutPlaqueta
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Patrimonioitem> findMaterialSemPlaquetaNoLocal(String whereIn) {
		return patrimonioitemDAO.findMaterialSemPlaquetaNoLocal(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#existePlaqueta
	 * 
	 * @param plaqueta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean existePlaqueta(String plaqueta) {
		return patrimonioitemDAO.existePlaqueta(plaqueta);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param patrimonioitem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Patrimonioitem> findComboTipoVeiculo(Patrimonioitem patrimonioitem) {
		return patrimonioitemDAO.findComboTipoVeiculo(patrimonioitem);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findForRomaneio(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Patrimonioitem> findForRomaneio(String whereIn) {
		return patrimonioitemDAO.findForRomaneio(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/09/2013
	 */
	public List<Patrimonioitem> findByMaterial(Material material, Integer cdpatrimonioitem, Patrimonioitemsituacao... patrimonioitemsituacao) {
		List<Patrimonioitem> lista = patrimonioitemDAO.findByMaterial(material, cdpatrimonioitem, patrimonioitemsituacao);
		for (Patrimonioitem patrimonioitem : lista) {
			if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECAPAGEM))){
				String plaquetaSerie = patrimonioitem.getPlaquetaSerie();
				if (plaquetaSerie == null || plaquetaSerie.trim().equals("")){
					patrimonioitem.setPlaqueta("-- sem plaqueta/s�rie --");
				}
			}else {
				if(patrimonioitem.getPlaqueta() == null || patrimonioitem.getPlaqueta().trim().equals("")){
					patrimonioitem.setPlaqueta("-- sem plaqueta --");
				}
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findForAutocompletePatrimonioitem(String q)
	 *
	 * @param q
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/09/2013
	 */
	public List<Patrimonioitem> findForAutocompletePatrimonioitem(String q){
		return patrimonioitemDAO.findForAutocompletePatrimonioitem(q);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param patrimonioitem
	 * @param ativo
	 * @author Rodrigo Freitas
	 * @since 07/10/2013
	 */
	public void updateAtivo(Patrimonioitem patrimonioitem, Boolean ativo) {
		patrimonioitemDAO.updateAtivo(patrimonioitem, ativo);
	}
	
	public List<PatrimonioItemW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de patrimonioitem a ser sincronizada com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PatrimonioItemW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PatrimonioItemW3producaoRESTModel> lista = new ArrayList<PatrimonioItemW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Patrimonioitem pi : patrimonioitemDAO.findForW3Producao(whereIn))
				lista.add(new PatrimonioItemW3producaoRESTModel(pi));
		}
		
		return lista;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PatrimonioitemDAO#findForAutocompleteEquipamento(String q, Material material)
	*
	* @param q
	* @return
	* @since 24/02/2016
	* @author Luiz Fernando
	*/
	public List<Patrimonioitem> findForAutocompleteEquipamento(String q) {
		Material material = null;
		try {
			material = new Material(Integer.parseInt(NeoWeb.getRequestContext().getParameter("material")));
		} catch (Exception e) {
			return new ArrayList<Patrimonioitem>();
		} 
		
		return patrimonioitemDAO.findForAutocompleteEquipamento(q, material);
	}
	
	public String getTextoPlaquetaSerie(){
//		if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECAPAGEM))){
//			return "Plaqueta/S�rie";
//		}else {
//			return "Plaqueta";
//		}
		
		return "Plaqueta/S�rie";
	}
	
//	public String getPlaquetaSerie(String plaqueta, Material material){
//		String valor;
//
//		if (plaqueta!=null && !plaqueta.trim().equals("")){
//			valor = plaqueta;
//		}else {
//			valor = "-";
//		}
//		
//		valor += "/";
//		
//		String serie = material != null ? material.getSerie() : null;
//		
//		if (material!=null && serie!=null){
//			valor += serie;			
//		}else {
//			valor += "-";
//		}
//		
//		if (valor.equals("-/-")){
//			return null;
//		}else {
//			return valor;
//		}		
//	}
	
	public List<Patrimonioitem> findByMaterial(Material material) {
		return patrimonioitemDAO.findByMaterial(material);
	}
	
	public void processarPatrimonioitemListagem(List<Patrimonioitem> lista) {
		Map<Integer, List<Materialnumeroserie>> mapa = new HashMap<Integer, List<Materialnumeroserie>>();
		String whereIn = "-1";
		
		for (Patrimonioitem patrimonioitem : lista){
			if (patrimonioitem.getBempatrimonio() != null){
				whereIn += ", " + patrimonioitem.getBempatrimonio().getCdmaterial();
			}
		}
		
		for (Materialnumeroserie materialnumeroserie : materialnumeroserieService.findByCdsmaterial(whereIn)){
			List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(materialnumeroserie.getMaterial().getCdmaterial());
			if (listaMaterialnumeroserie == null){
				listaMaterialnumeroserie = new ArrayList<Materialnumeroserie>();
			}
			listaMaterialnumeroserie.add(materialnumeroserie);
			mapa.put(materialnumeroserie.getMaterial().getCdmaterial(), listaMaterialnumeroserie);
		}			
		
		for (Patrimonioitem patrimonioitem: lista){
			if (patrimonioitem.getBempatrimonio() != null){
				List<Materialnumeroserie> listaMaterialnumeroserie = mapa.get(patrimonioitem.getBempatrimonio().getCdmaterial());
				if (listaMaterialnumeroserie!=null){
					patrimonioitem.getBempatrimonio().setListaMaterialnumeroserie(
							new ListSet<Materialnumeroserie>(Materialnumeroserie.class, listaMaterialnumeroserie));
				}
			}
		}
	}
	
	public Resource generateCsvReport(PatrimonioitemFiltro filtro, String nomeArquivo) {
		List<Patrimonioitem> lista = patrimonioitemDAO.findForCsvReport(filtro);
		processarPatrimonioitemListagem(lista);
		
		StringBuilder csv = new StringBuilder("\"Material (Patrim�nio)\";\"Grupo do material\";\"Tipo do material\";\"Plaqueta/S�rie\";\"Cliente\";\"Local\";\"Ativo\";\"Situa��o\"\n");
		String [] fields = {"bempatrimonio", "bempatrimonio.materialgrupo", "bempatrimonio.materialtipo", "plaquetaSerie", "clientenomelistagem", "localnomelistagem", "ativo", "vpatrimonioitem.situacao.nome"};
		
		for(Patrimonioitem bean : lista) {
			beanToCSV(fields, csv, bean);
		}
		
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().getBytes());
		return resource;
	}
	
	public List<Patrimonioitem> findByMaterialContrato(String plaqueta) {

		Integer cdmaterial = null;
		try {
			cdmaterial = Integer.parseInt(NeoWeb.getRequestContext().getParameter("cdmaterial"));
		} catch (Exception e) {} 
		List<Patrimonioitem> lista = new ArrayList<Patrimonioitem>();
		if(cdmaterial != null){
			lista = patrimonioitemDAO.findByMaterial(cdmaterial, plaqueta, Patrimonioitemsituacao.DISPONIVEL);			
		}
		
		if(SinedUtil.isListNotEmpty(lista)){
			for (Patrimonioitem patrimonioitem : lista) {
				if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECAPAGEM))){
					String plaquetaSerie = patrimonioitem.getPlaquetaSerie();
					if (plaquetaSerie == null || plaquetaSerie.trim().equals("")){
						patrimonioitem.setPlaqueta("-- sem plaqueta/s�rie --");
					}
				}else {
					if(patrimonioitem.getPlaqueta() == null || patrimonioitem.getPlaqueta().trim().equals("")){
						patrimonioitem.setPlaqueta("-- sem plaqueta --");
					}
				}
			}			
		}
		return lista;
	}
	
	public List<Patrimonioitem> findByMaterialContrato(Integer cdmaterial) {		
		List<Patrimonioitem> lista = patrimonioitemDAO.findByMaterial(cdmaterial, null, Patrimonioitemsituacao.DISPONIVEL);
		for (Patrimonioitem patrimonioitem : lista) {
			if ("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.RECAPAGEM))){
				String plaquetaSerie = patrimonioitem.getPlaquetaSerie();
				if (plaquetaSerie == null || plaquetaSerie.trim().equals("")){
					patrimonioitem.setPlaqueta("-- sem plaqueta/s�rie --");
				}
			}else {
				if(patrimonioitem.getPlaqueta() == null || patrimonioitem.getPlaqueta().trim().equals("")){
					patrimonioitem.setPlaqueta("-- sem plaqueta --");
				}
			}
		}
		return lista;
	}
	public List<Patrimonioitem> findByPlaqueta(String plaqueta) {
		return patrimonioitemDAO.findByPlaqueta(plaqueta);
	}
	
	public List<Patrimonioitem> loadWithPlaqueta(String whereIn) {
		return patrimonioitemDAO.loadWithPlaqueta(whereIn);
	}
}