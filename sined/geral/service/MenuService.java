package br.com.linkcom.sined.geral.service;

import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Menu;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.geral.dao.MenuDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MenuService extends GenericService<Menu> {
	
	private MenuDAO menuDAO;
	private HashMap<String, HashMap<String, String>> mapaTituloTela = new HashMap<String, HashMap<String,String>>();
	
	public void setMenuDAO(MenuDAO menuDAO) {
		this.menuDAO = menuDAO;
	}
	
	/* singleton */
	private static MenuService instance;
	public static MenuService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MenuService.class);
		}
		return instance;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MenuDAO#findForTelaEntrada(Integer cdmenu)
	 *
	 * @param cdmenu
	 * @return
	 * @since 27/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Menu> findForTelaEntrada(Integer cdmenu) {
		return menuDAO.findForTelaEntrada(cdmenu);
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MenuDAO#findByModulo(Menumodulo menumodulo)
	 *
	 * @param menumodulo
	 * @return
	 * @since 30/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Menu> findByModulo(Menumodulo menumodulo) {
		return menuDAO.findByModulo(menumodulo);
	}
	
	public String getTituloTela(String url) {
		String subdominio = SinedUtil.getSubdominioCliente();
		
		if (!mapaTituloTela.containsKey(subdominio)){
			List<Menu> listaMenu = findAll();
			HashMap<String, String> mapa = new HashMap<String, String>();
			for (Menu menu: listaMenu) {
				if(menu != null){
					try{
						String url2 = menu.getUrl();
						String nome = menu.getNome();
						mapa.put(url2, nome.trim());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			mapaTituloTela.put(subdominio, mapa);
		}
		
		return mapaTituloTela.get(subdominio).get(url);
	}
}
