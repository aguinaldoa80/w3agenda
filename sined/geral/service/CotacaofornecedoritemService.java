package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.dao.CotacaofornecedoritemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CotacaofornecedoritemService extends GenericService<Cotacaofornecedoritem> {

	private CotacaofornecedoritemDAO cotacaofornecedoritemDAO;
	
	public void setCotacaofornecedoritemDAO(CotacaofornecedoritemDAO cotacaofornecedoritemDAO) {
		this.cotacaofornecedoritemDAO = cotacaofornecedoritemDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaofornecedoritemDAO#findByCotacaofornecedor
	 * @param cotacaofornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaofornecedoritem> findByCotacaofornecedor(Cotacaofornecedor cotacaofornecedor){
		return cotacaofornecedoritemDAO.findByCotacaofornecedor(cotacaofornecedor);
	}

	public Double getUltimoValor(Integer cdmaterial, Integer cdfornecedor) {
		List<Cotacaofornecedoritem> lista = this.findByFornecedorMaterial(new Material(cdmaterial), new Fornecedor(cdfornecedor));
		if(lista != null && lista.size() > 0) return lista.get(0).getValorunitario();
		return null;
	}

	public List<Cotacaofornecedoritem> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		return cotacaofornecedoritemDAO.findByFornecedorMaterial(material, fornecedor);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param bean
	 * @param string
	 * @author Rodrigo Freitas
	 * @since 22/05/2015
	 */
	public void deleteNaoExistentes(Cotacaofornecedor bean, String whereIn) {
		cotacaofornecedoritemDAO.deleteNaoExistentes(bean, whereIn);
	}
}
