package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresaproprietario;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.LcdprArquivoFiltro;

public class EmpresaproprietarioService extends GenericService<Empresaproprietario> {
	
	EmpresaproprietarioDAO empresaproprietarioDAO;
	
	public void setEmpresaproprietarioDAO(EmpresaproprietarioDAO empresaproprietarioDAO) {
		this.empresaproprietarioDAO = empresaproprietarioDAO;
	}

	public List<Colaborador> findProprietariosFazendaAutoComplete(String nome) {
		List<Empresaproprietario> listaEmpresaProprietario = empresaproprietarioDAO.findProprietariosFazendaAutoComplete(nome);
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		for (Empresaproprietario empresaproprietario : listaEmpresaProprietario) {
			if(listaColaborador.contains(empresaproprietario.getColaborador())){
				continue;
			}
			Colaborador colaborador =  new Colaborador();
			colaborador.setCdpessoa(empresaproprietario.getColaborador().getCdpessoa());
			colaborador.setNome(empresaproprietario.getColaborador().getNome());
			listaColaborador.add(colaborador);
		}
		
		return listaColaborador;
	}

	public Empresaproprietario findColaboradorPrincipal(Integer cdpessoa) {
		return empresaproprietarioDAO.findColaboradorPrincipal(cdpessoa);
	}

	public List<Empresaproprietario> loadForLcdpr(LcdprArquivoFiltro filtro) {
		return empresaproprietarioDAO.loadForLcdpr(filtro);
	}

}
