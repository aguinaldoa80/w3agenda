package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialhistorico;
import br.com.linkcom.sined.geral.dao.MaterialhistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialhistoricoService extends GenericService<Materialhistorico>{
	
	private MaterialhistoricoDAO materialhistoricoDAO;
	
	public void setMaterialhistoricoDAO(
			MaterialhistoricoDAO materialhistoricoDAO) {
		this.materialhistoricoDAO = materialhistoricoDAO;
	}

	/**
	 * Seta os valores antigos no hist�rico do material.
	 *
	 * @param materialhistorico
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 03/04/2013
	 */
	public void setValoresAntigos(Materialhistorico materialhistorico, Material material) {
		materialhistorico.setAtivo(material.getAtivo());
		materialhistorico.setCodigobarras(material.getCodigobarras());
		materialhistorico.setMaterialgrupo(material.getMaterialgrupo());
		materialhistorico.setNcmcompleto(material.getNcmcompleto());
		materialhistorico.setNome(material.getNome());
		materialhistorico.setUnidademedida(material.getUnidademedida());
		materialhistorico.setValorvenda(material.getValorvenda());
		materialhistorico.setValorVendaMinimo(material.getValorvendaminimo());
		materialhistorico.setVendaecf(material.getVendaecf());
		materialhistorico.setVendapromocional(material.getVendapromocional());
		materialhistorico.setValorVendaMinimo(material.getValorvendaminimo());
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialhistoricoDAO#findByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/04/2013
	 */
	public List<Materialhistorico> findByMaterial(Material material) {
		return materialhistoricoDAO.findByMaterial(material);
	}
	
}
