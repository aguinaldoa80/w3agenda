package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.Notafiscalservicoduplicata;
import br.com.linkcom.sined.geral.dao.NotafiscalservicoduplicataDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalservicoduplicataService extends GenericService<Notafiscalservicoduplicata> {

	private NotafiscalservicoduplicataDAO notafiscalservicoduplicataDAO;
	
	public void setNotafiscalservicoduplicataDAO(NotafiscalservicoduplicataDAO notafiscalservicoduplicataDAO) {
		this.notafiscalservicoduplicataDAO = notafiscalservicoduplicataDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.NotafiscalservicoduplicataDAO#findByNotafiscalservico(NotaFiscalServico notaFiscalServico)
	*
	* @param notaFiscalServico
	* @return
	* @since 16/01/2017
	* @author Luiz Fernando
	*/
	public List<Notafiscalservicoduplicata> findByNotafiscalservico(NotaFiscalServico notaFiscalServico) {
		return notafiscalservicoduplicataDAO.findByNotafiscalservico(notaFiscalServico);
	}
	
	public Boolean existeDuplicata(NotaFiscalServico notaFiscalServico) {
		return notafiscalservicoduplicataDAO.existeDuplicata(notaFiscalServico);
	}

	public void updateNumeroDuplicata(Notafiscalservicoduplicata dup, String numero) {
		notafiscalservicoduplicataDAO.updateNumeroDuplicata(dup, numero);
	}
}
