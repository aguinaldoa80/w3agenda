package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.dao.EnderecotipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.enderecotipo.EnderecotipoRESTModel;

public class EnderecotipoService extends GenericService<Enderecotipo>{
	
	private EnderecotipoDAO enderecotipoDAO;
	
	public void setEnderecotipoDAO(EnderecotipoDAO enderecotipoDAO) {
		this.enderecotipoDAO = enderecotipoDAO;
	}

	/* singleton */
	private static EnderecotipoService instance;
	public static EnderecotipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EnderecotipoService.class);
		}
		return instance;
	}

	public List<EnderecotipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<EnderecotipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<EnderecotipoRESTModel> lista = new ArrayList<EnderecotipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Enderecotipo bean : enderecotipoDAO.findForAndroid(whereIn))
				lista.add(new EnderecotipoRESTModel(bean));
		}
		
		return lista;
	}
}
