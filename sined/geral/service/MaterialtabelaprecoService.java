package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoItemHistorico;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_VendamaterialTabelapreco;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.MaterialtabelaprecoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.materialtabelapreco.MaterialtabelaprecoRESTModel;

public class MaterialtabelaprecoService extends GenericService<Materialtabelapreco> {

	private MaterialtabelaprecoDAO materialtabelaprecoDAO;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService; 
	private ClienteService clienteService;
	private ParametrogeralService parametrogeralService;
	private MaterialunidademedidaService materialunidademedidaService;
	private MaterialService materialService;
	private PessoacategoriaService pessoacategoriaService;
	private MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService;
	
	public void setMaterialtabelaprecoDAO(MaterialtabelaprecoDAO materialtabelaprecoDAO) {
		this.materialtabelaprecoDAO = materialtabelaprecoDAO;
	}
	public void setMaterialtabelaprecoitemService(MaterialtabelaprecoitemService materialtabelaprecoitemService) {
		this.materialtabelaprecoitemService = materialtabelaprecoitemService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {
		this.pessoacategoriaService = pessoacategoriaService;
	}
	public void setMaterialTabelaPrecoItemHistoricoService(
			MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService) {
		this.materialTabelaPrecoItemHistoricoService = materialTabelaPrecoItemHistoricoService;
	}
	
	/* singleton */
	private static MaterialtabelaprecoService instance;
	public static MaterialtabelaprecoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialtabelaprecoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO#haveRegistroCliente(Integer cdmaterialtabelapreco, Cliente cliente, Date dtinicio, Date dtfim)
	 *
	 * @param cdmaterialtabelapreco
	 * @param cliente
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 * @param frequencia 
	 * @param empresas 
	 */
	public boolean haveRegistroCliente(Integer cdmaterialtabelapreco, Cliente cliente, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, boolean consideraSomenteCategoria, Empresa... empresas) {
		List<Materialtabelapreco> lista = findTabelaCliente(cdmaterialtabelapreco, cliente, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, consideraSomenteCategoria, empresas);
		return lista != null && lista.size() > 0;
	}
	
	public List<Materialtabelapreco> findTabelaCliente(Integer cdmaterialtabelapreco, Cliente cliente, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, boolean consideraSomenteCategoria, Empresa... empresas) {
		return materialtabelaprecoDAO.findTabelaCliente(cdmaterialtabelapreco, cliente, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, consideraSomenteCategoria, empresas);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO#haveRegistroMaterial(Integer cdmaterialtabelapreco, Cliente cliente, Set<Materialtabelaprecoitem> lista, Date dtinicio, Date dtfim)
	 *
	 * @param cdmaterialtabelapreco
	 * @param cliente
	 * @param material
	 * @param identificador
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 * @param frequencia 
	 */
	public boolean haveRegistroMaterial(Integer cdmaterialtabelapreco, String whereInCliente, Material material, String identificador, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, Empresa... empresas) {
		List<Materialtabelapreco> lista = findTabelaMaterial(cdmaterialtabelapreco, whereInCliente, material, identificador, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, empresas);
		return lista != null && lista.size() > 0;
	}
	
	public List<Materialtabelapreco> findTabelaMaterial(Integer cdmaterialtabelapreco, String whereInCliente, Material material, String identificador, Prazopagamento prazopagamento, Frequencia frequencia, Pedidovendatipo pedidovendatipo, Date dtinicio, Date dtfim, Materialgrupo materialgrupo, Categoria categoria, Empresa... empresas) {
		return materialtabelaprecoDAO.findTabelaMaterial(cdmaterialtabelapreco, whereInCliente, material, identificador, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, empresas);
	}

	/**
	 * Retorna o valor atual do material de acordo com a tabela de pre�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialtabelaprecoitemService#getItemTabelaPrecoAtual(Material material, Cliente cliente)
	 *
	 * @param material
	 * @param cliente
	 * @param prazopagamento 
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 * @param precovenda 
	 */
	public Aux_VendamaterialTabelapreco getValorTabelaPrecoAtual(Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, boolean pedidovenda, Double precovenda, Empresa empresa, Unidademedida unidademedida) {
		Materialgrupo materialgrupo = null;
		List<Categoria> listaCategoria = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
			if(listaPessoacategoria != null && !listaPessoacategoria.isEmpty()){
				listaCategoria = new ArrayList<Categoria>();
				for(Pessoacategoria pessoacategoria : listaPessoacategoria){
					if(pessoacategoria.getCategoria() != null && pessoacategoria.getCdpessoacategoria() != null){
						listaCategoria.add(pessoacategoria.getCategoria());
					}
				}
			}
		}
		
		if(material != null && material.getCdmaterial() != null){
			Material m = materialService.findWithMaterialgrupo(material);
			if(m != null && m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
				materialgrupo = m.getMaterialgrupo();
			}
		}
		
		Aux_VendamaterialTabelapreco auxVendamaterialTabelapreco = new Aux_VendamaterialTabelapreco(material);
		Double valorPrecoAtual = null;
		Money percentualDesconto = null;
		String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
		Integer numCasasDecimais = null; 
		if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
			String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			if(aux != null && !aux.trim().isEmpty()){
				try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
			}
		}
		Materialtabelapreco materialtabelapreco = this.getTabelaPrecoAtual(SinedDateUtils.currentDate(), materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, false);
		if(materialtabelapreco != null){
			valorPrecoAtual = materialtabelapreco.getValorComDescontoAcrescimo(precovenda, numCasasDecimais);
			percentualDesconto = materialtabelapreco.getPercentualDescontoDiscriminado();
			
		}else {
			Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemTabelaPrecoAtual(material, null, listaCategoria, prazopagamento, pedidovendatipo, empresa, unidademedida);
			if(materialtabelaprecoitem == null){
				materialtabelaprecoitem = materialtabelaprecoitemService.getItemTabelaPrecoAtual(material, cliente, null, prazopagamento, pedidovendatipo, empresa, unidademedida);
			}
			
			if(materialtabelaprecoitem != null){
				valorPrecoAtual = materialtabelaprecoitem.getValorcomtaxa(precovenda, numCasasDecimais);
				percentualDesconto = materialtabelaprecoitem.getPercentualDescontoDiscriminado();
			}
		}
		
		auxVendamaterialTabelapreco.setValorvendamaterial(valorPrecoAtual);
		auxVendamaterialTabelapreco.setPercentualdesconto(percentualDesconto);
		return auxVendamaterialTabelapreco;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO#findForComboVenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @param listaCategoria 
	 */
	public List<Materialtabelapreco> findForComboVenda(Cliente cliente, Date dtvenda, Empresa empresa, List<Categoria> listaCategoria) {
		return materialtabelaprecoDAO.findForComboVenda(cliente, dtvenda, empresa, listaCategoria);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MaterialtabelaprecoitemService#getItemTabelaPrecoAtual(Material material, Materialtabelapreco materialtabelapreco)
	 *
	 * @param material
	 * @param materialtabelapreco
	 * @return
	 * @author Luiz Fernando
	 * @param valorvenda 
	 */
	public Aux_VendamaterialTabelapreco getValorTabelaPrecoAtual(Material material, Unidademedida unidademedida, Materialtabelapreco materialtabelapreco, Double valorvenda) {
		Aux_VendamaterialTabelapreco auxVendamaterialTabelapreco = new Aux_VendamaterialTabelapreco(material);
		Materialtabelaprecoitem itemTabelaPrecoAtual = materialtabelaprecoitemService.getItemTabelaPrecoAtual(material, unidademedida, materialtabelapreco);
		String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
		Integer numCasasDecimais = null; 
		if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
			String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			if(aux != null && !aux.trim().isEmpty()){
				try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
			}
		}
		
		if(itemTabelaPrecoAtual != null){
			auxVendamaterialTabelapreco.setValorvendamaterial(itemTabelaPrecoAtual.getValorcomtaxa(valorvenda, numCasasDecimais));
			auxVendamaterialTabelapreco.setPercentualdesconto(itemTabelaPrecoAtual.getPercentualDescontoDiscriminado());
		}
		
		return auxVendamaterialTabelapreco;
	}
	
	/**
	 * M�todo que retira as refer�ncias para fazer a copia da tabela de pre�o
	 *
	 * @param materialtabelapreco
	 * @author Luiz Fernando
	 */
	public void limparReferenciasForCopiar(Materialtabelapreco materialtabelapreco) {
		if(materialtabelapreco != null){
			materialtabelapreco.setCdmaterialtabelapreco(null);
			materialtabelapreco.setDtinicio(new Date(System.currentTimeMillis()));
			if(materialtabelapreco.getDtfim() != null && SinedDateUtils.afterIgnoreHour(materialtabelapreco.getDtfim(), materialtabelapreco.getDtinicio())){
				materialtabelapreco.setDtfim(null);
			}
			if(materialtabelapreco.getListaMaterialtabelaprecoitem() != null && !materialtabelapreco.getListaMaterialtabelaprecoitem().isEmpty()){
				for(Materialtabelaprecoitem materialtabelaprecoitem : materialtabelapreco.getListaMaterialtabelaprecoitem()){
					materialtabelaprecoitem.setCdmaterialtabelaprecoitem(null);
				}
			}
			if(materialtabelapreco.getListaMaterialtabelaprecocliente() != null && !materialtabelapreco.getListaMaterialtabelaprecocliente().isEmpty()){
				for(Materialtabelaprecocliente materialtabelaprecocliente : materialtabelapreco.getListaMaterialtabelaprecocliente()){
					materialtabelaprecocliente.setCdmaterialtabelaprecocliente(null);
				}
			}
			if(materialtabelapreco.getListaMaterialtabelaprecoempresa() != null && !materialtabelapreco.getListaMaterialtabelaprecoempresa().isEmpty()){
				for(Materialtabelaprecoempresa materialtabelaprecoempresa : materialtabelapreco.getListaMaterialtabelaprecoempresa()){
					materialtabelaprecoempresa.setCdmaterialtabelaprecoempresa(null);
				}
			}
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Materialtabelapreco> findForAjustarPreco(String whereIn){
		return materialtabelaprecoDAO.findForAjustarPreco(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO#existTabelaByCliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existTabelaByCliente(Cliente cliente) {
		return materialtabelaprecoDAO.existTabelaByCliente(cliente);
	}
	
	/**
	 * M�todo que cria tabela de pre�o para o cliente - Venda
	 *
	 * @param venda
	 * @param cliente
	 * @author Luiz Fernando
	 */
	public void criaTabelaprecoByCliente(Venda venda, Cliente cliente){
		if(cliente != null && cliente.getCdpessoa() != null && !existTabelaByCliente(cliente)){
			cliente = clienteService.load(cliente, "cliente.cdpessoa, cliente.nome, cliente.identificador");
			String nometabelapreco = cliente.getIdentificador() != null ? cliente.getIdentificador() : cliente.getCdpessoa().toString();
			if(cliente.getNome().indexOf(" ") != -1){
				nometabelapreco += cliente.getNome().substring(0, cliente.getNome().indexOf(" "));
			}else {
				nometabelapreco += cliente.getNome();
			}
			
			if(venda != null && venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				HashMap<Material, Unidademedida> mapMaterialUnidademedida = new HashMap<Material, Unidademedida>();
				Materialtabelapreco materialtabelapreco = new Materialtabelapreco();
				materialtabelapreco.setTabelaprecotipo(getTipocalculoByParametro());
				Materialtabelaprecocliente materialtabelaprecocliente = new Materialtabelaprecocliente();
				Materialtabelaprecoitem materialtabelaprecoitem;
				
				Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
				Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = new ListSet<Materialtabelaprecocliente>(Materialtabelaprecocliente.class);
				
				materialtabelapreco.setDtinicio(new Date(System.currentTimeMillis()));
				materialtabelapreco.setNome(nometabelapreco);
				
				materialtabelaprecocliente.setCliente(cliente);
				listaMaterialtabelaprecocliente.add(materialtabelaprecocliente);
				
				String paramMaterialMestreTabelaPreco = parametrogeralService.getValorPorNome("MATERIAL_MESTRE_TABELAPRECO");
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					Material aux = materialService.loadWithMaterialMestre(vendamaterial.getMaterial(), true);					
					Material material;
					
					if(paramMaterialMestreTabelaPreco.equals("TRUE") && aux.getMaterialmestregrade() != null) {
						material = aux.getMaterialmestregrade();
					} else {
						material = vendamaterial.getMaterial();
					}
					
					if(vendamaterial.getMaterial() != null && 
								(mapMaterialUnidademedida.get(vendamaterial.getMaterial()) == null || 
								!mapMaterialUnidademedida.get(vendamaterial.getMaterial()).equals(vendamaterial.getUnidademedida()))
							 ){
						materialtabelaprecoitem = new Materialtabelaprecoitem();
						materialtabelaprecoitem.setUnidademedida(vendamaterial.getUnidademedida());
						materialtabelaprecoitem.setMaterial(material);
						materialtabelaprecoitem.setVendaMaterial(vendamaterial);
						
						if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setValor(vendamaterial.getPreco());
							
							if(material.getValorvendamaximo() != null)
								materialtabelaprecoitem.setValorvendamaximo(material.getValorvendamaximo());
							if(material.getValorvendaminimo() != null)
								materialtabelaprecoitem.setValorvendaminimo(material.getValorvendaminimo());
						}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setPercentualdesconto(vendamaterial.getPercentualdesconto() != null ? vendamaterial.getPercentualdesconto() : 0d);
						}
						
						mapMaterialUnidademedida.put(vendamaterial.getMaterial(), vendamaterial.getUnidademedida());
						listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
					}
				}
				
				materialtabelapreco.setListaMaterialtabelaprecocliente(listaMaterialtabelaprecocliente);
				materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
				
				this.saveOrUpdate(materialtabelapreco);
				materialTabelaPrecoItemHistoricoService.createHistorico(materialtabelapreco.getListaMaterialtabelaprecoitem(), materialtabelapreco.getTabelaprecotipo());
			}
		}
	}
	
	/**
	 * M�todo que cria tabela de pre�o para o cliente - Or�amento
	 *
	 * @param vendaorcamento
	 * @param cliente
	 * @author Luiz Fernando
	 */
	public void criaTabelaprecoByCliente(Vendaorcamento vendaorcamento, Cliente cliente){
		if(cliente != null && cliente.getCdpessoa() != null && !existTabelaByCliente(cliente)){
			cliente = clienteService.load(cliente, "cliente.cdpessoa, cliente.nome, cliente.identificador");
			String nometabelapreco = cliente.getIdentificador() != null ? cliente.getIdentificador() : cliente.getCdpessoa().toString();
			if(cliente.getNome().indexOf(" ") != -1){
				nometabelapreco += cliente.getNome().substring(0, cliente.getNome().indexOf(" "));
			}else {
				nometabelapreco += cliente.getNome();
			}
			if(vendaorcamento != null && vendaorcamento.getListavendaorcamentomaterial() != null && !vendaorcamento.getListavendaorcamentomaterial().isEmpty()){
				List<Material> listaMaterialCriado = new ArrayList<Material>();
				Materialtabelapreco materialtabelapreco = new Materialtabelapreco();
				materialtabelapreco.setTabelaprecotipo(getTipocalculoByParametro());
				Materialtabelaprecocliente materialtabelaprecocliente = new Materialtabelaprecocliente();
				Materialtabelaprecoitem materialtabelaprecoitem;
				
				Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
				Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = new ListSet<Materialtabelaprecocliente>(Materialtabelaprecocliente.class);
				
				materialtabelapreco.setDtinicio(new Date(System.currentTimeMillis()));
				materialtabelapreco.setNome(nometabelapreco);
				
				materialtabelaprecocliente.setCliente(cliente);
				listaMaterialtabelaprecocliente.add(materialtabelaprecocliente);
				
				for(Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
					if(vendaorcamentomaterial.getMaterial() != null && !listaMaterialCriado.contains(vendaorcamentomaterial.getMaterial())){
						materialtabelaprecoitem = new Materialtabelaprecoitem();
						materialtabelaprecoitem.setMaterial(vendaorcamentomaterial.getMaterial());
						materialtabelaprecoitem.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
						materialtabelaprecoitem.setVendaOrcamentoMaterial(vendaorcamentomaterial);
						
						if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setValor(vendaorcamentomaterial.getPreco());
							
							if(vendaorcamentomaterial.getMaterial().getValorvendamaximo() != null){
								materialtabelaprecoitem.setValorvendamaximo(vendaorcamentomaterial.getMaterial().getValorvendamaximo());
							} 
							if(vendaorcamentomaterial.getMaterial().getValorvendaminimo() != null){
								materialtabelaprecoitem.setValorvendaminimo(vendaorcamentomaterial.getMaterial().getValorvendaminimo());
							}
						}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto() != null ? vendaorcamentomaterial.getPercentualdesconto() : 0d);
						}
						
						listaMaterialCriado.add(vendaorcamentomaterial.getMaterial());
						listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
					}
				}
				
				materialtabelapreco.setListaMaterialtabelaprecocliente(listaMaterialtabelaprecocliente);
				materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
				
				this.saveOrUpdate(materialtabelapreco);
				materialTabelaPrecoItemHistoricoService.createHistorico(materialtabelapreco.getListaMaterialtabelaprecoitem(), materialtabelapreco.getTabelaprecotipo());
			}			
		}
	}
	
	/**
	 * M�todo que cria tabela de pre�o para o cliente - Pedido de venda
	 *
	 * @param pedidovenda
	 * @param cliente
	 * @author Luiz Fernando
	 */
	public void criaTabelaprecoByCliente(Pedidovenda pedidovenda, Cliente cliente){
		if(cliente != null && cliente.getCdpessoa() != null && !existTabelaByCliente(cliente)){
			cliente = clienteService.load(cliente, "cliente.cdpessoa, cliente.nome, cliente.identificador");
			String nometabelapreco = cliente.getIdentificador() != null ? cliente.getIdentificador() : cliente.getCdpessoa().toString();
			if(cliente.getNome().indexOf(" ") != -1){
				nometabelapreco += cliente.getNome().substring(0, cliente.getNome().indexOf(" "));
			}else {
				nometabelapreco += cliente.getNome();
			}
			if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
				List<Material> listaMaterialCriado = new ArrayList<Material>();
				Materialtabelapreco materialtabelapreco = new Materialtabelapreco();
				materialtabelapreco.setTabelaprecotipo(getTipocalculoByParametro());
				Materialtabelaprecocliente materialtabelaprecocliente = new Materialtabelaprecocliente();
				Materialtabelaprecoitem materialtabelaprecoitem;
				
				Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
				Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = new ListSet<Materialtabelaprecocliente>(Materialtabelaprecocliente.class);
				
				materialtabelapreco.setDtinicio(new Date(System.currentTimeMillis()));
				materialtabelapreco.setNome(nometabelapreco);
				
				materialtabelaprecocliente.setCliente(cliente);
				listaMaterialtabelaprecocliente.add(materialtabelaprecocliente);
				
				for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
					if(pedidovendamaterial.getMaterial() != null && !listaMaterialCriado.contains(pedidovendamaterial.getMaterial())){
						materialtabelaprecoitem = new Materialtabelaprecoitem();
						materialtabelaprecoitem.setMaterial(pedidovendamaterial.getMaterial());
						materialtabelaprecoitem.setUnidademedida(pedidovendamaterial.getUnidademedida());
						materialtabelaprecoitem.setPedidoVendaMaterial(pedidovendamaterial);
						
						if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setValor(pedidovendamaterial.getPreco());
							
							if(pedidovendamaterial.getMaterial().getValorvendamaximo() != null)
								materialtabelaprecoitem.setValorvendamaximo(pedidovendamaterial.getMaterial().getValorvendamaximo());
							if(pedidovendamaterial.getMaterial().getValorvendaminimo() != null)
								materialtabelaprecoitem.setValorvendaminimo(pedidovendamaterial.getMaterial().getValorvendaminimo());
						}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto() != null ? pedidovendamaterial.getPercentualdesconto() : 0d);
						}					
						
						listaMaterialCriado.add(pedidovendamaterial.getMaterial());
						listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
					}
				}
				
				materialtabelapreco.setListaMaterialtabelaprecocliente(listaMaterialtabelaprecocliente);
				materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
				
				this.saveOrUpdate(materialtabelapreco);
				materialTabelaPrecoItemHistoricoService.createHistorico(materialtabelapreco.getListaMaterialtabelaprecoitem(), materialtabelapreco.getTabelaprecotipo());
			}			
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoDAO#findByCliente(Cliente cliente, Date dtvenda)
	 *
	 * @param cliente
	 * @param dtvenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialtabelapreco> findByCliente(Cliente cliente, Date dtvenda, Empresa empresa, Boolean ativo, Tipocalculo tipocalculo) {
		return materialtabelaprecoDAO.findByCliente(cliente, dtvenda, empresa, ativo, tipocalculo);
	}
	
	public ModelAndView buscaMateriaisTabelaprecoByCliente(WebRequestContext request){
		Empresa empresa = null;
		String cdempresastr = request.getParameter("cdempresa");
		if(StringUtils.isNotBlank(cdempresastr)){
			try {
				empresa = new Empresa(Integer.parseInt(cdempresastr));
			} catch (Exception e) {}
		}
		Cliente cliente = null;
		String cdpessoastr = request.getParameter("cdpessoa");
		if(StringUtils.isNotBlank(cdpessoastr)){
			try {
				cliente = new Cliente(Integer.parseInt(cdpessoastr));
			} catch (Exception e) {}
		}
		return this.buscaMateriaisTabelaprecoByCliente(request, cliente, empresa);
	}
	
	/**
	 * M�todo que busca os materiais da tabela de preco do cliente
	 *
	 * @param request
	 * @param cliente
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView buscaMateriaisTabelaprecoByCliente(WebRequestContext request, Cliente cliente, Empresa empresa){
		
		String datastr = request.getParameter("data");
		
		java.sql.Date data = new java.sql.Date(System.currentTimeMillis());
		try {
			data = SinedDateUtils.stringToDate(datastr);
		} catch (Exception e) {}
		
		
		Set<Materialtabelaprecoitem> listamMaterialtabelaprecoitem = null;
		Prazopagamento prazopagamento = null;
		if(Util.objects.isPersistent(cliente) && "TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.TABELA_VENDA_CLIENTE))){
			List<Materialtabelapreco> listaMaterialtabelapreco = this.findByCliente(cliente, data,empresa, true, getTipocalculoByParametro());
			if(listaMaterialtabelapreco != null && !listaMaterialtabelapreco.isEmpty()){
				Materialtabelapreco materialtabelapreco = this.getMaterialtabelapreco(listaMaterialtabelapreco, empresa); 
				if(materialtabelapreco != null){
					ajustaItensTabelaPrecoUnidadeAtiva(materialtabelapreco);
					Set<Materialtabelaprecoitem> listaWhithUnidadeprincipal = materialtabelapreco.getListaMaterialtabelaprecoitemUnidadeprincipal();
					Set<Materialtabelaprecoitem> listaWhithUnidadesecundaria = materialtabelapreco.getListaMaterialtabelaprecoitemUnidadesecundaria();
					HashMap<Material, Materialtabelaprecoitem> mapMaterial = new HashMap<Material, Materialtabelaprecoitem>();
					
					if(listaWhithUnidadeprincipal != null && !listaWhithUnidadeprincipal.isEmpty()){
						for(Materialtabelaprecoitem materialtabelaprecoitem : listaWhithUnidadeprincipal){
							if(materialtabelaprecoitem.getMaterial() != null){
								if(mapMaterial.get(materialtabelaprecoitem.getMaterial()) == null){
									mapMaterial.put(materialtabelaprecoitem.getMaterial(), materialtabelaprecoitem);
								}
							}
						}
					}
					if(listaWhithUnidadesecundaria != null && !listaWhithUnidadesecundaria.isEmpty()){
						Materialunidademedida materialunidademedida = null;
						for(Materialtabelaprecoitem materialtabelaprecoitem : listaWhithUnidadesecundaria){
							if(materialtabelaprecoitem.getMaterial() != null){
								if(mapMaterial.get(materialtabelaprecoitem.getMaterial()) == null){
									materialunidademedida = materialunidademedidaService.getMaterialunidademedida(materialtabelaprecoitem.getMaterial(), materialtabelaprecoitem.getUnidademedida());
									if(materialunidademedida != null){
										materialtabelaprecoitem.setFracao(materialunidademedida.getFracao());
										materialtabelaprecoitem.setQtdereferencia(materialunidademedida.getQtdereferencia());
									}
									mapMaterial.put(materialtabelaprecoitem.getMaterial(), materialtabelaprecoitem);
								}
							}
						}
					}
					
					if(mapMaterial != null && mapMaterial.size() > 0){
						listamMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
						for(Material material : mapMaterial.keySet())
							listamMaterialtabelaprecoitem.add(mapMaterial.get(material));
					}
					
					prazopagamento = materialtabelapreco.getPrazopagamento();
				}
			}
			
		}
		
		return new JsonModelAndView()
				.addObject("lista", listamMaterialtabelaprecoitem != null ? listamMaterialtabelaprecoitem : "")
				.addObject("prazopagamento", prazopagamento != null ? prazopagamento : "");		
	}
	
	/**
	* M�todo que alterar a lista de itens da tabela de pre�o para ter apenas itens com unidade ativa ou nula
	*
	* @param materialtabelapreco
	* @since 25/09/2014
	* @author Luiz Fernando
	*/
	private void ajustaItensTabelaPrecoUnidadeAtiva(Materialtabelapreco materialtabelapreco) {
		if(materialtabelapreco != null && SinedUtil.isListNotEmpty(materialtabelapreco.getListaMaterialtabelaprecoitem())){
			Set<Materialtabelaprecoitem> itens = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);
			for(Materialtabelaprecoitem item : materialtabelapreco.getListaMaterialtabelaprecoitem()){
				if(item.getUnidademedida() == null || (item.getUnidademedida().getAtivo() != null && 
						item.getUnidademedida().getAtivo())){
					itens.add(item);
				}
			}
			materialtabelapreco.setListaMaterialtabelaprecoitem(itens);
		}		
	}
	
	/**
	 * M�todo que retorna a tabela de pre�o da empresa, ou tabela sem empresa
	 *
	 * @param listMaterialtabelapreco
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialtabelapreco getMaterialtabelapreco(List<Materialtabelapreco> listMaterialtabelapreco, Empresa empresa){
		Materialtabelapreco materialtabelapreco = null;
		boolean achoutabelaempresa = false;
		if(listMaterialtabelapreco != null && !listMaterialtabelapreco.isEmpty()){
			if(listMaterialtabelapreco.size() > 1 && empresa != null){
				for(Materialtabelapreco bean : listMaterialtabelapreco){
					if(bean.getListaMaterialtabelaprecoempresa() != null && !bean.getListaMaterialtabelaprecoempresa().isEmpty()){
						for(Materialtabelaprecoempresa materialtabelaprecoempresa : bean.getListaMaterialtabelaprecoempresa()){
							if(materialtabelaprecoempresa.getEmpresa() != null && materialtabelaprecoempresa.getEmpresa().getCdpessoa() != null &&
									materialtabelaprecoempresa.getEmpresa().equals(empresa)){
								materialtabelapreco = bean;
								achoutabelaempresa = true;
								break;
							}
						}
					}
					if(achoutabelaempresa) break;
				}
			}
			if(!achoutabelaempresa){
				materialtabelapreco = listMaterialtabelapreco.get(0);
			}
		}
		return materialtabelapreco;
	}
	
	/**
	 * Busca os dados para o cache da tela de pedido de venda offline
     * @author Luiz Fernando
	 * @date 20/04/2012
	 */
	public List<MaterialtabelaprecoOfflineJSON> findForPVOffline() {
		List<MaterialtabelaprecoOfflineJSON> lista = new ArrayList<MaterialtabelaprecoOfflineJSON>();
		for(Materialtabelapreco materialtabelapreco : materialtabelaprecoDAO.findForPVOffline())
			lista.add(new MaterialtabelaprecoOfflineJSON(materialtabelapreco));
		
		return lista;
	}
	
	/**
	 * M�todo que atualiza os materias da tabela de pre�o de acordo com a ultima venda do cliente.
	 * 
	 * @author Filipe Santos
	 * @param venda
	 * @param listaMaterialtabelapreco
	 */
	public void atualizaTabelaprecoByCliente(Venda venda, List<Materialtabelapreco> listaMaterialtabelapreco) {
		for (Materialtabelapreco materialtabelapreco : listaMaterialtabelapreco) {
			String paramMaterialMestreTabelaPreco = parametrogeralService.getValorPorNome("MATERIAL_MESTRE_TABELAPRECO");
			List<Material> listaMaterialCriado = new ArrayList<Material>();
			
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);			
			if(materialtabelapreco.getListaMaterialtabelaprecoitem() != null && !materialtabelapreco.getListaMaterialtabelaprecoitem().isEmpty()){
				listaMaterialtabelaprecoitem.addAll(materialtabelapreco.getListaMaterialtabelaprecoitem());
				for(Materialtabelaprecoitem mtpi : materialtabelapreco.getListaMaterialtabelaprecoitem()){
					if(mtpi.getMaterial() != null && !listaMaterialCriado.contains(mtpi.getMaterial())){
						listaMaterialCriado.add(mtpi.getMaterial());
					}
				}
			}
			for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
				if(vendamaterial.getMaterial() != null){
					Material aux = materialService.loadWithMaterialMestre(vendamaterial.getMaterial(), true);
					
					// se o parametro for TRUE, o processo de criacao/atualizacao na tabela de preco utiliza o material mestre
					if(paramMaterialMestreTabelaPreco.equals("TRUE") && aux.getMaterialmestregrade() != null) {
						if(!listaMaterialCriado.contains(aux.getMaterialmestregrade())) {
							incluirMaterialTabelaPrecoItemVenda(aux.getMaterialmestregrade(), vendamaterial, listaMaterialCriado, listaMaterialtabelaprecoitem, materialtabelapreco.getTabelaprecotipo());
						} else {
							if(vendamaterial.getDesconto() == null || vendamaterial.getDesconto().getValue().doubleValue() == 0 || Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
								for(Materialtabelaprecoitem mtpi : listaMaterialtabelaprecoitem){
									atualizarMaterialTabelaPrecoItemVenda(aux.getMaterialmestregrade(), mtpi, vendamaterial, materialtabelapreco.getTabelaprecotipo());
								}
							}
						}						
					} else if(!listaMaterialCriado.contains(vendamaterial.getMaterial())){
						incluirMaterialTabelaPrecoItemVenda(null, vendamaterial, listaMaterialCriado, listaMaterialtabelaprecoitem, materialtabelapreco.getTabelaprecotipo());
					} else {
						if(vendamaterial.getDesconto() == null || vendamaterial.getDesconto().getValue().doubleValue() == 0 || Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							for(Materialtabelaprecoitem mtpi : listaMaterialtabelaprecoitem){
								atualizarMaterialTabelaPrecoItemVenda(null, mtpi, vendamaterial, materialtabelapreco.getTabelaprecotipo());
							}
						}
					}
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaMaterialtabelaprecoitem)){
				for(Materialtabelaprecoitem materialtabelaprecoitem : listaMaterialtabelaprecoitem){
					if(materialtabelaprecoitem.getCdmaterialtabelaprecoitem() == null){
						materialtabelaprecoitem.setMaterialtabelapreco(materialtabelapreco);
						materialtabelaprecoitemService.saveOrUpdate(materialtabelaprecoitem);
					}else if(Boolean.TRUE.equals(materialtabelaprecoitem.getAtualizarValor())){
						if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitemService.updatePercentualDesconto(materialtabelaprecoitem, materialtabelaprecoitem.getPercentualdesconto());
						}else {
							materialtabelaprecoitemService.updatePreco(materialtabelaprecoitem, materialtabelaprecoitem.getValor());
						}
					}
				}
			}
			materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
//			this.saveOrUpdate(materialtabelapreco);	
		}		
	}
	
	public void atualizaTabelaprecoByCliente(Vendaorcamento vendaorcamento, List<Materialtabelapreco> listaMaterialtabelapreco) {
		for (Materialtabelapreco materialtabelapreco : listaMaterialtabelapreco) {
			String paramMaterialMestreTabelaPreco = parametrogeralService.getValorPorNome("MATERIAL_MESTRE_TABELAPRECO");
			List<Material> listaMaterialCriado = new ArrayList<Material>();
			
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);			
			if(materialtabelapreco.getListaMaterialtabelaprecoitem() != null && !materialtabelapreco.getListaMaterialtabelaprecoitem().isEmpty()){
				listaMaterialtabelaprecoitem.addAll(materialtabelapreco.getListaMaterialtabelaprecoitem());
				for(Materialtabelaprecoitem mtpi : materialtabelapreco.getListaMaterialtabelaprecoitem()){
					if(mtpi.getMaterial() != null && !listaMaterialCriado.contains(mtpi.getMaterial())){
						listaMaterialCriado.add(mtpi.getMaterial());
					}
				}
			}
			for(Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
				if(vendaorcamentomaterial.getMaterial() != null){
					Material aux = materialService.loadWithMaterialMestre(vendaorcamentomaterial.getMaterial(), true);
					
					if(paramMaterialMestreTabelaPreco.equals("TRUE") && aux.getMaterialmestregrade() != null) {
						if(!listaMaterialCriado.contains(aux.getMaterialmestregrade())) {
							incluirMaterialTabelaPrecoItemOrcamento(aux.getMaterialmestregrade(), vendaorcamentomaterial, listaMaterialCriado, listaMaterialtabelaprecoitem, materialtabelapreco.getTabelaprecotipo());
						} else {
							if(vendaorcamentomaterial.getDesconto() == null || vendaorcamentomaterial.getDesconto().getValue().doubleValue() == 0 || Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
								for(Materialtabelaprecoitem mtpi : listaMaterialtabelaprecoitem){
									atualizarMaterialTabelaPrecoItemOrcamento(aux.getMaterialmestregrade(), mtpi, vendaorcamentomaterial, materialtabelapreco.getTabelaprecotipo());
								}
							}
						}						
					} else if(!listaMaterialCriado.contains(vendaorcamentomaterial.getMaterial())){
						incluirMaterialTabelaPrecoItemOrcamento(null, vendaorcamentomaterial, listaMaterialCriado, listaMaterialtabelaprecoitem, materialtabelapreco.getTabelaprecotipo());
					}else {
						if(vendaorcamentomaterial.getDesconto() == null || vendaorcamentomaterial.getDesconto().getValue().doubleValue() == 0){
							for(Materialtabelaprecoitem mtpi : listaMaterialtabelaprecoitem){
								if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
									if(mtpi.getMaterial() != null && mtpi.getValor() != null && mtpi.getMaterial().equals(vendaorcamentomaterial.getMaterial())){
										boolean atualizarValor = false;
										if(mtpi.getUnidademedida() != null){
											if(mtpi.getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) && vendaorcamentomaterial.getPreco() != null &&
													vendaorcamentomaterial.getPreco() > mtpi.getValor()){
												atualizarValor = true;
											}
										}else {
											if(vendaorcamentomaterial.getPreco() != null &&	vendaorcamentomaterial.getPreco() > mtpi.getValor()){
												atualizarValor = true;
											}
										}
										
										if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALOR_MENOR_TABELA_PRECO))){
											atualizarValor = true;
										}	
										if(atualizarValor){
											mtpi.setAtualizarValor(true);
											mtpi.setValor(vendaorcamentomaterial.getPreco());
											this.setaVendaOrcamentoMaterial(mtpi, vendaorcamentomaterial);
										}
									}
								}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
									if(mtpi.getMaterial() != null && mtpi.getMaterial().equals(vendaorcamentomaterial.getMaterial())){
										boolean atualizarValor = false;
										if(mtpi.getUnidademedida() != null){
											if(mtpi.getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) &&
													(vendaorcamentomaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || vendaorcamentomaterial.getPercentualdesconto() < mtpi.getPercentualdesconto())){
												atualizarValor = true;
											}
										}else {
											if(vendaorcamentomaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || vendaorcamentomaterial.getPercentualdesconto() < mtpi.getPercentualdesconto()){
												atualizarValor = true;
											}
										}
										
										if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_PERCENTUAL_MAIOR_TABELA_PRECO))){
											atualizarValor = true;
										}	
										if(atualizarValor){
											mtpi.setAtualizarValor(true);
											mtpi.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto() != null ? vendaorcamentomaterial.getPercentualdesconto() : 0d);
											this.setaVendaOrcamentoMaterial(mtpi, vendaorcamentomaterial);
										}
									}
								}
							}
						}
					}
				}
			}			
			if(SinedUtil.isListNotEmpty(listaMaterialtabelaprecoitem)){
				for(Materialtabelaprecoitem materialtabelaprecoitem : listaMaterialtabelaprecoitem){
					if(materialtabelaprecoitem.getCdmaterialtabelaprecoitem() == null){
						materialtabelaprecoitem.setMaterialtabelapreco(materialtabelapreco);
						materialtabelaprecoitemService.saveOrUpdate(materialtabelaprecoitem);
					}else if(Boolean.TRUE.equals(materialtabelaprecoitem.getAtualizarValor())){
						if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitemService.updatePercentualDesconto(materialtabelaprecoitem, materialtabelaprecoitem.getPercentualdesconto());
						}else {
							materialtabelaprecoitemService.updatePreco(materialtabelaprecoitem, materialtabelaprecoitem.getValor());
						}
					}
				}
			}
			materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
//			this.saveOrUpdate(materialtabelapreco);	
		}		
	}
	
	public void atualizaTabelaprecoByCliente(Pedidovenda pedidovenda, List<Materialtabelapreco> listaMaterialtabelapreco) {
		for (Materialtabelapreco materialtabelapreco : listaMaterialtabelapreco) {
			List<Material> listaMaterialCriado = new ArrayList<Material>();
			
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ListSet<Materialtabelaprecoitem>(Materialtabelaprecoitem.class);			
			if(materialtabelapreco.getListaMaterialtabelaprecoitem() != null && !materialtabelapreco.getListaMaterialtabelaprecoitem().isEmpty()){
				listaMaterialtabelaprecoitem.addAll(materialtabelapreco.getListaMaterialtabelaprecoitem());
				for(Materialtabelaprecoitem mtpi : materialtabelapreco.getListaMaterialtabelaprecoitem()){
					if(mtpi.getMaterial() != null && !listaMaterialCriado.contains(mtpi.getMaterial())){
						listaMaterialCriado.add(mtpi.getMaterial());
					}
				}
			}
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				if(pedidovendamaterial.getMaterial() != null){
					if(!listaMaterialCriado.contains(pedidovendamaterial.getMaterial())){
						Materialtabelaprecoitem materialtabelaprecoitem = new Materialtabelaprecoitem();
						materialtabelaprecoitem.setMaterial(pedidovendamaterial.getMaterial());
						materialtabelaprecoitem.setUnidademedida(pedidovendamaterial.getUnidademedida());
						
						if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setValor(pedidovendamaterial.getPreco());
							if(pedidovendamaterial.getMaterial().getValorvendamaximo() != null){
								materialtabelaprecoitem.setValorvendamaximo(pedidovendamaterial.getMaterial().getValorvendamaximo());
							}
							if(pedidovendamaterial.getMaterial().getValorvendaminimo() != null){
								materialtabelaprecoitem.setValorvendaminimo(pedidovendamaterial.getMaterial().getValorvendaminimo());
							}
						}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitem.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto());
						} 
						
						listaMaterialCriado.add(pedidovendamaterial.getMaterial());
						listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
					}else {
						if(pedidovendamaterial.getDesconto() == null || pedidovendamaterial.getDesconto().getValue().doubleValue() == 0 || Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							for(Materialtabelaprecoitem mtpi : listaMaterialtabelaprecoitem){
								if(Tipocalculo.EM_VALOR.equals(materialtabelapreco.getTabelaprecotipo())){
									if(mtpi.getMaterial() != null && mtpi.getValor() != null && mtpi.getMaterial().equals(pedidovendamaterial.getMaterial())){
										boolean atualizarValor = false;
										if(mtpi.getUnidademedida() != null){
											if(mtpi.getUnidademedida().equals(pedidovendamaterial.getUnidademedida()) && pedidovendamaterial.getPreco() != null &&
													pedidovendamaterial.getPreco() > mtpi.getValor()){
												atualizarValor = true;
											}
										}else {
											if(pedidovendamaterial.getPreco() != null &&	pedidovendamaterial.getPreco() > mtpi.getValor()){
												atualizarValor = true;
											}
										}
										if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALOR_MENOR_TABELA_PRECO))){
											atualizarValor = true;
										}		
										if(atualizarValor){
											mtpi.setAtualizarValor(true);
											mtpi.setValor(pedidovendamaterial.getPreco());
											this.setaPedidoVendaMaterial(mtpi, pedidovendamaterial);
										}
									}
								}else if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
									if(mtpi.getMaterial() != null && mtpi.getMaterial().equals(pedidovendamaterial.getMaterial())){
										boolean atualizarValor = false;
										if(mtpi.getUnidademedida() != null){
											if(mtpi.getUnidademedida().equals(pedidovendamaterial.getUnidademedida()) && 
													(pedidovendamaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || pedidovendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto())){
												atualizarValor = true;
											}
										}else {
											if(pedidovendamaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || pedidovendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto()){
												atualizarValor = true;
											}
										}
										if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_PERCENTUAL_MAIOR_TABELA_PRECO))){
											atualizarValor = true;
										}		
										if(atualizarValor){
											mtpi.setAtualizarValor(true);
											mtpi.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto() != null ? pedidovendamaterial.getPercentualdesconto() : 0d);
											this.setaPedidoVendaMaterial(mtpi, pedidovendamaterial);
										}
									}
								}
							}
						}
					}
				}
			}			
			if(SinedUtil.isListNotEmpty(listaMaterialtabelaprecoitem)){
				for(Materialtabelaprecoitem materialtabelaprecoitem : listaMaterialtabelaprecoitem){
					if(materialtabelaprecoitem.getCdmaterialtabelaprecoitem() == null){
						materialtabelaprecoitem.setMaterialtabelapreco(materialtabelapreco);
						materialtabelaprecoitemService.saveOrUpdate(materialtabelaprecoitem);
					}else if(Boolean.TRUE.equals(materialtabelaprecoitem.getAtualizarValor())){
						if(Tipocalculo.PERCENTUAL.equals(materialtabelapreco.getTabelaprecotipo())){
							materialtabelaprecoitemService.updatePercentualDesconto(materialtabelaprecoitem, materialtabelaprecoitem.getPercentualdesconto());
						}else {
							materialtabelaprecoitemService.updatePreco(materialtabelaprecoitem, materialtabelaprecoitem.getValor());
						}
					}
				}
			}
			materialtabelapreco.setListaMaterialtabelaprecoitem(listaMaterialtabelaprecoitem);
//			this.saveOrUpdate(materialtabelapreco);	
		}		
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa) {
		return this.getTabelaPrecoAtual(material, cliente, prazopagamento, pedidovendatipo, empresa, null, null);
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Material materialKit, Materialtabelapreco materialtabelaprecoPesquisa) {
		return this.getTabelaPrecoAtual(SinedDateUtils.currentDate(), material, cliente, prazopagamento, pedidovendatipo, empresa, materialKit, false, materialtabelaprecoPesquisa);
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Date data, Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, boolean withoutMaterial, Materialtabelapreco materialtabelaprecoPesquisa) {
		return this.getTabelaPrecoAtual(data, material, cliente, prazopagamento, pedidovendatipo, empresa, null, withoutMaterial, materialtabelaprecoPesquisa);
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Date data, Materialgrupo materialgrupo, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Cliente cliente, boolean withoutMaterial) {
		return materialtabelaprecoDAO.getTabelaPrecoAtual(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial);
	}
	
	public Materialtabelapreco getTabelaPrecoAtual(Date data, Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Material materialKit, boolean withoutMaterial, Materialtabelapreco materialtabelaprecoPesquisa) {
		Materialgrupo materialgrupo = null;
		List<Categoria> listaCategoria = null;
		
		if(material != null && material.getCdmaterial() != null){
			Material m = materialService.findWithMaterialgrupo(materialKit != null && materialKit.getCdmaterial() != null ? materialKit : material);
			if(m != null && m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
				materialgrupo = m.getMaterialgrupo();
			}
		}
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
			if(listaPessoacategoria != null && !listaPessoacategoria.isEmpty()){
				listaCategoria = new ArrayList<Categoria>();
				for(Pessoacategoria pessoacategoria : listaPessoacategoria){
					if(pessoacategoria.getCategoria() != null && pessoacategoria.getCdpessoacategoria() != null){
						listaCategoria.add(pessoacategoria.getCategoria());
					}
				}
			}
		}
		
		List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoDAO.findTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, withoutMaterial);
		
		Materialtabelapreco tabelapreco = null; 
		if(materialtabelaprecoPesquisa != null){
			tabelapreco = getTabelaPrecoAtualPrincipalOtimizado(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(tabelapreco != null){
				return tabelapreco;
			}
		}
		
		tabelapreco = getTabelaPrecoAtualPrincipalOtimizado(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, null);;
		
		return tabelapreco;
	}
	
	public Materialtabelapreco getTabelaPrecoAtualPrincipalOtimizado(Date data, Materialgrupo materialgrupo, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Cliente cliente, boolean withoutMaterial, List<Materialtabelapreco> listaMaterialtabelapreco, Materialtabelapreco materialtabelaprecoPesquisa){
	
		Materialtabelapreco materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, null, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, prazopagamento, null, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, null, pedidovendatipo, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, prazopagamento, pedidovendatipo, empresa, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, null, null, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, listaCategoria, null, null, null, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		
		
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, null, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, prazopagamento, pedidovendatipo, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, null, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, prazopagamento, null, empresa, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, null, empresa, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, prazopagamento, null, null, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, null, null, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, pedidovendatipo, empresa, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, null, pedidovendatipo, null, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, materialgrupo, null, prazopagamento, null, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		
		if(materialtabelaprecoPesquisa != null){
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, prazopagamento, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, null, pedidovendatipo, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, null, null, empresa, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, prazopagamento, pedidovendatipo, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, null, null, null, cliente, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, null, null, empresa, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
			materialtabelapreco = this.getTabelaPrecoAtualOtimizado(data, null, listaCategoria, null, null, null, null, withoutMaterial, listaMaterialtabelapreco, materialtabelaprecoPesquisa);
			if(materialtabelapreco != null){
				return materialtabelapreco;
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * Este m�todo tem a mesma regra que o m�todo getTabelaPrecoAtual, s� que � passada a lista listaMaterialtabelapreco que foi carregada no BD anteriormente, assim s� acessando o BD uma vez ao inv�s de N vezes. 
	 * 
	 * @param data
	 * @param materialgrupo
	 * @param listaCategoria
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @param empresa
	 * @param cliente
	 * @param withoutMaterial
	 * @param listaMaterialtabelapreco
	 * @author Thiago Clemente
	 */
	private Materialtabelapreco getTabelaPrecoAtualOtimizado(Date data, Materialgrupo materialgrupo, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Cliente cliente, boolean withoutMaterial, List<Materialtabelapreco> listaMaterialtabelapreco, Materialtabelapreco materialtabelaprecoPesquisa){
		if(!withoutMaterial){
			//caso o grupo de material e a categoria sejam nulos, a busca deve ser feita pelo material 
			if(materialgrupo == null && (listaCategoria == null || listaCategoria.isEmpty()))
				return null;
		}
		
		List<Materialtabelapreco> listaEncontrado = new ArrayList<Materialtabelapreco>();
		for (Iterator<Materialtabelapreco> iterator = listaMaterialtabelapreco.iterator(); iterator.hasNext();) {
			Materialtabelapreco materialtabelapreco = iterator.next();
			boolean add = true;
			boolean achou = true;
			Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = materialtabelapreco.getListaMaterialtabelaprecocliente();
			Set<Materialtabelaprecoempresa> listaMaterialtabelaprecoempresa = materialtabelapreco.getListaMaterialtabelaprecoempresa();
			
			if (materialtabelaprecoPesquisa!=null && materialtabelaprecoPesquisa.getCdmaterialtabelapreco()!=null){
//				query.where("materialgrupo = ?", materialgrupo);
				add = add && materialtabelapreco.getCdmaterialtabelapreco()!=null && materialtabelapreco.getCdmaterialtabelapreco().equals(materialtabelaprecoPesquisa.getCdmaterialtabelapreco());
			}
			
			if (cliente!=null && cliente.getCdpessoa()!=null){
//				query.where("cliente = ?", cliente);
				achou = false;
				if (listaMaterialtabelaprecocliente!=null){
					for (Materialtabelaprecocliente materialtabelaprecocliente: listaMaterialtabelaprecocliente){
						if (materialtabelaprecocliente.getCliente()!=null && materialtabelaprecocliente.getCliente().equals(cliente)){
							achou = true;
							break;
						}
					}
				}
				add = add && achou;
			} else {
//				query.where("cliente is null");
				add = add && (listaMaterialtabelaprecocliente==null || listaMaterialtabelaprecocliente.isEmpty());
			}
			
			if (materialgrupo!=null && materialgrupo.getCdmaterialgrupo()!=null){
//				query.where("materialgrupo = ?", materialgrupo);
				add = add && materialtabelapreco.getMaterialgrupo()!=null && materialtabelapreco.getMaterialgrupo().equals(materialgrupo);
			} else {
//				query.where("materialgrupo is null");
				add = add && materialtabelapreco.getMaterialgrupo()==null;
			}
			
			if (listaCategoria!=null && !listaCategoria.isEmpty()){
//				query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
				achou = false;
				for (Categoria categoria: listaCategoria){
					if (materialtabelapreco.getCategoria()!=null && materialtabelapreco.getCategoria().equals(categoria)){
						achou = true;
						break;
					}
				}
				add = add && achou;
			} else {
//				query.where("categoria is null");
				add = add && materialtabelapreco.getCategoria()==null;
			}
			
			if (prazopagamento!=null && prazopagamento.getCdprazopagamento()!=null){
//				query.where("prazopagamento = ?", prazopagamento);
				add = add && materialtabelapreco.getPrazopagamento()!=null && materialtabelapreco.getPrazopagamento().equals(prazopagamento);
			} else {
//				query.where("prazopagamento is null");
				add = add && materialtabelapreco.getPrazopagamento()==null;
			}
			
			if (pedidovendatipo!=null && pedidovendatipo.getCdpedidovendatipo()!=null){
//				query.where("pedidovendatipo = ?", pedidovendatipo);
				add = add && materialtabelapreco.getPedidovendatipo()!=null && materialtabelapreco.getPedidovendatipo().equals(pedidovendatipo);
			} else {
//				query.where("pedidovendatipo is null");
				add = add && materialtabelapreco.getPedidovendatipo()==null;
			}
			
			if (empresa!=null && empresa.getCdpessoa()!=null){
//				query.where("empresa = ?", empresa);
				achou = false;
				if (listaMaterialtabelaprecoempresa!=null){
					for (Materialtabelaprecoempresa materialtabelaprecoempresa: listaMaterialtabelaprecoempresa){
						if (materialtabelaprecoempresa.getEmpresa()!=null && materialtabelaprecoempresa.getEmpresa().equals(empresa)){
							achou = true;
							break;
						}
					}
				}
				add = add && achou;
			} else {
//				query.where("empresa is null");
				add = add && (listaMaterialtabelaprecoempresa==null || listaMaterialtabelaprecoempresa.isEmpty());
			}
			
//			if (!add){
//				iterator.remove();
//			}
			if(add){
				listaEncontrado.add(materialtabelapreco);
			}
		}
		
		if (listaEncontrado!=null && listaEncontrado.size()==1){
			return listaEncontrado.get(0);
		} else {
			return null;
		}
	}
	
	public Materialtabelapreco getTabelaPrecoAtualForContrato(Material material, Cliente cliente, Frequencia frequencia, Empresa empresa) {
		Materialgrupo materialgrupo = null;
		List<Categoria> listaCategoria = null;
		
		if(material != null && material.getCdmaterial() != null){
			Material m = materialService.findWithMaterialgrupo(material);
			if(m != null && m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
				materialgrupo = m.getMaterialgrupo();
			}
		}
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
			if(listaPessoacategoria != null && !listaPessoacategoria.isEmpty()){
				listaCategoria = new ArrayList<Categoria>();
				for(Pessoacategoria pessoacategoria : listaPessoacategoria){
					if(pessoacategoria.getCategoria() != null && pessoacategoria.getCdpessoacategoria() != null){
						listaCategoria.add(pessoacategoria.getCategoria());
					}
				}
			}
		}
		
		Materialtabelapreco materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, listaCategoria, frequencia, empresa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, null, frequencia, empresa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, listaCategoria, null, empresa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, listaCategoria, frequencia, null);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, null, frequencia, empresa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		materialtabelapreco = this.getTabelaPrecoAtualForContrato(materialgrupo, listaCategoria, null, empresa);
		if(materialtabelapreco != null){
			return materialtabelapreco;
		}
		
		return null;
	}
	
	private Materialtabelapreco getTabelaPrecoAtualForContrato(Materialgrupo materialgrupo, List<Categoria> listaCategoria, Frequencia frequencia, Empresa empresa) {
		return materialtabelaprecoDAO.getTabelaPrecoAtualForContrato(materialgrupo, listaCategoria, frequencia, empresa);
	}
	
	public List<MaterialtabelaprecoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialtabelaprecoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialtabelaprecoRESTModel> lista = new ArrayList<MaterialtabelaprecoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialtabelapreco bean : materialtabelaprecoDAO.findForAndroid(whereIn))
				lista.add(new MaterialtabelaprecoRESTModel(bean));
		}
		
		return lista;
	}
	
	public boolean isPermitidoAlterarTabela(Materialtabelapreco materialtabelapreco){
		List<Cliente> listaClientes = clienteService.findByMaterialtabelapreco(materialtabelapreco);
		if(SinedUtil.isListNotEmpty(listaClientes)){
			if(SinedUtil.isListNotEmpty(clienteService.findClientesSemPermissaoClienteVendedor(CollectionsUtil.listAndConcatenate(listaClientes, "cdpessoa", ",")))){
				return false;
			}
		}
		return true;
	}
	
	public String getNomesTabelaPreco(List<Materialtabelapreco> listaMaterialtabelapreco) {
		StringBuilder sb = new StringBuilder();
		if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
			List<Integer> listaCdmaterialtabelapreco = new ArrayList<Integer>();
			for(Materialtabelapreco materialtabelapreco : listaMaterialtabelapreco){
				if(materialtabelapreco.getNome() != null && materialtabelapreco.getCdmaterialtabelapreco() != null && !listaCdmaterialtabelapreco.contains(materialtabelapreco.getCdmaterialtabelapreco())){
					sb.append("<br>ID: ").append(materialtabelapreco.getCdmaterialtabelapreco()).append(" Nome: ").append(materialtabelapreco.getNome());
					listaCdmaterialtabelapreco.add(materialtabelapreco.getCdmaterialtabelapreco());
				}
			}
		}
		return sb.toString();
	}
	
	public void incluirMaterialTabelaPrecoItemVenda(Material material, Vendamaterial vendamaterial, 
			List<Material> listaMaterialCriado, 
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem,
			Tipocalculo tipocalculo) {
		if(material == null) {
			material = vendamaterial.getMaterial();
		}
		
		Materialtabelaprecoitem materialtabelaprecoitem = new Materialtabelaprecoitem();
		materialtabelaprecoitem.setMaterial(material);
		materialtabelaprecoitem.setUnidademedida(vendamaterial.getUnidademedida());
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			materialtabelaprecoitem.setValor(vendamaterial.getPreco());
			
			if(material.getValorvendamaximo() != null)
				materialtabelaprecoitem.setValorvendamaximo(material.getValorvendamaximo());
	
			if(material.getValorvendaminimo() != null)
				materialtabelaprecoitem.setValorvendaminimo(material.getValorvendaminimo());
		}else if(Tipocalculo.PERCENTUAL.equals(tipocalculo)){
			materialtabelaprecoitem.setPercentualdesconto(vendamaterial.getPercentualdesconto() != null ? vendamaterial.getPercentualdesconto() : 0d);
		}
		
		listaMaterialCriado.add(material);
		listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
	}
	private void setaVendaMaterial(Materialtabelaprecoitem mtpi, Vendamaterial vendaMaterial){
		mtpi.setVendaMaterial(vendaMaterial);
		mtpi.setPedidoVendaMaterial(null);
		mtpi.setVendaOrcamentoMaterial(null);
	}
	
	private void setaPedidoVendaMaterial(Materialtabelaprecoitem mtpi, Pedidovendamaterial pedidoVendaMaterial){
		mtpi.setVendaMaterial(null);
		mtpi.setPedidoVendaMaterial(pedidoVendaMaterial);
		mtpi.setVendaOrcamentoMaterial(null);
	}
	
	private void setaVendaOrcamentoMaterial(Materialtabelaprecoitem mtpi, Vendaorcamentomaterial vendaOrcamentoMaterial){
		mtpi.setVendaMaterial(null);
		mtpi.setPedidoVendaMaterial(null);
		mtpi.setVendaOrcamentoMaterial(vendaOrcamentoMaterial);
	}
	
	public void atualizarMaterialTabelaPrecoItemVenda(Material material, Materialtabelaprecoitem mtpi, Vendamaterial vendamaterial, Tipocalculo tipocalculo) {
		if(material == null) {
			material = vendamaterial.getMaterial();
		}
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getValor() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarValor = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(vendamaterial.getUnidademedida()) && vendamaterial.getPreco() != null &&
							vendamaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				} else {
					if(vendamaterial.getPreco() != null &&	vendamaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALOR_MENOR_TABELA_PRECO))){
					atualizarValor = true;
				}	
				
				if(atualizarValor){
					mtpi.setAtualizarValor(true);
					mtpi.setValor(vendamaterial.getPreco());
					this.setaVendaMaterial(mtpi, vendamaterial);
				}
			}
		}else if(Tipocalculo.PERCENTUAL.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarPercentual = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(vendamaterial.getUnidademedida()) &&
							(vendamaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || vendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto())){
						atualizarPercentual = true;
					}
				} else {
					if(vendamaterial.getPercentualdesconto() == null ||	mtpi.getPercentualdesconto() == null || vendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto()){
						atualizarPercentual = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_PERCENTUAL_MAIOR_TABELA_PRECO))){
					atualizarPercentual = true;
				}	
				
				if(atualizarPercentual){
					mtpi.setAtualizarValor(true);
					mtpi.setPercentualdesconto(vendamaterial.getPercentualdesconto() != null ? vendamaterial.getPercentualdesconto() : 0d);
					this.setaVendaMaterial(mtpi, vendamaterial);
				}
			}
		} 
	}
	
	public void incluirMaterialTabelaPrecoItemOrcamento(Material material, Vendaorcamentomaterial vendaorcamentomaterial, 
			List<Material> listaMaterialCriado, 
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem,
			Tipocalculo tipocalculo) {
		if(material == null) {
			material = vendaorcamentomaterial.getMaterial();
		}
		
		Materialtabelaprecoitem materialtabelaprecoitem = new Materialtabelaprecoitem();
		materialtabelaprecoitem.setMaterial(material);
		materialtabelaprecoitem.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			materialtabelaprecoitem.setValor(vendaorcamentomaterial.getPreco());
			
			if(material.getValorvendamaximo() != null)
				materialtabelaprecoitem.setValorvendamaximo(material.getValorvendamaximo());
	
			if(material.getValorvendaminimo() != null)
				materialtabelaprecoitem.setValorvendaminimo(material.getValorvendaminimo());
		}else if(Tipocalculo.PERCENTUAL.equals(tipocalculo)){
			materialtabelaprecoitem.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto() != null ? vendaorcamentomaterial.getPercentualdesconto() : 0d);
		}
		
		listaMaterialCriado.add(material);
		listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
	}
	
	public void atualizarMaterialTabelaPrecoItemOrcamento(Material material, Materialtabelaprecoitem mtpi, Vendaorcamentomaterial vendaorcamentomaterial, Tipocalculo tipocalculo) {
		if(material == null) {
			material = vendaorcamentomaterial.getMaterial();
		}
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getValor() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarValor = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) && vendaorcamentomaterial.getPreco() != null &&
							vendaorcamentomaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				} else {
					if(vendaorcamentomaterial.getPreco() != null &&	vendaorcamentomaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALOR_MENOR_TABELA_PRECO))){
					atualizarValor = true;
				}	
				
				if(atualizarValor){
					mtpi.setAtualizarValor(true);
					mtpi.setValor(vendaorcamentomaterial.getPreco());
					this.setaVendaOrcamentoMaterial(mtpi, vendaorcamentomaterial);
				}
			}
		}else if(Tipocalculo.PERCENTUAL.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarValor = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(vendaorcamentomaterial.getUnidademedida()) && 
							(mtpi.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || vendaorcamentomaterial.getPercentualdesconto() < mtpi.getPercentualdesconto())){
						atualizarValor = true;
					}
				} else {
					if(vendaorcamentomaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || vendaorcamentomaterial.getPercentualdesconto() < mtpi.getPercentualdesconto()){
						atualizarValor = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_PERCENTUAL_MAIOR_TABELA_PRECO))){
					atualizarValor = true;
				}	
				
				if(atualizarValor){
					mtpi.setAtualizarValor(true);
					mtpi.setPercentualdesconto(vendaorcamentomaterial.getPercentualdesconto() != null ? vendaorcamentomaterial.getPercentualdesconto() : 0d);
					this.setaVendaOrcamentoMaterial(mtpi, vendaorcamentomaterial);
				}
			}
		}
	}
	
	public void incluirMaterialTabelaPrecoItemPedidovenda(Material material, Pedidovendamaterial pedidovendamaterial, 
			List<Material> listaMaterialCriado, 
			Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem,
			Tipocalculo tipocalculo) {
		if(material == null) {
			material = pedidovendamaterial.getMaterial();
		}
		
		Materialtabelaprecoitem materialtabelaprecoitem = new Materialtabelaprecoitem();
		materialtabelaprecoitem.setMaterial(material);
		materialtabelaprecoitem.setUnidademedida(pedidovendamaterial.getUnidademedida());
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			materialtabelaprecoitem.setValor(pedidovendamaterial.getPreco());
			
			if(material.getValorvendamaximo() != null)
				materialtabelaprecoitem.setValorvendamaximo(material.getValorvendamaximo());
	
			if(material.getValorvendaminimo() != null)
				materialtabelaprecoitem.setValorvendaminimo(material.getValorvendaminimo());
		}else if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			materialtabelaprecoitem.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto() != null ? pedidovendamaterial.getPercentualdesconto() : 0d);
		}
		
		listaMaterialCriado.add(material);
		listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
	}
	
	public void atualizarMaterialTabelaPrecoItemPedidovenda(Material material, Materialtabelaprecoitem mtpi, Pedidovendamaterial pedidovendamaterial, Tipocalculo tipocalculo) {
		if(material == null) {
			material = pedidovendamaterial.getMaterial();
		}
		
		if(Tipocalculo.EM_VALOR.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getValor() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarValor = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(pedidovendamaterial.getUnidademedida()) && pedidovendamaterial.getPreco() != null &&
							pedidovendamaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				} else {
					if(pedidovendamaterial.getPreco() != null && pedidovendamaterial.getPreco() > mtpi.getValor()){
						atualizarValor = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_VALOR_MENOR_TABELA_PRECO))){
					atualizarValor = true;
				}	
				
				if(atualizarValor){
					mtpi.setAtualizarValor(true);
					mtpi.setValor(pedidovendamaterial.getPreco());
					this.setaPedidoVendaMaterial(mtpi, pedidovendamaterial);
				}
			}
		}else if(Tipocalculo.PERCENTUAL.equals(tipocalculo)){
			if(mtpi.getMaterial() != null && mtpi.getMaterial().equals(material)){
				boolean atualizarValor = false;
	
				if(mtpi.getUnidademedida() != null){
					if(mtpi.getUnidademedida().equals(pedidovendamaterial.getUnidademedida()) && 
							(pedidovendamaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || pedidovendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto())){
						atualizarValor = true;
					}
				} else {
					if(pedidovendamaterial.getPercentualdesconto() == null || mtpi.getPercentualdesconto() == null || pedidovendamaterial.getPercentualdesconto() < mtpi.getPercentualdesconto()){
						atualizarValor = true;
					}
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZA_PERCENTUAL_MAIOR_TABELA_PRECO))){
					atualizarValor = true;
				}	
				
				if(atualizarValor){
					mtpi.setAtualizarValor(true);
					mtpi.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto() != null ? pedidovendamaterial.getPercentualdesconto() : 0d);
					this.setaPedidoVendaMaterial(mtpi, pedidovendamaterial);
				}
			}
		}
	}
	
	/**
	 * M�todo que retorna o tipo de calculo de acordo com o par�metro TIPO_TABELA_VENDA_CLIENTE
	 */
	public Tipocalculo getTipocalculoByParametro(){
		if("VALOR".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.TIPO_TABELA_VENDA_CLIENTE))){
			return Tipocalculo.EM_VALOR;
		}else if("PERCENTUAL".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.TIPO_TABELA_VENDA_CLIENTE))){
			return Tipocalculo.PERCENTUAL;
		}
		return Tipocalculo.EM_VALOR; 
	}
}
