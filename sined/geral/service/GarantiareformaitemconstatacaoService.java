package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Garantiareformaitemconstatacao;
import br.com.linkcom.sined.geral.dao.GarantiareformaitemconstatacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GarantiareformaitemconstatacaoService extends GenericService<Garantiareformaitemconstatacao>{
	
	private GarantiareformaitemconstatacaoDAO garantiareformaitemconstatacaoDAO;
	
	public void setGarantiareformaitemconstatacaoDAO(GarantiareformaitemconstatacaoDAO garantiareformaitemconstatacaoDAO) {
		this.garantiareformaitemconstatacaoDAO = garantiareformaitemconstatacaoDAO;
	}
	
	public void delete(Garantiareformaitem garantiareformaitem){
		garantiareformaitemconstatacaoDAO.delete(garantiareformaitem);
	}	
}
