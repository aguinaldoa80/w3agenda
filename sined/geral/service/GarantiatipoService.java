package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.dao.GarantiatipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.garantiatipo.GarantiaTipoW3producaoRESTModel;

public class GarantiatipoService extends GenericService<Garantiatipo>{

	private GarantiatipoDAO garantiatipoDAO;
	public void setGarantiatipoDAO(GarantiatipoDAO garantiatipoDAO) {
		this.garantiatipoDAO = garantiatipoDAO;
	}
	
	public List<Garantiatipo> findByDescricao(String descricao, Integer cdgarantiatipoIgnore){
		return garantiatipoDAO.findByDescricao(descricao, cdgarantiatipoIgnore);
	}
	
	public List<GarantiaTipoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<GarantiaTipoW3producaoRESTModel> lista = new ArrayList<GarantiaTipoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Garantiatipo garantiatipo : garantiatipoDAO.findForW3Producao(whereIn))
				lista.add(new GarantiaTipoW3producaoRESTModel(garantiatipo));
		}
		
		return lista;
	}
}
