package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoItemHistorico;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.dao.MaterialtabelaprecoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialtabelaprecoitem.MaterialtabelaprecoitemRESTModel;

public class MaterialtabelaprecoitemService extends GenericService<Materialtabelaprecoitem> {

	private MaterialtabelaprecoitemDAO materialtabelaprecoitemDAO;
	private PessoacategoriaService pessoacategoriaService;
	private MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService;
	
	public void setMaterialtabelaprecoitemDAO(
			MaterialtabelaprecoitemDAO materialtabelaprecoitemDAO) {
		this.materialtabelaprecoitemDAO = materialtabelaprecoitemDAO;
	}
	public void setPessoacategoriaService(PessoacategoriaService pessoacategoriaService) {
		this.pessoacategoriaService = pessoacategoriaService;
	}
	public void setMaterialTabelaPrecoItemHistoricoService(
			MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService) {
		this.materialTabelaPrecoItemHistoricoService = materialTabelaPrecoItemHistoricoService;
	}

	/* singleton */
	private static MaterialtabelaprecoitemService instance;
	public static MaterialtabelaprecoitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialtabelaprecoitemService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoitemDAO.getItemTabelaPrecoAtual(Material material, Cliente cliente)
	 *
	 * @param material
	 * @param cliente
	 * @param prazopagamento 
	 * @param pedidovendatipo 
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 * @param listaCategoriaCliente 
	 * @param empresa 
	 */
	public Materialtabelaprecoitem getItemTabelaPrecoAtual(Material material, Cliente cliente, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida){
		return materialtabelaprecoitemDAO.getItemTabelaPrecoAtual(material, cliente, listaCategoria, prazopagamento, pedidovendatipo, empresa, unidademedida);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoitemDAO#getItemTabelaPrecoAtual(Material material, Materialtabelapreco materialtabelapreco)
	 *
	 * @param material
	 * @param materialtabelapreco
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialtabelaprecoitem getItemTabelaPrecoAtual(Material material, Unidademedida unidademedidaEscolhida, Materialtabelapreco materialtabelapreco) {
		List<Materialtabelaprecoitem> lista = null; 
		
		if(unidademedidaEscolhida != null && unidademedidaEscolhida.getCdunidademedida() != null){
			lista = findTabelaPrecoAtual(material, unidademedidaEscolhida, materialtabelapreco);
		}
		
		if(lista == null || lista.isEmpty()){
			lista = findTabelaPrecoAtual(material, null, materialtabelapreco);
		}
		
		if(lista != null && !lista.isEmpty()){
			return lista.get(0);
		}else {
			return null;
		}
	}
	
	/**
	 * Este m�todo tem a mesma regra que o m�todo getItemTabelaPrecoAtual, s� que � passada a lista listaMaterialtabelaprecoitem que foi carregada no BD anteriormente, assim s� acessando o BD uma vez ao inv�s de N vezes.
	 * 
	 * @param material
	 * @param cliente
	 * @param listaCategoria
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @param empresa
	 * @param unidademedida
	 * @param listaMaterialtabelaprecoitem
	 * @return
	 */
	private Materialtabelaprecoitem getItemTabelaPrecoAtualOtimizado(Material material, Cliente cliente, List<Categoria> listaCategoria, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida, List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem) {
		List<Materialtabelaprecoitem> listaEncontrado = new ArrayList<Materialtabelaprecoitem>();
		for (Iterator<Materialtabelaprecoitem> iterator = listaMaterialtabelaprecoitem.iterator(); iterator.hasNext();) {
			Materialtabelaprecoitem materialtabelaprecoitem = iterator.next();
			boolean add = true;
			boolean achou;
			Materialtabelapreco materialtabelapreco = materialtabelaprecoitem.getMaterialtabelapreco(); 
			Set<Materialtabelaprecocliente> listaMaterialtabelaprecocliente = materialtabelapreco.getListaMaterialtabelaprecocliente();
			Set<Materialtabelaprecoempresa> listaMaterialtabelaprecoempresa = materialtabelapreco.getListaMaterialtabelaprecoempresa();
			
			if (listaCategoria!=null && !listaCategoria.isEmpty()){
//				query.whereIn("categoria.cdcategoria", CollectionsUtil.listAndConcatenate(listaCategoria, "cdcategoria", ","));
//				query.where("cliente is null");	
				achou = false;
				for (Categoria categoria: listaCategoria){
					if (materialtabelapreco.getCategoria()!=null && materialtabelapreco.getCategoria().equals(categoria)){
						achou = true;
						break;
					}
				}
				add = add && achou && (listaMaterialtabelaprecocliente==null || listaMaterialtabelaprecocliente.isEmpty());
			} else {
				if (cliente!=null && cliente.getCdpessoa()!=null){
//					query.where("cliente = ?", cliente);
//					query.where("categoria is null");
					achou = false;
					if (listaMaterialtabelaprecocliente!=null){
						for (Materialtabelaprecocliente materialtabelaprecocliente: listaMaterialtabelaprecocliente){
							if (materialtabelaprecocliente.getCliente()!=null && materialtabelaprecocliente.getCliente().equals(cliente)){
								achou = true;
								break;
							}
						}
					}
					add = add && achou && materialtabelapreco.getCategoria()==null;
				} else {
//					query.where("cliente is null");
//					query.where("categoria is null");
					add = add && (listaMaterialtabelaprecocliente==null || listaMaterialtabelaprecocliente.isEmpty()) && materialtabelapreco.getCategoria()==null;
				}
			}
			
			if (prazopagamento!=null && prazopagamento.getCdprazopagamento()!=null){
//				query.where("prazopagamento = ?", prazopagamento);
				add = add && materialtabelapreco.getPrazopagamento()!=null && materialtabelapreco.getPrazopagamento().equals(prazopagamento);
			} else {
//				query.where("prazopagamento is null");
				add = add && materialtabelapreco.getPrazopagamento()==null;
			}
			
			if (pedidovendatipo!=null && pedidovendatipo.getCdpedidovendatipo()!=null){
//				query.where("pedidovendatipo = ?", pedidovendatipo);
				add = add && materialtabelapreco.getPedidovendatipo()!=null && materialtabelapreco.getPedidovendatipo().equals(pedidovendatipo);
			} else {
//				query.where("pedidovendatipo is null");
				add = add && materialtabelapreco.getPedidovendatipo()==null;
			}
			
			if (empresa!=null && empresa.getCdpessoa()!=null){
//				query.where("empresa = ?", empresa);
				achou = false;
				if (listaMaterialtabelaprecoempresa!=null){
					for (Materialtabelaprecoempresa materialtabelaprecoempresa: listaMaterialtabelaprecoempresa){
						if (materialtabelaprecoempresa.getEmpresa()!=null && materialtabelaprecoempresa.getEmpresa().equals(empresa)){
							achou = true;
							break;
						}
					}
				}
				add = add && achou;
			} else {
//				query.where("empresa is null");
				add = add && (listaMaterialtabelaprecoempresa==null || listaMaterialtabelaprecoempresa.isEmpty());
			}
			
			if (unidademedida!=null && unidademedida.getCdunidademedida()!=null){
//				query.where("unidademedidatabela = ?", unidademedida);
				add = add && materialtabelaprecoitem.getUnidademedida()!=null && materialtabelaprecoitem.getUnidademedida().equals(unidademedida);
			} else {
//				query.openParentheses()
//				.where("unidademedidatabela is null").or()
//				.where("unidademedida.cdunidademedida = unidademedidatabela.cdunidademedida")
//				.closeParentheses();
				add = add && (materialtabelaprecoitem.getUnidademedida()==null || materialtabelaprecoitem.getMaterial()!=null && materialtabelaprecoitem.getMaterial().getUnidademedida()!=null && materialtabelaprecoitem.getMaterial().getUnidademedida().equals(materialtabelaprecoitem.getUnidademedida()));
			}
			
//			if (!add){
//				iterator.remove();
//			}
			if(add){
				listaEncontrado.add(materialtabelaprecoitem);
			}
		}
		
		if (listaEncontrado!=null && listaEncontrado.size()==1){
			return listaEncontrado.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtabelaprecoitemDAO#findTabelaPrecoAtual(Material material, Unidademedida unidademedidaEscolhida, Materialtabelapreco materialtabelapreco)
	 *
	 * @param material
	 * @param unidademedidaEscolhida
	 * @param materialtabelapreco
	 * @return
	 * @author Luiz Fernando
	 * @since 11/10/2013
	 */
	public List<Materialtabelaprecoitem> findTabelaPrecoAtual(Material material, Unidademedida unidademedidaEscolhida, Materialtabelapreco materialtabelapreco) {
		return materialtabelaprecoitemDAO.findTabelaPrecoAtual(material, unidademedidaEscolhida, materialtabelapreco);
	}
	
	/**
	 * M�todo que retono o item da tabela de pre�o com o valor de venda, valor m�nimo e m�ximo
	 *
	 * @param material
	 * @param cliente
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Materialtabelaprecoitem getItemWithValorMinimoMaximoTabelaPrecoAtual(Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida) {
		List<Categoria> listaCategoria = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			List<Pessoacategoria> listaPessoacategoria = pessoacategoriaService.findByCliente(cliente);
			if(listaPessoacategoria != null && !listaPessoacategoria.isEmpty()){
				listaCategoria = new ArrayList<Categoria>();
				for(Pessoacategoria pessoacategoria : listaPessoacategoria){
					if(pessoacategoria.getCategoria() != null && pessoacategoria.getCdpessoacategoria() != null){
						listaCategoria.add(pessoacategoria.getCategoria());
					}
				}
			}
		}
		
		Materialtabelaprecoitem materialtabelaprecoitem  = null;
		
		if(listaCategoria != null){
			materialtabelaprecoitem = this.getMaterialtabelaprecoitem(material, cliente, listaCategoria, prazopagamento, pedidovendatipo, empresa, unidademedida);
		}
		
		if(materialtabelaprecoitem == null){
			materialtabelaprecoitem = this.getMaterialtabelaprecoitem(material, cliente, null, prazopagamento, pedidovendatipo, empresa, unidademedida);
		}
		
		return materialtabelaprecoitem;
	}
	
	private Materialtabelaprecoitem getMaterialtabelaprecoitem(Material material, Cliente cliente, List<Categoria> listaCategoriaCliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida){
		List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = materialtabelaprecoitemDAO.findTabelaPrecoAtualOtimizado(material); 
		
		Materialtabelaprecoitem itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, pedidovendatipo, empresa, unidademedida, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, pedidovendatipo, empresa, null, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, pedidovendatipo, null, unidademedida, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, pedidovendatipo, null, null, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		if(cliente != null){
			if(pedidovendatipo != null){
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, empresa, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, empresa, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, null, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, null, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, null, empresa, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, null, empresa, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, null, null, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, null, null, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		if(prazopagamento != null && pedidovendatipo != null){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, pedidovendatipo, empresa, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, pedidovendatipo, empresa, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, pedidovendatipo, null, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, pedidovendatipo, null, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}

		
		if(prazopagamento != null){
			if(cliente != null){
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, null, empresa, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, null, empresa, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, null, null, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, prazopagamento, null, null, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, null, empresa, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, null, empresa, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, null, null, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, prazopagamento, null, null, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		if(pedidovendatipo != null){
			if(cliente != null){
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, empresa, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, empresa, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, null, unidademedida, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
				itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, cliente, listaCategoriaCliente, null, pedidovendatipo, null, null, listaMaterialtabelaprecoitem);
				if(itemTabelaPrecoAtual != null){
					return itemTabelaPrecoAtual;
				}
			}
		}
		
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, null, empresa, unidademedida, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, null, empresa, null, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, null, null, unidademedida, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, null, null, null, listaMaterialtabelaprecoitem);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		if(pedidovendatipo != null){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, pedidovendatipo, empresa, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, pedidovendatipo, empresa, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, pedidovendatipo, null, unidademedida, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualOtimizado(material, null, listaCategoriaCliente, null, pedidovendatipo, null, null, listaMaterialtabelaprecoitem);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param reajuste
	 * @author Rafael Salvio
	 */
	public void ajustarPreco(String whereIn, Double reajuste){
		materialtabelaprecoitemDAO.ajustarPrecos(whereIn, reajuste);
	}
	
	public Materialtabelaprecoitem getMaterialtabelaprecoitemForContrato(Material material, Cliente cliente, List<Categoria> listaCategoria, Frequencia frequencia, Empresa empresa){
		Materialtabelaprecoitem itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, cliente, listaCategoria, frequencia, empresa);
		if(itemTabelaPrecoAtual != null){
			return itemTabelaPrecoAtual;
		}
		
		if(listaCategoria != null && !listaCategoria.isEmpty()){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, listaCategoria, frequencia, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, listaCategoria, null, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, listaCategoria, frequencia, null);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, listaCategoria, null, null);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		if(cliente != null){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, cliente, null, frequencia, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, cliente, null, null, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, cliente, null, frequencia, null);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, cliente, null, null, null);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
		}
		
		if(frequencia != null){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, null, frequencia, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
			
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, null, frequencia, null);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		if(empresa != null){
			itemTabelaPrecoAtual = this.getItemTabelaPrecoAtualForContrato(material, null, null, null, empresa);
			if(itemTabelaPrecoAtual != null){
				return itemTabelaPrecoAtual;
			}
		}
		
		return null;
	}
	
	private Materialtabelaprecoitem getItemTabelaPrecoAtualForContrato(Material material, Cliente cliente, List<Categoria> listaCategoria, Frequencia frequencia, Empresa empresa) {
		return materialtabelaprecoitemDAO.getItemTabelaPrecoAtualForContrato(material, cliente, listaCategoria, frequencia, empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param materialtabelapreco
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/07/2014
	 */
	public List<Materialtabelaprecoitem> findByMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		return materialtabelaprecoitemDAO.findByMaterialtabelapreco(materialtabelapreco);
	}
	
	public List<MaterialtabelaprecoitemRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MaterialtabelaprecoitemRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialtabelaprecoitemRESTModel> lista = new ArrayList<MaterialtabelaprecoitemRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Materialtabelaprecoitem bean : materialtabelaprecoitemDAO.findForAndroid(whereIn))
				lista.add(new MaterialtabelaprecoitemRESTModel(bean));
		}
			
		return lista;
	}
	
	public Set<Material> findAtivosByWhereInMaterial(String whereIn){
		return materialtabelaprecoitemDAO.findAtivosByWhereInMaterial(whereIn);
	}
	
	public void updatePreco(Materialtabelaprecoitem materialtabelaprecoitem, Double valor, boolean naoCriarHistorico) {
		
		materialtabelaprecoitemDAO.updatePreco(materialtabelaprecoitem, valor);
		if(!naoCriarHistorico){
			MaterialTabelaPrecoItemHistorico historico =  new MaterialTabelaPrecoItemHistorico();
			historico.setMaterialTabelaPrecoItem(materialtabelaprecoitem);
			historico.setPedidoVendaMaterial(materialtabelaprecoitem.getPedidoVendaMaterial());
			historico.setVendaMaterial(materialtabelaprecoitem.getVendaMaterial());
			historico.setVendaOrcamentoMaterial(materialtabelaprecoitem.getVendaOrcamentoMaterial());
			historico.setValor(valor);
			materialTabelaPrecoItemHistoricoService.saveOrUpdate(historico);
		}
		
	}
	
	public void updatePreco(Materialtabelaprecoitem materialtabelaprecoitem, Double valor) {
		updatePreco(materialtabelaprecoitem, valor, false);
	}
	
	public void updatePercentualDesconto(Materialtabelaprecoitem materialtabelaprecoitem, Double valor, boolean naoCriarHistorico) {
		materialtabelaprecoitemDAO.updatePercentualDesconto(materialtabelaprecoitem, valor);
		if(!naoCriarHistorico){
			MaterialTabelaPrecoItemHistorico historico =  new MaterialTabelaPrecoItemHistorico();
			historico.setMaterialTabelaPrecoItem(materialtabelaprecoitem);
			historico.setPedidoVendaMaterial(materialtabelaprecoitem.getPedidoVendaMaterial());
			historico.setVendaMaterial(materialtabelaprecoitem.getVendaMaterial());
			historico.setVendaOrcamentoMaterial(materialtabelaprecoitem.getVendaOrcamentoMaterial());
			historico.setPercentualDesconto(valor);
			materialTabelaPrecoItemHistoricoService.saveOrUpdate(historico);
		}
	}
	
	public void updatePercentualDesconto(Materialtabelaprecoitem materialtabelaprecoitem, Double valor) {
		updatePercentualDesconto(materialtabelaprecoitem, valor, false);
	}
	
	public Boolean existeMaterialtabelaprecoitem(Materialtabelapreco materialtabelapreco){
		return materialtabelaprecoitemDAO.existeMaterialtabelaprecoitem(materialtabelapreco);
	}
	
	public Materialtabelaprecoitem loadByVendaMaterial(Vendamaterial vendaMaterial){
		return materialtabelaprecoitemDAO.loadByVendaMaterial(vendaMaterial);
	}
	
	public Materialtabelaprecoitem loadByPedidoVendaMaterial(Pedidovendamaterial pedidoVendaMaterial){
		return materialtabelaprecoitemDAO.loadByPedidoVendaMaterial(pedidoVendaMaterial);
	}
	
	public Materialtabelaprecoitem loadByVendaOrcamentoMaterial(Vendaorcamentomaterial vendaOrcamentoMaterial){
		return materialtabelaprecoitemDAO.loadByVendaOrcamentoMaterial(vendaOrcamentoMaterial);
	}
}
