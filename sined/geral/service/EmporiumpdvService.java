package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.EmporiumpdvDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumpdvService extends GenericService<Emporiumpdv>{

	private EmporiumpdvDAO emporiumpdvDAO;
	
	public void setEmporiumpdvDAO(EmporiumpdvDAO emporiumpdvDAO) {
		this.emporiumpdvDAO = emporiumpdvDAO;
	}
	
	public Emporiumpdv loadForIntegracaoEmporium(Emporiumpdv emporiumpdv) {
		return emporiumpdvDAO.loadForIntegracaoEmporium(emporiumpdv);
	}

	public List<Emporiumpdv> findByEmpresa(Empresa empresa) {
		return emporiumpdvDAO.findByEmpresa(empresa);
	}

	public List<Emporiumpdv> findForIntegracao() {
		return emporiumpdvDAO.findForIntegracao();
	}

	public List<Emporiumpdv> findForAcerto() {
		return emporiumpdvDAO.findForAcerto();
	}

}
