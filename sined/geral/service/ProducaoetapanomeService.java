package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.geral.dao.ProducaoetapanomeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.producaoetapanome.ProducaoEtapaNomeW3producaoRESTModel;

public class ProducaoetapanomeService extends GenericService<Producaoetapanome> {

	private ProducaoetapanomeDAO producaoetapanomeDAO;
	
	public void setProducaoetapanomeDAO(ProducaoetapanomeDAO producaoetapanomeDAO) {this.producaoetapanomeDAO = producaoetapanomeDAO;}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoetapanomeService#loadWithCamposAdicionais(Producaoetapanome producaoetapanome)
	*
	* @param producaoetapanome
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public Producaoetapanome loadWithCamposAdicionais(Producaoetapanome producaoetapanome) {
		return producaoetapanomeDAO.loadWithCamposAdicionais(producaoetapanome);
	}
	
	public List<ProducaoEtapaNomeW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	public List<ProducaoEtapaNomeW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<ProducaoEtapaNomeW3producaoRESTModel> lista = new ArrayList<ProducaoEtapaNomeW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Producaoetapanome pen : producaoetapanomeDAO.findForW3Producao(whereIn))
				lista.add(new ProducaoEtapaNomeW3producaoRESTModel(pen));
		}
		
		return lista;
	}
	
	public boolean isPermitirtrocarprodutofinal(Producaoetapanome producaoetapanome){
		return producaoetapanomeDAO.isPermitirtrocarprodutofinal(producaoetapanome);
	}
}