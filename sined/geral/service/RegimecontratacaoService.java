package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Regimecontratacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RegimecontratacaoService extends GenericService<Regimecontratacao> {	
	
	/* singleton */
	private static RegimecontratacaoService instance;
	public static RegimecontratacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RegimecontratacaoService.class);
		}
		return instance;
	}
	
}
