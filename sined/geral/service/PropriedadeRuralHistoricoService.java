package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.sined.geral.bean.ParticipantePropriedadeRural;
import br.com.linkcom.sined.geral.bean.PropriedadeRural;
import br.com.linkcom.sined.geral.bean.PropriedadeRuralHistorico;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PropriedadeRuralHistoricoService extends GenericService<PropriedadeRuralHistorico> {

	public void preencherHistorico(PropriedadeRural bean, PropriedadeRural propriedadeRuralAntiga, boolean criar) {
		PropriedadeRuralHistorico novoHistorico = new PropriedadeRuralHistorico();
		
		try {
			novoHistorico.setUsuarioaltera(SinedUtil.getUsuarioLogado());
		} catch (Exception e) {
		}
		novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		novoHistorico.setPropriedadeRural(bean);
		
		if(criar){
			novoHistorico.setAcao("CRIADO");
		}else {
			novoHistorico.setAcao("ALTERADO");
			
			Boolean telefoneDiferente = false;
			Boolean enderecoDiferente = false;
			Boolean visualizarNovoMaterial = false;
			
			//compara os telefones novos com os antigos
			if(bean.getListaPropriedadeRuralTelefone() == null && propriedadeRuralAntiga.getListaPropriedadeRuralTelefone().size() != 0){
				telefoneDiferente = true;
			}else if(bean.getListaPropriedadeRuralTelefone()!= null && bean.getListaPropriedadeRuralTelefone().size() != propriedadeRuralAntiga.getListaPropriedadeRuralTelefone().size()){
				telefoneDiferente = true;
			}else if(SinedUtil.isListNotEmpty(bean.getListaPropriedadeRuralTelefone())){
				for(int i = 0; i < bean.getListaPropriedadeRuralTelefone().size(); i++){
					if(!bean.getListaPropriedadeRuralTelefone().get(i).getTelefone().equals(propriedadeRuralAntiga.getListaPropriedadeRuralTelefone().get(i).getTelefone()) ||
							!bean.getListaPropriedadeRuralTelefone().get(i).getTelefoneTipo().getCdtelefonetipo().equals(propriedadeRuralAntiga.getListaPropriedadeRuralTelefone().get(i).getTelefoneTipo().getCdtelefonetipo())){
						telefoneDiferente = true;
					}
				}				
			}
			
			//compara os endere�os novos com os antigos
			if((bean.getListaEndereco() == null && propriedadeRuralAntiga.getListaEndereco().size() != 0)){
				enderecoDiferente = true;
			}else if(bean.getListaEndereco() != null && bean.getListaEndereco().size() != propriedadeRuralAntiga.getListaEndereco().size()){
				enderecoDiferente = true;
			}else if(SinedUtil.isListNotEmpty(bean.getListaEndereco())){
				for (int i = 0; i < bean.getListaEndereco().size(); i++){
					if(!bean.getListaEndereco().get(i).getLogradouroCompletoComBairroSemMunicipio().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getLogradouroCompletoComBairroSemMunicipio())){
						enderecoDiferente = true;
						break;
					}
					if((bean.getListaEndereco().get(i).getPais() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getPais() != null && 
							!bean.getListaEndereco().get(i).getPais().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getPais())) ||
							(bean.getListaEndereco().get(i).getPais() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getPais() == null) ||
							(bean.getListaEndereco().get(i).getPais() == null && propriedadeRuralAntiga.getListaEndereco().get(i).getPais() != null)){
						enderecoDiferente = true;
						break;
					}
					if((bean.getListaEndereco().get(i).getMunicipio() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() != null && 
							!bean.getListaEndereco().get(i).getMunicipio().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio())) ||
							(bean.getListaEndereco().get(i).getMunicipio() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() == null) ||
							(bean.getListaEndereco().get(i).getMunicipio() == null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() != null)){
						enderecoDiferente = true;
						break;
					}
					if((bean.getListaEndereco().get(i).getMunicipio() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() == null) ||
							(bean.getListaEndereco().get(i).getMunicipio() == null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() != null)){
						enderecoDiferente = true;
						break;
					}else if(bean.getListaEndereco().get(i).getMunicipio() != null && bean.getListaEndereco().get(i).getMunicipio().getUf() != null && bean.getListaEndereco().get(i).getMunicipio().getUf().getCduf() != null &&
							propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio().getUf() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio().getUf().getCduf() != null &&
							!bean.getListaEndereco().get(i).getMunicipio().getUf().getCduf().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getMunicipio().getUf().getCduf())){	
						enderecoDiferente = true;
						break;
					}
					if((bean.getListaEndereco().get(i).getCaixapostal() != null &&  propriedadeRuralAntiga.getListaEndereco().get(i).getCaixapostal() != null &&
							!bean.getListaEndereco().get(i).getCaixapostal().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getCaixapostal())) ||
							(bean.getListaEndereco().get(i).getCaixapostal() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getCaixapostal() == null) ||
							(bean.getListaEndereco().get(i).getCaixapostal() == null && propriedadeRuralAntiga.getListaEndereco().get(i).getCaixapostal() != null)){
						enderecoDiferente = true;
						break;
					}
					if((bean.getListaEndereco().get(i).getEnderecotipo() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getEnderecotipo() != null &&
							!bean.getListaEndereco().get(i).getEnderecotipo().equals(propriedadeRuralAntiga.getListaEndereco().get(i).getEnderecotipo())) ||
							(bean.getListaEndereco().get(i).getEnderecotipo() != null && propriedadeRuralAntiga.getListaEndereco().get(i).getEnderecotipo() == null) ||
							(bean.getListaEndereco().get(i).getEnderecotipo() == null && propriedadeRuralAntiga.getListaEndereco().get(i).getEnderecotipo() != null)){
						enderecoDiferente = true;
						break;
					}
				}	
			}
			
			if(telefoneDiferente && enderecoDiferente){
				novoHistorico.setObservacao("Altera��o de telefones e endere�os");
			}else if(telefoneDiferente){
				novoHistorico.setObservacao("Altera��o de telefones");
			}else if(enderecoDiferente){
				novoHistorico.setObservacao("Altera��o de endere�os");
			}
			
			//compara os demais campos da propriedade rural
			if(!bean.getNome().equals(propriedadeRuralAntiga.getNome())){
				visualizarNovoMaterial = true;
			}else if((bean.getCafir() != null && propriedadeRuralAntiga.getCafir() != null && !bean.getCafir().equals(propriedadeRuralAntiga.getCafir())) ||
					(bean.getCafir() != null && propriedadeRuralAntiga == null) || (bean.getCafir() == null && propriedadeRuralAntiga.getCaepf() != null)){
				visualizarNovoMaterial = true;
			}else if((bean.getCaepf() != null && propriedadeRuralAntiga.getCaepf() != null && !bean.getCaepf().equals(propriedadeRuralAntiga.getCaepf())) || 
					(bean.getCaepf() != null && propriedadeRuralAntiga.getCaepf() == null) || (bean.getCaepf() == null && propriedadeRuralAntiga.getCaepf() != null)){
				visualizarNovoMaterial = true;
			}else if((bean.getEmpresa() != null && propriedadeRuralAntiga.getEmpresa() != null && !bean.getEmpresa().equals(propriedadeRuralAntiga.getEmpresa())) || 
					(bean.getEmpresa() != null && propriedadeRuralAntiga.getEmpresa() == null) || (bean.getEmpresa() == null && propriedadeRuralAntiga.getEmpresa() != null)){
				visualizarNovoMaterial = true;
			}else if(!bean.getTipoExploracao().equals(propriedadeRuralAntiga.getTipoExploracao())){
				visualizarNovoMaterial = true;
			}else if((bean.getTipoTerceiros() != null && propriedadeRuralAntiga.getTipoTerceiros()!= null && !bean.getTipoTerceiros().equals(propriedadeRuralAntiga.getTipoTerceiros())) ||
					(bean.getTipoTerceiros() != null && propriedadeRuralAntiga.getTipoTerceiros() == null) || (bean.getTipoTerceiros() == null && propriedadeRuralAntiga != null)){
				visualizarNovoMaterial = true;
			}
			
			//compara os participantes e seus percentuais
			if(bean.getListaParticipantePropriedadeRural() == null && propriedadeRuralAntiga.getListaParticipantePropriedadeRural().size() != 0){
				visualizarNovoMaterial = true;				
			}else if(bean.getListaParticipantePropriedadeRural() != null && bean.getListaParticipantePropriedadeRural().size() != propriedadeRuralAntiga.getListaParticipantePropriedadeRural().size()){
				visualizarNovoMaterial = true;
			}else if(SinedUtil.isListNotEmpty(bean.getListaParticipantePropriedadeRural())){
				for (int i = 0; i < bean.getListaParticipantePropriedadeRural().size(); i++) {
					if(!bean.getListaParticipantePropriedadeRural().get(i).getColaborador().equals(propriedadeRuralAntiga.getListaParticipantePropriedadeRural().get(i).getColaborador())){
						visualizarNovoMaterial = true;
						break;
					}
					if(!bean.getListaParticipantePropriedadeRural().get(i).getPercentual().equals(propriedadeRuralAntiga.getListaParticipantePropriedadeRural().get(i).getPercentual())){
						visualizarNovoMaterial = true;
						break;
					}
				}				
			}
			
			if (!telefoneDiferente && !enderecoDiferente){
				visualizarNovoMaterial = true;
			}
			
			if(visualizarNovoMaterial){
				novoHistorico.setObservacao("Visualizar propriedade rural ");
				novoHistorico.setNome(propriedadeRuralAntiga.getNome());
				novoHistorico.setTipoExploracao(propriedadeRuralAntiga.getTipoExploracao());
				novoHistorico.setCodigoImovel(propriedadeRuralAntiga.getCodigoImovel());
				
				if(propriedadeRuralAntiga.getCafir() != null){
					novoHistorico.setCafir(propriedadeRuralAntiga.getCafir());
				}
				if(propriedadeRuralAntiga.getCaepf() != null){
					novoHistorico.setCaepf(propriedadeRuralAntiga.getCaepf());					
				}
				if(propriedadeRuralAntiga.getEmpresa() != null){
					novoHistorico.setEmpresa(propriedadeRuralAntiga.getEmpresa());					
				}
				if(propriedadeRuralAntiga.getTipoTerceiros() != null){
					novoHistorico.setTipoTerceiros(propriedadeRuralAntiga.getTipoTerceiros());					
				}
				
				String participantesConcatenados = "";
				for (ParticipantePropriedadeRural participante : propriedadeRuralAntiga.getListaParticipantePropriedadeRural()) {
					participantesConcatenados = participantesConcatenados + participante.getColaborador().getNome() + " - Porcentagem: " + participante.getPercentual().toString() +"%\n";
				}
		
				novoHistorico.setParticipantesPercentuais(participantesConcatenados);
			}
		}
		saveOrUpdate(novoHistorico);
	}
	
}
