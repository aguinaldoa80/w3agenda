package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRemessaSegmentoDAO;

public class BancoConfiguracaoRemessaSegmentoService extends GenericService<BancoConfiguracaoRemessaSegmento>{
	
	private BancoConfiguracaoRemessaSegmentoDAO bancoConfiguracaoRemessaSegmentoDAO;
	
	public void setBancoConfiguracaoRemessaSegmentoDAO(BancoConfiguracaoRemessaSegmentoDAO bancoConfiguracaoRemessaSegmentoDAO) {this.bancoConfiguracaoRemessaSegmentoDAO = bancoConfiguracaoRemessaSegmentoDAO;}
	
	public List<BancoConfiguracaoRemessaSegmento> findAllByBancoConfiguracaoRemessa(BancoConfiguracaoRemessa bancoConfiguracaoRemessa){
		return bancoConfiguracaoRemessaSegmentoDAO.findAllByBancoConfiguracaoRemessa(bancoConfiguracaoRemessa);
	}
	
}
