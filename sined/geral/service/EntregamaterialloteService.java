package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.dao.EntregamaterialloteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregamaterialloteService extends GenericService<Entregamateriallote> {

	private EntregamaterialloteDAO entregamaterialloteDAO;
	
	public void setEntregamaterialloteDAO(EntregamaterialloteDAO entregamaterialloteDAO) {
		this.entregamaterialloteDAO = entregamaterialloteDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregamaterialloteDAO#findByEntregamaterial(Entregamaterial entregamaterial)
	*
	* @param entregamaterial
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public List<Entregamateriallote> findByEntregamaterial(Entregamaterial entregamaterial) {
		return entregamaterialloteDAO.findByEntregamaterial(entregamaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntregamaterialloteDAO#findByEntregadocumento(Entregadocumento entregadocumento)
	*
	* @param entregadocumento
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public List<Entregamateriallote> findByEntregadocumento(Entregadocumento entregadocumento) {
		return entregamaterialloteDAO.findByEntregadocumento(entregadocumento);
	}
	
}