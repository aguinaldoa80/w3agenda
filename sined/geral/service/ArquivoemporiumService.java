package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaarquivo;
import br.com.linkcom.sined.geral.bean.Emporiumvendafinalizadora;
import br.com.linkcom.sined.geral.bean.Emporiumvendahistorico;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.bean.enumeration.Emporiumvendaacao;
import br.com.linkcom.sined.geral.dao.ArquivoemporiumDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.emporium.bean.Emporiumcliente;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU;
import br.com.linkcom.sined.util.emporium.registros.RegistroPLU_KIT;
import br.com.linkcom.sined.util.emporium.registros.RegistroSKU;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivoemporiumService extends GenericService<Arquivoemporium> {
	
	private ArquivoemporiumDAO arquivoemporiumDAO;
	private ArquivoService arquivoService;
	private EmporiumvendaService emporiumvendaService;
	private EmporiumvendaarquivoService emporiumvendaarquivoService;
	private EmporiumvendahistoricoService emporiumvendahistoricoService;
	private ClienteService clienteService;
	private PessoaService pessoaService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumtotalizadoresService emporiumtotalizadoresService;
	private TributacaoecfService tributacaoecfService;
	
	public void setTributacaoecfService(
			TributacaoecfService tributacaoecfService) {
		this.tributacaoecfService = tributacaoecfService;
	}
	public void setEmporiumtotalizadoresService(
			EmporiumtotalizadoresService emporiumtotalizadoresService) {
		this.emporiumtotalizadoresService = emporiumtotalizadoresService;
	}
	public void setEmporiumreducaozService(
			EmporiumreducaozService emporiumreducaozService) {
		this.emporiumreducaozService = emporiumreducaozService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setEmporiumvendahistoricoService(
			EmporiumvendahistoricoService emporiumvendahistoricoService) {
		this.emporiumvendahistoricoService = emporiumvendahistoricoService;
	}
	public void setEmporiumvendaarquivoService(
			EmporiumvendaarquivoService emporiumvendaarquivoService) {
		this.emporiumvendaarquivoService = emporiumvendaarquivoService;
	}
	public void setEmporiumvendaService(
			EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoemporiumDAO(ArquivoemporiumDAO arquivoemporiumDAO) {
		this.arquivoemporiumDAO = arquivoemporiumDAO;
	}
		
	public List<Object> getRegistrosExclusaoTabelasAuxiliares(String banco_cliente) {
		List<RegistroSKU> listaRegistroSKU = arquivoemporiumDAO.getRegistrosExclusaoSKU(banco_cliente);
		List<RegistroPLU> listaRegistroPLU = arquivoemporiumDAO.getRegistrosExclusaoPLU(banco_cliente);
		List<RegistroPLU> listaRegistroPLUFlag = arquivoemporiumDAO.getRegistrosExclusaoPLUFlag(banco_cliente);
		List<RegistroPLU_KIT> listaRegistroPLU_KIT = arquivoemporiumDAO.getRegistrosExclusaoPLU_KIT(banco_cliente);

		List<Object> retorno = new ArrayList<Object>();
		
		retorno.addAll(listaRegistroSKU);
		retorno.addAll(listaRegistroPLU);
		retorno.addAll(listaRegistroPLUFlag);
		retorno.addAll(listaRegistroPLU_KIT);
		
		return retorno;
	}
	
	public void deleteTabelasAuxiliares() {
		arquivoemporiumDAO.deleteTabelasAuxiliares();
	}
	
	/* singleton */
	private static ArquivoemporiumService instance;
	public static ArquivoemporiumService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ArquivoemporiumService.class);
		}
		return instance;
	}

	public void updateArquivoresultado(Arquivoemporium arquivoemporium) {
		arquivoemporiumDAO.updateArquivoresultado(arquivoemporium);
	}

	public List<Arquivoemporium> getResultadosFaltantes() {
		return arquivoemporiumDAO.getResultadosFaltantes();
	}
	
	public List<Arquivoemporium> findForWebserviceGET(Arquivoemporiumtipo arquivoemporiumtipo) {
		return arquivoemporiumDAO.findForWebserviceGET(arquivoemporiumtipo);
	}
	
	public List<Arquivoemporium> findForWebserviceSEND() {
		return arquivoemporiumDAO.findForWebserviceSEND();
	}

	public void processaExportacoesPendentes() {
		List<Arquivoemporium> listaArquivoemporium = this.getExportacoesPendentes();
		for (Arquivoemporium arquivoemporium : listaArquivoemporium) {
			try{
				this.processaExportacao(arquivoemporium);
			}catch (Exception e) {
				e.printStackTrace();
				NeoWeb.getRequestContext().addError(e.getMessage());
			}
		}
	}

	private List<Arquivoemporium> getExportacoesPendentes() {
		return arquivoemporiumDAO.getExportacoesPendentes();
	}
	
	public void processaExportacao(Arquivoemporium arquivoemporium) {
		Arquivo arquivoresultado = arquivoemporium.getArquivoresultado();
		if(arquivoresultado == null){
			throw new SinedException("Arquivo com o resultado n�o foi encontrado.");
		}
		arquivoresultado = arquivoService.loadWithContents(arquivoresultado);
		
		Arquivoemporiumtipo arquivoemporiumtipo = arquivoemporium.getArquivoemporiumtipo();
		if(arquivoemporiumtipo == null){
			throw new SinedException("Tipo do arquivo n�o foi encontrado.");
		}
		
		String arquivoStr = new String(arquivoresultado.getContent());
		
		if(arquivoStr == null || arquivoStr.trim().equals("")){
			this.updateProcessado(arquivoemporium);
			return;
		}
		
		String[] linha = arquivoStr.split("\\r?\\n");
		
		if(arquivoemporiumtipo.equals(Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO)){
			this.processaExportacaoMovimentoDetalhado(arquivoemporium, linha);
		} else if(arquivoemporiumtipo.equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISTRIBUTACAO)){
			this.processaExportacaoFiscaisTributacao(arquivoemporium, linha);
		} else if(arquivoemporiumtipo.equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISGERAIS)){
			this.processaExportacaoFiscaisGerais(arquivoemporium, linha);
		} else if(arquivoemporiumtipo.equals(Arquivoemporiumtipo.EXPORTACAO_CLIENTES)){
			this.processaExportacaoClientes(arquivoemporium, linha);
		}
		
		this.updateProcessado(arquivoemporium);
	}

	private void processaExportacaoClientes(Arquivoemporium arquivoemporium, String[] arquivo) {
		List<Emporiumcliente> listaEmporiumcliente = new ArrayList<Emporiumcliente>();
		
		for (String linha : arquivo) {
			String[] campo = linha.split("\\|");
			
			Integer codigo = this.getIntegerCampoArray(campo, 2);
			String cpfcnpj = this.getStringCampoArray(campo, 3);
			String nome = this.getStringCampoArray(campo, 5);
			String email = this.getStringCampoArray(campo, 21);
			String ie = this.getStringCampoArray(campo, 24);
			
			Emporiumcliente emporiumcliente = new Emporiumcliente();
			emporiumcliente.setCodigo(codigo);
			emporiumcliente.setCpfcnpj(StringUtils.trimToNull(cpfcnpj));
			emporiumcliente.setEmail(StringUtils.trimToNull(email));
			emporiumcliente.setIe(StringUtils.trimToNull(ie));
			emporiumcliente.setNome(StringUtils.trimToNull(nome));
			
			listaEmporiumcliente.add(emporiumcliente);
		}
		
		for (Emporiumcliente emporiumcliente : listaEmporiumcliente) {
			try{
				String cpfcnpj = emporiumcliente.getCpfcnpj();
				if(cpfcnpj == null){
					continue;
				}
				
				cpfcnpj = br.com.linkcom.neo.util.StringUtils.soNumero(cpfcnpj).trim().replaceAll(" ", "");
				String cpfString = br.com.linkcom.neo.util.StringUtils.stringCheia(cpfcnpj, "0", 11, false);
				String cnpjString = br.com.linkcom.neo.util.StringUtils.stringCheia(cpfcnpj, "0", 14, false);
				if(!Cpf.cpfValido(cpfString) && !Cnpj.cnpjValido(cnpjString)){
					continue;
				}
				
				Cnpj cnpj = null;
				Cpf cpf = null;
				
				if(cpfcnpj.length() > 11){
					cnpj = new Cnpj(cnpjString);
				} else {
					cpf = new Cpf(cpfString);
				}
				
				Cliente cliente = null;
				if(cnpj != null){
					cliente = clienteService.findByCnpj(cnpj);
					if(cliente == null){
						cliente = new Cliente();
						Pessoa pessoa = pessoaService.findPessoaByCnpj(cnpj);
						
						if(pessoa != null && pessoa.getCdpessoa() != null){
							cliente.setCdpessoa(pessoa.getCdpessoa());
							clienteService.insertSomenteCliente(cliente);
						} else {
							cliente = null;
						}
					}
				} else if(cpf != null){
					cliente = clienteService.findByCpf(cpf);
					if(cliente == null){
						cliente = new Cliente();
						Pessoa pessoa = pessoaService.findPessoaByCpf(cpf);
						
						if(pessoa != null && pessoa.getCdpessoa() != null){
							cliente.setCdpessoa(pessoa.getCdpessoa());
							clienteService.insertSomenteCliente(cliente);
						} else {
							cliente = null;
						}
					}
				} else {
					throw new SinedException("CPF ou CNPJ n�o encontrado no registro.");
				}
				
				if(cliente != null){
					continue;
				}
				
				cliente = new Cliente();
				cliente.setNome(emporiumcliente.getNome());
				cliente.setTipopessoa(cnpj != null ? Tipopessoa.PESSOA_JURIDICA : Tipopessoa.PESSOA_FISICA);
				cliente.setCnpj(cnpj);
				cliente.setCpf(cpf);
				cliente.setInscricaoestadual(emporiumcliente.getIe());
				cliente.setEmail(emporiumcliente.getEmail());
				
				clienteService.saveOrUpdate(cliente);
			} catch (Exception e) {
				e.printStackTrace();
				IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro na cria��o de cliente");
			}
		}
		
	}
	
	private void processaExportacaoFiscaisTributacao(Arquivoemporium arquivoemporium, String[] arquivo) {
		List<Emporiumpdv> listaPdv = emporiumpdvService.findAll();
		List<Tributacaoecf> listaTributacaoecf = tributacaoecfService.findAll();
		List<Emporiumtotalizadores> listaEmporiumtotalizadores = new ArrayList<Emporiumtotalizadores>();
		
		for (String linha : arquivo) {
			try{
				String[] campo = linha.split("\\|");
				
				String pdv = this.getStringCampoArray(campo, 0);
				if(pdv == null || pdv.trim().equals("")) continue;
				
				String legenda = this.getStringCampoArray(campo, 1);
				Double base = this.getDoubleCampoArray(campo, 2, 2);
				Double imposto = this.getDoubleCampoArray(campo, 3, 2);
				Double taxa = this.getDoubleCampoArray(campo, 4, 2);
				
				Emporiumpdv emporiumpdv = IntegracaoEmporiumUtil.util.getPDVEmporium(null, pdv, listaPdv);
				
				Tributacaoecf tributacaoecf = IntegracaoEmporiumUtil.util.getTributacaoecfEmporium(legenda, listaTributacaoecf);
				
				Emporiumtotalizadores emporiumtotalizadores = new Emporiumtotalizadores();
				emporiumtotalizadores.setData(arquivoemporium.getDtmovimento());
				emporiumtotalizadores.setEmporiumpdv(emporiumpdv);
				emporiumtotalizadores.setLegenda(legenda);
				emporiumtotalizadores.setTributacaoecf(tributacaoecf);
				emporiumtotalizadores.setValorbcicms(base);
				emporiumtotalizadores.setAliqicms(taxa);
				emporiumtotalizadores.setValoricms(imposto);
				
				listaEmporiumtotalizadores.add(emporiumtotalizadores);
				
			} catch (Exception e) {
				e.printStackTrace();
				IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro na interpreta��o dos dados dos totalizadores.");
			}
		}
		
		for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadores) {
			if(emporiumtotalizadores.getEmporiumpdv() != null &&
					emporiumtotalizadores.getEmporiumpdv().getCdemporiumpdv() != null &&
					emporiumtotalizadores.getData() != null){
				try {
					emporiumtotalizadoresService.deleteTotalizadores(emporiumtotalizadores.getData(), emporiumtotalizadores.getEmporiumpdv());
				} catch (Exception e) {}
			}
		}
		for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadores) {
			emporiumtotalizadoresService.saveOrUpdate(emporiumtotalizadores);
		}
	}
	
	private void processaExportacaoFiscaisGerais(Arquivoemporium arquivoemporium, String[] arquivo) {
		List<Emporiumpdv> listaPdv = emporiumpdvService.findAll();
		List<Emporiumreducaoz> listaEmporiumreducaoz = new ArrayList<Emporiumreducaoz>();
		
		for (String linha : arquivo) {
			try{
				String[] campo = linha.split("\\|");
				
				String pdv = this.getStringCampoArray(campo, 0);
				if(pdv == null || pdv.trim().equals("")) continue;
				
				Integer reducoes = this.getIntegerCampoArray(campo, 1);
				Integer ticket_final = this.getIntegerCampoArray(campo, 3);
				Double gt_final = this.getDoubleCampoArray(campo, 6, 2);
				Double venda_bruta = this.getDoubleCampoArray(campo, 7, 2);
				Double venda_cancelados = this.getDoubleCampoArray(campo, 8, 2);
				Double venda_desconto = this.getDoubleCampoArray(campo, 10, 2);
				Double venda_acrescimo = this.getDoubleCampoArray(campo, 11, 2);
				Integer cro = this.getIntegerCampoArray(campo, 14);
				
				Emporiumpdv emporiumpdv = IntegracaoEmporiumUtil.util.getPDVEmporium(null, pdv, listaPdv);
				
				Emporiumreducaoz emporiumreducaoz = new Emporiumreducaoz();
				emporiumreducaoz.setData(arquivoemporium.getDtmovimento());
				emporiumreducaoz.setEmporiumpdv(emporiumpdv);
				emporiumreducaoz.setCrz(reducoes);
				emporiumreducaoz.setNumerocoofinal(ticket_final);
				emporiumreducaoz.setGtfinal(gt_final);
				emporiumreducaoz.setValorvendabruta(venda_bruta);
				emporiumreducaoz.setCro(cro);
				emporiumreducaoz.setValorcancelados(venda_cancelados);
				emporiumreducaoz.setValordesconto(venda_desconto);
				emporiumreducaoz.setValoracrescimo(venda_acrescimo);
				
				listaEmporiumreducaoz.add(emporiumreducaoz);
			} catch (Exception e) {
				e.printStackTrace();
				IntegracaoEmporiumUtil.util.enviarEmailErro(e, "Erro na interpreta��o dos dados da Redu��o Z.");
			}
		}
		
		for (Emporiumreducaoz emporiumreducaoz : listaEmporiumreducaoz) {
			Emporiumreducaoz e_aux = emporiumreducaozService.loadReducaoz(emporiumreducaoz.getCrz(), emporiumreducaoz.getData(), emporiumreducaoz.getEmporiumpdv());
			if(e_aux != null){
				emporiumreducaozService.delete(e_aux);
			}
			emporiumreducaozService.saveOrUpdate(emporiumreducaoz);
		}
	}
	
	private void updateProcessado(Arquivoemporium arquivoemporium) {
		arquivoemporiumDAO.updateProcessado(arquivoemporium);
	}
	
	private void processaExportacaoMovimentoDetalhado(Arquivoemporium arquivoemporium, String[] arquivo) {
		List<Emporiumvenda> listaEmporiumvenda = new ArrayList<Emporiumvenda>();
		Emporiumvenda emporiumvenda = null;
		String kitatual = null;
		
		for (String linha : arquivo) {
			if(linha != null && !linha.trim().equals("")){
				String[] campo = linha.split("\\|");
				String registro = campo[1];
	
				if(registro.equals("00")){
					kitatual = null;
					
					if(emporiumvenda != null){
						listaEmporiumvenda.add(emporiumvenda);
						emporiumvenda = null;
					}
					
					Integer tipo = this.getIntegerCampoArray(campo, 8);
					if(tipo == null || (!tipo.equals(0) && !tipo.equals(65))){
						continue;
					}
					
					String pdv = this.getStringCampoArray(campo, 3);
					
					Integer cancelamentocupom = this.getIntegerCampoArray(campo, 7);
					Integer cancelamentovenda = this.getIntegerCampoArray(campo, 6);
					
					if(cancelamentocupom != null && cancelamentocupom.equals(1)){
						if(cancelamentovenda != null && cancelamentovenda.equals(1)){
							continue;
						}
						
						if(listaEmporiumvenda.size() > 0){
							Emporiumvenda emporiumvendaCancelamento = listaEmporiumvenda.get(listaEmporiumvenda.size() - 1);
							if(emporiumvendaCancelamento.getPdv().equals(pdv)){
								listaEmporiumvenda.remove(listaEmporiumvenda.size()-1);
							}
						}
						continue;
					}
					
					if(cancelamentovenda != null && cancelamentovenda.equals(1)){
						continue;
					}
					
					String loja = this.getStringCampoArray(campo, 2);
					String numero_ticket = this.getStringCampoArray(campo, 4);
					Timestamp data_hora = this.getTimestampCampoArray(campo, 5);
					String cliente_id = this.getStringCampoArray(campo, 9);
					Double valor = this.getDoubleCampoArray(campo, 10, 3);
					Double valor_troco = this.getDoubleCampoArray(campo, 11, 3);
					String vendedor_id = this.getStringCampoArray(campo, 13);
					String operador_id = this.getStringCampoArray(campo, 14);
					Double valor_desconto = this.getDoubleCampoArray(campo, 16, 3);
					Double valor_acrescimo = this.getDoubleCampoArray(campo, 17, 3);
					Double valor_juros = this.getDoubleCampoArray(campo, 18, 3);
					
					emporiumvenda = new Emporiumvenda();
					emporiumvenda.setNumero_ticket(numero_ticket);
					emporiumvenda.setLoja(loja);
					emporiumvenda.setPdv(pdv);
					emporiumvenda.setCliente_id(cliente_id);
					emporiumvenda.setData_hora(data_hora);
					emporiumvenda.setOperador_id(operador_id);
					emporiumvenda.setTipo(tipo);
					emporiumvenda.setValor(valor);
					emporiumvenda.setValor_acrescimo(valor_acrescimo);
					emporiumvenda.setValor_desconto(valor_desconto);
					emporiumvenda.setValor_juros(valor_juros);
					emporiumvenda.setValor_troco(valor_troco);
					emporiumvenda.setVendedor_id(vendedor_id);
					
				} else if(registro.equals("01")){
					if(emporiumvenda != null){
						Integer sequencial = this.getIntegerCampoArray(campo, 6);
						Integer produto_id = this.getIntegerCampoArray(campo, 8);
						String produto_desc = this.getStringCampoArray(campo, 9);
						Double quantidade = this.getDoubleCampoArray(campo, 10, 3);
						Double valorunitario = this.getDoubleCampoArray(campo, 11, 3);
						Double valortotal = this.getDoubleCampoArray(campo, 12, 3);
						Double desconto = this.getDoubleCampoArray(campo, 16, 3);
						String sku_id = this.getStringCampoArray(campo, 18);
						String legenda = this.getStringCampoArray(campo, 19);
						Double taxa = this.getDoubleCampoArray(campo, 20, 2);
						
						Integer cancelamentoitem = this.getIntegerCampoArray(campo, 7);
						if(cancelamentoitem != null && cancelamentoitem.equals(1)){
							if(emporiumvenda.getListaEmporiumvendaitem() != null){
								for (Iterator<Emporiumvendaitem> iterator = emporiumvenda.getListaEmporiumvendaitem().iterator(); iterator.hasNext();) {
									Emporiumvendaitem it = iterator.next();
									
									boolean produto_id_Igual = it.getProduto_id() != null && it.getProduto_id().equals(produto_id);
									boolean quantidade_Igual = it.getQuantidade() != null && it.getQuantidade().equals(quantidade);
									boolean valorunitario_Igual = it.getValorunitario() != null && it.getValorunitario().equals(valorunitario);
									boolean desconto_Igual = (it.getDesconto() == null && desconto == null) || (it.getDesconto() != null && it.getDesconto().equals(desconto));
									
									if(produto_id_Igual && quantidade_Igual && valorunitario_Igual && desconto_Igual){
										iterator.remove();
										break;
									}
								}
							}
							continue;
						}
						
						Emporiumvendaitem emporiumvendaitem = new Emporiumvendaitem();
						emporiumvendaitem.setProduto_desc(produto_desc);
						emporiumvendaitem.setProduto_id(produto_id);
						emporiumvendaitem.setQuantidade(quantidade);
						emporiumvendaitem.setSequencial(sequencial);
						emporiumvendaitem.setSku_id(sku_id);
						emporiumvendaitem.setValortotal(valortotal);
						emporiumvendaitem.setValorunitario(valorunitario);
						emporiumvendaitem.setDesconto(desconto);
						emporiumvendaitem.setLegenda(legenda);
						emporiumvendaitem.setTaxa(taxa);
						emporiumvendaitem.setKit(kitatual);
						
						if(emporiumvenda.getListaEmporiumvendaitem() == null){
							emporiumvenda.setListaEmporiumvendaitem(new ListSet<Emporiumvendaitem>(Emporiumvendaitem.class));
						}
						emporiumvenda.getListaEmporiumvendaitem().add(emporiumvendaitem);
					}
				} else if(registro.equals("09")){
					if(emporiumvenda != null){
						
						Integer sequencial = this.getIntegerCampoArray(campo, 6);
						Integer finalizadora_id = this.getIntegerCampoArray(campo, 7);
						Double valor = this.getDoubleCampoArray(campo, 8, 3);
						String flag = this.getStringCampoArray(campo, 10);
						
						Emporiumvendafinalizadora emporiumvendafinalizadora = new Emporiumvendafinalizadora();
						emporiumvendafinalizadora.setFinalizadora_id(finalizadora_id);
						emporiumvendafinalizadora.setSequencial(sequencial);
						emporiumvendafinalizadora.setValor(valor);
						emporiumvendafinalizadora.setFlag(flag);
						
						if(emporiumvenda.getListaEmporiumvendafinalizadora() == null){
							emporiumvenda.setListaEmporiumvendafinalizadora(new ListSet<Emporiumvendafinalizadora>(Emporiumvendafinalizadora.class));
						}
						emporiumvenda.getListaEmporiumvendafinalizadora().add(emporiumvendafinalizadora);
					}
				} else if(registro.equals("10")){
					if(emporiumvenda != null){
						String tipocampo = this.getStringCampoArray(campo, 7);
						
						if(tipocampo != null && tipocampo.trim().equals("6")){
							Integer pedidovenda = this.getIntegerCampoArray(campo, 9);
							emporiumvenda.setPedidovenda(pedidovenda);
						}
						
						// INICIO DE KIT
						if(tipocampo != null && tipocampo.trim().equals("195")){
							String kit = this.getStringCampoArray(campo, 9);
							kitatual = kit;
							if(emporiumvenda != null &&
									emporiumvenda.getListaEmporiumvendaitem() != null && 
									emporiumvenda.getListaEmporiumvendaitem().size() > 0){
								emporiumvenda.getListaEmporiumvendaitem().get(emporiumvenda.getListaEmporiumvendaitem().size() - 1).setKit(kit);
							}
						}
						
						// FIM DE KIT
						if(tipocampo != null && tipocampo.trim().equals("196")){
							kitatual = null;
							if(emporiumvenda != null &&
									emporiumvenda.getListaEmporiumvendaitem() != null && 
									emporiumvenda.getListaEmporiumvendaitem().size() > 0){
								emporiumvenda.getListaEmporiumvendaitem().get(emporiumvenda.getListaEmporiumvendaitem().size() - 1).setKit(null);
							}
						}
					}
				}
			}
		} 
		if(emporiumvenda != null){
			listaEmporiumvenda.add(emporiumvenda);
		}
		
		for (Emporiumvenda e : listaEmporiumvenda) {
			Emporiumvenda ev = emporiumvendaService.loadTicketCadastrado(e.getLoja(), e.getPdv(), e.getNumero_ticket());
			Venda venda = null;
			if(ev != null){
				venda = ev.getVenda();
				emporiumvendaService.delete(ev);
			}
			e.setVenda(venda);
			emporiumvendaService.saveOrUpdate(e);
			
			Emporiumvendaarquivo ea = new Emporiumvendaarquivo();
			ea.setArquivoemporium(arquivoemporium);
			ea.setEmporiumvenda(e);
			emporiumvendaarquivoService.saveOrUpdate(ea);
			
			Emporiumvendahistorico eh = new Emporiumvendahistorico();
			eh.setEmporiumvenda(e);
			eh.setAcao(Emporiumvendaacao.CRIADA);
			emporiumvendahistoricoService.saveOrUpdate(eh);
		}
	}
	
	private Double getDoubleCampoArray(String[] campo, int i, int decimais) {
		String campoStr = this.getStringCampoArray(campo, i);
		if(campoStr != null && !campoStr.equals("")){
			try {
				String inteiraStr = campoStr.substring(0, campoStr.length() - decimais);
				String decimalStr = campoStr.substring(campoStr.length() - decimais, campoStr.length());
				
				String doubleStr = this.retiraZeroEsquerda(inteiraStr) + "." + decimalStr;
				return Double.parseDouble(doubleStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private String retiraZeroEsquerda(String str){
		while(str.length() > 1 && str.startsWith("0")){
			str = str.substring(1, str.length());
		}
		return str;
	}
	
	private Integer getIntegerCampoArray(String[] campo, int i) {
		String campoStr = this.getStringCampoArray(campo, i);
		if(campoStr != null && !campoStr.equals("")){
			try {
				return Integer.parseInt(campoStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private Timestamp getTimestampCampoArray(String[] campo, int i) {
		String campoStr = this.getStringCampoArray(campo, i);
		if(campoStr != null && !campoStr.equals("")){
			try {
				Date data = SinedDateUtils.stringToDate(campoStr, "yyyyMMddHHmmss");
				return new Timestamp(data.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	private String getStringCampoArray(String[] campo, int index){
		return campo.length > index && campo[index] != null ? campo[index].trim() : null;
	}
	
	public void updateEnviadoemporium(Integer id) {
		arquivoemporiumDAO.updateEnviadoemporium(id);
	}
	
	public Arquivoemporium loadForEmporium(Arquivoemporium arquivoemporium) {
		return arquivoemporiumDAO.loadForEmporium(arquivoemporium);
	}
	
}
