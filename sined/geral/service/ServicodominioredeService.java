package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Servicodominiorede;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicodominioredeService extends GenericService<Servicodominiorede> {

	@Override
	public void saveOrUpdate(Servicodominiorede bean) {
		boolean novoRegistro = false;
		if(bean.getCdservicodominiorede() == null)
			novoRegistro = true;
		
		super.saveOrUpdate(bean);
		
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO_REDE, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCdservicodominiorede());
	}
	
	@Override
	public void delete(Servicodominiorede bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO_REDE, AcaoBriefCase.REMOVER, bean.getCdservicodominiorede());
		super.delete(bean);
	}
	
}
