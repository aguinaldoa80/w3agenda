package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletosituacao;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletosituacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoenvioboletosituacaoService extends GenericService<Documentoenvioboletosituacao> {
	
	private DocumentoenvioboletosituacaoDAO documentoenvioboletosituacaoDAO;
	
	public void setDocumentoenvioboletosituacaoDAO(
			DocumentoenvioboletosituacaoDAO documentoenvioboletosituacaoDAO) {
		this.documentoenvioboletosituacaoDAO = documentoenvioboletosituacaoDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletosituacao> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		return documentoenvioboletosituacaoDAO.findByDocumentoenvioboleto(documentoenvioboleto);
	}

}
