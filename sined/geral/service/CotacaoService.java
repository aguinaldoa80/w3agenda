package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedor;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritem;
import br.com.linkcom.sined.geral.bean.Cotacaofornecedoritemsolicitacaocompra;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.view.Vwultimovalorcotacao;
import br.com.linkcom.sined.geral.dao.CotacaoDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.CotacaoBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.CotacaoFornecedorBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.CotacaoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.CotacaoMapaAux;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.text.SimpleDateFormat;

public class CotacaoService extends GenericService<Cotacao> {

	private CotacaofornecedorService cotacaofornecedorService;
	private CotacaofornecedoritemService cotacaofornecedoritemService;
	private SolicitacaocompraService solicitacaocompraService;
	private CotacaoDAO cotacaoDAO;
	private CotacaoorigemService cotacaoorigemService;
	private EnderecoService enderecoService;
	private MaterialService materialService;
	private AvisoService avisoService;
	private SolicitacaocomprahistoricoService solicitacaocomprahistoricoService;
	private VwultimovalorcotacaoService vwultimovalorcotacaoService;
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	private ParametrogeralService parametrogeralService;
	private UsuarioService usuarioService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private EntregamaterialService entregamaterialService;
	private OrdemcompraService ordemcompraService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private PedidovendamaterialService pedidovendamaterialService;
	private UnidademedidaService unidademedidaService;
	private MotivoavisoService motivoavisoService;
	private EmpresaService empresaService;
	private AvisousuarioService avisoUsuarioService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCotacaoorigemService(
			CotacaoorigemService cotacaoorigemService) {
		this.cotacaoorigemService = cotacaoorigemService;
	}
	public void setCotacaoDAO(CotacaoDAO cotacaoDAO) {
		this.cotacaoDAO = cotacaoDAO;
	}
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setCotacaofornecedoritemService(
			CotacaofornecedoritemService cotacaofornecedoritemService) {
		this.cotacaofornecedoritemService = cotacaofornecedoritemService;
	}
	public void setCotacaofornecedorService(CotacaofornecedorService cotacaofornecedorService) {
		this.cotacaofornecedorService = cotacaofornecedorService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setSolicitacaocomprahistoricoService(
			SolicitacaocomprahistoricoService solicitacaocomprahistoricoService) {
		this.solicitacaocomprahistoricoService = solicitacaocomprahistoricoService;
	}
	public void setVwultimovalorcotacaoService(
			VwultimovalorcotacaoService vwultimovalorcotacaoService) {
		this.vwultimovalorcotacaoService = vwultimovalorcotacaoService;
	}
	public void setEnvioemailitemService(
			EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	public void setUnidademedidaconversaoService(
			UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setOrdemcompramaterialService(
			OrdemcompramaterialService ordemcompramaterialService) {
		this.ordemcompramaterialService = ordemcompramaterialService;
	}
	public void setColaboradorFornecedorService (ColaboradorFornecedorService colaboradorFornecedorService){
		this.colaboradorFornecedorService = colaboradorFornecedorService;
	}
	public void setPedidovendamaterialService (PedidovendamaterialService pedidovendamaterialService){
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	
	@Override
	public void saveOrUpdate(final Cotacao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				boolean isCriar = bean.getCdcotacao() == null;
				
				List<Cotacaofornecedor> lista = bean.getListaCotacaofornecedor();
				bean.setListaCotacaofornecedor(null);
				
				saveOrUpdateNoUseTransaction(bean);
				
				//Salva avisos gerais para os niveis
				if(isCriar){
					avisoService.salvarAvisos(montaAvisosCotacao(MotivoavisoEnum.COTACA_GERADA, bean, "Aguardando cota��o"), true);
				}
				
				if (bean.getFromSolicitacao() != null && bean.getFromSolicitacao() && bean.getListaSolicitacao() != null && bean.getListaSolicitacao().size() > 0) {
					
					Cotacaoorigem cotacaoorigem = null;
					List<Cotacaoorigem> listaSolicitacao = new ListSet<Cotacaoorigem>(Cotacaoorigem.class);			
					for (Solicitacaocompra solicitacaocompra : bean.getListaSolicitacao()) {
						cotacaoorigem = new Cotacaoorigem();
						cotacaoorigem.setCotacao(bean);
						cotacaoorigem.setSolicitacaocompra(solicitacaocompra);
						listaSolicitacao.add(cotacaoorigem);
						
						solicitacaocompra.getSolicitacaocomprahistorico().setObservacao("Cota��o: <a href=\"#\" onclick=\"redirecionaCotacao("+bean.getCdcotacao()+")\">"+bean.getCdcotacao()+"</a>");
						solicitacaocomprahistoricoService.saveOrUpdateNoUseTransaction(solicitacaocompra.getSolicitacaocomprahistorico());
					}
					
					for (Cotacaoorigem cotacaoorigem2 : listaSolicitacao) {
						cotacaoorigemService.saveOrUpdateNoUseTransaction(cotacaoorigem2);
					}
					
				}
				
				String string = "";
				for (Cotacaofornecedor cotacaofornecedor2 : lista) {
					if (cotacaofornecedor2.getCdcotacaofornecedor() != null) {
						string += cotacaofornecedor2.getCdcotacaofornecedor();
						string += ",";
					}
				}
				
				if (!string.equals("")) {
					string = string.substring(0, string.length()-1);
					cotacaofornecedorService.deleteNaoExistentes(bean,string);
				}
				
				
				for (Cotacaofornecedor cotacaofornecedor : lista) {
					cotacaofornecedor.setCotacao(bean);
					cotacaofornecedorService.saveOrUpdateNoUseTransaction(cotacaofornecedor);
				}
				
				return null;
			}
		});
		
		
		// ATUALIZA A TABELA AUXILIAR DE COTA��O
		this.callProcedureAtualizaCotacao(bean);
	}
	
	/**
	 * Monta uma lista de avisos gerais
	 * 
	 * @param listaPapeis
	 * @param cotacao
	 * @param assunto
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> montaAvisosCotacao(MotivoavisoEnum motivo, Cotacao cotacao, String assunto) {
		List<Aviso> avisos = new ListSet<Aviso>(Aviso.class);
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		if(motivoAviso != null){
			avisos.add(new Aviso(assunto, "Cota��o: "+cotacao.getCdcotacao(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
					motivoAviso.getUsuario(), AvisoOrigem.COTACAO, cotacao.getCdcotacao(), cotacao.getEmpresa(), null, motivoAviso));
		}
		
		return avisos;
	}
	
	@Override
	public void delete(final Cotacao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				Cotacao cotacao = loadForEntrada(bean);
				
				List<Cotacaofornecedor> listaCotacaofornecedor = cotacao.getListaCotacaofornecedor();
				List<Cotacaofornecedoritem> listaCotacaofornecedoritem = null;
				if (listaCotacaofornecedor != null) {
					for (Cotacaofornecedor cotacaofornecedor : listaCotacaofornecedor) {
						listaCotacaofornecedoritem = cotacaofornecedor.getListaCotacaofornecedoritem();
						if (listaCotacaofornecedoritem != null) {
							for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaofornecedoritem) {
								cotacaofornecedoritemService.delete(cotacaofornecedoritem);
							}
						}
						cotacaofornecedorService.delete(cotacaofornecedor);
					}
				}
				
				List<Cotacaoorigem> listaSolicitacao = cotacaoorigemService.findByCotacao(bean);
				for (Cotacaoorigem cotacaoorigem : listaSolicitacao) {
					cotacaoorigemService.delete(cotacaoorigem);
				}
				
				getGenericDAO().delete(cotacao);
				
				return null;
			}
		});
	}
	
	/**
	 * M�todo que verifica a exist�ncia de dois fornecedores iguais no cadastro.
	 * 
	 * @param errors
	 * @param listaFornecedor
	 * @author Rodrigo Freitas
	 */
	public void verificaFornecedores(BindException errors, List<Fornecedor> listaFornecedor) {
		Boolean ok = false;
		for (int i = 0; i < listaFornecedor.size(); i++) {
			for (int j = 0; j < listaFornecedor.size(); j++) {
				if (j==i) {
					continue;
				}
				if (listaFornecedor.get(i).equals(listaFornecedor.get(j))) {
					errors.reject("001","N�o � poss�vel criar uma cota��o com dois fornecedores iguais.");
					ok = true;
					break;
				}
				if (ok) {
					break;
				}
			}
		}
	}
	
	/**
	 * Verifica se material e local j� existe na lista, se existir soma a qtdesol do item.
	 * 
	 * @param cotacaofornecedoritem
	 * @param find
	 * @param listaCotacaofornecedoritem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean verificaMatLocalSoma(Cotacaofornecedoritem cotacaofornecedoritem, Boolean find, List<Cotacaofornecedoritem> listaCotacaofornecedoritem) {
		for (Cotacaofornecedoritem mat : listaCotacaofornecedoritem) {
			if (mat.getMaterial().equals(cotacaofornecedoritem.getMaterial()) &&
					mat.getLocalarmazenagem().equals(cotacaofornecedoritem.getLocalarmazenagem())) {
				mat.setQtdesol(mat.getQtdesol() + cotacaofornecedoritem.getQtdesol());
				find = true;
			}
		}
		return find;
	}
	
	/**
	 * M�todo que adiciona um material a todas as listas de materiais a ser cotados.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#verificaMatLocalSoma
	 * @param cotacao
	 * @param cotacaofornecedoritem
	 * @author Rodrigo Freitas
	 */
	public void adicionaFornMat(Cotacao cotacao, Cotacaofornecedoritem cotacaofornecedoritem) {
		
		Boolean find;
		List<Cotacaofornecedor> listaCotacaofornecedor = cotacao.getListaCotacaofornecedor();
		List<Cotacaofornecedoritem> listaCotacaofornecedoritem;
		
		for (Cotacaofornecedor cotacaofornecedor : listaCotacaofornecedor) {
			listaCotacaofornecedoritem = cotacaofornecedor.getListaCotacaofornecedoritem();
			find = false;
			
			find = this.verificaMatLocalSoma(cotacaofornecedoritem, find,listaCotacaofornecedoritem);
			if (!find) {
				cotacaofornecedoritem.setIsServico(materialService.load(cotacaofornecedoritem.getMaterial()).getServico());
				listaCotacaofornecedoritem.add(cotacaofornecedoritem);
			}
			cotacaofornecedor.setListaCotacaofornecedoritem(listaCotacaofornecedoritem);
		}
		cotacao.setListaCotacaofornecedor(listaCotacaofornecedor);
	}
		
	/**
	 * M�todo que copia do primeiro item da lista somente 
	 * os dados necess�rios para exibir nos proximos itens da lista de fornecedor na cota��o.
	 * 
	 * @author Taidson Santos
	 */
	public List<Cotacaofornecedoritem> getListaDefault(List<Cotacaofornecedor> lista) {
		List<Cotacaofornecedoritem> listaDefault = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		Cotacaofornecedor cotacaofornecedor = lista.get(0);
		for (Cotacaofornecedoritem item : cotacaofornecedor.getListaCotacaofornecedoritem()) {
			Cotacaofornecedoritem novoItem = new Cotacaofornecedoritem();
			listaDefault.add(novoItem);
			novoItem.setMaterial(item.getMaterial());
			novoItem.setFrequencia(item.getFrequencia());
			novoItem.setQtdesol(item.getQtdesol());
			novoItem.setQtdefrequencia(item.getQtdefrequencia());
			novoItem.setLocalarmazenagem(item.getLocalarmazenagem());
			if(SinedUtil.isListNotEmpty(item.getListaCotacaofornecedoritemsolicitacaocompra())){
				Set<Cotacaofornecedoritemsolicitacaocompra> listaCfisc = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
				for(Cotacaofornecedoritemsolicitacaocompra cfisc: item.getListaCotacaofornecedoritemsolicitacaocompra()){
					if(cfisc.getSolicitacaocompra() != null){
						Cotacaofornecedoritemsolicitacaocompra cfiscNovo = new Cotacaofornecedoritemsolicitacaocompra();
						cfiscNovo.setSolicitacaocompra(cfisc.getSolicitacaocompra());
						listaCfisc.add(cfiscNovo);
					}
				}
				novoItem.setListaCotacaofornecedoritemsolicitacaocompra(listaCfisc);
			}
		}
		
		return listaDefault;
	}
	
	/**
	 * Zera os atributos usados na sess�o.
	 * 
	 * @param request
	 * @author Rodrigo Freitas
	 */
	/*public void zeraAtributosSessao(WebRequestContext request){
		request.getSession().removeAttribute("cotacaoSolicitacao");
		request.getSession().removeAttribute("listaMaterialSessao");
	}*/
	
	/**
	 * Verifica a existencia de dois fornecedores iguais na lista de cota��es.
	 * 
	 * @param errors
	 * @param lista
	 * @author Rodrigo Freitas
	 */
	public void verificaFornecedorIgual(BindException errors,
			List<CotacaoFornecedorBean> lista) {
		boolean ok = false;
		for (int i = 0; i < lista.size(); i++) {
			for (int j = 0; j < lista.size(); j++) {
				if (i==j) {
					continue;
				}
				if (lista.get(i).getFornecedor().getCdpessoa().equals(lista.get(j).getFornecedor().getCdpessoa())) {
					errors.reject("001","N�o � poss�vel escolher dois fornecedores iguais.");
					ok = true;
					break;
				}
			}
			if (ok) {
				break;
			}
		}
	}
	
	/**
	 * Pega o whereIn que est� como Par�metro na requisi��o.
	 * 
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void getWhereInRequest(WebRequestContext request, CotacaoBean bean) {
		String whereIn = request.getParameter("selectedItens");
		if (whereIn == null || whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		bean.setIds(whereIn);
	}
	
	/**
	 * Preenche a lista de Cotacaofornecedor para a exibi��o na tela.
	 * 
	 * @param listaFornecedor
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacaofornecedor> preencheListaCotacaofornecedor(List<CotacaoFornecedorBean> listaFornecedor) {
		
		List<Cotacaofornecedor> listaForn = new ListSet<Cotacaofornecedor>(Cotacaofornecedor.class);
		Cotacaofornecedor cotacaofornecedor = null;
		
		for (CotacaoFornecedorBean cotacaoFornecedorBean : listaFornecedor) {
			if (cotacaoFornecedorBean.getFornecedor() != null) {
				cotacaofornecedor = new Cotacaofornecedor();
				cotacaofornecedor.setFornecedor(cotacaoFornecedorBean.getFornecedor());
				listaForn.add(cotacaofornecedor);
			}
		}
		return listaForn;
	}
	
	/**
	 * M�todo que preenche a lista dos fornecedores com os materiais
	 * 
	 * @param cotacao
	 * @param lista
	 * @author Tom�s Rabelo
	 */
	public void preencheListaCotacaofornecedoritem(Cotacao cotacao,	List<Cotacaofornecedoritem> lista) {
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			cotacaofornecedor.setConviteenviado(Boolean.FALSE);
			List<Cotacaofornecedoritem> itens = new ArrayList<Cotacaofornecedoritem>();
			for (Cotacaofornecedoritem cotacaofornecedoritem : lista) {
//				Date entrega  = cotacaofornecedoritem.getDtentrega();
//				List<Material> mat = materialService.findForOrdenarMaterialproducao(cotacaofornecedoritem.getMaterial().getCdmaterial().toString());
//				if(mat.get(0).getListaFornecedor() !=null && !mat.isEmpty() && !mat.get(0).getListaFornecedor().isEmpty()){
//					Set<Materialfornecedor> fornecedorCadastrado = mat.get(0).getListaFornecedor();	
//					for (Materialfornecedor fornecedor : fornecedorCadastrado) {
//						String nomeFornecedorCadastrado = fornecedor.getFornecedor().getNome();
//						String nomeFornecedorCotacao = cotacaofornecedor.getFornecedor().getNome();
//						if(nomeFornecedorCadastrado.equals(nomeFornecedorCotacao)){
//							if(fornecedor.getTempoentrega() !=null){
//								entrega = SinedDateUtils.addDiasUteisData(SinedDateUtils.currentDate(),fornecedor.getTempoentrega());	
//							}
//						}
//					}
//				}
//				
//				Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra();
//				Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompraNova = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
//				for (Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra : listaCotacaofornecedoritemsolicitacaocompra) {
//					listaCotacaofornecedoritemsolicitacaocompraNova.add(new Cotacaofornecedoritemsolicitacaocompra(cotacaofornecedoritemsolicitacaocompra.getSolicitacaocompra()));
//				}
//				
//				Cotacaofornecedoritem item = new Cotacaofornecedoritem(cotacaofornecedor, 
//						cotacaofornecedoritem.getQtdesol(),
//						cotacaofornecedoritem.getLocalarmazenagem(), 
//						cotacaofornecedoritem.getMaterial(), 
//						cotacaofornecedoritem.getMaterialclasse(),
//						cotacaofornecedoritem.getLocalarmazenagemsolicitacao(), 
//						cotacaofornecedoritem.getFrequencia(),
//						cotacaofornecedoritem.getQtdefrequencia(), 
//						cotacaofornecedoritem.getUnidademedida(), 
//						cotacaofornecedoritem.getObservacao(), 
//						listaCotacaofornecedoritemsolicitacaocompraNova,
//						entrega);
//				itens.add(item);
//				Removendo altera��o AT-196166  
				Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra();
				
				Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompraNova = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
				for (Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra : listaCotacaofornecedoritemsolicitacaocompra) {
					listaCotacaofornecedoritemsolicitacaocompraNova.add(new Cotacaofornecedoritemsolicitacaocompra(cotacaofornecedoritemsolicitacaocompra.getSolicitacaocompra()));
				}
				
				itens.add(new Cotacaofornecedoritem(cotacaofornecedor, cotacaofornecedoritem.getQtdesol(), cotacaofornecedoritem.getLocalarmazenagem(), 
						cotacaofornecedoritem.getMaterial(), cotacaofornecedoritem.getMaterialclasse(), cotacaofornecedoritem.getLocalarmazenagemsolicitacao(), 
						cotacaofornecedoritem.getFrequencia(), cotacaofornecedoritem.getQtdefrequencia(), cotacaofornecedoritem.getUnidademedida(), 
						cotacaofornecedoritem.getObservacao(), listaCotacaofornecedoritemsolicitacaocompraNova));
			}
			cotacaofornecedor.setListaCotacaofornecedoritem(itens);

		}
	}
	
	/**
	 * Verifica a lista de materiais se existir soma a qtdesol. 
	 * 
	 * @param lista
	 * @param find
	 * @param solicitacaocompra
	 * @return
	 * @author Rodrigo Freitas
	 * @param localEntregaUnico 
	 */
	public Boolean verificaMatLocalSoma(List<Cotacaofornecedoritem> lista, Boolean find, Solicitacaocompra solicitacaocompra, Localarmazenagem localEntregaUnico, Boolean zerarValores) {
		
		for (Cotacaofornecedoritem cotacaofornecedoritem : lista) {
			if (cotacaofornecedoritem.getMaterial().equals(solicitacaocompra.getMaterial()) &&
					(localEntregaUnico != null || 
					 cotacaofornecedoritem.getLocalarmazenagem().equals(solicitacaocompra.getLocalarmazenagem())) &&
					cotacaofornecedoritem.getFrequencia().equals(solicitacaocompra.getFrequencia()) &&
					cotacaofornecedoritem.getQtdefrequencia().equals(solicitacaocompra.getQtdefrequencia())) {
				Double valorunitario = cotacaofornecedoritem.getValorunitario();
				if(valorunitario == null) valorunitario = 0d;
				
				Double desconto = cotacaofornecedoritem.getDesconto() != null ? cotacaofornecedoritem.getDesconto().getValue().doubleValue() : 0d; 
				Double qtdeSol = solicitacaocompra.getQtde();
				if(cotacaofornecedoritem.getUnidademedida() != null && solicitacaocompra.getUnidademedida() != null && 
						!cotacaofornecedoritem.getUnidademedida().equals(solicitacaocompra.getUnidademedida())){
					qtdeSol = unidademedidaService.getQtdeConvertida(solicitacaocompra.getMaterial(), cotacaofornecedoritem.getUnidademedida(), solicitacaocompra.getUnidademedida(), qtdeSol);
				}
				
				cotacaofornecedoritem.setQtdesol(cotacaofornecedoritem.getQtdesol() + qtdeSol);
				if(cotacaofornecedoritem.getQtdecot() != null){
					cotacaofornecedoritem.setQtdecot(cotacaofornecedoritem.getQtdecot() + qtdeSol);
					if(zerarValores == null || !zerarValores){
						cotacaofornecedoritem.setValor(((cotacaofornecedoritem.getQtdecot() * cotacaofornecedoritem.getQtdefrequencia()) * valorunitario) - desconto);
					}
				}
				
				if(zerarValores != null && zerarValores){
					cotacaofornecedoritem.setValor(null);
					cotacaofornecedoritem.setDesconto(null);
					cotacaofornecedoritem.setFrete(null);
					cotacaofornecedoritem.setValorunitario(null);
					cotacaofornecedoritem.setIcmsiss(null);
					cotacaofornecedoritem.setIpi(null);
				}
				
				cotacaofornecedoritem.setLocalarmazenagem(localEntregaUnico != null ? localEntregaUnico : solicitacaocompra.getLocalarmazenagem());
				
				Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra = new Cotacaofornecedoritemsolicitacaocompra();
				cotacaofornecedoritemsolicitacaocompra.setSolicitacaocompra(solicitacaocompra);
				cotacaofornecedoritem.getListaCotacaofornecedoritemsolicitacaocompra().add(cotacaofornecedoritemsolicitacaocompra);
				
				find = true;
			}
		}
		return find;
	}
	
	
	
	/**
	 * Seta as informa��es para que tela seja carrega com todas as informa��es necess�rias.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#geraStringLink
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findByCotacao
	 * @param request
	 * @param form
	 * @author Rodrigo Freitas
	 * @author Tom�s T. Rabelo
	 */
	public void setInfo(WebRequestContext request, Cotacao form) {
		this.setInfOrdemcompragerada(form);
		request.setAttribute("tamanholista", form.getListaCotacaofornecedor() != null ? form.getListaCotacaofornecedor().size() : 0);
		request.setAttribute("tamanholistaItem", getTamanhoListaFornecedorItem(form.getListaCotacaofornecedor()));
//		Boolean fromSolicitacaoCompra = false;
		/* Modo antigo deixado como backup
		 * 
		 * if (form.getCdcotacao() != null) {
			List<Solicitacaocompra> findByCotacao = solicitacaocompraService.findByCotacao(form);
			fromSolicitacaoCompra = findByCotacao.size() > 0;	
			if (fromSolicitacaoCompra) {
				form.setIdssolicitacao(this.geraStringLink(findByCotacao,request));
			}
		} else if (form.getFromSolicitacao() != null && form.getFromSolicitacao()) {
			List<Solicitacaocompra> attribute = (List<Solicitacaocompra>)request.getSession().getAttribute("solicitacoesSalvar");
			form.setIdssolicitacao(this.geraStringLink(attribute,request));
		}*/
		List<OrigemsuprimentosBean> listaBeanOrigem = new ArrayList<OrigemsuprimentosBean>();
		List<OrigemsuprimentosBean> listaBeanDestino = new ArrayList<OrigemsuprimentosBean>();
		if (form.getCdcotacao() != null) {
			List<Solicitacaocompra> findByCotacao = solicitacaocompraService.findByCotacao(form);
//			fromSolicitacaoCompra = findByCotacao.size() > 0;	
//			if (fromSolicitacaoCompra) {
			listaBeanOrigem = this.montaOrigem(findByCotacao);
//			}
		} /*else if (form.getFromSolicitacao() != null && form.getFromSolicitacao()) {
			List<Solicitacaocompra> attribute = (List<Solicitacaocompra>)request.getSession().getAttribute("solicitacoesSalvar");
			listaBeanOrigem = this.montaOrigem(attribute);
		}*/
		
		if(form.getListaOrdemcompra() != null && form.getListaOrdemcompra().size() > 0){
			listaBeanDestino = this.montaDestino(form.getListaOrdemcompra());
		}
		
		if(form.getListaCotacaofornecedor() != null && !form.getListaCotacaofornecedor().isEmpty()){
			for(Cotacaofornecedor cf : form.getListaCotacaofornecedor()){
				List<Cotacaofornecedoritem> listaCFI = cf.getListaCotacaofornecedoritem();
				if( listaCFI != null && !listaCFI.isEmpty()){
					for(Cotacaofornecedoritem cfi : listaCFI){
						cfi.setUnidademedidatrans(cfi.getUnidademedida());
						request.setAttribute("listaUM"+cfi.getMaterial().getCdmaterial(), unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(cfi.getMaterial().getUnidademedida(), cfi.getMaterial()));
					}
				}
			}
		}
		request.setAttribute("listaOrigem", listaBeanOrigem);
		request.setAttribute("listaDestino", listaBeanDestino);
//		request.setAttribute("fromSolicitacaoCompra", fromSolicitacaoCompra || (form.getFromSolicitacao() != null ? form.getFromSolicitacao() : false));
	}
	
	/**
	 * M�todo que seta informa��es da cota��o com ordem de compra gerada
	 *
	 * @param cotacao
	 * @author Luiz Fernando
	 */
	public void setInfOrdemcompragerada(Cotacao cotacao){
		if(cotacao != null && cotacao.getCdcotacao() != null){
			List<Material> listaMateriaisOrdemcompra = new ArrayList<Material>();
			List<Ordemcompra> listaOrdemcompra = ordemcompraService.findForEdicaoCotacao(cotacao);
			if(listaOrdemcompra != null && !listaOrdemcompra.isEmpty()){
				for(Ordemcompra ordemcompra : listaOrdemcompra){
					if(ordemcompra.getListaMaterial() != null && !ordemcompra.getListaMaterial().isEmpty()){
						for(Ordemcompramaterial ordemcompramaterial : ordemcompra.getListaMaterial()){
							if(ordemcompramaterial.getMaterial() != null && 
									!listaMateriaisOrdemcompra.contains(ordemcompramaterial.getMaterial())){
									listaMateriaisOrdemcompra.add(ordemcompramaterial.getMaterial());
							}
						}
					}
				}
			}
			if(listaMateriaisOrdemcompra != null && !listaMateriaisOrdemcompra.isEmpty()){
				for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
					if(cf.getListaCotacaofornecedoritem() != null && !cf.getListaCotacaofornecedoritem().isEmpty()){
						for(Cotacaofornecedoritem cfitem : cf.getListaCotacaofornecedoritem()){
							if(cfitem.getMaterial() != null && listaMateriaisOrdemcompra.contains(cfitem.getMaterial())){
								cfitem.setOrdemcompragerada(true);
								cfitem.setChecado(false);
							}
						}
					}
				}
			}
		}
	}
	
	public int getTamanhoListaFornecedorItem(List<Cotacaofornecedor> listaCotacao){
		for (Cotacaofornecedor cotacaofornecedor : listaCotacao) {
			return cotacaofornecedor.getListaCotacaofornecedoritem().size();			
		}
		return 0;
	}
	
	/**
	 * M�todo que monta as destino(ordens de compra) com link que redireciona para tela de entrada
	 * em modo de consulta
	 * 
	 * @param listaOrdemcompra
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	private List<OrigemsuprimentosBean> montaDestino(Set<Ordemcompra> listaOrdemcompra) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		for (Ordemcompra ordemcompra : listaOrdemcompra) {
			if(ordemcompra.getDtcancelamento() == null){
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean("ORDEM DE COMPRA", ordemcompra.getCdordemcompra().toString());
				listaBean.add(origemsuprimentosBean);
			}
		}
		return listaBean;
	}
	
	/**
	 * M�todo que monta as origens(solicita��es de compra) com link que redireciona para tela de entrada
	 * em modo de consulta
	 * 
	 * @param findByCotacao
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	private List<OrigemsuprimentosBean> montaOrigem(List<Solicitacaocompra> findByCotacao) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		for (Solicitacaocompra solicitacaocompra : findByCotacao) {
			OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean("SOL. DE COMPRA:", solicitacaocompra.getCdsolicitacaocompra().toString(), (solicitacaocompra.getDescricao() != null ? solicitacaocompra.getDescricao().substring(0,solicitacaocompra.getDescricao().length() < 46 ? solicitacaocompra.getDescricao().length() : 45)+(solicitacaocompra.getDescricao().length() < 46 ? "" : "...") : "") );
			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * Gera a string com o link das solicita��es de compra que originaram a cota��o.
	 * 
	 * @param findByCotacao
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	/* Modo antigo deixado como backup
	 * 
	 * private String geraStringLink(List<Solicitacaocompra> findByCotacao, WebRequestContext request) {
		String string = "";
		for (Solicitacaocompra solicitacaocompra : findByCotacao) {
			string += "<a href=\""+request.getServletRequest().getContextPath()+"/suprimento/crud/Solicitacaocompra?ACAO=consultar&cdsolicitacaocompra="
			+ solicitacaocompra.getCdsolicitacaocompra()+"\">"+solicitacaocompra.getCdsolicitacaocompra()+"</a>, ";
		}
		if (!string.equals("")) {
			string = string.substring(0, string.length()-2);
		}
		return string;
	}*/
	
	/**
	 * Verifica se tem pelo menos um fornecedor cadastrado.
	 * 
	 * @param errors
	 * @param lista
	 * @author Rodrigo Freitas
	 */
	public void verificaUmFornecedor(BindException errors,List<CotacaoFornecedorBean> lista) {
		boolean ok = true;
		for (CotacaoFornecedorBean cotacaoFornecedorBean : lista) {
			if (cotacaoFornecedorBean.getFornecedor() != null) {
				ok = false;
			}
		}
		if (ok) {
			errors.reject("001", "� obrigat�rio escolher pelo menos um fornecedor.");
		}
	}
	
	/**
	 * Adiciona um novo fornecedor na lista.
	 * 
	 * @param cotacao
	 * @param lista
	 * @param listaDefault
	 * @author Rodrigo Freitas
	 */
	public void adicionaFornecedor(Cotacao cotacao, List<Cotacaofornecedor> lista,List<Cotacaofornecedoritem> listaDefault) {
		Cotacaofornecedor cotacaofornecedor = new Cotacaofornecedor();
		cotacaofornecedor.setListaCotacaofornecedoritem(listaDefault);
		cotacaofornecedor.setFornecedor(cotacao.getFornecedor());
		
		if(lista.size() > 0){
			boolean achou = false;
			if(cotacaofornecedor.getListaCotacaofornecedoritem() != null && !cotacaofornecedor.getListaCotacaofornecedoritem().isEmpty()){
				for(Cotacaofornecedoritem item :cotacaofornecedor.getListaCotacaofornecedoritem()){
					if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null){
						for(Cotacaofornecedor cf : lista){
							if(cf.getListaCotacaofornecedoritem() != null && !cf.getListaCotacaofornecedoritem().isEmpty()){
								for(Cotacaofornecedoritem cfitem : cf.getListaCotacaofornecedoritem()){
									if(cfitem.getMaterial() != null && item.getMaterial().equals(cfitem.getMaterial())){
										Unidademedida unidademedida = null;
										List<Solicitacaocompra> listaSC = new ArrayList<Solicitacaocompra>();
										if(SinedUtil.isListNotEmpty(cfitem.getListaCotacaofornecedoritemsolicitacaocompra())){
											for(Cotacaofornecedoritemsolicitacaocompra sfisc : cfitem.getListaCotacaofornecedoritemsolicitacaocompra()){
												if(sfisc.getSolicitacaocompra() != null){
													listaSC.add(sfisc.getSolicitacaocompra());
												}
											}
											if(SinedUtil.isListNotEmpty(listaSC)){
												listaSC = solicitacaocompraService.findWithUnidademedida(CollectionsUtil.listAndConcatenate(listaSC, "cdsolicitacaocompra", ","));
												if(SinedUtil.isListNotEmpty(listaSC)){
													for(Solicitacaocompra sc : listaSC){
														if(sc.getUnidademedida() != null){
															if(unidademedida == null){
																unidademedida = sc.getUnidademedida();
															}else if(!unidademedida.equals(sc.getUnidademedida())){
																if(sc.getMaterial() != null){
																	unidademedida = sc.getMaterial().getUnidademedida();
																}
																break;
															}
														}
													}
												}
											}
										}
										 
										Double qtdeCot = cfitem.getQtdecot();
										Double qtdeSol = cfitem.getQtdesol();
										if(cfitem.getUnidademedida() != null && unidademedida != null && 
												!cfitem.getUnidademedida().equals(unidademedida)){
											Double fracao = unidademedidaService.getFatorconversao(
																cfitem.getUnidademedida(), 
																qtdeSol, 
																unidademedida, 
																cfitem.getMaterial(), null, 1d);
											if(fracao != null){
												if(qtdeSol != null) qtdeSol = qtdeSol *fracao;
												if(qtdeCot != null) qtdeCot = qtdeCot *fracao;
											}
										}
										
										item.setUnidademedida(unidademedida);
										item.setUnidademedidatrans(unidademedida);
										item.setQtdecot(qtdeCot);
										item.setQtdesol(qtdeSol);
										achou = true;
										break;
									}
								}
								if(achou) break;
							}							
						}
						achou = false;
					}
				}
			}
		}
		
		lista.add(cotacaofornecedor);
		
		
		
		cotacao.setListaCotacaofornecedor(lista);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#loadWithLista
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return cotacaoDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#loadWithLista
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Cotacao loadWithLista(Cotacao cotacao, Fornecedor fornecedor, Localarmazenagem localarmazenagem, Material material) {
		return cotacaoDAO.loadWithLista(cotacao, fornecedor, localarmazenagem, material);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#updateStatus
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateStatus(String whereIn, Situacaosuprimentos situacao) {
		cotacaoDAO.updateStatus(whereIn,situacao);
		
		//ATUALIZA OS VALORES DA TABELA AUXILIAR DE COTA��O
		this.callProcedureAtualizaCotacao(whereIn);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#updateEstornada
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	public void updateEstornada(String whereIn) {
		cotacaoDAO.updateEstornada(whereIn);
		
		//ATUALIZA OS VALORES DA TABELA AUXILIAR DE COTA��O
		this.callProcedureAtualizaCotacao(whereIn);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#findForVerificacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> findForVerificacao(String whereIn){
		return cotacaoDAO.findForVerificacao(whereIn);
	}
	
	/**
	 * Gera relat�rio com valores iguais o da listagem.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#findForListagem(br.com.linkcom.neo.controller.crud.FiltroListagem)
	 * @see #loadWithLista(String, String, boolean)
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(CotacaoFiltro filtro) {
		Report report = new Report("/suprimento/cotacao");
		
		ListagemResult<Cotacao> cotacoes = cotacaoDAO.findForListagem(filtro);
		List<Cotacao> list = cotacoes.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcotacao", ",");
		list.removeAll(list);
		if(StringUtils.isNotBlank(whereIn)){
			list.addAll(loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}
		
		this.formataStringFornecedores(list);
		
		if(cotacoes != null && cotacoes.list().size() > 0)
			report.setDataSource(cotacoes.list());
		return report;
	}
	
	/**
	 * Gera relat�rio de emiss�o de cota��o, para o usu�rio enviar para os
	 * respectivos fornecedores
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#findFornCotacoes(String)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#carregaEnderecoEmpresa(Empresa)
	 * @see #montaInformacoesCotacaoFornecedorParaRelatorio(Cotacaofornecedor, Boolean)
	 * @see #complementosRelatorioEmissaoCotacao
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 * @param where 
	 */
	public Resource emitirCotacao(WebRequestContext request) throws Exception {
		MergeReport mergeReport = new MergeReport("cotacao_"+SinedUtil.datePatternForReport()+".pdf");
		
		Boolean convite = false;
		List<Cotacaofornecedor> cotacoes = cotacaofornecedorService.findFornCotacoes(request.getParameter("selectedItens"));
		if ("true".equals(request.getParameter("convite"))) {
			convite = true;
		}
		
//		Empresa empresa = EmpresaService.getInstance().loadPrincipal();
//		Endereco enderecoEmpresa = null;
//		if(empresa != null)
//			enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(empresa);
		
		Empresa empresaPrincipal = EmpresaService.getInstance().loadPrincipal();
		Empresa empresaCotacao = null;
		List<Cotacaoorigem> listaCO = cotacaoorigemService.findByCotacao(request.getParameter("selectedItens"));
		Map<Integer, Empresa> mapa = new HashMap<Integer, Empresa>();
		
		for (Cotacaoorigem cotacaoorigem: listaCO){
			if (cotacaoorigem.getSolicitacaocompra().getEmpresa()!=null 
					&& !mapa.containsKey(cotacaoorigem.getCotacao().getCdcotacao())){
				mapa.put(cotacaoorigem.getCotacao().getCdcotacao(), cotacaoorigem.getSolicitacaocompra().getEmpresa());
			}
			if(cotacaoorigem.getCotacao() != null && cotacaoorigem.getCotacao().getEmpresa() != null){
				empresaCotacao = cotacaoorigem.getCotacao().getEmpresa();
			}
		}
		
		//Carrega endere�o principal do fornecedor e telefone principal do contato
		for (Cotacaofornecedor cotacaofornecedor2 : cotacoes) {
			
			Empresa empresa = empresaCotacao != null ? empresaCotacao : mapa.get(cotacaofornecedor2.getCotacao().getCdcotacao());
			Endereco enderecoEmpresa = null;
			
			if (empresa==null){
				empresa = empresaPrincipal;
			}
			
			if (empresa!=null){
				enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(empresa);
			}
			
			montaInformacoesCotacaoFornecedorParaRelatorio(cotacaofornecedor2, convite);
			
			Report report = complementosRelatorioEmissaoCotacao(cotacaofornecedor2, empresa, enderecoEmpresa, convite);
			
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que pega as informa��es necess�rias para gerar o relat�rio de emiss�o de cota��o tanto por fornecedor quanto para a cota��o inteira
	 * 
	 * @see #buscaEnderecoFornecedor(Cotacaofornecedor)
	 * @see #buscaTelefoneContatoFornecedor(Cotacaofornecedor)
	 * @see #monstaCaracteristicasCotacaoFornecedorItem(List, Boolean)
	 * @param cotacaofornecedor2
	 * @param convite
	 */
	private void montaInformacoesCotacaoFornecedorParaRelatorio(Cotacaofornecedor cotacaofornecedor2, Boolean convite) {
		List<Cotacaofornecedoritem> list = new ArrayList<Cotacaofornecedoritem>();
		
		if(cotacaofornecedor2 != null && cotacaofornecedor2.getListaCotacaofornecedoritem() != null){
			for(Cotacaofornecedoritem item : cotacaofornecedor2.getListaCotacaofornecedoritem()){
				if(item.getNaocotar() == null || !item.getNaocotar()){
					list.add(item);
				}
			}
		}
		
		Collections.sort(list,new Comparator<Cotacaofornecedoritem>(){
			public int compare(Cotacaofornecedoritem o1, Cotacaofornecedoritem o2) {
				return o1.getLocalarmazenagem().getCdlocalarmazenagem().compareTo(o2.getLocalarmazenagem().getCdlocalarmazenagem());
			}
		});
		cotacaofornecedor2.setListaCotacaofornecedoritemAux(list);
		
		buscaEnderecoFornecedor(cotacaofornecedor2);
		buscaTelefoneContatoFornecedor(cotacaofornecedor2);
		montaCaracteristicasCotacaoFornecedorItem(list, convite);
	}
	
	/**
	 * M�todo que cria string com as caracteristicas dos materias para apresentar no relat�rio e caso o relat�rio seja "convite"
	 * ele retira os valores do produto e frete caso haja.
	 * 
	 * @param list
	 * @param convite
	 * @author Tom�s Rabelo
	 */
	private void montaCaracteristicasCotacaoFornecedorItem(List<Cotacaofornecedoritem> list, Boolean convite) {
		for (Cotacaofornecedoritem cotacaofornecedoritem : list) {
//			if(cotacaofornecedoritem.getMaterial().getListaCaracteristica() != null && cotacaofornecedoritem.getMaterial().getListaCaracteristica().size() > 0){
//				StringBuilder caracteristica = new StringBuilder();
//				for (Materialcaracteristica materialcaracteristica  : cotacaofornecedoritem.getMaterial().getListaCaracteristica()) {
//					caracteristica.append("* " + materialcaracteristica.getNome());
//					caracteristica.append(materialcaracteristica.getValor() != null ? " - "+materialcaracteristica.getValor()+"\n" : "\n");
//				}
//				if(caracteristica.length() > 0) caracteristica.delete(caracteristica.length()-2, caracteristica.length());
//				cotacaofornecedoritem.getMaterial().setCaracteristicas(caracteristica.toString());
//			}
			
			if (convite) {
				cotacaofornecedoritem.setValor(null);
				cotacaofornecedoritem.setFrete(null);
			}
		}
	}
	
	/**
	 * M�todo que busca o endere�o do fornecedor. Busca pelo primeiro que achar, pois se houver um �nico so poder� haver 1
	 * 
	 * @param cotacaofornecedor2
	 * @author Tom�s Rabelo
	 */
	private void buscaEnderecoFornecedor(Cotacaofornecedor cotacaofornecedor2) {
		if(cotacaofornecedor2.getFornecedor().getListaEndereco() != null && cotacaofornecedor2.getFornecedor().getListaEndereco().size() > 0){
			for (Endereco endereco : cotacaofornecedor2.getFornecedor().getListaEndereco()) {
				cotacaofornecedor2.getFornecedor().setEnderecoPrincipal(endereco);
				break;
			}
		}
	}
	
	/**
	 * M�todo que busca o telefone e fax do contato. Busca pelo telefone principal se n�o houver pega o primeiro
	 * que achar se ser fax.
	 * 
	 * @param cotacaofornecedor2
	 * @author Tom�s Rabelo
	 */
	private void buscaTelefoneContatoFornecedor(Cotacaofornecedor cotacaofornecedor2) {
		if(cotacaofornecedor2.getContato() != null){
			if(cotacaofornecedor2.getContato().getListaTelefone() != null && cotacaofornecedor2.getContato().getListaTelefone().size() > 0){
				boolean achouTelPrincipal = false;
				for (Telefone telefone : cotacaofornecedor2.getContato().getListaTelefone()) {
					if(cotacaofornecedor2.getContato().getFax() != null && achouTelPrincipal)
						break;
					
					if(!telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.FAX)){
						if(telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL))
							achouTelPrincipal = true;
						
						cotacaofornecedor2.getContato().setTelefonePrincipal(telefone);
					} else{
						cotacaofornecedor2.getContato().setFax(telefone);
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que cria inst�ncia do relat�rio passando os parametros necess�rios
	 * 
	 * @param cotacaofornecedor2
	 * @param empresa
	 * @param enderecoEmpresa
	 * @return
	 * @author Tom�s Rabelo
	 * @param convite 
	 */
	private Report complementosRelatorioEmissaoCotacao(Cotacaofornecedor cotacaofornecedor2, Empresa empresa, Endereco enderecoEmpresa, Boolean convite) {
		Report report = new Report("/suprimento/emissaocotacao");
		Report subreport1 = new Report("suprimento/sub_emissaocotacao");
		Report subreport2 = new Report("suprimento/sub_emissaocotacao_assinatura");
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		
		subreport2.addParameter("CRIADA", usuarioLogado.getNome());
		
		Usuario usuario = usuarioService.carregaRubricaUsuario(usuarioLogado);
		if(usuario != null && usuario.getRubrica() != null){
			try {
				Arquivo rubrica = usuario.getRubrica();
				rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
				Image imageRubricaElaboracao = ArquivoService.getInstance().loadAsImage(rubrica);
				report.addParameter("USUARIO_RUBRICA_ELABORACAO", imageRubricaElaboracao);
			} catch (Exception e) {
				//Se estiver testando local, n�o existe o arquivo
			}
		}
		
		report.addSubReport("SUB_EMISSAOCOTACAO", subreport1);
		report.addSubReport("SUB_EMISSAOCOTACAO_ASSINATURA", subreport2);
		
		report.setDataSource(new Cotacaofornecedor[]{cotacaofornecedor2});
		
		report.addParameter("CONVITE", new Boolean(convite));
		report.addParameter("TITULO", "COTA��O");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		if(empresa != null){
			try{
				Image image = SinedUtil.getLogo(empresa);
				report.addParameter("LOGO", image);
			} catch (Exception e) {
				e.printStackTrace();
			}
			report.addParameter("EMPRESA", empresa.getRazaosocialOuNome());
			report.addParameter("CNPJ", empresa.getCnpj());
			report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual());
			if(enderecoEmpresa != null)
				report.addParameter("endereco", enderecoEmpresa);
		}
		return report;
	}
	
	/**
	 * M�todo que gera relat�rio por fornecedor
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#findFornecedorCotacao(String)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @see br.com.linkcom.sined.geral.service.EnderecoService#carregaEnderecoEmpresa(Empresa)
	 * @see #montaInformacoesCotacaoFornecedorParaRelatorio(Cotacaofornecedor, Boolean)
	 * @see #complementosRelatorioEmissaoCotacao
	 * @param where
	 * @return
	 * @author Tom�s Rabelo
	 * @param filtro 
	 */
	public IReport emitirCotacaoFornecedor(CotacaoFiltro filtro, String where) {
		Cotacaofornecedor cotacaofornecedor = cotacaofornecedorService.findFornecedorCotacao(where);
		
		
		Empresa empresa = null;
		Endereco enderecoEmpresa = null;
		
		empresa = this.getEmpresaByCotacaofornecedor(cotacaofornecedor);
		
		if(empresa == null){
			empresa = EmpresaService.getInstance().loadPrincipal();
		}
		
		if(empresa != null)
			enderecoEmpresa = enderecoService.carregaEnderecoEmpresa(empresa);
		
		boolean convite = true;
		montaInformacoesCotacaoFornecedorParaRelatorio(cotacaofornecedor, convite);	
		
		if(filtro != null){
			filtro.setEmpresa(empresa);
		}
		
		return complementosRelatorioEmissaoCotacao(cotacaofornecedor, empresa, enderecoEmpresa, convite);
	}
	
	/**
	 * M�todo que retorna a empresa da cota��o
	 *
	 * @param cotacaofornecedor
	 * @return
	 * @author Luiz Fernando
	 * @since 10/10/2013
	 */
	private Empresa getEmpresaByCotacaofornecedor(Cotacaofornecedor cotacaofornecedor) {
		Empresa empresa = null;
		
		if(cotacaofornecedor != null && cotacaofornecedor.getCdcotacaofornecedor() != null){
			Cotacao cotacao = this.carregaCotacaoWithSolicitacaocompra(cotacaofornecedor);
			
			if(cotacao != null && cotacao.getListaOrigem() != null && !cotacao.getListaOrigem().isEmpty()){
				for(Cotacaoorigem cotacaoorigem : cotacao.getListaOrigem()){
					if(cotacaoorigem.getSolicitacaocompra() != null && cotacaoorigem.getSolicitacaocompra().getEmpresa() != null){
						empresa = cotacaoorigem.getSolicitacaocompra().getEmpresa();
						break;
					}
				}
			}
		}
		return empresa;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#carregaCotacaoWithSolicitacaocompra(Cotacaofornecedor cotacaofornecedor)
	 *
	 * @param cotacaofornecedor
	 * @return
	 * @author Luiz Fernando
	 * @since 10/10/2013
	 */
	public Cotacao carregaCotacaoWithSolicitacaocompra(Cotacaofornecedor cotacaofornecedor){
		return cotacaoDAO.carregaCotacaoWithSolicitacaocompra(cotacaofornecedor);
	}
	/**
	 * M�todo que monta uma matriz contendo fornecedores com os respectivos valores dos materiais.
	 * Como sa�da gera um arquivo com extens�o .CSV 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#carregaCotacao(Cotacao)
	 * @param cotacao
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Resource preparaArquivoCotacaoCSV(Cotacao cotacao) {
		cotacao = cotacaoDAO.carregaCotacao(cotacao);
		
		if(cotacao.getListaCotacaofornecedor() == null || cotacao.getListaCotacaofornecedor().size() == 0){
			throw new SinedException("Lista de fornecedores n�o pode ser nula.");
		}
		
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) 
			cotacaofornecedor.setListaCotacaofornecedoritem(cotacaofornecedoritemService.findByCotacaofornecedor(cotacaofornecedor));
		
		StringBuffer csv = new StringBuffer(";");
		List<Material> listaMateriaisSemRepeticao = new ArrayList<Material>();
		HashMap<Fornecedor, Double> total = new HashMap<Fornecedor, Double>();
		
		//Coloca o nome dos fornecedores na 1� linha do texto, cria uma lista com todos materiais sem repeti��o
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			csv.append(cotacaofornecedor.getFornecedor().getNome()+";");
			
			if(!total.containsKey(cotacaofornecedor.getFornecedor())){
				total.put(cotacaofornecedor.getFornecedor(), 0.0);
			}
			
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
				if(!listaMateriaisSemRepeticao.contains(cotacaofornecedoritem.getMaterial())){
					listaMateriaisSemRepeticao.add(cotacaofornecedoritem.getMaterial());
				}
			}
		}
		csv.append("\n");

		/*Itera sobre a lista de materiais colocando os valores correspondentes dos fornecedores 
		 * e calculando o total de cada fornecedor
		*/
		for (Material material : listaMateriaisSemRepeticao) {
			csv.append(material.getAutocompleteDescription()+";");
			
			for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
				boolean contemMaterial = false;
				
				for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
					if(material.equals(cotacaofornecedoritem.getMaterial())){
						contemMaterial = true;
						csv.append(cotacaofornecedoritem.getValor()+";");
						
						total.put(cotacaofornecedor.getFornecedor(), ((Double)total.get(cotacaofornecedor.getFornecedor()) + cotacaofornecedoritem.getValor()));
						break;
					}
				}
				
				if(!contemMaterial){
					csv.append(";");
				}
			}
			csv.append("\n");
		}
		
		//Adiciona o totalizador
		csv.append("Total;");
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			csv.append(total.get(cotacaofornecedor.getFornecedor())+";");
		}
		
		Resource resource = new Resource("text/csv", "exportarcotacao_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	/**
	 * M�todo verifica se o campo qtde. cotada dos itens selecionados � vazio ou possui valor igual a 0.
	 *  
	 * @param listaCotacaofornecedor
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean validaQtdCotada(List<Cotacaofornecedor> listaCotacaofornecedor) {
		boolean isQtdCotadaVazio = false;
		for (Cotacaofornecedor cotacaofornecedor : listaCotacaofornecedor) {
			if(cotacaofornecedor.getListaCotacaofornecedoritem() != null){
				for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
					if(cotacaofornecedoritem.getOrdemcompragerada() == null || !cotacaofornecedoritem.getOrdemcompragerada()){
						if(cotacaofornecedoritem.getChecado() && (cotacaofornecedoritem.getQtdecot() == null || cotacaofornecedoritem.getQtdecot() <= 0)){
							isQtdCotadaVazio = true;
							break;
						}
						if(cotacaofornecedoritem.getChecado() && (cotacaofornecedoritem.getValor() == null || cotacaofornecedoritem.getValor() <= 0)){
							isQtdCotadaVazio = true;
							break;
						}
					}
				}
			}
			if(isQtdCotadaVazio)
				break;
		}
		
		return isQtdCotadaVazio;
	}
	
	/**
	* M�todo que valida se o pre�o da cota��o � maior que o pre�o m�ximo de compra, 
	* caso o par�metro COTACAOMESESPRECOMAXIMO esteja preenchido e o usu�rio n�o tenha permiss�o de gerar com o valor acima do permitido
	*
	* @param listaCotacaofornecedor
	* @return
	* @since 16/03/2016
	* @author Luiz Fernando
	 * @param request 
	*/
	public boolean validaPrecoMaximoCompra(WebRequestContext request, List<Cotacaofornecedor> listaCotacaofornecedor) {
		String COTACAOMESESPRECOMAXIMO = parametrogeralService.getValorPorNome(Parametrogeral.COTACAOMESESPRECOMAXIMO);
		if(org.apache.commons.lang.StringUtils.isNotBlank(COTACAOMESESPRECOMAXIMO) && !SinedUtil.isUserHasAction(Acao.GERAR_ORDEMCOMPRA_ACIMA_PREXOMAXIMO)){
			Integer qtdeMeses = null;
			try {
				qtdeMeses = Integer.parseInt(COTACAOMESESPRECOMAXIMO);
			} catch (Exception e) {}
			
			StringBuilder materiais = new StringBuilder();
			for (Cotacaofornecedor cotacaofornecedor : listaCotacaofornecedor) {
				if(cotacaofornecedor.getListaCotacaofornecedoritem() != null){
					for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
						if(cotacaofornecedoritem.getMaterial() != null && cotacaofornecedoritem.getMaterial().getCdmaterial() != null &&
								(cotacaofornecedoritem.getOrdemcompragerada() == null || !cotacaofornecedoritem.getOrdemcompragerada())){
							if(cotacaofornecedoritem.getChecado() && cotacaofornecedoritem.getValorunitario() != null){
								Double precomaximocompra = null;
								if("ORDEM_COMPRA".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.COTACAOPRECOMAXIMO))){
									precomaximocompra = ordemcompramaterialService.getPrecoMaximoCompra(cotacaofornecedoritem.getMaterial().getCdmaterial().toString(), null, qtdeMeses);
								}else {
									precomaximocompra = entregamaterialService.getPrecoMaximoCompra(cotacaofornecedoritem.getMaterial().getCdmaterial().toString(), null, qtdeMeses);
								}
								if(precomaximocompra != null && precomaximocompra < cotacaofornecedoritem.getValorunitario()){
									materiais.append("<br>&nbsp;&nbsp;&nbsp;" + cotacaofornecedoritem.getMaterial().getNome() + " - Pre�o m�ximo: " + precomaximocompra);
								}
							}
						}
					}
				}
			}
			if(StringUtils.isNotBlank(materiais.toString())){
				request.addError("A ordem de compra n�o pode ser gerada porque a cota��o possui materiais com valor acima do Pre�o m�ximo de compra." + materiais);
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao getCotacaoDaOrdemCompraParaRateio(Ordemcompra ordemcompra) {
		return cotacaoDAO.getCotacaoDaOrdemCompraParaRateio(ordemcompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#getCotacaoDaOrdemCompraParaRateio(Cotacao cotacao)
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Cotacao getCotacaoDaOrdemCompraParaRateio(Cotacao cotacao) {
		return cotacaoDAO.getCotacaoDaOrdemCompraParaRateio(cotacao);
	}
	
	/**
	 * M�todo que salva as origens e atualiza todos os itens da cota��o
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoorigemService#saveOrUpdateNoUseTransaction(Cotacaoorigem)
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedoritemService#saveOrUpdateNoUseTransaction(Cotacaofornecedoritem)
	 * @param origens
	 * @param itens
	 * @author Tom�s Rabelo
	 */
	public void saveSolicitacaoNaCotacao(final List<Cotacaoorigem> origens, final List<Cotacaofornecedoritem> itens) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				for (Cotacaoorigem cotacaoorigem : origens) 
					cotacaoorigemService.saveOrUpdateNoUseTransaction(cotacaoorigem);
				
				for (Cotacaofornecedoritem cotacaofornecedoritem : itens) 
					cotacaofornecedoritemService.saveOrUpdateNoUseTransaction(cotacaofornecedoritem);

				return null;
			}
		});
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#callProcedureAtualizaCotacao
	 * @param cotacao
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaCotacao(Cotacao cotacao){
		cotacaoDAO.callProcedureAtualizaCotacao(cotacao);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	private void callProcedureAtualizaCotacao(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Cotacao cotacao;
			for (int i = 0; i < ids.length; i++) {
				cotacao = new Cotacao(Integer.parseInt(ids[i].trim()));
				this.callProcedureAtualizaCotacao(cotacao);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param where
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao findCotacaoParaEnviarEmail(String where) {
		return cotacaoDAO.findCotacaoParaEnviarEmail(where);
	}
	
	/**
	 * Valida cota��o para envio de e-mail
	 * 
	 * @param cotacao
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean validaEnviarEmail(Cotacao cotacao, BindException errors) {
		if(!cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) &&
		   !cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.PEDIDO_ENVIADO) &&
		   !cotacao.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)){
			errors.reject("001", "Situa��o diferente de em aberto, pedido enviado e em cota��o.");
			return false;
		}
		return true;
	}
	
	/**
	 * Pega resources do Report para enviar no email.
	 * 
	 * @param where
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public Resource emitirCotacaoFornecedorParaEmail(String where) throws Exception {
		MergeReport mergeReport = new MergeReport("cotacao_"+SinedUtil.datePatternForReport()+".pdf");
		mergeReport.addReport((Report)emitirCotacaoFornecedor(new CotacaoFiltro(), where));
		return mergeReport.generateResource();
	}
	
	/**
	 * Envia emails e atualiza as cotacoes fornecedores
	 * 
	 * @see #emitirCotacaoFornecedorParaEmail(String)
	 * @see br.com.linkcom.sined.geral.service.CotacaofornecedorService#doUpdateConviteEnviado(String)
	 * @see #callProcedureAtualizaCotacao(Cotacao)
	 * @param cotacao
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public void enviaConviteParaEmail(Cotacao cotacao) throws Exception {
		StringBuilder cdCotacaofornecedores = new StringBuilder();
		
		Envioemail envioemail = envioemailService.registrarEnvio(
				cotacao.getRemetente(),
				cotacao.getAssunto(),
				cotacao.getEmail(),
				Envioemailtipo.CONVITE_COTACAO);
		String emailusuariocopia = null;
		if(cotacao.getEnviaCopia() != null && cotacao.getEnviaCopia()){
			Usuario usuario = usuarioService.loadWithEmail(SinedUtil.getUsuarioLogado());
			if(usuario != null && usuario.getEmail() != null && !"".equals(usuario.getEmail())){
				emailusuariocopia = usuario.getEmail();
			}
		}
		
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()){
			try{
				if(cotacaofornecedor.getChecado()){
					if(cotacaofornecedor.getConviteenviado() == null || !cotacaofornecedor.getConviteenviado())
						cdCotacaofornecedores.append(cotacaofornecedor.getCdcotacaofornecedor()+",");
					
					EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
										.setFrom(cotacao.getRemetente())
										.setTo(cotacaofornecedor.getContato().getEmailcontato())
										.setSubject(cotacao.getAssunto())
										.addHtmlText(cotacao.getEmail() + EmailUtil.getHtmlConfirmacaoEmail(envioemail, cotacaofornecedor.getContato().getEmailcontato()));
			
					if(cotacaofornecedor.getEnviaranexonoemail() != null && cotacaofornecedor.getEnviaranexonoemail()){
						Resource report = emitirCotacaoFornecedorParaEmail(cotacaofornecedor.getCdcotacaofornecedor().toString());
						email.attachFileUsingByteArray(report.getContents(), "cotacao"+cotacaofornecedor.getCdcotacaofornecedor().toString()+"_"+SinedUtil.datePatternForReport()+".pdf", "application/pdf", "1");
						
						Resource excel = cotacaofornecedorService.preparaArquivoCotacaoExcelFormatado(cotacaofornecedor);
						email.attachFileUsingByteArray(excel.getContents(), "cotacao"+cotacaofornecedor.getCdcotacaofornecedor().toString()+"_"+SinedUtil.datePatternForReport()+".xls", "application/msexcel", "2");
					}
					
					if(cotacao.getListaArquivos() != null && !cotacao.getListaArquivos().isEmpty()){
						Integer contentId = 1;
						for (Arquivo arquivo : cotacao.getListaArquivos()) 
							if(arquivo.getArquivo().getContent() != null && arquivo.getArquivo().getContent().length > 0){
								email.attachFileUsingByteArray(arquivo.getArquivo().getContent(), arquivo.getArquivo().getNome(), arquivo.getArquivo().getTipoconteudo(), contentId.toString());
								contentId++;
							}
					}
				
					Envioemailitem envioemailitem = new Envioemailitem();
					envioemailitem.setEnvioemail(envioemail);
					envioemailitem.setEmail(cotacaofornecedor.getContato().getEmailcontato());
					envioemailitem.setNomecontato(cotacaofornecedor.getContato().getNome());
					envioemailitem.setPessoa(cotacaofornecedor.getFornecedor());
					envioemailitemService.saveOrUpdate(envioemailitem);
					
					if(emailusuariocopia != null && !"".equals(emailusuariocopia)){
						email.setCc(emailusuariocopia);
						emailusuariocopia = null;
					}
					email.setEmailItemId(envioemailitem.getCdenvioemailitem());
					email.sendMessage();
				}else{
					if(cotacaofornecedor.getConviteenviado() != null && cotacaofornecedor.getConviteenviado()){
						cdCotacaofornecedores.append(cotacaofornecedor.getCdcotacaofornecedor()+",");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				NeoWeb.getRequestContext().addMessage("E-mail para o " + cotacaofornecedor.getContato().getEmailcontato() + " n�o foi enviado.", MessageType.WARN);
			}
		}
		
		if(cdCotacaofornecedores != null && cdCotacaofornecedores.length() > 0){
			String whereIn = cdCotacaofornecedores.delete(cdCotacaofornecedores.length()-1, cdCotacaofornecedores.length()).toString();
			cotacaofornecedorService.doUpdateConviteEnviado(whereIn);
			callProcedureAtualizaCotacao(cotacao);
		}
		
	}
	
	/**
	 * M�todo que inseri uma solicita��o em uma cota��o da seguinte maneira ou adiciona um novo item ao cota��o fornecedor 
	 * ou apenas atualiza os valores
	 * 
	 * @see #verificaMatLocalSoma(List, Boolean, Solicitacaocompra)
	 * @param solicitacoes
	 * @param solicitacaocompra
	 * @param cotacao 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaofornecedoritem> preparaSolicitacoesParaInsercaoCotacao(List<Solicitacaocompra> solicitacoes, Solicitacaocompra solicitacaocompra, Cotacao cotacao) {
		List<Cotacaofornecedoritem> itens = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			for (Solicitacaocompra solicitacaocompra3 : solicitacoes) {
				boolean find = false;
				if(!verificaMatLocalSoma(cotacaofornecedor.getListaCotacaofornecedoritem(), find, solicitacaocompra3, solicitacaocompra.getLocalunicoentrega(), true)){
					Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
					Cotacaofornecedoritemsolicitacaocompra cotacaofornecedoritemsolicitacaocompra = new Cotacaofornecedoritemsolicitacaocompra();
					cotacaofornecedoritemsolicitacaocompra.setSolicitacaocompra(solicitacaocompra3);
					listaCotacaofornecedoritemsolicitacaocompra.add(cotacaofornecedoritemsolicitacaocompra);
					
					
					Cotacaofornecedoritem cotacaofornecedoritem = new Cotacaofornecedoritem(cotacaofornecedor, solicitacaocompra3.getQtde(), 
									(solicitacaocompra.getLocalunicoentrega() != null ? solicitacaocompra.getLocalunicoentrega() : solicitacaocompra3.getLocalarmazenagem()),
									solicitacaocompra3.getMaterial(), solicitacaocompra3.getMaterialclasse(),
									solicitacaocompra3.getLocalarmazenagem(), solicitacaocompra3.getFrequencia(), solicitacaocompra3.getQtdefrequencia(), listaCotacaofornecedoritemsolicitacaocompra);
					cotacaofornecedoritem.setUnidademedida(solicitacaocompra3.getUnidademedida());
					cotacaofornecedor.getListaCotacaofornecedoritem().add(cotacaofornecedoritem);
				} 
			}
			itens.addAll(cotacaofornecedor.getListaCotacaofornecedoritem());
		}
		return itens;
	}
	
	/**
	 * M�todo que inseri nas origens das cota��es as "novas" solicita��es que est�o sendo inseridas na cota��o
	 * 
	 * @param solicitacoes
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacaoorigem> preparaOrigensDaInsercaoCotacao(List<Solicitacaocompra> solicitacoes, Cotacao cotacao) {
		List<Cotacaoorigem> origens = new ListSet<Cotacaoorigem>(Cotacaoorigem.class);
		for (Solicitacaocompra solicitacaocompraaux : solicitacoes) {
			Cotacaoorigem cotacaoorigem = new Cotacaoorigem();
			cotacaoorigem.setCotacao(cotacao);
			cotacaoorigem.setSolicitacaocompra(solicitacaocompraaux);
			origens.add(cotacaoorigem);
		}
		return origens;
	}
	
	/**
	 * M�todo que gera arquivo no formato do excel formatado
	 * 
	 * @param cotacao
	 * @return
	 * @throws IOException
	 * @author Tom�s Rabelo
	 */
	public Resource preparaArquivoCotacaoExcelFormatado(Cotacao cotacao) throws IOException{
		cotacao = cotacaoDAO.carregaCotacao(cotacao);
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) 
			cotacaofornecedor.setListaCotacaofornecedoritem(cotacaofornecedoritemService.findByCotacaofornecedor(cotacaofornecedor));
		
		SinedExcel wb = new SinedExcel();

		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 35);
        
        wb.addSinedHeader("Planilha", "Exporta��o Cota��o");
 
		if(cotacao.getListaCotacaofornecedor() == null || cotacao.getListaCotacaofornecedor().size() == 0){
			throw new SinedException("Lista de fornecedores n�o pode ser nula.");
		}
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		for (int i = -1; i < cotacao.getListaCotacaofornecedor().size(); i++) {
			String cellValue = null;
			if(i > -1){
				Cotacaofornecedor cotacaofornecedor = cotacao.getListaCotacaofornecedor().get(i);
				row = planilha.getRow(4);
				cellValue = cotacaofornecedor.getFornecedor().getNome();
				
				cell = row.createCell((short) (i+1));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(cellValue);
				
				for (int j = 0; j < cotacaofornecedor.getListaCotacaofornecedoritem().size(); j++) {
					Cotacaofornecedoritem cotacaofornecedoritem = cotacaofornecedor.getListaCotacaofornecedoritem().get(j);
					
					row = planilha.getRow(4 +j+1);
					if(row != null){
						cell = row.createCell((short) (row.getLastCellNum()+1));
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
						cell.setCellValue(cotacaofornecedoritem.getValor() != null ? cotacaofornecedoritem.getValor() : 0.0);
					} else{
						row = planilha.createRow(planilha.getLastRowNum() + 1);
						cell = row.createCell((short) (0));
						cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
						cell.setCellValue(cotacaofornecedoritem.getMaterial().getAutocompleteDescription());
						j--;
					}
				}
				
				//Cria totaliza��o
				if(planilha.getLastRowNum() == cotacaofornecedor.getListaCotacaofornecedoritem().size()+4){
					row = planilha.createRow(planilha.getLastRowNum() +1);
					cell = row.createCell((short) 0);
					cell.setCellStyle(SinedExcel.STYLE_TOTAL_LEFT_WITHOUT_BORDERRIGHT);
					cell.setCellValue("Total");
					
					cell = row.createCell((short) 1);
					cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
					cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(1)+6+":"+SinedExcel.getAlgarismoColuna(1)+(planilha.getLastRowNum())+")");
				} else{
					row = planilha.getRow(planilha.getLastRowNum());
					cell = row.createCell((short) (row.getLastCellNum()+1));
					cell.setCellStyle(SinedExcel.STYLE_TOTAL_MONEY);
					cell.setCellFormula("SUM("+SinedExcel.getAlgarismoColuna(row.getLastCellNum())+6+":"+SinedExcel.getAlgarismoColuna(row.getLastCellNum())+(planilha.getLastRowNum())+")");
				}
			}				
			else{
				row = planilha.createRow(4);
				cell = row.createCell((short) (i+1));
				cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
				cell.setCellValue(cellValue);
			}
		}
		
		return wb.getWorkBookResource("exportarcotacao_" + SinedUtil.datePatternForReport() +  ".xls");
	}
	
	/**
	 * Prepara envio de email cota��o
	 * 
	 * @param cotacao
	 * @throws IOException 
	 */
	public void preparaEmail(Cotacao cotacao) throws IOException {
		Usuario usuario = (Usuario)Neo.getUser();
		//Empresa empresa = empresaService.loadPrincipal();
		String nomeEmpresa = "";
		
		if (cotacao.getListaOrigem()!=null){
			for (Cotacaoorigem cotacaoorigem: cotacao.getListaOrigem()){
				if (cotacaoorigem.getSolicitacaocompra()!=null && cotacaoorigem.getSolicitacaocompra().getEmpresa()!=null){
					nomeEmpresa = cotacaoorigem.getSolicitacaocompra().getEmpresa().getRazaosocialOuNome();
					break;
				}
			}
		}
		
		String emailUsuario = SinedUtil.getUsuarioLogado().getEmail();
		
		cotacao.setAssunto("CARTA CONVITE "+cotacao.getCdcotacao()+" - "+nomeEmpresa);
		
		TemplateManager template = new TemplateManager("/WEB-INF/template/templateEnvioPedidoCotacaoEmail.tpl");
		template
			.assign("cdcotacao", cotacao.getCdcotacao().toString())
			.assign("nomeUsuario", usuario.getNome())
			.assign("emailUsuario", emailUsuario != null ? emailUsuario : "")
			.assign("nomeEmpresa", nomeEmpresa);
		
		String templateEmail = parametrogeralService.getValorPorNome(Parametrogeral.DESCRICAO_PEDIDO_COTACAO_EMAIL);
		if(templateEmail != null && !templateEmail.equals("")){
			template.setTexto(templateEmail);
			cotacao.setEmail(template.getTemplateWithoutRead());
		} else {
			cotacao.setEmail(template.getTemplate());
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao loadCotacaoParaAjustarPedidoSolicitacaoCompra(Ordemcompra ordemcompra) {
		return cotacaoDAO.loadCotacaoParaAjustarPedidoSolicitacaoCompra(ordemcompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereInOrdemCompra
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Cotacao> loadCotacoesParaAjustarPedidoSolicitacaoCompra(String whereInOrdemCompra) {
		return cotacaoDAO.loadCotacoesParaAjustarPedidoSolicitacaoCompra(whereInOrdemCompra);
	}

	/**
	 * M�todo chamado via FLEX para escolha de fornecedores e produtos da cota��o
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 
	public CotacaoAux getCotacaoFlex(){
		String cdcotacao = (String) NeoWeb.getRequestContext().getSession().getAttribute("cdcotacao");
		Cotacao cotacao = null;
		if(cdcotacao != null && !cdcotacao.equals("")){
			 NeoWeb.getRequestContext().getSession().removeAttribute("cdcotacao");
			 cotacao = loadForEntrada(new Cotacao(Integer.parseInt(cdcotacao)));
		}
		return prepareCotacao(cotacao);
	}*/

	/**
	 * M�todo chamado via FLEX para cria��o do mapa de cota��o
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public CotacaoMapaAux getMapaCotacaoFlex(){
		String cdcotacao = (String) NeoWeb.getRequestContext().getSession().getAttribute("cdcotacao");
		Cotacao cotacao = null;
		if(cdcotacao != null && !cdcotacao.equals("")){
			NeoWeb.getRequestContext().getSession().removeAttribute("cdcotacao");
			cotacao = carregaCotacao(new Cotacao(Integer.parseInt(cdcotacao)));
			for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) 
				cotacaofornecedor.setListaCotacaofornecedoritem(cotacaofornecedoritemService.findByCotacaofornecedor(cotacaofornecedor));
//			cotacao = cotacaoDAO.loadForEntrada(new Cotacao(Integer.parseInt(cdcotacao)));
		}
		return prepareMapaCotacao(cotacao);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Cotacao carregaCotacao(Cotacao cotacao) {
		return cotacaoDAO.carregaCotacao(cotacao);
	}
	
	/**
	 * M�todo que prepara Cota��o para edi��o
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s rabelo
	 */
	private CotacaoMapaAux prepareMapaCotacao(Cotacao cotacao) {
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
			List<Vwultimovalorcotacao> lista = vwultimovalorcotacaoService.findUltimosValores(cotacaofornecedor.getFornecedor(), CollectionsUtil.listAndConcatenate(cotacaofornecedor.getListaCotacaofornecedoritem(), "material.cdmaterial",","));  
			if(lista != null && !lista.isEmpty()){
				for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()){
					for (Vwultimovalorcotacao vwultimovalorcotacao : lista) {
						if(cotacaofornecedoritem.getMaterial().equals(vwultimovalorcotacao.getMaterial())){
							cotacaofornecedoritem.setUltvalor(vwultimovalorcotacao.getValor());
							cotacaofornecedoritem.setTipo(vwultimovalorcotacao.getTipo());
							break;
						}
					}
				}
			}
		}
		
		List<Cotacaofornecedoritem> listaProdutos = new ArrayList<Cotacaofornecedoritem>();
		for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()){ 
			for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem())
				listaProdutos.add(cotacaofornecedoritem);
			break;
		}
		
		CotacaoMapaAux cotacaoMapaAux = new CotacaoMapaAux();
		if(cotacao.getAux_cotacao() != null && cotacao.getAux_cotacao().getCdcotacao() != null)
			cotacaoMapaAux.setSituacao(cotacao.getAux_cotacao().getSituacaosuprimentos().getDescricao());
		
		cotacaoMapaAux.setListaFornecedores(cotacao.getListaCotacaofornecedor());
		cotacaoMapaAux.setCdcotacao(cotacao.getCdcotacao());
		cotacaoMapaAux.setListaProdutos(listaProdutos);
		return cotacaoMapaAux;
	}

	/**
	 * M�todo que prepara Cota��o 1� tela
	 * 
	 * @param cotacao
	 * @return
	 * @author Tom�s Rabelo
	 
	private CotacaoAux prepareCotacao(Cotacao cotacao) {
		StringBuilder whereInMaterial = new StringBuilder("");
		StringBuilder whereInFornecedor = new StringBuilder("");
		List<Produto> listaMateriaisSelected = new ArrayList<Produto>();
		List<Fornecedor> listaFornecedoresSelected = new ArrayList<Fornecedor>();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CotacaoAux mapacotacao = new CotacaoAux();
		
		if(cotacao != null && cotacao.getCdcotacao() != null){
			mapacotacao.setCdcotacao(cotacao.getCdcotacao());
//			mapacotacao.setSituacao(cotacao.getAux_cotacao().getSituacaosuprimentos().getDescricao());
			if(cotacao.getAux_cotacao() != null && cotacao.getAux_cotacao().getCdcotacao() != null)
				mapacotacao.setSituacao(cotacao.getAux_cotacao().getSituacaosuprimentos().getDescricao());
			if(cotacao.getListaCotacaofornecedor() != null && !cotacao.getListaCotacaofornecedor().isEmpty()){
				for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
					mapacotacao.setDtvalidade(dateFormat.format(cotacaofornecedor.getDtvalidade()));
					whereInFornecedor.append(cotacaofornecedor.getFornecedor().getCdpessoa()).append(",");
					listaFornecedoresSelected.add(new Fornecedor(cotacaofornecedor.getFornecedor().getCdpessoa(), cotacaofornecedor.getFornecedor().getNome()));
					if(cotacaofornecedor.getListaCotacaofornecedoritem() != null && !cotacaofornecedor.getListaCotacaofornecedoritem().isEmpty() && listaMateriaisSelected.isEmpty()){
						for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
							listaMateriaisSelected.add(new Produto(cotacaofornecedoritem.getMaterial().getCdmaterial(), cotacaofornecedoritem.getMaterial().getNome(), cotacaofornecedoritem.getQtdesol()));
							whereInMaterial.append(cotacaofornecedoritem.getMaterial().getCdmaterial()).append(",");
						}
					}
				}
			}
		}
		
		mapacotacao.setListaProdutosSelected(listaMateriaisSelected);
		mapacotacao.setListaFornecedoresSelected(listaFornecedoresSelected);
//		mapacotacao.setListaProdutosUnselected(ProdutoService.getInstance().findMateriasWhereNotIn(whereInMaterial.toString().equals("") ? "" : whereInMaterial.substring(0, whereInMaterial.length()-1).toString()));
		mapacotacao.setListaFornecedoresUnselected(fornecedorService.findFornecedoresWhereNotIn(whereInFornecedor.toString().equals("") ? "" : whereInFornecedor.substring(0, whereInFornecedor.length()-1).toString()));
		
		return mapacotacao;
	}*/

	/**
	 * M�todo chamado pelo save da 1� tela de cota��o, prepara e salva
	 * 
	 * @param mapacotacao
	 * @author Tom�s Rabelo
	
	public CotacaoAux prepareToSaveCotacao(CotacaoAux mapacotacao){
		Cotacao cotacao = new Cotacao(mapacotacao.getCdcotacao() == 0 ? null : mapacotacao.getCdcotacao());
		List<Cotacaofornecedor> listaCotacaoFonecedor = new ArrayList<Cotacaofornecedor>();

		List<Cotacaofornecedor> listaCotacaoFonecedorExclusao = new ArrayList<Cotacaofornecedor>();
		List<Cotacaofornecedoritem> listaCotacaoFonecedorItemExclusao = new ArrayList<Cotacaofornecedoritem>();
		
		if(cotacao.getCdcotacao() != null){
			cotacao = loadForEntrada(cotacao);
		}
		for (Fornecedor fornecedor : mapacotacao.getListaFornecedoresSelected()) {
			Cotacaofornecedor cotacaofornecedorAux = null;
			
			if(cotacao.getListaCotacaofornecedor() != null && !cotacao.getListaCotacaofornecedor().isEmpty()){
				for (Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()) {
					List<Integer> produtosExistentes = new ArrayList<Integer>();

					if(!mapacotacao.getListaFornecedoresSelected().contains(cotacaofornecedor.getFornecedor()) && !listaCotacaoFonecedorExclusao.contains(cotacaofornecedor))
						listaCotacaoFonecedorExclusao.add(cotacaofornecedor);
					
					if(cotacaofornecedor.getFornecedor().equals(fornecedor)){
						cotacaofornecedorAux = cotacaofornecedor;
						for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()) {
							if(!mapacotacao.getListaProdutosSelected().contains(cotacaofornecedoritem.getMaterial()) && !listaCotacaoFonecedorItemExclusao.contains(cotacaofornecedoritem))
								listaCotacaoFonecedorItemExclusao.add(cotacaofornecedoritem);
							
							for (Produto produto : mapacotacao.getListaProdutosSelected()){
								if(cotacaofornecedoritem.getMaterial().equals(produto)){
									produtosExistentes.add(produto.getCdmaterial());
									cotacaofornecedoritem.setQtdesol(produto.getQuantidade());
								}
							}
						}
						
						for (Produto produto : mapacotacao.getListaProdutosSelected()){
							if (!produtosExistentes.contains(produto.getCdmaterial())){
								Cotacaofornecedoritem cotacaofornecedoritem = new Cotacaofornecedoritem();
								cotacaofornecedoritem.setMaterial((Material)produto);
								cotacaofornecedoritem.setQtdesol(produto.getQuantidade());
								cotacaofornecedoritem.setQtdecot(0.0);
								cotacaofornecedor.getListaCotacaofornecedoritem().add(cotacaofornecedoritem);
							}
						}
						
					}
				}
			}
			
			if(cotacaofornecedorAux == null){
				cotacaofornecedorAux = new Cotacaofornecedor(fornecedor, SinedDateUtils.stringToSqlDate(mapacotacao.getDtvalidade()));
			
				List<Cotacaofornecedoritem> listaCotacaoFonecedorItem = new ArrayList<Cotacaofornecedoritem>();
				for (Produto produtoAux : mapacotacao.getListaProdutosSelected()) 
					listaCotacaoFonecedorItem.add(new Cotacaofornecedoritem(new Material(produtoAux.getCdmaterial()), produtoAux.getQuantidade(), produtoAux.getQuantidade()));
				cotacaofornecedorAux.setListaCotacaofornecedoritem(listaCotacaoFonecedorItem);
			}
			listaCotacaoFonecedor.add(cotacaofornecedorAux);
		}
		
		if(listaCotacaoFonecedorExclusao != null && !listaCotacaoFonecedorExclusao.isEmpty())
			for (Cotacaofornecedor cotacaofornecedor : listaCotacaoFonecedorExclusao) 
				cotacao.getListaCotacaofornecedor().remove(cotacaofornecedor);

		if(listaCotacaoFonecedorItemExclusao != null && !listaCotacaoFonecedorItemExclusao.isEmpty())
			for (Cotacaofornecedoritem cotacaofornecedoritem : listaCotacaoFonecedorItemExclusao) 
				for (Cotacaofornecedor cotacaofornecedorAux : cotacao.getListaCotacaofornecedor()) 
					cotacaofornecedorAux.getListaCotacaofornecedoritem().remove(cotacaofornecedoritem);
		
		saveCotacao(cotacao, listaCotacaoFonecedor, listaCotacaoFonecedorExclusao, listaCotacaoFonecedorItemExclusao);
//		NeoWeb.getRequestContext().addMessage("Registro salvo com sucesso.", MessageType.INFO);
//		return new ModelAndView("redirect:/cotacao/crud/Cotacao");
		return new CotacaoAux(cotacao.getCdcotacao());
	} */
	
	/**
	 * M�todo chamado via FLEX para salvar o mapa da cota��o e depois gerar ordem de compra
	 * 
	 * @param mapacotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public CotacaoMapaAux prepareToSaveCotacaoMapaAndCreateOrdemCompra(CotacaoMapaAux mapacotacao){
		return prepareToSaveCotacaoMapa(mapacotacao);
	}
	
	/**
	 * M�todo chamado via FLEX para salvar o mapa da cota��o
	 * 
	 * @param mapacotacao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public CotacaoMapaAux prepareToSaveCotacaoMapa(final CotacaoMapaAux mapacotacao){
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Cotacaofornecedor cotacaofornecedor : mapacotacao.getListaFornecedores()){
					for (Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()){
						cotacaofornecedoritem.setValor(cotacaofornecedoritem.getCorvalor().getValor().getValue().doubleValue());
						cotacaofornecedoritem.setCotacaofornecedor(cotacaofornecedor);
						cotacaofornecedoritemService.saveOrUpdate(cotacaofornecedoritem);
					}
					cotacaofornecedorService.updatePrazoPagamento(cotacaofornecedor);
				}
				return status;
			}
		});
		return mapacotacao;
	}
	
	/**
	 * Salva a cota��o e verifica se sua situa��o est� 'em cota��o', se estiver � redirecionado
	 * para o crud ordem de compra que gera novas ordens.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CotacaoService#validaQtdCotada(List)
	 * @param request
	 * @param bean
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 
	public String gerarOrdemCompra(CotacaoMapaAux mapa) throws Exception {
		Cotacao cotacaoAux = new Cotacao();
		cotacaoAux.setCdcotacao(mapa.getCdcotacao());
		
		prepareToSaveCotacaoMapa(mapa);
		cotacaoAux = loadForEntrada(cotacaoAux);
		
		String msgErro = "";
		if(!cotacaoAux.getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_COTACAO)){
			msgErro = "Registro(s) com situa��o diferente de 'em cota��o'.";
		}else{
//			return ordemcompraService.geraOrdemCompra(cotacaoAux);
			return null;
		}
		
		return msgErro;
	}*/
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#findForCsv
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Cotacao> findForCsv(CotacaoFiltro filtro) {
		return cotacaoDAO.findForCsv(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param ordemcompra
	 * @return
	 * @authro Tom�s Rabelo
	 */
	public Cotacao findByOrdemcompra(Ordemcompra ordemcompra) {
		return cotacaoDAO.findByOrdemcompra(ordemcompra);
	}
	
	public void updateMapaCotacao(Cotacao cotacao){
		cotacaoDAO.updateMapaCotacao(cotacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#loadWithSituacao(Cotacao cotacao)
	 *
	 * @param cotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Cotacao loadWithSituacao(Cotacao cotacao){
		return cotacaoDAO.loadWithSituacao(cotacao);
	}
	
	/**
	 * Cria mapa com as entregas de menor valor
	 *
	 * @param mapaMenorValorMaterial
	 * @param listaCotacao
	 * @author Luiz Fernando
	 */
	public void addMapaMenorValor(HashMap<Integer, Entregamaterial> mapaMenorValorMaterial, List<Cotacao> listaCotacao){
		Entregamaterial entregamaterial;
		for(Cotacao cotacao : listaCotacao){
			for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
				for(Cotacaofornecedoritem item : cf.getListaCotacaofornecedoritem()){
					entregamaterial = mapaMenorValorMaterial.get(item.getMaterial().getCdmaterial());
					if(entregamaterial == null){
						entregamaterial = entregamaterialService.getUltimascomprasMelhorValorByMaterial(item.getMaterial().getCdmaterial(), null, 5);
						if(entregamaterial != null){
							mapaMenorValorMaterial.put(item.getMaterial().getCdmaterial(), entregamaterial);
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que cria excel do mapa de cota��o 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @throws IOException 
	 */
	public Resource gerarExcelMapacotacao(String whereInCotacao) throws IOException {
		List<Cotacao> listaCotacao = this.findForExcel(whereInCotacao);
		List<Material> listaMaterial = this.getlistaMateriais(listaCotacao);
		HashMap<Integer, Entregamaterial> mapaMenorValorMaterial = new HashMap<Integer, Entregamaterial>();
		this.addMapaMenorValor(mapaMenorValorMaterial, listaCotacao);
		Boolean isMapacotacaoprecoantigo = parametrogeralService.getBoolean("MAPACOTACAOPRECOANTIGO");
		Boolean exibirValorVendaCotacao= Boolean.parseBoolean(ParametrogeralService.getInstance().buscaValorPorNome(Parametrogeral.EXIBIRVALORVENDACOTACAO));
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 20);
        planilha.setColumnWidth((short) 0, ((short)(35*180)));
        planilha.setColumnWidth((short) 1, ((short)(35*50)));
        planilha.setColumnWidth((short) 2, ((short)(35*50)));
        
        HSSFRow headerRow = planilha.createRow((short) 0);
        headerRow.setHeight((short) 500);
        
		HSSFCell cellHeaderTitle = headerRow.createCell((short) 0);
		cellHeaderTitle.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
		cellHeaderTitle.setCellValue("Mapa de Cota��o");
		if (exibirValorVendaCotacao){
			planilha.addMergedRegion(new Region(0, (short)0, 0, (short)(11)));
		}
		else{
			planilha.addMergedRegion(new Region(0, (short)0, 0, (short)(10)));
		}
		
		HSSFRow row = null;
		HSSFCell cell = null;
		int linhaCotacao = 1;
		int linhaMaterial = 1;
		Entregamaterial entregamaterial;
		
		for(Cotacao cotacao : listaCotacao){
			cotacao.setListaSolicitacao(solicitacaocompraService.findByCotacao(cotacao));
			String itensSolicitacao = SinedUtil.listAndConcatenate(cotacao.getListaSolicitacao(), "identificador", ", ");
			if(itensSolicitacao.split(",").length > 10) itensSolicitacao = "DIVERSAS";
			String projetos = SinedUtil.listAndConcatenate(cotacao.getListaSolicitacao(), "projeto.nome", "\n ");
			
			row = planilha.createRow(linhaCotacao);
			cell = row.createCell((short) (0));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("Projeto");
			
			cell = row.createCell((short) (1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue(projetos);
			
			for (int i=2; i<7; i++){
				cell = row.createCell((short) (i));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellValue("");
			}
			
			cell = row.createCell((short) (7));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("Cota��o");
			
			cell = row.createCell((short) (8));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue(cotacao.getCdcotacao().toString());
			
			cell = row.createCell((short) (9));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("");
			
			cell = row.createCell((short) (10));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("");
			
			if (exibirValorVendaCotacao){
				cell = row.createCell((short) (11));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellValue("");
			}
			
			planilha.addMergedRegion(new Region(linhaCotacao, (short) 1, linhaCotacao, (short)(6)));
			
			linhaCotacao++;
			row = planilha.createRow(linhaCotacao);
			cell = row.createCell((short) (0));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("Solicita��o");
			
			cell = row.createCell((short) (1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue(itensSolicitacao);
			
			for (int i=2; i<7; i++){
				cell = row.createCell((short) (i));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellValue("");
			}
			
			cell = row.createCell((short) (7));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("In�cio");
			
			cell = row.createCell((short) (8));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("");
			
			cell = row.createCell((short) (9));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("Fechamento");	
			planilha.setColumnWidth((short) 9, ((short)(35*100)));
			
			cell = row.createCell((short) (10));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("");
			
			if (exibirValorVendaCotacao){
				cell = row.createCell((short) (11));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				cell.setCellValue("");
			}
			
			planilha.addMergedRegion(new Region(linhaCotacao, (short) 1, linhaCotacao, (short)(6)));
			
			linhaCotacao+= 2;
			
			row = planilha.createRow(linhaCotacao);
			cell = row.createCell((short) (0));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			cell.setCellValue("Fornecedor");
			
			row.createCell((short) (1)).setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			
			int colunafornecedor;
			if (exibirValorVendaCotacao){
				colunafornecedor = 2;
				row.createCell((short) (2)).setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
			}
			else{
				colunafornecedor = 1;
			}
			
			for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue(cf.getFornecedor().getNome() + cf.getFornecedor().getTelefoneprincipal());
				
				for(int col = colunafornecedor+1; col <= colunafornecedor+3; col++)
					row.createCell((short) (col)).setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				
				planilha.addMergedRegion(new Region(linhaCotacao, (short)colunafornecedor, linhaCotacao, (short)(colunafornecedor+3)));
				
				colunafornecedor+= 4;
			}
						
			if(isMapacotacaoprecoantigo){
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Pre�os Antigos");
				
				for(int col = colunafornecedor+1; col <= colunafornecedor+5; col++)
					row.createCell((short) (col)).setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
				
				planilha.addMergedRegion(new Region(linhaCotacao, (short)(colunafornecedor), linhaCotacao, (short)(colunafornecedor+5)));
			}					
			
			linhaCotacao++;
			
			row = planilha.createRow(linhaCotacao);
			cell = row.createCell((short) (0));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
			cell.setCellValue("Material");
			
			cell = row.createCell((short) (1));
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
			cell.setCellValue("Vr. Venda");
			planilha.setColumnWidth((short) 1, ((short)(35*80)));
			
			if (exibirValorVendaCotacao){
				colunafornecedor = 2;
			}
			else{
				colunafornecedor = 1;
			}
			for(int cont = 0; cont < cotacao.getListaCotacaofornecedor().size(); cont++){
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("U.M.");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*100)));
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Qtde");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*100)));
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Pre�o Unit�rio");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*100)));				
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Total");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*100)));
				colunafornecedor++;
			}
			
			if(isMapacotacaoprecoantigo){
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Menor");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*80)));			
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("OC");	
				planilha.setColumnWidth((short) (colunafornecedor), ((short)(35*90)));
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Fornecedor");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*80)));				
				colunafornecedor++;
				
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*300)));
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				planilha.addMergedRegion(new Region(linhaCotacao, (short) (colunafornecedor-1), linhaCotacao, (short)(colunafornecedor)));
				colunafornecedor++;				

				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Data");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*80)));				
				colunafornecedor++;
				
				cell = row.createCell((short) (colunafornecedor));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
				cell.setCellValue("Valor unit�rio da �ltima compra");
				planilha.setColumnWidth((short) colunafornecedor, ((short)(35*250)));				
				colunafornecedor++;
			}			
			
			linhaMaterial = linhaCotacao + 1;
			for(Material material : listaMaterial){
				row = planilha.createRow(linhaMaterial);
				List<Pedidovendamaterial> listaPedidovendamaterial = null;
				if (exibirValorVendaCotacao){
					listaPedidovendamaterial = pedidovendamaterialService.findByCotacao(cotacao);
					colunafornecedor = 2;
				}
				else{
					colunafornecedor = 1;
				}
				for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
					for(Cotacaofornecedoritem item : cf.getListaCotacaofornecedoritem()){
						if(item.getMaterial().equals(material)){
							cell = row.createCell((short) (0));
							cell.setCellStyle(SinedExcel.STYLE_LEFT);
							cell.setCellValue(item.getMaterial().getIdentificacao()+" - "+item.getMaterial().getNome());
							
							if(listaPedidovendamaterial != null){
								cell = row.createCell((short) (1));
								cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
								for(Pedidovendamaterial lista : listaPedidovendamaterial){
									if (lista.getMaterial() != null && lista.getMaterial().getCdmaterial().equals(item.getMaterial().getCdmaterial())){
										if(lista.getPreco() != null){
											cell.setCellValue(lista.getPreco());
										}	
									}
								}
							}
							
							cell = row.createCell((short) (colunafornecedor));
							cell.setCellStyle(SinedExcel.STYLE_CENTER);
							if(item.getUnidademedida() != null && item.getUnidademedida().getNome() != null) 
								cell.setCellValue(item.getUnidademedida().getNome());
							colunafornecedor++;
							
							cell = row.createCell((short) (colunafornecedor));
							cell.setCellStyle(SinedExcel.STYLE_CENTER);
							if(item.getQtdecot() != null ) cell.setCellValue(item.getQtdecot());
							
							colunafornecedor++;
							cell = row.createCell((short) (colunafornecedor));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
							if(item.getValorUnitarioSemDesconto() != null ) cell.setCellValue(item.getValorUnitarioSemDesconto());
							else cell.setCellValue(0.0);
							
							colunafornecedor++;
							cell = row.createCell((short) (colunafornecedor));
							cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
							if(item.getValorSemDesconto() != null ) cell.setCellValue(item.getValorSemDesconto());
							else cell.setCellValue(0.0);
							
							colunafornecedor++;
							break;
						}
					}
				}
				
				if(isMapacotacaoprecoantigo){
					entregamaterial = mapaMenorValorMaterial.get(material.getCdmaterial());
					if(entregamaterial != null){
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_CENTER);
						if(entregamaterial != null && entregamaterial.getQtde() != null) 
							cell.setCellValue(entregamaterial.getQtde());
						
						colunafornecedor++;
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_CENTER);
						if(entregamaterial != null && entregamaterial.getIdOrdemcompra() != null) 
							cell.setCellValue(entregamaterial.getIdOrdemcompra());
						
						colunafornecedor++;
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_CENTER);
						if(entregamaterial != null && entregamaterial.getEntregadocumento() != null && 
								entregamaterial.getEntregadocumento().getFornecedor() != null) 
							cell.setCellValue(entregamaterial.getEntregadocumento().getFornecedor().getNome());
						
						colunafornecedor++;
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_CENTER);
						planilha.addMergedRegion(new Region(linhaMaterial, (short) (colunafornecedor-1), linhaMaterial, (short)(colunafornecedor)));
						
						colunafornecedor++;
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_CENTER);
						if(entregamaterial != null && entregamaterial.getEntregadocumento() != null && 
								entregamaterial.getEntregadocumento().getDtentrada() != null){
							cell.setCellValue(format.format(entregamaterial.getEntregadocumento().getDtentrada()));
						}else if(entregamaterial != null && entregamaterial.getEntregadocumento() != null && 
								entregamaterial.getEntregadocumento().getEntrega() != null && 
								entregamaterial.getEntregadocumento().getEntrega().getDtentrega() != null){
							cell.setCellValue(format.format(entregamaterial.getEntregadocumento().getEntrega().getDtentrega()));
						}
		
						colunafornecedor++;
						cell = row.createCell((short) (colunafornecedor));
						cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
						if(entregamaterial != null && material.getCdmaterial() != null && entregamaterial.getEntregadocumento() != null
								&& entregamaterial.getEntregadocumento().getFornecedor() != null ){
							
							Double ultimoValor = ordemcompramaterialService.getUltimoValor(material.getCdmaterial(), entregamaterial.getEntregadocumento().getFornecedor().getCdpessoa());
						if (ultimoValor != null)
							cell.setCellValue(ultimoValor);								
														
						else  
							cell.setCellValue(0.0);
						}
					}
				}
				linhaMaterial++;
			}
			
			linhaCotacao = linhaMaterial + 1;
			
			int colunaFixa = 0;
			int linhaAtual = 0;
			
			linhaAtual = linhaMaterial;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("Comprador");

			linhaAtual++;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("Aprovado");

			linhaAtual++;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("Observa��o");
			
			linhaAtual = linhaMaterial;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa+1));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa+2));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			
			if (exibirValorVendaCotacao){
				row = planilha.createRow(linhaAtual);
				cell = row.createCell((short) (colunaFixa+3));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("");
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual, (short)(colunaFixa+2)));
			}
			else {
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual, (short)(colunaFixa+1)));
			}

			linhaAtual++;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa+1));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa+2));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			
			if (exibirValorVendaCotacao){
				row = planilha.createRow(linhaAtual);
				cell = row.createCell((short) (colunaFixa+3));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("");
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual, (short)(colunaFixa+2)));
			}
			else {
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual, (short)(colunaFixa+1)));
			}

			linhaAtual++;
			row = planilha.createRow(linhaAtual);
			cell = row.createCell((short) (colunaFixa+1));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			row = planilha.createRow(linhaAtual+1);
			cell = row.createCell((short) (colunaFixa+1));
			cell.setCellStyle(SinedExcel.STYLE_CENTER);
			cell.setCellValue("");
			
			if (exibirValorVendaCotacao){
				row = planilha.createRow(linhaAtual);
				cell = row.createCell((short) (colunaFixa+2));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("");
				row = planilha.createRow(linhaAtual+1);
				cell = row.createCell((short) (colunaFixa+2));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("");
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual+1, (short)(colunaFixa+2)));
			}
			else {
				planilha.addMergedRegion(new Region(linhaAtual, (short)(colunaFixa+1), linhaAtual+1, (short)(colunaFixa+1)));
			}
			
			int colunatotais;
			int linhatotais = 0;
			
			if (exibirValorVendaCotacao){
				colunatotais = 4;
			}
			else{
				colunatotais = 3;
			}
			
			for(Cotacaofornecedor cf : cotacao.getListaCotacaofornecedor()){
				linhatotais = linhaMaterial;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Desconto");
				
				linhatotais++;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Imposto");
				
				linhatotais++;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Frete");
				
				linhatotais++;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Total");
				
				linhatotais++;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Prazo Pgto");
				
				linhatotais++;
				row = planilha.createRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				cell.setCellValue("Entrega");
				
				linhatotais = linhaMaterial;
				colunatotais++;
				row = planilha.getRow(linhatotais);				
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(cf.getTotalDescontoItens().getValue().doubleValue());
				
				linhatotais++;
				row = planilha.getRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(cf.getTotalImpostoItens().getValue().doubleValue());
				
				linhatotais++;
				row = planilha.getRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(cf.getTotalFreteItens().getValue().doubleValue());
				
				linhatotais++;
				row = planilha.getRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_MONEY);
				cell.setCellValue(cf.getTotalItensCotacao().getValue().doubleValue());
				
				linhatotais++;
				row = planilha.getRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				if(cf.getPrazopagamento() != null) cell.setCellValue(cf.getPrazopagamento().getNome());
				
				linhatotais++;
				row = planilha.getRow(linhatotais);
				cell = row.createCell((short) (colunatotais));
				cell.setCellStyle(SinedExcel.STYLE_CENTER);
				if(cf.getdtEntregatrans() != null) cell.setCellValue(format.format(cf.getdtEntregatrans()));
				
				colunatotais += 3;
			}
		}
		return wb.getWorkBookResource("mapacotacao_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	/**
	 * M�todo que cria a lista de materiais da cotacao
	 *
	 * @param listaCotacao
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Material> getlistaMateriais(List<Cotacao> listaCotacao) {
		List<Material> lista = new ArrayList<Material>();
		
		if(listaCotacao != null && !listaCotacao.isEmpty()){
			for(Cotacao cotacao : listaCotacao){
				if(cotacao.getListaCotacaofornecedor() != null && !cotacao.getListaCotacaofornecedor().isEmpty()){
					for(Cotacaofornecedor cotacaofornecedor : cotacao.getListaCotacaofornecedor()){
						if(cotacaofornecedor.getListaCotacaofornecedoritem() != null && !cotacaofornecedor.getListaCotacaofornecedoritem().isEmpty()){
							for(Cotacaofornecedoritem cotacaofornecedoritem : cotacaofornecedor.getListaCotacaofornecedoritem()){
								if(!lista.contains(cotacaofornecedoritem.getMaterial())){
									lista.add(cotacaofornecedoritem.getMaterial());
								}
							}
						}
					}
				}
			}
		}
		return lista;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CotacaoDAO#findForExcel(String whereIn)
	 *
	 * @param whereInCotacao
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Cotacao> findForExcel(String whereInCotacao) {
		return cotacaoDAO.findForExcel(whereInCotacao);
	}
	
	public List<Integer> findCotacoesNaoCanceladas(String whereInCdcotacao) {
		return cotacaoDAO.findCotacoesNaoCanceladas(whereInCdcotacao);
	}
	
	
	/**
	 * M�todo que remove da cota��o o nome dos fornecedores n�o associados ao usuario logado
	 *
	 *
	 * @param list
	 * @author Lucas Costa
	 */
	public void formataStringFornecedores (List<Cotacao> list){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoFornecedorColaborador(usuarioLogado)){
			List<String> listaNomeFornecedor = colaboradorFornecedorService.findNomeFornecedorNaoVinculados();
			for(Cotacao cotacao : list){
				for (String nomeFornecedor : listaNomeFornecedor){
					if((cotacao.getAux_cotacao()!=null) && (cotacao.getAux_cotacao().getFornecedores()!=null)){
						if(cotacao.getAux_cotacao().getFornecedores().contains(nomeFornecedor)){
							if(cotacao.getAux_cotacao().getFornecedores().contains("/> "+nomeFornecedor)){
								String aux = cotacao.getAux_cotacao().getFornecedores();
								int posicaoFinal = aux.indexOf("/> "+nomeFornecedor);
								int posiscaoInicial = 0;
								int incremento = 0;
								while (aux.contains("<img")){
									if(aux.indexOf("<img") < posicaoFinal){
										posiscaoInicial = aux.indexOf("<img")+incremento;
										aux=aux.replaceFirst("<img", "");
										incremento+=4;
									}
									else{
										break;
									}
								}
								aux = cotacao.getAux_cotacao().getFornecedores();
								cotacao.getAux_cotacao().setFornecedores(aux.substring(0, posiscaoInicial) + aux.substring(posicaoFinal+3));
							}
							cotacao.getAux_cotacao().setFornecedores(cotacao.getAux_cotacao().getFornecedores().replace("<BR>"+nomeFornecedor, ""));
							cotacao.getAux_cotacao().setFornecedores(cotacao.getAux_cotacao().getFornecedores().replace(nomeFornecedor+"<BR>", ""));
							cotacao.getAux_cotacao().setFornecedores(cotacao.getAux_cotacao().getFornecedores().replace(nomeFornecedor, ""));
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @param cotacao
	 * @return
	 */
	public Cotacao loadWithListaCotacaoFornecedor(Cotacao cotacao) {
		return cotacaoDAO.loadWithListaCotacaoFornecedor(cotacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @param cdcotacaofornecedoritem
	 */
	public void deleteListaMaterialNaoCotado(String cdcotacaofornecedoritem){
		cotacaoDAO.deleteListaMaterialNaoCotado(cdcotacaofornecedoritem);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @param whereIn
	 */
	public void updateSituacaoSolicitacaoCompra(String whereIn){
		cotacaoDAO.updateSituacaoSolicitacaoCompra(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.CotacaoDAO#existeMaterial(String whereInSolicitacaocompra, Cotacao cotacao)
	*
	* @param whereInSolicitacaocompra
	* @param cotacao
	* @return
	* @since 01/12/2014
	* @author Luiz Fernando
	*/
	public boolean existeMaterial(String whereInSolicitacaocompra, Cotacao cotacao) {
		return cotacaoDAO.existeMaterial(whereInSolicitacaocompra, cotacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CotacaoDAO#loadWithEmpresa(Cotacao cotacao)
	*
	* @param cotacao
	* @return
	* @since 23/01/2015
	* @author Luiz Fernando
	*/
	public Cotacao loadWithEmpresa(Cotacao cotacao){
		return cotacaoDAO.loadWithEmpresa(cotacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CotacaoDAO#updateEmpresaUnicaentrega(Cotacao cotacao, Empresa empresa)
	*
	* @param cotacao
	* @param empresa
	* @since 10/03/2015
	* @author Luiz Fernando
	*/
	public void updateEmpresaUnicaentrega(Cotacao cotacao, Empresa empresa) {
		cotacaoDAO.updateEmpresaUnicaentrega(cotacao, empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.CotacaoDAO#findCotacoesEmAberto(String whereInCdcotacao)
	*
	* @param whereInCdcotacao
	* @return
	* @since 13/10/2015
	* @author Luiz Fernando
	*/
	public List<Cotacao> findCotacoesEmAberto(String whereInCdcotacao) {
		return cotacaoDAO.findCotacoesEmAberto(whereInCdcotacao);
	}
	
	public void criarAvisoCotacaoAtrasada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Cotacao> cotacaoList = cotacaoDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		for (Cotacao c : cotacaoList) {
			Aviso aviso = new Aviso("Cota��o com atraso", "C�digo da cota��o: " + c.getCdcotacao(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), c.getCdcotacao(), empresaService.loadPrincipal(), SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, c.getCdcotacao());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, c.getCdcotacao(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
}
