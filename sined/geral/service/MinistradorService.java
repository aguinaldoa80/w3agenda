package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Instrutor;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MinistradorService extends GenericService<Instrutor> {

	/* singleton */
	private static MinistradorService instance;
	public static MinistradorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MinistradorService.class);
		}
		return instance;
	}
	
}
