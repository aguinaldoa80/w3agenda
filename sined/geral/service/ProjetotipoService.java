package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.dao.ProjetotipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProjetotipoService extends GenericService<Projetotipo>{
	
	private ProjetotipoDAO projetotipoDAO;

	public void setProjetotipoDAO(ProjetotipoDAO projetotipoDAO) {
		this.projetotipoDAO = projetotipoDAO;
	}
	
	/* singleton */
	private static ProjetotipoService instance;
	public static ProjetotipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetotipoService.class);
		}
		return instance;
	}
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ProjetotipoDAO#findForComboFlex()
	*
	* @return
	* @since 11/08/2016
	* @author Luiz Fernando
	*/
	public List<Projetotipo> findForComboFlex() {
		return projetotipoDAO.findForComboFlex();
	}
}
