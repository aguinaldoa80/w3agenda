package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.dao.EmailCobrancaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobrancaDocumento;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmailCobrancaService extends GenericService<EmailCobranca>{
	EmailCobrancaDAO emailCobrancaDAO;
	public void setEmailCobrancaDAO(
			EmailCobrancaDAO emailCobrancaDAO) {
		this.emailCobrancaDAO = emailCobrancaDAO;
	}	
	private static EmailCobrancaService instance;
	public static EmailCobrancaService getInstance() {
		if(instance==null)
			instance = Neo.getObject(EmailCobrancaService.class);
		return instance;
	}
	
	/**
	 * Faz referência ao DAO.
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isEmailLido(String whereIn){
		return emailCobrancaDAO.isEmailLido(whereIn);		
	}	
	
	/**
	 * Faz referência ao DAO.
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public Boolean isEmailInvalido(String whereIn){
		return emailCobrancaDAO.isEmailInvalido(whereIn);		
	}
	
	/**
	 * Retorna uma string com o cddocumento 
	 * dos Emails selecionados para serem usados na validação 
	 * antes do reenvio
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public String createWhereInReenvioBoleto(String whereIn){
		List<EmailCobranca> listaEmailCobranca = emailCobrancaDAO.loadEmailCobrancaToReenvio(whereIn);
		String whereInRetorno = "";
		
		for(EmailCobranca emailCobranca : listaEmailCobranca){
			for (EmailCobrancaDocumento emailCobrancaDocumento : emailCobranca.getListaEmailCobrancaDocumento()) {
				whereInRetorno += emailCobrancaDocumento.getDocumento().getCddocumento() + ",";
			}
		}
		
		if (StringUtils.isNotEmpty(whereInRetorno) && whereInRetorno.length() > 1) {
			whereInRetorno = whereInRetorno.substring(0, whereInRetorno.length() - 1);
		}
		
		return whereInRetorno;
	}	
	/**
	 * Faz referência ao DAO.
	 * @param whereIn
	 * @return
	 * @author Andrey Leonardo
	 */
	public List<EmailCobranca> loadEmailCobrancaToReenvio(String whereIn){
		return emailCobrancaDAO.loadEmailCobrancaToReenvio(whereIn);
	}	
	/**
	 * Faz referência ao DAO.
	 * @param emailCobranca
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobranca(EmailCobranca emailCobranca){
		emailCobrancaDAO.updateEmailCobranca(emailCobranca);
	}	
	/**
	 * Faz referência ao DAO.
	 * @param emailCobranca
	 * @param status
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobrancaStatus(EmailCobranca emailCobranca, String status){
		emailCobrancaDAO.updateEmailCobrancaStatus(emailCobranca, status);
	}
	/**
	 * Faz referência ao DAO.
	 * @param emailCobranca
	 * @author Andrey Leonardo
	 */
	public void updateEmailCobrancaEmail(EmailCobranca emailCobranca){
		emailCobrancaDAO.updateEmailCobrancaEmail(emailCobranca);
	}
	/**
	 * Faz referência ao DAO.
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 */
	public EmailCobranca loadEmailCobranca(EmailCobranca emailCobranca){
		return emailCobrancaDAO.loadEmailCobranca(emailCobranca);
	}
	/**
	 * Faz referência ao DAO.
	 * @param emailCobranca
	 * @return
	 * @author Andrey Leonardo
	 */
	public EmailCobranca loadEmailPessoaContato(EmailCobranca emailCobranca){
		return emailCobrancaDAO.loadEmailPessoaContato(emailCobranca);
	}
}
