package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.dao.MotivodevolucaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.w3producao.motivodevolucao.MotivoDevolucaoW3producaoRESTModel;

public class MotivodevolucaoService extends GenericService<Motivodevolucao>{

	private MotivodevolucaoDAO motivodevolucaoDAO;

	public void setMotivodevolucaoDAO(MotivodevolucaoDAO motivodevolucaoDAO) {
		this.motivodevolucaoDAO = motivodevolucaoDAO;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.MotivodevolucaoDAO#findByMotivodevolucao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Motivodevolucao> findByMotivodevolucao(String whereIn){
		return motivodevolucaoDAO.findByMotivodevolucao(whereIn);
	}
	
	public List<MotivoDevolucaoW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de motivodevolucao para sincronizar com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<MotivoDevolucaoW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<MotivoDevolucaoW3producaoRESTModel> lista = new ArrayList<MotivoDevolucaoW3producaoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Motivodevolucao md : motivodevolucaoDAO.findForW3Producao(whereIn))
				lista.add(new MotivoDevolucaoW3producaoRESTModel(md));
		}
		
		return lista;
	}
	
	public List<Motivodevolucao> findForConstacacao(){
		return motivodevolucaoDAO.findForConstacacao();
	}
	
	public List<Motivodevolucao> findForMotivodevolucao(){
		return motivodevolucaoDAO.findForMotivodevolucao();
	}
}
