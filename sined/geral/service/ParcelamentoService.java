package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Parcelamento;
import br.com.linkcom.sined.geral.dao.ParcelamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ParcelamentoService extends GenericService<Parcelamento> {

	private ParcelamentoDAO parcelamentoDAO;
	private DocumentoService documentoService;
	private ContratoService contratoService;
	
	public void setParcelamentoDAO(ParcelamentoDAO parcelamentoDAO) {
		this.parcelamentoDAO = parcelamentoDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}


	/* singleton */
	private static ParcelamentoService instance;
	public static ParcelamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ParcelamentoService.class);
		}
		return instance;
	}
	
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m um parcelamento atrav�s de um documento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ParcelamentoDAO#findByDocumento(Documento)
	 * @param documento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Parcelamento findByDocumento(Documento documento){
		return parcelamentoDAO.findByDocumento(documento);
	}


	/**
	 * Retorna o n�mero de parcelas de uma conta a receber.
	 *
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/02/2013
	 */
	public Integer getNumeroParcelaByDocumento(Documento documento) {
		Integer numParcela = 1;
		Parcelamento parcelamento = this.findByDocumento(documento);
		if(parcelamento != null && parcelamento.getListaParcela() != null && parcelamento.getListaParcela().size() > 0){
			numParcela = parcelamento.getListaParcela().size();
		}
		return numParcela;
	}
	
	public Money getValorDividoNumParcelasDiferencapagamento(Documento documento, Money valorBruto, Money valorPagoComissionamento, Money valorNota, Integer numParcela) {
		Money valorbrutodividido = valorBruto.divide(new Money(numParcela));
		Money valornotadividido = valorNota.divide(new Money(numParcela));
		Parcelamento parcelamento;

		if(documento != null && documento.getCddocumento() != null){
			//Se o documento tiver origem de documento(s) negociado(s) � preciso saber quantas parcelas cada documento tem
			//isto � para quando o comissionamento tem que considerar diferen�a do valor pago
			List<Documento> listaDocumento = documentoService.findForQtdeParcelasComissionamento(documento);
			if(listaDocumento != null && !listaDocumento.isEmpty()){
				for(Documento d : listaDocumento){
					parcelamento = this.findByDocumento(d);
					if(parcelamento != null && parcelamento.getListaParcela() != null && parcelamento.getListaParcela().size() > 0){
						numParcela = parcelamento.getListaParcela().size();
						valorbrutodividido = valorbrutodividido.divide(new Money(numParcela));
						valornotadividido = valornotadividido.divide(new Money(numParcela));
					}
				}
			}
		}
		
		return contratoService.calculaValorDiferencapagamento(valorbrutodividido, valorPagoComissionamento, valornotadividido);
	}
	
	/**
	 * Caso a conta a receber seja estornada, retorna o valor original.
	 *
	 * @param documento, valorPrevista, descriacaoCorrecao
	 * @author Lucas Costa
	 * @since 17/11/2014
	 */
	public void voltaValorIndiceCorrecao(Documento documento, Money valorPrevista, String descriacaoCorrecao){
		
	}
}
