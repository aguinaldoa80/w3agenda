package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.ImagensEcommerce;
import br.com.linkcom.sined.geral.dao.ImagensEcommerceDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ImagensEcommerceService extends GenericService<ImagensEcommerce>{
	
	ImagensEcommerceDAO imagensEcommerceDAO;
	
	public static ImagensEcommerceService instance;
	
	public void setImagensEcommerceDAO(ImagensEcommerceDAO imagensEcommerceDAO) {this.imagensEcommerceDAO = imagensEcommerceDAO;}

	public Long loadCdFileByHash(String hash) {
		return imagensEcommerceDAO.loadCdFileByHash(hash);
	}

	public static ImagensEcommerceService getInstance(){
		if (instance==null){
		return Neo.getObject(ImagensEcommerceService.class);
		}else {
		return instance;
		}
		}
	
}
