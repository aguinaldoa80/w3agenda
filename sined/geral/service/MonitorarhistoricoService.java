package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.geral.dao.MonitorarhistoricoDAO;
import br.com.linkcom.sined.modulo.briefcase.controller.crud.filter.MonitorarhistoricoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MonitorarhistoricoService extends GenericService<Radiushistorico>{

	private MonitorarhistoricoDAO monitorarhistoricoDAO;
	
	public void setMonitorarhistoricoDAO(
			MonitorarhistoricoDAO monitorarhistoricoDAO) {
		this.monitorarhistoricoDAO = monitorarhistoricoDAO;
	}
	
	/**
	 * M�todo que gera relat�rio PDF da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarMonitoramentoHistorico(MonitorarhistoricoFiltro filtro) {
		Report report = new Report("/briefcase/monitorarhistorico");
		
		List<Radiushistorico> lista = monitorarhistoricoDAO.gerarMonitoramentoHistorico(filtro);
		if(lista != null && lista.size() > 0){
			for (Radiushistorico item : lista) {
				Double resultadoEntrada = item.getDadoentrada() != null ? item.getDadoentrada().doubleValue() : 0D;
				//Converte bytes para megabytes
				item.setDadoentradaMb(resultadoEntrada/1048576);
				
				Double resultadoSaida = item.getDadosaida() != null ? item.getDadosaida().doubleValue() : 0D;
				//Converte bytes para megabytes
				item.setDadosaidaMb(resultadoSaida/1048576);
			}
			
			Long somaUploads  = this.somaUploads(filtro);
			Double totalUploads = somaUploads.doubleValue();
			
			Long somaDownloads = this.somaDownloads(filtro);
			Double totalDownloads = somaDownloads.doubleValue();
			
			//Converte bytes para megabytes
			report.addParameter("TOTALUPLOAD", totalUploads/1048576);
			report.addParameter("TOTALDOWNLOAD", totalDownloads/1048576);
			
			Long totalTemposessao = this.somaTemposessao(filtro);
			report.addParameter("TOTALTEMPOSESSAO", this.convertSecoundsToHour(totalTemposessao));
			
			report.setDataSource(lista);
		}
		
		return report;
	}	
	
	/**
	 * Faz refer�ncia ao DAO
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 09/07/2010
	 */
	public Long somaUploads(MonitorarhistoricoFiltro filtro) {
		return monitorarhistoricoDAO.somaUploads(filtro);
	}
	/**
	 * Faz refer�ncia ao DAO
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 09/07/2010
	 */
	public Long somaDownloads(MonitorarhistoricoFiltro filtro) {
		return monitorarhistoricoDAO.somaDownloads(filtro);
	}
	
	/**
	 * Converte segundos em h:m:s
	 * @param secounds
	 * @return
	 * @author Taidson
	 * @since 09/07/2010
	 */
	public String convertSecoundsToHour(Long seconds){
		Long second = seconds % 60;
		Long minutes = seconds / 60;
		Long minute = minutes % 60;
		Long hour = minutes / 60;
		return String.format ("%02d:%02d:%02d", hour, minute, second); 
	}
	
	/**
	* Faz refer�ncia ao DAO
	* @param filtro
	* @return
	* @author Taidson
	* @since 09/07/2010
	*/
	public Long somaTemposessao(MonitorarhistoricoFiltro filtro) {
		return monitorarhistoricoDAO.somaTemposessao(filtro);
	}
	
}
