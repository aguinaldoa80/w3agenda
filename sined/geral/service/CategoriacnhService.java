package br.com.linkcom.sined.geral.service;

import java.util.Set;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Categoriacnh;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcategoriacnh;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CategoriacnhService extends GenericService<Categoriacnh> {	
	
	
	/**
	 * M�todo para salvar a categoria cnh escolhida para o colaborador
	 * 
	 * @param categoriacnh
	 * @param bean
	 * @return listaColaboradorcategoriacnh preenchida se diferente de null
	 * 
	 * @author Joao Paulo Zica
	 */	
	public Set<Colaboradorcategoriacnh> saveColaboradorcategoriacnh(Set<Categoriacnh> listaCategoriacnh, Colaborador bean){
		Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh = new ListSet<Colaboradorcategoriacnh>(Colaboradorcategoriacnh.class);
		if (listaCategoriacnh != null) {
			for (Categoriacnh categoriacnh : listaCategoriacnh) {
				Colaboradorcategoriacnh colaboradorcategoriacnh = new Colaboradorcategoriacnh();
				colaboradorcategoriacnh.setCategoriacnh(categoriacnh);
				colaboradorcategoriacnh.setColaborador(bean);
				listaColaboradorcategoriacnh.add(colaboradorcategoriacnh);
			}		
		}
		return listaColaboradorcategoriacnh;
	}
	
	/* singleton */
	private static CategoriacnhService instance;
	public static CategoriacnhService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CategoriacnhService.class);
		}
		return instance;
	}
	
}
