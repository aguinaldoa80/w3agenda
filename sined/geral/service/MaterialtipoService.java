package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.dao.MaterialtipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialtipoService extends GenericService<Materialtipo> {

	MaterialtipoDAO materialtipoDAO;
	
	public void setMaterialtipoDAO(MaterialtipoDAO materialtipoDAO) {
		this.materialtipoDAO = materialtipoDAO;
	}
	
	/* singleton */
	private static MaterialtipoService instance;
	public static MaterialtipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialtipoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.daoMaterialtipoDAO.DAO#findByTipo
	 * @param planejamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Materialtipo> findByTipo(Planejamento planejamento){
		return materialtipoDAO.findByTipo(planejamento);
	}
	
	public List<Materialtipo> findByServicoservidortipo(Servicoservidortipo... servicoservidortipos){
		return materialtipoDAO.findByServicoservidortipo(servicoservidortipos);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 * @param materialtipo 
	 */
	public List<Materialtipo> findMaterialTipoPossuiMaterialServico(Materialtipo materialtipo) {
		return materialtipoDAO.findMaterialTipoPossuiMaterialServico(materialtipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialtipoDAO#buscaMaterialtipoIntervaloAlturaLargura(Double altura, Double largura)
	 *
	 * @param altura
	 * @param largura
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Materialtipo> buscaMaterialtipoIntervaloAlturaLargura(Double altura, Double largura) {
		return materialtipoDAO.buscaMaterialtipoIntervaloAlturaLargura(altura, largura);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialtipoDAO#findByNomeForImportacao(String nome)
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2013
	 */
	public Materialtipo findByNomeForImportacao(String nome) {
		return materialtipoDAO.findByNomeForImportacao(nome);
	}

	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialtipoDAO#updateCdArquivoLegenda(Integer)
	 * 
	 * @param mapa
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public void bulkUpdateCdarquivolegenda(Map<Integer,Integer> mapa) throws Exception{
		materialtipoDAO.bulkUpdateCdarquivolegenda(mapa);
	}
}
