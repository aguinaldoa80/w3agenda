package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.NotaFiscalServicoItemDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaFiscalServicoItemService extends GenericService<NotaFiscalServicoItem> {

	private NotaFiscalServicoItemDAO notaFiscalServicoItemDAO;
	
	public void setNotaFiscalServicoItemDAO(NotaFiscalServicoItemDAO notaFiscalServicoItemDAO) {this.notaFiscalServicoItemDAO = notaFiscalServicoItemDAO;}

	/**
	 * Cria uma lista de Item de Nota fiscal de servi�o com base em uma lista de tarefas.
	 * Se houver tarefas repetidas ele agrupa e incrementa a quantidade do item correspondente.
	 * 
	 * @param listaTarefa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<NotaFiscalServicoItem> criaListaNotaItem(List<Tarefa> listaTarefa){
		List<NotaFiscalServicoItem> listaNotaItem = new ArrayList<NotaFiscalServicoItem>();
		
		for(int i = 0; i < listaTarefa.size(); i++) {
			Tarefa tarefa = listaTarefa.get(i);
			NotaFiscalServicoItem nfsi = new NotaFiscalServicoItem();
			int freq = Collections.frequency(listaTarefa, tarefa);
			nfsi.setDescricao(tarefa.getDescricao());
			nfsi.setQtde(new Double(Integer.valueOf(freq)));
			listaNotaItem.add(nfsi);
			while(listaTarefa.remove(tarefa));
		}
		
		return listaNotaItem;
	}

	/**
	* M�todo que ordena os servi�os de acordo com a ordem realizada na venda
	*
	* @param notaFiscalServico
	* @since 22/09/2016
	* @author Luiz Fernando
	*/
	public void ordenaListaItens(NotaFiscalServico notaFiscalServico) {
		if(notaFiscalServico != null && SinedUtil.isListNotEmpty(notaFiscalServico.getListaItens())){
			Collections.sort(notaFiscalServico.getListaItens(), new Comparator<NotaFiscalServicoItem>() {
				@Override
				public int compare(NotaFiscalServicoItem o1, NotaFiscalServicoItem o2) {
					int comparisson = 0;
					
					if(o1.getVendamaterial() != null && o1.getVendamaterial().getVenda() != null &&
							o2.getVendamaterial() != null && o2.getVendamaterial().getVenda() != null){
						Venda v1 = o1.getVendamaterial().getVenda();
						Venda v2 = o2.getVendamaterial().getVenda();
						if(v1 != null && v1.getDtvenda() != null && v2 != null && v2.getDtvenda() != null){
							comparisson = v1.getDtvenda().compareTo(v2.getDtvenda());
							if(comparisson == 0){
								comparisson = v1.getCdvenda().compareTo(v2.getCdvenda());
								if(comparisson == 0){
									if(o1.getVendamaterial().getOrdem() != null &&
											o2.getVendamaterial().getOrdem() != null){
										comparisson = o1.getVendamaterial().getOrdem().compareTo(o2.getVendamaterial().getOrdem());
									} else if(o1.getVendamaterial().getOrdem() == null){
										return 1;
									} else if(o2.getVendamaterial().getOrdem() == null){
										return -1;
									} else {
										return 0;
									}
								}
							}
						}
					}
					return comparisson;
				}
			});
		}
	}

	public List<NotaFiscalServicoItem> findForRateioFaturamento(NotaFiscalServico nfs) {
		return notaFiscalServicoItemDAO.findForRateioFaturamento(nfs);
	}

	public List<NotaFiscalServicoItem> findForPdfListagem(String whereIn) {
		return notaFiscalServicoItemDAO.findForPdfListagem(whereIn);
	}

	public Boolean existePedidoVendaMaterial(Pedidovendamaterial pedidovendamaterial) {
		return notaFiscalServicoItemDAO.existePedidoVendaMaterial(pedidovendamaterial);
	}
}
