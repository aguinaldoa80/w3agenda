package br.com.linkcom.sined.geral.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import br.com.linkcom.lkutil.csv.CSVWriter;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmento;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRetornoSegmentoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoSegmentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoRetornoSegmentoService extends GenericService<BancoConfiguracaoRetornoSegmento>{

	private BancoConfiguracaoRetornoSegmentoDAO bancoConfiguracaoRetornoSegmentoDAO;
	
	public void setBancoConfiguracaoRetornoSegmentoDAO(
			BancoConfiguracaoRetornoSegmentoDAO bancoConfiguracaoRetornoSegmentoDAO) {
		this.bancoConfiguracaoRetornoSegmentoDAO = bancoConfiguracaoRetornoSegmentoDAO;
	}
	
	public List<BancoConfiguracaoRetornoSegmento> findByBanco(Banco banco){
		return bancoConfiguracaoRetornoSegmentoDAO.findByBanco(banco);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * @param segmento
	 * @return
	 * @since 09/06/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetornoSegmento> findForImportacao (){
		return bancoConfiguracaoRetornoSegmentoDAO.findForImportacao();
	}
	
	/**
	 * M�todo com refer�ncias no DAO
	 * @param ids
	 * @return
	 * @author C�sar
	 * @since 08/06/2016
	 */
	public List<BancoConfiguracaoRetornoSegmento> findforWhereIn(String whereIn){
		return  bancoConfiguracaoRetornoSegmentoDAO.findforWhereIn(whereIn);
	}
	
	/**
	 * M�todo respons�vel por processar exporta��o do arquivo
	 * @param request
	 * @param filtro
	 * @throws IOException
	 * @author C�sar
	 * @since 07/06/2016
	 */
	public void processaExportar(WebRequestContext request, BancoConfiguracaoRetornoSegmentoFiltro filtro, ByteArrayOutputStream conteudoZip) throws IOException{
		List<BancoConfiguracaoRetornoSegmento> listaBancoconfigSegmento = new ArrayList<BancoConfiguracaoRetornoSegmento>();		
		ZipOutputStream zip = new ZipOutputStream(conteudoZip);
		zip.setLevel(Deflater.BEST_COMPRESSION);
		
		//Recebendo os ids
		String whereIn = filtro.getSelecteditens();
		//Carregando os campos necess�rios para exportar
		listaBancoconfigSegmento =  this.findforWhereIn(whereIn);
		//Preenchendo arquivo para exportar
		if(listaBancoconfigSegmento != null && !listaBancoconfigSegmento.isEmpty()){				
			for (BancoConfiguracaoRetornoSegmento segmento : listaBancoconfigSegmento) {
				
				CSVWriter csv = new CSVWriter();
				csv.setDelimiter("@");
							
				//preenchendo dados do segmento
				csv.add(segmento.getNome());
				csv.add(segmento.getIdentificador());
				csv.add(segmento.getTipo().name());
				csv.add(segmento.getBanco().getCdbanco());
				csv.newLine();
				
				zip.putNextEntry(new ZipEntry("planilha_banco_config_retorno_" + segmento.getNome().replace(" ", "_")  + ".csv"));
				zip.write(csv.toString().getBytes());
				zip.closeEntry();
			}
		}
		zip.close();		
	}

	/**
	 * M�todo respons�vel pelo recebimento e salvar os arquivos para importa��o
	 * @param arquivos
	 * @return
	 * @author C�sar
	 * @throws IOException 
	 * @since 08/06/2016
	 */
	public String importacaoDados (List<Arquivo> arquivos, WebRequestContext request) throws IOException{
		String mensagemErro = "";
		if(arquivos != null){
			for (Arquivo arquivo : arquivos) {				
				if(arquivo.getArquivo() != null){			
					BancoConfiguracaoRetornoSegmento bancoConfiguracaoRetornoSegmento = this.processaArquivo(arquivo.getArquivo(), request);								
					if(bancoConfiguracaoRetornoSegmento != null)
						bancoConfiguracaoRetornoSegmentoDAO.saveOrUpdate(bancoConfiguracaoRetornoSegmento);
					else
						mensagemErro += "A planilha " + arquivo.getArquivo().getNome() + " n�o foi importada por inconsist�ncias. <br>";
				}				
			}
		}
		return mensagemErro;
	}	
	/**
	 * M�todo respons�vel por processar os CSV importados
	 * @param arquivo
	 * @param bancoConfiguracaoSegmento
	 * @author C�sar
	 * @throws Exception 
	 * @since 08/06/2016
	 */
	private BancoConfiguracaoRetornoSegmento processaArquivo(Arquivo arquivo, WebRequestContext request) throws IOException {
		BufferedReader br = null;
		InputStream csv = new ByteArrayInputStream(arquivo.getContent());
		BancoConfiguracaoRetornoSegmento segmento = new BancoConfiguracaoRetornoSegmento();	

		String linha;
		String[] dados;
		
		try {
			br = new BufferedReader(new InputStreamReader(csv));
			while ((linha = br.readLine()) != null) {			
				dados = linha.split("@");			
				segmento.setNome(dados[0].replace("\"", ""));
				segmento.setIdentificador(dados[1].replace("\"", ""));
				segmento.setTipo(BancoConfiguracaoTipoSegmentoEnum.valueOf(dados[2].replace("\"", "")));
				if(!dados[3].replace("\"", "").isEmpty()){
					segmento.setBanco(new Banco(Integer.parseInt(dados[3].replace("\"", ""))));
				}
				linha = "";
			}
			//Verifica��o se cont�m nome duplicado e add caracteres para evitar nome duplicados
			List<BancoConfiguracaoRetornoSegmento> auxBanco;
			if(!segmento.getNome().isEmpty() && segmento.getNome().contains("_")){	
				segmento.setNome(segmento.getNome().substring(0, segmento.getNome().length()-2));
			}
			auxBanco = this.validateDuplicacdo(segmento);
			if(SinedUtil.isListNotEmpty(auxBanco) && auxBanco.size() > 1){
				Integer contUltimo = 0;
				try {
					contUltimo = Integer.parseInt(auxBanco.get(auxBanco.size()-1).getNome().substring(auxBanco.get(auxBanco.size()-1).getNome().length() - 1 , auxBanco.get(auxBanco.size()-1).getNome().length()));
				} catch (Exception e) {}
				Integer cont = contUltimo + 1;
					segmento.setNome(segmento.getNome() + "_" + cont);
			}else if (SinedUtil.isListNotEmpty(auxBanco) && auxBanco.size() == 1){
				segmento.setNome(segmento.getNome()+ "_1");
			}
		} catch (Exception e) {		
			e.printStackTrace();
			segmento = null;
		} finally {
			if (br != null) {
				br.close();		
			}
		}
		return segmento;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param bancoConfig
	 * @return
	 * @since 15/07/2016
	 * @author C�sar
	 */
	public List<BancoConfiguracaoRetornoSegmento> validateDuplicacdo(BancoConfiguracaoRetornoSegmento bancoConfig){
		return bancoConfiguracaoRetornoSegmentoDAO.validateDuplicacdo(bancoConfig);
	}
}
