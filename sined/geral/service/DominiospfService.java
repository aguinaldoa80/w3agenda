package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Dominiospf;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DominiospfService extends GenericService<Dominiospf>{

	@Override
	public void saveOrUpdate(Dominiospf bean) {
		boolean novoRegistro = false;
		Dominiospf dominioAnterior = null;
		if(bean.getCddominiospf() == null){
			novoRegistro = true;
		}else{
			dominioAnterior = load(bean);
		}
		
		//Muda a data da vers�o e incrementa o contador.
		if(dominioAnterior != null){
			bean.setVersao(SinedUtil.mudaVersaoBriefCase(bean.getVersao()));
		}else{
			bean.setVersao(SinedUtil.criaVersaoBriefCase());
		}
		
		super.saveOrUpdate(bean);
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCddominiospf());
	}
	
	@Override
	public void delete(Dominiospf bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.DOMINIO, AcaoBriefCase.REMOVER, bean.getCddominiospf());
		super.delete(bean);
	}
	
}
