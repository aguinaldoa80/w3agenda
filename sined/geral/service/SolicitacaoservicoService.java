package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Agendamentoservico;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Solicitacaoservico;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicorequisitado;
import br.com.linkcom.sined.geral.bean.auxiliar.AgendaVO;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.geral.dao.RequisicaoDAO;
import br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.SolicitacaoservicoFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class SolicitacaoservicoService extends GenericService<Solicitacaoservico> {
	
	private EmpresaService empresaService;
	private SolicitacaoservicoDAO solicitacaoservicoDAO;
	private SolicitacaoservicotipoService solicitacaoservicotipoService;
	private RequisicaoDAO requisicaoDAO;
	private EnderecoService enderecoService;
	private OportunidadeService oportunidadeService;
	private AgendamentoservicoService agendamentoservicoService;
	private EnvioemailService envioemailService;
	private AgendainteracaoService agendainteracaoService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;

	public void setAgendainteracaoService(AgendainteracaoService agendainteracaoService) {this.agendainteracaoService = agendainteracaoService;}
	public void setAgendamentoservicoService(AgendamentoservicoService agendamentoservicoService) {this.agendamentoservicoService = agendamentoservicoService;}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {this.oportunidadeService = oportunidadeService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setSolicitacaoservicotipoService(SolicitacaoservicotipoService solicitacaoservicotipoService) {this.solicitacaoservicotipoService = solicitacaoservicotipoService;}
	public void setSolicitacaoservicoDAO(SolicitacaoservicoDAO solicitacaoservicoDAO) {this.solicitacaoservicoDAO = solicitacaoservicoDAO;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setRequisicaoDAO(RequisicaoDAO requisicaoDAO) {this.requisicaoDAO = requisicaoDAO;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	/**
	 * Fun��o para a gera��o do relat�rio de eventos
	 * 
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#findForRelatorioEvento(EventoservicointernoFiltro)
	 */
	public List<Solicitacaoservico> findForRelatorioEvento(EventoservicointernoFiltro filtro){
		return solicitacaoservicoDAO.findForRelatorioEvento(filtro);
	}
	
	/**
	 * Envia o e-mail de cancelamento de uma solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#corpoEmail
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#loadForEmailConcluir
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmail
	 *
	 * @param bean
	 * @param obs
	 * @author Rodrigo Freitas
	 */
	public void enviaEmailCancelar(Solicitacaoservico bean, String obs){
		String corpo = this.corpoEmail(bean, "/WEB-INF/template/templateMensagemEmailSolServCancela.tpl", obs);
		bean = this.loadForEmailConcluir(bean);
		this.enviaEmail(bean.getRequisitante().getEmail(), corpo, bean.getSolicitacaoservicotipo().getNome(), bean.getRequisitante(), bean.getRequisitante().getNome());
	}
	
	/**
	 * Envia o e-mail de conclus�o de uma solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#corpoEmail
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#loadForEmailConcluir
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmail
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void enviaEmailConcluir(Solicitacaoservico bean){
		String corpo = this.corpoEmail(bean, "/WEB-INF/template/templateMensagemEmailSolServConclui.tpl", null);
		bean = this.loadForEmailConcluir(bean);
		this.enviaEmail(bean.getRequisitante().getEmail(), corpo, bean.getSolicitacaoservicotipo().getNome(), bean.getRequisitante(), bean.getRequisitante().getNome());
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#loadForEmailConcluir
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Solicitacaoservico loadForEmailConcluir(Solicitacaoservico bean) {
		return solicitacaoservicoDAO.loadForEmailConcluir(bean);
	}
	
	/**
	 * Envia o e-mail de cria��o da solicita��o de servi�o.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#corpoEmail
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#enviaEmail
	 * @see br.com.linkcom.sined.geral.service.SolicitacaoservicoService#updateDataEnvio
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void enviaEmailCriar(Solicitacaoservico bean) {
		Set<Solicitacaoservicorequisitado> listaRequisitado = bean.getListaRequisitado();
		String corpo = this.corpoEmail(bean, "/WEB-INF/template/templateMensagemEmailSolServicoNovo.tpl", null);
		
		bean.setSolicitacaoservicotipo(solicitacaoservicotipoService.load(bean.getSolicitacaoservicotipo(), "solicitacaoservicotipo.cdsolicitacaoservicotipo, solicitacaoservicotipo.nome"));
		
		for (Solicitacaoservicorequisitado req : listaRequisitado) {
			this.enviaEmail(req.getEmail(), corpo, bean.getSolicitacaoservicotipo().getNome(), req.getColaborador(), req.getColaborador().getNome());
		}
		
		this.updateDataEnvio(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#updateDataEnvio
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	private void updateDataEnvio(Solicitacaoservico bean) {
		solicitacaoservicoDAO.updateDataEnvio(bean);
	}

	/**
	 * Cria o corpo do e-mail de solicita��o de servi�o a partir do template.
	 *
	 * @param solicitacao
	 * @param template
	 * @param conteudo
	 * @return
	 * @author Rodrigo Freitas
	 */
	private String corpoEmail(Solicitacaoservico solicitacao, String template, String conteudo){
		String corpo = null;
		try {
			TemplateManager temp = new TemplateManager(template)
							.assign("link", SinedUtil.getUrlWithContext() + "/servicointerno/crud/Solicitacaoservico?ACAO=consultar&cdsolicitacaoservico=" + solicitacao.getCdsolicitacaoservico())
							.assign("codigo", solicitacao.getCdsolicitacaoservico().toString())
							.assign("app", SinedUtil.isSined() ? "SINED": "W3ERP");
			if(conteudo != null) temp.assign("conteudo", conteudo);
			corpo = temp.getTemplate();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro no processamento do template.");
		}
		return corpo;
	}
	
	/**
	 * Envia o e-mail de solicita��o de servi�o.
	 *
	 * @author Rodrigo Freitas
	 * 
	 * @param para
	 * @param corpo
	 * @param assunto
	 * @param pessoa 
	 * @param nomeContato 
	 */
	private void enviaEmail(String para, String corpo, String assunto, Pessoa pessoa, String nomeContato){
		Empresa empresa = empresaService.loadPrincipal();

		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		
		Envioemail envioemail = envioemailService.registrarEnvio(empresa.getEmail(), assunto, corpo, Envioemailtipo.AVISO_SOLICITACAO_SERVICO, 
				pessoa, para, nomeContato, email);

		try {
			email.setFrom(empresa.getEmail());
			email.setSubject(assunto);
			email.setTo(para);
			email.addHtmlText(corpo + EmailUtil.getHtmlConfirmacaoEmail(envioemail, para));
			
			email.sendMessage();
		} catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro no envio de email.");
		}
	}
	
	public void salvarAvisoServico(String tipo, Solicitacaoservico bean) {
		Motivoaviso motivoaviso = motivoavisoService.findByMotivo(MotivoavisoEnum.SOLICITACAO_SERVICO);
		if(motivoaviso != null){
			List<Aviso> avisoList = new ArrayList<Aviso>();
			avisoList.add(new Aviso("Solicita��o de servi�o " + tipo, "C�digo da solicita��o de servi�o: " + bean.getCdsolicitacaoservico(), motivoaviso.getTipoaviso(), 
					motivoaviso.getPapel(), motivoaviso.getUsuario(), motivoaviso.getAvisoorigem(), bean.getCdsolicitacaoservico(), empresaService.loadPrincipal(), 
					bean.getProjeto() != null && bean.getProjeto().getCdprojeto() != null ? bean.getProjeto().getCdprojeto().toString() : null, motivoaviso));
			
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#haveSolicitacaoSituacaoDiferente
	 *
	 * @param whereIn
	 * @param situacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveSolicitacaoSituacaoDiferente(String whereIn, Solicitacaoservicosituacao... situacao){
		return solicitacaoservicoDAO.haveSolicitacaoSituacaoDiferente(whereIn, situacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#updateSituacao
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Solicitacaoservicosituacao situacao) {
		solicitacaoservicoDAO.updateSituacao(whereIn, situacao);
	}
	
	/**
	 * Concatena hor�rio da hora da data in�cio com a hora da data fim 
	 * @param dtinicio
	 * @param dtFim
	 * @return
	 * @author Taidson
	 * @since 05/07/2010
	 */
	public String concatenaHorario(Timestamp dtinicio, Timestamp dtFim){
		String hrInicio = null;
		String hrFim = null;
		String horario = null;
		
		if(dtinicio != null)
			hrInicio = new SimpleDateFormat("HH:mm").format(dtinicio);
		if(dtFim != null)
			hrFim = new SimpleDateFormat("HH:mm").format(dtFim);
		
		if (hrInicio != null && hrFim != null){
			horario =  hrInicio +" - "+ hrFim;
		}else if (hrInicio != null){
			horario =  hrInicio;
		}else if (hrFim != null){
			horario = hrFim;
		}
		return horario;
	}
	
	/**
	 * Atribui o usu�rio logado a um colaborador
	 * @return
	 * @author Taidson
	 * @since 07/07/2010
	 */
	public Colaborador obtemPessoaColaborador(){
		Colaborador colaborador = new Colaborador();
		colaborador.setCdpessoa(SinedUtil.getUsuarioLogado().getCdpessoa());
		return colaborador;
	}
	
	/**
	 * Retorna as solicita��es de servi�os requisitadas ao colaborador logado.
	 * @param dataString
	 * @return
	 * @author Taidson
	 * @since 02/07/2010
	 */
	public List<Solicitacaoservico> findForAgendaRequisitadoFlex(Date data){
		List<Solicitacaoservico> listaSoliciatacaoservico = new ListSet<Solicitacaoservico>(Solicitacaoservico.class);
		
		listaSoliciatacaoservico = solicitacaoservicoDAO.findForAgendaRequisitadoFlex(obtemPessoaColaborador(), data);
		
		if(listaSoliciatacaoservico != null && listaSoliciatacaoservico.size() > 0){
			for (Solicitacaoservico item : listaSoliciatacaoservico) {
				if(item.getDtinicio() != null || item.getDtfim() != null){
					item.setHorario(concatenaHorario(item.getDtinicio(), item.getDtfim()));
				}
			}	
		}
		
		return listaSoliciatacaoservico;
	}
	
	public List<AgendaVO> findForAgenda(String dataString){
		try {
			Date data = SinedDateUtils.stringToDate(dataString);
			List<AgendaVO> lista = new ArrayList<AgendaVO>();
			AgendaVO agendaVO;
			StringBuilder sb;
			
			// AGENDA DE INTERA��O
			List<Agendainteracao> listaAgendainteracao = agendainteracaoService.findForAgendaPortlet(data);
			for (Agendainteracao agendainteracao : listaAgendainteracao) {
				agendaVO = new AgendaVO();
				
				sb = new StringBuilder();
				if(agendainteracao.getCliente() != null && agendainteracao.getCliente().getNome() != null){
					sb.append(agendainteracao.getCliente().getNome());
				}
				
				if(agendainteracao.getContrato() != null && agendainteracao.getContrato().getDescricao() != null){
					sb.append(" (").append(agendainteracao.getContrato().getDescricao()).append(")");
				}
				
				if(agendainteracao.getMaterial() != null && agendainteracao.getMaterial().getNome() != null){
					sb.append(" (").append(agendainteracao.getMaterial().getNome()).append(")");
				}
				
				agendaVO.setHorario("");
				agendaVO.setDescricao(sb.toString());
				
				lista.add(agendaVO);
			}
			
			
			// SOLICITA��O DE SERVI�O
			List<Solicitacaoservico> listaSolicitacaoservico = this.findForAgendaRequisitadoFlex(data);
			for (Solicitacaoservico solicitacaoservico : listaSolicitacaoservico) {
				agendaVO = new AgendaVO();
				agendaVO.setHorario(solicitacaoservico.getHorario());
				agendaVO.setDescricao(solicitacaoservico.getDescricao());
				
				lista.add(agendaVO);
			}
			
			// OPORTUNIDADE
			List<Oportunidade> listaOportunidade = oportunidadeService.findForAgenda(data);
			for (Oportunidade oportunidade : listaOportunidade) {
				agendaVO = new AgendaVO();
				
				sb = new StringBuilder();
				if(oportunidade.getNome() != null) 
					sb.append(oportunidade.getNome());
				
				if(oportunidade.getContacrm() != null && oportunidade.getContacrm().getNome() != null) 
					sb.append(" (").append(oportunidade.getContacrm().getNome()).append(")");
				
				agendaVO.setHorario("");
				agendaVO.setDescricao(sb.toString());
				
				lista.add(agendaVO);
			}
			
			// AGENDA DE ATENDIMENTO
			List<Agendamentoservico> listaAgendamentoservico = agendamentoservicoService.findForAgenda(data);
			for (Agendamentoservico agendamentoservico : listaAgendamentoservico) {
				agendaVO = new AgendaVO();
				
				sb = new StringBuilder();
				
				if(agendamentoservico.getCliente() != null && agendamentoservico.getCliente().getNome() != null) 
					sb.append(agendamentoservico.getCliente().getNome());
				
				if(agendamentoservico.getMaterial() != null && agendamentoservico.getMaterial().getNome() != null) 
					sb.append(" (").append(agendamentoservico.getMaterial().getNome()).append(")");
				
				if(agendamentoservico.getEscalahorario() != null) agendaVO.setHorario(agendamentoservico.getEscalahorario().getHorario());
				agendaVO.setDescricao(sb.toString());
				
				lista.add(agendaVO);
			}
			
			Collections.sort(lista,new Comparator<AgendaVO>(){
				public int compare(AgendaVO o1, AgendaVO o2) {
					return o1.getHorario().compareTo(o2.getHorario());
				}
			});
			
			return lista;
		} catch (Exception e) {
			return new ArrayList<AgendaVO>();
		}
	}
	
	/**
	 * Retorna a quantidade de solicita��es de servi�os pendentes requisitadas ao colaborador logado.
	 * @return
	 * @author Taidson
	 * @since 07/07/2010
	 */
	public String solicitacoesPendentesForFlex(){
		return solicitacaoservicoDAO.solicitacoesPendentesForFlex(obtemPessoaColaborador()).toString();
	}
	
	/**
	 * Retorna a quantidade de requisi��es (Em espera, Em andamento e Em teste) 
	 * vinculadas ao colaborador logado.
	 * @return
	 * @author Taidson
	 * @since 07/07/2010
	 */
	public String qtdeRequisicoesForFlex(){
		return requisicaoDAO.qtdeRequisicoesForFlex(obtemPessoaColaborador()).toString();
	}
	
	public IReport gerarRelatorioListagem(SolicitacaoservicoFiltro filtro) {
		List<Solicitacaoservico> lista = this.findForRelatorio(filtro);
		
		for (Solicitacaoservico solicitacaoservico : lista) {
			if(solicitacaoservico.getEnderecocliente() != null){
				solicitacaoservico.setEnderecocliente(enderecoService.loadEndereco(solicitacaoservico.getEnderecocliente()));
			}
		}
		
		Report report = new Report("/servicointerno/solicitacaoservico");
		if(lista != null && lista.size() > 0)
			report.setDataSource(lista);
		
		return report;
	}
	
	private List<Solicitacaoservico> findForRelatorio(SolicitacaoservicoFiltro filtro) {
		return solicitacaoservicoDAO.findForRelatorio(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaoservicoDAO#loadForEnviarSolicitacao(Solicitacaoservico bean)
	 *
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	public Solicitacaoservico loadForEnviarSolicitacao(Solicitacaoservico bean) {
		return solicitacaoservicoDAO.loadForEnviarSolicitacao(bean);
	}
}
