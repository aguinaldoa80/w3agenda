package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contratoservicostatus;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratoservicostatusService extends GenericService<Contratoservicostatus> {

	/* singleton */
	private static ContratoservicostatusService instance;
	
	public static ContratoservicostatusService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContratoservicostatusService.class);
		}
		return instance;
	}
}
