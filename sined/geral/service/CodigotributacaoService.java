package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.dao.CodigotributacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CodigotributacaoService extends GenericService<Codigotributacao> {

	private CodigotributacaoDAO codigotributacaoDAO;
	
	public void setCodigotributacaoDAO(CodigotributacaoDAO codigotributacaoDAO) {
		this.codigotributacaoDAO = codigotributacaoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CodigotributacaoDAO#loadByCodigo(String codigo)
	 *
	 * @param codigo
	 * @return
	 * @since 30/03/2012
	 * @author Rodrigo Freitas
	 */
	public Codigotributacao loadByCodigo(String codigo) {
		return codigotributacaoDAO.loadByCodigo(codigo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.CodigotributacaoDAO#findByCodigo(String codigo)
	 *
	 * @param codigo
	 * @return
	 * @author Luiz Fernando
	 */
	public Codigotributacao findByCodigo(String codigo) {
		return codigotributacaoDAO.findByCodigo(codigo);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CodigotributacaoDAO#findForAutocompleteWithMunicipio(String s)
	 *
	 * @param s
	 * @return
	 * @since 25/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Codigotributacao> findForAutocompleteWithMunicipio(String s){
		return codigotributacaoDAO.findForAutocompleteWithMunicipio(s);
	}

}
