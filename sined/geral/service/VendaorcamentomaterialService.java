package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.dao.VendaorcamentomaterialDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaorcamentomaterialService extends GenericService<Vendaorcamentomaterial> {
	
	private VendaorcamentomaterialDAO vendaorcamentomaterialDAO;
	public void setVendaorcamentomaterialDAO(VendaorcamentomaterialDAO vendaorcamentomaterialDAO) {
		this.vendaorcamentomaterialDAO = vendaorcamentomaterialDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param vendaorcamento
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Vendaorcamentomaterial> findByMaterial (Vendaorcamento vendaorcamento){
		return vendaorcamentomaterialDAO.findByMaterial (vendaorcamento, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentomaterialDAO#findByMaterial(Vendaorcamento vendaorcamento, String orderBy)
	 *
	 * @param vendaorcamento
	 * @param orderBy
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public List<Vendaorcamentomaterial> findByMaterial (Vendaorcamento vendaorcamento, String orderBy){
		return vendaorcamentomaterialDAO.findByMaterial(vendaorcamento, orderBy);
	}
		
	/* singleton */
	private static VendaorcamentomaterialService instance;
	public static VendaorcamentomaterialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendaorcamentomaterialService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentomaterialDAO#findForOrdemProducao
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vendaorcamentomaterial> findForOrdemProducao(Vendaorcamento vendaorcamento) {
		return vendaorcamentomaterialDAO.findForOrdemProducao(vendaorcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentomaterialDAO#findForMontarGrade(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public List<Vendaorcamentomaterial> findForMontarGrade (String whereIn){
		return vendaorcamentomaterialDAO.findForMontarGrade (whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param vendaorcamento
	 * @author Tom�s Rabelo
	 */
	public void deleteAllFromVendaorcamento(Vendaorcamento vendaorcamento) {
		vendaorcamentomaterialDAO.deleteAllFromVendaorcamento(vendaorcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 *
	 * @param material,cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public Double getUltimoValorMaterialByCliente(Material material,Cliente cliente, Empresa empresa){
		List<Vendaorcamentomaterial> lista = vendaorcamentomaterialDAO.getUltimoValorMaterialByCliente(material, cliente, empresa);
		if(lista!=null && !lista.isEmpty()){
			for(Vendaorcamentomaterial vm:lista){
				if(vm.getMaterial()!=null && vm.getPreco()!=null)
					return vm.getPreco();
			}
		}
		return null;
	}
	
	public Vendaorcamentomaterial getUltimoValorDataMaterialByCliente(Material material,Cliente cliente, Empresa empresa){
		List<Vendaorcamentomaterial> lista = vendaorcamentomaterialDAO.getUltimoValorMaterialByCliente(material, cliente, empresa);
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendaorcamentomaterialDAO#findForComissaoRepresentante(Vendaorcamento vendaorcamento)
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendaorcamentomaterial> findForComissaoRepresentante(Vendaorcamento vendaorcamento) {
		return vendaorcamentomaterialDAO.findForComissaoRepresentante(vendaorcamento);
	}
	
	/**
	 * M�todo que identifica o fornecedor do material no or�amento
	 *
	 * @param listaVendamaterial
	 * @param fornecedor
	 * @return
	 * @author Jo�o Vitor
	 */
	public Boolean isFornecedorVendaorcamentomaterial(List<Vendaorcamentomaterial> listaVendaorcamentomaterial, Fornecedor fornecedor){
		Boolean exist = Boolean.FALSE;
		if(SinedUtil.isListNotEmpty(listaVendaorcamentomaterial)){
			for(Vendaorcamentomaterial vendamaterial : listaVendaorcamentomaterial){
				if(vendamaterial.getFornecedor() != null){
					if(vendamaterial.getFornecedor() != null && vendamaterial.getFornecedor().equals(fornecedor)){
						return Boolean.TRUE;
					}
				}
			}
		}
		return exist;
	}	
	
	public boolean haveVendaOrcamentoMaterial(Material material) {
		return vendaorcamentomaterialDAO.haveVendaOrcamentoMaterial(material);
	}
}
