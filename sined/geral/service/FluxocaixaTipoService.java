package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.FluxocaixaTipo;
import br.com.linkcom.sined.geral.dao.FluxocaixaTipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FluxocaixaTipoService extends GenericService<FluxocaixaTipo>{

	private FluxocaixaTipoDAO fluxocaixaTipoDAO;
	
	public void setFluxocaixaTipoDAO(FluxocaixaTipoDAO fluxocaixaTipoDAO) {
		this.fluxocaixaTipoDAO = fluxocaixaTipoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public FluxocaixaTipo getFluxoCaixaTipoOrdemCompra() {
		return fluxocaixaTipoDAO.getFluxoCaixaTipoOrdemCompra();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public FluxocaixaTipo getFluxoCaixaTipoFornecimentoContrato() {
		return fluxocaixaTipoDAO.getFluxoCaixaTipoFornecimentoContrato();
	}

}
