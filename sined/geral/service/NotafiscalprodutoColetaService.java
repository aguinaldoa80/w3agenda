package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.NotafiscalprodutoColeta;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoColetaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoColetaService extends GenericService<NotafiscalprodutoColeta> {
	
	private NotafiscalprodutoColetaDAO notafiscalprodutoColetaDAO;
	private ColetaService coletaService;
	
	public void setNotafiscalprodutoColetaDAO(
			NotafiscalprodutoColetaDAO notafiscalprodutoColetaDAO) {
		this.notafiscalprodutoColetaDAO = notafiscalprodutoColetaDAO;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}


	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/10/2015
	 */
	public boolean haveNotaDevolucaoNaoCanceladaByColeta(String whereInColeta, String whereInPedidovendamaterial) {
		return notafiscalprodutoColetaDAO.haveNotaDevolucaoNaoCanceladaByColeta(whereInColeta, whereInPedidovendamaterial);
	}
	
	public String getWhereInColeta(Notafiscalproduto nota) {
		if(StringUtils.isNotBlank(nota.getWhereInColeta())){
			return nota.getWhereInColeta();
		}else if(StringUtils.isNotBlank(nota.getWhereInPedidovenda())){
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(nota.getWhereInPedidovenda());
			if(listaColeta != null && !listaColeta.isEmpty()){				
				return CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ",");
			}
		}
		return notafiscalprodutoColetaDAO.getWhereInColeta(nota);
	}
	
	public String getWhereInNotaColeta(Notafiscalproduto nota, String whereInColeta) {
		return notafiscalprodutoColetaDAO.getWhereInNotaColeta(nota, whereInColeta);
	}
	
	public boolean haveNotaRetornoPedidovendaOrVenda(String whereInColeta) {
		return notafiscalprodutoColetaDAO.haveNotaRetornoPedidovendaOrVenda(whereInColeta);
	}
	
	public List<NotafiscalprodutoColeta> findNotaDevolucao(String whereInColeta, String whereInPedidovendamaterial, Boolean considerarSomenteDevolucao) {
		return notafiscalprodutoColetaDAO.findNotaDevolucao(whereInColeta, whereInPedidovendamaterial, considerarSomenteDevolucao);
	}
	
	public Boolean existsColetaInNotaFiscalProduto(Integer cdnotafiscalproduto) {
		return notafiscalprodutoColetaDAO.existsColetaInNotaFiscalProduto(cdnotafiscalproduto);
	}
}
