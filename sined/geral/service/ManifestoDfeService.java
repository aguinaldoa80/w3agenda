package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ManifestoDfeDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.ManifestoDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.LoteConsultaDfeItemStatusEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.ManifestoDfeSituacaoEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.TipoLoteConsultaDfeEnum;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

public class ManifestoDfeService extends GenericService<ManifestoDfe> {

	private ManifestoDfeDAO manifestoDfeDAO;
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaonfeService;
	private LoteConsultaDfeService loteConsultaDfeService;
	private ArquivoService arquivoService;
	private LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService;
	private ManifestoDfeHistoricoService manifestoDfeHistoricoService; 
	
	public void setManifestoDfeDAO(ManifestoDfeDAO manifestoDfeDAO) {
		this.manifestoDfeDAO = manifestoDfeDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setLoteConsultaDfeService(LoteConsultaDfeService loteConsultaDfeService) {
		this.loteConsultaDfeService = loteConsultaDfeService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setLoteConsultaDfeHistoricoService(LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService) {
		this.loteConsultaDfeHistoricoService = loteConsultaDfeHistoricoService;
	}
	public void setManifestoDfeHistoricoService(ManifestoDfeHistoricoService manifestoDfeHistoricoService) {
		this.manifestoDfeHistoricoService = manifestoDfeHistoricoService;
	}
	
	
	public ManifestoDfe procurarPorChaveAcesso(String chaveAcesso) {
		return manifestoDfeDAO.procurarPorChaveAcesso(chaveAcesso);
	}

	public void atualizarStatusEnum(ManifestoDfe manifestoDfe) {
		manifestoDfeDAO.atualizarStatusEnum(manifestoDfe);
	}

	public void atualizarArquivoXml(ManifestoDfe manifestoDfe) {
		manifestoDfeDAO.atualizarArquivoXml(manifestoDfe);
	}

	public Boolean existeManifestoDfeSituacaoEnumDiferenteDe(String selectedItens, List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum) {
		return manifestoDfeDAO.procurarManifestoDfeSituacaoEnumDiferenteDe(selectedItens, listaManifestoDfeSituacaoEnum).size() > 0;
	}

	public void atualizarArquivoNfeCompletoXml(ManifestoDfe manifestoDfe) {
		manifestoDfeDAO.atualizarArquivoNfeCompletoXml(manifestoDfe);
	}
	
	public Boolean existeMaisDeUmaEmpresaDosManifestoDfe(String selectedItens) {
		return manifestoDfeDAO.procurarEmpresasDosManifestoDfe(selectedItens).size() > 1;
	}

	public ManifestoDfe procurarEmpresaPorId(Integer cdManifestoDfe) {
		return manifestoDfeDAO.procurarEmpresaPorId(cdManifestoDfe);
	}

	public ManifestoDfe procurarPorId(Integer cdManifestoDfe) {
		return manifestoDfeDAO.procurarPorId(cdManifestoDfe);
	}

	public void atualizarManifestoDfeSituacaoEnum(ManifestoDfe manifestoDfe) {
		manifestoDfeDAO.atualizarManifestoDfeSituacaoEnum(manifestoDfe);
	}
	
	public Boolean existeMaisDeUmaEmpresaDosManifestoDfeEvento(String selectedItens) {
		return manifestoDfeDAO.procurarEmpresasDosManifestoDfe(selectedItens).size() > 1;
	}

	public List<ManifestoDfe> procurarStatusOuManifestoDfeSituacaoDiferenteDe(String selectedItens, LoteConsultaDfeItemStatusEnum statusEnum, 
			List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum) {
		return manifestoDfeDAO.procurarStatusOuManifestoDfeSituacaoDiferenteDe(selectedItens, statusEnum, listaManifestoDfeSituacaoEnum);
	}
	
	public ManifestoDfe procurarPorChaveAcessoImportacao(String chaveAcesso) {
		return manifestoDfeDAO.procurarPorChaveAcesso(chaveAcesso);
	}

	public String validarManifestoDfe(String selectedItens, String operacao) {
		if (StringUtils.isEmpty(operacao)) {
			return "Opera��o n�o selecionada.";
		}
		
		if (this.existeMaisDeUmaEmpresaDosManifestoDfeEvento(selectedItens)) {
			return "Selecione apenas regitros de Manifesta��o de DF-e de uma empresa.";
		}
		
		List<ManifestoDfeSituacaoEnum> listaManifestoDfeSituacaoEnum = new ArrayList<ManifestoDfeSituacaoEnum>();
		
		if ("210200".equals(operacao)) {
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.CIENCIA);
			
			List<ManifestoDfe> listaManifestoDfe = this.procurarStatusOuManifestoDfeSituacaoDiferenteDe(selectedItens, 
					LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA, listaManifestoDfeSituacaoEnum);
			
			if (listaManifestoDfe != null && listaManifestoDfe.size() > 0) {
				return "� permitido apenas realizar a confirma��o de opera��o de registros que n�o tiveram manifesta��o final realizada e que a situa��o da nfe seja igual a 'Autorizado'.<br>" +
						"Os registros " + CollectionsUtil.listAndConcatenate(listaManifestoDfe, "cdManifestoDfe", ", ") + " est�o com a situa��o igual h� 'Confirma��o realizada', 'Opera��o n�o Realizada' ou 'Desconhecimento realizado' e/ou " +
						"com a situa��o de nfe na situa��o diferente de 'Autorizado'.";
			}
		} else if ("210220".equals(operacao)) {
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.CIENCIA);
			
			List<ManifestoDfe> listaManifestoDfe = this.procurarStatusOuManifestoDfeSituacaoDiferenteDe(selectedItens, 
					LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA, listaManifestoDfeSituacaoEnum);
			
			if (listaManifestoDfe != null && listaManifestoDfe.size() > 0) {
				return "� permitido apenas realizar o desconhecimento da opera��o de registros que n�o tiveram manifesta��o realizada ou seja igual a 'Ci�ncia da Opera��o'.<br>" +
						"Os registros " + CollectionsUtil.listAndConcatenate(listaManifestoDfe, "cdManifestoDfe", ", ") + " tiveram algum registro de manifesto realizado ou a situa��o da nfe � diferente de 'Autorizado'.";
			}
		} else if ("210240".equals(operacao)) {
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
			
			List<ManifestoDfe> listaManifestoDfe = this.procurarStatusOuManifestoDfeSituacaoDiferenteDe(selectedItens, 
					LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA, listaManifestoDfeSituacaoEnum);
			
			if (listaManifestoDfe != null && listaManifestoDfe.size() > 0) {
				return "� permitido informar 'Opera��o n�o realizada' apenas para os registros que n�o tiveram a manifesta��o final realizada e que a situa��o da NF-e seja igual a 'Autorizado'.<br>" +
						"Os registros " + CollectionsUtil.listAndConcatenate(listaManifestoDfe, "cdManifestoDfe", ", ") + " tiveram algum registro de manifesto final realizado ou a situa��o da nfe � diferente de 'Autorizado'.";
			}
		} else if ("210210".equals(operacao)) {
			listaManifestoDfeSituacaoEnum.add(ManifestoDfeSituacaoEnum.SEM_MANIFESTO);
			
			List<ManifestoDfe> listaManifestoDfe = this.procurarStatusOuManifestoDfeSituacaoDiferenteDe(selectedItens, 
					LoteConsultaDfeItemStatusEnum.DFE_AUTORIZADA, listaManifestoDfeSituacaoEnum);
			
			if (listaManifestoDfe != null && listaManifestoDfe.size() > 0) {
				return "� permitido apenas realizar a ci�ncia da opera��o de registros que n�o tiveram manifesta��o realizada e cuja a situa��o da nota seja igual � 'Autorizado'.<br>" +
						"Os registros " + CollectionsUtil.listAndConcatenate(listaManifestoDfe, "cdManifestoDfe", ", ") + " tiveram algum registro de manifesto realizado ou a situa��o da nfe � diferente de 'Autorizado'.";
			}
		} 
		
		return null;
	}

	public void criarLoteConsultaDfe(Empresa empresa, Configuracaonfe configuracaonfe, String whereIn, boolean automatico) {
		Empresa empresaLote = empresaService.carregaEmpresa(empresa);
		Configuracaonfe configuracaonfeLote = configuracaonfeService.carregaConfiguracao(configuracaonfe);
		
		String[] ids = whereIn.split(",");
		
		for (String id : ids) {
			ManifestoDfe manifestoDfe = this.procurarPorId(Integer.parseInt(id));
			LoteConsultaDfe loteConsultaDfe = new LoteConsultaDfe(manifestoDfe.getChaveAcesso(), TipoLoteConsultaDfeEnum.CHAVE_ACESSO);
			
			String xml = loteConsultaDfeService.criarXmlLoteConsultaDfe(loteConsultaDfe, empresaLote,configuracaonfeLote);
			
			Arquivo arquivoXml = new Arquivo(xml.getBytes(), "loteConsultaDfe_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
			
			loteConsultaDfe.setManifestoDfe(manifestoDfe);
			loteConsultaDfe.setArquivoXml(arquivoXml);
			loteConsultaDfe.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
			loteConsultaDfe.setEmpresa(empresaLote);
			loteConsultaDfe.setConfiguracaoNfe(configuracaonfeLote);
			loteConsultaDfe.setDhEnvio(SinedDateUtils.currentTimestamp());
			
			arquivoService.saveFile(loteConsultaDfe, "arquivoXml");
			arquivoService.saveOrUpdate(arquivoXml);
			
			loteConsultaDfeService.saveOrUpdate(loteConsultaDfe);
			
			LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
			
			Usuario usuario = SinedUtil.getUsuarioLogado();
			loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
			loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
			loteConsultaDfeHistorico.setObservacao("Gerada a consulta de NF-e completa do Manifesto DF-e " +
					"<a href=\"javascript:irParaRegistroManifestoDfe(" + manifestoDfe.getCdManifestoDfe() + ");\">" + manifestoDfe.getCdManifestoDfe() + "</a>" +
					(automatico ? " (Registro criado automaticamente ap�s a ci�ncia da opera��o)." : "."));
			loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
			
			loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
			
			ManifestoDfeHistorico manifestoDfeHistorico = new ManifestoDfeHistorico();
            manifestoDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
            manifestoDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
			manifestoDfeHistorico.setManifestoDfe(manifestoDfe);
			manifestoDfeHistorico.setObservacao("Gerada a consulta da NF-e completa.<br>Lote Consulta DF-e: " +
					"<a href=\"javascript:irParaLoteConsultaDfe(" + loteConsultaDfe.getCdLoteConsultaDfe() + ");\">" + loteConsultaDfe.getCdLoteConsultaDfe() + "</a>" +
					(automatico ? " (Registro criado automaticamente ap�s a ci�ncia da opera��o)." : "."));
			manifestoDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
			
			manifestoDfeHistoricoService.saveOrUpdate(manifestoDfeHistorico);
		}
		
	}
	
	public ModelAndView downloadDfeXmlCompleto(WebRequestContext request, String whereIn) throws IOException {
		List<ManifestoDfe> listaManifestoDfe = findForDownloadXmlCompleto(whereIn);
		
		if(SinedUtil.isListEmpty(listaManifestoDfe)){
			throw new SinedException("Nenhum documento fiscal encontrado com o xml completo.");
		}
		
		if(listaManifestoDfe.size() == 1){
			ManifestoDfe manifestoDfe = listaManifestoDfe.get(0);
			Arquivo arquivo = arquivoService.loadWithContents(manifestoDfe.getArquivoNfeCompletoXml()); 
			
			Resource resource = new Resource("text/xml", arquivo.getName(), arquivo.getContent());
			return new ResourceModelAndView(resource);
			
		} else {
			Integer contador = 1; 
			List<String> listaNomeArquivo = new ArrayList<String>();
			List<Arquivo> listaXML = new ArrayList<Arquivo>();
			for (ManifestoDfe manifestoDfe : listaManifestoDfe) {
				if(manifestoDfe.getArquivoNfeCompletoXml() != null){
					Arquivo arquivoXML = arquivoService.loadWithContents(manifestoDfe.getArquivoNfeCompletoXml()); 
					if(arquivoXML != null){
						if(arquivoXML.getName() != null && arquivoXML.getName().contains("null")){
							arquivoXML.setName(arquivoXML.getName().replace("null", "_sem_chave_acesso_"+contador));
							contador++;
						}else if(listaNomeArquivo.contains(arquivoXML.getName())){
							arquivoXML.setName(arquivoXML.getName().replace(arquivoXML.getName().substring(0, arquivoXML.getName().indexOf(".xml")), arquivoXML.getName().substring(0, arquivoXML.getName().indexOf(".xml"))+"_"+contador));
							contador++;
						}else {
							listaNomeArquivo.add(arquivoXML.getName());
						}
						listaXML.add(arquivoXML);
					}
				}
			}
			
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			if(listaXML != null && !listaXML.isEmpty()){				
				ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
				ZipEntry zipEntry;
				
				for (Arquivo arquivo : listaXML) {
					if(arquivo != null){
						zipEntry = new ZipEntry(arquivo.getName());
						zipEntry.setSize(arquivo.getSize());
						zip.putNextEntry(zipEntry);
						zip.write(arquivo.getContent());
					}
				}
				
				zip.closeEntry();
				zip.close();
			}	
			
			Resource resource = new Resource("application/zip", "dfe_" + SinedUtil.datePatternForReport() + ".zip", byteArrayOutputStream.toByteArray());
			return new ResourceModelAndView(resource);
		}
	}
	
	private List<ManifestoDfe> findForDownloadXmlCompleto(String whereIn) {
		return manifestoDfeDAO.findForDownloadXmlCompleto(whereIn);
	}
}