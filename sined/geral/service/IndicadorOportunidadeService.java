package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.dao.IndicadorOportunidadeDAO;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorOportunidadeMes;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorOportunidadeSemanas;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.IndicadorortunidadeBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.SituacaoOportunidade;
import br.com.linkcom.sined.modulo.crm.controller.report.filter.IndicadoroportunidadeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class IndicadorOportunidadeService extends GenericService<IndicadorortunidadeBean>{
	
	protected IndicadorOportunidadeDAO indicadorOportunidadeDAO;
	
	public void setIndicadorOportunidadeDAO(
			IndicadorOportunidadeDAO indicadorOportunidadeDAO) {
		this.indicadorOportunidadeDAO = indicadorOportunidadeDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 10/02/2012
	 * @param filtro
	 * @return
	 */
	public List<IndicadorortunidadeBean> carregarIndicadoresOportunidadeTrue(IndicadoroportunidadeFiltro filtro){
		return indicadorOportunidadeDAO.carregarIndicadoresOportunidadeTrue(filtro);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param filtro
	 * @return
	 */
	public List<IndicadorortunidadeBean> carregarIndicadoresOportunidade(IndicadoroportunidadeFiltro filtro){
		return indicadorOportunidadeDAO.carregarIndicadoresOportunidade(filtro);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param whereIn
	 * @return
	 */
	public List<IndicadorortunidadeBean> carregarListaIndicadoresPorCdOportunidadeHistorico(String whereIn){
		return indicadorOportunidadeDAO.carregarListaIndicadoresPorCdOportunidadeHistorico(whereIn);
	}
	
	/**
	 * 
	 * M�todo para carregar e ordenar toda a listagem de Indicadores de Oportunidade aonde as situa��es alteradas s�o = true.
	 *
	 *@author Thiago Augusto
	 *@date 13/02/2012
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<SituacaoOportunidade> setListagemInfoTrue(WebRequestContext request, IndicadoroportunidadeFiltro filtro) throws Exception {
		
		List<IndicadorortunidadeBean> listagemResult = carregarIndicadoresOportunidadeTrue(filtro);
		
		String statusAnterior = null;
		int semanaAtual = 0;
		List<SituacaoOportunidade> listaSituacaoOportunidade = new ArrayList<SituacaoOportunidade>();
		List<IndicadorOportunidadeSemanas> listaSemanas = new ArrayList<IndicadorOportunidadeSemanas>();
		SituacaoOportunidade situacaoOportunidade;
		IndicadorOportunidadeSemanas semanas = new IndicadorOportunidadeSemanas();
		int interacoes = 1;
		int contador = 0;
		String descricao = "";
		int total = 0;
		
		for (IndicadorortunidadeBean io : listagemResult) {
			contador ++;
			if (statusAnterior == null){
				semanas.setNumeroInteracoes(interacoes);
				semanas.setNumeroSemana(getSemana(io.getData()));
				semanas.setWhereInCdOportunidadeHistorico(io.getCdOportunidadeHistorico().toString());
				semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
				semanaAtual = semanas.getNumeroSemana();
				statusAnterior = io.getStatus();
				descricao = io.getStatus();
				
				if(contador == listagemResult.size()){
					listaSemanas.add(semanas);
					situacaoOportunidade = new SituacaoOportunidade();
					situacaoOportunidade.setDescricao(descricao);
					total = 0;
					for (IndicadorOportunidadeSemanas s : listaSemanas) {
						total += s.getNumeroInteracoes();
						situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
								situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
					}
					situacaoOportunidade.setTotal(total);
					situacaoOportunidade.setListaSemanas(listaSemanas);
					situacaoOportunidade.setListaMes(getListaMeses(filtro));
					listaSituacaoOportunidade.add(situacaoOportunidade);
				}
			}
			else {
				if (statusAnterior.equals(io.getStatus())){
					if (semanaAtual == getSemana(io.getData())){
						semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
						semanas.setNumeroSemana(getSemana(io.getData()));
						semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
						semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
						semanaAtual = semanas.getNumeroSemana();
						if(contador == listagemResult.size()){
							listaSemanas.add(semanas);
							situacaoOportunidade = new SituacaoOportunidade();
							situacaoOportunidade.setDescricao(descricao);
							total = 0;
							for (IndicadorOportunidadeSemanas s : listaSemanas) {
								total += s.getNumeroInteracoes();
								situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
										situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
							}
							situacaoOportunidade.setTotal(total);
							situacaoOportunidade.setListaSemanas(listaSemanas);
							situacaoOportunidade.setListaMes(getListaMeses(filtro));
							listaSituacaoOportunidade.add(situacaoOportunidade);
						}
					} else {
						descricao = io.getStatus();
						listaSemanas.add(semanas);
						semanas = new IndicadorOportunidadeSemanas();
						semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
						semanas.setNumeroSemana(getSemana(io.getData()));
						semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
						semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
						semanaAtual = semanas.getNumeroSemana();
						if(contador == listagemResult.size()){
							listaSemanas.add(semanas);
							situacaoOportunidade = new SituacaoOportunidade();
							situacaoOportunidade.setDescricao(descricao);
							total = 0;
							for (IndicadorOportunidadeSemanas s : listaSemanas) {
								total += s.getNumeroInteracoes();
								situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
										situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
							}
							situacaoOportunidade.setTotal(total);
							situacaoOportunidade.setListaSemanas(listaSemanas);
							situacaoOportunidade.setListaMes(getListaMeses(filtro));
							listaSituacaoOportunidade.add(situacaoOportunidade);
						}
					}
				} else {
					listaSemanas.add(semanas);
					situacaoOportunidade = new SituacaoOportunidade();
					situacaoOportunidade.setDescricao(descricao);
					total = 0;
					for (IndicadorOportunidadeSemanas s : listaSemanas) {
						total += s.getNumeroInteracoes();
						situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
								situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
					}
					situacaoOportunidade.setTotal(total);
					situacaoOportunidade.setListaSemanas(listaSemanas);
					situacaoOportunidade.setListaMes(getListaMeses(filtro));
					listaSituacaoOportunidade.add(situacaoOportunidade);
					listaSemanas = new ArrayList<IndicadorOportunidadeSemanas>();
					semanas = new IndicadorOportunidadeSemanas();
					semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
					semanas.setNumeroSemana(getSemana(io.getData()));
					semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
					semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
					semanaAtual = semanas.getNumeroSemana();
					descricao = io.getStatus();
					statusAnterior = io.getStatus();
					if(contador == listagemResult.size()){
						listaSemanas.add(semanas);
						situacaoOportunidade = new SituacaoOportunidade();
						situacaoOportunidade.setDescricao(descricao);
						total = 0;
						for (IndicadorOportunidadeSemanas s : listaSemanas) {
							total += s.getNumeroInteracoes();
							situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
									situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
						}
						situacaoOportunidade.setTotal(total);
						situacaoOportunidade.setListaSemanas(listaSemanas);
						situacaoOportunidade.setListaMes(getListaMeses(filtro));
						listaSituacaoOportunidade.add(situacaoOportunidade);
					}
				}
			}
		}
		return listaSituacaoOportunidade;
	}
	
	/**
	 * 
	 * M�todo que retorna o n�mero da semana dentro de cada m�s.
	 *
	 *@author Thiago Augusto
	 *@date 13/02/2012
	 * @param date
	 * @return
	 */
	public Integer getSemana(Date date){
		Calendar retorno = Calendar.getInstance();
		retorno.setTime(date);
		return retorno.get(Calendar.WEEK_OF_MONTH);
	}
	
	/**
	 * 
	 * M�todo para retornor a string do m�s.
	 *
	 *@author Thiago Augusto
	 *@date 14/02/2012
	 * @param date
	 * @return
	 */
	public String getMesString(String date){
		String [] stringMes = date.split("-");
		int mes = Integer.parseInt(stringMes[1]);
		String retorno = "";
		switch (mes) {
		case 1:
			retorno = "JANEIRO";
			break;
		case 2:
			retorno = "FEVEREIRO";
			break;
		case 3:
			retorno = "MAR�O";
			break;
		case 4:
			retorno = "ABRIL";
			break;
		case 5:
			retorno = "MAIO";
			break;
		case 6:
			retorno = "JUNHO";
			break;
		case 7:
			retorno = "JULHO";
			break;
		case 8:
			retorno = "AGOSTO";
			break;
		case 9:
			retorno = "SETEMBRO";
			break;
		case 10:
			retorno = "OUTUBRO";
			break;
		case 11:
			retorno = "NOVEMBRO";
			break;
		case 12:
			retorno = "DEZEMBRO";
			break;
		}
		return retorno;
	}
	
	/**
	 * 
	 * M�todo que retorna a lista de meses.
	 *
	 *@author Thiago Augusto
	 *@date 14/02/2012
	 * @param filtro
	 * @return
	 */
	public List<IndicadorOportunidadeMes> getListaMeses(IndicadoroportunidadeFiltro filtro){
		int diferenca = getDiferencaMeses(filtro.getDtPeriodoDe(),filtro.getDtPeriodoAte());
		List<IndicadorOportunidadeMes> retorno = new ArrayList<IndicadorOportunidadeMes>();
		Calendar calendar = Calendar.getInstance();
		Date data = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i <= diferenca; i++) {
			calendar.setTime(filtro.getDtref1());
			if(i > 0)
				calendar.add(Calendar.MONTH, i);
			data = calendar.getTime();
			IndicadorOportunidadeMes mes = new IndicadorOportunidadeMes();
			mes.setMesAnoCorrente(getMesString(format.format(data)) + "/"+ getAno(format.format(data)));
			retorno.add(mes);
		}
		return retorno;
		
	}
	
	/**
	 * 
	 * M�todo para pegar a diferen�a de meses dentro das datas
	 *
	 *@author Thiago Augusto
	 *@date 14/02/2012
	 * @param primeiraData
	 * @param segundaData
	 * @return
	 */
	public Integer getDiferencaMeses(String primeiraData, String segundaData){
		String [] primeiraDataString = primeiraData.split("/");
		String [] segundaDataString = segundaData.split("/");
		int primeiroMes = Integer.parseInt(primeiraDataString[0]);
		int segundoMes = Integer.parseInt(segundaDataString[0]);
		int primeiroAno = Integer.parseInt(primeiraDataString[1]);
		int segundoAno = Integer.parseInt(segundaDataString[1]);
		int retorno = 0;
		for (int ano = 0; primeiroAno <= segundoAno; ano++) {
			if (primeiroMes == 13){
				primeiroMes = 1;
				primeiroAno++;
			} 
			if (primeiroMes == segundoMes && primeiroAno == segundoAno)
				break;
			retorno++;
			primeiroMes++;
		}
		return retorno;
	}
	
	/**
	 * 
	 * M�todo para carregar e ordenar toda a listagem de Indicadores de Oportunidade.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 */
	public List<SituacaoOportunidade> setListagemInfo(WebRequestContext request, IndicadoroportunidadeFiltro filtro, List<SituacaoOportunidade> listaSituacaoOportunidade) throws Exception {
		
		List<IndicadorortunidadeBean> listagemResult = carregarIndicadoresOportunidade(filtro);
		
		String statusAnterior = null;
		int semanaAtual = 0;
		List<IndicadorOportunidadeSemanas> listaSemanas = new ArrayList<IndicadorOportunidadeSemanas>();
		SituacaoOportunidade situacaoOportunidade;
		IndicadorOportunidadeSemanas semanas = new IndicadorOportunidadeSemanas();
		int interacoes = 1;
		int contador = 0;
		String descricao = "";
		
		for (IndicadorortunidadeBean io : listagemResult) {
			contador ++;
			if (statusAnterior == null){
				semanas.setNumeroInteracoes(interacoes);
				semanas.setNumeroSemana(getSemana(io.getData()));
				semanas.setWhereInCdOportunidadeHistorico(io.getCdOportunidadeHistorico().toString());
				semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
				semanaAtual = semanas.getNumeroSemana();
				statusAnterior = io.getStatus();
				descricao = "Intera��o em " + io.getStatus().replace("Em", "");
			}
			else {
				if (statusAnterior.equals(io.getStatus())){
					if (semanaAtual == getSemana(io.getData())){
						semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
						semanas.setNumeroSemana(getSemana(io.getData()));
						semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
						semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
						semanaAtual = semanas.getNumeroSemana();
					} else {
						descricao = "Intera��o em " + io.getStatus().replace("Em", "");
						listaSemanas.add(semanas);
						semanas = new IndicadorOportunidadeSemanas();
						semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
						semanas.setNumeroSemana(getSemana(io.getData()));
						semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
						semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
						semanaAtual = semanas.getNumeroSemana();
						if(contador == listagemResult.size()){
							listaSemanas.add(semanas);
							situacaoOportunidade = new SituacaoOportunidade();
							situacaoOportunidade.setDescricao(descricao);
							for (IndicadorOportunidadeSemanas s : listaSemanas) {
								situacaoOportunidade.setTotal(situacaoOportunidade.getTotal() != null ? situacaoOportunidade.getTotal() + s.getNumeroInteracoes() : s.getNumeroInteracoes());
								situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
										situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
							}
							situacaoOportunidade.setListaSemanas(listaSemanas);
							situacaoOportunidade.setListaMes(getListaMeses(filtro));
							listaSituacaoOportunidade.add(situacaoOportunidade);
						}
					}
				} else {
					listaSemanas.add(semanas);
					situacaoOportunidade = new SituacaoOportunidade();
					situacaoOportunidade.setDescricao(descricao);
					for (IndicadorOportunidadeSemanas s : listaSemanas) {
						situacaoOportunidade.setTotal(situacaoOportunidade.getTotal() != null ? situacaoOportunidade.getTotal() + s.getNumeroInteracoes() : s.getNumeroInteracoes());
						situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
								situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
					}
					situacaoOportunidade.setListaSemanas(listaSemanas);
					situacaoOportunidade.setListaMes(getListaMeses(filtro));
					listaSituacaoOportunidade.add(situacaoOportunidade);
					listaSemanas = new ArrayList<IndicadorOportunidadeSemanas>();
					semanas = new IndicadorOportunidadeSemanas();
					semanas.setNumeroInteracoes((semanas.getNumeroInteracoes() != null ? semanas.getNumeroInteracoes() : 0) + interacoes);
					semanas.setNumeroSemana(getSemana(io.getData()));
					semanas.setWhereInCdOportunidadeHistorico(semanas.getWhereInCdOportunidadeHistorico() != null ? semanas.getWhereInCdOportunidadeHistorico() + "," + io.getCdOportunidadeHistorico() : io.getCdOportunidadeHistorico().toString());
					semanas.setMesCorrente(getMesString(io.getData().toString())+"/"+getAnoCorrente(io.getData()));
					semanaAtual = semanas.getNumeroSemana();
					descricao = "Intera��o em " + io.getStatus().replace("Em", "");
					statusAnterior = io.getStatus();
					if(contador == listagemResult.size()){
						listaSemanas.add(semanas);
						situacaoOportunidade = new SituacaoOportunidade();
						situacaoOportunidade.setDescricao(descricao);
						for (IndicadorOportunidadeSemanas s : listaSemanas) {
							situacaoOportunidade.setTotal(situacaoOportunidade.getTotal() != null ? situacaoOportunidade.getTotal() + s.getNumeroInteracoes() : s.getNumeroInteracoes());
							situacaoOportunidade.setWhereInCdOportunidadeHistorico(situacaoOportunidade.getWhereInCdOportunidadeHistorico() != null ? 
									situacaoOportunidade.getWhereInCdOportunidadeHistorico() + "," + s.getWhereInCdOportunidadeHistorico() : s.getWhereInCdOportunidadeHistorico());
						}
						situacaoOportunidade.setListaSemanas(listaSemanas);
						situacaoOportunidade.setListaMes(getListaMeses(filtro));
						listaSituacaoOportunidade.add(situacaoOportunidade);
					}
				}
			}
		}
		return listaSituacaoOportunidade;
	}
	
	
	/**
	 * 
	 * M�todo para pegar o ano corrente
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param data
	 * @return
	 */
	public String getAnoCorrente(Date data){
		String [] retorno = data.toString().split("-");
		return retorno[0];
	}
	
	public String getAno(String ano){
		String [] retorno = ano.split("-");
		return retorno[0];
	}
	
	/**
	 * 
	 * M�todo para verificar a diferen�a de dias entre as duas datas do bean.
	 *
	 *@author Thiago Augusto
	 *@date 15/02/2012
	 * @param bean
	 * @return
	 */
	public int getDiferencaDias(IndicadorortunidadeBean bean){
		return SinedDateUtils.diferencaDias(bean.getData(), bean.getInicio());
	}
	
	/**
	 * 
	 * M�todo para formatar a data para a listagem detalhada.
	 *
	 *@author Thiago Augusto
	 *@date 16/02/2012
	 * @param data
	 * @return
	 */
	public String formataDataEvento(Date data){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return format.format(data);
	}
	
	/**
	 * 
	 * M�todo para gerar o CSV da listagem
	 *
	 *@author Thiago Augusto
	 *@date 16/02/2012
	 * @param filtro
	 * @return
	 */
	public Resource prepararArquivoIndicadorOportunidadeCSV(WebRequestContext request, IndicadoroportunidadeFiltro filtro){
		List<SituacaoOportunidade> listaSituacaoOportunidade;
		StringBuilder csvCabecalho = new StringBuilder();
		StringBuilder csvLinhas = new StringBuilder();
		try {
			listaSituacaoOportunidade = setListagemInfoTrue(request, filtro);
			listaSituacaoOportunidade = setListagemInfo(request, filtro, listaSituacaoOportunidade);
			listaSituacaoOportunidade = getlistaCompletaMes(listaSituacaoOportunidade);
			List<IndicadorOportunidadeMes> listaMes = getListaMeses(filtro);
			
			csvCabecalho.append("\"Indicador\";");
			for (IndicadorOportunidadeMes mes : listaMes) {
				csvCabecalho.append("\"" + mes.getMesAnoCorrente());
				csvCabecalho.append("\";");
				csvCabecalho.append("\"\";");
				csvCabecalho.append("\"\";");
				csvCabecalho.append("\"\";");
				csvCabecalho.append("\"\";");
				csvCabecalho.append("\"\";");
				csvCabecalho.append("\"\";");
			}
			csvCabecalho.append("\"Total\";");
			csvCabecalho.append("\n");
			csvCabecalho.append("\"Semanas\";");
			for (@SuppressWarnings("unused") IndicadorOportunidadeMes mes : listaMes) {
				csvCabecalho.append("\"1\";");
				csvCabecalho.append("\"2\";");
				csvCabecalho.append("\"3\";");
				csvCabecalho.append("\"4\";");
				csvCabecalho.append("\"5\";");
				csvCabecalho.append("\"6\";");
				csvCabecalho.append("\"Total\";");
			}
			csvCabecalho.append("\";");
			csvCabecalho.append("\n");
			
			for (SituacaoOportunidade so : listaSituacaoOportunidade) {
				csvLinhas.append("\"" + so.getDescricao());
				csvLinhas.append("\";");
				for (IndicadorOportunidadeMes mes : listaMes) {
					for (int contadorSemana = 1; contadorSemana <= 6; contadorSemana++) {
						boolean existeSemana = false;
						for (IndicadorOportunidadeSemanas ios : so.getListaSemanas()) {
							if (mes.getMesAnoCorrente().equals(ios.getMesCorrente()) && contadorSemana == ios.getNumeroSemana()){
								csvLinhas.append("\"" + ios.getNumeroInteracoes());
								csvLinhas.append("\";");
								existeSemana = true;
							} 
						}
						if(!existeSemana) {
							csvLinhas.append("\"0\";");
						}
					}
					for (IndicadorOportunidadeMes iom : so.getListaMes()) {
						if (iom.getMesAnoCorrente().equals(mes.getMesAnoCorrente()))
							csvLinhas.append("\"" + iom.getTotal());
					}
					csvLinhas.append("\";");
				}
				csvLinhas.append("\"" + so.getTotal());
				csvLinhas.append("\";");
				csvLinhas.append("\n");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Resource resource = new Resource("text/csv", "indicadorOportunidade_" + SinedUtil.datePatternForReport() + ".csv", csvCabecalho.append(csvLinhas).toString().getBytes());
		return resource;
	}

	/**
	 * 
	 * M�todo que recebe uma lista com todas as situa��es e completa o total por m�s e seta o cdOportunidadeHistorico na lista
	 *
	 *@author Thiago Augusto
	 *@date 16/02/2012
	 * @return
	 */
	public List<SituacaoOportunidade> getlistaCompletaMes(List<SituacaoOportunidade> listaBean){
		for (SituacaoOportunidade so : listaBean) {
			for (IndicadorOportunidadeMes iom : so.getListaMes()) {
				int total = 0;
				String whereInCdOportunidadeHistorico = "";
				int contador = 1;
				List<String> listaCds = new ArrayList<String>();
				for (IndicadorOportunidadeSemanas ios : so.getListaSemanas()) {
					if (iom.getMesAnoCorrente().equals(ios.getMesCorrente())){
						total += ios.getNumeroInteracoes();
						listaCds.add(ios.getWhereInCdOportunidadeHistorico());
					}
					contador++;
				}
;				whereInCdOportunidadeHistorico = CollectionsUtil.concatenate(listaCds, ",");
				iom.setTotal(total);
				iom.setWhereInCdOportunidadeHistorico(whereInCdOportunidadeHistorico);
			}
		}
		return listaBean;
	}
}
