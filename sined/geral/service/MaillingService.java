package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.view.Vwemailcontatocliente;
import br.com.linkcom.sined.geral.dao.MaillingDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.thread.EmailManagerThread;

public class MaillingService extends GenericService<Vwemailcontatocliente> {
	
	private MaillingDAO maillingDAO;
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	
	public void setMaillingDAO(MaillingDAO maillingDAO) {
		this.maillingDAO = maillingDAO;
	}
	
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}
	
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	@Override
	public void saveOrUpdate(Vwemailcontatocliente bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	public void delete(Vwemailcontatocliente bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Vwemailcontatocliente> findClientesContatosByWhereIn(String whereIn) {
		return maillingDAO.findClientesContatosByWhereIn(whereIn);
	}

	/**
	 * M�todo que envia mailling para o cliente
	 * 
	 * @param listaClientesContatos
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 * @param request 
	 */
	public void enviaEmail(List<Vwemailcontatocliente> listaClientesContatos, MaillingFiltro filtro, WebRequestContext request) {
		
		Envioemail envioemail = envioemailService.registrarEnvio(filtro.getEmail(), filtro.getAssunto(), filtro.getMensagem(), Envioemailtipo.MAILING_CLIENTE);
		
		StringBuilder error = new StringBuilder();
		for (Vwemailcontatocliente vwemailcontatocliente : listaClientesContatos) {
			if(vwemailcontatocliente.getEmailcontatocliente() != null && !vwemailcontatocliente.getEmailcontatocliente().equals("")){
				try {
					String assunto = filtro.getAssunto();
					if(vwemailcontatocliente.getCliente() != null && vwemailcontatocliente.getCliente().getNome() != null && 
						!"".equals(vwemailcontatocliente.getCliente().getNome())){
						assunto += " - " + vwemailcontatocliente.getCliente().getNome();
					}
					EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
									.setFrom(filtro.getEmail())
									.setTo(vwemailcontatocliente.getEmailcontatocliente())
									.setSubject(assunto);

					//anexar arquivo
					if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
						try {
							email.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
						} catch (Exception e) {
							throw new SinedException("Erro ao anexar arquivo para "+vwemailcontatocliente.getEmailcontatocliente());
						}						
					}
					
					String strLoginSenha = "";
					if(filtro.getEnviaUsuarioSenha()){
						if(vwemailcontatocliente.getCliente() != null && vwemailcontatocliente.getCliente().getLogin() != null && !vwemailcontatocliente.getCliente().getLogin().equals("") && 
						   vwemailcontatocliente.getCliente().getSenha() != null && !vwemailcontatocliente.getCliente().getSenha().equals("")){
							strLoginSenha = "<BR><BR>Login: " + vwemailcontatocliente.getCliente().getLogin() + "<BR>" + "Senha: " + vwemailcontatocliente.getCliente().getSenha();
						} else {
							error.append(vwemailcontatocliente.getEmailcontatocliente()+", ");
						}
					}
					
					Envioemailitem envioemailitem = new Envioemailitem();
					envioemailitem.setEnvioemail(envioemail);
					envioemailitem.setEmail(vwemailcontatocliente.getEmailcontatocliente());
					envioemailitem.setNomecontato(vwemailcontatocliente.getContatonome());
					envioemailitem.setPessoa(vwemailcontatocliente.getCliente());
					envioemailitemService.saveOrUpdate(envioemailitem);
					email.setEmailItemId(envioemailitem.getCdenvioemailitem());
					
					String htmlConfirmacaoEmail = EmailUtil.getHtmlConfirmacaoEmail(envioemail, vwemailcontatocliente.getEmailcontatocliente());
					
					email
						.addHtmlText(filtro.getMensagem() + strLoginSenha + htmlConfirmacaoEmail);
					
					new EmailManagerThread(email, SinedUtil.getSubdominioCliente()).start();
				} catch (Exception e) {
					e.printStackTrace();
					throw new SinedException("N�o foi poss�vel enviar o mailling para "+vwemailcontatocliente.getEmailcontatocliente());
				}
			} else {
				throw new SinedException("N�o foram enviados o mailling para o contato " + vwemailcontatocliente.getEmailcontatocliente() + " pois n�o possui e-mail cadastrado.");
			}
		}
		
		//enviar c�pia do email para o usu�rio logado atualmente
		if(filtro.getEnviaCopia() != null && filtro.getEnviaCopia()){
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
											.setFrom(SinedUtil.getUsuarioLogado().getEmail())
											.setTo(SinedUtil.getUsuarioLogado().getEmail())
											.setSubject(filtro.getAssunto());
				
				//anexar arquivo
				if(filtro.getAnexo() != null && filtro.getAnexo().getContent() != null && filtro.getAnexo().getName() != null && filtro.getAnexo().getContenttype() != null ){
					try {
						email.attachFileUsingByteArray(filtro.getAnexo().getContent(), filtro.getAnexo().getName(), filtro.getAnexo().getContenttype(), "");
					} catch (Exception e) {
						throw new SinedException("Erro ao anexar arquivo para "+SinedUtil.getUsuarioLogado().getEmail());
					}						
				}
				
				email.addHtmlText(filtro.getMensagem());
				new EmailManagerThread(email, SinedUtil.getSubdominioCliente()).start();
			} catch (Exception e) {
				e.printStackTrace();
				throw new SinedException("N�o foi poss�vel enviar para voc� a c�pia do mailling ");
			}
		}
		if(error != null && !error.toString().equals(""))
			request.addError("Login e senha n�o enviado(s) para " + error.substring(0, error.length()-2).toString() + " pois n�o possui(em) login e senha cadastrado.");
		
		request.addMessage("Mailling enviado com sucesso.");
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param listaClientesContatos
	 * @return
	 * @author Igor Silv�rio Costa
	 */
	public void salvarHistoricosCliente(List<Vwemailcontatocliente> listaClientesContatos, String assunto) {
		maillingDAO.salvarHistoricosCliente(listaClientesContatos, assunto);
		
	}
}
