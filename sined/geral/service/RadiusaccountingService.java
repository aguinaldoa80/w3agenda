package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Radiusaccounting;
import br.com.linkcom.sined.geral.dao.RadiusaccountingDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RadiusaccountingService extends GenericService<Radiusaccounting >{	

	private RadiusaccountingDAO radiusaccountingDAO;
	public void setRadiusaccountingDAO(RadiusaccountingDAO radiusaccountingDAO) {
		this.radiusaccountingDAO = radiusaccountingDAO;
	}
	
	/**
	 * Preenche a data de fim da sess�o dos registros passados como par�metro
	 * @param lista
	 * @author Thiago Gon�alves
	 */
	public void confirmartempo(String lista){
		radiusaccountingDAO.confirmartempo(lista);
	}
	
}
