package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.ColaboradorOcorrencia;
import br.com.linkcom.sined.geral.dao.ColaboradorOcorrenciaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorOcorrenciaService extends GenericService<ColaboradorOcorrencia> {

	private ColaboradorOcorrenciaDAO colaboradorOcorrenciaDAO;
	
	public void setColaboradorOcorrenciaDAO(ColaboradorOcorrenciaDAO colaboradorOcorrenciaDAO) {
		this.colaboradorOcorrenciaDAO = colaboradorOcorrenciaDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ColaboradorOcorrenciaDAO#findByColaborador(Colaborador colaborador)
	*
	* @param colaborador
	* @return
	* @since 22/10/2014
	* @author Luiz Fernando
	*/
	public List<ColaboradorOcorrencia> findByColaborador(Colaborador colaborador) {
		return colaboradorOcorrenciaDAO.findByColaborador(colaborador);
	}

}
