package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Oportunidadefonte;


public class OportunidadefonteService extends GenericService<Oportunidadefonte> {

	private static OportunidadefonteService instance;
	public static OportunidadefonteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(OportunidadefonteService.class);
		}
		return instance;
	}
	
}
