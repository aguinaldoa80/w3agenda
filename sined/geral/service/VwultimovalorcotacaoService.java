package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.view.Vwultimovalorcotacao;
import br.com.linkcom.sined.geral.dao.VwultimovalorcotacaoDAO;

public class VwultimovalorcotacaoService extends GenericService<Vwultimovalorcotacao> {

	private VwultimovalorcotacaoDAO vwultimovalorcotacaoDAO;
	
	public void setVwultimovalorcotacaoDAO(
			VwultimovalorcotacaoDAO vwultimovalorcotacaoDAO) {
		this.vwultimovalorcotacaoDAO = vwultimovalorcotacaoDAO;
	}
	
	public List<Vwultimovalorcotacao> findUltimosValores(Fornecedor fornecedor,	String whereInMaterial) {
		return vwultimovalorcotacaoDAO.findUltimosValores(fornecedor, whereInMaterial);
	}

}
