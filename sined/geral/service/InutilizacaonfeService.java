package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.jdom.Element;
import org.jdom.JDOMException;

import br.com.linkcom.lknfe.xml.nfe.InfInut;
import br.com.linkcom.lknfe.xml.nfe.InutNFe;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.InutilizacaonfeFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.MarcarInutilizacaonfeBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.LkNfeUtil;
import br.com.linkcom.utils.StringUtils;

import com.ibm.icu.text.SimpleDateFormat;

public class InutilizacaonfeService extends GenericService<Inutilizacaonfe> {

	private InutilizacaonfeDAO inutilizacaonfeDAO;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setInutilizacaonfeDAO(InutilizacaonfeDAO inutilizacaonfeDAO) {
		this.inutilizacaonfeDAO = inutilizacaonfeDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#findForSolicitacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findForSolicitacao(String whereIn) {
		return inutilizacaonfeDAO.findForSolicitacao(whereIn);
	}

	/**
	 * Cria o XML de envio da solicita��o de inutiliza��o de NF-e
	 *
	 * @param lista
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public void enviarSolicitacao(List<Inutilizacaonfe> lista, ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) throws JDOMException, IOException {
		if(lista != null){
			for (Inutilizacaonfe inutilizacaonfe : lista) {
				
				InutNFe inutNFe = new InutNFe();
				
				Configuracaonfe configuracaonfe = inutilizacaonfe.getConfiguracaonfe();
				
				inutNFe.setVersao("4.00");
				
				InfInut infInut = new InfInut();
				inutNFe.setInfInut(infInut);
				
				String cUf = StringUtils.stringCheia(configuracaonfe.getUf().getCdibge().toString(), "0", 2, false);
				String ano = new SimpleDateFormat("yy").format(inutilizacaonfe.getDtinutilizacao());
				String cnpj = inutilizacaonfe.getEmpresa().getCnpj().getValue();
				String modelo = ModeloDocumentoFiscalEnum.NFE.equals(modeloDocumentoFiscalEnum) ? "55" : "65";
				String serie = configuracaonfe.getEmpresa().getSerienfe().toString(); 
				String numinicio = inutilizacaonfe.getNuminicio().toString();
				String numfim = inutilizacaonfe.getNumfim().toString();
				
				String id = "ID" + cUf + ano + cnpj + modelo + 
				StringUtils.stringCheia(serie, "0", 3, false) + 
				StringUtils.stringCheia(numinicio, "0", 9, false) + 
				StringUtils.stringCheia(numfim, "0", 9, false);
				
				infInut.setId(id);
				infInut.setAno(ano);
				infInut.setcNPJ(cnpj);
				infInut.setcUF(cUf);
				infInut.setMod(modelo);
				infInut.setnNFFin(numfim);
				infInut.setnNFIni(numinicio);
				infInut.setSerie(serie);
				
				int tpAmb = 1;
				if(configuracaonfe.getPrefixowebservice().name().endsWith("_HOM")){
					tpAmb = 2;
				}
				infInut.setTpAmb(tpAmb);
				
				infInut.setxJust(inutilizacaonfe.getJustificativa());
				infInut.setxJust(addScape(inutilizacaonfe.getJustificativa()));
				infInut.setxServ("INUTILIZAR");
				
				String xml = Util.strings.tiraAcento(inutNFe.toString());
				inutilizacaonfe.setXml(xml);
			}
			
			for (Inutilizacaonfe inutilizacaonfe : lista) {
				Arquivo arquivo = new Arquivo(("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + inutilizacaonfe.getXml()).getBytes(), "inut_" + SinedUtil.datePatternForReport() + ".xml", "text/xml");
				
				inutilizacaonfe.setArquivoxmlenvio(arquivo);
				
				arquivoDAO.saveFile(inutilizacaonfe, "arquivoxmlenvio");
				arquivoService.saveOrUpdate(arquivo);
				
				this.updateArquivoxmlenvio(inutilizacaonfe);
				this.updateSituacao(inutilizacaonfe, Inutilizacaosituacao.ENVIANDO);
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#updateArquivoxmlenvio(Inutilizacaonfe inutilizacaonfe)
	 *
	 * @param inutilizacaonfe
	 * @since 01/06/2012
	 * @author Rodrigo Freitas
	 */
	private void updateArquivoxmlenvio(Inutilizacaonfe inutilizacaonfe) {
		inutilizacaonfeDAO.updateArquivoxmlenvio(inutilizacaonfe);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#updateSituacao(Inutilizacaonfe inutilizacaonfe, Inutilizacaosituacao inutilizacaosituacao)
	 *
	 * @param inutilizacaonfe
	 * @param inutilizacaosituacao
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(Inutilizacaonfe inutilizacaonfe, Inutilizacaosituacao inutilizacaosituacao){
		inutilizacaonfeDAO.updateSituacao(inutilizacaonfe, inutilizacaosituacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#marcarInutilizacao(MarcarInutilizacaonfeBean bean)
	 *
	 * @param bean
	 * @return
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public int marcarInutilizacao(String whereIn) {
		return inutilizacaonfeDAO.marcarInutilizacao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#updateArquivoxmlretorno(Inutilizacaonfe inutilizacaonfe)
	 *
	 * @param inutilizacaonfe
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxmlretorno(Inutilizacaonfe inutilizacaonfe) {
		inutilizacaonfeDAO.updateArquivoxmlretorno(inutilizacaonfe);
	}
	
	@SuppressWarnings("unchecked")
	public void processaRetornoInutilizacaoSefaz(Inutilizacaonfe inutilizacaonfe, byte[] content) {
		try{
			Element rootElement = SinedUtil.getRootElementXML(content);
			
			Element infInutElt = SinedUtil.getChildElement("infInut", rootElement.getContent());
			if(infInutElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			Element cStatElt = SinedUtil.getChildElement("cStat", infInutElt.getContent());
			if(cStatElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			inutilizacaonfe = this.loadForEntrada(inutilizacaonfe);
			
			String cStat = cStatElt.getText();
			if(cStat != null && cStat.trim().equals("102")){
				Element dhRecbtoElt = SinedUtil.getChildElement("dhRecbto", infInutElt.getContent());
				Element nProtElt = SinedUtil.getChildElement("nProt", infInutElt.getContent());
				
				Date dtprocessamento_date = LkNfeUtil.FORMATADOR_DATA_HORA.parse(dhRecbtoElt.getText());
				Timestamp dtprocessamento = new Timestamp(dtprocessamento_date.getTime());
				String protocolo = nProtElt.getText();
				
				inutilizacaonfe.setDtprocessamento(dtprocessamento);
				inutilizacaonfe.setProtocoloinutilizacao(protocolo);
				
				this.updateSituacao(inutilizacaonfe, Inutilizacaosituacao.PROCESSADO_COM_SUCESSO);
				this.updateProtocoloDtprocessamento(inutilizacaonfe);
			} else {
				Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infInutElt.getContent());
				
				inutilizacaonfe.setMotivoerro(xMotivoElt.getText());
				
				this.updateSituacao(inutilizacaonfe, Inutilizacaosituacao.PROCESSADO_COM_ERRO);
				this.updateMotivoerro(inutilizacaonfe);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#updateProtocoloDtprocessamento(Inutilizacaonfe inutilizacaonfe)
	 *
	 * @param inutilizacaonfe
	 * @since 03/06/2012
	 * @author Rodrigo Freitas
	 */
	private void updateProtocoloDtprocessamento(Inutilizacaonfe inutilizacaonfe) {
		inutilizacaonfeDAO.updateProtocoloDtprocessamento(inutilizacaonfe);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#updateMotivoerro(Inutilizacaonfe inutilizacaonfe)
	 *
	 * @param inutilizacaonfe
	 * @since 03/06/2012
	 * @author Rodrigo Freitas
	 */
	private void updateMotivoerro(Inutilizacaonfe inutilizacaonfe) {
		inutilizacaonfeDAO.updateMotivoerro(inutilizacaonfe);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.InutilizacaonfeDAO#findForEnvio()
	 *
	 * @return
	 * @since 02/06/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findForEnvio() {
		return inutilizacaonfeDAO.findForEnvio();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @since 14/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Inutilizacaonfe> findInutilizacoesNovas(Empresa empresa) {
		return inutilizacaonfeDAO.findInutilizacoesNovas(empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param filtro
	* @return
	* @since 19/09/2014
	* @author Luiz Fernando
	*/
	public List<Inutilizacaonfe> findForSIntegraRegistro50(SintegraFiltro filtro) {
		return inutilizacaonfeDAO.findForSIntegraRegistro50(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param filtro
	* @return
	* @since 12/02/2015
	* @author Jo�o Vitor
	*/
	public List<Inutilizacaonfe> findForNumeroNfeNaoUtilizado(Empresa empresa) {
		return inutilizacaonfeDAO.findForNumeroNfeNaoUtilizado(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param id
	 * @return
	 * @since 01/03/2016
	 * @author C�sar
	 */
	public List<Inutilizacaonfe> validaInutilizacao(Integer id){
		return inutilizacaonfeDAO.validaInutilizacao(id);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @since 01/03/2016
	 * @author C�sar
	 */
	public List<Inutilizacaonfe> findForInutilizacao(String whereIn){
		return inutilizacaonfeDAO.findForInutilizacao(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @since 03/03/2016
	 * @author C�sar
	 */
	public List<Inutilizacaonfe> findForExcluirInutilizacao(String whereIn){
		return inutilizacaonfeDAO.findForExcluirInutilizacao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2017
	 */
	public List<Inutilizacaonfe> findForSageFiscal(SagefiscalFiltro filtro) {
		return inutilizacaonfeDAO.findForSageFiscal(filtro);
	}
	
	public List<Inutilizacaonfe> findForSpedRegC100(SpedarquivoFiltro filtro) {
		return inutilizacaonfeDAO.findForSpedRegC100(filtro);
	}
	
	private String addScape(String string){
		if(string == null) return null;
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			switch (string.charAt(i)) {
			case '"' :
				stringBuilder.append("&quot;");
				break;
			case '\'' :
				stringBuilder.append("&#39;");
				break;
			case '<' :
				stringBuilder.append("&lt;");
				break;
			case '>' :
				stringBuilder.append("&gt;");
				break;
			case '&' :
				stringBuilder.append("&amp;");
				break;
			default :
				stringBuilder.append(string.charAt(i));
			}
		}
		
		return Util.strings.tiraAcento(stringBuilder.toString().replace("\n", " ").replace("\r", " ")).trim();
	}
	
	private List<Inutilizacaonfe> findForCsv(InutilizacaonfeFiltro filtro) {
		return inutilizacaonfeDAO.findForCsv(filtro);
	}
	
	public Resource geraArquivoCSVListagem(InutilizacaonfeFiltro filtro, boolean isNfe) {
		StringBuilder csv = new StringBuilder();
		
		List<Inutilizacaonfe> lista = this.findForCsv(filtro);
		
		csv.append("\"Id\"").append(";");
		csv.append("\"Empresa\"").append(";");
		csv.append("\"Configura��o de NF-e\"").append(";");
		csv.append("\"Data da inutiliza��o\"").append(";");
		csv.append("\"N�mero inicial\"").append(";");
		csv.append("\"N�mero final\"").append(";");
		csv.append("\"Situa��o\"").append(";");
		csv.append("\n");
		
		for (Inutilizacaonfe inutilizacaonfe: lista) {
			csv.append("\"" + (inutilizacaonfe.getCdinutilizacaonfe() != null ? inutilizacaonfe.getCdinutilizacaonfe() : "") + "\";");
			csv.append("\"" + (inutilizacaonfe.getEmpresa() != null ? inutilizacaonfe.getEmpresa().getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa() : "") + "\";");
			csv.append("\"" + (inutilizacaonfe.getConfiguracaonfe() != null ? inutilizacaonfe.getConfiguracaonfe().getDescricao() : "") + "\";");			
			csv.append("\"" + (inutilizacaonfe.getDtinutilizacao() != null ? SinedDateUtils.toString(inutilizacaonfe.getDtinutilizacao()) : "") + "\";");
			csv.append("\"" + (inutilizacaonfe.getNuminicio() != null ? inutilizacaonfe.getNuminicio() : "") + "\";");
			csv.append("\"" + (inutilizacaonfe.getNumfim() != null ? inutilizacaonfe.getNumfim() : "") + "\";");
			csv.append("\"" + (inutilizacaonfe.getSituacao() != null ? inutilizacaonfe.getSituacao().getNome() : "") + "\";");

			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "inutilizacao" + (isNfe ? "nfe" : "nfce") + "_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		
		return resource;
	}
	
}
