package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Animal;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.dao.AnimalDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AnimalService extends GenericService<Animal> {

	
	private AnimalDAO animalDAO;
	public void setAnimalDAO(AnimalDAO animalDAO) { this.animalDAO = animalDAO; }

	/**
	 * Chama pesquisa autocomplete para exibir somente animais que n�o setados como obito
	 * 
	 * @author Cleiton Amorim Coqueiro
	 * @since 11/11/2013
	 * @param Cliente
	 * @return lista de nomes
	 */
	public List<Animal> findExcetoObitoByCliente(Cliente cliente){
		return animalDAO.findExcetoObitoByCliente(cliente);
	}

}