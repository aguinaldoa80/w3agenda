package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendasituacaoService extends GenericService<Vendasituacao>{

	public List<Vendasituacao> getSituacaoVendaPorVendedor() {
		List<Vendasituacao> list = new ArrayList<Vendasituacao>();
		
		list.add(Vendasituacao.REALIZADA);
		list.add(Vendasituacao.FATURADA);
		list.add(Vendasituacao.EMPRODUCAO);
		list.add(Vendasituacao.CANCELADA);
		list.add(Vendasituacao.AGUARDANDO_APROVACAO);
		
		return list;
	}
	
	public List<Vendasituacao> getSituacaoVendaPorVendedorPadrao() {
		List<Vendasituacao> list = new ArrayList<Vendasituacao>();
		
		list.add(Vendasituacao.REALIZADA);
		list.add(Vendasituacao.FATURADA);
		list.add(Vendasituacao.EMPRODUCAO);
		
		return list;
	}
}
