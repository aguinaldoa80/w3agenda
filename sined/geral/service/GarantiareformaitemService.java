package br.com.linkcom.sined.geral.service;

import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Garantiareformahistorico;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.Valecompraorigem;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.geral.dao.GarantiareformaitemDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GarantiareformaitemService extends GenericService<Garantiareformaitem>{

	protected GarantiareformaitemDAO garantiareformaitemDAO;
	protected PedidovendaService pedidovendaService;
	protected PedidovendamaterialService pedidovendamaterialService;
	protected GarantiareformaService garantiareformaService;
	protected ParametrogeralService parametrogeralService;
	protected ValecompraService valecompraService;
	protected GarantiareformahistoricoService garantiareformahistoricoService;
	protected VendaService vendaService;
	
	public void setGarantiareformaitemDAO(
			GarantiareformaitemDAO garantiareformaitemDAO) {
		this.garantiareformaitemDAO = garantiareformaitemDAO;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setPedidovendamaterialService(
			PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setGarantiareformaService(
			GarantiareformaService garantiareformaService) {
		this.garantiareformaService = garantiareformaService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	public void setGarantiareformahistoricoService(
			GarantiareformahistoricoService garantiareformahistoricoService) {
		this.garantiareformahistoricoService = garantiareformahistoricoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	
	public void calculaValorGarantia(Garantiareformaitem bean, Pedidovendatipo pedidovendatipo, Prazopagamento prazopagamento){
		Garantiareforma garantia = bean.getGarantiareforma();
		Double valorvendaAux = bean.getMaterial().getValorvenda();
		pedidovendaService.preecheValorVendaComTabelaPreco(bean.getMaterial(),
				garantia.getPedidovendaorigem() != null? garantia.getPedidovendaorigem().getPedidovendatipo(): pedidovendatipo,
				garantia.getCliente(), garantia.getEmpresa(), bean.getMaterial().getUnidademedida(),
				garantia.getPedidovendaorigem() != null? garantia.getPedidovendaorigem().getPrazopagamento(): prazopagamento);
		
		Double valorvenda = bean.getMaterial().getValorvenda();
		if((valorvenda == null || valorvenda <= 0) && bean.getGarantiareforma().getPedidovendaorigem() != null){
			Pedidovendamaterial pedidovendamaterial = pedidovendamaterialService.findByReformapneu(bean.getGarantiareforma().getPedidovendaorigem(), bean.getMaterial(), bean.getPneu(), null);
			valorvenda = pedidovendamaterial.getValorvendamaterial();
		}
		
		if(valorvenda == null || valorvenda == 0){
			throw new SinedException("N�o foi poss�vel calcular o valor de garantia porque o servi�o "+bean.getMaterial().getNome()+" n�o possui pre�o de venda. " + 
					Util.objects.getIdForMensagem("Pedido de venda origem: ", bean.getGarantiareforma().getPedidovendaorigem()));
		}
		
		if(bean.getMaterial() != null && bean.getMaterial().getPercentualdescontoTabela() != null && bean.getMaterial().getPercentualdescontoTabela().doubleValue() > 0){
			valorvenda = valorvenda - ((valorvenda/100) * bean.getMaterial().getPercentualdescontoTabela().doubleValue());
		}
		
		Double valorCreditoGarantia = bean.getGarantiatipopercentual() == null || bean.getGarantiatipopercentual().getPercentual() == null? 0: (valorvenda / 100) * bean.getGarantiatipopercentual().getPercentual();
		bean.setValorCreditoGarantia(new Money(valorCreditoGarantia));
		
		bean.getMaterial().setValorvenda(valorvendaAux);
	}
	
	public void utilizarGarantiaNoPedido(Garantiareformaitem garantiareformaitem, Pedidovendamaterial pedidovendamaterial){
		Garantiareforma garantiareforma = garantiareformaService.load(garantiareformaitem.getGarantiareforma());
		if(!Garantiasituacao.UTILIZADA.equals(garantiareforma.getGarantiasituacao())){
			this.garantiareformaService.updateSituacaoGarantia(garantiareformaitem.getGarantiareforma().getCdgarantiareforma().toString(),
															Garantiasituacao.UTILIZADA);
			this.updatePedidovendamaterial(garantiareformaitem, pedidovendamaterial);
			Garantiareformahistorico historico = new Garantiareformahistorico();
			historico.setAcao(Garantiasituacao.UTILIZADA);
			historico.setGarantiareforma(garantiareforma);
			
			Integer cdpedidovenda = this.getCdpedidovenda(pedidovendamaterial);
			
			historico.setObservacao("Visualizar pedido de venda <a href=\"javascript:visualizarPedidovenda("+cdpedidovenda+")\">"+cdpedidovenda +"</a>.");
			garantiareformahistoricoService.saveOrUpdate(historico);
		}
	}
	
	public void utilizarGarantiaNaVenda(Garantiareformaitem garantiareformaitem, Vendamaterial vendamaterial){
		Garantiareforma garantiareforma = garantiareformaService.load(garantiareformaitem.getGarantiareforma());
		if(!Garantiasituacao.UTILIZADA.equals(garantiareforma.getGarantiasituacao())){
			this.garantiareformaService.updateSituacaoGarantia(garantiareformaitem.getGarantiareforma().getCdgarantiareforma().toString(), Garantiasituacao.UTILIZADA);
			this.updateVendamaterial(garantiareformaitem, vendamaterial);
			Garantiareformahistorico historico = new Garantiareformahistorico();
			historico.setAcao(Garantiasituacao.UTILIZADA);
			historico.setGarantiareforma(garantiareforma);
			
			Integer cdvenda = this.getCdvenda(vendamaterial);
			
			historico.setObservacao("Visualizar venda <a href=\"javascript:visualizarVenda("+cdvenda+")\">"+cdvenda +"</a>.");
			garantiareformahistoricoService.saveOrUpdate(historico);
		}
	}
	
	public void updatePedidovendamaterial(Garantiareformaitem bean, Pedidovendamaterial pedidovendamaterial){
		garantiareformaitemDAO.updatePedidovendamaterial(bean, pedidovendamaterial);
	}
	
	public void updateVendamaterial(Garantiareformaitem bean, Vendamaterial vendamaterial){
		garantiareformaitemDAO.updateVendamaterial(bean, vendamaterial);
	}
	
	public void retiraVinculoGarantiaPedidovendamaterial(Pedidovendamaterial pedidovendamaterialdestino){
		if(pedidovendamaterialdestino == null || pedidovendamaterialdestino.getCdpedidovendamaterial() == null){
			return;
		}
		Garantiareformaitem bean = this.loadByPedidovendamaterial(pedidovendamaterialdestino);
		if(bean != null){
			garantiareformaService.updateSituacaoGarantia(bean.getGarantiareforma().getCdgarantiareforma().toString(), Garantiasituacao.DISPONIVEL);
			garantiareformaitemDAO.desassociaPedidovendamaterial(bean);
			Garantiareformahistorico historico = new Garantiareformahistorico();
			historico.setAcao(Garantiasituacao.CANCELADA);
			historico.setGarantiareforma(bean.getGarantiareforma());
			
			Integer cdpedidovenda = this.getCdpedidovenda(pedidovendamaterialdestino);
			
			historico.setObservacao("Cancelamento do uso da garantia no pedido de venda <a href=\"javascript:visualizarPedidovenda("+cdpedidovenda+")\">"+cdpedidovenda +"</a>.");
			garantiareformahistoricoService.saveOrUpdate(historico);
		}
	}
	
	public void retiraVinculoGarantiaVendamaterial(Vendamaterial vendamaterialdestino){
		if(vendamaterialdestino == null || vendamaterialdestino.getCdvendamaterial() == null){
			return;
		}
		Garantiareformaitem bean = this.loadByVendamaterial(vendamaterialdestino);
		if(bean != null){
			garantiareformaService.updateSituacaoGarantia(bean.getGarantiareforma().getCdgarantiareforma().toString(), Garantiasituacao.DISPONIVEL);
			garantiareformaitemDAO.desassociaVendamaterial(bean);
			Garantiareformahistorico historico = new Garantiareformahistorico();
			historico.setAcao(Garantiasituacao.CANCELADA);
			historico.setGarantiareforma(bean.getGarantiareforma());
			
			Integer cdvenda = this.getCdvenda(vendamaterialdestino);
			
			historico.setObservacao("Cancelamento do uso da garantia na venda <a href=\"javascript:visualizarVenda("+cdvenda+")\">"+cdvenda +"</a>.");
			garantiareformahistoricoService.saveOrUpdate(historico);
		}
	}
	
	private Integer getCdvenda(Vendamaterial vendamaterial){
		if(vendamaterial.getVenda() != null && vendamaterial.getVenda().getCdvenda() != null){
			return vendamaterial.getVenda().getCdvenda();
		}
		Venda venda = vendaService.findByVendamaterial(vendamaterial);
		if(venda != null && venda.getCdvenda() != null){
			return venda.getCdvenda();
		}
		return null;
	}
	
	private Integer getCdpedidovenda(Pedidovendamaterial pedidovendamaterial){
		if(pedidovendamaterial.getPedidovenda() != null && pedidovendamaterial.getPedidovenda().getCdpedidovenda() != null){
			return pedidovendamaterial.getPedidovenda().getCdpedidovenda();
		}
		Pedidovenda pedidovenda = pedidovendaService.findByPedidovendamaterial(pedidovendamaterial);
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			return pedidovenda.getCdpedidovenda();
		}
		return null;
	}
	
	public Garantiareformaitem loadByPedidovendamaterial(Pedidovendamaterial pedidovendamaterialdestino){
		return garantiareformaitemDAO.loadByPedidovendamaterial(pedidovendamaterialdestino);
	}
	
	public Garantiareformaitem loadByVendamaterial(Vendamaterial vendamaterialdestino){
		return garantiareformaitemDAO.loadByVendamaterial(vendamaterialdestino);
	}
	
	public void cancelaValecompraByGarantia(Garantiareformaitem bean, String identificacao){
		Money saldoValecompra = valecompraService.getSaldoByCliente(bean.getGarantiareforma().getCliente());
		if(saldoValecompra != null && saldoValecompra.getValue().doubleValue() > 0){
			List<Valecompra> listavalecompracredito = valecompraService.findByGarantiareforma(bean.getGarantiareforma(), Tipooperacao.TIPO_CREDITO);
			
			if(!listavalecompracredito.isEmpty() && listavalecompracredito.get(0).getValor().getValue().doubleValue() <= saldoValecompra.getValue().doubleValue()){
				Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
				
				Valecompraorigem valecompraorigem = new Valecompraorigem();
				valecompraorigem.setGarantiareforma(bean.getGarantiareforma());
				listaValecompraorigem.add(valecompraorigem);
				
				
				Valecompra valecompra = new Valecompra();
				valecompra.setCliente(bean.getGarantiareforma().getCliente());
				valecompra.setData(SinedDateUtils.currentDate());
				valecompra.setIdentificacao(identificacao);
				valecompra.setListaValecompraorigem(listaValecompraorigem);
				valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				
				valecompra.setValor(listavalecompracredito.get(0).getValor());
				
				valecompraService.saveOrUpdate(valecompra);
			}
		}
	}
	
	public Garantiareformaitem loadByGarantiareformaitem(Garantiareformaitem garantiareformaitem){
		return garantiareformaitemDAO.loadByGarantiareformaitem(garantiareformaitem);
	}
	
	public Garantiareformaitem loadByGarantiareforma(Garantiareforma garantiareforma, Garantiasituacao garantiasituacao){
		return garantiareformaitemDAO.loadByGarantiareforma(garantiareforma, garantiasituacao);
	}
	
	public Garantiareformaitem loadByGarantiareformaForCancelarValecompra(Garantiareforma garantiareforma){
		return garantiareformaitemDAO.loadByGarantiareformaForCancelarValecompra(garantiareforma, null);
	}
	
	public Garantiareformaitem loadByGarantiareformaForCancelarValecompra(Garantiareforma garantiareforma, Garantiareformaitem garantiareformaitem){
		return garantiareformaitemDAO.loadByGarantiareformaForCancelarValecompra(garantiareforma, garantiareformaitem);
	}
	
	public Garantiareformaitem loadByVerificacaoUsoVenda(Garantiareformaitem garantiareformaitem){
		if(garantiareformaitem == null){
			return null;
		}
		return garantiareformaitemDAO.loadByVerificacaoUsoVenda(garantiareformaitem, null);
	}
	
	public Garantiareformaitem loadByVerificacaoUsoVenda(Garantiareforma garantiareforma){
		if(garantiareforma == null){
			return null;
		}
		return garantiareformaitemDAO.loadByVerificacaoUsoVenda(null, garantiareforma);
	}
}
