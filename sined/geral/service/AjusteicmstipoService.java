package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ajusteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.geral.dao.AjusteicmstipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AjusteicmstipoService extends GenericService<Ajusteicmstipo>{
	
	private AjusteicmstipoDAO ajusteicmstipoDAO;
	
	public void setAjusteicmstipoDAO(AjusteicmstipoDAO ajusteicmstipoDAO) {
		this.ajusteicmstipoDAO = ajusteicmstipoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param tipoajuste
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	public List<Ajusteicmstipo> findByTipoajuste(TipoAjuste tipoajuste){
		return ajusteicmstipoDAO.findByTipoajuste(tipoajuste);
	}
	
}
