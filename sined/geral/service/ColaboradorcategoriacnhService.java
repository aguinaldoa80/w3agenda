package br.com.linkcom.sined.geral.service;

import java.util.Set;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Categoriacnh;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcategoriacnh;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorcategoriacnhService extends GenericService<Colaboradorcategoriacnh> {	
	
	/**
	 * M�todo para carregar as categorias selecionadas para o colaborador
	 * 
	 * @param listaColaboradorcategoriacnh
	 * @param colaborador
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Set<Categoriacnh> carregaCategoriacnh(Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh, Colaborador colaborador){
		Set<Categoriacnh> listaCategoriacnh = new ListSet<Categoriacnh>(Categoriacnh.class);
		if (colaborador.getListaColaboradorcategoriacnh() != null) {
			listaColaboradorcategoriacnh = colaborador.getListaColaboradorcategoriacnh();
			for (Colaboradorcategoriacnh colaboradorcategoriacnh : listaColaboradorcategoriacnh) {
				listaCategoriacnh.add(colaboradorcategoriacnh.getCategoriacnh());
			}
			
		}
		return listaCategoriacnh;
	}
	/* singleton */
	private static ColaboradorcategoriacnhService instance;
	public static ColaboradorcategoriacnhService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorcategoriacnhService.class);
		}
		return instance;
	}
	
}
