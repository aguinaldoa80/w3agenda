package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Arquivofolha;
import br.com.linkcom.sined.geral.bean.Arquivofolhahistorico;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Colaboradordespesahistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhatipo;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesahistoricoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ArquivofolhaDAO;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoArquivofolhaBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoDepositoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoEnvioBancarioAgrupamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoEnvioBancarioBean;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportacaoEnvioBancarioRateioBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivofolhaService extends GenericService<Arquivofolha>{

	private ArquivofolhaDAO arquivofolhaDAO;
	private ArquivofolhahistoricoService arquivofolhahistoricoService;
	private ColaboradorService colaboradorService;
	private ColaboradordespesaService colaboradordespesaService;
	private ColaboradordespesahistoricoService colaboradordespesahistoricoService;
	private DocumentoService documentoService;
	private ProjetoService projetoService;
	private ContagerencialService contagerencialService;
	private BancoService bancoService;
	private ArquivofolhaService arquivofolhaService;
	
	public void setArquivofolhaService(ArquivofolhaService arquivofolhaService) {this.arquivofolhaService = arquivofolhaService;}
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setColaboradordespesahistoricoService(ColaboradordespesahistoricoService colaboradordespesahistoricoService) {this.colaboradordespesahistoricoService = colaboradordespesahistoricoService;}
	public void setColaboradordespesaService(ColaboradordespesaService colaboradordespesaService) {this.colaboradordespesaService = colaboradordespesaService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}	
	public void setArquivofolhahistoricoService(ArquivofolhahistoricoService arquivofolhahistoricoService) {this.arquivofolhahistoricoService = arquivofolhahistoricoService;}
	public void setArquivofolhaDAO(ArquivofolhaDAO arquivofolhaDAO) {this.arquivofolhaDAO = arquivofolhaDAO;}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca lista de Arquivofolha para o processo de autoriza��o e estorno.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ArquivofolhaDAO#findForAutorizarEstornarCancelar(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivofolha> findForAutorizarEstornarCancelar(String whereIn) {
		return arquivofolhaDAO.findForAutorizarEstornarCancelar(whereIn);
	}

	/**
	 * M�todo que realiza a autoriza��o do Arquivofolha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao)
	 *
	 * @param arquivofolha
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void autorizar(Arquivofolha arquivofolha) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(arquivofolha, Arquivofolhasituacao.AUTORIZADO);
		
		// REGISTRA NO HIST�RICO
		Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
		arquivofolhahistorico.setAcao(Arquivofolhaacao.AUTORIZADO);
		arquivofolhahistorico.setArquivofolha(arquivofolha);
		arquivofolhahistoricoService.saveOrUpdate(arquivofolhahistorico);
	}
	
	/**
	 * M�todo que realiza o processamento do Arquivofolha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao)
	 *
	 * @param arquivofolha
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void processar(Arquivofolha arquivofolha) {
		if(arquivofolha.getArquivofolhasituacao() == null){
			arquivofolha = this.load(arquivofolha, "arquivofolha.cdarquivofolha, arquivofolha.arquivofolhasituacao");
		}
		if(!arquivofolha.getArquivofolhasituacao().equals(Arquivofolhasituacao.PROCESSADO)){
			// ATUALIZA A SITUA��O DO ARQUIVO
			this.updateSituacao(arquivofolha, Arquivofolhasituacao.PROCESSADO);
			
			// REGISTRA NO HIST�RICO
			Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
			arquivofolhahistorico.setAcao(Arquivofolhaacao.PROCESSADO);
			arquivofolhahistorico.setArquivofolha(arquivofolha);
			arquivofolhahistoricoService.saveOrUpdate(arquivofolhahistorico);
		}
	}
	
	/**
	 * M�todo que realiza o estorno do Arquivofolha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao)
	 * 
	 * @param obs 
	 * @param arquivofolha
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void estornar(Arquivofolha arquivofolha, String obs) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(arquivofolha, Arquivofolhasituacao.EM_ABERTO);
		
		// REGISTRA NO HIST�RICO
		Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
		arquivofolhahistorico.setAcao(Arquivofolhaacao.ESTORNADO);
		arquivofolhahistorico.setArquivofolha(arquivofolha);
		arquivofolhahistorico.setObservacao(obs);
		arquivofolhahistoricoService.saveOrUpdate(arquivofolhahistorico);
	}
	
	/**
	 * M�todo que realiza o cancelamento do Arquivofolha.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao)
	 * 
	 * @param obs 
	 * @param arquivofolha
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	public void cancelar(Arquivofolha arquivofolha, String obs) {
		// ATUALIZA A SITUA��O DO ARQUIVO
		this.updateSituacao(arquivofolha, Arquivofolhasituacao.CANCELADO);
		
		// REGISTRA NO HIST�RICO
		Arquivofolhahistorico arquivofolhahistorico = new Arquivofolhahistorico();
		arquivofolhahistorico.setAcao(Arquivofolhaacao.CANCELADO);
		arquivofolhahistorico.setArquivofolha(arquivofolha);
		arquivofolhahistorico.setObservacao(obs);
		arquivofolhahistoricoService.saveOrUpdate(arquivofolhahistorico);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * Atualiza a situa��o do Arquivofolha de acordo com o par�metro Arquivofolhasituacao.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivofolhaDAO#updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao)
	 *
	 * @param arquivofolha
	 * @param arquivofolhasituacao
	 * @since Jun 8, 2011
	 * @author Rodrigo Freitas
	 */
	private void updateSituacao(Arquivofolha arquivofolha, Arquivofolhasituacao arquivofolhasituacao){
		arquivofolhaDAO.updateSituacao(arquivofolha, arquivofolhasituacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca informa��es necess�rias para o processamento do arquivo de folha. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivofolhaDAO#findForProcessamento(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since Jun 16, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivofolha> findForProcessamento(String whereIn) {
		return arquivofolhaDAO.findForProcessamento(whereIn);
	}
	
	/**
	 * Preenche a lista de Dep�sitos para gerar despesa para o(s) colaborador(es) que est�(�o) no(s) arquivo(s) selecionado(s).
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findByMatricula(Integer matricula)
	 * 
	 * @param lista
	 * @return
	 * @throws ParseException
	 * @since Jun 17, 2011
	 * @author Rodrigo Freitas
	 */
	public List<ImportacaoDepositoBean> preencheDeposito(List<Arquivofolha> lista) throws ParseException {
		
		List<ImportacaoDepositoBean> listaDeposito = new ArrayList<ImportacaoDepositoBean>();
		ImportacaoDepositoBean bean;
		String[] linhas;
		String[] campos;
		String matriculaStr, agenciaStr, contaStr, valorStr, dtdepositoStr, bancoStr;
		Long matricula;
		
		for (Arquivofolha af : lista) {
			if(af.getArquivofolhatipo().equals(Arquivofolhatipo.DEPOSITO)){
				linhas = af.getStringArquivo();
				for (int i = 0; i < linhas.length; i++) {
					campos = linhas[i].split(";");
					if(campos != null && campos.length == 7){
						matriculaStr = campos[0];
						bancoStr = campos[1];
						agenciaStr = campos[2];
						contaStr = campos[3];
						valorStr = campos[4];
						dtdepositoStr = campos[5];
						
						bean = new ImportacaoDepositoBean();
						
						bean.setCdarquivofolha(af.getCdarquivofolha());
						bean.setMatricula(matriculaStr);
						bean.setBanco(bancoStr);
						bean.setAgencia(agenciaStr);
						bean.setConta(contaStr);
						
						valorStr = valorStr.replaceAll("\\.", "").replaceAll(",", ".");
						bean.setValor(new Money(Double.valueOf(valorStr)));
						bean.setDtdeposito(SinedDateUtils.stringToDate(dtdepositoStr));
						
						while(matriculaStr.startsWith("0")){
							matriculaStr = matriculaStr.substring(1);
						}
						matricula = Long.parseLong(matriculaStr);
						bean.setColaborador(colaboradorService.findByMatricula(matricula));
						
						listaDeposito.add(bean);
					}
				}
			}
		}
		
		return listaDeposito;
	}
	
	/**
	 * Preenche a lista de envio banc�rio para gerar conta(s) a pagar para o total do valor que est� no arquivo.
	 *
	 * @param lista
	 * @return
	 * @throws ParseException
	 * @since Jun 17, 2011
	 * @author Rodrigo Freitas
	 */
	public List<ImportacaoEnvioBancarioBean> preencheEnvioBancario(List<Arquivofolha> lista) throws ParseException {
		
		List<ImportacaoEnvioBancarioAgrupamentoBean> listaBean = new ArrayList<ImportacaoEnvioBancarioAgrupamentoBean>();
		ImportacaoEnvioBancarioAgrupamentoBean bean;
		
		String[] linhas;
		String[] campos;
		
		for (Arquivofolha af : lista) {
			if(af.getArquivofolhatipo().equals(Arquivofolhatipo.ENVIO_BANCARIO)){
				linhas = af.getStringArquivo();
				for (int i = 0; i < linhas.length; i++) {
					campos = linhas[i].split(";");
					if(campos != null && campos.length == 7){
						bean = new ImportacaoEnvioBancarioAgrupamentoBean(af.getCdarquivofolha(), campos[1], campos[4], campos[5], campos[6]);
						listaBean.add(bean);
					}
				}
			}
		}
		
		Map<Integer, String> mapData = new HashMap<Integer, String>();		
		for (ImportacaoEnvioBancarioAgrupamentoBean b : listaBean) {
			if(b.getCdarquivofolha() != null && 
					b.getDtdeposito() != null && 
					!b.getDtdeposito().equals("00/00/0000") &&
					!mapData.containsKey(b.getCdarquivofolha())){
				mapData.put(b.getCdarquivofolha(), b.getDtdeposito());
			}
		}
		
		for (ImportacaoEnvioBancarioAgrupamentoBean b : listaBean) {
			if(b.getCdarquivofolha() != null && 
					b.getDtdeposito() != null && 
					b.getDtdeposito().equals("00/00/0000") &&
					mapData.containsKey(b.getCdarquivofolha())){
				b.setDtdeposito(mapData.get(b.getCdarquivofolha()));
			}
		}
		
		ImportacaoEnvioBancarioBean envioBancarioBean;
		List<ImportacaoEnvioBancarioBean> listaEnvioBancario = new ArrayList<ImportacaoEnvioBancarioBean>();
		java.sql.Date dtdeposito;
		Money valor;
		
		for (ImportacaoEnvioBancarioAgrupamentoBean b : listaBean) {
			dtdeposito = SinedDateUtils.stringToDate(b.getDtdeposito());
			valor = new Money(Double.valueOf(b.getValor().replaceAll("\\.", "").replaceAll(",", ".")));
			
			envioBancarioBean = new ImportacaoEnvioBancarioBean(b.getBanco(), dtdeposito);
			
			if(listaEnvioBancario.contains(envioBancarioBean)){
				envioBancarioBean = listaEnvioBancario.get(listaEnvioBancario.indexOf(envioBancarioBean));
				
				envioBancarioBean.addValor(valor);
				envioBancarioBean.addCdarquivofolha(b.getCdarquivofolha());
				envioBancarioBean.addRateio(b.getProjeto(), valor);
			} else {
				envioBancarioBean.setValor(valor);
				envioBancarioBean.addCdarquivofolha(b.getCdarquivofolha());
				envioBancarioBean.addRateio(b.getProjeto(), valor);
				
				listaEnvioBancario.add(envioBancarioBean);
			}
		}
		
		String sigla;
		Projeto projeto;
		for (ImportacaoEnvioBancarioBean b : listaEnvioBancario) {
			
			try{
				b.setNomebanco(bancoService.findByNumero(Integer.parseInt(b.getBanco())).getNome());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			for (ImportacaoEnvioBancarioRateioBean it : b.getListaRateio()) {
				if(it.getSiglaprojeto() != null){
					sigla = it.getSiglaprojeto();
					projeto = projetoService.findBySigla(sigla);
					if(projeto != null){
						it.setProjeto(projeto);
						it.setCentrocusto(projeto.getCentrocusto());
						it.setContagerencial(contagerencialService.findByProjetoOrcamentoMOD(projeto));
					}
				}
			}
		}
		
		return listaEnvioBancario;
	}
	
	/**
	 * Faz o processamento dos itens de Dep�sito que foram marcados na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#processar(Arquivofolha arquivofolha)
	 * 
	 * @param bean
	 * @since Jun 20, 2011
	 * @author Rodrigo Freitas
	 */
	public void processarDeposito(ImportacaoArquivofolhaBean bean) {
		if(bean.getListaDeposito() != null && bean.getListaDeposito().size() > 0){
			List<Colaboradordespesa> listaColaboradordespesa = new ArrayList<Colaboradordespesa>();
			Colaboradordespesa colaboradordespesa;
			
			List<ImportacaoDepositoBean> listaDeposito = bean.getListaDeposito();
			for (ImportacaoDepositoBean it : listaDeposito) {
				if(it.getMarcado() != null && it.getMarcado()){
					colaboradordespesa = new Colaboradordespesa();
					
					colaboradordespesa.setColaborador(it.getColaborador());
					colaboradordespesa.setColaboradordespesamotivo(bean.getColaboradordespesamotivo());
					colaboradordespesa.setDtdeposito(it.getDtdeposito());
					colaboradordespesa.setDtinsercao(SinedDateUtils.currentDate());
					colaboradordespesa.setSituacao(Colaboradordespesasituacao.EM_ABERTO);
					colaboradordespesa.setValor(it.getValor());
					colaboradordespesa.setTotal(it.getValor());
					colaboradordespesa.setCdarquivofolha(it.getCdarquivofolha());
					
					listaColaboradordespesa.add(colaboradordespesa);
				}
			}
			
			if(listaColaboradordespesa.size() > 0){
				Colaboradordespesahistorico colaboradordespesahistorico;
				Integer id;
				List<Integer> listaIds = new ArrayList<Integer>();
				for (Colaboradordespesa cd : listaColaboradordespesa) {
					id = cd.getCdarquivofolha();
					colaboradordespesaService.saveOrUpdate(cd);
					
					colaboradordespesahistorico = new Colaboradordespesahistorico();
					colaboradordespesahistorico.setAcao(Colaboradordespesahistoricoacao.CRIADO);
					colaboradordespesahistorico.setObservacao("Despesa gerada a partir do arquivo de folha " + id + ".");
					colaboradordespesahistorico.setColaboradordespesa(cd);
					colaboradordespesahistoricoService.saveOrUpdate(colaboradordespesahistorico);
					
					if(!listaIds.contains(id)){
						listaIds.add(id);
					}
				}
				
				Arquivofolha arquivofolha;
				for (Integer i : listaIds) {
					arquivofolha = new Arquivofolha(i);
					this.processar(arquivofolha);
				}
			}
		}
		
	}
	
	/**
	 * Faz o processamento dos itens de Envio Banc�rio que foram marcados na tela.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ArquivofolhaService#processar(Arquivofolha arquivofolha)
	 * 
	 * @param bean
	 * @since Jun 20, 2011
	 * @author Rodrigo Freitas
	 */
	public void processarEnviobancario(ImportacaoArquivofolhaBean bean) {
		if(bean.getListaEnviobancario() != null && bean.getListaEnviobancario().size() > 0){
			Documento documento;
			Documentohistorico documentohistorico;
			Set<Documentohistorico> listaHistorico;
			List<Rateioitem> listaRateioitem;
			Rateioitem rateioitem;
			Rateio rateio;
			List<ImportacaoEnvioBancarioRateioBean> listaRateio;
			List<Arquivofolha> listaArquivo;
			
			List<ImportacaoEnvioBancarioBean> listaEnviobancario = bean.getListaEnviobancario();
			for (ImportacaoEnvioBancarioBean it : listaEnviobancario) {
				if(it.getMarcado() != null && it.getMarcado()){
					documento = new Documento();
					
					listaArquivo = new ArrayList<Arquivofolha>();
					String[] ids = it.getWhereInArquivofolha().split(",");
					for (String string : ids) {
						listaArquivo.add(arquivofolhaService.loadForEntrada(new Arquivofolha(Integer.parseInt(string))));
					}
					
					String outrospagamento = "SAL�RIOS E ORDENADOS PESSOAL - " + SinedUtil.listAndConcatenate(listaArquivo, "arquivofolhamotivo.descricao", ", ");
					if(outrospagamento.length() > 100){
						outrospagamento = outrospagamento.substring(0, 100);
					}
					
					Date referencia = SinedDateUtils.addMesData(it.getDtdeposito(), -1);
					String descricao = "REF " + SinedDateUtils.getDescricaoMes(referencia) + "/" + 
						SinedDateUtils.getDateProperty(referencia, Calendar.YEAR) + " - " + it.getNomebanco();
					
					documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
					documento.setDocumentotipo(bean.getDocumentotipo());
					documento.setDescricao(descricao.toUpperCase());
					documento.setDtemissao(SinedDateUtils.currentDate());
					documento.setDtvencimento(it.getDtdeposito());
					documento.setValor(it.getValor());
					documento.setEmpresa(bean.getEmpresa());
					documento.setTipopagamento(Tipopagamento.OUTROS);
					documento.setOutrospagamento(outrospagamento.toUpperCase());
					documento.setDocumentoacao(Documentoacao.PREVISTA);
					
					documentohistorico = new Documentohistorico(documento);
					documentohistorico.setObservacao("Criada a partir do processmento do arquivo de folha");
					listaHistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
					listaHistorico.add(documentohistorico);
					documento.setListaDocumentohistorico(listaHistorico);
					
					listaRateioitem = new ArrayList<Rateioitem>();
					listaRateio = it.getListaRateio();
					
					for (ImportacaoEnvioBancarioRateioBean b : listaRateio) {
						rateioitem = new Rateioitem();
						rateioitem.setContagerencial(b.getContagerencial());
						rateioitem.setCentrocusto(b.getCentrocusto());
						rateioitem.setProjeto(b.getProjeto());
						rateioitem.setValor(b.getValor());
						rateioitem.setPercentual(SinedUtil.calculaPorcentagem(b.getValor(), it.getValor()));
						
						listaRateioitem.add(rateioitem);
					}
					
					rateio = new Rateio();
					rateio.setListaRateioitem(listaRateioitem);
					documento.setRateio(rateio);
					
					documentoService.saveOrUpdate(documento);
					documentoService.callProcedureAtualizaDocumento(documento);
					
					for (Arquivofolha af : listaArquivo) {
						this.processar(af);
					}
				}
			}
		}
	}
	
}
