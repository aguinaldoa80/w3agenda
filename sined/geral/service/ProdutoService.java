package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.dao.ProdutoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProdutoService extends GenericService<Produto> {

	private ProdutoDAO produtoDAO;
	
	public void setProdutoDAO(ProdutoDAO produtoDAO) {
		this.produtoDAO = produtoDAO;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#deleteProduto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void deleteProduto(Material material) {
		produtoDAO.deleteProduto(material);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#insertProduto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void insertProduto(Produto produto) {
		produtoDAO.insertProduto(produto);		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#carregaProduto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Produto carregaProduto(Material material){
		return produtoDAO.carregaProduto(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Produto qtdeMinimaProduto(Integer cdmaterial){
		return produtoDAO.qtdeMinimaProduto(cdmaterial);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#updateProduto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public void updateProduto(Produto produto){
		produtoDAO.updateProduto(produto);
	}
	
	public void updateDimensoesProduto(Produto produto){
		produtoDAO.updateDimensoesProduto(produto);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#countProduto
	 * @param material
	 * @author Rodrigo Freitas
	 */
	public Long countProduto(Material material){
		return produtoDAO.countProduto(material);
	}
	
	public Produto carregaAlturaComprimentoLargura(Produto produto) {
		return produtoDAO.carregaAlturaComprimentoLargura(produto);
	}
	
	public List<Produto> findWithAlturaComprimentoLargura(String whereIn) {
		return produtoDAO.findWithAlturaComprimentoLargura(whereIn);
	}
	
	public Produto getProduto(List<Produto> listaProduto, Material material){
		if(listaProduto == null || listaProduto.isEmpty() || material == null || material.getCdmaterial() == null)
			return null;
		
		for(Produto produto : listaProduto){
			if(produto.getCdmaterial() != null && produto.getCdmaterial().equals(material.getCdmaterial()))
				return produto;
		}
		
		return null;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Long verificaProduto(Material bean) {
		return produtoDAO.verificaProduto(bean);
	}
	

	/* singleton */
	private static ProdutoService instance;
	public static ProdutoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProdutoService.class);
		}
		return instance;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#findForIntegracaoEmporium()
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param idMaterial 
	 * @since 02/04/2013
	 */
	public List<Produto> findForIntegracaoEmporium(Integer idMaterial) {
		return produtoDAO.findForIntegracaoEmporium(idMaterial);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProdutoDAO#findForCustovenda(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Produto> findForCustovenda(String whereIn) {
		return produtoDAO.findForCustovenda(whereIn);
	}

	public List<Produto> findForCalculoFormula(String whereIn) {
		return produtoDAO.findForCalculoFormula(whereIn);
	}
}
