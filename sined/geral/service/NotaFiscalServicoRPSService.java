package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.lknfe.xml.nfse.geisweb.LoteGeisWeb;
import br.com.linkcom.lknfe.xml.nfse.geisweb.LoteTomadorGeisWeb;
import br.com.linkcom.lknfe.xml.nfse.geisweb.NfseGeisWeb;
import br.com.linkcom.lknfe.xml.nfse.geisweb.TomadorGeisWeb;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.DownloadFileServlet;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoLoteRPS;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoRPS;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.NotaFiscalServicoRPSDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.NotaFiscalServicoRPSFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaFiscalServicoRPSService extends GenericService<NotaFiscalServicoRPS> {
	
	private NotaFiscalServicoRPSDAO notaFiscalServicoRPSDAO;
	private NotaFiscalServicoService notaFiscalServicoService;
	private EmpresaService empresaService;
	private NotaService notaService;
	private NotaHistoricoService notaHistoricoService;
	private NotaFiscalServicoLoteRPSService notaFiscalServicoLoteRPSService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private ConfiguracaonfeService configuracaonfeService;
	private ParametrogeralService parametrogeralService;
	private VendaService vendaService;
	
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setNotaFiscalServicoLoteRPSService(
			NotaFiscalServicoLoteRPSService notaFiscalServicoLoteRPSService) {
		this.notaFiscalServicoLoteRPSService = notaFiscalServicoLoteRPSService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setNotaHistoricoService(
			NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setNotaService(NotaService notaService) {
		this.notaService = notaService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotaFiscalServicoService(
			NotaFiscalServicoService notaFiscalServicoService) {
		this.notaFiscalServicoService = notaFiscalServicoService;
	}
	public void setNotaFiscalServicoRPSDAO(
			NotaFiscalServicoRPSDAO notaFiscalServicoRPSDAO) {
		this.notaFiscalServicoRPSDAO = notaFiscalServicoRPSDAO;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	/**
	 * Processo de gera��o de RPS
	 * 
	 * @param request
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public int gerarRPS(WebRequestContext request, String whereIn) {
		int sucesso = 0;
		
		List<NotaFiscalServico> listaNFS = notaFiscalServicoService.findForNFe(whereIn);
		for (NotaFiscalServico nfs : listaNFS) {
			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
			if(!haveVotorantim){
				request.addError("S� � poss�vel gerar RPS de empresas com configura��o do fornecedor GeisWeb cadastrada.");
				return 0;
			}
		}
		
		for (NotaFiscalServico nfs : listaNFS) {
			// Verifica se existe algum RPS j� criado para a nota
			if(this.haveRPS(nfs)){
				request.addError("Nota com ID " + nfs.getCdNota() + " j� possui RPS.");
				continue;
			}
			
			// Carrega o pr�ximo n�mero da Nota para servir como RPS
			Empresa empresa = nfs.getEmpresa();
			Integer numero = empresaService.carregaProxNumNF(empresa);
			if(numero == null){
				numero = 1;
			}
			
			// Cria o RPS e salva no banco
			NotaFiscalServicoRPS rps = new NotaFiscalServicoRPS();
			rps.setDataemissao(SinedDateUtils.currentTimestamp());
			rps.setNotaFiscalServico(nfs);
			rps.setNumero(numero);
			this.saveOrUpdate(rps);
			
			// Cria o hist�rico de nota para registro
			NotaHistorico notaHistorico = new NotaHistorico();
			notaHistorico.setNota(nfs);
			notaHistorico.setNotaStatus(NotaStatus.ALTERADA);
			notaHistorico.setObservacao("RPS gerado com o n�mero " + numero + "." + (StringUtils.isNotBlank(nfs.getNumero()) ? " RPS anterior: " + nfs.getNumero() : ""));
			notaHistoricoService.saveOrUpdate(notaHistorico);
			
			// Atualiza o n�mero da nota
			notaService.updateNumeroNota(nfs, numero);
			
			if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_NFSE)){
				try {
					vendaService.adicionarParcelaNumeroDocumento(nfs, ""+numero, true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// Atualiza o contador de n�mero de nota
			numero++;
			empresaService.updateProximoNumNF(empresa, numero);
			
			sucesso++;
		}
		
		return sucesso;
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public boolean haveRPS(NotaFiscalServico notaFiscalServico) {
		return notaFiscalServicoRPSDAO.haveRPS(notaFiscalServico);
	}
	
	/**
	 * Faz refer�ncia ao DAO
	 * 
	 * @param notaFiscalServico
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	public NotaFiscalServicoRPS findByNota(NotaFiscalServico notaFiscalServico) {
		return notaFiscalServicoRPSDAO.findByNota(notaFiscalServico);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/05/2016
	 */
	public List<NotaFiscalServicoRPS> findByNota(String whereIn) {
		return notaFiscalServicoRPSDAO.findByNota(whereIn);
	}
	
	/**
	 * Busca as notas para a gera��o de NFS-e
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/04/2016
	 */
	private List<NotaFiscalServicoRPS> findForNfe(NotaFiscalServicoRPSFiltro filtro) {
		return notaFiscalServicoRPSDAO.findForNfe(filtro);
	}
	
	public void gerarArquivoNFSe(NotaFiscalServicoRPSFiltro filtro) {
		List<NotaFiscalServicoRPS> lista = this.findForNfe(filtro);
		if(lista == null || lista.size() == 0){
			throw new SinedException("N�o existem registros para gerar o arquivo.");
		}
		
		List<String> listaErro = new ArrayList<String>();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		
		LoteGeisWeb loteGeisWeb = new LoteGeisWeb();
		for (NotaFiscalServicoRPS rps : lista) {
			NotaFiscalServico nfs = rps.getNotaFiscalServico();
			Empresa empresa = nfs.getEmpresa();
			empresaService.setInformacoesPropriedadeRural(empresa);
			Cliente cliente = nfs.getCliente();
			Municipio municipio = nfs.getMunicipioissqn();
			Money valor = nfs.getValorBruto();
			Codigotributacao codigotributacao = nfs.getCodigotributacao();
			Naturezaoperacao naturezaoperacao = nfs.getNaturezaoperacao();
			Integer numero = rps.getNumero();
			
			if(!listaCliente.contains(cliente)){
				listaCliente.add(cliente);
			}
			
			if(codigotributacao == null){
				listaErro.add("C�digo de tributa��o para o RPS " + numero + " n�o encontrado."); 
			}
			if(naturezaoperacao == null){
				listaErro.add("Natureza de opera��o para o RPS " + numero + " n�o encontrada."); 
			}
			
			NfseGeisWeb nfseGeisWeb = new NfseGeisWeb();
			nfseGeisWeb.setCnpjcpfprestador(empresa.getCpfOuCnpjValueProprietarioPrincipalOuEmpresa());
			nfseGeisWeb.setTiponfse("RPS");
			nfseGeisWeb.setSerie("U");
			nfseGeisWeb.setNumero(numero.toString());
			nfseGeisWeb.setCodigoservico(codigotributacao != null ? codigotributacao.getCodigo() : null);
			nfseGeisWeb.setTipolancamento(naturezaoperacao != null ? naturezaoperacao.getCodigonfse() : null);
			nfseGeisWeb.setDatacompetencia(nfs.getDtEmissao());
			nfseGeisWeb.setValortotal(valor != null ? valor.getValue().doubleValue() : 0d);
			nfseGeisWeb.setValorbc(valor != null ? valor.getValue().doubleValue() : 0d);
			nfseGeisWeb.setCnpjcpftomador(cliente.getCpfOuCnpjValue());
			nfseGeisWeb.setMunicipio(municipio.getNome());
			nfseGeisWeb.setDataemissao(rps.getDataemissao());
			
			String descricaoServico = nfs.getDescricaoServico();
			if(descricaoServico != null && descricaoServico.length() > 900){
				descricaoServico = "Presta��o de servi�os de recauchutagem e regenera��o de pneus";
			}
			nfseGeisWeb.setDescricaoservico(descricaoServico);
			
			boolean incidepis = nfs.getIncidepis() != null && nfs.getIncidepis();
			boolean incidecofins = nfs.getIncidecofins() != null && nfs.getIncidecofins();
			boolean incidecsll = nfs.getIncidecsll() != null && nfs.getIncidecsll();
			boolean incideir = nfs.getIncideir() != null && nfs.getIncideir();
			boolean incideinss = nfs.getIncideinss() != null && nfs.getIncideinss();
			
			Money valorPis = nfs.getValorPis();
			Money valorCofins = nfs.getValorCofins();
			Money valorCsll = nfs.getValorCsll();
			Money valorIr = nfs.getValorIr();
			Money valorInss = nfs.getValorInss();
			
			nfseGeisWeb.setPis(incidepis && valorPis != null ? valorPis.getValue().doubleValue() : 0d);
			nfseGeisWeb.setCofins(incidecofins && valorCofins != null ? valorCofins.getValue().doubleValue() : 0d);
			nfseGeisWeb.setCsll(incidecsll && valorCsll != null ? valorCsll.getValue().doubleValue() : 0d);
			nfseGeisWeb.setIrrf(incideir && valorIr != null ? valorIr.getValue().doubleValue() : 0d);
			nfseGeisWeb.setInss(incideinss && valorInss != null ? valorInss.getValue().doubleValue() : 0d);
			
			loteGeisWeb.addNfse(nfseGeisWeb);
		}
		
		if(listaErro.size() > 0){
			for (String string : listaErro) {
				NeoWeb.getRequestContext().addError(string);
			}
			throw new SinedException("Erro de valida��o.");
		}
		
		LoteTomadorGeisWeb loteTomadorGeisWeb = new LoteTomadorGeisWeb();
		for (Cliente c : listaCliente) {
			TomadorGeisWeb tomadorGeisWeb = new TomadorGeisWeb();
			
			String razaosocial = c.getRazaosocial();
			String nome = c.getNome();
			Endereco endereco = c.getEndereco();
			Telefone telefone = c.getTelefone();
			Municipio municipio = endereco != null ? endereco.getMunicipio() : null;
			Uf uf = municipio != null ? municipio.getUf() : null;
			
			tomadorGeisWeb.setCnpjcpf(c.getCpfOuCnpjValue());
			tomadorGeisWeb.setTipopessoa(c.getTipopessoa().getPrefix());
			tomadorGeisWeb.setRazaosocial(StringUtils.isNotBlank(razaosocial) ? razaosocial : nome);
			tomadorGeisWeb.setEndereco(endereco != null ? endereco.getLogradouroCompleto() : null);
			tomadorGeisWeb.setBairro(endereco != null ? endereco.getBairro() : null);
			tomadorGeisWeb.setCidade(municipio != null ? municipio.getNome() : null);
			tomadorGeisWeb.setEstado(uf != null ? uf.getSigla() : null);
			tomadorGeisWeb.setCep(endereco != null && endereco.getCep() != null ? endereco.getCep().getValue() : null);
			tomadorGeisWeb.setTelefone(telefone != null ? br.com.linkcom.neo.util.StringUtils.soNumero(telefone.getTelefone()) : null);
			tomadorGeisWeb.setEmail(c.getEmail());
			
			loteTomadorGeisWeb.addTomador(tomadorGeisWeb);
		}
		
		String stringtomador = loteTomadorGeisWeb.toString();
		Arquivo arquivotomador = new Arquivo(stringtomador.getBytes(), "TOMADOR-" + SinedUtil.datePatternForReport() + ".txt", "text/plain");
		
		String string = loteGeisWeb.toString();
		Arquivo arquivo = new Arquivo(string.getBytes(), "NFE_LOTE-" + SinedUtil.datePatternForReport() + ".txt", "text/plain");
		
		NotaFiscalServicoLoteRPS notaFiscalServicoLoteRPS = new NotaFiscalServicoLoteRPS();
		notaFiscalServicoLoteRPS.setArquivo(arquivo);
		notaFiscalServicoLoteRPS.setArquivotomador(arquivotomador);
		notaFiscalServicoLoteRPS.setDatacriacao(SinedDateUtils.currentTimestamp());
		
		arquivoDAO.saveFile(notaFiscalServicoLoteRPS, "arquivo");
		arquivoDAO.saveFile(notaFiscalServicoLoteRPS, "arquivotomador");
		arquivoService.saveOrUpdate(arquivo);
		arquivoService.saveOrUpdate(arquivotomador);
		
		notaFiscalServicoLoteRPSService.saveOrUpdate(notaFiscalServicoLoteRPS);
		
		for (NotaFiscalServicoRPS rps : lista) {
			this.updateLoteRPS(rps, notaFiscalServicoLoteRPS);
		}
		
		DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), arquivo.getCdarquivo());
		DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), arquivotomador.getCdarquivo());
		NeoWeb.getRequestContext().addMessage("Para fazer o download do arquivo para ser importado no site da prefeitura: <a href='" + SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + arquivo.getCdarquivo() + "'>RPS</a> e <a href='" + SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + arquivotomador.getCdarquivo() + "'>TOMADOR</a>.");
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param rps
	 * @param loteRPS
	 * @author Rodrigo Freitas
	 * @since 05/04/2016
	 */
	private void updateLoteRPS(NotaFiscalServicoRPS rps, NotaFiscalServicoLoteRPS loteRPS) {
		notaFiscalServicoRPSDAO.updateLoteRPS(rps, loteRPS);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/11/2016
	 */
	public List<NotaFiscalServicoRPS> findForConferenciaNfseGeisweb(Empresa empresa) {
		return notaFiscalServicoRPSDAO.findForConferenciaNfseGeisweb(empresa);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 	 
	 * @param empresa
	 * @param numero
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2017
	 */
	public NotaFiscalServicoRPS findByNumeroEmpresa(Empresa empresa, Integer numero) {
		return notaFiscalServicoRPSDAO.findByNumeroEmpresa(empresa, numero);
	}
	
	
}
