package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Licenca;
import br.com.linkcom.sined.geral.dao.LicencaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LicencaService extends GenericService<Licenca> {

	private LicencaDAO licencaDAO;
	
	public void setLicencaDAO(LicencaDAO licencaDAO) {
		this.licencaDAO = licencaDAO;
	}
	
	/**
	 * Limpa a tabela
	 * @author Thiago Gon�alves
	 */
	public void deleteAll(){
		licencaDAO.deleteAll();
	}
	
	/* singleton */
	private static LicencaService instance;
	
	public static LicencaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LicencaService.class);
		}
		return instance;
	}
}
