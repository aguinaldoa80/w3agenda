package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.dao.MaterialinspecaoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialinspecaoitemService extends GenericService<Materialinspecaoitem> {

	private MaterialinspecaoitemDAO materialinspecaoitemDAO;
	
	public void setMaterialinspecaoitemDAO(
			MaterialinspecaoitemDAO materialinspecaoitemDAO) {
		this.materialinspecaoitemDAO = materialinspecaoitemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Materialinspecaoitem> findItensDosMateriais(String whereIn) {
		return materialinspecaoitemDAO.findItensDosMateriais(whereIn);
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListaMaterialInspecaoByMaterial
	 * @param material
	 * @return
	 * @return List<Materialinspecaoitem>
	 * @author Thiago Augusto
	 * @date 04/07/2012
	 *
	 */
	public List<Materialinspecaoitem> findListaMaterialInspecaoByMaterial(Material material) {
		return materialinspecaoitemDAO.findListaMaterialInspecaoByMaterial(material);
	}
}
