package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.dao.LeadsegmentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LeadsegmentoService extends GenericService<Leadsegmento>{
	
	protected LeadsegmentoDAO leadsegmentoDAO;
	
	public void setLeadsegmentoDAO(LeadsegmentoDAO leadsegmentoDAO) {
		this.leadsegmentoDAO = leadsegmentoDAO;
	}
	
	
	
	public List<Leadsegmento> findByLead(Lead lead) {
		return leadsegmentoDAO.findByLead(lead);
	}

}
