package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.ReservaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ReservaService extends GenericService<Reserva> {

	private ReservaDAO reservaDAO;
	private MaterialService materialService;
	private MaterialrelacionadoService materialrelacionadoService;
	
	public void setReservaDAO(ReservaDAO reservaDAO) {
		this.reservaDAO = reservaDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {
		this.materialrelacionadoService = materialrelacionadoService;
	}
	
	public static ReservaService instance;
	
	public static ReservaService getInstance(){
		if (instance==null){
			return Neo.getObject(ReservaService.class);
		}else {
			return instance;
		}
	}
	
	public List<Reserva> findByProducaoagenda(String whereIn){
		return reservaDAO.findByProducaoagenda(whereIn);
	}
	
	public void deleteReservaByProducaoagenda(String whereIn){
		reservaDAO.deleteReservaByProducaoagenda(whereIn);
	}
	
	public void deleteReservaByPedidovenda(String whereIn){
		reservaDAO.deleteReservaByPedidovenda(whereIn);
	}
	
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque, String whereNotInReserva){
		return reservaDAO.getQtdeReservada(material, empresa, localarmazenagem, loteestoque, whereNotInReserva);
	}
	
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, String whereNotInReserva){
		return getQtdeReservada(material, empresa, localarmazenagem, null, whereNotInReserva);
	}
	
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem){
		return getQtdeReservada(material, empresa, localarmazenagem, null, null);
	}
	
	public void deleteByOrdemservicoveterinariaMaterial(String whereInOSVM){
		reservaDAO.deleteByOrdemservicoveterinariaMaterial(whereInOSVM);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInOSV
	 * @author Rodrigo Freitas
	 * @since 14/11/2016
	 */
	public void deleteByOrdemservicoveterinaria(String whereInOSV) {
		reservaDAO.deleteByOrdemservicoveterinaria(whereInOSV);
	}

	public void excluirReservaOSVM(Pedidovenda pedidovenda) {
		if(pedidovenda != null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
			for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
				if(StringUtils.isNotBlank(pvm.getWhereInOSVM())){
					deleteByOrdemservicoveterinariaMaterial(pvm.getWhereInOSVM());
				}
			}
		}
		
	}
	
	public Reserva createUpdateReserva(Ordemservicoveterinariamaterial item, Ordemservicoveterinaria bean, Material material, Material materialmestre, Localarmazenagem localarmazenagem, Empresa empresa, Loteestoque loteestoque, Double quantidade){
		Reserva reserva = item.getReservaMaterial(material);
		if (reserva==null){
			reserva = new Reserva();
		}
		
//		reserva.setMaterialmestre(materialmestre);
		reserva.setLocalarmazenagem(localarmazenagem);
		reserva.setEmpresa(empresa);
		reserva.setLoteestoque(loteestoque);
		reserva.setMaterial(material);
		reserva.setOrdemservicoveterinaria(bean);
		reserva.setOrdemservicoveterinariamaterial(item);
		reserva.setQuantidade(quantidade);
		reserva.setUnidademedida(item.getMaterial().getUnidademedida());
		saveOrUpdate(reserva);
		
		return reserva;
	}
	
	public void createUpdateReserva(Ordemservicoveterinariamaterial item, Ordemservicoveterinaria bean, Localarmazenagem localarmazenagem, Double quantidade) {
		if(item != null){
			List<Reserva> listaReserva = new ArrayList<Reserva>();
			if(quantidade != null && item.getMaterial() != null && ((bean.getAtividadetipoveterinaria() != null && bean.getAtividadetipoveterinaria().getLocalarmazenagem() != null) || localarmazenagem != null)){
				if(localarmazenagem == null){
					localarmazenagem = bean.getAtividadetipoveterinaria().getLocalarmazenagem();
				}
				Material material = materialService.findForReserva(item.getMaterial());
				if(material != null){
					if(Boolean.TRUE.equals(material.getVendapromocional())){
						if(SinedUtil.isListNotEmpty(material.getListaMaterialrelacionado())){
							try{
								materialrelacionadoService.calculaQuantidadeKit(material);			
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
								if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null && 
										(materialrelacionado.getMaterialpromocao().getServico() == null || !materialrelacionado.getMaterialpromocao().getServico())){
									listaReserva.add(createUpdateReserva(item, bean, materialrelacionado.getMaterialpromocao(), material, localarmazenagem, bean.getEmpresa(), item.getLoteestoque(), materialrelacionado.getQuantidade() * quantidade));
								}
							}
						}
					}else {
						listaReserva.add(createUpdateReserva(item, bean, material, null, localarmazenagem, bean.getEmpresa(), item.getLoteestoque(), item.getQtdeusada()));
					}
					
				}
			}
		}
	}

	public Reserva createUpdateReserva(Pedidovendamaterial item, Pedidovenda bean, Material material, Localarmazenagem localarmazenagem, Empresa empresa, Loteestoque loteestoque, Double quantidade){
		if(materialService.isServico(material)){
			return null;
		}
		Reserva reserva = this.loadByPedidovendamaterial(item.getCdpedidovendamaterial(), material);
		if (reserva==null){
			reserva = new Reserva();
			reserva.setQuantidade(quantidade);
		}else{
			reserva.setQuantidade(reserva.getQuantidade()+quantidade);
		}
		Unidademedida un = material.getUnidademedida() != null? material.getUnidademedida(): new Unidademedida(materialService.getIdUnidadeMedida(material));
		reserva.setLocalarmazenagem(localarmazenagem);
		reserva.setEmpresa(empresa);
		reserva.setLoteestoque(loteestoque);
		reserva.setMaterial(material);
		reserva.setPedidovenda(bean);
		reserva.setPedidovendamaterial(item);
		
		reserva.setUnidademedida(un);
		saveOrUpdate(reserva);
		
		return reserva;		
	}
	
	public Reserva createUpdateReserva(Vendamaterial item, Venda bean, Material material, Localarmazenagem localarmazenagem, Empresa empresa, Loteestoque loteestoque, Double quantidade){
		if(materialService.isServico(material)){
			return null;
		}
		Reserva reserva = this.loadByVendamaterial(item.getCdvendamaterial(), material);
		if (reserva==null){
			reserva = new Reserva();
			reserva.setQuantidade(quantidade);
		}else{
			reserva.setQuantidade(reserva.getQuantidade()+quantidade);
		}
		Unidademedida un = material.getUnidademedida() != null? material.getUnidademedida(): new Unidademedida(materialService.getIdUnidadeMedida(material));
		reserva.setLocalarmazenagem(localarmazenagem);
		reserva.setEmpresa(empresa);
		reserva.setLoteestoque(loteestoque);
		reserva.setMaterial(material);
		reserva.setVenda(bean);
		reserva.setVendamaterial(item);
		reserva.setUnidademedida(un);
		saveOrUpdate(reserva);
		
		return reserva;		
	}
	
	public Reserva createUpdateReserva(Expedicaoitem item, Expedicao bean, Material material, Localarmazenagem localarmazenagem, Empresa empresa, Loteestoque loteestoque, Double quantidade){
		if(materialService.isServico(material)){
			return null;
		}
		Reserva reserva = this.loadByExpedicaoitem(item.getCdexpedicaoitem(), material);
		if (reserva==null){
			reserva = new Reserva();
			reserva.setQuantidade(quantidade);
		}else{
			reserva.setQuantidade(reserva.getQuantidade()+quantidade);
		}
		Unidademedida un = material.getUnidademedida() != null? material.getUnidademedida(): new Unidademedida(materialService.getIdUnidadeMedida(material));
		reserva.setLocalarmazenagem(localarmazenagem);
		reserva.setEmpresa(empresa);
		reserva.setLoteestoque(loteestoque);
		reserva.setMaterial(material);
		reserva.setExpedicao(bean);
		reserva.setExpedicaoitem(item);
		reserva.setUnidademedida(un);
		saveOrUpdate(reserva);
		
		return reserva;		
	}
	
	public void updateReserva(Reserva reserva){
		
	}
	
	public Reserva loadByPedidovendamaterial(Integer cdpedidovendamaterial, Material material){
		if(cdpedidovendamaterial == null){
			return null;
		}
		return reservaDAO.loadByPedidovendamaterial(cdpedidovendamaterial, material);
	}
	
	public Reserva loadByVendamaterial(Integer cdvendamaterial, Material material){
		if(cdvendamaterial == null){
			return null;
		}
		return reservaDAO.loadByVendamaterial(cdvendamaterial, material);
	}
	
	public Reserva loadByExpedicaoitem(Integer cdexpedicaoitem, Material material){
		if(cdexpedicaoitem == null){
			return null;
		}
		return reservaDAO.loadByExpedicaoitem(cdexpedicaoitem, material);
	}
	
	public void desfazerReserva(Pedidovendamaterial pedidovendamaterial, Material material, Double qtde){
		Reserva bean = null;
		if(pedidovendamaterial != null && pedidovendamaterial.getCdpedidovendamaterial() != null &&
			material != null && material.getCdmaterial() != null){
			bean = loadByPedidovendamaterial(pedidovendamaterial.getCdpedidovendamaterial(), material);
		}
		desfazerReserva(bean, qtde);
		
	}
	
	public void desfazerReserva(Vendamaterial vendamaterial, Material material, Double qtde){
		Reserva bean = null;
		if(vendamaterial != null && vendamaterial.getCdvendamaterial() != null &&
			material != null && material.getCdmaterial() != null){
			bean = loadByVendamaterial(vendamaterial.getCdvendamaterial(), material);
		}
		desfazerReserva(bean, qtde);
	}

	public void desfazerReserva(Expedicaoitem expedicaoitem, Material material, Double qtde){
		Reserva bean = null;
		if(expedicaoitem != null && expedicaoitem.getCdexpedicaoitem() != null &&
			material != null && material.getCdmaterial() != null){
			bean = loadByExpedicaoitem(expedicaoitem.getCdexpedicaoitem(), material);			
		}
		desfazerReserva(bean, qtde);
	}
	
	public void desfazerReserva(Reserva bean, Double qtde){
		reservaDAO.desfazerReserva(bean, qtde);
	}
	
	public boolean existsReserva(Pedidovendamaterial pedidovendamaterial, Material material){
		return pedidovendamaterial != null && pedidovendamaterial.getCdpedidovendamaterial() != null && loadByPedidovendamaterial(pedidovendamaterial.getCdpedidovendamaterial(), material) != null;
	}
	
	public boolean existsReserva(Vendamaterial vendamaterial, Material material){
		return vendamaterial != null && vendamaterial.getCdvendamaterial() != null && loadByVendamaterial(vendamaterial.getCdvendamaterial(), material) != null;
	}
	
	public boolean existsReserva(Expedicaoitem expedicaoitem, Material material){
		return expedicaoitem != null && expedicaoitem.getCdexpedicaoitem() != null && loadByExpedicaoitem(expedicaoitem.getCdexpedicaoitem(), material) != null;
	}
	
	public List<Reserva> findByPedidovenda(Pedidovenda pedidovenda, Material material){
		return reservaDAO.findByPedidovenda(pedidovenda, material);
	}
	
	public List<Reserva> findByVenda(Venda venda, Material material){
		return reservaDAO.findByVenda(venda, material);
	}
	
	public String montaWhereInReservas(Pedidovenda pedidovenda, Material material){
		List<Reserva> reservas = this.findByPedidovenda(pedidovenda, material);
		if(reservas != null && SinedUtil.isListNotEmpty(reservas)){
			return CollectionsUtil.listAndConcatenate(reservas, "cdreserva", ",");
		}
		return null;
	}
	
	public String montaWhereInReservas(Venda venda, Material material){
		List<Reserva> reservas = this.findByVenda(venda, material);
		if(reservas != null && SinedUtil.isListNotEmpty(reservas)){
			return CollectionsUtil.listAndConcatenate(reservas, "cdreserva", ",");
		}
		return null;
	}
	
	public List<Reserva> recuperarReservaPorMaterial(Integer codMaterial,Integer cdEmpresa, Integer cdLocalarmazenagem){
		return reservaDAO.findByMaterial(codMaterial,cdEmpresa,cdLocalarmazenagem);
	}
	public boolean existReservaPedidoVenda(Pedidovenda pv) {
		return reservaDAO.existReservaPedidoVenda(pv);
	}
	public Double getReservaByVendamaterialOrExpedicaoitem(Vendamaterial vm, Expedicaoitem ei, Material material){
		return getReservaByVendamaterialOrExpedicaoitem(vm, ei, material, null);
	}
	public Double getReservaByVendamaterialOrExpedicaoitem(Vendamaterial vm, Expedicaoitem ei, Material material, Loteestoque loteestoque){
		Double qtdeReserva = 0D;
		
		Reserva reserva = this.findByVendamaterial(vm, material);
		if(reserva != null && (loteestoque == null || loteestoque.equals(reserva.getLoteestoque()))){
			qtdeReserva += reserva.getQuantidade();
		}
		
		reserva = this.findByExpedicaoitem(ei, material);
		if(reserva != null && (loteestoque == null || loteestoque.equals(reserva.getLoteestoque()))){
			qtdeReserva += reserva.getQuantidade();
		}
		return qtdeReserva;
	}
	
	public Reserva findByVendamaterial(Vendamaterial vendamaterial, Material material){
		return reservaDAO.findByVendamaterial(vendamaterial, material);
	}
	
	public Reserva findByExpedicaoitem(Expedicaoitem expedicaoitem, Material material){
		return reservaDAO.findByExpedicaoitem(expedicaoitem, material);
	}
}