package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Filtro;
import br.com.linkcom.sined.geral.dao.FiltroDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FiltroService extends GenericService<Filtro> {

	private static FiltroService instance;
	public static FiltroService getInstance(){
		if(instance == null){
			instance = NeoWeb.getObject(FiltroService.class);
		}
		return instance;
	}
	private FiltroDAO filtroDAO;
	public void setFiltroDAO(FiltroDAO filtroDAO) {this.filtroDAO = filtroDAO;}
	
	public Filtro loadByCdfiltro(WebRequestContext request) {
		Filtro filtroBean = new Filtro();
		Integer cdfiltro = Integer.parseInt(request.getParameter("__cdfiltro__"));
		filtroBean.setCdfiltro(cdfiltro);
		filtroBean = load(filtroBean);
		return filtroBean;
	}
	
	public List<Filtro> findForSinedCrudListagem(WebRequestContext request){
		String path = request.getServletRequest().getRequestURI().replace("/w3erp", "");
		if(path.contains("?"))
			path = path.substring(0, path.lastIndexOf("?"));
		
		return filtroDAO.findByTelaPath(path);
	}

}
