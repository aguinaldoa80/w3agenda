package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Obslancamentofiscal;
import br.com.linkcom.sined.geral.dao.ObslancamentofiscalDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ObslancamentofiscalService extends GenericService<Obslancamentofiscal> {

	private ObslancamentofiscalDAO obslancamentofiscalDAO;
	
	public void setObslancamentofiscalDAO(ObslancamentofiscalDAO obslancamentofiscalDAO) {
		this.obslancamentofiscalDAO = obslancamentofiscalDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ObslancamentofiscalDAO#existeDescricao(Obslancamentofiscal obslancamentofiscal)
	*
	* @param obslancamentofiscal
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public boolean existeDescricao(Obslancamentofiscal obslancamentofiscal) {
		return obslancamentofiscalDAO.existeDescricao(obslancamentofiscal);
	}
}
