package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Arquivonf;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaonfeService extends GenericService<Configuracaonfe> {

	private ConfiguracaonfeDAO configuracaonfeDAO;

	public void setConfiguracaonfeDAO(ConfiguracaonfeDAO configuracaonfeDAO) {
		this.configuracaonfeDAO = configuracaonfeDAO;
	}
	
	/**
	 * Atualiza o contador de lote de evento.
	 *
	 * @param configuracaonfe
	 * @param contador
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateContadorloteevento(Configuracaonfe configuracaonfe, Integer contador){
		configuracaonfeDAO.updateContadorloteevento(configuracaonfe, contador);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO#findByTipo(Tipoconfiguracaonfe tipoconfiguracaonfe)
	 *
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 25/01/2012
	 * @author Rodrigo Freitas
	 */
	public List<Configuracaonfe> findByTipo(Tipoconfiguracaonfe tipoconfiguracaonfe){
		return configuracaonfeDAO.findByTipo(tipoconfiguracaonfe);
	}
	
	public List<Configuracaonfe> findForComboMDFe(){
		return findByTipo(Tipoconfiguracaonfe.MDFE);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 30/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Configuracaonfe> findAtivosByTipoEmpresa(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa){
		return configuracaonfeDAO.findAtivosByTipoEmpresa(tipoconfiguracaonfe, empresa);
	}
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 30/10/2015
	 * @author C�sar
	 */
	public List<Configuracaonfe> findAtivosByTipoEmpresaPrefixowebservice(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa){
		return configuracaonfeDAO.findAtivosByTipoEmpresaPrefixowebservice(tipoconfiguracaonfe, empresa);
	}

	/**
	 * M�todo respons�vel por buscar todos os estados configurado para empresa e setar no request
	 * @param request
	 * @author C�sar
	 */
	public void setEstadosConfigNFe(WebRequestContext request) {
		List<Configuracaonfe> listaConfiguracaoNfe =  this.findAtivosByTipoEmpresaPrefixowebservice(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, null);
		String estado = null;
		for (Configuracaonfe configuracaonfe : listaConfiguracaoNfe) {
			if(estado == null){
				estado = configuracaonfe.getPrefixowebservice() != null ? configuracaonfe.getPrefixowebservice().getEstadoservico() : "";
			}else{
				estado = estado + "," + (configuracaonfe.getPrefixowebservice() != null ? configuracaonfe.getPrefixowebservice().getEstadoservico() : "");
			}
		}
		request.setAttribute("estado", estado);
	}	
	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 10/08/2015
	 */
	public List<Configuracaonfe> findServicoAtivosByEmpresa(Empresa empresa){
		return configuracaonfeDAO.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO, empresa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/01/2017
	 */
	public List<Configuracaonfe> findProdutoAtivosByEmpresa(Empresa empresa){
		return configuracaonfeDAO.findAtivosByTipoEmpresa(Tipoconfiguracaonfe.NOTA_FISCAL_PRODUTO, empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO#findByArquivonf(Arquivonf arquivonf)
	 *
	 * @param arquivonf
	 * @return
	 * @since 13/02/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe findByArquivonf(Arquivonf arquivonf) {
		return configuracaonfeDAO.findByArquivonf(arquivonf);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO#findByArquivonfnota(Arquivonfnota arquivonfnota)
	 *
	 * @param arquivonfnota
	 * @return
	 * @since 29/02/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe findByArquivonfnota(Arquivonfnota arquivonfnota) {
		return configuracaonfeDAO.findByArquivonfnota(arquivonfnota);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO#havePadrao(Integer cdconfiguracaonfe, Tipoconfiguracaonfe tipoconfiguracaonfe)
	 *
	 * @param cdconfiguracaonfe
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 * @param empresa 
	 */
	public boolean havePadrao(Integer cdconfiguracaonfe, Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa) {
		return configuracaonfeDAO.havePadrao(cdconfiguracaonfe, tipoconfiguracaonfe, empresa);
	}
	
	public boolean havePrefixoativo(Empresa empresa, Prefixowebservice... prefixowebservice){
		return configuracaonfeDAO.havePrefixoativo(empresa, prefixowebservice);
	}
	
	public boolean havePrefixoativo(Prefixowebservice... prefixowebservice){
		return this.havePrefixoativo(null, prefixowebservice);
	}
	
	/**
	 * M�todo criado para na tela de Configura��o de NFe exibir somente os prefixos certos.
	 *
	 * @param tipoconfiguracaonfe
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Prefixowebservice> findByTipoconfiguracaonfe(Tipoconfiguracaonfe tipoconfiguracaonfe){
		Prefixowebservice[] prefixos = Prefixowebservice.values();
		List<Prefixowebservice> retorno = new ArrayList<Prefixowebservice>();
		
		for (int i = 0; i < prefixos.length; i++) {
			if (tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.NOTA_FISCAL_SERVICO)) {
				if (prefixos[i].isNfservico())
					retorno.add(prefixos[i]);
			} else if(tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.MDFE)) {
				if (prefixos[i].isMdfe())
					retorno.add(prefixos[i]);
			} else if (tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.DDFE)) {
				if (prefixos[i].isDde())
					retorno.add(prefixos[i]);
			} else if (tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.MD)) {
				if (prefixos[i].isMd())
					retorno.add(prefixos[i]);
			} else {
				if (!prefixos[i].isNfservico() && !prefixos[i].isMdfe() && !prefixos[i].isDde() && !prefixos[i].isMd()){
					if(!tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR) || 
							(tipoconfiguracaonfe.equals(Tipoconfiguracaonfe.NOTA_FISCAL_CONSUMIDOR) && 
									!Prefixowebservice.SVAN_PROD.equals(prefixos[i]) &&
									!Prefixowebservice.SVAN_HOM.equals(prefixos[i]))){
						retorno.add(prefixos[i]);
					}
				}
			}
		}
		
		return retorno;
	}
	
	/**
	 * Carrega e atualiza o pr�ximo n�mero de NF-e da empresa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaProxNumLoteNFe
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumLoteNFe
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer getNextNumLoteNfe(Configuracaonfe configuracaonfe){
		Integer proxNum = configuracaonfeDAO.carregaProxNumLoteNFe(configuracaonfe);
		
		if(proxNum == null){
			proxNum = 1;
		} else {
			proxNum++;
		}
		
		configuracaonfeDAO.updateProximoNumLoteNFe(configuracaonfe, proxNum);
		return proxNum;
	}
	
	/**
	 * Carrega e atualiza o pr�ximo n�mero de NFS-e da empresa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaProxNumLoteNFSe
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumLoteNFSe
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer getNextNumLoteNfse(Configuracaonfe configuracaonfe){
		Integer proxNum = configuracaonfeDAO.carregaProxNumLoteNFSe(configuracaonfe);
		
		if(proxNum == null){
			proxNum = 1;
		} else {
			proxNum++;
		}
		
		configuracaonfeDAO.updateProximoNumLoteNFSe(configuracaonfe, proxNum);
		return proxNum;
	}
	
	/**
	 * Carrega e atualiza o pr�ximo n�mero de NF-e da empresa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#carregaProxNumNFe
	 * @see br.com.linkcom.sined.geral.dao.EmpresaDAO#updateProximoNumNFe
	 * 
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Integer getNextNumNfe(Configuracaonfe configuracaonfe){
		Integer proxNum = configuracaonfeDAO.carregaProxNumNFe(configuracaonfe);
		
		if(proxNum == null){
			proxNum = 1;
		} else {
			proxNum++;
		}
		
		configuracaonfeDAO.updateProximoNumNFe(configuracaonfe, proxNum);
		return proxNum;
	}
	
	public Integer getNextNumMdfe(Configuracaonfe configuracaonfe){
		Integer proxNum = configuracaonfeDAO.carregaProxNumMdfe(configuracaonfe);
		
		if(proxNum == null){
			proxNum = 1;
		} else {
			proxNum++;
		}
		
		configuracaonfeDAO.updateProximoNumMdfe(configuracaonfe, proxNum);
		return proxNum;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ConfiguracaonfeDAO#getConfiguracaoPadrao(Tipoconfiguracaonfe tipoconfiguracaonfe, Empresa empresa)
	 *
	 * @param tipo
	 * @param empresa
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 */
	public Configuracaonfe getConfiguracaoPadrao(Tipoconfiguracaonfe tipo, Empresa empresa) {
		return configuracaonfeDAO.getConfiguracaoPadrao(tipo, empresa);
	}

	public List<Configuracaonfe> findAllForWebservice() {
		return configuracaonfeDAO.findAllForWebservice();
	}

	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @param empresa
	 * @author Danilo Guimar�es
	 */
	public List<Configuracaonfe> listaConfiguracaoNfproduto(Empresa empresa){
		return configuracaonfeDAO.listaConfiguracaoNfproduto(empresa);
	}

	public Configuracaonfe findByArquivoMdfe(ArquivoMdfe arquivoMdfe) {
		return configuracaonfeDAO.findByArquivoMdfe(arquivoMdfe);
	}

	public void updateUltNsu(Configuracaonfe configuracaoNfe) {
		configuracaonfeDAO.updateUltNsu(configuracaoNfe);
	}

	public Configuracaonfe carregaConfiguracao(Configuracaonfe configuracaonfe) {
		return configuracaonfeDAO.carregaConfiguracao(configuracaonfe);
	}

	public List<Configuracaonfe> procurarDde() {
		return configuracaonfeDAO.procurarDde();
	}

	public List<Configuracaonfe> procurarMd(Empresa empresa, Configuracaonfe configuracaonfe, boolean producao) {
		return configuracaonfeDAO.procurarMd(empresa, configuracaonfe, producao);
	}
	
	public List<Configuracaonfe> buscaPorModeloEmpresa(ModeloDocumentoFiscalEnum modelo, Empresa empresa) {
		return configuracaonfeDAO.buscaPorModeloEmpresa(modelo, empresa);
	}
}
