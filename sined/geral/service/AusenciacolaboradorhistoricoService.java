package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Ausenciacolaborador;
import br.com.linkcom.sined.geral.bean.Ausenciacolaboradorhistorico;
import br.com.linkcom.sined.geral.dao.AusenciacolaboradorhistoricoDAO;

public class AusenciacolaboradorhistoricoService extends GenericService<Ausenciacolaboradorhistorico> {
	
	protected AusenciacolaboradorhistoricoDAO ausenciacolaboradorhistoricoDAO;

	public AusenciacolaboradorhistoricoDAO getAusenciacolaboradorhistoricoDAO() {
		return ausenciacolaboradorhistoricoDAO;
	}

	public void setAusenciacolaboradorhistoricoDAO(AusenciacolaboradorhistoricoDAO ausenciacolaboradorhistoricoDAO) {
		this.ausenciacolaboradorhistoricoDAO = ausenciacolaboradorhistoricoDAO;
	}
	
	public List<Ausenciacolaboradorhistorico> findByAusenciacolaborador(Ausenciacolaborador ausenciacolaborador){
		return ausenciacolaboradorhistoricoDAO.findByAusenciacolaborador(ausenciacolaborador);
	}
	
	

}
