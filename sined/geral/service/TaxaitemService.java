package br.com.linkcom.sined.geral.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.dao.TaxaitemDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TaxaitemService extends GenericService<Taxaitem>{
	private TaxaitemDAO taxaitemDAO;
	private ContaService contaService;
	
	public void setTaxaitemDAO(TaxaitemDAO taxaitemDAO) {this.taxaitemDAO = taxaitemDAO;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 * 
	 * @author Taidson Santos
	 * @since 20/05/2010	 
	 */
	public Taxaitem findByTaxaMulta(Taxa taxa){
		return taxaitemDAO.findByTaxaMulta(taxa);
	}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 * 
	 * @author Taidson Santos
	 * @since 20/05/2010	 
	 */
	public Taxaitem findByTaxaJuros(Taxa taxa){
		return taxaitemDAO.findByTaxaJuros(taxa);
	}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 * 
	 * @author Taidson Santos
	 * @since 01/06/2010 
	 */
	public List<Taxaitem> findTaxasItens (Taxa taxa){
		return taxaitemDAO.findTaxasItens(taxa);
	}
	
	/**
	 *<p>M�todo de refer�ncia ao DAO</p> 
	 * 
	 * @author Marden Silva
	 * @since 16/08/2011	 
	 */
	public Taxaitem findByTaxaDesconto(Taxa taxa){
		return taxaitemDAO.findByTaxaDesconto(taxa);
	}

	/**
	 * C�lcula o percentual/valor do tipo de taxa que foi passado por par�metro.
	 *
	 * @see br.com.linkcom.sined.geral.service.TaxaitemService#findTaxasItens(Taxa taxa)
	 * 
	 * @param d
	 * @param multa
	 * @return
	 * @since 28/10/2011
	 * @author Rodrigo Freitas
	 */
	public Double getValorPercentualTipotaxa(Documento d, Tipotaxa multa, boolean percentual) {
		Double valor = 0.0;
		Taxa taxa = d.getTaxa();
		if(taxa != null){
			List<Taxaitem> lista = this.findTaxasItens(taxa);
			if(lista != null && lista.size() > 0){
				Taxaitem tx = null;
				for (Taxaitem taxaitem : lista) {
					if(taxaitem.getTipotaxa() != null && taxaitem.getTipotaxa().equals(multa)){
						tx = taxaitem;
						break;
					}
				}
				if(tx != null && tx.getValor() != null){
					if(percentual){
						if(tx.isPercentual()){
							valor = tx.getValor().getValue().doubleValue();
						} else {
							if(d.getValor() != null){
								valor = (tx.getValor().getValue().doubleValue() * 100.0)/d.getValor().getValue().doubleValue();
							}
						}
					} else {
						if(tx.isPercentual()){
							if(d.getValor() != null){
								valor = (d.getValor().getValue().doubleValue()*tx.getValor().getValue().doubleValue())/100.0;
							}
						} else {
							valor = tx.getValor().getValue().doubleValue();
						}
					}
				}
			}
		}
		return valor;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 15/03/2012
	 * @param taxa
	 * @return
	 */
	public List<Taxaitem> findForTaxa(Taxa taxa){
		return taxaitemDAO.findForTaxa(taxa);
	}
	
	public void atualizaDataVencimento(String selectedItens, Date dtlimite){
		taxaitemDAO.atualizaDataVencimento(selectedItens,dtlimite);
	}

	public void updateDtlimite(Taxaitem taxaitem, java.sql.Date dtlimite) {
		taxaitemDAO.updateDtlimite(taxaitem, dtlimite);
	}
	
	/**
	* M�todo que retorna o valor com a taxa de baixa
	*
	* @param valorboleto
	* @param listaTaxaItem
	* @param dtemissao
	* @return
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	public Money getValorTaxaBaixaDesconto(Money valorboleto, Set<Taxaitem> listaTaxaItem, java.sql.Date dtemissao) {
		if(dtemissao != null){
			return getValorTaxaBaixaDesconto(valorboleto, listaTaxaItem, new Date(dtemissao.getTime()));
		}
		return valorboleto;
	}

	/**
	* M�todo que retorna o valor com a taxa de baixa
	*
	* @param valorboleto
	* @param listaTaxaItem
	* @param dtemissao
	* @return
	* @since 23/08/2016
	* @author Luiz Fernando
	*/
	public Money getValorTaxaBaixaDesconto(Money valorboleto, Set<Taxaitem> listaTaxaItem, Date dtemissao) {
		if(valorboleto != null && SinedUtil.isListNotEmpty(listaTaxaItem) && dtemissao != null){
			for(Taxaitem item : listaTaxaItem){
				if (item.getTipotaxa().equals(Tipotaxa.TAXABAIXA_DESCONTO)){
					if(item.getDtlimite() != null && SinedDateUtils.beforeOrEqualIgnoreHour(item.getDtlimite(), dtemissao)){
						if(item.isPercentual()){
							valorboleto = valorboleto.subtract(valorboleto.multiply(item.getValor().divide(new Money(100d))));
						} else {
							valorboleto = valorboleto.subtract(item.getValor());
						}
					}
				}
			}
		}
		return valorboleto;
	}
	
	public Money getValorTaxaBaixaAcrescimo(Money valorboleto, Set<Taxaitem> listaTaxaItem, java.sql.Date dtreferencia) {
		if(dtreferencia != null){
			return getValorTaxaBaixaAcrescimo(valorboleto, listaTaxaItem, new Date(dtreferencia.getTime()));
		}
		return valorboleto;
	}
	
	public Money getValorTaxaBaixaAcrescimo(Money valorboleto, Set<Taxaitem> listaTaxaItem, Date dtreferencia) {
		if(valorboleto != null && SinedUtil.isListNotEmpty(listaTaxaItem) && dtreferencia != null){
			for(Taxaitem item : listaTaxaItem){
				if (item.getTipotaxa().equals(Tipotaxa.TAXABAIXA_ACRESCIMO)){
					if(item.getDtlimite() != null && SinedDateUtils.beforeOrEqualIgnoreHour(item.getDtlimite(), dtreferencia)){
						if(item.isPercentual()){
							valorboleto = valorboleto.add(valorboleto.multiply(item.getValor().divide(new Money(100d))));
						} else {
							valorboleto = valorboleto.add(item.getValor());
						}
					}
				}
			}
		}
		return valorboleto;
	}
	
	/**
	* M�todo que verifica se j� existe o tipo de taxa 
	*
	* @param taxa
	* @param tipotaxa
	* @return
	* @since 28/03/2017
	* @author Luiz Fernando
	*/
	public boolean existeTaxaitem(Taxa taxa, Tipotaxa tipotaxa){
		if(taxa != null && tipotaxa != null && Hibernate.isInitialized(taxa.getListaTaxaitem()) && SinedUtil.isListNotEmpty(taxa.getListaTaxaitem())){
			return existeTaxaitem(taxa.getListaTaxaitem(), tipotaxa);
		}
		return false;
	}
	
	public boolean existeTaxaitem(Set<Taxaitem> lista, Tipotaxa tipotaxa){
		if(Hibernate.isInitialized(lista) && SinedUtil.isListNotEmpty(lista)){
			for(Taxaitem ti : lista){
				if(tipotaxa.equals(ti.getTipotaxa())){
					return true;
				}
			}
		}
		return false;
	}

	public Taxaitem loadTaxaitemByDocumento(Documento documento, Tipotaxa tipotaxa) {
		return taxaitemDAO.loadTaxaitemByDocumento(documento, tipotaxa);
	}

	public String criaMotivoValorTaxa(java.sql.Date data, Money valortaxa) {
		String motivo = ""; 
		if(valortaxa != null){
			if(data != null){
				motivo = new SimpleDateFormat("dd/MM/yyyy").format(data) + " - R$ ";
			}
			motivo += valortaxa.toString();
		}
		return motivo;
	}

	public void updateValorMotivo(Taxaitem taxaitem, Money valor, Date dtlimite, String motivo) {
		taxaitemDAO.updateValorMotivo(taxaitem, valor, dtlimite, motivo);
	}
	
	public void deleteFromArquivoretornoAtualizarJurosMulta(Taxa taxa) {
		if (taxa!=null && taxa.getCdtaxa()!=null){
			taxaitemDAO.deleteFromArquivoretornoAtualizarJurosMulta(taxa);
		}
	}
	
	public List<Taxaitem> findTaxaitemByDocumento(Documento documento, Tipotaxa... tipotaxa) {
		return taxaitemDAO.findTaxaitemByDocumento(documento, tipotaxa);
	}
	
	public Money calcula_juros_mes(Money valor, Money valortaxa, int difdias, Conta conta) {
		Money v_retorno = new Money();
		
		if(valor != null && valortaxa != null){
			Double p_valor = valor.getValue().doubleValue();
			Double p_valortaxa = valortaxa.getValue().doubleValue();
			int p_difdias = difdias;
			
			Conta contabancaria = conta != null ? contaService.load(conta, "conta.cdconta, conta.banco") : null;
			String numero_banco = contabancaria != null && contabancaria.getBanco() != null && contabancaria.getBanco().getNumero() != null ? contabancaria.getBanco().getNumero().toString() : null;
			
			Integer qntDiaMesAtual = SinedDateUtils.getDia(SinedDateUtils.addDiasData(SinedDateUtils.addMesData(SinedDateUtils.firstDateOfMonth(), 1), -1));
			
			if("756".equals(numero_banco)){
				v_retorno =  new Money(new Double(SinedUtil.truncateFormat((p_valor * ((p_valortaxa / 30) * p_difdias))/100d, 2)));
			}else {
				v_retorno = new Money(new Double(SinedUtil.truncateFormat(((p_valor * ((p_valortaxa / qntDiaMesAtual) * p_difdias))/100d), 4)));
			    
			    String v_decimal = new DecimalFormat("#,##0.0000").format(v_retorno.getValue().doubleValue()).replaceAll("\\.", "").replace(",", ".").split("\\.")[1];
			    
			    if(v_decimal != null && v_decimal.length() > 2){ 
			    	Integer v_valorterceiraposicao = Integer.parseInt(v_decimal.substring(2, 3));
			        if(v_valorterceiraposicao < 5){
			        	v_retorno = new Money(new Double(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue(), 2)));
			        }else if(v_valorterceiraposicao == 5){
			        	Integer v_valorsegundaposicao = Integer.parseInt(v_decimal.substring(1, 2));
			            if(v_valorsegundaposicao % 2 != 0) {
			            	v_retorno = new Money(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue() + 0.01d, 2));
			            }else {
			            	if (v_decimal.length() < 4){
			                	v_retorno = new Money(new Double(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue(), 2)));
			            	}else {
			                	Integer v_valorquartaposicao = Integer.parseInt(v_decimal.substring(3, 4));
			                	if(v_valorquartaposicao == 0){ 
			                    	v_retorno = new Money(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue(), 2));
			                	}else {
			                        v_retorno = new Money(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue() + 0.01d, 2));
			                    }
			                }
			            }
			        } else { 
			            v_retorno = new Money(SinedUtil.truncateFormat(v_retorno.getValue().doubleValue() + 0.01d, 2));
			        }
			    }
			}
		}
		
		return v_retorno;
	}
	
	public Money calcula_juros_dia(Money valor, Money valortaxa, Conta conta) {
		Money v_retorno = new Money();
		
		if(valor != null && valortaxa != null){
			Double p_valor = valor.getValue().doubleValue();
			Double p_valortaxa = valortaxa.getValue().doubleValue();
			
			Conta contabancaria = conta != null ? contaService.load(conta, "conta.cdconta, conta.banco") : null;
			String numero_banco = contabancaria != null && contabancaria.getBanco() != null && contabancaria.getBanco().getNumero() != null ? contabancaria.getBanco().getNumero().toString() : null;
			
			if("104".equals(numero_banco)){
				v_retorno = new Money(SinedUtil.round(p_valor * (p_valortaxa / 100d), 2));
			}else {
				v_retorno = valor.multiply(valortaxa.divide(new Money(100d)));
			}
		}
		
		return v_retorno;
	}
}