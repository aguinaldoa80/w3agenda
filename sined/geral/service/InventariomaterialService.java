package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.InventarioHistorico;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioAcao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.dao.InventariomaterialDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.GerarInventarioFiltroBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InventariomaterialService extends GenericService<Inventariomaterial>{
	
	private InventariomaterialDAO inventariomaterialDAO;
	private InventarioService inventarioService;
	private InventarioHistoricoService inventarioHistoricoService;
	
	public void setInventarioHistoricoService(
			InventarioHistoricoService inventarioHistoricoService) {
		this.inventarioHistoricoService = inventarioHistoricoService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	public void setInventariomaterialDAO(
			InventariomaterialDAO inventariomaterialDAO) {
		this.inventariomaterialDAO = inventariomaterialDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param inventario
	 * @return
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Inventariomaterial> findByInventario(Inventario inventario) {
		return inventariomaterialDAO.findByInventario(inventario);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<Inventariomaterial> findForGerarInventario(GerarInventarioFiltroBean filtro) {
		return inventariomaterialDAO.findForGerarInventario(filtro);
	}
	
	/**
	 * Processo de gerar CSV da tela de gera��o de invent�rio
	 *
	 * @param filtro
	 * @return
	 * @since 07/10/2019
	 * @author Rodrigo Freitas
	 */
	public Resource gerarCSV(GerarInventarioFiltroBean filtro) {
		List<Inventariomaterial> listaInventariomaterial = this.findForGerarInventario(filtro);
		
		Double somatorioTotal = 0d;
		Double somatorioTotalPesoLiquido = 0d;
		Double somatorioTotalPesoBruto = 0d;
		Double somatorioQtde = 0d;
		Double somatorioValorUnitario = 0d;
		
		StringBuilder csv = new StringBuilder();
		csv.append("Refer�ncia;");
		csv.append("C�digo do material;");
		csv.append("Identificador;");
		csv.append("Material;");
		csv.append("Lote;");
		csv.append("Unidade de medida;");
		csv.append("Quantidade;");
		csv.append("Valor unit�rio;");
		csv.append("Total;");
		csv.append("Total peso l�quido;");
		csv.append("Total peso bruto;");
		csv.append("\n");
		
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			Double qtde = inventariomaterial.getQtde();
			Double valorUnitario = inventariomaterial.getValorUnitario();
			Double total = inventariomaterial.getTotal();
			Double totalPesoLiquido = inventariomaterial.getTotalPesoLiquido();
			Double totalPesoBruto = inventariomaterial.getTotalPesoBruto();
			
			somatorioQtde += qtde;
			somatorioValorUnitario += valorUnitario;
			somatorioTotal += total;
			somatorioTotalPesoLiquido += totalPesoLiquido;
			somatorioTotalPesoBruto += totalPesoBruto;
			
			csv.append(inventariomaterial.getMaterial() != null && inventariomaterial.getMaterial().getReferencia() != null ? inventariomaterial.getMaterial().getReferencia() : "");
			csv.append(";");
			csv.append(inventariomaterial.getMaterial() != null && inventariomaterial.getMaterial().getCdmaterial() != null ? inventariomaterial.getMaterial().getCdmaterial() : "");
			csv.append(";");
			csv.append(inventariomaterial.getMaterial() != null && inventariomaterial.getMaterial().getIdentificacao() != null ? inventariomaterial.getMaterial().getIdentificacao() : "");
			csv.append(";");
			csv.append(inventariomaterial.getMaterial() != null && inventariomaterial.getMaterial().getNome() != null ? inventariomaterial.getMaterial().getNome() : "");
			csv.append(";");
			csv.append(inventariomaterial.getLoteestoque() != null ? inventariomaterial.getLoteestoque().getNumerovalidade() : "");
			csv.append(";");
			csv.append(inventariomaterial.getMaterial() != null && inventariomaterial.getMaterial().getUnidademedida() != null && inventariomaterial.getMaterial().getUnidademedida().getSimbolo() != null ? inventariomaterial.getMaterial().getUnidademedida().getSimbolo() : "");
			csv.append(";");
			csv.append(qtde != null ? SinedUtil.descriptionDecimal(qtde) : "");
			csv.append(";");
			csv.append(valorUnitario != null ? SinedUtil.descriptionDecimal(valorUnitario) : "");
			csv.append(";");
			csv.append(total != null ? SinedUtil.descriptionDecimal(total) : "");
			csv.append(";");
			csv.append(totalPesoLiquido != null ? SinedUtil.descriptionDecimal(totalPesoLiquido) : "");
			csv.append(";");
			csv.append(totalPesoBruto != null ? SinedUtil.descriptionDecimal(totalPesoBruto) : "");
			csv.append(";");
			csv.append("\n");
		}
		
		csv.append(";;;;;;");
		csv.append(somatorioQtde != null ? SinedUtil.descriptionDecimal(somatorioQtde) : "").append(";");
		csv.append(somatorioValorUnitario != null ? SinedUtil.descriptionDecimal(somatorioValorUnitario) : "").append(";");
		csv.append(somatorioTotal != null ? SinedUtil.descriptionDecimal(somatorioTotal) : "").append(";");
		csv.append(somatorioTotalPesoLiquido != null ? SinedUtil.descriptionDecimal(somatorioTotalPesoLiquido) : "").append(";");
		csv.append(somatorioTotalPesoBruto != null ? SinedUtil.descriptionDecimal(somatorioTotalPesoBruto) : "").append(";");
		
		Resource resource = new Resource("text/csv","inventariomaterial_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().replaceAll(";null;", ";;").getBytes());
		return resource;
	}

	/**
	 * Gera o registro de invent�rio
	 *
	 * @param filtro
	 * @since 07/10/2019
	 * @author Rodrigo Freitas
	 */
	public Inventario gerarInventario(GerarInventarioFiltroBean filtro) {
		Inventario inventario = new Inventario();
		inventario.setDtinventario(filtro.getDtinventario());
		inventario.setLocalarmazenagem(filtro.getLocalarmazenagem());
		inventario.setMotivoinventario(filtro.getMotivoinventario());
		inventario.setCodigocontaanalitica(filtro.getContacontabil());
		inventario.setInventarioTipo(filtro.getInventarioTipo());
		inventario.setInventarioSituacao(InventarioSituacao.EM_ABERTO);
		inventario.setEmpresa(filtro.getEmpresa());
		inventario.setProjeto(filtro.getProjeto());
		inventarioService.saveOrUpdate(inventario);
		
		List<Inventariomaterial> listaInventariomaterial = this.findForGerarInventario(filtro);
		for (Inventariomaterial inventariomaterial : listaInventariomaterial) {
			inventariomaterial.setContaanaliticacontabil(filtro.getContacontabil());
			inventariomaterial.setIndicadorpropriedade(filtro.getIndicadorpropriedade());
			inventariomaterial.setInventario(inventario);
			inventariomaterial.setTipopessoainventario(filtro.getTipopessoainventario());
			inventariomaterial.setPessoa(filtro.getPessoa());
			this.saveOrUpdate(inventariomaterial);
		}
		
		InventarioHistorico inventarioHistorico = new InventarioHistorico();
		inventarioHistorico.setInventario(inventario);
		inventarioHistorico.setInventarioAcao(InventarioAcao.CRIADO);
		inventarioHistoricoService.saveOrUpdate(inventarioHistorico);
		
		return inventario;
	}
	
	public Inventariomaterial findGerarBloco (String whereIn){
		return inventariomaterialDAO.findGerarBloco(whereIn);
	}

}
