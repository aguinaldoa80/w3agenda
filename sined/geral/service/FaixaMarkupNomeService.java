package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.dao.FaixaMarkupNomeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FaixaMarkupNomeService extends GenericService<FaixaMarkupNome>{

	private FaixaMarkupNomeDAO faixaMarkupNomeDAO;
	
	public void setFaixaMarkupNomeDAO(FaixaMarkupNomeDAO faixaMarkupNomeDAO) {
		this.faixaMarkupNomeDAO = faixaMarkupNomeDAO;
	}
	
	public FaixaMarkupNome loadByNome(String nome, Integer cdFaixaMarkupNomeExcecao){
		return faixaMarkupNomeDAO.loadByNome(nome, cdFaixaMarkupNomeExcecao);
	}
}
