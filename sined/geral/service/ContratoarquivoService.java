package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratoarquivo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ContratoarquivoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratoarquivoService extends GenericService<Contratoarquivo>{
	
	private ContratoarquivoDAO contratoarquivoDAO;
	private ArquivoDAO arquivoDAO;
	
	public void setContratoarquivoDAO(ContratoarquivoDAO contratoarquivoDAO) {this.contratoarquivoDAO = contratoarquivoDAO;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	
	/**
	 * M�todo para salvar os arquivos de uma lista de contratoarquivo
	 * @param contrato
	 * @author Taidson
	 * @since 03/01/2011
	 */
	public void saveArquivos(Contrato contrato) {
		if (contrato == null) {
			throw new SinedException("O par�metro contrato n�o pode ser null.");
		}
		List<Contratoarquivo> listaContratoarquivo = contrato.getListaContratoarquivo();
		if (listaContratoarquivo != null && listaContratoarquivo.size() > 0) {
			for (Contratoarquivo contratoarquivo : listaContratoarquivo) {
				contratoarquivo.setCdusuarioaltera(contrato.getCdusuarioaltera());
				contratoarquivo.setDtaltera(contrato.getDtaltera());
				contratoarquivo.setContrato(contrato);
				arquivoDAO.saveFile(contratoarquivo, "arquivo");
			}
		}
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/08/2015
	 */
	public List<Contratoarquivo> findByContrato(Contrato contrato) {
		return contratoarquivoDAO.findByContrato(contrato);
	}

}
