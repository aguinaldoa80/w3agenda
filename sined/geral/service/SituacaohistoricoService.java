package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Situacaohistorico;
import br.com.linkcom.sined.geral.dao.SituacaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.situacaohistorico.SituacaohistoricoRESTModel;

public class SituacaohistoricoService extends GenericService<Situacaohistorico>{

	private SituacaohistoricoDAO situacaohistoricoDAO;
	
	public void setSituacaohistoricoDAO(SituacaohistoricoDAO situacaohistoricoDAO) {
		this.situacaohistoricoDAO = situacaohistoricoDAO;
	}
	
	/* singleton */
	private static SituacaohistoricoService instance;
	public static SituacaohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(SituacaohistoricoService.class);
		}
		return instance;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.SituacaohistoricoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<SituacaohistoricoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SituacaohistoricoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<SituacaohistoricoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<SituacaohistoricoRESTModel> lista = new ArrayList<SituacaohistoricoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Situacaohistorico bean : situacaohistoricoDAO.findForAndroid(whereIn))
				lista.add(new SituacaohistoricoRESTModel(bean));
		}
		
		return lista;
	}
}
