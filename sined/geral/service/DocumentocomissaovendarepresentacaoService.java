package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.dao.DocumentocomissaovendarepresentacaoDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendarepresentacaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentocomissaovendarepresentacaoService extends GenericService<Documentocomissao>{

	private DocumentocomissaovendarepresentacaoDAO documentocomissaovendarepresentacaoDAO;
	private VendaService vendaService;
	private VendapagamentoService vendapagamentoService;
	private VendamaterialService vendamaterialService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private PedidovendaService pedidovendaService;
	private PedidovendamaterialService pedidovendamaterialService;
	
	public void setDocumentocomissaovendarepresentacaoDAO(
			DocumentocomissaovendarepresentacaoDAO documentocomissaovendarepresentacaoDAO) {
		this.documentocomissaovendarepresentacaoDAO = documentocomissaovendarepresentacaoDAO;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {
		this.pedidovendapagamentoService = pedidovendapagamentoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForTotalDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissaovenda(DocumentocomissaovendarepresentacaoFiltro filtro){
		return documentocomissaovendarepresentacaoDAO.findForTotalDocumentocomissaovenda(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#deleteDocumentocomissaovenda(Documentocomissao documentocomissao)
	 *
	 * @param documentocomissao
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissaovenda(Documentocomissao documentocomissao) {
		documentocomissaovendarepresentacaoDAO.deleteDocumentocomissaovenda(documentocomissao);		
	}
	
	public List<Documentocomissao> findForPDF(DocumentocomissaovendarepresentacaoFiltro filtro) {
		return documentocomissaovendarepresentacaoDAO.findForPDF(filtro);
	}
	
	public void recalcularComissaovendarepresentacao(Venda venda, Colaborador colaborador) {
		List<Documentocomissao> lista = findForRecalcularComissaoNotPedidovenda(venda);
		Venda bean = vendaService.loadForRecalcularcomissao(venda);
		bean.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(bean));
		bean.setListavendamaterial(vendamaterialService.findByVenda(bean));
		
		boolean geranotaaposvenda = true;
		if(bean != null && 
				bean.getPedidovendatipo() != null &&
				bean.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
				!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_VENDAREALIZADA)){
			geranotaaposvenda = false;		
		}
		if(geranotaaposvenda){
			vendaService.verificaComissionamentoRepresentacao(bean, lista, colaborador, true);
		}
		
		lista = findForRecalcularComissaoWithPedidovenda(venda);
		bean = vendaService.loadWithPedidovenda(venda);
		
		if(bean != null && 
				bean.getPedidovenda() != null && 
				bean.getPedidovenda().getCdpedidovenda() != null &&
				!geranotaaposvenda){
			Pedidovenda pedidovenda = bean.getPedidovenda();
			
			pedidovenda.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(pedidovenda));
			pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
			
			pedidovendaService.verificaComissionamentoRepresentacao(pedidovenda, lista, colaborador, bean, true);
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissao(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @param representacao 
	 */
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda) {
		return findForRecalcularComissaoNotPedidovenda(venda, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento)
	*
	* @param venda
	* @param documento
	* @return
	* @since 15/05/2015
	* @author Luiz Fernando
	*/
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento) {
		return documentocomissaovendarepresentacaoDAO.findForRecalcularComissaoNotPedidovenda(venda, documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissaoWithPedidovenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Venda venda) {
		return documentocomissaovendarepresentacaoDAO.findForRecalcularComissaoWithPedidovenda(venda);
	}
	
	public void recalcularComissaopedidovendarepresentacao(Pedidovenda pedidovenda, Colaborador colaborador) {
		List<Documentocomissao> lista = findForRecalcularComissao(pedidovenda);
		Pedidovenda bean = pedidovendaService.loadForRecalcularcomissao(pedidovenda);
		bean.setListaPedidovendapagamento(pedidovendapagamentoService.findPedidovendaPagamentoByPedidovenda(bean));
		bean.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(bean));
		
		boolean geranotaaposvenda = true;
		if(bean != null && 
				bean.getPedidovendatipo() != null &&
				bean.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
				!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_VENDAREALIZADA)){
			geranotaaposvenda = false;		
		}
		if(geranotaaposvenda){
			pedidovendaService.verificaComissionamentoRepresentacao(bean, lista, null, null, true);
		}
		
		lista = findForRecalcularComissaoWithPedidovenda(pedidovenda);
		bean = pedidovendaService.loadPedidovenda(pedidovenda);
		
		if(bean != null && 
				bean.getCdpedidovenda() != null &&
				!geranotaaposvenda){
			pedidovenda.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(pedidovenda));
			pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
			
			pedidovendaService.verificaComissionamentoRepresentacao(bean, lista, colaborador, null, true);
		}
	}
	
	private List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Pedidovenda pedidovenda) {
		return documentocomissaovendarepresentacaoDAO.findForRecalcularComissaoWithPedidovenda(pedidovenda);
	}
	private List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda) {
		return findForRecalcularComissao(pedidovenda, null);
	}
	
	private List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda, Documento documento) {
		return documentocomissaovendarepresentacaoDAO.findForRecalcularComissao(pedidovenda, documento);
	}

	public void processarListagem(ListagemResult<Documentocomissao> listagemResult,
			DocumentocomissaovendarepresentacaoFiltro filtro) {
		List<Documentocomissao> list = listagemResult.list();
		
		Money totalComissao = new Money();
		for (Documentocomissao documentocomissao : list) {
			if (documentocomissao.getValorcomissao() != null)
				totalComissao = totalComissao.add(documentocomissao.getValorcomissao());
		}
		
		filtro.setTotalComissao(totalComissao);
	}
	
	public Resource gerarListagemCSV(DocumentocomissaovendarepresentacaoFiltro filtro) {
		ListagemResult<Documentocomissao> listagemResult = findForExportacao(filtro);
		processarListagem(listagemResult, filtro);
		
		String cabecalho = "\"Nome\";\"Cliente\";\"Venda\";\"Valor da Comiss�o\";\"Repassado\";\"Valor Repassado\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Documentocomissao bean : listagemResult.list()) {
			
			csv.append("\"" + (bean.getPessoa() != null ? bean.getPessoa().getNome() : "") + "\";");
			
			csv.append("\"");
			
			if(bean.getVenda() != null) {
				csv.append("\"" + (bean.getVenda() != null ? bean.getVenda().getCliente().getNome() : "") + "\";");
				csv.append("\"" + (bean.getVenda() != null ? bean.getVenda().getCdvenda() : "") + "\";");
			}
			
			csv.append("\";");
			
			csv.append("\"" + (bean.getValorcomissao() != null ? bean.getValorcomissao() : "") + "\";");
			
			if(bean.getColaboradorcomissao() != null
					|| bean.getColaboradorcomissao().getCdcolaboradorcomissao() != null
					|| bean.getColaboradorcomissao().getDocumento() != null
					|| bean.getColaboradorcomissao().getDocumento().getDocumentoacao() != null 
					|| bean.getColaboradorcomissao().getDocumento().getDocumentoacao().getCddocumentoacao() != 0) {
				
				csv.append("\"Sim\";");
				csv.append("\"" + bean.getValorcomissao() + "\";");
			} else {
				csv.append("\"N�o\";");
				csv.append("\" - \";");
			}
			
			csv.append("\n");
		}
		
		return new Resource("text/csv", "comissionamento_representacao.csv", csv.toString().getBytes());
	}
}
