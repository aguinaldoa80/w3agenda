package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.geral.bean.view.Vwsaidafiscal;
import br.com.linkcom.sined.geral.dao.VwsaidafiscalDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.VwsaidafiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Saidafiscal;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwsaidafiscalService extends GenericService<Vwsaidafiscal>{
	
	private VwsaidafiscalDAO vwsaidafiscalDAO;
	private ArquivonfnotaService arquivonfnotaService;
	private NotafiscalprodutoitemService notafiscalprodutoitemService;
	private EmpresaService empresaService;
	private NotaFiscalServicoItemService notafiscalservicoitemService;
	
	public void setVwsaidafiscalDAO(VwsaidafiscalDAO vwsaidafiscalDAO) {
		this.vwsaidafiscalDAO = vwsaidafiscalDAO;
	}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {
		this.arquivonfnotaService = arquivonfnotaService;
	}
	public void setNotafiscalprodutoitemService(NotafiscalprodutoitemService notafiscalprodutoitemService) {
		this.notafiscalprodutoitemService = notafiscalprodutoitemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setNotafiscalservicoitemService(NotaFiscalServicoItemService notafiscalservicoitemService) {
		this.notafiscalservicoitemService = notafiscalservicoitemService;
	}

	public Report createReportSaidafiscalListagem(VwsaidafiscalFiltro filtro){
		
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Vwsaidafiscal> listaListagem = findForListagem(filtro).list();
		
		if (listaListagem.isEmpty()){
			return null;
		}
		
		Empresa empresa = null;
		boolean buscarEmpresaPrincipal = false;
		Map<String, Saidafiscal> mapa = new HashMap<String, Saidafiscal>();
		
		List<NotaFiscalServicoItem> listaNotaFiscalServicoItem = notafiscalservicoitemService.findForPdfListagem(CollectionsUtil.listAndConcatenate(listaListagem, "cdnota", ","));
		
		for (NotaFiscalServicoItem nfseItem : listaNotaFiscalServicoItem) {
			NotaFiscalServico nfse = nfseItem.getNotaFiscalServico();
			Cliente cliente = nfse.getCliente();
			
			if (!buscarEmpresaPrincipal && nfse.getEmpresa() != null && nfse.getEmpresa().getCdpessoa() != null) {
				if (empresa == null) {
					empresa = nfse.getEmpresa();
				} else if(!nfse.getEmpresa().equals(empresa)) {
					buscarEmpresaPrincipal = true;
				}
			}
			
			String serie = null, numeroNfse = null;
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaServicoAndCancelada(nfse);
			
			if (arquivonfnota != null && 
					arquivonfnota.getArquivonf() != null && 
					arquivonfnota.getArquivonf().getConfiguracaonfe() != null) {
				if (arquivonfnota.getNota().getSerienfe() != null) {
					serie = arquivonfnota.getNota().getSerienfe().toString();
				} else if (arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse() != null) {
					serie = arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse();
				}
				
				if (arquivonfnota.getNumeronfse() != null) {
					numeroNfse = arquivonfnota.getNumeronfse();
				}
			}
			
			Saidafiscal bean = new Saidafiscal();
			
			bean.setEspecie("NFS-e");
			bean.setCdnota(nfse.getCdNota());
			bean.setSerie(serie);
			bean.setNumero(numeroNfse);
			
			if (nfse.getDtEmissao() != null) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nfse.getDtEmissao());
				bean.setDia(calendar.get(Calendar.DAY_OF_MONTH));
			}
			
			if (cliente.getListaEndereco() != null && !cliente.getListaEndereco().isEmpty()) {
				List<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class, cliente.getListaEndereco());
				
				if (listaEndereco.get(0).getMunicipio() != null && listaEndereco.get(0).getMunicipio().getUf() != null){
					bean.setUfDestino(listaEndereco.get(0).getMunicipio().getUf().getSigla());
				}
			}
			
			bean.setValorContabil(nfseItem.getTotalParcial());
			bean.setFiscal(null);
			
			boolean icms = false;
			
			if (nfse.getBasecalculoicms() != null && nfse.getIcms() != null && nfse.getValorIcms() != null) {
				icms = true;
			}
			
			if (!icms) {
				Saidafiscal beanClone = bean.getClone();
				beanClone.setIcmsIpi("");
				
				preencherMapaSaidaFiscal(beanClone, mapa);
			} else {
				if (icms) {
					Saidafiscal beanClone = bean.getClone();
					
					beanClone.setIcmsIpi("ICMS");
					beanClone.setValorBC(nfse.getBasecalculoicms());
					beanClone.setAliquota(nfse.getIcms());
					beanClone.setValorDebitado(nfse.getValorIcms());
					
					preencherMapaSaidaFiscal(beanClone, mapa);
				}
			}
		}
		
		List<Notafiscalprodutoitem> listaNotafiscalprodutoitem = notafiscalprodutoitemService.findForPdfListagem(CollectionsUtil.listAndConcatenate(listaListagem, "cdnota", ","));
		
		for (Notafiscalprodutoitem notafiscalprodutoitem: listaNotafiscalprodutoitem){
			
			Notafiscalproduto notafiscalproduto = notafiscalprodutoitem.getNotafiscalproduto();
			Cliente cliente = notafiscalproduto.getCliente();
			if(!buscarEmpresaPrincipal && notafiscalproduto.getEmpresa() != null && notafiscalproduto.getEmpresa().getCdpessoa() != null){
				if(empresa == null){
					empresa = notafiscalproduto.getEmpresa();
				}else if(!notafiscalproduto.getEmpresa().equals(empresa)){
					buscarEmpresaPrincipal = true;
				}
			}
			
			String serie = null;
			Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProdutoAndCancelada(notafiscalproduto);
			if (arquivonfnota != null && 
					arquivonfnota.getArquivonf() != null && 
					arquivonfnota.getArquivonf().getConfiguracaonfe() != null){
				if(arquivonfnota.getNota().getSerienfe() != null){
					serie = arquivonfnota.getNota().getSerienfe().toString();
				} else if(arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse() != null){
					serie = arquivonfnota.getArquivonf().getConfiguracaonfe().getSerienfse();
				}
			}
			
			Saidafiscal bean = new Saidafiscal();
			bean.setCdnota(notafiscalproduto.getCdNota());
			bean.setSerie(serie);
			bean.setNumero(notafiscalproduto.getNumero());
			
			if (notafiscalproduto.getDtEmissao()!=null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(notafiscalproduto.getDtEmissao());
				bean.setDia(calendar.get(Calendar.DAY_OF_MONTH));
			}
			
			if (cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){
				List<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class, cliente.getListaEndereco());
				if (listaEndereco.get(0).getMunicipio()!=null && listaEndereco.get(0).getMunicipio().getUf()!=null){
					bean.setUfDestino(listaEndereco.get(0).getMunicipio().getUf().getSigla());
				}
			}
			
			bean.setValorContabil(notafiscalprodutoitem.getValorbruto());
			bean.setFiscal(notafiscalprodutoitem.getCfop()!=null ? notafiscalprodutoitem.getCfop().getCodigo() : null);
			
			boolean icms = false;
			boolean ipi = false;
			
			if (notafiscalprodutoitem.getValorbcicms()!=null 
					&& notafiscalprodutoitem.getIcms()!=null
					&& notafiscalprodutoitem.getValoricms()!=null){
				icms = true;
			}
			
			if (notafiscalprodutoitem.getValorbcipi()!=null 
					&& notafiscalprodutoitem.getIpi()!=null
					&& notafiscalprodutoitem.getValoripi()!=null){
				ipi = true;
			}
			
			if (!icms && !ipi){
				Saidafiscal beanClone = bean.getClone();
				beanClone.setIcmsIpi("");
				preencherMapaSaidaFiscal(beanClone, mapa);
			}else {
				if (icms){
					Saidafiscal beanClone = bean.getClone();
					beanClone.setIcmsIpi("ICMS");
					if (ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorBC(notafiscalprodutoitem.getValorbcicms());
						beanClone.setAliquota(notafiscalprodutoitem.getIcms());
						beanClone.setValorDebitado(notafiscalprodutoitem.getValoricms());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorIsentasNaoTributadas(notafiscalprodutoitem.getValorbruto());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(notafiscalprodutoitem.getValorFiscalIcms())){
						beanClone.setValorOutras(notafiscalprodutoitem.getValorbruto());
					}
					preencherMapaSaidaFiscal(beanClone, mapa);
				}
				if (ipi){
					Saidafiscal beanClone = bean.getClone();
					beanClone.setIcmsIpi("IPI");
					if (ValorFiscal.OPERACOES_COM_CREDITO_IMPOSTO.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorBC(notafiscalprodutoitem.getValorbcipi());
						beanClone.setAliquota(notafiscalprodutoitem.getIpi());
						beanClone.setValorDebitado(notafiscalprodutoitem.getValoripi());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorIsentasNaoTributadas(notafiscalprodutoitem.getValorbruto());
					}else if (ValorFiscal.OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS.equals(notafiscalprodutoitem.getValorFiscalIpi())){
						beanClone.setValorOutras(notafiscalprodutoitem.getValorbruto());
					}
					if(icms) beanClone.setValorContabil(new Money());
					
					preencherMapaSaidaFiscal(beanClone, mapa);
				}
			}
		}		
		
		List<Saidafiscal> listaRelatorio = new ArrayList<Saidafiscal>(mapa.values());
		
		Collections.sort(listaRelatorio, new Comparator<Saidafiscal>() {
			@Override
			public int compare(Saidafiscal o1, Saidafiscal o2) {
				String numero1 = o1.getNumero();
				String numero2 = o2.getNumero();
				int compCdnota = o1.getCdnota().compareTo(o2.getCdnota());
				int compIcmsipi = o1.getIcmsIpi().compareTo(o2.getIcmsIpi());
				if (numero1==null && numero2==null){
					return compCdnota;
				}else if (numero1!=null && numero2==null){
					return -1;
				}else if (numero1==null && numero2!=null){
					return 1;
				}else {
					int compNumero = numero1.compareTo(numero2);
					if (compNumero==0){
						if (compCdnota==0){
							return compIcmsipi;
						}else {
							return compCdnota;
						}
					}else {
						return compNumero;
					}
				}
			}
		});
		
		if(filtro.getEmpresa() != null){
			empresa = empresaService.loadComArquivo(filtro.getEmpresa());
		}else if(!buscarEmpresaPrincipal && empresa != null){
			empresa = empresaService.loadComArquivo(empresa);
		}else {
			empresa = empresaService.loadPrincipal();
		}
		
		empresaService.setInformacoesPropriedadeRural(empresa);
		
		Report report = new Report("/fiscal/saidafiscal");
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa.getRazaoSocialOuNomeProprietarioPrincipalOuEmpresa());
		report.addParameter("CNPJ", empresa.getCpfCnpjProprietarioPrincipalOuEmpresa()!=null ? empresa.getCpfCnpjProprietarioPrincipalOuEmpresa().toString() : "");
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual());
		report.addParameter("TITULO", "SA�DA FISCAL");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.setDataSource(listaRelatorio);
		
		return report;
	}
	
	private void preencherMapaSaidaFiscal(Saidafiscal bean, Map<String, Saidafiscal> mapa){
		Saidafiscal beanAux = mapa.get(bean.getKey());
		if (beanAux==null){
			mapa.put(bean.getKey(), bean);
		}else {
			beanAux.somarCampos(bean);
			mapa.put(bean.getKey(), beanAux);	
		}
	}
	
	public List<Vwsaidafiscal> findForCsv(VwsaidafiscalFiltro filtro){
		return vwsaidafiscalDAO.findForCsv(filtro);
	}
}