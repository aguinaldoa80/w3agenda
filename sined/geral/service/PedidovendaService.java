package br.com.linkcom.sined.geral.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import net.sf.json.JSON;

import org.apache.commons.io.FileUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.util.pdf.WKHTMLBuilder;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.*;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_VendamaterialTabelapreco;
import br.com.linkcom.sined.geral.bean.auxiliar.ConfiguracaoEcommerceBeanAux;
import br.com.linkcom.sined.geral.bean.auxiliar.ConfiguracaoEcommerceConta;
import br.com.linkcom.sined.geral.bean.auxiliar.ConfiguracaoEcommerceFormaPagamento;
import br.com.linkcom.sined.geral.bean.auxiliar.PedidoVendaParametersBean;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.VendaFornecedorTicketMedioBean;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.CampoComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentotipo;
import br.com.linkcom.sined.geral.bean.enumeration.ContasFinanceiroSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.EcommerceEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pneureforma;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoPendencia;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.TipoComprovanteEnum;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCriacaoPedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoiniciodata;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.PedidovendaDAO;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaClienteRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaEmpresaRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitItemRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitflexivelItemRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialKitflexivelRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaMaterialRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaOutrosRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaPagamentoRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.VendaRTF;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClienteInfoFinanceiraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ClientedevedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ColetarProducaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ComissaoMaterialVenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EtiquetaPneuFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.EtiquetaPneuItem;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.GerarProducaoConferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Impostovendamaterial;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialDisponivelTransferenciaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendaDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialDevolucaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.TipoReservaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PedidovendaFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.AjaxRealizarVendaPagamentoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.EmitirFichaColetaReport;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EnderecoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.PagamentoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutoMestreGradeComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitflexivelComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokitflexivelitemComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ProdutokititemComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ResumoMaterialunidadeconversao;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.TotaisComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitircustovendaFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.FluxocaixaBeanReport;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.FluxocaixaFiltroReport;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CancelarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConfirmarPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaPagamentoBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.CriarPedidovendaRespostaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ExistePedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.InserirClienteBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ParcelasPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaParametroBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaWSResponseBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.FaturarColetaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.FaturarColetaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaItemBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ManterPedidovendaParcelaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PrazopagamentoFiltro;
import br.com.linkcom.sined.util.AndroidUtil;
import br.com.linkcom.sined.util.CacheWebserviceUtil;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ImpostoUtil;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.VelocityUtil;
import br.com.linkcom.sined.util.neo.ComprovanteTxtFileServlet;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.sined.util.rest.android.materialQtd.MaterialQtdRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovenda.PedidovendaRESTModel;
import br.com.linkcom.sined.util.rest.android.pedidovenda.PedidovendaRESTWSBean;
import br.com.linkcom.sined.util.rest.android.pedidovenda.PedidovendamaterialRESTWSBean;
import br.com.linkcom.sined.util.rest.android.pedidovenda.PedidovendapagamentoRESTWSBean;
import br.com.linkcom.sined.util.rest.android.pneu.PneuRESTWSBean;
import br.com.linkcom.sined.util.rest.w3producao.pedidovenda.PedidoVendaW3producaoRESTModel;
import br.com.linkcom.sined.wms.WmsWebServiceStub;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedidoResponse;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedidoResponse;

import com.ibm.icu.util.Calendar;

public class PedidovendaService extends GenericService<Pedidovenda>{

	protected static final String MSG_IDENTIFICADOR_EXTERNO="N�o foi poss�vel confirmar o pedido de venda pois o valor do campo 'Identificador Externo' j� est� cadastrado no sistema." +
			" Favor conferir o valor informado para a confirma��o o pedido de venda.";
	protected PedidovendaDAO pedidovendaDAO;
	protected ColaboradorService colaboradorService;
	protected ClienteService clienteService;
	protected PedidovendamaterialService pedidovendamaterialService;
	protected EnderecoService enderecoService;
	protected TelefoneService telefoneService;
	protected EmpresaService empresaService;
	protected VendaService vendaService;
	protected VendahistoricoService vendahistoricoService;
	protected PedidovendahistoricoService pedidovendahistoricoService;
	protected PedidovendapagamentoService pedidovendapagamentoService;
	protected PedidoVendaNegociacaoService pedidoVendaNegociacaoService;
	protected RateioService rateioService;
	protected RateioitemService rateioitemService;
	protected DocumentoService documentoService;
	protected ParcelamentoService parcelamentoService;
	protected DocumentotipoService documentotipoService;
	protected ParcelaService parcelaService;
	protected DocumentohistoricoService documentohistoricoService;
	protected DocumentoorigemService documentoorigemService;
	protected PedidovendasincronizacaoService pedidovendasincronizacaoService;
	protected MaterialService materialService;
	protected ContaService contaService;
	protected RestricaoService restricaoService;
	protected UnidademedidaService unidademedidaService;
	protected MaterialcolaboradorService materialcolaboradorService;
	protected ColaboradorcargoService colaboradorcargoService;
	protected DocumentocomissaoService documentocomissaoService;
	protected DocumentocomissaovendaService documentocomissaovendaService;
	protected PedidovendasincronizacaohistoricoService pedidovendasincronizacaohistoricoService;
	protected ParametrogeralService parametrogeralService;
	protected MaterialdevolucaoService materialdevolucaoService;
	protected MaterialtabelaprecoitemService materialtabelaprecoitemService;
	protected UsuarioService usuarioService;
	protected PrazopagamentoService prazopagamentoService;
	protected ContacorrenteService contacorrenteService;
	protected ContareceberService contareceberService;
	protected MovimentacaoestoqueService movimentacaoestoqueService;
	protected VendaorcamentoService vendaorcamentoService;
	protected EcommaterialidentificadorService ecommaterialidentificadorService;
	protected ContatoService contatoService;
	protected PrazopagamentoitemService prazopagamentoitemService;
	protected MaterialtabelaprecoService materialtabelaprecoService;
	protected UnidademedidaconversaoService unidademedidaconversaoService;
	protected ConfiguracaoecomService configuracaoecomService;
	protected MaterialsimilarService materialsimilarService;
	protected PedidovendavalorcampoextraService pedidovendavalorcampoextraService;
	protected PedidovendamaterialmestreService pedidovendamaterialmestreService;
	protected MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	protected PedidovendamaterialseparacaoService pedidovendamaterialseparacaoService;
	protected MaterialrelacionadoService materialrelacionadoService;
	protected ColetaService coletaService;
	protected LoteestoqueService loteestoqueService;
	protected LocalarmazenagemService localarmazenagemService;
	protected ColetahistoricoService coletahistoricoService;
	protected NotafiscalprodutoService notafiscalprodutoService;
	protected ProdutoService produtoService;
	protected PedidovendatipoService pedidovendatipoService;
	protected PedidovendaofflineService pedidovendaofflineService;
	protected FornecedorService fornecedorService;
	protected EmpresarepresentacaoService empresarepresentacaoService;
	protected PedidovendapagamentorepresentacaoService pedidovendapagamentorepresentacaoService;
	protected ValecompraService valecompraService;
	protected ContratoService contratoService;
	protected ContratohistoricoService contratohistoricoService;
	protected MaterialgrupocomissaovendaService materialgrupocomissaovendaService;
	protected VdocumentonegociadoService vdocumentonegociadoService;
	protected TabelavalorService tabelavalorService;
	protected OrdemservicoveterinariaService ordemservicoveterinariaService;
	protected EmporiumpedidovendaService emporiumpedidovendaService;
	protected PneuService pneuService;
	protected NotafiscalprodutoColetaService notafiscalprodutoColetaService;
	protected PneumarcaService pneumarcaService;
	protected PneumodeloService pneumodeloService;
	protected PneumedidaService pneumedidaService;
	protected ColetaMaterialService coletaMaterialService;
	protected ProducaoagendaService producaoagendaService;
	protected MaterialproducaoService materialproducaoService;
	protected ChequeService chequeService;
	protected ChequehistoricoService chequehistoricoService;
	protected NotafiscalprodutoretornopedidovendaService notafiscalprodutoretornopedidovendaService;
	protected NotafiscalprodutoretornovendaService notafiscalprodutoretornovendaService;
	protected ComissionamentoService comissionamentoService;
	protected GarantiareformaitemService garantiareformaitemService;
	protected ProjetoService projetoService;
	protected TipotaxaService tipotaxaService;
	protected RegiaoService regiaoService;
	protected ReservaService reservaService;
	protected OrdemservicoveterinariamaterialService ordemservicoveterinariamaterialService;
	protected PedidovendaordemservicoveterinariaService pedidovendaordemservicoveterinariaService;
	protected ClientevendedorService clientevendedorService;
	protected MaterialFaixaMarkupService materialFaixaMarkupService;
	protected MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService;
	protected MaterialkitflexivelService materialkitflexivelService;
	protected MaterialformulavalorvendaService materialformulavalorvendaService;
	protected MaterialunidademedidaService materialunidademedidaService;
	protected ArquivoService arquivoService;
	protected PessoaService pessoaService;
	protected PessoaContatoService pessoaContatoService;
	protected CampoextrapedidovendatipoService campoextrapedidovendatipoService;
	protected MaterialvendaService materialvendaService;
	protected VendaorcamentohistoricoService vendaorcamentohistoricoService;
	protected OrdemservicoveterinariahistoricoService ordemservicoveterinariahistoricoService;
	protected ProducaoagendamaterialService producaoagendamaterialService;
	protected VendaorcamentomaterialService vendaorcamentomaterialService;
	protected VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService;
	protected OrcamentovalorcampoextraService orcamentovalorcampoextraService;
	protected CentrocustoService centrocustoService;
	protected PneuSegmentoService pneuSegmentoService;
	protected OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService;
	protected ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService;
	protected ContareceberService contaReceberService;
	protected Ecom_PedidoVendaService ecom_PedidoVendaService;
	protected VendamaterialService vendamaterialService;
	protected EmitirFichaColetaReport emitirFichaColetaReport;
	
	public void setPedidoVendaNegociacaoService(PedidoVendaNegociacaoService pedidoVendaNegociacaoService) {this.pedidoVendaNegociacaoService = pedidoVendaNegociacaoService;}
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {this.coletaMaterialService = coletaMaterialService;}
	public void setPneumodeloService(PneumodeloService pneumodeloService) {this.pneumodeloService = pneumodeloService;}
	public void setPneumedidaService(PneumedidaService pneumedidaService) {this.pneumedidaService = pneumedidaService;}
	public void setPneumarcaService(PneumarcaService pneumarcaService) {this.pneumarcaService = pneumarcaService;}
	public void setPneuService(PneuService pneuService) {this.pneuService = pneuService;}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {this.vdocumentonegociadoService = vdocumentonegociadoService;}
	public void setColetahistoricoService(ColetahistoricoService coletahistoricoService) {this.coletahistoricoService = coletahistoricoService;}
	public void setConfiguracaoecomService(ConfiguracaoecomService configuracaoecomService) {this.configuracaoecomService = configuracaoecomService;}
	public void setEcommaterialidentificadorService(EcommaterialidentificadorService ecommaterialidentificadorService) {this.ecommaterialidentificadorService = ecommaterialidentificadorService;}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {this.prazopagamentoService = prazopagamentoService;}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {this.pedidovendapagamentoService = pedidovendapagamentoService;}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {this.pedidovendahistoricoService = pedidovendahistoricoService;}
	public void setVendahistoricoService(VendahistoricoService vendahistoricoService) {this.vendahistoricoService = vendahistoricoService;}
	public void setVendaService(VendaService vendaService) {this.vendaService = vendaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPedidovendaDAO(PedidovendaDAO pedidovendaDAO) {this.pedidovendaDAO = pedidovendaDAO;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setParcelamentoService(ParcelamentoService parcelamentoService) {this.parcelamentoService = parcelamentoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setParcelaService(ParcelaService parcelaService) {this.parcelaService = parcelaService;}
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setPedidovendasincronizacaoService(PedidovendasincronizacaoService pedidovendasincronizacaoService) {this.pedidovendasincronizacaoService = pedidovendasincronizacaoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setRestricaoService(RestricaoService restricaoService) {this.restricaoService = restricaoService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setMaterialcolaboradorService(MaterialcolaboradorService materialcolaboradorService) {this.materialcolaboradorService = materialcolaboradorService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setDocumentocomissaovendaService(DocumentocomissaovendaService documentocomissaovendaService) {this.documentocomissaovendaService = documentocomissaovendaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setMaterialdevolucaoService(MaterialdevolucaoService materialdevolucaoService) {this.materialdevolucaoService = materialdevolucaoService;}
	public void setMaterialtabelaprecoitemService(MaterialtabelaprecoitemService materialtabelaprecoitemService) {this.materialtabelaprecoitemService = materialtabelaprecoitemService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {this.contacorrenteService = contacorrenteService;}
	public void setContareceberService(ContareceberService contareceberService) {this.contareceberService = contareceberService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setVendaorcamentoService(VendaorcamentoService vendaorcamentoService) {this.vendaorcamentoService = vendaorcamentoService;}
	public void setContatoService(ContatoService contatoService) {this.contatoService = contatoService;}
	public void setPrazopagamentoitemService(PrazopagamentoitemService prazopagamentoitemService) {this.prazopagamentoitemService = prazopagamentoitemService;}
	public void setMaterialtabelaprecoService(MaterialtabelaprecoService materialtabelaprecoService) {this.materialtabelaprecoService = materialtabelaprecoService;}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {this.unidademedidaconversaoService = unidademedidaconversaoService;}
	public void setMaterialsimilarService(MaterialsimilarService materialsimilarService) {this.materialsimilarService = materialsimilarService;}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;}
	public void setPedidovendamaterialmestreService(PedidovendamaterialmestreService pedidovendamaterialmestreService) {this.pedidovendamaterialmestreService = pedidovendamaterialmestreService;}
	public void setPedidovendasincronizacaohistoricoService(PedidovendasincronizacaohistoricoService pedidovendasincronizacaohistoricoService) {this.pedidovendasincronizacaohistoricoService = pedidovendasincronizacaohistoricoService;}
	public void setPedidovendavalorcampoextraService(PedidovendavalorcampoextraService pedidovendavalorcampoextraService) {this.pedidovendavalorcampoextraService = pedidovendavalorcampoextraService;}
	public void setPedidovendamaterialseparacaoService(PedidovendamaterialseparacaoService pedidovendamaterialseparacaoService) {this.pedidovendamaterialseparacaoService = pedidovendamaterialseparacaoService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setColetaService(ColetaService coletaService) {this.coletaService = coletaService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setProdutoService(ProdutoService produtoService) {this.produtoService = produtoService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setPedidovendaofflineService(PedidovendaofflineService pedidovendaofflineService) {this.pedidovendaofflineService = pedidovendaofflineService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setEmpresarepresentacaoService(EmpresarepresentacaoService empresarepresentacaoService) {this.empresarepresentacaoService = empresarepresentacaoService;}
	public void setPedidovendapagamentorepresentacaoService(PedidovendapagamentorepresentacaoService pedidovendapagamentorepresentacaoService) {this.pedidovendapagamentorepresentacaoService = pedidovendapagamentorepresentacaoService;}
	public void setValecompraService(ValecompraService valecompraService) {this.valecompraService = valecompraService;}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setContratohistoricoService(ContratohistoricoService contratohistoricoService) {this.contratohistoricoService = contratohistoricoService;}
	public void setMaterialgrupocomissaovendaService(MaterialgrupocomissaovendaService materialgrupocomissaovendaService) {this.materialgrupocomissaovendaService = materialgrupocomissaovendaService;}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {this.tabelavalorService = tabelavalorService;}
	public void setOrdemservicoveterinariaService(OrdemservicoveterinariaService ordemservicoveterinariaService) {this.ordemservicoveterinariaService = ordemservicoveterinariaService;}
	public void setEmporiumpedidovendaService(EmporiumpedidovendaService emporiumpedidovendaService) {this.emporiumpedidovendaService = emporiumpedidovendaService;}
	public void setNotafiscalprodutoColetaService(NotafiscalprodutoColetaService notafiscalprodutoColetaService) {this.notafiscalprodutoColetaService = notafiscalprodutoColetaService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setNotafiscalprodutoretornopedidovendaService(NotafiscalprodutoretornopedidovendaService notafiscalprodutoretornopedidovendaService) {this.notafiscalprodutoretornopedidovendaService = notafiscalprodutoretornopedidovendaService;}
	public void setNotafiscalprodutoretornovendaService(NotafiscalprodutoretornovendaService notafiscalprodutoretornovendaService) {this.notafiscalprodutoretornovendaService = notafiscalprodutoretornovendaService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}
	public void setGarantiareformaitemService(GarantiareformaitemService garantiareformaitemService) {this.garantiareformaitemService = garantiareformaitemService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setRegiaoService(RegiaoService regiaoService) {this.regiaoService = regiaoService;}
	public void setReservaService(ReservaService reservaService) {this.reservaService = reservaService;}
	public void setOrdemservicoveterinariamaterialService(OrdemservicoveterinariamaterialService ordemservicoveterinariamaterialService) {this.ordemservicoveterinariamaterialService = ordemservicoveterinariamaterialService;	}
	public void setPedidovendaordemservicoveterinariaService(PedidovendaordemservicoveterinariaService pedidovendaordemservicoveterinariaService) {this.pedidovendaordemservicoveterinariaService = pedidovendaordemservicoveterinariaService;}
	public void setClientevendedorService(ClientevendedorService clientevendedorService) {this.clientevendedorService = clientevendedorService;}
	public void setMaterialFaixaMarkupService(MaterialFaixaMarkupService materialFaixaMarkupService) {this.materialFaixaMarkupService = materialFaixaMarkupService;}
	public void setMaterialTabelaPrecoItemHistoricoService(MaterialTabelaPrecoItemHistoricoService materialTabelaPrecoItemHistoricoService) {this.materialTabelaPrecoItemHistoricoService = materialTabelaPrecoItemHistoricoService;}
	public void setMaterialkitflexivelService(MaterialkitflexivelService materialkitflexivelService) {this.materialkitflexivelService = materialkitflexivelService;}
	public void setMaterialformulavalorvendaService(MaterialformulavalorvendaService materialformulavalorvendaService) {this.materialformulavalorvendaService = materialformulavalorvendaService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setCampoextrapedidovendatipoService(CampoextrapedidovendatipoService campoextrapedidovendatipoService) {this.campoextrapedidovendatipoService = campoextrapedidovendatipoService;}
	public void setMaterialvendaService(MaterialvendaService materialvendaService) {this.materialvendaService = materialvendaService;}
	public void setVendaorcamentohistoricoService(VendaorcamentohistoricoService vendaorcamentohistoricoService) {this.vendaorcamentohistoricoService = vendaorcamentohistoricoService;}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {this.producaoagendamaterialService = producaoagendamaterialService;}
	public void setVendaorcamentomaterialService(VendaorcamentomaterialService vendaorcamentomaterialService) {this.vendaorcamentomaterialService = vendaorcamentomaterialService;}
	public void setVendaorcamentomaterialmestreService(VendaorcamentomaterialmestreService vendaorcamentomaterialmestreService) {this.vendaorcamentomaterialmestreService = vendaorcamentomaterialmestreService;}
	public void setOrcamentovalorcampoextraService(OrcamentovalorcampoextraService orcamentovalorcampoextraService) {this.orcamentovalorcampoextraService = orcamentovalorcampoextraService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {this.pneuSegmentoService = pneuSegmentoService;}
	public void setOtrPneuTipoOtrClassificacaoCodigoService(OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService) {this.otrPneuTipoOtrClassificacaoCodigoService = otrPneuTipoOtrClassificacaoCodigoService;}
	public void setOrdemservicoveterinariahistoricoService(OrdemservicoveterinariahistoricoService ordemservicoveterinariahistoricoService) {this.ordemservicoveterinariahistoricoService = ordemservicoveterinariahistoricoService;}
	public void setColetaMaterialPedidovendamaterialService(ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService) {this.coletaMaterialPedidovendamaterialService = coletaMaterialPedidovendamaterialService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {this.pessoaContatoService = pessoaContatoService;}
	public void setContaReceberService(ContareceberService contaReceberService) {this.contaReceberService = contaReceberService;}
	public void setEcom_PedidoVendaService(Ecom_PedidoVendaService ecom_PedidoVendaService) {this.ecom_PedidoVendaService = ecom_PedidoVendaService;}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {this.vendamaterialService = vendamaterialService;}
	public void setEmitirFichaColetaReport(EmitirFichaColetaReport emitirFichaColetaReport) {this.emitirFichaColetaReport = emitirFichaColetaReport;}

	public static PedidovendaService instance;
	
	public static PedidovendaService getInstance(){
		if (instance==null){
			return Neo.getObject(PedidovendaService.class);
		}else {
			return instance;
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForConfirmacao(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public Pedidovenda loadForConfirmacao(Pedidovenda pedidovenda){
		if(pedidovenda == null || pedidovenda.getCdpedidovenda() == null)
			throw new SinedException("O pedido de venda n�o pode ser nulo.");
		
		List<Pedidovenda> listaPedidovenda = loadForConfirmacao(pedidovenda.getCdpedidovenda().toString());
		return SinedUtil.isListNotEmpty(listaPedidovenda) ? listaPedidovenda.get(0) : null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForConfirmacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 30/07/2014
	* @author Luiz Fernando
	*/
	public List<Pedidovenda> loadForConfirmacao(String whereIn){
		return pedidovendaDAO.loadForConfirmacao(whereIn);
	}
	
	public List<Pedidovenda> findForSolicitacaocompra(String whereIn){
		return pedidovendaDAO.findForSolicitacaocompra(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForComprovante(Integer id)
	 *
	 * @param cdpedidovenda
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public Pedidovenda loadForComprovante(Integer cdpedidovenda){
		return pedidovendaDAO.loadForComprovante(cdpedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForComprovante(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForComprovante(String whereIn){
		return pedidovendaDAO.findForComprovante(whereIn);
	}
	
	/**
	 * M�todo que faz a gera��o do comprovante do pedido de venda.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PedidovendamaterialService#findByPedidoVenda(Pedidovenda pedidovenda)
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#loadForComprovante(Integer cdpedidovenda)
	 * @see br.com.linkcom.sined.geral.service.ClienteService#carregarDadosCliente(Cliente cliente)
	 *
	 * @param filtro
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public Report gerarComprovante(PedidovendaFiltro filtro){
		String whereIn = filtro.getWhereIn();
		
		if(whereIn == null) {
			whereIn = filtro.getCdpedidovenda().toString();
		}
		
		if(whereIn == null){
			throw new SinedException("Par�metro cdpedidovenda inv�lido.");
		}
		
		Pedidovenda pedidoVenda = this.loadForComprovante(Integer.parseInt(whereIn.split(",")[0]));
		
		return criaComprovantePedidovendaReport(pedidoVenda);
	}
	
	/**
	 * M�todo que faz a gera��o do comprovante de v�rios pedidos de venda.
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#findForComprovante(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#criaComprovantePedidovendaReport(Pedidovenda pedidoVenda)
	 * 
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarVariosComprovantes(PedidovendaFiltro filtro) throws Exception {
		MergeReport mergeReport = new MergeReport("comprovantes_pedidovenda_"+SinedUtil.datePatternForReport()+".pdf");
		
		String whereIn = filtro.getWhereIn();
		
		if(whereIn == null || "".equals(whereIn)){
			throw new SinedException("Nenhum item selecionado");
		}
		
		List<Pedidovenda> listaPedidovenda = this.findForComprovante(whereIn);
		
		if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
			for(Pedidovenda pedidovenda : listaPedidovenda){
				mergeReport.addReport(this.criaComprovantePedidovendaReport(pedidovenda));
			}
		}		
		return mergeReport.generateResource();
	}
	
	/**
	 * Gerar o comprovante de pedido de venda configuravel
	 * @param request
	 * @param cdpedidovenda
	 * @param comprovante
	 * @return
	 */
	public ModelAndView gerarComprovanteConfiguravel(WebRequestContext request, Integer cdpedidovenda, ComprovanteConfiguravel comprovante) {
		Pedidovenda pedidoVenda = this.loadForComprovante(cdpedidovenda);
		StringBuilder comprovanteHTML = new StringBuilder();
		
		boolean emitirTXT = comprovante.getTxt() != null && comprovante.getTxt();
		
		try {
			comprovanteHTML.append(gerarComprovanteConfiguravelOnly(comprovante, pedidoVenda));
			if(!emitirTXT){
				comprovanteHTML.append("</div>");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		} 
		
		String print = comprovanteHTML.toString() + "<script>javascript:window.print();</script>";
		if(emitirTXT){
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ComprovanteTxtFileServlet.add(request.getSession(), key, comprovanteHTML.toString());
			print = "<script>window.open('" + request.getServletRequest().getContextPath() + "/COMPROVANTETXT/" + key + "');window.close();</script>";
		}
		
		request.getServletResponse().setContentType("text/html");
		try {
			request.getServletResponse().getWriter().print(print);
		} catch (IOException e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		}
	
		return null;
	}
	
	protected String gerarComprovanteConfiguravelOnly(ComprovanteConfiguravel comprovante, Pedidovenda pedidoVenda) throws Exception {
		return VelocityUtil.renderTemplate(comprovante.getLayout(), this.gerarMapaComprovanteVendaVariavel(pedidoVenda));
	}

	/**
	 * Gera os comprovantes de venda configur�veis para mais de um pedido de venda
	 * @param request
	 * @param comprovante
	 * @param whereIn
	 * @return
	 */
	public ModelAndView gerarComprovantesConfiguraveis(WebRequestContext request, String whereIn, ComprovanteConfiguravel comprovante) {
		List<Pedidovenda> listaPedidovenda = this.findForComprovante(whereIn);
		StringBuilder comprovanteHTML = new StringBuilder();
		
		boolean emitirTXT = comprovante.getTxt() != null && comprovante.getTxt();
		
		for(int i=0; i<listaPedidovenda.size(); i++ ){
			Pedidovenda pedidovenda = listaPedidovenda.get(i);
			
			if(!emitirTXT){
				if(i != (listaPedidovenda.size()-1) )
					comprovanteHTML.append("<div style='page-break-after: always;'>");
				else comprovanteHTML.append("<div>");
			}
			
			Map<String, Object> map = gerarMapaComprovanteVendaVariavel(pedidovenda);
			try {
				comprovanteHTML.append(VelocityUtil.renderTemplate(comprovante.getLayout(), map));
				if(!emitirTXT){
					comprovanteHTML.append("</div>");
				}
			} catch (Throwable e) {
				e.printStackTrace();
				throw new SinedException("Erro ao gerar o comprovante.");
			} 

		}
		
		String print = comprovanteHTML.toString()+"<script>javascript:window.print();</script>";
		if(emitirTXT){
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ComprovanteTxtFileServlet.add(request.getSession(), key, comprovanteHTML.toString());
			print = "<script>window.opener.location = '" + request.getServletRequest().getContextPath() + "/COMPROVANTETXT/" + key + "'; window.close();</script>";
		}
		
		request.getServletResponse().setContentType("text/html");
		try {
			request.getServletResponse().getWriter().print(print);
		} catch (IOException e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar o comprovante.");
		}
	
		return null;
	}
	
	@SuppressWarnings("unchecked")
	protected Map<String, Object> gerarMapaComprovanteVendaVariavel(Pedidovenda pedidoVenda) {
		List<Pedidovendamaterial> listaProdutos = pedidovendamaterialService.findByPedidoVenda(pedidoVenda, "pedidovendamaterial.ordem, pedidovendamaterial.cdpedidovendamaterial, listaCaracteristica.cdmaterialcaracteristica");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String logoNaoDisponivel = "'" + SinedUtil.getUrlWithContext() +  "/imagens/imgnaodisponivel.jpg'";
		
		Colaborador colaborador = colaboradorService.loadWithoutPermissao(pedidoVenda.getColaborador(), "colaborador.cdpessoa, colaborador.nome, colaborador.email");
		colaborador.setListaTelefone(SinedUtil.listToSet(telefoneService.carregarListaTelefone(colaborador), Telefone.class));
		Usuario usuario = usuarioService.findUsuariocolaborador(colaborador.getCdpessoa());
		colaborador.setApelido(usuario !=null && usuario.getApelido()!=null ? usuario.getApelido() : "");
		
		Cliente cliente = clienteService.carregarDadosCliente(pedidoVenda.getCliente()); 
		List<Pedidovendapagamento> listaPedidoVendaPagamento = pedidovendapagamentoService.findByPedidovenda(pedidoVenda);
		List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre = pedidovendamaterialmestreService.findByPedidovenda(pedidoVenda);
		
		pedidoVenda.setListaPedidovendavalorcampoextra(pedidovendavalorcampoextraService.findByPedidoVenda(pedidoVenda));
		pedidoVenda.setListaPedidovendamaterial(listaProdutos);
//		this.ordenarMaterialproducao(pedidoVenda);
		
		Contato contato = null;
		if(pedidoVenda.getContato() != null){
			contato = contatoService.carregaContato(pedidoVenda.getContato()); 
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put(CampoComprovanteConfiguravel.TITULO.getNome(), "Pedido de Venda");
		if(pedidoVenda.getPedidovendasituacao() != null){
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_SITUACAO.getNome(), pedidoVenda.getPedidovendasituacao().getValue());
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_SITUACAONOME.getNome(), pedidoVenda.getPedidovendasituacao().getNome());
		}
		
		map.put(CampoComprovanteConfiguravel.CLIENTE_IDENTIFICADOR.getNome(), cliente.getIdentificador()==null ? "" : cliente.getIdentificador());
		map.put(CampoComprovanteConfiguravel.CLIENTE_NOME.getNome(), cliente.getNome());
		map.put(CampoComprovanteConfiguravel.CLIENTE_CPF.getNome(), cliente.getCpf());
		map.put(CampoComprovanteConfiguravel.CLIENTE_CNPJ.getNome(), cliente.getCnpj());
		map.put(CampoComprovanteConfiguravel.CLIENTE_EMAIL.getNome(), cliente.getEmail());
		map.put(CampoComprovanteConfiguravel.CLIENTE_RAZAOSOCIAL.getNome(), cliente.getRazaosocial() != null ? cliente.getRazaosocial() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_ENDERECO.getNome(), cliente.getEndereco() != null ? cliente.getEndereco().getLogradouroCompletoComCep() : "");
		if(cliente.getEndereco() != null && cliente.getEndereco().getMunicipio() != null && cliente.getEndereco().getMunicipio().getUf() != null && cliente.getEndereco().getMunicipio().getUf().getSigla() != null){
			map.put(CampoComprovanteConfiguravel.CLIENTE_UF_SIGLA.getNome(), cliente.getEndereco().getMunicipio().getUf().getSigla());			
		}else {
			map.put(CampoComprovanteConfiguravel.CLIENTE_UF_SIGLA.getNome(), "");
		}
		map.put(CampoComprovanteConfiguravel.CLIENTE_INSCRICAOESTADUAL.getNome(), cliente.getInscricaoestadual() == null ? "" : cliente.getInscricaoestadual());
		map.put(CampoComprovanteConfiguravel.CLIENTE_OBSERVACAO.getNome(), cliente.getObservacao() != null ? cliente.getObservacao() : "");
		map.put(CampoComprovanteConfiguravel.CLIENTE_RG.getNome(), cliente.getRg() != null ? cliente.getRg() : "");
		
		List<EnderecoComprovanteConfiguravel> listaEndereco = new ArrayList<EnderecoComprovanteConfiguravel>();
		if(cliente.getListaEndereco()!=null && !cliente.getListaEndereco().isEmpty()){			
			for (Endereco endereco : cliente.getListaEndereco()){
				if(endereco.getLogradouroCompletoComCep()!=null && endereco.getEnderecotipo()!=null){
					EnderecoComprovanteConfiguravel ecc = new EnderecoComprovanteConfiguravel();
					ecc.setENDERECOCOMPLETO(endereco.getLogradouroCompletoComCep());
					ecc.setENDERECOTIPO(endereco.getEnderecotipo().getCdenderecotipo() != null ? endereco.getEnderecotipo().getCdenderecotipo().toString() : "");
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null){
						ecc.setENDERECOUFSIGLA(endereco.getMunicipio().getUf().getSigla());						
					}else {
						ecc.setENDERECOUFSIGLA("");	
					}
					listaEndereco.add(ecc);
				}
			}			
		}
		map.put(CampoComprovanteConfiguravel.ENDERECOCLIENTE_LISTA.getNome(),listaEndereco);
		
		String telefones = cliente.getTelefonesSemQuebraLinha();
		map.put(CampoComprovanteConfiguravel.CLIENTE_TELEFONES.getNome(), telefones==null ? "" : telefones);
		
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATONOME.getNome(), contato == null ? "" : contato.getNome());
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATOEMAIL.getNome(), contato != null && contato.getEmailcontato() != null ? contato.getEmailcontato() : "");
		String telefonesContato = contato != null ? contato.getTelefonesSemQuebraLinha() : null;
		map.put(CampoComprovanteConfiguravel.CLIENTE_CONTATOTELEFONES.getNome(), telefonesContato==null ? "" : telefonesContato);
		
		Contato contatoresponsavel = clienteService.getContatoresponsavelByCliente(cliente);
		map.put(CampoComprovanteConfiguravel.CLIENTE_RESPONSAVEL.getNome(), contatoresponsavel != null && contatoresponsavel.getNome() != null ? contatoresponsavel.getNome() : "");
		
		map.put(CampoComprovanteConfiguravel.VENDEDOR_CODIGO.getNome(), colaborador.getCdpessoa());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_NOME.getNome(), colaborador.getNome());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_EMAIL.getNome(), colaborador.getEmail());
		map.put(CampoComprovanteConfiguravel.VENDEDOR_NOMEEXIBICAO.getNome(), colaborador.getApelido()!=null ? colaborador.getApelido() : colaborador.getNome());
		
		String telefonesVendedor = colaborador.getTelefonesSemQuebraLinha();
		map.put(CampoComprovanteConfiguravel.VENDEDOR_TELEFONES.getNome(), telefonesVendedor == null ? "" : telefonesVendedor);
		
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_CODIGO.getNome(), pedidoVenda.getCdpedidovenda());
		if(pedidoVenda.getVendaorcamento() != null && pedidoVenda.getVendaorcamento().getCdvendaorcamento() != null){
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_CODIGOORCAMENTO.getNome(), pedidoVenda.getVendaorcamento().getCdvendaorcamento());
		}
		
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DATA.getNome(), sdf.format(pedidoVenda.getDtpedidovenda()));
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DATA_HORA.getNome(), sdfHora.format(pedidoVenda.getDtpedidovenda()));
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_ENDERECO.getNome(), pedidoVenda.getEndereco() != null ? pedidoVenda.getEndereco().getLogradouroCompletoComCep() : "");
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_IDENTIFICADOREXTERNO.getNome(), pedidoVenda.getIdentificacaoexterna());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_PROJETO_NOME.getNome(), pedidoVenda.getProjeto() != null && pedidoVenda.getProjeto().getNome() != null ? pedidoVenda.getProjeto().getNome() : "");		
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_TIPO.getNome(), pedidoVenda.getPedidovendatipo()!=null && pedidoVenda.getPedidovendatipo().getDescricao() != null ? pedidoVenda.getPedidovendatipo().getDescricao() : "");
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_TERCEIRO.getNome(), pedidoVenda.getTerceiro()!=null  ? pedidoVenda.getTerceiro().getNome() : "");
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DATAHORAATUALIZACAO.getNome(), sdfHora.format(pedidoVenda.getDtaltera()));
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_TAXA.getNome(), pedidoVenda.getTaxapedidovenda());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_OBSERVACAOPEDIDO.getNome(), pedidoVenda.getObservacaoPedidoVendaTipo() != null ? pedidoVenda.getObservacaoPedidoVendaTipo() : "");
		for (Pedidovendamaterial pedidoVendaMaterial : listaProdutos) {
			pedidoVendaMaterial.setTotal(vendaService.getValortotal(pedidoVendaMaterial.getPreco(), pedidoVendaMaterial.getQuantidade(), pedidoVendaMaterial.getDesconto(), pedidoVendaMaterial.getMultiplicador(),
																pedidoVendaMaterial.getOutrasdespesas(), pedidoVendaMaterial.getValorSeguro()));
		}
		
		Map<String, String> camposAdicionais = new HashMap<String, String>();
		if (pedidoVenda.getListaPedidovendavalorcampoextra() != null){
			for (Pedidovendavalorcampoextra campoextra : pedidoVenda.getListaPedidovendavalorcampoextra()){
				camposAdicionais.put(campoextra.getCampoextrapedidovendatipo().getNome(), campoextra.getValor());
			}
		}
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_CAMPOS_ADICIONAIS.getNome(), camposAdicionais);
		
		Double pesoliquidototal = 0.0;
		Double pesobrutototal = 0.0;
		Money valorTotalProdutos = new Money();
		Double qtdemetrocubico = 0.0;
		Double fracao = 1.0;
		Double qtdetotalmetrocubico = 0.0;
		Double qtdeProduto = 0.0, qtdeServico = 0.0;
		Money valorTotalIcms = new Money();
		Money valorTotalDesoneracaoIcms = new Money();
		Money valorTotalIcmsSt = new Money();
		Money valorTotalFcp = new Money();
		Money valorTotalFcpSt = new Money();
		Money valorTotalDifal = new Money();
		Material material;
		
		List<ProdutoComprovanteConfiguravel> listaProdutosmestre = new ArrayList<ProdutoComprovanteConfiguravel>();
		List<ProdutoComprovanteConfiguravel> listaProdutosComprovante = new ArrayList<ProdutoComprovanteConfiguravel>();
		List<TotaisComprovanteConfiguravel> listaTotaisGrupomaterialComprovante = new ArrayList<TotaisComprovanteConfiguravel>();
		List<ProdutokitComprovanteConfiguravel> listaProdutoskit = new ArrayList<ProdutokitComprovanteConfiguravel>();
		List<ProdutokitflexivelComprovanteConfiguravel> listaProdutoskitflexivel = new ArrayList<ProdutokitflexivelComprovanteConfiguravel>();
		List<TotaisComprovanteConfiguravel> listaTotaisGrupomaterialmestreComprovante = new ArrayList<TotaisComprovanteConfiguravel>();
		List<ProdutoMestreGradeComprovanteConfiguravel> listaProdutosMestreGrade = new ArrayList<ProdutoMestreGradeComprovanteConfiguravel>();
		String msgacimaestoque = null;
		
		for(Pedidovendamaterial vm : pedidoVenda.getListaPedidovendamaterial()){
			ProdutoComprovanteConfiguravel pcc = new ProdutoComprovanteConfiguravel();
			if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
				pcc.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
			}
			if(vm.getMaterial() != null && vm.getMaterial().getPesobruto() != null){
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				if(vm.getMaterial().getPesobruto() != null){
					Double pesoBruto = vm.getMaterial().getPesobruto();
					if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
							vm.getMaterial().getUnidademedida() != null && 
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
					}
					pcc.setPESOBRUTO(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)).toString());
					pesobrutototal +=  pesoBruto * qtde;
				}
			}
			if(vm.getPesoVendaOuMaterial() != null){
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				pcc.setPESOLIQUIDO(SinedUtil.roundByParametro(new Double(vm.getPesoVendaOuMaterial() * qtde)).toString());
				pesoliquidototal += vm.getPesoVendaOuMaterial() * qtde;
			}
			if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
				pcc.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
			}
						
			if(vm.getMaterial().getMaterialmestregrade() != null) {
				ProdutoMestreGradeComprovanteConfiguravel produtoMestreGrade = new ProdutoMestreGradeComprovanteConfiguravel();
				produtoMestreGrade.setCODIGO(vm.getMaterial().getMaterialmestregrade().getCdmaterial().toString());
				if(!listaProdutosMestreGrade.contains(produtoMestreGrade)){
					produtoMestreGrade.setDESCRICAO(vm.getMaterial().getMaterialmestregrade().getNome());
					produtoMestreGrade.setIDENTIFICADORMATERIALMESTRE(vm.getMaterial().getMaterialmestregrade().getIdentificacao());
					listaProdutosMestreGrade.add(produtoMestreGrade);
				}			
				pcc.setCDMATERIALMESTREGRADE(vm.getMaterial().getMaterialmestregrade().getCdmaterial().toString());
			}
							
			pcc.setCDPEDIDOVENDAMATERIAL(vm.getCdpedidovendamaterial().toString());
			pcc.setCDMATERIAL(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCODIGO(vm.getMaterial().getIdentificacaoOuCdmaterial());
			pcc.setIDENTIFICADOR(vm.getMaterial().getIdentificacao() != null && !"".equals(vm.getMaterial().getIdentificacao()) ? vm.getMaterial().getIdentificacao() : "");
			pcc.setIDENTIFICADORESPECIFICO(vm.getIdentificadorespecifico() != null && !"".equals(vm.getIdentificadorespecifico()) ? vm.getIdentificadorespecifico() : "");
			pcc.setIDENTIFICADORINTEGRACAO(vm.getIdentificadorintegracao() != null ? vm.getIdentificadorintegracao() + "" : "");
			pcc.setDESCRICAO(vm.getMaterial().getNome());
			pcc.setCDMATERIALTIPO(vm.getMaterial().getMaterialtipo() != null ? vm.getMaterial().getMaterialtipo().getCdmaterialtipo() : null);
			pcc.setCDMATERIALCATEGORIA(vm.getMaterial().getMaterialcategoria() != null ? vm.getMaterial().getMaterialcategoria().getCdmaterialcategoria() : null);
			
			if(vm.getMaterialcoleta() != null){
				pcc.setCOLETACODIGO(vm.getMaterialcoleta().getIdentificacaoOuCdmaterial());
				pcc.setCOLETAIDENTIFICADOR(vm.getMaterialcoleta().getIdentificacao());
				pcc.setCOLETADESCRICAO(vm.getMaterialcoleta().getNome());
				if(vm.getMaterialcoleta() != null && vm.getMaterialcoleta().getListaCaracteristica() != null && !vm.getMaterialcoleta().getListaCaracteristica().isEmpty()){
					pcc.setCOLETACARACTERISTICA(vm.getMaterialcoleta().getListaCaracteristica().iterator().next().getNome());
				}
			}
			if(vm.getPneu() != null && vm.getPneu().getMaterialbanda() != null){
				pcc.setBANDACODIGO(vm.getPneu().getMaterialbanda().getIdentificacaoOuCdmaterial());
				pcc.setBANDAIDENTIFICADOR(vm.getPneu().getMaterialbanda().getIdentificacao());
				pcc.setBANDADESCRICAO(vm.getPneu().getMaterialbanda().getNome());
			}
			pcc.setUNIDADEMEDIDA(vm.getUnidademedida().getNome());
			pcc.setUNIDADEMEDIDASIMBOLO(vm.getUnidademedida().getSimbolo());
			pcc.setPRECO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
			pcc.setQUANTIDADE(vm.getQuantidade());
			pcc.setTOTAL(vm.getTotal().toString());
			pcc.setDESCONTO(vm.getDesconto() != null ? new java.text.DecimalFormat("#,###,##0.00").format(vm.getDesconto().getValue().doubleValue()) : "" );
			pcc.setOBSERVACAO(vm.getObservacao());
			pcc.setNCM(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null ? vm.getMaterial().getNcmcapitulo().getCodeFormat() : "");
			pcc.setNCMCOMPLETO(vm.getMaterial()!=null && vm.getMaterial().getNcmcompleto()!=null ? vm.getMaterial().getNcmcompleto() : "");
			pcc.setNCMDESCRICAO(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null && vm.getMaterial().getNcmcapitulo().getDescricao()!=null ? vm.getMaterial().getNcmcapitulo().getDescricao() : "");
			pcc.setIDENTIFICADORMATERIALMESTRE(vm.getIdentificadorinterno() != null ? vm.getIdentificadorinterno() : "");
			pcc.setENTREGA(vm.getDtprazoentrega()==null ? "" : sdf.format(vm.getDtprazoentrega()));
			pcc.setMARCA(vm.getPneu() != null && vm.getPneu().getPneumarca() != null ? vm.getPneu().getPneumarca().getNome() : "");
			pcc.setSERIE(vm.getPneu() != null && vm.getPneu().getSerie() != null ? vm.getPneu().getSerie() : "");
			pcc.setDOT(vm.getPneu() != null && vm.getPneu().getDot() != null ? vm.getPneu().getDot() : "");
			pcc.setMEDIDA(vm.getPneu() != null && vm.getPneu().getPneumedida() != null ? vm.getPneu().getPneumedida().getNome() : "");
			pcc.setMODELO(vm.getPneu() != null && vm.getPneu().getPneumodelo() != null ? vm.getPneu().getPneumodelo().getNome() : "");
			pcc.setNUMEROREFORMA(vm.getPneu() != null && vm.getPneu().getNumeroreforma() != null ? vm.getPneu().getNumeroreforma().toString() : "");
			pcc.setPNEUID(vm.getPneu() != null && vm.getPneu().getCdpneu() != null ? vm.getPneu().getCdpneu().toString() : "");
			pcc.setPNEUDESCRICAO(vm.getPneu() != null && vm.getPneu().getDescricao() != null ? vm.getPneu().getDescricao(): "");
			if(vm.getPneu() != null && vm.getPneu().getAcompanhaRoda() != null){
				pcc.setRODA(vm.getPneu().getAcompanhaRoda() == true ? "Sim": "N�o");	
			}
			pcc.setLONAS(vm.getPneu() != null && vm.getPneu().getLonas() != null ? vm.getPneu().getLonas().toString(): "");
            pcc.setVALORIPI(vm.getValoripi() != null ? vm.getValoripi().getValue().doubleValue() : null);
            pcc.setVALORIPI(MoneyUtils.moneyToDouble(vm.getValoripi()));
            pcc.setIPI(vm.getIpi());
            pcc.setVALORICMS(MoneyUtils.moneyToDouble(vm.getValoricms()));
            pcc.setICMS(vm.getIcms());
            pcc.setVALORICMSST(MoneyUtils.moneyToDouble(vm.getValoricmsst()));
            pcc.setICMSST(vm.getIcmsst());
            pcc.setVALORFCP(MoneyUtils.moneyToDouble(vm.getValorfcp()));
            pcc.setFCP(vm.getFcp());
            pcc.setVALORFCPST(MoneyUtils.moneyToDouble(vm.getValorfcpst()));
            pcc.setFCPST(vm.getFcpst());
            pcc.setVALORDIFAL(MoneyUtils.moneyToDouble(vm.getValordifal()));
            pcc.setDIFAL(vm.getDifal());
			pcc.setOUTRASDESPESAS(MoneyUtils.moneyToDouble(vm.getOutrasdespesas()));
			pcc.setSEGURO(MoneyUtils.moneyToDouble(vm.getValorSeguro()));
			
			if(vm.getPneu() != null && vm.getPneu().getOtrPneuTipo() != null){
				String desenhos ="";
				String servicoTipo ="";
				
				List<String> listaOtrDesenhos = new ArrayList<String>();
				List<String> listaOtrServicoTipo = new ArrayList<String>();	
				List<OtrPneuTipoOtrClassificacaoCodigo> listaotrclassificacaocodigo = otrPneuTipoOtrClassificacaoCodigoService.findListOtrPneuTipoOtrClassificacaoCodigo(vm.getPneu().getOtrPneuTipo());
				
				if(listaotrclassificacaocodigo != null && !listaotrclassificacaocodigo.isEmpty()){
					for(OtrPneuTipoOtrClassificacaoCodigo otrPneuTipoOtrClassificacaoCodigo: listaotrclassificacaocodigo){
						if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho() != null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho() != null){	
							if(!listaOtrDesenhos.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho())) {
								if(listaOtrDesenhos.size() > 0) desenhos+= " | ";
								desenhos+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho();
								listaOtrDesenhos.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho());		
							}
						}
						if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo()!= null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico() != null){
							if(!listaOtrServicoTipo.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico())){
								if(listaOtrServicoTipo.size() > 0) servicoTipo+=" | ";
								servicoTipo+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico();
								listaOtrServicoTipo.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico());
							}
						}
					}				
					pcc.setOTRPNEUTIPODESENHO(desenhos);
					pcc.setOTRPNEUTIPOSERVICO(servicoTipo);
				}
			}
			
			if(vm.getDtprazoentrega() != null && !org.apache.commons.lang.StringUtils.isBlank(vm.getMaterial().getNcmcompleto())){
				Date vencimento = SinedDateUtils.addMesData(vm.getDtprazoentrega() , Integer.parseInt(vm.getMaterial().getNcmcompleto()));
				pcc.setVENCIMENTOPRODUTOMESES(sdf.format(vencimento).toString());
			}
			
			if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
					!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
				pcc.setQUANTIDADEUNIDADEPRINCIPAL(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
			}
			
			qtdemetrocubico = null;
			fracao = null;
			
			if(vm.getUnidademedida() != null && vm.getUnidademedida().getSimbolo() != null && "M3".equalsIgnoreCase(vm.getUnidademedida().getSimbolo())){
				pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(vm.getQuantidade()),2));
				qtdetotalmetrocubico += vm.getQtdeMetrocunico(vm.getQuantidade());
				pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
			}else {
				if(vm.getMaterial().getUnidademedida() != null && vm.getMaterial().getUnidademedida().getSimbolo() != null && 
						"M3".equalsIgnoreCase(vm.getMaterial().getUnidademedida().getSimbolo())){
					if(vm.getFatorconversao() != null && vm.getFatorconversao() > 0){
						qtdemetrocubico = vm.getQuantidade();
						pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(qtdemetrocubico),2));
						pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco()*vm.getFatorconversaoQtdereferencia())).format(vm.getPreco()*vm.getFatorconversaoQtdereferencia()));
						qtdetotalmetrocubico += vm.getQtdeMetrocunico(qtdemetrocubico) ;
					}
				}else {
					material = materialService.findListaMaterialunidademedida(vm.getMaterial());
					if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
						for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
							if(materialunidademedida.getUnidademedida() != null && 
									"M3".equalsIgnoreCase(materialunidademedida.getUnidademedida().getSimbolo())){
								if(vm.getFatorconversao() != null && vm.getFatorconversao() > 0){
									fracao = vm.getFatorconversao();
								}else {
									fracao = unidademedidaService.getFatorconversao(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), materialunidademedida.getUnidademedida(), material, vm.getFatorconversao(), vm.getQtdereferencia());
								}
								if(fracao != null && fracao > 0){
									qtdemetrocubico = vm.getQuantidade() / fracao;
									pcc.setQUANTIDADEMETROCUBICO(SinedUtil.round(vm.getQtdeMetrocunico(qtdemetrocubico),2));
									qtdetotalmetrocubico += vm.getQtdeMetrocunico(qtdemetrocubico);
									pcc.setVALORMETROCUBICO(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco()*vm.getFatorconversaoQtdereferencia())).format(vm.getPreco()*vm.getFatorconversaoQtdereferencia()));
								}
							}
						}
					}
				}
			}
			
			if(vm.getLoteestoque() != null){
				pcc.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
			}
			if(vm.getMaterial().getMaterialgrupo() != null && vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null){
				pcc.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
			}
			
			if(vm.getComprimentooriginal() != null){
				pcc.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
			} else if(vm.getMaterial() != null && 
					vm.getMaterial().getMaterialproduto() != null && 
					vm.getMaterial().getMaterialproduto().getComprimento() != null){
				pcc.setCOMPRIMENTO(vm.getMaterial().getMaterialproduto().getComprimento().toString());
			}
			if(vm.getLargura() != null){
				pcc.setLARGURA(vm.getLargura().toString());
			} else if(vm.getMaterial() != null && 
					vm.getMaterial().getMaterialproduto() != null && 
					vm.getMaterial().getMaterialproduto().getLargura() != null){
				pcc.setLARGURA(vm.getMaterial().getMaterialproduto().getLargura().toString());
			}
			if(vm.getAltura() != null){
				pcc.setALTURA(vm.getAltura().toString());
			} else if(vm.getMaterial() != null && 
					vm.getMaterial().getMaterialproduto() != null && 
					vm.getMaterial().getMaterialproduto().getAltura() != null){
				pcc.setALTURA(vm.getMaterial().getMaterialproduto().getAltura().toString());
			}
			if(vm.getMaterial() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto()){
				qtdeProduto += vm.getQuantidade();
			}
			if(vm.getMaterial() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico()){
				qtdeServico += vm.getQuantidade();
			}
			
			Fornecedor fornecedor = vm.getFornecedor();
			if (fornecedor != null){
				pcc.setFORNECEDOR(fornecedor.getNome() != null ? fornecedor.getNome() : "");
				pcc.setFORNECEDORRAZAOSOCIAL(fornecedor.getRazaosocial() != null ? fornecedor.getRazaosocial() : "");
				pcc.setFORNECEDORCPFCNPJ(fornecedor.getCpfOuCnpj() != null ? fornecedor.getCpfOuCnpj() : "");
				pcc.setFORNECEDORINSCRICAOESTADUAL(fornecedor.getInscricaoestadual() != null ? fornecedor.getInscricaoestadual() : "");
				
				Endereco endereco = enderecoService.getEnderecoPrincipalFornecedor(fornecedor);
				List<Telefone> listaTelefone = telefoneService.findByPessoa(fornecedor);
				if(endereco != null){
					pcc.setFORNECEDORENDERECO(endereco.getLogradouroCompletoComCep());
				}
				if(listaTelefone != null && !listaTelefone.isEmpty()){
					for(Telefone tel : listaTelefone){
						if(tel.getTelefonetipo().equals(Telefonetipo.PRINCIPAL)){
							pcc.setFORNECEDORTELEFONE(tel.getTelefone());
						}
					}
				}
			}
			
			if(vm.getMaterial()!=null && vm.getMaterial().getLocalizacaoestoque()!=null && vm.getMaterial().getLocalizacaoestoque().getDescricao() !=null){
				pcc.setLOCALIZACAOESTOQUE(vm.getMaterial().getLocalizacaoestoque().getDescricao());
			}
			
			valorTotalProdutos = valorTotalProdutos.add(vm.getTotal());
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getCdmaterial() != null){
				pcc.setCDMATERIALMESTRE(vm.getMaterialmestre().getCdmaterial().toString());
			}
			listaProdutosComprovante.add(pcc);
			
			if((vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && 
					vm.getMaterialmestre().getVendapromocional()) || 
					(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional())){
				addMaterialKit(listaProdutoskit, vm);
			}else if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() != null && 
					vm.getMaterialmestre().getKitflexivel()){
				addMaterialKitflexivel(listaProdutoskitflexivel, vm, getItemMestre(listaPedidovendamaterialmestre, vm));
			}
			
			
			vendaService.adicionaGrupomaterial(vm.getMaterial().getMaterialgrupo(), listaTotaisGrupomaterialComprovante, vm.getTotal());
			if(vm.getMaterialmestre() != null){
				vendaService.adicionaGrupomaterialmestre(vm.getMaterialmestre().getMaterialgrupo(), listaTotaisGrupomaterialmestreComprovante, vm.getTotal());
			}
			
			if(pedidoVenda.getLocalarmazenagem() != null && pedidoVenda.getLocalarmazenagem().getQtdeacimaestoque() != null && 
					org.apache.commons.lang.StringUtils.isEmpty(msgacimaestoque)){
				Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(vm.getMaterial(), pedidoVenda.getLocalarmazenagem(), pedidoVenda.getEmpresa(), pedidoVenda.getProjeto());
				if (entrada == null){
					entrada = 0d;
				}
				Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(vm.getMaterial(), pedidoVenda.getLocalarmazenagem(), pedidoVenda.getEmpresa(), pedidoVenda.getProjeto());
				if (saida == null){
					saida = 0d;
				}
		
				Double qtddisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
				Double qtddisponivelAcimaestoque = qtddisponivel * pedidoVenda.getLocalarmazenagem().getQtdeacimaestoque() / 100;
				Double qtdreservada = pedidovendamaterialService.getQtdeReservada(vm.getMaterial(), pedidoVenda.getEmpresa(), pedidoVenda.getLocalarmazenagem());
				if(qtdreservada >= (qtddisponivel + qtddisponivelAcimaestoque)){
					msgacimaestoque = pedidoVenda.getLocalarmazenagem().getMsgacimaestoque();
				}
			}
			
			valorTotalIcms = valorTotalIcms.add(vm.getValoricms());
			valorTotalIcmsSt = valorTotalIcmsSt.add(vm.getValoricmsst());
			valorTotalDesoneracaoIcms = valorTotalDesoneracaoIcms.add(vm.getValordesoneracaoicms());
			valorTotalFcp = valorTotalFcp.add(vm.getValorfcp());
			valorTotalFcpSt = valorTotalFcpSt.add(vm.getValorfcpst());
			valorTotalDifal = valorTotalDifal.add(vm.getValordifal());
		}	
		
		
		for(Pedidovendamaterialmestre vmm : listaPedidovendamaterialmestre) {			
			ProdutoComprovanteConfiguravel pcc = new ProdutoComprovanteConfiguravel();
			pcc.setCDMATERIAL(vmm.getMaterial()!=null && vmm.getMaterial().getCdmaterial() != null ? vmm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCODIGO(vmm.getMaterial()!=null && vmm.getMaterial().getCdmaterial()!=null ? vmm.getMaterial().getCdmaterial().toString() : "");
			pcc.setCDMATERIALMESTRE(vmm.getMaterial()!=null && vmm.getMaterial().getCdmaterial()!=null ? vmm.getMaterial().getCdmaterial().toString() : "");
			pcc.setDESCRICAO(vmm.getMaterial()!=null && vmm.getMaterial().getNome()!=null ? vmm.getMaterial().getNome() : "");
			pcc.setCDMATERIALTIPO(vmm.getMaterial() != null && vmm.getMaterial().getMaterialtipo() != null ? vmm.getMaterial().getMaterialtipo().getCdmaterialtipo() : null);
			pcc.setCDMATERIALCATEGORIA(vmm.getMaterial() != null && vmm.getMaterial().getMaterialcategoria() != null ? vmm.getMaterial().getMaterialcategoria().getCdmaterialcategoria() : null);
			pcc.setALTURA(vmm.getAltura()!=null ? vmm.getAltura().toString() : "");
			pcc.setCOMPRIMENTO(vmm.getComprimento()!=null ? vmm.getComprimento().toString() : "");
			pcc.setLARGURA(vmm.getLargura()!=null ? vmm.getLargura().toString() : "");
			pcc.setQUANTIDADE(vmm.getQtde()!=null ? vmm.getQtde().doubleValue() : 0D);
			pcc.setCDMATERIALGRUPO(vmm.getMaterial()!=null && vmm.getMaterial().getMaterialgrupo() != null ? vmm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString() : "");
			pcc.setIDENTIFICADORMATERIALMESTRE(vmm.getIdentificadorinterno() != null ? vmm.getIdentificadorinterno() : "");
			pcc.setVALORIPI(vmm.getValoripi() != null? vmm.getValoripi().getValue().doubleValue(): 0D);
			listaProdutosmestre.add(pcc);
		}
		
		map.put(CampoComprovanteConfiguravel.TOTAL_METROCUBICO.getNome(), new DecimalFormat("#.##").format(qtdetotalmetrocubico));
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_PESOBRUTOTOTAL.getNome(), SinedUtil.roundByParametro(pesobrutototal));
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_PESOLIQUIDOTOTAL.getNome(), SinedUtil.roundByParametro(pesoliquidototal));
		map.put(CampoComprovanteConfiguravel.MATERIALGRUPO_LISTA.getNome(), listaTotaisGrupomaterialComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTO_LISTA.getNome(), listaProdutosComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTOMESTRE_LISTA.getNome(), listaProdutosmestre);
		map.put(CampoComprovanteConfiguravel.PRODUTO_VALORTOTAL.getNome(), valorTotalProdutos.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DATAENTREGA.getNome(), pedidoVenda.getDataentregaTrans() != null ? sdf.format(pedidoVenda.getDataentregaTrans()) : "");
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_OBSERVACAOINTERNA.getNome(), pedidoVenda.getObservacaointerna() != null ? pedidoVenda.getObservacaointerna() : "");
		map.put(CampoComprovanteConfiguravel.MATERIALGRUPOMESTRE_LISTA.getNome(), listaTotaisGrupomaterialmestreComprovante);
		
		map.put(CampoComprovanteConfiguravel.PRODUTO_VALORTOTAL.getNome(), valorTotalProdutos.toString());
		map.put(CampoComprovanteConfiguravel.PRODUTO_QTDETOTALPRODUTO.getNome(), qtdeProduto.toString());
		map.put(CampoComprovanteConfiguravel.PRODUTO_QTDETOTALSERVICO.getNome(), qtdeServico.toString());
		map.put(CampoComprovanteConfiguravel.PRODUTO_LISTA.getNome(), listaProdutosComprovante);
		map.put(CampoComprovanteConfiguravel.PRODUTOKIT_LISTA.getNome(), listaProdutoskit);
		map.put(CampoComprovanteConfiguravel.PRODUTOKITFLEXIVEL_LISTA.getNome(), listaProdutoskitflexivel);
		map.put(CampoComprovanteConfiguravel.LOCALARMAZENAGEM_MENSAGEMLIMITE.getNome(), org.apache.commons.lang.StringUtils.isNotEmpty(msgacimaestoque) ? msgacimaestoque : "");
		
		map.put(CampoComprovanteConfiguravel.PRODUTOMESTREGRADE_LISTA.getNome(), listaProdutosMestreGrade);
		
		vendaService.addListaProdutoagrupadoForComprovante(listaProdutosComprovante, map);
		vendaService.addListaColetaForComprovante(pedidoVenda, map);
		
		List<PagamentoComprovanteConfiguravel> listaPagamentoComprovante = new ArrayList<PagamentoComprovanteConfiguravel>();
		Money valorTotalPagamento = new Money();
		for(Pedidovendapagamento pvp : listaPedidoVendaPagamento){
			PagamentoComprovanteConfiguravel pcc = new PagamentoComprovanteConfiguravel();
			pcc.setFORMAPAGAMENTO(pvp.getDocumentotipo().getNome());
			pcc.setNUMEROBANCO(pvp.getBanco());
			pcc.setCONTA(pvp.getConta());
			pcc.setAGENCIA(pvp.getAgencia());
			if(org.apache.commons.lang.StringUtils.isNotEmpty(pvp.getNumero())){
				pcc.setNUMERO(pvp.getNumero());
			}else if(pvp.getDocumento() != null && pvp.getDocumento().getCddocumento() != null){
				pcc.setNUMERO(pvp.getDocumento().getCddocumento().toString());
			}
			pcc.setVENCIMENTO( sdf.format(pvp.getDataparcela()) );
			pcc.setVALOR(pvp.getValororiginal().toString());
			valorTotalPagamento = valorTotalPagamento.add(pvp.getValororiginal());
			listaPagamentoComprovante.add(pcc);
		}
		map.put(CampoComprovanteConfiguravel.PAGAMENTO_VALORTOTAL.getNome(), valorTotalPagamento.toString());
		map.put(CampoComprovanteConfiguravel.PAGAMENTO_LISTA.getNome(), listaPagamentoComprovante);
		
		StringBuilder observacao = new StringBuilder();
		if(pedidoVenda.getObservacao() != null){
			observacao.append(pedidoVenda.getObservacao());
		}
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getObservacaovenda() != null && !"".equals(pedidoVenda.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(pedidoVenda.getEmpresa().getObservacaovenda());
		}
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_OBSERVACAO.getNome(), observacao.toString());
		
		if(pedidoVenda.getPrazopagamento() != null){
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_PRAZOPAGAMENTO.getNome(), pedidoVenda.getPrazopagamento().getNome());
		}
		
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_FORMAPAGAMENTO.getNome(), pedidoVenda.getDocumentotipo()==null ? "" : pedidoVenda.getDocumentotipo().getNome() );
		
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getCdpessoa() != null){
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_NOME.getNome(), pedidoVenda.getEmpresa().getRazaosocialOuNome() != null ?  pedidoVenda.getEmpresa().getRazaosocialOuNome() : null);
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_NOMEFANTASIA.getNome(), pedidoVenda.getEmpresa().getNomefantasia() != null ?  pedidoVenda.getEmpresa().getNomefantasia() : null);
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_CNPJ.getNome(), pedidoVenda.getEmpresa().getCnpj() != null ?  pedidoVenda.getEmpresa().getCnpj().toString() : null);
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_INSCRICAOESTADUAL.getNome(), pedidoVenda.getEmpresa().getInscricaoestadual());
			pedidoVenda.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(pedidoVenda.getEmpresa())));
			pedidoVenda.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.findByPessoa(pedidoVenda.getEmpresa())));
			Endereco endereco = pedidoVenda.getEmpresa().getEndereco();
			String telefonesEmpresa = pedidoVenda.getEmpresa().getTelefonesComTipoSemQuebraLinha();
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_ENDERECO.getNome(), endereco != null ? endereco.getLogradouroCompletoComBairro() : null);
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_TELEFONE.getNome(), telefonesEmpresa != null ? telefonesEmpresa : "");
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_SITE.getNome(), pedidoVenda.getEmpresa().getSite() != null && !pedidoVenda.getEmpresa().getSite().equals("") ? pedidoVenda.getEmpresa().getSite() : "");
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_EMPRESA_EMAIL.getNome(), pedidoVenda.getEmpresa().getEmail() != null && !pedidoVenda.getEmpresa().getEmail().equals("") ? pedidoVenda.getEmpresa().getEmail() : "");
			if(pedidoVenda.getEmpresa().getLogomarca() != null)
				map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_LOGO.getNome(), "'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+pedidoVenda.getEmpresa().getLogomarca().getCdarquivo().toString()+"'");
			else
				map.put(CampoComprovanteConfiguravel.LOGO.getNome(), logoNaoDisponivel);
		}
		
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORVALECOMPRA.getNome(), pedidoVenda.getValorusadovalecompra() == null ? new Money(0.0) : pedidoVenda.getValorusadovalecompra());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORFRETE.getNome(), pedidoVenda.getValorfrete() == null ? new Money(0.0) : pedidoVenda.getValorfrete());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORDESCONTO.getNome(), pedidoVenda.getDesconto() == null ? new Money(0.0).toString() : pedidoVenda.getDesconto().toString());
		if(pedidoVenda.getFrete() != null){
			if(pedidoVenda.getFrete().equals(ResponsavelFrete.TERCEIROS) && pedidoVenda.getTerceiro() != null){
				map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DESCRICAOFRETE.getNome(), " (" + pedidoVenda.getFrete().getNome() + " - " + pedidoVenda.getTerceiro().getNome() + ")");
			} else {
				map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DESCRICAOFRETE.getNome(), " (" + pedidoVenda.getFrete().getNome() + ")");
			}
		} else {
			map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_DESCRICAOFRETE.getNome(), "");
		}
		
		Money valorFinal = valorTotalProdutos;
		if (pedidoVenda.getValorfrete() != null)
			valorFinal = valorFinal.add(pedidoVenda.getValorfrete());
		if (pedidoVenda.getDesconto() != null)
			valorFinal = valorFinal.subtract(pedidoVenda.getDesconto());
		if (pedidoVenda.getValorusadovalecompra() != null)
			valorFinal = valorFinal.subtract(pedidoVenda.getValorusadovalecompra());
		pedidoVenda.setValorfinal(valorFinal);

		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORTOTAL.getNome(), pedidoVenda.getValorfinal() == null ? new Money(0.0).toString() : pedidoVenda.getValorfinal().toString());
		this.setTotalImpostos(pedidoVenda);
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORFINALMAISIPI.getNome(), pedidoVenda.getValorfinal().add(pedidoVenda.getTotalipi()).toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORFINALCOMIMPOSTOS.getNome(), pedidoVenda.getValorfinal()
																				.add(pedidoVenda.getTotalipi())
																				.add(valorTotalIcmsSt)
																				.subtract(valorTotalDesoneracaoIcms)
																				.add(valorTotalFcpSt).toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_LOCALARMAZENAGEM.getNome(), pedidoVenda.getLocalarmazenagem() != null && pedidoVenda.getLocalarmazenagem().getNome() != null ? pedidoVenda.getLocalarmazenagem().getNome() : "");
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORICMS.getNome(), valorTotalIcms.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORICMSST.getNome(), valorTotalIcmsSt.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORDESONERACAOICMS.getNome(), valorTotalDesoneracaoIcms.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORFCP.getNome(), valorTotalFcp.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORFCPST.getNome(), valorTotalFcpSt.toString());
		map.put(CampoComprovanteConfiguravel.PEDIDOVENDA_VALORDIFAL.getNome(), valorTotalDifal.toString());
		
		Empresa empresa = empresaService.loadEmpresa(pedidoVenda.getEmpresa());
		
		if(empresa.getLogomarca() != null && empresa.getLogomarca().getCdarquivo() != null)
			DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), empresa.getLogomarca().getCdarquivo());
		
		if(empresa.getLogomarca() != null)
			map.put(CampoComprovanteConfiguravel.LOGO.getNome(), "'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+empresa.getLogomarca().getCdarquivo().toString()+"'");
		else
			map.put(CampoComprovanteConfiguravel.LOGO.getNome(), logoNaoDisponivel);
		map.put(CampoComprovanteConfiguravel.EMPRESA.getNome(), empresa == null ? null : empresa.getRazaosocialOuNome());
		map.put(CampoComprovanteConfiguravel.USUARIO.getNome(), Util.strings.toStringDescription(Neo.getUser()));
		map.put(CampoComprovanteConfiguravel.DATA.getNome(), sdf.format(new Date(System.currentTimeMillis())));
		map.put(CampoComprovanteConfiguravel.DATA_EXTENSO.getNome(), SinedDateUtils.dataExtenso(new java.sql.Date(System.currentTimeMillis())));
		map.put(CampoComprovanteConfiguravel.QUEBRA_LINHA_TXT.getNome(), "\r\n");
		map.put(CampoComprovanteConfiguravel.QUEBRA_LINHA_HTML.getNome(), "<br/>");
		
		return map;
	}
	
	protected void addMaterialKit(List<ProdutokitComprovanteConfiguravel> listaProdutoskit, Pedidovendamaterial vm) {
		if(listaProdutoskit != null && vm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() && vm.getMaterialmestre().getVendapromocional()){
				boolean adicionar = true;
				for(ProdutokitComprovanteConfiguravel pkc : listaProdutoskit){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterialmestre())){
						adicionar = false;
						ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
						pkic.setDESCRICAO(vm.getMaterial().getNome());
						pkic.setPRECO(vm.getPreco());
						pkic.setQUANTIDADE(vm.getQuantidade());
						pkic.setTOTAL(vm.getTotalproduto().getValue().doubleValue());
						pkic.setMaterial(vm.getMaterial());
						
						if(pkc.getLISTAPRODUTOKITITEM() == null) 
							pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
						
						if(pkc.getTOTAL() == null) pkc.setTOTAL(pkic.getTOTAL());
						else pkc.setTOTAL(pkc.getTOTAL() + pkic.getTOTAL());
						
						pkc.getLISTAPRODUTOKITITEM().add(pkic);
						break;
					}
				}
				if(adicionar){
					ProdutokitComprovanteConfiguravel pkc = new ProdutokitComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(vm.getMaterialmestre().getNome());
					pkc.setMaterial(vm.getMaterialmestre());
					pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
					
					ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
					pkic.setDESCRICAO(vm.getMaterial().getNome());
					pkic.setPRECO(vm.getPreco());
					pkic.setQUANTIDADE(vm.getQuantidade());
					pkic.setTOTAL(vm.getTotalproduto().getValue().doubleValue());
					
					pkic.setMaterial(vm.getMaterial());
					pkc.setTOTAL(pkic.getTOTAL());
					
					pkc.getLISTAPRODUTOKITITEM().add(pkic);
					listaProdutoskit.add(pkc);
				}
			}else if(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional()){
				boolean adicionar = true;
				for(ProdutokitComprovanteConfiguravel pkc : listaProdutoskit){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterial())){
						adicionar = false;
						break;
					}
				}
				if(adicionar){
					ProdutokitComprovanteConfiguravel pkc = new ProdutokitComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterial().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(vm.getMaterial().getNome());
					pkc.setQUANTIDADE(vm.getQuantidade());
					pkc.setMaterial(vm.getMaterial());
					pkc.setLISTAPRODUTOKITITEM(new ArrayList<ProdutokititemComprovanteConfiguravel>());
					
					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vm.getMaterial().getCdmaterial());
					material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
					
					try{
						material.setProduto_altura(vm.getAltura());
						material.setProduto_largura(vm.getLargura());
						material.setQuantidade(vm.getQuantidade());
						materialrelacionadoService.calculaQuantidadeKit(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
						Double total = 0d;
						Double precoTotal = 0d;
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getQuantidade() != null && vm.getQuantidade() != null){
								ProdutokititemComprovanteConfiguravel pkic = new ProdutokititemComprovanteConfiguravel();
								pkic.setDESCRICAO(materialrelacionado.getMaterialpromocao().getNome());
								Double valorvendaItemKit = materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0d;
								Double preco = vm.getPreco() != null ? vm.getPreco() : 0d;
								Double valorvendaMaterial = material.getValorvenda() != null ? material.getValorvenda() : 0d;
								if(valorvendaMaterial == 0) valorvendaMaterial = 1d;
								pkic.setPRECO(preco * ((valorvendaItemKit * 100) / valorvendaMaterial) / 100);
								pkic.setQUANTIDADE(materialrelacionado.getQuantidade() * vm.getQuantidade());
								pkic.setTOTAL(pkic.getPRECO() * pkic.getQUANTIDADE());
								total += pkic.getTOTAL();
								pkic.setMaterial(vm.getMaterial());
								
								pkc.getLISTAPRODUTOKITITEM().add(pkic);
							}
						}
						pkc.setTOTAL(total);
						pkc.setPRECO(precoTotal);
					}
					listaProdutoskit.add(pkc);
				}
			}
		}
	}
	
	protected Pedidovendamaterialmestre getItemMestre(List<Pedidovendamaterialmestre> lista, Pedidovendamaterial vendamaterial){
		if(SinedUtil.isListNotEmpty(lista) && vendamaterial != null && 
				org.apache.commons.lang.StringUtils.isNotEmpty(vendamaterial.getIdentificadorinterno())){
			for(Pedidovendamaterialmestre vendamaterialmestre : lista){
				if(org.apache.commons.lang.StringUtils.isNotEmpty(vendamaterialmestre.getIdentificadorinterno()) && 
						vendamaterialmestre.getIdentificadorinterno().equals(vendamaterial.getIdentificadorinterno())){
					return vendamaterialmestre;
				}
			}
		}
		return null;
	}
	
	protected void addMaterialKitflexivel(List<ProdutokitflexivelComprovanteConfiguravel> listaProdutoskitflexivel, Pedidovendamaterial vm, Pedidovendamaterialmestre vmm) {
		if(listaProdutoskitflexivel != null && vm != null && vmm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() && vm.getMaterialmestre().getKitflexivel()){
				boolean adicionar = true;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				for(ProdutokitflexivelComprovanteConfiguravel pkc : listaProdutoskitflexivel){
					if(pkc.getMaterial() != null && pkc.getMaterial().equals(vm.getMaterialmestre()) && 
							pkc.getIDENTIFICADORMATERIALMESTRE() != null && 
							vm.getIdentificadorinterno() != null &&
							pkc.getIDENTIFICADORMATERIALMESTRE().equals(vm.getIdentificadorinterno())){
						adicionar = false;
						ProdutokitflexivelitemComprovanteConfiguravel pkic = new ProdutokitflexivelitemComprovanteConfiguravel();
						pkic.setCODIGO(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
						pkic.setDESCRICAO(vm.getMaterial().getNome());
						pkic.setPRECO(vm.getPreco());
						pkic.setQUANTIDADE(vm.getQuantidade());
						pkic.setTOTALITEM(vm.getTotalproduto().getValue().doubleValue());
						
						if(vm.getMaterial().getMaterialgrupo() != null && 
								vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
							pkic.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
						
						pkic.setNCMCOMPLETO(vm.getMaterial().getNcmcompleto());
						
						if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
							DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
							pkic.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
						}
						
						pkic.setOBSERVACAO(vm.getObservacao());
						if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
							pkic.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
						}
						pkic.setPESOBRUTO(vm.getMaterial().getPesobruto());
						pkic.setPESOLIQUIDO(vm.getMaterial().getPesobruto());
						
						pkic.setENTREGA(vm.getDtprazoentrega()==null ? "" : sdf.format(vm.getDtprazoentrega()));
						if(vm.getComprimentooriginal() != null){
							pkic.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							pkic.setLARGURA(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							pkic.setALTURA(vm.getAltura().toString());
						}
						if(vm.getLoteestoque() != null){
							pkic.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
						}
						
						pkic.setMaterial(vm.getMaterial());
						
						if(pkc.getLISTAPRODUTOKIFLEXIVELTITEM() == null) 
							pkc.setLISTAPRODUTOKIFLEXIVELTITEM(new ArrayList<ProdutokitflexivelitemComprovanteConfiguravel>());
						
//						if(pkc.getTOTALKIT() == null) pkc.setTOTALKIT(pkic.getTOTALITEM());
//						else pkc.setTOTALKIT(pkc.getTOTALKIT() + pkic.getTOTALITEM());
						
						pkc.getLISTAPRODUTOKIFLEXIVELTITEM().add(pkic);
						break;
					}
				}
				if(adicionar){
					ProdutokitflexivelComprovanteConfiguravel pkc = new ProdutokitflexivelComprovanteConfiguravel();
					pkc.setCODIGO(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					pkc.setDESCRICAO(org.apache.commons.lang.StringUtils.isNotEmpty(vmm.getNomealternativo()) ? vmm.getNomealternativo() : vmm.getMaterial().getNome());
					pkc.setQUANTIDADE(vmm.getQtde());
					pkc.setPRECO(vmm.getPreco());
					pkc.setDESCONTO(vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : null);
					pkc.setTOTALKIT(vmm.getPreco()*vmm.getQtde() - (vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : 0d));
					pkc.setIDENTIFICADORMATERIALMESTRE(vmm.getIdentificadorinterno() != null ? vmm.getIdentificadorinterno() : "");
					pkc.setENTREGA(vmm.getDtprazoentrega()==null ? "" : sdf.format(vmm.getDtprazoentrega()));
					pkc.setUNIDADEMEDIDA(vmm.getUnidademedida()!=null && vmm.getUnidademedida().getNome()!=null ? vmm.getUnidademedida().getNome() : "");
					
					if(vm.getMaterialmestre().getMaterialgrupo() != null && 
							vm.getMaterialmestre().getMaterialgrupo().getCdmaterialgrupo() != null)
						pkc.setCDMATERIALGRUPO(vm.getMaterialmestre().getMaterialgrupo().getCdmaterialgrupo().toString());
					
					pkc.setNCMCOMPLETO(vmm.getMaterial().getNcmcompleto());
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getArquivo() != null && vm.getMaterialmestre().getArquivo().getCdarquivo() != null){
						DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterialmestre().getArquivo().getCdarquivo());
						pkc.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterialmestre().getArquivo().getCdarquivo().toString()+"'");
					}
					
					pkc.setOBSERVACAO(vm.getObservacao());
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						pkc.setCARACTERISTICA(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					pkc.setPESOBRUTO(vm.getMaterialmestre().getPesobruto());
					pkc.setPESOLIQUIDO(vm.getMaterialmestre().getPesobruto());
					
					pkc.setMaterial(vm.getMaterialmestre());
					pkc.setLISTAPRODUTOKIFLEXIVELTITEM(new ArrayList<ProdutokitflexivelitemComprovanteConfiguravel>());
					
					ProdutokitflexivelitemComprovanteConfiguravel pkic = new ProdutokitflexivelitemComprovanteConfiguravel();
					pkic.setCODIGO(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
					pkic.setDESCRICAO(vm.getMaterial().getNome());
					pkic.setPRECO(vm.getPreco());
					pkic.setQUANTIDADE(vm.getQuantidade());
					pkic.setTOTALITEM(vm.getTotalproduto().getValue().doubleValue());
					
					if(vm.getMaterial().getMaterialgrupo() != null && 
							vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo() != null)
						pkic.setCDMATERIALGRUPO(vm.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
					
					pkic.setNCMCOMPLETO(vm.getMaterial().getNcmcompleto());
					
					if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
						DownloadFileServlet.addCdfile(NeoWeb.getRequestContext().getSession(), vm.getMaterial().getArquivo().getCdarquivo());
						pkic.setIMAGEM("'" + SinedUtil.getUrlWithContext() +  "/DOWNLOADFILE/"+vm.getMaterial().getArquivo().getCdarquivo().toString()+"'");
					}
					
					pkic.setOBSERVACAO(vm.getObservacao());
					if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
						pkic.setCARACTERISTICA(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
					}
					pkic.setPESOBRUTO(vm.getMaterial().getPesobruto());
					pkic.setPESOLIQUIDO(vm.getMaterial().getPesobruto());
					
					pkic.setENTREGA(vm.getDtprazoentrega()==null ? "" : sdf.format(vm.getDtprazoentrega()));
					if(vm.getComprimentooriginal() != null){
						pkic.setCOMPRIMENTO(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						pkic.setLARGURA(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						pkic.setALTURA(vm.getAltura().toString());
					}
					if(vm.getLoteestoque() != null){
						pkic.setLOTEESTOQUE(vm.getLoteestoque().getNumerovalidade());
					}
					
					pkic.setMaterial(vm.getMaterial());
					
					pkc.getLISTAPRODUTOKIFLEXIVELTITEM().add(pkic);
					listaProdutoskitflexivel.add(pkc);
				}
			}
		}
	}
	
	/**
	 * M�todo que cria o Report do comprovante do pedido de venda
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Report criaComprovantePedidovendaReport(Pedidovenda pedidoVenda){	
		
		List<Pedidovendamaterial> listaProdutos = pedidovendamaterialService.findByPedidoVenda(pedidoVenda, "pedidovendamaterial.ordem, pedidovendamaterial.cdpedidovendamaterial");
		List<Pedidovendapagamento> listaPagamentos = pedidovendapagamentoService.findByPedidovenda(pedidoVenda);
		
		List<ResumoMaterialunidadeconversao> listaResumoMaterialunidadeconversao = materialService.preencheMaterialWithConversoesPedidovenda(listaProdutos);
		
//		Colaborador colaborador = colaboradorService.load(pedidoVenda.getColaborador(), "colaborador.cdpessoa, colaborador.nome, colaborador.email");
		Colaborador colaborador = pedidoVenda.getColaborador();
		Cliente cliente = clienteService.carregarDadosCliente(pedidoVenda.getCliente()); 
		
		Report report = new Report("/faturamento/comprovantepedidovenda");		
		report.addParameter("TITULO", "Pedido de Venda");
		report.addSubReport("SUB_PEDIDOVENDAMATERIAL", new Report("/faturamento/pedidovendamaterial_sub"));
		report.addSubReport("SUB_PEDIDOVENDAPAGAMENTO", new Report("/faturamento/pedidovendapagamento_sub"));
		report.addParameter("CLIENTE", cliente.getNome());
		report.addParameter("CPF", cliente.getCpf());
		report.addParameter("CNPJ", cliente.getCnpj());
		report.addParameter("VENDEDOR", colaborador.getNome());
		report.addParameter("VENDEDOREMAIL", colaborador.getEmail());
		String identificador = pedidoVenda.getIdentificador() != null && !"".equals(pedidoVenda.getIdentificador()) ? pedidoVenda.getIdentificador() : null; 
		report.addParameter("CODIGOVENDA", identificador != null ? identificador : pedidoVenda.getCdpedidovenda().toString());
		report.addParameter("DATAVENDA", pedidoVenda.getDtpedidovenda());
		report.addParameter("ENDERECO", pedidoVenda.getEndereco() != null ? pedidoVenda.getEndereco().getLogradouroCompletoComCep() : (cliente.getEndereco() != null ? cliente.getEndereco().getLogradouroCompletoComCep() : ""));
		
		String telefones = cliente.getTelefonesSemQuebraLinha();
		if(telefones != null){
			report.addParameter("CLIENTETELEFONES", telefones.replaceAll("\\|", ""));
		}
		
		String pedidovendatipo = "";
		if(pedidoVenda.getPedidovendatipo()!=null){
			pedidovendatipo = pedidoVenda.getPedidovendatipo().getDescricao();
		}
		report.addParameter("PEDIDOVENDATIPO", pedidovendatipo);	
		
		Double totalpesobruto = 0.0;
		Double totalpesoliquido = 0.0;
		Double pesobruto = null;
		for (Pedidovendamaterial pedidoVendaMaterial : listaProdutos) {
			pedidoVendaMaterial.setTotal(vendaService.getValortotal(pedidoVendaMaterial.getPreco(), pedidoVendaMaterial.getQuantidade(), pedidoVendaMaterial.getDesconto(), pedidoVendaMaterial.getMultiplicador(),
															pedidoVendaMaterial.getOutrasdespesas(), pedidoVendaMaterial.getValorSeguro()));
			if(pedidoVendaMaterial.getMaterial() != null){
				if(pedidoVendaMaterial.getMaterial().getPesobruto() != null){
					pesobruto = pedidoVendaMaterial.getMaterial().getPesobruto();
					if(pedidoVendaMaterial.getMaterial().getUnidademedida() != null && !pedidoVendaMaterial.getMaterial().getUnidademedida().equals(pedidoVendaMaterial.getUnidademedida())){
						Double fracao = pedidoVendaMaterial.getFatorconversaoQtdereferencia();
						if(fracao != null && fracao > 0){
							pesobruto = pesobruto/fracao;
						}
					}
					totalpesobruto += pedidoVendaMaterial.getQuantidade() * pesobruto;
				}
				if(pedidoVendaMaterial.getLoteestoque() != null && pedidoVendaMaterial.getLoteestoque().getDescriptionAutocomplete() != null){
					pedidoVendaMaterial.setMaterial(new Material(pedidoVendaMaterial.getMaterial().getCdmaterial(), pedidoVendaMaterial.getMaterial().getNome() + " - "  + pedidoVendaMaterial.getLoteestoque().getDescriptionAutocomplete()));
				}
				if(pedidoVendaMaterial.getObservacao() != null && !"".equals(pedidoVendaMaterial.getObservacao())){
					pedidoVendaMaterial.setMaterial(new Material(pedidoVendaMaterial.getMaterial().getCdmaterial(), pedidoVendaMaterial.getMaterial().getNome() + "\n" + pedidoVendaMaterial.getObservacao(), pedidoVendaMaterial.getMaterial().getIdentificacao()));
				}
				if(pedidoVendaMaterial.getPesoVendaOuMaterial() != null){
					totalpesoliquido += pedidoVendaMaterial.getQuantidade() * pedidoVendaMaterial.getPesoVendaOuMaterial();
				}
			}
		}
		report.addParameter("PESOBRUTOTOTAL", totalpesobruto == null || totalpesobruto == 0 ? null : SinedUtil.roundByParametro(totalpesobruto));
		report.addParameter("PESOLIQUIDOTOTAL", totalpesoliquido == null || totalpesoliquido == 0 ? null : SinedUtil.roundByParametro(totalpesoliquido));
		report.addParameter("LISTAPAGAMENTOS", listaPagamentos);
		report.addParameter("LISTAPRODUTOS", listaProdutos);
		
		StringBuilder observacao = new StringBuilder();
		if(pedidoVenda.getObservacao() != null){
			observacao.append(pedidoVenda.getObservacao());
		}
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getObservacaovenda() != null && !"".equals(pedidoVenda.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(pedidoVenda.getEmpresa().getObservacaovenda());
		}
		report.addParameter("OBSERVACAO", observacao.toString());
//		report.addParameter("PRAZOENTREGA", pedidoVenda.getDtprazoentrega());
		
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getCdpessoa() != null){
			report.addParameter("EMPRESAVENDA", pedidoVenda.getEmpresa().getRazaosocialOuNome() != null ?  pedidoVenda.getEmpresa().getRazaosocialOuNome() : null);
			report.addParameter("CNPJEMPRESA", pedidoVenda.getEmpresa().getCnpj() != null ?  pedidoVenda.getEmpresa().getCnpj().toString() : null);
			report.addParameter("INSCRICAOESTADUAL", pedidoVenda.getEmpresa().getInscricaoestadual());
			pedidoVenda.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(pedidoVenda.getEmpresa())));
			pedidoVenda.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.carregarListaTelefone(pedidoVenda.getEmpresa())));
			Endereco endereco = pedidoVenda.getEmpresa().getEndereco();
			Telefone telefone = pedidoVenda.getEmpresa().getTelefone();
			report.addParameter("ENDERECOEMPRESA", endereco != null ? endereco.getLogradouroCompletoComBairro() : null);
			report.addParameter("TELEFONEEMPRESA", telefone != null ? telefone.getTelefone() : null);
			report.addParameter("LOGOVENDA", SinedUtil.getLogo(pedidoVenda.getEmpresa()));
		}
		
		report.addParameter("VALORFRETE", pedidoVenda.getValorfrete() == null ? new Money(0.0) : pedidoVenda.getValorfrete());
		report.addParameter("VALORDESCONTO", pedidoVenda.getDesconto() == null ? new Money(0.0) : pedidoVenda.getDesconto());
		
		if(pedidoVenda.getFrete() != null){
			if(pedidoVenda.getFrete().equals(ResponsavelFrete.TERCEIROS) && pedidoVenda.getTerceiro() != null){
				report.addParameter("DESCRICAOFRETE", " (" + pedidoVenda.getFrete().getNome() + " - " + pedidoVenda.getTerceiro().getNome() + ")");
			} else {
				report.addParameter("DESCRICAOFRETE", " (" + pedidoVenda.getFrete().getNome() + ")");
			}
		} else {
			report.addParameter("DESCRICAOFRETE", "");
		}
		
		Empresa empresa = empresaService.loadPrincipal();
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		if(listaResumoMaterialunidadeconversao != null && !listaResumoMaterialunidadeconversao.isEmpty()){
			report.addParameter("LISTARESUMOMATERIALUNIDADECONVERSAO", listaResumoMaterialunidadeconversao);
			report.addSubReport("SUB_RESUMOMATERIALUNIDADECONVERSAO", new Report("/faturamento/resumomaterialunidadeconversao"));
		}
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForCancelamento(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovenda> findForCancelamento(String whereIn) {
		return pedidovendaDAO.findForCancelamento(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateCancelamento(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateCancelamento(String whereIn) {
		pedidovendaDAO.updateCancelamento(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @since 25/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updatePrevista(String whereIn) {
		pedidovendaDAO.updatePrevista(whereIn);
		try{
			pedidovendaDAO.updateDepencia(whereIn);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateConfirmacao(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateConfirmacao(String whereIn) {
		pedidovendaDAO.updateConfirmacao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateConfirmacaoParcial(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public void updateConfirmacaoParcial(String whereIn) {
		pedidovendaDAO.updateConfirmacaoParcial(whereIn);
	}
	
	public List<Vendamaterial> preencheListaVendaMaterial(Pedidovenda pedidovenda, Integer percentual) {
		return preencheListaVendaMaterial(pedidovenda, percentual, null, null);
	}
	
	/**
	 * Preenche a lista de Vendamaterial a partir de um pedido de venda.
	 *
	 * @param pedidoVenda
	 * @return
	 * @since 08/11/2011
	 * @author Rodrigo Freitas
	 * @param percentual 
	 * @param somenteproduto 
	 * @param somenteservico 
	 */
	public List<Vendamaterial> preencheListaVendaMaterial(Pedidovenda pedidovenda, Integer percentual, Boolean somenteservico, Boolean somenteproduto) {
		List<Vendamaterial> list = new ArrayList<Vendamaterial>();
		
		if(pedidovenda.getListaPedidovendamaterial() != null){
			Vendamaterial vendamaterial;
			Double qtdeRestante;
			String vendasaldoporminmax = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX);
			Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
			Integer posicaoPvm = 0;
			Boolean emporiumpedidovenda = emporiumpedidovendaService.havePedidovendaEnviado(pedidovenda);
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				if((pedidovendamaterial.getMaterial().getVendaecf() == null || !pedidovendamaterial.getMaterial().getVendaecf() || (emporiumpedidovenda == null || !emporiumpedidovenda)) && (
						(somenteproduto == null && somenteservico == null) || (!somenteproduto && !somenteservico) ||
						(somenteproduto && pedidovendamaterial.getMaterial().getProduto() != null && pedidovendamaterial.getMaterial().getProduto()) ||
						(somenteservico && pedidovendamaterial.getMaterial().getServico() != null && pedidovendamaterial.getMaterial().getServico()))){
					qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
					qtdeRestante = SinedUtil.round(qtdeRestante * percentual / 100D, 10);
					
					vendamaterial = new Vendamaterial();
					
					if(percentual != null && !percentual.equals(100)){
						Double quantidadePercentual = new Money(pedidovendamaterial.getQuantidade()).multiply(new Money(percentual, false).divide(new Money(100d))).getValue().doubleValue();
						if(pedidovendamaterial.getUnidademedida() != null && 
								pedidovendamaterial.getUnidademedida().getCasasdecimaisestoque() != null){
							quantidadePercentual = SinedUtil.round(quantidadePercentual, pedidovendamaterial.getUnidademedida().getCasasdecimaisestoque());
						}
						
						if(qtdeRestante == null || (qtdeRestante != null && quantidadePercentual < qtdeRestante)){
							vendamaterial.setQuantidade(quantidadePercentual.doubleValue());
						} else {
							if(pedidovendamaterial.getUnidademedida() != null 
									&& pedidovendamaterial.getUnidademedida().getCasasdecimaisestoque() != null
									&& pedidovendamaterial.getUnidademedida().getCasasdecimaisestoque() == 0) {
								vendamaterial.setQuantidade(quantidadePercentual);
							} else {
								vendamaterial.setQuantidade(qtdeRestante != null ? qtdeRestante : pedidovendamaterial.getQuantidade());
							}
						}
						
					} else {
						vendamaterial.setQuantidade(qtdeRestante != null ? qtdeRestante : pedidovendamaterial.getQuantidade());
					}
					vendamaterial.setQuantidadeTotal(pedidovendamaterial.getQuantidade());
					vendamaterial.setPesomedio(pedidovendamaterial.getPesomedio());
					vendamaterial.setValorSeguro(pedidovendamaterial.getValorSeguro());
					
					
					if(vendamaterial.getQuantidade() != null && vendamaterial.getQuantidade() > 0){
						vendamaterial = preencheVendamaterial(pedidovenda, posicaoPvm, vendamaterial, vendasaldoporminmax, desconsiderardescontosaldoproduto, null, percentual);
						vendamaterial.setOrdem(pedidovendamaterial.getOrdem());
						list.add(vendamaterial);
					} else {
						pedidovenda.setGerarnovasparcelas(Boolean.TRUE);
					}
				}
				posicaoPvm++;
			}
		}
		return list;
	}
	
	/**
	 * 
	 * @param pedidovenda
	 * @param posicaoPvm
	 * @param vendamaterial
	 * @param vendasaldoporminmax
	 * @param desconsiderardescontosaldoproduto
	 * @return
	 * @author Rafael Salvio
	 */
	protected Vendamaterial preencheVendamaterial(Pedidovenda pedidovenda, Integer posicaoPvm, Vendamaterial vendamaterial, String vendasaldoporminmax, Boolean desconsiderardescontosaldoproduto, ConfirmarPedidovendaItemBean item, Integer percentual){
		Pedidovendamaterial pedidovendamaterial = pedidovenda.getListaPedidovendamaterial().get(posicaoPvm);
		Material material = pedidovendamaterial.getMaterial().copiaMaterial();					
		Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
		
		vendamaterial.setOrdem(pedidovendamaterial.getOrdem());
		vendamaterial.setMaterial(material);
		vendamaterial.setIdentificadorespecifico(pedidovendamaterial.getIdentificadorespecifico());
		vendamaterial.setIdentificadorintegracao(pedidovendamaterial.getIdentificadorintegracao());
		vendamaterial.setIdentificadorinterno(pedidovendamaterial.getIdentificadorinterno());
		vendamaterial.setMaterialmestre(pedidovendamaterial.getMaterialmestre());
		vendamaterial.setPreco(pedidovendamaterial.getPreco());
		vendamaterial.setMultiplicador(pedidovendamaterial.getMultiplicador() != null ? pedidovendamaterial.getMultiplicador() : 1d);
		vendamaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
		vendamaterial.setTotal(pedidovendamaterial.getTotal());
		vendamaterial.setPedidovendamaterial(pedidovendamaterial);
		vendamaterial.setLoteestoque(pedidovendamaterial.getLoteestoque()); 
		vendamaterial.setFornecedor(pedidovendamaterial.getFornecedor());
		vendamaterial.setObservacao(pedidovendamaterial.getObservacao());
		vendamaterial.setFatorconversao(pedidovendamaterial.getFatorconversao());
		vendamaterial.setQtdereferencia(pedidovendamaterial.getQtdereferencia());

		Double valorcustoproducao = null;
		boolean recapagem = SinedUtil.isRecapagem();
		
		if(vendamaterial.getMaterial().getProducao() != null && vendamaterial.getMaterial().getProducao() && recapagem){
			material = materialService.loadMaterialComMaterialproducao(vendamaterial.getMaterial());
			
			if(material != null){
				try{
					material.setProduto_altura(pedidovenda.getAlturas());
					material.setProduto_largura(pedidovenda.getLarguras());
					material.setQuantidade(pedidovenda.getQuantidades());
					
					valorcustoproducao = materialproducaoService.getValorvendaproducao(material);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (valorcustoproducao != null) {
			vendamaterial.setValorcustomaterial(valorcustoproducao);
		} else {
			vendamaterial.setValorcustomaterial(material.getValorcusto() != null ? material.getValorcusto() : pedidovendamaterial.getValorcustomaterial());
		}
		
		ImpostoUtil.copyDadosImposto(pedidovendamaterial, vendamaterial);
		
		vendamaterial.setValorvendamaterial(pedidovendamaterial.getValorvendamaterial());
		vendamaterial.setQtdereferencia(pedidovendamaterial.getQtdereferencia() != null ? pedidovendamaterial.getQtdereferencia() : 1.0);
		vendamaterial.setAltura(pedidovendamaterial.getAltura());
		vendamaterial.setLargura(pedidovendamaterial.getLargura());
		vendamaterial.setComprimento(pedidovendamaterial.getComprimento());
		vendamaterial.setComprimentooriginal(pedidovendamaterial.getComprimentooriginal());
		vendamaterial.setMaterialcoleta(pedidovendamaterial.getMaterialcoleta());
		vendamaterial.setPercentualrepresentacao(pedidovendamaterial.getPercentualrepresentacao());
		vendamaterial.setCustooperacional(pedidovendamaterial.getCustooperacional());
		vendamaterial.setPercentualcomissaoagencia(pedidovendamaterial.getPercentualcomissaoagencia());
		vendamaterial.setPneu(pedidovendamaterial.getPneu());
		vendamaterial.setAliquotareaisipi(pedidovendamaterial.getAliquotareaisipi());
		vendamaterial.setTipocalculoipi(pedidovendamaterial.getTipocalculoipi());
		vendamaterial.setComissionamento(pedidovendamaterial.getComissionamento());
		vendamaterial.setPeso(pedidovendamaterial.getPesoVendaOuMaterial());
		if(pedidovendamaterial.getServicoalterado() != null && pedidovendamaterial.getServicoalterado()){
			vendamaterial.setServicoalterado(pedidovendamaterial.getServicoalterado());
		}
		if(pedidovendamaterial.getBandaalterada() != null && pedidovendamaterial.getBandaalterada() && 
				materialService.existeFormulaValorVendaComPrecoBanda(material)){
			vendamaterial.setBandaalterada(pedidovendamaterial.getBandaalterada());
		}
		
		if(item != null){
			if(item.getLote() != null && !item.getLote().trim().isEmpty()){
				vendamaterial.setLoteestoque(loteestoqueService.findByNumeroAndFornecedor(item.getLote(), pedidovendamaterial.getFornecedor()));
			}
			vendamaterial.setQtdevolumeswms(item.getQuantidade_volumes());
			vendamaterial.setQuantidade(getQtdeConvertida(vendamaterial, item.getQuantidade()));
		} else if(percentual != null && percentual > 0){
			vendamaterial.setDesconto(pedidovendamaterial.getDesconto().multiply(new Money(percentual, false).divide(new Money(100d))));
		} else{
			vendamaterial.setDesconto(pedidovendamaterial.getDesconto());
		}
		if((pedidovendamaterial.getPercentualdesconto() != null || item != null) && vendamaterial.getQuantidadeTotal() != null && 
				vendamaterial.getQuantidadeTotal() > 0){
			Money perc = new Money(vendamaterial.getQuantidade(), false).divide(new Money(vendamaterial.getQuantidadeTotal(), false));
			vendamaterial.setDesconto(pedidovendamaterial.getDesconto().multiply(perc));
		}

		vendamaterial.setPercentualdesconto(pedidovendamaterial.getPercentualdesconto());
		java.sql.Date dtprazoentrega = pedidovendamaterial.getDtprazoentrega();
		java.sql.Date currentDate = SinedDateUtils.currentDate();
		if(dtprazoentrega != null && SinedDateUtils.beforeIgnoreHour(dtprazoentrega, currentDate)){
			dtprazoentrega = currentDate;
		}
		vendamaterial.setPrazoentrega(dtprazoentrega);
		
		if(material != null && material.getCdmaterial() != null){
			vendamaterial.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(material));
		}
		if(pedidovendamaterial.getMaterialcoleta() != null && pedidovendamaterial.getMaterialcoleta().getCdmaterial() != null){
			vendamaterial.setExistematerialsimilarColeta(materialsimilarService.existMaterialsimilar(pedidovendamaterial.getMaterialcoleta()));
		}
		if(vendamaterial.getMaterial() != null){
			vendamaterial.setIsMaterialmestregrade(materialService.existMaterialitemByMaterialgrademestre(vendamaterial.getMaterial()));
			if(vendamaterial.getIsMaterialmestregrade() == null || !vendamaterial.getIsMaterialmestregrade()) 
				vendamaterial.setIsControlegrade(materialService.isControleGrade(vendamaterial.getMaterial()));
		}
		
		Double preco = vendamaterial.getPreco();
		Double precoComDesconto = preco;
		if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0)
			precoComDesconto = precoComDesconto - vendamaterial.getDesconto().getValue().doubleValue();
		Double minimo = 0.0;
		Double maximo = 0.0;
		Double valorMaximoTabela = null;
		Double valorMinimoTabela = null;
		
		if(!vendamaterial.getUnidademedida().equals(unidademedida) && vendamaterial.getFatorconversao() != null && vendamaterial.getFatorconversao() > 0){
			if(material.getValorvendaminimo() != null)
				minimo = material.getValorvendaminimo() / vendamaterial.getFatorconversaoQtdereferencia();
			if(material.getValorvendamaximo() != null)
				maximo = material.getValorvendamaximo() / vendamaterial.getFatorconversaoQtdereferencia();
		}else{
			minimo = material.getValorvendaminimo();
			maximo = material.getValorvendamaximo();
		}
		
		Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(material, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa());
		if(materialtabelapreco == null){
			Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(material, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
			if(materialtabelaprecoitem != null){
				if(vendamaterial.getFatorconversao() != null && vendamaterial.getFatorconversao() > 0){
					if(materialtabelaprecoitem.getValorvendaminimo() != null)
						valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo() / vendamaterial.getFatorconversaoQtdereferencia();
					if(materialtabelaprecoitem.getValorvendamaximo() != null)
						valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo() / vendamaterial.getFatorconversaoQtdereferencia();
				}else {
					valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
					valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
				}
			}
		}
		
		if(valorMinimoTabela != null){
			minimo = valorMinimoTabela;
		}
		if(valorMaximoTabela != null){
			maximo = valorMaximoTabela;
		}
		material.setValorvendaminimo(minimo);
		material.setValorvendamaximo(maximo);
		
		if("TRUE".equalsIgnoreCase(vendasaldoporminmax)){
			vendaService.calculaSaldoMaterial(vendamaterial, 
					vendamaterial.getPreco(), 
					vendamaterial.getMaterial().getValorvendaminimo(), 
					vendamaterial.getMaterial().getValorvendamaximo(), 
					vendamaterial.getQuantidade(), 
					vendamaterial.getDesconto(), 
					desconsiderardescontosaldoproduto,
					vendamaterial.getMultiplicador());
		}else {
			vendaService.calculaSaldoMaterialPorValorvenda(vendamaterial, 
					vendamaterial.getPreco(), 
					vendamaterial.getMaterial().getValorvenda(), 
					vendamaterial.getQuantidade(), 
					vendamaterial.getDesconto(),
					desconsiderardescontosaldoproduto,
					vendamaterial.getMultiplicador());
		}
		
		return vendamaterial;
	}
	
	/**
	 * Preenche a lista de Vendamaterial a partir de um pedido de venda e dos itens recebidos na sincronizacao.
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Vendamaterial> preencheListaVendaMaterial(Pedidovenda pedidovenda, List<ConfirmarPedidovendaItemBean> listaItens) {
		List<Vendamaterial> list = new ArrayList<Vendamaterial>();
		
		if(pedidovenda.getListaPedidovendamaterial() != null){
			Vendamaterial vendamaterial;
			Double qtdeRestante;
			String vendasaldoporminmax = parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX);
			Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
		
			int posicaoPvm = 0;
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null 
						|| pedidovendamaterial.getMaterial() == null || pedidovendamaterial.getMaterial().getCdmaterial() == null){
					continue;
				}
				
				
				for (Iterator<ConfirmarPedidovendaItemBean> iterator = listaItens.iterator(); iterator.hasNext();){
					ConfirmarPedidovendaItemBean item = (ConfirmarPedidovendaItemBean) iterator.next();
					if(pedidovendamaterial.getCdpedidovendamaterial().equals(item.getId())){
						vendamaterial = new Vendamaterial();
						vendamaterial.setQuantidadeTotal(pedidovendamaterial.getQuantidade());
						qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);				
						vendamaterial.setQuantidade(qtdeRestante != null ? qtdeRestante : pedidovendamaterial.getQuantidade());
						
						//Caso o material separado seja diferente do vendido deve ser registrada venda com o que de fato foi carregado.
						if(item.getId_material() != null && item.getId_material() > 0 && !pedidovendamaterial.getMaterial().getCdmaterial().equals(item.getId_material())){	
							pedidovendamaterial.setMaterialmestre(pedidovendamaterial.getMaterial());
							pedidovendamaterial.setMaterial(materialService.loadByCodigoMaterial(item.getId_material()));
						} 
						
						vendamaterial = preencheVendamaterial(pedidovenda, posicaoPvm, vendamaterial, vendasaldoporminmax, desconsiderardescontosaldoproduto, item, null);
						
						list.add(vendamaterial);
						iterator.remove();
					}
				}
				posicaoPvm++;
			}
		}
		
		return list;
	}
		
	public List<Vendavalorcampoextra> preecheVendavalorcampoextra(List<Pedidovendavalorcampoextra> listaPedidovalorcampoextra) {
		List<Vendavalorcampoextra> lista = new ArrayList<Vendavalorcampoextra>();
		Vendavalorcampoextra vendavalorcampoextra;
		if(Hibernate.isInitialized(listaPedidovalorcampoextra) && SinedUtil.isListNotEmpty(listaPedidovalorcampoextra)){
			for (Pedidovendavalorcampoextra bean : listaPedidovalorcampoextra) {
				vendavalorcampoextra = new Vendavalorcampoextra();
				vendavalorcampoextra.setCampoextrapedidovendatipo(bean.getCampoextrapedidovendatipo());
				vendavalorcampoextra.setValor(bean.getValor());
				lista.add(vendavalorcampoextra);
			}
		}
		
		return lista;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#havePedidovendaSituacao(String whereIn, Pedidovendasituacao[] situacoes)
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public boolean havePedidovendaSituacao(String whereIn, Pedidovendasituacao... situacoes) {
		return pedidovendaDAO.havePedidovendaSituacao(whereIn, situacoes);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#haveMaterialproducao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 03/02/2014
	 */
	public boolean haveMaterialproducao(String whereIn) {
		return pedidovendaDAO.haveMaterialproducao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#havePedidovendaDiferenteSituacao(String whereIn, Pedidovendasituacao[] situacoes)
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public boolean havePedidovendaDiferenteSituacao(String whereIn, Pedidovendasituacao... situacoes) {
		return pedidovendaDAO.havePedidovendaDiferenteSituacao(whereIn, situacoes);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#havePedidovendaDiferenteSituacao(String whereIn, Pedidovendasituacao[] situacoes)
	 *
	 * @param whereIn
	 * @return
	 * @since 10/05/2012
	 * @author Marden Silva
	 */
	public boolean havePedidovendaDiferenteReserva(String whereIn) {
		return pedidovendaDAO.havePedidovendaDiferenteReserva(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#havePedidovendaIntegracaoWms(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 04/03/2015
	* @author Luiz Fernando
	*/
	public boolean havePedidovendaIntegracaoWms(String whereIn) {
		return pedidovendaDAO.havePedidovendaIntegracaoWms(whereIn);
	}
	
	/** M�todo com refer�ncia no DAO
	*
	*
	* @param whereIn
	* @return
	* @author Rafael Salvio
	*/
	public boolean havePedidovendaIntegracaoManualPendenteWms(String whereIn) {
		return pedidovendaDAO.havePedidovendaIntegracaoManualPendenteWms(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#havePedidovendaDiferenteBonificacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean havePedidovendaDiferenteBonificacao(String whereIn) {
		return pedidovendaDAO.havePedidovendaDiferenteBonificacao(whereIn);
	}
	
	/**
	 * M�todo que faz a integra��o com o Web Service do WMS
	 * 
	 * @author Thiago Augusto
	 * @param pedidoVenda
	 */
	public void integracaoWMS(Pedidovenda pedidoVenda, String observacao, Boolean itemexcluido){
		
		Empresa empresa;
		if (pedidoVenda != null && pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getCdpessoa() != null)
			empresa = empresaService.carregaEmpresa(pedidoVenda.getEmpresa());
		else 
			empresa = empresaService.loadPrincipal();
		pedidoVenda.setEmpresa(empresa);

		Pedidovendasincronizacao pedidovendasincronizacao = null;
		
		pedidovendasincronizacao = pedidovendasincronizacaoService.findByPedidovenda(pedidoVenda, null);
		boolean reenvio = false;
		if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
			reenvio = pedidovendasincronizacao.getReenvio() != null ? pedidovendasincronizacao.getReenvio() : false;
			if(itemexcluido) reenvio = true;
			Pedidovendasincronizacao pvsnovo =  pedidovendasincronizacaoService.criarPedidovendasincronizacao(pedidoVenda, reenvio);
			pvsnovo.setCdpedidovendasincronizacao(pedidovendasincronizacao.getCdpedidovendasincronizacao());
			pedidovendasincronizacaoService.saveOrUpdate(pvsnovo);
			if(reenvio){
				pedidovendasincronizacaohistoricoService.criarSalvarHistoricoSincronizacao(pedidovendasincronizacao, observacao +  (reenvio ? " - Reenvio" : "") );
			}
			pedidovendasincronizacao = pvsnovo;
		}
		
		if(pedidovendasincronizacao == null){
			pedidovendasincronizacao = pedidovendasincronizacaoService.criarPedidovendasincronizacao(pedidoVenda, reenvio);
		}
		
		if(pedidovendasincronizacao != null){
			if(pedidovendasincronizacao.getPedidovenda() != null && pedidovendasincronizacao.getPedidovenda().getPedidovendasituacao() != null){
				if(pedidovendasincronizacao.getPedidovenda().getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO)){
					if(pedidovendasincronizacao.getCdpedidovendasincronizacao() == null){
						Pedidovendasincronizacao pedidovendasincronizacaoaux = pedidovendasincronizacaoService.findUpdatePedidovenda(pedidoVenda);
						if(pedidovendasincronizacaoaux != null && pedidovendasincronizacaoaux.getCdpedidovendasincronizacao() != null){
							pedidovendasincronizacao.setCdpedidovendasincronizacao(pedidovendasincronizacaoaux.getCdpedidovendasincronizacao());
						}
						pedidovendasincronizacaoService.saveOrUpdate(pedidovendasincronizacao);
						pedidovendasincronizacaohistoricoService.criarSalvarHistoricoSincronizacao(pedidovendasincronizacao, observacao + " - Pedido de venda Aguardando aprova��o " + (reenvio ? " - reenvio" : ""));
					}
					return ;
				}
			}
			
			if(reenvio){
				pedidovendasincronizacao.setSincronizado(Boolean.FALSE);
				if(pedidovendasincronizacao.getListaPedidovendamaterialsincronizacao() != null && !pedidovendasincronizacao.getListaPedidovendamaterialsincronizacao().isEmpty()){
					for(Pedidovendamaterialsincronizacao pvms : pedidovendasincronizacao.getListaPedidovendamaterialsincronizacao()){
						pvms.setPedidovendaprodutoFlagReenvio("V");
					}
				}				
			}
			pedidovendasincronizacao.setReenvio(reenvio);
			pedidovendasincronizacaoService.saveOrUpdate(pedidovendasincronizacao);
		}
		
	}
			
	
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findByItens(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovenda> findByItens(String whereIn) {
		return pedidovendaDAO.findByItens(whereIn);
	}
	
	public void confirmarViaWebservice(Pedidovenda pedidovenda, List<ConfirmarPedidovendaItemBean> listaItens, Integer idcarregamento) {		
		Venda venda = new Venda();
		
		venda.setDtvenda(new Date(System.currentTimeMillis()));
		venda.setColaborador(pedidovenda.getColaborador());
		venda.setEmpresa(pedidovenda.getEmpresa());
		venda.setPresencacompradornfe(pedidovenda.getPresencacompradornfe());
		venda.setIdentificadorcarregamento(idcarregamento);
		venda.setIndicecorrecao(pedidovenda.getIndicecorrecao());
		venda.setContaboleto(pedidovenda.getConta());
		venda.setDocumentotipo(pedidovenda.getDocumentotipo());
		venda.setPrazopagamento(pedidovenda.getPrazopagamento());
		venda.setCliente(pedidovenda.getCliente());
		venda.setEndereco(pedidovenda.getEndereco());
		venda.setObservacao(pedidovenda.getObservacao());
		venda.setValorfrete(pedidovenda.getValorfrete());
		venda.setListavendamaterial(this.preencheListaVendaMaterial(pedidovenda, listaItens));
		venda.setListavendapagamento(this.preencheListaVendaPagamento(pedidovenda));
		venda.setPedidovenda(pedidovenda);
		venda.setVendasituacao(Vendasituacao.REALIZADA);
		venda.setFrete(pedidovenda.getFrete());
		venda.setTerceiro(pedidovenda.getTerceiro());
		venda.setProjeto(pedidovenda.getProjeto());
		venda.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
		venda.setPedidovendatipo(pedidovenda.getPedidovendatipo());
		
		if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){
			venda.setValorusadovalecompra(pedidovenda.getValorusadovalecompra());
			Money valorValecompraJaUsado = vendaService.getValorValeCompraJaUsado(pedidovenda);
			if(valorValecompraJaUsado != null && valorValecompraJaUsado.getValue().doubleValue() > 0){
				venda.setValorusadovalecompra(venda.getValorusadovalecompra().subtract(valorValecompraJaUsado));
			}
			if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){
				Money totalvendaSemValecompra = venda.getTotalvendaSemValecompra();
				if(totalvendaSemValecompra != null && totalvendaSemValecompra.getValue().doubleValue() < 
						venda.getValorusadovalecompra().getValue().doubleValue()){
					venda.setValorusadovalecompra(totalvendaSemValecompra);
				}
			}
		}
		
		boolean naoGerarReceita = false;
		if(pedidovenda.getPedidovendatipo() != null && 
				pedidovenda.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
				!GeracaocontareceberEnum.APOS_VENDAREALIZADA.equals(pedidovenda.getPedidovendatipo().getGeracaocontareceberEnum())){
			naoGerarReceita = true;
		}
		
		venda.setDesconto(calculaDescontoPercentual(venda, pedidovenda));
		
		if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
			vendaService.calcularImposto(venda);
			vendaService.saveOrUpdate(venda);
			
			Vendahistorico vendahistorico = new Vendahistorico();
			vendahistorico.setVenda(venda);
			vendahistorico.setAcao("Cria��o venda");
			vendahistorico.setObservacao("Cria��o a partir do pedido de venda: <a href=\"javascript:visualizarPedidovenda(" + 
					venda.getPedidovenda().getCdpedidovenda() + 
					");\">" + 
					venda.getPedidovenda().getCdpedidovenda() + 
					"</a>");
			
			vendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
			vendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			
			vendahistoricoService.saveOrUpdate(vendahistorico);
			
			boolean registrarValecompra = true;
			String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
			if(!"VENDA".equalsIgnoreCase(param_utilizacao_valecompra)){
				registrarValecompra = false;
			}
			
			if(registrarValecompra){
			// REGISTRO DO USO DE VALE COMPRA
				if(venda.getValorusadovalecompra() != null && venda.getValorusadovalecompra().getValue().doubleValue() > 0){
					Valecompraorigem valecompraorigem = new Valecompraorigem();
					valecompraorigem.setVenda(venda);
				
					Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
					listaValecompraorigem.add(valecompraorigem);
				
					Valecompra valecompra = new Valecompra();
					valecompra.setCliente(venda.getCliente());
					valecompra.setData(SinedDateUtils.castUtilDateForSqlDate(venda.getDtvenda()));
					valecompra.setIdentificacao("Uso do vale compra na venda " + venda.getCdvenda());
					valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
					valecompra.setValor(venda.getValorusadovalecompra());
					valecompra.setListaValecompraorigem(listaValecompraorigem);
				
					valecompraService.saveOrUpdate(valecompra);			
				}
			}
			
			boolean confirmado_total = true;
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
				if(qtdeRestante == null || qtdeRestante > 0){
					confirmado_total = false;
					break;
				}
			}
			
			Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
			pedidovendahistorico.setPedidovenda(venda.getPedidovenda());
			pedidovendahistorico.setObservacao("Venda criada: <a href=\"javascript:visualizarVenda(" + 
					venda.getCdvenda() + 
					");\">" + 
					venda.getCdvenda() + 
					"</a>");
			
			if(confirmado_total){
				pedidovendahistorico.setAcao("Confirmado");
				this.updateConfirmacao(venda.getPedidovenda().getCdpedidovenda().toString());
			} else {
				pedidovendahistorico.setAcao("Confirmado parcialmente");
				this.updateConfirmacaoParcial(venda.getPedidovenda().getCdpedidovenda().toString());
			}
			
			pedidovendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
			pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				vendamaterial.setTotal(vendaService.getValortotal(vendamaterial.getPreco(), 
																	vendamaterial.getQuantidade(),
																	vendamaterial.getDesconto(), vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
			}
			
			Boolean ajusteEstoque = venda.getPedidovendatipo() == null;
			if(venda.getPedidovendatipo() != null && (BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) || 
					BaixaestoqueEnum.APOS_VENDAREALIZADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()))){
				ajusteEstoque = true;
			}
			List<Rateioitem> listarateioitem = vendaService.prepareAndSaveVendaMaterial(venda, ajusteEstoque, venda.getEmpresa(), false, null);
			venda.setConfirmacaowms(Boolean.TRUE);
			vendaService.prepareAndSaveReceitaWms(venda, listarateioitem, venda.getEmpresa(),naoGerarReceita);
		}
	}
	
	public void registrarUsoValecompra(Pedidovenda venda) {
		// REGISTRO DO USO DE VALE COMPRA
		if(venda.getValorusadovalecompra() != null && venda.getValorusadovalecompra().getValue().doubleValue() > 0){
			Valecompraorigem valecompraorigem = new Valecompraorigem();
			valecompraorigem.setPedidovenda(venda);
			
			Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
			listaValecompraorigem.add(valecompraorigem);
			
			Valecompra valecompra = new Valecompra();
			valecompra.setCliente(venda.getCliente());
			valecompra.setData(SinedDateUtils.castUtilDateForSqlDate(venda.getDtpedidovenda()));
			valecompra.setIdentificacao("Uso do vale compra no pedido de venda " + venda.getCdpedidovenda());
			valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
			valecompra.setValor(venda.getValorusadovalecompra());
			valecompra.setListaValecompraorigem(listaValecompraorigem);
			
			valecompraService.saveOrUpdate(valecompra);			
		}
	}
	
	public void registrarCreditoValecompra(Pedidovenda pedidovenda) {
		if(pedidovenda.getValorvalecompranovo() != null && pedidovenda.getValorvalecompranovo().getValue().doubleValue() > 0){
			Valecompraorigem valecompraorigem = new Valecompraorigem();
			valecompraorigem.setPedidovenda(pedidovenda);
			
			Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
			listaValecompraorigem.add(valecompraorigem);
			
			Valecompra valecompra = new Valecompra();
			valecompra.setCliente(pedidovenda.getCliente());
			valecompra.setData(SinedDateUtils.castUtilDateForSqlDate(pedidovenda.getDtpedidovenda()));
			valecompra.setIdentificacao("Cr�dto no pedido de venda " + pedidovenda.getCdpedidovenda());
			valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			valecompra.setValor(pedidovenda.getValorvalecompranovo());
			valecompra.setListaValecompraorigem(listaValecompraorigem);
			
			valecompraService.saveOrUpdate(valecompra);			
		}
	}
	
	protected Double getQtdeConvertida(Vendamaterial vendamaterial, Double qtdeWms) {
		Double quantidade = qtdeWms;
		if(vendamaterial != null && qtdeWms != null && vendamaterial.getMaterial() != null && 
				vendamaterial.getMaterial().getCdmaterial() != null && 
				vendamaterial.getMaterial().getUnidademedida() != null && 
				vendamaterial.getMaterial().getUnidademedida().getCdunidademedida() != null){
			if(vendamaterial.getUnidademedida() != null && vendamaterial.getUnidademedida().getCdunidademedida() != null && 
					!vendamaterial.getUnidademedida().getCdunidademedida().equals(vendamaterial.getMaterial().getUnidademedida().getCdunidademedida())){
				Double fatorConversao = unidademedidaService.getFatorconversao(vendamaterial.getMaterial().getUnidademedida(), 
																			qtdeWms, 
																			vendamaterial.getUnidademedida(), 
																			vendamaterial.getMaterial(), 
																			vendamaterial.getFatorconversao(), 
																			vendamaterial.getQtdereferencia());
				
				if(fatorConversao != null && fatorConversao > 0){
					quantidade = qtdeWms * fatorConversao;
				}
			}
		}
		return (quantidade != null ? quantidade : 1d);
	}
	
	/**
	 * Preenche a lista de Vendapagamento a partir de um Pedido de venda.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PedidovendapagamentoService#findByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @since 16/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Vendapagamento> preencheListaVendaPagamento(Pedidovenda pedidovenda) {
		List<Pedidovendapagamento> listaPedidovendapagamento = pedidovendapagamentoService.findByPedidovenda(pedidovenda);
		return preencheListaVendaPagamento(listaPedidovendapagamento);
	}
	
	/**
	 * Preenche a lista de Vendapagamento a partir de uma lista de PedidoVendaPagamento
	 * 
	 * @param listaPedidovendapagamento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/03/2017
	 */
	public List<Vendapagamento> preencheListaVendaPagamento(List<Pedidovendapagamento> listaPedidovendapagamento) {
		if(listaPedidovendapagamento != null && listaPedidovendapagamento.size() > 0){
			Vendapagamento vendapagamento;
			List<Vendapagamento> listaVendapagamento = new ArrayList<Vendapagamento>();
			for (Pedidovendapagamento pedidovendapagamento : listaPedidovendapagamento) {
				vendapagamento = new Vendapagamento();
				
				vendapagamento.setAgencia(pedidovendapagamento.getAgencia());
				vendapagamento.setBanco(pedidovendapagamento.getBanco());
				vendapagamento.setConta(pedidovendapagamento.getConta());
				vendapagamento.setDataparcela(pedidovendapagamento.getDataparcela());
				vendapagamento.setDocumentotipo(pedidovendapagamento.getDocumentotipo());
				vendapagamento.setDocumento(pedidovendapagamento.getDocumento());
				vendapagamento.setNumero(pedidovendapagamento.getNumero());
				vendapagamento.setValororiginal(pedidovendapagamento.getValororiginal());
				vendapagamento.setDocumentoantecipacao(pedidovendapagamento.getDocumentoantecipacao());
				vendapagamento.setValorjuros(pedidovendapagamento.getValorjuros());
				vendapagamento.setCheque(pedidovendapagamento.getCheque());
				
				listaVendapagamento.add(vendapagamento);
			}
			return listaVendapagamento;
		} else return new ArrayList<Vendapagamento>();
	}
	
	/**
	* M�todo que preenche a lista de pagamento de representa��o
	*
	* @param pedidovenda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Vendapagamentorepresentacao> preencheListaVendaPagamentoRepresentacao(Pedidovenda pedidovenda) {
		List<Pedidovendapagamentorepresentacao> listaPedidovendapagamentorepresentacao = pedidovendapagamentorepresentacaoService.findPedidovendaPagamentoRepresentacaoByPedidovenda(pedidovenda);
		if(listaPedidovendapagamentorepresentacao != null && listaPedidovendapagamentorepresentacao.size() > 0){
			Vendapagamentorepresentacao vendapagamentorepresentacao;
			List<Vendapagamentorepresentacao> listaVendapagamentorepresentacao = new ArrayList<Vendapagamentorepresentacao>();
			for (Pedidovendapagamentorepresentacao pedidovendapagamentorepresentacao : listaPedidovendapagamentorepresentacao) {
				vendapagamentorepresentacao = new Vendapagamentorepresentacao();
				
				vendapagamentorepresentacao.setDataparcela(pedidovendapagamentorepresentacao.getDataparcela());
				vendapagamentorepresentacao.setDocumentotipo(pedidovendapagamentorepresentacao.getDocumentotipo());
				vendapagamentorepresentacao.setDocumento(pedidovendapagamentorepresentacao.getDocumento());
				vendapagamentorepresentacao.setValororiginal(pedidovendapagamentorepresentacao.getValororiginal());
				vendapagamentorepresentacao.setFornecedor(pedidovendapagamentorepresentacao.getFornecedor());
				listaVendapagamentorepresentacao.add(vendapagamentorepresentacao);
			}
			return listaVendapagamentorepresentacao;
		} else return new ArrayList<Vendapagamentorepresentacao>();
	}
	
	/**
	 * M�todo que salva a lista de materiais da venda e prepara o rateio
	 * 
	 * @param venda
	 * @param ajusteEstoque
	 * @param empresa
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Rateioitem> prepareAndSaveVendaMaterial(Pedidovenda pedidovenda, Boolean ajusteEstoque, Empresa empresa, Boolean notSalvar, NotaTipo notaTipo) {
		
		empresa = empresaService.loadComContagerencialCentrocusto(pedidovenda.getEmpresa());
		List<Rateioitem> listarateioitem = new ArrayList<Rateioitem>();
		Double valorVenda = 0.0;
		for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
			if(pedidovendamaterial.getTotal() == null){
				pedidovendamaterial.setTotal(vendaService.getValortotal(pedidovendamaterial.getPreco(), 
						pedidovendamaterial.getQuantidade(),
						pedidovendamaterial.getDesconto(), 
						pedidovendamaterial.getMultiplicador(),
						pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro()));
			}
			valorVenda += pedidovendamaterial.getTotal().getValue().doubleValue();
		}
		valorVenda = SinedUtil.round(valorVenda, 2);
		
		for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
			pedidovendamaterial.setMaterial(materialService.getPrecoCustoProduto(pedidovendamaterial.getMaterial()));
						
			Rateioitem rateioitem = new Rateioitem();
						
			Contagerencial contagerencial = empresa.getContagerencial();
			if(pedidovendamaterial.getMaterial().getContagerencialvenda() != null && pedidovendamaterial.getMaterial().getContagerencialvenda().getCdcontagerencial() != null){
				contagerencial = pedidovendamaterial.getMaterial().getContagerencialvenda();
			}
			rateioitem.setContagerencial(contagerencial);

			Centrocusto centrocusto = null;
			if(pedidovenda.getCentrocusto() != null){
				centrocusto = pedidovenda.getCentrocusto(); 
			}else{
				centrocusto = empresa.getCentrocusto();
				Material material = materialService.load(pedidovendamaterial.getMaterial(), "material.centrocustovenda");
				if(material != null && material.getCentrocustovenda() != null && material.getCentrocustovenda().getCdcentrocusto() != null){
					centrocusto = material.getCentrocustovenda();
				}				
			}
			
			rateioitem.setCentrocusto(centrocusto);
			if(pedidovenda.getProjeto() != null && pedidovenda.getProjeto().getCdprojeto() != null)
				rateioitem.setProjeto(pedidovenda.getProjeto());
			
			rateioitem.setValor(pedidovendamaterial.getTotal());
			rateioitem.setPercentual(pedidovendamaterial.getTotal().getValue().doubleValue() / valorVenda * 100);
			listarateioitem.add(rateioitem);
			
			pedidovendamaterial.setPedidovenda(pedidovenda);
			
			if(notSalvar == null || !notSalvar)
				pedidovendamaterialService.saveOrUpdate(pedidovendamaterial);
		}
		return listarateioitem;
	}
	

		
	
	public List<Integer> prepareAndSaveReceita(Pedidovenda pedidovenda, List<Rateioitem> listarateioitem, Empresa empresa) {
		return prepareAndSaveReceita(pedidovenda, listarateioitem, empresa, null, false, null);
	}

	/**
	 * M�todo que salva as vendas pagamento e gera Conta a receber
	 * 
	 * @param venda
	 * @param listarateioitem
	 * @param empresa
	 * @author Ramon Brazil
	 */
	public List<Integer> prepareAndSaveReceita(Pedidovenda pedidovenda, List<Rateioitem> listarateioitem, Empresa empresa, List<Documento> listaContareceber, boolean origemContareceberAposEmissaoNota, Conta contaboletoNF) {
		List<Integer> listaDocumentosGerados = new ArrayList<Integer>();
		Documento documento;
		Documentohistorico documentohistorico;
		Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		Parcelamento parcelamento = new Parcelamento();
		
		rateioitemService.agruparItensRateio(listarateioitem);
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listarateioitem);
		rateioService.saveOrUpdate(rateio);
		
		Integer iteracoes = getQtdeIteracoesPgamento(pedidovenda);
		if(pedidovenda.getPrazopagamento() != null){
			parcelamento.setPrazopagamento(pedidovenda.getPrazopagamento());
			parcelamento.setIteracoes(iteracoes);
			
			parcelamentoService.saveOrUpdate(parcelamento);
		}
		Parcela parcela;
		int i = 1;
		
		String msg = "";
		StringBuilder msg2 = new StringBuilder();
		StringBuilder msg3 = new StringBuilder();
		String preposicao = "";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				
		Rateio rateioParcelas;
		Documentotipo documentotipo;
		Taxa taxa;
		Taxaitem taxaitem;
		Set<Taxaitem> listaTaxaitem;
		int numParcela = 1;
		Documentoacao documentoacao = pedidovendatipoService.getSituacaoDocumento(pedidovenda.getPedidovendatipo());
		
		if (empresa!=null && empresa.getCdpessoa()!=null){
			empresa = empresaService.loadForEntrada(empresa);
		}
		
		for (Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()) {
			msg = "";
			msg2 = new StringBuilder();
			msg3 = new StringBuilder();
			preposicao = "";
			
			taxa = new Taxa();
			listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
			
			Conta contaboleto = null;
			documentotipo = documentotipoService.load(pedidovendapagamento.getDocumentotipo());
			
			if (documentotipo.getBoleto() != null && documentotipo.getBoleto()) {
				contaboleto = pedidovenda.getConta();
			} else {
				contaboletoNF = null;
			}
			
			if(pedidovendapagamento.getContadestino() != null){
				contaboleto = pedidovendapagamento.getContadestino();
			}
			Endereco enderecoPessoa = null;
			if(empresa != null && empresa.getCdpessoa() != null){
				enderecoPessoa = enderecoService.getEnderecoPrincipalPessoa(empresa);
			}
			
			pedidovendapagamento.setDocumentotipo(documentotipo);
			if(documentotipo.getContadestino() != null){
				Conta contadestino = contaService.loadConta(documentotipo.getContadestino());
				if(contadestino != null && 
						contadestino.getBaixaautomaticafaturamento() != null && 
						contadestino.getBaixaautomaticafaturamento()){
					contaboleto = contadestino;
				}
			}
			
			if (contaboletoNF!=null && contaboletoNF.getCdconta()!=null){
				contaboleto = contaboletoNF;
			}
			
			if (contaboleto == null && Boolean.TRUE.equals(documentotipo.getBoleto()) && empresa!=null && empresa.getContabancariacontareceber()!=null && empresa.getContabancariacontareceber().getCdconta()!=null){
				contaboleto = empresa.getContabancariacontareceber();
			}
			
			if(contaboleto != null && contaboleto.getCdconta() != null){
				listaTaxaitem = contacorrenteService.getTaxas(contaboleto, pedidovendapagamento.getDataparcela(), enderecoPessoa);
			}
			
			//Se estiver associado a um documento de antecipa��o, n�o gera conta a receber
			if (pedidovendapagamento.getDocumentoantecipacao() == null){	
				
				if(pedidovendapagamento.getDocumento() == null){
					documento = new Documento();
		//			Money valorParcela = vendapagamento.getValororiginal();
					Double taxaVenda = documentotipo.getTaxavenda();
				    if (pedidovenda.getDocumentoAposEmissaoNota() != null && pedidovenda.getDocumentoAposEmissaoNota()){
				    	documento.setDocumentoacao(Documentoacao.DEFINITIVA);
				    }else{
				    	documento.setDocumentoacao(Documentoacao.PREVISTA);
				    	if(documentoacao != null){
				    		documento.setDocumentoacao(documentoacao);
				    	}
				    }
				    documento.setConta(contaboleto);
					if(contaboleto != null && contaboleto.getCdconta() != null){
						Conta conta = contaService.carregaContaComMensagem(contaboleto, null);
						if(conta != null && conta.getContacarteira().getCdcontacarteira() != null){
							//Preenche a carteira do documento com a carteira padr�o
							documento.setContacarteira(conta.getContacarteira());
							
							if(conta.getContacarteira().getMsgboleto1() != null && !"".equals(conta.getContacarteira().getMsgboleto1()))
								documento.setMensagem1(conta.getContacarteira().getMsgboleto1());
							if(conta.getContacarteira().getMsgboleto2() != null && !"".equals(conta.getContacarteira().getMsgboleto2()))
								documento.setMensagem2(conta.getContacarteira().getMsgboleto2());
						}
						
						documento.setVinculoProvisionado(contaboleto);
					} else {
						documento.setVinculoProvisionado(documentotipo.getContadestino());
					}
					documento.setIndicecorrecao(pedidovenda.getIndicecorrecao());
					documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					
					if(pedidovenda.getDtpedidovendaEmissaonota() != null){
						documento.setDtemissao(new java.sql.Date(pedidovenda.getDtpedidovendaEmissaonota().getTime()));
					}else {
						documento.setDtemissao(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()));
					}
					
					if(pedidovenda.getDtSaidaNotaFiscalProduto() != null) {
						documento.setDtcompetencia(pedidovenda.getDtSaidaNotaFiscalProduto());
					} else {
						documento.setDtcompetencia(documento.getDtemissao());
					}
					
					String idExterno = "";
					if(pedidovenda.getIdentificacaoexterna() != null && !pedidovenda.getIdentificacaoexterna().equals("")){
						idExterno =  " - Ident. Externo: " + pedidovenda.getIdentificacaoexterna();
					}
					documento.setDescricao("Conta a receber referente ao pedido de venda "+pedidovenda.getCdpedidovenda()+". - " + i + "/" + iteracoes +  idExterno);
					
					pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
					
					documento.setValor(pedidovendapagamento.getValororiginal());
					if(taxaVenda != null && taxaVenda > 0.0 &&
						!Boolean.TRUE.equals(documentotipo.getTaxanoprocessamentoextratocartaocredito())){
						taxaitem = new Taxaitem();
						if(documentotipo.getTipotaxacontareceber() != null && documentotipo.getTipotaxacontareceber().getCdtipotaxa() != null){
							taxaitem.setTipotaxa(documentotipo.getTipotaxacontareceber());	
						}else{
							taxaitem.setTipotaxa(Tipotaxa.TAXAMOVIMENTO);
						}
						taxaitem.setPercentual(documentotipo.getPercentual() != null ? documentotipo.getPercentual() : true);
						taxaitem.setValor(new Money(taxaVenda));
						taxaitem.setDtlimite(documento.getDtemissao());
						listaTaxaitem.add(taxaitem);
					}
					
					if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
						taxa.setListaTaxaitem(listaTaxaitem);
						documento.setTaxa(taxa);
						
						if(documento.getMensagem2() != null)
							msg2.append(documento.getMensagem2());
						if(documento.getMensagem3() != null)
							msg3.append(documento.getMensagem3());
						
						for(Taxaitem txitem : listaTaxaitem){
							if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
								if(tipotaxaService.isApos(txitem.getTipotaxa())) {
									preposicao = " ap�s ";
								}else {
									preposicao = " at� ";
								}
								msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + format.format(txitem.getDtlimite()) : "") + ". ";
								if((msg2.length() + msg.length()) <= 80){
									msg2.append(msg);
								}else if((msg3.length() + msg.length()) <= 80){
									msg3.append(msg);
								}
							}
						}
						if(!msg2.toString().equals("")){
							documento.setMensagem2(msg2.toString());
						}
						if(!msg3.toString().equals("")){
							documento.setMensagem3(msg3.toString());
						}
					}
					
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					documento.setPessoa(pedidovenda.getCliente());
					documento.setReferencia(Referencia.MES_ANO_CORRENTE);
					
					if(pedidovendapagamento.getNumeroEmissaonota() != null && !pedidovendapagamento.getNumeroEmissaonota().equals("")){
						documento.setNumero(pedidovendapagamento.getNumeroEmissaonota().contains("/" + numParcela) ? pedidovendapagamento.getNumeroEmissaonota() : pedidovendapagamento.getNumeroEmissaonota() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + numParcela : ""));
					}else if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_VENDA)){
						documento.setNumero(pedidovenda.getCdpedidovenda() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + numParcela : ""));
					}else {
						documento.setNumero(pedidovendapagamento.getNumero() != null ? pedidovendapagamento.getNumero().toString() : "");
					}
					
					documento.setDocumentotipo(pedidovendapagamento.getDocumentotipo());
					if(pedidovendapagamento.getDataparcelaEmissaonota() != null){
						documento.setDtvencimento(pedidovendapagamento.getDataparcelaEmissaonota());
					}else {
						documento.setDtvencimento(pedidovendapagamento.getDataparcela());
					}
					documento.setEmpresa(empresa);
					documento.setEndereco(enderecoService.carregaEnderecoClienteForDocumento(pedidovenda.getCliente()));
					if(documento.getEndereco() == null)
						documento.setEndereco(pedidovenda.getEndereco());
					
					rateioParcelas = new Rateio();
					rateioParcelas.setListaRateioitem(rateio.getListaRateioitem());
					rateioService.atualizaValorRateio(rateioParcelas, documento.getValor());
					
					documento.setRateio(rateioParcelas);
					
					boolean documentoAgrupado = false;
					if(SinedUtil.isListNotEmpty(listaContareceber) && parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA)){
						for(Documento documentoVendaNota : listaContareceber){
							if(documentoVendaNota.getDtvencimento() != null && documento.getDtvencimento() != null && 
									SinedDateUtils.equalsIgnoreHour(documentoVendaNota.getDtvencimento(), documento.getDtvencimento()) && 
									documentoVendaNota.getDocumentotipo() != null && documento.getDocumentotipo() != null &&
									documentoVendaNota.getDocumentotipo().equals(documento.getDocumentotipo()) &&
									SinedUtil.isListNotEmpty(documentoVendaNota.getListaPedidovenda())){
								boolean vendaDiferente = true;
								for(Pedidovenda pedidovendaDocumento : documentoVendaNota.getListaPedidovenda()){
									if(pedidovenda.getCdpedidovenda().equals(pedidovendaDocumento.getCdpedidovenda())){
										vendaDiferente = false;
										break;
									}
								}
								if(vendaDiferente){
									if(documentoVendaNota.getValor() == null) documentoVendaNota.setValor(new Money());
									if(documento.getValor() == null) documento.setValor(new Money());
									documentoVendaNota.setValor(documentoVendaNota.getValor().add(documento.getValor()));
									
									Rateio rateioVendaNotaNovo = rateioService.getNovoRateio(documentoVendaNota.getRateio());
									Rateio rateioDocumento = rateioService.getNovoRateio(documento.getRateio());
									
									rateioService.ajustaPercentualRateioitem(documentoVendaNota.getValor(), rateioVendaNotaNovo.getListaRateioitem());
									rateioService.ajustaPercentualRateioitem(documentoVendaNota.getValor(), rateioDocumento.getListaRateioitem());
									
									rateioVendaNotaNovo.getListaRateioitem().addAll(rateioDocumento.getListaRateioitem());
									rateioitemService.agruparItensRateio(rateioVendaNotaNovo.getListaRateioitem());
									documentoVendaNota.setRateio(rateioVendaNotaNovo);
									rateioService.atualizaValorRateio(documentoVendaNota.getRateio(), documentoVendaNota.getValor());
									
									// FEITO ISSO PARA AJUSTAR O RATEIO
									BigDecimal somaItens = new BigDecimal(0);
									for (Rateioitem ri : rateioVendaNotaNovo.getListaRateioitem()) {
										somaItens = somaItens.add(SinedUtil.roundFloor(ri.getValor().getValue(), 2));			
									}
									BigDecimal diferenca = documentoVendaNota.getValor().getValue().subtract(somaItens);
									if(diferenca.compareTo(BigDecimal.ZERO) != 0){
										rateioVendaNotaNovo.getListaRateioitem().get(0).setValor(rateioVendaNotaNovo.getListaRateioitem().get(0).getValor().add(new Money(diferenca)));
									}
									
									documentoVendaNota.setDescricao("Referente aos pedidos de venda " + CollectionsUtil.listAndConcatenate(documentoVendaNota.getListaPedidovenda(), "cdpedidovenda", ",")+ "," + pedidovenda.getCdpedidovenda());
									
									StringBuilder linkHistorico = new StringBuilder();
									
									for(Pedidovenda pedidovendaDoc : documentoVendaNota.getListaPedidovenda()){
										linkHistorico.append(" <a href=\"javascript:visualizaPedidoVenda("+ pedidovendaDoc.getCdpedidovenda()+");\">"+pedidovendaDoc.getCdpedidovenda() +"</a>,");
									}
									if(Boolean.TRUE.equals(pedidovenda.getOrigemOtr())){
										linkHistorico.append(" <a href=\"javascript:visualizaPedidoVendaOtr("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
									}else{
										linkHistorico.append(" <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
									}
									
									documentohistorico = documentohistoricoService.geraHistoricoDocumento(documentoVendaNota);
									documentohistorico.setObservacao("Origem Pedido de venda: " + linkHistorico.toString());
									documentohistoricoService.saveOrUpdate(documentohistorico);
									
									listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
									listaDocumentohistorico.add(documentohistorico);
									documentoVendaNota.setListaDocumentohistorico(listaDocumentohistorico);
									
									documentoService.saveOrUpdate(documentoVendaNota);
									documentoService.callProcedureAtualizaDocumento(documentoVendaNota);
									addPedidovendaListaDocumento(documentoVendaNota, new Pedidovenda(pedidovenda.getCdpedidovenda()));
									listaDocumentosGerados.add(documentoVendaNota.getCddocumento());
									
									Documentoorigem documentoorigem = new Documentoorigem();
									documentoorigem.setPedidovenda(pedidovenda);
									documentoorigem.setDocumento(documentoVendaNota);
									documentoorigemService.saveOrUpdate(documentoorigem);
									
									pedidovendapagamento.setDocumento(documentoVendaNota);
									
									documentoAgrupado = true;
								}
							}
						}
					}
					
					if(!documentoAgrupado){
						documentoService.saveOrUpdate(documento);
						addPedidovendaListaDocumento(documento, new Pedidovenda(pedidovenda.getCdpedidovenda()));
						listaDocumentosGerados.add(documento.getCddocumento());
						if(parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA) && listaContareceber != null){
							listaContareceber.add(documento);
						}
						
						parcela = new Parcela();
						parcela.setDocumento(documento);
						parcela.setParcelamento(parcelamento);
						parcela.setOrdem(i);
						i++;
						parcelaService.saveOrUpdate(parcela);
						
						documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
						if(Boolean.TRUE.equals(pedidovenda.getOrigemOtr())){
							documentohistorico.setObservacao("Origem Pedido de venda: <a href=\"javascript:visualizaPedidoVendaOtr("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
						}else{
							documentohistorico.setObservacao("Origem Pedido de venda: <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
						}
						documentohistoricoService.saveOrUpdate(documentohistorico);
						listaDocumentohistorico.add(documentohistorico);
						documento.setListaDocumentohistorico(listaDocumentohistorico);
						
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setPedidovenda(pedidovenda);
						documentoorigem.setDocumento(documento);
						documentoorigemService.saveOrUpdate(documentoorigem);
						
						pedidovendapagamento.setDocumento(documento);
					}
					if(pedidovendapagamento.getPodeGerarCheque()){
						Cheque cheque = new Cheque();
						cheque.setBanco(pedidovendapagamento.getBanco());
						cheque.setAgencia(pedidovendapagamento.getAgencia());
						cheque.setConta(pedidovendapagamento.getConta());
						cheque.setNumero(pedidovendapagamento.getNumero() != null ? pedidovendapagamento.getNumero().toString() : "");
						cheque.setEmitente(pedidovendapagamento.getEmitente());
						cheque.setDtbompara(pedidovendapagamento.getDataparcela());
						cheque.setValor(pedidovendapagamento.getValororiginal());
						cheque.setEmpresa(pedidovenda.getEmpresa());
						cheque.setChequesituacao(Chequesituacao.PREVISTO);
						cheque.setCpfcnpj(pedidovendapagamento.getCpfcnpj());
						
						chequeService.saveOrUpdate(cheque);
						chequehistoricoService.criaHistorico(cheque,  pedidovenda);
						pedidovendapagamento.setCheque(cheque);
						if(pedidovendapagamento.getDocumento() != null && pedidovendapagamento.getDocumento().getCddocumento() != null){
							documentoService.updateCheque(pedidovendapagamento.getDocumento(), cheque);
							documento.setCheque(cheque);
						}
					}
					
					if(!origemContareceberAposEmissaoNota || !parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA)){ // a verifica��o da baixa ser� feita depois de criar todos os documentos por causa do agrupamento
						boolean podeRealizarBaixa = true;
						if(SinedUtil.isIntegracaoEcompleto() && this.isPedidoEcommerce(pedidovenda)){
							podeRealizarBaixa = ecom_PedidoVendaService.isPedidoEcompletoPago(pedidovenda);
						}
						if(podeRealizarBaixa){
							documentoService.baixaAutomaticaByVinculoProvisionado(documento);
						}
					}
				} else {
					documento = pedidovendapagamento.getDocumento();
					
					if(pedidovenda.getDocumentoAposEmissaoNota() != null && pedidovenda.getDocumentoAposEmissaoNota() && documento != null && documento.getCddocumento() != null){
						if(pedidovendapagamento.getNumeroEmissaonota() != null && !"".equals(pedidovendapagamento.getNumeroEmissaonota())){
							documentoService.updateNumeroNFDocumentoVenda(pedidovendapagamento.getDocumento(), pedidovendapagamento.getNumeroEmissaonota());
						}
						if(pedidovendapagamento.getDataparcela() != null){
							documentoService.updateDtvencimentoNFDocumentoVenda(pedidovendapagamento.getDocumento(), pedidovendapagamento.getDataparcela());
						}
					}else {
						if(documentotipo == null || documentotipo.getAntecipacao() == null || !documentotipo.getAntecipacao()){
							parcela = new Parcela();
							parcela.setDocumento(documento);
							parcela.setParcelamento(parcelamento);
							parcela.setOrdem(i);
							i++;
							parcelaService.saveOrUpdate(parcela);
						}
						
						documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
						if(Boolean.TRUE.equals(pedidovenda.getOrigemOtr())){
							documentohistorico.setObservacao("Origem Pedido de venda: <a href=\"javascript:visualizaPedidoVendaOtr("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
						}else{
							documentohistorico.setObservacao("Origem Pedido de venda: <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
						}
						documentohistoricoService.saveOrUpdate(documentohistorico);
						listaDocumentohistorico.add(documentohistorico);
						documento.setListaDocumentohistorico(listaDocumentohistorico);
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setPedidovenda(pedidovenda);
						documentoorigem.setDocumento(documento);
						documentoorigemService.saveOrUpdate(documentoorigem);
					}
					
					Rateio rateioDocumento = rateioService.getNovoRateio(rateio);
					documento.setRateio(rateioDocumento);
					rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
					rateioService.saveOrUpdate(documento.getRateio());
					documentoService.updateRateio(documento);
				}
			}
			
			pedidovendapagamento.setPedidovenda(pedidovenda);
			pedidovendapagamentoService.saveOrUpdate(pedidovendapagamento);
			if(documentotipo == null || documentotipo.getAntecipacao() == null || !documentotipo.getAntecipacao()){
				numParcela++;
			}
			verificaComissionamento(pedidovendapagamento, pedidovenda.getListaPedidovendapagamento().size(), null, null, null,false);
		}
		
		return listaDocumentosGerados;
	}
	
	protected void addPedidovendaListaDocumento(Documento documento, Pedidovenda pedidovenda) {
		if(pedidovenda.getCdpedidovenda() != null){
			if(documento.getListaPedidovenda() == null){
				documento.setListaPedidovenda(new ArrayList<Pedidovenda>());
			}
			boolean adicionar = true;
			if(SinedUtil.isListNotEmpty(documento.getListaPedidovenda())){
				for(Pedidovenda pedidovendaDocumento : documento.getListaPedidovenda()){
					if(pedidovenda.getCdpedidovenda().equals(pedidovendaDocumento.getCdpedidovenda())){
						adicionar = false;
						break;
					}
				}
			}
			if(adicionar){
				documento.getListaPedidovenda().add(pedidovenda);
			}
		}
	}
	
	protected Integer getQtdeIteracoesPgamento(Pedidovenda venda) {
		Integer iteracoes = 0;
		for (Pedidovendapagamento vendapagamento : venda.getListaPedidovendapagamento()) {
			if(!documentotipoService.isAntecipacao(vendapagamento.getDocumentotipo())){
				iteracoes++;
			}
		}
		return iteracoes;
	}
	protected List<Pedidovendapagamento> criarPagamentoPelaNegociacao(Pedidovenda pv){
		ArrayList<Pedidovendapagamento> listaPvPagamento = new ArrayList<Pedidovendapagamento>();
		List<PedidoVendaNegociacao> listaNegociacao = pedidoVendaNegociacaoService.findByPedidovenda(pv,false); 	
		for (PedidoVendaNegociacao pvNegociacao : listaNegociacao) {
			Pedidovendapagamento pvPagamento = new Pedidovendapagamento();	
			pvPagamento.setDocumentoantecipacao(pvNegociacao.getDocumentoAntecipacao());
			pvPagamento.setDocumentotipo(pvNegociacao.getDocumentoTipo());
			pvPagamento.setValororiginal(pvNegociacao.getValorOriginal());
			pvPagamento.setPedidovenda(pv);
			pvPagamento.setDataparcela(pvNegociacao.getDataParcela());
			pvPagamento.setValorjuros(pvNegociacao.getValorJuros());
			pvPagamento.setDocumento(pvNegociacao.getDocumento());
			pvPagamento.setEmitente(pvNegociacao.getEmitente());
			pvPagamento.setCpfcnpj(pvNegociacao.getCpfcnpj());
			pvPagamento.setAgencia(pvNegociacao.getAgencia());
			pvPagamento.setNumero(pvNegociacao.getNumero());
			pvPagamento.setCheque(pvNegociacao.getCheque());
			pvPagamento.setConta(pvNegociacao.getConta());
			pvPagamento.setBanco(pvNegociacao.getBanco());
			listaPvPagamento.add(pvPagamento);
		}	
		return listaPvPagamento;
	}
	public void verificaCancelamentoOuRecalculoComissionamento(Venda venda,Boolean isNegociacao) {
		if (venda != null && venda.getPedidovenda() != null && venda.getPedidovenda().getCdpedidovenda() != null) {
			Pedidovenda bean = loadForEntrada(venda.getPedidovenda());
			if (vendaService.isVendaCancelada(venda)) {
				documentocomissaoService.deleteDocumentocomissaoByPedidovendaCancelado(bean);
			} else {
				List<Venda> listaVenda = vendaService.findByPedidovenda(venda.getPedidovenda());
				List<Documentocomissao> listaDocumentocomissao = new ArrayList<Documentocomissao>();
				if(!Boolean.TRUE.equals(isNegociacao)){
					listaDocumentocomissao = documentocomissaovendaService.findForRecalcularComissaoPedidovenda(bean, SinedUtil.isListNotEmpty(listaVenda) ? CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",") : null);
					bean.setListaPedidovendapagamento(pedidovendapagamentoService.findPedidovendaPagamentoByPedidovenda(bean));
				}else{				
					bean.setListaPedidovendapagamento(criarPagamentoPelaNegociacao(bean));
				}
				bean.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidovenda(bean));
				
 				if (vendaService.verificaDevolucaototal(venda) && !isNegociacao) {
					documentocomissaoService.deleteDocumentocomissaoByPedidovendaCancelado(bean);
				} else if (bean.getListaPedidovendapagamento() != null && !bean.getListaPedidovendapagamento().isEmpty()) {
					for (Pedidovendapagamento pedidovendapagamento: bean.getListaPedidovendapagamento()) {
						verificaComissionamento(pedidovendapagamento, bean.getListaPedidovendapagamento().size(), listaDocumentocomissao, null, venda,isNegociacao);
					}
				}
			}
		}
		if(!Boolean.TRUE.equals(isNegociacao)){
			verificaCancelamentoOuRecalculoComissionamento(venda,true);
		}
	}
	
	/**
	 * M�todo que verifica e gera o comissionanento da venda de acordo com o n�mero de parcelas e 
	 * cadastro de colaboradores/comiss�o no material
	 *
	 * @param pedidovendapagamento
	 * @param qtdeParcelas
	 * @author Luiz Fernando
	 * @param venda 
	 */
	public void verificaComissionamento(Pedidovendapagamento pedidovendapagamento, Integer qtdeParcelas, List<Documentocomissao> listaDocumentocomissao, Colaborador vendedoranterior, Venda venda, Boolean isNegociacao){
		boolean CALCULAR_COMISSAO_PROPORCIONAL_VENDA = parametrogeralService.getBoolean(Parametrogeral.CALCULAR_COMISSAO_PROPORCIONAL_VENDA);
		
		if(pedidovendapagamento != null && pedidovendapagamento.getPedidovenda() != null && pedidovendapagamento.getPedidovenda().getCdpedidovenda() != null && 
				((pedidovendapagamento.getDocumento() != null && pedidovendapagamento.getDocumento().getCddocumento() != null)
						||pedidovendapagamento.getDocumentoantecipacao() != null && pedidovendapagamento.getDocumentoantecipacao().getCddocumento() != null)){
			Pedidovenda pedidovenda = this.findForComissiaoPedidovendaTipoReserva(pedidovendapagamento.getPedidovenda());
			
			if(pedidovenda != null){
				Pedidovendatipo pedidovendatipo = null;
				if(pedidovenda.getPedidovendatipo() != null && pedidovenda.getPedidovendatipo().getCdpedidovendatipo() != null){
					pedidovendatipo = pedidovenda.getPedidovendatipo();
				}
				
				if(!pedidovendatipoService.tipoRepresentacao(pedidovendatipo)){
					Documento documentoForComissao = pedidovendapagamento.getDocumentoantecipacao() != null? pedidovendapagamento.getDocumentoantecipacao(): pedidovendapagamento.getDocumento();
					Boolean jurosvendacomissao = Boolean.valueOf("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.JUROS_VENDA_COMISSAO)) ? Boolean.TRUE : Boolean.FALSE);
					
					//valor da diferen�a (valor e valor atual do documento) (isto � preciso para saber o valor do desconto e/ou juros da conta a receber)
					Money valorDescontoJuros = null;
					Documento doc = pedidovendapagamento.getDocumento();
					if(doc != null && doc.getCddocumento() != null){
						if(doc.getAux_documento() == null){
							doc = contareceberService.loadWithValoratual(doc);
						}
					}
					if(doc != null && doc.getValor() != null && 
							doc.getAux_documento() != null && doc.getAux_documento().getValoratual() != null){
						valorDescontoJuros = doc.getAux_documento().getValoratual().subtract(doc.getValor());
					}
					if(jurosvendacomissao){
						Money juros = new Money(pedidovenda.getTotalparcela()).subtract(pedidovenda.getTotalvenda());
						if(juros.getValue().doubleValue() > 0){
							if(valorDescontoJuros == null){
								valorDescontoJuros = juros.divide(new Money(qtdeParcelas));
							}else {
								valorDescontoJuros = valorDescontoJuros.add(juros.divide(new Money(qtdeParcelas)));
							}
						}
					}
					if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
						setMaterialcolaboradorByMaterialVenda(pedidovenda);
						
						Documentocomissao documentocomissao = new Documentocomissao();
						Documentocomissao documentocomissaoByMaterial = null;
						List<Documentocomissao> listaComissao = new ArrayList<Documentocomissao>();
						List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
						Double valor = 0.0;
						Double valorprodutogrupo = 0.0;
						Double valorgrupo = 0.0;
						Double valorgrupovendedorprincipal = 0.0;
						Material material = null;
	//					Double percentualcomissaodividirprincipal = 0.0;
	//					Double valorcomissionamentogrupodividirprincipal = 0.0;
						Boolean comissaogrupomaterial = Boolean.FALSE;
						Boolean comissaotabelapreco = Boolean.FALSE;
						Double qtdeMaterial = 0.0;
						Double qtdedevolvida = 0.0;
						Colaboradorcargo colaboradorcargo;
						Money valorDesconto = new Money(0.0);
						
						List<Documentocomissao> listaComissaoCliente = new ArrayList<Documentocomissao>();
						Cliente clienteindicacao = pedidovenda.getClienteindicacao();
						Double valorprodutogrupoCliente = 0.0;
						Double valorgrupoCliente = 0.0;
						Double percentualcomissaoCliente = 0.0;
						Double valorcomissionamentogrupoCliente = 0.0;
						
						Double valorTotalVenda = 0d;
						
						if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
							for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
								material = materialService.findForComissaovendaGrupomaterial(pvm.getMaterial(), pedidovenda.getColaborador());
								if(material == null){
									material = materialService.findForComissaovenda(pvm.getMaterial());
								}
								qtdedevolvida = materialdevolucaoService.getQtdeJaDevolvidaByPedidoVenda(pvm);
								qtdeMaterial = pvm.getQuantidade();
								if(pvm.getMultiplicador() != null){
									qtdeMaterial = qtdeMaterial * pvm.getMultiplicador();
								}
								if(qtdedevolvida != null && qtdedevolvida > 0 && qtdeMaterial != null){
									qtdeMaterial = qtdeMaterial - qtdedevolvida;
									if(Boolean.TRUE.equals(isNegociacao)){	
										pvm.setQtdOriginal(qtdeMaterial);
									}
									pvm.setQuantidade(qtdeMaterial);
								}
								if(Boolean.TRUE.equals(isNegociacao)){
									Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pvm);
									if (qtdeRestante != null && qtdeRestante < 0) {
										pvm.setQuantidade(qtdeRestante * (-1));	
									}
									if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().compareTo(0.0) != 0){
										Money descontoGeral = pedidovenda.getDesconto();
										Money divisor = new Money(qtdeMaterial);
										Money mutiplicador = new Money(pvm.getQuantidade());
										Money unitario = descontoGeral.divide(divisor);
										valorDesconto.add(unitario.multiply(mutiplicador));
									}		
									if(pvm.getDesconto() !=null && pvm.getDesconto().compareTo(0.0)!=0){
										Money descontoGeral = pvm.getDesconto();
										Money divisor = new Money(qtdeMaterial);
										Money mutiplicador = new Money(pvm.getQuantidade());
										Money unitario = descontoGeral.divide(divisor);
										pvm.setDesconto(unitario.multiply(mutiplicador));
									}
								}
								
								valorTotalVenda += (qtdeMaterial * pvm.getPreco()) - (pvm.getDesconto() != null ? pvm.getDesconto().getValue().doubleValue() : 0.0);
								if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){											
									valorTotalVenda = valorTotalVenda - valorDescontoProporcional(pedidovenda.getDesconto(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
								}
								if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){											
									valorTotalVenda = valorTotalVenda - valorDescontoProporcional(pedidovenda.getValorusadovalecompra(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
								}
							}
							if(Boolean.TRUE.equals(isNegociacao)){
								pedidovenda.setDesconto(valorDesconto);
							}
							
							Double proporcaoParcelaTotal = 1d / qtdeParcelas;
							if(CALCULAR_COMISSAO_PROPORCIONAL_VENDA && pedidovendapagamento.getValororiginal() != null && valorTotalVenda != null && valorTotalVenda > 0) {
								proporcaoParcelaTotal = pedidovendapagamento.getValororiginal().doubleValue() / valorTotalVenda;
							}
							
							for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
								if(pvm.getQtdOriginal() == null && !Boolean.TRUE.equals(isNegociacao)){
									continue;
								}
								
								comissaogrupomaterial = Boolean.FALSE;
	//							valorcomissionamentogrupodividirprincipal = 0.0;
	//							percentualcomissaodividirprincipal = 0.0;
								valorprodutogrupo = 0.0;
								material =  materialService.findForComissaovendaGrupomaterial(pvm.getMaterial(), pedidovenda.getColaborador());
								if(material == null){
									material = materialService.findForComissaovenda(pvm.getMaterial());
								}
								colaboradorcargo = colaboradorcargoService.findCargoAtual(pedidovenda.getColaborador());
								qtdeMaterial = pvm.getQuantidade();
								
								//if(qtdeMaterial != null && qtdeMaterial > 0){
									if(pvm.getComissionamento() != null || (pvm.getMaterial().getListaMaterialcolaborador() == null || pvm.getMaterial().getListaMaterialcolaborador().isEmpty())){
										if(material != null && material.getMaterialgrupo() != null && (pvm.getComissionamento() != null ||  
												(material.getMaterialgrupo().getListaMaterialgrupocomissaovenda() != null && !material.getMaterialgrupo().getListaMaterialgrupocomissaovenda().isEmpty()))){
											
											ComissaoMaterialVenda comissaoMaterialVenda = new ComissaoMaterialVenda();
											comissionamentoService.existeComissaoMategrial(comissaoMaterialVenda, pedidovenda, pvm, material, pedidovendapagamento, colaboradorcargo, pedidovendatipo, qtdeParcelas, valorDescontoJuros, false, valorTotalVenda);
											comissaogrupomaterial = comissaoMaterialVenda.isComissaogrupomaterial();
											comissaotabelapreco = comissaoMaterialVenda.isComissaotabelapreco();
											
											//List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
											Boolean diferente = Boolean.TRUE;
											if(comissaoMaterialVenda.isDividirprincipal()){
//												Cliente cliente  = clienteService.findVendedores(pedidovenda.getCliente());
//												if(cliente != null && cliente.getListaClientevendedor() != null && !cliente.getListaClientevendedor().isEmpty()){
//													for(Clientevendedor clientevendedor : cliente.getListaClientevendedor()){
//														if(clientevendedor.getColaborador() != null){
//															if(!vendaService.existeColaborador(listaColaborador, clientevendedor.getColaborador())){														
//																listaColaborador.add(clientevendedor.getColaborador());
//															}
//															if(pedidovenda.getColaborador() != null && clientevendedor.getColaborador().equals(pedidovenda.getColaborador())){
//																diferente = Boolean.FALSE;
//																listaColaborador = new ArrayList<Colaborador>();
//																listaColaborador.add(pedidovenda.getColaborador());
//																break;
//															}
//														}
//													}
//												}
//												
//												if(diferente || cliente == null){
//													if(!vendaService.existeColaborador(listaColaborador, pedidovenda.getColaborador())){														
//														listaColaborador.add(pedidovenda.getColaborador());
//													}
//												}
												
												Colaborador vendedorPrincipal = clientevendedorService.getVendedorPrincipal(pedidovenda.getCliente());
												if(vendedorPrincipal != null && !vendaService.existeColaborador(listaColaborador, vendedorPrincipal)) {
													vendedorPrincipal.setVendedorprincipalTrans(true);
													listaColaborador.add(vendedorPrincipal);
												}
												if(pedidovenda.getColaborador() != null) {
													if(vendaService.existeColaborador(listaColaborador, pedidovenda.getColaborador())) {
														diferente = false;
													} else {
														listaColaborador.add(pedidovenda.getColaborador());
													}
												}
												
		//										valorgrupo += vm.getQuantidade() * vm.getPreco() / qtdeParcelas * percentualcomissao;
												valorprodutogrupo += (qtdeMaterial * pvm.getPreco()) - (pvm.getDesconto() != null ? pvm.getDesconto().getValue().doubleValue() : 0.0);
												if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){											
													valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getDesconto(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){											
													valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getValorusadovalecompra(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												
												valorprodutogrupo = valorprodutogrupo * proporcaoParcelaTotal;
//												valorprodutogrupo = valorprodutogrupo / qtdeParcelas;
												
												if(valorDescontoJuros != null){
													valorprodutogrupo += valorDescontojurosProporcional(valorDescontoJuros, pvm.getQuantidade(), pvm.getPreco(), pedidovenda.getListaPedidovendamaterial());
												}
												if(diferente && listaColaborador.size() > 1 ){	
													valorgrupovendedorprincipal = 0.0;
													if(comissaoMaterialVenda.getPercentualcomissaovendedorprincipal() != null && comissaoMaterialVenda.getPercentualcomissaovendedorprincipal() > 0){
														valorgrupovendedorprincipal = valorprodutogrupo * comissaoMaterialVenda.getPercentualcomissaovendedorprincipal();
														if(valorgrupovendedorprincipal != 0) valorgrupovendedorprincipal = valorgrupovendedorprincipal / 100;
													}
													if(comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() != null && comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() > 0) {
														valorgrupovendedorprincipal += (comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() * proporcaoParcelaTotal);
//														valorgrupovendedorprincipal += (comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() / qtdeParcelas);
													}											
												}
												valorgrupo = 0.0;
												if(comissaoMaterialVenda.getPercentualcomissao() != null && comissaoMaterialVenda.getPercentualcomissao() > 0){
													Double percentual = comissaoMaterialVenda.getPercentualcomissao();
													if(!diferente && listaColaborador.size() == 1 && comissaoMaterialVenda.getPercentualcomissaovendedorprincipal() != null && comissaoMaterialVenda.getPercentualcomissaovendedorprincipal() > 0) {
														percentual += comissaoMaterialVenda.getPercentualcomissaovendedorprincipal();
													}
													
													valorgrupo = valorprodutogrupo * percentual;
													if(valorgrupo != 0)
														valorgrupo = valorgrupo / 100;
												}
												if(comissaoMaterialVenda.getValorcomissionamentogrupo() != null && comissaoMaterialVenda.getValorcomissionamentogrupo() > 0) {
													Double valorComissao = comissaoMaterialVenda.getValorcomissionamentogrupo();
													if(!diferente && listaColaborador.size() == 1 && comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() != null && comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal() > 0) {
														valorComissao += comissaoMaterialVenda.getValorcomissionamentogrupovendedorprincipal();
													}
													
													valorgrupo += (valorComissao * proporcaoParcelaTotal);
//													valorgrupo += (comissaoMaterialVenda.getValorcomissionamentogrupo() / qtdeParcelas);
												}
											}else {
		//										valorgrupo += vm.getQuantidade() * vm.getPreco() / qtdeParcelas * percentualcomissao;
												valorprodutogrupo = (qtdeMaterial * pvm.getPreco()) - (pvm.getDesconto() != null ? pvm.getDesconto().getValue().doubleValue() : 0.0);;
												if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
													valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getDesconto(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												if(pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0){											
													valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getValorusadovalecompra(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												valorprodutogrupo = valorprodutogrupo * proporcaoParcelaTotal;
//												valorprodutogrupo = valorprodutogrupo / qtdeParcelas;
												if(valorDescontoJuros != null){
													valorprodutogrupo += valorDescontojurosProporcional(valorDescontoJuros, pvm.getQuantidade(), pvm.getPreco(), pedidovenda.getListaPedidovendamaterial());
												}
												
												valorgrupo = 0.0;
												if(comissaoMaterialVenda.getPercentualcomissao() != null && comissaoMaterialVenda.getPercentualcomissao() > 0){
													valorgrupo = valorprodutogrupo * comissaoMaterialVenda.getPercentualcomissao();
													if(valorgrupo != 0)
														valorgrupo = valorgrupo / 100;
												}
												if(comissaoMaterialVenda.getValorcomissionamentogrupo() != null && comissaoMaterialVenda.getValorcomissionamentogrupo() > 0) {
													valorgrupo += (comissaoMaterialVenda.getValorcomissionamentogrupo() * proporcaoParcelaTotal);
//													valorgrupo += (comissaoMaterialVenda.getValorcomissionamentogrupo() / qtdeParcelas);
												}
												
												if(!vendaService.existeColaborador(listaColaborador, pedidovenda.getColaborador())){														
													listaColaborador.add(pedidovenda.getColaborador());
												}
											}
											
											
											
											if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
												for(Documentocomissao dc : listaDocumentocomissao){
													documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
												}
											}
											
											for(Colaborador colaborador : listaColaborador){
												if(valorgrupo != null && valorgrupo != 0 && (!colaborador.isVendedorprincipalTrans() || listaColaborador.size() == 1)){
													documentocomissao = new Documentocomissao();
													documentocomissao.setPessoa(new Pessoa(colaborador.getCdpessoa()));
													documentocomissao.setValorcomissao( new Money(valorgrupo));
													documentocomissao.setDocumento(documentoForComissao);
													documentocomissao.setPedidovenda(pedidovenda);
													if(venda != null && venda.getCdvenda() != null)
														documentocomissao.setVenda(venda);
													listaComissao.add(documentocomissao);
												}else if(valorgrupovendedorprincipal != null && valorgrupovendedorprincipal != 0 && colaborador.isVendedorprincipalTrans()){
													documentocomissao = new Documentocomissao();
													documentocomissao.setPessoa(new Pessoa(colaborador.getCdpessoa()));
													documentocomissao.setValorcomissao( new Money(valorgrupovendedorprincipal));
													documentocomissao.setDocumento(documentoForComissao);
													documentocomissao.setPedidovenda(pedidovenda);
													if(venda != null && venda.getCdvenda() != null)
														documentocomissao.setVenda(venda);
													listaComissao.add(documentocomissao);
												}
											}
											
											if (clienteindicacao != null && material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getListaMaterialgrupocomissaovenda() != null && !material.getMaterialgrupo().getListaMaterialgrupocomissaovenda().isEmpty()) {

												Fornecedor fornecedor = null;
												if (pvm.getFornecedor() != null && pvm.getFornecedor().getCdpessoa() != null) {
													fornecedor = pvm.getFornecedor();
												}
												
												percentualcomissaoCliente = null;
												valorcomissionamentogrupoCliente = null;

												for (Materialgrupocomissaovenda materialgrupocomissaovenda: material.getMaterialgrupo().getListaMaterialgrupocomissaovenda()) {
													if (vendaService.existCargoComissaoDocumentotipo(materialgrupocomissaovenda, pedidovendapagamento.getDocumentotipo(), null, pedidovendatipo, fornecedor)) {
														if (materialgrupocomissaovenda.getComissionamento() != null && materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento() != null) {
															if (Criteriocomissionamento.INDICADOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())) {
																if (materialgrupocomissaovenda.getComissionamento().getPercentual() != null && materialgrupocomissaovenda.getComissionamento().getPercentual() > 0) percentualcomissaoCliente = materialgrupocomissaovenda.getComissionamento().getPercentual();
																else if (materialgrupocomissaovenda.getComissionamento().getValor() != null && materialgrupocomissaovenda.getComissionamento().getValor() > 0) valorcomissionamentogrupoCliente = materialgrupocomissaovenda.getComissionamento().getValor();
															}
														}
													}
												}

												valorprodutogrupoCliente = (qtdeMaterial * pvm.getPreco()) - (pvm.getDesconto() != null ? pvm.getDesconto().getValue().doubleValue() : 0.0);;
												if (pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0) {
													valorprodutogrupoCliente = valorprodutogrupoCliente - valorDescontoProporcional(
														pedidovenda.getDesconto(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												if (pedidovenda.getValorusadovalecompra() != null && pedidovenda.getValorusadovalecompra().getValue().doubleValue() > 0) {
													valorprodutogrupoCliente = valorprodutogrupoCliente - valorDescontoProporcional(
														pedidovenda.getValorusadovalecompra(), pvm.getPreco(), pvm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												valorprodutogrupoCliente = valorprodutogrupoCliente * proporcaoParcelaTotal;
//												valorprodutogrupoCliente = valorprodutogrupoCliente / qtdeParcelas;
												if (valorDescontoJuros != null) {
													valorprodutogrupoCliente += valorDescontojurosProporcional(
													valorDescontoJuros, pvm.getQuantidade() * (pvm.getMultiplicador() != null && pvm.getMultiplicador() > 0 ? pvm.getMultiplicador() : 1d), pvm.getPreco(), pedidovenda.getListaPedidovendamaterial());
												}
												
												valorgrupoCliente = null;
												if (percentualcomissaoCliente != null && percentualcomissaoCliente > 0) {
													valorgrupoCliente = valorprodutogrupoCliente * percentualcomissaoCliente;
													if (valorgrupoCliente != 0) valorgrupoCliente = valorgrupoCliente / 100;
												}
												if (valorcomissionamentogrupoCliente != null && valorcomissionamentogrupoCliente > 0) {
													valorgrupoCliente = (valorcomissionamentogrupoCliente * proporcaoParcelaTotal);
//													valorgrupoCliente += (valorcomissionamentogrupoCliente / qtdeParcelas);
												}

												if (listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()) {
													for (Documentocomissao dc: listaDocumentocomissao) {
														documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
													}
												}

												if (valorgrupoCliente != null && valorgrupoCliente != 0) {
													documentocomissao = new Documentocomissao();
													documentocomissao.setPessoa(new Pessoa(
													clienteindicacao.getCdpessoa()));
													documentocomissao.setValorcomissao(new Money(valorgrupoCliente));
													documentocomissao.setDocumento(documentoForComissao);
													documentocomissao.setPedidovenda(pedidovenda);
													documentocomissao.setVenda(venda);
													documentocomissao.setIndicacao(true);
													listaComissaoCliente.add(documentocomissao);
												}

											}
										}
									}
									
									if(pvm.getComissionamento() == null && pvm.getMaterial() != null && pvm.getMaterial().getListaMaterialcolaborador() != null && 
											!pvm.getMaterial().getListaMaterialcolaborador().isEmpty() && pvm.getPreco() != null){
										ComissaoMaterialVenda comissaoMaterialVenda = new ComissaoMaterialVenda();
										comissionamentoService.existeComissaoMategrial(comissaoMaterialVenda, pedidovenda, pvm, material, pedidovendapagamento, colaboradorcargo, pedidovendatipo, qtdeParcelas, valorDescontoJuros, false, valorTotalVenda);
										documentocomissaoByMaterial = comissaoMaterialVenda.getDocumentocomissaoByMaterial();
										
										if(comissaoMaterialVenda.isAchou() && comissaoMaterialVenda.getValor() != null){
											valor += comissaoMaterialVenda.getValor();
										}
									}
								}
							}
							
							for(Colaborador colaborador : listaColaborador){
								if(!documentocomissaovendaService.existeComissaoRepassadaParaColaborador(colaborador, null, pedidovenda)){
									Money valorComissao = new Money(0.0);
									for(Documentocomissao dc : listaComissao){
										if(colaborador.getCdpessoa().equals(dc.getPessoa().getCdpessoa())){
											valorComissao = valorComissao.add(dc.getValorcomissao());
										}
									}
									if((comissaogrupomaterial || comissaotabelapreco) && valor != null && valor > 0 && documentocomissaoByMaterial != null && documentocomissaoByMaterial.getColaborador() != null){
										if(documentocomissaoByMaterial.getColaborador().equals(colaborador)){
											valorComissao = valorComissao.add(new Money(valor / 100));
											valor = 0d;
											documentocomissaoByMaterial = null;
										}								
									}
									
									if(valorComissao != null && valorComissao.getValue().doubleValue() > 0){
										Documentocomissao documentocomissaoColaborador = new Documentocomissao();
										documentocomissaoColaborador.setPessoa(new Pessoa(colaborador.getCdpessoa()));
										documentocomissaoColaborador.setValorcomissao(valorComissao);
										documentocomissaoColaborador.setDocumento(documentoForComissao);
										documentocomissaoColaborador.setPedidovenda(pedidovenda);
										if(venda != null && venda.getCdvenda() != null)
											documentocomissaoColaborador.setVenda(venda);
										documentocomissaoService.saveOrUpdate(documentocomissaoColaborador);
									}
								}
							}
						//}
						
						if (clienteindicacao != null && listaComissaoCliente != null && !listaComissaoCliente.isEmpty()) {
							Money valorComissaoCliente = new Money(0.0);
							for (Documentocomissao dc: listaComissaoCliente) {
								if (clienteindicacao.getCdpessoa().equals(dc.getPessoa().getCdpessoa())) {
									valorComissaoCliente = valorComissaoCliente.add(dc.getValorcomissao());
								}
							}

							Documentocomissao documentocomissaoCliente = new Documentocomissao();
							documentocomissaoCliente.setPessoa(new Pessoa(
							clienteindicacao.getCdpessoa()));
							documentocomissaoCliente.setValorcomissao(valorComissaoCliente);
							documentocomissaoCliente.setDocumento(documentoForComissao);
							documentocomissaoCliente.setPedidovenda(pedidovenda);
							if(venda != null && venda.getCdvenda() != null)
								documentocomissaoCliente.setVenda(venda);
							documentocomissaoCliente.setIndicacao(true);
							documentocomissaoService.saveOrUpdate(documentocomissaoCliente);
						}
						
						if(!comissaogrupomaterial && !comissaotabelapreco && valor != null && valor > 0 && documentocomissaoByMaterial != null && documentocomissaoByMaterial.getColaborador() != null){
							
							if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
								for(Documentocomissao dc : listaDocumentocomissao){
									documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
								}
							}
							
							documentocomissaoByMaterial.setPessoa(new Pessoa(pedidovenda.getColaborador().getCdpessoa()));
							documentocomissaoByMaterial.setValorcomissao( new Money(valor / 100));
							documentocomissaoByMaterial.setDocumento(documentoForComissao);
							documentocomissaoByMaterial.setPedidovenda(pedidovenda);
							if(venda != null && venda.getCdvenda() != null)
								documentocomissaoByMaterial.setVenda(venda);
							documentocomissaoService.saveOrUpdate(documentocomissaoByMaterial);
							
						}
					}
				}
			}
		}
	}
	
	public void verificaComissionamentoRepresentacao(Pedidovenda bean, List<Documentocomissao> listaDocumentocomissao, Colaborador vendedoranterior, Venda venda, boolean recalcular){
		if(bean != null){
			Pedidovenda pedidovenda = this.findForComissiaoPedidovendaTipoReserva(bean);
			Pedidovendatipo pedidovendatipo = null;
			
			if(pedidovenda != null){
				if(pedidovenda.getPedidovendatipo() != null && pedidovenda.getPedidovendatipo().getCdpedidovendatipo() != null){
					pedidovendatipo = pedidovenda.getPedidovendatipo();
				}

				if(pedidovendatipoService.tipoRepresentacao(pedidovendatipo)){
					if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
						StringBuilder whereIn = new StringBuilder();
						for(Pedidovendamaterial vendamaterial : pedidovenda.getListaPedidovendamaterial()){
							if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null){
								if(!"".equals(whereIn.toString())) whereIn.append(",");						
								whereIn.append(vendamaterial.getMaterial().getCdmaterial().toString());
							}
						}
						List<Materialcolaborador> listaMaterialcolaborador = materialcolaboradorService.findMaterialcolaboradorByVenda(whereIn.toString(), pedidovenda.getColaborador());
						Set<Materialcolaborador> listaNova;
						if(listaMaterialcolaborador != null && !listaMaterialcolaborador.isEmpty()){
							for(Pedidovendamaterial vendamaterial : pedidovenda.getListaPedidovendamaterial()){
								if(vendamaterial.getMaterial() != null){
									listaNova = new ListSet<Materialcolaborador>(Materialcolaborador.class);
									for(Materialcolaborador mc : listaMaterialcolaborador){
										if(mc.getMaterial() != null && mc.getMaterial().equals(vendamaterial.getMaterial()))
											listaNova.add(mc);
									}
									if(!listaNova.isEmpty())
										vendamaterial.getMaterial().setListaMaterialcolaborador(listaNova);
								}
							}
						}
						
						Boolean achou = false;
						Documentocomissao documentocomissao = new Documentocomissao();
						Documentocomissao documentocomissaoByMaterial = null;
						List<Documentocomissao> listaComissao = new ArrayList<Documentocomissao>();
						List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
						List<Documentocomissao> listaComissaoFornecedoragencia = new ArrayList<Documentocomissao>();
						Fornecedor fornecedoragencia = pedidovenda.getAgencia();
						Double valorprodutoFornecedoragencia = 0.0;
						Double valor = 0.0;
						Double valorprodutogrupo = 0.0;
						Double valorgrupo = 0.0;
						Double valorgrupovendedorprincipal = 0.0;
						Double valortotalmaterial = 0.0;
						Material material = null;
						Double percentualcomissao = 0.0;
						Double percentualcomissaovendedorprincipal = 0.0;
						Double valorcomissionamentogrupo = 0.0;
						Double valorcomissionamentogrupovendedorprincipal = 0.0;
						Boolean dividirprincipal = Boolean.FALSE;
						Boolean comissaogrupomaterial = Boolean.FALSE;
						Boolean existcomissaogrupomaterial = Boolean.FALSE;
						Double qtdedevolvida = 0.0;
						Double qtdeMaterial = 0.0;
						Colaboradorcargo colaboradorcargo;
						
						if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
							for(Pedidovendamaterial vm : pedidovenda.getListaPedidovendamaterial()){
								if(recalcular){
									Materialgrupocomissaovenda mgca = null;
									if(venda != null && venda.getFornecedorAgencia() != null){
										mgca = materialgrupocomissaovendaService.getComissaoAgencia(
														vm.getMaterial(),
														vm.getFornecedor(),
														venda.getFornecedorAgencia(),
														venda.getDocumentotipo(),
														pedidovendatipo
														);
										
										if(mgca != null && mgca.getComissionamento() != null && mgca.getComissionamento().getPercentual() != null){
											vm.setPercentualcomissaoagencia(mgca.getComissionamento().getPercentual());
										}
									}
								}
								
								material = materialService.findForComissaovendaGrupomaterial(vm.getMaterial(), pedidovenda.getColaborador());
								if(material == null){
									material = materialService.findForComissaovenda(vm.getMaterial());
								}
								
								qtdedevolvida = materialdevolucaoService.getQtdeJaDevolvida(venda, vm.getMaterial(), null);
								qtdeMaterial = vm.getQuantidade();
								if(vm.getMultiplicador() != null){
									qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
								}
								if(qtdedevolvida != null && qtdedevolvida > 0 && qtdeMaterial != null){
									qtdeMaterial = qtdeMaterial - qtdedevolvida;
									vm.setQuantidade(qtdeMaterial);
								}
							}
							
							for(Pedidovendamaterial vm : pedidovenda.getListaPedidovendamaterial()){
								comissaogrupomaterial = Boolean.FALSE;
								percentualcomissao = 0.0;
								valorcomissionamentogrupo = 0.0;
								valorcomissionamentogrupovendedorprincipal = 0.0;
								percentualcomissaovendedorprincipal = 0.0;
								valorprodutogrupo = 0.0;
								material = materialService.findForComissaovendaGrupomaterial(vm.getMaterial(), pedidovenda.getColaborador());
								if(material == null){
									material = materialService.findForComissaovenda(vm.getMaterial());
								}
								colaboradorcargo = colaboradorcargoService.findCargoAtual(pedidovenda.getColaborador());
								
								qtdeMaterial = vm.getQuantidade();
								if(vm.getMultiplicador() != null){
									qtdeMaterial = qtdeMaterial * vm.getMultiplicador();
								}
								
								if(vm.getMaterial().getListaMaterialcolaborador() == null || vm.getMaterial().getListaMaterialcolaborador().isEmpty()){
									if(material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getListaMaterialgrupocomissaovenda() != null &&
											!material.getMaterialgrupo().getListaMaterialgrupocomissaovenda().isEmpty()){
										
										Fornecedor fornecedor = null;
										if (vm.getFornecedor() != null && vm.getFornecedor().getCdpessoa() != null){
											fornecedor = vm.getFornecedor();
										}
										
										for(Materialgrupocomissaovenda materialgrupocomissaovenda : material.getMaterialgrupo().getListaMaterialgrupocomissaovenda()){
											if(vendaService.existCargoComissaoDocumentotipo(materialgrupocomissaovenda, pedidovenda.getDocumentotipo(), colaboradorcargo, pedidovendatipo, fornecedor)){
												if(materialgrupocomissaovenda.getComissionamento() != null && 
														materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento() != null){
													if(Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())){
														dividirprincipal = true;
														comissaogrupomaterial = Boolean.TRUE;
														existcomissaogrupomaterial = true;
														
														if(materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() != null && materialgrupocomissaovenda.getComissionamento().getPercentualdivisao() > 0)
															percentualcomissaovendedorprincipal += materialgrupocomissaovenda.getComissionamento().getPercentualdivisao();
														else if(materialgrupocomissaovenda.getComissionamento().getValordivisao() != null && materialgrupocomissaovenda.getComissionamento().getValordivisao() > 0)														
															valorcomissionamentogrupovendedorprincipal += materialgrupocomissaovenda.getComissionamento().getValordivisao();
														
													}else { 
														if(Criteriocomissionamento.SEM_CRITERIO.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento())){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}else if(Criteriocomissionamento.IGUAL_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && this.isIgualValorvenda(material, pedidovenda, vm)){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}else if(Criteriocomissionamento.ABAIXO_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && this.isAbaixoValorvenda(material, pedidovenda, vm)){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}else if(Criteriocomissionamento.ACIMA_VALOR_VENDA.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && this.isAcimaValorvenda(material, pedidovenda, vm)){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}else if(Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && this.isPrimeiraVendaCliente(pedidovenda.getCliente(), pedidovenda.getEmpresa(), pedidovenda)){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}else if(Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE.equals(materialgrupocomissaovenda.getComissionamento().getCriteriocomissionamento()) && this.isPrimeiraVendaGrupoCliente(pedidovenda.getCliente(), material.getMaterialgrupo(), pedidovenda.getEmpresa(), pedidovenda)){
															comissaogrupomaterial = Boolean.TRUE;
															existcomissaogrupomaterial = true;
														}
													}
													
													if(existcomissaogrupomaterial){
														if(materialgrupocomissaovenda.getComissionamento().getPercentual() != null && materialgrupocomissaovenda.getComissionamento().getPercentual() > 0)
															percentualcomissao += materialgrupocomissaovenda.getComissionamento().getPercentual();
														else if(materialgrupocomissaovenda.getComissionamento().getValor() != null && materialgrupocomissaovenda.getComissionamento().getValor() > 0)														
															valorcomissionamentogrupo += materialgrupocomissaovenda.getComissionamento().getValor();
													}
													
													existcomissaogrupomaterial = false;
												}
											}
										}
	
										Boolean diferente = Boolean.TRUE;
										if(dividirprincipal != null && dividirprincipal){
											Cliente cliente  = clienteService.findVendedores(pedidovenda.getCliente());
											if(cliente != null && cliente.getListaClientevendedor() != null && !cliente.getListaClientevendedor().isEmpty()){
												for(Clientevendedor clientevendedor : cliente.getListaClientevendedor()){
													if(clientevendedor.getColaborador() != null){
														if(!vendaService.existeColaborador(listaColaborador, clientevendedor.getColaborador())){														
															listaColaborador.add(clientevendedor.getColaborador());
														}
														if(pedidovenda.getColaborador() != null && clientevendedor.getColaborador().equals(pedidovenda.getColaborador())){
															diferente = Boolean.FALSE;
															listaColaborador = new ArrayList<Colaborador>();
															listaColaborador.add(pedidovenda.getColaborador());
															break;
														}
													}
												}
											}
											
											if(diferente || cliente == null){
												if(!vendaService.existeColaborador(listaColaborador, pedidovenda.getColaborador())){	
													pedidovenda.getColaborador().setVendedorprincipalTrans(true);
													listaColaborador.add(pedidovenda.getColaborador());
												}
											}
											valorprodutogrupo += (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);
											if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){											
												valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
											}
											if(diferente && listaColaborador.size() > 1 ){	
												valorgrupovendedorprincipal = 0.0;
												if(percentualcomissaovendedorprincipal != null && percentualcomissaovendedorprincipal > 0){
													valorgrupovendedorprincipal = valorprodutogrupo * percentualcomissaovendedorprincipal;
													if(valorgrupovendedorprincipal != 0) valorgrupovendedorprincipal = valorgrupovendedorprincipal / 100;
												}
												if(valorcomissionamentogrupovendedorprincipal != null && valorcomissionamentogrupovendedorprincipal > 0) {
													valorgrupovendedorprincipal += valorcomissionamentogrupovendedorprincipal;
												}											
											}
											
											valorgrupo = 0.0;
											if(percentualcomissao != null && percentualcomissao > 0){
												valorgrupo = valorprodutogrupo * percentualcomissao;
												if(valorgrupo != 0)
													valorgrupo = valorgrupo / 100;
											}
											if(valorcomissionamentogrupo != null && valorcomissionamentogrupo > 0) {
												valorgrupo += valorcomissionamentogrupo;
											}
										}else {
											valorprodutogrupo = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
											if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
												valorprodutogrupo = valorprodutogrupo - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
											}
											if(percentualcomissao != null && percentualcomissao > 0){
												valorgrupo = valorprodutogrupo * percentualcomissao;
												if(valorgrupo != 0)
													valorgrupo = valorgrupo / 100;
											}
											if(valorcomissionamentogrupo != null && valorcomissionamentogrupo > 0) {
												valorgrupo += valorcomissionamentogrupo;
											}
											if(!vendaService.existeColaborador(listaColaborador, pedidovenda.getColaborador())){														
												listaColaborador.add(pedidovenda.getColaborador());
											}
										}
										
										if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
											for(Documentocomissao dc : listaDocumentocomissao){
												documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
											}
										}
										
										for(Colaborador colaborador : listaColaborador){
											if(valorgrupo != null && valorgrupo != 0 && (!colaborador.isVendedorprincipalTrans() || listaColaborador.size() == 1)){
												documentocomissao = new Documentocomissao();
												documentocomissao.setPessoa(new Pessoa(colaborador.getCdpessoa()));
												documentocomissao.setValorcomissao( new Money(valorgrupo));
												documentocomissao.setPedidovenda(pedidovenda);
												documentocomissao.setRepresentacao(true);
												if(venda != null && venda.getCdvenda() != null)
													documentocomissao.setVenda(venda);
												listaComissao.add(documentocomissao);
											}else if(valorgrupovendedorprincipal != null && valorgrupovendedorprincipal != 0 && colaborador.isVendedorprincipalTrans()){
												documentocomissao = new Documentocomissao();
												documentocomissao.setPessoa(new Pessoa(colaborador.getCdpessoa()));
												documentocomissao.setValorcomissao( new Money(valorgrupovendedorprincipal));
												documentocomissao.setPedidovenda(pedidovenda);
												documentocomissao.setRepresentacao(true);
												if(venda != null && venda.getCdvenda() != null)
													documentocomissao.setVenda(venda);
												listaComissao.add(documentocomissao);
											}
										}
										
									}
								}
								
								if(vm.getMaterial() != null && vm.getMaterial().getListaMaterialcolaborador() != null && 
										!vm.getMaterial().getListaMaterialcolaborador().isEmpty() && vm.getPreco() != null){
									for(Materialcolaborador mc : vm.getMaterial().getListaMaterialcolaborador()){
										if(mc.getColaborador() != null && mc.getColaborador().equals(pedidovenda.getColaborador()) && 
												pedidovenda.getDocumentotipo() != null && mc.getDocumentotipo() != null &&
												pedidovenda.getDocumentotipo().equals(mc.getDocumentotipo())){
											if(documentocomissaoByMaterial == null){
												documentocomissaoByMaterial = new Documentocomissao();
											}
											documentocomissaoByMaterial.setColaborador(pedidovenda.getColaborador());
											if(mc.getPercentual() != null){
												valortotalmaterial = (qtdeMaterial*vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
												if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
													valortotalmaterial = valortotalmaterial - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
												}
												valortotalmaterial = valortotalmaterial * mc.getPercentual();
												valor += valortotalmaterial;
											} else if(mc.getValor() != null){
												valor += mc.getValor().getValue().doubleValue()*100;
											}
											achou = true;
											comissaogrupomaterial = false;
										}
									}
									
									if(!achou){
										for(Materialcolaborador mc : vm.getMaterial().getListaMaterialcolaborador()){
											if(mc.getColaborador() == null && pedidovenda.getDocumentotipo() != null && mc.getDocumentotipo() != null &&
													pedidovenda.getDocumentotipo().equals(mc.getDocumentotipo())){									
												if(documentocomissaoByMaterial == null){
													documentocomissaoByMaterial = new Documentocomissao();
												}
												documentocomissaoByMaterial.setColaborador(pedidovenda.getColaborador());
												if(mc.getPercentual() != null){
													valortotalmaterial = (qtdeMaterial*vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
													if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
														valortotalmaterial = valortotalmaterial - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
													}
													valortotalmaterial = valortotalmaterial * mc.getPercentual();
													valor += valortotalmaterial;
												}
												else if(mc.getValor() != null)
													valor += mc.getValor().getValue().doubleValue()*100;
												comissaogrupomaterial = false;
												achou = true;
											}
										}
									}
									
									if(!achou){
										for(Materialcolaborador mc : vm.getMaterial().getListaMaterialcolaborador()){
											if(mc.getColaborador() != null && mc.getDocumentotipo() == null && mc.getColaborador().equals(pedidovenda.getColaborador())){									
												if(documentocomissaoByMaterial == null){
													documentocomissaoByMaterial = new Documentocomissao();
												}
												documentocomissaoByMaterial.setColaborador(pedidovenda.getColaborador());
												if(mc.getPercentual() != null){
													valortotalmaterial = (qtdeMaterial*vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
													if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
														valortotalmaterial = valortotalmaterial - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
													}
													valortotalmaterial = valortotalmaterial * mc.getPercentual();
													valor += valortotalmaterial;
												} else if(mc.getValor() != null)
													valor += mc.getValor().getValue().doubleValue()*100;
												comissaogrupomaterial = false;
												achou = true;
											}
										}
									}
									
									if(!achou){
										for(Materialcolaborador mc : vm.getMaterial().getListaMaterialcolaborador()){
											if(mc.getColaborador() == null && mc.getDocumentotipo() == null){									
												if(documentocomissaoByMaterial == null){
													documentocomissaoByMaterial = new Documentocomissao();
												}
												documentocomissaoByMaterial.setColaborador(pedidovenda.getColaborador());
												if(mc.getPercentual() != null){
													valortotalmaterial = (qtdeMaterial*vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
													if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
														valortotalmaterial = valortotalmaterial - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
													}
													valortotalmaterial = valortotalmaterial * mc.getPercentual();
													valor += valortotalmaterial;
												} else if(mc.getValor() != null)
													valor += mc.getValor().getValue().doubleValue()*100;
												
												comissaogrupomaterial = false;
												achou = true;
											}
										}
									}
									
									achou = false;
								}
								
								if(fornecedoragencia != null && vm.getPercentualcomissaoagencia() != null && vm.getPercentualcomissaoagencia() > 0){
									valorprodutoFornecedoragencia = (qtdeMaterial * vm.getPreco()) - (vm.getDesconto() != null ? vm.getDesconto().getValue().doubleValue() : 0.0);;
									if(pedidovenda.getDesconto() != null && pedidovenda.getDesconto().getValue().doubleValue() > 0){
										valorprodutoFornecedoragencia = valorprodutoFornecedoragencia - valorDescontoProporcional(pedidovenda.getDesconto(), vm.getPreco(), vm.getDesconto(), qtdeMaterial, pedidovenda.getListaPedidovendamaterial());
									}
									valorprodutoFornecedoragencia = valorprodutoFornecedoragencia * vm.getPercentualcomissaoagencia();
									if(valorprodutoFornecedoragencia != 0){
										valorprodutoFornecedoragencia = valorprodutoFornecedoragencia / 100;
									}
									
									if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
										for(Documentocomissao dc : listaDocumentocomissao){
											documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
										}
									}
									
									if(valorprodutoFornecedoragencia != null && valorprodutoFornecedoragencia != 0){
										documentocomissao = new Documentocomissao();
										documentocomissao.setPessoa(new Pessoa(fornecedoragencia.getCdpessoa()));
										documentocomissao.setValorcomissao( new Money(valorprodutoFornecedoragencia));
										documentocomissao.setPedidovenda(pedidovenda);
										documentocomissao.setRepresentacao(true);
										if(venda != null && venda.getCdvenda() != null)
											documentocomissao.setVenda(venda);
										documentocomissao.setAgencia(true);
										listaComissaoFornecedoragencia.add(documentocomissao);
									}
								}
							}
						}
						
						for(Colaborador colaborador : listaColaborador){
							Money valorComissao = new Money(0.0);
							for(Documentocomissao dc : listaComissao){
								if(colaborador.getCdpessoa().equals(dc.getPessoa().getCdpessoa())){
									valorComissao = valorComissao.add(dc.getValorcomissao());
								}
							}
							if(comissaogrupomaterial && valor != null && valor > 0 && documentocomissaoByMaterial != null && documentocomissaoByMaterial.getColaborador() != null){
								if(documentocomissaoByMaterial.getColaborador().equals(colaborador)){
									valorComissao = valorComissao.add(new Money(valor / 100));
									valor = 0d;
									documentocomissaoByMaterial = null;
								}								
							}
							
							Documentocomissao documentocomissaoColaborador = new Documentocomissao();
							documentocomissaoColaborador.setPessoa(new Pessoa(colaborador.getCdpessoa()));
							documentocomissaoColaborador.setValorcomissao(valorComissao);
							documentocomissaoColaborador.setPedidovenda(pedidovenda);
							documentocomissaoColaborador.setRepresentacao(true);
							if(venda != null && venda.getCdvenda() != null)
								documentocomissao.setVenda(venda);
							documentocomissaoService.saveOrUpdate(documentocomissaoColaborador);
						}
						
						if(fornecedoragencia != null && listaComissaoFornecedoragencia != null && !listaComissaoFornecedoragencia.isEmpty()){
							Money valorComissaoFornecedoragencia = new Money(0.0);
							for(Documentocomissao dc : listaComissaoFornecedoragencia){
								if(fornecedoragencia.getCdpessoa().equals(dc.getPessoa().getCdpessoa())){
									valorComissaoFornecedoragencia = valorComissaoFornecedoragencia.add(dc.getValorcomissao());
								}
							}
							
							Documentocomissao documentocomissaoFornecedoragencia = new Documentocomissao();
							documentocomissaoFornecedoragencia.setPessoa(new Pessoa(fornecedoragencia.getCdpessoa()));
							documentocomissaoFornecedoragencia.setValorcomissao(valorComissaoFornecedoragencia);
							documentocomissaoFornecedoragencia.setPedidovenda(pedidovenda);
							documentocomissaoFornecedoragencia.setRepresentacao(true);
							if(venda != null && venda.getCdvenda() != null)
								documentocomissao.setVenda(venda);
							documentocomissaoFornecedoragencia.setAgencia(true);
							documentocomissaoService.saveOrUpdate(documentocomissaoFornecedoragencia);
						}
							
						if(!comissaogrupomaterial && valor != null && valor > 0 && documentocomissaoByMaterial != null && documentocomissaoByMaterial.getColaborador() != null){
							if(listaDocumentocomissao != null && !listaDocumentocomissao.isEmpty()){
								for(Documentocomissao dc : listaDocumentocomissao){
									documentocomissaovendaService.deleteDocumentocomissaovenda(dc);
								}
							}
							
							documentocomissaoByMaterial.setPessoa(new Pessoa(pedidovenda.getColaborador().getCdpessoa()));
							documentocomissaoByMaterial.setValorcomissao( new Money(valor / 100));
							documentocomissaoByMaterial.setPedidovenda(pedidovenda);
							documentocomissaoByMaterial.setRepresentacao(true);
							if(venda != null && venda.getCdvenda() != null)
								documentocomissao.setVenda(venda);
							documentocomissaoService.saveOrUpdate(documentocomissaoByMaterial);
							
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que retorna se � a primeira venda do cliente e do grupo de material
	 *
	 * @param cliente
	 * @param materialgrupo
	 * @param empresa
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isPrimeiraVendaGrupoCliente(Cliente cliente, Materialgrupo materialgrupo, Empresa empresa, Pedidovenda pedidovenda) {
		boolean retornopedidovenda = false;
		boolean retornovenda = false;
		
		if(cliente != null && materialgrupo != null){
			retornopedidovenda = !this.existPedidovendaByClienteMaterialgrupo(cliente, materialgrupo, empresa, pedidovenda);
			retornovenda = !vendaService.existVendaByClienteMaterialgrupo(cliente, materialgrupo, empresa, null, pedidovenda);
		}
			
		return retornovenda && retornopedidovenda;
	}
	
	/**
	 * M�todo que retorna se � a primeira venda do cliente
	 *
	 * @param cliente
	 * @param empresa
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isPrimeiraVendaCliente(Cliente cliente, Empresa empresa, Pedidovenda pedidovenda) {
		boolean retorno = false;
		
		if(cliente != null)
			retorno = !this.existPedidovendaByClienteMaterialgrupo(cliente, null, empresa, pedidovenda);
		
		return retorno;
	}
	
	/**
	 * M�todo que verifica se o pre�o do item est� acima do valor de venda
	 *
	 * @param material
	 * @param pedidovenda
	 * @param vendamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isAcimaValorvenda(Material material, Pedidovenda pedidovenda, Pedidovendamaterial vendamaterial) {
		boolean retorno = false;
		
		if(material != null && pedidovenda != null && vendamaterial != null && vendamaterial.getPreco() != null){
			Material mat = new Material();
			mat.setCdmaterial(material.getCdmaterial());
			mat.setValorvenda(material.getValorvenda());
			
			this.preencheValorVendaComTabelaPreco(mat, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), vendamaterial.getUnidademedida());
			
			if(mat.getValorvenda() != null){
				if(vendamaterial.getPreco() > mat.getValorvenda())
					retorno = true;
			}
		}
		return retorno;
	}
	/**
	* M�todo que verifica se o pre�o do item est� acima do valor de venda
	*
	* @param material
	* @param pedidovenda
	* @param vendamaterial
	* @return
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	public boolean isIgualValorvenda(Material material, Pedidovenda pedidovenda, Pedidovendamaterial vendamaterial) {
		boolean retorno = false;
		
		if(material != null && pedidovenda != null && vendamaterial != null && vendamaterial.getPreco() != null){
			Material mat = new Material();
			mat.setCdmaterial(material.getCdmaterial());
			mat.setValorvenda(material.getValorvenda());
			
			this.preencheValorVendaComTabelaPreco(mat, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), vendamaterial.getUnidademedida());
			
			if(mat.getValorvenda() != null){
				if(vendamaterial.getPreco().compareTo(mat.getValorvenda()) == 0)
					retorno = true;
			}
		}
		return retorno;
	}
	
	/**
	 * M�todo que verifica se o pre�o do item est� abaixo do valor de venda
	 *
	 * @param material
	 * @param pedidovenda
	 * @param pedidovendamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean isAbaixoValorvenda(Material material, Pedidovenda pedidovenda, Pedidovendamaterial pedidovendamaterial) {
		boolean retorno = false;
		
		if(material != null && pedidovenda != null && pedidovendamaterial != null && pedidovendamaterial.getPreco() != null){
			Material mat = new Material();
			mat.setCdmaterial(material.getCdmaterial());
			mat.setValorvenda(material.getValorvenda());
			
			this.preencheValorVendaComTabelaPreco(mat, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
			
			if(mat.getValorvenda() != null){
				if(pedidovendamaterial.getPreco() < mat.getValorvenda())
					retorno = true;
			}
		}
		
		return retorno;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#existPedidovendaByClienteMaterialgrupo(Cliente cliente, Materialgrupo materialgrupo, Empresa empresa, Pedidovenda pedidovenda)
	 *
	 * @param cliente
	 * @param materialgrupo
	 * @param empresa
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existPedidovendaByClienteMaterialgrupo(Cliente cliente, Materialgrupo materialgrupo, Empresa empresa, Pedidovenda pedidovenda) {
		return pedidovendaDAO.existPedidovendaByClienteMaterialgrupo(cliente, materialgrupo, empresa, pedidovenda);
	}
	
	/**
	 * M�todo que preenche o valor de venda do material 
	 *
	 * @param material
	 * @param cliente
	 * @param prazopagamento
	 * @param pedidovendatipo
	 * @author Luiz Fernando
	 */
	public void preencheValorVendaComTabelaPreco(Material material, Cliente cliente, Prazopagamento prazopagamento, Pedidovendatipo pedidovendatipo, Empresa empresa, Unidademedida unidademedida) {
		Double valorvenda = material.getValorvenda();
		Double valortabelapreco = null;
		Materialtabelaprecoitem materialtabelaprecoitem = null;
		Double valorMaximoTabela = null;
		Double valorMinimoTabela = null;
		String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
		Integer numCasasDecimais = null; 
		if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
			String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
			if(aux != null && !aux.trim().isEmpty()){
				try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
			}
		}
		Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(material, cliente, prazopagamento, pedidovendatipo, empresa);
		if(materialtabelapreco != null){
			if(valorvenda != null){
				if(materialtabelapreco.getDescontopadrao() != null){
					valortabelapreco = valorvenda - (valorvenda*materialtabelapreco.getDescontopadrao()/100);
				}
				if(materialtabelapreco.getAcrescimopadrao() != null){
					valortabelapreco = valorvenda + (valorvenda*materialtabelapreco.getAcrescimopadrao()/100);
				}
			}
		}else if(materialtabelaprecoitem == null){
			materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(material, cliente, prazopagamento, pedidovendatipo, empresa, unidademedida);
			if(materialtabelaprecoitem != null){
				valortabelapreco = materialtabelaprecoitem.getValorcomtaxa(valorvenda, numCasasDecimais);
				valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
				valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
			}
		}
		if(valortabelapreco != null){
			valorvenda = valortabelapreco;
		}
		
		Double valorMaximo = material.getValorvendamaximo();
		Double valorMinimo = material.getValorvendaminimo();
		if(valorMaximoTabela != null){
			valorMaximo = valorMaximoTabela;
		}
		if(valorMinimoTabela != null){
			valorMinimo = valorMinimoTabela;
		}
		
		if(numCasasDecimais != null && numCasasDecimais > 0){
			valorvenda = SinedUtil.round(valorvenda, numCasasDecimais);
		}
		material.setValorvenda(valorvenda);
		material.setValorvendamaximo(valorMaximo);
		material.setValorvendaminimo(valorMinimo);
	}
	
	/**
	 * C�lcula o percentual do desconto/juros da conta a receber
	 *
	 * @param descontojuros
	 * @param preco
	 * @param quantidade
	 * @param listavendamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Double valorDescontojurosProporcional(Money descontojuros, Double preco, Double quantidade, List<Pedidovendamaterial> listaPedidovendamaterial){
		Double valordesconto = 0d;
		Double percentual = 0d;
		Money valortotal = new Money(0);
		
		if(quantidade == null) quantidade = 1.0;		
		
		if(listaPedidovendamaterial != null && !listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
				valortotal = valortotal.add(new Money(pedidovendamaterial.getPreco().doubleValue() * (pedidovendamaterial.getQuantidade() == null || pedidovendamaterial.getQuantidade() == 0 ? 1 : pedidovendamaterial.getQuantidade())));
			}
		}
		
		if(valortotal != null && preco != null && descontojuros != null){
			percentual = ((preco * quantidade) * 100) / valortotal.getValue().doubleValue();
			valordesconto = (percentual/100) * descontojuros.getValue().doubleValue();
		}
		return valordesconto;		
	}
	
	public Double valorDescontoProporcional(Money desconto, Double preco, Money descontoItem, Double quantidade, List<Pedidovendamaterial> listaPedidovendamaterial){
		Double valordesconto = 0d;
		Double percentual = 0d;
		Money valortotal = new Money(0);
		
		if(quantidade == null) quantidade = 1.0;		
		
		if(listaPedidovendamaterial != null && !listaPedidovendamaterial.isEmpty()){
			for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
				double qtde = pedidovendamaterial.getQuantidade() == null || pedidovendamaterial.getQuantidade() == 0 ? 1 : pedidovendamaterial.getQuantidade();
				valortotal = valortotal.add(new Money(pedidovendamaterial.getPreco().doubleValue() * qtde));
				if(pedidovendamaterial.getDesconto() != null){
					valortotal = valortotal.subtract(pedidovendamaterial.getDesconto());
				}
			}
		}
		
		if(valortotal != null && preco != null && desconto != null){
			percentual = ((preco * quantidade - (descontoItem != null ? descontoItem.getValue().doubleValue() : 0d)) * 100) / valortotal.getValue().doubleValue();
			valordesconto = (percentual/100) * desconto.getValue().doubleValue();
		}
		return valordesconto;		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForComissiaoPedidovendaTipoReserva(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovenda findForComissiaoPedidovendaTipoReserva(Pedidovenda pedidovenda) {
		return pedidovendaDAO.findForComissiaoPedidovendaTipoReserva(pedidovenda);
	}
	/**
	 * Atualiza a mensagem de erro de um pedido de venda.
	 *
	 * @since 12/03/2012
	 * @author Igor Silv�rio Costa
	 */
	public void updateMsgErro(Pedidovenda form, String message) {
		pedidovendaDAO.updateMsgErro(form, message);
	}
	
		/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateOrdemcompra(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public void updateOrdemcompra(Pedidovenda pedidovenda) {
		pedidovendaDAO.updateOrdemcompra(pedidovenda);
	}
	
	/**
	 * M�todo que retorna a lista com os pedidos de venda material que tenham integra��o com o wms
	 * de acordo com a empresa
	 *
	 * @param lista
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendamaterial> getListaIntegracaoWms(List<Pedidovendamaterial> lista){
		List<Pedidovendamaterial> listaIntegracao = new ArrayList<Pedidovendamaterial>();
		if(lista != null && !lista.isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : lista){
				Pedidovenda pedidovenda = pedidovendamaterial.getPedidovenda();
				if(pedidovenda != null && 
						pedidovenda.getPedidovendatipo() != null && 
						pedidovenda.getPedidovendatipo().getSincronizarComWMS() != null && 
						pedidovenda.getPedidovendatipo().getSincronizarComWMS() && 
						empresaService.isIntegracaoWms(pedidovenda.getEmpresa())){					
					listaIntegracao.add(pedidovendamaterial);
				}
			}
		}
		
		return listaIntegracao;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 24/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdePrevistos() {
		return pedidovendaDAO.getQtdePrevistos();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService.updatePedidoAguardandoAprovacaoRestricaocliente(String whereIn)
	 * @see br.com.linkcom.sined.geral.service.PedidovendasincronizacaoService#criarPedidovendasincronizacaoByWhereIn(String whereIn, boolean reenvio)
	 *
	 * @param cliente
	 * @author Luiz Fernando
	 */
	public void verificaPedidovendaComRestricaocliente(Cliente cliente) {
		if(cliente != null && cliente.getCdpessoa() != null){
			if(!restricaoService.verificaRestricaoSemDtLiberacao(cliente)){
				String whereIn = findForRestricaocliente(cliente);
				if(whereIn != null && !"".equals(whereIn)){
					updatePedidoAguardandoAprovacaoRestricaocliente(whereIn);
					pedidovendasincronizacaoService.criarPedidovendasincronizacaoByWhereIn(whereIn, "Pedido de venda liberado para sincroniza��o ap�s restri��o cliente ");
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForRestricaocliente(Cliente cliente)
	 *
	 * @param cliente
	 * @return
	 * @author Luiz Fernando
	 */
	public String findForRestricaocliente(Cliente cliente) {
		return pedidovendaDAO.findForRestricaocliente(cliente);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#updatePedidoAguardandoAprovacaoRestricaocliente(String whereIn)
	 *
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	protected void updatePedidoAguardandoAprovacaoRestricaocliente(String whereIn) {
		pedidovendaDAO.updatePedidoAguardandoAprovacaoRestricaocliente(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForSincronizacaowms(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForSincronizacaowms(String whereIn) {
		return pedidovendaDAO.findForSincronizacaowms(whereIn);
	}
	
	public List<Pedidovenda> findForEnvioECF(String whereIn) {
		return pedidovendaDAO.findForEnvioECF(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Pedidovenda> findAllNaoConfirmadosByClienteOrderbyData(Cliente cliente, Empresa empresa){
		Boolean isRestricaoClienteVendedor = usuarioService.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		Boolean isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(SinedUtil.getUsuarioLogado());
		return pedidovendaDAO.findAllNaoConfirmadosByClienteOrderbyData(cliente, isRestricaoClienteVendedor, isRestricaoVendaVendedor, empresa);	
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadWithCliente(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovenda loadWithCliente(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadWithCliente(pedidovenda);
	}
	
	public Pedidovenda loadWithPedidovendatipo(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadWithPedidovendatipo(pedidovenda);
	}
	
	/**
	 * M�todo que cria o resource do comprovante de pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 * @param comprovante 
	 */
	public Resource criaComprovantepedidovendaForEnviaremail(Pedidovenda pedidovenda, ComprovanteConfiguravel comprovante) throws Exception {
		
		String nomeArquivo = "comprovante_pedidovenda_"+SinedUtil.datePatternForReport();
		
		if(comprovante != null && comprovante.getLayout() != null && (comprovante.getTxt() == null || !comprovante.getTxt())){
			String comprovanteHTML = this.gerarComprovanteConfiguravelOnly(comprovante, pedidovenda);
			comprovanteHTML = SinedUtil.criarUrlsPublicas(comprovanteHTML);
			
			return new WKHTMLBuilder()
							.HTML(comprovanteHTML)
							.resource(nomeArquivo+ ".pdf");
			
		} else {
			MergeReport mergeReport = new MergeReport(nomeArquivo+".pdf");
			
			Report report = criaComprovantePedidovendaReport(pedidovenda);
			mergeReport.addReport(report);
			
			return mergeReport.generateResource();
		}
	}
	
	/**
	 * M�todo envia comprovante de pedido de venda para o fornecedor
	 *
	 * @param request
	 * @param pedidovenda
	 * @param empresa
	 * @param fornecedor
	 * @param report
	 * @return
	 * @author Luiz Fernando
	 * @param extensao 
	 * @param mimeType 
	 */
	public Boolean enviaComprovanteEmailRepresentante(WebRequestContext request, Pedidovenda pedidovenda, Empresa empresa, Fornecedor fornecedor, Resource report) {		
		if(empresa == null || report == null || fornecedor == null){
			request.addError("Fornecedor/Empresa n�o pode ser nulo.");
			return false;
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(pedidovenda.getColaborador() != null && pedidovenda.getColaborador().getCdpessoa() != null){
			usuario = new Usuario(pedidovenda.getColaborador().getCdpessoa());
		}
		usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.email");
		
		String template = null;
		try{
			TemplateManager templateAux = new TemplateManager("/WEB-INF/template/templateEnvioEmailComprovantevenda.tpl");				
			template = templateAux
						.assign("msgcomprovantepedidovenda", "o pedido de venda")
						.assign("numero", pedidovenda.getCdpedidovenda().toString())
						.assign("data", new SimpleDateFormat("dd/MM/yyyy").format(pedidovenda.getDtpedidovenda()))
						.assign("empresa", empresa.getRazaosocialOuNome())
						.getTemplate();
			
		} catch (IOException e) {
			request.addError(e.getMessage());
			request.addError("Falha na obten��o do template.");
			return false;
		}
		
		if(fornecedor.getEmail() == null || fornecedor.getEmail().equals("")){
			request.addError("Fornecedor " + fornecedor.getNome() + " sem e-mail cadastrado.");
			return false;
		}	
		
		try {
			EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			email
				.setFrom((usuario != null && usuario.getEmail() != null ? usuario.getEmail() : pedidovenda.getEmpresa().getEmail()))
				.setSubject("Comprovante de Pedido de Venda")
				.setTo(fornecedor.getEmail())
				.attachFileUsingByteArray(report.getContents(), report.getFileName(), report.getContentType(), "comprovante_pedidovenda_"+pedidovenda.getCdpedidovenda())
				.addHtmlText(template + usuarioService.addImagemassinaturaemailUsuario(usuario, email))
				.sendMessage();
			
			return true;
		} catch (Exception e) {
			request.addError(e.getMessage());
			request.addError("Falha no envio de e-mail do fornecedor " + fornecedor.getNome() + ".");
			return false;
		}
	}
	
	/**
	 * M�todo que verifica se existe fornecedor exclusivo no material
	 *
	 * @param listaPedidovendamaterial
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existMaterialFornecedorExclusivo(List<Pedidovendamaterial> listaPedidovendamaterial, Fornecedor fornecedor){
		Boolean exist = Boolean.FALSE;
		
		if(listaPedidovendamaterial != null && !listaPedidovendamaterial.isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial){
				if(pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getListaFornecedor() != null &&  
						!pedidovendamaterial.getMaterial().getListaFornecedor().isEmpty()){
					for(Materialfornecedor materialfornecedor : pedidovendamaterial.getMaterial().getListaFornecedor()){
						if(materialfornecedor.getFornecedor() != null && materialfornecedor.getFornecedor().equals(fornecedor)){
							return Boolean.TRUE;
						}
					}
				}
			}
		}
		return exist;
	}
	
	protected void calculaSaldoMaterialPorValorvenda(Pedidovendamaterial pedidovendamaterial, Double preco, Double valorvenda, Double qtde, Money desconto, Boolean desconsiderardescontosaldoproduto, Double multiplicador) {
		Double saldo = 0.0;
		Double desc = 0.0;
		if(desconto != null) desc = desconto.getValue().doubleValue();
		
		multiplicador = multiplicador != null ? multiplicador : 1d;
		qtde = qtde * multiplicador;
		
		if(pedidovendamaterial != null && preco != null && qtde != null && qtde > 0 && valorvenda != null){
			if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
				saldo = (preco - valorvenda) * qtde;
			}else {
				saldo = ((preco - (desc/qtde)) - valorvenda) * qtde;
			}
			pedidovendamaterial.setSaldo(saldo != null ? new Money(saldo) : new Money());
		}
	}
	
	protected void calculaSaldoMaterial(Pedidovendamaterial pedidovendamaterial, Double preco, Double valorvendaminimo, Double valorvendamaximo, Double qtde, Money desconto, Boolean desconsiderardescontosaldoproduto, Double multiplicador) {
		Double saldo = 0.0;
		Double desc = 0.0;
		if(desconto != null) desc = desconto.getValue().doubleValue();
		
		multiplicador = multiplicador != null ? multiplicador : 1d;
		qtde = qtde * multiplicador;
		
		if(pedidovendamaterial != null && preco != null && qtde != null){
			if(valorvendamaximo != null && preco > valorvendamaximo){
				if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
					saldo = (preco - valorvendamaximo) * qtde;
				}else {
					saldo = ((preco - (desc/qtde)) - valorvendamaximo) * qtde;
				}
			}else if(valorvendaminimo != null && preco < valorvendaminimo){
				if(desconsiderardescontosaldoproduto != null && desconsiderardescontosaldoproduto){
					saldo = (preco - valorvendaminimo) * qtde;
				}else {
					saldo = ((preco - (desc/qtde)) - valorvendaminimo) * qtde;
				}
			}
			pedidovendamaterial.setSaldo(saldo != null ? new Money(saldo) : new Money());
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#getUltimoidentificador(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public String getUltimoidentificador(Empresa empresa) {
		return pedidovendaDAO.getUltimoidentificador(empresa);
	}
	
	/**
	 * M�todo que retorno o �ltimo identificador do pedido de venda. Caso o campo n�o contenha letra, retorna null
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getUltimoidentificadorInteger(Empresa empresa) {
		try {
			String identificadorstr = this.getUltimoidentificador(empresa);
			return Integer.parseInt(identificadorstr);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	
	/**
	 * M�todo que copia o pedido de venda
	 *
	 * @param origem
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovenda criarCopia(Pedidovenda origem) {
		origem = loadForEntrada(origem);
		origem.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(origem, true));
		origem.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(origem));
		origem.setListaPedidovendavalorcampoextra(pedidovendavalorcampoextraService.findByPedidoVenda(origem));
		
		Pedidovenda copia = new Pedidovenda();
		copia = origem;
		copia.setVendaorcamento(null);
		copia.setCdpedidovenda(null);
		copia.setDtpedidovenda(new java.sql.Date(System.currentTimeMillis()));
		copia.setPedidovendasituacao(null);
		copia.setIdentificador(null);
		copia.setIdentificadorcarregamento(null);
		copia.setListaPedidovendapagamento(null);
		copia.setListaPedidovendapagamentorepresentacao(null);
		copia.setValorbrutorepresentacao(origem.getValorbrutorepresentacao());
		copia.setValordescontorepresentacao(origem.getValordescontorepresentacao());
		copia.setProtocolooffline(null);
		copia.setLimitecreditoexcedido(null);
		copia.setGarantiareformaitem(null);
		copia.setDescontogarantiareforma(null);
		copia.setConta(origem.getConta());
		copia.setOrigemOtr(origem.getOrigemOtr());
		copia.setEnderecoFaturamento(origem.getEnderecoFaturamento());
		if(copia.getProjeto() != null && copia.getProjeto().getCdprojeto() != null){
			Projeto projetoCopiado = projetoService.load(copia.getProjeto());
			if(Situacaoprojeto.CANCELADO.equals(projetoCopiado.getSituacao()) || Situacaoprojeto.CONCLUIDO.equals(projetoCopiado.getSituacao())){
				copia.setProjeto(null);
			}
		}
		
		Map<String, String> mapaIdentificador = new HashMap<String, String>();
		if(origem.getListaPedidovendamaterialmestre() != null && !origem.getListaPedidovendamaterialmestre().isEmpty()){
			Integer i = 0; 
			for(Pedidovendamaterialmestre item : origem.getListaPedidovendamaterialmestre()){
				item.setCdpedidovendamaterialmestre(null);
				item.setPedidovenda(null);
				if(item.getIdentificadorinterno() != null && !item.getIdentificadorinterno().trim().isEmpty()){		
					String identificador = System.currentTimeMillis() + i.toString() + item.getMaterial().getCdmaterial().toString();
					mapaIdentificador.put(item.getIdentificadorinterno(), identificador);
					item.setIdentificadorinterno(identificador);
					i++;
				}
			}
			copia.setListaPedidovendamaterialmestre(origem.getListaPedidovendamaterialmestre());
		}
		if(origem.getListaPedidovendamaterial() != null && !origem.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial item : origem.getListaPedidovendamaterial()){
				item.setCdpedidovendamaterial(null);
				item.setPedidovenda(null);
				item.setDtprazoentrega(null);
				item.setValorcustomaterial(item.getMaterial().getValorcusto());
				item.setServicoalterado(null);
				item.setBandaalterada(null);
				item.setComissionamento(null);
				if(item.getIdentificadorinterno() != null && !item.getIdentificadorinterno().trim().isEmpty()
						&& mapaIdentificador.containsKey(item.getIdentificadorinterno())){
					item.setIdentificadorinterno(mapaIdentificador.get(item.getIdentificadorinterno()));
				}
				if(item.getPneu() != null && !Boolean.TRUE.equals(origem.getOrigemOtr())){
					item.getPneu().setCdpneu(null);
				}
				
				item.setPercentualcomissaoagencia(null);
				if(origem.getAgencia() != null){
					Materialgrupocomissaovenda mgca = materialgrupocomissaovendaService.getComissaoAgencia(item.getMaterial(), item.getFornecedor(), origem.getAgencia(), origem.getDocumentotipo(), origem.getPedidovendatipo()
									);
					if(mgca != null && mgca.getComissionamento() != null && mgca.getComissionamento().getPercentual() != null){
						item.setPercentualcomissaoagencia(mgca.getComissionamento().getPercentual());
					}
				}
				
				pedidovendamaterialService.setPercentualDesconto(item);
			}
			copia.setListaPedidovendamaterial(origem.getListaPedidovendamaterial());
		}
		if(origem.getListaPedidovendavalorcampoextra() != null && !origem.getListaPedidovendavalorcampoextra().isEmpty()){
			for(Pedidovendavalorcampoextra item : origem.getListaPedidovendavalorcampoextra()){
				item.setCdpedidovendavalorcampoextra(null);
				item.setPedidovenda(null);
			}
			copia.setListaPedidovendavalorcampoextra(origem.getListaPedidovendavalorcampoextra());
		}
		return copia;
	}
	
	public List<Pedidovenda> findByCliente(Cliente cliente) {
		return pedidovendaDAO.findByCliente(cliente);
	}
	
	/**
	 * Gera o relat�rio de custo x venda de pedido de venda
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarRelatorioCustopedidovenda(EmitircustovendaFiltro filtro) {
		Report report = new Report("/faturamento/custopedidovenda");
		Report reportSub = new Report("/faturamento/sub_custopedidovenda");
		Report reportSubPedidoVendamaterial = new Report("/faturamento/sub_custopedidovendamaterial");
		reportSubPedidoVendamaterial.addParameter("calcularmargempor", filtro.getCalcularmargempor() != null ? filtro.getCalcularmargempor() : "Custo");
		reportSub.addSubReport("SUB_CUSTOPEDIDOVENDAMATERIAL", reportSubPedidoVendamaterial);
		report.addSubReport("SUB_CUSTOPEDIDOVENDA", reportSub );
		
		List<Pedidovenda> lista = this.findForEmitircustopedidovenda(filtro);
		
		if (lista == null || lista.size() == 0) 
			throw new SinedException("N�o h� registros para este per�odo de datas. ");
		
		HashMap<Cliente, String> mapClienteVendedorPrincipal = new HashMap<Cliente, String>();
		StringBuilder whereInProduto = new StringBuilder();
		for(Pedidovenda venda : lista){
			if(Boolean.TRUE.equals(filtro.getIsVendedorPrincipal())){
				venda.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(venda.getCliente(), mapClienteVendedorPrincipal));
			}
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				if(venda.getPedidovendatipo() != null && venda.getPedidovendatipo().getBonificacao() != null && 
						venda.getPedidovendatipo().getBonificacao()){
					vendamaterial.setPreco(0d);
				}
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getProduto() != null && 
						vendamaterial.getMaterial().getProduto()){
					if(!"".equals(whereInProduto.toString())) whereInProduto.append(",");
					whereInProduto.append(vendamaterial.getMaterial().getCdmaterial());
				}
			}
		}
		
		List<Produto> listaProdutos = null;
		if(whereInProduto != null && !"".equals(whereInProduto.toString())){
			listaProdutos = produtoService.findForCustovenda(whereInProduto.toString());
			if(listaProdutos != null && !listaProdutos.isEmpty()){
				for(Produto produto : listaProdutos){
					for(Pedidovenda venda : lista){
						for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
							if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null && 
									vendamaterial.getMaterial().getCdmaterial().equals(produto.getCdmaterial())){
								vendamaterial.setProdutotrans(produto);
							}
						}
					}
				}
			}
		}
		
		Double totalcusto = 0.0;
		Double totalvendamaterial = 0.0;
		Double totaldescontoaplicado = 0.0;
		Money totaldescontovenda = new Money();
		Money totaldescontovendaitem = new Money();
		Money totallucrobruto = new Money();
		Double totalvalorprodutos = 0.0;
		Double totalvalorprodutossemdesconto = 0d;
		Double totalvalorvendaprodutos = 0.0;
		Money margemmedia = new Money();
		Money totalvenda = new Money();
		Money valorvalecompraproporcional = new Money();
		Money descontoproporcionalsemvale = new Money(); 
		Money descontoproporcional;
		Boolean conversao;
		boolean considerarvalecompra = filtro.getConsiderarValeCompra() != null ? filtro.getConsiderarValeCompra() : false;
		List<Pedidovenda> listaVendaAjustada = new ArrayList<Pedidovenda>();
		for(Pedidovenda venda : lista){
			List<Pedidovendamaterial> listaVendamaterialAjustada = new ArrayList<Pedidovendamaterial>();
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				if(vendamaterial.getMaterial() != null && !vendaService.verificaFiltroCustovenda(filtro, vendamaterial.getMaterial())){
					continue;
				}
				
				listaVendamaterialAjustada.add(vendamaterial);
				vendamaterial.setCalcularmargempor(filtro.getCalcularmargempor());
				vendamaterial.setConsiderarDesconto(filtro.getConsiderarDescontos() != null ? filtro.getConsiderarDescontos() : false);
				vendamaterial.setConsiderarValeCompra(considerarvalecompra);
				valorvalecompraproporcional = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, null, venda.getValorusadovalecompra(), false);
				descontoproporcionalsemvale = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, venda.getDesconto(), null, false);
				
  			    //Valor proporcional vale compra
				if(valorvalecompraproporcional != null){
					vendamaterial.setValorvalecomprapropocional(valorvalecompraproporcional.getValue().doubleValue());
				}
				
				//Valor proporcional do desconto sem vale compra
				if(descontoproporcionalsemvale != null){
					vendamaterial.setValordescontosemvalecompra(descontoproporcionalsemvale.getValue().doubleValue());
				}
				
				//Caso true, vale compra adicionado para os c�lculos, sen�o o mesmo � retirado enviando null no lugar
				if(considerarvalecompra){
					descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), false);
				}else{
					descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, venda.getDesconto(), null, false);				
				}
				
				if(descontoproporcional != null){
					vendamaterial.setDescontoProporcional(descontoproporcional.getValue().doubleValue());
				}
				
				conversao = (vendamaterial.getMaterial().getUnidademedida() != null && vendamaterial.getUnidademedida() != null &&
						!vendamaterial.getMaterial().getUnidademedida().equals(vendamaterial.getUnidademedida()) &&
						vendamaterial.getFatorconversao() != null);
				
				Double valorcustoproducao = null;
				boolean recapagem = SinedUtil.isRecapagem();
				Material material = materialService.loadMaterialComMaterialproducao(vendamaterial.getMaterial());
				if(material == null){
					material = vendamaterial.getMaterial();
				}
				if(vendamaterial.getMaterial().getProducao() != null && vendamaterial.getMaterial().getProducao() && recapagem){
					if(material != null){
						try{
							material.setProduto_altura(venda.getAlturas());
							material.setProduto_largura(venda.getLarguras());
							material.setQuantidade(venda.getQuantidades());
							
							valorcustoproducao = materialproducaoService.getValorvendaproducao(material);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

				
				if (!Pedidovendasituacao.CONFIRMADO.equals(venda.getPedidovendasituacao()) && valorcustoproducao != null) {
					if (conversao) {
						vendamaterial.setValorcustomaterial(SinedUtil.roundByParametro((valorcustoproducao / vendamaterial.getFatorconversaoQtdereferencia()), 2));
					} else {
						vendamaterial.setValorcustomaterial(valorcustoproducao);
					}					
				} else {
					if (conversao) {
						if (!Pedidovendasituacao.CONFIRMADO.equals(venda.getPedidovendasituacao()) && material != null && material.getValorcusto() != null) {
							vendamaterial.setValorcustomaterial(SinedUtil.roundByParametro((material.getValorcusto() / vendamaterial.getFatorconversaoQtdereferencia()), 2));
						} else if (vendamaterial != null && vendamaterial.getValorcustomaterial() != null) {
							vendamaterial.setValorcustomaterial(SinedUtil.roundByParametro((vendamaterial.getValorcustomaterial() / vendamaterial.getFatorconversaoQtdereferencia()), 2));
						}
					} else if(!Pedidovendasituacao.CONFIRMADO.equals(venda.getPedidovendasituacao())){
						if (material != null && material.getValorcusto() != null) {
							vendamaterial.setValorcustomaterial(material.getValorcusto());
						}
					}
				}

				conversao = conversao && (vendamaterial.getUnidademedida().getCasasdecimaisestoque() != null);
				
				vendamaterial.setPreco(SinedUtil.roundByParametro(vendamaterial.getPreco(), 2));
				if (conversao){
					if(vendamaterial.getValorvendamaterial() != null){
						vendamaterial.setValorvendamaterial(SinedUtil.roundByParametro(vendamaterial.getValorvendamaterial(), 2));
					}else if(vendamaterial.getMaterial().getValorvenda() != null){
						vendamaterial.setValorvendamaterial(SinedUtil.roundByParametro(vendamaterial.getMaterial().getValorvenda() / vendamaterial.getFatorconversaoQtdereferencia(), 2));
					}
				}
				
				vendamaterial.setConsiderarMultiplicadorCusto(!parametrogeralService.getBoolean("DESCONSIDERAR_MULTIPLICADOR_CUSTO"));
				Double valorcustoRelatorio = vendamaterial.getValorcustoRelatorio();
				
				if(valorcustoRelatorio != null && !Double.isInfinite(valorcustoRelatorio) && vendamaterial.getQuantidade() != null){
					if (conversao) {
						totalcusto += SinedUtil.roundByParametro(valorcustoRelatorio*vendamaterial.getQuantidade(), 2);
					} else { 
						totalcusto += valorcustoRelatorio*vendamaterial.getQuantidade();
					}
				}
				Money totalvalorvendaRelatorio = new Money();
				Double valorvendaRelatorio = vendamaterial.getValorvendaRelatorio();
				if(valorvendaRelatorio != null && !Double.isInfinite(valorvendaRelatorio) && vendamaterial.getQuantidadeRelatorio() != null){
					if (conversao) {
						totalvalorvendaRelatorio = new Money(SinedUtil.roundByParametro(valorvendaRelatorio*vendamaterial.getQuantidadeRelatorio(), 2));
					} else { 
						totalvalorvendaRelatorio = new Money(valorvendaRelatorio*vendamaterial.getQuantidadeRelatorio());
					}
				}
				Double valorprecoRelatorio = vendamaterial.getPrecoRelatorio();
				if(valorprecoRelatorio != null && !Double.isInfinite(valorprecoRelatorio) && vendamaterial.getQuantidadeRelatorio() != null){
					if (conversao) {
						totalvendamaterial += SinedUtil.roundByParametro(valorprecoRelatorio*vendamaterial.getQuantidadeRelatorio(), 2);
					} else { 
						totalvendamaterial += valorprecoRelatorio*vendamaterial.getQuantidadeRelatorio();
					}
				}
				if(vendamaterial.getPercentualdescontocustopedidovenda() != null){
					totaldescontoaplicado += vendamaterial.getPercentualdescontocustopedidovenda();
				}
				if(vendamaterial.getConsiderarDesconto() != null && vendamaterial.getConsiderarDesconto() 
						&& vendamaterial.getDescontoProporcional() != null){
						totaldescontovenda = totaldescontovenda.add(new Money(vendamaterial.getDescontoProporcional()));
				}
				if(vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra() 
							&& vendamaterial.getValorvalecomprapropocional() != null){
						totaldescontovenda = totaldescontovenda.add(new Money(vendamaterial.getValorvalecomprapropocional()));
				}
				if(vendamaterial.getConsiderarDesconto() != null && vendamaterial.getConsiderarDesconto() 
						&& vendamaterial.getValordescontosemvalecompra() != null){
					totaldescontovendaitem = totaldescontovendaitem.add(new Money(vendamaterial.getValordescontosemvalecompra()));
				}
				if(vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra() 
						&& vendamaterial.getValorvalecomprapropocional() != null){
					totaldescontovenda = totaldescontovenda.add(new Money(vendamaterial.getValorvalecomprapropocional()));
				}
				if(valorvalecompraproporcional != null && filtro.getConsiderarValeCompra() != null && filtro.getConsiderarValeCompra()){
					totaldescontovendaitem = totaldescontovendaitem.add(valorvalecompraproporcional);
				}
				if(vendamaterial.getPreco() != null && vendamaterial.getQuantidade() != null){
					vendamaterial.setTotalprodutoReport(new Money(valorprecoRelatorio * vendamaterial.getQuantidadeRelatorio()).subtract(vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() : new Money()));
					
					if(filtro.getConsiderarDescontos() != null && filtro.getConsiderarDescontos() && vendamaterial.getDescontoProporcional() != null && 
							vendamaterial.getDescontoProporcional() > 0){
						vendamaterial.setTotalprodutoReport(vendamaterial.getTotalprodutoReport().subtract(new Money(vendamaterial.getDescontoProporcional())));
					}
					
					if(filtro.getConsiderarValeCompra() != null && filtro.getConsiderarValeCompra() 
							&& vendamaterial.getValorvalecomprapropocional() != null && vendamaterial.getValorvalecomprapropocional() > 0 &&
							(filtro.getConsiderarDescontos() == null || !filtro.getConsiderarDescontos())){
						vendamaterial.setTotalprodutoReport(vendamaterial.getTotalprodutoReport().subtract(new Money(vendamaterial.getValorvalecomprapropocional())));
					}
					
					if (conversao){
						vendamaterial.setTotalprodutoReport(new Money(SinedUtil.roundByParametro(vendamaterial.getTotalprodutoReport().getValue().doubleValue(), 2)));
					}
					vendamaterial.setTotalprodutoDescontoReport(new Money(valorprecoRelatorio * vendamaterial.getQuantidadeRelatorio()));
					
					if (conversao){						
						vendamaterial.setTotalprodutoDescontoReport(new Money(SinedUtil.roundByParametro(vendamaterial.getTotalprodutoDescontoReport().getValue().doubleValue(), 2)));
					}
					
					totalvalorprodutos += totalvalorvendaRelatorio.getValue().doubleValue();
					totalvalorprodutossemdesconto += vendamaterial.getTotalprodutoDescontoReport().getValue().doubleValue();
					totalvalorvendaprodutos += vendamaterial.getTotalprodutoReport().getValue().doubleValue();
				}
			}
			if(SinedUtil.isListNotEmpty(listaVendamaterialAjustada)){
				venda.setListaPedidovendamaterial(listaVendamaterialAjustada);
				listaVendaAjustada.add(venda);
			}
			totalvenda = totalvenda.add(venda.getTotalvenda());
		}
		
		report.addParameter("LISTAPEDIDOVENDA", listaVendaAjustada);
		report.addParameter("QTDEPEDIDOVENDA", listaVendaAjustada.size());
		
//		Money totalProdutos = new Money(totalvalorprodutos);
		Money totalvendaProdutos = new Money(totalvalorprodutossemdesconto);
		totallucrobruto = new Money(totalvalorprodutossemdesconto).subtract(new Money(totalcusto)).subtract(totaldescontovenda);
		if(totalvendaProdutos != null && totalvendaProdutos.getValue() != null && totalcusto != null) {
			Money valorTotalAux = totalvendaProdutos.subtract(new Money(totalcusto));
			
			if(valorTotalAux != null && valorTotalAux.getValue() != null 
					&& filtro.getConsiderarDescontos() != null && filtro.getConsiderarDescontos() 
					&& totaldescontovenda != null && totaldescontovenda.getValue() != null) {
				valorTotalAux = valorTotalAux.subtract(totaldescontovenda);
			}
			if(valorTotalAux != null && valorTotalAux.getValue() != null){				
				if("CUSTO".equalsIgnoreCase(filtro.getCalcularmargempor()) && totalcusto > 0.){
					margemmedia = new Money(valorTotalAux.getValue().doubleValue()*100/totalcusto);
				}else if("VENDA".equalsIgnoreCase(filtro.getCalcularmargempor()) && totalvendamaterial > 0.){
					margemmedia = new Money(valorTotalAux.getValue().doubleValue()*100/totalvendamaterial);
				}
			}
		}
		
		report.addParameter("totalcusto", new Money(totalcusto));
		report.addParameter("totaldescontoaplicado", totalvalorprodutossemdesconto != null && totalvalorprodutossemdesconto > 0 ?
				new Money(totaldescontovendaitem.getValue().doubleValue()/totalvalorprodutossemdesconto * 100) : new Money());
		report.addParameter("totaldescontopedidovenda", totaldescontovendaitem);
		report.addParameter("totallucrobruto", totallucrobruto);
		report.addParameter("totalvalorprodutos", new Money(totalvalorprodutos));
		report.addParameter("margemmedia", margemmedia);
		report.addParameter("totalpedidovenda", new Money(totalvenda));
		report.addParameter("exibirformulacalculo", filtro.getExibirformulacalculo() != null ? filtro.getExibirformulacalculo() : false);
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForEmitircustopedidovenda(EmitircustovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForEmitircustopedidovenda(EmitircustovendaFiltro filtro) {
		return pedidovendaDAO.findForEmitircustopedidovenda(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForEnviocomprovanteemail(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForEnviocomprovanteemail(String whereIn) {
		return pedidovendaDAO.findForEnviocomprovanteemail(whereIn);
	}
	
	/**
	 * M�todo para atualizar o saldo do pedido de venda
	 *
	 * @param request
	 * @param dtinicio
	 * @param dtfim
	 * @param cdpedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer atualizarSaldoPedidovenda(WebRequestContext request, String whereIn) {
		Integer qtdeatualizada = 0;
		List<Pedidovenda> listaPedidovenda = this.findForAtualizarsaldo(whereIn);
		
		Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
		Boolean vendasaldoporminmax = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX)) ? true : false ;
		
		if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
			Materialtabelaprecoitem materialtabelaprecoitem;
			Money saldofinal = new Money();
			Double valorvendaminimo;
			Double valorvendamaximo;
			Double valorvenda;
			for(Pedidovenda pv : listaPedidovenda){
				saldofinal = new Money();
				for(Pedidovendamaterial pvm : pv.getListaPedidovendamaterial()){
					if(pvm.getMaterial() != null && pvm.getPreco() != null && pvm.getQuantidade() != null){
						Double fatoraconversaoQtdereferencia = materialService.getFatorConversao(pvm.getMaterial(), pvm.getUnidademedida());
						
						valorvendaminimo = null;
						valorvendamaximo = null;
						if(pvm.getMaterial().getValorvendaminimo() != null){
							valorvendaminimo = pvm.getMaterial().getValorvendaminimo() / fatoraconversaoQtdereferencia;
						}
						if(pvm.getMaterial().getValorvendamaximo() != null){
							valorvendamaximo = pvm.getMaterial().getValorvendamaximo() / fatoraconversaoQtdereferencia;
						}
						
						valorvenda = null;
						if(pvm.getMaterial().getValorvenda() != null){
							valorvenda = pvm.getMaterial().getValorvenda() / fatoraconversaoQtdereferencia;
						}
						
						materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(pvm.getMaterial(), pv.getCliente(), pv.getPrazopagamento(), pv.getPedidovendatipo(), pv.getEmpresa(), pvm.getUnidademedida());
						if(materialtabelaprecoitem != null){
							if(fatoraconversaoQtdereferencia != null && fatoraconversaoQtdereferencia > 0 && !materialtabelaprecoitem.isUnidadesecundaria()){
								if(materialtabelaprecoitem.getValorvendaminimo() != null)
									valorvendaminimo = materialtabelaprecoitem.getValorvendaminimo() / fatoraconversaoQtdereferencia;
								if(materialtabelaprecoitem.getValorvendamaximo() != null)
									valorvendamaximo = materialtabelaprecoitem.getValorvendamaximo() / fatoraconversaoQtdereferencia;
								if(materialtabelaprecoitem.getValor() != null)
									valorvenda = materialtabelaprecoitem.getValor() / fatoraconversaoQtdereferencia;
							} else {
								valorvendaminimo = materialtabelaprecoitem.getValorvendaminimo();
								valorvendamaximo = materialtabelaprecoitem.getValorvendamaximo();
								valorvenda = materialtabelaprecoitem.getValor();
							}
						}
						
						if(vendasaldoporminmax){
							this.calculaSaldoMaterial(pvm, 
									pvm.getPreco(), 
									valorvendaminimo, 
									valorvendamaximo, 
									pvm.getQuantidade(), 
									pvm.getDesconto(), 
									desconsiderardescontosaldoproduto, 
									pvm.getMultiplicador());
						} else {
							this.calculaSaldoMaterialPorValorvenda(pvm, 
									pvm.getPreco(), 
									valorvenda, 
									pvm.getQuantidade(), 
									pvm.getDesconto(), 
									desconsiderardescontosaldoproduto, 
									pvm.getMultiplicador());
						}
						
						if(pvm.getSaldo() != null) {
							saldofinal = saldofinal.add(pvm.getSaldo());
							pedidovendamaterialService.updateSaldo(pvm, pvm.getSaldo());
						}
					}
				}
				if(saldofinal != null){
					this.updateSaldofinalByPedidovenda(pv, saldofinal);
					qtdeatualizada++;
				}
			}
		}
		return qtdeatualizada;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateSaldofinalByPedidovenda(Pedidovenda pedidovenda, Money saldofinal)
	 *
	 * @param pedidovenda
	 * @param saldofinal
	 * @author Luiz Fernando
	 */
	protected void updateSaldofinalByPedidovenda(Pedidovenda pedidovenda, Money saldofinal) {
		pedidovendaDAO.updateSaldofinalByPedidovenda(pedidovenda, saldofinal);		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendaService#findForAtualizarsaldo(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForAtualizarsaldo(String whereIn) {
		return pedidovendaDAO.findForAtualizarsaldo(whereIn);
	}
	
	/**
	 * M�todo para verificar a conta gerencial e centro custo no material
	 * 
	 * @param request
	 * @param pedidovenda
	 * @return
	 */
	public Boolean validaContagerencialCentrocusto(WebRequestContext request, Pedidovenda pedidovenda){
		List<Pedidovendamaterial> listapedidovendamaterial = pedidovenda.getListaPedidovendamaterial();
		Empresa empresa = empresaService.loadComContagerencialCentrocusto(pedidovenda.getEmpresa());
		List<String> listError = new ArrayList<String>();
		
		for (Pedidovendamaterial pedidovendamaterial : listapedidovendamaterial) {
			Material material = materialService.loadComContagerencialCentrocusto(pedidovendamaterial.getMaterial());
			
			Contagerencial  cg = material.getContagerencialvenda() == null ?  empresa.getContagerencial() : material.getContagerencialvenda();
			Centrocusto cc = material.getCentrocustovenda() == null ?  empresa.getCentrocusto() : material.getCentrocustovenda();
			
			if(cc == null){
				listError.add("Informar o centro de custo de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa o centro de custo de venda.");
			}
			
			if(cg == null){
				listError.add("Informar a conta gerencial de venda no material " + material.getCdmaterial() + " ou informar no cadastro de empresa a conta gerencial de venda.");
			}	
	
			if(listError.size() > 0){
				if(request == null){
					throw new SinedException(CollectionsUtil.concatenate(listError, "\n"));
				} else {
					for (String string : listError) {
						request.addError(string);
					}
				}
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Integer criarPedidovendaWebservice(CriarPedidovendaBean bean, boolean bonificacao) {
		Integer cdprazopagamento = ParametrogeralService.getInstance().getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PRAZOPAGAMENTO);
		Integer cdcolaborador = ParametrogeralService.getInstance().getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_COLABORADOR);
		Integer cddocumentotipo = ParametrogeralService.getInstance().getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_DOCUMENTOTIPO);
		
		Integer cdpedidovendatipo_bonificacao = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PEDIDOVENDATIPO_BONIFICACAO);
		Integer cdpedidovendatipo = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PEDIDOVENDATIPO);
		Integer cdconta = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_CONTA);
		Integer cdprojeto = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PROJETO);
		Integer cdlocalarmazenagem = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_LOCALARMAZENAGEM);
		
		Prazopagamento prazopagamento = prazopagamentoService.loadAll(new Prazopagamento(cdprazopagamento));
		Colaborador colaborador = new Colaborador(cdcolaborador);
		Documentotipo documentotipo = new Documentotipo(cddocumentotipo);
		
		Pedidovendatipo pedidovendatipo_bonificacao = cdpedidovendatipo_bonificacao != null ? new Pedidovendatipo(cdpedidovendatipo_bonificacao) : null;
		Pedidovendatipo pedidovendatipo = cdpedidovendatipo != null ? pedidovendatipoService.load(new Pedidovendatipo(cdpedidovendatipo)) : null;
		Conta conta = cdconta != null ? new Conta(cdconta) : null;
		Projeto projeto = cdprojeto != null ? new Projeto(cdprojeto) : null;
		Localarmazenagem localarmazenagem = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		Contrato contrato = bean.getCdcontrato()==null ? null : contratoService.loadForWS( new Contrato(bean.getCdcontrato()) );
		
		if(bean.getColaborador_matricula() != null){
			Colaborador colaborador_aux = colaboradorService.findByMatricula(bean.getColaborador_matricula());
			if(colaborador_aux != null){
				colaborador = colaborador_aux;
			}
		}
		
		ResponsavelFrete frete = null;
		if(bean.getFrete_tipo() != null && 
				(bean.getFrete_tipo().equals(1) ||
				bean.getFrete_tipo().equals(2) ||
				bean.getFrete_tipo().equals(3) ||
				bean.getFrete_tipo().equals(4))){
			frete = new ResponsavelFrete(bean.getFrete_tipo());
		} else {
			frete = ResponsavelFrete.SEM_FRETE;
		}
		
		Empresa empresa = null;
		if(bean.getEmpresa_cnpj() != null && !bean.getEmpresa_cnpj().trim().equals("")){
			try{
				empresa = empresaService.findByCnpj(new Cnpj(bean.getEmpresa_cnpj()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(empresa == null){
			empresa = empresaService.loadPrincipal();
		}
		
		Map<Documentotipo, Conta> mapDocumentotipoConta = new HashMap<Documentotipo, Conta>();
		Configuracaoecom configuracaoecom = configuracaoecomService.findPrincipalByEmpresa(empresa);
		if(configuracaoecom != null){
			if(configuracaoecom.getLocalarmazenagem() != null){
				localarmazenagem = configuracaoecom.getLocalarmazenagem();
			}
			if(configuracaoecom.getConta() != null){
				conta = configuracaoecom.getConta();
			}
			if(configuracaoecom.getListaConfiguracaoecomconta() != null){
				for (Configuracaoecomconta configuracaoecomconta : configuracaoecom.getListaConfiguracaoecomconta()) {
					mapDocumentotipoConta.put(configuracaoecomconta.getDocumentotipo(), configuracaoecomconta.getConta());
				}
			}
		}
		
		Cliente cliente = null;
		if(bean.getCliente_cpfcnpj() != null && !bean.getCliente_cpfcnpj().trim().equals("")){
			try{
				String cnpjcpf = StringUtils.soNumero(bean.getCliente_cpfcnpj());
				
				if(cnpjcpf.length() == 11){
					cliente = clienteService.findByCpf(new Cpf(cnpjcpf));
				} else if(cnpjcpf.length() == 14){
					cliente = clienteService.findByCnpj(new Cnpj(cnpjcpf));
				}
			} catch (Exception e) {
				throw new SinedException("Erro na busca do cliente na base: " + e.getMessage());
			}
		}
		if(cliente == null){
			throw new SinedException("Cliente n�o encontrado na base.");
		}
		
		clientevendedorService.criarRelacaoClientevendedor(cliente, bean);
		
		Pedidovenda pedidovenda = new Pedidovenda();
		pedidovenda.setIdentificador(bean.getPedidovenda_identificador());
		
		if(bonificacao){
			pedidovenda.setPedidovendatipo(pedidovendatipo_bonificacao);
		} else {
			pedidovenda.setPedidovendatipo(pedidovendatipo);
		}
		
		List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
		cliente.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
		
		pedidovenda.setPresencacompradornfe(Presencacompradornfe.INTERNET);
		pedidovenda.setEmpresa(empresa);
		pedidovenda.setCliente(cliente);
		pedidovenda.setEndereco(bean.getCliente_cdendereco() != null ? new Endereco(bean.getCliente_cdendereco()) : cliente.getEndereco());
		pedidovenda.setColaborador(colaborador);
		pedidovenda.setDtpedidovenda(bean.getPedidovenda_data());
		pedidovenda.setObservacao(bean.getPedidovenda_observacao());
		pedidovenda.setObservacaointerna(bean.getPedidovenda_observacaointerna());
		pedidovenda.setPrazopagamento(prazopagamento);
		pedidovenda.setPrazomedio(Boolean.TRUE);
		pedidovenda.setConta(conta);
		pedidovenda.setDocumentotipo(documentotipo);
		pedidovenda.setFrete(frete);
		pedidovenda.setValorfrete(bean.getFrete_valor());
		pedidovenda.setProjeto(projeto);
		pedidovenda.setLocalarmazenagem(localarmazenagem);
		pedidovenda.setDesconto(bean.getPedidovenda_desconto());
		
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		List<CriarPedidovendaItemBean> pedidovendaItem = bean.getPedidovenda_item();
		for (CriarPedidovendaItemBean it : pedidovendaItem) {
			Material material = null;
			String identificadorMaterial = it.getMaterial_identificador();
			if(configuracaoecom != null){
				Ecommaterialidentificador ecommaterialidentificador = ecommaterialidentificadorService.findByIdentificador(identificadorMaterial, configuracaoecom);
				if(ecommaterialidentificador != null){
					material = ecommaterialidentificador.getMaterial();
				}
			}
			
			if(material == null){
				material = materialService.findByIdentificacao(identificadorMaterial);
			}
			
			if(material == null){
				throw new SinedException("Material com identificador '" + identificadorMaterial + "' n�o encontrado na base.");
			}
			
			Pedidovendamaterial pedidovendamaterial = new Pedidovendamaterial();
			pedidovendamaterial.setMaterial(material);
			pedidovendamaterial.setUnidademedida(material.getUnidademedida());
			pedidovendamaterial.setQuantidade(it.getQuantidade());
			
			/**
			 * O valor unit�rio ser� obtido automaticamente pelo w3erp considerando o valor do item informado no contrato, 
			 * se n�o houver o item no contrato, considera o valor conforme tabela de pre�o (Considera Cliente e per�odo para carregar a tabela) 
			 * se n�o houver tabela de pre�o considera o valor de venda do material.
			 */
			if(bean.getCdcontrato()!=null && contrato != null){
				boolean existeItemNoContrato = false;
				for(Contratomaterial cm : contrato.getListaContratomaterial()){
					if(cm.getServico().getIdentificacao().equals(identificadorMaterial)){
						pedidovendamaterial.setPreco(cm.getValorunitario());
						it.setValor_unitario(cm.getValorunitario());
						existeItemNoContrato = true;
					}
				}
				
				if(!existeItemNoContrato){
					preencheValorVendaComTabelaPreco(material, cliente, prazopagamento, pedidovendatipo, empresa, null);
					pedidovendamaterial.setPreco(material.getValorvenda());
					it.setValor_unitario(material.getValorvenda());
				}
				
				if(material.getValorvenda()==null){
					pedidovendamaterial.setPreco(it.getValor_unitario());
				}
				
			}else{
				pedidovendamaterial.setPreco(it.getValor_unitario());
			}
			pedidovendamaterial.setDesconto(it.getDesconto());
			pedidovendamaterial.setDtprazoentrega(it.getPrazo_entrega());
			pedidovendamaterial.setObservacao(it.getObservacao());
			
			pedidovendamaterial.setTotal(vendaService.getValortotal(pedidovendamaterial.getPreco(), 
					pedidovendamaterial.getQuantidade(),
					pedidovendamaterial.getDesconto(), 
					pedidovendamaterial.getMultiplicador(),
					pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro()));
			
			addItemPedidovendamaterialWebservice(listaPedidovendamaterial, pedidovendamaterial);
		}
		pedidovenda.setListaPedidovendamaterial(listaPedidovendamaterial);
		setOrdemItem(pedidovenda);
		
		if(!bonificacao){
			List<Pedidovendapagamento> listaPedidovendapagamento = new ArrayList<Pedidovendapagamento>();
			List<CriarPedidovendaPagamentoBean> pedidovendaPag = bean.getPedidovenda_pagamento();
			
			Documentotipo documentotipoAux = null;
			boolean documentotipoUnico = true;
			
			for (CriarPedidovendaPagamentoBean pag : pedidovendaPag) {
				Pedidovendapagamento pedidovendapagamento = new Pedidovendapagamento();
				
				pedidovendapagamento.setDocumentotipo(documentotipo);
				if(pag.getCddocumentotipo() != null){
					Documentotipo doctipo = documentotipoService.load(new Documentotipo(pag.getCddocumentotipo()));
					if(doctipo != null) pedidovendapagamento.setDocumentotipo(doctipo);
				} 
				
				if(pag.getCddocumento() != null){
					Documento documento = documentoService.carregaDocumento(new Documento(pag.getCddocumento()));
					pedidovendapagamento.setValororiginal(documento.getValor());
					pedidovendapagamento.setDocumento(documento);
					pedidovendapagamento.setDataparcela(documento.getDtvencimento());
					if(documento.getDocumentotipo() != null){
						Documentotipo documentotipoDocumento = documentotipoService.load(documento.getDocumentotipo());
						pedidovendapagamento.setDocumentotipo(documentotipoDocumento);
					}
					
					if(documento.getConta() != null){
						pedidovendapagamento.setContadestino(documento.getConta());
					}
				} else {
					if(pedidovendapagamento.getDocumentotipo() != null){
						Conta contadestino = mapDocumentotipoConta.get(pedidovendapagamento.getDocumentotipo());
						pedidovendapagamento.setContadestino(contadestino);
					}
					pedidovendapagamento.setValororiginal(pag.getValor());
					pedidovendapagamento.setDataparcela(pag.getDatavencimento());
				}
				
				if(documentotipoAux == null){
					documentotipoAux = pedidovendapagamento.getDocumentotipo();
				} else if(!documentotipoAux.equals(pedidovendapagamento.getDocumentotipo())){
					documentotipoUnico = false;
				}
				
				listaPedidovendapagamento.add(pedidovendapagamento);
			}
			pedidovenda.setListaPedidovendapagamento(listaPedidovendapagamento);
			
			if(documentotipoUnico && documentotipoAux != null){
				pedidovenda.setDocumentotipo(documentotipoAux);
			}
			
			boolean boleto = false;
			Conta contaunica = null;
			for (Pedidovendapagamento pedidovendapagamento : listaPedidovendapagamento) {
				if(pedidovendapagamento.getDocumentotipo() != null){
					Documentotipo doctipo = documentotipoService.load(pedidovendapagamento.getDocumentotipo());
					if(doctipo.getBoleto() != null && doctipo.getBoleto()){
						boleto = true;
					}
				}
				
				if(contaunica == null){
					contaunica = pedidovendapagamento.getContadestino();
				} else if(pedidovendapagamento.getContadestino() != null && !contaunica.equals(pedidovendapagamento.getContadestino())){
					contaunica = null;
					break;
				}
			}
			if(contaunica != null){
				pedidovenda.setConta(contaunica);
			}
			if(!boleto){
				pedidovenda.setConta(null);
			}
		}
		
		/**
		 * Definindo a situa��o do pedido
		 */
		if(bean.getCdcontrato()==null){
			Boolean restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(pedidovenda.getCliente());
			if(bonificacao || restricaoCliente){
				if(bonificacao && parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_PEDIDOVENDA_SITUACAO_PREVISTA)){
					pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
				}else {
					pedidovenda.setRestricaocliente(restricaoCliente);
					pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				}
			} else {					
				pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
		}else{
			// O pedido � cadastrado com o status de 'Previsto' exceto se a marca��o no tipo de pedido de venda 
			//esteja marcada para sempre requer aprova��o, neste caso ser� criado um pedido 'Aguardando aprova��o'
			if(pedidovendatipo.getRequeraprovacaopedido()){
				pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			}else{
				pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
				
		}
		
		if(pedidovenda.getPrazopagamento() != null && 
				pedidovenda.getPrazopagamento().getCdprazopagamento() != null && 
				pedidovenda.getDesconto() != null && 
				pedidovenda.getDesconto().getValue().doubleValue() > 0){
			Prazopagamento prazopagamentoAux = prazopagamentoService.load(pedidovenda.getPrazopagamento(), "prazopagamento.cdprazopagamento, prazopagamento.valordescontomaximo");
			if(prazopagamentoAux.getValordescontomaximo() != null && prazopagamentoAux.getValordescontomaximo() > 0){
				Double totalvenda = pedidovenda.getTotalvenda().getValue().doubleValue();
				Double desconto = pedidovenda.getDesconto().getValue().doubleValue();
				Double percentual = (desconto * 100d) / totalvenda;
				if(totalvenda > 0){
					pedidovenda.setPercentualdesconto(percentual);
				}
				
				if(pedidovenda.getPercentualdesconto() != null && pedidovenda.getPercentualdesconto() > prazopagamentoAux.getValordescontomaximo()){
					pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				}
			}
		}
		
		
		/**
		  * Ser� obtido o vencimento, conforme prazo de pagamento informado no contrato, 
		 gerando as parcelas conforme o prazo e 
		 considerando a data de incio para calculo da data de vencimento, a data de entrega do primeiro item.
		   * Valor das parcelas: Valor total / Total de parcelas do prazo de pagamento do contrato
		   * - O tipo de documento das parcelas ser� Boleto e a conta para boleto ser� a conta informada no contrato. 
		   *  Preencher o tipo do pedido de venda, com o tipo de pedido de venda definido no tipo de contrato informado 
		   *   O pedido � cadastrado com o status de 'Previsto' exceto se a marca��o no tipo de pedido de venda 
		   *   esteja marcada para sempre requer aprova��o, neste caso ser� criado um pedido 'Aguardando aprova��o'	
		  */ 
		java.sql.Date dtprazoentrega = bean.getPedidovenda_item().get(0).getPrazo_entrega();
		
		Double valorTotal = 0D;
		for(CriarPedidovendaItemBean it : bean.getPedidovenda_item()){
			if(it.getValor_unitario() != null && it.getQuantidade() != null){
				valorTotal += (it.getValor_unitario() * it.getQuantidade());				
			}
		}
		Pedidovenda aux = new Pedidovenda();
		aux.setEmpresa(empresa);
		aux.setPrazopagamento(prazopagamento);
		aux.setValor(valorTotal);
		aux.setDtprazoentrega(dtprazoentrega);
		aux.setDtpedidovenda(new java.sql.Date(System.currentTimeMillis()));
		aux.setQtdeParcelas(prazopagamento.getListaPagamentoItem().size());
//		aux.setPagamentoDataUltimoVencimento(pagamentoDataUltimoVencimento)
		
		pagamento(aux, null);
		
		pedidovenda.setOrigemwebservice(true);
		this.saveOrUpdate(pedidovenda);
		
		if(empresaService.isIntegracaoWms(pedidovenda.getEmpresa())){
			this.integracaoWMS(pedidovenda, "Cria��o do pedido de venda. Qtde de itens = " + pedidovenda.getListaPedidovendamaterial().size(), false);
		}
		
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Criado");
		pedidovendahistorico.setObservacao("Cria��o a partir do WebService");
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		
		if(contrato!=null && contrato.getCdcontrato()!=null){
			contratohistoricoService.criaHistoricoForPedidovendaWS(contrato, pedidovenda);
		}
		
		if(!bonificacao){
			List<Rateioitem> listarateioitem = this.prepareAndSaveVendaMaterial(pedidovenda, false, empresa, false, null);
			this.prepareAndSaveReceita(pedidovenda, listarateioitem, empresa);
		}
		return pedidovenda.getCdpedidovenda();
	}
	
	@SuppressWarnings("unchecked")
	public Integer criarPedidovendaWebserviceEcommerce(Ecom_PedidoVenda bean, ConfiguracaoEcommerceBeanAux conf) {
		boolean isTrayCorp = bean.getTipoEcommerce() != null && EcommerceEnum.TRAY_CORP.ordinal() == bean.getTipoEcommerce();
		Ecom_PedidoVenda criado = Ecom_PedidoVendaService.getInstance().findWithPedidoCriado(bean);
		if(criado != null){//J� criado. S� incluir a situa��o atual no hist�rico
			if(SinedUtil.isListNotEmpty(bean.getPedidoSituacoes())){
				Ecom_PedidoVendaSituacao situacao = bean.getPedidoSituacoes().get(bean.getPedidoSituacoes().size() - 1);
				Pedidovendahistorico historico = new Pedidovendahistorico();
				
				if(isTrayCorp){
					historico.setAcao("Altera��o no Tray Corp: "+situacao.getSituacao());
				}else{
					historico.setAcao("Altera��o no Ecompleto: "+situacao.getSituacao());
				}
				
				historico.setPedidovenda(criado.getPedidoVenda());
				historico.setObservacao(situacao.getTexto());
				historico.setDtaltera(SinedDateUtils.currentTimestamp());
				PedidovendahistoricoService.getInstance().saveOrUpdate(historico);
			}
			return criado.getPedidoVenda().getCdpedidovenda();
		}
		Integer cdempresa = conf.getCdEmpresa();

		Integer cdprazopagamento = conf.getCdPrazoPagamento();
		Integer cdcolaborador = conf.getCdColaborador();
		Integer cddocumentotipo = null;
		Integer cdParceiro = null;
		Conta conta = null;
		if(bean.getTipoEcommerce() != null && EcommerceEnum.TRAY_CORP.ordinal() == bean.getTipoEcommerce()){
			cdParceiro = bean.getCodigoParceiro();
			boolean sairPag = false;
			for(Ecom_PedidoVendaPagamento pagamento: bean.getPagamentos()){
				boolean sairConfig = false;
				for(ConfiguracaoEcommerceFormaPagamento config: conf.getListaPagamentos()){
					if(config.getIdEcommerce().equals(pagamento.getFormaPagamentoId())){
						for(ConfiguracaoEcommerceConta ct: conf.getListaContas()){
							if(ct.getIdFormaPagamentoW3erp().equals(config.getCdFormaPagamentoW3erp())
									&& ((ct.getCodigoParceiro() == null && cdParceiro == null) || cdParceiro.equals(ct.getCodigoParceiro()))){
								pagamento.setFormaPagamentoId(config.getCdFormaPagamentoW3erp());
								pagamento.setDocumentoTipo(new Documentotipo(config.getCdFormaPagamentoW3erp()));
								conta = new Conta(ct.getIdContaBancariaW3erp());
								sairConfig = true;
								break;
							}
						}
						
						if(sairConfig){
							break;
						}
					}
				}
				if(sairPag){
					break;
				}
			}
			cddocumentotipo = SinedUtil.isListNotEmpty(bean.getPagamentos())? bean.getPagamentos().get(0).getFormaPagamentoId(): null;
		}else{
			for(ConfiguracaoEcommerceFormaPagamento config: conf.getListaPagamentos()){
				if(config.getIdEcommerce().equals(bean.getId_formapagto())){
					cddocumentotipo = config.getCdFormaPagamentoW3erp();
					for(ConfiguracaoEcommerceConta ct: conf.getListaContas()){
						if(ct.getIdFormaPagamentoW3erp().equals(config.getCdFormaPagamentoW3erp())){
							conta = new Conta(ct.getIdContaBancariaW3erp());
							break;
						}
					}
					break;
				}
			}
		}
		
		//Integer cdpedidovendatipo_bonificacao = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PEDIDOVENDATIPO_BONIFICACAO);
		Integer cdpedidovendatipo = conf.getCdPedidoVendaTipo();
		Integer cdlocalarmazenagem = conf.getCdLocalArmazenagem();
		
		Prazopagamento prazopagamento = cdprazopagamento != null? prazopagamentoService.loadAll(new Prazopagamento(cdprazopagamento)): null;
		Colaborador colaborador = null;
		if(cdcolaborador != null){
			colaborador = new Colaborador(cdcolaborador);
		}
		Documentotipo documentotipo = new Documentotipo(cddocumentotipo);
		
		//Pedidovendatipo pedidovendatipo_bonificacao = cdpedidovendatipo_bonificacao != null ? new Pedidovendatipo(cdpedidovendatipo_bonificacao) : null;
		Pedidovendatipo pedidovendatipo = cdpedidovendatipo != null ? pedidovendatipoService.load(new Pedidovendatipo(cdpedidovendatipo)) : null;
		//Conta conta = cdconta != null ? new Conta(cdconta) : null;
		//Projeto projeto = cdprojeto != null ? new Projeto(cdprojeto) : null;
		Localarmazenagem localarmazenagem = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		//Contrato contrato = bean.getCdcontrato()==null ? null : contratoService.loadForWS( new Contrato(bean.getCdcontrato()) );
		
		/*if(bean.getColaborador_matricula() != null){
			Colaborador colaborador_aux = colaboradorService.findByMatricula(bean.getColaborador_matricula());
			if(colaborador_aux != null){
				colaborador = colaborador_aux;
			}
		}*/
		
		ResponsavelFrete frete = null;
		/*if(bean.getFrete_tipo() != null && 
				(bean.getFrete_tipo().equals(1) ||
				bean.getFrete_tipo().equals(2) ||
				bean.getFrete_tipo().equals(3) ||
				bean.getFrete_tipo().equals(4))){
			frete = new ResponsavelFrete(bean.getFrete_tipo());
		} else {
			frete = ResponsavelFrete.SEM_FRETE;
		}*/
		
		Empresa empresa = empresaService.load(new Empresa(cdempresa));
		
		Ecom_Cliente clienteEcom = bean.getPedidoCliente();

		
		InserirClienteBean clienteBean = new InserirClienteBean();
		clienteBean.setNome(clienteEcom.getNome());
		clienteBean.setEmail(clienteEcom.getEmail());
		
		
		
		clienteBean.setSexo(clienteEcom.getSexo());
		clienteBean.setRazaosocial(clienteEcom.getRazaoSocial());
		
		Cpf cpf = null;
		Cnpj cnpj = null;
		try {
			cpf = new Cpf(clienteEcom.getCpf_cnpj());
		} catch (Exception e) {}
		try {
			cnpj = new Cnpj(clienteEcom.getCpf_cnpj());
		} catch (Exception e) {}
		
		clienteBean.setCnpj(cnpj != null ? cnpj.getValue() : null);
		clienteBean.setCpf(cpf != null ? cpf.getValue() : null);
		
		//clienteBean.setTelefonecontato(bean.getPedidoCliente().getTelefones().get(0).getTelefone());
		//clienteBean.setCelularcontato(bean.getCliente_celularcontato());
		if(isTrayCorp){
			if(SinedUtil.isListNotEmpty(bean.getPedidoCliente().getTelefones())){
				for(Ecom_Telefone telefone: bean.getPedidoCliente().getTelefones()){
					if(Telefonetipo.CELULAR.toString().equals(telefone.getTipo())){
						clienteBean.setCelular(telefone.getTelefone());
					}else if(Telefonetipo.COMERCIAL.toString().equals(telefone.getTipo())){
						clienteBean.setTelefonecomercial(telefone.getTelefone());
					}else if(Telefonetipo.RESIDENCIAL.toString().equals(telefone.getTipo())){
						clienteBean.setTelefone(telefone.getTelefone());
					}
				}
			}
		}else if(SinedUtil.isListNotEmpty(bean.getPedidoCliente().getTelefones())){
			clienteBean.setTelefone(bean.getPedidoCliente().getTelefones().get(0).getTelefone());
			//clienteBean.setCelular(bean.getCliente_celular());
		}

		Ecom_Endereco endereco = clienteEcom.getEndereco();
		if(isTrayCorp){
			endereco = bean.getPedidoCliente().getListaEnderecos().get(0);
		}
		clienteBean.setEnderecologradouro(endereco.getEndereco());
		clienteBean.setEndereconumero(endereco.getNumero());
		clienteBean.setEnderecocomplemento(endereco.getComplemento());
		clienteBean.setEnderecobairro(endereco.getBairro());
		clienteBean.setEnderecocep(endereco.getCep());
		clienteBean.setEnderecomunicipio(endereco.getCidade());
		clienteBean.setEnderecouf(endereco.getEsta_cod());
		clienteBean.setEnderecotipo(conf.getCdEnderecoTipo());
		clienteBean.setInscricaoestadual(clienteEcom.getInscricao_estadual());
		clienteBean.setEnderecoreferencia(endereco.getReferencia());
		clienteBean.setFromEcompleto(true);
		clienteBean.setPermitirCriarEnderecoSemMunicipio(true);
		Integer cdcliente = clienteService.criarClienteWebservice(clienteBean);
		Cliente cliente = new Cliente(cdcliente);
		
		if(bean.getTipoEcommerce() != null && EcommerceEnum.TRAY_CORP.ordinal() == bean.getTipoEcommerce()){
			List<Telefone> telefonesJaCadastrados = telefoneService.carregarListaTelefone(cliente);
			for(Ecom_Telefone telefone: bean.getPedidoCliente().getTelefones()){
				if(org.apache.commons.lang.StringUtils.isBlank(telefone.getTelefone())){
					continue;
				}
				Integer cdTelefoneTipo = null;
				try {
					cdTelefoneTipo = Integer.parseInt(telefone.getTipo());
				} catch (Exception e) {
					continue;
				}
				boolean exists = false;
				for(Telefone telJaCadastrado: telefonesJaCadastrados){
					if(telefone.getTelefone().equals(telJaCadastrado.getTelefone()) &&
						cdTelefoneTipo.equals(telJaCadastrado.getTelefonetipo().getCdtelefonetipo())){
						exists = true;
						break;
					}
				}
				
				if(!exists){
					Telefone fone = new Telefone();
					fone.setPessoa(cliente);
					fone.setTelefonetipo(new Telefonetipo(cdTelefoneTipo));
					fone.setTelefone(telefone.getTelefone());
					
					telefoneService.saveOrUpdate(fone);
				}
			}
		}
		
		//clientevendedorService.criarRelacaoClientevendedor(cliente, bean);
		
		Pedidovenda pedidovenda = new Pedidovenda();
		pedidovenda.setIdentificador(bean.getId().toString());
		pedidovenda.setPedidovendatipo(pedidovendatipo);
		
		//List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
		//cliente.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
		
		pedidovenda.setPresencacompradornfe(Presencacompradornfe.INTERNET);
		pedidovenda.setEmpresa(empresa);
		pedidovenda.setCliente(cliente);
		
		if(clienteBean.getCdendereco() == null){
			throw new SinedException("Endere�o n�o cadastrado. Pedido: '" + bean.getId() + "', Cliente: " + clienteBean.getNome());
		}
		
		pedidovenda.setEndereco(new Endereco(clienteBean.getCdendereco()));
		pedidovenda.setColaborador(colaborador);
		try {
			pedidovenda.setDtpedidovenda(SinedDateUtils.stringToDate(bean.getData(), "yyyy-MM-dd"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pedidovenda.setObservacao(bean.getObservacao());
		//pedidovenda.setObservacaointerna(bean.getPedidovenda_observacaointerna());
		pedidovenda.setPrazopagamento(prazopagamento);
		//pedidovenda.setPrazomedio(Boolean.TRUE);
		
		Integer codigoParceiro = bean.getCodigoParceiro();
		
		if(bean.getTipoEcommerce() == null || EcommerceEnum.TRAY_CORP.ordinal() != bean.getTipoEcommerce()){
			for(ConfiguracaoEcommerceConta pag: conf.getListaContas()){
				if(pag.getIdFormaPagamentoW3erp().equals(documentotipo.getCddocumentotipo())){
					//conta = new Conta(pag.getIdContaBancariaW3erp());
					codigoParceiro = pag.getCodigoParceiro();
					break;
				}
			}
		}
		if(codigoParceiro != null){
			pedidovenda.setParceiro(fornecedorService.findByCodigoParceiro(codigoParceiro));
		}
		pedidovenda.setConta(conta);
		pedidovenda.setDocumentotipo(documentotipo);
		pedidovenda.setFrete(frete);
		pedidovenda.setValorfrete(new Money(bean.getValor_frete()));
		//pedidovenda.setProjeto(projeto);
		pedidovenda.setLocalarmazenagem(localarmazenagem);
		pedidovenda.setDesconto(new Money(bean.getValor_desconto()));
		
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		List<Ecom_PedidoVendaItem> listaPedidovendaItem = bean.getPedidoItens();
		for (Ecom_PedidoVendaItem it : listaPedidovendaItem) {
			Material material = materialService.loadByIdEcommerce(it.getId());
			if(material == null){
				throw new SinedException("Material n�o encontrado com o idEcommerce '" + it.getId() + "'.");
			}
			
			Pedidovendamaterial pedidovendamaterial = new Pedidovendamaterial();
			pedidovendamaterial.setPreco(it.getPreco_venda());
			pedidovendamaterial.setQuantidade(it.getQuantidade());
			if(it.getDesconto() != null){
				pedidovendamaterial.setDesconto(new Money(it.getDesconto()));
			}
			pedidovendamaterial.setMaterial(material);
			pedidovendamaterial.setObservacao(it.getObservacao());
			
			pedidovendamaterial.setUnidademedida(material.getUnidademedida());
			pedidovendamaterial.setQuantidade(it.getQuantidade());
			
			//pedidovendamaterial.setDesconto(it.getDesconto());
			//pedidovendamaterial.setDtprazoentrega(it.getPrazo_entrega());
			//pedidovendamaterial.setObservacao(it.getObservacao());
			
			pedidovendamaterial.setTotal(vendaService.getValortotal(pedidovendamaterial.getPreco(), 
					pedidovendamaterial.getQuantidade(),
					pedidovendamaterial.getDesconto(), 
					pedidovendamaterial.getMultiplicador(),
					pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro()));
			
			addItemPedidovendamaterialWebservice(listaPedidovendamaterial, pedidovendamaterial);
		}
		pedidovenda.setListaPedidovendamaterial(listaPedidovendamaterial);
		setOrdemItem(pedidovenda);
		
		List<Pedidovendapagamento> listaPedidovendapagamento = new ArrayList<Pedidovendapagamento>();
		//List<CriarPedidovendaPagamentoBean> pedidovendaPag = bean.getPedidovenda_pagamento();
		
		Documentotipo documentotipoAux = null;
		boolean documentotipoUnico = true;
		Documentotipo documentoTipo = null;
		for(ConfiguracaoEcommerceFormaPagamento pag: conf.getListaPagamentos()){
			if(pag.getIdEcommerce().equals(bean.getId_formapagto())){
				documentoTipo = new Documentotipo(pag.getCdFormaPagamentoW3erp());
				break;
			}
		}
		
		if(isTrayCorp){
			for(Ecom_PedidoVendaPagamento pagamento: bean.getPagamentos()){
				List<Pedidovendapagamento> listaAux = new ArrayList<Pedidovendapagamento>();
				Double totalParcelasForma = 0D;
				for(int i=0; i < pagamento.getNumeroParcelas(); i++){
					Pedidovendapagamento pvp = new Pedidovendapagamento();

					pvp.setValororiginal(new Money(pagamento.getValorParcela()));
					if(pagamento.getValorJuros() != null){
						if(pagamento.getNumeroParcelas() != null && pagamento.getNumeroParcelas() > 0){
							pvp.setValorjuros(new Money(pagamento.getValorJuros() / pagamento.getNumeroParcelas()));
						}else {
							pvp.setValorjuros(new Money(pagamento.getValorJuros()));
						}
					}
					//pvp.setDocumentotipo(new Documentotipo(pagamento.getFormaPagamentoId()));
					pvp.setDocumentotipo(pagamento.getDocumentoTipo());
					pvp.setContadestino(conta);
					pvp.setDataparcela(pagamento.getDataPagamento());
					pvp.setDataparcelaEmissaonota(pagamento.getDataPagamento());
					listaAux.add(pvp);
				}
				
				this.verificaDiferencaTotal(pagamento.getValorTotal(), listaAux, pagamento.getNumeroParcelas(), pagamento.getValorJuros());
				listaPedidovendapagamento.addAll(listaAux);
			}
		}else{
			List<Pedidovendapagamento> listaAux = new ArrayList<Pedidovendapagamento>();
			for(int i=0; i < bean.getNumero_parcelas(); i++){
				Pedidovendapagamento pedidovendapag = new Pedidovendapagamento();
				
				pedidovendapag.setDocumentotipo(documentotipo);
				
				/*if(pag.getCddocumentotipo() != null){
					Documentotipo doctipo = documentotipoService.load(new Documentotipo(pag.getCddocumentotipo()));
					if(doctipo != null) pedidovendapag.setDocumentotipo(doctipo);
				} */
				
				pedidovendapag.setValororiginal(new Money(bean.getValor_parcela()));
				//pedidovendapag.setValorjuros(new Money(bean.getValor_parcela()));
				
				pedidovendapag.setDataparcela(pedidovenda.getDtpedidovenda());
				pedidovendapag.setContadestino(conta);
				listaAux.add(pedidovendapag);
			}
			this.verificaDiferencaTotal(bean.getValor_total(), listaAux, bean.getNumero_parcelas(), bean.getValor_juros());
			listaPedidovendapagamento.addAll(listaAux);
		}
				
		pedidovenda.setListaPedidovendapagamento(listaPedidovendapagamento);
		
		if(documentotipoUnico && documentotipoAux != null){
			pedidovenda.setDocumentotipo(documentotipoAux);
		}
		
		pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);;

		/*// O pedido � cadastrado com o status de 'Previsto' exceto se a marca��o no tipo de pedido de venda 
		//esteja marcada para sempre requer aprova��o, neste caso ser� criado um pedido 'Aguardando aprova��o'
		if(pedidovendatipo != null && pedidovendatipo.getRequeraprovacaopedido()){
			pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
		}*/
		
		if(pedidovenda.getPrazopagamento() != null && 
				pedidovenda.getPrazopagamento().getCdprazopagamento() != null && 
				pedidovenda.getDesconto() != null && 
				pedidovenda.getDesconto().getValue().doubleValue() > 0){
			Prazopagamento prazopagamentoAux = prazopagamentoService.load(pedidovenda.getPrazopagamento(), "prazopagamento.cdprazopagamento, prazopagamento.valordescontomaximo");
			if(prazopagamentoAux.getValordescontomaximo() != null && prazopagamentoAux.getValordescontomaximo() > 0){
				Double totalvenda = pedidovenda.getTotalvenda().getValue().doubleValue();
				Double desconto = pedidovenda.getDesconto().getValue().doubleValue();
				Double percentual = (desconto * 100d) / totalvenda;
				if(totalvenda > 0){
					pedidovenda.setPercentualdesconto(percentual);
				}
				
				if(pedidovenda.getPercentualdesconto() != null && pedidovenda.getPercentualdesconto() > prazopagamentoAux.getValordescontomaximo()){
					pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				}
			}
		}
		
		
		/**
		  * Ser� obtido o vencimento, conforme prazo de pagamento informado no contrato, 
		 gerando as parcelas conforme o prazo e 
		 considerando a data de incio para calculo da data de vencimento, a data de entrega do primeiro item.
		   * Valor das parcelas: Valor total / Total de parcelas do prazo de pagamento do contrato
		   * - O tipo de documento das parcelas ser� Boleto e a conta para boleto ser� a conta informada no contrato. 
		   *  Preencher o tipo do pedido de venda, com o tipo de pedido de venda definido no tipo de contrato informado 
		   *   O pedido � cadastrado com o status de 'Previsto' exceto se a marca��o no tipo de pedido de venda 
		   *   esteja marcada para sempre requer aprova��o, neste caso ser� criado um pedido 'Aguardando aprova��o'	
		  */ 
		//java.sql.Date dtprazoentrega = bean.getPedidovenda_item().get(0).getPrazo_entrega();
		
		Double valorTotal = 0D;
		for(Ecom_PedidoVendaItem it : bean.getPedidoItens()){
			if(it.getPreco_venda() != null && it.getQuantidade() != null){
				valorTotal += (it.getPreco_venda() * it.getQuantidade());				
			}
		}
		if(bean.getTipoEcommerce() != null && EcommerceEnum.TRAY_CORP.ordinal() == bean.getTipoEcommerce()){
			if(bean.getData_pagamento() != null){
				pedidovenda.setDtpedidovendaEmissaonota(new java.sql.Date(bean.getData_pagamento().getTime()));
			}
		}
		/*if(bean.getTipoEcommerce() == null || EcommerceEnum.TRAY_CORP.ordinal() != bean.getTipoEcommerce()){
			Pedidovenda aux = new Pedidovenda();
			aux.setEmpresa(empresa);
			aux.setPrazopagamento(prazopagamento);
			aux.setValor(valorTotal);
			aux.setDtprazoentrega(pedidovenda.getDtpedidovenda());
			aux.setDtpedidovenda(new java.sql.Date(System.currentTimeMillis()));
			aux.setQtdeParcelas(prazopagamento.getListaPagamentoItem().size());
	//		aux.setPagamentoDataUltimoVencimento(pagamentoDataUltimoVencimento)
			
			pagamento(aux, null);
		}*/
		
		pedidovenda.setOrigemwebservice(true);
		this.saveOrUpdate(pedidovenda);
		Ecom_PedidoVendaService.getInstance().updateProcessado(bean.getCdEcom_pedidovenda(), pedidovenda.getCdpedidovenda());
		
		if(empresaService.isIntegracaoWms(pedidovenda.getEmpresa())){
			this.integracaoWMS(pedidovenda, "Cria��o do pedido de venda. Qtde de itens = " + pedidovenda.getListaPedidovendamaterial().size(), false);
		}
		
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Criado");
		if(isTrayCorp){
			pedidovendahistorico.setObservacao("Cria��o a partir do TrayCorp");
		}else{
			pedidovendahistorico.setObservacao("Cria��o a partir do E-Completo");
		}
			
		
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		
		if(SinedUtil.isListNotEmpty(bean.getPedidoSituacoes())){
			for(Ecom_PedidoVendaSituacao situacao: bean.getPedidoSituacoes()){
				pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setPedidovenda(pedidovenda);
				pedidovendahistorico.setAcao(situacao.getSituacao());
				pedidovendahistorico.setObservacao(situacao.getSituacao());
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
		}else{
			pedidovendahistorico = new Pedidovendahistorico();
			pedidovendahistorico.setPedidovenda(pedidovenda);
			pedidovendahistorico.setAcao("Criado");
			pedidovendahistorico.setObservacao(bean.getNome_situacao());
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		}
		
		
		List<Rateioitem> listarateioitem = this.prepareAndSaveVendaMaterial(pedidovenda, false, empresa, false, null);
		this.gerarReservaAoSalvar(pedidovendatipo, pedidovenda);
		if(GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidovendatipo.getGeracaocontareceberEnum())){
			this.prepareAndSaveReceita(pedidovenda, listarateioitem, empresa);
		}
		return pedidovenda.getCdpedidovenda();
	}
	
	/**
	* M�todo que adiciona os itens do kit na lista de pedido de venda, 
	* caso o material tenha a marca��o de exibir os itens, ou apenas insere o item na lista
	 *
	* @param listaPedidovendamaterial
	* @param pedidovendamaterial
	* @since 23/04/2015
	* @author Luiz Fernando
	*/
	protected void addItemPedidovendamaterialWebservice(List<Pedidovendamaterial> listaPedidovendamaterial,	Pedidovendamaterial pedidovendamaterial) {
		if(pedidovendamaterial != null && pedidovendamaterial.getMaterial() != null && 
				pedidovendamaterial.getQuantidade() != null && 
				pedidovendamaterial.getQuantidade() > 0){
			if(listaPedidovendamaterial == null)
				listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
			
			boolean adicionarItem = true;
			Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(pedidovendamaterial.getMaterial().getCdmaterial());
			if(material != null && material.getVendapromocional() != null && material.getVendapromocional() && 
					material.getExibiritenskitvenda() != null && material.getExibiritenskitvenda()){
				if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
					Money totalKit = new Money();
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getValorvenda() != null && materialrelacionado.getQuantidade() != null && 
								materialrelacionado.getQuantidade() > 0){	
							totalKit = totalKit.add(new Money(materialrelacionado.getValorvenda().getValue().doubleValue() * materialrelacionado.getQuantidade() * pedidovendamaterial.getQuantidade()));
						}
					}
					
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null && 
								materialrelacionado.getQuantidade() > 0){	
							Pedidovendamaterial pvm = new Pedidovendamaterial();
							pvm.setMaterial(materialrelacionado.getMaterialpromocao());
							pvm.setUnidademedida(materialrelacionado.getMaterialpromocao().getUnidademedida());
							pvm.setQuantidade(materialrelacionado.getQuantidade() * pedidovendamaterial.getQuantidade());
							if(materialrelacionado.getValorvenda() != null){
								pvm.setPreco(materialrelacionado.getValorvenda().getValue().doubleValue());
								if(totalKit != null && 
										totalKit.getValue().doubleValue() > 0 && 
										pedidovendamaterial.getDesconto() != null && 
										pedidovendamaterial.getDesconto().getValue().doubleValue() > 0){
									pvm.setDesconto(new Money(pvm.getPreco() * pvm.getQuantidade() * 100d / totalKit.getValue().doubleValue() * pedidovendamaterial.getDesconto().getValue().doubleValue() / 100d));
								}
							}else {
								pvm.setPreco(0d);
								pvm.setDesconto(new Money());
							}
							pvm.setDtprazoentrega(pedidovendamaterial.getDtprazoentrega());
							pvm.setObservacao(pedidovendamaterial.getObservacao());
							
							pvm.setTotal(vendaService.getValortotal(pvm.getPreco(), 
									pvm.getQuantidade(),
									pvm.getDesconto(), 
									pvm.getMultiplicador(),
									pvm.getOutrasdespesas(), pvm.getValorSeguro()));
							
							listaPedidovendamaterial.add(pvm);
							adicionarItem = false;
						}
					}
				}
			}
			if(adicionarItem){
				listaPedidovendamaterial.add(pedidovendamaterial);
			}
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForAutocompletePedidovendacomodato(String q, Integer cdpedidovenda)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findForAutocompletePedidovendacomodato(String q){
		Integer cdpedidovenda = null;
		try {
			cdpedidovenda = Integer.parseInt(NeoWeb.getRequestContext().getParameter("cdpedidovenda"));
		} catch (Exception e) {} 
		return this.findForAutocompletePedidovendacomodato(q, cdpedidovenda);
	}
	
	public List<Pedidovenda> findForAutocompletePedidovendacomodato(String q, Integer cdPedidoVenda){
		return pedidovendaDAO.findForAutocompletePedidovendacomodato(q, cdPedidoVenda);
	}
	
	public List<PedidoVendaWSBean> findForAutocompletePedidovendacomodatoWSVenda(String q, Integer cdPedidoVenda){
		List<PedidoVendaWSBean> lista = new ArrayList<PedidoVendaWSBean>();
		for(Pedidovenda pedido: this.findForAutocompletePedidovendacomodato(q, cdPedidoVenda)){
			PedidoVendaWSBean bean = new PedidoVendaWSBean();
			bean.setId(Util.strings.toStringIdStyled(pedido));
			bean.setValue(pedido.getCdpedidovenda());
			bean.setCliente(ObjectUtils.translateEntityToGenericBean(pedido.getCliente()));
			
			lista.add(bean);
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#isMaterialReservadoOutrosPedidos(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isMaterialReservadoOutrosPedidos(Pedidovenda pedidovenda, Boolean verificarQtdeDisponivel){
		return pedidovendaDAO.isMaterialReservadoOutrosPedidos(pedidovenda, verificarQtdeDisponivel);
	}
	
	public Boolean isMaterialReservadoOutrosPedidos(Pedidovenda pedidovenda){
		return pedidovendaDAO.isMaterialReservadoOutrosPedidos(pedidovenda, false);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findReservasMateriais(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovenda> findReservasMateriais(String whereIn) {
		return pedidovendaDAO.findReservasMateriais(whereIn);
	}
	
	/**
	 * M�todo que verifica se a qtde solicitada � maior ou igual a qtde reservada
	 *
	 * @param request
	 * @param pedidovenda
	 * @param aprovandoAndTipopedidovalidaestoque
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isQtdeSolicitadaMaiorOrIgualQtdeReservada(WebRequestContext request, Pedidovenda pedidovenda, boolean aprovandoAndTipopedidovalidaestoque) {
		Boolean isqtdeMaiorIgual = false;
		String bloquearvendareservada = parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEAR_VENDA_RESERVADA);
		Boolean localPermiteEstoqueNegativo = false;
		Localarmazenagem localarmazenagem = null;
		if(pedidovenda.getLocalarmazenagem() != null){
			localarmazenagem = localarmazenagemService.loadWithInformacoesEstoque(pedidovenda.getLocalarmazenagem());
			if(localarmazenagem != null){
				localPermiteEstoqueNegativo = Boolean.TRUE.equals(localarmazenagem.getPermitirestoquenegativo());
			}
		}
		
		if(("TRUE".equalsIgnoreCase(bloquearvendareservada) || aprovandoAndTipopedidovalidaestoque) &&
				pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && 
				!pedidovenda.getListaPedidovendamaterial().isEmpty() &&
				!pedidovenda.getIsNotVerificarestoquePedidovenda() &&
				!pedidovenda.getPermitirvendasemestoque() &&
				!localPermiteEstoqueNegativo){
			Material material;
			Double entrada = 0d;
			Double saida = 0d;
			Double qtddisponivel = 0d;
			Double qtddisponivelproducao = null;
			Integer qtde = null;
			Double qtdereservada = 0.0;
			Double qtdereservadaItemKit = 0.0;
			boolean buscarQtdeReserva = true;
			
			Double qtdeacimaestoque = 0d;
			String msgacimaestoque = null;

			if(localarmazenagem != null){
				if(localarmazenagem.getQtdeacimaestoque() != null){
					qtdeacimaestoque = localarmazenagem.getQtdeacimaestoque() / 100;
				}
				if(qtdeacimaestoque > 0){
					if(org.apache.commons.lang.StringUtils.isNotEmpty(localarmazenagem.getMsgacimaestoque())){
						msgacimaestoque = localarmazenagem.getMsgacimaestoque();
					}
				}
			}
			
			Boolean permiteMaterialMestreVenda = pedidovenda.getPedidovendatipo() != null ? pedidovendatipoService.verificarPedidoVendaTipoPermiteMaterialMestreVenda(pedidovenda.getPedidovendatipo()) : Boolean.FALSE;
			for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
				entrada = 0d;
				saida = 0d;
				qtddisponivel = 0d;
				qtddisponivelproducao = null;
				qtde = null;
				qtdereservadaItemKit = 0d;
				qtdereservada = null;
				
				if(pvm.getMaterial() != null && pvm.getMaterial().getCdmaterial() != null && pvm.getQuantidade() != null){
					material = materialService.loadWithCodFlex(pvm.getMaterial().getCdmaterial());
					if((material.getPatrimonio() != null && material.getPatrimonio()) ||
							(material.getServico() != null && material.getServico() && (material.getVendapromocional() == null || !material.getVendapromocional())) 
							|| (Boolean.TRUE.equals(permiteMaterialMestreVenda) && pvm.getMaterialmestre() == null && material.getMaterialmestregrade() == null && materialService.isControleMaterialitemgrade(material))) continue;
					
					Unidademedida unidademedida = material.getUnidademedida();
					
					if(material.getVendapromocional() != null && material.getVendapromocional()){
						buscarQtdeReserva = false;
						material = materialService.loadMaterialPromocionalComMaterialrelacionado(pvm.getMaterial().getCdmaterial());
						if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
							for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
								if(materialrelacionado.getMaterialpromocao() != null && !Boolean.TRUE.equals(materialrelacionado.getMaterialpromocao().getServico())){	
									entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(materialrelacionado.getMaterialpromocao(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
									if (entrada == null){
										entrada = 0d;
									}
									saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(materialrelacionado.getMaterialpromocao(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
									if (saida == null){
										saida = 0d;
									}
									String whereInNotReserva = null;
									if(Util.strings.isNotEmpty(pvm.getWhereInOSVM())){
										whereInNotReserva = ordemservicoveterinariamaterialService.getWhereInReserva(ordemservicoveterinariamaterialService.findWithReserva(pvm.getWhereInOSVM()));
									}
									qtdereservadaItemKit = pedidovendamaterialService.getQtdeReservada(materialrelacionado.getMaterialpromocao(), pedidovenda, pvm.getLoteestoque(), whereInNotReserva);
									
									qtddisponivel = (entrada-saida-(qtdereservadaItemKit != null ? qtdereservadaItemKit : 0d))/materialrelacionado.getQuantidade();
									if(qtde == null)
										qtde = qtddisponivel.intValue();
									else if(qtddisponivel.intValue() < qtde){
										qtde = qtddisponivel.intValue();
									}
								}
							}
						}
						if(qtde == null){
							continue;
						}
					}else if(pvm.getMaterial().getProducao() != null && pvm.getMaterial().getProducao()){
						entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(pvm.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
						saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(pvm.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
						
						if (entrada == null)
							entrada = new Double(0);
						if (saida == null)
							saida = new Double(0);
		
						qtddisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
						
						if(!unidademedida.equals(pvm.getUnidademedida())){
							if(pvm.getFatorconversao() != null && pvm.getFatorconversao() > 0)
								qtddisponivel = qtddisponivel * pvm.getFatorconversaoQtdereferencia();
						}
						if (pvm.getQuantidade() <= qtddisponivel){
							qtddisponivelproducao = qtddisponivel;
						}else if(!materialService.existProducaoetapa(material)){
							material = materialService.loadMaterialComMaterialproducao(pvm.getMaterial());
							if(material != null){
								Venda venda = new Venda();
								venda.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
								venda.setEmpresa(pedidovenda.getEmpresa());
								qtddisponivelproducao = vendaService.getQtdeDisponivelMaterialproducao(material, venda);
							}
						}else {
							continue;
						}
					}else{
						entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(pvm.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
						if (entrada == null){
							entrada = 0d;
						}
						saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(pvm.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pvm.getLoteestoque());
						if (saida == null){
							saida = 0d;
						}
					}
					
					if(qtddisponivelproducao != null){
						qtddisponivel = qtddisponivelproducao;
					}else if(qtde != null){
						qtddisponivel = qtde.doubleValue();
					}else {
						qtddisponivel = entrada-saida;
					}
					
					if(buscarQtdeReserva){
						String whereInNotReserva = null;
						if(Util.strings.isNotEmpty(pvm.getWhereInOSVM())){
							whereInNotReserva = ordemservicoveterinariamaterialService.getWhereInReserva(ordemservicoveterinariamaterialService.findWithReserva(pvm.getWhereInOSVM()));
						}
						Reserva reserva = reservaService.loadByPedidovendamaterial(pvm.getCdpedidovendamaterial(), pvm.getMaterial());
						if(reserva != null){
							whereInNotReserva = Util.strings.isNotEmpty(whereInNotReserva) ? whereInNotReserva + "," + reserva.getCdreserva() : reserva.getCdreserva().toString();
						}
						if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
							List<Reserva> listaReserva = reservaService.findByPedidovenda(pedidovenda, pvm.getMaterial());
							if(listaReserva != null && !listaReserva.isEmpty()){
								whereInNotReserva = Util.strings.isNotEmpty(whereInNotReserva) ? whereInNotReserva + "," + CollectionsUtil.listAndConcatenate(listaReserva, "cdreserva", ",") : CollectionsUtil.listAndConcatenate(listaReserva, "cdreserva", ",");
							}
						}
						qtdereservada = pedidovendamaterialService.getQtdeReservada(pvm.getMaterial(), pedidovenda, pvm.getLoteestoque(), whereInNotReserva);
					}
					if(qtdereservada == null) qtdereservada = 0.0;
					
					if(qtddisponivel != null && qtdereservada != null){
						if(((qtddisponivel - qtdereservada) - pvm.getQuantidade()) < 0){
							if(qtdeacimaestoque != null && qtdeacimaestoque > 0){
								Double qtddisponivelAcimaestoque = qtddisponivel * qtdeacimaestoque;
								if((qtdereservada + pvm.getQuantidade()) > (qtddisponivelAcimaestoque + qtddisponivel)){
									isqtdeMaiorIgual = true;
									request.addError("Venda n�o permitida. Valor limite do produto acima do estoque zerado. Material: " + pvm.getMaterial().getNome() + ". " + msgacimaestoque);
								}								
							}else {
								isqtdeMaiorIgual = true;
								if(aprovandoAndTipopedidovalidaestoque){
									request.addError("O pedido n�o pode ser aprovado porque o item " + pvm.getMaterial().getNome() + " n�o possui quantidade suficiente em estoque. " + vendaService.getNumeroLote(pvm.getLoteestoque()));
								}else{
									request.addError("A quantidade solicitada � maior do que a quantidade " + (qtdereservada > 0 ? "reservada" : "dispon�vel" ) +". Material: " + pvm.getMaterial().getNome() + vendaService.getNumeroLote(pvm.getLoteestoque()));	
								}
							}
						}
					}
				}
			}
			
		}
		return isqtdeMaiorIgual;
	}
	
	/**
	 * M�todo que faz o processo de cancelamento do pedido de venda
	 *
	 * @param whereInPedidoCancelados
	 * @param listaPedidovendaCancelado
	 * @param justificativa
	 * @author Luiz Fernando
	 */
	public void cancelamentoByPedidovenda(String whereInPedidoCancelados, List<Pedidovenda> listaPedidovendaCancelado, String justificativa){
		this.updateCancelamento(whereInPedidoCancelados.toString());
		for(Pedidovenda pedidovenda: listaPedidovendaCancelado){
			this.insertPedidoCanceladoTabelaSincronizacaoEcommerce(pedidovenda);
		}
		
		boolean estornarValecompra = false;
		String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
		if("PEDIDO".equalsIgnoreCase(param_utilizacao_valecompra)){
			estornarValecompra = true;
		}
	
		
		Pedidovendahistorico pedidovendahistorico;
		for (Pedidovenda pv : listaPedidovendaCancelado) {
			
			pedidovendahistorico = new Pedidovendahistorico();
			
			pedidovendahistorico.setPedidovenda(pv);
			pedidovendahistorico.setAcao("Cancelado");
			pedidovendahistorico.setObservacao(justificativa);
			
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			
			documentocomissaoService.deleteDocumentocomissaoByPedidovendaCancelado(pv);
			
			if(estornarValecompra || pedidovendapagamentoService.existeDocumento(pv)){
				if(pv.getValorusadovalecompra() != null && pv.getValorusadovalecompra().getValue().doubleValue() > 0 && !Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(pv.getPedidovendasituacao())){
					Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
					Valecompraorigem valecompraorigem = new Valecompraorigem();
					valecompraorigem.setPedidovenda(pv);
					
					Valecompra valecompra = new Valecompra();
					valecompra.setCliente(pv.getCliente());
					valecompra.setData(SinedDateUtils.currentDate());
					valecompra.setIdentificacao("Cancelamento do pedido de venda " + pv.getCdpedidovenda());
					valecompra.setListaValecompraorigem(listaValecompraorigem);
					valecompra.setTipooperacao(Tipooperacao.TIPO_CREDITO);
					valecompra.setValor(pv.getValorusadovalecompra());
					
					valecompraService.saveOrUpdate(valecompra);
				}
			}
			
			Valecompra valecompraCredito = valecompraService.findByPedidovenda(pv, Tipooperacao.TIPO_CREDITO);
			if(valecompraCredito != null) {
				Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
				Valecompraorigem valecompraorigem = new Valecompraorigem();
				valecompraorigem.setPedidovenda(pv);
				
				Valecompra valecompra = new Valecompra();
				valecompra.setCliente(pv.getCliente());
				valecompra.setData(SinedDateUtils.currentDate());
				valecompra.setIdentificacao("Cancelamento do pedido de venda " + pv.getCdpedidovenda());
				valecompra.setListaValecompraorigem(listaValecompraorigem);
				valecompra.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				valecompra.setValor(valecompraCredito.getValor());
				
				valecompraService.saveOrUpdate(valecompra);
			}
			
			StringBuilder whereInContasBaixadas = new StringBuilder();
			Documento documento;
			List<Documentoorigem> listadocumentoorigem = documentoorigemService.getListaDocumentosDePedidovenda(pv);
			for (Documentoorigem documentoorigem : listadocumentoorigem) {
				documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
				if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
					whereInContasBaixadas.append(documento.getCddocumento()).append(",");
				}
			}
			for (Documentoorigem documentoorigem : listadocumentoorigem) {
				documento = documentoService.carregaDocumento(documentoorigem.getDocumento());
				if(documento.getDocumentoacao().equals(Documentoacao.DEFINITIVA) || 
						documento.getDocumentoacao().equals(Documentoacao.PREVISTA)){
					documento.setDocumentoacao(Documentoacao.CANCELADA);
					documentoService.updateDocumentoacao(documento);
					Documentohistorico documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
					documentohistoricoService.saveOrUpdate(documentohistorico);
				} 
			}
			
			deleteChequeAposCancelamentoPedido(pv);
			vendaorcamentoService.updateVendaorcamentoByCancelamentoOrigemPedidovenda(whereInPedidoCancelados.toString());
			ordemservicoveterinariaService.updateItemFaturadoAposPedidoCancelado(whereInPedidoCancelados);
			
			List<Movimentacaoestoque> listaMovimentacaoestoque = null;
			for (Pedidovendamaterial vendamaterial : pv.getListaPedidovendamaterial()) {
				listaMovimentacaoestoque = movimentacaoestoqueService.findByPedidovenda(pv ,vendamaterial.getMaterial());
				if (SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
					movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
				}
				materialService.reutilizarMaterialComEstoque(vendamaterial.getMaterial(), pv.getEmpresa(), pv.getLocalarmazenagem());
				
				if(SinedUtil.isRecapagem()){
					if(vendamaterial.getGarantiareformaitem() != null){
						garantiareformaitemService.retiraVinculoGarantiaPedidovendamaterial(vendamaterial);
						vendamaterial.setGarantiareformaitem(null);
						pedidovendamaterialService.limpaGarantiareformaitem(vendamaterial);
					}
				}
				Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(vendamaterial.getMaterial(),
																				vendamaterial.getUnidademedida(),
																				vendamaterial.getQuantidade());
				
				Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendamaterial.getMaterial().getCdmaterial());
				if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialrelacionado())){
					Double qtdeItemDoKit = 0D;
					for(Materialrelacionado mr: material.getListaMaterialrelacionado()){
						qtdeItemDoKit = quantidade * mr.getQuantidade();
						reservaService.desfazerReserva(vendamaterial, mr.getMaterialpromocao(), qtdeItemDoKit);
					}
				}else{
					reservaService.desfazerReserva(vendamaterial, vendamaterial.getMaterial(), quantidade);
				}
				cancelaAtualizacaoPreco(vendamaterial);
			}
		}
	}
	
	public void deleteChequeAposCancelamentoPedido(Pedidovenda pedidovenda){
		List<Pedidovendapagamento> listaPedidovendapagamento = pedidovendapagamentoService.findByPedidovenda(pedidovenda);
		if(listaPedidovendapagamento != null && !listaPedidovendapagamento.isEmpty()){
			List<Cheque> chequesDelete = new ArrayList<Cheque>();
			for(Pedidovendapagamento pvp: listaPedidovendapagamento){
				if(pvp.getCheque() != null && pvp.getCheque().getCdcheque() != null){
					chequesDelete.add(pvp.getCheque());
				}
			}
			if(!chequesDelete.isEmpty()){
				String idsPagamentos = CollectionsUtil.listAndConcatenate(listaPedidovendapagamento, "cdpedidovendapagamento", ",");
				pedidovendapagamentoService.updateReferenciaCheque(idsPagamentos);
				for(Cheque cheque: chequesDelete){
					try {					
						chequeService.delete(cheque);
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/**
	 * Webservice para verificar e exist�ncia de venda ou pedido de venda
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/02/2014
	 */
	public boolean existePedidovendaWebservice(ExistePedidovendaBean bean) {
		TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
		
		Empresa empresa = null;
		if(bean.getEmpresa_cnpj() != null && !bean.getEmpresa_cnpj().trim().equals("")){
			try{
				empresa = empresaService.findByCnpj(new Cnpj(bean.getEmpresa_cnpj()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(empresa == null){
			empresa = empresaService.loadPrincipal();
		}
		
		if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
			return vendaService.haveVendaByIdentificadorEmpresa(bean.getIdentificador(), empresa, bean.getVerificarCancelado());
		} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA) || 
					tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)){
			return this.havePedidovendaByIdentificadorEmpresa(bean.getIdentificador(), empresa, bean.getVerificarCancelado());
		}else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.ORCAMENTO)){
			return vendaorcamentoService.haveOrcamentoByIdentificadorEmpresa(bean.getIdentificador(), empresa, bean.getVerificarCancelado());
		}
		
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param identificador
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/02/2014
	 */
	protected boolean havePedidovendaByIdentificadorEmpresa(String identificador, Empresa empresa, boolean verificarCancelado) {
		return pedidovendaDAO.havePedidovendaByIdentificadorEmpresa(identificador, empresa, verificarCancelado);
	}
	
	public List<ParcelasPedidovendaBean> consultarParcelasPedidovendaWebservice(ExistePedidovendaBean bean) {
		TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
		
		Empresa empresa = null;
		if(bean.getEmpresa_cnpj() != null && !bean.getEmpresa_cnpj().trim().equals("")){
			try{
				empresa = empresaService.findByCnpj(new Cnpj(bean.getEmpresa_cnpj()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(empresa == null){
			empresa = empresaService.loadPrincipal();
		}
		
		if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
			return vendaService.getParcelasVendaByIdentificador(bean.getIdentificador(), empresa);
		} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA)){
			return this.getParcelasVendaByIdentificador(bean.getIdentificador(), empresa);
		}
		
		return null;
	}
	
	public List<ParcelasPedidovendaBean> getParcelasVendaByIdentificador(String identificador, Empresa empresa) {
		List<Pedidovenda> listaPedidovenda = this.findByEmpresaIdentificador(empresa.getCnpj().getValue(), identificador);
		
		if(listaPedidovenda == null || listaPedidovenda.size() == 0){
			throw new SinedException("Nenhum pedido encontrada com o identificador informado.");
		} else if(listaPedidovenda.size() > 1){
			throw new SinedException("Foi encontrada mais de um pedido com o identificador informado.");
		}
		
		List<Documento> listaDocumento = contareceberService.findByPedidovenda(CollectionsUtil.listAndConcatenate(listaPedidovenda, "cdpedidovenda", ","));
		List<ParcelasPedidovendaBean> listaParcelas = new ArrayList<ParcelasPedidovendaBean>();
		
		for (Documento documento : listaDocumento) {
			ParcelasPedidovendaBean bean = new ParcelasPedidovendaBean();
			bean.setCddocumento(documento.getCddocumento());
			bean.setDtvencimento(SinedDateUtils.toString(documento.getDtvencimento()));
			bean.setValor(SinedUtil.descriptionDecimal(documento.getValorDouble(), true));
			
			listaParcelas.add(bean);
		}
		
		return listaParcelas;
	}
	
	/**
	 * Cria o pedido de venda ou venda e cliente juntos para o webservice.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public CriarPedidovendaRespostaBean criarPedidovendaClienteWebservice(CriarPedidovendaClienteBean bean) {
		InserirClienteBean clienteBean = new InserirClienteBean();
		
		clienteBean.setNome(bean.getCliente_nome());
		clienteBean.setEmail(bean.getCliente_email());
		clienteBean.setCdcategoria(bean.getCliente_cdcategoria());
		clienteBean.setSubstituircategoria(bean.getCliente_substituircategoria());
		clienteBean.setCpf(bean.getCliente_cpf());
		clienteBean.setSexo(bean.getCliente_sexo());
		clienteBean.setRazaosocial(bean.getCliente_razaosocial());
		clienteBean.setCnpj(bean.getCliente_cnpj());
		clienteBean.setInscricaomunicipal(bean.getCliente_inscricaomunicipal());
		clienteBean.setInscricaoestadual(bean.getCliente_inscricaoestadual());
		clienteBean.setSite(bean.getCliente_site());
		clienteBean.setNomecontato(bean.getCliente_nomecontato());
		clienteBean.setTelefonecontato(bean.getCliente_telefonecontato());
		clienteBean.setCelularcontato(bean.getCliente_celularcontato());
		clienteBean.setTelefone(bean.getCliente_telefone());
		clienteBean.setCelular(bean.getCliente_celular());
		clienteBean.setEnderecologradouro(bean.getCliente_enderecologradouro());
		clienteBean.setEndereconumero(bean.getCliente_endereconumero());
		clienteBean.setEnderecocomplemento(bean.getCliente_enderecocomplemento());
		clienteBean.setEnderecobairro(bean.getCliente_enderecobairro());
		clienteBean.setEnderecocep(bean.getCliente_enderecocep());
		clienteBean.setEnderecomunicipio(bean.getCliente_enderecomunicipio());
		clienteBean.setEnderecouf(bean.getCliente_enderecouf());
		
		clienteService.criarClienteWebservice(clienteBean);
		
		if(clienteBean != null && clienteBean.getCdendereco() != null){
			bean.setCliente_cdendereco(clienteBean.getCdendereco());
		}
		if(bean.getCliente_cpf() != null && !bean.getCliente_cpf().equals("")){
			bean.setCliente_cpfcnpj(bean.getCliente_cpf());
		} else if(bean.getCliente_cnpj() != null && !bean.getCliente_cnpj().equals("")){
			bean.setCliente_cpfcnpj(bean.getCliente_cnpj());
		}
		
		return this.criarPedidovendaVendaWebservice(bean);
	}
	
	/**
	 * Faz a cria��o do pedido de venda ou venda atrav�s de webservice
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public CriarPedidovendaRespostaBean criarPedidovendaVendaWebservice(CriarPedidovendaBean bean) {
		TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
		CriarPedidovendaRespostaBean respostaBean = new CriarPedidovendaRespostaBean();
		
		if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
			respostaBean.setCdvenda(vendaService.criarVendaWebservice(bean));
		} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA) || 
					tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)){
			respostaBean.setCdpedidovenda(this.criarPedidovendaWebservice(bean, tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)));
		} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.ORCAMENTO)){
			respostaBean.setCdvendaorcamento(vendaorcamentoService.criarOrcamentoWebService(bean));
		} else throw new RuntimeException("Tipo de cria��o de pedido de venda n�o previsto.");
		
		return respostaBean;
	}
	
	/**
	 * Realiza o cancelamento de venda ou pedido de venda atrav�s de webservice
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 02/07/2013
	 */
	public void cancelarPedidovendaVendaWebservice(CancelarPedidovendaBean bean) {
		TipoCriacaoPedidovenda tipoCriacaoPedidovenda = SinedUtil.getTipoCriacaoPedidovenda(bean.getTipo_criacao());
		
		if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.VENDA)){
			vendaService.cancelarVendaWebservice(bean);
		} else if(tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA) || 
					tipoCriacaoPedidovenda.equals(TipoCriacaoPedidovenda.PEDIDOVENDA_BONIFICACAO)){
			this.cancelarPedidovendaWebservice(bean);
		}
	}
	
	protected void cancelarPedidovendaWebservice(CancelarPedidovendaBean bean) {
		String identificador = bean.getId();
		Cnpj cnpj = null; 
		
		if(org.apache.commons.lang.StringUtils.isNotEmpty(bean.getEmpresa_cnpj())){
			try {
				cnpj = new Cnpj(bean.getEmpresa_cnpj());
			} catch (Exception e) {}
		}
		
		if(identificador == null || identificador.equals("")){
			throw new SinedException("Favor informar o identificador do pedido de venda.");
		}
		
		List<Pedidovenda> lista = this.findByIdentificadorCnpj(identificador, cnpj);
		
		if(lista == null || lista.size() == 0){
			throw new SinedException("Nenhum pedido de venda encontrado com o identificador informado.");
		} else if(lista.size() > 1){
			throw new SinedException("Foi encontrada mais de um pedido de venda com o identificador informado.");
		}
		
		String whereIn = lista.get(0).getCdpedidovenda().toString();
		List<Pedidovenda> listaPedidovenda = this.findForCancelamento(whereIn);
		this.cancelamentoByPedidovenda(whereIn, listaPedidovenda, "");
	}
	
	public List<Pedidovenda> findByIdentificador(String identificador) {
		return pedidovendaDAO.findByIdentificador(identificador);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findByIdentificadorCnpj(String identificador, Cnpj cnpj)
	*
	* @param identificador
	* @param cnpj
	* @return
	* @since 01/06/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovenda> findByIdentificadorCnpj(String identificador, Cnpj cnpj) {
		return pedidovendaDAO.findByIdentificadorCnpj(identificador, cnpj);
	}
	
	/**
	 * M�todo que retorna a lista de pedidovendamaterial com os valores de pre�o, valorminimo e valormaximo
	 * 
	 * @param pedidovenda
	 * @return
	 */
	public List<Pedidovendamaterial> getListaPedidovendamaterialForCriarTabela(Pedidovenda pedidovenda) {
		List<Pedidovendamaterial> listaVendamaterial = new ArrayList<Pedidovendamaterial>();
		if(pedidovenda != null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
			Pedidovendamaterial vendamaterial;
			for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
				if(pvm.getMaterial() != null && pvm.getMaterial().getCdmaterial() != null){
					vendamaterial = new Pedidovendamaterial();
					
					vendamaterial.setMaterial(new Material(pvm.getMaterial().getCdmaterial()));
					vendamaterial.setUnidademedida(pvm.getUnidademedida());
					vendamaterial.setPreco(pvm.getPreco());
					vendamaterial.getMaterial().setValorvendaminimo(pvm.getMaterial().getValorvendaminimo());
					vendamaterial.getMaterial().setValorvendamaximo(pvm.getMaterial().getValorvendamaximo());
					vendamaterial.setDesconto(pvm.getDesconto());
					vendamaterial.setPercentualdesconto(pvm.getPercentualdesconto());
					vendamaterial.setCdpedidovendamaterial(pvm.getCdpedidovendamaterial());
					
					vendaService.calcularMarkup(pedidovenda, pvm, false);
					
					listaVendamaterial.add(vendamaterial);
				}
			}
		}
		return listaVendamaterial;
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public void cancelamentoDiario(){
		pedidovendaDAO.cancelamentoDiario();
	}
	
	/**
	 * M�todo que ajusta a data das parcelas para gera��o de documento. Gerar documento na emiss�o da nota
	 *
	 * @param dtemissao
	 * @param pedidovenda
	 * @author Luiz Fernando
	 * @since 21/09/2013
	 */
	public void ajustaDataParcelaPedidovenda(java.sql.Date dtemissao, Pedidovenda pedidovenda) {
		if(dtemissao != null && pedidovenda != null && pedidovenda.getDtpedidovenda() != null && pedidovenda.getListaPedidovendapagamento() != null &&
				!pedidovenda.getListaPedidovendapagamento().isEmpty()){
			
			Collections.sort(pedidovenda.getListaPedidovendapagamento(), new Comparator<Pedidovendapagamento>(){
				public int compare(Pedidovendapagamento item1, Pedidovendapagamento item2) {
					try{
						if(SinedDateUtils.equalsIgnoreHour(item1.getDataparcela(), item2.getDataparcela())){
							return 0;
						}else if(SinedDateUtils.beforeOrEqualIgnoreHour(item1.getDataparcela(), item2.getDataparcela())){
							return -1;
						}else {
							return 1;
						}
					} catch (Exception e) {
						return 0;
					}
				}
			});
			
			java.sql.Date dtPrimeira = pedidovenda.getListaPedidovendapagamento().get(0).getDataparcela();
			java.sql.Date dtAnteriorNovaparcela = dtPrimeira;
			java.sql.Date dtParcelaAnterior = dtPrimeira;
			Integer diferencaDias = SinedDateUtils.diferencaDias(dtemissao, pedidovenda.getDtpedidovenda());
			boolean primeiraparcela = true;
			
			if(dtPrimeira != null && diferencaDias > 0){
				diferencaDias = SinedDateUtils.diferencaDias(dtPrimeira, pedidovenda.getDtpedidovenda());
				pedidovenda.setDtpedidovendaEmissaonota(dtemissao);
				for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
					if(pedidovendapagamento.getDataparcela() != null){
						if(primeiraparcela){
							primeiraparcela = false;
							dtParcelaAnterior = new java.sql.Date(pedidovendapagamento.getDataparcela().getTime());
							diferencaDias = SinedDateUtils.diferencaDias(dtParcelaAnterior,pedidovenda.getDtpedidovenda());
							pedidovendapagamento.setDataparcela(SinedDateUtils.addDiasData(dtemissao, diferencaDias));
							dtAnteriorNovaparcela = new java.sql.Date(pedidovendapagamento.getDataparcela().getTime());
						}else {
							diferencaDias = SinedDateUtils.diferencaDias(pedidovendapagamento.getDataparcela(),dtParcelaAnterior);
							dtParcelaAnterior = new java.sql.Date(pedidovendapagamento.getDataparcela().getTime());
							pedidovendapagamento.setDataparcela(SinedDateUtils.addDiasData(dtAnteriorNovaparcela, diferencaDias));
							dtAnteriorNovaparcela = new java.sql.Date(pedidovendapagamento.getDataparcela().getTime());
						}
					}
				}
			}
		}
	}
	

	@SuppressWarnings("rawtypes")
	public void ajustaValorParcelaPedidovenda(Pedidovenda pedidovenda, Money valorTotalNota, List listaDuplicata, boolean recalcularValoresParcelas, Boolean naoatualizarvencimento) {
		if(pedidovenda != null && valorTotalNota != null){
			if(recalcularValoresParcelas){
				pedidovenda.setValor(SinedUtil.round(valorTotalNota.getValue().doubleValue(),2));
			}else {
				pedidovenda.setValor(pedidovenda.getTotalvenda().getValue().doubleValue());
			}
			AjaxRealizarVendaPagamentoBean bean = this.pagamento(pedidovenda, null);
			
			if(bean != null && bean.getLista() != null && !bean.getLista().isEmpty()){
				Notafiscalprodutoduplicata nfpDuplicata;
				Notafiscalservicoduplicata nfsDuplicata;
				int i = 0;
				for(AjaxRealizarVendaPagamentoItemBean item : bean.getLista()){
					if(pedidovenda.getListaPedidovendapagamento().get(i) != null){
						if(naoatualizarvencimento == null || !naoatualizarvencimento){
							if(item.getDtparcela() != null){
								try {
									pedidovenda.getListaPedidovendapagamento().get(i).setDataparcela(SinedDateUtils.stringToDate(item.getDtparcela()));
								} catch (Exception e) {}
							}
						}
						if(item.getValor().contains(".") && item.getValor().contains(",")){
							item.setValor(item.getValor().replace(".", ""));
						}
						pedidovenda.getListaPedidovendapagamento().get(i).setValororiginal(new Money(new Double(item.getValor().replace(",", "."))));
						if(recalcularValoresParcelas){
							if(listaDuplicata != null && listaDuplicata.size() == bean.getLista().size()){
								try {
									if(listaDuplicata.get(i) instanceof Notafiscalservicoduplicata){
										nfsDuplicata = (Notafiscalservicoduplicata) listaDuplicata.get(i);
										
										String numero = nfsDuplicata.getNumero();
										if(numero != null && !numero.equals("")){
											pedidovenda.getListaPedidovendapagamento().get(i).setNumeroEmissaonota(numero);
										}
										Money valor = nfsDuplicata.getValor();
										if(valor != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setValororiginal(valor);
										}
										Date dataparcela = nfsDuplicata.getDtvencimento();
										if(dataparcela != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setDataparcela(new java.sql.Date(dataparcela.getTime()));
										}
										
										Documentotipo documentotipo = nfsDuplicata.getDocumentotipo();
										if(documentotipo != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setDocumentotipo(documentotipo);
										}
									}else if(listaDuplicata.get(i) instanceof Notafiscalprodutoduplicata){
										nfpDuplicata = (Notafiscalprodutoduplicata) listaDuplicata.get(i);
										
										String numero = nfpDuplicata.getNumero();
										if(numero != null && !numero.equals("")){
											pedidovenda.getListaPedidovendapagamento().get(i).setNumeroEmissaonota(numero);
										}
										Money valor = nfpDuplicata.getValor();
										if(valor != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setValororiginal(valor);
										}
										Date dataparcela = nfpDuplicata.getDtvencimento();
										if(dataparcela != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setDataparcela(new java.sql.Date(dataparcela.getTime()));
										}
										
										Documentotipo documentotipo = nfpDuplicata.getDocumentotipo();
										if(documentotipo != null){
											pedidovenda.getListaPedidovendapagamento().get(i).setDocumentotipo(documentotipo);
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
					i++;
				}
			}
		}
	}
	
	public void ajustePedidovendapagamento(Pedidovenda pedidovenda) {
		if (pedidovenda != null && pedidovenda.getListaPedidovendapagamento() != null && !pedidovenda.getListaPedidovendapagamento().isEmpty()) {
			Money totalparcela = pedidovenda.getTotalparcela();
			if (totalparcela != null && totalparcela.getValue().doubleValue() > 0) {
				Money diferenca = pedidovenda.getTotalvendaMaisImpostos().divide(totalparcela);
				for (Pedidovendapagamento vendapagamento : pedidovenda.getListaPedidovendapagamento()) {
					if (diferenca != null && !diferenca.equals(0))
						vendapagamento.setValororiginal(vendapagamento.getValororiginal().multiply(diferenca));
				}
			}
		}
	}
	
	/**
	 * M�todo que converte a Lista de pagamento ajax para lista de pedidovendapagamento 
	 *
	 * @param ajaxRealizarVendaPagamentoBean
	 * @return
	 * @throws ParseException
	 * @author Luiz Fernando
	 * @since 23/10/2013
	 */
	public List<Pedidovendapagamento> converterForPedidovendapagamento(AjaxRealizarVendaPagamentoBean ajaxRealizarVendaPagamentoBean) throws ParseException{
		List<Pedidovendapagamento> listaPedidovendapagamento = null;
		if(ajaxRealizarVendaPagamentoBean != null && ajaxRealizarVendaPagamentoBean.getLista() != null && 
				!ajaxRealizarVendaPagamentoBean.getLista().isEmpty()){
			Pedidovendapagamento pedidovendapagamento;
			listaPedidovendapagamento = new ArrayList<Pedidovendapagamento>();
			for(AjaxRealizarVendaPagamentoItemBean ajaxRealizarVendaPagamentoItemBean: ajaxRealizarVendaPagamentoBean.getLista()){
				pedidovendapagamento = new Pedidovendapagamento();
				if(ajaxRealizarVendaPagamentoItemBean.getDtparcela() != null){
					pedidovendapagamento.setDataparcela(SinedDateUtils.stringToDate(ajaxRealizarVendaPagamentoItemBean.getDtparcela()));
				}
				if(ajaxRealizarVendaPagamentoItemBean.getValor() != null){
					pedidovendapagamento.setValororiginal(new Money(new Double(ajaxRealizarVendaPagamentoItemBean.getValor().replace(".", "").replace(",", "."))));
				}
				listaPedidovendapagamento.add(pedidovendapagamento);
			}
		}
		return listaPedidovendapagamento;
	}
	
	/**
	 * M�todo que gerar as parcelas do pedido de venda
	 *
	 * @param pedidovenda
	 * @param paramDtprazoentrega
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public AjaxRealizarVendaPagamentoBean pagamento(Pedidovenda pedidovenda, String paramDtprazoentrega){
		List<Prazopagamentoitem> listaPagItem = prazopagamentoitemService.findByPrazo(pedidovenda.getPrazopagamento());
		Double juros = prazopagamentoService.loadAll(pedidovenda.getPrazopagamento()).getJuros();
		
		Prazopagamento pg = prazopagamentoService.load(pedidovenda.getPrazopagamento());
		Tipoiniciodata tipoiniciodata = Tipoiniciodata.DATA_VENDA;
		if(pg.getTipoiniciodata() != null)
			tipoiniciodata = pg.getTipoiniciodata();
		
		Boolean isValorminimo = Boolean.TRUE;
		Money valorMinimoPagamento = null;
		if(pg.getPrazomedio() != null && pg.getPrazomedio() && pg.getValorminimo() != null){
			if(pedidovenda.getValor() < pg.getValorminimo().getValue().doubleValue()){
				isValorminimo = Boolean.FALSE;
				valorMinimoPagamento = pg.getValorminimo();
			}
		}
		
		AjaxRealizarVendaPagamentoBean bean = new AjaxRealizarVendaPagamentoBean();
		if(pg.getPrazomedio() != null && pg.getPrazomedio())
			bean.setParcela(pedidovenda.getQtdeParcelas());
		else
			bean.setParcela(listaPagItem.size());
		
		AjaxRealizarVendaPagamentoItemBean item;
		Money valor;
		Money valorJurosParcela;
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date dtparcela;
		
		List<AjaxRealizarVendaPagamentoItemBean> lista = new ArrayList<AjaxRealizarVendaPagamentoItemBean>();
		
		Date dataPrimeiraParcela = pedidovenda.getDtpedidovenda();
		if(Tipoiniciodata.DATA_ENTREGA.equals(tipoiniciodata)){
//			String dtPrimeiraEntrega = request.getParameter("dtprazoentrega");
//			if(dtPrimeiraEntrega != null && !"".equals(dtPrimeiraEntrega)){
			if(paramDtprazoentrega != null && !"".equals(paramDtprazoentrega)){
				try {
					dataPrimeiraParcela = SinedDateUtils.stringToDate(paramDtprazoentrega);
					if(pg.getDiasparainiciar() != null && pg.getDiasparainiciar() > 0){
						dataPrimeiraParcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), pg.getDiasparainiciar().intValue(), Calendar.DAY_OF_MONTH);
					}
				} catch (ParseException e) {}
			}
		}
		if(pedidovenda.getPagamentoDataUltimoVencimento() != null){
			dataPrimeiraParcela = pedidovenda.getPagamentoDataUltimoVencimento();
		}
		
		if(pg.getPrazomedio() != null && pg.getPrazomedio()){
			
			Prazopagamentoitem it = listaPagItem.get(0);
			
			if(it.getDias() != null){
				if(bean.getParcela() % 2 != 0){
					Integer meio = (bean.getParcela()/2) + (bean.getParcela() % 2);
					Integer dias = it.getDias();
					Date dtParcelaMeio = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), dias, Calendar.DAY_OF_MONTH); 
					Integer intervaloParcealas = bean.getParcela(); 
					if(bean.getParcela() > 1)
						intervaloParcealas = SinedDateUtils.diferencaDias(dtParcelaMeio, dataPrimeiraParcela) / (bean.getParcela() / 2);
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						valorJurosParcela = null;
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
							valorJurosParcela = valor.subtract(new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())));
						}
						item.setValor(valor.toString());
						if(valorJurosParcela != null){
							item.setValorjuros(valorJurosParcela.toString());
						}
						
						if( (meio-1) == i){
							dtparcela = dtParcelaMeio;
						}else if(i == 0){
							dtparcela = dataPrimeiraParcela;
						}else if(i < (meio - 1)){
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), -intervaloParcealas, Calendar.DAY_OF_MONTH);
						}else {
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						}
						
						if(pg.getDataparceladiautil() != null && pg.getDataparceladiautil()){
							dtparcela = SinedDateUtils.getProximaDataUtil(new java.sql.Date(dtparcela.getTime()), pedidovenda.getEmpresa(), pg.getDataparcelaultimodiautilmes());
						}
						
						item.setDtparcela(formatador.format(dtparcela));
						if(i != 0)
							dataPrimeiraParcela = dtparcela;
						else
							dataPrimeiraParcela = dtParcelaMeio;
													
						lista.add(item);
					}
				}else {					
					Integer intervaloParcealas = it.getDias() * 2 / bean.getParcela();
					for(int i = 0; i < bean.getParcela(); i++){
						item = new AjaxRealizarVendaPagamentoItemBean();
						valorJurosParcela = null;
						
						if(juros == null || juros.equals(0d)){			  
							valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())); 
						}else{
							valor = calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
							valorJurosParcela = valor.subtract(new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela())));
						}
						item.setValor(valor.toString());
						if(valorJurosParcela != null){
							item.setValorjuros(valorJurosParcela.toString());
						}
						
						if(i == 0)
							dtparcela = dataPrimeiraParcela;
						else
							dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), intervaloParcealas, Calendar.DAY_OF_MONTH);
						
						if(pg.getDataparceladiautil() != null && pg.getDataparceladiautil()){
							dtparcela = SinedDateUtils.getProximaDataUtil(new java.sql.Date(dtparcela.getTime()), pedidovenda.getEmpresa(), pg.getDataparcelaultimodiautilmes());
						}
						
						item.setDtparcela(formatador.format(dtparcela));
						dataPrimeiraParcela = dtparcela;						
						lista.add(item);
					}
				}
			}			
		}else{
			for (Prazopagamentoitem it : listaPagItem) {
				item = new AjaxRealizarVendaPagamentoItemBean();
				valorJurosParcela = null;
				
				if(juros == null || juros.equals(0d)){			  
//					valor = new Money(pedidovenda.getValor()).divide(new Money(bean.getParcela()));
					Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(pedidovenda.getValor() / bean.getParcela()), 2).doubleValue();
					valor = new Money(valorTruncado); 
				}else{
					valor = this.calculaJuros(new Money(pedidovenda.getValor()), bean.getParcela(), juros);
					Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(pedidovenda.getValor() / bean.getParcela()), 2).doubleValue();
					valorJurosParcela = new Money(valorTruncado); 
					valorJurosParcela = valor.subtract(valorJurosParcela);
				}
				item.setValor(valor.toString());
				if(valorJurosParcela != null){
					item.setValorjuros(valorJurosParcela.toString());
				}
				
				
				if(it.getDias() != null && it.getDias() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), it.getDias().intValue(), Calendar.DAY_OF_MONTH);
				} else if(it.getMeses() != null && it.getMeses() > 0){
					dtparcela = SinedDateUtils.incrementDate(new java.sql.Date(dataPrimeiraParcela.getTime()), it.getMeses().intValue(), Calendar.MONTH);
				} else {
					dtparcela = dataPrimeiraParcela;
				}
				
				if(pg.getDataparceladiautil() != null && pg.getDataparceladiautil()){
					dtparcela = SinedDateUtils.getProximaDataUtil(new java.sql.Date(dtparcela.getTime()), pedidovenda.getEmpresa(), pg.getDataparcelaultimodiautilmes());
				}
				
				item.setDtparcela(formatador.format(dtparcela));		
				
				lista.add(item);
			}
		}
		
		verificaDiferencaTotal(pedidovenda, lista, bean.getParcela(), juros);
		
		bean.setLista(lista);
		
		bean.setJuros(juros);
		bean.setIsValorminimo(isValorminimo);
		bean.setValorMinimoPagamento(valorMinimoPagamento);
		return bean;
	}
	
	public Money calculaJuros(Money totalvenda, Integer parcela, Double juros){
		Double total = totalvenda.getValue().doubleValue();
		
		juros = juros/100.00; 
		Double valor = total*juros*Math.pow((1+juros),parcela)/(Math.pow((1+juros),parcela)-1);
		
		Money resultado = new Money(valor);
		return resultado;
	}
	
	/**
	 * M�todo para verificar e corrigir diferen�a no total de pagamentos no pedido de venda
	 *
	 * @param pedidovenda
	 * @param lista
	 * @param qtdParcelas
	 * @param juros
	 * @author Luiz Fernando
	 */
	protected void verificaDiferencaTotal(Pedidovenda pedidovenda, List<AjaxRealizarVendaPagamentoItemBean> lista, Integer qtdParcelas, Double juros) {
		if(pedidovenda != null && lista != null && lista.size() > 1){
			try {
				Double valorfinalpedidovenda = pedidovenda.getValor();
				Money valortotalpagamentos = new Money(0.0);
				Double diferenca = 0.0;
				
				for(AjaxRealizarVendaPagamentoItemBean item : lista){
					valortotalpagamentos = valortotalpagamentos.add(new Money(item.getValor().replace(".", "").replace(",", ".")));
				}
				
				if(valorfinalpedidovenda != null && valortotalpagamentos != null && 
						valorfinalpedidovenda > valortotalpagamentos.getValue().doubleValue()){				
					if(valorfinalpedidovenda % qtdParcelas != 0){
						diferenca = valorfinalpedidovenda - valortotalpagamentos.getValue().doubleValue();
						
						AjaxRealizarVendaPagamentoItemBean item = lista.get(lista.size()-1);
						item.setValor(new Money((new Double(item.getValor().replace(".", "").replace(",", ".")) + diferenca)).toString());
					}
				}else if((juros == null || juros == 0) && valortotalpagamentos.getValue().doubleValue() > valorfinalpedidovenda) {
					diferenca = valortotalpagamentos.getValue().doubleValue() - valorfinalpedidovenda;
					
					AjaxRealizarVendaPagamentoItemBean item = lista.get(lista.size()-1);					
					item.setValor(new Money((new Double(item.getValor().replace(",", ".")) - diferenca)).toString());
				}
			} catch (Exception e) {}	
		}
	}
	
	/**
	 * M�todo que adiciona a observa��o da venda o link dos documentos do pedido de venda   
	 *
	 * @param pedidovenda
	 * @param vendahistorico
	 * @author Luiz Fernando
	 * @since 17/10/2013
	 */
	public void adicionarObservacaoReceitagerada(Pedidovenda pedidovenda, Vendahistorico vendahistorico) {
		if(pedidovenda != null && vendahistorico != null){
			try {
				List<Pedidovendapagamento> listaPedidovendapagmento = pedidovendapagamentoService.findByPedidovenda(pedidovenda);
				if(listaPedidovendapagmento != null && !listaPedidovendapagmento.isEmpty()){
					StringBuilder obsDoc = new StringBuilder(); 
					for(Pedidovendapagamento pvp : listaPedidovendapagmento){
						if(pvp.getDocumento() != null){
							if(!"".equals(obsDoc.toString())) obsDoc.append(",");
							obsDoc.append("<a href= '/"+NeoWeb.getApplicationName()
										+"/financeiro/crud/Contareceber?ACAO=consultar&cddocumento=" 
											+ pvp.getDocumento().getCddocumento() 
											+ "'> "
											+ pvp.getDocumento().getCddocumento() 
											+ " </a>");
						}
					}
					if(!obsDoc.toString().equals("")){
						if(vendahistorico.getObservacao() == null || "".equals(vendahistorico.getObservacao())){
							vendahistorico.setObservacao("Contas a receber geradas no pedido de venda: " + obsDoc.toString());
						}else {
							vendahistorico.setObservacao(vendahistorico.getObservacao() + ". Contas a receber geradas no pedido de venda: " + obsDoc.toString());
						}
					}
				}
			} catch (Exception e) {}
		}
	}
	
	/**
	 * M�todo que verifica se o prazo do pedido de venda � maior do que o prazo da tabela de pre�o
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Luiz Fernando
	 * @since 23/10/2013
	 */
	public Boolean isPrazomaiorprazotabela(Pedidovenda pedidoVenda) {
		boolean prazomaiorprazotabela = false;
		
		if(pedidoVenda != null && pedidoVenda.getListaPedidovendamaterial() != null && !pedidoVenda.getListaPedidovendamaterial().isEmpty() &&
				pedidoVenda.getPrazopagamento() != null && pedidoVenda.getPrazopagamento().getCdprazopagamento() != null){
			Prazopagamento prazopagamento = null;
//			Materialtabelaprecoitem materialtabelaprecoitem;
//			for(Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()){
//				materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(pedidovendamaterial.getMaterial(), pedidoVenda.getCliente(), null, true, pedidoVenda.getPedidovendatipo(), true, pedidoVenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
//				if(materialtabelaprecoitem != null && materialtabelaprecoitem.getMaterialtabelapreco() != null && 
//						materialtabelaprecoitem.getMaterialtabelapreco().getPrazopagamento() != null && 
//						materialtabelaprecoitem.getMaterialtabelapreco().getListaMaterialtabelaprecocliente() != null && 
//						!materialtabelaprecoitem.getMaterialtabelapreco().getListaMaterialtabelaprecocliente().isEmpty()){
//					prazopagamento = materialtabelaprecoitem.getMaterialtabelapreco().getPrazopagamento();
//					break;
//				}
//			}
			List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findByCliente(pedidoVenda.getCliente(),SinedDateUtils.castUtilDateForSqlDate(pedidoVenda.getDtpedidovenda()),null,null, materialtabelaprecoService.getTipocalculoByParametro());
			if(listaMaterialtabelapreco!=null && !listaMaterialtabelapreco.isEmpty()){
				Materialtabelapreco materialtabelapreco = listaMaterialtabelapreco.get(0);
				if(materialtabelapreco.getPrazopagamento() != null){
					prazopagamento = materialtabelapreco.getPrazopagamento();
				}
			}
			
			if(prazopagamento != null && prazopagamento.getCdprazopagamento() != null && pedidoVenda.getListaPedidovendapagamento() != null && 
					pedidoVenda.getListaPedidovendapagamento().size() > 0){ 
				try {
					Integer qtdeDiasPrazoPedido = 0;
					Date dtultimaParcela = pedidoVenda.getListaPedidovendapagamento().get(pedidoVenda.getListaPedidovendapagamento().size()-1).getDataparcela();
					if(dtultimaParcela != null){
						qtdeDiasPrazoPedido = SinedDateUtils.diferencaDias(dtultimaParcela,pedidoVenda.getDtpedidovenda());
					}						
					
					Integer qtdeDiasPrazoTabela = 0;
					Money total = pedidoVenda.getTotalvenda();
					
					Pedidovenda pedidovendaTabela = new Pedidovenda();
					pedidovendaTabela.setDtpedidovenda(pedidoVenda.getDtpedidovenda());
					pedidovendaTabela.setPrazopagamento(prazopagamento);
					pedidovendaTabela.setQtdeParcelas(pedidoVenda.getQtdeParcelas());
					pedidovendaTabela.setValor(total != null ? total.getValue().doubleValue() : 0d);
					
					List<Pedidovendapagamento> listaPedidovendapagamentoTabela = this.converterForPedidovendapagamento(this.pagamento(pedidovendaTabela, null));
					if(listaPedidovendapagamentoTabela != null && listaPedidovendapagamentoTabela.size() > 0){
						dtultimaParcela = listaPedidovendapagamentoTabela.get(listaPedidovendapagamentoTabela.size()-1).getDataparcela();
						if(dtultimaParcela != null){
							qtdeDiasPrazoTabela = SinedDateUtils.diferencaDias(dtultimaParcela,pedidoVenda.getDtpedidovenda());
						}
					}
					
					if(qtdeDiasPrazoPedido > qtdeDiasPrazoTabela){
						prazomaiorprazotabela = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return prazomaiorprazotabela;
	}
	
	/**
	 * M�todo para ordenar os materiais de produ��o
	 *
	 * @param venda
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public void ordenarMaterialproducao(Pedidovenda venda) {
		try {
			
			if(venda != null && venda.getListaPedidovendamaterial() != null && !venda.getListaPedidovendamaterial().isEmpty()){
				Map<Material, List<Pedidovendamaterial>> mapMaterialmestre = new HashMap<Material, List<Pedidovendamaterial>>();
				StringBuilder whereInCdmaterialmestre = new StringBuilder();
				List<Pedidovendamaterial> listaVendamaterialOrdenada = new ArrayList<Pedidovendamaterial>();
				List<Pedidovendamaterial> listaVendamaterialAvulso = new ArrayList<Pedidovendamaterial>();
				
				for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
					if(vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().getProducao() != null && 
							vendamaterial.getMaterialmestre().getProducao()){
						if(mapMaterialmestre.get(vendamaterial.getMaterialmestre()) != null){
							List<Pedidovendamaterial> listaVendamaterial = mapMaterialmestre.get(vendamaterial.getMaterialmestre());
							listaVendamaterial.add(vendamaterial);
							mapMaterialmestre.put(vendamaterial.getMaterialmestre(), listaVendamaterial);
						}else {
							if(!whereInCdmaterialmestre.toString().equals(""))
								whereInCdmaterialmestre.append(",");
							whereInCdmaterialmestre.append(vendamaterial.getMaterialmestre().getCdmaterial());
							
							List<Pedidovendamaterial> listaVendamaterial = new ArrayList<Pedidovendamaterial>();
							listaVendamaterial.add(vendamaterial);
							mapMaterialmestre.put(vendamaterial.getMaterialmestre(), listaVendamaterial);
							
							
						}
					}else {
						listaVendamaterialAvulso.add(vendamaterial);
					}
				}
				
				if(!whereInCdmaterialmestre.toString().equals("")){
					List<Material> listaMaterial = materialService.findForOrdenarMaterialproducao(whereInCdmaterialmestre.toString());
					if(listaMaterial != null && !listaMaterial.isEmpty()){
						for(Material materialmestre : listaMaterial){
							List<Pedidovendamaterial> lista_aux = mapMaterialmestre.get(materialmestre);
							if(materialmestre.getListaProducao() != null && !materialmestre.getListaProducao().isEmpty() && 
									mapMaterialmestre.get(materialmestre) != null){
								for(Materialproducao materialproducao : materialmestre.getListaProducao()){
									if(materialproducao.getMaterial() != null){
										for(Pedidovendamaterial vendamaterial : lista_aux){
											if(materialproducao.getMaterial().equals(vendamaterial.getMaterial())){
												vendamaterial.setOrdemexibicao(materialproducao.getOrdemexibicao());
												break;
											}
										}
									}
								}
							}
							
							Collections.sort(lista_aux,new Comparator<Pedidovendamaterial>(){
								public int compare(Pedidovendamaterial vm1, Pedidovendamaterial vm2) {
									try {
										return vm1.getOrdemexibicao().compareTo(vm2.getOrdemexibicao());
									} catch (Exception e) {
										return 1;
									}
								}
							});
							
						}
						
						if(mapMaterialmestre.size() > 0){
							for(List<Pedidovendamaterial> lista : mapMaterialmestre.values()){
								listaVendamaterialOrdenada.addAll(lista);
							}
							
							listaVendamaterialOrdenada.addAll(listaVendamaterialAvulso);
							
							if(listaVendamaterialOrdenada.size() == venda.getListaPedidovendamaterial().size()){
								venda.setListaPedidovendamaterial(listaVendamaterialOrdenada);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * M�todo que verifica se existe item com valor abaixo do minimo
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 13/11/2013
	 */
	public String verificaMaterialWithValorAbaixoMinimo(Pedidovenda pedidovenda) {
		StringBuilder materiais = new StringBuilder();
		if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				if(pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getCdmaterial() != null){
					Double valorvendaminimo = pedidovendamaterial.getValorvendaminimo();
					if(valorvendaminimo == null){
						if(pedidovendamaterial.getMaterial().getValorvendaminimo() == null && (pedidovendamaterial.getFatorconversao() == null || pedidovendamaterial.getFatorconversao().equals(0d))){
							Material material = materialService.loadSined(pedidovendamaterial.getMaterial(), "material.cdmaterial, material.valorvendaminimo");
							if(material != null){
								valorvendaminimo = material.getValorvendaminimo();
							}
						}else {
							valorvendaminimo = pedidovendamaterial.getMaterial().getValorvendaminimo();
						}
					}
					
					if(valorvendaminimo != null && pedidovendamaterial.getPreco() != null && 
							valorvendaminimo > pedidovendamaterial.getPreco()){
						Material material = materialService.load(pedidovendamaterial.getMaterial(), "material.cdmaterial, material.nome");
						if(material != null && material.getNome() != null && !"".equals(material.getNome())){
							if(!materiais.toString().equals("")) materiais.append(",");
							materiais.append(material.getNome());
						}
					}
				}
			}
		}
		return materiais.toString();
	}
	
	public List<Pedidovenda> findForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return pedidovendaDAO.findForReportFluxocaixa(filtro, false);
	}
	
	public List<Pedidovenda> findAnterioresForReportFluxocaixa(FluxocaixaFiltroReport filtro) {
		return pedidovendaDAO.findForReportFluxocaixa(filtro, true);
	}
	
	public void adicionaPedidovendaFluxocaixa(FluxocaixaFiltroReport filtro, List<FluxocaixaBeanReport> listaFluxo,	List<Pedidovenda> listaPedidovenda, java.sql.Date periodoDe, Boolean valoresAnteriores) {
		listaFluxo.addAll(this.criaListaFluxocaixa(listaPedidovenda, periodoDe, valoresAnteriores));
	}
	
	protected List<FluxocaixaBeanReport> criaListaFluxocaixa(List<Pedidovenda> listaPedidovenda, java.sql.Date periodoDe, Boolean valoresAnteriores) {
		List<FluxocaixaBeanReport> listaFluxo = new ArrayList<FluxocaixaBeanReport>();
		
		for (Pedidovenda pedidovenda : listaPedidovenda) {
			if(pedidovenda.getListaPedidovendapagamento() != null && pedidovenda.getListaPedidovendapagamento().isEmpty()){
				for(Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()){
					FluxocaixaBeanReport fluxocaixaBeanReport = new FluxocaixaBeanReport(pedidovenda, pedidovendapagamento);
					if(valoresAnteriores != null && valoresAnteriores && fluxocaixaBeanReport.getData().before(periodoDe)){
						fluxocaixaBeanReport.setDataAtrasada(pedidovenda.getDtpedidovenda());
						fluxocaixaBeanReport.setData(SinedDateUtils.remoteDate());
					}
					listaFluxo.add(fluxocaixaBeanReport);
				}
			}
		}
		return listaFluxo;
	}
	
	public void preecheValorVendaComTabelaPreco(Material material, Pedidovendatipo pedidovendatipo, Cliente cliente, Empresa empresa, Unidademedida unidademedida, Prazopagamento prazopagamento){
		preecheValorVendaComTabelaPreco(material, pedidovendatipo, cliente, empresa, unidademedida, prazopagamento, null);
	}
	
	/**
	 * Preeche valores de venda
	 *
	 * @param material
	 * @param pedidovendatipo
	 * @param cliente
	 * @param empresa
	 * @param unidademedida
	 * @param prazopagamento
	 * @author Rodrigo Freitas
	 * @since 22/11/2013
	 */
	public void preecheValorVendaComTabelaPreco(Material material, Pedidovendatipo pedidovendatipo, Cliente cliente, Empresa empresa, Unidademedida unidademedida, Prazopagamento prazopagamento, Material materialKit){
		Double valorvenda = material.getValorvenda();
		Double valorvendasemdesconto = material.getValorvenda();
		Double valorMaximo = material.getValorvendamaximo();
		Double valorMinimo = material.getValorvendaminimo();
		
		Double valortabelapreco = null;
		Double valortabelaprecosemdesconto = null;
		Double valorMaximoTabela = null;
		Double valorMinimoTabela = null;
		
		boolean tabelaprecoComPrazopagamento = false;
		Aux_VendamaterialTabelapreco pedidovendamaterialAuxPreco =  new Aux_VendamaterialTabelapreco(material);
		Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(material, cliente, prazopagamento, pedidovendatipo, empresa, materialKit, null);
		if(materialtabelapreco != null){
//			if(valorvenda != null){
				//valortabelapreco = materialtabelapreco.getValorComDescontoAcrescimo(valorvenda, numCasasDecimais);
				tabelaprecoComPrazopagamento = materialtabelapreco.getPrazopagamento() != null;
				vendaService.calculaValoresForVenda(pedidovendamaterialAuxPreco, materialtabelapreco, null, valorvenda, materialKit != null, false);
				valortabelapreco = pedidovendamaterialAuxPreco.getValorvendamaterial();
				valortabelaprecosemdesconto = pedidovendamaterialAuxPreco.getValorvendamaterialsemdesconto();
				
				material.setTemTabelaPrecoVigenteMaterial(true);
//			}
		}else {
			Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(material, cliente, prazopagamento, pedidovendatipo, empresa, unidademedida);
			if(materialtabelaprecoitem != null){
				//valortabelapreco = materialtabelaprecoitem.getValorcomtaxa(valorvenda, numCasasDecimais);
				valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa(valorMinimo);
				valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa(valorMaximo);

				vendaService.calculaValoresForVenda(pedidovendamaterialAuxPreco, materialtabelaprecoitem.getMaterialtabelapreco(), materialtabelaprecoitem, valorvenda, materialKit != null, true);
				valortabelapreco = pedidovendamaterialAuxPreco.getValorvendamaterial();
				valortabelaprecosemdesconto = pedidovendamaterialAuxPreco.getValorvendamaterialsemdesconto();
				tabelaprecoComPrazopagamento = materialtabelaprecoitem.getMaterialtabelapreco() != null &&
									materialtabelaprecoitem.getMaterialtabelapreco().getPrazopagamento() != null;
				
				material.setTemTabelaPrecoVigenteMaterial(true);
			} else {
				material.setTemTabelaPrecoVigenteMaterial(false);
			}
		}
		
		if(pedidovendamaterialAuxPreco.getPercentualdesconto() != null){
			material.setPercentualdescontoTabela(pedidovendamaterialAuxPreco.getPercentualdesconto().getValue().doubleValue());
		}
		
		if(valortabelapreco != null){
			valorvenda = valortabelapreco;
		}
		if(valorMaximoTabela != null){
			valorMaximo = valorMaximoTabela;
		}
		if(valorMinimoTabela != null){
			valorMinimo = valorMinimoTabela;
		}
		if(valortabelaprecosemdesconto != null){
			valorvendasemdesconto = valortabelaprecosemdesconto;
		}
		
		material.setValorvenda(valorvenda);
		material.setValorvendasemdesconto(valorvendasemdesconto);
		material.setValorvendamaximo(valorMaximo);
		material.setValorvendaminimo(valorMinimo);
		material.setTabelaprecoComPrazopagamento(tabelaprecoComPrazopagamento);
	}
	
	public void ajustaValorvendaMaximoMinimoItem(Pedidovenda pedidovenda) {
		if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && 
				!pedidovenda.getListaPedidovendamaterial().isEmpty()){
			List<Unidademedidaconversao> lista;
			Material material;
			Double fracao = null;
			Set<Material> listaMateriaisComTabelaPreco = materialtabelaprecoitemService.findAtivosByWhereInMaterial(SinedUtil.listAndConcatenate(pedidovenda.getListaPedidovendamaterial(), "material.cdmaterial", ","));
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				fracao = null;
				if(pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getCdmaterial() != null){
					lista = unidademedidaconversaoService.conversoesByUnidademedida(pedidovendamaterial.getUnidademedida());
					material = materialService.findListaMaterialunidademedida(pedidovendamaterial.getMaterial());
					Boolean achou = Boolean.FALSE;
					
					Double valorMin = pedidovendamaterial.getMaterial().getValorvendaminimo();
					Double valorMax = pedidovendamaterial.getMaterial().getValorvendamaximo();
					Double valorVenda = null;
					
					if(pedidovendamaterial.getFatorconversao() != null){
						if(material != null && material.getListaMaterialunidademedida() != null && !material.getListaMaterialunidademedida().isEmpty()){
							for(Materialunidademedida materialunidademedida : material.getListaMaterialunidademedida()){
								if(materialunidademedida.getUnidademedida() != null && materialunidademedida.getUnidademedida().getCdunidademedida() != null &&
										materialunidademedida.getFracao() != null){
									if(materialunidademedida.getUnidademedida().equals(pedidovendamaterial.getUnidademedida())){
										fracao = pedidovendamaterial.getFatorconversaoQtdereferencia();
										if(materialunidademedida.getValorminimo() != null){
											valorMin = materialunidademedida.getValorminimo();
										}else if(pedidovendamaterial.getMaterial().getValorvendaminimo() != null)
											valorMin = pedidovendamaterial.getMaterial().getValorvendaminimo() / fracao;
										
										if(materialunidademedida.getValormaximo() != null){
											valorMax = materialunidademedida.getValormaximo();
										}else if(pedidovendamaterial.getMaterial().getValorvendamaximo() != null)
											valorMax = pedidovendamaterial.getMaterial().getValorvendamaximo() / fracao;
										
										if(materialunidademedida.getValorunitario() != null){
											valorVenda = materialunidademedida.getValorunitario();
										}else if(pedidovendamaterial.getMaterial().getValorvenda() != null)
											valorVenda = pedidovendamaterial.getMaterial().getValorvenda() / fracao;
										achou = Boolean.TRUE;									
										break;
									}
								}
							}
						}else {
							lista = unidademedidaconversaoService.conversoesByUnidademedida(pedidovendamaterial.getMaterial().getUnidademedida());
							if(!achou && lista != null && lista.size() > 0){
								for (Unidademedidaconversao item : lista) {
									if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
										if(item.getUnidademedidarelacionada().equals(pedidovendamaterial.getUnidademedida())){
											fracao = pedidovendamaterial.getFatorconversaoQtdereferencia();
											if(pedidovendamaterial.getMaterial().getValorvendaminimo() != null)
												valorMin = pedidovendamaterial.getMaterial().getValorvendaminimo() / fracao;
											if(pedidovendamaterial.getMaterial().getValorvendamaximo() != null)
												valorMax = pedidovendamaterial.getMaterial().getValorvendamaximo() / fracao;
											if(pedidovendamaterial.getMaterial().getValorvenda() != null)
												valorVenda = pedidovendamaterial.getMaterial().getValorvenda() / fracao;
											achou = Boolean.TRUE;									
											break;
										}
									}
								}
							}
						}
					}
					
					if(!achou) {
						if(lista != null && lista.size() > 0){
							for (Unidademedidaconversao item : lista) {
								if(item.getUnidademedida() != null && item.getUnidademedida().getCdunidademedida() != null && item.getFracao() != null){
									if(item.getUnidademedidarelacionada().equals(pedidovendamaterial.getUnidademedida())){
										if(pedidovendamaterial.getMaterial().getValorvendaminimo() != null)
											valorMin = pedidovendamaterial.getMaterial().getValorvendaminimo() / item.getFracaoQtdereferencia();
										if(pedidovendamaterial.getMaterial().getValorvendamaximo() != null)
											valorMax = pedidovendamaterial.getMaterial().getValorvendamaximo() / item.getFracaoQtdereferencia();
										if(pedidovendamaterial.getMaterial().getValorvenda() != null)
											valorVenda = pedidovendamaterial.getMaterial().getValorvenda() / item.getFracaoQtdereferencia();
										break;
									}
								}
							}
						}
					}
					
					Double valorMaximoTabela = null;
					Double valorMinimoTabela = null;
					Double valorVendaTabela = null;
					
					if(listaMateriaisComTabelaPreco != null && listaMateriaisComTabelaPreco.contains(pedidovendamaterial.getMaterial())){
						Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(pedidovendamaterial.getMaterial(), pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa());
						if(materialtabelapreco == null){
							Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(pedidovendamaterial.getMaterial(), pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
							if(materialtabelaprecoitem != null){
								fracao = pedidovendamaterial.getFatorconversaoQtdereferencia();
								if(fracao != null && fracao > 0 && !materialtabelaprecoitem.isUnidadesecundaria()){
									if(materialtabelaprecoitem.getValorvendaminimo() != null)
										valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo() / fracao;
									if(materialtabelaprecoitem.getValorvendamaximo() != null)
										valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo() / fracao;
									if(materialtabelaprecoitem.getValor() != null)
										valorVendaTabela = materialtabelaprecoitem.getValor() / fracao;
								} else {
									valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
									valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
									valorVendaTabela = materialtabelaprecoitem.getValor();
								}
							}
						}
					}
					if(valorMinimoTabela != null){
						valorMin = valorMinimoTabela;
					}
					if(valorMaximoTabela != null){
						valorMax = valorMaximoTabela;
					}
					if(valorVendaTabela != null){
						valorVenda = valorVendaTabela;
					}
					
					if(valorMin != null){
						pedidovendamaterial.getMaterial().setValorvendaminimo(valorMin);
					}
					if(valorMax != null){
						pedidovendamaterial.getMaterial().setValorvendamaximo(valorMax);
					}
					if(valorVenda != null){
						pedidovendamaterial.getMaterial().setValorvenda(valorVenda);
						pedidovendamaterial.setValorvendamaterial(valorVenda);
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.PedidovendaDAO#findByOrcamento(Vendaorcamento vendaorcamento)
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public List<Pedidovenda> findByOrcamento(Vendaorcamento vendaorcamento) {
		return pedidovendaDAO.findByOrcamento(vendaorcamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForTrocarvendedor(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public Pedidovenda loadForTrocarvendedor(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForTrocarvendedor(pedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#updateColaboradorByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void updateColaboradorByPedidovenda(Pedidovenda pedidovenda) {
		pedidovendaDAO.updateColaboradorByPedidovenda(pedidovenda);		
	}
	
	/**
	 * M�todo que processa a troca de respons�vel do pedido de venda
	 *
	 * @param pedidovendaWithColaboradorAtual
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void processaTrocaVendedor(Pedidovenda pedidovendaWithColaboradorAtual) {
		if(pedidovendaWithColaboradorAtual != null && pedidovendaWithColaboradorAtual.getCdpedidovenda() != null && pedidovendaWithColaboradorAtual.getColaborador() != null && 
				pedidovendaWithColaboradorAtual.getColaborador().getCdpessoa() != null){
			Pedidovenda pvWithColaboradorAnterior = loadForTrocarvendedor(pedidovendaWithColaboradorAtual);
			boolean isColaboradorDiferente = false;
			if(pvWithColaboradorAnterior != null && pvWithColaboradorAnterior.getColaborador() != null && pvWithColaboradorAnterior.getColaborador().getCdpessoa() != null &&
					!pvWithColaboradorAnterior.getColaborador().equals(pedidovendaWithColaboradorAtual.getColaborador())){
				isColaboradorDiferente = true;
			}
			
			if(isColaboradorDiferente){
				updateColaboradorByPedidovenda(pedidovendaWithColaboradorAtual);
				
				List<Venda> listaVenda = vendaService.findByPedidovenda(pedidovendaWithColaboradorAtual);
				if(listaVenda != null && !listaVenda.isEmpty()){
					for(Venda venda : listaVenda){
						if(venda.getCdvenda() != null){
							venda.setColaborador(pedidovendaWithColaboradorAtual.getColaborador());
							vendaService.processaTrocaVendedor(venda);
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo que verifica cria��o e cria/atualiza tabela de pre�o do cliente
	 *
	 * @param request
	 * @param bean
	 * @param listaVendamaterialForCriarTabela
	 * @author Luiz Fernando
	 * @since 29/11/2013
	 */
	public void verificarAndCriarTabelaprecoCliente(WebRequestContext request, Pedidovenda bean, List<Pedidovendamaterial> listaVendamaterialForCriarTabela) {
		if(bean != null && listaVendamaterialForCriarTabela != null && !listaVendamaterialForCriarTabela.isEmpty()){
			if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.TABELA_VENDA_CLIENTE))){
				try {
					if(bean.getPedidovendasituacao() != null && !bean.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO)){
						bean.setListaPedidovendamaterial(listaVendamaterialForCriarTabela);
						List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findByCliente(bean.getCliente(),SinedDateUtils.castUtilDateForSqlDate(bean.getDtpedidovenda()),bean.getEmpresa(),null, materialtabelaprecoService.getTipocalculoByParametro());
						if(listaMaterialtabelapreco!=null && !listaMaterialtabelapreco.isEmpty())
							materialtabelaprecoService.atualizaTabelaprecoByCliente(bean, listaMaterialtabelapreco);
						else { 
							materialtabelaprecoService.criaTabelaprecoByCliente(bean, bean.getCliente());
						}
					}
				}  catch (DataIntegrityViolationException e) {
					if(request != null){
						e.printStackTrace();
						request.addError("N�o foi poss�vel criar Tabela de pre�o para o Cliente");
						if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_1")) {
							request.addError("Existe uma tabela cadastrada para o Cliente.");
						} else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
							request.addError("Existe uma tabela cadastrada para o Tipo de Pedido de venda.");
						}else if (DatabaseError.isKeyPresent(e, "IDX_MATERIALTABELAPRECO_2")) {
							request.addError("Existe uma tabela cadastrada para o Prazo de pagmento.");
						} 
					}
				} catch (Exception e) {
					if(request != null){
						request.addError("N�o foi poss�vel criar Tabela de pre�o para o Cliente");
					}
				}
			}
		}
	}
	
	public IReport gerarRelatorioListagem(PedidovendaFiltro filtro) {
		List<Pedidovenda> lista = this.findForRelatorioListagem(filtro);
		
		Report report = new Report("/faturamento/gerenciarpedidovenda");
		if(lista != null && lista.size() > 0)
			report.setDataSource(lista);
		
		Report subreport = new Report("/faturamento/pedidovendamaterial");
		report.addSubReport("PEDIDOVENDAMATERIAL", subreport);
		
		if(lista != null && !lista.isEmpty()){
			Money aux_totalgeral = new Money(0);
			Money aux_saldoprodutos = new Money(0);
			
			for (Pedidovenda pedidovenda : lista) {
				aux_totalgeral = aux_totalgeral.add(pedidovenda.getTotalvendaMaisImpostos());
				aux_saldoprodutos = aux_saldoprodutos.add(pedidovenda.getSaldofinal());
			}
			
			report.addParameter("TOTALGERAL", aux_totalgeral);
			if("TRUE".equals(parametrogeralService.buscaValorPorNome(Parametrogeral.VENDASALDOPRODUTO))){
				report.addParameter("SALDOGERAL", aux_saldoprodutos);
			}
		}
		
		return report;
	}
	
	public Resource gerarRelatorioListagemCsv(PedidovendaFiltro filtro) {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadForFiltro(filtro.getRegiao()));
		}
		List<Pedidovenda> lista = this.findForRelatorioListagem(filtro);
		StringBuilder csv = new StringBuilder();
		
		csv.append("\"C�DIGO\";\"EMPRESA\";\"CLIENTE\";\"LOCAL ENTREGA\";\"DATA DA VENDA\";\"DESCONTO\";\"VALOR DA VENDA\";").append("\n");
		for (Pedidovenda pedidovenda : lista) {
			
			csv.append("\"" + pedidovenda.getCdpedidovenda());
			csv.append("\";");
			
			csv.append("\"").append(pedidovenda.getEmpresa().getRazaosocialOuNome()).append("\"");
			csv.append(";");
			
			csv.append("\"").append(pedidovenda.getCliente().getNome()).append("\"");
			csv.append(";");
			
			if(pedidovenda.getEndereco() != null)
				csv.append("\"").append(pedidovenda.getEndereco().getLogradouroCompletoComCep()).append("\"");
			csv.append(";");
			
			csv.append("\"" + SinedDateUtils.toString(pedidovenda.getDtpedidovenda()));
			csv.append("\";");
			
			csv.append("\"");
			if(pedidovenda.getDesconto() != null)
				csv.append(pedidovenda.getDesconto());
			csv.append("\";");
			
			Money totalvenda = pedidovenda.getTotalvendaMaisImpostos();
			csv.append("\"");
			if(totalvenda != null)
				csv.append(totalvenda.toString());
			csv.append("\";");
			
			List<Pedidovendamaterial> listaPedidovendamaterial = pedidovenda.getListaPedidovendamaterial();
			if(listaPedidovendamaterial != null && listaPedidovendamaterial.size() > 0){
				csv.append("\n");
				csv.append("\"\";\"\";\"\";\"\";\"\";\"\";\"\";");
				csv.append("\"DESCRI��O DOS PRODUTOS\";\"QTDE.\";\"VALOR\";\"DESCONTO\";\"SEGURO\";\"OUTRAS DESPESAS\";\"OBSERVA��O\";\"PRAZO DE ENTREGA\";");
				csv.append("\n");
				for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
					csv.append("\"\";\"\";\"\";\"\";\"\";\"\";\"\";");
					
					csv.append("\"").append(pedidovendamaterial.getMaterial().getNome()).append("\"");
					csv.append(";");
					
					csv.append("\"" + SinedUtil.descriptionDecimal(pedidovendamaterial.getQuantidade()));
					csv.append("\";");
					
					csv.append("\"" + SinedUtil.descriptionDecimal(pedidovendamaterial.getPreco(), true));
					csv.append("\";");
					
					if(pedidovendamaterial.getDesconto() != null)
						csv.append("\"" + pedidovendamaterial.getDesconto() + "\"");
					else
						csv.append("\"\"");
					csv.append(";");
					
					if(pedidovendamaterial.getValorSeguro() != null)
						csv.append("\"" + pedidovendamaterial.getValorSeguro() + "\"");
					else
						csv.append("\"\"");
					csv.append(";");
					
					if(pedidovendamaterial.getOutrasdespesas() != null)
						csv.append("\"" + pedidovendamaterial.getOutrasdespesas() + "\"");
					else
						csv.append("\"\"");
					csv.append(";");
					
					if(pedidovendamaterial.getObservacao() != null)
						csv.append("\"").append(pedidovendamaterial.getObservacao()).append("\"");
					else 
						csv.append("\"\"");
					csv.append(";");
					
					if(pedidovendamaterial.getDtprazoentrega() != null)
						csv.append("\"" + SinedDateUtils.toString(pedidovendamaterial.getDtprazoentrega()) + "\"");
					else 
						csv.append("\"\"");
					csv.append(";");
					
					csv.append("\n");
				}
			}
			
			csv.append("\n");
		}
		
		return new Resource("text/csv", "gerenciarpedidovenda_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForRelatorioListagem(PedidovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/12/2013
	 */
	protected List<Pedidovenda> findForRelatorioListagem(PedidovendaFiltro filtro) {
		List<Pedidovenda> lista = pedidovendaDAO.findForRelatorioListagem(filtro);
		if(SinedUtil.isListNotEmpty(lista)){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdpedidovenda", ",");
			lista = null;
			lista = pedidovendaDAO.findForRelatorioListagem(whereIn);
		}
		
		return lista;
	}
	
	public List<Pedidovenda> findByClienteAndSituacao(Cliente cliente, Pedidovendasituacao situacao){
		return pedidovendaDAO.findByClienteAndSituacao(cliente, situacao);
	}
	
	public List<Pedidovenda> findForOrdemcompra(String whereInPedidovenda) {
		return pedidovendaDAO.findForOrdemcompra(whereInPedidovenda);
	}
	
	public Pedidovenda findPedidovendabyTipo(String cdpedidovenda){
		return pedidovendaDAO.findPedidovendabyTipo(cdpedidovenda);
	}
	
	public List<Pedidovenda> findForColetarProducao(String whereIn) {
		List<Pedidovenda> lista = pedidovendaDAO.findForColetarProducao(whereIn);
		if(SinedUtil.isSegmentoOtr()){
			for(Pedidovenda pv: lista){
				List<Pedidovendamaterial> listaItensSemColeta = new ArrayList<Pedidovendamaterial>();
				for(Pedidovendamaterial pvm: pv.getListaPedidovendamaterial()){
					if(!coletaMaterialService.haveColetaMaterialByPedidovendamaterialOtr(pvm)){
						listaItensSemColeta.add(pvm);
					}
				}
				pv.setListaPedidovendamaterial(listaItensSemColeta);
			}
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	public List<Coleta> saveColetarProducao(final ColetarProducaoBean coletarProducaoBean, final List<ManterPedidovendaItemBean> item) {
		final List<Coleta> lista = (List<Coleta>) getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				List<Coleta> listaColeta = new ArrayList<Coleta>();
				for(Pedidovenda pedidovenda : coletarProducaoBean.getListaPedidovenda()){
					StringBuilder linkMe = new StringBuilder();
					
					List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
					
					if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
						Coleta coleta = new Coleta();
						coleta.setTipo(Coleta.TipoColeta.CLIENTE);
						coleta.setCliente(pedidovenda.getCliente());
						coleta.setColaborador(pedidovenda.getColaborador());
						coleta.setObservacao(pedidovenda.getObservacao());
						coleta.setPedidovenda(pedidovenda);
						coleta.setEmpresa(pedidovenda.getEmpresa());
						coleta.setListaColetaMaterial(new ListSet<ColetaMaterial>(ColetaMaterial.class));
						
						Map<Pneu, List<Pedidovendamaterial>> mapa = new HashMap<Pneu, List<Pedidovendamaterial>>();
						List<Pedidovendamaterial> listaItensGerarMovimentacaoEstoque = new ArrayList<Pedidovendamaterial>();						
						
						for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
							if(pedidovendamaterial.getQuantidadecoleta() != null && pedidovendamaterial.getQuantidadecoleta() > 0){
								if(pedidovendamaterial.getPneu() != null){
									if(pedidovendamaterial.getPneu().existeDados()){
										pneuService.saveOrUpdate(pedidovendamaterial.getPneu());
									} else {
										pedidovendamaterial.setPneu(null);
									}
								}
								
								Boolean existeMaterialColeta = false;
								for(Pedidovendamaterial item : listaItensGerarMovimentacaoEstoque) {
									if(item.getMaterialcoleta().equals(pedidovendamaterial.getMaterialcoleta()) && item.getPneu() != null && item.getPneu().equals(pedidovendamaterial.getPneu())) {
										existeMaterialColeta = true;
										break;
									}
								}
								if(!existeMaterialColeta) { 
									listaItensGerarMovimentacaoEstoque.add(pedidovendamaterial); 
								}
								
								List<Pedidovendamaterial> lista;
								if(!mapa.containsKey(pedidovendamaterial.getPneu())) {
									lista = new ArrayList<Pedidovendamaterial>();
								} else {
									lista = mapa.get(pedidovendamaterial.getPneu());
								}

								if(pedidovendamaterial.getPneu() != null){
									pedidovendamaterial.getPneu().setValorColeta(getValorColeta(pedidovendamaterial, item));
									mapa.put(pedidovendamaterial.getPneu(), lista);
									if(org.apache.commons.lang.StringUtils.isNotBlank(pedidovendamaterial.getWhereInVincularColeta())){
										for(String cdpvm: pedidovendamaterial.getWhereInVincularColeta().split(",")){
											Pedidovendamaterial pvmAux = new Pedidovendamaterial();
											pvmAux.setCdpedidovendamaterial(Integer.parseInt(cdpvm));
											pvmAux.setQuantidadecoleta(pedidovendamaterial.getQuantidadecoleta());
											pvmAux.setObservacaocoleta(pedidovendamaterial.getObservacaocoleta());
											pvmAux.setMaterialcoleta(pedidovendamaterial.getMaterialcoleta());
											pvmAux.setLocalarmazenagemcoleta(pedidovendamaterial.getLocalarmazenagemcoleta());
											
											lista.add(pvmAux);
										}
									}
									lista.add(pedidovendamaterial);
								}else {
									ColetaMaterial coletaMaterial = new ColetaMaterial();
									coletaMaterial.setLocalarmazenagem(pedidovendamaterial.getLocalarmazenagemcoleta());
									coletaMaterial.setMaterial(pedidovendamaterial.getMaterialcoleta());
									coletaMaterial.setObservacao(pedidovendamaterial.getObservacaocoleta());
									coletaMaterial.setQuantidade(pedidovendamaterial.getQuantidadecoleta());
									coletaMaterial.setValorunitario(getValorColeta(pedidovendamaterial, item));
									
									List<ColetaMaterialPedidovendamaterial> listaServicos = new ArrayList<ColetaMaterialPedidovendamaterial>();
									ColetaMaterialPedidovendamaterial servico = new ColetaMaterialPedidovendamaterial();
									servico.setColetaMaterial(coletaMaterial);
									servico.setPedidovendamaterial(pedidovendamaterial);
									listaServicos.add(servico);
									
									coletaMaterial.setListaColetaMaterialPedidovendamaterial(listaServicos);
									coleta.getListaColetaMaterial().add(coletaMaterial);
								}
							}
						}
						
						for(Pedidovendamaterial pedidovendamaterial : listaItensGerarMovimentacaoEstoque) {							
							Movimentacaoestoque me = new Movimentacaoestoque();
							me.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
							me.setMaterial(pedidovendamaterial.getMaterialcoleta());
							me.setMaterialclasse(Materialclasse.PRODUTO);
							me.setQtde(pedidovendamaterial.getQuantidadecoleta());
							me.setEmpresa(pedidovenda.getEmpresa());
							me.setLocalarmazenagem(pedidovendamaterial.getLocalarmazenagemcoleta());
							me.setValor(getValorColeta(pedidovendamaterial, item));
							me.setObservacao(pedidovenda.getObservacaohistorico());
							me.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
							me.setObservacao(pedidovendamaterial.getObservacaocoleta());
							
							listaMovimentacaoestoque.add(me);
						}
						
						if(mapa != null && !mapa.isEmpty()) {
							for(Pneu pneu : mapa.keySet()) {
								List<Pedidovendamaterial> lista = mapa.get(pneu);
								
								ColetaMaterial coletaMaterial = new ColetaMaterial();
								coletaMaterial.setLocalarmazenagem(lista.get(0).getLocalarmazenagemcoleta());
								coletaMaterial.setMaterial(lista.get(0).getMaterialcoleta());
								coletaMaterial.setObservacao(lista.get(0).getObservacaocoleta());
								coletaMaterial.setQuantidade(lista.get(0).getQuantidadecoleta());
								coletaMaterial.setValorunitario(pneu.getValorColeta());
								coletaMaterial.setPneu(pneu);
								
								List<ColetaMaterialPedidovendamaterial> listaServicos = new ArrayList<ColetaMaterialPedidovendamaterial>();
								for(Pedidovendamaterial pedidovendamaterial : lista) {
									ColetaMaterialPedidovendamaterial servico = new ColetaMaterialPedidovendamaterial();
									servico.setColetaMaterial(coletaMaterial);
									servico.setPedidovendamaterial(pedidovendamaterial);
									listaServicos.add(servico);
								}
								
								coletaMaterial.setListaColetaMaterialPedidovendamaterial(listaServicos);
								coleta.getListaColetaMaterial().add(coletaMaterial);
							}
						}
						
						coletaService.saveOrUpdate(coleta);
						listaColeta.add(coleta);
						
						for (Movimentacaoestoque me : listaMovimentacaoestoque) {
							Movimentacaoestoqueorigem meo = new Movimentacaoestoqueorigem();
							meo.setDtaltera(new Timestamp(System.currentTimeMillis()));
							meo.setPedidovenda(pedidovenda);
							meo.setColeta(coleta);
							movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(meo);
							
							me.setMovimentacaoestoqueorigem(meo);
							movimentacaoestoqueService.saveOrUpdateNoUseTransaction(me);
							
							if(!linkMe.toString().equals("")) linkMe.append(",");
							linkMe.append(movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(me));
						}
						
						Pedidovendahistorico pvh = new Pedidovendahistorico();
						pvh.setPedidovenda(pedidovenda);
						pvh.setDtaltera(new Timestamp(System.currentTimeMillis()));
						
						Integer cdususarioaltera = null;
						Usuario usuario = SinedUtil.getUsuarioLogado();
						if(usuario != null){
							cdususarioaltera = usuario.getCdpessoa();
						}else if(pedidovenda.getColaborador() != null){
							cdususarioaltera = pedidovenda.getColaborador().getCdpessoa();
						}
						pvh.setCdusuarioaltera(cdususarioaltera);
						pvh.setAcao("Coleta para produ��o");
						pvh.setObservacao("Coleta: <a href=\"/w3erp/faturamento/crud/Coleta?ACAO=consultar&cdcoleta="+ coleta.getCdcoleta() +"\">"+ coleta.getCdcoleta() +"</a>");
						
						pedidovendahistoricoService.saveOrUpdateNoUseTransaction(pvh);
						
						Coletahistorico coletahistorico = new Coletahistorico();
						coletahistorico.setAcao(Coletaacao.CRIADO);
						coletahistorico.setColeta(coleta);
						coletahistoricoService.saveOrUpdateNoUseTransaction(coletahistorico); 

						if(linkMe != null && !"".equals(linkMe.toString())){
							pvh = new Pedidovendahistorico();
							pvh.setPedidovenda(pedidovenda);
							pvh.setDtaltera(new Timestamp(System.currentTimeMillis()));
							pvh.setCdusuarioaltera(cdususarioaltera);
							pvh.setAcao("Coleta para produ��o");
							pvh.setObservacao("Entrada no estoque: "+ linkMe.toString());
							
							pedidovendahistoricoService.saveOrUpdateNoUseTransaction(pvh);
						}
					}
				}
				if(parametrogeralService.getValorPorNome(Parametrogeral.EXPEDICAO_RECAPAGEM).equals("COLETA")){
					if(SinedUtil.isListNotEmpty(listaColeta)){
						for (Coleta c : listaColeta) {
							coletaService.updateSituacaoColeta(c,SituacaoVinculoExpedicao.PENDENTE, false, null, null);
						}	
					}
				}
				return listaColeta;
			}
		});
		return lista;
	}
	
	private Double getValorColeta(Pedidovendamaterial pedidovendamaterial, List<ManterPedidovendaItemBean> item) {
		Double valorcoleta = pedidovendamaterial.getMaterialcoleta() != null ? pedidovendamaterial.getMaterialcoleta().getValorcusto() : null;
		if(item != null && !item.isEmpty()){
			for (ManterPedidovendaItemBean it : item) {
				if(pedidovendamaterial.getIdentificadorintegracao() != null && pedidovendamaterial.getIdentificadorintegracao().equals(it.getIdentificador())){
					return it.getValorcoleta();
				}
			}
		}		
		return valorcoleta;
	}
	
	public List<Vendamaterialmestre> preecheVendaMaterialmestre(List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre) {
		List<Vendamaterialmestre> listaVendamaterialmestre = new ArrayList<Vendamaterialmestre>();
		Vendamaterialmestre vendamaterialmestre;
		for (Pedidovendamaterialmestre pedidovendamaterialmestre : listaPedidovendamaterialmestre) {
			vendamaterialmestre = new Vendamaterialmestre();
			vendamaterialmestre.setAltura(pedidovendamaterialmestre.getAltura());
			vendamaterialmestre.setComprimento(pedidovendamaterialmestre.getComprimento());
			vendamaterialmestre.setLargura(pedidovendamaterialmestre.getLargura());
			vendamaterialmestre.setMaterial(pedidovendamaterialmestre.getMaterial());
			vendamaterialmestre.setPeso(pedidovendamaterialmestre.getPeso());
			vendamaterialmestre.setQtde(pedidovendamaterialmestre.getQtde());
			vendamaterialmestre.setIdentificadorinterno(pedidovendamaterialmestre.getIdentificadorinterno());
			vendamaterialmestre.setPreco(pedidovendamaterialmestre.getPreco());
			vendamaterialmestre.setDtprazoentrega(pedidovendamaterialmestre.getDtprazoentrega());
			vendamaterialmestre.setExibiritenskitflexivel(pedidovendamaterialmestre.getExibiritenskitflexivel());
			vendamaterialmestre.setNomealternativo(pedidovendamaterialmestre.getNomealternativo());
			vendamaterialmestre.setQtdeanterior(pedidovendamaterialmestre.getQtde());
			vendamaterialmestre.setUnidademedida(pedidovendamaterialmestre.getMaterial().getUnidademedida());
			vendamaterialmestre.setDesconto(pedidovendamaterialmestre.getDesconto());
			
			vendamaterialmestre.setAliquotaReaisIpi(pedidovendamaterialmestre.getAliquotaReaisIpi());
			vendamaterialmestre.setTipoCalculoIpi(pedidovendamaterialmestre.getTipoCalculoIpi());
			vendamaterialmestre.setGrupotributacao(pedidovendamaterialmestre.getGrupotributacao());
			
			ImpostoUtil.copyDadosImposto(pedidovendamaterialmestre, vendamaterialmestre);
			
			listaVendamaterialmestre.add(vendamaterialmestre);
		}
		return listaVendamaterialmestre;
	}
	
	public void trocarMaterialProducao(Material material, Pedidovenda pedidovenda) {
		if(material != null && material.getListaProducao() != null && !material.getListaProducao().isEmpty() && 
				pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			for(Materialproducao materialproducao : material.getListaProducao()){
				if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() != null){
					for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
						if(pedidovendamaterial.getMaterialAnterior() != null && pedidovendamaterial.getMaterialAnterior().getCdmaterial() != null &&
								materialproducao.getMaterial().equals(pedidovendamaterial.getMaterialAnterior())){
							if(pedidovendamaterial.getDtprazoentrega() != null){
								materialproducao.setPrazoentrega(new Date(pedidovendamaterial.getDtprazoentrega().getTime()));
							}
							materialproducao.setProducaosemestoque(pedidovendamaterial.getProducaosemestoque());
							materialproducao.setLoteestoque(pedidovendamaterial.getLoteestoque());
							if(pedidovendamaterial.getMaterial() != null && pedidovendamaterial.getMaterial().getCdmaterial() != null){
								materialproducao.setMaterial(materialService.loadForTrocaproducao(pedidovendamaterial.getMaterial()));
								materialproducao.setMaterialanterior(pedidovendamaterial.getMaterialAnterior());
							}
							break;
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * 
	 * @param parseInt
	 * @return
	 */
	public Pedidovenda findPedidovendaByCdpedidovenda(Integer cdpedidovenda) {
		return pedidovendaDAO.findPedidovendaByCdpedidovenda(cdpedidovenda);
	}
	
	
	public boolean isPedidovendaAprovado(Pedidovenda pedidovenda) {
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			List<Pedidovendahistorico> listaPedidovendahistorico = pedidovendahistoricoService.findByPedidovenda(pedidovenda);
			if(listaPedidovendahistorico != null && !listaPedidovendahistorico.isEmpty()){
				for(Pedidovendahistorico pvh : listaPedidovendahistorico){
					if(pvh.getAcao() != null && pvh.getAcao().toUpperCase().contains("APROVADO"))
						return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void saveOrUpdate(final Pedidovenda bean) {

		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				PedidovendaService.super.saveOrUpdateNoUseTransaction(bean);
				if (bean.getListaPedidovendamaterial() != null){
					for (Pedidovendamaterial item : bean.getListaPedidovendamaterial()){
						if (item.getListaPedidovendamaterialseparacao() != null){
							Iterator<Pedidovendamaterialseparacao> iterator = item.getListaPedidovendamaterialseparacao().iterator();
							while (iterator.hasNext()){
								Pedidovendamaterialseparacao itemSeparacao = iterator.next();
								if (itemSeparacao.getQuantidade() != null && itemSeparacao.getQuantidade() > 0){
									itemSeparacao.setPedidovendamaterial(item);
									pedidovendamaterialseparacaoService.saveOrUpdateNoUseTransaction(itemSeparacao);
								}else if (itemSeparacao.getCdpedidovendamaterialseparacao() != null){
									pedidovendamaterialseparacaoService.delete(itemSeparacao);
								}
							}
						}
					}
				}
				
				return status;
			}
		});
	}
	/**
	 * M�todo que retorna a lista de pedido com apenas os itens com qtde maior que zero para a coleta
	 *
	 * @param listaPedidovenda
	 * @param listaMovEstoque
	 * @return
	 * @author Luiz Fernando
	 * @since 02/04/2014
	 */
	public List<Pedidovenda> createListaPedidovendaForColetarProducao(List<Pedidovenda> listaPedidovenda,	List<Movimentacaoestoque> listaMovEstoque) {
		List<Pedidovenda> listaNova = new ArrayList<Pedidovenda>();
		if(listaPedidovenda != null && !listaPedidovenda.isEmpty()){
			for(Pedidovenda pedidovenda : listaPedidovenda){
				boolean isOtr = SinedUtil.isSegmentoOtr();
				if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
					List<Pedidovendamaterial> listaPedidovendamaterial  = new ArrayList<Pedidovendamaterial>();
					for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
						if(pedidovendamaterial.getMaterialcoleta() != null && pedidovendamaterial.getQuantidade() != null){
							if(!isOtr){
								List<Movimentacaoestoque> listaMovItem = getMovimentacaoestoque(pedidovendamaterial.getMaterialcoleta(), pedidovenda, listaMovEstoque);
								if(listaMovItem != null && !listaMovItem.isEmpty()){
									for(Movimentacaoestoque me : listaMovItem){
										if(me != null && me.getQtde() != null){
											if(pedidovendamaterial.getQuantidadecoletada() == null){
												pedidovendamaterial.setQuantidadecoletada(me.getQtde());
												me.setItemutilizado(true);
											}else if(pedidovendamaterial.getQuantidadecoletada() < me.getQtde()){
												pedidovendamaterial.setQuantidadecoletada(pedidovendamaterial.getQuantidadecoletada()+me.getQtde());
												me.setItemutilizado(true);
											}
										}
									}
								}
							}
							if(pedidovendamaterial.getQuantidadecoletada() == null){
								pedidovendamaterial.setQuantidadecoletada(0.);
							}
							if(pedidovendamaterial.getQuantidade() > pedidovendamaterial.getQuantidadecoletada()){
								if(pedidovendamaterial.getQuantidade() - pedidovendamaterial.getQuantidadecoletada() >= 1.){
									pedidovendamaterial.setQuantidadecoleta(pedidovendamaterial.getQuantidade() - pedidovendamaterial.getQuantidadecoletada());
								} else{
									pedidovendamaterial.setQuantidadecoleta(0.);
								}
								listaPedidovendamaterial.add(pedidovendamaterial);
							}
						}
					}
					if(!listaPedidovendamaterial.isEmpty()){
						pedidovenda.setListaPedidovendamaterial(listaPedidovendamaterial);
						listaNova.add(pedidovenda);
					}
				}
			}
		}
		return listaNova;
	}
	
	protected List<Movimentacaoestoque> getMovimentacaoestoque(Material material, Pedidovenda pedidovenda, List<Movimentacaoestoque> listaMovEstoque){
		List<Movimentacaoestoque> listaMovItem = new ArrayList<Movimentacaoestoque>();
		if(material != null && pedidovenda != null && pedidovenda.getCdpedidovenda() != null && 
				listaMovEstoque != null && !listaMovEstoque.isEmpty()){
			for(Movimentacaoestoque movimentacaoestoque : listaMovEstoque){
				if((movimentacaoestoque.getItemutilizado() == null || !movimentacaoestoque.getItemutilizado()) &&
					movimentacaoestoque.getMaterial() != null && movimentacaoestoque.getMaterial().equals(material) &&
					movimentacaoestoque.getMovimentacaoestoqueorigem() != null && 
					movimentacaoestoque.getMovimentacaoestoqueorigem().getPedidovenda() != null &&
					movimentacaoestoque.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda() != null &&
					movimentacaoestoque.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda().equals(pedidovenda.getCdpedidovenda()))
					listaMovItem.add(movimentacaoestoque);
			}
		}
		return listaMovItem;
	}
	
	public String getAlturaLarguraComprimentoConcatenados(Pedidovendamaterial pedidovendamaterial, Material material){
		StringBuilder alturaLarguraComprimentoConcatenados = new StringBuilder();
		if(material != null && material.getCdmaterial() != null && pedidovendamaterial != null){
			if(pedidovendamaterial.getLargura() != null && pedidovendamaterial.getLargura() > 0){
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(pedidovendamaterial.getLargura()));
			}
			if(pedidovendamaterial.getAltura() != null && pedidovendamaterial.getAltura() > 0){
				if(!"".equals(alturaLarguraComprimentoConcatenados.toString()))
					alturaLarguraComprimentoConcatenados.append("x");
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(pedidovendamaterial.getAltura()));
			}
			if(pedidovendamaterial.getComprimentooriginal() != null && pedidovendamaterial.getComprimentooriginal() > 0){
				if(!"".equals(alturaLarguraComprimentoConcatenados.toString()))
					alturaLarguraComprimentoConcatenados.append("x");
				alturaLarguraComprimentoConcatenados.append(new DecimalFormat("#.#######").format(pedidovendamaterial.getComprimentooriginal()));
			}
		}
		return alturaLarguraComprimentoConcatenados.toString();
	}
	
	/**
	 * 
	 * @param param
	 * @return
	 * @author Lucas Costa
	 */
	public  List<Pedidovenda> findPedidoVendaForAutocomplete(String param) {
		return pedidovendaDAO.findPedidoVendaForAutocomplete(param);
	}
	
	/**
	 * M�todo que retorna o item da lista de acordo com o material mestre
	 *
	 * @param listaPedidovendamaterialmestre
	 * @param materialmestre
	 * @return
	 * @author Luiz Fernando
	 * @since 17/04/2014
	 */
	public Pedidovendamaterialmestre getPVMForVenda(List<Pedidovendamaterialmestre> listaPedidovendamaterialmestre,	Material materialmestre) {
		if(listaPedidovendamaterialmestre != null && !listaPedidovendamaterialmestre.isEmpty() && materialmestre != null){
			for(Pedidovendamaterialmestre pvmm : listaPedidovendamaterialmestre){
				if(pvmm.getMaterial() != null && pvmm.getMaterial().equals(materialmestre))
					return pvmm;
			}
		}
		return null;
	}
	
	public List<Pedidovenda> findForGerarNotaByWhereIn(String whereIn) throws Exception{
		return pedidovendaDAO.findForGerarNotaByWhereIn(whereIn);
	}
	
	public boolean existeIdentificador(Empresa empresa, String identificador, Integer cdpedidovenda) {
		return pedidovendaDAO.existeIdentificador(empresa, identificador, cdpedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* Confirma pedido de venda sem abrir a tela de entrada da venda
	*
	* @see br.com.linkcom.sined.geral.service.VendaService#comprar(WebRequestContext request, Venda venda, Pedidovenda pedidovenda, List<Vendamaterial> listaVendamaterialForCriarTabela)
	*
	* @param request
	* @param whereIn
	* @param percentualPedidoVenda
	* @since 30/07/2014
	* @author Luiz Fernando
	*/
	public void confirmarVariosPedidovenda(WebRequestContext request, String whereIn, Integer percentualPedidoVenda) {
		request.getSession().setAttribute("confirmarVariosPedidovendaWhereInPedidoVenda", whereIn);
		
		boolean confirmarVariosPedidos = parametrogeralService.getBoolean(Parametrogeral.PERMITIR_CONFIRMAR_VARIOS_PEDIDOS);
		List<Pedidovenda> listaPedidovenda = this.loadForConfirmacao(whereIn);
		String url = request.getFirstRequestUrl();//mudou"/faturamento/crud/Pedidovenda";
		if(SinedUtil.isListEmpty(listaPedidovenda)){
			request.getSession().removeAttribute("confirmarVariosPedidovendaWhereInPedidoVenda");
			request.addError("Nenhum pedido de venda encontrado.");
			SinedUtil.redirecionamento(request, url);
		}		
		
		String paramAtualizaData = parametrogeralService.getValorPorNome(Parametrogeral.ATUALIZAR_VENCIMENTO_CONFIRMACAO_VENDA);
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		Usuario usuario_aux = usuarioService.load(usuario, "usuario.cdpessoa, usuario.limitepercentualdesconto");
			
		boolean erroPedidovendaReserva = false;
		boolean erroPedidovendaGrade = false;
		List<Pedidovenda> listaPedidovendaConfirmado = new ArrayList<Pedidovenda>();
		List<Pedidovenda> listaPedidovendaErro = new ArrayList<Pedidovenda>();
		List<Venda> listaVenda = new ArrayList<Venda>();
		for(Pedidovenda pedidovenda : listaPedidovenda){
			Venda form = new Venda();
			form.setLimitepercentualdesconto(usuario_aux.getLimitepercentualdesconto());
			form.setDtvenda(new Date(System.currentTimeMillis()));

			if(this.isMaterialReservadoOutrosPedidos(pedidovenda) && !confirmarVariosPedidos){
				erroPedidovendaReserva = true;
				break;
			}
			pedidovenda.setPercentualPedidovenda(percentualPedidoVenda);
			pedidovenda.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(pedidovenda));
			
			form.setColaborador(pedidovenda.getColaborador());
			form.setEmpresa(pedidovenda.getEmpresa());
			form.setIdentificador(pedidovenda.getIdentificador());
			form.setIdentificadorexterno(pedidovenda.getIdentificacaoexterna());
			form.setEmpresa(pedidovenda.getEmpresa());
			form.setProjeto(pedidovenda.getProjeto());
			form.setIndicecorrecao(pedidovenda.getIndicecorrecao());
			form.setContaboleto(pedidovenda.getConta());
			form.setDocumentotipo(pedidovenda.getDocumentotipo());
			form.setPrazomedio(pedidovenda.getPrazomedio());
			form.setPresencacompradornfe(pedidovenda.getPresencacompradornfe());
			form.setPrazopagamento(pedidovenda.getPrazopagamento());
			form.setCliente(pedidovenda.getCliente());
			form.setContato(pedidovenda.getContato());
			form.setEndereco(pedidovenda.getEndereco());
			form.setDtvenda(new Date(System.currentTimeMillis()));
			form.setObservacao(pedidovenda.getObservacao());
			form.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
			form.setListavendamaterial(this.preencheListaVendaMaterial(pedidovenda, pedidovenda.getPercentualPedidovenda()));
			if(SinedUtil.isListNotEmpty(form.getListavendamaterial())){
				for (Vendamaterial vendamaterial : form.getListavendamaterial()) {
					vendamaterial.setTotal(vendaService.getValortotal(vendamaterial.getPreco(), 
																		vendamaterial.getQuantidade(),
																		vendamaterial.getDesconto(), vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
				}
			}
			form.setListaVendamaterialmestre(this.preecheVendaMaterialmestre(pedidovenda.getListaPedidovendamaterialmestre()));
			
			if(form.getListavendamaterial() != null && !form.getListavendamaterial().isEmpty()){
				boolean isMaterialmestregrade = false;
				boolean existGrade = false;
				for(Vendamaterial vendamaterial : form.getListavendamaterial()){
					if(vendamaterial.getIsMaterialmestregrade() != null && vendamaterial.getIsMaterialmestregrade()){
						isMaterialmestregrade = vendamaterial.getIsMaterialmestregrade();
					}
					if(vendamaterial.getIsControlegrade() != null && vendamaterial.getIsControlegrade()){
						existGrade = vendamaterial.getIsControlegrade();
					}
				}
				if(existGrade || isMaterialmestregrade){
					erroPedidovendaGrade = true;
					break;
				}
			}
			
			if(pedidovenda.getGerarnovasparcelas() == null || !pedidovenda.getGerarnovasparcelas()){
				form.setListavendapagamento(this.preencheListaVendaPagamento(pedidovenda));
			}
			form.setQtdeParcelas(form.getListavendapagamento().size());
			form.setFrete(pedidovenda.getFrete());
			form.setValorfrete(pedidovenda.getValorfrete());
			form.setTerceiro(pedidovenda.getTerceiro());
			form.setPedidovenda(pedidovenda);
			form.setTaxapedidovenda(pedidovenda.getTaxapedidovenda());
			form.setPedidovendatipo(pedidovenda.getPedidovendatipo());
			form.setObservacaointerna(pedidovenda.getObservacaointerna());
			form.setValorusadovalecompra(pedidovenda.getValorusadovalecompra());
			form.setSaldofinal(pedidovenda.getSaldofinal());
			if(pedidovenda.getPedidovendatipo() != null){
				form.setBonificacao(pedidovenda.getPedidovendatipo().getBonificacao());
				form.setComodato(pedidovenda.getPedidovendatipo().getComodato());
			}
			
			Money totalparcela = form.getTotalparcela();
			if(totalparcela == null || totalparcela.getValue().doubleValue() <= 0){
				form.setListavendapagamento(new ArrayList<Vendapagamento>());
			}
			
			form.setDesconto(calculaDescontoPercentual(form, pedidovenda));
				
			//vendaService.ordenarMaterialproducao(form);
			listaVenda.add(form);
		}
		
		if(erroPedidovendaReserva || erroPedidovendaGrade){
			if(erroPedidovendaReserva && !confirmarVariosPedidos){
				request.addError("N�o � permitido confirma��o direta de pedido de venda com reserva."); 
			}
			if(erroPedidovendaGrade){
				request.addError("N�o � permitido confirma��o direta de pedido de venda com grade."); 
			}
		}else {
			for(Venda venda : listaVenda){
				if(paramAtualizaData != null && paramAtualizaData.trim().toUpperCase().equals("TRUE")){
					if(venda.getPrazopagamento() != null && venda.getPrazopagamento().getCdprazopagamento() != null){
						vendaService.ajustaVencimentos(request, venda);
					}
				}
				
				List<Vendamaterial> listaVendamaterialForCriarTabela = vendaService.getListavendamaterialForCriarTabela(venda);
				
				if(venda.getPedidovendatipo() != null){
					venda.setIsNotVerificarestoquePedidovenda(vendaService.isNotVerificarEstoqueByPedidovendatipo(venda.getPedidovendatipo()));
				}
				
				if(vendaService.isQtdeSolicitadaMaiorOrIgualQtdeReservada(request, venda, false)){
					listaPedidovendaErro.add(venda.getPedidovenda());
					continue;
				}
				
				if (!vendaService.validaVenda(request, venda)) {
					listaPedidovendaErro.add(venda.getPedidovenda());
					continue;
				}
				
				if(restricaoService.verificaRestricaoSemDtLiberacao(venda.getCliente())){
					venda.getPedidovenda().setErroConfirmacao("O cliente possui restri��o(�es) sem data de libera��o.");
					listaPedidovendaErro.add(venda.getPedidovenda());
					continue;
				}
				
				Pedidovenda pedidovenda = load(venda.getPedidovenda(), "pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao");
				if(pedidovenda.getPedidovendasituacao() != null && pedidovenda.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO)){
					venda.getPedidovenda().setErroConfirmacao("Pedido de venda j� confirmado.");
					listaPedidovendaErro.add(venda.getPedidovenda());
					continue;
				}
				
				vendaService.comprar(request, venda, venda.getPedidovenda(), listaVendamaterialForCriarTabela,  null);
				
				listaPedidovendaConfirmado.add(venda.getPedidovenda());
			}
		
			request.clearMessages();
			if(SinedUtil.isListNotEmpty(listaPedidovendaConfirmado)){
				StringBuilder whereInPedidoConfirmado =  new StringBuilder();
				for(Pedidovenda pedidovenda : listaPedidovendaConfirmado){
					whereInPedidoConfirmado.append(pedidovenda.getCdpedidovenda()).append(",");
				}
				request.addMessage("Pedido(s) de venda confirmado(s) com sucesso. " + 
						whereInPedidoConfirmado.toString().substring(0, whereInPedidoConfirmado.toString().length()-1));
			}
			if(SinedUtil.isListNotEmpty(listaPedidovendaErro)){
				for(Pedidovenda pedidovenda : listaPedidovendaErro){
					request.addError("Pedido de venda " + pedidovenda.getCdpedidovenda() + " n�o confirmado. " + 
							(org.apache.commons.lang.StringUtils.isNotEmpty(pedidovenda.getErroConfirmacao()) ? 
									pedidovenda.getErroConfirmacao(): ""));
				}
			}
		}
		
		request.getSession().removeAttribute("confirmarVariosPedidovendaWhereInPedidoVenda");
		SinedUtil.redirecionamento(request, url);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#existeGrade(String whereInPedidovenda)
	*
	* @param whereInPedidovenda
	* @return
	* @since 30/07/2014
	* @author Luiz Fernando
	*/
	public boolean existeGrade(String whereInPedidovenda){
		return pedidovendaDAO.existeGrade(whereInPedidovenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/08/2014
	 */
	public List<Pedidovenda> findByColeta(String whereInColeta) {
		return pedidovendaDAO.findByColeta(whereInColeta);
	}
	
	/**
	 * M�todo que atualiza o campo 'integrar' de um pedido venda
	 * 
	 * @param pedidovenda
	 * @param integrar
	 * @throws Exception 
	 * @author Rafael Salvio
	 */
	public void updateIntegrar(Pedidovenda pedidovenda, boolean integrar) throws Exception{
		pedidovendaDAO.updateIntegrar(pedidovenda, integrar);
	}
	
	public Money calculaValorVenda(Pedidovenda pedidovenda){
		Money valor = new Money(0);
		if(pedidovenda.getListaPedidovendamaterial()!= null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				Double qtde = pedidovendamaterial.getQuantidade() == null || pedidovendamaterial.getQuantidade() == 0 ? 0 : pedidovendamaterial.getQuantidade();
				Double multiplicador = pedidovendamaterial.getMultiplicador() == null || pedidovendamaterial.getMultiplicador() == 0 ? 1 : pedidovendamaterial.getMultiplicador();
				Double valorTruncado = SinedUtil.round(BigDecimal.valueOf(pedidovendamaterial.getPreco().doubleValue() * qtde * multiplicador), 2).doubleValue();
				valor = valor.add(new Money(valorTruncado));
				if(pedidovendamaterial.getDesconto() != null){
					valor = valor.subtract(pedidovendamaterial.getDesconto());
				}
			}
		}
		valor = valor.add(pedidovenda.getValorfrete()!= null ? pedidovenda.getValorfrete() : new Money(0.0));
		valor = valor.add(pedidovenda.getTaxapedidovenda() != null ? pedidovenda.getTaxapedidovenda() : new Money(0.0));
		valor = valor.subtract(pedidovenda.getDesconto() != null ? pedidovenda.getDesconto() : new Money(0.0));
		valor = valor.subtract(pedidovenda.getValorusadovalecompra() != null ? pedidovenda.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	public PedidovendaRESTModel salvarPedidovendaAndroid(PedidovendaRESTWSBean command) {
		PedidovendaRESTModel pedidovendaRESTModel = new PedidovendaRESTModel();
		try {
			Pedidovenda bean = createPedidovendaByAndroid(command);
			
			if(bean.getCliente() == null){
				pedidovendaRESTModel.setErroSincronizacao(SinedUtil.FORMATADOR_DATA_HORA.format(new Timestamp(System.currentTimeMillis())) + " - Pedido de venda sem cliente");
				return pedidovendaRESTModel;
			}
			
			Cliente beanCliente = clienteService.load(bean.getCliente(), "cliente.observacaoVenda");
			if(beanCliente != null && org.apache.commons.lang.StringUtils.isNotBlank(beanCliente.getObservacaoVenda())){
				if(parametrogeralService.getBoolean(Parametrogeral.OBSERVACAOVENDA)){
					if(bean.getObservacao() != null && bean.getObservacao().length() > 0){
						if(!bean.getObservacao().contains(beanCliente.getObservacaoVenda())){
							bean.setObservacao(bean.getObservacao() + ". " + beanCliente.getObservacaoVenda());
						}
					}else {
						bean.setObservacao(beanCliente.getObservacaoVenda());
					}
				}else {
					if(bean.getObservacaointerna() != null && bean.getObservacaointerna().length() > 0){
						if(!bean.getObservacaointerna().contains(beanCliente.getObservacaoVenda())){
							bean.setObservacaointerna(bean.getObservacaointerna() + ". " + beanCliente.getObservacaoVenda());
						}
					}else {
						bean.setObservacaointerna(beanCliente.getObservacaoVenda());
					}
				}
			}
			List<Pedidovendamaterial> listaVendamaterialForCriarTabela = getListaPedidovendamaterialForCriarTabela(bean);
			Boolean restricaoCliente = Boolean.FALSE;
			
			bean.setDtsincronizacaooffline(new Timestamp(System.currentTimeMillis())); 
			Pedidovendatipo pedidovendatipo = null;
			if(bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getCdpedidovendatipo() != null){
				pedidovendatipo = pedidovendatipoService.loadForEntrada(bean.getPedidovendatipo());
			}
			boolean bonificacao = false;
			boolean sincronizarComWMS = false;
			boolean gerarreceita = false;
			boolean comodato = false;
			boolean limitecreditocliente = false;
			boolean validadeLimiteCreditoExpirada = false;
			boolean requeraprovacaopedido = false;
			boolean requeraprovacaovalorvendaminimo = false;
			boolean prazomaiorprazotabela = isPrazomaiorprazotabela(bean);
			String materiaisWithValorAbaixoMinimo = "";
			Presencacompradornfe presencacompradornfe = null;
			
			if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
				bean.setPedidovendatipo(pedidovendatipo);
				sincronizarComWMS = pedidovendatipo.getSincronizarComWMS() != null ? pedidovendatipo.getSincronizarComWMS() : false;
				if(pedidovendatipo.getBonificacao() != null && pedidovendatipo.getBonificacao()){
					bonificacao = true;
				} else if(pedidovendatipo.getComodato() != null && pedidovendatipo.getComodato()){
					comodato = true;
				}
				if(pedidovendatipo.getGeracaocontareceberEnum() != null && 
						GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidovendatipo.getGeracaocontareceberEnum())){
					gerarreceita = true;
				}
				if(pedidovendatipo.getRequeraprovacaopedido() != null){
					requeraprovacaopedido = pedidovendatipo.getRequeraprovacaopedido();
				}
				if(pedidovendatipo.getRequeraprovacaovalorvendaminimo() != null){
					requeraprovacaovalorvendaminimo = pedidovendatipo.getRequeraprovacaovalorvendaminimo();
				}
				presencacompradornfe = pedidovendatipo.getPresencacompradornfe();
			}
			
			if(presencacompradornfe == null && bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
				Empresa empresa = empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.presencacompradornfe");
				if(empresa != null){
					presencacompradornfe = empresa.getPresencacompradornfe();
				}
			}
			if(presencacompradornfe == null){
				presencacompradornfe = Presencacompradornfe.PRESENCIAL;
			}
			bean.setPresencacompradornfe(presencacompradornfe);
			
			
			if(!bonificacao && !comodato){
				restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(bean.getCliente());
			}
			
			if(requeraprovacaovalorvendaminimo){
				materiaisWithValorAbaixoMinimo = verificaMaterialWithValorAbaixoMinimo(bean);
			}
			
			if(bonificacao || restricaoCliente || comodato || requeraprovacaopedido || prazomaiorprazotabela ||
					(requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !materiaisWithValorAbaixoMinimo.equals(""))){
				if(restricaoCliente){
					bean.setRestricaocliente(restricaoCliente);
				}
				bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			} else {					
				bean.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
			
			if(bean.getAprovacaopercentualdesconto() != null && bean.getAprovacaopercentualdesconto()){
				bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				if(bean.getObservacao() == null || !bean.getObservacao().contains("Necessita de aprova��o do desconto")){
					bean.setObservacao("Necessita de aprova��o do desconto. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
				}
			}
			
//			Money valorFinal =
			this.calculaValorVenda(bean);
			
			Boolean paramLimite = parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE);
			Boolean paramBloquearvendadevedor = parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_DEVEDOR);
			
			boolean existePedidovendapagamento = SinedUtil.isListNotEmpty(bean.getListaPedidovendapagamento());
			boolean permitevendasemanalise = false;
			if(bean.getListaPedidovendapagamento() != null){
				List<Integer> idsDocumentotipo = new ArrayList<Integer>();
				for(Pedidovendapagamento pedidovendapagamento : bean.getListaPedidovendapagamento()){
					if(pedidovendapagamento.getDocumentotipo() != null &&
							pedidovendapagamento.getDocumentotipo().getCddocumentotipo() != null &&
							!idsDocumentotipo.contains(pedidovendapagamento.getDocumentotipo().getCddocumentotipo())){
						idsDocumentotipo.add(pedidovendapagamento.getDocumentotipo().getCddocumentotipo());
					}
				}
				
				if(idsDocumentotipo != null && idsDocumentotipo.size() > 0){
					String whereInDocumentotipo = CollectionsUtil.concatenate(idsDocumentotipo, ",");
					permitevendasemanalise = documentotipoService.getPermiteVendaSemAnaliseByWhereIn(whereInDocumentotipo);
				}
			}
			
			Money valorTotal = new Money();
			Money valortotaldebito = new Money();
			if(existePedidovendapagamento && !permitevendasemanalise && (paramLimite || paramBloquearvendadevedor)){
				if(bean.getPrazopagamento() != null && bean.getPrazopagamento().getCdprazopagamento() != null){
					Prazopagamento prazopagamento = prazopagamentoService.load(bean.getPrazopagamento(), "prazopagamento.cdprazopagamento, " +
							"prazopagamento.avista");
					if(prazopagamento == null || (prazopagamento.getAvista() == null || !prazopagamento.getAvista())){
						if(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_CHEQUE)){
							ClientedevedorBean clientedevedorBean = vendaService.verificaDebitoAcimaLimiteByCliente(bean.getDocumentotipo() != null ? bean.getDocumentotipo().getCddocumentotipo().toString() : null, bean.getCliente(), bean.getValorfinal());
							if(clientedevedorBean != null && clientedevedorBean.getChequependente() != null && clientedevedorBean.getChequependente()){
								bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
							}
						}
						
						Cliente cliente = clienteService.load(bean.getCliente());
						
						if(paramLimite && clienteService.getValidadeLimiteCreditoExpirada(bean.getCliente())){
							bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
							validadeLimiteCreditoExpirada = true;
						}else {
							boolean clientepossuicontaatrasada = contareceberService.clientePossuiContasAtrasadas(cliente);
							if(paramBloquearvendadevedor && clientepossuicontaatrasada){
								bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
								clientepossuicontaatrasada = true;
							}
							
							if(paramLimite && (!clientepossuicontaatrasada || !paramBloquearvendadevedor)){
								ClienteInfoFinanceiraBean limiteCredido = vendaService.criarBeanClienteInfoFinanceira(null ,bean.getCliente());
								Money valorTotalPedidovendaComIpi = bean.getTotalvendaMaisImpostos();
								
//								valortotaldebito = contareceberService.getValorDebitoContasEmAbertoByCliente(cliente);
								valorTotal = valorTotalPedidovendaComIpi != null && limiteCredido.getSaldolimite() != null ? limiteCredido.getSaldolimite().subtract(valorTotalPedidovendaComIpi) : new Money();
								if(cliente.getCreditolimitecompra() != null && valorTotal.compareTo(0.006) != -1){
									bean.setLimitecreditoexcedido(Boolean.TRUE);
									bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
									limitecreditocliente = true;
								}
								if(cliente.getCreditolimitecompra() != null && cliente.getCreditolimitecompra().equals(0.0) && clientepossuicontaatrasada){
									bean.setLimitecreditoexcedido(Boolean.TRUE);
									bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);		
									limitecreditocliente = true;
								}
							}
						}
					}
				}
			}
			
	//		ATUALIZA O IDENTIFICADOR DA EMPRESA
			boolean isCriar = bean.getCdpedidovenda() == null;
			if(isCriar){
				Integer proximoIdentificador = empresaService.carregaProximoIdentificadorPedidovenda(bean.getEmpresa());
				if(proximoIdentificador != null){
					empresaService.updateProximoIdentificadorPedidovenda(bean.getEmpresa(), proximoIdentificador+1);
					bean.setIdentificador(proximoIdentificador.toString());
				}
			}
			
			setOrdemItem(bean);
			
			if(sincronizarComWMS && Pedidovendasituacao.PREVISTA.equals(bean.getPedidovendasituacao())){
				bean.setIntegrar(Boolean.TRUE);
			}
			
			saveOrUpdate(bean);
			pedidovendaRESTModel.setCdpedidovenda(bean.getCdpedidovenda());
			
			if(sincronizarComWMS){
				if(empresaService.isIntegracaoWms(bean.getEmpresa())){
					integracaoWMS(bean, "Cria��o do pedido de venda offline (app). Qtde de itens = " + bean.getListaPedidovendamaterial().size(), false);
				}
			}
			
			if (gerarreceita && (bean.getPedidovendasituacao() == null || !Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(bean.getPedidovendasituacao()))){
				Empresa empresa = empresaService.loadComContagerencialCentrocusto(bean.getEmpresa());
				List<Rateioitem> listarateioitem = prepareAndSaveVendaMaterial(bean, false, empresa, false, null);
				prepareAndSaveReceita(bean, listarateioitem, empresa);
				String contasareceber = "";
				for (Pedidovendapagamento pvp : bean.getListaPedidovendapagamento()){
					if(pvp.getDocumento() != null){
						if("".equals(contasareceber)) contasareceber = " Contas a receber geradas: ";
						contasareceber += "<a href= '/"+NeoWeb.getApplicationName()
									+"/financeiro/crud/Contareceber?ACAO=consultar&cddocumento=" 
										+ pvp.getDocumento().getCddocumento() 
										+ "'> "
										+ pvp.getDocumento().getCddocumento() 
										+ " </a>,";
					}
				}
				bean.setObservacao(bean.getObservacao() + (contasareceber.length() > 0 ? contasareceber.substring(0, contasareceber.length()-1) : ""));		
			}
			
			if(limitecreditocliente){
				String obsLimitecredito = "Necessita de aprova��o do limite de cr�dito. ";
				if(valortotaldebito != null && valortotaldebito.getValue().doubleValue() > 0){
					Date dtvencimento = documentoService.getPrimeiroVencimentoContasEmAbertoByCliente(bean.getCliente());
					Integer numDias = 0;							
					if(dtvencimento != null)
						numDias = SinedDateUtils.diferencaDias(SinedDateUtils.currentDateToBeginOfDay(), dtvencimento);
					
					obsLimitecredito += "( Cliente com pend�ncia financeira " +
							(numDias != null && numDias > 0 ? (", em atraso: " + numDias + " dias) ") : ") ");
				}
				bean.setObservacao(obsLimitecredito + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			
	//		if(!usarSequencial && isCriar){
	//			bean.setObservacao("N�o foi usado o sequencial do identificador. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
	//		}
			
			if(prazomaiorprazotabela){
				bean.setObservacao("Aguardando aprova��o devido a mudan�a no prazo de pagamento. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			
			if (validadeLimiteCreditoExpirada) {
				bean.setObservacao("Cliente com validade do limite de cr�dito expirada. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			
			if(requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !"".equals(materiaisWithValorAbaixoMinimo)){
				bean.setObservacao("Aguardando aprova��o por ter material com valor de venda abaixo do m�nimo. (Materiais: " + materiaisWithValorAbaixoMinimo + "). " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
	
			gerarReservaAoSalvar(pedidovendatipo, bean);
			
			pedidovendahistoricoService.salvaPedidoHistoricoAndroid(bean);
					
			try {
				if(prazomaiorprazotabela){
					bean.setObservacao("Aguardando aprova��o devido a mudan�a no prazo de pagamento. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
				}
				
				if(sincronizarComWMS){
					if(empresaService.isIntegracaoWms(bean.getEmpresa())){
						integracaoWMS(bean, "Cria��o do pedido de venda offline (app). Qtde de itens = " + bean.getListaPedidovendamaterial().size(), false);
					}
				}
				
				verificarAndCriarTabelaprecoCliente(null, bean, listaVendamaterialForCriarTabela);
			} catch (Exception e) {}
			
			try {
				if(bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getColetaautomatica() != null && bean.getPedidovendatipo().getColetaautomatica()){
					gerarColetaAutomatica(bean.getCdpedidovenda().toString(), bean.getEmitirnotacoleta(), null);
				}
			} catch (Exception e) {
				String nomeMaquina = "N�o encontrado.";
				try {  
		            InetAddress localaddr = InetAddress.getLocalHost();  
		            nomeMaquina = localaddr.getHostName();  
		        } catch (UnknownHostException e3) {}  
				
	
		        String url = "N�o encontrado";
				try {  
					url = SinedUtil.getUrlWithContext();
		        } catch (Exception e3) {}
		        
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				enviarEmailErro("[W3ERP] Erro ao gerar coleta autom�tica.","Erro ao gerar coleta ao salvar pedido de venda (app android). (M�quina: " + nomeMaquina + ". URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
			}
		}  catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PEDIDOVENDA_PROTOCOLOOFFLINE")) {
				Pedidovenda pedidovenda = loadByProtocolooffline(command.getProtocolo());
				pedidovendaRESTModel.setCdpedidovenda(pedidovenda.getCdpedidovenda());
				
				String nomeMaquina = "N�o encontrado.";
				try {  
		            InetAddress localaddr = InetAddress.getLocalHost();  
		            nomeMaquina = localaddr.getHostName();  
		        } catch (UnknownHostException e3) {}  
				

		        String url = "N�o encontrado";
				try {  
					url = SinedUtil.getUrlWithContext();
		        } catch (Exception e3) {}
		        
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				
				String erro = "Erro ao salvar pedido de venda (app android). (M�quina: " + nomeMaquina + ". URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
				enviarEmailErro("[W3ERP] Erro ao sincronizar pedido de venda. Protocolo: " + command.getProtocolo() + ", Cdpedidovenda: " + pedidovenda.getCdpedidovenda(),erro);
			}
		} catch (Exception e){
			String nomeMaquina = "N�o encontrado.";
			try {  
	            InetAddress localaddr = InetAddress.getLocalHost();  
	            nomeMaquina = localaddr.getHostName();  
	        } catch (UnknownHostException e3) {}  
			

	        String url = "N�o encontrado";
			try {  
				url = SinedUtil.getUrlWithContext();
	        } catch (Exception e3) {}
	        
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			
			String erro = "Erro ao salvar pedido de venda (app android). (M�quina: " + nomeMaquina + ". URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
			enviarEmailErro("[W3ERP] Erro ao sincronizar pedido de venda.",erro);
			
			Pedidovendaoffline pedidovendaoffline = new Pedidovendaoffline();
			pedidovendaoffline.setJson(View.convertToJson(command).toString());
			pedidovendaoffline.setMsgErro(e.getMessage());
			pedidovendaoffline.setDtcriacaooffline(command.getDtpedidovenda() != null ? new Timestamp(command.getDtpedidovenda().getTime()) : null);
			pedidovendaoffline.setProtocolooffline(command.getProtocolo());
			
			pedidovendaofflineService.saveOrUpdate(pedidovendaoffline);
			
			pedidovendaRESTModel.setErroSincronizacao(SinedUtil.FORMATADOR_DATA_HORA.format(new Timestamp(System.currentTimeMillis())) + " - " + erro);
		}
		
		return pedidovendaRESTModel;
	}
	
	public void faturarColetaWSSOAP(FaturarColetaBean bean) {
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		List<FaturarColetaItemBean> item = bean.getItem();
		if(item != null && item.size() > 0){
			List<String> ids = new ArrayList<String>();
			
			for (FaturarColetaItemBean itBean : item) {
				String identificador = itBean.getIdentificador();
				
				List<Pedidovenda> listaPedidovendaIdentificadorCnpj = this.findByIdentificadorCnpj(identificador, cnpj_empresa);
				if(listaPedidovendaIdentificadorCnpj != null && listaPedidovendaIdentificadorCnpj.size() > 0){
					if(listaPedidovendaIdentificadorCnpj.size() > 1){
						throw new SinedException("Mais de um pedido encontrado com o identificador para a empresa.");
					}
					
					Pedidovenda pedidovenda = listaPedidovendaIdentificadorCnpj.get(0);
					ids.add(pedidovenda.getCdpedidovenda() + "");
				}
			}
			
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(CollectionsUtil.concatenate(ids, ","));
			StringBuilder whereInColeta = new StringBuilder();
			if (SinedUtil.isListNotEmpty(listaColeta)){
				for(Coleta coleta : listaColeta){
					if(!coletaService.isColetaPossuiNotaTodosItensSemSaldo(coleta.getCdcoleta().toString(), Tipooperacaonota.ENTRADA)){
						whereInColeta.append(coleta.getCdcoleta()).append(",");
					}
				}
				if(whereInColeta.length() == 0){
					throw new SinedException("J� foram emitidas notas para todos os itens da coleta.");
				}
			}
			
			if(whereInColeta.length() == 0){
				throw new SinedException("Nenhuma coleta encontrada.");
			}
			
			this.geraNotaByColeta(whereInColeta.substring(0, whereInColeta.length()-1), item);
		}
	}
	
	private void updateValorUnitarioColeta(Pedidovenda pedidovenda, List<ManterPedidovendaItemBean> item){
		if(pedidovenda.getListaPedidovendamaterial() != null && pedidovenda.getListaPedidovendamaterial().size() > 0){
			String whereInPedidovendamaterial = CollectionsUtil.listAndConcatenate(pedidovenda.getListaPedidovendamaterial(), "cdpedidovendamaterial", ","); 
			List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByPedidovendamaterial(whereInPedidovendamaterial);
			for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
				for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
					boolean achou = false;
					List<ColetaMaterialPedidovendamaterial> listaColetaMaterialPedidovendamaterial = coletaMaterial.getListaColetaMaterialPedidovendamaterial();
					for (ColetaMaterialPedidovendamaterial coletaMaterialPedidovendamaterial : listaColetaMaterialPedidovendamaterial) {
						if(coletaMaterialPedidovendamaterial.getPedidovendamaterial() != null && 
								coletaMaterialPedidovendamaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null && 
								coletaMaterialPedidovendamaterial.getPedidovendamaterial().getCdpedidovendamaterial().equals(pedidovendamaterial.getCdpedidovendamaterial())){
							achou = true;
						}
					}
					if(achou){
						Double valorColeta = getValorColeta(pedidovendamaterial, item);
						if(valorColeta != null){
							coletaMaterialService.updateValorUnitario(coletaMaterial, valorColeta);
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo que gera coleta autom�tica dos pedidos de venda
	*
	* @param whereIn
	* @param emitirNotaColeta
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	protected void gerarColetaAutomatica(String whereIn, Boolean emitirNotaColeta, List<ManterPedidovendaItemBean> item) {
		List<Pedidovenda> listaPedidovenda = findForColetarProducao(whereIn);
		if(SinedUtil.isListNotEmpty(listaPedidovenda)){
			List<Movimentacaoestoque> listaMovimentacaoestoqueColeta = movimentacaoestoqueService.findByPedidovenda(whereIn);
			listaPedidovenda = createListaPedidovendaForColetarProducao(listaPedidovenda, listaMovimentacaoestoqueColeta);
			
			if(SinedUtil.isListNotEmpty(listaPedidovenda)){
				Localarmazenagem localarmazenagemcoleta;
				for(Pedidovenda pedidovenda : listaPedidovenda){
					localarmazenagemcoleta = pedidovenda.getLocalarmazenagem();
					if(pedidovenda.getPedidovendatipo() != null && pedidovenda.getPedidovendatipo().getLocalarmazenagemcoleta() != null){
						localarmazenagemcoleta = pedidovenda.getPedidovendatipo().getLocalarmazenagemcoleta();
					}
					if(localarmazenagemcoleta != null){
						if(SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
							for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
								pedidovendamaterial.setLocalarmazenagemcoleta(localarmazenagemcoleta);
							}
						}
					}
				}
				
				ColetarProducaoBean coletarProducaoBean = new ColetarProducaoBean();
				coletarProducaoBean.setListaPedidovenda(listaPedidovenda);
				List<Coleta> listaColeta = saveColetarProducao(coletarProducaoBean, item);
				if(emitirNotaColeta != null && emitirNotaColeta){
					this.geraNotaByColeta(CollectionsUtil.listAndConcatenate(listaColeta, "cdcoleta", ","), null);
				}
			}
		}
	}
	
	/**
	 * M�todo que gera a nota de produto baseado nas coletas passadas por par�metro.
	 *
	 * @param whereInColeta
	 * @author Rodrigo Freitas
	 * @since 17/08/2016
	 */
	protected void geraNotaByColeta(String whereInColeta, List<FaturarColetaItemBean> item) {
		Boolean devolucao = false;
		Notafiscalproduto notafiscalproduto = notafiscalprodutoService.criaNotaByColeta(whereInColeta, devolucao, item);
		
		if (notafiscalproduto==null){
			return;
		}
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInColeta)){
			String[] ids = whereInColeta.split(",");
			String link = "/w3erp/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=";
		
			StringBuilder sb = new StringBuilder("Coleta: ");
			
			for(int i=0; i<ids.length; i++){
				sb.append("<a href='" + link + ids[i] +"'>"+ids[i]+", </a>");
			}
			
			if(sb.length() > 0){
				sb.setCharAt(sb.lastIndexOf(","), '.');

				notafiscalproduto.setListaNotaHistorico(new ArrayList<NotaHistorico>());
				NotaHistorico nh = new NotaHistorico();
				
				Usuario usuario = SinedUtil.getUsuarioLogado();
				if(usuario != null){
					nh.setCdusuarioaltera(usuario.getCdpessoa());
				}
				nh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				nh.setNotaStatus(notafiscalproduto.getNotaStatus());
				nh.setObservacao(sb.toString());
				notafiscalproduto.getListaNotaHistorico().add(nh);
			}
		}
		
		if(notafiscalproduto.getEmpresa() == null){
			notafiscalproduto.setEmpresa(empresaService.loadPrincipal());
		}

		notafiscalprodutoService.saveOrUpdate(notafiscalproduto);
		notafiscalprodutoService.updateNumeroNFProduto(notafiscalproduto);
		
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereInColeta)){
			NotafiscalprodutoColeta nfpc;
			for(String cdcoleta : whereInColeta.split(",")){
				nfpc = new NotafiscalprodutoColeta(Integer.parseInt(cdcoleta), notafiscalproduto.getCdNota(), devolucao);
				notafiscalprodutoColetaService.saveOrUpdate(nfpc);
			}
		}
	}
	
	public Pedidovenda loadByProtocolooffline(String protocolo) {
		return pedidovendaDAO.loadByProtocolooffline(protocolo);
	}
	
	public void enviarEmailErro(String assunto,String mensagem) {
		if(!SinedUtil.isAmbienteDesenvolvimento()){
			List<String> listTo = new ArrayList<String>();
			listTo.add("rodrigo.freitas@linkcom.com.br");
			listTo.add("luiz.silva@linkcom.com.br");
			
			try {
				EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
				
				email
					.setFrom("w3erp@w3erp.com.br")
					.setListTo(listTo)
					.setSubject(assunto)
					.addHtmlText(mensagem)
					.sendMessage();
			} catch (Exception e) {
				throw new SinedException("Erro ao enviar e-mail ao administrador.",e);
			}
		}
	}
	
	protected Pedidovenda createPedidovendaByAndroid(PedidovendaRESTWSBean command) {
		Pedidovenda pedidovenda = new Pedidovenda();
		
		pedidovenda.setCliente(command.getCdcliente() != null ? new Cliente(command.getCdcliente()) : null);
		pedidovenda.setContato(command.getCdcontato() != null ? new Contato(command.getCdcontato()) : null);
		pedidovenda.setPedidovendatipo(command.getCdpedidovendatipo() != null ? new Pedidovendatipo(command.getCdpedidovendatipo()) : null);
		pedidovenda.setProjeto(command.getCdprojeto() != null ? new Projeto(command.getCdprojeto()) : null);
		pedidovenda.setObservacao(command.getObservacoes());
		pedidovenda.setFrete(command.getCdresponsavelfrete() != null ? new ResponsavelFrete(command.getCdresponsavelfrete()) : null);
		pedidovenda.setTerceiro(command.getCdtransportadora() != null ? new Fornecedor(command.getCdtransportadora()) : null);
		pedidovenda.setEndereco(command.getCdendereco() != null ? new Endereco(command.getCdendereco()) : null);
		pedidovenda.setIdentificador(command.getIdentificador());
		pedidovenda.setDtpedidovenda(command.getDtpedidovenda() != null ? new java.sql.Date(command.getDtpedidovenda().getTime()) : null);
		pedidovenda.setDtprazoentrega(command.getDtprazoentrega() != null ? new java.sql.Date(command.getDtprazoentrega().getTime()) : null);
		pedidovenda.setEmpresa(command.getCdempresa() != null ? new Empresa(command.getCdempresa()) : empresaService.loadPrincipal());
		pedidovenda.setColaborador(command.getCdcolaborador() != null ? new Colaborador(command.getCdcolaborador()) : null);
		pedidovenda.setValor(command.getValor());
		pedidovenda.setPercentualdesconto(command.getPercentualdesconto());
		pedidovenda.setDesconto(command.getDesconto() != null ? new Money(command.getDesconto()) : null);
		pedidovenda.setValorusadovalecompra(command.getValorusadovalecompra() != null ? new Money(command.getValorusadovalecompra()): null);
		pedidovenda.setValorfrete(command.getValorfrete() != null ? new Money(command.getValorfrete()) : null);
		pedidovenda.setValorfinal(command.getValorfinal() != null ? new Money(command.getValorfinal()) : null);
		pedidovenda.setSaldofinal(command.getSaldofinal() != null ? new Money(command.getSaldofinal()) : null);
		pedidovenda.setPrazopagamento(command.getCdprazopagamento() != null ? new Prazopagamento(command.getCdprazopagamento()) : null);
		pedidovenda.setDocumentotipo(command.getCddocumentotipo() != null ? new Documentotipo(command.getCddocumentotipo()) : null);
		pedidovenda.setConta(command.getCdconta() != null ? new Conta(command.getCdconta()) : null);
		pedidovenda.setIndicecorrecao(command.getCdindicecorrecao() != null ? new Indicecorrecao(command.getCdindicecorrecao()) : null);
		pedidovenda.setProtocolooffline(command.getProtocolo());
		pedidovenda.setCdusuarioaltera(command.getCdusuarioaltera() != null ? command.getCdusuarioaltera().intValue() : null);
		pedidovenda.setLocalarmazenagem(command.getCdlocalarmazenagem() != null ? new Localarmazenagem(command.getCdlocalarmazenagem()) : null);
		pedidovenda.setEmitirnotacoleta(command.getEmitirnotacoleta());
		
		if(command.getPedidovendasituacao() != null){
			if(Pedidovendasituacao.AGUARDANDO_APROVACAO.ordinal() == command.getPedidovendasituacao().intValue()){
				pedidovenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			}else if(Pedidovendasituacao.PREVISTA.ordinal() == command.getPedidovendasituacao().intValue()){
				pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
		}
		
		if(SinedUtil.isListNotEmpty(command.getListaPedidovendamaterialRESTWSBean())){
			List<Pedidovendamaterial> lista = new ArrayList<Pedidovendamaterial>();
			PneuSegmento pneuSegmentoPrincipal = pneuSegmentoService.loadPrincipal();
			for(PedidovendamaterialRESTWSBean item : command.getListaPedidovendamaterialRESTWSBean()){
				Pedidovendamaterial pvm = new Pedidovendamaterial();
				Material material = item.getCdmaterial() != null ? new Material(item.getCdmaterial()) : null;
				if(material != null){
					pvm.setMaterial(material);
					
					Unidademedida unidademedida = null;
					if(item.getCdunidademedida() != null){
						unidademedida = new Unidademedida(item.getCdunidademedida());
					} else {
						material = materialService.unidadeMedidaMaterial(material);
						if(material != null){
							unidademedida = material.getUnidademedida();
						}
					}
				    pvm.setUnidademedida(unidademedida);
				    pvm.setQuantidade(item.getQuantidade());
				    pvm.setPreco(item.getPreco() != null ? item.getPreco() : 0d);
				    pvm.setDesconto(item.getDesconto() != null ? new Money(item.getDesconto()) : null);
				    pvm.setSaldo(item.getSaldo() != null ? new Money(item.getSaldo()) : null);
				    pvm.setDtprazoentrega(item.getDtprazoentrega());
				    pvm.setObservacao(item.getObservacoes());
				    pvm.setQtdereferencia(item.getQtdereferencia());
				    pvm.setFatorconversao(item.getFatorconversao());
				    pvm.setValorcustomaterial(item.getValorcustomaterial() != null && item.getValorcustomaterial() != 0 ? item.getValorcustomaterial() : null);
				    pvm.setValorvendamaterial(item.getValorvendamaterial() != null && item.getValorvendamaterial() != 0 ? item.getValorvendamaterial() : null);
				    pvm.setMaterialcoleta(item.getCdmaterialcoleta() != null ? new Material(item.getCdmaterialcoleta()) : null);
				    
				    if(SinedUtil.isRecapagem() && existeInfoPneu(item.getPneuRESTWSBean())){
				    	Pneu pneu = new Pneu();
				    	pneu.setPneuSegmento(pneuSegmentoPrincipal);
				    	pneu.setCdpneu(item.getPneuRESTWSBean().getCdpneu());
				    	
				    	if(item.getPneuRESTWSBean().getCdpneumarca() != null) pneu.setPneumarca(new Pneumarca(item.getPneuRESTWSBean().getCdpneumarca()));
				    	if(item.getPneuRESTWSBean().getCdpneumodelo() != null) pneu.setPneumodelo(new Pneumodelo(item.getPneuRESTWSBean().getCdpneumodelo()));
				    	if(item.getPneuRESTWSBean().getCdpneumedida() != null) pneu.setPneumedida(new Pneumedida(item.getPneuRESTWSBean().getCdpneumedida()));
				    	pneu.setSerie(item.getPneuRESTWSBean().getSerie());
				    	pneu.setDot(item.getPneuRESTWSBean().getDot());
				    	if(item.getPneuRESTWSBean().getNumeroreforma() != null) pneu.setNumeroreforma(Pneureforma.values()[item.getPneuRESTWSBean().getNumeroreforma()]);
				    	if(item.getPneuRESTWSBean().getCdmaterialbanda() != null) pneu.setMaterialbanda(new Material(item.getPneuRESTWSBean().getCdmaterialbanda()));
				    	
				    	pneuService.saveOrUpdate(pneu);
				    	pvm.setPneu(pneu);
				    }
				    
				    try {
				    	if(pvm.getPercentualdesconto() == null){
				    		pedidovendamaterialService.setPercentualDesconto(pvm);
				    	}
					} catch (Exception e) {
					}
				    
					lista.add(pvm);
				}
			}
			pedidovenda.setListaPedidovendamaterial(lista);
		}
		if(SinedUtil.isListNotEmpty(command.getListaPedidovendapagamentoRESTWSBean())){
			List<Pedidovendapagamento> lista = new ArrayList<Pedidovendapagamento>();
			for(PedidovendapagamentoRESTWSBean item : command.getListaPedidovendapagamentoRESTWSBean()){
				Pedidovendapagamento pvp = new Pedidovendapagamento();
				pvp.setDocumentotipo(item.getCddocumentotipo() != null ? new Documentotipo(item.getCddocumentotipo()) : null);
			    pvp.setDataparcela(item.getDataparcela() != null ? new java.sql.Date(item.getDataparcela().getTime()) : null);
			    pvp.setValororiginal(item.getValororiginal() != null ? new Money(item.getValororiginal()) : null);
			    try {pvp.setBanco(item.getBanco() != null ? Integer.parseInt(item.getBanco()) : null);} catch (Exception e) {}
			    try {pvp.setAgencia(item.getAgencia() != null ? Integer.parseInt(item.getBanco()) : null);} catch (Exception e) {}
			    try {pvp.setConta(item.getConta() != null ? Integer.parseInt(item.getBanco()) : null);} catch (Exception e) {}
			    pvp.setNumero(item.getNumero());
				lista.add(pvp);
			}
			pedidovenda.setListaPedidovendapagamento(lista);
		}
		
		calcularImposto(pedidovenda);
		ajustePedidovendapagamento(pedidovenda);
		return pedidovenda;
	}
	
	/**
	* M�todo que verifica se existe informa��o de pneu
	*
	* @param pneuRESTWSBean
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	protected boolean existeInfoPneu(PneuRESTWSBean pneuRESTWSBean) {
		return pneuRESTWSBean != null && (
				pneuRESTWSBean.getCdpneumarca() != null ||
				pneuRESTWSBean.getCdpneumodelo() != null ||
				pneuRESTWSBean.getCdpneumedida() != null ||
				org.apache.commons.lang.StringUtils.isNotBlank(pneuRESTWSBean.getSerie()) ||
				org.apache.commons.lang.StringUtils.isNotBlank(pneuRESTWSBean.getDot()) ||
				pneuRESTWSBean.getNumeroreforma() != null ||
				pneuRESTWSBean.getCdmaterialbanda() != null);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa_cnpj
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Pedidovenda> findByEmpresaIdentificador(String empresa_cnpj, String identificador) {
		return pedidovendaDAO.findByEmpresaIdentificador(empresa_cnpj, identificador);
	}
	
	public void preecheValorVendaComTabelaPrecoItensKit(Material material, Pedidovendatipo pedidovendatipo, Cliente cliente, Empresa empresa,	Prazopagamento prazopagamento) {
		try {
			if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialrelacionado())){
				for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
					if(materialrelacionado.getMaterialpromocao() != null){
						if(materialrelacionado.getValorvenda() != null){
							materialrelacionado.getMaterialpromocao().setValorvenda(materialrelacionado.getValorvenda().getValue().doubleValue());
						}
						preecheValorVendaComTabelaPreco(materialrelacionado.getMaterialpromocao(), pedidovendatipo, cliente, empresa, materialrelacionado.getMaterialpromocao().getUnidademedida(), prazopagamento);
						if(materialrelacionado.getMaterialpromocao().getValorvenda() != null){
							materialrelacionado.setValorvenda(new Money(materialrelacionado.getMaterialpromocao().getValorvenda()));
							materialrelacionado.setValorvendasemdesconto(new Money(materialrelacionado.getMaterialpromocao().getValorvendasemdesconto()));
							materialrelacionado.getMaterialpromocao().setConsiderarValorvendaCalculado(true);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void preecheValorVendaComTabelaPrecoItensKitflexivel(Material material, Pedidovendatipo pedidovendatipo, Cliente cliente, Empresa empresa,	Prazopagamento prazopagamento) {
		try {
			if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialkitflexivel())){
				for(Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()){
					if(materialkitflexivel.getMaterialkit() != null){
						if(materialkitflexivel.getValorvenda() != null){
							materialkitflexivel.getMaterialkit().setValorvenda(materialkitflexivel.getValorvenda().getValue().doubleValue());
						}
						preecheValorVendaComTabelaPreco(materialkitflexivel.getMaterialkit(), pedidovendatipo, cliente, empresa, materialkitflexivel.getMaterialkit().getUnidademedida(), prazopagamento, material);
						if(materialkitflexivel.getMaterialkit().getValorvenda() != null){
							materialkitflexivel.setValorvenda(new Money(materialkitflexivel.getMaterialkit().getValorvenda()));
							materialkitflexivel.getMaterialkit().setConsiderarValorvendaCalculado(true);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void preecheValorVendaComTabelaPrecoItensProducao(Material material, Pedidovendatipo pedidovendatipo, Cliente cliente, Empresa empresa,	Prazopagamento prazopagamento) {
		try {
			if(material != null && SinedUtil.isListNotEmpty(material.getListaProducao())){
				for(Materialproducao materialprpducao : material.getListaProducao()){
					if(materialprpducao.getMaterial() != null){
						preecheValorVendaComTabelaPreco(materialprpducao.getMaterial(), pedidovendatipo, cliente, empresa, materialprpducao.getMaterial().getUnidademedida(), prazopagamento);
						if(materialprpducao.getMaterial().getValorvenda() != null){
							materialprpducao.setPreco(materialprpducao.getMaterial().getValorvenda());
							materialprpducao.getMaterial().setConsiderarValorvendaCalculado(true);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void ajaxSetDadosSessaoForLoteAutocomplete(WebRequestContext request, String tela){
		String cdmaterial = request.getParameter("cdmaterial");
		String cdempresa = request.getParameter("cdempresa");
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		String cdunidademedida = request.getParameter("cdunidademedida");
		
		Material material = null;
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		Unidademedida unidademedida = null;
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
				request.getSession().setAttribute(tela + "_EMPRESA_LOTE_AUTOCOMPLETE", empresa);
			}
		} catch (NumberFormatException e) {}
		try {
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
				request.getSession().setAttribute(tela + "_LOCALARMAZENAGEM_LOTE_AUTOCOMPLETE", localarmazenagem);
			}
		} catch (NumberFormatException e) {}
		try {
			if(cdmaterial != null && !"".equals(cdmaterial)){
				material = new Material(Integer.parseInt(cdmaterial));
				request.getSession().setAttribute(tela + "_MATERIAL_LOTE_AUTOCOMPLETE", material);
			}
		} catch (NumberFormatException e) {}
		try {
			if(cdunidademedida != null && !"".equals(cdunidademedida)){
				unidademedida = new Unidademedida(Integer.parseInt(cdunidademedida));
				request.getSession().setAttribute(tela + "_UNIDADEMEDIDA_LOTE_AUTOCOMPLETE", unidademedida);
			}
		} catch (NumberFormatException e) {}
	}
	
	public boolean isIdentificadorCadastrado(Pedidovenda pedidovenda){
		return pedidovendaDAO.isIdentificadorCadastrado(pedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public boolean verificaSincronizacaoComWmsForConfirmacao(String whereIn){
		return pedidovendaDAO.verificaSincronizacaoComWmsForConfirmacao(whereIn);
	}
	
	public void setOrdemItem(Pedidovenda pedidoVenda) {
		if(pedidoVenda != null && SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendamaterial())){
			int ordem = 1;
			for(Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()){
				pedidovendamaterial.setOrdem(ordem);
				ordem++;
			}
		}
	}
	
	/**
	* M�todo que valida o total dos pagamentos de representa��o
	*
	* @param request
	* @param venda
	* @param pedidovendatipo
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public boolean validaListaVendapagamentorepresentacao(WebRequestContext request, Pedidovenda venda, Pedidovendatipo pedidovendatipo) {
		if(venda != null && pedidovendatipo != null && pedidovendatipo.getRepresentacao() != null &&
				pedidovendatipo.getRepresentacao() &&
				venda.getValortotalrepresentacao() != null){
			
			Money valortotalrepresentacao = venda.getValortotalrepresentacao();
			Money valortotalpagamentosrepresentacao = venda.getTotalparcelarepresentacao();
				
			if(valortotalrepresentacao != null && valortotalpagamentosrepresentacao != null && 
					SinedUtil.round(valortotalrepresentacao.getValue(), 2).compareTo(SinedUtil.round(valortotalpagamentosrepresentacao.getValue(), 2)) != 0){
				request.addError("Parcelas de representa��o com valor diferente do valor total da comiss�o.");
				return Boolean.FALSE;
			}
			if(valortotalpagamentosrepresentacao != null && valortotalpagamentosrepresentacao.getValue().doubleValue() < 0){
				request.addError("Valor total das parcelas de representa��o n�o pode ser negativo.");
				return Boolean.FALSE;
			}
		}
		return true;
	}
	
	public boolean validaListaPedidovendapagamento(WebRequestContext request, Pedidovenda bean, BindException errors){
		boolean retorno = true;
		if(bean.getListaPedidovendapagamento() != null && !bean.getListaPedidovendapagamento().isEmpty()){
			List<Cheque> listaCheque = new ArrayList<Cheque>();
			Cheque cheque;
			for(Pedidovendapagamento pedidovendapagamento : bean.getListaPedidovendapagamento()){
				Documentotipo documentotipo = documentotipoService.load(pedidovendapagamento.getDocumentotipo());
				pedidovendapagamento.setDocumentotipo(documentotipo);
				
				if(pedidovendapagamento.getPodeGerarCheque()){
					
					cheque = new Cheque();
					cheque.setBanco(pedidovendapagamento.getBanco());
					cheque.setAgencia(pedidovendapagamento.getAgencia());
					cheque.setConta(pedidovendapagamento.getConta());
					cheque.setNumero(pedidovendapagamento.getNumero() != null ? pedidovendapagamento.getNumero().toString() : "");
					cheque.setEmitente(pedidovendapagamento.getEmitente());
					cheque.setCpfcnpj(pedidovendapagamento.getCpfcnpj());
					cheque.setDtbompara(pedidovendapagamento.getDataparcela());
					cheque.setValor(pedidovendapagamento.getValororiginal());
					cheque.setEmpresa(bean.getEmpresa());
					cheque.setChequesituacao(Chequesituacao.PREVISTO);
					
					listaCheque.add(cheque);
					if(chequeService.verificarDuplicidadeCheque(cheque)){
						retorno = false;
						if(errors != null){
							errors.reject("001", "Cheque de n�mero " + cheque.getNumero() + " j� existe.");
						}else {
							request.addError("Cheque de n�mero " + cheque.getNumero() + " j� existe.");
						}
					}					
				}
			}
			if(listaCheque != null && !listaCheque.isEmpty()){
				if(chequeService.isDuplicidadeCheque(listaCheque)){
					retorno = false;
					if(errors != null){
						errors.reject("001", "Existe cheque duplicado nas parcelas.");
					}else {
						request.addError("Existe cheque duplicado nas parcelas.");
					}
				}
			}
		}
		return retorno;
	}
	
	public List<Integer> prepareAndSaveReceitaRepresentacao(Pedidovenda venda, List<Rateioitem> listarateioitem, Empresa empresa, Boolean naoGerarreceita) {
		List<Integer> listaDocumentosGerados = new ArrayList<Integer>();
		
		if(venda != null && SinedUtil.isListNotEmpty(venda.getListaPedidovendapagamentorepresentacao())){
			Empresa aux_empresa = empresaService.loadComContagerencialCentrocusto(empresa);
			List<Rateioitem> listaRateioitemRepresentacao = new ArrayList<Rateioitem>();
			if(SinedUtil.isListNotEmpty(listarateioitem)){
				for(Rateioitem ri : listarateioitem){
					Rateioitem novoRateioitem = new Rateioitem(ri);
					if(aux_empresa != null){
						if(aux_empresa.getContagerencial() != null)
							novoRateioitem.setContagerencial(aux_empresa.getContagerencial());
						if(aux_empresa.getCentrocusto() != null)
							novoRateioitem.setCentrocusto(aux_empresa.getCentrocusto());
					}
					novoRateioitem.setProjeto(venda.getProjeto());
					listaRateioitemRepresentacao.add(novoRateioitem);
				}
			}
			
			HashMap<Integer, List<Pedidovendapagamentorepresentacao>> mapFornecedorRepresentacao = new HashMap<Integer, List<Pedidovendapagamentorepresentacao>>();
			for(Pedidovendapagamentorepresentacao vendapagamentorepresentacao : venda.getListaPedidovendapagamentorepresentacao()){
				if(vendapagamentorepresentacao.getFornecedor() != null){
					if(mapFornecedorRepresentacao.get(vendapagamentorepresentacao.getFornecedor().getCdpessoa()) == null)
						mapFornecedorRepresentacao.put(vendapagamentorepresentacao.getFornecedor().getCdpessoa(), new ArrayList<Pedidovendapagamentorepresentacao>());
					mapFornecedorRepresentacao.get(vendapagamentorepresentacao.getFornecedor().getCdpessoa()).add(vendapagamentorepresentacao);
				}
			}
			
			if(mapFornecedorRepresentacao.size() > 0){
				for(Integer cdfornecedor : mapFornecedorRepresentacao.keySet()){
					listaDocumentosGerados.addAll(prepareAndSaveReceitaRepresentacao(venda, listaRateioitemRepresentacao, mapFornecedorRepresentacao.get(cdfornecedor), new Fornecedor(cdfornecedor), empresa, naoGerarreceita));
				}
			}
		}
		
		if(naoGerarreceita == null || !naoGerarreceita ){
			verificaComissionamentoRepresentacao(venda, null, null, null, false);
		}
		
		return listaDocumentosGerados;
	}
	
	public List<Integer> prepareAndSaveReceitaRepresentacao(Pedidovenda venda, List<Rateioitem> listarateioitem, List<Pedidovendapagamentorepresentacao> listaVendapagamentorepresentacao, Fornecedor fornecedor, Empresa empresa, Boolean naoGerarreceita) {
		List<Integer> listaDocumentosGerados = new ArrayList<Integer>();
		Rateio rateio = null;
		Documento contareceber;
		Documentohistorico documentohistoricoContareceber;
		Parcelamento parcelamento = new Parcelamento();
		Fornecedor aux_fornecedor = fornecedorService.carregaFornecedor(fornecedor);
		Empresa fornecedorEmpresa = null;
		Empresarepresentacao empresarepresentacao = empresarepresentacaoService.getEmpresarepresentacao(empresa, aux_fornecedor);
		
		rateioitemService.agruparItensRateio(listarateioitem);
		rateio = new Rateio();
		rateio.setListaRateioitem(listarateioitem);
		rateioService.limpaReferenciaRateio(rateio);
		rateioService.saveOrUpdate(rateio);
		
		if(venda.getPrazopagamentorepresentacao() != null){
			parcelamento.setPrazopagamento(venda.getPrazopagamentorepresentacao());
			parcelamento.setIteracoes(venda.getListaPedidovendapagamentorepresentacao().size());
			parcelamentoService.saveOrUpdate(parcelamento);
		}
		
		Parcela parcela;
		int i = 1;
		
		List<Documento> listaContapagarRepresentacao = new ArrayList<Documento>(); 
				
		Rateio rateioParcelas;
		Documentotipo documentotipo;
		int numParcela = 1;
		Conta contaboletorepresentacao = venda.getContaboletorepresentacao();
		for (Pedidovendapagamentorepresentacao vpr : venda.getListaPedidovendapagamentorepresentacao()) {
			if(vpr.getDocumento() == null && (naoGerarreceita == null || !naoGerarreceita )){
				contareceber = new Documento();
				documentotipo = documentotipoService.load(vpr.getDocumentotipo());
			    contareceber.setDocumentoacao(Documentoacao.PREVISTA);
				contareceber.setConta(contaboletorepresentacao);
				if(contaboletorepresentacao != null && contaboletorepresentacao.getCdconta() != null){
					Conta conta = contaService.carregaContaComMensagem(contaboletorepresentacao, null);
					if(conta != null){
						if(conta.getContacarteira() != null && conta.getContacarteira().getCdcontacarteira() != null){
							contareceber.setContacarteira(conta.getContacarteira());
						}
					}
					contareceber.setVinculoProvisionado(contaboletorepresentacao);
				} else {
					contareceber.setVinculoProvisionado(documentotipo.getContadestino());
				}
				contareceber.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
				String idExterno = "";
				if(venda.getIdentificacaoexterna() != null && !venda.getIdentificacaoexterna().equals("")){
					idExterno =  " - Ident. Externo: " + venda.getIdentificacaoexterna();
					if(idExterno.length() > 80){
						idExterno = idExterno.substring(0, 80);
					}
				}
				
				contareceber.setDescricao("Comiss�o representante referente ao pedido de venda "+venda.getCdpedidovenda()+ (org.apache.commons.lang.StringUtils.isNotEmpty(venda.getIdentificador()) ? 
						" - " + venda.getIdentificador() : "") + ". - " + i + "/" + listaVendapagamentorepresentacao.size() +  idExterno);
				
				contareceber.setValor(vpr.getValororiginal());
				contareceber.setDtemissao(new java.sql.Date(venda.getDtpedidovenda().getTime()));
				if(contareceber.getValor().getValue().doubleValue() > 0){
					contareceber.setTipopagamento(Tipopagamento.FORNECEDOR);
					contareceber.setPessoa(aux_fornecedor);
					contareceber.setReferencia(Referencia.MES_ANO_CORRENTE);
					contareceber.setNumero((org.apache.commons.lang.StringUtils.isNotEmpty(venda.getIdentificador()) ? 
							venda.getIdentificador() : venda.getCdpedidovenda()) + "/" + numParcela);
					contareceber.setDocumentotipo(vpr.getDocumentotipo());
					if(vpr.getDataparcelaEmissaonota() != null){
						contareceber.setDtvencimento(vpr.getDataparcelaEmissaonota());
					}else {
						contareceber.setDtvencimento(vpr.getDataparcela());
					}
					contareceber.setEmpresa(empresa);
					contareceber.setEndereco(enderecoService.carregaEnderecoFornecedorForDocumento(aux_fornecedor));
					
					rateioParcelas = new Rateio();
					rateioParcelas.setListaRateioitem(rateio.getListaRateioitem());
					rateioService.atualizaValorRateio(rateioParcelas, contareceber.getValor());
					
					contareceber.setRateio(rateioParcelas);
					documentoService.saveOrUpdate(contareceber);
					listaDocumentosGerados.add(contareceber.getCddocumento());
					
					parcela = new Parcela();
					parcela.setDocumento(contareceber);
					parcela.setParcelamento(parcelamento);
					parcela.setOrdem(i);
					i++;
					parcelaService.saveOrUpdate(parcela);
					
					
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setPedidovenda(venda);
					documentoorigem.setDocumento(contareceber);
					documentoorigemService.saveOrUpdate(documentoorigem);	
					
					vpr.setDocumento(contareceber);
					
					if (empresa!=null && empresa.getCdpessoa()!=null && fornecedorService.isFornecedor(empresa.getCdpessoa()) &&
							empresaService.isEmpresa(aux_fornecedor.getCdpessoa())){
						if(fornecedorEmpresa == null)
							fornecedorEmpresa = empresaService.loadComContagerencialCentrocusto(new Empresa(aux_fornecedor.getCdpessoa()));
						
						Documento contapagarRepresentacao = new Documento();
						contapagarRepresentacao.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
						contapagarRepresentacao.setPessoa(empresa);
						contapagarRepresentacao.setTipopagamento(contareceber.getTipopagamento());
						contapagarRepresentacao.setValor(contareceber.getValor());
						contapagarRepresentacao.setEmpresa(new Empresa(aux_fornecedor.getCdpessoa()));
						contapagarRepresentacao.setDescricao(contareceber.getDescricao());
						contapagarRepresentacao.setDocumentoacao(contareceber.getDocumentoacao());
						contapagarRepresentacao.setDtemissao(contareceber.getDtemissao());
						contapagarRepresentacao.setDtvencimento(contareceber.getDtvencimento());
						contapagarRepresentacao.setDocumentotipo(contareceber.getDocumentotipo());
						
						if(empresarepresentacao != null){
							if(empresarepresentacao.getContagerencial() != null){
								contapagarRepresentacao.setContagerencialDespesaRepresentacao(empresarepresentacao.getContagerencial());
							}
							if(empresarepresentacao.getCentrocusto() != null){
								contapagarRepresentacao.setCentrocustoDespesaRepresentacao(empresarepresentacao.getCentrocusto());
							}
						}
						
						contapagarRepresentacao.setContareceberRepresentacao(contareceber);
						listaContapagarRepresentacao.add(contapagarRepresentacao);
					}else {
						documentohistoricoContareceber = documentohistoricoService.geraHistoricoDocumento(contareceber);
						documentohistoricoContareceber.setObservacao("Origem Venda: <a href=\"javascript:visualizaVenda("+ venda.getCdpedidovenda()+");\">"+venda.getCdpedidovenda() +"</a>.");
						documentohistoricoService.saveOrUpdate(documentohistoricoContareceber);
					}
				}
			} else if(vpr.getDocumento() != null && (naoGerarreceita == null || !naoGerarreceita )){
				if(venda.getDocumentoAposEmissaoNota() != null && venda.getDocumentoAposEmissaoNota() &&
						vpr.getDocumento() != null && vpr.getDocumento().getCddocumento() != null){
					if(vpr.getDataparcelaEmissaonota() != null){
						documentoService.updateDtvencimentoNFDocumentoVenda(vpr.getDocumento(), vpr.getDataparcelaEmissaonota());
					}
				}else {
					contareceber = vpr.getDocumento();
					
					parcela = new Parcela();
					parcela.setDocumento(contareceber);
					parcela.setParcelamento(parcelamento);
					parcela.setOrdem(i);
					i++;
					parcelaService.saveOrUpdate(parcela);
					
					documentohistoricoContareceber = documentohistoricoService.geraHistoricoDocumento(contareceber);
					documentohistoricoContareceber.setObservacao("Origem Venda: <a href=\"javascript:visualizaVenda("+ venda.getCdpedidovenda()+");\">"+venda.getCdpedidovenda() +"</a>.");
					documentohistoricoService.saveOrUpdate(documentohistoricoContareceber);
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setPedidovenda(venda);
					documentoorigem.setDocumento(contareceber);
					documentoorigemService.saveOrUpdate(documentoorigem);	
				}
			}
			
			vpr.setPedidovenda(venda);
			pedidovendapagamentorepresentacaoService.saveOrUpdate(vpr);
			numParcela++;
		}
			
		if(naoGerarreceita == null || !naoGerarreceita ){
			if(listaContapagarRepresentacao != null && !listaContapagarRepresentacao.isEmpty()){
				Rateio rateioParcelasContapagarRepresentacao;
				for(Documento documentoContapagar : listaContapagarRepresentacao){
					rateioParcelasContapagarRepresentacao = new Rateio();
					if(documentoContapagar.getContagerencialDespesaRepresentacao() != null){
						List<Rateioitem> listaRateioitem  = new ArrayList<Rateioitem>();
						listaRateioitem.add(new Rateioitem(documentoContapagar.getContagerencialDespesaRepresentacao(), 
								documentoContapagar.getCentrocustoDespesaRepresentacao(), null, documentoContapagar.getValor(), 100d));
						rateioParcelasContapagarRepresentacao.setListaRateioitem(listaRateioitem);
					}else {
						List<Rateioitem> listaRateioitem  = new ArrayList<Rateioitem>();
						listaRateioitem.add(new Rateioitem(fornecedorEmpresa.getContagerencial(), 
								fornecedorEmpresa.getCentrocusto(), null, documentoContapagar.getValor(), 100d));
						rateioParcelasContapagarRepresentacao.setListaRateioitem(listaRateioitem);
					}
					
					rateioService.atualizaValorRateio(rateioParcelasContapagarRepresentacao, documentoContapagar.getValor());	
					rateioService.limpaReferenciaRateio(rateioParcelasContapagarRepresentacao);
					rateioService.saveOrUpdate(rateioParcelasContapagarRepresentacao);
					
					documentoContapagar.setRateio(rateioParcelasContapagarRepresentacao);
					
					documentoService.saveOrUpdate(documentoContapagar);
					
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setPedidovenda(venda);
					documentoorigem.setDocumento(documentoContapagar);
					documentoorigemService.saveOrUpdate(documentoorigem);
					
					Documentohistorico documentohistoricoContapagar = documentohistoricoService.geraHistoricoDocumento(documentoContapagar);
					documentohistoricoContapagar.setObservacao("Origem Venda: <a href=\"javascript:visualizaVenda(" + venda.getCdpedidovenda() + ");\">" + venda.getCdpedidovenda() + "</a>.");
					if(documentoContapagar.getContareceberRepresentacao() != null){
						documentohistoricoContapagar.setObservacao(documentohistoricoContapagar.getObservacao() + 
								" (Conta a receber <a href=\"javascript:visualizaContareceber(" + 
								documentoContapagar.getContareceberRepresentacao().getCddocumento() + ");\">" + 
								documentoContapagar.getContareceberRepresentacao().getCddocumento() + "</a>.)");
						
						documentohistoricoContareceber = documentohistoricoService.geraHistoricoDocumento(documentoContapagar.getContareceberRepresentacao());
						documentohistoricoContareceber.setObservacao("Origem Venda: <a href=\"javascript:visualizaVenda("+ venda.getCdpedidovenda()+");\">"+venda.getCdpedidovenda() +"</a>.");
						documentohistoricoContareceber.setObservacao(documentohistoricoContareceber.getObservacao() + 
								" (Conta a pagar <a href=\"javascript:visualizaContapagar(" + 
								documentoContapagar.getCddocumento() + ");\">" + 
								documentoContapagar.getCddocumento() + "</a>.)");
						
						documentohistoricoService.saveOrUpdate(documentohistoricoContareceber);
					}
					documentohistoricoService.saveOrUpdate(documentohistoricoContapagar);
				}
			}
		}
		
		return listaDocumentosGerados;
	}
	
	public boolean existePedidoVendaRepresentacao(String whereIn) {
		return pedidovendaDAO.existePedidoVendaRepresentacao(whereIn);
	}
	
	/**
	* M�todo que valida se existe conta gerencial e centro de custo de despesa do fornecedor de representa��o
	*
	* @param request
	* @param pedidoVenda
	* @return
	* @since 23/03/2015
	* @author Luiz Fernando
	*/
	public boolean validaContaDespesaCentrocustoRepresentacao(WebRequestContext request, Pedidovenda pedidoVenda) {
		List<String> listError = new ArrayList<String>();
		if(pedidoVenda != null && pedidoVenda.getEmpresa() != null && SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendamaterial())){
			Empresa empresa = pedidoVenda.getEmpresa();
			Boolean isEmpresafornecedor = false;
			Boolean erroEepresentacao = false;
			if (empresa != null && empresa.getCdpessoa() != null){ 
				isEmpresafornecedor = fornecedorService.isFornecedor(empresa.getCdpessoa());
			}
			Fornecedor aux_fornecedor = null; 
			
			for (Pedidovendamaterial vendamaterial : pedidoVenda.getListaPedidovendamaterial()) {
				if(!erroEepresentacao && SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendapagamento()) && vendamaterial.getFornecedor() != null){
					if(aux_fornecedor == null){
						aux_fornecedor = fornecedorService.carregaFornecedor(vendamaterial.getFornecedor());
					}
					if (empresa != null && empresa.getCdpessoa() != null && aux_fornecedor != null && isEmpresafornecedor &&
							empresaService.isEmpresa(aux_fornecedor.getCdpessoa())){
						Empresarepresentacao empresarepresentacao = empresarepresentacaoService.getEmpresarepresentacao(empresa, aux_fornecedor);
						if(empresarepresentacao != null){
							if(empresarepresentacao.getContagerencial() == null){
								erroEepresentacao = true;
								listError.add("� preciso informar a conta de despesa do fornecedor de representa��o no cadastro da empresa.");
							}
							if(empresarepresentacao.getCentrocusto() == null){
								erroEepresentacao = true;
								listError.add("� preciso informar o centro de custo de despesa do fornecedor de representa��o no cadastro da empresa.");
							}
						}
					}
				}
			}
		}
		
		if(listError.size() > 0){
			for (String string : listError) {
				request.addError(string);
			}
			return false;
		}
		return true;
	}
	
	public boolean validaKitFlexivel(WebRequestContext request,	Pedidovenda pedidovenda) {
		boolean existeItemKitFlexivel = false;
		boolean existeKitFlexivel = false;
		if(pedidovenda != null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterialmestre())){
			for(Pedidovendamaterialmestre pvmm : pedidovenda.getListaPedidovendamaterialmestre()){
				if(pvmm.getMaterial() != null && pvmm.getMaterial().getKitflexivel() != null && pvmm.getMaterial().getKitflexivel() &&
						org.apache.commons.lang.StringUtils.isNotEmpty(pvmm.getIdentificadorinterno())){
					existeKitFlexivel = true; 
					existeItemKitFlexivel = false;
					if(SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
						for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
							if(org.apache.commons.lang.StringUtils.isNotEmpty(pvm.getIdentificadorinterno()) &&
									pvm.getIdentificadorinterno().equals(pvmm.getIdentificadorinterno())){
								existeItemKitFlexivel = true;
								break;
							}
						}
					}
					if(existeKitFlexivel && !existeItemKitFlexivel){
						request.addError("Existe material de Kit Flex�vel sem itens inclusos para sua composi��o");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public void atualizarQuantidadesViaWebservice(Pedidovenda pedidovenda, List<ConfirmarPedidovendaItemBean> listaItens) throws Exception {
		if(pedidovenda.getListaPedidovendamaterial() != null){
			List<Vendamaterial> lista = this.preencheListaVendaMaterial(pedidovenda, listaItens);
			if(lista != null){
				if(Boolean.TRUE.equals(pedidovenda.getPedidovendatipo().getAtualizaPedidoVendaWMS())){
					for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
						pvm.setQuantidade(0.);
						pvm.setQtdevolumeswms(0.);
						for(Vendamaterial vm : lista){
							if(vm.getPedidovendamaterial() != null && vm.getPedidovendamaterial().getCdpedidovendamaterial() != null
									&& vm.getPedidovendamaterial().getCdpedidovendamaterial().equals(pvm.getCdpedidovendamaterial())){
								pvm.setQuantidade(vm.getQuantidade() + pvm.getQuantidade());
								if(vm.getQtdevolumeswms() != null){
									pvm.setQtdevolumeswms(vm.getQtdevolumeswms() + pvm.getQtdevolumeswms());
								}
							}
						}
					}
					for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
						pedidovendamaterialService.updateQuantidadeWms(pvm);
						pedidovendamaterialService.updateQtdeVolumeWms(pvm);
					}
					recalcularESalvarImposto(pedidovenda);
					pedidovendahistoricoService.salvaPedidoHistoricoAtualizacaoWS(pedidovenda);
				}
				
				if(pedidovenda.getPedidovendatipo() != null && BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(pedidovenda.getPedidovendatipo().getBaixaestoqueEnum())){
					for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
						for(Vendamaterial vm : lista){
							if(vm.getPedidovendamaterial() != null && vm.getPedidovendamaterial().getCdpedidovendamaterial() != null
									&& vm.getPedidovendamaterial().getCdpedidovendamaterial().equals(pvm.getCdpedidovendamaterial())){
								geraMovimentacaoPedidoVenda(pedidovenda, pvm);
							}
						}
					}
				}
			}
			updateCarregamentoWms(pedidovenda);
		}
	}
	
	/**
	* M�todo que gera movimenta��o de estoque referente ao pedido de venda
	*
	* @param pedidovenda
	* @param pedidovendamaterial
	* @since 16/11/2016
	* @author Luiz Fernando
	*/
	public void geraMovimentacaoPedidoVenda(Pedidovenda pedidovenda, Pedidovendamaterial pedidovendamaterial) {
		Material material = pedidovendamaterial.getMaterial();
		
		Pedidovendamaterialmestre vendamaterialmestre = null;
		Double valormestre = null;
		Boolean registrarEntradaSaidaProducao = false;
		Boolean registrarSaidaKit = false;
		if(pedidovendamaterial.getMaterialmestre() != null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterialmestre())){
			for(Pedidovendamaterialmestre vmmestre : pedidovenda.getListaPedidovendamaterialmestre()){
				if(vmmestre.getMaterial() != null && pedidovendamaterial.getMaterialmestre().equals(vmmestre.getMaterial()) &&
						!materialService.isKit(vmmestre.getMaterial()) && 
						(vmmestre.getMaterial().getKitflexivel() == null || !vmmestre.getMaterial().getKitflexivel())){
					if(vmmestre.getEstoquerealizado() != null && vmmestre.getEstoquerealizado())
						return;
					
					if(SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
						for(Pedidovendamaterial vm : pedidovenda.getListaPedidovendamaterial()){
							if(vm.getPreco() != null && vm.getMaterialmestre() != null && vm.getMaterialmestre().equals(vmmestre.getMaterial())){
								if(valormestre == null) valormestre = 0.0;
								valormestre += vm.getPreco();
							}
						}
					}
					vmmestre.setEstoquerealizado(true);
					vendamaterialmestre = vmmestre;
					break;
				}
			}
		}
		
		Material materialProducaoKit = materialService.load(pedidovendamaterial.getMaterial(), "material.cdmaterial, material.producao, material.vendapromocional");
		if(materialProducaoKit != null && materialProducaoKit.getVendapromocional() != null && materialProducaoKit.getVendapromocional()){
			registrarSaidaKit = true;
		}else if(materialProducaoKit != null && materialProducaoKit.getProducao() != null && materialProducaoKit.getProducao()){
			//caso o material tenha qtde disponivel, efetuar a saida apenas do material de produ��o
			if(pedidovendamaterial.getSomentesaidaMaterialproducao() == null || !pedidovendamaterial.getSomentesaidaMaterialproducao()){
				//Se o produto tiver etapas de produ��o, retirar do estoque a quantidade do produto de produ��o,
				//Caso contr�rio, o sistema deve registrar a quantidade (campo Consumo em UC0379)  dos 
				//produtos de mat�ria-prima e registrar a entrada e sa�da do produto de produ��o.
				registrarEntradaSaidaProducao = !materialService.existProducaoetapa(pedidovendamaterial.getMaterial());
			}
		} 
		
		if(registrarEntradaSaidaProducao && producaoagendaService.existProducaoagendaByPedidovenda(pedidovenda)){
			registrarEntradaSaidaProducao = false;
		}
		
		if(registrarEntradaSaidaProducao){
			registrarEntradaSaidaProducao(pedidovenda, pedidovendamaterial);
		}else if(registrarSaidaKit){
			registrarSaidaKit(pedidovenda, pedidovendamaterial);
		}else {
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setPedidovenda(pedidovenda);
			Materialclasse materialclasse = new Materialclasse();
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
			movimentacaoestoque.setEmpresa(pedidovenda.getEmpresa());
			movimentacaoestoque.setProjeto(pedidovenda.getProjeto());
			movimentacaoestoque.setLoteestoque(pedidovendamaterial.getLoteestoque());
			
			if(vendamaterialmestre != null){
				movimentacaoestoque.setMaterial(vendamaterialmestre.getMaterial());
				movimentacaoestoque.setComprimento(vendamaterialmestre.getComprimento());
	
			}else {
				movimentacaoestoque.setMaterial(material);
				movimentacaoestoque.setComprimento(pedidovendamaterial.getComprimento());
			}
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			if(vendamaterialmestre != null){
				movimentacaoestoque.setQtde(vendamaterialmestre.getQtde());
				movimentacaoestoque.setValor(valormestre);
			}else {
				if(materialProducaoKit != null && materialProducaoKit.getProducao() != null && materialProducaoKit.getProducao()){
					movimentacaoestoque.setQtde(pedidovendamaterial.getQuantidade());
				}else {
					if(pedidovendamaterial.getFatorconversao() != null && pedidovendamaterial.getFatorconversao() > 0)
						movimentacaoestoque.setQtde(pedidovendamaterial.getQuantidade() / pedidovendamaterial.getFatorconversaoQtdereferencia());
					else
						movimentacaoestoque.setQtde(pedidovendamaterial.getQuantidade());
				}
				
				movimentacaoestoque.setValor(pedidovendamaterial.getPreco());
				if(pedidovendamaterial.getMultiplicador() != null && movimentacaoestoque.getValor() != null){
					movimentacaoestoque.setValor(movimentacaoestoque.getValor() * pedidovendamaterial.getMultiplicador());
				}
			}
			
			vendaService.verificaUnidademedidaMaterialDiferenteByVenda(movimentacaoestoque, pedidovendamaterial, pedidovendamaterial.getUnidademedida(), pedidovendamaterial.getQuantidade());
			
			if(pedidovendamaterial.getMaterial().getProduto()){
				materialclasse = Materialclasse.PRODUTO;	
			}else if(pedidovendamaterial.getMaterial().getServico()){
				materialclasse = Materialclasse.SERVICO;	
			}else if(pedidovendamaterial.getMaterial().getPatrimonio()){
				materialclasse = Materialclasse.PATRIMONIO;	
			}else {
				materialclasse = Materialclasse.EPI;	
			}
			
			if(pedidovenda.getLocalarmazenagem() != null){
				movimentacaoestoque.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
			}else if (pedidovenda.getEmpresa() != null) {
				List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(pedidovenda.getEmpresa());
				if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
					movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
			}
			movimentacaoestoque.setMaterialclasse(materialclasse);
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
			
			materialService.inutilizarMaterialSemEstoque(movimentacaoestoque.getMaterial(), 
															movimentacaoestoque.getEmpresa(),
															movimentacaoestoque.getLocalarmazenagem(),
															movimentacaoestoque.getLoteestoque());
		}
		
	}
	
	protected void registrarSaidaKit(Pedidovenda venda, Pedidovendamaterial vendamaterial) {
		if(vendamaterial.getMaterial() != null){
			Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendamaterial.getMaterial().getCdmaterial());
			venda.setMaterial(material);
			
			try{
				material.setProduto_altura(vendamaterial.getAltura());
				material.setProduto_largura(vendamaterial.getLargura());
				material.setQuantidade(vendamaterial.getQuantidade());
				materialrelacionadoService.getValorvendakit(material);			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				if(materialrelacionado.getQuantidade() != null && materialrelacionado.getQuantidade() > 0 && vendamaterial.getQuantidade() != null){
					Double qtde = vendamaterial.getQuantidade();
					if(vendamaterial.getUnidademedida() != null && !vendamaterial.getUnidademedida().equals(material.getUnidademedida()) && 
							vendamaterial.getFatorconversao() != null && vendamaterial.getQtdereferencia() != null &&
							vendamaterial.getFatorconversaoQtdereferencia() != 0){
						qtde = qtde / vendamaterial.getFatorconversaoQtdereferencia();
					}
					materialrelacionado.setQuantidade(materialrelacionado.getQuantidade() * qtde);
				}
			}
			
			if(material.getVendapromocional() != null && material.getVendapromocional()){
				Set<Materialrelacionado> listaMaterialrelacionado = material.getListaMaterialrelacionado();
				if(listaMaterialrelacionado != null && listaMaterialrelacionado.size() > 0){
					for (Materialrelacionado materialrelacionado : listaMaterialrelacionado) {
						if(materialrelacionado.getQuantidade() != null && materialrelacionado.getQuantidade() > 0 && materialrelacionado.getMaterialpromocao() != null && 
								(materialrelacionado.getMaterialpromocao().getServico() == null || !materialrelacionado.getMaterialpromocao().getServico())){
							Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
							movimentacaoestoqueorigem.setPedidovenda(venda);
							Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
							movimentacaoestoque.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
							movimentacaoestoque.setEmpresa(venda.getEmpresa());
							movimentacaoestoque.setProjeto(venda.getProjeto());
							movimentacaoestoque.setMaterial(materialrelacionado.getMaterialpromocao());
							movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
							movimentacaoestoque.setQtde(materialrelacionado.getQuantidade());
							if(materialrelacionado.getValorvenda() != null)
								movimentacaoestoque.setValor(materialrelacionado.getValorvenda().getValue().doubleValue());
							if(venda.getLocalarmazenagem() != null){
								movimentacaoestoque.setLocalarmazenagem(venda.getLocalarmazenagem());
							}else if (venda.getEmpresa() != null) {
								List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(venda.getEmpresa());
								if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
									movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
							}
							movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
							movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
							movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
						}
					}
				}
			}
		}
	}
	
	protected void registrarEntradaSaidaProducao(Pedidovenda venda, Pedidovendamaterial vendamaterial) {
		if(vendamaterial.getMaterial() != null){
			Material material = materialService.loadMaterialComMaterialproducao(vendamaterial.getMaterial());
			venda.setMaterial(material);
			
			try{
				material.setProduto_altura(vendamaterial.getAltura());
				material.setProduto_largura(vendamaterial.getLargura());
				material.setQuantidade(vendamaterial.getQuantidade());
				materialproducaoService.getValorvendaproducao(material);			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			for (Materialproducao materialproducao : material.getListaProducao()) {
				if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && vendamaterial.getQuantidade() != null){
					materialproducao.setConsumo(materialproducao.getConsumo() * vendamaterial.getQuantidade());
				}
			}
			
			if(material.getProducao() != null && material.getProducao()){
				Set<Materialproducao> listaProducao = material.getListaProducao();
				if(listaProducao != null && listaProducao.size() > 0){
					for (Materialproducao materialproducao : listaProducao) {
						if(materialproducao.getConsumo() != null && materialproducao.getConsumo() > 0 && materialproducao.getMaterial() != null && 
								(materialproducao.getMaterial().getServico() == null || !materialproducao.getMaterial().getServico())){
							Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
							movimentacaoestoqueorigem.setPedidovenda(venda);
							Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
							movimentacaoestoque.setDtmovimentacao(new java.sql.Date(venda.getDtpedidovenda().getTime()));
							movimentacaoestoque.setEmpresa(venda.getEmpresa());
							movimentacaoestoque.setProjeto(venda.getProjeto());
							movimentacaoestoque.setMaterial(materialproducao.getMaterial());
							movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
							movimentacaoestoque.setQtde(materialproducao.getConsumo());
							movimentacaoestoque.setValor(materialproducao.getValorconsumo());
							if(venda.getLocalarmazenagem() != null){
								movimentacaoestoque.setLocalarmazenagem(venda.getLocalarmazenagem());
							}else if (venda.getEmpresa() != null) {
								List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(venda.getEmpresa());
								if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
									movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
							}
							movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
							movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
							movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
						}
					}
					
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setPedidovenda(venda);
					Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
					movimentacaoestoque.setDtmovimentacao(new java.sql.Date(System.currentTimeMillis()));
					movimentacaoestoque.setEmpresa(venda.getEmpresa());
					movimentacaoestoque.setProjeto(venda.getProjeto());
					movimentacaoestoque.setMaterial(vendamaterial.getMaterial());
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
					
					if(material.getProducao() != null && material.getProducao()){
						movimentacaoestoque.setQtde(vendamaterial.getQuantidade());
					}else {
						if(venda.getFatorconversao() != null && venda.getFatorconversao() > 0)
							movimentacaoestoque.setQtde(vendamaterial.getQuantidade() / venda.getFatorconversao());
						else if(vendamaterial.getFatorconversao() != null && vendamaterial.getFatorconversao() > 0)
							movimentacaoestoque.setQtde(vendamaterial.getQuantidade() / vendamaterial.getFatorconversaoQtdereferencia());
						else
							movimentacaoestoque.setQtde(vendamaterial.getQuantidade());
					}
					
					movimentacaoestoque.setValor(vendamaterial.getPreco());
					if(vendamaterial.getMultiplicador() != null && movimentacaoestoque.getValor() != null){
						movimentacaoestoque.setValor(movimentacaoestoque.getValor() * vendamaterial.getMultiplicador());
					}
				
					vendaService.verificaUnidademedidaMaterialDiferenteByVenda(movimentacaoestoque, vendamaterial, vendamaterial.getUnidademedida(), vendamaterial.getQuantidade());
				
					if(venda.getLocalarmazenagem() != null){
						movimentacaoestoque.setLocalarmazenagem(venda.getLocalarmazenagem());
					}else if (venda.getEmpresa() != null) {
						List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(venda.getEmpresa());
						if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
							movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
					}
					movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
					
					movimentacaoestoqueorigem.setCdmovimentacaoestoqueorigem(null);
					movimentacaoestoque.setCdmovimentacaoestoque(null);
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
				}
			}
		}
	}
	
	/**
	 * M�todo que atualiza a refer�ncia ao carregamento no pedidovenda para permitir ligar a futura venda ao carregamento separado no wms.
	 * 
	 * @param pedidovenda
	 * @throws Exception
	 * @author Rafael Salvio
	 * @throws Exception 
	 */
	public void updateCarregamentoWms(Pedidovenda pedidovenda) throws Exception{
		pedidovendaDAO.updateCarregamentoWms(pedidovenda);
	}
	
	/**
	* M�todo que seta o custo operacional nos itens do pedido de venda
	*
	* @param venda
	* @since 26/05/2015
	* @author Luiz Fernando
	*/
	public void setCustooperacional(Pedidovenda venda){
		if(venda != null && SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			StringBuilder whereInMaterial = new StringBuilder();
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null){
					whereInMaterial.append(vendamaterial.getMaterial().getCdmaterial()).append(",");
				}
			}
			if(!whereInMaterial.toString().equals("")){
				List<Material> listaMaterial = materialService.findForCustoOperacional(whereInMaterial.substring(0, whereInMaterial.length()-1));
				if(SinedUtil.isListNotEmpty(listaMaterial)){
					Double custooperacionaladministrativo = tabelavalorService.getValor(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO);
					for(Material material : listaMaterial){
						if(material.getProducao() != null && material.getProducao()){
							for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
								if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().equals(material)){
									Double custooperacionalitem = custooperacionaladministrativo;
									if(material.getMaterialgrupo() != null && 
										material.getMaterialgrupo().getRateiocustoproducao() != null){
										if(custooperacionaladministrativo != null){
											custooperacionalitem = custooperacionaladministrativo * material.getMaterialgrupo().getRateiocustoproducao();
										}else {
											custooperacionalitem = material.getMaterialgrupo().getRateiocustoproducao();
										}
									}
									vendamaterial.setCustooperacional(custooperacionalitem);
								}
							}
						}
					}
				}
			}
		}
	}
	
	class ContadorArquivoAndroid {
		
		int contador = 0;
		
		protected void increment(){
			contador++;
		}
		
		@Override
		public String toString() {
			return contador + "";
		}
		
	}
	
	public void gerarArquivosAndroid(String nomeBanco){
			removerArquivosAndroid(nomeBanco);
			
			ContadorArquivoAndroid contador = new ContadorArquivoAndroid();
			//os arquivos devem ser gerados na ordem de acordo com a dependencia
			
			exportarParametrogeral(contador, nomeBanco);
			exportarEnderecotipo(contador, nomeBanco);
			exportarMaterialgrupo(contador, nomeBanco);
			exportarUnidademedida(contador, nomeBanco);
			exportarUf(contador, nomeBanco);
			exportarMunicipio(contador, nomeBanco);
			exportarContatotipo(contador, nomeBanco);
			exportarColaborador(contador, nomeBanco);
			exportarFornecedor(contador, nomeBanco);
			exportarEmpresa(contador, nomeBanco);
			exportarLocalarmazenagem(contador, nomeBanco);
			exportarLocalarmazenagemempresa(contador, nomeBanco);
			exportarCategoria(contador, nomeBanco);
			exportarCliente(contador, nomeBanco);
			exportarEndereco(contador, nomeBanco);
			exportarContato(contador, nomeBanco);
			exportarClientevendedor(contador, nomeBanco);
			exportarClientecategoria(contador, nomeBanco);
			exportarDocumentotipo(contador, nomeBanco);
			exportarClientedocumentotipo(contador, nomeBanco);
			exportarAtividadetipo(contador, nomeBanco);
			exportarMeiocontato(contador, nomeBanco);
			exportarSituacaohistorico(contador, nomeBanco);
			exportarClientehistorico(contador, nomeBanco);
			
			exportarResponsavelFrete(contador, nomeBanco);
			exportarBanco(contador, nomeBanco);
			exportarContatipo(contador, nomeBanco);
			exportarConta(contador, nomeBanco);
			exportarContaempresa(contador, nomeBanco);
			exportarFormapagamento(contador, nomeBanco);
			exportarIndicecorrecao(contador, nomeBanco);
			
			exportarProjeto(contador, nomeBanco);
			exportarPedidovendatipo(contador, nomeBanco);
			exportarPrazopagamento(contador, nomeBanco);
			exportarPrazopagamentoitem(contador, nomeBanco);
			exportarClienteprazopagamento(contador, nomeBanco);
			
			exportarMaterial(contador, nomeBanco);
			exportarMaterialproducao(contador, nomeBanco);
			exportarMaterialrelacionado(contador, nomeBanco);
			exportarMaterialsimilar(contador, nomeBanco);
			exportarMaterialunidademedida(contador, nomeBanco);
			exportarMaterialformulavalorvenda(contador, nomeBanco);
			
			exportarMaterialtabelapreco(contador, nomeBanco);
			exportarMaterialtabelaprecoitem(contador, nomeBanco);
			exportarMaterialtabelaprecocliente(contador, nomeBanco);
			exportarMaterialtabelaprecoempresa(contador, nomeBanco);
			exportarMaterialempresa(contador, nomeBanco);
			
			exportarPneumarca(contador, nomeBanco);
			exportarPneumedida(contador, nomeBanco);
			exportarPneumodelo(contador, nomeBanco);
			exportarPneu(contador, nomeBanco);
			
			exportarTabelavalor(contador, nomeBanco);
			exportarDocumento(contador, nomeBanco);
//			exportarDocumentohistorico(contador, nomeBanco);
			
			exportarValecompra(contador, nomeBanco);
			exportarVwultimascomprasandroid(contador, nomeBanco);
			
			
	}
	
	protected void removerArquivosAndroid(String nomeBanco) {
		File[] files = new File(AndroidUtil.getDirAndroidJSON(nomeBanco)).listFiles(); 
		if(files != null && files.length > 0){
			for(File file : files) { 
				file.delete(); 
			}
		}
	}
	
	protected void prepareAndSaveToFile(ContadorArquivoAndroid contador,List<?> list, String filename,String nomeBanco){
		int pagina = 1;
		final Integer REGISTRO_POR_ARQUIVO = 200;
		for (int i = 0; i < list.size(); i += REGISTRO_POR_ARQUIVO){
			int fim = (i + REGISTRO_POR_ARQUIVO > list.size()) ? list.size() : i + REGISTRO_POR_ARQUIVO;
			saveToFile(View.convertToJson(list.subList(i, fim)), (contador + "_" + filename + "_" + pagina),nomeBanco);
			pagina++;
			contador.increment();
		}
	}
	
	protected void exportarParametrogeral(ContadorArquivoAndroid contador, String nomeBanco) {
		ParametrogeralService parametrogeralService = Neo.getObject(ParametrogeralService.class);
		prepareAndSaveToFile(contador, parametrogeralService.findForAndroid(), "parametrogeral", nomeBanco);
	}
	
	protected void exportarCliente(ContadorArquivoAndroid contador, String nomeBanco) {
		ClienteService clienteService = Neo.getObject(ClienteService.class);
		prepareAndSaveToFile(contador, clienteService.findForAndroid(), "cliente", nomeBanco);
	}
	
	protected void exportarEnderecotipo(ContadorArquivoAndroid contador, String nomeBanco){
		EnderecotipoService enderecotipoService = Neo.getObject(EnderecotipoService.class);
		prepareAndSaveToFile(contador, enderecotipoService.findForAndroid(), "enderecotipo", nomeBanco);
	}
	
	protected void exportarMaterialgrupo(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialgrupoService materialgrupoService = Neo.getObject(MaterialgrupoService.class);
		prepareAndSaveToFile(contador, materialgrupoService.findForAndroid(), "materialgrupo", nomeBanco);
	}
	
	protected void exportarUnidademedida(ContadorArquivoAndroid contador, String nomeBanco) {
		UnidademedidaService unidademedidaService = Neo.getObject(UnidademedidaService.class);
		prepareAndSaveToFile(contador, unidademedidaService.findForAndroid(), "unidademedida", nomeBanco);
	}
	
	protected void exportarEmpresa(ContadorArquivoAndroid contador, String nomeBanco) {
		EmpresaService empresaService = Neo.getObject(EmpresaService.class);
		prepareAndSaveToFile(contador, empresaService.findForAndroid(), "empresa", nomeBanco);
	}
	
//	protected void exportarUsuarioempresa() {
//		UsuarioempresaService usuarioempresaService = Neo.getObject(UsuarioempresaService.class);
//		prepareAndSaveToFile(contador, usuarioempresaService.findForAndroid(), "usuarioempresa", nomeBanco);
//	}
//	
//	protected void exportarUsuario() {
//		UsuarioService usuarioService = Neo.getObject(UsuarioService.class);
//		prepareAndSaveToFile(contador, usuarioService.findForAndroid(), "usuario", nomeBanco);
//	}
	
	protected void exportarMaterial(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialService materialService = Neo.getObject(MaterialService.class);
		prepareAndSaveToFile(contador, materialService.findForAndroid(), "material", nomeBanco);
	}
	
	protected void exportarEndereco(ContadorArquivoAndroid contador, String nomeBanco) {
		EnderecoService enderecoService = Neo.getObject(EnderecoService.class);
		prepareAndSaveToFile(contador, enderecoService.findForAndroid(), "endereco", nomeBanco);
	}
	
	protected void exportarMaterialempresa(ContadorArquivoAndroid contador,String nomeBanco) {
		MaterialempresaService materialEmpresaService = Neo.getObject(MaterialempresaService.class);
		prepareAndSaveToFile(contador, materialEmpresaService.findForAndroid(), "materialempresa", nomeBanco);
	}
	
	protected void exportarMunicipio(ContadorArquivoAndroid contador, String nomeBanco) {
		MunicipioService municipioService = Neo.getObject(MunicipioService.class);
		prepareAndSaveToFile(contador, municipioService.findForAndroid(), "municipio", nomeBanco);
	}
	
	protected void exportarUf(ContadorArquivoAndroid contador, String nomeBanco) {
		UfService ufService = Neo.getObject(UfService.class);
		prepareAndSaveToFile(contador, ufService.findForAndroid(), "uf", nomeBanco);
	}
	
	protected void exportarContatipo(ContadorArquivoAndroid contador, String nomeBanco) {
		ContatipoService contatipoService = Neo.getObject(ContatipoService.class);
		prepareAndSaveToFile(contador, contatipoService.findForAndroid(), "contatipo", nomeBanco);
	}
	
	protected void exportarConta(ContadorArquivoAndroid contador, String nomeBanco) {
		ContaService contaService = Neo.getObject(ContaService.class);
		prepareAndSaveToFile(contador, contaService.findForAndroid(), "conta", nomeBanco);
	}
	
	protected void exportarContaempresa(ContadorArquivoAndroid contador, String nomeBanco) {
		ContaempresaService contaempresaService = Neo.getObject(ContaempresaService.class);
		prepareAndSaveToFile(contador, contaempresaService.findForAndroid(), "contaempresa", nomeBanco);
	}
	
	protected void exportarDocumentotipo(ContadorArquivoAndroid contador, String nomeBanco) {
		DocumentotipoService documentotipoService = Neo.getObject(DocumentotipoService.class);
		prepareAndSaveToFile(contador, documentotipoService.findForAndroid(), "documentotipo", nomeBanco);
	}
	
	protected void exportarFormapagamento(ContadorArquivoAndroid contador, String nomeBanco) {
		FormapagamentoService unidademedidaService = Neo.getObject(FormapagamentoService.class);
		prepareAndSaveToFile(contador, unidademedidaService.findForAndroid(), "formapagamento", nomeBanco);
	}
	
	protected void exportarIndicecorrecao(ContadorArquivoAndroid contador, String nomeBanco) {
		IndicecorrecaoService empresaService = Neo.getObject(IndicecorrecaoService.class);
		prepareAndSaveToFile(contador, empresaService.findForAndroid(), "indicecorrecao", nomeBanco);
	}
	
	protected void exportarMaterialproducao(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialproducaoService materialproducaoService = Neo.getObject(MaterialproducaoService.class);
		prepareAndSaveToFile(contador, materialproducaoService.findForAndroid(), "materialproducao", nomeBanco);
	}
	
	protected void exportarMaterialrelacionado(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialrelacionadoService materialService = Neo.getObject(MaterialrelacionadoService.class);
		prepareAndSaveToFile(contador, materialService.findForAndroid(), "materialrelacionado", nomeBanco);
	}
	
	protected void exportarMaterialtabelapreco(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialtabelaprecoService materialtabelaprecoService = Neo.getObject(MaterialtabelaprecoService.class);
		prepareAndSaveToFile(contador, materialtabelaprecoService.findForAndroid(), "materialtabelapreco", nomeBanco);
	}
	
	protected void exportarMaterialtabelaprecoitem(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialtabelaprecoitemService materialtabelaprecoitemService = Neo.getObject(MaterialtabelaprecoitemService.class);
		prepareAndSaveToFile(contador, materialtabelaprecoitemService.findForAndroid(), "materialtabelaprecoitem", nomeBanco);
	}
	
	protected void exportarVwultimascomprasandroid(ContadorArquivoAndroid contador, String nomeBanco) {
		VwultimascomprasandroidService vwultimascomprasandroidService = Neo.getObject(VwultimascomprasandroidService.class);
		prepareAndSaveToFile(contador, vwultimascomprasandroidService.findForAndroid(), "vwultimascomprasandroid", nomeBanco);
	}
	
	protected void exportarMaterialunidademedida(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialunidademedidaService materialunidademedidaService = Neo.getObject(MaterialunidademedidaService.class);
		prepareAndSaveToFile(contador, materialunidademedidaService.findForAndroid(), "materialunidademedida", nomeBanco);
	}
	
	protected void exportarMaterialformulavalorvenda(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialformulavalorvendaService materialformulavalorvendaService = Neo.getObject(MaterialformulavalorvendaService.class);
		prepareAndSaveToFile(contador, materialformulavalorvendaService.findForAndroid(), "materialformulavalorvenda", nomeBanco);
	}
	
	protected void exportarTabelavalor(ContadorArquivoAndroid contador, String nomeBanco) {
		TabelavalorService tabelavalorService = Neo.getObject(TabelavalorService.class);
		prepareAndSaveToFile(contador, tabelavalorService.findForAndroid(), "tabelatabelavalor", nomeBanco);
	}
	
	protected void exportarPedidovendatipo(ContadorArquivoAndroid contador, String nomeBanco) {
		PedidovendatipoService pedidovendatipoService = Neo.getObject(PedidovendatipoService.class);
		prepareAndSaveToFile(contador, pedidovendatipoService.findForAndroid(), "pedidovendatipo", nomeBanco);
	}
	
	
	protected void exportarPrazopagamento(ContadorArquivoAndroid contador, String nomeBanco) {
		PrazopagamentoService prazopagamentoService = Neo.getObject(PrazopagamentoService.class);
		prepareAndSaveToFile(contador, prazopagamentoService.findForAndroid(), "prazopagamento", nomeBanco);
	}
	
	protected void exportarPrazopagamentoitem(ContadorArquivoAndroid contador, String nomeBanco) {
		PrazopagamentoitemService prazopagamentoitemService = Neo.getObject(PrazopagamentoitemService.class);
		prepareAndSaveToFile(contador, prazopagamentoitemService.findForAndroid(), "prazopagamentoitem", nomeBanco);
	}
	
	protected void exportarProjeto(ContadorArquivoAndroid contador, String nomeBanco) {
		ProjetoService projetoService = Neo.getObject(ProjetoService.class);
		prepareAndSaveToFile(contador, projetoService.findForAndroid(), "projeto", nomeBanco);
	}
	
	
	protected void exportarBanco(ContadorArquivoAndroid contador, String nomeBanco) {
		BancoService bancoService = Neo.getObject(BancoService.class);
		prepareAndSaveToFile(contador, bancoService.findForAndroid(), "banco", nomeBanco);
	}
	
	protected void exportarCategoria(ContadorArquivoAndroid contador, String nomeBanco) {
		CategoriaService categoriaService = Neo.getObject(CategoriaService.class);
		prepareAndSaveToFile(contador, categoriaService.findForAndroid(), "categoria", nomeBanco);
	}
	
	protected void exportarColaborador(ContadorArquivoAndroid contador, String nomeBanco) {
		ColaboradorService colaboradorService = Neo.getObject(ColaboradorService.class);
		prepareAndSaveToFile(contador, colaboradorService.findForAndroid(), "colaborador", nomeBanco);
	}
	
	protected void exportarContato(ContadorArquivoAndroid contador, String nomeBanco) {
		ContatoService contatoService = Neo.getObject(ContatoService.class);
		prepareAndSaveToFile(contador, contatoService.findForAndroid(), "contato", nomeBanco);
	}

	protected void exportarContatotipo(ContadorArquivoAndroid contador, String nomeBanco) {
		ContatotipoService contatotipoService = Neo.getObject(ContatotipoService.class);
		prepareAndSaveToFile(contador, contatotipoService.findForAndroid(), "contatotipo", nomeBanco);
	}
	
	protected void exportarFornecedor(ContadorArquivoAndroid contador, String nomeBanco) {
		FornecedorService fornecedorService = Neo.getObject(FornecedorService.class);
		prepareAndSaveToFile(contador, fornecedorService.findForAndroid(), "fornecedor", nomeBanco);
	}

	protected void exportarMaterialsimilar(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialsimilarService projetoService = Neo.getObject(MaterialsimilarService.class);
		prepareAndSaveToFile(contador, projetoService.findForAndroid(), "materialsimilar", nomeBanco);
	}
	
	protected void exportarMaterialtabelaprecocliente(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialtabelaprecoclienteService materialtabelaprecoclienteService = Neo.getObject(MaterialtabelaprecoclienteService.class);
		prepareAndSaveToFile(contador, materialtabelaprecoclienteService.findForAndroid(), "materialtabelaprecocliente", nomeBanco);
	}

	protected void exportarMaterialtabelaprecoempresa(ContadorArquivoAndroid contador, String nomeBanco) {
		MaterialtabelaprecoempresaService materialtabelaprecoempresaService = Neo.getObject(MaterialtabelaprecoempresaService.class);
		prepareAndSaveToFile(contador, materialtabelaprecoempresaService.findForAndroid(), "materialtabelaprecoempresa", nomeBanco);
	}

	protected void exportarResponsavelFrete(ContadorArquivoAndroid contador, String nomeBanco) {
		ResponsavelFreteService responsavelFreteService = Neo.getObject(ResponsavelFreteService.class);
		prepareAndSaveToFile(contador, responsavelFreteService.findForAndroid(), "responsavelfrete", nomeBanco);
	}
	
	protected void exportarValecompra(ContadorArquivoAndroid contador, String nomeBanco) {
		ValecompraService valecompraService = Neo.getObject(ValecompraService.class);
		prepareAndSaveToFile(contador, valecompraService.findForAndroid(), "valecompra", nomeBanco);
	}
	
	protected void exportarClientevendedor(ContadorArquivoAndroid contador, String nomeBanco) {
		ClientevendedorService clientevendedorService = Neo.getObject(ClientevendedorService.class);
		prepareAndSaveToFile(contador, clientevendedorService.findForAndroid(), "clientevendedor", nomeBanco);
	}
	
	protected void exportarClientecategoria(ContadorArquivoAndroid contador, String nomeBanco) {
		PessoacategoriaService pessoacategoriaService = Neo.getObject(PessoacategoriaService.class);
		prepareAndSaveToFile(contador, pessoacategoriaService.findClienteForAndroid(), "clientecategoria", nomeBanco);
	}

	protected void exportarAtividadetipo(ContadorArquivoAndroid contador, String nomeBanco) {
		AtividadetipoService atividadetipoService = Neo.getObject(AtividadetipoService.class);
		prepareAndSaveToFile(contador, atividadetipoService.findForAndroid(), "atividadetipo", nomeBanco);
	}
	
	protected void exportarClientedocumentotipo(ContadorArquivoAndroid contador, String nomeBanco) {
		ClienteDocumentoTipoService clienteDocumentoTipoService = Neo.getObject(ClienteDocumentoTipoService.class);
		prepareAndSaveToFile(contador, clienteDocumentoTipoService.findForAndroid(), "clientedocumentotipo", nomeBanco);
	}
	
	protected void exportarClientehistorico(ContadorArquivoAndroid contador, String nomeBanco) {
		ClientehistoricoService clientehistoricoService = Neo.getObject(ClientehistoricoService.class);
		prepareAndSaveToFile(contador, clientehistoricoService.findForAndroid(), "clientehistorico", nomeBanco);
	}
	
	protected void exportarClienteprazopagamento(ContadorArquivoAndroid contador, String nomeBanco) {
		ClientePrazoPagamentoService clienteprazopagamentoService = Neo.getObject(ClientePrazoPagamentoService.class);
		prepareAndSaveToFile(contador, clienteprazopagamentoService.findForAndroid(), "clienteprazopagamento", nomeBanco);
	}
	
	protected void exportarDocumento(ContadorArquivoAndroid contador, String nomeBanco) {
		DocumentoService documentoService = Neo.getObject(DocumentoService.class);
		prepareAndSaveToFile(contador, documentoService.findForAndroid(), "documento", nomeBanco);
	}
	
//	protected void exportarDocumentohistorico(ContadorArquivoAndroid contador, String nomeBanco) {
//		DocumentohistoricoService documentohistoricoService = Neo.getObject(DocumentohistoricoService.class);
//		prepareAndSaveToFile(contador, documentohistoricoService.findForAndroid(), "documentohistorico", nomeBanco);
//	}
	
	protected void exportarLocalarmazenagem(ContadorArquivoAndroid contador, String nomeBanco) {
		LocalarmazenagemService localarmazenagemService = Neo.getObject(LocalarmazenagemService.class);
		prepareAndSaveToFile(contador, localarmazenagemService.findForAndroid(), "localarmazenagem", nomeBanco);
	}
	
	protected void exportarLocalarmazenagemempresa(ContadorArquivoAndroid contador, String nomeBanco) {
		LocalarmazenagemempresaService localarmazenagemempresaService = Neo.getObject(LocalarmazenagemempresaService.class);
		prepareAndSaveToFile(contador, localarmazenagemempresaService.findForAndroid(), "localarmazenagemempresa", nomeBanco);
	}
	
	protected void exportarMeiocontato(ContadorArquivoAndroid contador, String nomeBanco) {
		MeiocontatoService meiocontatoService = Neo.getObject(MeiocontatoService.class);
		prepareAndSaveToFile(contador, meiocontatoService.findForAndroid(), "meiocontato", nomeBanco);
	}
	
	protected void exportarPneu(ContadorArquivoAndroid contador, String nomeBanco) {
		PneuService pneuService = Neo.getObject(PneuService.class);
		prepareAndSaveToFile(contador, pneuService.findForAndroid(), "pneu", nomeBanco);
	}
	
	protected void exportarPneumarca(ContadorArquivoAndroid contador, String nomeBanco) {
		PneumarcaService pneumarcaService = Neo.getObject(PneumarcaService.class);
		prepareAndSaveToFile(contador, pneumarcaService.findForAndroid(), "pneumarca", nomeBanco);
	}
	
	protected void exportarPneumedida(ContadorArquivoAndroid contador, String nomeBanco) {
		PneumedidaService pneumedidaService = Neo.getObject(PneumedidaService.class);
		prepareAndSaveToFile(contador, pneumedidaService.findForAndroid(), "pneumedida", nomeBanco);
	}
	
	protected void exportarPneumodelo(ContadorArquivoAndroid contador, String nomeBanco) {
		PneumodeloService pneumodeloService = Neo.getObject(PneumodeloService.class);
		prepareAndSaveToFile(contador, pneumodeloService.findForAndroid(), "pneumodelo", nomeBanco);
	}
	
	protected void exportarSituacaohistorico(ContadorArquivoAndroid contador, String nomeBanco) {
		SituacaohistoricoService situacaohistoricoService = Neo.getObject(SituacaohistoricoService.class);
		prepareAndSaveToFile(contador, situacaohistoricoService.findForAndroid(), "situacaohistorico", nomeBanco);
	}
	
	/**
	* M�todo que salva os arquivos jsons na pasta de arquivos android
	*
	* @param json
	* @param filename
	* @since 30/09/2014
	* @author Luiz Fernando
	*/
	protected void saveToFile(JSON json, String filename, String nomeBanco) {
		try {
			final String SEPARATOR = System.getProperty("file.separator");
			String path = AndroidUtil.getDirAndroidJSON(nomeBanco);
			
			File fileNew = new File(path + SEPARATOR + filename + "_temp.json");
			String jsonStr = json.toString();
			FileOutputStream fileOS; 
			
			if(!fileNew.exists()){
				fileNew.getParentFile().mkdirs();
				fileNew.createNewFile();
			}
			fileOS = FileUtils.openOutputStream(fileNew);
			FileChannel outChannel = fileOS.getChannel();
			ByteBuffer buf = ByteBuffer.allocate(jsonStr.getBytes("iso-8859-1").length);
			
			byte[] bytes = jsonStr.getBytes("iso-8859-1");
			buf.put(bytes);
			buf.flip();
	
			outChannel.write(buf);
			
			fileOS.close();
			
			File fileOld = new File(path + SEPARATOR + filename + ".json");
			fileOld.delete();
			
			fileNew.renameTo(fileOld);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#findForEntradafiscal(String whereInPedidovenda)
	*
	* @param whereInPedidovenda
	* @return
	* @since 28/05/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovenda> findForEntradafiscal(String whereInPedidovenda) {
		return pedidovendaDAO.findForEntradafiscal(whereInPedidovenda);
	}
	
	/**
	* M�todo que retorna a empresa de acordo com os pedidos de venda. Caso exista mais de uma empresa, ir� retornar nulo.
	*
	* @param lista
	* @return
	* @since 28/05/2015
	* @author Luiz Fernando
	*/
	public Empresa getEmpresa(List<Pedidovenda> lista){
		Empresa empresa = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Pedidovenda bean : lista){
				if(bean.getEmpresa() != null){
					if(empresa == null){
						empresa = bean.getEmpresa();
					}else if(!bean.getEmpresa().equals(empresa)){
						empresa = null;
						break;
					}
				}
			}
		}
		return empresa;
	}
	
	/**
	 * @param material
	 * @return
	 * @since 13/07/2015
	 * @author Andrey Leonardo
	 */
	public Date getMinDataPedidovendaReserva(Material material){
		return pedidovendaDAO.getMinDataPedidovendaReserva(material);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2015
	 */
	public List<Pedidovenda> findByPneu(Pneu pneu) {
		return pedidovendaDAO.findByPneu(pneu);
	}
	
	/**
	 * Verifica o status do pagamento atrav�s do identificador do pedido
	 *
	 * @param empresa_cnpj
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/09/2015
	 */
	public String verificaStatusPagamentoWebservice(String empresa_cnpj, String identificador) {
		List<Pedidovenda> listaPedidovenda = this.findByEmpresaIdentificador(empresa_cnpj, identificador);
		
		if(listaPedidovenda == null || listaPedidovenda.size() == 0){
			throw new SinedException("Nenhum pedido de venda encontrada com o identificador informado.");
		} else if(listaPedidovenda.size() > 1){
			throw new SinedException("Foi encontrada mais de um pedido de venda com o identificador informado.");
		}
		
		List<Documento> listaDocumento = contareceberService.findByPedidovenda(CollectionsUtil.listAndConcatenate(listaPedidovenda, "cdpedidovenda", ","));
		if(listaDocumento == null || listaDocumento.size() == 0){
			throw new SinedException("Nenhum documento encontrado para a venda.");
		}
		
		String statusdocumento = "0";
		for (Documento documento : listaDocumento) {
			if(documento.getDocumentoacao().equals(Documentoacao.BAIXADA)){
				statusdocumento = "1";
			} else if(documento.getDocumentoacao().equals(Documentoacao.CANCELADA)) {
				statusdocumento = "2";
			} else if(documento.getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
				String cache = CacheWebserviceUtil.getConsultaPagamento(documento.getCddocumento());
				if(cache != null){
					statusdocumento = cache;
					continue;
				}
				
				List<Vdocumentonegociado> lista = vdocumentonegociadoService.findByDocumentos(documento.getCddocumento().toString());
				boolean haveBaixada = false;
				boolean haveCancelada = false;
				boolean haveEmaberto = false;
				
				for (Vdocumentonegociado vdocumentonegociado : lista) {
					if(vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.BAIXADA.getCddocumentoacao())){
						haveBaixada = true;
					} else if(vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.CANCELADA.getCddocumentoacao())){
						haveCancelada = true;
					} else if(!vdocumentonegociado.getCddocumentoacaonegociado().equals(Documentoacao.NEGOCIADA.getCddocumentoacao())){
						haveEmaberto = true;
					}
				}
				
				if(haveBaixada && !haveEmaberto){
					statusdocumento = "1";
				} else if(haveCancelada && !haveEmaberto){
					statusdocumento = "2";
				} else {
					statusdocumento = "0";
				}
				
				CacheWebserviceUtil.putConsultaPagamento(documento.getCddocumento(), statusdocumento);
			} else {
				statusdocumento = "0";
			}
		}
		
		return statusdocumento;
	}

	/**
	* M�todo que verifica se o pedido de venda tem sincroniza��o com wms e est� marcado para confirma��o manual
	*
	* @param bean
	* @return
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public boolean bloquearEdicaoQtde(Pedidovenda bean) {
		if(bean != null && bean.getCdpedidovenda() != null){
			Pedidovenda pedidovenda = loadWithPedidovendatipo(bean);
			return pedidovenda.getIdentificadorcarregamento() != null &&
					pedidovenda.getPedidovendatipo() != null &&
					pedidovenda.getPedidovendatipo().getSincronizarComWMS() != null &&
					pedidovenda.getPedidovendatipo().getSincronizarComWMS() &&
					pedidovenda.getPedidovendatipo().getConfirmacaoManualWMS() != null &&
					pedidovenda.getPedidovendatipo().getConfirmacaoManualWMS();
		}
		return false;
	}
	

	
	/**
	* M�todo que calcula o valor aproximado de imposto do pedido de venda
	*
	* @param venda
	* @return
	* @since 27/10/2015
	* @author Luiz Fernando
	*/
	public void calcularValorAproximadoImposto(Pedidovenda venda) {
		Money valoraproximadoimposto = null;
		
		if(SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			List<Material> listaMaterial = materialService.findCalcularImposto(CollectionsUtil.listAndConcatenate(venda.getListaPedidovendamaterial(), "material.cdmaterial", ","));
			
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				vendamaterial.setValorimposto(null);
				Material material = materialService.getMaterialByLista(vendamaterial.getMaterial(), listaMaterial);
				if(material != null){
					if(venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0){
						Money descontoproporcional = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, venda.getDesconto(), venda.getValorusadovalecompra(), true);
						if(descontoproporcional != null){
							vendamaterial.setDesconto(descontoproporcional);
						}
					}
					
					Origemproduto origemproduto = material.getOrigemproduto();
					boolean servico = material.getServico() != null && material.getServico();
					
					Double percentualimposto = notafiscalprodutoService.getPercentualImposto(venda.getEmpresa(), material.getPercentualimpostoecf(), origemproduto, material.getNcmcompleto(),
																				material.getExtipi(), material.getCodigonbs(), material.getCodlistaservico(), !servico, null);
					
					if(percentualimposto != null){
						Double valorImpostoItem = 0d;
						if(vendamaterial.getQuantidade() != null && vendamaterial.getPreco() != null){
							valorImpostoItem += vendamaterial.getQuantidade() * vendamaterial.getPreco();
						}
						if(vendamaterial.getDesconto() != null){
							valorImpostoItem -= vendamaterial.getDesconto().getValue().doubleValue();
						}
						
						if(valoraproximadoimposto == null) valoraproximadoimposto = new Money();
						vendamaterial.setValorimposto(new Money(valorImpostoItem * percentualimposto / 100));
						valoraproximadoimposto = valoraproximadoimposto.add(vendamaterial.getValorimposto()) ;
					}
				}
			}
		}
		venda.setValoraproximadoimposto(valoraproximadoimposto);
	}
	
	/**
	 * M�todo que atualiza em massa as observa��es dos pedidos de venda contidos no par�metro.
	 * 
	 * @param listaPedidovenda
	 * @author Rafael Salvio
	 */
	public void atualizarObservacaoPedidovenda(final List<Pedidovenda> listaPedidovenda){
		pedidovendaDAO.atualizarObservacaoPedidovenda(listaPedidovenda);
	}
	
	/**
	 * M�todo que calcula o percentual de desconto de uma venda a partir de um pedidovenda.
	 * 
	 * @param venda
	 * @param pedidovenda
	 * @return
	 * @author Rafael Salvio
	 */
	public Money calculaDescontoPercentual(Venda venda, Pedidovenda pedidovenda){
		Double descontoVenda = 0d;
		if(venda.getTotalvenda().getValue().doubleValue() > 0){
			descontoVenda = (venda.getTotalvenda().getValue().doubleValue() * pedidovenda.getDesconto().getValue().doubleValue()) 
									/ (pedidovenda.getTotalvenda().add(pedidovenda.getDesconto()).getValue().doubleValue());

		}
		return new Money(descontoVenda);
	}
	
	/**
	 * M�todo que retorna o pedido de venda a partir do item passado por par�metro.
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public Pedidovenda getPedidovendaByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null) return null;
		pedidovendamaterial = pedidovendamaterialService.loadWithPedidovenda(pedidovendamaterial);
		return pedidovendamaterial != null ? pedidovendamaterial.getPedidovenda() : null;
	}
	
	/**
	* M�todo que verifica se o produto � tributado por ipi e n�o existe valor de ipi
	*
	* @param listaVendamaterial
	* @return
	* @since 11/12/2015
	* @author Luiz Fernando
	*/
	public Boolean existeProdutoTributadoIpiSemValorIpi(List<Pedidovendamaterial> listaVendamaterial, Empresa empresa){
		if(empresaService.exibirIpiVenda(empresa) &&
				SinedUtil.isListNotEmpty(listaVendamaterial)){
			for(Pedidovendamaterial item : listaVendamaterial){
				if(item.getMaterial() != null){
					if(item.getMaterial().getTributacaoipi() == null){
						item.getMaterial().setTributacaoipi(materialService.load(item.getMaterial(), "material.cdmaterial, material.tributacaoipi").getTributacaoipi());
					}
					if(item.getMaterial().getTributacaoipi() != null && item.getMaterial().getTributacaoipi() &&
							item.getTipocobrancaipi() == null){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	* M�todo que calcula o imposto do pedido de venda
	*
	* @param venda
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	public void calcularImposto(Pedidovenda venda) {
		try {
			if(venda.getCdpedidovenda() != null){
				venda.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(venda));
				if(SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterialmestre())){
					for(Pedidovendamaterialmestre mestre: venda.getListaPedidovendamaterialmestre()){
						if(!Boolean.TRUE.equals(mestre.getExibiritenskitflexivel())){
							List<Pedidovendamaterial> lista = this.findByMestre(mestre, venda.getListaPedidovendamaterial());
							for(Pedidovendamaterial pvm: lista){
								if(pvm.getMaterialmestre() != null){
									pvm.getMaterialmestre().setExibirItensKitFlexivelTrans(false);
								}
							}
						}
					}
				}
			}
			if(empresaService.exibirIpiVenda(venda.getEmpresa()) || empresaService.exibirIcmsVenda(venda.getEmpresa())){
				setValorImpostoVenda(venda, vendaService.calcularImposto(criaImpostovenda(venda)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	protected Pedidovendamaterialmestre findMestre(Pedidovendamaterialmestre vendaMaterialFind, List<Pedidovendamaterialmestre> listaVendamaterialmestre){
//		for(Pedidovendamaterialmestre vmm: listaVendamaterialmestre){
//			if(vmm.getIdentificadorinterno() != null && vmm.getIdentificadorinterno().equals(vendaMaterialFind.getIdentificadorinterno())){
//				return vmm;
//			}
//		}
//		return null;
//	}
	
	protected List<Pedidovendamaterial> findByMestre(Pedidovendamaterialmestre vendaMaterialMestreFind, List<Pedidovendamaterial> listaVendamaterial){
		List<Pedidovendamaterial> listaVendamaterialRetorno = new ArrayList<Pedidovendamaterial>();
		for(Pedidovendamaterial vm: listaVendamaterial){
			if(vm.getIdentificadorinterno() != null && vm.getIdentificadorinterno().equals(vendaMaterialMestreFind.getIdentificadorinterno())){
				listaVendamaterialRetorno.add(vm);
			}
		}
		return listaVendamaterialRetorno;
	}
	
	/**
	* M�todo que seta os valores de impostos no pedido de venda
	*
	* @param venda
	* @param impostovenda
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	protected void setValorImpostoVenda(Pedidovenda venda, Impostovenda impostovenda) {
		for(Impostovendamaterial impostovendamaterial : impostovenda.getListaImpostovendamaterial()){
			if(Boolean.TRUE.equals(impostovendamaterial.getConsiderarIpiMestre())){
				Pedidovendamaterialmestre vendamaterialmestre = impostovendamaterial.getPedidovendamaterialmestre();
				if(vendamaterialmestre != null){
					vendamaterialmestre.setValoripi(impostovendamaterial.getValoripi());
					vendamaterialmestre.setIpi(impostovendamaterial.getIpi());
					vendamaterialmestre.setTipocobrancaipi(impostovendamaterial.getTipocobrancaipi());
					vendamaterialmestre.setTipoCalculoIpi(impostovendamaterial.getTipocalculoipi());
					vendamaterialmestre.setGrupotributacao(impostovendamaterial.getGrupotributacao());
					
					
					vendamaterialmestre.setTipotributacaoicms(impostovendamaterial.getTipotributacaoicms());
					vendamaterialmestre.setTipocobrancaicms(impostovendamaterial.getTipocobrancaicms());
					vendamaterialmestre.setModalidadebcicms(impostovendamaterial.getModalidadebcicms());
					vendamaterialmestre.setValorbcicms(impostovendamaterial.getValorbcicms());
					vendamaterialmestre.setIcms(impostovendamaterial.getIcms());
					vendamaterialmestre.setValoricms(impostovendamaterial.getValoricms());
					vendamaterialmestre.setPercentualdesoneracaoicms(impostovendamaterial.getPercentualdesoneracaoicms());
					vendamaterialmestre.setValordesoneracaoicms(impostovendamaterial.getValordesoneracaoicms());
					vendamaterialmestre.setAbaterdesoneracaoicms(impostovendamaterial.getAbaterdesoneracaoicms());
					
					vendamaterialmestre.setValorbcicmsst(impostovendamaterial.getValorbcicmsst());
					vendamaterialmestre.setIcmsst(impostovendamaterial.getIcmsst());
					vendamaterialmestre.setValoricmsst(impostovendamaterial.getValoricmsst());
					vendamaterialmestre.setModalidadebcicmsst(impostovendamaterial.getModalidadebcicmsst());
					
					vendamaterialmestre.setReducaobcicmsst(impostovendamaterial.getReducaobcicmsst());
					vendamaterialmestre.setMargemvaloradicionalicmsst(impostovendamaterial.getMargemvaloradicionalicmsst());
					
					vendamaterialmestre.setValorbcfcp(impostovendamaterial.getValorbcfcp());
					vendamaterialmestre.setFcp(impostovendamaterial.getFcp());
					vendamaterialmestre.setValorfcp(impostovendamaterial.getValorfcp());
					vendamaterialmestre.setValorbcfcpst(impostovendamaterial.getValorbcfcpst());
					vendamaterialmestre.setFcpst(impostovendamaterial.getFcpst());
					vendamaterialmestre.setValorfcpst(impostovendamaterial.getValorfcpst());
					
					vendamaterialmestre.setDadosicmspartilha(impostovendamaterial.getDadosicmspartilha());
					vendamaterialmestre.setValorbcdestinatario(impostovendamaterial.getValorbcdestinatario());
					vendamaterialmestre.setValorbcfcpdestinatario(impostovendamaterial.getValorbcfcpdestinatario());
					vendamaterialmestre.setFcpdestinatario(impostovendamaterial.getFcpdestinatario());
					vendamaterialmestre.setIcmsdestinatario(impostovendamaterial.getIcmsdestinatario());
					vendamaterialmestre.setIcmsinterestadual(impostovendamaterial.getIcmsinterestadual());
					vendamaterialmestre.setIcmsinterestadualpartilha(impostovendamaterial.getIcmsinterestadualpartilha());
					vendamaterialmestre.setValorfcpdestinatario(impostovendamaterial.getValorfcpdestinatario());
					vendamaterialmestre.setValoricmsdestinatario(impostovendamaterial.getValoricmsdestinatario());
					vendamaterialmestre.setValoricmsremetente(impostovendamaterial.getValoricmsremetente());
					vendamaterialmestre.setDifal(impostovendamaterial.getDifal());
					vendamaterialmestre.setValordifal(impostovendamaterial.getValordifal());
					
					vendamaterialmestre.setCfop(impostovendamaterial.getCfop());
					vendamaterialmestre.setNcmcapitulo(impostovendamaterial.getNcmcapitulo());
					vendamaterialmestre.setNcmcompleto(impostovendamaterial.getNcmcompleto());
				}
			}else{
				Pedidovendamaterial vendamaterial = impostovendamaterial.getPedidovendamaterial();
				if(vendamaterial != null){
					vendamaterial.setValoripi(impostovendamaterial.getValoripi());
					vendamaterial.setIpi(impostovendamaterial.getIpi());
					vendamaterial.setTipocobrancaipi(impostovendamaterial.getTipocobrancaipi());
					vendamaterial.setAliquotareaisipi(impostovendamaterial.getAliquotareaisipi());
					vendamaterial.setTipocalculoipi(impostovendamaterial.getTipocalculoipi());
					vendamaterial.setGrupotributacao(impostovendamaterial.getGrupotributacao());
					
					vendamaterial.setTipotributacaoicms(impostovendamaterial.getTipotributacaoicms());
					vendamaterial.setTipocobrancaicms(impostovendamaterial.getTipocobrancaicms());
					vendamaterial.setModalidadebcicms(impostovendamaterial.getModalidadebcicms());
					vendamaterial.setValorbcicms(impostovendamaterial.getValorbcicms());
					vendamaterial.setIcms(impostovendamaterial.getIcms());
					vendamaterial.setValoricms(impostovendamaterial.getValoricms());
					vendamaterial.setPercentualdesoneracaoicms(impostovendamaterial.getPercentualdesoneracaoicms());
					vendamaterial.setValordesoneracaoicms(impostovendamaterial.getValordesoneracaoicms());
					vendamaterial.setAbaterdesoneracaoicms(impostovendamaterial.getAbaterdesoneracaoicms());
					
					vendamaterial.setValorbcicmsst(impostovendamaterial.getValorbcicmsst());
					vendamaterial.setIcmsst(impostovendamaterial.getIcmsst());
					vendamaterial.setValoricmsst(impostovendamaterial.getValoricmsst());
					vendamaterial.setModalidadebcicmsst(impostovendamaterial.getModalidadebcicmsst());
					
					vendamaterial.setReducaobcicmsst(impostovendamaterial.getReducaobcicmsst());
					vendamaterial.setMargemvaloradicionalicmsst(impostovendamaterial.getMargemvaloradicionalicmsst());
					
					vendamaterial.setValorbcfcp(impostovendamaterial.getValorbcfcp());
					vendamaterial.setFcp(impostovendamaterial.getFcp());
					vendamaterial.setValorfcp(impostovendamaterial.getValorfcp());
					vendamaterial.setValorbcfcpst(impostovendamaterial.getValorbcfcpst());
					vendamaterial.setFcpst(impostovendamaterial.getFcpst());
					vendamaterial.setValorfcpst(impostovendamaterial.getValorfcpst());
					
					vendamaterial.setDadosicmspartilha(impostovendamaterial.getDadosicmspartilha());
					vendamaterial.setValorbcdestinatario(impostovendamaterial.getValorbcdestinatario());
					vendamaterial.setValorbcfcpdestinatario(impostovendamaterial.getValorbcfcpdestinatario());
					vendamaterial.setFcpdestinatario(impostovendamaterial.getFcpdestinatario());
					vendamaterial.setIcmsdestinatario(impostovendamaterial.getIcmsdestinatario());
					vendamaterial.setIcmsinterestadual(impostovendamaterial.getIcmsinterestadual());
					vendamaterial.setIcmsinterestadualpartilha(impostovendamaterial.getIcmsinterestadualpartilha());
					vendamaterial.setValorfcpdestinatario(impostovendamaterial.getValorfcpdestinatario());
					vendamaterial.setValoricmsdestinatario(impostovendamaterial.getValoricmsdestinatario());
					vendamaterial.setValoricmsremetente(impostovendamaterial.getValoricmsremetente());
					vendamaterial.setDifal(impostovendamaterial.getDifal());
					vendamaterial.setValordifal(impostovendamaterial.getValordifal());
					
					vendamaterial.setCfop(impostovendamaterial.getCfop());
					vendamaterial.setNcmcapitulo(impostovendamaterial.getNcmcapitulo());
					vendamaterial.setNcmcompleto(impostovendamaterial.getNcmcompleto());
				}
			}
		}
	}
	
	/**
	* M�todo que cria o bean de imposto a partir do pedido de venda
	*
	* @param venda
	* @return
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	protected Impostovenda criaImpostovenda(Pedidovenda venda) {
		Impostovenda impostovenda = new Impostovenda();
		impostovenda.setCliente(venda.getCliente());
		impostovenda.setDesconto(venda.getDesconto());
		impostovenda.setEmpresa(venda.getEmpresa());
		impostovenda.setEndereco(venda.getEndereco());
		impostovenda.setValorfrete(venda.getValorfrete());
		impostovenda.setValorusadovalecompra(venda.getValorusadovalecompra());
		impostovenda.setPedidovendatipo(venda.getPedidovendatipo());
		
		List<Impostovendamaterial> listaImpostovendamaterial = new ArrayList<Impostovendamaterial>();
		for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
			Impostovendamaterial impostovendamaterial = new Impostovendamaterial(vendamaterial);
			impostovendamaterial.setPedidovendamaterial(vendamaterial);
			impostovendamaterial.setMaterialMestre(vendamaterial.getMaterialmestre());
			if(vendamaterial.getMaterialmestre() != null){
				Material materialAux = materialService.load(vendamaterial.getMaterialmestre(), "material.cdmaterial, material.kitflexivel, material.tributacaoipi");
				if(materialAux != null && Boolean.TRUE.equals(materialAux.getKitflexivel()) && !Boolean.TRUE.equals(vendamaterial.getMaterialmestre().getExibirItensKitFlexivelTrans())){
					impostovendamaterial.setConsiderarIpiMestre(true);
					for(Pedidovendamaterialmestre vmm: venda.getListaPedidovendamaterialmestre()){
						if(Util.strings.emptyIfNull(vendamaterial.getIdentificadorinterno()).equals(vmm.getIdentificadorinterno())){
							impostovendamaterial.setPedidovendamaterialmestre(vmm);
							break;
						}
					}
				}
			}
			listaImpostovendamaterial.add(impostovendamaterial);
		}
		
		impostovenda.setListaImpostovendamaterial(listaImpostovendamaterial);
		return impostovenda;
	}
	
	/**
	* M�todo que recalcula e executa update dos impostos do pedido de venda
	*
	* @param pedidovenda
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	protected void recalcularESalvarImposto(Pedidovenda pedidovenda) {
		if(pedidovenda.getEmpresa() != null && pedidovenda.getEmpresa().getExibiripivenda() != null &&
				pedidovenda.getEmpresa().getExibiripivenda()){
			Pedidovenda pv = loadForEntrada(pedidovenda);
			if(pv != null){
				calcularImposto(pv);
				if(SinedUtil.isListNotEmpty(pv.getListaPedidovendamaterial())){
					for(Pedidovendamaterial pvm : pv.getListaPedidovendamaterial()){
						pedidovendamaterialService.updateInformacoesImposto(pvm);
					}
				}
			}
		}
	}
	
	public void montarVendaOrdemServicoVeterinaria(String whereIn, Pedidovenda form) {
		List<Ordemservicoveterinaria> listOS = ordemservicoveterinariaService.carregaRequisicaoForFaturar(whereIn);
		if(listOS != null && !listOS.isEmpty()){
			for (Ordemservicoveterinaria os : listOS) {
				if(form.getCliente() == null){
					form.setCliente(os.getCliente());
				}
				if(os.getEmpresa() != null){
					form.setEmpresa(os.getEmpresa());
				}
				if(os.getAtividadetipoveterinaria() != null && os.getAtividadetipoveterinaria().getLocalarmazenagem() != null){
					form.setLocalarmazenagem(os.getAtividadetipoveterinaria().getLocalarmazenagem());
				}
				ordemservicoveterinariaService.ajustaListaOrdemservicoveterinariamaterial(os);
				if (os.getListaOrdemservicoveterinariamaterial() != null && !os.getListaOrdemservicoveterinariamaterial().isEmpty()) {
					for (Ordemservicoveterinariamaterial iterable : os.getListaOrdemservicoveterinariamaterial()) {
						if (iterable != null && (iterable.getFaturado() == null || iterable.getFaturado() == false) && iterable.getMaterial() != null && iterable.getQtdeusada() != null && iterable.getQtdeusada() > 0d) {
							boolean adicionar = true;
							if(SinedUtil.isListNotEmpty(form.getListaPedidovendamaterial())){
								for (Pedidovendamaterial vendamaterial : form.getListaPedidovendamaterial()) {
									if(vendamaterial.getMaterial().equals(iterable.getMaterial()) && vendamaterial.getMaterialmestre() == null &&
											((vendamaterial.getLoteestoque() == null && iterable.getLoteestoque() == null) ||
											 (vendamaterial.getLoteestoque() != null && iterable.getLoteestoque() != null && vendamaterial.getLoteestoque().equals(iterable.getLoteestoque())))){
										vendamaterial.setQuantidade(vendamaterial.getQuantidade() + iterable.getQtdeusada());
										if(iterable.getCdordemservicoveterinariamaterial() != null){
											if(org.apache.commons.lang.StringUtils.isNotBlank(vendamaterial.getWhereInOSVM())){
												vendamaterial.setWhereInOSVM(vendamaterial.getWhereInOSVM() + "," + iterable.getCdordemservicoveterinariamaterial().toString());
											}else {
												vendamaterial.setWhereInOSVM(iterable.getCdordemservicoveterinariamaterial().toString());
											}
										}
										adicionar = false;
									}
								}
							} 
							
							if(adicionar){
								Pedidovendamaterial item =  new Pedidovendamaterial();
								item.setMaterial(iterable.getMaterial());
								item.setMaterialmestre(iterable.getMaterialmestre());
//								item.setPreco(item.getMaterial().getValorvenda());
								if (iterable.getValortotal()!=null && iterable.getQtdeusada()!=null && iterable.getQtdeusada()>0D){
									item.setPreco(SinedUtil.round(iterable.getValortotal().getValue().doubleValue() / iterable.getQtdeusada(), 2));
								}
								item.setQuantidade(iterable.getQtdeusada());
								if(iterable.getLoteestoque() != null && iterable.getLoteestoque().getCdloteestoque() != null){
									if(iterable.getLoteestoque().getQtde() == null){
										iterable.getLoteestoque().setQtde(loteestoqueService.getQtdeLoteestoque(item.getMaterial(), null, form.getEmpresa(), iterable.getLoteestoque(), null));
									}
									item.setLoteestoque(iterable.getLoteestoque());
								}
								if(iterable.getMaterial().getUnidademedida() != null)
									item.setUnidademedida(iterable.getMaterial().getUnidademedida());
								if(iterable.getCdordemservicoveterinariamaterial() != null){
									item.setWhereInOSVM(iterable.getCdordemservicoveterinariamaterial().toString());
								}
								form.getListaPedidovendamaterial().add(item);
							}
						}
					}
				}
				if (form.getColaborador()==null && os.getResponsavel()!=null){
					form.setColaborador(os.getResponsavel());
				}
			}
		}
		form.setPedidovendatipo(pedidovendatipoService.carregarTipoOSVeterinaria());
		form.setWhereInOSV(whereIn);
		
		setOrdemItem(form);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#existePedidoComServicoEProduto(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 04/02/2016
	* @author Luiz Fernando
	*/
	public boolean existePedidoComServicoEProduto(String whereIn) {
		return pedidovendaDAO.existePedidoComServicoEProduto(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForRegistrarDevolucao(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 04/02/2016
	* @author Luiz Fernando
	*/
	public Pedidovenda loadForRegistrarDevolucao(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForRegistrarDevolucao(pedidovenda);
	}
	
	public VendaDevolucaoBean criaVendaDevolucaoBean(Pedidovenda venda){
		VendaDevolucaoBean bean = new VendaDevolucaoBean(venda);
		
		List<VendamaterialDevolucaoBean> listaItens = new ArrayList<VendamaterialDevolucaoBean>();
		if(SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			Double qtdeJaDevolvida;
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null && vendamaterial.getQuantidade() != null){
					VendamaterialDevolucaoBean itemBean = new VendamaterialDevolucaoBean();
					itemBean.setMaterial(vendamaterial.getMaterial());
					
					if(vendamaterial.getAltura() != null || vendamaterial.getLargura() != null && vendamaterial.getComprimento() != null){
						itemBean.setMaterial(vendamaterial.getMaterial().copiaMaterial());
						itemBean.getMaterial().setNome(vendamaterial.getMaterial().getNome() + " " + getAlturaLarguraComprimentoConcatenados(vendamaterial, vendamaterial.getMaterial()));
					}
					
					if(vendamaterial.getMultiplicador() == null) vendamaterial.setMultiplicador(1d);
					Money valorUsadoValeCompra = new Money();//venda.getValorusadovalecompra()//Na devolu��o, o valor de vale compra n�o deve ser considerado como desconto, para que o valor de vale compra usado volte como cr�dito para o cliente
					Money valorDesconto = notafiscalprodutoService.getValorDescontoVenda(venda.getListaPedidovendamaterial(), vendamaterial, venda.getDesconto(), valorUsadoValeCompra, true);
					Money valorDescontoItem = vendamaterial.getDesconto() != null ? vendamaterial.getDesconto() :  new Money();
					if(vendamaterial.getQuantidade() != null && vendamaterial.getQuantidade() > 0 && 
							vendamaterial.getMultiplicador() != null && vendamaterial.getMultiplicador() > 0){
						valorDescontoItem = valorDescontoItem.divide(new Money(vendamaterial.getQuantidade() * vendamaterial.getMultiplicador()));
					}
					
					itemBean.setPreco(vendamaterial.getPreco());
					if(valorDesconto != null){
						itemBean.setPreco(vendamaterial.getPreco() - valorDesconto.getValue().doubleValue() - valorDescontoItem.getValue().doubleValue());
					}
					
					if(materialdevolucaoService.existeDevolucaoUnidadeDiferente(venda, vendamaterial.getMaterial(), vendamaterial, vendamaterial.getUnidademedida())){
						qtdeJaDevolvida = materialdevolucaoService.getQtdeJaDevolvida(venda, vendamaterial.getMaterial(), vendamaterial);
						if(qtdeJaDevolvida != null && qtdeJaDevolvida > 0 && vendamaterial.getMaterial().getUnidademedida() != null && vendamaterial.getUnidademedida() != null &&
							!vendamaterial.getUnidademedida().equals(vendamaterial.getMaterial().getUnidademedida())){
								if(vendamaterial.getFatorconversao() != null && vendamaterial.getQtdereferencia() != null && 
										vendamaterial.getFatorconversaoQtdereferencia() != null &&
										vendamaterial.getFatorconversaoQtdereferencia() > 0){
									qtdeJaDevolvida = qtdeJaDevolvida * vendamaterial.getFatorconversaoQtdereferencia();
								} else {
									qtdeJaDevolvida = unidademedidaService.converteQtdeUnidademedida(vendamaterial.getMaterial().getUnidademedida(), qtdeJaDevolvida, vendamaterial.getUnidademedida(), vendamaterial.getMaterial(), vendamaterial.getFatorconversao(), vendamaterial.getQtdereferencia());
								}
						}
					}else {
						qtdeJaDevolvida = materialdevolucaoService.getQtdeJaDevolvida(venda, vendamaterial.getMaterial(), vendamaterial);
					}
					
					itemBean.setQuantidadeTrans(vendamaterial.getQuantidade());
					itemBean.setQtdejadevolvida(SinedUtil.round(qtdeJaDevolvida, 6));
					itemBean.setSaldo(vendamaterial.getQuantidade() - qtdeJaDevolvida);
					itemBean.setQuantidade(vendamaterial.getQuantidade());
					itemBean.setQtdedevolvida(itemBean.getSaldo());
					itemBean.setUnidademedida(vendamaterial.getUnidademedida());
					itemBean.setUnidademedidaDevolucao(vendamaterial.getUnidademedida());
					itemBean.setPedidovendamaterial(vendamaterial);
					if(itemBean.getPreco() != null && itemBean.getQtdedevolvida() != null){
						itemBean.setValordevolvido(itemBean.getPreco() * itemBean.getQtdedevolvida());
					}
					listaItens.add(itemBean);
				}
			}
			bean.setListaItens(listaItens);
		}
		return bean;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadWithLocal(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 12/02/2016
	* @author Luiz Fernando
	*/
	public Pedidovenda loadWithLocalAndProjeto(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadWithLocalAndProjeto(pedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.PedidovendaService#existeSomenteProdutoVendaECF(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 17/03/2016
	* @author Luiz Fernando
	*/
	public boolean existeSomenteProdutoVendaECF(String whereIn) {
		return pedidovendaDAO.existeSomenteProdutoVendaECF(whereIn);
	}
	
	@SuppressWarnings("unchecked")
	public void manterPedidovendaWSSOAP(ManterPedidovendaBean bean) {
		String identificador = bean.getIdentificador();
		java.sql.Date dtpedidovenda = bean.getDataPedido();
		java.sql.Date dtprazoentrega = bean.getPrazoEntrega();
		Double desconto = bean.getDesconto();
		String observacao = bean.getObservacao();
		String observacaoInterna = bean.getObservacaoInterna();
		Boolean emitirNotaColeta = bean.getEmitirNotaColeta();
		
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		Integer codigo_cliente = bean.getCliente();
		Cliente cliente = clienteService.load(new Cliente(codigo_cliente), "cliente.cdpessoa, cliente.nome");
		if(cliente == null){
			throw new SinedException("Cliente n�o encontrado com o c�digo '" + codigo_cliente + "'.");
		}
		
		Integer codigo_colaborador = bean.getVendedor();
		Colaborador colaborador = colaboradorService.load(new Colaborador(codigo_colaborador), "colaborador.cdpessoa, colaborador.nome");
		if(colaborador == null){
			throw new SinedException("Vendedor n�o encontrado com o c�digo '" + codigo_colaborador + "'.");
		}
		
		Integer tipoPedidoVenda = bean.getTipoPedidoVenda();
		Pedidovendatipo pedidovendatipo = null; 
		if(tipoPedidoVenda != null){
			pedidovendatipo = pedidovendatipoService.load(new Pedidovendatipo(tipoPedidoVenda));
			if(pedidovendatipo == null){
				throw new SinedException("Tipo de pedido de venda n�o encontrado com o c�digo '" + tipoPedidoVenda + "'.");
			}
		}
		
		Map<Integer, Material> mapMaterial = new HashMap<Integer, Material>();
		List<ManterPedidovendaItemBean> item = bean.getItem();
		for (ManterPedidovendaItemBean itemBean : item) {
			Integer codigo_material = itemBean.getCodigo();
			
			if(!mapMaterial.containsKey(codigo_material)){
				Material material = materialService.loadForIntegracaoSOAP(new Material(codigo_material));
				if(material == null){
					throw new SinedException("Material n�o encontrado com o c�digo '" + codigo_material + "'.");
				}
				mapMaterial.put(codigo_material, material);
			}
		}
		
		Map<Integer, Documentotipo> mapDocumentotipo = new HashMap<Integer, Documentotipo>();
		List<ManterPedidovendaParcelaBean> parcela = bean.getParcela();
		for (ManterPedidovendaParcelaBean parcelaBean : parcela) {
			Integer codigo_documentotipo = parcelaBean.getTipo();
			if(!mapDocumentotipo.containsKey(codigo_documentotipo)){
				Documentotipo documentotipo = documentotipoService.load(new Documentotipo(codigo_documentotipo), "documentotipo.cddocumentotipo, documentotipo.nome");
				if(documentotipo == null){
					throw new SinedException("Tipo de documento n�o encontrado com o c�digo '" + codigo_documentotipo + "'.");
				}
				mapDocumentotipo.put(codigo_documentotipo, documentotipo);
			}
		}
		
		Integer cdprazopagamento = ParametrogeralService.getInstance().getInteger(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PRAZOPAGAMENTO);
		Integer cdpedidovendatipo = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_PEDIDOVENDATIPO);
		Integer cdlocalarmazenagem = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_LOCALARMAZENAGEM);
		Integer cdconta = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_CONTA);
		
		Prazopagamento prazopagamento = prazopagamentoService.loadAll(new Prazopagamento(cdprazopagamento));
		Pedidovendatipo pedidovendatipo_padrao = cdpedidovendatipo != null ? pedidovendatipoService.load(new Pedidovendatipo(cdpedidovendatipo)) : null;
		Localarmazenagem localarmazenagem = cdlocalarmazenagem != null ? new Localarmazenagem(cdlocalarmazenagem) : null;
		if(bean.getLocalarmazenagem() != null){
			localarmazenagem = localarmazenagemService.load(new Localarmazenagem(bean.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo"));
			if(localarmazenagem == null){
				throw new SinedException("Local de armazenagem n�o encontrado com o c�digo '" + bean.getLocalarmazenagem() + "'.");
			}
		}
		Conta conta = cdconta != null ? new Conta(cdconta) : null;
		
		List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
		cliente.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
		
		Pedidovenda pedidovenda = new Pedidovenda();
		
		boolean criar = true;
		List<Pedidovenda> listaPedidovendaIdentificadorCnpj = this.findByIdentificadorCnpj(identificador, cnpj_empresa);
		if(listaPedidovendaIdentificadorCnpj != null && listaPedidovendaIdentificadorCnpj.size() > 0){
			if(listaPedidovendaIdentificadorCnpj.size() > 1){
				throw new SinedException("Mais de um pedido encontrado com o identificador para a empresa.");
			}
			pedidovenda = listaPedidovendaIdentificadorCnpj.get(0);
			pedidovenda = this.loadForEntrada(pedidovenda);
			
			criar = false;
		}
		
		pedidovenda.setIdentificador(identificador);
		pedidovenda.setPedidovendatipo(pedidovendatipo != null ? pedidovendatipo : pedidovendatipo_padrao);
		pedidovenda.setPresencacompradornfe(Presencacompradornfe.OUTROS);
		pedidovenda.setCliente(cliente);
		pedidovenda.setEmpresa(empresa);
		pedidovenda.setEndereco(cliente.getEndereco());
		pedidovenda.setColaborador(colaborador);
		pedidovenda.setDtpedidovenda(dtpedidovenda);
		pedidovenda.setObservacao(observacao);
		pedidovenda.setObservacaointerna(observacaoInterna);
		pedidovenda.setPrazopagamento(prazopagamento);
		pedidovenda.setPrazomedio(Boolean.TRUE);
		pedidovenda.setQtdeParcelas(parcela.size());
		pedidovenda.setConta(conta);
		pedidovenda.setFrete(ResponsavelFrete.SEM_FRETE);
		pedidovenda.setLocalarmazenagem(localarmazenagem);
		pedidovenda.setDesconto(desconto != null ? new Money(desconto) : null);
		pedidovenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
		pedidovenda.setOrigemwebservice(true);
		pedidovenda.setEmitirnotacoleta(emitirNotaColeta);
		pedidovenda.setIdentificacaoexterna(bean.getIdentificadorexterno());
		
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		for (ManterPedidovendaItemBean itemBean : item) {
			Integer codigo = itemBean.getCodigo();
			Integer identificadorItem = itemBean.getIdentificador();
			Double quantidade = itemBean.getQuantidade();
			Double valor = itemBean.getValor();
			Double valorcoleta = itemBean.getValorcoleta();
			Double descontoItem = itemBean.getDesconto();
			String observacaoItem = itemBean.getObservacao();
			
			Pneu pneu = null;
			Integer cdpneu = itemBean.getPneu();
			if(cdpneu != null && !cdpneu.equals(0)){
				Integer banda = itemBean.getBanda();
				Integer marca = itemBean.getMarca();
				Integer medida = itemBean.getMedida();
				Integer modelo = itemBean.getModelo();
				
				pneu = pneuService.loadForEntrada(new Pneu(cdpneu));
				if(pneu == null){
					pneu = pneuService.insertWithId(cdpneu);
				}
				
				pneu.setSerie(itemBean.getSerie());
				pneu.setDot(itemBean.getDot());
				
				if(marca != null) {
					Pneumarca pneumarca = pneumarcaService.load(new Pneumarca(marca));
					if(pneumarca == null) throw new SinedException("Marca do pneu n�o encontrada.");
					pneu.setPneumarca(pneumarca);
				} else pneu.setPneumarca(null);
				
				if(medida != null) {
					Pneumedida pneumedida = pneumedidaService.load(new Pneumedida(medida));
					if(pneumedida == null) throw new SinedException("Medida do pneu n�o encontrada.");
					pneu.setPneumedida(pneumedida);
				} else pneu.setPneumedida(null);
				
				if(modelo != null) {
					Pneumodelo pneumodelo = pneumodeloService.load(new Pneumodelo(modelo));
					if(pneumodelo == null) throw new SinedException("Modelo do pneu n�o encontrado.");
					pneu.setPneumodelo(pneumodelo);
				} else pneu.setPneumodelo(null);
				
				if(banda != null) {
					Material materialbanda = materialService.load(new Material(banda));
					if(materialbanda == null) throw new SinedException("Banda do pneu n�o encontrada.");
					pneu.setMaterialbanda(materialbanda);
				} else pneu.setMaterialbanda(null);
				
				pneuService.saveOrUpdate(pneu);
			}
			
			Material material = mapMaterial.get(codigo);
			
			Pedidovendamaterial pedidovendamaterial = new Pedidovendamaterial();
			if(!criar){
				List<Pedidovendamaterial> listaPedidovendamaterial_antigo = pedidovenda.getListaPedidovendamaterial();
				for (Pedidovendamaterial pedidovendamaterial_antigo : listaPedidovendamaterial_antigo) {
					if(pedidovendamaterial_antigo.getIdentificadorintegracao() != null && pedidovendamaterial_antigo.getIdentificadorintegracao().equals(identificadorItem)){
						pedidovendamaterial = pedidovendamaterial_antigo;
						break;
					}
				}
			}
			
			pedidovendamaterial.setPneu(pneu);
			pedidovendamaterial.setMaterial(material);
			pedidovendamaterial.setUnidademedida(material.getUnidademedida());
			pedidovendamaterial.setQuantidade(quantidade);
			pedidovendamaterial.setPreco(valor);
			pedidovendamaterial.setDesconto(descontoItem != null ? new Money(descontoItem) : null);
			pedidovendamaterial.setDtprazoentrega(dtprazoentrega);
			pedidovendamaterial.setObservacao(observacaoItem != null ? observacaoItem.trim() : "");
			pedidovendamaterial.setIdentificadorintegracao(identificadorItem);
			pedidovendamaterial.setMaterialcoleta(material.getMaterialcoleta());
			pedidovendamaterial.setValorcoleta(valorcoleta);
			
			pedidovendamaterial.setTotal(vendaService.getValortotal(pedidovendamaterial.getPreco(), 
					pedidovendamaterial.getQuantidade(),
					pedidovendamaterial.getDesconto(), 
					pedidovendamaterial.getMultiplicador(),
					pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro()));
			
			listaPedidovendamaterial.add(pedidovendamaterial);
		}
		pedidovenda.setListaPedidovendamaterial(listaPedidovendamaterial);
		setOrdemItem(pedidovenda);
		
		List<Pedidovendapagamento> listaPedidovendapagamento = new ArrayList<Pedidovendapagamento>();
		for (ManterPedidovendaParcelaBean parcelaBean : parcela) {
			Integer tipo = parcelaBean.getTipo();
			Double valor = parcelaBean.getValor();
			java.sql.Date vencimento = parcelaBean.getVencimento();
			
			Documentotipo documentotipo = mapDocumentotipo.get(tipo);
			
			Pedidovendapagamento pedidovendapagamento = new Pedidovendapagamento();
			pedidovendapagamento.setDocumentotipo(documentotipo);
			pedidovendapagamento.setValororiginal(new Money(valor));
			pedidovendapagamento.setDataparcela(vencimento);
			
			listaPedidovendapagamento.add(pedidovendapagamento);
		}
		pedidovenda.setListaPedidovendapagamento(listaPedidovendapagamento);
		
		this.saveOrUpdate(pedidovenda);
		
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		pedidovendahistorico.setPedidovenda(pedidovenda);
		if(criar){
			pedidovendahistorico.setAcao("Criado");
			pedidovendahistorico.setObservacao("Cria��o a partir do WebService SOAP");
		} else {
			pedidovendahistorico.setAcao("Alterado");
			pedidovendahistorico.setObservacao("Alterado a partir do WebService SOAP");
		}
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		
		try {
			updateValorUnitarioColeta(pedidovenda, item);
			
			if(pedidovenda.getPedidovendatipo() != null && pedidovenda.getPedidovendatipo().getColetaautomatica() != null && pedidovenda.getPedidovendatipo().getColetaautomatica()){
				gerarColetaAutomatica(pedidovenda.getCdpedidovenda().toString(), pedidovenda.getEmitirnotacoleta(), item);
			}
		} catch (Exception e) {
			String nomeMaquina = "N�o encontrado.";
			try {  
	            InetAddress localaddr = InetAddress.getLocalHost();  
	            nomeMaquina = localaddr.getHostName();  
	        } catch (UnknownHostException e3) {}  
			

	        String url = "N�o encontrado";
			try {  
				url = SinedUtil.getUrlWithContext();
	        } catch (Exception e3) {}
	        
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			enviarEmailErro("[W3ERP] Erro ao gerar coleta autom�tica.","Erro ao gerar coleta ao salvar pedido de venda (integra��o SOAP). (M�quina: " + nomeMaquina + ". URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>");
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param identificadorintegracao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 09/05/2016
	 */
	public Pedidovenda findByIdentificadorIntegracao(Integer identificadorintegracao) {
		return pedidovendaDAO.findByIdentificadorIntegracao(identificadorintegracao);
	}
	
	/**
	 * Realiza a confirma��o do pedido de venda para o webservice SOAP
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 09/05/2016
	 */
	public boolean confirmarPedidovendaWSSOAP(ConfirmarPedidovendaBean bean) {
		String identificador = bean.getIdentificador();
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		Pedidovenda pedidovenda = null;
		List<Pedidovenda> listaPedidovendaIdentificadorCnpj = this.findByIdentificadorCnpj(identificador, cnpj_empresa);
		if(listaPedidovendaIdentificadorCnpj != null && listaPedidovendaIdentificadorCnpj.size() > 0){
			if(listaPedidovendaIdentificadorCnpj.size() > 1){
				throw new SinedException("Mais de um pedido encontrado com o identificador para a empresa.");
			}
			pedidovenda = listaPedidovendaIdentificadorCnpj.get(0);
		}
		if(pedidovenda == null){
			throw new SinedException("Nenhum pedido de venda encontrado.");
		}
		pedidovenda = this.loadForConfirmacao(pedidovenda);
		
		List<br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean> item = bean.getItem();
		boolean haveConfirmacao = false;
		for (br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean itBean : item) {
			if(itBean.getRecusado() == null || !itBean.getRecusado()){
				haveConfirmacao = true;
			}
		}
		
		if(haveConfirmacao){

			boolean ajusteEstoque = false;
			Pedidovendatipo pedidovendatipo = pedidovenda.getPedidovendatipo();
			if(bean.getTipoPedidoVenda() != null && bean.getTipoPedidoVenda() > 0){
				Pedidovendatipo pedidovendatipoWS =  pedidovendatipoService.loadForEntrada(new Pedidovendatipo(bean.getTipoPedidoVenda()));
				if(pedidovendatipoWS != null){
					pedidovendatipo = pedidovendatipoWS;
				}
			}
			if(pedidovendatipo != null){
				BaixaestoqueEnum baixaestoqueEnum = pedidovendatipo.getBaixaestoqueEnum();
				if(baixaestoqueEnum != null && baixaestoqueEnum.equals(BaixaestoqueEnum.APOS_VENDAREALIZADA)){
					ajusteEstoque = true;
				}
			}
			
			boolean naoBaixarEstoque = false;
			boolean baixaEstoqueAposExpedicaoWMS = false;
			boolean permitirVendaSemEstoque = false;
			if(pedidovendatipo != null){
				naoBaixarEstoque = pedidovendatipo.getBaixaestoqueEnum() != null && BaixaestoqueEnum.NAO_BAIXAR.equals(pedidovendatipo.getBaixaestoqueEnum());
				baixaEstoqueAposExpedicaoWMS = pedidovendatipo.getBaixaestoqueEnum() != null && BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(pedidovendatipo.getBaixaestoqueEnum());
				permitirVendaSemEstoque = pedidovendatipo.getPermitirvendasemestoque() != null && pedidovendatipo.getPermitirvendasemestoque(); 
			}
			
			if(!localarmazenagemService.getPermitirestoquenegativo(pedidovenda.getLocalarmazenagem()) && !naoBaixarEstoque && !baixaEstoqueAposExpedicaoWMS && !permitirVendaSemEstoque){
				for (br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean itBean : item) {
					for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
						if(pedidovendamaterial == null || 
								pedidovendamaterial.getCdpedidovendamaterial() == null || 
								pedidovendamaterial.getMaterial() == null || 
								pedidovendamaterial.getMaterial().getCdmaterial() == null){
							continue;
						}
						
						if(pedidovendamaterial.getIdentificadorintegracao().equals(itBean.getIdentificador()) && (pedidovendamaterial.getMaterial().getServico() == null || !pedidovendamaterial.getMaterial().getServico())){
							
							
							Double quantidadeIt = itBean.getQuantidade();
							
							Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(pedidovendamaterial.getMaterial(), pedidovenda.getLocalarmazenagem(), empresa, null);
							if (entrada == null){
								entrada = 0d;
							}
							Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(pedidovendamaterial.getMaterial(), pedidovenda.getLocalarmazenagem(), empresa, null);
							if (saida == null){
								saida = 0d;
							}

							Double qtddisponivel = SinedUtil.getQtdeEntradaSubtractSaida(entrada, saida);
							
							if(quantidadeIt > qtddisponivel){
								return false;
							}
						}
					}
				}
			}
			
			
			Venda venda = new Venda();
			
			venda.setIdentificador(pedidovenda.getIdentificador());
			venda.setIdentificadorexterno(pedidovenda.getIdentificacaoexterna());
			venda.setDtvenda(new Date(System.currentTimeMillis()));
			venda.setColaborador(pedidovenda.getColaborador());
			venda.setEmpresa(pedidovenda.getEmpresa());
			venda.setPresencacompradornfe(pedidovenda.getPresencacompradornfe());
			venda.setIndicecorrecao(pedidovenda.getIndicecorrecao());
			venda.setContaboleto(pedidovenda.getConta());
			venda.setDocumentotipo(pedidovenda.getDocumentotipo());
			venda.setPrazopagamento(pedidovenda.getPrazopagamento());
			venda.setPrazomedio(pedidovenda.getPrazomedio());
			venda.setCliente(pedidovenda.getCliente());
			venda.setEndereco(pedidovenda.getEndereco());
			venda.setObservacao(pedidovenda.getObservacao());
			venda.setValorfrete(pedidovenda.getValorfrete());
			venda.setListavendamaterial(this.preencheListaVendaMaterialWSSOAP(pedidovenda, item));
			if(SinedUtil.isListEmpty(venda.getListavendamaterial())){
				throw new SinedException("N�o existe nenhum item no pedido de venda para confirma��o.");
			}
			venda.setListavendapagamento(this.preencheListaVendaPagamento(pedidovenda));
			venda.setQtdeParcelas(venda.getListavendapagamento() != null ? venda.getListavendapagamento().size() : null);
			venda.setPedidovenda(pedidovenda);
			venda.setVendasituacao(Vendasituacao.REALIZADA);
			venda.setFrete(pedidovenda.getFrete());
			venda.setTerceiro(pedidovenda.getTerceiro());
			venda.setProjeto(pedidovenda.getProjeto());
			venda.setLocalarmazenagem(pedidovenda.getLocalarmazenagem());
			venda.setPedidovendatipo(pedidovendatipo);
			venda.setDesconto(calculaDescontoPercentual(venda, pedidovenda));
			
			vendaService.calcularImposto(venda);
			vendaService.recalcularValorParcelaVenda(venda, venda.getTotalvenda());
			vendaService.saveOrUpdate(venda);
			
			Vendahistorico vendahistorico = new Vendahistorico();
			vendahistorico.setVenda(venda);
			vendahistorico.setAcao("Cria��o venda");
			vendahistorico.setObservacao("Cria��o a partir do pedido de venda: <a href=\"javascript:visualizarPedidovenda(" + 
					venda.getPedidovenda().getCdpedidovenda() + 
					");\">" + 
					venda.getPedidovenda().getCdpedidovenda() + 
					"</a> atrav�s do webservice SOAP");
			
			vendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
			vendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			
			vendahistoricoService.saveOrUpdate(vendahistorico);
			
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				vendamaterial.setTotal(vendaService.getValortotal(vendamaterial.getPreco(), 
																	vendamaterial.getQuantidade(),
																	vendamaterial.getDesconto(), vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
			}
			
			boolean naoGerarReceita = false;
			if(pedidovendatipo != null && 
					pedidovendatipo.getGeracaocontareceberEnum() != null &&
					!GeracaocontareceberEnum.APOS_VENDAREALIZADA.equals(pedidovendatipo.getGeracaocontareceberEnum())){
				naoGerarReceita = true;
			}
			
			List<Rateioitem> listarateioitem = vendaService.prepareAndSaveVendaMaterial(venda, ajusteEstoque, venda.getEmpresa(), false, null);
			venda.setConfirmacaowms(Boolean.TRUE);
			vendaService.prepareAndSaveReceitaWms(venda, listarateioitem, venda.getEmpresa(), naoGerarReceita);
			
			Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
			pedidovendahistorico.setPedidovenda(venda.getPedidovenda());
			pedidovendahistorico.setObservacao("Venda criada: <a href=\"javascript:visualizarVenda(" + 
					venda.getCdvenda() + 
					");\">" + 
					venda.getCdvenda() + 
					"</a> atrav�s do webservice SOAP");
			pedidovendahistorico.setAcao("Confirmado via SOAP");
			pedidovendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
			pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			
			pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		}
		
		List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(pedidovenda.getCdpedidovenda().toString()); 
		if(listaColeta != null && listaColeta.size() > 0){
			if(pedidovenda.getListaPedidovendamaterial() != null){
				for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
					if(pedidovendamaterial == null || pedidovendamaterial.getCdpedidovendamaterial() == null){
						continue;
					}
					
					for (br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean itBean : item) {
						if(itBean.getRecusado() != null && itBean.getRecusado()){
							if(pedidovendamaterial.getIdentificadorintegracao().equals(itBean.getIdentificador())){
								for (Coleta coleta : listaColeta) {
									List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
									for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
										if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
											for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
												if(itemServico.getPedidovendamaterial() != null && 
														itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null &&
														itemServico.getPedidovendamaterial().getCdpedidovendamaterial().equals(pedidovendamaterial.getCdpedidovendamaterial())){
													coletaMaterialService.adicionaQtdeDevolvida(coletaMaterial, itBean.getQuantidade());
													
													Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
													pedidovendahistorico.setPedidovenda(pedidovenda);
													pedidovendahistorico.setAcao("Devolvido via SOAP");
													pedidovendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
													pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
													
													pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
													break;
												}
											}
										}
//										if(coletaMaterial.getPedidovendamaterial() != null && 
//												coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null &&
//												coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial().equals(pedidovendamaterial.getCdpedidovendamaterial())){
//											coletaMaterialService.adicionaQtdeDevolvida(coletaMaterial, itBean.getQuantidade());
//											
//											Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
//											pedidovendahistorico.setPedidovenda(pedidovenda);
//											pedidovendahistorico.setAcao("Devolvido via SOAP");
//											pedidovendahistorico.setCdusuarioaltera(pedidovenda.getColaborador().getCdpessoa());
//											pedidovendahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
//											
//											pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
//										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(existeVenda(pedidovenda)){
			boolean confirmado_total = true;
			for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
				Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
				if(qtdeRestante == null || qtdeRestante > 0){
					confirmado_total = false;
					break;
				}
			}
			
			if(confirmado_total){
				this.updateConfirmacao(pedidovenda.getCdpedidovenda().toString());
			} else {
				this.updateConfirmacaoParcial(pedidovenda.getCdpedidovenda().toString());
			}
		}
		
		return true;
	}
	
	protected boolean existeVenda(Pedidovenda pedidovenda) {
		List<Venda> listaVenda = pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? vendaService.findByPedidovenda(pedidovenda) : null;
		return listaVenda != null && listaVenda.size() > 0;
	}
	
	public List<Vendamaterial> preencheListaVendaMaterialWSSOAP(Pedidovenda pedidovenda, List<br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean> listaItens) {
		List<Vendamaterial> list = new ArrayList<Vendamaterial>();
		
		if(pedidovenda.getListaPedidovendamaterial() != null){
			Vendamaterial vendamaterial;
			Double qtdeRestante = null;
			int ordem = 0;
			for (br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean item : listaItens) {
				for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
					if(pedidovendamaterial == null || 
							pedidovendamaterial.getCdpedidovendamaterial() == null || 
							pedidovendamaterial.getMaterial() == null || 
							pedidovendamaterial.getMaterial().getCdmaterial() == null){
						continue;
					}
					
					if(pedidovendamaterial.getIdentificadorintegracao().equals(item.getIdentificador())){
						vendamaterial = (preencheVendamaterialWSSOAP(pedidovenda, pedidovendamaterial, item, ordem));
						qtdeRestante = pedidovendamaterialService.getQtdeRestante(pedidovendamaterial);
						if(vendamaterial != null && qtdeRestante != null && qtdeRestante > 0){
							list.add(preencheVendamaterialWSSOAP(pedidovenda, pedidovendamaterial, item, ordem));
						}
					}
				}
				
				ordem++;
			}
		}
		
		return list;
	}
	
	protected Vendamaterial preencheVendamaterialWSSOAP(Pedidovenda pedidovenda, Pedidovendamaterial pedidovendamaterial, br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.ConfirmarPedidovendaItemBean item, int ordem){
		Vendamaterial vendamaterial = new Vendamaterial();
		
		Material material = pedidovendamaterial.getMaterial().copiaMaterial();					
		Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
		
		vendamaterial.setQuantidade(item.getQuantidade());
		vendamaterial.setOrdem(ordem);
		vendamaterial.setMaterial(material);
		vendamaterial.setIdentificadorespecifico(pedidovendamaterial.getIdentificadorespecifico());
		vendamaterial.setIdentificadorintegracao(pedidovendamaterial.getIdentificadorintegracao());
		vendamaterial.setIdentificadorinterno(pedidovendamaterial.getIdentificadorinterno());
		vendamaterial.setMaterialmestre(pedidovendamaterial.getMaterialmestre());
		vendamaterial.setPreco(pedidovendamaterial.getPreco());
		vendamaterial.setMultiplicador(pedidovendamaterial.getMultiplicador() != null ? pedidovendamaterial.getMultiplicador() : 1d);
		vendamaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
		vendamaterial.setTotal(pedidovendamaterial.getTotal());
		vendamaterial.setPedidovendamaterial(pedidovendamaterial);
		vendamaterial.setLoteestoque(pedidovendamaterial.getLoteestoque()); 
		vendamaterial.setFornecedor(pedidovendamaterial.getFornecedor());
		vendamaterial.setObservacao(pedidovendamaterial.getObservacao());
		vendamaterial.setFatorconversao(pedidovendamaterial.getFatorconversao());
		vendamaterial.setQtdereferencia(pedidovendamaterial.getQtdereferencia());
		vendamaterial.setValorcustomaterial(pedidovendamaterial.getValorcustomaterial() != null ? pedidovendamaterial.getValorcustomaterial() : material.getValorcusto());
		vendamaterial.setValorvendamaterial(pedidovendamaterial.getValorvendamaterial());
		vendamaterial.setQtdereferencia(pedidovendamaterial.getQtdereferencia() != null ? pedidovendamaterial.getQtdereferencia() : 1.0);
		vendamaterial.setAltura(pedidovendamaterial.getAltura());
		vendamaterial.setLargura(pedidovendamaterial.getLargura());
		vendamaterial.setComprimento(pedidovendamaterial.getComprimento());
		vendamaterial.setComprimentooriginal(pedidovendamaterial.getComprimentooriginal());
		vendamaterial.setMaterialcoleta(pedidovendamaterial.getMaterialcoleta());
		vendamaterial.setPercentualrepresentacao(pedidovendamaterial.getPercentualrepresentacao());
		vendamaterial.setCustooperacional(pedidovendamaterial.getCustooperacional());
		vendamaterial.setPercentualcomissaoagencia(pedidovendamaterial.getPercentualcomissaoagencia());
		vendamaterial.setPneu(pedidovendamaterial.getPneu());
		vendamaterial.setValoripi(pedidovendamaterial.getValoripi());
		vendamaterial.setIpi(pedidovendamaterial.getIpi());
		vendamaterial.setTipocobrancaipi(pedidovendamaterial.getTipocobrancaipi());
		vendamaterial.setAliquotareaisipi(pedidovendamaterial.getAliquotareaisipi());
		vendamaterial.setTipocalculoipi(pedidovendamaterial.getTipocalculoipi());
		vendamaterial.setGrupotributacao(pedidovendamaterial.getGrupotributacao());
		vendamaterial.setPeso(pedidovendamaterial.getPesoVendaOuMaterial());
		
		
		java.sql.Date dtprazoentrega = pedidovendamaterial.getDtprazoentrega();
		java.sql.Date currentDate = SinedDateUtils.currentDate();
		if(dtprazoentrega != null && SinedDateUtils.beforeIgnoreHour(dtprazoentrega, currentDate)){
			dtprazoentrega = currentDate;
		}
		vendamaterial.setPrazoentrega(dtprazoentrega);
		
		if(material != null && material.getCdmaterial() != null){
			vendamaterial.setExistematerialsimilar(materialsimilarService.existMaterialsimilar(material));
		}
		if(pedidovendamaterial.getMaterialcoleta() != null && pedidovendamaterial.getMaterialcoleta().getCdmaterial() != null){
			vendamaterial.setExistematerialsimilarColeta(materialsimilarService.existMaterialsimilar(pedidovendamaterial.getMaterialcoleta()));
		}
		if(vendamaterial.getMaterial() != null){
			vendamaterial.setIsMaterialmestregrade(materialService.existMaterialitemByMaterialgrademestre(vendamaterial.getMaterial()));
			if(vendamaterial.getIsMaterialmestregrade() == null || !vendamaterial.getIsMaterialmestregrade()) 
				vendamaterial.setIsControlegrade(materialService.isControleGrade(vendamaterial.getMaterial()));
		}
		
		Double preco = vendamaterial.getPreco();
		Double precoComDesconto = preco;
		if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0)
			precoComDesconto = precoComDesconto - vendamaterial.getDesconto().getValue().doubleValue();
		Double minimo = 0.0;
		Double maximo = 0.0;
		Double valorMaximoTabela = null;
		Double valorMinimoTabela = null;
		
		if(!vendamaterial.getUnidademedida().equals(unidademedida) && vendamaterial.getFatorconversao() != null && vendamaterial.getFatorconversao() > 0){
			if(material.getValorvendaminimo() != null)
				minimo = material.getValorvendaminimo() / vendamaterial.getFatorconversaoQtdereferencia();
			if(material.getValorvendamaximo() != null)
				maximo = material.getValorvendamaximo() / vendamaterial.getFatorconversaoQtdereferencia();
		}else{
			minimo = material.getValorvendaminimo();
			maximo = material.getValorvendamaximo();
		}
		
		Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(material, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa());
		if(materialtabelapreco == null){
			Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(material, pedidovenda.getCliente(), pedidovenda.getPrazopagamento(), pedidovenda.getPedidovendatipo(), pedidovenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
			if(materialtabelaprecoitem != null){
				if(vendamaterial.getFatorconversao() != null && vendamaterial.getFatorconversao() > 0){
					if(materialtabelaprecoitem.getValorvendaminimo() != null)
						valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo() / vendamaterial.getFatorconversaoQtdereferencia();
					if(materialtabelaprecoitem.getValorvendamaximo() != null)
						valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo() / vendamaterial.getFatorconversaoQtdereferencia();
				}else {
					valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
					valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
				}
			}
		}
		
		if(valorMinimoTabela != null){
			minimo = valorMinimoTabela;
		}
		if(valorMaximoTabela != null){
			maximo = valorMaximoTabela;
		}
		material.setValorvendaminimo(minimo);
		material.setValorvendamaximo(maximo);
		
		return vendamaterial;
	}
	
	public void cancelarPedidovendaWSSOAP(br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.CancelarPedidovendaBean bean) {
		String identificador = bean.getIdentificador();
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		Pedidovenda pedidovenda = null;
		List<Pedidovenda> listaPedidovendaIdentificadorCnpj = this.findByIdentificadorCnpj(identificador, cnpj_empresa);
		if(listaPedidovendaIdentificadorCnpj != null && listaPedidovendaIdentificadorCnpj.size() > 0){
			if(listaPedidovendaIdentificadorCnpj.size() > 1){
				throw new SinedException("Mais de um pedido encontrado com o identificador para a empresa.");
			}
			pedidovenda = listaPedidovendaIdentificadorCnpj.get(0);
		}
		if(pedidovenda == null){
			throw new SinedException("Nenhum pedido de venda encontrado.");
		}
		
		String whereIn = pedidovenda.getCdpedidovenda().toString();
		List<Pedidovenda> listaPedidovenda = this.findForCancelamento(whereIn);
		this.cancelamentoByPedidovenda(whereIn, listaPedidovenda, "Cancelado a partir do webservice SOAP");
	}
	
	/**
	* M�todo para atualizar o item do pedido de venda caso existe altera��o de banda na produ��o
	*
	* @param listaPedidovendamaterialBandaAlterada
	* @since 21/06/2016
	* @author Luiz Fernando
	*/
	public void updateBandaAlterada(List<Pedidovendamaterial> listaPedidovendamaterialBandaAlterada) {
		if(SinedUtil.isListNotEmpty(listaPedidovendamaterialBandaAlterada)){
			 pedidovendamaterialService.updateBandaAlterada(CollectionsUtil.listAndConcatenate(listaPedidovendamaterialBandaAlterada, "cdpedidovendamaterial", ","));
		}
	}
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#existeItemComGarantia(Integer cdpedidovenda)
	*
	* @param cdpedidovenda
	* @return
	* @since 22/06/2016
	* @author Luiz Fernando
	*/
	public boolean existeItemComGarantia(Integer cdpedidovenda) {
		return pedidovendaDAO.existeItemComGarantia(cdpedidovenda);
	}
	
	/**
	* M�todo que valida o pedido de garantia. 
	* 
	* O pedido de venda tem que ter o mesmo material ou similar ao do pedido de origem
	* A quantidade do material do pedido deve ser menor ou igual ao do pedido de origem
	* A garantia deve ser feita dentro do prazo de garantia
	*
	* @param pedidovenda
	* @param errors
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public String validaPedidoGarantia(Pedidovenda pedidovenda, BindException errors) {
		if(pedidovenda != null && pedidovenda.getPedidovendatipo() != null && pedidovenda.getPedidovendatipo().getCdpedidovendatipo() != null &&
				pedidovenda.getPedidovendaorigem() != null && pedidovenda.getPedidovendaorigem().getCdpedidovenda() != null &&
				SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendamaterial())){
			Pedidovendatipo pedidovendatipo = pedidovendatipoService.load(pedidovenda.getPedidovendatipo(), "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.garantia");
			if(pedidovendatipo != null && pedidovendatipo.getGarantia() != null && pedidovendatipo.getGarantia()){
				Pedidovenda pedidovendaorigem = loadForValidarGarantia(pedidovenda.getPedidovendaorigem());
				if(pedidovendaorigem != null && SinedUtil.isListNotEmpty(pedidovendaorigem.getListaPedidovendamaterial())){
					for(Pedidovendamaterial pvmOrigem : pedidovendaorigem.getListaPedidovendamaterial()){
						if(pvmOrigem.getMaterial() != null && pvmOrigem.getMaterial().getPrazogarantia() != null){
							List<Material> listaMaterial = new ArrayList<Material>();
							listaMaterial.add(pvmOrigem.getMaterial());
							
							Date dtreferencia = pvmOrigem.getDtprazoentrega() != null ? pvmOrigem.getDtprazoentrega() : pedidovendaorigem.getDtpedidovenda();
							Date dtatual = pedidovenda.getDtpedidovenda() != null ? pedidovenda.getDtpedidovenda() : new Date(System.currentTimeMillis());
							
							if(pvmOrigem.getPneu() == null){
								if(SinedUtil.isListEmpty(pvmOrigem.getMaterial().getListaMaterialsimilar())){
									for(Materialsimilar materialsimilar : pvmOrigem.getMaterial().getListaMaterialsimilar()){
										if(materialsimilar.getMaterialsimilaritem() != null){
											listaMaterial.add(materialsimilar.getMaterialsimilaritem());
										}
									}
								}
                                Integer prazoGarantia = materialService.prazoGarantiaConvertido(pvmOrigem.getMaterial().getPrazogarantia());
								boolean materialEncontrado = false;
								for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
									if(pvm.getMaterial() != null && listaMaterial.contains(pvm.getMaterial())){
										materialEncontrado = true;
										
										if(pvm.getQuantidade() != null && pvmOrigem.getQuantidade() != null &&
												pvm.getQuantidade() > pvmOrigem.getQuantidade()){
											if(errors != null) errors.reject("001", "A quantidade do produto de garantia deve ser menor ou igual ao produto do pedido origem.");
											return "A quantidade do produto de garantia deve ser menor ou igual ao produto do pedido origem.";
                                        }else if(SinedDateUtils.diferencaDias(dtatual, dtreferencia) > prazoGarantia){
											if(errors != null) errors.reject("001", "O pedido n�o pode ser aprovado porque o item " + pvmOrigem.getMaterial().getNome() + " est� fora do prazo de garantia.");
											return "O pedido n�o pode ser aprovado porque o item " + pvmOrigem.getMaterial().getNome() + " est� fora do prazo de garantia.";
										}
										break;
									}
								}
								
								if(!materialEncontrado){
									if(errors != null) errors.reject("001", "O produto de garantia deve ser igual ao produto do pedido de venda origem, ou um de seus similares.");
									return "O produto de garantia deve ser igual ao produto do pedido de venda origem, ou um de seus similares.";
								}
							} else if(pvmOrigem.getPneu() != null){
								boolean materialEncontrado = false;
                                Integer prazoGarantia = materialService.prazoGarantiaConvertido(pvmOrigem.getMaterial().getPrazogarantia());
								
								for(Pedidovendamaterial pvm : pedidovenda.getListaPedidovendamaterial()){
									if(pvm.getMaterial() != null && pvm.getMaterial().equals(pvmOrigem.getMaterial())){
										materialEncontrado = true;
										Integer dias = SinedDateUtils.diferencaDias(dtatual, dtreferencia);
										if(!pneuService.pneuIgual(pvm.getPneu(), pvmOrigem.getPneu())){
											if(errors != null) errors.reject("001", "O pneu informado n�o foi identificado no pedido de origem.");
											return "O pneu informado n�o foi identificado no pedido de origem.";
                                        }else if(SinedDateUtils.diferencaDias(dtatual, dtreferencia) > prazoGarantia){
											if(errors != null) errors.reject("001", "O pedido n�o pode ser aprovado porque o item " + pvmOrigem.getMaterial().getNome() + " est� fora do prazo de garantia.");
											return "O pedido n�o pode ser aprovado porque o item " + pvmOrigem.getMaterial().getNome() + " est� fora do prazo de garantia.";
										}
										break;
									}
								}
								
								if(!materialEncontrado){
									if(errors != null) errors.reject("001", "O produto de garantia deve ser igual ao produto do pedido de venda origem, ou um de seus similares.");
									return "O produto de garantia deve ser igual ao produto do pedido de venda origem, ou um de seus similares.";
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	protected Pedidovenda loadForValidarGarantia(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForValidarGarantia(pedidovenda);
	}
	
	public Integer[] consultarSituacaoPedidoWSSOAP(ConfirmarPedidovendaBean bean) {
		String identificador = bean.getIdentificador();
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		Pedidovenda pedidovenda = null;
		List<Pedidovenda> listaPedidovendaIdentificadorCnpj = this.findByIdentificadorCnpj(identificador, cnpj_empresa);
		if(listaPedidovendaIdentificadorCnpj != null && listaPedidovendaIdentificadorCnpj.size() > 0){
			if(listaPedidovendaIdentificadorCnpj.size() > 1){
				throw new SinedException("Mais de um pedido encontrado com o identificador para a empresa.");
			}
			pedidovenda = listaPedidovendaIdentificadorCnpj.get(0);
		}
		if(pedidovenda == null){
			throw new SinedException("Nenhum pedido de venda encontrado.");
		}
		
		List<Integer> situacoes = new ArrayList<Integer>();
		situacoes.add(pedidovenda.getPedidovendasituacao().getValue());
		
		List<Venda> listaVenda = vendaService.findByPedidovenda(pedidovenda);
		if(listaVenda != null && listaVenda.size() > 0){
			boolean faturada = false;
			boolean cancelada = false;
			for (Venda venda : listaVenda) {
				Vendasituacao vendasituacao = venda.getVendasituacao();
				if(vendasituacao.equals(Vendasituacao.FATURADA)){
					faturada = true;
				} else if(vendasituacao.equals(Vendasituacao.CANCELADA)){
					cancelada = true;
				}
			}
			
			if(faturada){
				situacoes.add(2);
			} else if(!cancelada){
				situacoes.add(5);
			} else {
				situacoes.add(4);
			}
		}
		
		return situacoes.toArray(new Integer[situacoes.size()]);
	}
	
	public List<PedidoVendaW3producaoRESTModel> findForW3Producao() {
		return findForW3Producao(null, true);
	}
	
	/**
	 * Monta a lista de pedidovenda a ser sincronizada com o W3Producao
	 * @param whereIn
	 * @param sincronizacaoInicial
	 * @return
	 */
	public List<PedidoVendaW3producaoRESTModel> findForW3Producao(String whereIn, boolean sincronizacaoInicial) {
		List<PedidoVendaW3producaoRESTModel> lista = new ArrayList<PedidoVendaW3producaoRESTModel>();
		if(sincronizacaoInicial || org.apache.commons.lang.StringUtils.isNotEmpty(whereIn)){
			for(Pedidovenda pv : pedidovendaDAO.findForW3Producao(whereIn)){
				lista.add(new PedidoVendaW3producaoRESTModel(pv));
			}	
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendaDAO#loadForValidacaoPedidoDiferenteVenda(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 20/01/2017
	* @author Luiz Fernando
	*/
	public Pedidovenda loadForValidacaoPedidoDiferenteVenda(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForValidacaoPedidoDiferenteVenda(pedidovenda);
	}
	
	public List<Pedidovenda> findForNotaRetorno(String whereIn) {
		return pedidovendaDAO.findForNotaRetorno(whereIn);
	}
	
	public Pedidovenda findForNotaRetorno(Pedidovenda pedidovenda) {
		List<Pedidovenda> lista = pedidovenda != null && pedidovenda.getCdpedidovenda() != null ? pedidovendaDAO.findForNotaRetorno(pedidovenda.getCdpedidovenda().toString()) : null;
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	public void gerarNotaFiscalRetorno(WebRequestContext request, List<Pedidovenda> listaPedidovenda) throws IOException {
		if(SinedUtil.isListNotEmpty(listaPedidovenda)){
			for(Pedidovenda pedidovenda : listaPedidovenda){
				gerarNotaFiscalRetorno(request, pedidovenda, null, true);
			}
		}
	}
	
	public boolean validaComissionamento(Pedidovenda venda, WebRequestContext request) {
		if(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_SEM_COMISSAO) && SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA; 

			Pedidovendatipo pedidovendatipo = venda.getPedidovendatipo();
			if (pedidovendatipo != null) {
				pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidovendatipo);
				if (pedidovendatipo.getGeracaocontareceberEnum() != null) {
					geracaocontareceberEnum = pedidovendatipo.getGeracaocontareceberEnum();
				}
			}
				
			if (geracaocontareceberEnum != null && GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(geracaocontareceberEnum)){
				setMaterialcolaboradorByMaterialVenda(venda);
				
				StringBuilder materiais = new StringBuilder();
				Material material;
				Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(venda.getColaborador());
				Integer qtdeParcelas = venda.getListaPedidovendapagamento().size();
				
				Double valorTotalVenda = 0d;
				for(Pedidovendamaterial pvm : venda.getListaPedidovendamaterial()) {
					valorTotalVenda += (pvm.getQuantidade() * pvm.getPreco()) - (pvm.getDesconto() != null ? pvm.getDesconto().getValue().doubleValue() : 0.0);
					if(venda.getDesconto() != null && venda.getDesconto().getValue().doubleValue() > 0){											
						valorTotalVenda = valorTotalVenda - valorDescontoProporcional(venda.getDesconto(), pvm.getPreco(), pvm.getDesconto(), pvm.getQuantidade(), venda.getListaPedidovendamaterial());
					}
					if(venda.getValorusadovalecompra() != null && venda.getValorusadovalecompra().getValue().doubleValue() > 0){											
						valorTotalVenda = valorTotalVenda - valorDescontoProporcional(venda.getValorusadovalecompra(), pvm.getPreco(), pvm.getDesconto(), pvm.getQuantidade(), venda.getListaPedidovendamaterial());
					}
				}
				
				for(Pedidovendapagamento vendapagamento : venda.getListaPedidovendapagamento()){
					for (Pedidovendamaterial vm: venda.getListaPedidovendamaterial()) {
						material = materialService.findForComissaovendaGrupomaterial(vm.getMaterial(), venda.getColaborador());
						if(material == null){
							material = materialService.findForComissaovenda(vm.getMaterial());
						}
						
						if(!comissionamentoService.existeComissaoMategrial(new ComissaoMaterialVenda(), venda, vm, material, vendapagamento, colaboradorcargo, pedidovendatipo, qtdeParcelas, new Money(), true, valorTotalVenda)){
							if(material != null && !materiais.toString().contains(material.getNome())){
								materiais.append(material.getNome()).append("<br>");
							}
						}
					}
					
				}
				
				
				if(materiais.length() > 0){
					request.addError("N�o foi poss�vel calcular a comiss�o dos itens: " + materiais.substring(0, materiais.length() -1));
					return false;
				}
			}
		}
		
		return true;
	}
	
	public boolean validaObrigatoriedadePneu(Pedidovenda venda, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(SinedUtil.isRecapagem() && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA) &&
				venda != null && SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			for(Pedidovendamaterial item : venda.getListaPedidovendamaterial()){
				if(item.getPneu() != null){
					if(!pneuService.validaObrigatoriedadePneu(item.getMaterial(), item.getPneu(), request, errors)){
						valido = false;
					}
				}
			}
		}
		
		return valido;
	}
	
	protected void setMaterialcolaboradorByMaterialVenda(Pedidovenda venda) {
		StringBuilder whereIn = new StringBuilder();
		for (Pedidovendamaterial vendamaterial: venda.getListaPedidovendamaterial()) {
			if (vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getCdmaterial() != null) {
				if (!"".equals(whereIn.toString())) whereIn.append(",");
				whereIn.append(vendamaterial.getMaterial().getCdmaterial().toString());
			}
		}
		List < Materialcolaborador > listaMaterialcolaborador = materialcolaboradorService.findMaterialcolaboradorByVenda(whereIn.toString(), venda.getColaborador());
		Set < Materialcolaborador > listaNova;
		for (Pedidovendamaterial vendamaterial: venda.getListaPedidovendamaterial()) {
			if (vendamaterial.getMaterial() != null) {
				listaNova = new ListSet <Materialcolaborador> (Materialcolaborador.class);
				if (listaMaterialcolaborador != null && !listaMaterialcolaborador.isEmpty()) {
					for (Materialcolaborador mc: listaMaterialcolaborador) {
						if (mc.getMaterial() != null && mc.getMaterial().equals(vendamaterial.getMaterial())) listaNova.add(mc);
					}
				}
				vendamaterial.getMaterial().setListaMaterialcolaborador(listaNova);
			}
		}
	}
	
	public void gerarNotaFiscalRetorno(WebRequestContext request, Pedidovenda pedidovenda, Venda venda, boolean geracaoAutomarica) throws IOException {
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			if(SinedUtil.isListNotEmpty(pedidovenda.getListaColeta())){
				Set<Presencacompradornfe> listaPresencacompradornfe = new HashSet<Presencacompradornfe>();
				if (pedidovenda.getPedidovendatipo()!=null && pedidovenda.getPedidovendatipo().getPresencacompradornfecoleta()!=null){
					listaPresencacompradornfe.add(pedidovenda.getPedidovendatipo().getPresencacompradornfecoleta());
				}
				
				Tipooperacaonota tipooperacaonota = "ENTRADA".equals(request.getParameter("tipooperacaoNota")) ? Tipooperacaonota.ENTRADA : Tipooperacaonota.SAIDA;  
				String whereInColeta = CollectionsUtil.listAndConcatenate(pedidovenda.getListaColeta(), "cdcoleta", ",");
				if (coletaService.isColetaPossuiNotaTodosItensSemSaldo(whereInColeta, tipooperacaonota) && geracaoAutomarica){
					return;
				}
				
				Notafiscalproduto nota = notafiscalprodutoService.gerarNotaFiscalRetorno(
																		request, 
																		null, 
																		whereInColeta, 
																		venda == null && pedidovenda != null ? pedidovenda.getCdpedidovenda().toString() : null, 
																		venda != null ? venda.getCdvenda().toString() : null, 
																		false, 
																		listaPresencacompradornfe, 
																		geracaoAutomarica);
				
				if(SinedUtil.isListEmpty(nota.getListaItens())){
					return;
				}
				
				nota.setTipooperacaonota(tipooperacaonota);
				
				if (listaPresencacompradornfe.size()==1){
					nota.setPresencacompradornfe(new ListSet<Presencacompradornfe>(Presencacompradornfe.class, listaPresencacompradornfe).get(0));
				}
				
				List<Coleta> listaColeta = org.apache.commons.lang.StringUtils.isNotBlank(whereInColeta) ? coletaService.loadForGerarNotafiscalproduto(whereInColeta) : new ArrayList<Coleta>();

				if(SinedUtil.isListNotEmpty(listaColeta)){
					notafiscalprodutoService.loadInfoForTemplateInfoContribuinte(nota);
					String informacoesContribuinte = notafiscalprodutoService.montaInformacoesContribuinteTemplate(null, listaColeta, nota);
					if(org.apache.commons.lang.StringUtils.isNotBlank(informacoesContribuinte)){
						nota.setInfoadicionalcontrib(informacoesContribuinte);
					}
				}
				
				String[] ids = whereInColeta.split(",");
				String link = "/w3erp/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=";
				String descricao = "Coleta: ";
				
				notafiscalprodutoService.setObservacaoHistoricoNota(nota, ids, link, descricao, false);
				
				NotaHistorico nh = nota.getListaNotaHistorico().get(0); 
				nh.setNotaStatus(nota.getNotaStatus());
				nh.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				nh.setDtaltera(new Timestamp(System.currentTimeMillis()));
				
				if(venda == null && pedidovenda != null){
					ids = new String[]{pedidovenda.getCdpedidovenda().toString()};
					link = "/w3erp"+request.getFirstRequestUrl()+"?ACAO=consultar&cdpedidovenda=";//mudou
					descricao = "Pedido de Venda: ";
				}else if(venda != null){
					ids = new String[]{venda.getCdvenda().toString()};
					link = "/w3erp"+request.getFirstRequestUrl()+"?ACAO=consultar&cdvenda=";//mudou
					descricao = "Venda: ";
				} 
				notafiscalprodutoService.setObservacaoHistoricoNota(nota, ids, link, descricao, true);
				
				notafiscalprodutoService.verificarInfAproveitamentocredito(nota);
				notafiscalprodutoService.saveOrUpdate(nota);
				notafiscalprodutoService.updateNumeroNFProduto(nota);
				
				NotafiscalprodutoColeta nfpc;
				for(String cdcoleta : whereInColeta.split(",")){
					nfpc = new NotafiscalprodutoColeta(Integer.parseInt(cdcoleta), nota.getCdNota(), false);
					notafiscalprodutoColetaService.saveOrUpdate(nfpc);
					
					coletahistoricoService.gerarHistoricoNotaRetorno(nota, new Coleta(Integer.parseInt(cdcoleta)));
				}
				
				if(venda == null && pedidovenda != null){
					notafiscalprodutoretornopedidovendaService.saveOrUpdate(new Notafiscalprodutoretornopedidovenda(pedidovenda.getCdpedidovenda(), nota.getCdNota()));
					pedidovendahistoricoService.gerarHistoricoNotaRetorno(nota, pedidovenda);
				}else if(venda != null){
					notafiscalprodutoretornovendaService.saveOrUpdate(new Notafiscalprodutoretornovenda(venda.getCdvenda(), nota.getCdNota()));
					vendahistoricoService.gerarHistoricoNotaRetorno(nota, venda);
				}
			}
		}
	}
	
	public  List<Pedidovenda> findPedidoVendaGarantiaForAutocomplete(String param) {
		return pedidovendaDAO.findPedidoVendaGarantiaForAutocomplete(param);
	}
	
	
	public Pedidovenda findForSelecaoservicogarantido(Pedidovenda pedidovenda, String whereInPneusJaSelecionados){
		return pedidovendaDAO.findForSelecaoservicogarantido(pedidovenda, whereInPneusJaSelecionados);
	}
	
	public ModelAndView abrePopupSelecaoServicogarantido(WebRequestContext request, Pedidovenda pedidovenda){
		String whereInPneusJaSelecionados = request.getParameter("whereInPneus");
		pedidovenda = this.findForSelecaoservicogarantido(pedidovenda, whereInPneusJaSelecionados);
		return new ModelAndView("direct:/crud/popup/popupSelecaoServicoGarantido", "bean", pedidovenda);
	}
	
	public void buscarTabelaPrecoVenda(Pedidovenda venda) {
		if(venda != null && Hibernate.isInitialized(venda.getListaPedidovendamaterial()) && SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
			Material materialmestre;
			for(Pedidovendamaterial vendamaterial : venda.getListaPedidovendamaterial()){
				if (vendamaterial.getIdentificadorespecifico() != null && !vendamaterial.getIdentificadorespecifico().equals("") && "TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO))) {
					String identificador = vendamaterial.getIdentificadorespecifico();
					Materialtabelapreco materialtabelapreco_aux = materialtabelaprecoService.getTabelaPrecoAtual(
					SinedDateUtils.castUtilDateForSqlDate(venda.getDtpedidovenda()), null, venda.getCliente(), venda.getPrazopagamento(), venda.getPedidovendatipo(), venda.getEmpresa(), true, null);
					if (materialtabelapreco_aux == null) {
						materialtabelapreco_aux = materialtabelaprecoService.getTabelaPrecoAtual(
								SinedDateUtils.castUtilDateForSqlDate(venda.getDtpedidovenda()), null, venda.getCliente(), venda.getPrazopagamento(), venda.getPedidovendatipo(), venda.getEmpresa(), true, null);
					}
					if (materialtabelapreco_aux != null) {
						List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = materialtabelaprecoitemService.findByMaterialtabelapreco(materialtabelapreco_aux);
						for (Materialtabelaprecoitem materialtabelaprecoitem: listaMaterialtabelaprecoitem) {
							if (materialtabelaprecoitem.getIdentificadorespecifico() != null && materialtabelaprecoitem.getIdentificadorespecifico().equals(identificador)) {
								vendamaterial.setMaterialtabelapreco(materialtabelaprecoitem.getMaterialtabelapreco());
								vendamaterial.setMaterialtabelaprecoitem(materialtabelaprecoitem);
								vendamaterial.setComissionamento(materialtabelaprecoitem.getComissionamentoItemOuComissionamentoTabela(Comissionamentotipo.VENDA));
								break;
							}
						}
					}
				} else {
					materialmestre = vendamaterial.getMaterialmestre() != null ? materialService.load(vendamaterial.getMaterialmestre(), "material.cdmaterial, material.kitflexivel") : null;
					boolean kitflexivel = materialmestre != null && materialmestre.getKitflexivel() != null && materialmestre.getKitflexivel(); 
					Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(vendamaterial.getMaterial(), venda.getCliente(), venda.getPrazopagamento(), 
							venda.getPedidovendatipo(), venda.getEmpresa(), (kitflexivel ? vendamaterial.getMaterialmestre() : null), null);
					if (materialtabelapreco != null) {
						vendamaterial.setMaterialtabelapreco(materialtabelapreco);
						vendamaterial.setMaterialtabelaprecoitem(null);
						vendamaterial.setComissionamento(materialtabelapreco.getComissionamento());
					} else {
						Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(
						vendamaterial.getMaterial(), venda.getCliente(), venda.getPrazopagamento(), venda.getPedidovendatipo(), venda.getEmpresa(), vendamaterial.getUnidademedida());
						if (materialtabelaprecoitem != null) {
							vendamaterial.setMaterialtabelapreco(materialtabelaprecoitem.getMaterialtabelapreco());
							vendamaterial.setMaterialtabelaprecoitem(materialtabelaprecoitem);
							vendamaterial.setComissionamento(materialtabelaprecoitem.getComissionamentoItemOuComissionamentoTabela(Comissionamentotipo.VENDA));
						}
					}
				}
			}
		}
		
	}
	
	public Pedidovenda findForGarantiareforma(Pedidovenda pedidovenda){
		return pedidovendaDAO.findForGarantiareforma(pedidovenda);
	}
	
	public Pedidovenda findByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial){
		return pedidovendaDAO.findByPedidovendamaterial(pedidovendamaterial);
	}

	public Boolean isProducaoEmAndamento(String whereInPedido){
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByPedidovenda(whereInPedido);
		if(listaProducaoagenda != null && listaProducaoagenda.size() > 0){
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				if(producaoagenda.getProducaoagendasituacao() != null && 
						!producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA) &&
						!producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.CANCELADA) &&
						producaoagenda.getPedidovenda() != null &&
						producaoagenda.getPedidovenda().getPedidovendasituacao() != null){
					return true;
				}
			}
		}
		return false;
	}
	
	public Boolean isProducaoEmEspera(String whereInPedido){
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByPedidovenda(whereInPedido);
		if(listaProducaoagenda != null && listaProducaoagenda.size() > 0){
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				if(producaoagenda.getProducaoagendasituacao() != null && 
						producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA)){
					return true;
				}
			}
		}
		return false;
	}
	
	public String montaWhereinDocumentotipoJaUsado(Pedidovenda beanJaCarregado){
		List<String> listaDocumentotipoWherein = new ArrayList<String>();
		if(beanJaCarregado != null && beanJaCarregado.getCdpedidovenda() != null){
			
			if(beanJaCarregado.getDocumentotipo() != null){
				listaDocumentotipoWherein.add(beanJaCarregado.getDocumentotipo().getCddocumentotipo().toString());
			}
			
			for(Pedidovendapagamento vendapag: beanJaCarregado.getListaPedidovendapagamento()){
				if(vendapag.getDocumentotipo() != null && vendapag.getDocumentotipo().getCddocumentotipo() != null){
					listaDocumentotipoWherein.add(vendapag.getDocumentotipo().getCddocumentotipo().toString());
				}
			}
		}
		return listaDocumentotipoWherein.isEmpty()? null: CollectionsUtil.concatenate(listaDocumentotipoWherein, ",");
	}
	
	public Pedidovenda loadWithAllDocumentotipo(String cdpedidovenda){
		return pedidovendaDAO.loadWithAllDocumentotipo(cdpedidovenda);
	}
	
	public boolean validarEstoqueForAprovacao(WebRequestContext request, Pedidovenda pedidovenda, List<Pedidovendamaterial> listaPedidovendamaterial){
		List<Material> listaProdutoSemSaldoDisponivel = this.getListaMateriaisSemSaldoDisponivel(pedidovenda, listaPedidovendamaterial);
	
		if (SinedUtil.isListNotEmpty(listaProdutoSemSaldoDisponivel)){
			for(Material mat: listaProdutoSemSaldoDisponivel){
				request.addError("O pedido n�o pode ser aprovado porque o item "+mat.getNome()+" n�o possui quantidade suficiente em estoque.");
			}
			return false;
		}
		return true;
	}
	
	public List<Material> getListaMateriaisSemSaldoDisponivel(Pedidovenda pedidovenda, List<Pedidovendamaterial> listaPedidovendamaterial){
		List<Material> listaRetorno = new ArrayList<Material>();
		for (Pedidovendamaterial pedidovendamaterial: listaPedidovendamaterial) {
			if (pedidovendamaterial.getMaterial()!=null && Boolean.TRUE.equals(pedidovendamaterial.getMaterial().getProduto())){
				Double quantidade = pedidovendamaterial.getQuantidade();
				if (quantidade==null){
					quantidade = 0d;
				}
				
				Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(pedidovendamaterial.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pedidovendamaterial.getLoteestoque());
				if (entrada==null){
					entrada = 0d;
				}
				
				Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(pedidovendamaterial.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovenda.getProjeto(), pedidovendamaterial.getLoteestoque());
				if (saida==null){
					saida = 0d;
				}
				
				Double qtdereservada = pedidovendamaterialService.getQtdeReservada(pedidovendamaterial.getMaterial(), pedidovenda.getEmpresa(), pedidovenda.getLocalarmazenagem(), pedidovendamaterial.getLoteestoque(), pedidovenda);
				if (qtdereservada==null){
					qtdereservada = 0d;
				}
				
				Double qtddisponivel = entrada - saida;
				
				if (((qtddisponivel - qtdereservada) - quantidade) < 0D){
					listaRetorno.add(pedidovendamaterial.getMaterial());
				}
			}
		}
		return listaRetorno;
	}
	
	protected boolean isGerarReservaForPedido(Pedidovendatipo pedidovendatipo){
		return pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getReserva()) && TipoReservaEnum.PEDIDO_VENDA.equals(pedidovendatipo.getReservarApartir()) && !BaixaestoqueEnum.NAO_BAIXAR.equals(pedidovendatipo.getBaixaestoqueEnum());
	}
	
	public void gerarReservaAoSalvar(Pedidovendatipo pedidovendatipo, Pedidovenda pedidovenda){
		if(this.isGerarReservaForPedido(pedidovendatipo)){
			for (Pedidovendamaterial pedidovendamaterial: pedidovenda.getListaPedidovendamaterial()) {
				if (!this.verificarPermitirMaterialMestreVenda(pedidovendamaterial)) {
					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(pedidovendamaterial.getMaterial().getCdmaterial());
					if(material != null && SinedUtil.isListNotEmpty(material.getListaMaterialrelacionado())){
						for(Materialrelacionado mr: material.getListaMaterialrelacionado()){
							if(Boolean.TRUE.equals(mr.getMaterialpromocao().getServico())){
								continue;
							}
							Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(pedidovendamaterial.getMaterial(),
																						pedidovendamaterial.getUnidademedida(),
																						pedidovendamaterial.getQuantidade()) * mr.getQuantidade();
							Reserva reserva = reservaService.loadByPedidovendamaterial(pedidovendamaterial.getCdpedidovendamaterial(), mr.getMaterialpromocao());
							if(reserva != null){
								quantidade = quantidade-reserva.getQuantidade();
							}
							reservaService.createUpdateReserva(pedidovendamaterial, pedidovenda, mr.getMaterialpromocao(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovendamaterial.getLoteestoque(), quantidade);						
						}
	
					}else{
						if(Boolean.TRUE.equals(material.getServico())){
							continue;
						}
						Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(pedidovendamaterial.getMaterial(), pedidovendamaterial.getUnidademedida(), pedidovendamaterial.getQuantidade());
						Reserva reserva = reservaService.loadByPedidovendamaterial(pedidovendamaterial.getCdpedidovendamaterial(), pedidovendamaterial.getMaterial());
						if(reserva != null){
							quantidade = quantidade-reserva.getQuantidade();
						}
						reservaService.createUpdateReserva(pedidovendamaterial, pedidovenda, pedidovendamaterial.getMaterial(), pedidovenda.getLocalarmazenagem(), pedidovenda.getEmpresa(), pedidovendamaterial.getLoteestoque(), quantidade);					
					}
				}
			}
		}
	}
	
	public boolean existeVariasVendas(Pedidovenda pedidovenda) {
		return pedidovendaDAO.existeVariasVendas(pedidovenda);
	}
	
	public void executaCancelamentoReservaOSV(String whereIn) {
		if(org.apache.commons.lang.StringUtils.isNotBlank(whereIn)){
			ordemservicoveterinariaService.updateItemFaturadoAposPedidoCancelado(whereIn);
			
			List<Pedidovendaordemservicoveterinaria> listaPedidovendaordemservicoveterinaria = pedidovendaordemservicoveterinariaService.findForReservarAposCancelamentoPedido(whereIn);
			if(SinedUtil.isListNotEmpty(listaPedidovendaordemservicoveterinaria) && parametrogeralService.getBoolean(Parametrogeral.RESERVAR_VETERINARIA_OS)){
				for(Pedidovendaordemservicoveterinaria beanPvOsv : listaPedidovendaordemservicoveterinaria){
					if(beanPvOsv.getOrdemservicoveterinaria() != null && beanPvOsv.getOrdemservicoveterinaria().getAtividadetipoveterinaria() != null &&
							beanPvOsv.getOrdemservicoveterinaria().getAtividadetipoveterinaria().getReserva() != null && 
							beanPvOsv.getOrdemservicoveterinaria().getAtividadetipoveterinaria().getReserva() &&
							beanPvOsv.getPedidovenda() != null && Pedidovendasituacao.CANCELADO.equals(beanPvOsv.getPedidovenda().getPedidovendasituacao()) &&
							beanPvOsv.getOrdemservicoveterinaria().getAtividadetipoveterinaria().getLocalarmazenagem() != null &&
							beanPvOsv.getOrdemservicoveterinaria().getRequisicaoestado() != null &&
							!Requisicaoestado.CANCELADA.equals(beanPvOsv.getOrdemservicoveterinaria().getRequisicaoestado().getCdrequisicaoestado()) &&
							SinedUtil.isListNotEmpty(beanPvOsv.getOrdemservicoveterinaria().getListaOrdemservicoveterinariamaterial())){
						for(Ordemservicoveterinariamaterial item : beanPvOsv.getOrdemservicoveterinaria().getListaOrdemservicoveterinariamaterial()){
							if(item.getMaterial() != null && !Boolean.TRUE.equals(item.getMaterial().getServico())){
								reservaService.createUpdateReserva(item, beanPvOsv.getOrdemservicoveterinaria(), beanPvOsv.getOrdemservicoveterinaria().getAtividadetipoveterinaria().getLocalarmazenagem(), item.getQtdeusada());
							}
						}
					}
				}
			}
		}
	}
		
	
	public List<Pedidovenda> recuperarVendasPorMaterial(WebRequestContext request, String codMaterial) {
		int cod = Integer.parseInt(codMaterial);
		List<Pedidovenda> list = pedidovendaDAO.findByCodMaterial(cod);
		return list;
	
	}
	public boolean isIdentificadorExternoRepetido(Pedidovenda form) {
		if(form.getIdentificacaoexterna() !=null){
			List<Pedidovenda> isRepetido = pedidovendaDAO.identificadorExternoRepetido(form.getCdpedidovenda(),form.getIdentificacaoexterna());
			if(isRepetido != null && isRepetido.size()>0){
				return true;
			}
		}
		return false;
	}
	public boolean isOrigemOrcamento(Vendaorcamento bean) {
		if(pedidovendaDAO.isOrigemOrcamento(bean.getCdvendaorcamento())){
			return true;
		}
		return false;
	}
	public Money tatalSemContaReceber(Cliente cliente) {
		Money  total = new Money();
		List<Pedidovendapagamento> pagamentos = pedidovendapagamentoService.buscarPorVendaDoClienteSemContaReceber(cliente);
		if(pagamentos!= null && !pagamentos.isEmpty()){
			List<Integer> listaCdpedidovenda = new ArrayList<Integer>();
			Money valorParcial;
			for (Pedidovendapagamento pagamento : pagamentos) {
				valorParcial = new Money();
				if(pagamento.getPedidovenda() != null && !listaCdpedidovenda.contains(pagamento.getPedidovenda().getCdpedidovenda()) && SinedUtil.isListNotEmpty(pagamento.getPedidovenda().getListaVenda()) ){
					for(Venda venda : pagamento.getPedidovenda().getListaVenda()){
						if(SinedUtil.isListNotEmpty(venda.getListavendapagamento())){
							for(Vendapagamento vendapagamento : venda.getListavendapagamento()){
								if(vendapagamento.getValororiginal() != null){
									valorParcial = valorParcial.add(vendapagamento.getValororiginal());
								}
							}
						}
					}
					listaCdpedidovenda.add(pagamento.getPedidovenda().getCdpedidovenda());
				}
				 
				Money valor = pagamento.getValororiginal();
				if(valorParcial != null && valorParcial.getValue().doubleValue() > 0){
					valor = valor.subtract(valorParcial);
				}
				total = total.add(valor);
			}
		}
		return total;
	}
	
	
	public Double retornarQtdDisponivel(Double qtde,Double entrada,Double saida){
		Double qtddisponivel;
		if(qtde != null){
			qtddisponivel = qtde;
		} else {
			qtddisponivel = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
		}
		return qtddisponivel;
	}
	
	public Double retornaReservaKitMaterial(Material material,  Empresa empresa,Localarmazenagem localarmazenagem){
		Double qtdereservadaItemKit = pedidovendamaterialService.getQtdeReservada(material, empresa, localarmazenagem, null);
		if(qtdereservadaItemKit == null){
			qtdereservadaItemKit = 0d;
		}
		return qtdereservadaItemKit;
	}
	
	public Double retornaSaidaMaterial(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto){
		Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(material,localarmazenagem,empresa, true, projeto);
		if (saida == null){
			saida = 0d;
		}
		return saida;
	}
	
	public Double retornaEntradaMaterial(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto){
		Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(material,localarmazenagem,empresa, true, projeto);
		if (entrada == null){
			entrada = 0d;
		}
		return entrada;
	}
	
	public MaterialQtdRESTModel buscarQuantidadeAPP(Venda venda){
		Double entrada = 0d;
		Double saida = 0d;
		Double qtdreservada = null;
		Double qtddisponivel = 0d;
		Double qtde = null;
		Projeto projeto = venda.getProjeto();
		Material material = materialService.loadWithCodFlex(venda.getMaterial().getCdmaterial());
		Materialclasse classeMaterial = materialService.getMaterialClasse(material, true);
		
		if(classeMaterial != null){
			if(Boolean.FALSE.equals(material.getProducao()) && 
					!Materialclasse.PRODUTO.getCdmaterialclasse().equals(classeMaterial.getCdmaterialclasse())){
				return null;
			}
		}
		if (venda.getMaterial() != null){
			if(venda.getMaterial().getVendapromocional() != null && venda.getMaterial().getVendapromocional()){
				Venda vendaaux = new Venda();
				venda.setCodigo(venda.getCodigo());
				venda.setLocalarmazenagem(venda.getLocalarmazenagem());
				venda.setEmpresa(venda.getEmpresa());
				material = materialService.loadMaterialPromocionalComMaterialrelacionado(Integer.parseInt(venda.getCodigo()));
				
				
				if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
					Pedidovenda pedidovenda = new Pedidovenda();
					pedidovenda.setEmpresa(vendaaux.getEmpresa());
					pedidovenda.setLocalarmazenagem(vendaaux.getLocalarmazenagem());
					
					Double qtdereservadaItemKit = null;
					qtdreservada = 0d;
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null && !Boolean.TRUE.equals(materialrelacionado.getMaterialpromocao().getServico())){	
							vendaaux.setMaterial(materialrelacionado.getMaterialpromocao());
							vendaaux.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaaux.setEmpresa(venda.getEmpresa());
							
							entrada = retornaEntradaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							saida = retornaSaidaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							qtdereservadaItemKit = retornaReservaKitMaterial(materialrelacionado.getMaterialpromocao(), vendaaux.getEmpresa(), vendaaux.getLocalarmazenagem());
			
							
							qtddisponivel = (entrada-saida-qtdereservadaItemKit)/materialrelacionado.getQuantidade();
							if(qtde == null){
								qtde = qtddisponivel;
							}else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
						}
					}
				}
			}else if(venda.getMaterial().getKitflexivel() != null && venda.getMaterial().getKitflexivel()){
				Venda vendaaux = new Venda();
				venda.setCodigo(venda.getCodigo());
				venda.setLocalarmazenagem(venda.getLocalarmazenagem());
				venda.setEmpresa(venda.getEmpresa());
				material = materialService.loadKitFlexivel(venda.getMaterial());
				
				if(material.getListaMaterialkitflexivel() != null && !material.getListaMaterialkitflexivel().isEmpty()){
					for(Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()){
						if(materialkitflexivel.getMaterialkit() != null && materialkitflexivel.getQuantidade() != 0d){	
							vendaaux.setMaterial(materialkitflexivel.getMaterialkit());
							vendaaux.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaaux.setEmpresa(venda.getEmpresa());

							entrada = retornaEntradaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							saida = retornaSaidaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							
							qtddisponivel = (entrada-saida)/materialkitflexivel.getQuantidade();
							if(qtde == null)
								qtde = qtddisponivel;
							else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
						}
					}
				}
			}else {
				if(venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()){
					material = materialService.loadMaterialComMaterialproducao(venda.getMaterial());
				}
		
				entrada = retornaEntradaMaterial(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), projeto);
				saida = retornaSaidaMaterial(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), projeto);
		
				if(venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()){
					qtde = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
					if(qtde <= 0){
						Material materialP = materialService.loadMaterialComMaterialproducao(venda.getMaterial());
						if(materialP != null){
							Venda vendaP = new Venda();
							vendaP.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaP.setEmpresa(venda.getEmpresa());
							vendaP.setProjeto(venda.getProjeto());
							qtde = vendaService.getQtdeDisponivelMaterialproducao(materialP, vendaP);
						}
					}
				}
			}
		}
		if(qtdreservada == null){
			qtdreservada = pedidovendamaterialService.getQtdeReservada(material, venda.getEmpresa(), venda.getLocalarmazenagem());
		}
		qtddisponivel = retornarQtdDisponivel(qtde,entrada,saida);
		
		MaterialQtdRESTModel rest = new MaterialQtdRESTModel();
		rest.setCdmaterial(material.getCdmaterial());
		rest.setQtdEstoque(qtddisponivel);
		rest.setQtdReservada(qtdreservada);
		
		return rest;
	}
	
	public Pedidovenda loadForRecalcularcomissao(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForRecalcularcomissao(pedidovenda);
	}
	
	public boolean existeDocumento(Pedidovenda pedidovenda) {
		if (pedidovenda != null && SinedUtil.isListNotEmpty(pedidovenda.getListaPedidovendapagamento())) {
			for (Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()) {
				if (pedidovendapagamento.getDocumento() != null) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public Pedidovenda loadPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadPedidovenda(pedidovenda);
	}
	
	public Boolean verificarPermitirMaterialMestreVenda(List<Pedidovendamaterial> listaPedidovendaMaterial) {
		for (Pedidovendamaterial pedidovendamaterial : listaPedidovendaMaterial) {
			if (verificarPermitirMaterialMestreVenda(pedidovendamaterial)) {
				return Boolean.TRUE;
			}
		}
		
		return Boolean.FALSE;
	}
	
	public Boolean verificarPermitirMaterialMestreVenda(Pedidovendamaterial pedidovendamaterial) {
		if (pedidovendamaterial.getMaterial() != null && materialService.isMaterialMestre(pedidovendamaterial.getMaterial())) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	public List<Pedidovendamaterial> buscaListaMaterial(Pedidovenda pedidovenda) {
		pedidovenda = pedidovendaDAO.carregarListaMaterial(pedidovenda);
//		if(pedidovenda.getPedidovendatipo() == null || !GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidovenda.getPedidovendatipo().getGeracaocontareceberEnum())){
//			return null;
//		}
		return pedidovenda.getListaPedidovendamaterial();
	}
	public Boolean verificarPermitirMaterialMestreVenda(String itens) {
		for (String id : itens.split(",")) {
			Pedidovenda pedidovenda = pedidovendaDAO.findCdMaterialById(id);
			
			if (pedidovenda != null & pedidovenda.getListaPedidovendamaterial() != null) {
				for (Pedidovendamaterial m : pedidovenda.getListaPedidovendamaterial()) {
					if (m.getMaterial() != null && materialService.isMaterialMestre(m.getMaterial())) {
						return Boolean.TRUE;
					}
				}
			}
		}
		
		return Boolean.FALSE;
	}
	
	
	public void updateSituacaoPendenciaNegociacao(Pedidovenda pedidovenda, SituacaoPendencia situacaoPendencia) {
		pedidovendaDAO.updateSituacaoPendenciaNegociacao(pedidovenda, situacaoPendencia);
	}
	
	public Pedidovenda carregarInfoPedidoSituacao(Pedidovenda pedidovenda) {
		return pedidovendaDAO.carregarInfoPedidoSituacao(pedidovenda);
	}
	
	public Boolean verificarPedidoVendaTipoPermiteMaterialMestreVenda(String itens) {
		if (org.apache.commons.lang.StringUtils.isNotEmpty(itens)) {
			for (String id : itens.split(",")) {
				Pedidovenda pedidovenda = this.load(new Pedidovenda(Integer.parseInt(id)), "pedidovenda.pedidovendatipo");
				return pedidovendatipoService.verificarPedidoVendaTipoPermiteMaterialMestreVenda(pedidovenda.getPedidovendatipo());
			}
		}
		return Boolean.FALSE;
	}
	
	public void finalizarPedidoParcial(Pedidovenda pv) {
		pedidovendaDAO.updateConfirmacao(pv.getCdpedidovenda().toString());
		Pedidovendahistorico pvHistorico = new Pedidovendahistorico();
		pvHistorico.setAcao("Finalizado");
		pvHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		pvHistorico.setObservacao("O pedido foi confirmado atrav�s da a��o Finalizar Pedido Confirmado Parcial.");
		pvHistorico.setPedidovenda(pv);
		pedidovendahistoricoService.saveOrUpdate(pvHistorico);
		removerRevervas(pv);
	}
	
	public void removerRevervas(Pedidovenda pv){
		List<Reserva> listaReserva = reservaService.findByPedidovenda(pv, null);
		if(SinedUtil.isListNotEmpty(listaReserva)){
			for (Reserva reserva : listaReserva) {
				reservaService.desfazerReserva(reserva,reserva.getQuantidade());
			}
		}	
	}
	
	public void negociarPendencia(WebRequestContext request, Pedidovenda pv) {
		if(Boolean.TRUE.equals(request.getAttribute("gerarVale"))){
			Valecompra vale = new Valecompra();
			vale.setCliente(pv.getCliente());
			vale.setData(pv.getDtGerarValeCompra());
			vale.setIdentificacao(pv.getDescricaoValeCompra());
			vale.setTipooperacao(Tipooperacao.TIPO_CREDITO);
			vale.setValor(pv.getValorPendentes());
			valecompraService.saveOrUpdate(vale);
		}else {
			List<Pedidovendapagamento> listaPag = pv.getListaPedidovendapagamento();
			pv.setListaPedidoVendaNegociacao(new ArrayList<PedidoVendaNegociacao>());  
			for (Pedidovendapagamento pag : listaPag) {
				PedidoVendaNegociacao negociacao = new PedidoVendaNegociacao();
				negociacao.setAgencia(pag.getAgencia());
				negociacao.setBanco(pag.getBanco());
				if(pv.getConta() != null ){
					negociacao.setConta(pv.getConta().getCdconta());
				}else {
					negociacao.setConta(pag.getConta());
				}
				negociacao.setCpfcnpj(pag.getCpfcnpj());
				negociacao.setDataParcela(pag.getDataparcela());
				negociacao.setDocumento(pag.getDocumento());
				negociacao.setDocumentoAntecipacao(pag.getDocumentoantecipacao());
				negociacao.setDocumentoTipo(pag.getDocumentotipo());
				negociacao.setEmitente(pag.getEmitente());
				negociacao.setNumero(pag.getNumero());
				negociacao.setValorJuros(pag.getValorjuros());
				negociacao.setValorOriginal(pag.getValororiginal());	
				negociacao.setPedidovenda(pv);
				pv.getListaPedidoVendaNegociacao().add(negociacao);
			}	
			for (Pedidovendamaterial pedidovendamaterial : pv.getListaPedidovendamaterial()) {
				pedidovendamaterial.setTotal(vendaService.getValortotal(pedidovendamaterial.getPreco(), 
						pedidovendamaterial.getQuantidade(),
						pedidovendamaterial.getDesconto(), 
						pedidovendamaterial.getMultiplicador(),
						pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro()));	
			}
			
			List<Rateioitem> listaRateioItem = prepareAndSaveVendaMaterial(pv, false, pv.getEmpresa(), false, null);
			gerarContaNegociacao(pv,listaRateioItem,pv.getEmpresa());
			registrarHistoricoParaAcoesDeNegociacao(pv,"Finalizado","O pedido foi negociado atrav�s da a��o negociar pendencia.");
			removerRevervas(pv);
			List<Venda> listaVenda = vendaService.findByPedidovenda(pv);
			if(SinedUtil.isListNotEmpty(listaVenda)){
				for (Venda venda : listaVenda) {
					verificaCancelamentoOuRecalculoComissionamento(venda,true);
				}
			}
		}
	}
	
	protected List<Integer> gerarContaNegociacao(Pedidovenda pedidovenda, List<Rateioitem> listarateioitem, Empresa empresa){
		List<Integer> listaDocumentosGerados = new ArrayList<Integer>();
		Documento documento;
		Documentohistorico documentohistorico;
		Set<Documentohistorico> listaDocumentohistorico = new ListSet<Documentohistorico>(Documentohistorico.class);
		Parcelamento parcelamento = new Parcelamento();
		
		rateioitemService.agruparItensRateio(listarateioitem);
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(listarateioitem);
		rateioService.saveOrUpdate(rateio);
		
		Integer iteracoes = getQtdeIteracoesPgamento(pedidovenda);
		if(pedidovenda.getPrazopagamento() != null){
			parcelamento.setPrazopagamento(pedidovenda.getPrazopagamento());
			parcelamento.setIteracoes(iteracoes);
			parcelamentoService.saveOrUpdate(parcelamento);
		}
		Parcela parcela;
		int i = 1;
		
		String msg = "";
		StringBuilder msg2 = new StringBuilder();
		StringBuilder msg3 = new StringBuilder();
		String preposicao = "";
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				
		Rateio rateioParcelas;
		Documentotipo documentotipo;
		Taxa taxa;
		Taxaitem taxaitem;
		Set<Taxaitem> listaTaxaitem;
		int numParcela = 1;
		Documentoacao documentoacao = Documentoacao.PREVISTA;
		
		if (empresa!=null && empresa.getCdpessoa()!=null){
			empresa = empresaService.loadForEntrada(empresa);
		}
		
		for (PedidoVendaNegociacao pedidoVendaNegociacao : pedidovenda.getListaPedidoVendaNegociacao()) {
			msg = "";
			msg2 = new StringBuilder();
			msg3 = new StringBuilder();
			preposicao = "";
			
			taxa = new Taxa();
			listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
			
			Conta contaboleto = null;
			documentotipo = documentotipoService.load(pedidoVendaNegociacao.getDocumentoTipo());
	
			
			if (documentotipo.getBoleto() != null && documentotipo.getBoleto()) {
				contaboleto = pedidovenda.getConta();
			} 
			
			Endereco enderecoPessoa = null;
			if(empresa != null && empresa.getCdpessoa() != null){
				enderecoPessoa = enderecoService.getEnderecoPrincipalPessoa(empresa);
			}
			
			pedidoVendaNegociacao.setDocumentoTipo(documentotipo);
			if(documentotipo.getContadestino() != null){
				Conta contadestino = contaService.loadConta(documentotipo.getContadestino());
				if(contadestino != null && 
						contadestino.getBaixaautomaticafaturamento() != null && 
						contadestino.getBaixaautomaticafaturamento()){
					contaboleto = contadestino;
				}
			}
			
			if (contaboleto == null && Boolean.TRUE.equals(documentotipo.getBoleto()) && empresa!=null && empresa.getContabancariacontareceber()!=null && empresa.getContabancariacontareceber().getCdconta()!=null){
				contaboleto = empresa.getContabancariacontareceber();
			}
			
			if(contaboleto != null && contaboleto.getCdconta() != null){
				listaTaxaitem = contacorrenteService.getTaxas(contaboleto, pedidoVendaNegociacao.getDataParcela(), enderecoPessoa);
			}
			
			//Se estiver associado a um documento de antecipa��o, n�o gera conta a receber
			if (pedidoVendaNegociacao.getDocumentoAntecipacao() == null){	
				
				if(pedidoVendaNegociacao.getDocumento() == null){
					documento = new Documento();
		//			Money valorParcela = vendapagamento.getValororiginal();
					Double taxaVenda = documentotipo.getTaxavenda();
				    if (pedidovenda.getDocumentoAposEmissaoNota() != null && pedidovenda.getDocumentoAposEmissaoNota()){
				    	documento.setDocumentoacao(Documentoacao.DEFINITIVA);
				    }else{
				    	documento.setDocumentoacao(Documentoacao.PREVISTA);
				    	if(documentoacao != null){
				    		documento.setDocumentoacao(documentoacao);
				    	}
				    }
				    documento.setConta(contaboleto);
					if(contaboleto != null && contaboleto.getCdconta() != null){
						Conta conta = contaService.carregaContaComMensagem(contaboleto, null);
						if(conta != null && conta.getContacarteira().getCdcontacarteira() != null){
							//Preenche a carteira do documento com a carteira padr�o
							documento.setContacarteira(conta.getContacarteira());
							
							if(conta.getContacarteira().getMsgboleto1() != null && !"".equals(conta.getContacarteira().getMsgboleto1()))
								documento.setMensagem1(conta.getContacarteira().getMsgboleto1());
							if(conta.getContacarteira().getMsgboleto2() != null && !"".equals(conta.getContacarteira().getMsgboleto2()))
								documento.setMensagem2(conta.getContacarteira().getMsgboleto2());
						}
						
						documento.setVinculoProvisionado(contaboleto);
					} else {
						documento.setVinculoProvisionado(documentotipo.getContadestino());
					}
					documento.setIndicecorrecao(pedidovenda.getIndicecorrecao());
					documento.setDocumentoclasse(Documentoclasse.OBJ_RECEBER);
					
					if(pedidovenda.getDtpedidovendaEmissaonota() != null){
						documento.setDtemissao(new java.sql.Date(pedidovenda.getDtpedidovendaEmissaonota().getTime()));
					}else {
						documento.setDtemissao(new java.sql.Date(pedidovenda.getDtpedidovenda().getTime()));
					}
					
					String idExterno = "";
					if(pedidovenda.getIdentificacaoexterna() != null && !pedidovenda.getIdentificacaoexterna().equals("")){
						idExterno =  " - Ident. Externo: " + pedidovenda.getIdentificacaoexterna();
					}
					documento.setDescricao("Conta a receber referente ao pedido de venda "+pedidovenda.getCdpedidovenda()+". - " + i + "/" + iteracoes +  idExterno);
					
					pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
					
					documento.setValor(pedidoVendaNegociacao.getValorOriginal());
					if(taxaVenda != null && taxaVenda > 0.0 &&
						!Boolean.TRUE.equals(documentotipo.getTaxanoprocessamentoextratocartaocredito())){
						taxaitem = new Taxaitem();
						if(documentotipo.getTipotaxacontareceber() != null && documentotipo.getTipotaxacontareceber().getCdtipotaxa() != null){
							taxaitem.setTipotaxa(documentotipo.getTipotaxacontareceber());	
						}else{
							taxaitem.setTipotaxa(Tipotaxa.TAXAMOVIMENTO);
						}
						taxaitem.setPercentual(documentotipo.getPercentual() != null ? documentotipo.getPercentual() : true);
						taxaitem.setValor(new Money(taxaVenda));
						taxaitem.setDtlimite(documento.getDtemissao());
						listaTaxaitem.add(taxaitem);
					}
					
					if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
						taxa.setListaTaxaitem(listaTaxaitem);
						documento.setTaxa(taxa);
						
						if(documento.getMensagem2() != null)
							msg2.append(documento.getMensagem2());
						if(documento.getMensagem3() != null)
							msg3.append(documento.getMensagem3());
						
						for(Taxaitem txitem : listaTaxaitem){
							if(txitem.getGerarmensagem() != null && txitem.getGerarmensagem()){
								if(tipotaxaService.isApos(txitem.getTipotaxa())) {
									preposicao = " ap�s ";
								}else {
									preposicao = " at� ";
								}
								msg = txitem.getTipotaxa().getNome() + " de " +(txitem.isPercentual() ? "" : "R$") + txitem.getValor() + (txitem.isPercentual() ? "%" : "") + (txitem.getDtlimite() != null ? preposicao + format.format(txitem.getDtlimite()) : "") + ". ";
								if((msg2.length() + msg.length()) <= 80){
									msg2.append(msg);
								}else if((msg3.length() + msg.length()) <= 80){
									msg3.append(msg);
								}
							}
						}
						if(!msg2.toString().equals("")){
							documento.setMensagem2(msg2.toString());
						}
						if(!msg3.toString().equals("")){
							documento.setMensagem3(msg3.toString());
						}
					}
					
					documento.setTipopagamento(Tipopagamento.CLIENTE);
					documento.setPessoa(pedidovenda.getCliente());
					documento.setReferencia(Referencia.MES_ANO_CORRENTE);
					
					if(parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_VENDA)){
						documento.setNumero(pedidovenda.getCdpedidovenda() + (parametrogeralService.getBoolean(Parametrogeral.DOCUMENTO_NUMERO_PARCELA_VENDA) ? "/" + numParcela : ""));
					}else {
						documento.setNumero(pedidoVendaNegociacao.getNumero() != null ? pedidoVendaNegociacao.getNumero().toString() : "");
					}
					
					documento.setDocumentotipo(pedidoVendaNegociacao.getDocumentoTipo());
					documento.setDtvencimento(pedidoVendaNegociacao.getDataParcela());
					documento.setEmpresa(empresa);
					documento.setEndereco(enderecoService.carregaEnderecoClienteForDocumento(pedidovenda.getCliente()));
					if(documento.getEndereco() == null){
						documento.setEndereco(pedidovenda.getEndereco());
					}
					rateioParcelas = new Rateio();
					rateioParcelas.setListaRateioitem(rateio.getListaRateioitem());
					rateioService.atualizaValorRateio(rateioParcelas, documento.getValor());
					
					documento.setRateio(rateioParcelas);
					
					boolean documentoAgrupado = false;
					
					List<Documento> listaContareceber = new ArrayList<Documento>();
					
					if(!documentoAgrupado){
						documentoService.saveOrUpdate(documento);
						addPedidovendaListaDocumento(documento, new Pedidovenda(pedidovenda.getCdpedidovenda()));
						listaDocumentosGerados.add(documento.getCddocumento());
						if(parametrogeralService.getBoolean(Parametrogeral.AGRUPAR_CONTARECEBER_VENDA_NOTA) && listaContareceber != null){
							listaContareceber.add(documento);
						}
						
						parcela = new Parcela();
						parcela.setDocumento(documento);
						parcela.setParcelamento(parcelamento);
						parcela.setOrdem(i);
						i++;
						parcelaService.saveOrUpdate(parcela);
						
						documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
						documentohistorico.setObservacao("Origem Negocia��o do pedido de venda: <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
						documentohistoricoService.saveOrUpdate(documentohistorico);
						listaDocumentohistorico.add(documentohistorico);
						documento.setListaDocumentohistorico(listaDocumentohistorico);
						
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setPedidovenda(pedidovenda);
						documentoorigem.setDocumento(documento);
						documentoorigemService.saveOrUpdate(documentoorigem);
						
						pedidoVendaNegociacao.setDocumento(documento);
					}
					if(pedidoVendaNegociacao.getPodeGerarCheque()){
						Cheque cheque = new Cheque();
						cheque.setBanco(pedidoVendaNegociacao.getBanco());
						cheque.setAgencia(pedidoVendaNegociacao.getAgencia());
						cheque.setConta(pedidoVendaNegociacao.getConta());
						cheque.setNumero(pedidoVendaNegociacao.getNumero() != null ? pedidoVendaNegociacao.getNumero().toString() : "");
						cheque.setEmitente(pedidoVendaNegociacao.getEmitente());
						cheque.setDtbompara(pedidoVendaNegociacao.getDataParcela());
						cheque.setValor(pedidoVendaNegociacao.getValorOriginal());
						cheque.setEmpresa(pedidovenda.getEmpresa());
						cheque.setChequesituacao(Chequesituacao.PREVISTO);
						cheque.setCpfcnpj(pedidoVendaNegociacao.getCpfcnpj());
						
						chequeService.saveOrUpdate(cheque);
						chequehistoricoService.criaHistorico(cheque,  pedidovenda);
						pedidoVendaNegociacao.setCheque(cheque);
						if(pedidoVendaNegociacao.getDocumento() != null && pedidoVendaNegociacao.getDocumento().getCddocumento() != null){
							documentoService.updateCheque(pedidoVendaNegociacao.getDocumento(), cheque);
							documento.setCheque(cheque);
						}
					}
					
				} else {
					documento = pedidoVendaNegociacao.getDocumento();
					
					if(documentotipo == null || documentotipo.getAntecipacao() == null || !documentotipo.getAntecipacao()){
						parcela = new Parcela();
						parcela.setDocumento(documento);
						parcela.setParcelamento(parcelamento);
						parcela.setOrdem(i);
						i++;
						parcelaService.saveOrUpdate(parcela);
					}
					
					documentohistorico = documentohistoricoService.geraHistoricoDocumento(documento);
					documentohistorico.setObservacao("Origem Pedido de venda: <a href=\"javascript:visualizaPedidoVenda("+ pedidovenda.getCdpedidovenda()+");\">"+pedidovenda.getCdpedidovenda() +"</a>.");
					documentohistoricoService.saveOrUpdate(documentohistorico);
					listaDocumentohistorico.add(documentohistorico);
					documento.setListaDocumentohistorico(listaDocumentohistorico);
					Documentoorigem documentoorigem = new Documentoorigem();
					documentoorigem.setPedidovenda(pedidovenda);
					documentoorigem.setDocumento(documento);
					documentoorigemService.saveOrUpdate(documentoorigem);
				
				
				Rateio rateioDocumento = rateioService.getNovoRateio(rateio);
				documento.setRateio(rateioDocumento);
				rateioService.atualizaValorRateio(documento.getRateio(), documento.getValor());
				rateioService.saveOrUpdate(documento.getRateio());
				documentoService.updateRateio(documento);
				}
			}
			
			pedidoVendaNegociacao.setPedidovenda(pedidovenda);
			pedidoVendaNegociacaoService.saveOrUpdate(pedidoVendaNegociacao);
			if(documentotipo == null || documentotipo.getAntecipacao() == null || !documentotipo.getAntecipacao()){
				numParcela++;
			}
		}
		return listaDocumentosGerados;	
	}
	
	public void registrarHistoricoParaAcoesDeNegociacao(Pedidovenda pv,String acao,String obs) {
		Pedidovendahistorico pvHistorico = new Pedidovendahistorico();
		pvHistorico.setAcao(acao);
		pvHistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		pvHistorico.setObservacao(obs);
		pvHistorico.setPedidovenda(pv);
		pedidovendahistoricoService.saveOrUpdate(pvHistorico);
	}
	
	public String desfazerNegociacao(WebRequestContext request, Pedidovenda pv) {
		if(Boolean.TRUE.equals(request.getAttribute("gerarVale"))){
			Money saldo = valecompraService.getSaldoByCliente(pv.getCliente());
			if(saldo.compareTo(pv.getValorPendentes()) <= 0){
				Valecompra vale = new Valecompra();
				vale.setCliente(pv.getCliente());
				vale.setData(pv.getDtGerarValeCompra());
				vale.setIdentificacao("REF. ANULA��O DA NEGOCI��O DE PEND�NCIA DO PEDIDO "+pv.getCdpedidovenda());
				vale.setTipooperacao(Tipooperacao.TIPO_DEBITO);
				vale.setValor(pv.getValorPendentes());
				valecompraService.saveOrUpdate(vale);
				registrarHistoricoParaAcoesDeNegociacao(pv,"Negocia��o desfeita","A negocia��o realizada anteriormente est� sendo desfeita.");
			}else{
				return "N�o � poss�vel desfazer esta negocia��o, o cliente n�o possui saldo suficiente em vale-compras.";
			}
		}else {
			List<PedidoVendaNegociacao> listaNegociacao = pv.getListaPedidoVendaNegociacao();
			List<Documentohistorico> listaHistorico = new ArrayList<Documentohistorico>();
			boolean cancelar= false;
			if(SinedUtil.isListNotEmpty(listaNegociacao)){
				for (PedidoVendaNegociacao negociacao : listaNegociacao) {
					if(negociacao.getDocumento() != null){
						Documentoacao status = negociacao.getDocumento().getDocumentoacao();
						if(status != null && (status.equals(Documentoacao.PREVISTA) || status.equals(Documentoacao.DEFINITIVA))){
							Documentohistorico documentohistorico = new Documentohistorico();
							documentohistorico.setIds(negociacao.getDocumento().getCddocumento().toString());
							documentohistorico.setObservacao("Cancelamento de negocia��o");
							documentohistorico.setDocumentoacao(Documentoacao.CANCELADA);
							documentohistorico.setFromcontareceber(Boolean.TRUE);
							listaHistorico.add(documentohistorico);
							cancelar = true;	
							documentocomissaoService.deleteDocumentocomissaoByDocumento(negociacao.getDocumento());
						}		
					}
				}	
			}
			if(cancelar){
				for (Documentohistorico documentohistorico : listaHistorico) {
					documentoService.doCancelar(documentohistorico);	
				}
				registrarHistoricoParaAcoesDeNegociacao(pv,"Negocia��o desfeita","A negocia��o realizada anteriormente est� sendo desfeita.");
			}else{
				return "N�o � poss�vel desfazer esta negocia��o, as contas a receber vinculadas est�o com situa��o diferente de previsto.";
			}
		}
		updateSituacaoPendenciaNegociacao(pv, SituacaoPendencia.NEGOCIACAO_PENDENTE);
		return "Negocia��o desfeita com sucesso";
	}
	public Pedidovenda loadForNegociacao(Pedidovenda pv) {
		return pedidovendaDAO.loadForNegociacao(pv);
	}
	public void validarFichaEtiquetaPneu(WebRequestContext request) {
		if(org.apache.commons.lang.StringUtils.isNotBlank(request.getParameter("whereInPedidoVenda"))){
			String whereInPedidoVenda = request.getParameter("whereInPedidoVenda");
			if(this.havePedidovendaSituacao(whereInPedidoVenda, Pedidovendasituacao.CANCELADO, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
				request.addError("N�o � permitido emitir Ficha/Etiqueta de pedido de venda cancelado ou aguardando aprova��o.");
				throw new SinedException("N�o � permitido emitir Ficha/Etiqueta de pedido de venda cancelado ou aguardando aprova��o.");
			}
			
			List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereInPedidoVenda);
			if(SinedUtil.isListEmpty(listaColeta)){
				request.addError("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
				throw new SinedException("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
			}
		}else if(org.apache.commons.lang.StringUtils.isNotBlank(request.getParameter("whereInVenda"))){
			String whereInVenda = request.getParameter("whereInVenda");
			if(vendaService.haveVendaSituacao(whereInVenda, Boolean.FALSE, Vendasituacao.CANCELADA, Vendasituacao.AGUARDANDO_APROVACAO)){
				request.addError("N�o � permitido emitir Ficha/Etiqueta de venda cancelada ou aguardando aprova��o.");
				throw new SinedException("N�o � permitido emitir Ficha/Etiqueta de venda cancelada ou aguardando aprova��o.");
			}
			List<Coleta> listaColeta = coletaService.findByWhereInVenda(whereInVenda);
			if(SinedUtil.isListEmpty(listaColeta)){
				request.addError("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
				throw new SinedException("N�o � permitido emitir Ficha/Etiqueta de Pneu de venda que n�o possui coleta vinculada.");
			}
		}
	}

	public Boolean isCompraAbaixoDoMarkup(Pedidovenda pedidovenda, Usuario usuario) {
		Double descontoPedido = pedidovenda.getDesconto() != null ? pedidovenda.getDesconto().getValue().doubleValue() : 0d;
		Double totalDescontoItens = 0d, totalPrecoItens = 0d, totalCustoItens = 0d;
		
		List<Pedidovendamaterial> listaItens = pedidovenda.getListaPedidovendamaterial();
		StringBuilder whereIn = new StringBuilder();
		
		for(int i = 0; i < listaItens.size(); i++) {
			whereIn.append(i < listaItens.size() - 1 ? listaItens.get(i).getMaterial().getCdmaterial() + "," : listaItens.get(i).getMaterial().getCdmaterial());
		}
		
		List<Material> listaMaterialComCusto = materialService.findForAtualizarmaterial(whereIn.toString());
		
		for(Pedidovendamaterial item : pedidovenda.getListaPedidovendamaterial()) {
			totalPrecoItens += item.getPreco() * item.getQuantidade();
			totalDescontoItens += (item.getDesconto() != null ? item.getDesconto().getValue().doubleValue() : 0d);
			
			for(Material material : listaMaterialComCusto) {
				totalCustoItens += (material.equals(item.getMaterial()) ? material.getValorcusto() : 0d) * item.getQuantidade();
			}
		}
		
		Double percentualMarkupPedido = ((totalPrecoItens - totalDescontoItens - descontoPedido - totalCustoItens) / totalCustoItens) * 100;
		
		return percentualMarkupPedido < usuario.getLimitePercentualMarkupVenda();
	}
	
	public Pedidovenda loadForCalculoMarkupPedido(Pedidovenda pedidovenda) {
		return pedidovendaDAO.loadForCalculoMarkupPedido(pedidovenda);
	}
	
	public String montaMensagemMotivosAguardandoAprovacao(List<String> listaMotivos) {
		StringBuilder mensagem = new StringBuilder();
		
		if(SinedUtil.isListNotEmpty(listaMotivos)) {
			int tamanho = listaMotivos.size();
			
			if(tamanho == 1) {
				mensagem.append(listaMotivos.get(0));
			} else if(tamanho == 2) {
				mensagem.append(listaMotivos.get(0) + " e " + listaMotivos.get(1));
			} else if(tamanho > 2) {
				for(String motivo : listaMotivos) {
					if(listaMotivos.indexOf(motivo) < tamanho - 2)
						mensagem.append(motivo + ", ");
					else if(listaMotivos.indexOf(motivo) < tamanho - 1)
						mensagem.append(motivo + " e ");
					else
						mensagem.append(motivo);
				}
			}
		}
		
		return mensagem.toString();
	}
	
	public void cancelaAtualizacaoPreco(Pedidovendamaterial pedidoVendaMaterial){
		if(parametrogeralService.getBoolean(Parametrogeral.TABELA_VENDA_CLIENTE)){
			Materialtabelaprecoitem bean = materialtabelaprecoitemService.loadByPedidoVendaMaterial(pedidoVendaMaterial);
			if(bean != null){
				MaterialTabelaPrecoItemHistorico ultimoPreco = materialTabelaPrecoItemHistoricoService.loadUltimoHistoricoComVendaNaoCancelada(bean);
				if(ultimoPreco != null){
					if(Tipocalculo.PERCENTUAL.equals(bean.getMaterialtabelapreco().getTabelaprecotipo())){
						materialtabelaprecoitemService.updatePercentualDesconto(bean, ultimoPreco.getPercentualDesconto(), true);
					}else {
						materialtabelaprecoitemService.updatePreco(bean, ultimoPreco.getValor(), true);
					}
				}
			}
		}
	}
	
	public ModelAndView recalcularFaixaMarkup(WebRequestContext request, String whereIn){
		List<Pedidovenda> listaVenda = this.findForRecalculoFaixaMarkup(whereIn);
		for(Pedidovenda venda: listaVenda){
			if (venda != null && SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())) {
				Double valorvalecompra = venda.getValorusadovalecompra() != null ? venda.getValorusadovalecompra().getValue().doubleValue() : 0d;
				Double desconto = venda.getDesconto() != null ? venda.getDesconto().getValue().doubleValue() : 0d;
				Double valortotal = venda.getTotalvendaSemAredondamento().getValue().doubleValue();
				
				valortotal += desconto + valorvalecompra;
				for (Pedidovendamaterial vm: venda.getListaPedidovendamaterial()) {
					vendaService.calcularMarkup(venda, vm);
				}
			}
		}
		pedidovendahistoricoService.makeHistoricoRecalculoFaixaMarkup(whereIn);
		return null;
	}
	
	public List<Pedidovenda> findForRecalculoFaixaMarkup(String whereIn){
		return pedidovendaDAO.findForRecalculoFaixaMarkup(whereIn);
	}
	
	public void setFaixaPedidovendaMaterial(Pedidovenda bean){
		if(SinedUtil.isListNotEmpty(bean.getListaPedidovendamaterial())){
			for (Pedidovendamaterial item : bean.getListaPedidovendamaterial()) {
				vendaService.calcularMarkup(bean, item, false);
			}
		}
	}
	
	public ModelAndView ajaxTipoDocumentoBoleto(WebRequestContext request, Documentotipo documentotipo) {
		JsonModelAndView json = new JsonModelAndView();		
		String contaboleto = "";
		Documentotipo bean = new Documentotipo();
		Empresa empresa = documentotipo.getEmpresa();
		if (documentotipo != null && documentotipo.getCddocumentotipo() != null) {
			documentotipo = documentotipoService.load(documentotipo);
			bean.setCddocumentotipo(documentotipo.getCddocumentotipo());
			bean.setBoleto(documentotipo.getBoleto());
			
			if(Boolean.TRUE.equals(documentotipo.getBoleto())){
				if(empresa != null && empresa.getCdpessoa() != null){
					try {
						Empresa empresaWithConta = empresaService.loadWithContaECarteira(empresa);
						if(empresaWithConta != null && empresaWithConta.getContabancariacontareceber() != null && empresaWithConta.getContabancariacontareceber().getCdconta() != null){
							contaboleto = "br.com.linkcom.sined.geral.bean.Conta[cdconta=" + empresaWithConta.getContabancariacontareceber().getCdconta() + "]";
						}
					} catch (Exception e1) {
					}
					try {
						if(contaboleto.equals("")){
							List<Conta> listaconta = contaService.findContasAtivasComPermissaoByEmpresa(empresa);
							if(listaconta != null && listaconta.size() == 1){
								contaboleto = "br.com.linkcom.sined.geral.bean.Conta[cdconta=" + listaconta.get(0).getCdconta() + "]";
							}
						}
					} catch (NumberFormatException e) {}
					
				}
			}
			
		}
		
		json.addObject("documentotipo", bean);
		json.addObject("contaboleto", contaboleto);
		
		return json;
	}
	
	public Money getTaxapedidovendaCliente(WebRequestContext request, Cliente cliente) {
		Money taxapedidovenda = null;
		if(cliente != null && cliente.getCdpessoa() != null){
			cliente = clienteService.carregarTaxapedidovendaCreditolimitecompraByCliente(cliente);
			if(cliente != null && cliente.getTaxapedidovenda() != null){
				taxapedidovenda = cliente.getTaxapedidovenda();
			}
		}
		
		return taxapedidovenda;
	}
	
	public ModelAndView buscarMaterialAJAXJson(WebRequestContext request, Venda venda, boolean origemCrudVenda){
		Materialtabelapreco materialtabelapreco = venda.getMaterialtabelapreco();
		Material material = materialService.loadWithCodFlex(Integer.parseInt(venda.getCodigo()));
		Pedidovendatipo pedidovendatipo = venda.getPedidovendatipo();
		venda.setMaterial(material);
		Boolean existMaterialsimilar = materialsimilarService.existMaterialsimilar(material);
		Boolean existematerialsimilarColeta = false;
		Boolean exibirMaterialcoleta = false;
		Material materialcoleta = null;
		Boolean exibiritensproducaovenda = false;
		Boolean exibiritenskitvenda = false;
		Double entrada = 0d;
		Double saida = 0d;
		Double qtddisponivel = 0d;
		Double qtde = null;
		Double valorvendaproducao = null;
		Double valorcustoproducao = null;
		Double qtddisponivelAcimaestoque = 0d;
		String msgacimaestoque = "";
		Projeto projeto = venda.getProjeto();
		Material materialPesquisaKitflexivel = request.getParameter("cdmaterialkitflexivel") != null ? new Material(Integer.parseInt(request.getParameter("cdmaterialkitflexivel"))) : null;		
		
		Materialgrupocomissaovenda mgca = null;
		if(venda.getFornecedorAgencia() != null){
			mgca = materialgrupocomissaovendaService.getComissaoAgencia(venda.getMaterial(), venda.getFornecedor(), venda.getFornecedorAgencia(), venda.getDocumentotipo(), pedidovendatipo);
		}
		
		if(material.getProducaoetapa() != null && material.getMaterialcoleta() != null){
			exibirMaterialcoleta = true;
			materialcoleta = material.getMaterialcoleta();
			existematerialsimilarColeta = materialsimilarService.existMaterialsimilar(materialcoleta);
		}
		
		Produto produto = produtoService.carregaAlturaComprimentoLargura(new Produto(material.getCdmaterial(), null, null));
		Boolean showBotaoLargCompAlt = Boolean.FALSE;
		
		boolean alturaNula = venda.getAlturas() == null;
		boolean larguraNula = venda.getLarguras() == null;
		
		Double altura = null;
		Double largura = null;
		Double comprimento = null;
		if(produto != null && (produto.getAltura() != null || produto.getComprimento() != null || produto.getLargura() != null)){
			showBotaoLargCompAlt = Boolean.TRUE;
			if(produto.getFatorconversao() == null) produto.setFatorconversao(1000d);
			if(produto.getAltura() != null){
				altura  = produto.getAltura()/produto.getFatorconversao();
			}
			if(produto.getLargura() != null){
				largura  = produto.getLargura()/produto.getFatorconversao();
			}
			if(produto.getComprimento() != null){
				comprimento  = produto.getComprimento()/produto.getFatorconversao();
			}
		}
		if(venda.getAlturas() == null) venda.setAlturas(1d);
		if(venda.getLarguras() == null) venda.setLarguras(1d);
		
		List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
		boolean existeitemgrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty() && !materialService.isControleMaterialgrademestre(material);
		boolean pesquisasomentemestre = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty() && materialService.isControleItemGradeEPesquisasomentemestre(material);
		boolean existecontroleGrade = existeitemgrade || material.getMaterialmestregrade() != null;
		
		Material materialbanda = null;
		Pneumedida pneumedida = null;
		PneuSegmento pneuSegmento = null;
		Double qtdreservada = null;
		boolean abrirPopupPneu = false;
		if (venda.getMaterial() != null){
			if(venda.getMaterial().getVendapromocional() != null && venda.getMaterial().getVendapromocional()){
				Venda vendaaux = new Venda();
				material = materialService.loadMaterialPromocionalComMaterialrelacionado(Integer.parseInt(venda.getCodigo()));
				if(material != null){
					if(material.getExibiritenskitvenda() != null){
						exibiritenskitvenda = material.getExibiritenskitvenda();
					}
					try{
						material.setProduto_altura(venda.getAlturas());
						material.setProduto_largura(venda.getLarguras());
						material.setQuantidade(venda.getQuantidades());
						if(exibiritenskitvenda){
							this.preecheValorVendaComTabelaPrecoItensKit(material, pedidovendatipo, venda.getCliente(), venda.getEmpresa(), venda.getPrazopagamento());
							venda.getMaterial().setConsiderarValorvendaCalculado(true);
						}
						valorvendaproducao = materialrelacionadoService.getValorvendakit(material);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(!exibiritenskitvenda && (request.getParameter("somentevenda") == null || !"true".equals(request.getParameter("somentevenda")))){
						valorvendaproducao = material.getValorvenda();
					}
				}
				if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
					Pedidovenda pedidovenda = new Pedidovenda();
					pedidovenda.setEmpresa(vendaaux.getEmpresa());
					pedidovenda.setLocalarmazenagem(vendaaux.getLocalarmazenagem());
					
					Double qtdereservadaItemKit = 0d;
					qtdreservada = 0d;
					for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
						if(materialrelacionado.getMaterialpromocao() != null && !Boolean.TRUE.equals(materialrelacionado.getMaterialpromocao().getServico())){	
							vendaaux.setMaterial(materialrelacionado.getMaterialpromocao());
							vendaaux.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaaux.setEmpresa(venda.getEmpresa());
							
							entrada = this.retornaEntradaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							saida = this.retornaSaidaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							if("pedido".equals(request.getParameter("origem"))){
								qtdereservadaItemKit = this.retornaReservaKitMaterial(materialrelacionado.getMaterialpromocao(), vendaaux.getEmpresa(), vendaaux.getLocalarmazenagem());
							}
			
							
							qtddisponivel = (entrada-saida-qtdereservadaItemKit)/materialrelacionado.getQuantidade();
							qtddisponivel = SinedUtil.roundByUnidademedidaRoundFloor(qtddisponivel, material.getUnidademedida());
							if(qtde == null){
								qtde = qtddisponivel;
							}else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
							
//							qtdereservadaItemKit = reservaService.getQtdeReservada(materialrelacionado.getMaterialpromocao(), vendaaux.getEmpresa(), vendaaux.getLocalarmazenagem(), null);
//							
//							if(qtdereservadaItemKit == null) qtdereservadaItemKit = 0d;
//							qtdereservadaItemKit = qtdereservadaItemKit / materialrelacionado.getQuantidade();
//							if(qtdreservada == null)
//								qtdreservada = qtdereservadaItemKit;
//							else if(qtdreservada > qtdereservadaItemKit){
//								qtdreservada = qtdereservadaItemKit;
//							}
						}
					}
				}
			}else if(venda.getMaterial().getKitflexivel() != null && venda.getMaterial().getKitflexivel()){
				Venda vendaaux = new Venda();
				venda.setCodigo(venda.getCodigo());
				venda.setLocalarmazenagem(venda.getLocalarmazenagem());
				venda.setEmpresa(venda.getEmpresa());
				material = materialService.loadKitFlexivel(venda.getMaterial());
				if(material != null){
					try{
						material.setProduto_altura(venda.getAlturas());
						material.setProduto_largura(venda.getLarguras());
						material.setQuantidade(venda.getQuantidades());
						this.preecheValorVendaComTabelaPrecoItensKitflexivel(material, pedidovendatipo, venda.getCliente(), venda.getEmpresa(), venda.getPrazopagamento());
						valorvendaproducao = materialkitflexivelService.getValorvendakitflexivel(material);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(material.getListaMaterialkitflexivel() != null && !material.getListaMaterialkitflexivel().isEmpty()){
					for(Materialkitflexivel materialkitflexivel : material.getListaMaterialkitflexivel()){
						if(materialkitflexivel.getMaterialkit() != null && materialkitflexivel.getQuantidade() != 0d){	
							vendaaux.setMaterial(materialkitflexivel.getMaterialkit());
							vendaaux.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaaux.setEmpresa(venda.getEmpresa());

							entrada = this.retornaEntradaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							saida = this.retornaSaidaMaterial(vendaaux.getMaterial(),vendaaux.getLocalarmazenagem(),vendaaux.getEmpresa(), projeto);
							
							
							qtddisponivel = (entrada-saida)/materialkitflexivel.getQuantidade();
							if(qtde == null)
								qtde = qtddisponivel;
							else if(qtddisponivel < qtde){
								qtde = qtddisponivel;
							}
						}
					}
				}
			}else {
				if(venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()){
					material = materialService.loadMaterialComMaterialproducao(venda.getMaterial());
					boolean recapagem = SinedUtil.isRecapagem();
					
					pneuSegmento = pneuSegmentoService.loadPrincipal();
					if(material != null){
						abrirPopupPneu = Util.objects.isPersistent(material.getPneumedida());
						if(Util.objects.isPersistent(pneuSegmento) && Util.objects.isPersistent(material.getPneumedida())){
							if(Boolean.TRUE.equals(pneuSegmento.getOtr()) == Boolean.TRUE.equals(material.getPneumedida())){
								pneumedida = material.getPneumedida();
							}
						}
						
						Set<Materialproducao> listaProducao = material.getListaProducao();
						for (Materialproducao materialproducao : listaProducao) {
							if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
								materialbanda = materialproducao.getMaterial();
								exibiritensproducaovenda = true;
								if(!abrirPopupPneu){
									abrirPopupPneu = Util.objects.isPersistent(materialbanda);
								}
							}
						}
						try{
							material.setProduto_altura(venda.getAlturas());
							material.setProduto_largura(venda.getLarguras());
							material.setQuantidade(venda.getQuantidades());
							if(exibiritensproducaovenda){
								this.preecheValorVendaComTabelaPrecoItensProducao(material, pedidovendatipo, venda.getCliente(), venda.getEmpresa(), venda.getPrazopagamento());
							}
							if(!recapagem){
								valorvendaproducao = materialproducaoService.getValorvendaproducao(material);
							} else {
								valorcustoproducao = materialproducaoService.getValorvendaproducao(material);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						if(/*exibiritensproducaovenda && */material.getValorvenda() != null){
							if(request.getParameter("somentevenda") == null || !"true".equals(request.getParameter("somentevenda"))){
								valorvendaproducao = material.getValorvenda();
							}
						}
					}
				}
				
				entrada = this.retornaEntradaMaterial(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), projeto);
				saida = this.retornaSaidaMaterial(venda.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), projeto);
				
				
				if(venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()){
					qtde = vendaService.getQtdeEntradaSubtractSaida(entrada, saida);
					if(qtde <= 0){
						Material materialP = materialService.loadMaterialComMaterialproducao(venda.getMaterial());
						if(materialP != null){
							Venda vendaP = new Venda();
							vendaP.setLocalarmazenagem(venda.getLocalarmazenagem());
							vendaP.setEmpresa(venda.getEmpresa());
							vendaP.setProjeto(venda.getProjeto());
							qtde = vendaService.getQtdeDisponivelMaterialproducao(materialP, vendaP);
						}
					}
				}
			}
		}
		qtddisponivel = this.retornarQtdDisponivel(qtde,entrada,saida);
		
		
		boolean showqtddisponivelAcimaestoque = false;
		if(qtddisponivel != null && venda.getLocalarmazenagem() != null){
			Localarmazenagem localarmazenagem = localarmazenagemService.loadWithInformacoesEstoque(venda.getLocalarmazenagem());
			if(localarmazenagem != null){
				if(localarmazenagem.getQtdeacimaestoque() != null){
					qtddisponivelAcimaestoque = qtddisponivel * localarmazenagem.getQtdeacimaestoque() / 100;
					showqtddisponivelAcimaestoque = true;
				}
				if(qtddisponivelAcimaestoque > 0){
					if(org.apache.commons.lang.StringUtils.isNotEmpty(localarmazenagem.getMsgacimaestoque())){
						msgacimaestoque = localarmazenagem.getMsgacimaestoque();
					}
				}
			}
		}
		
		venda.getMaterial().setVendaunidademedida(venda.getMaterial().getUnidademedida());
		Double peso = materialService.getPesoMaterial(venda.getMaterial(), altura, largura, comprimento);
		Double multiplicador = 1.0;
		if(venda.getMaterial() != null && 
				venda.getMaterial().getPesoliquidovalorvenda() != null && 
				venda.getMaterial().getPesoliquidovalorvenda() &&
				peso != null){
			multiplicador = peso;
		}
		
		Boolean metrocubicovalorvenda = venda.getMaterial().getMetrocubicovalorvenda();
		if(metrocubicovalorvenda == null) metrocubicovalorvenda = Boolean.FALSE;
		
		Boolean buscarDtVencimentoLote = Boolean.parseBoolean(request.getParameter("SELECAO_AUTOMATICA_LOTE_FATURAMENTO"));
		
		//buscar a menor data de vencimento, considerando ou n�o as quantidades reservadas
		String dtMenorVencimentoLote = "";
		if(buscarDtVencimentoLote){
			java.sql.Date dtMenorVencimento = loteestoqueService.buscarDtVencimentoLote(material.getCdmaterial(), venda.getEmpresa().getCdpessoa(), venda.getLocalarmazenagem() != null ? venda.getLocalarmazenagem().getCdlocalarmazenagem() : null);
			
			if(dtMenorVencimento != null){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				dtMenorVencimentoLote = sdf.format(dtMenorVencimento);				
			}
		}
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		ModelAndView retorno = new JsonModelAndView();
		if(venda.getMaterial() != null){
			if(qtdreservada == null){
				qtdreservada = pedidovendamaterialService.getQtdeReservada(material, venda.getEmpresa(), venda.getLocalarmazenagem());
			}
			if(valorvendaproducao != null){
				venda.getMaterial().setValorvenda(valorvendaproducao);
			}
			if(valorcustoproducao != null){
				venda.getMaterial().setValorcusto(valorcustoproducao);
			}else{
				if(! (Boolean.TRUE.equals(venda.getMaterial().getKitflexivel()) || Boolean.TRUE.equals(venda.getMaterial().getVendapromocional())
						|| Boolean.TRUE.equals(venda.getMaterial().getProducao()) || existecontroleGrade)){
					venda.getMaterial().setValorcusto(materialService.getValorCustoAtual(venda.getMaterial(), venda.getMaterial().getUnidademedida(), venda.getEmpresa()));
				}
			}
			materialformulavalorvendaService.setValorvendaByFormula(venda.getMaterial(), 
					venda.getAlturas() != null && !alturaNula ? venda.getAlturas() : altura, 
					venda.getLarguras() != null && !larguraNula ? venda.getLarguras() : largura,
					venda.getComprimentos() != null ? venda.getComprimentos() : comprimento,
					venda.getValorfrete() != null ? venda.getValorfrete().getValue().doubleValue() : null);
			
			String valorvendaminimo = "";
			String valorvendamaximo = "";
			if(!exibiritenskitvenda){// Se est� pesquisando um produto do tipo "kit" e na aba "Composi��o do kit" est� marcado para exibir os itens da venda, os valores m�ximo e minimo do produto mestre n�o dever�o ser exibidos na tela.
				if("pedido".equals(request.getParameter("origem"))){
					this.preecheValorVendaComTabelaPreco(venda.getMaterial(), pedidovendatipo, venda.getCliente(), venda.getEmpresa(), venda.getMaterial().getUnidademedida(), venda.getPrazopagamento(), materialPesquisaKitflexivel);
				}else{
					vendaService.preencheValorVendaComTabelaPreco(venda.getMaterial(), materialtabelapreco, pedidovendatipo, venda.getCliente(), venda.getEmpresa(), venda.getMaterial().getUnidademedida(), venda.getPrazopagamento(), materialPesquisaKitflexivel);
				}
				valorvendaminimo = SinedUtil.descriptionDecimal(venda.getMaterial().getValorvendaminimo());
				valorvendamaximo = SinedUtil.descriptionDecimal(venda.getMaterial().getValorvendamaximo());
			}
			if(pedidovendatipo != null){
				pedidovendatipo = pedidovendatipoService.load(pedidovendatipo, "pedidovendatipo.obrigarLoteVenda, pedidovendatipo.obrigarLotePedidovenda");				
			}
			
			if((altura != null || comprimento != null || largura != null)){
				retorno.addObject("comprimentos", (comprimento != null ? new DecimalFormat("#.#######").format(comprimento) : ""));
				retorno.addObject("alturas", (altura != null ? new DecimalFormat("#.#######").format(altura) : ""));
				retorno.addObject("larguras", (largura != null ? new DecimalFormat("#.#######").format(largura) : ""));
//				view.println("var fatorconversaocomprimento", (produto.getFatorconversao() != null ? produto.getFatorconversao() : "") + "'; ");
				retorno.addObject("fatorConversaoComprimento", "");
				retorno.addObject("margemArredondamento", (produto.getMargemarredondamento() != null ? new DecimalFormat("#.#####").format(produto.getMargemarredondamento()/1000.0) : ""));
			}
			retorno.addObject("exibirmaterialcoleta", exibirMaterialcoleta);
			retorno.addObject("materialcoleta_id", Util.strings.toStringIdStyled(materialcoleta));
			retorno.addObject("materialcoleta_label", Util.strings.toStringDescription(materialcoleta));
			if(exibirMaterialcoleta){
				retorno.addObject("existematerialsimilarColeta", existematerialsimilarColeta);
			}
			retorno.addObject("existecontroleGrade", existecontroleGrade);
			retorno.addObject("existeitemgrade", existeitemgrade);
			retorno.addObject("pesquisasomentemestre", pesquisasomentemestre);
			retorno.addObject("metrocubicovalorvenda", metrocubicovalorvenda);
			retorno.addObject("identificacaomaterial", (material.getIdentificacao() != null ? material.getIdentificacao() : ""));
			retorno.addObject("multiplicador", SinedUtil.descriptionDecimal(multiplicador));
			retorno.addObject("showBotaoLargCompAlt", showBotaoLargCompAlt);
			retorno.addObject("existMaterialsimilar", existMaterialsimilar);
			retorno.addObject("vendapromocional", (venda.getMaterial().getVendapromocional() != null ?  venda.getMaterial().getVendapromocional() : false ));
			retorno.addObject("kitflexivel", (venda.getMaterial().getKitflexivel() != null ?  venda.getMaterial().getKitflexivel() : false ));
			retorno.addObject("valorproduto", SinedUtil.descriptionDecimal(venda.getMaterial().getValorvenda(), true));
			retorno.addObject("valorminimo", valorvendaminimo);
			retorno.addObject("valormaximo", valorvendamaximo);
			retorno.addObject("percentualdescontoTabelapreco", SinedUtil.descriptionDecimal(venda.getMaterial().getPercentualdescontoTabela()));
			retorno.addObject("tabelaprecoComPrazopagamento", (venda.getMaterial().getTabelaprecoComPrazopagamento() != null ? venda.getMaterial().getTabelaprecoComPrazopagamento() : false));
			retorno.addObject("naoexibirminmaxvenda", (venda.getMaterial().getNaoexibirminmaxvenda() != null ? venda.getMaterial().getNaoexibirminmaxvenda() : false));
			retorno.addObject("qtddisponivel", SinedUtil.descriptionDecimal(qtddisponivel));
			retorno.addObject("showqtddisponivelAcimaestoque", showqtddisponivelAcimaestoque);
			retorno.addObject("qtddisponivelAcimaestoque", SinedUtil.descriptionDecimal(qtddisponivelAcimaestoque));
			retorno.addObject("msgacimaestoque", msgacimaestoque);
			retorno.addObject("obrigarLoteVenda", pedidovendatipo != null && pedidovendatipo.getObrigarLoteVenda() != null ? pedidovendatipo.getObrigarLoteVenda() : null);
			retorno.addObject("obrigarLotePedidovenda", pedidovendatipo != null && pedidovendatipo.getObrigarLotePedidovenda() != null ? pedidovendatipo.getObrigarLotePedidovenda() : null);
			
			Boolean validaestoque = Boolean.TRUE;
			if(origemCrudVenda){
				validaestoque = !(venda.getMaterial().getServico() != null && venda.getMaterial().getServico()) && !(venda.getMaterial().getPatrimonio() != null && venda .getMaterial().getPatrimonio());
			}else {
				validaestoque = !((venda.getMaterial().getServico() != null && venda.getMaterial().getServico()) || (venda.getMaterial().getProducao() != null && venda.getMaterial().getProducao()));
			}
			retorno.addObject("validaestoque", validaestoque);
			retorno.addObject("msgacimaestoque", msgacimaestoque);
			retorno.addObject("materialservico", (venda.getMaterial().getServico() != null && venda.getMaterial().getServico()));
			retorno.addObject("exibiritensproducaovenda", exibiritensproducaovenda);
			retorno.addObject("exibiritenskitvenda", exibiritenskitvenda);
			retorno.addObject("producao", (venda.getMaterial().getProducao() == null ? false : venda.getMaterial().getProducao()));
			retorno.addObject("qtdereservada", (qtdreservada == null ? "" : SinedUtil.descriptionDecimal(qtdreservada)));
			retorno.addObject("qtdeunidade", (venda.getMaterial().getQtdeunidade() != null && venda.getMaterial().getConsiderarvendamultiplos() != null && venda.getMaterial().getConsiderarvendamultiplos() ? venda.getMaterial().getQtdeunidade() : ""));
			retorno.addObject("considerarvendamultiplos", (venda.getMaterial().getConsiderarvendamultiplos() != null ? venda.getMaterial().getConsiderarvendamultiplos() : ""));
			retorno.addObject("valorcustomaterial", (venda.getMaterial().getValorcusto() != null ? SinedUtil.descriptionDecimal(venda.getMaterial().getValorcusto()) : ""));
			retorno.addObject("valorvendamaterial", (venda.getMaterial().getValorvendaSemDescontoOuValorvenda() != null ? SinedUtil.descriptionDecimal(venda.getMaterial().getValorvendaSemDescontoOuValorvenda()) : ""));
			retorno.addObject("pesoliquidovalorvenda", (venda.getMaterial().getPesoliquidovalorvenda() != null && venda.getMaterial().getPesoliquidovalorvenda()));
			retorno.addObject("peso", SinedUtil.descriptionDecimal(peso));
			retorno.addObject("pesobruto", SinedUtil.descriptionDecimal(venda.getMaterial().getPesobruto()));
			retorno.addObject("tributacaoipi", (venda.getMaterial().getTributacaoipi() == null ? false : venda.getMaterial().getTributacaoipi()));
			retorno.addObject("materialobrigalote", (material.getObrigarlote() != null ? material.getObrigarlote() : Boolean.FALSE).toString());
			retorno.addObject("materialpatrimonio", Boolean.TRUE.equals(venda.getMaterial().getPatrimonio()));
			retorno.addObject("exibirmaterialcoleta", exibirMaterialcoleta);
			
			retorno.addObject("pneumedida_id", Util.strings.toStringIdStyled(pneumedida, true));
			retorno.addObject("pneumedida_label", Util.strings.toStringDescription(pneumedida));
			retorno.addObject("pneuSegmento_id", Util.strings.toStringIdStyled(pneuSegmento, true));
			retorno.addObject("pneuSegmento_label", Util.strings.toStringDescription(pneuSegmento));
			retorno.addObject("materialbanda_id", Util.strings.toStringIdStyled(materialbanda, true));
			retorno.addObject("materialbanda_label", Util.strings.toStringDescription(materialbanda));
			retorno.addObject("abrirPopupPneu", abrirPopupPneu);
			retorno.addObject("dtMenorVencimentoLote", dtMenorVencimentoLote);
			
			if (venda.getMaterial().getArquivo() != null){
				carregarImagem(request, venda.getMaterial().getArquivo(), "exibefoto");
				retorno.addObject("cdarquivo", venda.getMaterial().getArquivo().getCdarquivo());
			}
			else retorno.addObject("cdarquivo", "");
			produto = produtoService.load(new Produto(venda.getMaterial().getCdmaterial(), null, null));
			if(produto != null && produto.getObservacao() != null){
				retorno.addObject("anotacoes", Util.strings.addScapesToDescription(produto.getObservacao().replaceAll("\\r?\\n", " ")));
			}
			else retorno.addObject("anotacoes", "");
			
			List<Materialunidademedida> unidades = materialunidademedidaService.findByMaterial(produto);
			
			StringBuilder sb = new StringBuilder();
			sb.append("[");
			DecimalFormat df = new DecimalFormat("#.#######");
			boolean incluirUnidadePrincipal = true;
			for (Materialunidademedida unidade : unidades){
				
				if (unidade != unidades.get(0))
					sb.append(",");
				
				String nomeUnidade = unidade.getUnidademedida().getNome().replace("\"", "\\\"");

				if (material.getUnidademedida().equals(unidade.getUnidademedida())){
					incluirUnidadePrincipal = false;
					nomeUnidade = "Quantidade restante";
				}

				sb.append("{");
				sb.append("nomeUnidademedida:\"").append(nomeUnidade).append("\",");
				sb.append("unidademedida:\"").append(Util.strings.toStringIdStyled(unidade.getUnidademedida())).append("\",");
				sb.append("fracao:\"").append(df.format(unidade.getFracao())).append("\",");
				sb.append("qtdereferencia:\"").append(df.format(unidade.getQtdereferencia())).append("\"");
				sb.append("}");
			}
			
			if (unidades.size() > 0 && incluirUnidadePrincipal){
				sb.append(",{");
				sb.append("nomeUnidademedida:\"").append("Quantidade restante").append("\",");
				sb.append("unidademedida:\"").append(Util.strings.toStringIdStyled(material.getUnidademedida())).append("\",");
				sb.append("fracao:\"1\",");
				sb.append("qtdereferencia:\"1\"");
				sb.append("}");
			}
			
			sb.append("]");
			
			retorno.addObject("listaMaterialUnidadeMedida", sb.toString());
			retorno.addObject("unidadeMedidaMaterial", Util.strings.toStringIdStyled(material.getUnidademedida()));
			retorno.addObject("percentualComissaoAgencia", (mgca != null && mgca.getComissionamento() != null && mgca.getComissionamento().getPercentual() != null ?  SinedUtil.descriptionDecimal(mgca.getComissionamento().getPercentual()) : ""));
			
			if(qtddisponivel == null) qtddisponivel = 0d;
			if(qtdreservada == null) qtdreservada = 0d;
			
			Double quantidadeDisponivelMenosReservada = qtddisponivel - qtdreservada;
			retorno.addObject("quantidadeDisponivelMenosReservada", SinedUtil.descriptionDecimal(quantidadeDisponivelMenosReservada));
		}
		
		return retorno;
		
	}
	
	public ModelAndView abrirMaterialDisponivelTransferencia(WebRequestContext request, Venda venda){		
		MaterialDisponivelTransferenciaBean bean = criarMaterialDisponivelTransferencia(request, venda);
		return new ModelAndView("direct:/crud/popup/popUpMaterialDisponivelTransferencia", "bean", bean);
	}
	
	public MaterialDisponivelTransferenciaBean criarMaterialDisponivelTransferencia(WebRequestContext request, Venda venda){
		
		MaterialDisponivelTransferenciaBean bean = new MaterialDisponivelTransferenciaBean();
		
		
		bean.setQtdpedida(Double.parseDouble(request.getParameter("qtdpedido").replace(".", "").replace(",", ".")));
		Integer localAtual = Integer.parseInt(request.getParameter("cdlocalarmazenagem"));
		Boolean disponivelLocalAtual = false;
		Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(Integer.parseInt(request.getParameter("cdmaterial")));
		Empresa empresa = venda.getEmpresa();
		List<Localarmazenagem> listaLocais = localarmazenagemService.findAtivosByEmpresa(empresa);
		Projeto projeto = venda.getProjeto();
		
		Double qtdereservadaItemKit = 0d;
		Double qtddisponivel = 0d;
		
		List<Materialrelacionado> materialalternativo = new ArrayList<Materialrelacionado>();
		List<Materialrelacionado> materialSemLocal = new ArrayList<Materialrelacionado>();
		
		if (material != null && material.getCdmaterial() != null) {
			bean.setMaterial(material);
						
			for (Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()) {
				Boolean materialDisponivel = false;
				if (materialrelacionado.getMaterialpromocao() != null && !Boolean.TRUE.equals(materialrelacionado.getMaterialpromocao().getServico())) {
										
					List<Localarmazenagem> localdisponivel = new ArrayList<Localarmazenagem>();
													
					for (Localarmazenagem localarmazenagem : listaLocais) {
							
							disponivelLocalAtual = false;
							Localarmazenagem altlocal = new Localarmazenagem();
							altlocal.setCdlocalarmazenagem(localarmazenagem.getCdlocalarmazenagem());
							altlocal.setNome(localarmazenagem.getNome());
							
							Double entrada = this.retornaEntradaMaterial(materialrelacionado.getMaterialpromocao(),localarmazenagem,empresa, projeto);
							Double saida = this.retornaSaidaMaterial(materialrelacionado.getMaterialpromocao(),localarmazenagem,empresa, projeto);
							
							if("pedido".equals(request.getParameter("origem"))){
								qtdereservadaItemKit = this.retornaReservaKitMaterial(materialrelacionado.getMaterialpromocao(), empresa, localarmazenagem);
							}
							
							qtddisponivel = (entrada-saida-qtdereservadaItemKit);
							//Double qtdkitDisponivel = (entrada-saida-qtdereservadaItemKit)/materialrelacionado.getQuantidade();							
							//qtdkitDisponivel = SinedUtil.roundByUnidademedidaRoundFloor(qtddisponivel, material.getUnidademedida());
														
							Double qtdTotal = materialrelacionado.getQuantidade() * bean.getQtdpedida();
							if (localarmazenagem.getCdlocalarmazenagem().equals(localAtual)) {								
								if (qtddisponivel >= qtdTotal) {
									disponivelLocalAtual = true;
									materialDisponivel = true;
									break;
								}
							}
							
							if (qtddisponivel != null && qtddisponivel > 0 && qtddisponivel >= qtdTotal) {
								if (!disponivelLocalAtual) {
									altlocal.setQtddisponivel(qtddisponivel);
									localdisponivel.add(altlocal);
									materialDisponivel = true;
								}																
							}						
					}
					materialrelacionado.setListalocalarmazenagem(localdisponivel);					
				}
				if (materialDisponivel && !disponivelLocalAtual) {
					materialalternativo.add(materialrelacionado);
				}else if (!materialDisponivel) {
					materialSemLocal.add(materialrelacionado);
				}
			}
		}
		
		bean.setListaMaterialrelacionado(materialalternativo);
		bean.setListaMaterialrelacionadoSemLocal(materialSemLocal);
		
		return bean;
	}
	

	
	public void carregarImagem(WebRequestContext request, Arquivo arquivo, String atributo){
		try {
			arquivoService.loadAsImage(arquivo);
			if(arquivo.getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivo.getCdarquivo());
				request.setAttribute(atributo, true);
			}
		} catch (Exception e) {
			request.setAttribute(atributo, false);
		}
	}
	
	public ModelAndView ajaxCamposAdicionais(WebRequestContext request, Pedidovenda bean){
		
		if(!Util.objects.isPersistent(bean.getPedidovendatipo())){
			throw new SinedException("O tipo de Atividade n�o pode se nulo.");
		}
		
		List<Campoextrapedidovendatipo> lista = campoextrapedidovendatipoService.findByTipo(bean.getPedidovendatipo());
		List<Pedidovendavalorcampoextra> listaAtual = pedidovendavalorcampoextraService.findByPedidoVenda(bean);
		
		if (listaAtual != null && lista != null){
			for (Pedidovendavalorcampoextra campoExtraAtual : listaAtual){
				for (Campoextrapedidovendatipo campoExtra : lista){
					if (campoExtra.getCdcampoextrapedidovendatipo().equals(campoExtraAtual.getCampoextrapedidovendatipo().getCdcampoextrapedidovendatipo())){
						campoExtra.setCdvalorcampoextraatual(campoExtraAtual.getCdpedidovendavalorcampoextra());
						campoExtra.setValorAtual(campoExtraAtual.getValor());
					}
				}
			}
		}
		
		Pedidovendatipo pedidovendatipo = pedidovendatipoService.load(bean.getPedidovendatipo());
		
		return new JsonModelAndView()
					.addObject("size", lista.size())
					.addObject("lista", lista)
					.addObject("isGarantia", Boolean.TRUE.equals(pedidovendatipo.getGarantia()));	
	}
	
	public ModelAndView preencherObservacaoPadrao(WebRequestContext request, Pedidovendatipo pedidoVendaTipo) {
		JsonModelAndView json = new JsonModelAndView();
		
		String observacao = "";
		if(Util.objects.isPersistent(pedidoVendaTipo)) {
			observacao = pedidovendatipoService
					.load(pedidoVendaTipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.observacaoPadrao")
					.getObservacaoPadrao();
		}
		json.addObject("observacaoPadrao", Util.strings.emptyIfNull(observacao));
		
		return json;
	}
	
	public ModelAndView exibirConfirmGerarValeCompra(WebRequestContext request, Pedidovendatipo pedidoVendaTipo) {
		JsonModelAndView json = new JsonModelAndView();
		
		Boolean exibir = false;
		String parametroUtilizacaoValeCompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
		
		if(parametroUtilizacaoValeCompra != null && parametroUtilizacaoValeCompra.equals("PEDIDO") && Util.objects.isPersistent(pedidoVendaTipo)) {
			pedidoVendaTipo = pedidovendatipoService.loadForEntrada(pedidoVendaTipo);
			
			if(GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidoVendaTipo.getGeracaocontareceberEnum())) {
				exibir = true;
			} 
		}
		json.addObject("exibirConfirmGerarValeCompra", exibir);
		
		return json;
	}
	
	public ModelAndView verificarFornecedorProdutos(WebRequestContext request, Empresa empresa, String whereInMaterial){
		Set<Integer> listaCdmaterial = new HashSet<Integer>();
		
		if (!whereInMaterial.equals("")){
			if(empresa != null && empresa.getCdpessoa() == null) empresa = null;
			
			List<Fornecedor> listaFornecedor = fornecedorService.findForFaturamento(empresa, whereInMaterial);
			for (Fornecedor fornecedor: listaFornecedor){
				for (Materialfornecedor materialfornecedor: fornecedor.getListaMaterialfornecedor()){
					listaCdmaterial.add(materialfornecedor.getMaterial().getCdmaterial());
				}
			}
		}
		
		return new JsonModelAndView().addObject("listaCdmaterial", listaCdmaterial);
	}

	public ModelAndView buscarSugestoesVendaAJAX(WebRequestContext request, Venda venda, String whereInMaterial){
		List<Sugestaovenda> listaSugestoes = materialvendaService.findForSugestao(whereInMaterial);

		if(listaSugestoes != null && !listaSugestoes.isEmpty()){
			for(Sugestaovenda sugestaovenda : listaSugestoes){
				if(sugestaovenda.getCdmaterial() != null){
					Double valortabelapreco = null;
					Materialtabelaprecoitem materialtabelaprecoitem = null;
					Double valorMaximoTabela = null;
					Double valorMinimoTabela = null;
					Double valorVenda = sugestaovenda.getValor();
					
					String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
					Integer numCasasDecimais = null; 
					if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
						String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
						if(aux != null && !aux.trim().isEmpty()){
							try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
						}
					}
					Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(new Material(sugestaovenda.getCdmaterial()), venda.getCliente(), venda.getPrazopagamento(), venda.getPedidovendatipo(), venda.getEmpresa());
					if(materialtabelapreco != null){
						if(valorVenda != null){
							valortabelapreco = materialtabelapreco.getValorComDescontoAcrescimo(valorVenda, numCasasDecimais);
						}
					}else {
						materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(new Material(sugestaovenda.getCdmaterial()), venda.getCliente(), venda.getPrazopagamento(), venda.getPedidovendatipo(), venda.getEmpresa(), new Unidademedida(sugestaovenda.getCdunidademedida()));
						if(materialtabelaprecoitem != null){
							valortabelapreco = materialtabelaprecoitem.getValorcomtaxa(sugestaovenda.getValor(), numCasasDecimais);
							valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo();
							valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo();
						}
					}
					
					if(valortabelapreco != null){
						sugestaovenda.setValor(valortabelapreco);
					}
					if(valorMaximoTabela != null){
						sugestaovenda.setValorvendamaximo(valorMaximoTabela);
					}
					if(valorMinimoTabela != null){
						sugestaovenda.setValorvendaminimo(valorMinimoTabela);
					}
					
					Boolean buscarDtVencimentoLote = Boolean.parseBoolean(request.getParameter("SELECAO_AUTOMATICA_LOTE_FATURAMENTO"));
					String dtMenorVencimentoLote = "";
					if(buscarDtVencimentoLote){
						java.sql.Date dtMenorVencimento = loteestoqueService.buscarDtVencimentoLote(sugestaovenda.getCdmaterial(), venda.getEmpresa().getCdpessoa(), venda.getLocalarmazenagem() != null ? venda.getLocalarmazenagem().getCdlocalarmazenagem() : null);
						
						if(dtMenorVencimento != null){
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
							dtMenorVencimentoLote = sdf.format(dtMenorVencimento);				
						}
					
						sugestaovenda.setMenorDataValidade(dtMenorVencimentoLote);
					}
				}
			}
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("listaSugestoes", listaSugestoes);
		return jsonModelAndView;
	}
	
	public ModelAndView getQtdeLoteestoque(WebRequestContext request, Material material, Empresa empresa, Localarmazenagem localArmazenagem, Loteestoque loteEstoque){
		Double qtde = 0.0;
		if(Util.objects.isPersistent(loteEstoque)  && Util.objects.isPersistent(material)){
			boolean cadastroErrado = false;
			boolean whereMaterialmestregrade = false;
			Material bean = materialService.loadWithGrade(material);
			if(bean != null && bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getGradeestoquetipo() != null){
				if(Gradeestoquetipo.MESTRE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
					if(bean.getMaterialmestregrade() != null){
						whereMaterialmestregrade = true;
					}else {
						whereMaterialmestregrade = false;
					}
				}else if(Gradeestoquetipo.ITEMGRADE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
					if(bean.getMaterialmestregrade() != null){
						whereMaterialmestregrade = false;
					}else {
						/*verifica se o material est� sendo controlado por item de grade e n�o
						existe filhos vinculados a ele. Se retornar true, o cadastro do material est� errado ( material n�o � de grade) 
						e a quantidade retornada em estoque deve ser zero.*/
						if(materialService.isControleItemgradeSemMaterialgrademestre(material)){
							cadastroErrado = true;
						}else {
							whereMaterialmestregrade = true;
						}
					}
				}
			}
			if(!cadastroErrado){
				qtde = loteestoqueService.getQtdeLoteestoque(material, localArmazenagem, empresa, loteEstoque, whereMaterialmestregrade);
			}
		}
		return new JsonModelAndView().addObject("qtdeloteestoque", qtde);
	}
	
	public ModelAndView ajaxBuscaInfoPrazopagamento(Prazopagamento prazoPagamento){
		Boolean prazomedio = false;
		Boolean aVista = false;
		if(Util.objects.isPersistent(prazoPagamento)){
			Prazopagamento prazopagamento = prazopagamentoService.load(prazoPagamento, "prazopagamento.cdprazopagamento, prazopagamento.avista, prazopagamento.prazomedio");
			if(Util.objects.isPersistent(prazopagamento)){
				prazomedio = Boolean.TRUE.equals(prazopagamento.getPrazomedio());
				aVista = Boolean.TRUE.equals(prazopagamento.getAvista());
			}
		}
		return new JsonModelAndView()
					.addObject("prazoMedio", prazomedio)
					.addObject("aVista", aVista);
	
	}
	
	public ModelAndView pagamento(WebRequestContext request, Pedidovenda pedidovenda, String dtPrazoEntrega){
		
		AjaxRealizarVendaPagamentoBean bean = this.pagamento(pedidovenda, dtPrazoEntrega);
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("bean", bean);
		json.addObject("isValorminimo", bean.getIsValorminimo());
		json.addObject("valorMinimoPagamento", bean.getValorMinimoPagamento());
		json.addObject("existjuros", (bean.getJuros() != null && bean.getJuros() > 0 ? "true" : "false"));
		
		return json;
	}
	
	public Boolean validate(Pedidovenda bean, BindException errors){
		WebRequestContext request = NeoWeb.getRequestContext();
		
		if(bean.getCdpedidovenda() != null && Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(bean.getPedidovendasituacao()) && 
				bean.getAprovarpedidovenda() != null && bean.getAprovarpedidovenda()){
			if(this.havePedidovendaDiferenteSituacao(bean.getCdpedidovenda().toString(), Pedidovendasituacao.AGUARDANDO_APROVACAO)){
				errors.reject("001","Pedido de venda j� aprovado.");
			}
			
			this.validaListaPedidovendapagamento(request, bean, errors);
		}
		
		if(bean.getCdpedidovenda() != null){
			boolean permiteAlterarProducaoEmAndamento = parametrogeralService.getBoolean(Parametrogeral.PERMITIR_ALTERAR_PEDIDO_COM_PRODUCAO_EM_ANDAMENTO) || Boolean.TRUE.equals(bean.getOrigemOtr());
			if(this.isProducaoEmAndamento(bean.getCdpedidovenda().toString()) && !permiteAlterarProducaoEmAndamento){
				errors.reject("001", "N�o � poss�vel editar o pedido. A produ��o est� em andamento. Necess�rio cancelar a agenda de produ��o para realizar a edi��o.");
			}
		}
		
		vendaService.validaOrdenacao(errors, bean.getListaPedidovendamaterial());
		
		if(!validaListaPedidovendamaterialComListaPedidoVendaPagamento(bean)){
			errors.reject("001","Valor final do pedido de venda n�o pode ser maior que o total dos pagamentos.");
			NeoWeb.getRequestContext().setAttribute("aprovarPedidovenda", bean.getAprovarpedidovenda());
		}
		
		if (!validaPagamentoAntecipado(bean)){
			errors.reject("001","O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
			NeoWeb.getRequestContext().setAttribute("aprovarPedidovenda", bean.getAprovarpedidovenda());
		}
		
		Pedidovendatipo pedidovendatipo = null;
		if(bean.getPedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(bean.getPedidovendatipo());
			bean.setIsNotVerificarestoquePedidovenda(vendaService.isNotVerificarEstoqueByPedidovendatipo(pedidovendatipo));
			bean.setPermitirvendasemestoque((pedidovendatipo != null && pedidovendatipo.getPermitirvendasemestoque() != null && pedidovendatipo.getPermitirvendasemestoque()));
		}
		if(this.isQtdeSolicitadaMaiorOrIgualQtdeReservada(request, bean, false)){
			errors.reject("001", "");
		}
		if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && bean.getEmpresa() != null && org.apache.commons.lang.StringUtils.isNotBlank(bean.getIdentificador()) && this.isIdentificadorCadastrado(bean)) {
			errors.reject("001","Aten��o! Identificador j� cadastrado no sistema.");
			NeoWeb.getRequestContext().setAttribute("aprovarPedidovenda", bean.getAprovarpedidovenda());
		}	
		if(!this.validaKitFlexivel(request, bean)){
			errors.reject("001","");
		}
		
		if((bean.getPedidovendatipo() != null && (Boolean.TRUE.equals(pedidovendatipo.getReserva()) && TipoReservaEnum.PEDIDO_VENDA.equals(pedidovendatipo.getReservarApartir())) 
				|| (pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getObrigarLotePedidovenda()))) || Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO))){
			boolean retornaFalse = false;
			for(Pedidovendamaterial pvm: bean.getListaPedidovendamaterial()){
				if(!materialService.validateObrigarLote(pvm, request)){
					retornaFalse = true;
				}
			}
			if(retornaFalse){
				return false;
			}
		}
		
		if(pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getValidarTicketMedioPorFornecedor())){
			materialService.validateDadosForCalculoTicketMedio(materialService.translatePedidoVendaMaterialToMaterial(bean.getListaPedidovendamaterial()), errors);
			List<VendaFornecedorTicketMedioBean> lista = vendaService.calculaTicketMedioByFornecedor(bean, bean.getListaPedidovendamaterial());
			if((SinedUtil.isListNotEmpty(lista))){
				if(!vendaService.validateTicketMedioPorFornecedor(lista, errors)){
					return false;
				}
				this.preencheListaPedidoVendaFornecedorTicketMedio(bean, lista);
			}
		}	
		
		this.validaObrigatoriedadePneu(bean, null, errors);
			
		return true;
	}
	
	public boolean validaPagamentoAntecipado(Pedidovenda pedidovenda){
		List<Pedidovendapagamento> listaPedidovendapagamento = pedidovenda.getListaPedidovendapagamento();
		
		if (listaPedidovendapagamento != null && !listaPedidovendapagamento.isEmpty()){

			List<Documento> listaAntecipacao = new ArrayList<Documento>();
			Boolean isWms = empresaService.isIntegracaoWms(pedidovenda.getEmpresa());
			
			if ((isWms != null && isWms) || parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO))
				listaAntecipacao = documentoService.findForAntecipacaoConsideraPedidoVenda(pedidovenda.getCliente(), null, pedidovenda);
			else
				listaAntecipacao = documentoService.findForAntecipacaoConsideraVenda(pedidovenda.getCliente(), null);
			
			Map<Integer, Double> mapaAntecipacao = new HashMap<Integer, Double>();
			
			for (Documento antecipacao : listaAntecipacao){
				mapaAntecipacao.put(antecipacao.getCddocumento(), antecipacao.getValor().getValue().doubleValue());
			}
			
			for (Pedidovendapagamento pedidovendapagamento : listaPedidovendapagamento){
				if (pedidovendapagamento.getDocumentoantecipacao() != null && pedidovendapagamento.getDocumentoantecipacao().getCddocumento() != null){
					Double valor = mapaAntecipacao.get(pedidovendapagamento.getDocumentoantecipacao().getCddocumento());
					if (valor != null && pedidovendapagamento.getValororiginal() != null){
						valor = valor - pedidovendapagamento.getValororiginal().getValue().doubleValue();
						if (valor < 0){
							return false;
						} else {
							mapaAntecipacao.put(pedidovendapagamento.getDocumentoantecipacao().getCddocumento(), valor);
						}
					}
				}
			}			
		}		
		
		return true;
	}
	
	public boolean validaListaPedidovendamaterialComListaPedidoVendaPagamento(Pedidovenda pedidovenda) {
		Money valorfinalpedidovenda = new Money();
		Money valorfinalpedidovendaTruncado = new Money();
		Money valortotalpagamentos = new Money();
		if(pedidovenda != null){
			boolean consideraripiconta = false;
			boolean considerarIcmsstconta = false;
			boolean considerardesoneracaoconta = false;
			boolean considerarFcpstconta = false;
			Pedidovendatipo pedidovendatipo = pedidovenda.getPedidovendatipo();
			if(pedidovendatipo != null){
				pedidovendatipo = pedidovendatipoService.loadForConsiderarImpostos(pedidovendatipo);
				if((pedidovendatipo.getBonificacao() != null && pedidovendatipo.getBonificacao()) || 
						(pedidovendatipo.getComodato() != null && pedidovendatipo.getComodato())){
					return true;
				}
				consideraripiconta = pedidovendatipo.getConsideraripiconta() != null && pedidovendatipo.getConsideraripiconta(); 
				considerarIcmsstconta = Boolean.TRUE.equals(pedidovendatipo.getConsideraricmsstconta());
				considerardesoneracaoconta = Boolean.TRUE.equals(pedidovendatipo.getConsiderardesoneracaoconta());
				considerarFcpstconta = Boolean.TRUE.equals(pedidovendatipo.getConsiderarfcpstconta());
			}
			this.setTotalImpostos(pedidovenda);
			valorfinalpedidovenda = pedidovenda.getTotalvenda();
			valorfinalpedidovendaTruncado = pedidovenda.getTotalvenda(true);
			if(consideraripiconta){
				valorfinalpedidovenda = valorfinalpedidovenda.add(pedidovenda.getTotalipi());
				valorfinalpedidovendaTruncado = valorfinalpedidovenda.add(pedidovenda.getTotalipi());
			}
			if(considerarIcmsstconta){
				valorfinalpedidovenda = valorfinalpedidovenda.add(pedidovenda.getTotalIcmsSt());
				valorfinalpedidovendaTruncado = valorfinalpedidovenda.add(pedidovenda.getTotalIcmsSt());
			}
			if(considerardesoneracaoconta){
				valorfinalpedidovenda = valorfinalpedidovenda.subtract(pedidovenda.getTotalDesoneracaoIcms());
				valorfinalpedidovendaTruncado = valorfinalpedidovenda.subtract(pedidovenda.getTotalDesoneracaoIcms());
			}
			if(considerarFcpstconta){
				valorfinalpedidovenda = valorfinalpedidovenda.add(pedidovenda.getTotalFcpSt());
				valorfinalpedidovendaTruncado = valorfinalpedidovenda.add(pedidovenda.getTotalFcpSt());
			}
				
			valortotalpagamentos = pedidovenda.getTotalparcela();
			
			if(pedidovenda.getValorusadovalecompra() != null){
				Money valortotalsemvalecompra = pedidovenda.getTotalvendaSemValecompra();
				if(valortotalsemvalecompra != null && 
						SinedUtil.round(valortotalsemvalecompra.getValue(), 2).compareTo(SinedUtil.round(pedidovenda.getValorusadovalecompra().getValue(), 2)) == 0){
					return true;
				}
			}
			
			if(valorfinalpedidovenda != null && valortotalpagamentos != null && 
					SinedUtil.round(valorfinalpedidovenda.getValue(), 2).compareTo(SinedUtil.round(valortotalpagamentos.getValue(), 2)) > 0
					){	
				if(valorfinalpedidovendaTruncado != null && 
						SinedUtil.round(valorfinalpedidovendaTruncado.getValue(), 2).compareTo(SinedUtil.round(valortotalpagamentos.getValue(), 2)) > 0){
					return false;
				}
			}else if(valorfinalpedidovenda != null && valorfinalpedidovenda.getValue().doubleValue() < 0){
				return Boolean.FALSE;
			}
		}
		return true;
	}
	
	public Boolean validateComprar(WebRequestContext request, Pedidovenda pedidoVenda){
		List<String> listaMotivosAguardandoAprovacao = new ArrayList<String>();
		
		this.buscarTabelaPrecoVenda(pedidoVenda);
		
		String externo = parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
		if(externo.equals("TRUE")){
			if(this.isIdentificadorExternoRepetido(pedidoVenda)){
				request.addError(MSG_IDENTIFICADOR_EXTERNO);
				return false;
			}else if (pedidoVenda.getIdentificacaoexterna() == null || pedidoVenda.getIdentificacaoexterna() == ""){
				request.addError("Campo identificador externo � obrigat�rio.");
				return false;
			}	
		}
		
		Boolean restricaoCliente = Boolean.FALSE;
		
		Pedidovendatipo pedidovendatipo = null;
		if(pedidoVenda.getPedidovendatipo() != null && pedidoVenda.getPedidovendatipo().getCdpedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidoVenda.getPedidovendatipo());
			pedidoVenda.setIsNotVerificarestoquePedidovenda(vendaService.isNotVerificarEstoqueByPedidovendatipo(pedidovendatipo));
			pedidoVenda.setPermitirvendasemestoque((pedidovendatipo != null && pedidovendatipo.getPermitirvendasemestoque() != null && pedidovendatipo.getPermitirvendasemestoque()));
		}
		
		if(this.isQtdeSolicitadaMaiorOrIgualQtdeReservada(request, pedidoVenda, false)){
			if(pedidoVenda.getListaPedidovendamaterial() != null){
				for (Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()) {
					if(pedidovendamaterial.getLoteestoque() != null && pedidovendamaterial.getLoteestoque().getCdloteestoque() != null){
						pedidovendamaterial.setLoteestoque(loteestoqueService.load(pedidovendamaterial.getLoteestoque()));
					}
				}
			}
			return false;
		}
		
		if(pedidoVenda.getColaborador() == null || pedidoVenda.getColaborador().getCdpessoa() == null){
			pedidoVenda.setColaborador(SinedUtil.getUsuarioComoColaborador());
		}
		vendaService.validaOrdenacao(request, pedidoVenda.getListaPedidovendamaterial());
		if(request.getBindException().hasErrors()){
			return false;
		}
		
		Colaborador colaborador = colaboradorService.findColaboradorusuario(pedidoVenda.getColaborador().getCdpessoa());
		if(colaborador == null || colaborador.getCdpessoa() == null){
			request.addError("Aten��o! Este usu�rio n�o � colaborador.");
			return false;
		}
		if(pedidoVenda.getCliente() == null || pedidoVenda.getCliente().getCdpessoa() == null){
			request.addError("O cliente � obrigat�rio.");
			return false;
		}
		if(SinedDateUtils.afterIgnoreHour(new java.sql.Date(System.currentTimeMillis()), new java.sql.Date(pedidoVenda.getDtpedidovenda().getTime()))){
			request.addError("Data do pedido de venda n�o pode ser anterior a data atual.");
			return false;
		}
		if (!validaPagamentoAntecipado(pedidoVenda)){
			request.addError("O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.");
			return false;
		}
		if(!this.validaListaVendapagamentorepresentacao(request, pedidoVenda, pedidovendatipo)){
			return false;
		}
		
		if(!this.validaListaPedidovendapagamento(request, pedidoVenda, null)){
			return false;
		}
		
		if(!this.validaContaDespesaCentrocustoRepresentacao(request, pedidoVenda)){
			return false;
		}
		
		if(!this.validaKitFlexivel(request, pedidoVenda)){
			return false;
		}
		
		if(!vendaService.validaVendedorprincipal(pedidoVenda, request)){
			return false;
		}
		
		if(!this.validaComissionamento(pedidoVenda, request)){
			return false;
		}
		
		if(!this.validaObrigatoriedadePneu(pedidoVenda, request, null)){
			return false;
		}
		boolean garantia = false;
		boolean bonificacao = false;
		boolean comodato = false; 
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			pedidoVenda.setPedidovendatipo(pedidovendatipo);
			if(pedidovendatipo.getBonificacao() != null && pedidovendatipo.getBonificacao()){
				bonificacao = true;
			}else if(pedidovendatipo.getComodato() != null && pedidovendatipo.getComodato()){
				comodato = true;
			}
			garantia = pedidovendatipo.getGarantia() != null && pedidovendatipo.getGarantia();
		}
		
		if(!bonificacao && !comodato){
			if(!validaListaPedidovendamaterialComListaPedidoVendaPagamento(pedidoVenda)){
				request.addError("Valor final do pedido de venda n�o pode ser negativo ou maior que o total dos pagamentos.");
				return false;
			}
			restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(pedidoVenda.getCliente());
		}
		if(SinedUtil.isRecapagem() && pedidoVenda.getListaPedidovendamaterial() != null){
			for (Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()) {
				if(garantia){
/*							if(pedidovendamaterial.getPneu() != null && pedidovendamaterial.getPneu().getCdpneu() != null){
						Pneu pneu = pneuService.load(pedidovendamaterial.getPneu());
						Pneuqualificacao qualificacao = pneu.getPneuqualificacao() == null? null: pneuqualificacaoService.load(pneu.getPneuqualificacao());
						if(qualificacao == null || Boolean.TRUE.equals(qualificacao.getGarantia())){
							qualificacaoSemGarantia = false;
						}
					}*/
					if(pedidovendamaterial.getPedidovendamaterialgarantido() != null){
						Pedidovendamaterial pedidovendamaterialgarantido = pedidovendamaterialService.findByReformapneu(null, null, null, pedidovendamaterial.getPedidovendamaterialgarantido());
						if(pedidovendamaterialgarantido.getPneu() != null &&
							((pedidovendamaterial.getPneu().getCdpneu() == null) ||!pedidovendamaterialgarantido.getPneu().getCdpneu().equals(pedidovendamaterial.getPneu().getCdpneu()))){
							request.addError("O pneu do item do pedido de venda deve ser igual ao pneu do servi�o garantido selecionado.");
							return false;
						}
					}
				}
				if(pedidovendamaterial.getGarantiareformaitem() != null && pedidovendamaterial.getGarantiareformaitem().getCdgarantiareformaitem() != null){
					if(pedidovendamaterial.getDesconto() != null && pedidovendamaterial.getDescontogarantiareforma() != null &&
						pedidovendamaterial.getTotalproduto().getValue().doubleValue() > pedidovendamaterial.getDesconto().getValue().doubleValue() &&
						pedidovendamaterial.getDesconto().getValue().doubleValue() < pedidovendamaterial.getDescontogarantiareforma().getValue().doubleValue()){
						request.addError("O campo Desconto do item  n�o pode ser menor que o desconto de garantia.");
						return false;
					}
				}
			}
		}
		if(!validaContagerencialCentrocusto(request, pedidoVenda) || !validaListaPedidovendamaterial(request, pedidoVenda)){
			return false;
		}
		if((pedidoVenda.getPedidovendatipo() != null && (Boolean.TRUE.equals(pedidovendatipo.getReserva()) && TipoReservaEnum.PEDIDO_VENDA.equals(pedidovendatipo.getReservarApartir())) 
				|| (pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getObrigarLotePedidovenda()))) || Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO))){
			boolean retornaFalse = false;
			for(Pedidovendamaterial pvm: pedidoVenda.getListaPedidovendamaterial()){
				if(!materialService.validateObrigarLote(pvm, request)){
					retornaFalse = true;
				}
			}
			if(retornaFalse){
				return false;
			}
		}
		
		if(pedidovendatipo != null && Boolean.TRUE.equals(pedidovendatipo.getValidarTicketMedioPorFornecedor())){
			if(!materialService.validateDadosForCalculoTicketMedio(materialService.translatePedidoVendaMaterialToMaterial(pedidoVenda.getListaPedidovendamaterial()), request)){
				return false;
			}
			List<VendaFornecedorTicketMedioBean> lista = vendaService.calculaTicketMedioByFornecedor(pedidoVenda, pedidoVenda.getListaPedidovendamaterial());
			if((SinedUtil.isListNotEmpty(lista))){
				if(!vendaService.validateTicketMedioPorFornecedor(lista, request)){
					return false;
				}
				this.preencheListaPedidoVendaFornecedorTicketMedio(pedidoVenda, lista);
			}
		}
		return true;
	}
	
	private void preencheListaPedidoVendaFornecedorTicketMedio(Pedidovenda venda, List<VendaFornecedorTicketMedioBean> lista){
		venda.setListaPedidoVendaFornecedorTicketMedio(new ArrayList<PedidoVendaFornecedorTicketMedio>());
		for(VendaFornecedorTicketMedioBean beanTicket: lista){
			venda.getListaPedidoVendaFornecedorTicketMedio().add(beanTicket.translateForPedidoVenda(venda));
		}
	}
	
	protected Boolean validaListaPedidovendamaterial(WebRequestContext request, Pedidovenda pedidoVenda) {
		if(pedidoVenda.getListaPedidovendamaterial() == null || pedidoVenda.getListaPedidovendamaterial().isEmpty()){
			request.addError("Nenhum produto encontrado no pedido de venda.");
			return Boolean.FALSE;
		}
		
		Boolean vendasaldoproduto = Boolean.valueOf(parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO));
		Boolean vendasaldoporminmax = Boolean.valueOf(parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX));
		Boolean desconsiderardescontosaldoproduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO)) ? true : false ;
		
		Boolean validoValorTabelaPreco = Boolean.TRUE;
		Double qtdemultiplo = null;
		for(Pedidovendamaterial pedidovendamaterial : pedidoVenda.getListaPedidovendamaterial()){
			if(pedidovendamaterial.getDtprazoentrega() == null && org.apache.commons.lang.StringUtils.isNotBlank(pedidovendamaterial.getDtprazoentregaStr())) {
				try {
					pedidovendamaterial.setDtprazoentrega(new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(pedidovendamaterial.getDtprazoentregaStr()).getTime()));				
				} catch(ParseException e) {
					e.printStackTrace();
				}
			}
			Material material = materialService.load(pedidovendamaterial.getMaterial());
			pedidovendamaterial.getMaterial().setTributacaoipi(material.getTributacaoipi());
			
			Double preco = pedidovendamaterial.getPreco();
			Double precoComDesconto = preco;
			
			if(!desconsiderardescontosaldoproduto && pedidovendamaterial.getDesconto() != null && pedidovendamaterial.getDesconto().getValue().doubleValue() > 0){
				Money descontoRealDoItem = pedidovendamaterial.getDesconto();
				if(pedidovendamaterial.getDescontogarantiareforma() != null){
					descontoRealDoItem = descontoRealDoItem.subtract(pedidovendamaterial.getDescontogarantiareforma());
					if(descontoRealDoItem.getValue().doubleValue() < 0){
						descontoRealDoItem = new Money();
					}
				}
				precoComDesconto = precoComDesconto - (descontoRealDoItem.getValue().doubleValue() / pedidovendamaterial.getQuantidade());
			}
			
			
			Unidademedida unidademedida = unidademedidaService.findByMaterial(material);
			
			Double minimo = material.getValorvendaminimo();
			Double maximo = material.getValorvendamaximo();
			Double valorpreco = material.getValorvenda();
			Double valorprecoTabela = null;
			Double valorMaximoTabela = null;
			Double valorMinimoTabela = null;
			
			if(!pedidovendamaterial.getUnidademedida().equals(unidademedida) && pedidovendamaterial.getFatorconversao() != null){
				Materialunidademedida materialunidademedida = materialunidademedidaService.getMaterialunidademedida(materialService.loadMaterialunidademedida(material), pedidovendamaterial.getUnidademedida());
				
				if(materialunidademedida != null && materialunidademedida.getValorminimo() != null){
					minimo = materialunidademedida.getValorminimo();
				}else if(material.getValorvendaminimo() != null)
					minimo = material.getValorvendaminimo() / pedidovendamaterial.getFatorconversaoQtdereferencia();
				
				if(materialunidademedida != null && materialunidademedida.getValormaximo() != null){
					maximo = materialunidademedida.getValormaximo();
				}else if(material.getValorvendamaximo() != null)
					maximo = material.getValorvendamaximo() / pedidovendamaterial.getFatorconversaoQtdereferencia();
				
				if(material.getValorvenda() != null){
					valorpreco = material.getValorvenda() / pedidovendamaterial.getFatorconversaoQtdereferencia();
				}
			}
			
			String utilizarArredondamentoDesconto = parametrogeralService.buscaValorPorNome(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO);
			Integer numCasasDecimais = null; 
			if(utilizarArredondamentoDesconto != null && Boolean.parseBoolean(utilizarArredondamentoDesconto)){
				String aux = parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO);
				if(aux != null && !aux.trim().isEmpty()){
					try {numCasasDecimais = Integer.parseInt(aux);} catch (Exception e) {}
				}
			}
			if(pedidovendamaterial.getIdentificadorespecifico() != null && 
					!pedidovendamaterial.getIdentificadorespecifico().equals("") &&
					"TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO))){
				String identificador = pedidovendamaterial.getIdentificadorespecifico();
				
				Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(pedidoVenda.getDtpedidovenda(), null, pedidoVenda.getCliente(), pedidoVenda.getPrazopagamento(), pedidoVenda.getPedidovendatipo(), pedidoVenda.getEmpresa(), true, null);
				if(materialtabelapreco != null){
					List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = materialtabelaprecoitemService.findByMaterialtabelapreco(materialtabelapreco);
					for (Materialtabelaprecoitem materialtabelaprecoitem : listaMaterialtabelaprecoitem) {
						if(materialtabelaprecoitem.getIdentificadorespecifico() != null && 
								materialtabelaprecoitem.getIdentificadorespecifico().equals(identificador)){
							valorprecoTabela = materialtabelaprecoitem.getValor();
							valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximo() != null ? materialtabelaprecoitem.getValorvendamaximo() : 0;
							valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimo() != null ? materialtabelaprecoitem.getValorvendaminimo() : 0;
							break;
						}
					}
				}
			} else {
				Materialtabelapreco materialtabelapreco = materialtabelaprecoService.getTabelaPrecoAtual(pedidovendamaterial.getMaterial(), pedidoVenda.getCliente(), pedidoVenda.getPrazopagamento(), pedidoVenda.getPedidovendatipo(), pedidoVenda.getEmpresa());
				if(materialtabelapreco != null){
					if(valorpreco != null){
						valorprecoTabela = materialtabelapreco.getValorComDescontoAcrescimo(valorpreco, numCasasDecimais);
					}
				}else {
					Materialtabelaprecoitem materialtabelaprecoitem = materialtabelaprecoitemService.getItemWithValorMinimoMaximoTabelaPrecoAtual(pedidovendamaterial.getMaterial(), pedidoVenda.getCliente(), pedidoVenda.getPrazopagamento(), pedidoVenda.getPedidovendatipo(), pedidoVenda.getEmpresa(), pedidovendamaterial.getUnidademedida());
					if(materialtabelaprecoitem != null){
						valorMinimoTabela = materialtabelaprecoitem.getValorvendaminimocomtaxa((!materialtabelaprecoitem.isUnidadesecundaria() ? minimo : null));
						valorMaximoTabela = materialtabelaprecoitem.getValorvendamaximocomtaxa((!materialtabelaprecoitem.isUnidadesecundaria() ? maximo : null));
						valorprecoTabela = materialtabelaprecoitem.getValorcomtaxa((!materialtabelaprecoitem.isUnidadesecundaria() ? valorpreco : null), numCasasDecimais);
						
						if(!pedidovendamaterial.getUnidademedida().equals(unidademedida) && pedidovendamaterial.getFatorconversao() != null && !materialtabelaprecoitem.isUnidadesecundaria()){
							if(valorMinimoTabela != null)
								valorMinimoTabela = valorMinimoTabela / pedidovendamaterial.getFatorconversaoQtdereferencia();
							if(valorMaximoTabela != null)							
								valorMaximoTabela = valorMaximoTabela / pedidovendamaterial.getFatorconversaoQtdereferencia();
							if(valorprecoTabela != null)							
								valorprecoTabela = valorprecoTabela / pedidovendamaterial.getFatorconversaoQtdereferencia();
						}
					}
				}
			}
			
			if(valorMinimoTabela != null){
				minimo = valorMinimoTabela;
			}
			if(valorMaximoTabela != null){
				maximo = valorMaximoTabela;
			}
			if(valorprecoTabela != null){
				valorpreco = valorprecoTabela;
			}
			
			if(!pedidovendatipoService.considerarValorcustoMaterial(pedidoVenda.getPedidovendatipo())){
				if (minimo != null && minimo > 0.0){
					pedidovendamaterial.setValorvendaminimo(minimo);
					if(!vendasaldoproduto || !vendasaldoporminmax){
						Usuario usuarioLogado = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.salvarAbaixoMinimoPedido");
						Boolean venderAbaixoMinimo = usuarioLogado.getSalvarAbaixoMinimoPedido();
						if (!(venderAbaixoMinimo != null ? venderAbaixoMinimo : Boolean.FALSE) && precoComDesconto < minimo){
							request.addError("Pre�o menor que o m�nimo permitido do material " + material.getNome() + ".");
							validoValorTabelaPreco = false;
						}
					}
				}
				if (maximo != null && maximo > 0){
					if(!vendasaldoproduto || !vendasaldoporminmax){
						if (precoComDesconto > maximo){
							request.addError("Pre�o maior que o m�ximo permitido do material " + material.getNome() + ".");
							validoValorTabelaPreco = false;
						}
					}
				}
			}
			
			if(pedidoVenda.getPedidovendatipo() != null){
				Pedidovendatipo pedidovendatipo = pedidovendatipoService.load(pedidoVenda.getPedidovendatipo());
				if(pedidovendatipo != null && pedidovendatipo.getObrigarPrazoEntrega() != null && pedidovendatipo.getObrigarPrazoEntrega() && pedidovendamaterial.getDtprazoentrega() == null){
					request.addError("O campo prazo de entrega da lista de produtos � obrigat�rio.");
					return Boolean.FALSE;
				}
				if(pedidovendatipo != null && pedidovendatipo.getRepresentacao() != null && pedidovendatipo.getRepresentacao()){
					Fornecedor fornecedor = null;
					if(SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendamaterial())){
						for(Pedidovendamaterial vendamaterial : pedidoVenda.getListaPedidovendamaterial()){
							if(vendamaterial.getFornecedor() != null){
								if(fornecedor == null){
									fornecedor = vendamaterial.getFornecedor();
								}else if(!fornecedor.equals(vendamaterial.getFornecedor())){
									request.addError("N�o � permitido realizar venda de representa��o com fornecedor diferente.");
									return Boolean.FALSE;
								}
							}
						}
					}
				}
			}
			if(material.getConsiderarvendamultiplos() != null && material.getConsiderarvendamultiplos()){
				qtdemultiplo = materialService.validaMultiplo(pedidovendamaterial.getQuantidade(), material.getQtdeunidade(), pedidovendamaterial.getFatorconversao(), pedidovendamaterial.getQtdereferencia());
				if(qtdemultiplo != null && qtdemultiplo > 0){
					request.addError("S� � permitido vender o material " + material.getNome() + " com a quantidade m�ltiplo de " + qtdemultiplo);
					return  Boolean.FALSE;
				}
			}
		}
		
		return validoValorTabelaPreco;
	}
	
	public ModelAndView comprar(WebRequestContext request, Pedidovenda pedidoVenda, PedidoVendaParametersBean parameters){
		List<String> listaMotivosAguardandoAprovacao = new ArrayList<String>();
		StringBuilder observacaoHistorico = new StringBuilder();
		boolean fromWebService = parameters.getFromWebService();
		boolean restricaoCliente = false;
		Pedidovendatipo pedidovendatipo = null;
		if(pedidoVenda.getPedidovendatipo() != null && pedidoVenda.getPedidovendatipo().getCdpedidovendatipo() != null){
			pedidovendatipo = pedidovendatipoService.loadForEntrada(pedidoVenda.getPedidovendatipo());
		}
		if (SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendamaterial())) {
			
			boolean bonificacao = false;
			boolean sincronizarComWMS = false;
			boolean gerarreceita = false;
			boolean comodato = false;
			boolean garantia = false;
			boolean limitecreditocliente = false;
			boolean validadeLimiteCreditoExpirada = false;
			boolean requeraprovacaopedido = false;
			boolean requeraprovacaovalorvendaminimo = false;
			boolean prazomaiorprazotabela = this.isPrazomaiorprazotabela(pedidoVenda);
			String materiaisWithValorAbaixoMinimo = "";
			boolean coletaautomatica = false;
			boolean producaoautomatica = false;
			boolean representacao = false;
			boolean aguardandoAprovacaoIpi = false;
			boolean compraAbaixoDoMarkup = false;
			
			if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
				pedidoVenda.setPedidovendatipo(pedidovendatipo);
				if(pedidovendatipo.getBonificacao() != null && pedidovendatipo.getBonificacao()){
					bonificacao = true;
				}else if(pedidovendatipo.getComodato() != null && pedidovendatipo.getComodato()){
					comodato = true;
				}
				garantia = pedidovendatipo.getGarantia() != null && pedidovendatipo.getGarantia();
				if(pedidovendatipo.getGeracaocontareceberEnum() != null && 
						GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(pedidovendatipo.getGeracaocontareceberEnum())){
					gerarreceita = true;
				}
				if(pedidovendatipo.getRequeraprovacaopedido() != null){
					requeraprovacaopedido = pedidovendatipo.getRequeraprovacaopedido();
				}
				if(pedidovendatipo.getRequeraprovacaovalorvendaminimo() != null){
					requeraprovacaovalorvendaminimo = pedidovendatipo.getRequeraprovacaovalorvendaminimo();
				}
				sincronizarComWMS = pedidovendatipo.getSincronizarComWMS() != null ? pedidovendatipo.getSincronizarComWMS() : false;
				coletaautomatica = pedidovendatipo.getColetaautomatica() != null && pedidovendatipo.getColetaautomatica();
				producaoautomatica = pedidovendatipo.getProducaoautomatica() != null && pedidovendatipo.getProducaoautomatica();
				representacao = pedidovendatipo.getRepresentacao() != null && pedidovendatipo.getRepresentacao();
			}
			
			if(!bonificacao && !comodato){
				restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(pedidoVenda.getCliente());
			}
			
			if(requeraprovacaovalorvendaminimo){
				materiaisWithValorAbaixoMinimo = this.verificaMaterialWithValorAbaixoMinimo(pedidoVenda);
			}
			
			if(this.existeProdutoTributadoIpiSemValorIpi(pedidoVenda.getListaPedidovendamaterial(), pedidoVenda.getEmpresa())){
				aguardandoAprovacaoIpi = true;
			}
			
			
			// o calculo e compara��o do percentual de markup com o valor do pedido s� acontece se o usu�rio possui esse campo preenchido
			Usuario usuario = SinedUtil.getUsuarioLogado();
			usuario = usuarioService.load(usuario, "usuario.cdpessoa, usuario.limitePercentualMarkupVenda");
			
			if(usuario.getLimitePercentualMarkupVenda() != null) {
				compraAbaixoDoMarkup = this.isCompraAbaixoDoMarkup(pedidoVenda, usuario);
			}
			
			if(bonificacao 
					|| restricaoCliente 
					|| comodato 
					|| garantia 
					|| requeraprovacaopedido 
					|| prazomaiorprazotabela 
					|| aguardandoAprovacaoIpi
					|| compraAbaixoDoMarkup
					|| (requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !materiaisWithValorAbaixoMinimo.equals(""))){
				
				if(restricaoCliente){
					pedidoVenda.setRestricaocliente(restricaoCliente);
				}
				
				if(!materiaisWithValorAbaixoMinimo.equals("")) {
					listaMotivosAguardandoAprovacao.add("existem produtos com valor abaixo do valor m�nimo cadastrado");
				}
				
				if(compraAbaixoDoMarkup) {
					listaMotivosAguardandoAprovacao.add("o valor de markup do pedido de venda est� abaixo do permitido para o seu usu�rio");
				}
				
				pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			} else {					
				pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
			
			if(pedidoVenda.getAprovacaopercentualdesconto() != null && pedidoVenda.getAprovacaopercentualdesconto()){
				pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				observacaoHistorico.append("Necessita de aprova��o do desconto. ");
				
				listaMotivosAguardandoAprovacao.add("o total de descontos do pedido de venda est� acima do permitido para o seu usu�rio");
			}
			
//			Money valorFinal = pedidovendaService.calculaValorVenda(pedidoVenda);
			
			boolean permitevendasemanalise = false;
			boolean existePedidovendapagamento = SinedUtil.isListNotEmpty(pedidoVenda.getListaPedidovendapagamento());
			if(existePedidovendapagamento){
				List<Integer> idsDocumentotipo = new ArrayList<Integer>();
				for(Pedidovendapagamento pedidovendapagamento : pedidoVenda.getListaPedidovendapagamento()){
					if(pedidovendapagamento.getDocumentotipo() != null &&
							pedidovendapagamento.getDocumentotipo().getCddocumentotipo() != null &&
							!idsDocumentotipo.contains(pedidovendapagamento.getDocumentotipo().getCddocumentotipo())){
						idsDocumentotipo.add(pedidovendapagamento.getDocumentotipo().getCddocumentotipo());
					}
				}
				
				if(idsDocumentotipo != null && idsDocumentotipo.size() > 0){
					String whereInDocumentotipo = CollectionsUtil.concatenate(idsDocumentotipo, ",");
					permitevendasemanalise = documentotipoService.getPermiteVendaSemAnaliseByWhereIn(whereInDocumentotipo);
				}
			}
			
			Boolean paramLimite = parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE);
			Boolean paramBloquearvendadevedor = parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_DEVEDOR);
			Money valorTotal = new Money();
			Money valortotaldebito = new Money();
			if(existePedidovendapagamento && !permitevendasemanalise && (paramLimite || paramBloquearvendadevedor)){
				Prazopagamento prazopagamento = null;
				if(pedidoVenda.getPrazopagamento() != null && pedidoVenda.getPrazopagamento().getCdprazopagamento() != null){
					prazopagamento = prazopagamentoService.load(pedidoVenda.getPrazopagamento(), "prazopagamento.cdprazopagamento, prazopagamento.avista");
				}
				if(prazopagamento == null || (prazopagamento.getAvista() == null || !prazopagamento.getAvista())){
					if(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_CHEQUE)){
						ClientedevedorBean clientedevedorBean = vendaService.verificaDebitoAcimaLimiteByCliente(pedidoVenda.getDocumentotipo() != null ? pedidoVenda.getDocumentotipo().getCddocumentotipo().toString() : null, pedidoVenda.getCliente(), pedidoVenda.getValorfinal());
						if(clientedevedorBean != null && clientedevedorBean.getChequependente() != null && clientedevedorBean.getChequependente()){
							pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
						}
					}
					
					Cliente cliente = clienteService.load(pedidoVenda.getCliente());
					
					if(paramLimite && clienteService.getValidadeLimiteCreditoExpirada(pedidoVenda.getCliente())){
						pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
						validadeLimiteCreditoExpirada = true;
					}else {
						boolean clientepossuicontaatrasada = contareceberService.clientePossuiContasAtrasadas(cliente);
						if(paramBloquearvendadevedor && clientepossuicontaatrasada){
							pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
							clientepossuicontaatrasada = true;
						}
						
						if(paramLimite && (!clientepossuicontaatrasada || !paramBloquearvendadevedor)){
							ClienteInfoFinanceiraBean limiteCredido = vendaService.criarBeanClienteInfoFinanceira(request,pedidoVenda.getCliente());
							Money valorTotalPedidovendaComIpi = pedidoVenda.getTotalvendaMaisImpostos();
							
//							valortotaldebito = contareceberService.getValorDebitoContasEmAbertoByCliente(cliente);
							valorTotal = valorTotalPedidovendaComIpi != null && limiteCredido.getSaldolimite() != null ? limiteCredido.getSaldolimite().subtract(valorTotalPedidovendaComIpi) : new Money();
							if(cliente.getCreditolimitecompra() != null && valorTotal.compareTo(0.006) != -1){
								pedidoVenda.setLimitecreditoexcedido(Boolean.TRUE);
								pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
								limitecreditocliente = true;
							}
							if(cliente.getCreditolimitecompra() != null && cliente.getCreditolimitecompra().equals(0.0) && clientepossuicontaatrasada){
								pedidoVenda.setLimitecreditoexcedido(Boolean.TRUE);
								pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);		
								limitecreditocliente = true;
							}
						}
					}
				}
			}
			if(garantia && SinedUtil.isRecapagem()){
				pedidoVenda.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
			String whereInOSV = null;
			if(pedidoVenda.getWhereInOSV() != null && !pedidoVenda.getWhereInOSV().trim().isEmpty()){
				whereInOSV = pedidoVenda.getWhereInOSV();
			}
			
//			ATUALIZA O IDENTIFICADOR DA EMPRESA
			boolean usarSequencial = pedidoVenda.getIdentificadorAutomatico() != null && pedidoVenda.getIdentificadorAutomatico();
			boolean isCriar = pedidoVenda.getCdpedidovenda() == null;
			if(usarSequencial && isCriar){
				Integer proximoIdentificador = empresaService.carregaProximoIdentificadorPedidovenda(pedidoVenda.getEmpresa());
				if(proximoIdentificador == null){
					proximoIdentificador = 1;
				}
				empresaService.updateProximoIdentificadorPedidovenda(pedidoVenda.getEmpresa(), proximoIdentificador+1);
				pedidoVenda.setIdentificador(proximoIdentificador.toString());
			}
			
			if ("TRUE".equalsIgnoreCase(parametrogeralService.buscaValorPorNome(Parametrogeral.IDENTIFICADOR_UNICO_PEDIDO_VENDA_ORCAMENTO)) && pedidoVenda.getEmpresa() != null && org.apache.commons.lang.StringUtils.isNotBlank(pedidoVenda.getIdentificador()) && this.isIdentificadorCadastrado(pedidoVenda)) {
				request.addError("Aten��o! Identificador j� cadastrado no sistema.");
				return null;
			}
			
			if(pedidoVenda.getListaPedidovendamaterial() != null && pedidoVenda.getListaPedidovendamaterial().size() > 0){
				for (Pedidovendamaterial item : pedidoVenda.getListaPedidovendamaterial()) {
					if (item.getPneu() != null) {
						if(Boolean.TRUE.equals(pedidoVenda.getOrigemOtr())){//Para o segmento otr, o pneu s� sleciona pneu j� persistido no banco. Se n�o tem dados pneu, � porque � servi�o sem pneu
							if(!Util.objects.isPersistent(item.getPneu())){
								item.setPneu(null);
							}
						}else{
							if(item.getPneu().existeDados()){
								pneuService.saveOrUpdate(item.getPneu());
							} else {
								item.setPneu(null);
							}
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(item.getDtprazoentregaStr())) {
						try {
							item.setDtprazoentrega(new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(item.getDtprazoentregaStr()).getTime()));				
						} catch(ParseException e) {
							e.printStackTrace();
						}
					}
				}
			}
		
			this.setCustooperacional(pedidoVenda);
			this.setFaixaPedidovendaMaterial(pedidoVenda);
			this.saveOrUpdate(pedidoVenda);
			
			if(sincronizarComWMS){
				if(empresaService.isIntegracaoWms(pedidoVenda.getEmpresa())){
					this.integracaoWMS(pedidoVenda, "Cria��o do pedido de venda. Qtde de itens = " + pedidoVenda.getListaPedidovendamaterial().size(), false);
					if(pedidoVenda.getPedidovendasituacao() != null && Pedidovendasituacao.PREVISTA.equals(pedidoVenda.getPedidovendasituacao())){
						try {
							this.updateIntegrar(pedidoVenda, Boolean.TRUE);
						} catch (Exception e) {
							request.addError("Erro ao tentar marcar pedido de venda para sincronizar com o wms. " + e.getMessage());
						}
					}
				}
			}
			
			boolean registrarValecompra = false;
//			if ((reserva && gerarreceita)){
			if (gerarreceita && (pedidoVenda.getPedidovendasituacao() == null || !Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(pedidoVenda.getPedidovendasituacao()))){
				Empresa empresa = empresaService.loadComContagerencialCentrocusto(pedidoVenda.getEmpresa());
				List<Rateioitem> listarateioitem = this.prepareAndSaveVendaMaterial(pedidoVenda, false, empresa, false, null);
				if(!representacao){
					this.prepareAndSaveReceita(pedidoVenda, listarateioitem, empresa);
				}
				this.prepareAndSaveReceitaRepresentacao(pedidoVenda, listarateioitem, empresa, false);
				String contasareceber = "";
				for (Pedidovendapagamento pvp : pedidoVenda.getListaPedidovendapagamento()){
					if(pvp.getDocumento() != null){
						if("".equals(contasareceber)) contasareceber = " Contas a receber geradas: ";
						contasareceber += "<a href= '/"+NeoWeb.getApplicationName()
									+"/financeiro/crud/Contareceber?ACAO=consultar&cddocumento=" 
										+ pvp.getDocumento().getCddocumento() 
										+ "'> "
										+ pvp.getDocumento().getCddocumento() 
										+ " </a>,";
					}
					if(pvp.getDocumento()!=null || pvp.getDocumentoantecipacao()!=null){
						registrarValecompra = true;							
					}
				}
				
				if(registrarValecompra){
					this.registrarCreditoValecompra(pedidoVenda);	
				}
				
				if(contasareceber.length() > 0){
					observacaoHistorico.append(contasareceber.substring(0, contasareceber.length()-1));
				}
			}

			this.gerarReservaAoSalvar(pedidovendatipo, pedidoVenda);
			
			try {
				//TODO
				String urlLogVether = SinedUtil.getUrlWithContext();
				if(urlLogVether != null && urlLogVether.contains("vether")){
					StringBuilder obsVetherLog = new StringBuilder("### log_vether_pedido_venda: ");
					obsVetherLog.append("gerarreceita = " + gerarreceita);
					obsVetherLog.append(", situacao = " + (pedidoVenda.getPedidovendasituacao() == null ? "nula" : pedidoVenda.getPedidovendasituacao().ordinal()));
					obsVetherLog.append(", representacao = " + representacao);
					System.out.println(obsVetherLog.toString());
				}
			} catch (Exception e) {
				System.out.println("### log_vether_pedido_venda: erro ao gerar log do pedido de venda" + e.getMessage());
			}
			
			if(limitecreditocliente){
				String obsLimitecredito = "Necessita de aprova��o do limite de cr�dito. ";
				if(valortotaldebito != null && valortotaldebito.getValue().doubleValue() > 0){
					Date dtvencimento = documentoService.getPrimeiroVencimentoContasEmAbertoByCliente(pedidoVenda.getCliente());
					Integer numDias = 0;							
					if(dtvencimento != null)
						numDias = SinedDateUtils.diferencaDias(SinedDateUtils.currentDateToBeginOfDay(), dtvencimento);
					
					obsLimitecredito += "( Cliente com pend�ncia financeira " +
							(numDias != null && numDias > 0 ? (", em atraso: " + numDias + " dias) ") : ") ");
				}
				observacaoHistorico.append(obsLimitecredito);
			}
			
			if(validadeLimiteCreditoExpirada){
				observacaoHistorico.append("Cliente com validade do limite de cr�dito expirada. ");
			}
			
			if(!usarSequencial && isCriar){
				observacaoHistorico.append("N�o foi usado o sequencial do identificador. " + (pedidoVenda.getObservacao() != null ? pedidoVenda.getObservacao() : ""));
			}
			
			if(prazomaiorprazotabela){
				observacaoHistorico.append("Aguardando aprova��o devido a mudan�a no prazo de pagamento. " + (pedidoVenda.getObservacao() != null ? pedidoVenda.getObservacao() : ""));
			}
			
			if(requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !"".equals(materiaisWithValorAbaixoMinimo)){
				observacaoHistorico.append("Aguardando aprova��o por ter material com valor de venda abaixo do m�nimo. (Materiais: " + materiaisWithValorAbaixoMinimo + "). " + (pedidoVenda.getObservacao() != null ? pedidoVenda.getObservacao() : ""));
			}
			
			if(aguardandoAprovacaoIpi){
				observacaoHistorico.append("Necessita de aprova��o referente ao produto tributado por ipi sem valor de ipi. " + (pedidoVenda.getObservacao() != null ? pedidoVenda.getObservacao() : ""));
			}
			
			this.salvaPedidoHistorico(pedidoVenda, observacaoHistorico.toString());
			
			String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
			if("PEDIDO".equalsIgnoreCase(param_utilizacao_valecompra) && pedidoVenda.getPedidovendasituacao() != null && 
						!Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(pedidoVenda.getPedidovendasituacao())){
				registrarValecompra = true;
			}
			
			if(registrarValecompra){
				this.registrarUsoValecompra(pedidoVenda);
			}

			if(pedidoVenda.getVendaorcamento() != null){
				vendaorcamentoService.updateSituacaoVendaorcamento(pedidoVenda.getVendaorcamento().getCdvendaorcamento().toString(), Vendaorcamentosituacao.AUTORIZADO);
				
				Vendaorcamentohistorico vendaorcamentohistorico = new Vendaorcamentohistorico();
				vendaorcamentohistorico.setVendaorcamento(pedidoVenda.getVendaorcamento());
				vendaorcamentohistorico.setAcao("Autorizado");
				vendaorcamentohistorico.setObservacao("Pedido de venda criado: <a href=\"javascript:visualizarPedidovenda(" + 
						pedidoVenda.getCdpedidovenda() + 
						");\">" + 
						pedidoVenda.getCdpedidovenda() + 
						"</a>");
				
				vendaorcamentohistoricoService.saveOrUpdate(vendaorcamentohistorico);
			}else if(org.apache.commons.lang.StringUtils.isNotBlank(whereInOSV)){
				ordemservicoveterinariahistoricoService.saveHistoricoOSPedidovenda(whereInOSV, pedidoVenda.getCdpedidovenda());
				ordemservicoveterinariamaterialService.updateFaturado(whereInOSV);
				ordemservicoveterinariaService.updateFaturado(whereInOSV);
				pedidovendaordemservicoveterinariaService.savePedidovendaordemservicoveterinaria(whereInOSV, pedidoVenda);
				reservaService.deleteByOrdemservicoveterinaria(whereInOSV);
			}
			
			if(SinedUtil.isRecapagem()){
				if(pedidoVenda.getListaPedidovendamaterial() != null && pedidoVenda.getListaPedidovendamaterial().size() > 0){
					for (Pedidovendamaterial item : pedidoVenda.getListaPedidovendamaterial()) {
						if(item.getGarantiareformaitem() != null && item.getGarantiareformaitem().getCdgarantiareformaitem() != null){
							Garantiareformaitem garantiareformaitem = garantiareformaitemService.load(item.getGarantiareformaitem());
							garantiareformaitemService.utilizarGarantiaNoPedido(garantiareformaitem, item);
						}else{
							garantiareformaitemService.retiraVinculoGarantiaPedidovendamaterial(item);
						}
					}
				}
			}
			
			String mensagem = "Pedido de Venda cadastrado com sucesso.";
			String retorno = Boolean.TRUE.equals(pedidoVenda.getOrigemOtr())? "PedidovendaOtr?ACAO=criar": "Pedidovenda?ACAO=criar";
			
			if(Boolean.TRUE.equals(parameters.getEmitirComprovante())){
				retorno += "&emitirComprovante=true";
			}
			
			if(pedidoVenda.getVendaorcamento() != null){
				retorno = request.getServletRequest().getContextPath() + "/faturamento/crud/VendaOrcamento?ACAO=listagem";
				mensagem = "Or�amento " + pedidoVenda.getVendaorcamento().getCdvendaorcamento() + " autorizado como pedido de venda com sucesso.";
			}
			
			if(coletaautomatica){
				List<Pedidovenda> listaColeta = this.findForColetarProducao(pedidoVenda.getCdpedidovenda().toString());
				if(listaColeta == null || listaColeta.size() == 0){
					coletaautomatica = false;
				}
			}
			if(coletaautomatica){
				retorno = request.getServletRequest().getContextPath() + request.getFirstRequestUrl() + "?ACAO=consultar&cdpedidovenda=" + pedidoVenda.getCdpedidovenda() + "&coletaautomatica=" + coletaautomatica + "&producaoautomatica=" + producaoautomatica;//mudou
			} else if(producaoautomatica){
				retorno = request.getServletRequest().getContextPath() + request.getFirstRequestUrl() + "?ACAO=abrirGerarProducaoConferencia&selectedItens=" + pedidoVenda.getCdpedidovenda();//mudou
			}
			
			List<Pedidovendamaterial> listaVendamaterialForCriarTabela = this.getListaPedidovendamaterialForCriarTabela(pedidoVenda);
			this.verificarAndCriarTabelaprecoCliente(request, pedidoVenda, listaVendamaterialForCriarTabela);
			
			request.addMessage(mensagem);
			pedidoVenda.getMensagemAposSalvar().add(new Message(MessageType.WARN, mensagem));
			Boolean emitirComprovanteAguardandoAprovacao = parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO");
			String mensagemMotivoAguardandoAprovacao = this.montaMensagemMotivosAguardandoAprovacao(listaMotivosAguardandoAprovacao);
			
			if(org.apache.commons.lang.StringUtils.isNotBlank(mensagemMotivoAguardandoAprovacao)) {
				mensagemMotivoAguardandoAprovacao = "Este pedido de venda ficar� como Aguardando Aprova��o porque " + mensagemMotivoAguardandoAprovacao;
				
				if(emitirComprovanteAguardandoAprovacao) {
					mensagemMotivoAguardandoAprovacao += ".";
				} else {
					int tamanho = listaMotivosAguardandoAprovacao.size();
					mensagemMotivoAguardandoAprovacao += ". Por " + (tamanho > 1 ? "esses motivos" : "esse motivo") +
							" n�o ser� poss�vel a emiss�o ou envio de comprovante at� a sua aprova��o.";
				}
				
				pedidoVenda.getMensagemAposSalvar().add(new Message(MessageType.WARN, mensagemMotivoAguardandoAprovacao));
			}
			if(fromWebService){
				return new JsonModelAndView().addObject("success", true);
			}
			request.getServletResponse().setContentType("text/html");
			StringBuilder response = new StringBuilder();
			response.append("<html><body><script>");
			if(org.apache.commons.lang.StringUtils.isNotBlank(mensagemMotivoAguardandoAprovacao)) {
				response.append("alert('" + mensagemMotivoAguardandoAprovacao + "');");
			}
		
			if ((Pedidovendasituacao.PREVISTA.equals(pedidoVenda.getPedidovendasituacao()) || emitirComprovanteAguardandoAprovacao) && parameters.getEmitirComprovante()) {
				String comprovanteMatricial = parametrogeralService.buscaValorPorNome(Parametrogeral.COMPROVANTE_PEDIDOVENDA_MATRICIAL);
				if (comprovanteMatricial != null && "true".equalsIgnoreCase(comprovanteMatricial)){
					//response.append("window.location = '../../faturamento/relatorio/PedidovendaMatricial?ACAO=gerar&whereIn="+pedidoVenda.getCdpedidovenda()+"';");
					response.append("window.location = '" + retorno + "&cdpedidovendaimprimir=" + pedidoVenda.getCdpedidovenda() + "';");
				} else {
					response.append("window.open('../../faturamento/relatorio/Pedidovenda?ACAO=gerar&cdpedidovenda="+pedidoVenda.getCdpedidovenda() + "');");
					response.append("window.location = '" + retorno + "';");
				}
			} else {
				response.append("window.location = '" + retorno + "';");
			}
			
			response.append("if("+parameters.getCloseOnSave()+"){window.close();}");
			response.append("</script>");
			response.append("</body></html>");
			View.getCurrent().println(response.toString());
		}
		return null;
	}
	
	public void salvaPedidoHistorico(Pedidovenda pedidovenda, String observacaoHistorico){
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		
		pedidovendahistorico.setPedidovenda(pedidovenda);
		pedidovendahistorico.setAcao("Criado");
		
		String obs = pedidovenda.getObservacao();
		if(org.apache.commons.lang.StringUtils.isNotBlank(observacaoHistorico)){
			obs = observacaoHistorico + (obs != null ? obs : "");
		}
		
		if(pedidovenda.getVendaorcamento() != null){
			pedidovendahistorico.setObservacao("Cria��o a partir do or�amento: <a href=\"javascript:visualizarVendaorcamento(" + 
					pedidovenda.getVendaorcamento().getCdvendaorcamento() + 
					");\">" + 
					pedidovenda.getVendaorcamento().getCdvendaorcamento() + 
					"</a>");
			
			if(obs != null){
				pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + " " + obs);
			}
		}else if(org.apache.commons.lang.StringUtils.isNotBlank(pedidovenda.getWhereInOSV())){
			StringBuilder obsHistoricoVenda = new StringBuilder();
			for(String id : pedidovenda.getWhereInOSV().split(",")){
				if(!obsHistoricoVenda.toString().equals("")) obsHistoricoVenda.append(", ");
				obsHistoricoVenda.append(" <a href=\"javascript:submitFormLoginVeterinaria('/w3erpveterinaria/veterinaria/crud/Ordemservicoveterinaria?ACAO=consultar&cdordemservicoveterinaria=" + 
						id + 
						"');\">" + 
						id + 
						"</a>");
				
			}
			pedidovendahistorico.setObservacao("Cria��o a partir da(s) Ordem(ns) de Servi�o Veterinaria: " + obsHistoricoVenda.toString());
			if(obs != null){
				pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + ". " + obs);
			}
		}else {
			pedidovendahistorico.setObservacao(obs);
		}
		
		if(pedidovenda.getConfirmadoparceladiferenteproduto() != null && pedidovenda.getConfirmadoparceladiferenteproduto()){
			pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() != null ? pedidovendahistorico.getObservacao() +  " (Usu�rio confirmou os valores das parcelas)" : "(Usu�rio confirmou os valores das parcelas)");
		}
		
		if(pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			StringBuilder historicoTroca = new StringBuilder();
			for(Pedidovendamaterial vm : pedidovenda.getListaPedidovendamaterial()){
				if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
					if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
					historicoTroca.append(vm.getHistoricoTroca());
				}
			}
			if(!"".equals(historicoTroca.toString())){
				if(pedidovendahistorico.getObservacao() != null && !"".equals(pedidovendahistorico.getObservacao())){
					pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + " <br>Trocas de materiais efetuadas: " + historicoTroca.toString());
				}else {
					pedidovendahistorico.setObservacao("Trocas de materiais efetuadas: " + historicoTroca.toString());
				}
			}
		}
		
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
	}
	
	public void salvarCrud(WebRequestContext request, Pedidovenda bean) throws Exception {
		List<String> listaMotivosAguardandoAprovacao = new ArrayList<String>();
		
		Pedidovenda aux = this.load(bean, "pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao");
		
		//Tratamento de tela obsoleta
		if(bean.getPedidovendasituacao() != null && !bean.getPedidovendasituacao().equals(aux.getPedidovendasituacao())){
			if(Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(bean.getPedidovendasituacao())){
				throw new SinedException("O pedido de venda possui situa��o diferente de 'AGUARDANDO APROVA��O'.");
			}
		}
		String externo = parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO);
		if(externo.equals("TRUE")){
			if(this.isIdentificadorExternoRepetido(bean)){
				throw new SinedException(MSG_IDENTIFICADOR_EXTERNO);
			}
		}
		
		List<Pedidovendamaterial> listaVendamaterialForCriarTabela = this.getListaPedidovendamaterialForCriarTabela(bean);
		
		boolean fromEditar = bean.getCdpedidovenda() != null;
		if(bean.getColaboradoraux() != null && bean.getColaboradoraux().getCdpessoa() != null){
			bean.setColaborador(bean.getColaboradoraux());
		}else{
			if(bean.getColaborador() == null)
				bean.setColaborador(SinedUtil.getUsuarioComoColaborador());
		}
		
		boolean existDocumento = false;
		boolean existePedidovendapagamentoSemDocumento = false;
		boolean pedidovendaaprovado = false;
		boolean aguardandoaprovacao = false;
		boolean sincronizarComWMS = false;
		boolean gerarreceita = false;
		boolean comodato = false;
		boolean previstaaguardandoaprovacao = false;
		boolean limitecreditocliente = false;
		boolean validadeLimiteCreditoExpirada = false;
		boolean requeraprovacaopedido = false;
		boolean requeraprovacaovalorvendaminimo = false;
		boolean prazomaiorprazotabela = this.isPrazomaiorprazotabela(bean);
		boolean representacao = false;
		String materiaisWithValorAbaixoMinimo = "";
		boolean aguardandoAprovacaoIpi = false;
		boolean compraAbaixoDoMarkup = false;
		
		boolean coletaautomatica = false;
		boolean producaoautomatica = false;
		StringBuilder observacaoHistorico = new StringBuilder();
		boolean garantia = false;
		boolean isAtualizaEcommerceNaAprovacaoVenda = false;
		Pedidovendatipo tipo = null;
		if (bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getCdpedidovendatipo() != null){
			tipo = pedidovendatipoService.load(bean.getPedidovendatipo());
			garantia = Boolean.TRUE.equals(tipo.getGarantia());
			isAtualizaEcommerceNaAprovacaoVenda = Boolean.TRUE.equals(tipo.getAtualizarPedidoEcommerceStatusAoAprovar());
		}
		if(fromEditar){
			
			this.buscarTabelaPrecoVenda(bean);
			
			if (bean.getListaPedidovendapagamento() != null && !bean.getListaPedidovendapagamento().isEmpty()){
				for (Pedidovendapagamento pvp : bean.getListaPedidovendapagamento()){
					if(pvp.getDocumento() != null || pvp.getDocumentoantecipacao() != null){
						existDocumento = true;
					}
					if(pvp.getDocumento() == null && pvp.getDocumentoantecipacao() == null){
						existePedidovendapagamentoSemDocumento = true;
					}
				}
			}
			
			aguardandoaprovacao = bean.getPedidovendasituacao() != null && bean.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO);
			
			if(aguardandoaprovacao && bean.getAprovarpedidovenda() != null && bean.getAprovarpedidovenda()){
				bean.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
				pedidovendaaprovado = true;
			}
			
			boolean bonificacao = false;
			if (bean.getPedidovendatipo() != null && bean.getPedidovendatipo().getCdpedidovendatipo() != null){
				sincronizarComWMS = tipo.getSincronizarComWMS() != null ? tipo.getSincronizarComWMS() : false;	
//				if(reserva){
//					if(tipo.getGerarreceita() != null && tipo.getGerarreceita()){
//						gerarreceita = true;
//					}
//				}
				if(tipo.getGeracaocontareceberEnum() != null && 
						GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA.equals(tipo.getGeracaocontareceberEnum())){
					gerarreceita = true;
				}
				bonificacao = tipo.getBonificacao() != null ? tipo.getBonificacao() : false;
				comodato = tipo.getComodato() != null ? tipo.getComodato() : false;
				if(tipo.getRequeraprovacaopedido() != null){
					requeraprovacaopedido = tipo.getRequeraprovacaopedido();
				}
				if(tipo.getRequeraprovacaovalorvendaminimo() != null){
					requeraprovacaovalorvendaminimo = tipo.getRequeraprovacaovalorvendaminimo();
				}
				representacao = tipo.getRepresentacao() != null && tipo.getRepresentacao();
				
				coletaautomatica = tipo.getColetaautomatica() != null && tipo.getColetaautomatica();
				producaoautomatica = tipo.getProducaoautomatica() != null && tipo.getProducaoautomatica();
			}
			
			Prazopagamento prazoPagamento = prazopagamentoService.load(bean.getPrazopagamento());
			if(prazoPagamento!=null && (prazoPagamento.getAvista()==null || !prazoPagamento.getAvista())){
				String whereInDocumentotipo = "";
				if(bean.getListaPedidovendapagamento() != null){
					List<Integer> idsDocumentotipo = new ArrayList<Integer>();
					for(Pedidovendapagamento pedidovendapagamento : bean.getListaPedidovendapagamento()){
						if(pedidovendapagamento.getDocumentotipo() != null &&
								pedidovendapagamento.getDocumentotipo().getCddocumentotipo() != null &&
								!idsDocumentotipo.contains(pedidovendapagamento.getDocumentotipo().getCddocumentotipo())){
							idsDocumentotipo.add(pedidovendapagamento.getDocumentotipo().getCddocumentotipo());
						}
					}
					
					if(idsDocumentotipo != null && idsDocumentotipo.size() > 0){
						whereInDocumentotipo = CollectionsUtil.concatenate(idsDocumentotipo, ",");
					}
				}
				
				ClientedevedorBean clientedevedorBean = vendaService.verificaDebitoAcimaLimiteByCliente(whereInDocumentotipo, bean.getCliente(), bean.getValorfinal());
				if(clientedevedorBean.getLimitecreditoacimapermitido() != null && clientedevedorBean.getLimitecreditoacimapermitido()){
					limitecreditocliente = true;
				}
			}
			
			if(requeraprovacaovalorvendaminimo){
				materiaisWithValorAbaixoMinimo = this.verificaMaterialWithValorAbaixoMinimo(bean);
			}
			
			if(this.existeProdutoTributadoIpiSemValorIpi(bean.getListaPedidovendamaterial(), bean.getEmpresa())){
				aguardandoAprovacaoIpi = true;
			}
			
			boolean permitevendasemanalise = false;
			if(bean.getListaPedidovendapagamento() != null){
				List<Integer> idsDocumentotipo = new ArrayList<Integer>();
				for(Pedidovendapagamento pedidovendapagamento : bean.getListaPedidovendapagamento()){
					if(pedidovendapagamento.getDocumentotipo() != null &&
							pedidovendapagamento.getDocumentotipo().getCddocumentotipo() != null &&
							!idsDocumentotipo.contains(pedidovendapagamento.getDocumentotipo().getCddocumentotipo())){
						idsDocumentotipo.add(pedidovendapagamento.getDocumentotipo().getCddocumentotipo());
					}
				}
				
				if(idsDocumentotipo != null && idsDocumentotipo.size() > 0){
					String whereInDocumentotipo = CollectionsUtil.concatenate(idsDocumentotipo, ",");
					permitevendasemanalise = documentotipoService.getPermiteVendaSemAnaliseByWhereIn(whereInDocumentotipo);
				}
			}
			
			Boolean paramLimite = parametrogeralService.getBoolean(Parametrogeral.LIMITE_CREDITO_COMPRA_CLIENTE);
			Boolean paramBloquearvendadevedor = parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_DEVEDOR);
			boolean clientepossuicontaatrasada = false;
			if(!pedidovendaaprovado && !permitevendasemanalise && (paramLimite || paramBloquearvendadevedor)){
				Prazopagamento prazopagamento = null;
				if(bean.getPrazopagamento() != null && bean.getPrazopagamento().getCdprazopagamento() != null){
					prazopagamento = prazopagamentoService.load(bean.getPrazopagamento(), "prazopagamento.cdprazopagamento, prazopagamento.avista");
				}
				if(prazopagamento == null || (prazopagamento.getAvista() == null || !prazopagamento.getAvista())){
					if(paramLimite && clienteService.getValidadeLimiteCreditoExpirada(bean.getCliente())){
						bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
						validadeLimiteCreditoExpirada = true;
					} else {
						if(paramBloquearvendadevedor && contareceberService.clientePossuiContasAtrasadas(bean.getCliente())){
							bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
							clientepossuicontaatrasada = true;
						}
					}
				}
			}
			
			// o calculo e compara��o do percentual de markup com o valor do pedido s� acontece se o usu�rio possui esse campo preenchido
			Usuario usuario = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.limitePercentualMarkupVenda");
			
			if(usuario.getLimitePercentualMarkupVenda() != null) {
				compraAbaixoDoMarkup = this.isCompraAbaixoDoMarkup(bean, usuario);
			}
			
			if((bonificacao || comodato || limitecreditocliente || validadeLimiteCreditoExpirada || clientepossuicontaatrasada 
					|| requeraprovacaopedido || prazomaiorprazotabela || aguardandoAprovacaoIpi || compraAbaixoDoMarkup 
					|| (requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !"".equals(materiaisWithValorAbaixoMinimo))) && !pedidovendaaprovado) {
				if(bean.getPedidovendasituacao() != null && Pedidovendasituacao.PREVISTA.equals(bean.getPedidovendasituacao())){
					previstaaguardandoaprovacao = true;
				}
				bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
				
				if(!materiaisWithValorAbaixoMinimo.equals("")) {
					listaMotivosAguardandoAprovacao.add("existem produtos com valor abaixo do valor m�nimo cadastrado");
				}
				
				if(compraAbaixoDoMarkup) {
					listaMotivosAguardandoAprovacao.add("o valor de markup do pedido de venda est� abaixo do permitido para o seu usu�rio");
				}
			}else{
				Boolean restricaoCliente = restricaoService.verificaRestricaoSemDtLiberacao(bean.getCliente());
				if(!aguardandoaprovacao && !restricaoCliente){
					//se n�o � bonifica��o e O cliente n�o tem restri��o, o pedido volta para situa��o PREVISTA
					bean.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
				}
				
				if(aguardandoaprovacao && requeraprovacaovalorvendaminimo){
					bean.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
				}
			}
			
			if(!pedidovendaaprovado || (parametrogeralService.getBoolean(Parametrogeral.VALIDAR_ALTERACAO_VENDA) && !aguardandoaprovacao)){ 
				if(Boolean.TRUE.equals(bean.getAprovacaopercentualdesconto())){
					bean.setPedidovendasituacao(Pedidovendasituacao.AGUARDANDO_APROVACAO);
					observacaoHistorico.append("Necessita de aprova��o do desconto. ");
					
					listaMotivosAguardandoAprovacao.add("o total de descontos do pedido de venda est� acima do permitido para o seu usu�rio");
				}
				
				if(aguardandoAprovacaoIpi && !pedidovendaaprovado){
					observacaoHistorico.append("Necessita de aprova��o referente ao produto tributado por ipi sem valor de ipi. ");
				}
			}
			
			if(garantia && SinedUtil.isRecapagem()){
				bean.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
			}
			
			if(!aguardandoaprovacao && !this.bloquearEdicaoQtde(bean)){
				boolean erro = false;
				try{
					Boolean isWms = empresaService.isIntegracaoWms(bean.getEmpresa());
					if (isWms != null && isWms){
						Pedidovenda pedidovendaSessao = new Pedidovenda((Integer) request.getSession().getAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS"));
						if(pedidovendaSessao != null && pedidovendaSessao.getCdpedidovenda() != null && bean.getCdpedidovenda().equals(pedidovendaSessao.getCdpedidovenda())){
							request.getSession().removeAttribute("pedidovendaparasincronizacaowms");
						}
						WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(bean.getEmpresa()) + "/webservices/WmsWebService");
						
						List<Pedidovendamaterial> lista = pedidovendamaterialService.findByPedidoVenda(bean);
						
						String itens = CollectionsUtil.listAndConcatenate(lista, "cdpedidovendamaterial", ",");
						
						MarcarItemPedido marcarItemPedido = new MarcarItemPedido();
						marcarItemPedido.setIn0(itens);
						
						MarcarItemPedidoResponse marcarItemPedidoResponse = stub.marcarItemPedido(marcarItemPedido);
						if(!marcarItemPedidoResponse.getOut()){
							erro = true;
						} else {
							ConfirmarExclusaoItemPedido confirmarExclusaoItemPedido = new ConfirmarExclusaoItemPedido();
							confirmarExclusaoItemPedido.setIn0(itens);
							
							ConfirmarExclusaoItemPedidoResponse confirmarExclusaoItemPedidoResponse = stub.confirmarExclusaoItemPedido(confirmarExclusaoItemPedido);
							if(!confirmarExclusaoItemPedidoResponse.getOut()){
								erro = true;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(erro){
					throw new SinedException("N�o � poss�vel editar este registro.");
				}
			}
		}
		
		if(SinedUtil.isRecapagem() && bean.getListaPedidovendamaterial() != null){
			for (Pedidovendamaterial pedidovendamaterial : bean.getListaPedidovendamaterial()) {
				if(pedidovendamaterial.getGarantiareformaitem() != null && pedidovendamaterial.getGarantiareformaitem().getCdgarantiareformaitem() != null){
					if(pedidovendamaterial.getDesconto() != null && pedidovendamaterial.getDescontogarantiareforma() != null &&
							pedidovendamaterial.getTotalproduto().getValue().doubleValue() > pedidovendamaterial.getDesconto().getValue().doubleValue() &&
							pedidovendamaterial.getDesconto().getValue().doubleValue() < pedidovendamaterial.getDescontogarantiareforma().getValue().doubleValue()){
						throw new SinedException("O campo Desconto do item  n�o pode ser menor que o desconto de garantia.");
					}
				}
				if(garantia && pedidovendamaterial.getPedidovendamaterialgarantido() != null){
					Pedidovendamaterial pedidovendamaterialgarantido = pedidovendamaterialService.findByReformapneu(null, null, null, pedidovendamaterial.getPedidovendamaterialgarantido());
					if(pedidovendamaterialgarantido.getPneu() != null &&
						((pedidovendamaterial.getPneu().getCdpneu() == null) ||!pedidovendamaterialgarantido.getPneu().getCdpneu().equals(pedidovendamaterial.getPneu().getCdpneu()))){
						throw new SinedException("O pneu do item do pedido de venda deve ser igual ao pneu do servi�o garantido selecionado.");
					}
				}
			}
		}
		
		Pedidovenda pedidovendaFound = this.load(bean, "pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao");
		if(pedidovendaFound != null && pedidovendaFound.getPedidovendasituacao() != null && (
				pedidovendaFound.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO) ||
				pedidovendaFound.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)
		)){
			throw new SinedException("Pedido de venda j� confirmado.");
		}
		
		if(bean.getListaPedidovendamaterial() != null && bean.getListaPedidovendamaterial().size() > 0){
			for (Pedidovendamaterial item : bean.getListaPedidovendamaterial()) {
				if (item.getPneu() != null) {
					if(Boolean.TRUE.equals(bean.getOrigemOtr())){//Para o segmento otr, o pneu s� sleciona pneu j� persistido no banco. Se n�o tem dados pneu, � porque � servi�o sem pneu
						if(!Util.objects.isPersistent(item.getPneu())){
							item.setPneu(null);
						}
					}else if(item.getPneu().existeDados()){
							pneuService.saveOrUpdate(item.getPneu());
					}else {
						item.setPneu(null);
					}
				}
				if(parametrogeralService.getBoolean(Parametrogeral.RECAPAGEM)){
					if(item.getGarantiareformaitem() != null && item.getGarantiareformaitem().getCdgarantiareformaitem() != null){
						Garantiareformaitem garantiareformaitem = garantiareformaitemService.load(item.getGarantiareformaitem());
						garantiareformaitemService.utilizarGarantiaNoPedido(garantiareformaitem, item);
					}else{
						garantiareformaitemService.retiraVinculoGarantiaPedidovendamaterial(item);
					}
				}
				
				if(item.getDtprazoentrega() == null && org.apache.commons.lang.StringUtils.isNotBlank(item.getDtprazoentregaStr())) {
					try {
						item.setDtprazoentrega(new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy").parse(item.getDtprazoentregaStr()).getTime()));				
					} catch(ParseException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		this.setCustooperacional(bean);
		this.setFaixaPedidovendaMaterial(bean);
		
		
		try {
			this.saveOrUpdate(bean);
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "COLETAMATERIALPEDIDOVENDAMATERIAL_PEDIDOVENDAMATERIAL_FK")) {
				throw new SinedException("Item n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
			} else {
				throw new SinedException(e.getMessage());
			}
		}
		
		Pedidovendahistorico pedidovendahistorico = new Pedidovendahistorico();
		if(aguardandoaprovacao || fromEditar){
			if(pedidovendaaprovado){
				pedidovendahistorico.setAcao(Pedidovendahistorico.APROVADO);
			}else{
				pedidovendahistorico.setAcao("Alterado");
			}
			
			pedidovendahistorico.setObservacao(observacaoHistorico.toString());
			
			if(bean.getConfirmadoparceladiferenteproduto() != null && bean.getConfirmadoparceladiferenteproduto()){
				pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() != null ? pedidovendahistorico.getObservacao() +  " (Usu�rio confirmou os valores das parcelas)" : "(Usu�rio confirmou os valores das parcelas)");
			}
			
			if(prazomaiorprazotabela && !pedidovendaaprovado){
				pedidovendahistorico.setObservacao("Aguardando aprova��o devido a mudan�a no prazo de pagamento. " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			if(requeraprovacaovalorvendaminimo && materiaisWithValorAbaixoMinimo != null && !"".equals(materiaisWithValorAbaixoMinimo)){
				pedidovendahistorico.setObservacao("Aguardando aprova��o por ter material com valor de venda abaixo do m�nimo. (Materiais: " + materiaisWithValorAbaixoMinimo + "). " + (bean.getObservacao() != null ? bean.getObservacao() : ""));
			}
			if(validadeLimiteCreditoExpirada){
				pedidovendahistorico.setObservacao("Cliente com validade do limite de cr�dito expirada. " + (pedidovendahistorico.getObservacao() != null ? pedidovendahistorico.getObservacao() : ""));
			}
			
			if(bean.getListaPedidovendamaterial() != null && !bean.getListaPedidovendamaterial().isEmpty()){
				StringBuilder historicoTroca = new StringBuilder();
				for(Pedidovendamaterial vm : bean.getListaPedidovendamaterial()){
					if(vm.getHistoricoTroca() != null && !"".equals(vm.getHistoricoTroca())){
						if(!"".equals(historicoTroca.toString())) historicoTroca.append("<br>");
						historicoTroca.append(vm.getHistoricoTroca());
					}
				}
				if(!"".equals(historicoTroca.toString())){
					if(pedidovendahistorico.getObservacao() != null && !"".equals(pedidovendahistorico.getObservacao())){
						pedidovendahistorico.setObservacao(pedidovendahistorico.getObservacao() + " <br>Trocas de materiais efetuadas: " + historicoTroca.toString());
					}else {
						pedidovendahistorico.setObservacao("Trocas de materiais efetuadas: " + historicoTroca.toString());
					}
				}
			}
			
		}
		
		//Se for reserva gera os documentos a receber
//		if (((reserva && gerarreceita) || gerarcontareceber) && !fromEditar){
		if (gerarreceita && (!existDocumento || existePedidovendapagamentoSemDocumento) && (!fromEditar || (pedidovendaaprovado && 
				bean.getPedidovendasituacao() != null && !Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(bean.getPedidovendasituacao())))){
			Empresa empresa = empresaService.loadComContagerencialCentrocusto(bean.getEmpresa());
			List<Rateioitem> listarateioitem = this.prepareAndSaveVendaMaterial(bean, false, empresa, false, null);
			if(!representacao){
				this.prepareAndSaveReceita(bean, listarateioitem, empresa);
			}
			this.prepareAndSaveReceitaRepresentacao(bean, listarateioitem, empresa, false);
			Boolean registrarValecompra = false;
			String contasareceber = "";
			for (Pedidovendapagamento pvp : bean.getListaPedidovendapagamento()){
				if(pvp.getDocumento() != null){
					if("".equals(contasareceber)) contasareceber = " Contas a receber geradas: ";
					contasareceber += "<a href= '/"+NeoWeb.getApplicationName()
								+"/financeiro/crud/Contareceber?ACAO=consultar&cddocumento=" 
									+ pvp.getDocumento().getCddocumento() 
									+ "'> "
									+ pvp.getDocumento().getCddocumento() 
									+ " </a>,";
				}
				if(pvp.getDocumento()!=null || pvp.getDocumentoantecipacao()!=null){
					registrarValecompra = true;					
				}
			}
			
			if(registrarValecompra){
				this.registrarCreditoValecompra(bean);	
			}
			pedidovendahistorico.setObservacao((pedidovendahistorico.getObservacao() != null ? pedidovendahistorico.getObservacao() : "") +
					(contasareceber.length() > 0 ? contasareceber.substring(0, contasareceber.length()-1) : ""));		
		}else{
			if(existDocumento && pedidovendaaprovado){
				for (Pedidovendapagamento pvp : bean.getListaPedidovendapagamento()){
					if(pvp.getDocumentoantecipacao() != null){
						this.verificaComissionamento(pvp, bean.getListaPedidovendapagamento().size(), null, null, null,false);
					}
				}
			}
		}

		this.gerarReservaAoSalvar(tipo, bean);

		pedidovendahistorico.setPedidovenda(bean);
		pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
		
		boolean registrarValecompra = false;
		String param_utilizacao_valecompra = parametrogeralService.getValorPorNome(Parametrogeral.UTILIZACAO_VALECOMPRA);
		if("PEDIDO".equalsIgnoreCase(param_utilizacao_valecompra)){
			registrarValecompra = true;
			
			Valecompra valecompra = valecompraService.findByPedidovenda(bean, Tipooperacao.TIPO_DEBITO);
			if(bean.getPedidovendasituacao() != null && Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(bean.getPedidovendasituacao())){
				registrarValecompra = false;
				if(valecompra != null && valecompra.getCdvalecompra() != null){
					valecompraService.delete(valecompra);
				}
			}else if(valecompra != null){
				registrarValecompra = false;
				valecompraService.updateSaldoByValecompra(valecompra, bean.getValorusadovalecompra());
			}
		}
		
		if(registrarValecompra){
			this.registrarUsoValecompra(bean);
		}
		
		if(pedidovendaaprovado){
			this.verificarAndCriarTabelaprecoCliente(request, bean, listaVendamaterialForCriarTabela);
		}
		
		if(fromEditar){
			if(SinedUtil.isSegmentoOtr()){
				this.associarItemAColetasJaCriadas(bean);
			}
			try{
				Boolean isWms = empresaService.isIntegracaoWms(bean.getEmpresa());
				if (isWms != null && isWms && sincronizarComWMS && !this.bloquearEdicaoQtde(bean)){
					bean = this.loadForEntrada(bean);
					bean.setListaPedidovendahistorico(pedidovendahistoricoService.findByPedidovenda(bean));
					bean.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(bean));
					if(aguardandoaprovacao && !pedidovendaaprovado){
						if(previstaaguardandoaprovacao)
							this.integracaoWMS(bean, "Pedido de venda estava previsto e foi para aguardando aprova��o (alterado) ", true);
						else
							this.integracaoWMS(bean, "Pedido de venda ainda aguardando aprova��o (alterado) ", true);
					}else {
						this.integracaoWMS(bean, (pedidovendaaprovado ? "Pedido de venda aprovado (aprovado/editado) " : "Pedido de venda alterado "), true);
						this.updateIntegrar(bean, Boolean.TRUE);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(coletaautomatica){
				coletaautomatica = false;
				
				if(SinedUtil.isListNotEmpty(bean.getListaPedidovendamaterial())){
					List<Pedidovendamaterial> listaAuxiliar = new ArrayList<Pedidovendamaterial>();
					for(Pedidovendamaterial pedidovendamaterial : bean.getListaPedidovendamaterial()){
						if(pedidovendamaterial.getCdpedidovendamaterial() != null && pedidovendamaterial.getMaterialcoleta() != null){
							listaAuxiliar.add(pedidovendamaterial);
						}
					}
					if(SinedUtil.isListNotEmpty(listaAuxiliar)){
						coletaautomatica = coletaMaterialService.existeItemSemColeta(listaAuxiliar);
					}
				}
			}
			if(producaoautomatica){
				producaoautomatica = false;
				
				StringBuilder whereInPedidovendamaterial = new StringBuilder();
				if(SinedUtil.isListNotEmpty(bean.getListaPedidovendamaterial())){
					for(Pedidovendamaterial pedidovendamaterial : bean.getListaPedidovendamaterial()){
						if(pedidovendamaterial.getCdpedidovendamaterial() != null){
							whereInPedidovendamaterial.append(pedidovendamaterial.getCdpedidovendamaterial()).append(",");
						}
					}
					if(!whereInPedidovendamaterial.toString().equals("")){
						producaoautomatica = producaoagendamaterialService.existeItemSemProducao(whereInPedidovendamaterial.substring(0, whereInPedidovendamaterial.length()-1));
					}
				}
			}
			
			request.setAttribute("coletaautomatica", coletaautomatica);
			request.setAttribute("producaoautomatica", producaoautomatica);
			
			Boolean emitirComprovanteAguardandoAprovacao = parametrogeralService.getBoolean("EMITIR_COMPROVANTE_AGUARDANDOAPROVACAO");
			String mensagemMotivoAguardandoAprovacao = this.montaMensagemMotivosAguardandoAprovacao(listaMotivosAguardandoAprovacao);
			
			if(org.apache.commons.lang.StringUtils.isNotBlank(mensagemMotivoAguardandoAprovacao)) {
				mensagemMotivoAguardandoAprovacao = "Este pedido de venda ficar� como Aguardando Aprova��o porque " + mensagemMotivoAguardandoAprovacao;
				
				if(emitirComprovanteAguardandoAprovacao) {
					mensagemMotivoAguardandoAprovacao += ".";
				} else {
					int tamanho = listaMotivosAguardandoAprovacao.size();
					mensagemMotivoAguardandoAprovacao += ". Por " + (tamanho > 1 ? "esses motivos" : "esse motivo") +
							" n�o ser� poss�vel a emiss�o ou envio de comprovante at� a sua aprova��o.";
				}
				
				bean.getMensagemAposSalvar().add(new Message(MessageType.WARN, mensagemMotivoAguardandoAprovacao));

				if(!Boolean.TRUE.equals(bean.getFromWebService())){
					HttpSession sessao = NeoWeb.getRequestContext().getSession();
					sessao.setAttribute("mensagemMotivoAguardandoAprovacao", mensagemMotivoAguardandoAprovacao);
				}
			}
			
		}
		
	}
	
	public void associarItemAColetasJaCriadas(Pedidovenda bean){
		Map<Pneu, List<PneuItemVendaInterface>> mapa = vendaService.criaMapaAgrupamentoItensPorPneu(bean.getListaPneuItemInterface());
		for(Entry<Pneu, List<PneuItemVendaInterface>> entry: mapa.entrySet()){
			for(PneuItemVendaInterface item: entry.getValue()){
				Pedidovendamaterial pvm = (Pedidovendamaterial)item;
				if(!materialService.isMaterialProducao(pvm.getMaterial())){
					continue;
				}
				ColetaMaterial coletaMaterial = coletaMaterialService.loadColetaByPneuAndMaterialcoleta(bean, pvm.getPneu(), pvm.getMaterialcoleta());
				if(Util.objects.isPersistent(coletaMaterial) && !coletaMaterialService.haveColetaMaterialByPedidovendamaterialOtr(pvm)){
					ColetaMaterialPedidovendamaterial novoVinculo = new ColetaMaterialPedidovendamaterial();
					novoVinculo.setColetaMaterial(coletaMaterial);
					novoVinculo.setPedidovendamaterial(pvm);
					coletaMaterialPedidovendamaterialService.saveOrUpdate(novoVinculo);
				}
			}
		}
	}
	
	public Pedidovenda loadBeanForEntrada(WebRequestContext request, Pedidovenda form, PedidoVendaParametersBean parameters){
		boolean fromWebService = Boolean.TRUE.equals(parameters.getFromWebService());
		PedidoVendaParametroBean parametro = new PedidoVendaParametroBean();
		if(fromWebService && form.getCdpedidovenda() != null){
			form = this.loadForEntrada(form);
		}
		form.setIncluirEnderecoFaturamento(Util.objects.isPersistent(form.getEnderecoFaturamento()));
		Usuario usuario = SinedUtil.getUsuarioLogado();
		Boolean confirmarPercentualPedidoVenda = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMARPERCENTUALPEDIDOVENDA));
		Boolean confirmarPvServicoProduto = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.CONFIRMAR_PV_SERVICO_PRODUTO));
		Boolean replicarMaterial = "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.REPLICAR_MATERIAL_PEDIDO_VENDA));
		Boolean closeOnSave = parameters.getCloseOnSave();
		Boolean emitirComprovante = parameters.getEmitirComprovante();
		
		if(SinedUtil.isListNotEmpty(form.getListaPedidovendamaterial())){
			for (Pedidovendamaterial pedidovendamaterial : form.getListaPedidovendamaterial()) {
				if(Util.objects.isPersistent(pedidovendamaterial.getLoteestoque())){
					pedidovendamaterial.setLoteestoque(loteestoqueService.loadWithNumeroValidade(pedidovendamaterial.getLoteestoque(), pedidovendamaterial.getMaterial(), false));
				}
			}			
		}
		
		if(!fromWebService){
			HttpSession sessao = NeoWeb.getRequestContext().getSession();
			String mensagemMotivoAguardandoAprovacao = (String) sessao.getAttribute("mensagemMotivoAguardandoAprovacao");
			
			if(org.apache.commons.lang.StringUtils.isNotBlank(mensagemMotivoAguardandoAprovacao)) {
				request.setAttribute("mensagemMotivoAguardandoAprovacao", mensagemMotivoAguardandoAprovacao);
				sessao.removeAttribute("mensagemMotivoAguardandoAprovacao");
			}
			
			vendaService.setAtributeDimensaovenda(request);
			request.setAttribute("ignoreHackAndroidDownload", true);
			
			request.setAttribute("closeOnSave", closeOnSave);
			request.setAttribute("emitirComprovante", emitirComprovante);
			
			if(Util.objects.isPersistent(form) && this.isPedidoEcommerce(form)){
				request.setAttribute("listaPedidoVendaTipo", pedidovendatipoService.findForCombo());
			}else{
				request.setAttribute("listaPedidoVendaTipo", pedidovendatipoService.findForComboWithoutVendaEcommerce());
			}
			if(request.getBindException().hasErrors()){
				request.setAttribute("ACAO", form.getAcao());
				request.setAttribute("showCancelLink", form.getShowCancelLink());
				
				if(form.getColaboradoraux() != null && form.getColaboradoraux().getCdpessoa() != null){
					form.setColaborador(form.getColaboradoraux());
				}else{
					if(form.getColaborador() != null){
						form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
					}
				}
				if(form.getCliente() != null){
					form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
				}
			}
			
			request.setAttribute("listaCentroCusto", centrocustoService.findAtivos());
			request.setAttribute("negociarPendencia", parametrogeralService.getBoolean("PERMITIR_NEGOCIAR_PENDENCIA_PEDIDO"));
			request.setAttribute("coletaautomatica", parameters.getColetaAutomatica());
			request.setAttribute("producaoautomatica", parameters.getProducaoAutomatica());
			
			
			String paramExibirValorMinMaxVenda = parametrogeralService.getValorPorNome(Parametrogeral.EXIBIRVALORMINMAXVENDA);
			request.setAttribute("EXIBIRVALORMINMAXVENDA", paramExibirValorMinMaxVenda.toUpperCase().equals("TRUE"));
			
			request.setAttribute("PARAMETRO_RECAPAGEM", parametrogeralService.getValorPorNome(Parametrogeral.RECAPAGEM));
			request.setAttribute("PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO));
			
			request.setAttribute("CONFIRMARPERCENTUALPEDIDOVENDA", confirmarPercentualPedidoVenda);
			request.setAttribute("CONFIRMAR_PV_SERVICO_PRODUTO", confirmarPvServicoProduto);
			request.setAttribute("REPLICAR_MATERIAL_PEDIDO_VENDA", replicarMaterial);
		}else{
			HttpSession sessao = NeoWeb.getRequestContext().getSession();
			String mensagemMotivoAguardandoAprovacao = (String) sessao.getAttribute("mensagemMotivoAguardandoAprovacao");
			
			parametro.setMensagemMotivoAguardandoAprovacao(mensagemMotivoAguardandoAprovacao);
			
			
			parametro.setDimensaoVenda1(parametrogeralService.getDimensaovenda(0));
			parametro.setDimensaoVenda2(parametrogeralService.getDimensaovenda(1));
			parametro.setDimensaoVenda2(parametrogeralService.getDimensaovenda(2));
			parametro.setIgnoreHackAndroidDownload(true);
			
			parametro.setCloseOnSave(closeOnSave);
			parametro.setEmitirComprovante(emitirComprovante);
			if(request.getBindException().hasErrors()){
				request.setAttribute("ACAO", form.getAcao());
				request.setAttribute("showCancelLink", form.getShowCancelLink());
				
				if(form.getColaboradoraux() != null && form.getColaboradoraux().getCdpessoa() != null){
					form.setColaborador(form.getColaboradoraux());
				}else{
					if(form.getColaborador() != null){
						form.setColaborador(colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
					}
				}
				if(form.getCliente() != null){
					form.setCliente(clienteService.load(form.getCliente(), "cliente.cdpessoa, cliente.nome"));
				}
			}

			
			//request.setAttribute("listaCentroCusto", centrocustoService.findAtivos());
			parametro.setNegociarPendencia(parametrogeralService.getBoolean("PERMITIR_NEGOCIAR_PENDENCIA_PEDIDO"));
			parametro.setColetaautomatica(parameters.getColetaAutomatica());
			parametro.setProducaoautomatica(parameters.getProducaoAutomatica());
			
			parametro.setExibirValorMinMaxVenda(parametrogeralService.getBoolean(Parametrogeral.EXIBIRVALORMINMAXVENDA));
			
			parametro.setParametroRecapagem(parametrogeralService.getBoolean(Parametrogeral.RECAPAGEM));
			parametro.setPedidoBuscaIdentificadorTabelaPreco(parametrogeralService.getBoolean(Parametrogeral.PEDIDOVENDA_BUSCA_IDENTIFICADOR_TABELA_PRECO));
			
			parametro.setConfirmarPercentualPedidoVenda(confirmarPercentualPedidoVenda);
			parametro.setConfirmarPvServicoProduto(confirmarPvServicoProduto);
			parametro.setReplicarMaterial(replicarMaterial);
		}
		
		Cliente cliente = null;
		if(parameters.getCdCliente() != null) {
			cliente = new Cliente(parameters.getCdCliente());
			cliente = ClienteService.getInstance().loadForProcessFromPainelInteracao(cliente);
			form.setCliente(cliente);
		}
		
		
		
		
		Empresa empresaPrincipal = empresaService.loadPrincipalOrEmpresaUsuario();
		if(empresaPrincipal.getProximoidentificadorpedidovenda() == null){
			form.setIdentificadorAutomatico(Boolean.FALSE);
		}
		
		Usuario usuario_aux = usuarioService.load(usuario, "usuario.cdpessoa, usuario.limitepercentualdesconto");
		form.setLimitepercentualdesconto(usuario_aux.getLimitepercentualdesconto());
		
		if(form.getCdpedidovenda() == null){
			if(form.getPedidovendatipo() == null){
				form.setPedidovendatipo(pedidovendatipoService.loadPrincipal());
			}
			request.getSession().removeAttribute("cdpedidovendaforcomodato");
			if(request.getSession().getAttribute("empresaSelecionada") == null && form.getEmpresa() == null)
				form.setEmpresa(empresaPrincipal);
		
			form.setColaborador(form.getColaborador()!= null ? colaboradorService.loadWithoutPermissao(form.getColaborador(), "colaborador.cdpessoa, colaborador.nome") : SinedUtil.getUsuarioComoColaborador());
			form.setPedidovendasituacao(Pedidovendasituacao.PREVISTA);
						
			Boolean isWms = empresaService.isIntegracaoWms(form.getEmpresa());
			request.setAttribute("WMS_INTEGRACAO", isWms != null && isWms ? "TRUE" : "FALSE");
			
			if(form.getLocalarmazenagem() == null){
				request.setAttribute("buscarLocalUnico", true);
				if(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null && new SinedUtil().getUsuarioPermissaoEmpresa(form.getEmpresa())){
					List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(form.getEmpresa());
					if(listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1){
						form.setLocalarmazenagem(listaLocalarmazenagem.get(0));
					}
				}
			}
			
			if(org.apache.commons.lang.StringUtils.isNotBlank(parameters.getWhereInOrdemservicoveterinaria())){
				this.montarVendaOrdemServicoVeterinaria(parameters.getWhereInOrdemservicoveterinaria(), form);
			}
			
			
			if(parameters.getCdVendaOrcamento() != null){
				Vendaorcamento vendaorcamento = vendaorcamentoService.loadForEntrada(new Vendaorcamento(parameters.getCdVendaOrcamento()));
				if (vendaorcamento.getColaborador()!=null && vendaorcamento.getColaborador().getCdpessoa()!=null){
					vendaorcamento.setColaborador(colaboradorService.loadWithoutPermissao(vendaorcamento.getColaborador(), "colaborador.cdpessoa, colaborador.nome"));
				}
				if (vendaorcamento.getCliente()!=null && vendaorcamento.getCliente().getCdpessoa()!=null){
					vendaorcamento.setCliente(clienteService.load(vendaorcamento.getCliente(), "cliente.cdpessoa, cliente.nome"));
				}
				if (vendaorcamento.getEmpresa()!=null && vendaorcamento.getEmpresa().getCdpessoa()!=null){
					vendaorcamento.setEmpresa(empresaService.loadForVenda(vendaorcamento.getEmpresa()));
				}
				if (vendaorcamento.getAgencia() != null && vendaorcamento.getAgencia().getCdpessoa() != null) {
					vendaorcamento.setAgencia(fornecedorService.load(vendaorcamento.getAgencia(), "fornecedor.cdpessoa, fornecedor.nome, fornecedor.cpf, fornecedor.cnpj"));
				}
				
				vendaorcamento.setListavendaorcamentomaterial(vendaorcamentomaterialService.findByMaterial(vendaorcamento));
				vendaorcamento.setListavendaorcamentomaterialmestre(vendaorcamentomaterialmestreService.findByVendaOrcamento(vendaorcamento));
				vendaorcamento.setListaOrcamentovalorcampoextra(orcamentovalorcampoextraService.findByVendaorcamento(vendaorcamento));
				vendaorcamentoService.verificaTrocaMaterialsimilar(vendaorcamento.getListavendaorcamentomaterial());
				
				form.setEmpresa(vendaorcamento.getEmpresa());
				form.setPedidovendatipo(vendaorcamento.getPedidovendatipo());
				form.setCliente(vendaorcamento.getCliente());
				form.setContato(vendaorcamento.getContato());
				form.setEndereco(vendaorcamento.getEndereco());
				form.setEnderecoFaturamento(vendaorcamento.getEnderecoFaturamento());
				form.setColaborador(vendaorcamento.getColaborador());
				form.setDtpedidovenda(SinedDateUtils.currentDate());
				form.setObservacao(vendaorcamento.getObservacao());
				form.setFrete(vendaorcamento.getFrete());
				form.setTerceiro(vendaorcamento.getTerceiro());
				form.setValorfrete(vendaorcamento.getValorfrete());
				form.setValorFreteCIF(vendaorcamento.getValorFreteCIF());
				form.setIdentificacaoexterna(vendaorcamento.getIdentificadorexterno());
				form.setDesconto(vendaorcamento.getDesconto());
				form.setPercentualdesconto(vendaorcamento.getPercentualdesconto());
				form.setSaldofinal(vendaorcamento.getSaldofinal());
				form.setProjeto(vendaorcamento.getProjeto());
				form.setObservacao(vendaorcamento.getObservacao());
				form.setObservacaointerna(vendaorcamento.getObservacaointerna());
				form.setLocalarmazenagem(vendaorcamento.getLocalarmazenagem());
				form.setValorusadovalecompra(vendaorcamento.getValorusadovalecompra());
				form.setVendaorcamento(vendaorcamento);
				form.setPrazopagamento(vendaorcamentoService.getPrazopagamentoConfirmacaoVenda(vendaorcamento));
				form.setListaPedidovendamaterial(vendaorcamentoService.preechePedidoVendaMaterial(vendaorcamento.getListavendaorcamentomaterial(), vendaorcamento.getEmpresa()));
				form.setListaPedidovendavalorcampoextra(vendaorcamentoService.preechePedidoVendavalorcampoextra(vendaorcamento.getListaOrcamentovalorcampoextra()));
				form.setListaPedidovendamaterialmestre(vendaorcamentoService.preechePedidovendaMaterialmestre(vendaorcamento.getListavendaorcamentomaterialmestre()));
				form.setAgencia(vendaorcamento.getAgencia());
				
				if(form.getListaPedidovendamaterial() != null && !form.getListaPedidovendamaterial().isEmpty()){
					boolean isMaterialmestregrade = false;
					for(Pedidovendamaterial pedidovendamaterial : form.getListaPedidovendamaterial()){
						if(pedidovendamaterial.getIsMaterialmestregrade() != null && pedidovendamaterial.getIsMaterialmestregrade()){
							isMaterialmestregrade = pedidovendamaterial.getIsMaterialmestregrade();
							break;
						}
					}
					request.setAttribute("isMaterialmestregrade", isMaterialmestregrade);
				}
			}
		} else {
			form.setExibeIconeNaoVisualizadoEcommerce(this.isExibeIconeNaoVisualizadoEcommerce(form));
			request.getSession().setAttribute("cdpedidovendaforcomodato", form.getCdpedidovenda());
			if (form.getEmpresa() == null){
				form.setEmpresa(empresaPrincipal);
			} 
			
			for(Pedidovendamaterial pvm: form.getListaPedidovendamaterial()){
				pedidovendamaterialService.setaProducaoGerada(pvm, form);
			}
			
			boolean showCancelLink = false;
			boolean showEditarLink = false;
			if(form.getPedidovendasituacao() != null){
				showEditarLink = (form.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) || form.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO));
			}
			form.setListaPedidovendahistorico(pedidovendahistoricoService.findByPedidovenda(form));
			form.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(form));
			form.setListaPedidoVendaNegociacao(pedidoVendaNegociacaoService.findByPedidovenda(form,true));
			form.setListaPedidovendapagamentorepresentacao(pedidovendapagamentorepresentacaoService.findPedidovendaPagamentoRepresentacaoByPedidovenda(form));
			
			if(SinedUtil.isListNotEmpty(form.getListaPedidoVendaNegociacao())){
				request.setAttribute("isNegociado", true);
			}
			request.setAttribute("showEditarLink", showEditarLink);
			
			Boolean bloquearEdicaoQtde = this.bloquearEdicaoQtde(form);
			Boolean isWms = empresaService.isIntegracaoWms(form.getEmpresa());
			if(isWms){
				request.getSession().setAttribute("integracao", true);
				request.setAttribute("showCancelLink", showCancelLink);
				
				if(!bloquearEdicaoQtde){
					if(parameters.getSincronizarPedidovenda()){
						pedidovendasincronizacaoService.desmarcarItensPedidovenda(form);
						Integer cdpedidovendasincronizacaoSessao = (Integer) request.getSession().getAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS");
						if(cdpedidovendasincronizacaoSessao != null){
							Integer cdpvs = pedidovendasincronizacaoService.getCdpedidovendasincronizacaoByPedidovenda(form);
							if(cdpvs != null && cdpvs.equals(cdpedidovendasincronizacaoSessao)){
								request.getSession().removeAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS");
							}
						}
					}else {
						if("editar".equals(parameters.getAction())){
							Integer cdpedidovendasincronizacao = pedidovendasincronizacaoService.getCdpedidovendasincronizacaoByPedidovenda(form);
							if(cdpedidovendasincronizacao != null)
								request.getSession().setAttribute("CDPEDIDOVENDASINCRONIZACAO_WMS", cdpedidovendasincronizacao);
						}
					}
				}
			} else {
				request.getSession().setAttribute("integracao", false);
			}
			
			if(isWms && form.getCdpedidovenda() != null){
				request.setAttribute("showReenviarListagem", true);
			}else {
				request.setAttribute("showReenviarListagem", false);
			}
			
			request.setAttribute("WMS_INTEGRACAO", isWms != null && isWms ? "TRUE" : "FALSE");
			
			if(!request.getBindException().hasErrors()){
				form.setAcao(Util.strings.emptyIfNull(parameters.getAction()));
				form.setShowCancelLink(showCancelLink);
			}
			
			if(form.getPedidovendatipo() != null && 
					form.getPedidovendatipo().getGeracaocontareceberEnum() != null ){
				request.setAttribute("geracaocontareceberEnum", form.getPedidovendatipo().getGeracaocontareceberEnum().ordinal());
				
				if(form.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) && form.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_PEDIDOVENDAREALIZADA)){
					showEditarLink = false;
				}
			} else {
				request.setAttribute("geracaocontareceberEnum", GeracaocontareceberEnum.APOS_VENDAREALIZADA.ordinal());
			}
			
			request.setAttribute("showEditarLink", showEditarLink);
			
			boolean bonificacaoPedidovenda = false;
			boolean reservaPedidovenda = false;
			boolean comodatoPedidovenda = false;
			boolean garantiaPedidovenda = false;
			boolean desfazerReserva = false;
			
			if(form.getPedidovendatipo() != null){
				Pedidovendatipo pedidovendatipo = pedidovendatipoService.load(form.getPedidovendatipo());
				if(pedidovendatipo != null){
					if(Boolean.TRUE.equals(pedidovendatipo.getBonificacao())){
						bonificacaoPedidovenda = true;
					}
					if(Boolean.TRUE.equals(pedidovendatipo.getReserva()) && TipoReservaEnum.PEDIDO_VENDA.equals(pedidovendatipo.getReservarApartir())){
						reservaPedidovenda = true;
						desfazerReserva = parameters.getDesfazerReserva();
						if(form.getDesfazerReserva() != null){
							desfazerReserva = form.getDesfazerReserva();
						}else {
							form.setDesfazerReserva(desfazerReserva);
						}
					}
					if(Boolean.TRUE.equals(pedidovendatipo.getComodato())){
						comodatoPedidovenda = true;
					}
					if(pedidovendatipo.getGarantia() != null){
						garantiaPedidovenda = pedidovendatipo.getGarantia();
					}
				}
			}
			
			
			request.setAttribute("bonificacaoPedidovenda", bonificacaoPedidovenda);
			request.setAttribute("reservaPedidovenda", reservaPedidovenda);
			request.setAttribute("comodatoPedidovenda", comodatoPedidovenda);
			request.setAttribute("garantiaPedidovenda", garantiaPedidovenda);
			request.setAttribute("desfazerReserva", desfazerReserva);
			
			if (form.getDocumentotipo() != null && form.getDocumentotipo().getAntecipacao() != null &&
					form.getDocumentotipo().getAntecipacao()){
				request.setAttribute("formaPagamentoAntecipacao", Boolean.TRUE);
			} else {
				request.setAttribute("formaPagamentoAntecipacao", Boolean.FALSE);
			}
			form.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.findByPedidovenda(form));
			
			verificaContasFinanceiroSitua�ao(form);
		}
		
		Boolean obrigarLote = Boolean.FALSE;
		if(form.getPedidovendatipo() != null){
			obrigarLote = form.getPedidovendatipo().getObrigarLotePedidovenda() != null 
					? form.getPedidovendatipo().getObrigarLotePedidovenda() : Boolean.FALSE;
		}
		
		request.setAttribute("obrigarLote", obrigarLote);
		if (form.getCdpedidovenda() != null && form.getListaPedidovendamaterial() != null){
			List<Pedidovendamaterial> listaPedidovendamaterialExistenciaColeta = null;
			if(form.getListaPedidovendamaterial().size() > 0){
				listaPedidovendamaterialExistenciaColeta = pedidovendamaterialService.findForExistenciaColetaProducao(CollectionsUtil.listAndConcatenate(form.getListaPedidovendamaterial(), "cdpedidovendamaterial", ","));
			}
			
			//Preenchendo a lista de separa��o para que o usu�rio consiga editar
			for (Pedidovendamaterial item : form.getListaPedidovendamaterial()){
				if(listaPedidovendamaterialExistenciaColeta != null){
					for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterialExistenciaColeta) {
						if(pedidovendamaterial.getCdpedidovendamaterial().equals(item.getCdpedidovendamaterial())){
							item.setExisteVinculoColetaProducao(Boolean.TRUE);
							request.setAttribute("haveVinculoColetaProducao", Boolean.TRUE);
						}
					}
				}
				
				if(item.getMaterial() != null){
					Material m = materialService.load(item.getMaterial(), "material.materialgrupo");
					item.setRegistraPesomedioMaterialgrupo(m != null && m.getMaterialgrupo() != null &&
																			Boolean.TRUE.equals(m.getMaterialgrupo().getRegistrarpesomedio()));
					
					if(item.getMaterial().getListaMaterialunidademedida() != null){
						proxima_unidade_conversao:
						for (Materialunidademedida unidadeconversao : item.getMaterial().getListaMaterialunidademedida()){
							for (Pedidovendamaterialseparacao itemSeparacao : item.getListaPedidovendamaterialseparacao()){
								if (itemSeparacao.getUnidademedida().equals(unidadeconversao.getUnidademedida())){
									itemSeparacao.setFracao(unidadeconversao.getFracao());
									itemSeparacao.setQtdereferencia(unidadeconversao.getQtdereferencia());
									continue proxima_unidade_conversao;
								}
							}
							
							//se chegou aqui � porque n�o tem separa��o para esta unidade de medida
							//Vou criar um novo item para que o usu�rio consiga editar caso deseje
							Pedidovendamaterialseparacao novoItem = new Pedidovendamaterialseparacao();
							novoItem.setUnidademedida(unidadeconversao.getUnidademedida());
							novoItem.setFracao(unidadeconversao.getFracao());
							novoItem.setQtdereferencia(unidadeconversao.getQtdereferencia());
							item.getListaPedidovendamaterialseparacao().add(novoItem);
						}
					}
				
					boolean incluirUnidadePrincipal = true;
					for (Pedidovendamaterialseparacao itemSeparacao : item.getListaPedidovendamaterialseparacao()){
						if(item.getMaterial().getUnidademedida() == null){
							Material material  = materialService.unidadeMedidaMaterial(item.getMaterial());
							if(material != null){
								item.getMaterial().setUnidademedida(material.getUnidademedida());
							}
						}
						if (item.getMaterial().getUnidademedida() != null && item.getMaterial().getUnidademedida().equals(itemSeparacao.getUnidademedida())){
							incluirUnidadePrincipal = false;
							itemSeparacao.setUnidademedida(new Unidademedida(item.getMaterial().getUnidademedida().getCdunidademedida(), "Quantidade restante", "Quantidade restante"));
						}
					}
					if (incluirUnidadePrincipal){
						Pedidovendamaterialseparacao novoItem = new Pedidovendamaterialseparacao();
						if(item.getMaterial().getUnidademedida() == null){
							Material material  = materialService.unidadeMedidaMaterial(item.getMaterial());
							if(material != null){
								item.getMaterial().setUnidademedida(material.getUnidademedida());
							}
						}
						novoItem.setUnidademedida(new Unidademedida(item.getMaterial().getUnidademedida().getCdunidademedida(), "Quantidade restante", "Quantidade restante"));
						novoItem.setFracao(1.0);
						novoItem.setQtdereferencia(1.0);
						item.getListaPedidovendamaterialseparacao().add(novoItem);
					}
				
					Collections.sort(item.getListaPedidovendamaterialseparacao(), new Comparator<Pedidovendamaterialseparacao>(){
	
						@Override
						public int compare(Pedidovendamaterialseparacao o1, Pedidovendamaterialseparacao o2) {
	
							if (o1.getFracao() == null || o2.getFracao() == null)
								return -1;
							
							return o2.getFracao().compareTo(o1.getFracao());
						}
						
					});
				}
			}
		}

		
	
		if(form.getListaPedidovendamaterial() != null && !form.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : form.getListaPedidovendamaterial()){
				if(pedidovendamaterial.getAltura() != null || pedidovendamaterial.getLargura() != null && pedidovendamaterial.getComprimento() != null){
					pedidovendamaterial.setMaterial(pedidovendamaterial.getMaterial().copiaMaterial());
					if(pedidovendamaterial.getMaterial().getNome() != null && !"".equals(pedidovendamaterial.getMaterial().getNome())){
						pedidovendamaterial.getMaterial().setNome(pedidovendamaterial.getMaterial().getNome() + " " + this.getAlturaLarguraComprimentoConcatenados(pedidovendamaterial, pedidovendamaterial.getMaterial()));
					}else {
						pedidovendamaterial.getMaterial().setNome(materialService.load(pedidovendamaterial.getMaterial(), "material.nome").getNome() + " " + this.getAlturaLarguraComprimentoConcatenados(pedidovendamaterial, pedidovendamaterial.getMaterial()));
					}
				}
			}
		}
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		boolean existDocumento = false;
		if (form.getCliente() != null){
			if(parametrogeralService.getBoolean(Parametrogeral.CONSIDERAR_PEDIDO_ANTECIPACAO)){
				listaDocumento = documentoService.findForAntecipacaoConsideraPedidoVenda(form.getCliente(), null, form);
			}else {
				listaDocumento = documentoService.findForAntecipacaoConsideraVenda(form.getCliente(), null);
			}
		}
		if (form.getListaPedidovendapagamento() != null && !form.getListaPedidovendapagamento().isEmpty()){
			for (Pedidovendapagamento pvp : form.getListaPedidovendapagamento()){
				if(form.getCdpedidovenda() != null && (
						(pvp.getDocumento() != null && !Documentoacao.CANCELADA.equals(pvp.getDocumento().getDocumentoacao())) || 
						(pvp.getDocumentoantecipacao() != null && !Documentoacao.CANCELADA.equals(pvp.getDocumentoantecipacao().getDocumentoacao())))){
					existDocumento = true;
				}
				if (pvp.getDocumentoantecipacao() != null && !listaDocumento.contains(pvp.getDocumentoantecipacao())){
					listaDocumento.add(pvp.getDocumentoantecipacao());
				}
			}
		}		
		
		if(parameters.getCopiar() || (!"consultar".equalsIgnoreCase(parameters.getAction()))){
			this.ajustaValorvendaMaximoMinimoItem(form);
		}
		
		Boolean prazomedio = Boolean.FALSE;
		if(form.getPrazomedio() != null && form.getPrazomedio()){
			prazomedio = Boolean.TRUE;
		}
		if(parameters.getAprovarPedidovenda()){
			form.setAprovarpedidovenda(true);
		}else{
			form.setAprovarpedidovenda(false);
		}
		
		
		
		if(form.getCliente() != null){
			List<Prazopagamento> listaPrazopagamentoCliente = clienteService.findPrazoPagamento(form.getCliente(), prazomedio, false);
			if(form.getPrazopagamento() != null){
				if(listaPrazopagamentoCliente == null){
					listaPrazopagamentoCliente = new ArrayList<Prazopagamento>();
				}
				if(!listaPrazopagamentoCliente.contains(form.getPrazopagamento())){
					Prazopagamento prazoPagamentoFound = prazopagamentoService.findByPrazoPagamento(form.getPrazopagamento());
					listaPrazopagamentoCliente.add(prazoPagamentoFound);
				}
			}
			
			PrazopagamentoFiltro prazopagamentoFiltro = new PrazopagamentoFiltro();
			prazopagamentoFiltro.setAsc(true);
			prazopagamentoFiltro.setOrderBy("prazopagamento.nome");
			//ordenaPrazoPagamento(prazopagamentoFiltro, listaPrazopagamentoCliente);
			
			request.setAttribute("listaPrazopagamento", listaPrazopagamentoCliente);
		}
		
		form.setDtprazoentrega(vendaService.calculaPrazoEntrega(form.getEmpresa()));
		String whereInCddocumentotipo = null;
		if(form.getCdpedidovenda() != null){
			whereInCddocumentotipo = this.montaWhereinDocumentotipoJaUsado(form);
		}
		
		Boolean permissaoProjeto = false;
		if(!usuario.getTodosprojetos()){				
			permissaoProjeto = org.apache.commons.lang.StringUtils.isNotBlank(SinedUtil.getListaProjeto());
			request.setAttribute("permissaoProjeto", new Boolean(permissaoProjeto));
		}
		
		if(!parameters.getCopiar()){
//			pedidovendaService.ordenarMaterialproducao(form);
		}
		
		if(form.getCliente() != null){
			form.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(form.getCliente()));
		}
		if(!fromWebService){
			request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
			List<Endereco> listaEnderecoByCliente = new ArrayList<Endereco>();
			if(form.getCliente() != null && form.getCliente().getCdpessoa() != null)
				listaEnderecoByCliente = enderecoService.findByPessoaWithoutInativo(form.getCliente());
			request.setAttribute("listaEnderecoByCliente", listaEnderecoByCliente);
			
			
			//List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
			//request.setAttribute("listaPrazopagamento", listaPrazopagamento);
			request.setAttribute("listaDocumento", listaDocumento);
			request.setAttribute("existDocumento", existDocumento);

			request.setAttribute("listaContaboleto", contaService.findContasAtivasComPermissao(form.getEmpresa(), true));
			
			request.setAttribute("listaFornecedorTransportadora", fornecedorService.findFornecedoresTransportadora(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null ? form.getEmpresa().getCdpessoa().toString() : null));
			request.setAttribute("VENDASALDOPORMINMAX", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPORMINMAX));
			request.setAttribute("VENDASALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTO));
			request.setAttribute("VENDASALDOPRODUTONEGATIVO", parametrogeralService.getValorPorNome(Parametrogeral.VENDASALDOPRODUTONEGATIVO));
			request.setAttribute("DESCONSIDERARDESCONTOSALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
			request.setAttribute("BLOQUEARVENDARESERVADA", parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEAR_VENDA_RESERVADA));
			request.setAttribute("MENSAGEMVENDA_SALDOPRODUTO", parametrogeralService.getValorPorNome(Parametrogeral.MENSAGEMVENDA_SALDOPRODUTO));
			
			ClientedevedorBean clientedevedorBean = vendaService.verificaDebitoAcimaLimiteByCliente(form.getDocumentotipo() != null ? form.getDocumentotipo().getCddocumentotipo().toString() : null, form.getCliente(), form.getValorfinal());
			request.setAttribute("BLOQUEAR_VENDA_CHEQUE", parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_CHEQUE));
			request.setAttribute("CLIENTE_POSSUICHEQUEPENDENTE", clientedevedorBean != null && clientedevedorBean.getChequependente() != null && clientedevedorBean.getChequependente());
			List<Documentotipo> listaDocumentotipo = documentotipoService.findDocumentotipoUsuarioLogadoForVenda(form.getCliente(), whereInCddocumentotipo);
			
			request.setAttribute("listaDocumentotipo", listaDocumentotipo);
			
			request.setAttribute("localvendaobrigatorio", parametrogeralService.getValorPorNome(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
			request.setAttribute("condicaopagamentoinclusaoproduto", parametrogeralService.getValorPorNome(Parametrogeral.CONDICAO_PAGAMENTO_INCLUSAO_PRODUTO));
			request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
			vendaService.addAtributoContatoForEntradaOrcamentoVendaPedido(request, form.getCliente());
			request.setAttribute("OBRIGAR_TIPOPEDIDOVENDA", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_TIPOPEDIDOVENDA));
			request.setAttribute("ALTERARQTDEITEMPRODUCAOVENDA", SinedUtil.isUserHasAction("ALTERARQTDEITEMPRODUCAOVENDA"));
			request.setAttribute(Acao.DEFINIR_LOTE_PEDIDO,  (boolean)SinedUtil.isUserHasAction(Acao.DEFINIR_LOTE_PEDIDO));
			request.setAttribute("includeAtalho", Boolean.TRUE);
			request.setAttribute("uriAtalhoPersonalizado", "/faturamento/process/Realizarpedidovenda");
			
			Usuario usuarioLogado = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.salvarAbaixoMinimoPedido");
			request.setAttribute("venderAbaixoMinimo", usuarioLogado.getSalvarAbaixoMinimoPedido() != null 
														? usuarioLogado.getSalvarAbaixoMinimoPedido() : Boolean.FALSE);
			
			request.setAttribute(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO, parametrogeralService.getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO));
			request.setAttribute(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO, parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO));
			request.setAttribute("listaPresencacompradornfe", Presencacompradornfe.getListaPresencacompradornfeForVenda());
			request.setAttribute("bloquearEdicaoQtde", this.bloquearEdicaoQtde(form));
			//request.setAttribute("haveTabelaimposto", tabelaimpostoService.haveTabelaimposto());
			request.setAttribute("EXIBIR_IPI_NA_VENDA", empresaService.exibirIpiVenda(form.getEmpresa()));
			request.setAttribute("EXIBIR_ICMS_NA_VENDA", empresaService.exibirIcmsVenda(form.getEmpresa()));
			request.setAttribute("EXIBIR_DIFAL_NA_VENDA", empresaService.exibirDifalVenda(form.getEmpresa()));
			Boolean teste = empresaService.exibirCalculoImpostosVenda(form.getEmpresa());
			request.setAttribute("EXIBIR_IMPOSTO_NA_VENDA", empresaService.exibirCalculoImpostosVenda(form.getEmpresa()));
			request.setAttribute("EXIBIR_REPLICAR_PNEU", true);
			request.setAttribute("PEDIDOVENDAMATERIAL", parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDAMATERIAL));
			request.setAttribute("VALIDADE_EXPIRADA_LIMITECREDITO", clienteService.getValidadeLimiteCreditoExpirada(form.getCliente()));
			request.setAttribute("MOSTRAR_LIMITE_CREDITO", parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO));
			request.setAttribute("obrigarEnderecocliente", form.getPedidovendatipo() != null && form.getPedidovendatipo().getObrigarEnderecocliente() != null && 
																		form.getPedidovendatipo().getObrigarEnderecocliente());
			request.setAttribute("qtdeDiaUtilPrazoEntrega", parametrogeralService.buscaValorPorNome(Parametrogeral.QTDE_DIAUTIL_PRAZOENTREGA));
			request.setAttribute("BLOQUEAR_LIMITE_DESCONTO", parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_LIMITE_DESCONTO));
			request.setAttribute("EXIBIR_ORDEM_PEDIDOVENDA", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.EXIBIR_ORDEM_PEDIDOVENDA)));
			request.setAttribute("EXIBIR_NOME_ALTERNATIVO_ALTURA_LARGURA", parametrogeralService.getBoolean(Parametrogeral.EXIBIR_NOME_ALTERNATIVO_ALTURA_LARGURA));
			request.setAttribute("COPIAR_NOMEALTERNATIVO_PARA_OBSMATERIAL", parametrogeralService.getBoolean(Parametrogeral.COPIAR_NOMEALTERNATIVO_PARA_OBSMATERIAL));
			request.setAttribute("identificadorExterno",parametrogeralService.getValorPorNome(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO));
			request.setAttribute("HISTORICO_DE_CLIENTE", parametrogeralService.getBoolean(Parametrogeral.HISTORICO_DE_CLIENTE));
			request.setAttribute("BLOQUEIO_DIMENSOES_VENDA", parametrogeralService.getBoolean(Parametrogeral.BLOQUEIO_DIMENSOES_VENDA));
			request.setAttribute("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
			request.setAttribute("SELECAO_AUTOMATICA_LOTE_FATURAMENTO", parametrogeralService.getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO));
			request.setAttribute("ALTERAR_LOTE_FATURAMENTO", SinedUtil.isUserHasAction("ALTERAR_LOTE_FATURAMENTO"));
			request.setAttribute("isTrayCorp", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE) && SinedUtil.isIntegracaoTrayCorp());
			
			String paramConsiderarFreteCIF = parametrogeralService.getValorPorNome("CONSIDERAR_FRETE_CIF");
			if(paramConsiderarFreteCIF != null) {
				request.setAttribute("CONSIDERAR_FRETE_CIF", paramConsiderarFreteCIF.equals("TRUE") ? true : false);
			}
			vendaService.setAtributoBloqueioVendaDevedor(request, form.getCliente(), null);
		}else{
			//request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
			//List<Endereco> listaEnderecoByCliente = new ArrayList<Endereco>();
			//if(form.getCliente() != null && form.getCliente().getCdpessoa() != null)
				//listaEnderecoByCliente = enderecoService.findByPessoaWithoutInativo(form.getCliente());
			//request.setAttribute("listaEnderecoByCliente", listaEnderecoByCliente);
			
			
			//List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByPrazomedio(prazomedio);
			//request.setAttribute("listaPrazopagamento", listaPrazopagamento);
			//request.setAttribute("listaDocumento", listaDocumento);
			parametro.setExistDocumento(existDocumento);

			//request.setAttribute("listaContaboleto", contaService.findContasAtivasComPermissao(form.getEmpresa(), true));
			
			//request.setAttribute("listaFornecedorTransportadora", fornecedorService.findFornecedoresTransportadora(form.getEmpresa() != null && form.getEmpresa().getCdpessoa() != null ? form.getEmpresa().getCdpessoa().toString() : null));
			parametro.setVendaSaldoPorMinMax(parametrogeralService.getBoolean(Parametrogeral.VENDASALDOPORMINMAX));
			parametro.setVendaSaldoProduto(parametrogeralService.getBoolean(Parametrogeral.VENDASALDOPRODUTO));
			parametro.setVendaSaldoProdutoNegativo(parametrogeralService.getBoolean(Parametrogeral.VENDASALDOPRODUTONEGATIVO));
			parametro.setDesconsiderarDescontoSaldoProduto(parametrogeralService.getBoolean(Parametrogeral.DESCONSIDERAR_DESCONTO_SALDOPRODUTO));
			parametro.setBloquearVendaReservada(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_RESERVADA));
			parametro.setMenagemVendaSaldoProduto(parametrogeralService.getValorPorNome(Parametrogeral.MENSAGEMVENDA_SALDOPRODUTO));
			
			ClientedevedorBean clientedevedorBean = vendaService.verificaDebitoAcimaLimiteByCliente(form.getDocumentotipo() != null ? form.getDocumentotipo().getCddocumentotipo().toString() : null, form.getCliente(), form.getValorfinal());
			parametro.setBloquearVendaCheque(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_CHEQUE));
			parametro.setClientePossuiChequePendente(clientedevedorBean != null && Boolean.TRUE.equals(clientedevedorBean.getChequependente()));
			//List<Documentotipo> listaDocumentotipo = documentotipoService.findDocumentotipoUsuarioLogadoForVenda(form.getCliente(), whereInCddocumentotipo);
			
			//request.setAttribute("listaDocumentotipo", listaDocumentotipo);
			
			parametro.setLocalVendaObrigatorio(parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
			parametro.setCondicaoPagamentoInclusaoProduto(parametrogeralService.getValorPorNome(Parametrogeral.CONDICAO_PAGAMENTO_INCLUSAO_PRODUTO));
			//request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getProjeto(), permissaoProjeto));
			//vendaService.addAtributoContatoForEntradaOrcamentoVendaPedido(request, form.getCliente());
			//parametro.setObrigarPedidoVenda(parametrogeralService.getBoolean(Parametrogeral.OBRIGAR_TIPOPEDIDOVENDA));
			parametro.setAlterarQtdeItemProducaoVenda(SinedUtil.isUserHasAction("ALTERARQTDEITEMPRODUCAOVENDA"));
			parametro.setDefinirLoteProduto(SinedUtil.isUserHasAction(Acao.DEFINIR_LOTE_PEDIDO));
			parametro.setIncludeAtalho(Boolean.TRUE);
			parametro.setUriAtalhoPersonalizado("/faturamento/process/Realizarpedidovenda");
			
			Usuario usuarioLogado = usuarioService.load(SinedUtil.getUsuarioLogado(), "usuario.cdpessoa, usuario.salvarAbaixoMinimoPedido");
			parametro.setVenderAbaixoMinimo(Boolean.TRUE.equals(usuarioLogado.getSalvarAbaixoMinimoPedido()));
			
			parametro.setUtilizarArredondamentoDesconto(parametrogeralService.getBoolean(Parametrogeral.UTILIZAR_ARREDONDAMENTO_DESCONTO));
			parametro.setCasasDecimaisArredondamento(parametrogeralService.buscaValorPorNome(Parametrogeral.CASAS_DECIMAIS_ARREDONDAMENTO));
			//request.setAttribute("listaPresencacompradornfe", Presencacompradornfe.getListaPresencacompradornfeForVenda());
			parametro.setBloqueaEdicaoQtde(this.bloquearEdicaoQtde(form));
			//request.setAttribute("haveTabelaimposto", tabelaimpostoService.haveTabelaimposto());
			parametro.setExibirIpiNaVenda(empresaService.exibirIpiVenda(form.getEmpresa()));
			parametro.setExibirReplicarPneu(true);
			parametro.setPedidoVendaMaterial(parametrogeralService.getBoolean(Parametrogeral.PEDIDOVENDAMATERIAL));
			parametro.setValidadeExpiradaLimiteCredito(clienteService.getValidadeLimiteCreditoExpirada(form.getCliente()));
			parametro.setMostrarLimiteCredito(parametrogeralService.getBoolean(Parametrogeral.MOSTRAR_LIMITE_CREDITO));
			parametro.setObrigarEnderecoCliente(form.getPedidovendatipo() != null && Boolean.TRUE.equals(form.getPedidovendatipo().getObrigarEnderecocliente()));
			parametro.setQtdeDiaUtilPrazoEntrega(parametrogeralService.buscaValorPorNome(Parametrogeral.QTDE_DIAUTIL_PRAZOENTREGA));
			parametro.setBloquearLimiteDesconto(parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_LIMITE_DESCONTO));
			parametro.setExibirOrdemPedidoVenda(parametrogeralService.getBoolean(Parametrogeral.EXIBIR_ORDEM_PEDIDOVENDA));
			parametro.setExibirNomeAlternativoAlturaLargura(parametrogeralService.getBoolean(Parametrogeral.EXIBIR_NOME_ALTERNATIVO_ALTURA_LARGURA));
			parametro.setCopiarNomeAlternativoParaObsMaterial(parametrogeralService.getBoolean(Parametrogeral.COPIAR_NOMEALTERNATIVO_PARA_OBSMATERIAL));
			parametro.setIdentificadorExterno(parametrogeralService.getBoolean(Parametrogeral.PEDIDOVENDA_IDENTIFICADOR_EXTERNO));
			parametro.setHistoricoDeCliente(parametrogeralService.getBoolean(Parametrogeral.HISTORICO_DE_CLIENTE));
			parametro.setBloqueioDimensoesVenda(parametrogeralService.getBoolean(Parametrogeral.BLOQUEIO_DIMENSOES_VENDA));
			
			parametro.setConsiderarFreteCif(parametrogeralService.getBoolean("CONSIDERAR_FRETE_CIF"));
			Boolean bloqueiovendadevedor = parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_VENDA_DEVEDOR);
			parametro.setBloquearVendaDevedor(bloqueiovendadevedor);
			
			request.setAttribute("QTDE_DISPONIVEL_CONSIDERAR_RESERVADO", parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO));
			
			if (cliente != null && bloqueiovendadevedor && parameters.getCopiar() || (!"consultar".equalsIgnoreCase(parameters.getAction()))) {
				parametro.setClientePossuiContaAtrasada(contareceberService.clientePossuiContasAtrasadas(cliente));
			}
			parametro.setPedidoVendaAprovado(Util.objects.isPersistent(form) ? this.isPedidovendaAprovado(form) : false);
			
			request.setAttribute("parametros", parametro);
		}
		
		
		return form;
	}
	
	public PedidoVendaWSResponseBean loadForEntradaWS(Pedidovenda bean){
		
		PedidoVendaWSResponseBean pedido = new PedidoVendaWSResponseBean();
		pedido.setAgencia(ObjectUtils.translateEntityToGenericBean(pedido.getAgencia()));
		pedido.setCdpedidovenda(pedido.getCdpedidovenda());
		pedido.setCentrocusto(ObjectUtils.translateEntityToGenericBean(pedido.getCentrocusto()));
		pedido.setCliente(ObjectUtils.translateEntityToGenericBean(pedido.getCliente()));
		pedido.setClienteindicacao(ObjectUtils.translateEntityToGenericBean(pedido.getClienteindicacao()));
		pedido.setConta(ObjectUtils.translateEntityToGenericBean(pedido.getConta()));
		pedido.setColaborador(ObjectUtils.translateEntityToGenericBean(pedido.getColaborador()));
		pedido.setContaboletorepresentacao(ObjectUtils.translateEntityToGenericBean(pedido.getContaboletorepresentacao()));
		pedido.setContato(ObjectUtils.translateEntityToGenericBean(pedido.getContato()));
		pedido.setDocumentotipo(ObjectUtils.translateEntityToGenericBean(pedido.getDocumentotipo()));
		pedido.setDocumentotiporepresentacao(ObjectUtils.translateEntityToGenericBean(pedido.getDocumentotiporepresentacao()));
		pedido.setEmpresa(ObjectUtils.translateEntityToGenericBean(pedido.getEmpresa()));
		pedido.setEndereco(ObjectUtils.translateEntityToGenericBean(pedido.getEndereco()));
		pedido.setFornecedor(ObjectUtils.translateEntityToGenericBean(pedido.getFornecedor()));
		pedido.setFrete(ObjectUtils.translateEntityToGenericBean(pedido.getFrete()));
		pedido.setIndicecorrecao(ObjectUtils.translateEntityToGenericBean(pedido.getIndicecorrecao()));
		pedido.setDesconto(bean.getDesconto());
		pedido.setDtpedidovenda(bean.getDtpedidovenda());
		pedido.setDtprazoentregamax(bean.getDtprazoentregamax());
		pedido.setIdentificacaoExterna(bean.getIdentificacaoexterna());
		pedido.setIdentificador(bean.getIdentificador());
		pedido.setIdentificadorcarregamento(bean.getIdentificadorcarregamento());
		pedido.setIntegrar(bean.getIntegrar());
		pedido.setLimitecreditoexcedido(bean.getLimitecreditoexcedido());
		pedido.setObservacao(bean.getObservacao());
		pedido.setObservacaointerna(bean.getObservacao());
		pedido.setOrdemcompra(bean.getOrdemcompra());
		pedido.setPercentualdesconto(bean.getPercentualdesconto());
		pedido.setPrazoentregaMinimo(bean.getPrazoentregaMinimo());
		pedido.setRestricaocliente(bean.getRestricaocliente());
		pedido.setWhereInOSV(bean.getWhereInOSV());
		pedido.setValorusadovalecompra(bean.getValorusadovalecompra());
		pedido.setValorFreteCIF(bean.getValorfrete());
		pedido.setValorfrete(bean.getValorfrete());
		pedido.setValorfinalipi(bean.getValorfinalMaisImpostos());
		pedido.setValorfinal(bean.getValorfinal());
		pedido.setValordescontorepresentacao(bean.getValordescontorepresentacao());
		pedido.setValorbrutorepresentacao(bean.getValorbrutorepresentacao());
		pedido.setValoraproximadoimposto(bean.getValordescontorepresentacao());
		pedido.setTaxapedidovenda(bean.getTaxapedidovenda());
		pedido.setPedidovendasituacao(EnumUtils.translateEnum(bean.getPedidovendasituacao()));
		pedido.setPresencacompradornfe(EnumUtils.translateEnum(bean.getPresencacompradornfe()));
		pedido.setSituacaoPendencia(EnumUtils.translateEnum(bean.getSituacaoPendencia()));
		//pedido.setTaxapedidovenda(taxapedidovenda)
		
		pedido.setListaPedidovendamaterial(pedidovendamaterialService.toWSList(bean.getListaPedidovendamaterial()));
		pedido.setListaPedidovendapagamento(pedidovendapagamentoService.toWSList(bean.getListaPedidovendapagamento()));
		pedido.setListaPedidovendahistorico(pedidovendahistoricoService.toWSList(bean.getListaPedidovendahistorico()));
		pedido.setListaPedidovendavalorcampoextra(pedidovendavalorcampoextraService.toWSList(bean.getListaPedidovendavalorcampoextra()));
		pedido.setListaPedidovendamaterialmestre(pedidovendamaterialmestreService.toWSList(bean.getListaPedidovendamaterialmestre()));
		pedido.setListaPedidovendapagamentorepresentacao(pedidovendapagamentorepresentacaoService.toWSList(bean.getListaPedidovendapagamentorepresentacao()));

		//Util.objects
		
		return pedido;
	}
	
	public void validateEditar(WebRequestContext request, Pedidovenda form, PedidoVendaParametersBean parameters){
		boolean permiteAlterarProducaoEmAndamento = parametrogeralService.getBoolean(Parametrogeral.PERMITIR_ALTERAR_PEDIDO_COM_PRODUCAO_EM_ANDAMENTO) || Boolean.TRUE.equals(form.getOrigemOtr());
		if(this.isProducaoEmAndamento(form.getCdpedidovenda().toString()) && !permiteAlterarProducaoEmAndamento){
			request.addError("N�o � poss�vel editar o pedido. A produ��o est� em andamento. Necess�rio cancelar a agenda de produ��o para realizar a edi��o.");
		}
		if(this.isProducaoEmEspera(form.getCdpedidovenda().toString()) && !permiteAlterarProducaoEmAndamento){
			request.addError("N�o � poss�vel editar o pedido. A produ��o est� em espera. Necess�rio cancelar a agenda de produ��o para realizar a edi��o.");
		}
		
		if(!parameters.getAprovarPedidovenda() && !this.bloquearEdicaoQtde(form)){
			boolean erro = false;
			try{
				List<Pedidovendamaterial> lista = pedidovendamaterialService.findByPedidoVenda(form);
				List<Pedidovendamaterial> listaIntegracao =  this.getListaIntegracaoWms(lista);
				if(listaIntegracao != null && !listaIntegracao.isEmpty()){
					WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(form.getEmpresa()) + "/webservices/WmsWebService");
					
					MarcarItemPedido marcarItemPedido = new MarcarItemPedido();
					marcarItemPedido.setIn0(CollectionsUtil.listAndConcatenate(listaIntegracao, "cdpedidovendamaterial", ","));
					
					MarcarItemPedidoResponse marcarItemPedidoResponse = stub.marcarItemPedido(marcarItemPedido);
					if(!marcarItemPedidoResponse.getOut()){
						erro = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(erro){
				request.addError("N�o � poss�vel editar este registro, ele j� se encontra em processo de expedi��o/carregamento.");
			}
		}
		
		if(emporiumpedidovendaService.havePedidovendaEnviado(form)){
			request.addError("N�o � poss�vel editar este registro. Pedido enviado ao ECF.");
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(usuarioService.isRestricaoClienteVendedor(usuario) ){
			Pedidovenda pedidovenda = this.loadWithCliente(form);
			if(pedidovenda != null && pedidovenda.getCliente() != null && pedidovenda.getCliente().getCdpessoa() != null && 
					!clienteService.isUsuarioPertenceRestricaoclientevendedor(pedidovenda.getCliente(), usuario)){
				request.addError("Usu�rio n�o tem permiss�o para editar esta venda!");
			}
		}
		
		Pedidovenda bean = this.load(form, "pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao");
		if(bean != null && Pedidovendasituacao.CONFIRMADO.equals(bean.getPedidovendasituacao())){
			request.addError("N�o � poss�vel editar este registro.");
		}
	}
	
	public boolean validateAprovar(WebRequestContext request, String itens){
		if (itens == null || itens.equals("")) {
			request.addError("Nenhum item selecionado.");
			return false;
		}
		
		if(this.havePedidovendaDiferenteSituacao(itens, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
			request.addError("Pedido(s) de venda com situa��o(�es) diferente de 'AGUARDANDO APROVA��O'.");
			return false;
		}
		
		Pedidovenda pedidovenda = this.loadWithCliente(new Pedidovenda(Integer.parseInt(itens)));
		Usuario usuario = usuarioService.load(SinedUtil.getUsuarioLogado(), 
				"usuario.cdpessoa, usuario.restricaoaprovarpedidoclientedevedor, usuario.limitePercentualMarkupVenda");
		
		if(pedidovenda != null && pedidovenda.getCliente() != null && usuario != null && usuario.getRestricaoaprovarpedidoclientedevedor() != null &&
				usuario.getRestricaoaprovarpedidoclientedevedor() &&
				contareceberService.clientePossuiContasAtrasadas(pedidovenda.getCliente())){
			request.addError("Cliente inadimplente. Este usu�rio n�o possui permiss�o para aprovar  pedido/ venda, favor entrar em contato com o administrador do sistema.");
			return false;
		}
		
		pedidovenda = this.loadWithPedidovendatipo(new Pedidovenda(Integer.parseInt(itens)));
		if(pedidovenda != null && pedidovenda.getPedidovendatipo() != null && Boolean.TRUE.equals(pedidovenda.getPedidovendatipo().getValidarestoquepedidonaaprovacao())){
			pedidovenda.setIsNotVerificarestoquePedidovenda(vendaService.isNotVerificarEstoqueByPedidovendatipo(pedidovenda.getPedidovendatipo()));
			pedidovenda.setPermitirvendasemestoque(pedidovenda.getPedidovendatipo() != null && Boolean.TRUE.equals(pedidovenda.getPedidovendatipo().getPermitirvendasemestoque()));
			List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findByPedidoVenda(pedidovenda);
			pedidovenda.setListaPedidovendamaterial(listaPedidovendamaterial);
			if(this.isQtdeSolicitadaMaiorOrIgualQtdeReservada(request, pedidovenda, true)){
				return false;
			}
		}
		
		pedidovenda = this.loadForCalculoMarkupPedido(new Pedidovenda(Integer.parseInt(itens)));
		if(usuario.getLimitePercentualMarkupVenda() != null && this.isCompraAbaixoDoMarkup(pedidovenda, usuario)) {
			request.addError("N�o � poss�vel aprovar este pedido de venda pois o markup est� abaixo do permitido para o seu usu�rio.");
			request.setAttribute("redirectToEntrada", true);
			return false;
		}
		
		return true;
	}

	public boolean validateCancelamento(WebRequestContext request, Pedidovenda pedidovenda){
		if (pedidovenda == null || pedidovenda.getIds() == null || "".equals(pedidovenda.getIds())) {
			request.addError("Nenhum item selecionado.");
			return false;
		}
		if(vendaService.isExisteECF(pedidovenda.getIds(),"pedidovenda")){
			request.addError("N�o � poss�vel cancelar este registro. Pedido enviado ao ECF.");
			return false;
		}
		boolean erro = false;
		try {
			List<Pedidovendamaterial> lista = pedidovendamaterialService.findByPedidoVenda(pedidovenda.getIds());
			List<Pedidovendamaterial> listaIntegracao = this.getListaIntegracaoWms(lista);			
			if (listaIntegracao != null && !listaIntegracao.isEmpty()){
				Map<Empresa, List<Pedidovendamaterial>> mapa = new HashMap<Empresa, List<Pedidovendamaterial>>();
				Map<Empresa, List<Pedidovendamaterial>> mapaAguardandoaprovacao = new HashMap<Empresa, List<Pedidovendamaterial>>();
				for(Pedidovendamaterial pvm : listaIntegracao){
					if(Pedidovendasituacao.AGUARDANDO_APROVACAO.equals(pvm.getPedidovenda().getPedidovendasituacao())){
						if(!mapaAguardandoaprovacao.containsKey(pvm.getPedidovenda().getEmpresa())){
							mapaAguardandoaprovacao.put(pvm.getPedidovenda().getEmpresa(), new ListSet<Pedidovendamaterial>(Pedidovendamaterial.class));
						}
						Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pvm);
						if(qtdeRestante != null && !qtdeRestante.equals(0.)){
							mapaAguardandoaprovacao.get(pvm.getPedidovenda().getEmpresa()).add(pvm);						
						}
					}else {
						if(!mapa.containsKey(pvm.getPedidovenda().getEmpresa())){
							mapa.put(pvm.getPedidovenda().getEmpresa(), new ListSet<Pedidovendamaterial>(Pedidovendamaterial.class));
						}
						Double qtdeRestante = pedidovendamaterialService.getQtdeRestante(pvm);
						if(qtdeRestante != null && !qtdeRestante.equals(0.)){
							mapa.get(pvm.getPedidovenda().getEmpresa()).add(pvm);						
						}
					}
				}
				for(Empresa empresa : mapa.keySet()){
					List<Pedidovendamaterial> listaItensCancelaveis = mapa.get(empresa);
					if(!listaItensCancelaveis.isEmpty()){
						WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(empresa) + "/webservices/WmsWebService");		
						
						String itensmaterial = CollectionsUtil.listAndConcatenate(listaItensCancelaveis, "cdpedidovendamaterial", ",");
						ConfirmarExclusaoItemPedido confirmarExclusaoItemPedido = new ConfirmarExclusaoItemPedido();
						confirmarExclusaoItemPedido.setIn0(itensmaterial);
						
						ConfirmarExclusaoItemPedidoResponse confirmarExclusaoItemPedidoResponse = stub.confirmarExclusaoItemPedido(confirmarExclusaoItemPedido);
						if(!confirmarExclusaoItemPedidoResponse.getOut()){
							erro = true;
							break;
						}
					}
				}
				
				try {
					for(Empresa empresa : mapaAguardandoaprovacao.keySet()){
						List<Pedidovendamaterial> listaItensCancelaveis = mapaAguardandoaprovacao.get(empresa);
						if(!listaItensCancelaveis.isEmpty()){
							WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(empresa) + "/webservices/WmsWebService");		
							
							String itensmaterial = CollectionsUtil.listAndConcatenate(listaItensCancelaveis, "cdpedidovendamaterial", ",");
							ConfirmarExclusaoItemPedido confirmarExclusaoItemPedido = new ConfirmarExclusaoItemPedido();
							confirmarExclusaoItemPedido.setIn0(itensmaterial);
							
							stub.confirmarExclusaoItemPedido(confirmarExclusaoItemPedido);
						}
					}
				} catch (Exception e) {}
			}
		} catch (Exception e) {
			e.printStackTrace();
			erro = true;
		}
		if(erro){
			request.addError("N�o � poss�vel cancelar este(s) registro(s).");
			SinedUtil.redirecionamento(request, request.getFirstRequestUrl());//mudou
			return false;
		}
		
		
		List<Pedidovenda> listaPedidovenda = this.findForCancelamento(pedidovenda.getIds());
		List<Pedidovenda> listaPedidovendaCancelado = new ArrayList<Pedidovenda>();
		List<Pedidovenda> listaPedidovendaConfirmadoParcialmente = new ArrayList<Pedidovenda>();
		StringBuilder idsCancelados = new StringBuilder();
		StringBuilder idsConfirmadoparcialmente = new StringBuilder(); 
		
		for (Pedidovenda pv : listaPedidovenda) {
			if(!pv.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) && !pv.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO) && 
					!pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				request.addError("Pedido de venda com situa��o diferente de 'PREVISTA' ou 'AGUARDANDO APROVA��O' ou 'CONFIRMADO PARCIALMENTE'.");
				return false;
			}
			
			if(pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				if(!"".equals(idsConfirmadoparcialmente.toString())) idsConfirmadoparcialmente.append(",");
				idsConfirmadoparcialmente.append(pv.getCdpedidovenda());
				listaPedidovendaConfirmadoParcialmente.add(pv);
			}else {
				if(!"".equals(idsCancelados.toString())) idsCancelados.append(",");
				idsCancelados.append(pv.getCdpedidovenda());
				listaPedidovendaCancelado.add(pv);
			}
			
		}
		Map<String, String> mapaRetorno = new HashMap<String, String>();
		mapaRetorno.put("idsCancelados", idsCancelados.toString());
		mapaRetorno.put("idsConfirmadoparcialmente", idsConfirmadoparcialmente.toString());
		request.setAttribute("mapaAuxiliar", mapaRetorno);
		Map<String, List<Pedidovenda>> mapaList = new HashMap<String, List<Pedidovenda>>();
		mapaList.put("listaPedidovendaConfirmadoParcialmente", listaPedidovendaConfirmadoParcialmente);
		mapaList.put("listaPedidovendaCancelado", listaPedidovendaCancelado);
		request.setAttribute("mapaList", mapaList);
		return true;
	}
	
	/**
	 *	Action que faz o cancelamento do pedido de venda.
	 *
	 * @param request
	 * @return
	 * @since 07/11/2011
	 * @author Rodrigo Freitas
	 */
	public void cancelar(WebRequestContext request, Pedidovenda pedidovenda){
		String justificativa = pedidovenda.getObservacaohistorico() != null ? pedidovenda.getObservacaohistorico() : "";
		
		Map<String, String> mapa = (Map<String, String>)request.getAttribute("mapaAuxiliar");
		String idsCancelados = mapa.get("idsCancelados");
		String idsConfirmadoparcialmente = mapa.get("idsConfirmadoparcialmente");
		Map<String, List<Pedidovenda>> mapaList = (Map<String, List<Pedidovenda>>)request.getAttribute("mapaList");
		List<Pedidovenda> listaPedidovendaCancelado = mapaList.get("listaPedidovendaCancelado");
		List<Pedidovenda> listaPedidovendaConfirmadoParcialmente = mapaList.get("listaPedidovendaConfirmadoParcialmente");
		if(!"".equals(idsCancelados)){
			this.cancelamentoByPedidovenda(idsCancelados.toString(), listaPedidovendaCancelado, justificativa);
		}
		
		if(!"".equals(idsConfirmadoparcialmente)){
			Pedidovendahistorico pedidovendahistorico;
			List<Venda> listaVendaByPedidovenda;
			boolean retornoParaConfirmado = false;
			for (Pedidovenda pv : listaPedidovendaConfirmadoParcialmente) {
				listaVendaByPedidovenda = vendaService.findByPedidovenda(pv);
				pedidovendahistorico = new Pedidovendahistorico();
				retornoParaConfirmado = false;
				if(listaVendaByPedidovenda != null && !listaVendaByPedidovenda.isEmpty()){
					for(Venda venda : listaVendaByPedidovenda){
						if(venda.getVendasituacao() != null && !Vendasituacao.CANCELADA.equals(venda.getVendasituacao())){
							retornoParaConfirmado = true;
						}
					}
				}

				if(retornoParaConfirmado){
					pedidovendahistorico.setAcao("Cancelamento dos itens restantes.");
					this.updateConfirmacao(pv.getCdpedidovenda().toString());
					this.insertPedidoConfirmadoTabelaSincronizacaoEcommerce(pedidovenda);
					
					for (Pedidovendamaterial vendamaterial : pv.getListaPedidovendamaterial()) {
						Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(vendamaterial.getMaterial(),
																						vendamaterial.getUnidademedida(),
																						vendamaterial.getQuantidade());
						reservaService.desfazerReserva(vendamaterial, vendamaterial.getMaterial(), quantidade);
					}
				}else {
					pedidovendahistorico.setAcao("Cancelado");
					this.updateCancelamento(pv.getCdpedidovenda().toString());
					this.insertPedidoCanceladoTabelaSincronizacaoEcommerce(pedidovenda);
				}
				 
				pedidovendahistorico.setPedidovenda(pv);
				pedidovendahistorico.setObservacao(justificativa);
				
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
		}
		
		List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(pedidovenda.getIds());
		for (Coleta coleta : listaColeta) {
			if(coleta.getPedidovenda() != null && 
					coleta.getPedidovenda().getPedidovendasituacao() != null && 
					coleta.getPedidovenda().getPedidovendasituacao().equals(Pedidovendasituacao.CANCELADO)){
				try {
					List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByColeta(coleta);
					if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
						movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
					}
					coletaService.delete(coleta);	
				} 
				catch (DataAccessException e){
					request.addError("N�o foi poss�vel excluir a coleta : " + coleta.getCdcoleta()+", j� possui refer�ncias em outros registros do sistema.");
				}
				catch (Exception e) {
					e.printStackTrace();
					request.addError("N�o foi poss�vel excluir a coleta : " + coleta.getCdcoleta());
				}
			}
		}
		
		List<Producaoagenda> listaProducaoagenda = producaoagendaService.findByPedidovenda(pedidovenda.getIds());
		if(listaProducaoagenda != null && listaProducaoagenda.size() > 0){
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				if(producaoagenda.getProducaoagendasituacao() != null && 
						producaoagenda.getProducaoagendasituacao().equals(Producaoagendasituacao.EM_ESPERA) &&
						producaoagenda.getPedidovenda() != null &&
						producaoagenda.getPedidovenda().getPedidovendasituacao() != null &&
						producaoagenda.getPedidovenda().getPedidovendasituacao().equals(Pedidovendasituacao.CANCELADO)){
					try {
						producaoagendaService.cancelar(null, producaoagenda.getCdproducaoagenda() + "", "Pedido de venda cancelado.", true);
					} catch (Exception e) {
						e.printStackTrace();
						request.addError("N�o foi poss�vel cancelar a(s) agenda(s) de produ��o: " + e.getMessage());
					}
				}
			}
		}
		
		this.executaCancelamentoReservaOSV(pedidovenda.getIds());
	}
	
	public boolean validateAbrirJustificativaCancelamentoPedidovenda(WebRequestContext request, String whereIn){
		if(org.apache.commons.lang.StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
			return false;
		}
		
		if(vendaService.isExisteECF(whereIn,"pedidovenda")){
			request.addError("N�o � poss�vel cancelar este registro. Pedido enviado ao ECF.");
			return false;
		}
		
		
		StringBuilder whereInCancelar = new StringBuilder();
		List<Pedidovenda> listaPedidovenda = this.findForCancelamento(whereIn);
		
		boolean registrarDevolucao = false;
		Pedidovenda pedidovendaDevolucao = null; 
		
		List<String> listaErro = new ArrayList<String>();
		
		for (Pedidovenda pv : listaPedidovenda) {
			
			if(!pv.getPedidovendasituacao().equals(Pedidovendasituacao.PREVISTA) && 
					!pv.getPedidovendasituacao().equals(Pedidovendasituacao.AGUARDANDO_APROVACAO) && 
					!pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				request.addError("Pedido de venda com situa��o diferente de 'PREVISTA' ou 'AGUARDANDO APROVA��O' ou 'CONFIRMADO PARCIALMENTE.");
				return false;
			}
			
			if(!pv.getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
				whereInCancelar.append(pv.getCdpedidovenda().toString()).append(",");
			}
			
			if(pv.getEmpresa() != null && pv.getEmpresa().getIntegracaowms() != null && pv.getEmpresa().getIntegracaowms() &&
					pv.getIdentificadorcarregamento() != null){
				registrarDevolucao = true;
				pedidovendaDevolucao = pv;
			}
			
			Valecompra valecompra = valecompraService.findByPedidovenda(pv, Tipooperacao.TIPO_CREDITO);
			if(valecompra != null) {
				Money saldo = valecompraService.getSaldoByCliente(pv.getCliente());
				if(saldo.getValue().doubleValue() - valecompra.getValor().getValue().doubleValue() < 0) {
					listaErro.add("N�o ser� poss�vel cancelar o pedido "+ pv.getCdpedidovenda() +" pois o saldo do vale compra � insuficiente.");
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaErro)) {
			for(String erro : listaErro) {
				request.addError(erro);
			}
			return false;
		}
		
		if(registrarDevolucao && listaPedidovenda.size() > 1){
			request.addError("O pedido de venda " + pedidovendaDevolucao.getCdpedidovenda() + " ir� gerar devolu��o. � preciso fazer o cancelamento individual deste pedido.");
			return false;
		}
		
		List<Coleta> listaColeta = coletaService.findByWhereInPedidovenda(whereIn);
		if(listaColeta != null && listaColeta.size() > 0){
			for (Coleta coleta : listaColeta) {
				if(coleta.getPedidovenda() != null && 
						coleta.getPedidovenda().getPedidovendasituacao() != null && 
						!coleta.getPedidovenda().getPedidovendasituacao().equals(Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
					List<ColetaMaterial> listaColetaMaterial = coleta.getListaColetaMaterial();
					if(listaColetaMaterial != null && listaColetaMaterial.size() > 0){
						for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
							if(coletaMaterial.getQuantidadedevolvida() != null && coletaMaterial.getQuantidadedevolvida() > 0){
								request.addError("N�o � poss�vel cancelar. J� existem coletas devolvidas.");
								return false;
							}
						}
					}
				}
			}
		}
		
		
		if(whereInCancelar.length() > 0 && this.isProducaoEmAndamento(whereInCancelar.substring(0, whereInCancelar.length()-1))){
			request.addError("N�o � poss�vel cancelar o pedido. A produ��o est� em andamento. Necess�rio cancelar a agenda de produ��o manualmente.");
			return false;
		}
		request.setAttribute("acao", registrarDevolucao ? "abrirRegistrarDevolucao" : "cancelar");
		return true;
	}
	
	/**
	* M�todo ajax para validar a garantia do pedido de venda
	*
	* @param request
	* @param pedidovenda
	* @return
	* @since 23/06/2016
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxValidaPedidoGarantia(WebRequestContext request, Pedidovenda pedidovenda){
		String mensagemPedidoGarantia = "";
		if(Util.objects.isPersistent(pedidovenda) && Boolean.TRUE.equals(pedidovenda.getAprovarpedidovenda())){
			mensagemPedidoGarantia = this.validaPedidoGarantia(pedidovenda, null);
		}
		return new JsonModelAndView().addObject("mensagemPedidoGarantia", mensagemPedidoGarantia);
	}
	
	/**
	 * M�todo ajax para saber se o tipo de documento � boleto
	 * busca o documentotipo e a lista de conta
	 *
	 * @param request
	 * @param documentotipo
	 * @return
	 * @author Luiz Fernando
	 */
	public void ajaxTipoDocumentoBoletoPagamento(WebRequestContext request) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");		
		String boleto = "false";
		String contaboleto = "";
		String whereIn = request.getParameter("cddocumentotipo");
		if (whereIn != null && !"".equals(whereIn)) {
			List<Documentotipo> lista = documentotipoService.findDocumentoTipoBoleto(whereIn);
			if(lista != null && !lista.isEmpty()){
				for(Documentotipo documentotipo : lista){			
					if(documentotipo.getBoleto() != null && documentotipo.getBoleto()){
						String cdempresa = request.getParameter("cdempresa");
						boleto =  "true";						
						if(cdempresa != null && !"".equals(cdempresa)){
							try {
								List<Conta> listaConta = contaService.findContasAtivasComPermissaoByEmpresa(new Empresa(Integer.parseInt(cdempresa)));
								
								if(listaConta != null && listaConta.size() == 1){
									Conta conta  = listaConta.get(0);
									if(conta != null && conta.getCdconta() != null){
										contaboleto = "br.com.linkcom.sined.geral.bean.Conta[cdconta=" + conta.getCdconta() + "]";
									}
								}								
							} catch (NumberFormatException e) {}							
						}
						break;
					}
				}
			}			
		}
		
		view.println("var boleto = '" + boleto + "'; ");
		view.println("var contaboleto = '" + contaboleto + "'; ");
	}
	
	public boolean validateConfirmar(WebRequestContext request, PedidoVendaParametersBean parameters){
		String itens = request.getParameter("selectedItens");
		if (itens == null || itens.equals("")) {
			request.addError("Nenhum item selecionado.");
			return false;
		}
		
		Integer qtdeItens = itens.split(",").length;
		boolean confirmacaoDiretaPedido = parametrogeralService.getBoolean(Parametrogeral.CONFIRMACAO_DIRETA_PEDIDO);
		boolean confirmarVariosPedidos = parametrogeralService.getBoolean(Parametrogeral.PERMITIR_CONFIRMAR_VARIOS_PEDIDOS);
		parameters.setExisteGrade(false);

		if(qtdeItens > 1 && !(confirmacaoDiretaPedido || confirmarVariosPedidos)){
			request.addError("S� � permitido confirmar um pedido de venda por vez.");
			return false;
		}else if(itens.split(",").length > 30){
			request.addError("S� � permitido confirmar at� 30 pedidos de venda por vez.");
			return false;
		}
		
		if(confirmacaoDiretaPedido){
			if(qtdeItens > 1 && parameters.getExisteReserva()){
				request.addError("N�o � permitido confirma��o direta de pedido de venda com reserva.");
				return false;
			}else {
				boolean existeGrade = this.existeGrade(itens);
				parameters.setExisteGrade(existeGrade);
				if(qtdeItens > 1 && existeGrade){
					request.addError("N�o � permitido confirma��o direta de pedido de venda de grade.");
					return false;
				}
			}
		}

		Boolean isWms = empresaService.isIntegracaoWms(empresaService.loadPrincipal());
		if (isWms != null && isWms){ 
			if(this.havePedidovendaIntegracaoWms(itens)){
				request.addError("Esta a��o � permitida somente para pedidos de venda que n�o s�o sincronizados com o wms.");
				return false;
			} else if(this.havePedidovendaIntegracaoManualPendenteWms(itens)){
				request.addError("Esta a��o � permitida somente para pedidos de venda cujo processo de expedi��o no wms foi finalizado.");
				return false;
			}
		}
		
		if(this.havePedidovendaDiferenteSituacao(itens, Pedidovendasituacao.PREVISTA, Pedidovendasituacao.CONFIRMADO_PARCIALMENTE)){
			request.addError("Pedido(s) de venda com situa��o(�es) diferente de 'PREVISTA' e 'CONFIRMADO PARCIALMENTE'.");
			return false;
		}
		
		if(emporiumpedidovendaService.havePedidovendaEnviado(itens) && this.existeSomenteProdutoVendaECF(itens)){
			request.addError("Pedido de venda enviado para o ECF, n�o � poss�vel confirmar manualmente.");
			return false;
		}
		
		if(emporiumpedidovendaService.havePedidovendaEnviado(itens) && this.existeSomenteProdutoVendaECF(itens)){
			request.addError("Pedido de venda enviado para o ECF, n�o � poss�vel confirmar manualmente.");
			return false;
		}
		
		if (parametrogeralService.getBoolean("CONFIRMACAO_DIRETA_PEDIDO") && !this.verificarPedidoVendaTipoPermiteMaterialMestreVenda(itens) 
				&& this.verificarPermitirMaterialMestreVenda(itens)) {
			request.addError("N�o � poss�vel confirmar pedido de venda enquanto existirem materiais mestres de grade no pedido.");
			return false;
		}
		
		for(String id : itens.split(",")){
			if(Producaoagendasituacao.CANCELADA.equals(producaoagendaService.getProducaoagendasituacaoByPedidovenda(new Pedidovenda(Integer.parseInt(id)))) &&
					parametrogeralService.getBoolean(Parametrogeral.BLOQUEAR_CONFIRMACAO_PEDIDOVENDA_INTERROMPIDO)){
				request.addError("Pedido de venda interrompido por completo, n�o � poss�vel confirmar.");
				return false;
			}
			
			if(SinedUtil.isRecapagem()){
				if(Pedidovendasituacao.PREVISTA.equals(load(new Pedidovenda(Integer.parseInt(id)), "pedidovenda.cdpedidovenda, pedidovenda.pedidovendasituacao").getPedidovendasituacao())){
					Pedidovenda pv = findForListaProducaoAgenda((new Pedidovenda(Integer.parseInt(id))));
					if(SinedUtil.isListNotEmpty(pv.getListaProduocaoagenda())){
						int qtde_agenda_normal = 0;
						Producaoagenda producaoAgendaCancelada = null;
						for(Producaoagenda producaoAgenda : pv.getListaProduocaoagenda()){
							if(Producaoagendasituacao.CANCELADA.equals(producaoAgenda.getProducaoagendasituacao())){
								producaoAgendaCancelada = producaoAgenda;
							}else {
								qtde_agenda_normal++;
							}
						}
						Boolean existeItemSemPneuOuProduto = false;
						for(Pedidovendamaterial pvm : pv.getListaPedidovendamaterial()){
							if(!Util.objects.isPersistent(pvm.getPneu()) || (pvm.getMaterial() != null && !Boolean.TRUE.equals(pvm.getMaterial().getServico()))){
								existeItemSemPneuOuProduto = true;
							}							
						}
						if(!existeItemSemPneuOuProduto && producaoAgendaCancelada != null && qtde_agenda_normal == 0){
							request.addError("Agenda de produ��o "+ (producaoAgendaCancelada.getCdproducaoagenda())+ " foi cancelada. Cancele o pedido de venda");
						}								
					}
				}
			}
			
			Pedidovenda bean = this.load(new Pedidovenda(Integer.parseInt(id)));
			if(!vendaService.validaVendedorprincipal(bean, request)){
				return false;
			}
		}
		
		return true;
	}
	
	public Pedidovenda findForListaProducaoAgenda(Pedidovenda pedidoVenda){
		return pedidovendaDAO.findForListaProducaoAgenda(pedidoVenda);
	}
	
/*	
	public List<Pedidovenda> findForAutocompletePedidovendacomodato(String q){
		Integer cdpedidovenda = null;
		try {
			cdpedidovenda = Integer.parseInt(NeoWeb.getRequestContext().getParameter("cdpedidovenda"));
		} catch (Exception e) {} 
		return pedidovendaDAO.findForAutocompletePedidovendacomodato(q, cdpedidovenda);
	}*/
	
	public boolean isOtr(Pedidovenda bean){
		Pedidovenda pedido = this.load(bean, "pedidovenda.cdpedidovenda, pedidovenda.origemOtr");
		return pedido != null && Boolean.TRUE.equals(pedido.getOrigemOtr());
	}
	
	public boolean isOtr(Integer cdPedidoVenda){
		return this.isOtr(new Pedidovenda(cdPedidoVenda));
	}
	
	public void setTotalImpostos(Pedidovenda venda){
		if(venda != null){
			Money valorIpi = new Money(0);
			Money valorIcms = new Money(0);
			Money valorIcmsSt = new Money(0);
			Money valorFcp = new Money(0);
			Money valorFcpSt = new Money(0);
			Money valorDifal = new Money(0);
			Money valorDesoneracaoIcms = new Money(0);
			if(SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterial())){
				for (Pedidovendamaterial item : venda.getListaPedidovendamaterial()) {
					valorIpi = valorIpi.add(item.getValoripi());
					valorIcms = valorIcms.add(item.getValoricms());
					valorIcmsSt = valorIcmsSt.add(item.getValoricmsst());
					valorFcp = valorFcp.add(item.getValorfcp());
					valorFcpSt = valorFcpSt.add(item.getValorfcpst());
					valorDifal = valorDifal.add(item.getValordifal());
					if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
						valorDesoneracaoIcms = valorDesoneracaoIcms.add(item.getValordesoneracaoicms());
					}
				}
			}
			if(SinedUtil.isListNotEmpty(venda.getListaPedidovendamaterialmestre())){
				for (Pedidovendamaterialmestre item : venda.getListaPedidovendamaterialmestre()) {
					valorIpi = valorIpi.add(item.getValoripi());
					valorIcms = valorIcms.add(item.getValoricms());
					valorIcmsSt = valorIcmsSt.add(item.getValoricmsst());
					valorFcp = valorFcp.add(item.getValorfcp());
					valorFcpSt = valorFcpSt.add(item.getValorfcpst());
					valorDifal = valorDifal.add(item.getValordifal());
					if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
						valorDesoneracaoIcms = valorDesoneracaoIcms.add(item.getValordesoneracaoicms());
					}
				}
			}
			venda.setTotalFcp(valorFcp);
			venda.setTotalFcpSt(valorFcpSt);
			venda.setTotalIcms(valorIcms);
			venda.setTotalIcmsSt(valorIcmsSt);
			venda.setTotalipi(valorIpi);
			venda.setTotalDifal(valorDifal);
			venda.setTotalDesoneracaoIcms(valorDesoneracaoIcms);
		}
	}
	public boolean existeErro(WebRequestContext request) {
		if(request != null && request.getMessages() != null){
			for(Message message : request.getMessages()){
				if(MessageType.ERROR.equals(message.getType())){
					return true;
				}
			}
		}
		return false;
	}
	
	public Pedidovenda findForComprovanteRTF(Pedidovenda pedidoVenda){
		return pedidovendaDAO.findForComprovanteRTF(pedidoVenda);
	}
	
	
	public VendaRTF makeComprovanteOrcamento(Pedidovenda pedidovendabean){
		Pedidovenda pedidoVenda = this.findForComprovanteRTF(pedidovendabean);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		VendaRTF bean = new VendaRTF();
		
		//Populando dados do cabe�alho do or�amento
		bean.setTipopedidovenda(pedidoVenda.getPedidovendatipo() != null? pedidoVenda.getPedidovendatipo().getDescricao(): "");
		bean.setResponsavel(pedidoVenda.getColaborador() != null? pedidoVenda.getColaborador().getNome(): "");
		bean.setDtvenda(sdf.format(pedidoVenda.getDtpedidovenda()));
		bean.setObservacoes(pedidoVenda.getObservacao());
		bean.setObservacoesinternas(pedidoVenda.getObservacaointerna());
		bean.setCodigo(pedidoVenda.getCdpedidovenda().toString());
		
		//Populando novos campos
		bean.setQtdeParcelas(pedidoVenda.getQtdeParcelas());
		bean.setPedidovendaSituacao(pedidoVenda.getPedidovendasituacao().toString());
		bean.setIdentificadorexterno(pedidoVenda.getIdentificacaoexterna());
		if(pedidoVenda.getFrete()!= null && pedidoVenda.getFrete().getNome()!=null){
			bean.setFrete(pedidoVenda.getFrete().getNome());
		}
		bean.setValorFrete(pedidoVenda.getValorfrete());
		bean.setTaxaPedidoVenda(pedidoVenda.getTaxapedidovenda());
		if(pedidoVenda.getProjeto()!= null && pedidoVenda.getProjeto().getNome()!=null){
			bean.setProjetoNome(pedidoVenda.getProjeto().getNome());
		}
		bean.setValorValeCompra(pedidoVenda.getValorusadovalecompra());
		bean.setValorFinalipi(pedidoVenda.getTotalvendaMaisImpostos());
		bean.setObservacaoPedidoVendaTipo(pedidoVenda.getObservacaoPedidoVendaTipo());
		if(pedidoVenda.getLocalarmazenagem()!= null && pedidoVenda.getLocalarmazenagem().getNome()!=null){
			bean.setLocalArmazenagem(pedidoVenda.getLocalarmazenagem().getNome());
		}
		if(pedidoVenda.getVendaorcamento()!= null){
			bean.setCodigoOrcamento(pedidoVenda.getVendaorcamento().getCdvendaorcamento().toString());
		}
		if(pedidoVenda.getPrazopagamento()!= null && pedidoVenda.getPrazopagamento().getNome()!=null){
			bean.setPrazoPagamento(pedidoVenda.getPrazopagamento().getNome());
		}
		if(pedidoVenda.getTerceiro()!= null && pedidoVenda.getTerceiro().getNome()!=null){
			bean.setTerceiro(pedidoVenda.getTerceiro().getNome());
		}
		
		//Populando dados da empresa
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getCdpessoa() != null){
			pedidoVenda.getEmpresa().setListaEndereco(new ListSet<Endereco>(Endereco.class, enderecoService.carregarListaEndereco(pedidoVenda.getEmpresa())));
			pedidoVenda.getEmpresa().setListaTelefone(new ListSet<Telefone>(Telefone.class, telefoneService.findByPessoa(pedidoVenda.getEmpresa())));
		}
		bean.setEmpresa(new VendaEmpresaRTF(pedidoVenda.getEmpresa()));
		
		Cliente cliente = pedidoVenda.getCliente() != null ? clienteService.carregarDadosCliente(pedidoVenda.getCliente()) : null;
		if(cliente != null){
			cliente.setNome(pessoaService.carregaPessoa(new Pessoa(cliente.getCdpessoa())).getNome());
			bean.setVendedorprincipal(clientevendedorService.getNomeVendedorPrincipal(cliente));
			cliente.setListaContato(pessoaContatoService.findByPessoa(cliente));
		}
		
		//Populando dados do cliente
		bean.setCliente(new VendaClienteRTF(cliente));
		
		bean.setOutros(new VendaOutrosRTF(pedidoVenda));
		
		List<VendaMaterialKitRTF> listaProdutoskit = new ArrayList<VendaMaterialKitRTF>();
		List<VendaMaterialKitflexivelRTF> listaProdutoskitflexivel = new ArrayList<VendaMaterialKitflexivelRTF>();
		List<Pedidovendapagamento> listaPedidoVendaFormaPagamento = pedidovendapagamentoService.findByPedidovenda(pedidoVenda);
		List<Pedidovendamaterial> listaprodutos = pedidovendamaterialService.findByPedidovenda(pedidoVenda);
		List<Pedidovendamaterialmestre> listaVendaorcamentomaterialmestre = pedidovendamaterialmestreService.findByPedidovenda(pedidoVenda);
		pedidoVenda.setListaPedidovendamaterial(listaprodutos);
		
		pedidoVenda.setListaPedidovendavalorcampoextra(pedidovendavalorcampoextraService.findByPedidoVenda(pedidoVenda));
		bean.setQtdeParcelasPagamento(listaPedidoVendaFormaPagamento != null ? listaPedidoVendaFormaPagamento.size() : null);
		
		//Populando dados de pagamento
		for(Pedidovendapagamento formapagamento: listaPedidoVendaFormaPagamento){
			bean.getListaPagamento().add(new VendaPagamentoRTF(formapagamento));
		}
		/*BigDecimal pesoliquidototal = new BigDecimal(0);
		BigDecimal pesobrutototal = new BigDecimal(0);
		Double qtdetotalmetrocubico = 0.0;
		Double qtdemetrocubico = 0.0;
		Double fracao = 1.0;*/
		for (Pedidovendamaterial pedidovendamaterial : listaprodutos) {
			Money valorTotal = vendaService.getValortotal(pedidovendamaterial.getPreco(), pedidovendamaterial.getQuantidade(), pedidovendamaterial.getDesconto(), pedidovendamaterial.getMultiplicador(),
					pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro());
			pedidovendamaterial.setTotal(valorTotal);
		}
		Double qtdeProduto = 0.0, qtdeServico = 0.0;
		Money valorTotalProdutos = new Money();
		
		//Populando itens do or�amento
		for(Pedidovendamaterial vm : listaprodutos){
			VendaMaterialRTF produto = new VendaMaterialRTF();
			if(vm.getMaterial() != null && vm.getMaterial().getArquivo() != null && vm.getMaterial().getArquivo().getCdarquivo() != null){
				try {
					File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterial().getArquivo()));
					BufferedImage image = ImageIO.read(input);
					
					File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterial().getArquivo()));
					ImageIO.write(image, "png", output);  
					
					produto.setImagem(new FileInputStream(output));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(vm.getMaterial() != null && vm.getMaterial().getPesobruto() != null){
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				if(vm.getMaterial().getPesobruto() != null){
					Double pesoBruto = vm.getMaterial().getPesobruto();
					if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
							vm.getMaterial().getUnidademedida() != null && 
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
					}
					produto.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)).toString());
					//pesobrutototal =  pesoliquidototal.add(new BigDecimal(pesoBruto * qtde));
				}
			}
			if(vm.getPesoVendaOuMaterial() != null){
				produto.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())).toString());
				//pesoliquidototal = pesoliquidototal.add(new BigDecimal(vm.getQuantidade() * vm.getPesoVendaOuMaterial()));
			}
			produto.setCdmaterial(vm.getMaterial().getCdmaterial() != null ? vm.getMaterial().getCdmaterial().toString() : "");
			produto.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
			produto.setIdentificador(vm.getMaterial().getIdentificacao());
			produto.setDescricao(vm.getMaterial().getNome());
			produto.setUnidademedida(vm.getUnidademedida().getNome());
			produto.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
			produto.setQuantidade(vm.getQuantidade());
			if(vm.getPneu()!=null){
				produto.setDot(vm.getPneu().getDot());
				produto.setSerie(vm.getPneu().getSerie());
				if(vm.getPneu().getPneumarca()!=null)
					produto.setMarca(vm.getPneu().getPneumarca().getNome());
				if(vm.getPneu().getPneumodelo()!=null)
					produto.setModelo(vm.getPneu().getPneumodelo().getNome());
				if(vm.getPneu().getPneumedida()!=null)
					produto.setMedida(vm.getPneu().getPneumedida().getNome());
			}
			
			if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
					!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
				produto.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
			}
			produto.setPreco(new java.text.DecimalFormat(SinedUtil.getFormatacaoCasasdecimais(vm.getPreco())).format(vm.getPreco()));
			produto.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
			produto.setCdvendamaterial(vm.getCdpedidovendamaterial().toString());
			produto.setTotal(vm.getTotal().toString());
			produto.setObservacao(vm.getObservacao());
			if(vm.getLoteestoque() != null){
				produto.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
			}
			produto.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );
			if(vm.getComprimentooriginal() != null){
				produto.setComprimento(vm.getComprimentooriginal().toString());
			}
			if(vm.getAltura() != null){
				produto.setAltura(vm.getAltura().toString());
			}
			if(vm.getLargura() != null){
				produto.setLargura(vm.getLargura().toString());
			}
			produto.setNcm(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null ? vm.getMaterial().getNcmcapitulo().getCodeFormat() : "");
			produto.setNcmcompleto(vm.getMaterial()!=null && vm.getMaterial().getNcmcompleto()!=null ? vm.getMaterial().getNcmcompleto() : "");
			produto.setNcmdescricao(vm.getMaterial()!=null && vm.getMaterial().getNcmcapitulo()!=null && vm.getMaterial().getNcmcapitulo().getDescricao()!=null ? vm.getMaterial().getNcmcapitulo().getDescricao() : "");
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getCdmaterial() != null){
				produto.setCdmaterialmestre(vm.getMaterialmestre().getCdmaterial().toString());
			}
			produto.setIdentificadormaterialmestre(vm.getIdentificadorinterno() != null ? vm.getIdentificadorinterno() : "");
			if(vm.getMaterial()!=null && vm.getMaterial().getLocalizacaoestoque()!=null && vm.getMaterial().getLocalizacaoestoque().getDescricao() !=null){
				produto.setLocalizacaoestoque(vm.getMaterial().getLocalizacaoestoque().getDescricao());
			}
			if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
				produto.setCaracteristica(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
			}
			
			if(vm.getMaterial() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto() != null && vm.getMaterial().getProduto()){
				qtdeProduto += vm.getQuantidade();
			}
			if(vm.getMaterial() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico() != null && vm.getMaterial().getServico()){
				qtdeServico += vm.getQuantidade();
			}
			Money total = new Money(SinedUtil.round(vm.getTotal().getValue(), 2));
			valorTotalProdutos = valorTotalProdutos.add(total);
			
			bean.getListaVendamaterial().add(produto);
			
			if((vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && 
					vm.getMaterialmestre().getVendapromocional()) || 
					(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional())){
				//Populando itens de kit
				addMaterialKitRTF(listaProdutoskit, vm);
			}else if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() != null && 
					vm.getMaterialmestre().getKitflexivel()){
				//Populando itens de kit flex�vel
				addMaterialKitflexivelRTF(listaProdutoskitflexivel, vm, getItemMestre(listaVendaorcamentomaterialmestre, vm));
			}else{
				bean.getListaVendamaterialavulso().add(produto);
			}
		}
		
		bean.setQtdetotalproduto(qtdeProduto);
		bean.setQtdetotalservico(qtdeServico);
		bean.setValortotal(valorTotalProdutos.toString());
		bean.setValorporextenso(valorTotalProdutos != null? new Extenso(valorTotalProdutos).toString():"");
		bean.getListaVendamaterialkit().addAll(listaProdutoskit);
		bean.getListaVendamaterialkitflexivel().addAll(listaProdutoskitflexivel);
		
		HashMap<String, String> camposAdicionais = new HashMap<String, String>();
		if (pedidoVenda.getListaPedidovendavalorcampoextra() != null){
			for (Pedidovendavalorcampoextra campoextra : pedidoVenda.getListaPedidovendavalorcampoextra()){
				camposAdicionais.put(campoextra.getCampoextrapedidovendatipo().getNome(), campoextra.getValor());
			}
		}
		bean.setCamposadicionais(camposAdicionais);
		
		StringBuilder observacao = new StringBuilder();
		if(pedidoVenda.getObservacao() != null){
			observacao.append(pedidoVenda.getObservacao());
		}
		if(pedidoVenda.getEmpresa() != null && pedidoVenda.getEmpresa().getObservacaovenda() != null && !"".equals(pedidoVenda.getEmpresa().getObservacaovenda())){
			observacao.append("\n").append(pedidoVenda.getEmpresa().getObservacaovenda());
		}
		bean.setDesconto(pedidoVenda.getDesconto() == null ? new Money(0.0).toString() : pedidoVenda.getDesconto().toString());
		
		bean.setValorfinal(pedidoVenda.getTotalvenda().toString());
		bean.setValorFinalipi(pedidoVenda.getTotalvendaMaisImpostos());
		bean.setValortotalprodutos(valorTotalProdutos != null ? valorTotalProdutos.toString() : new Money(0.0).toString());
		return bean;
	}
	
	private void addMaterialKitRTF(List<VendaMaterialKitRTF> listaProdutoskit, Pedidovendamaterial vm) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(listaProdutoskit != null && vm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getVendapromocional() != null && vm.getMaterialmestre().getVendapromocional()){
				boolean adicionar = true;
				for(VendaMaterialKitRTF kit : listaProdutoskit){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterialmestre())){
						adicionar = false;
						VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
						itemKit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
						itemKit.setDescricao(vm.getMaterial().getNome());
						itemKit.setUnidademedida(vm.getUnidademedida().getNome());
						itemKit.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
						itemKit.setPreco(vm.getPreco());
						itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
						itemKit.setQuantidade(vm.getQuantidade());
						itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
						itemKit.setObservacao(vm.getObservacao());
						if(vm.getLoteestoque() != null){
							itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
						}
						itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

						if(vm.getComprimentooriginal() != null){
							itemKit.setComprimento(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							itemKit.setLargura(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							itemKit.setAltura(vm.getAltura().toString());
						}
						itemKit.setMaterial(vm.getMaterial());
						
						if(kit.getTotal() == null) kit.setTotal(itemKit.getTotal());
						else kit.setTotal(kit.getTotal() + itemKit.getTotal());
						
						kit.getItens().add(itemKit);
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitRTF kit = new VendaMaterialKitRTF();
					kit.setCodigo(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					kit.setDescricao(vm.getMaterialmestre().getNome());
					kit.setMaterial(vm.getMaterialmestre());
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getPesobruto() != null){
						Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
						if(vm.getMaterialmestre().getPesobruto() != null){
							Double pesoBruto = vm.getMaterialmestre().getPesobruto();
							if(vm.getUnidademedida() != null && vm.getMaterialmestre() != null && 
									vm.getMaterialmestre().getUnidademedida() != null && 
									!vm.getUnidademedida().equals(vm.getMaterialmestre().getUnidademedida())){
								pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
							}
							kit.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)));
						}
					}
					if(vm.getPesoVendaOuMaterial() != null){
						kit.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())));
					}
					kit.setNcmcompleto(vm.getMaterialmestre().getNcmcompleto());
					if(vm.getMaterialmestre().getArquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterialmestre().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterialmestre().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					
					VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
					itemKit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
					itemKit.setDescricao(vm.getMaterial().getNome());
					itemKit.setUnidademedida(vm.getUnidademedida().getNome());
					itemKit.setUnidademedidasimbolo(vm.getUnidademedida().getSimbolo());
					itemKit.setPreco(vm.getPreco());
					itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
					itemKit.setQuantidade(vm.getQuantidade());
					itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
					itemKit.setObservacao(vm.getObservacao());
					if(vm.getLoteestoque() != null){
						itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
					}
					itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

					if(vm.getComprimentooriginal() != null){
						itemKit.setComprimento(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						itemKit.setLargura(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						itemKit.setAltura(vm.getAltura().toString());
					}
					itemKit.setMaterial(vm.getMaterial());
					kit.setTotal(itemKit.getTotal());
					
					kit.getItens().add(itemKit);
					listaProdutoskit.add(kit);
				}
			}else if(vm.getMaterial().getVendapromocional() != null && vm.getMaterial().getVendapromocional()){
				boolean adicionar = true;
				for(VendaMaterialKitRTF kit : listaProdutoskit){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterial())){
						adicionar = false;
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitRTF kit = new VendaMaterialKitRTF();
					kit.setCodigo(vm.getMaterial().getIdentificacaoOuCdmaterial());
					kit.setDescricao(vm.getMaterial().getNome());
					//kit.setQuantidade(vm.getQuantidade());
					kit.setMaterial(vm.getMaterial());
					if(vm.getMaterial().getPesobruto() != null){
						Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
						if(vm.getMaterial().getPesobruto() != null){
							Double pesoBruto = vm.getMaterial().getPesobruto();
							if(vm.getUnidademedida() != null && vm.getMaterial() != null && 
									vm.getMaterial().getUnidademedida() != null && 
									!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
								pesoBruto = pesoBruto / vm.getFatorconversaoQtdereferencia();
							}
							kit.setPesobruto(SinedUtil.roundByParametro(new Double(pesoBruto * qtde)));
						}
					}
					if(vm.getPesoVendaOuMaterial() != null){
						kit.setPesoliquido(SinedUtil.roundByParametro(new Double(vm.getQuantidade() * vm.getPesoVendaOuMaterial())));
					}
					kit.setNcmcompleto(vm.getMaterial().getNcmcompleto());
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterial() != null && vm.getMaterial().getListaCaracteristica() != null && !vm.getMaterial().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterial().getListaCaracteristica().iterator().next().getNome());
					}
					if(vm.getMaterial().getArquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterial().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterial().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vm.getMaterial().getCdmaterial());
					material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
					
					try{
						material.setProduto_altura(vm.getAltura());
						material.setProduto_largura(vm.getLargura());
						material.setQuantidade(vm.getQuantidade());
						materialrelacionadoService.calculaQuantidadeKit(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
						Double total = 0d;
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getQuantidade() != null && vm.getQuantidade() != null){
								Material materialItemDoKit = materialrelacionado.getMaterialpromocao();
								VendaMaterialKitItemRTF itemKit = new VendaMaterialKitItemRTF();
								itemKit.setDescricao(materialItemDoKit.getNome());
								itemKit.setCodigo(materialItemDoKit.getIdentificacaoOuCdmaterial());
								Double valorvendaItemKit = materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0d;
								Double preco = vm.getPreco() != null ? vm.getPreco() : 0d;
								Double valorvendaMaterial = material.getValorvenda() != null ? material.getValorvenda() : 0d;
								if(valorvendaMaterial == 0) valorvendaMaterial = 1d;
								itemKit.setPreco(preco * ((valorvendaItemKit * 100) / valorvendaMaterial) / 100);
								itemKit.setQuantidade(materialrelacionado.getQuantidade() * vm.getQuantidade());
								itemKit.setTotal(itemKit.getPreco() * itemKit.getQuantidade());
								itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
								total += itemKit.getTotal();
								
								Unidademedida unidademedida = materialItemDoKit.getUnidademedida();

								itemKit.setUnidademedida(unidademedida.getNome());
								itemKit.setUnidademedidasimbolo(unidademedida.getSimbolo());
								
								itemKit.setObservacao(vm.getObservacao());
								if(vm.getLoteestoque() != null){
									itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
								}
								/*itemKit.setDesconto(vm.getDesconto() != null ? new java.text.DecimalFormat("#,####,##0.0000").format(vm.getDesconto().getValue().doubleValue()) : "" );

								if(vm.getComprimentooriginal() != null){
									itemKit.setComprimento(vm.getComprimentooriginal().toString());
								}
								if(vm.getLargura() != null){
									itemKit.setLargura(vm.getLargura().toString());
								}
								if(vm.getAltura() != null){
									itemKit.setAltura(vm.getAltura().toString());
								}*/
								
								
								
								itemKit.setMaterial(vm.getMaterial());
								
								kit.getItens().add(itemKit);
							}
						}
						kit.setTotal(total);
					}
					listaProdutoskit.add(kit);
				}
			}
		}
	}
	
	private void addMaterialKitflexivelRTF(List<VendaMaterialKitflexivelRTF> listaProdutoskitflexivel, Pedidovendamaterial vm, Pedidovendamaterialmestre vmm) {
		if(listaProdutoskitflexivel != null && vm != null && vmm != null){
			if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getKitflexivel() && vm.getMaterialmestre().getKitflexivel()){
				boolean adicionar = true;
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				for(VendaMaterialKitflexivelRTF kit : listaProdutoskitflexivel){
					if(kit.getMaterial() != null && kit.getMaterial().equals(vm.getMaterialmestre()) && 
							kit.getIdentificadormaterialmestre() != null && 
							vm.getIdentificadorinterno() != null &&
							kit.getIdentificadormaterialmestre().equals(vm.getIdentificadorinterno())){
						adicionar = false;
						VendaMaterialKitflexivelItemRTF itemKit = new VendaMaterialKitflexivelItemRTF();
						itemKit.setUnidademedida(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getNome(): "");
						itemKit.setUnidademedidasimbolo(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getSimbolo(): "");
						itemKit.setQuantidade(vm.getQuantidade());
						if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
								!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
							itemKit.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
						}
						itemKit.setPreco(vm.getPreco());
						itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
						itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
						itemKit.setObservacao(vm.getObservacao());
						itemKit.setDescricao(vm.getMaterial().getNome());
						if(vm.getLoteestoque() != null){
							itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
						}
						itemKit.setDesconto(vm.getDesconto() != null? vm.getDesconto().getValue().doubleValue(): null);
						if(vm.getComprimentooriginal() != null){
							itemKit.setComprimento(vm.getComprimentooriginal().toString());
						}
						if(vm.getLargura() != null){
							itemKit.setLargura(vm.getLargura().toString());
						}
						if(vm.getAltura() != null){
							itemKit.setAltura(vm.getAltura().toString());
						}
						
						itemKit.setNcmcompleto(vm.getMaterial().getNcmcompleto());

						itemKit.setPesobruto(vm.getMaterial().getPesobruto());
						itemKit.setPesoliquido(vm.getMaterial().getPesobruto());
						
						itemKit.setMaterial(vm.getMaterial());
						
						kit.getItens().add(itemKit);
						break;
					}
				}
				if(adicionar){
					VendaMaterialKitflexivelRTF kit = new VendaMaterialKitflexivelRTF();
					kit.setCodigo(vm.getMaterialmestre().getIdentificacaoOuCdmaterial());
					kit.setDescricao(org.apache.commons.lang.StringUtils.isNotEmpty(vmm.getNomealternativo()) ? vmm.getNomealternativo() : vmm.getMaterial().getNome());
					kit.setPesobruto(vm.getMaterialmestre().getPesobruto());
					kit.setPesoliquido(vm.getMaterialmestre().getPesobruto());
					kit.setNcmcompleto(vmm.getMaterial().getNcmcompleto());
					kit.setObservacao(vm.getObservacao());
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getListaCaracteristica() != null && !vm.getMaterialmestre().getListaCaracteristica().isEmpty()){
						kit.setCaracteristica(vm.getMaterialmestre().getListaCaracteristica().iterator().next().getNome());
					}
					kit.setIdentificadormaterialmestre(vmm.getIdentificadorinterno() != null ? vmm.getIdentificadorinterno() : "");
					kit.setDesconto(vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : null);
					kit.setDtentrega(vmm.getDtprazoentrega()==null ? "" : sdf.format(vmm.getDtprazoentrega()));
					kit.setUnidademedida(vmm.getUnidademedida()!=null && vmm.getUnidademedida().getNome()!=null ? vmm.getUnidademedida().getNome() : "");
					kit.setQuantidade(vmm.getQtde());
					kit.setPreco(vmm.getPreco());
					
					kit.setTotal(vmm.getPreco()*vmm.getQtde() - (vmm.getDesconto() != null ? vmm.getDesconto().getValue().doubleValue() : 0d));
					
					if(vm.getMaterialmestre() != null && vm.getMaterialmestre().getArquivo() != null && vm.getMaterialmestre().getArquivo().getCdarquivo() != null){
						try {
							File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(vm.getMaterialmestre().getArquivo()));
							BufferedImage image = ImageIO.read(input);
							
							File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(vm.getMaterialmestre().getArquivo()));
							ImageIO.write(image, "png", output);  
							
							kit.setImagem(new FileInputStream(output));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					kit.setMaterial(vm.getMaterialmestre());
					
					VendaMaterialKitflexivelItemRTF itemKit = new VendaMaterialKitflexivelItemRTF();
					itemKit.setDescricao(vm.getMaterial().getNome());
					itemKit.setPreco(vm.getPreco());
					itemKit.setQuantidade(vm.getQuantidade());
					if(vm.getUnidademedida() != null && vm.getMaterial().getUnidademedida() != null &&
							!vm.getUnidademedida().equals(vm.getMaterial().getUnidademedida())){
						itemKit.setQuantidadeunidadeprincipal(unidademedidaService.converteQtdeUnidademedida(vm.getMaterial().getUnidademedida(), vm.getQuantidade(), vm.getUnidademedida(), vm.getMaterial(), vm.getFatorconversao(), vm.getQtdereferencia()));
					}
					itemKit.setUnidademedida(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getNome(): "");
					itemKit.setUnidademedidasimbolo(vm.getMaterial().getUnidademedida() != null? vm.getMaterial().getUnidademedida().getSimbolo(): "");
					itemKit.setTotal(vm.getTotalproduto().getValue().doubleValue());
					
					itemKit.setNcmcompleto(vm.getMaterial().getNcmcompleto());
					
					itemKit.setObservacao(vm.getObservacao());
					itemKit.setPesobruto(vm.getMaterial().getPesobruto());
					itemKit.setPesoliquido(vm.getMaterial().getPesobruto());
					
					itemKit.setDtentrega(vm.getPrazoentrega()==null ? "" : sdf.format(vm.getPrazoentrega()));
					if(vm.getComprimentooriginal() != null){
						itemKit.setComprimento(vm.getComprimentooriginal().toString());
					}
					if(vm.getLargura() != null){
						itemKit.setLargura(vm.getLargura().toString());
					}
					if(vm.getAltura() != null){
						itemKit.setAltura(vm.getAltura().toString());
					}
					if(vm.getLoteestoque() != null){
						itemKit.setLoteestoque(vm.getLoteestoque().getNumerovalidade());
					}
					
					itemKit.setMaterial(vm.getMaterial());
					
					kit.getItens().add(itemKit);
					listaProdutoskitflexivel.add(kit);
				}
			}
		}		
	}

	public GerarProducaoConferenciaBean validaProducaoEmAndamento(WebRequestContext request, String whereIn){
		if(this.havePedidovendaSituacao(whereIn, Pedidovendasituacao.CANCELADO, Pedidovendasituacao.CONFIRMADO, Pedidovendasituacao.AGUARDANDO_APROVACAO)){
			request.addError("Para gerar produ��o, o(s) pedido(s) de venda deve(m) estar com situa��o diferente de 'AGUARDANDO APROVA��O', 'CANCELADO' e 'CONFIRMADO'.");
			return null;
		}
		
		if(!this.haveMaterialproducao(whereIn)){
			request.addError("Para gerar produ��o, o(s) pedido(s) de venda deve(m) ter material(is) de produ��o.");
			return null;
		}
		
		List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findForGerarProducao(whereIn);
		if(SinedUtil.isListEmpty(listaPedidovendamaterial)){
			request.addError("Produ��o j� gerada para o(s) pedido(s) de venda selecionado(s).");
			return null;
		}
		
		List<Pedidovendamaterial> listaPedidovendamaterialDisponivel = new ArrayList<Pedidovendamaterial>();
		if(SinedUtil.isListNotEmpty(listaPedidovendamaterial)){
			List<Pneu> listaPneu = new ArrayList<Pneu>();
			List<ColetaMaterial> listaColetaMaterial = new ArrayList<ColetaMaterial>();
			try {
				listaColetaMaterial = coletaMaterialService.findByPedidovendaForProducao(whereIn, CollectionsUtil.listAndConcatenate(listaPedidovendamaterial, "cdpedidovendamaterial", ","));
			} catch (Exception e) {e.printStackTrace();}
			
			Map<Producaoetapa, List<Pneu>> mapa = new HashMap<Producaoetapa, List<Pneu>>();
			
			for(Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial){
				Pedidovendamaterial pvmAux = pedidovendamaterialService.loadForEntrada(pedidovendamaterial);
				if(Util.objects.isPersistent(pvmAux) && Util.objects.isPersistent(pvmAux.getMaterial())){
					pedidovendamaterial.setMaterial(pvmAux.getMaterial());
				}
				Map<Integer, Double> mapaQtdeDisponivel = pedidovendamaterialService.montaMapaQuantidadeDisponivelForProducao(listaColetaMaterial);
				Double qtde = mapaQtdeDisponivel.containsKey(pvmAux.getCdpedidovendamaterial()) ? mapaQtdeDisponivel.get(pvmAux.getCdpedidovendamaterial()) : pvmAux.getQuantidade();
				Material mat = materialService.loadWithProducaoetapa(pedidovendamaterial.getMaterial());
				if(Util.objects.isNotPersistent(mat) || Util.objects.isNotPersistent(mat.getProducaoetapa())){
					continue;
				}
				if(!mapa.containsKey(mat.getProducaoetapa())){
					mapa.put(mat.getProducaoetapa(), new ArrayList<Pneu>());
				}
				mapa.get(mat.getProducaoetapa()).add(pedidovendamaterial.getPneu());
				
				if(SinedUtil.isListEmpty(listaColetaMaterial) || (qtde != null && qtde > 0)){
					listaPedidovendamaterialDisponivel.add(pedidovendamaterial);
					if(Util.objects.isPersistent(pedidovendamaterial.getPneu()) && !listaPneu.contains(pedidovendamaterial.getPneu())){
						listaPneu.add(pedidovendamaterial.getPneu());
					}
				}
			}
			if(!mapa.isEmpty()){
				for(Entry<Producaoetapa, List<Pneu>> entry: mapa.entrySet()){
					Producaoetapa producaoEtapa = entry.getKey();
					String whereInPneu = CollectionsUtil.listAndConcatenate(listaPneu, "cdpneu", ",");
					List<Pneu> listaPneuComProducao = pneuService.findPneuComProducaoagenda(whereInPneu, producaoEtapa, Producaoagendasituacao.EM_ESPERA, Producaoagendasituacao.EM_ANDAMENTO, Producaoagendasituacao.CHAO_DE_FABRICA);
					if(SinedUtil.isListNotEmpty(listaPneuComProducao)){
						request.addError("J� existe uma agenda de produ��o em andamento com mesmo ciclo para o(s) pneu(s): " + SinedUtil.makeLinkHtml(CollectionsUtil.listAndConcatenate(listaPneuComProducao, "cdpneu", ","), "/sistema/crud/Pneu?ACAO=consultar&cdpneu=", "color: white") + ".");
						return null;
					}
				}
			}
		}
		
		if(listaPedidovendamaterialDisponivel.isEmpty()){
			request.addError("N�o existe nenhum item para gerar produ��o.");
			return null;
		}
		
		GerarProducaoConferenciaBean bean = new GerarProducaoConferenciaBean();
		bean.setWhereInPedidovenda(whereIn);
		bean.setWhereInPedidovendamaterial(CollectionsUtil.listAndConcatenate(listaPedidovendamaterialDisponivel, "cdpedidovendamaterial", ","));

		return bean;
	}
	
	public void verificaModeloRTF(ModelAndView retorno, Empresa bean, TipoComprovanteEnum tipoComprovanteEnum) {
		if(!SinedUtil.isObjectValid(bean))
			throw new SinedException("Empresa n�o pode ser nula.");
			
		Empresa empresa = empresaService.loadWithModeloorcamentoRTF(bean);
		
		int index = 0;
		int qtdeTemplates = 0;
		
		if(SinedUtil.isListNotEmpty(empresa.getListaEmpresamodelocomprovanteorcamento())){
			for(Empresamodelocomprovanteorcamento modeloComprovante : empresa.getListaEmpresamodelocomprovanteorcamento()){
				if(tipoComprovanteEnum.equals(modeloComprovante.getTipoComprovante())){
					qtdeTemplates++;
					if(qtdeTemplates == 1){
						index = empresa.getListaEmpresamodelocomprovanteorcamento().indexOf(modeloComprovante);
					}
				}
			}
		}
		
		retorno.addObject("qtdeTemplates", qtdeTemplates);
		retorno.addObject("cdempresamodelocomprovante", qtdeTemplates == 1? empresa.getListaEmpresamodelocomprovanteorcamento().get(index).getCdempresamodelocomprovanteorcamento(): "");
	}
	
	public void setInfoSelecaoModeloComprovanteRTF(WebRequestContext request, Empresa empresaBean, TipoComprovanteEnum tipoComprovanteEnum) {
		Empresa empresa = empresaService.loadWithModeloorcamentoRTF(empresaBean);
		List<Empresamodelocomprovanteorcamento> listaModeloComprovante = new ArrayList<Empresamodelocomprovanteorcamento>(); 
		for(Empresamodelocomprovanteorcamento modeloComprovante : empresa.getListaEmpresamodelocomprovanteorcamento()){
			if(tipoComprovanteEnum.equals(modeloComprovante.getTipoComprovante())){
				listaModeloComprovante.add(modeloComprovante);
			}
		}
		request.setAttribute("empresa", empresa);
		request.setAttribute("listaModelocomprovante", listaModeloComprovante);
	}
	public Venda geraVendaAjusteFaturamento(WebRequestContext request, Pedidovenda pedidovenda) {
		List<Venda> listaVendas = vendaService.findByPedidovendaNaoNegociado(pedidovenda);
		pedidovenda = pedidovendaDAO.loadForVendaNaoNegociada(pedidovenda);
		
		HashMap<Integer, Money> mapaMaterialValor = new HashMap<Integer, Money>();
		//HashMap<Integer, Double> mapaVendaMaterialDiferenca = new HashMap<Integer, Double>();
		List<Vendamaterial> listaVendaMaterial = new ArrayList<Vendamaterial>();
		boolean criarVenda = false;
		
		//percorre a lista de itens das vendas criadas a partir do pedido, somando os valores dos materiais agrupando por pedidovendamaterial 
		for (Venda venda : listaVendas) {
			for (Vendamaterial vendamaterial : venda.getListavendamaterial()) {
				Money valorVendaMaterial = new Money();
				if(mapaMaterialValor.get(vendamaterial.getPedidovendamaterial().getCdpedidovendamaterial()) != null){
					valorVendaMaterial = mapaMaterialValor.get(vendamaterial.getPedidovendamaterial().getCdpedidovendamaterial());
					valorVendaMaterial = valorVendaMaterial.add(vendaService.getValortotal(vendamaterial.getPreco(), vendamaterial.getQuantidade(), vendamaterial.getDesconto(), vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro()));
					mapaMaterialValor.put(vendamaterial.getPedidovendamaterial().getCdpedidovendamaterial(), valorVendaMaterial);
				}else {
					valorVendaMaterial = vendaService.getValortotal(vendamaterial.getPreco(), vendamaterial.getQuantidade(), vendamaterial.getDesconto(), vendamaterial.getMultiplicador(), vendamaterial.getOutrasdespesas(), vendamaterial.getValorSeguro());
					mapaMaterialValor.put(vendamaterial.getPedidovendamaterial().getCdpedidovendamaterial(), valorVendaMaterial);
				}
			}
		}
		
		//percorre os itens do pedido de venda, comparando com os valores dos materiais agrupados de todas as vendas. Caso os valores do pedido de venda sejam maiores, cria uma venda 
		for (Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()) {
			Money valorPedidoVendaMaterial = vendaService.getValortotal(pedidovendamaterial.getPreco(), pedidovendamaterial.getQuantidade(), pedidovendamaterial.getDesconto(), pedidovendamaterial.getMultiplicador(), pedidovendamaterial.getOutrasdespesas(), pedidovendamaterial.getValorSeguro());
			Money diferenca = null;
			boolean considerarItem = false;
			if(mapaMaterialValor.get(pedidovendamaterial.getCdpedidovendamaterial()) != null){
				if(valorPedidoVendaMaterial.compareTo(mapaMaterialValor.get(pedidovendamaterial.getCdpedidovendamaterial())) == -1){
					criarVenda = true;
					considerarItem = true;
					diferenca = valorPedidoVendaMaterial.subtract(mapaMaterialValor.get(pedidovendamaterial.getCdpedidovendamaterial()));				
				}
			}else {
				criarVenda = true;
				considerarItem = true;
			}
			if(considerarItem){
				Vendamaterial vendamaterial = new Vendamaterial();
				if(materialService.isMaterialMestre(pedidovendamaterial.getMaterial())){
					Material materialItemGradePadrao = materialService.findItemGradePadrao(pedidovendamaterial.getMaterial());
					if(materialItemGradePadrao != null){
						vendamaterial.setMaterial(materialItemGradePadrao);						
					}else {
						request.addError("Para pedidos com material mestre deve existir um item de grade padr�o para o material mestre do pedido.");
						return null;
					}
				}else {
					vendamaterial.setMaterial(pedidovendamaterial.getMaterial());					
				}
				vendamaterial.setQuantidade(1.0);
				vendamaterial.setPreco(diferenca != null ? diferenca.getValue().doubleValue() : valorPedidoVendaMaterial.getValue().doubleValue());
				vendamaterial.setPedidovendamaterial(pedidovendamaterial);
				vendamaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
				
				listaVendaMaterial.add(vendamaterial);
			}
		}
		
		Venda venda = new Venda();
		
		if(criarVenda){
			vendaService.preencheDadosBasicosVenda(pedidovenda, venda);
			venda.setListavendamaterial(listaVendaMaterial);
			venda.setColaborador(pedidovenda.getColaborador());
			venda.setPedidovendatipo(new Pedidovendatipo(parametrogeralService.getInteger(Parametrogeral.TIPOPEDIDO_SEM_NEGOCIACAO)));
			venda.setVendaPendenciaNaoNegociada(Boolean.TRUE);
			venda.setPedidovenda(pedidovenda);
			
			if(parametrogeralService.getInteger(Parametrogeral.CONDICAOPAGAMENTO_SEM_NEGOCIACAO) != null){
				venda.setPrazopagamento(new Prazopagamento(parametrogeralService.getInteger(Parametrogeral.CONDICAOPAGAMENTO_SEM_NEGOCIACAO)));
			}
			
			if(parametrogeralService.getInteger(Parametrogeral.FORMAPAGAMENTO_SEM_NEGOCIACAO) != null){
				Documentotipo documentotipo = new Documentotipo(parametrogeralService.getInteger(Parametrogeral.FORMAPAGAMENTO_SEM_NEGOCIACAO));
				venda.getListavendapagamento().get(0).setDocumentotipo(documentotipo);
				venda.getListavendapagamento().get(0).setDataparcela(new java.sql.Date(System.currentTimeMillis()));
				venda.getListavendapagamento().get(0).setValororiginal(vendaService.calculaValorVenda(venda));
			}
			
			return venda;
		}else {
			return null;
		}
	}
	
	public Integer getPedidosAguardandoAprovacaoForAndroid(Integer cdusuario) {
		boolean isColaborador = colaboradorService.isColaborador(cdusuario);
		if(!isColaborador) return 0;
		return pedidovendaDAO.getPedidosAguardandoAprovacaoForAndroid(cdusuario);
	}
	
	public Integer getPedidosPrevistosForAndroid(Integer cdusuario) {
		boolean isColaborador = colaboradorService.isColaborador(cdusuario);
		if(!isColaborador) return 0;
		return pedidovendaDAO.getPedidosPrevistosForAndroid(cdusuario);
	}
	
	public Integer getPedidosConfirmadorForAndroid(Integer cdusuario) {
		boolean isColaborador = colaboradorService.isColaborador(cdusuario);
		if(!isColaborador) return 0;
		return pedidovendaDAO.getPedidosConfirmadorForAndroid(cdusuario);
	}
	
	public void verificaContasFinanceiroSitua�ao (Pedidovenda pedidovenda){
		if(Util.objects.isPersistent(pedidovenda) && Util.objects.isPersistent(pedidovenda.getPedidovendatipo())){
			Pedidovendatipo pedidovendatipo = pedidovendatipoService.load(pedidovenda.getPedidovendatipo(), "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.permitirConfirmarPedidoBaixado");
			if(Boolean.TRUE.equals(pedidovendatipo.getPermitirConfirmarPedidoBaixado())){
				List<Documento> listaDocumento = contaReceberService.findSituacaoDocumentoByPedidovenda(pedidovenda);
				if(SinedUtil.isListNotEmpty(listaDocumento)){
					for(Documento documento : listaDocumento){
						if(Documentoacao.PREVISTA.equals(documento.getDocumentoacao()) || Documentoacao.DEFINITIVA.equals(documento.getDocumentoacao())){
							pedidovenda.setContasFinanceiroSituacao(ContasFinanceiroSituacao.CONTA_EM_ABERTO);
							break;
						}
					}
					if(pedidovenda.getContasFinanceiroSituacao() == null){
						pedidovenda.setContasFinanceiroSituacao(ContasFinanceiroSituacao.CONTA_BAIXADA);
					}
				}
			}
		}
	}
	
	public void insertPedidoAprovadoTabelaSincronizacaoEcommerce(Pedidovenda bean) {
		Ecom_PedidoVenda ecom_pedidoVenda = Ecom_PedidoVendaService.getInstance().loadByCdPedidoVenda(bean.getCdpedidovenda());
		if(ecom_pedidoVenda != null){
			pedidovendaDAO.insertPedidoAprovadoTabelaSincronizacaoEcommerce(ecom_pedidoVenda);
		}
	}
	
	public boolean isExibeIconeNaoVisualizadoEcommerce(Pedidovenda pedidovenda){
		Ecom_PedidoVenda ecom_PedidoVenda = ecom_PedidoVendaService.loadByCdPedidoVenda(pedidovenda.getCdpedidovenda());
		return ecom_PedidoVenda != null && SinedUtil.isIntegracaoTrayCorp() && parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE)
				&&	!Boolean.TRUE.equals(pedidovenda.getPedidoEcommerceVisualizado());
	}
	
	public void updateCampo(Pedidovenda venda, String nomeCampo, Object valor) {
		pedidovendaDAO.updateCampo(venda, nomeCampo, valor);
	}
	
	public void insertPedidoConfirmadoTabelaSincronizacaoEcommerce(Pedidovenda bean) {
		Ecom_PedidoVenda ecom_pedidoVenda = Ecom_PedidoVendaService.getInstance().loadByCdPedidoVenda(bean.getCdpedidovenda());
		if(ecom_pedidoVenda != null){
			pedidovendaDAO.insertPedidoConfirmadoTabelaSincronizacaoEcommerce(ecom_pedidoVenda);
		}
	}
	
	public void insertPedidoCanceladoTabelaSincronizacaoEcommerce(Pedidovenda bean) {
		Ecom_PedidoVenda ecom_pedidoVenda = Ecom_PedidoVendaService.getInstance().loadByCdPedidoVenda(bean.getCdpedidovenda());
		if(ecom_pedidoVenda != null){
			pedidovendaDAO.insertPedidoCanceladoTabelaSincronizacaoEcommerce(ecom_pedidoVenda);
		}
	}

	public boolean isPedidoEcommerce(Pedidovenda pedidovenda){
		if(Util.objects.isNotPersistent(pedidovenda)){
			return false;
		}
		Ecom_PedidoVenda ecom_PedidoVenda = ecom_PedidoVendaService.loadByCdPedidoVenda(pedidovenda.getCdpedidovenda());
		return ecom_PedidoVenda != null;
	}
	

	public boolean containsPedidoEcommerce (String whereInPedidovenda){
		return ecom_PedidoVendaService.findByWhereInPedidovenda(whereInPedidovenda).size() > 0;
	}

	public boolean isPedidoEcommerce(Integer cdPedidoVenda){
		if(cdPedidoVenda == null){
			return false;
		}		
		return this.isPedidoEcommerce(new Pedidovenda(cdPedidoVenda));
	}

	protected void verificaDiferencaTotal(Double valorFinal, List<Pedidovendapagamento> lista, Integer qtdParcelas, Double juros) {
		if(DoubleUtils.isMaiorQueZero(valorFinal) && SinedUtil.isListNotEmpty(lista) && lista.size() > 1){
			try {
				Double valorfinalpedidovenda = valorFinal;
				Money valortotalpagamentos = new Money(0.0);
				Double diferenca = 0.0;
				
				for(Pedidovendapagamento item : lista){
					valortotalpagamentos = valortotalpagamentos.add(item.getValororiginal().round());
				}
				
				if(valorfinalpedidovenda != null && valortotalpagamentos != null && 
						valorfinalpedidovenda > valortotalpagamentos.getValue().doubleValue()){				
					if(valorfinalpedidovenda % qtdParcelas != 0){
						diferenca = valorfinalpedidovenda - valortotalpagamentos.getValue().doubleValue();
						
						Pedidovendapagamento item = lista.get(lista.size()-1);
						item.setValororiginal(item.getValororiginal().add(diferenca));
					}
				}else if((juros == null || juros == 0) && valortotalpagamentos.getValue().doubleValue() > valorfinalpedidovenda) {
					diferenca = valortotalpagamentos.getValue().doubleValue() - valorfinalpedidovenda;
					
					Pedidovendapagamento item = lista.get(lista.size()-1);					
					item.setValororiginal(item.getValororiginal().subtract(diferenca));
				}
			} catch (Exception e) {}	
		}
	}
	
	public ModelAndView abrirPopupSelecaoPneus(WebRequestContext request, String whereIn) throws Exception {
		try{
			validarFichaEtiquetaPneu(request);
		}catch (Exception e) {
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		EtiquetaPneuFiltro etiquetapneufiltro = new EtiquetaPneuFiltro();
		List<EtiquetaPneuItem> listaetiquetapneuitem = new ArrayList<EtiquetaPneuItem>();
		EtiquetaPneuItem etiquetapneuitem;
		for(String id : whereIn.split(",")){
			List<Pedidovendamaterial> listapedidovendamaterial = pedidovendamaterialService.findByPedidoVendaWithMaterial(id);
			for(Pedidovendamaterial pvm: listapedidovendamaterial){
					etiquetapneuitem = new EtiquetaPneuItem();
					etiquetapneuitem.setCdpedidovendamaterial(pvm.getCdpedidovendamaterial());
					etiquetapneuitem.setCdpedidovenda(pvm.getPedidovenda().getCdpedidovenda());
					etiquetapneuitem.setNome(pvm.getMaterial().getNome());
					etiquetapneuitem.setPneu(pvm.getPneu());
					if(etiquetapneuitem.getPneu()!=null){
						listaetiquetapneuitem.add(etiquetapneuitem);
					}
			}
		}
		etiquetapneufiltro.setListaEtiquetapneuitem(listaetiquetapneuitem);
		
		if(SinedUtil.isListNotEmpty(etiquetapneufiltro.getListaEtiquetapneuitem())){
			request.setAttribute("whereInPedidoVenda", whereIn);
			request.setAttribute("whereInVenda", "");
			request.setAttribute("listaTemplates", emitirFichaColetaReport.buscarlisaTemplates());
			return new ModelAndView("direct:/crud/popup/popupSelecaoPneus", "bean", etiquetapneufiltro);
		}else{
			SinedUtil.fechaPopUp(request);
			return null;
		}
	}
}
