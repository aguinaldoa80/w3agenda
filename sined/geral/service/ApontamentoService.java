package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.GenericValidator;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cargoatividade;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorescala;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.geral.dao.ApontamentoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraRecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraSubReportBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.Apontamentotarefaatraso;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ApontamentoService extends GenericService<Apontamento>{

	private ApontamentoDAO apontamentoDAO;
	private ApontamentoHorasService apontamentoHorasService;
	private ClienteService clienteService;
	private ContratoService contratoService;
	private ColaboradorService colaboradorService;
	private CargoService cargoService;
	private AtividadetipoService atividadetipoService;
	private ProjetoService projetoService;
	private PlanejamentoService planejamentoService;
	private TarefaService tarefaService;
	private EmpresaService empresaService;
	private CalendarioService calendarioService; 
	private EnvioemailService envioemailService;
	private ColaboradorcargoService colaboradorcargoService;
	private FormularhService formularhService;
	
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setCargoService(CargoService cargoService) {this.cargoService = cargoService;}
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {this.atividadetipoService = atividadetipoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {this.planejamentoService = planejamentoService;}
	public void setTarefaService(TarefaService tarefaService) {this.tarefaService = tarefaService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setApontamentoHorasService(ApontamentoHorasService apontamentoHorasService) {this.apontamentoHorasService = apontamentoHorasService;}
	public void setApontamentoDAO(ApontamentoDAO apontamentoDAO) {this.apontamentoDAO = apontamentoDAO;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setCalendarioService(CalendarioService calendarioService) {this.calendarioService = calendarioService;}
	public void setEnvioemailService(EnvioemailService envioemailService) {this.envioemailService = envioemailService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setFormularhService(FormularhService formularhService) {this.formularhService = formularhService;}

	/* singleton */
	private static ApontamentoService instance;
	public static ApontamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ApontamentoService.class);
		}
		return instance;
	}
	
	@Override
	public void saveOrUpdate(Apontamento bean) {
		//Atualiza o total de hora de acordo com os apontamentos lan�ados
		Double qtdeHoras = 0d;
		if (bean.getListaApontamentoHoras() != null && bean.getListaApontamentoHoras().size() > 0){
			for (ApontamentoHoras apontamentoHoras : bean.getListaApontamentoHoras()){
				if (apontamentoHoras.getHrinicio() != null && apontamentoHoras.getHrfim() != null)
					qtdeHoras += SinedDateUtils.calculaDiferencaHoras(apontamentoHoras.getHrinicio().toString(), apontamentoHoras.getHrfim().toString());
			}
		}
		if (qtdeHoras > 0){
			bean.setQtdehoras(qtdeHoras);
		}else if (bean.getQtdehoras()==null){
			bean.setQtdehoras(0D);
		}
		
		if (bean.getSituacao()==null){
			bean.setSituacao(ApontamentoSituacaoEnum.REGISTRADO);
		}
		
		super.saveOrUpdate(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findListagem
	 * 
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @param b 
	 * @param string 
	 */
	public List<Apontamento> findListagem(String whereIn, String orderBy, boolean asc){
		return apontamentoDAO.findListagem(whereIn, orderBy, asc);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findForDiarioObraMaterial
	 * @param dia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<DiarioObraSubReportBean> findForDiarioObraMaterial(Date dia, Projeto projeto){
		return apontamentoDAO.findForDiarioObraMaterial(dia, projeto);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findForDiarioObraCargo
	 * @param dtdia
	 * @param projeto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<DiarioObraSubReportBean> findForDiarioObraCargo(Date dtdia, Projeto projeto) {
		return apontamentoDAO.findForDiarioObraCargo(dtdia, projeto);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findByWhereIn(String)
	 * @author Jo�o Paulo Zica
	 * 
	 */
	public List<Apontamento> findByWhereIn(String whereIn){
		return apontamentoDAO.findByWhereIn(whereIn);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#getTotalHoras
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Double getTotalHoras(Colaboradorescala colaboradorescala) {
		return apontamentoDAO.getTotalHoras(colaboradorescala);
	}

	/**
	 * Preenche os totais para a listagem de apontamento
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#getTotalHoras
	 * 
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	public void preencheTotais(ApontamentoFiltro filtro) {
		filtro.setTotalRecursogeral(apontamentoDAO.getTotalRgHoras(filtro));
		filtro.setTotalRecursohumano(apontamentoDAO.getTotalRhMinutos(filtro));
		filtro.setTotalRecursohumanoQtde(apontamentoDAO.getTotalRhQtde(filtro));
	}
	
	public IReport createReportListagem(ApontamentoFiltro filtro) {
		
		Report report = new Report("projeto/apontamento");
		this.preencheTotais(filtro);
		
		filtro.setPageSize(Integer.MAX_VALUE);
		
		Colaboradorcargo colaboradorcargo = null;
		Formularh formularh = null;
		Money valorhora;
		if(filtro.getColaborador() != null){
			colaboradorcargo = colaboradorcargoService.findCargoAtualForTotalatividade(filtro.getColaborador());
			if(colaboradorcargo != null && colaboradorcargo.getCargo() != null && colaboradorcargo.getCargo().getCdcargo() != null){
				formularh = formularhService.findByCargo(colaboradorcargo.getCargo());
			}
		}
		
		List<Apontamento> lista = this.findListagem(CollectionsUtil.listAndConcatenate(this.findForListagem(filtro).list(), "cdapontamento", ","), "apontamento.dtapontamento", false);
		for (Apontamento apontamento : lista) {
			valorhora = null;
			if(apontamento.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
				apontamento.setListaApontamentoHoras(apontamentoHorasService.findByApontamento(apontamento));
			}
			if(formularh != null && filtro.getColaborador() != null){
				if(apontamento.getApontamentoTipo() != null && apontamento.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
					valorhora = this.getValorAtividade(apontamento, colaboradorcargo, filtro.getColaborador());
					if(valorhora != null){ 
						apontamento.setValorbrutoReceber(new Money(valorhora.getValue().doubleValue()*apontamento.getQtdehoras()));
					}
				}else {
					valorhora = this.getValorAtividade(apontamento, colaboradorcargo, filtro.getColaborador());
					if(valorhora != null){ 
						apontamento.setValorbrutoReceber(new Money(valorhora.getValue().doubleValue()*8));
					}
				}
			}
			if(valorhora == null){
				apontamento.setValorbrutoReceber(new Money());
			}
			
		}
		report.addParameter("EXISTEFORMULA", formularh != null && filtro.getColaborador() != null ? "true" : "false");
		report.addParameter("CLIENTE", filtro.getCliente() != null ? clienteService.load(filtro.getCliente(), "cliente.nome").getNome() : null);
		report.addParameter("CONTRATO", filtro.getContrato() != null ? contratoService.load(filtro.getContrato(), "contrato.descricao").getDescricao() : null);
		report.addParameter("TIPOATIVIDADE", filtro.getAtividadetipo() != null ? atividadetipoService.load(filtro.getAtividadetipo(), "atividadetipo.nome").getNome() : null);
		report.addParameter("PROJETO", filtro.getProjeto() != null ? projetoService.load(filtro.getProjeto(), "projeto.nome").getNome() : null);
		report.addParameter("PLANEJAMENTO", filtro.getPlanejamento() != null ? planejamentoService.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao() : null);
		report.addParameter("TAREFA", filtro.getTarefa() != null ? tarefaService.load(filtro.getTarefa(), "tarefa.descricao").getDescricao() : null);
		report.addParameter("FUNCAO", filtro.getCargo() != null ? cargoService.load(filtro.getCargo(), "cargo.nome").getNome() : null);
		report.addParameter("COLABORADOR", filtro.getColaborador() != null ? colaboradorService.load(filtro.getColaborador(), "colaborador.nome").getNome() : null);
		report.addParameter("PERIODO", SinedUtil.getDescricaoPeriodo(filtro.getDtapontamentoDe(), filtro.getDtapontamentoAte()));
		
		long minutos = ((long)filtro.getTotalRecursohumano()) % 60;
		long horas = ((long)filtro.getTotalRecursohumano()) / 60;
		
		report.addParameter("TOTALGERAL", filtro.getTotalRecursogeral());
		report.addParameter("TOTALHUMANO", horas + ":" + (minutos < 10 ? "0" : "") + minutos);
		
		
		report.setDataSource(lista);
		
		return report;
	}
	
	public Resource gerarCSVListagem(ApontamentoFiltro filtro){
		
		filtro.setPageSize(Integer.MAX_VALUE);
		this.preencheTotais(filtro);
		
		List<Apontamento> lista = this.findListagem(CollectionsUtil.listAndConcatenate(this.findForListagem(filtro).list(), "cdapontamento", ","), "apontamento.dtapontamento", false);;
		for (Apontamento apontamento : lista) {
			if(apontamento.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
				apontamento.setListaApontamentoHoras(apontamentoHorasService.findByApontamento(apontamento));
			}
		}
		
		if( lista == null || lista.isEmpty())
			throw new SinedException("Nenhum registro a ser mostrado.");

		StringBuilder csv = new StringBuilder();
		if(filtro.getCliente() != null){
			csv.append("CLIENTE:;").append(clienteService.load(filtro.getCliente(), "cliente.nome").getNome()).append(";\n");
		}
		if(filtro.getContrato() != null){
			csv.append("CONTRATO:;").append(contratoService.load(filtro.getContrato(), "contrato.descricao").getDescricao()).append(";\n");
		}
		if(filtro.getAtividadetipo() != null){
			csv.append("TIPOATIVIDADE:;").append(atividadetipoService.load(filtro.getAtividadetipo(), "atividadetipo.nome").getNome()).append(";\n");
		}
		if(filtro.getProjeto() != null){
			csv.append("PROJETO:;").append(projetoService.load(filtro.getProjeto(), "projeto.nome").getNome()).append(";\n");
		}
		if(filtro.getPlanejamento() != null){
			csv.append("PLANEJAMENTO:;").append(planejamentoService.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao()).append(";\n");
		}
		if( filtro.getTarefa() != null ){
			csv.append("TAREFA:;").append(tarefaService.load(filtro.getTarefa(), "tarefa.descricao").getDescricao()).append(";\n");
		}
		if(filtro.getCargo() != null ){
			csv.append("FUNCAO:;").append(cargoService.load(filtro.getCargo(), "cargo.nome").getNome()).append(";\n");
		}
		if(filtro.getColaborador() != null){
			csv.append("COLABORADOR:;").append(colaboradorService.load(filtro.getColaborador(), "colaborador.nome").getNome()).append(";\n");
		}
		if(SinedUtil.getDescricaoPeriodo(filtro.getDtapontamentoDe(), filtro.getDtapontamentoAte()) != null){
			csv.append("PERIODO:;").append(SinedUtil.getDescricaoPeriodo(filtro.getDtapontamentoDe(), filtro.getDtapontamentoAte())).append(";\n");
		}
		
		
		
		csv.append("Data;Contrato;Planejamento;Tarefa;Descri��o;Colaborador;Quantidade;Hor�rios;\n");
		
		for (Apontamento apontamento : lista) {
			csv
			.append(apontamento.getDtapontamento() != null ? apontamento.getDtapontamento().toString() : "").append(";")
			.append(apontamento.getContrato() != null ? apontamento.getContrato().getDescricao() : "").append(";")
			.append(apontamento.getPlanejamento() != null ? apontamento.getPlanejamento().getDescricao() : "").append(";")
			.append(apontamento.getTarefa() != null ? apontamento.getTarefa().getDescricao() : "").append(";")
			.append(apontamento.getDescricao() != null ? "\"" + apontamento.getDescricao() + "\"": "").append(";")
			.append(apontamento.getColaborador() != null ? apontamento.getColaborador().getNome() : "").append(";")
			.append(apontamento.getQtdeStr() != null ? apontamento.getQtdeStr() : "").append(";")
			.append(apontamento.getHrinicioApontamento() != null ? apontamento.getHrinicioApontamento().toString() : "")
			.append(apontamento.getHrfimApontamento() != null ? " - "+apontamento.getHrfimApontamento().toString(): "").append(";\n");
		}
		long minutos = ((long)filtro.getTotalRecursohumano()) % 60;
		long horas = ((long)filtro.getTotalRecursohumano()) / 60;
		
		csv
		.append("\nTOTALGERAL:;").append(filtro.getTotalRecursogeral()).append(";")
		.append("\nTOTALHUMANO:;").append(horas + ":" + (minutos < 10 ? "0" : "") + minutos).append(";\n");
		
		Resource resource = new Resource("text/csv", "apontamento_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	public List<DiarioObraRecursosReportBean> getListaDiarioObraMod(Date dtdia, Projeto projeto) {
		return apontamentoDAO.getListaDiarioObra(dtdia, projeto, Tipocargo.MOD);
	}
	
	public List<DiarioObraRecursosReportBean> getListaDiarioObraMoi(Date dtdia, Projeto projeto) {
		return apontamentoDAO.getListaDiarioObra(dtdia, projeto, Tipocargo.MOI);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Verifica se existe um registro de Apontamento na data para o Colaborador.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#existeByColaboradorData(Colaborador colaborador, Date data)
	 * 
	 * @param colaborador
	 * @param data
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public Boolean existeByColaboradorData(Colaborador colaborador, Date data) {
		return apontamentoDAO.existeByColaboradorData(colaborador, data);
	}
	
	/**
	* M�todo para verificar se existe colaborador que est� 24h (considerando dia �til) sem lan�ar as atividades
	* e enviar email para o colaborador e o respons�vel pelo projeto
	* obs: este m�todo ser� executado uma vez por dia
	*  
	*
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	public void verificaAtrasoLancamentoApontamento(){		
		
		Date dataAtual = new Date(System.currentTimeMillis());
		List<Apontamentotarefaatraso> listaApontamentotarefaatraso = tarefaService.buscaTarefaVerificarAtrasoApontamento();		
		
		if(listaApontamentotarefaatraso != null && !listaApontamentotarefaatraso.isEmpty() ){
			List<Apontamento> listaApontamento = new ArrayList<Apontamento>();
			Apontamento apontamento = new Apontamento();
			Empresa empresaPrincipal = empresaService.loadPrincipal();
			Empresa empresa;			
			boolean enviarEmail = false;
			
			for(Apontamentotarefaatraso at : listaApontamentotarefaatraso){
				if(at.getCdtarefa() != null){
					if(apontamento != null && 
							apontamento.getPlanejamento() != null && 
							apontamento.getPlanejamento().getProjeto() != null &&
							apontamento.getPlanejamento().getProjeto().getEmpresa() != null){
						empresa = apontamento.getPlanejamento().getProjeto().getEmpresa();
					} else {
						empresa = empresaPrincipal;
					}
					
					Municipio municipio = null;
					Uf uf = null;
					if(empresa != null && empresa.getCdpessoa() != null){
						empresa = empresaService.loadWithEndereco(empresa);
						if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
							for (Endereco endereco : empresa.getListaEndereco()) {
								if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
									municipio = endereco.getMunicipio();
									if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
										uf = municipio.getUf();
									}
								}
							}
						}
					}
					
					List<Calendario> listaCalendario = new ArrayList<Calendario>();
					Calendario calendario = calendarioService.getByProjeto(new Projeto(at.getCdprojeto()));
					if(calendario == null) {
						listaCalendario = calendarioService.findCalendarioGeral(uf, municipio);
					} else {
						listaCalendario.add(calendario);
					}
					
					listaApontamento = this.buscaApontamentoByTarefa(at.getCdtarefa(), at.getCdpessoa());
					
					if(listaApontamento != null && !listaApontamento.isEmpty()){						
						for(Apontamento a : listaApontamento){
							if(a != null && a.getDtapontamento() != null){								
								if(listaCalendario != null && listaCalendario.size() > 0 &&
										SinedDateUtils.diaValidoCalendar(dataAtual, listaCalendario) &&
										SinedDateUtils.calculaDiferencaDias(a.getDtapontamento(), dataAtual) > 0){
									enviarEmail = true;
									apontamento = a;									
								}
							}else{
								enviarEmail = true;								
							}
							break;
						}
					}else {
						enviarEmail = true;	
					}
					
					if(enviarEmail){
						String tarefadescricao = "";
						
						if(apontamento != null && apontamento.getTarefa() != null && apontamento.getTarefa().getDescricao() != null){
							tarefadescricao = apontamento.getTarefa().getDescricao();
						}						
						
						try {
							
							if (GenericValidator.isEmail(empresa.getEmail()) && GenericValidator.isEmail(at.getEmail())){
							
								String assunto = "Atraso no lan�amento de apontamentos da tarefa " + tarefadescricao;
								String mensagem = "<b>Aviso do sistema:</b> Aten��o! Voc� est� atrasado no registro de apontamento de horas trabalhadas";

								EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
								
								Envioemail envioemail = envioemailService.registrarEnvio(
												empresa.getEmail(),
												assunto,
												mensagem,
												Envioemailtipo.AVISO_ATRASO_LANCAMENTO,
												new Pessoa(at.getCdpessoa()),
												at.getEmail(), 
												at.getPessoaNome(),
												email);
								
								email.setFrom(empresa.getEmail());
								email.setSubject(assunto);
								email.setTo(at.getEmail()); 
								email.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, at.getEmail()));
								email.setCc(at.getResponsavelEmail()); 
						
								email.sendMessage();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						enviarEmail = false;
					}
				
				}
			}
		}
	}
	
	/**
	* M�todo que busca apontamento da tarefa
	*
	* @param cdtarefa
	* @return
	* @since Aug 16, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Apontamento> buscaApontamentoByTarefa(Integer cdtarefa, Integer cdpessoa){
		return apontamentoDAO.buscaApontamentoByTarefa(cdtarefa, cdpessoa);
	}
	
	/**
	 * M�todo que retorna a lista de apontamentos de uma requisicao
	 * @param requisicao
	 * @return
	 * 
	 * @author Rafael Salvio Martins
	 */
	public List<Apontamento> getApontamentosByRequisicao(Requisicao requisicao){
		return apontamentoDAO.getApontamentosByRequisicao(requisicao, null);
	}
	
	/**
	 * M�todo que retorna a lista de apontamentos de uma requisicao
	 * @param requisicao
	 * @return
	 * 
	 * @author Rafael Salvio Martins
	 */
	public List<Apontamento> getApontamentosByRequisicaoAndColaborador(Requisicao requisicao, Colaborador colaborador){
		return apontamentoDAO.getApontamentosByRequisicao(requisicao, colaborador);
	}
	
	/**
	 * M�todo que padroniza a lista de apontamento para registrar as atividades de Ordem de servi�o.
	 * 
	 * @param listaApontamento
	 * @return
	 * 
	 * @author Rafael Salvio Martins
	 */
	public void getTotalHorasForRequisicao(Requisicao bean){
		bean.setQtde(0.0);
		if( bean.getListaApontamento() != null && ! bean.getListaApontamento().isEmpty()){
			Colaborador usuarioColaborador = SinedUtil.getUsuarioComoColaborador();
			for(Apontamento apontamento : bean.getListaApontamento()){
				ApontamentoHoras apontamentoHoras = new ApontamentoHoras();
				if(apontamento.getCdapontamentohoras() != null)
					apontamentoHoras.setCdapontamentohoras(apontamento.getCdapontamentohoras());
				apontamentoHoras.setApontamento(apontamento);
				apontamentoHoras.setHrinicio(apontamento.getHrinicioApontamento());
				apontamentoHoras.setHrfim(apontamento.getHrfimApontamento());
				
				List<ApontamentoHoras> listaApontamentoHoras = new ListSet<ApontamentoHoras>(ApontamentoHoras.class);
				listaApontamentoHoras.add(apontamentoHoras);
				
				bean.setQtde(bean.getQtde() + SinedDateUtils.calculaDiferencaHoras(apontamento.getHrinicioApontamento().toString(), apontamento.getHrfimApontamento().toString()));
				apontamento.setListaApontamentoHoras(listaApontamentoHoras);
				apontamento.setRequisicao(bean);
				apontamento.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
				if(apontamento.getColaborador() == null)
					apontamento.setColaborador(usuarioColaborador);
			}
		}
	}
	
	/**
	 * M�todo que salva a lista de apontamentos de uma requisi��o do colaborador logado 
	 *  
	 * @param requisicao
	 * 
	 * @author Rafael Salvio
	 */
	public void saveApontamentoFromRequisicao(Requisicao requisicao){
		Colaborador colaboradorLogado = SinedUtil.getUsuarioComoColaborador();

		if(colaboradorLogado != null){
			List<Apontamento> listaOriginal = new ArrayList<Apontamento>();
			if(SinedUtil.isUsuarioLogadoAdministrador()){
				listaOriginal  = this.getApontamentosByRequisicao(requisicao);
			}else {
				listaOriginal = this.getApontamentosByRequisicaoAndColaborador(requisicao, colaboradorLogado);
			}
			List<Apontamento> listaDelete = new ListSet<Apontamento>(Apontamento.class);
			
			if(listaOriginal != null && !listaOriginal.isEmpty()){
				for(Apontamento original : listaOriginal){
					Boolean delete = Boolean.TRUE;
					if(requisicao.getListaApontamento() != null && !requisicao.getListaApontamento().isEmpty()){
						for(Apontamento ap : requisicao.getListaApontamento()){
							if(ap.getCdapontamento() != null && ap.getCdapontamento().equals(original.getCdapontamento())){
								delete = Boolean.FALSE;
								break;
							}
						}
					}
					if(delete)
						listaDelete.add(original);
				}
				
				if(listaDelete != null && !listaDelete.isEmpty()){
					apontamentoDAO.deleteMultiplosApontamentos(SinedUtil.listAndConcatenateIDs(listaDelete));
				}
			}
			if(requisicao.getListaApontamento() != null && !requisicao.getListaApontamento().isEmpty()){
				List<Planejamento> listaPlanejamento = null;
				if(requisicao.getProjeto() != null){
					listaPlanejamento = planejamentoService.findAutorizadosByProjeto(requisicao.getProjeto());
				}
				
				for(Apontamento apontamento : requisicao.getListaApontamento()){
					if(apontamento.getCdautorizacaotrabalho()==null && (apontamento.getColaborador().getCdpessoa().equals(colaboradorLogado.getCdpessoa()) || SinedUtil.isUsuarioLogadoAdministrador())){
						apontamento.setContrato(requisicao.getContrato());
						apontamento.setCliente(requisicao.getCliente());
						apontamento.setPlanejamento(listaPlanejamento != null && listaPlanejamento.size() > 0? listaPlanejamento.get(0) : null);
						apontamento.setRequisicao(requisicao);
						saveOrUpdate(apontamento);
					}
				}
			}
		}
	}
	/**
	 * M�todo que retorna o valor total da atividade do Colaborador
	 *
	 * @param empresa
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValortotalAtividadeByColaborador(List<Apontamento> listaApontamento, Colaborador colaborador) {
		Money valortotal = new Money();
		
		if(listaApontamento != null && !listaApontamento.isEmpty()){
			Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtualForTotalatividade(colaborador);
			Money valorhora;
			
			for(Apontamento apontamento : listaApontamento){
				valorhora = this.getValorAtividade(apontamento, colaboradorcargo, colaborador);
				
				if(valorhora != null){
					if(apontamento.getApontamentoTipo() != null && apontamento.getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
						if(apontamento.getQtdehoras() != null)
							valortotal = valortotal.add(new Money(valorhora.getValue().doubleValue()*apontamento.getQtdehoras()));
					}else {
						valortotal = valortotal.add(new Money(valorhora.getValue().doubleValue()*8));
					}
				}
			}
		}
		return valortotal;
	}
	
	/**
	 * M�todo que retorna o valor hora de acordo com as regras.
	 *
 	 * Primeiro: considerar valor hora do contrato relacionado para o colaborador. 
	 * Segundo: obter valor diferenciado do cargo atual do colaborador de acordo com o tipo de atividade escolhido. 
     * Terceiro: obter o valor padr�o do cargo do colaborador.
	 *
	 * @param apontamento
	 * @param colaboradorcargo
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Money getValorAtividade(Apontamento apontamento, Colaboradorcargo colaboradorcargo, Colaborador colaborador){
		boolean achou = false;
		Money valorhora = null;
		
		if(apontamento != null){
			valorhora = apontamento.getValorhora();
			
			if(apontamento.getContrato() != null && apontamento.getContrato().getListaContratocolaborador() != null &&
					!apontamento.getContrato().getListaContratocolaborador().isEmpty()){
				for(Contratocolaborador contratocolaborador : apontamento.getContrato().getListaContratocolaborador()){
					if(contratocolaborador.getColaborador() != null && contratocolaborador.getColaborador().equals(colaborador)){
						if(contratocolaborador.getValorhora() != null && 
								contratocolaborador.getValorhora().getValue().doubleValue() > 0){
							valorhora = contratocolaborador.getValorhora();
							achou = true;
							break;
						}
					}
				}
			}
			if(!achou){
				if(apontamento.getAtividadetipo() != null && colaboradorcargo != null && colaboradorcargo.getCargo() != null && colaboradorcargo.getCargo().getListaCargoatividade() != null &&
						!colaboradorcargo.getCargo().getListaCargoatividade().isEmpty()){
					for(Cargoatividade cargoatividade : colaboradorcargo.getCargo().getListaCargoatividade()){
						if(cargoatividade.getValorhoradiferenciada() != null && cargoatividade.getAtividadetipo() != null && 
								cargoatividade.getAtividadetipo().getCdatividadetipo() != null && cargoatividade.getAtividadetipo().getCdatividadetipo().equals(apontamento.getAtividadetipo().getCdatividadetipo())){
							valorhora = cargoatividade.getValorhoradiferenciada();
							achou = true;
							break;
						}
					}
				}
				
				if(!achou && colaboradorcargo != null && colaboradorcargo.getCargo() != null && colaboradorcargo.getCargo().getCustohora() != null){
					valorhora = colaboradorcargo.getCargo().getCustohora();
				}
			}
		}
		
		return valorhora;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findByColaborador(Empresa empresa, Colaborador colaborador, Date dataGerada)
	 *
	 * @param empresa
	 * @param colaborador
	 * @param dataGerada
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Apontamento> findByColaborador(Empresa empresa, Colaborador colaborador, Date dtreferencia1, Date dtreferencia2, ApontamentoTipo apontamentoTipo, Projeto projeto, ApontamentoSituacaoEnum situacao) {
		return apontamentoDAO.findByColaborador(empresa, colaborador, dtreferencia1, dtreferencia2, apontamentoTipo, projeto, situacao);
	}
	
	/**
	 * 
	 * @param itens
	 * @author Thiago Clemente
	 * 
	 */
	public boolean autorizar(String itens){
		return apontamentoDAO.autorizar(itens);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#updateSituacao(ApontamentoSituacaoEnum situacao, String whereIn)
	 *
	 * @param situacao
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateSituacao(ApontamentoSituacaoEnum situacao, String whereIn) {
		apontamentoDAO.updateSituacao(situacao, whereIn);	
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#findForFaturarByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 * @since 03/12/2013
	 */
	public List<Apontamento> findForFaturarByContrato(Contrato contrato) {
		return apontamentoDAO.findForFaturarByContrato(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoDAO#getTotalHoraByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @author Luiz Fernando
	 * @since 03/12/2013
	 */
	public Double getTotalHoraByContrato(Contrato contrato) {
		return apontamentoDAO.getTotalHoraByContrato(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 	
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Apontamento> findAllForAtualizarRateio(Date dtinicio, Date dtfim){
		return apontamentoDAO.findAllForAtualizarRateio(dtinicio, dtfim);
	}
}