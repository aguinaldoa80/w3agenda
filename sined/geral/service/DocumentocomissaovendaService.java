package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentocomissao;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamento;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendapagamento;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.DocumentocomissaovendaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentocomissaovendaService extends GenericService<Documentocomissao>{

	private DocumentocomissaovendaDAO documentocomissaovendaDAO;
	private VendaService vendaService;
	private VendapagamentoService vendapagamentoService;
	private VendamaterialService vendamaterialService;
	private PedidovendapagamentoService pedidovendapagamentoService;
	private PedidovendamaterialService pedidovendamaterialService;
	private PedidovendaService pedidovendaService;
	private DocumentocomissaovendaService documentocomissaovendaService;
	private VdocumentonegociadoService vdocumentonegociadoService;
	private DocumentocomissaoService documentocomissaoService;
	private ComissionamentoService comissionamentoService;
	
	public void setDocumentocomissaovendaDAO(
			DocumentocomissaovendaDAO documentocomissaovendaDAO) {
		this.documentocomissaovendaDAO = documentocomissaovendaDAO;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setVendapagamentoService(VendapagamentoService vendapagamentoService) {
		this.vendapagamentoService = vendapagamentoService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setPedidovendapagamentoService(PedidovendapagamentoService pedidovendapagamentoService) {
		this.pedidovendapagamentoService = pedidovendapagamentoService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setDocumentocomissaovendaService(DocumentocomissaovendaService documentocomissaovendaService) {
		this.documentocomissaovendaService = documentocomissaovendaService;
	}
	public void setVdocumentonegociadoService(VdocumentonegociadoService vdocumentonegociadoService) {this.vdocumentonegociadoService = vdocumentonegociadoService;}
	public void setDocumentocomissaoService(DocumentocomissaoService documentocomissaoService) {this.documentocomissaoService = documentocomissaoService;}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForTotalDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public Money findForTotalDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro){
		return documentocomissaovendaDAO.findForTotalDocumentocomissaovenda(filtro);
	}

	/**
	 * M�todo que verifica o comissionamento caso o vendedor seja alterado
	 * 
	 * @see  br.com.linkcom.sined.geral.service.VendaService#verificaComissionamento(Vendapagamento vendapagamento, Integer qtdeParcelas, List<Documentocomissao> listaDocumentocomissao, Colaborador vendedoranterior)
	 * @see  br.com.linkcom.sined.geral.service.VendapagamentoService#findVendaPagamentoByVenda(Venda venda)
	 * @see  br.com.linkcom.sined.geral.service.VendamaterialService#findByVenda(Venda venda)
	 *
	 * @param venda
	 * @param colaborador
	 * @author Luiz Fernando
	 */
	public void recalcularComissaovenda(Venda venda, Colaborador colaborador) {
		Venda bean = vendaService.loadForEntrada(venda);
		bean.setListavendamaterial(vendamaterialService.findByVenda(bean));
		
		Comissionamento comissionamentoPorFaixa = comissionamentoService.findByCriterio(Criteriocomissionamento.FAIXA_MARKUP);
		
		if (comissionamentoPorFaixa != null 
				&& vendaService.existemItensComMaterialFaixaMarkup(bean.getListavendamaterial())) {
			
			if(bean.getVendasituacao().equals(Vendasituacao.REALIZADA) || bean.getVendasituacao().equals(Vendasituacao.FATURADA)){
				documentocomissaoService.deleteDocumentocomissaoByVendaCancelada(venda);
				vendaService.gerarComissionamentoPorFaixas(bean, comissionamentoPorFaixa);
				venda.setGerouComissionamentoPorFaixas(true);
			}
		} else {
			List<Documentocomissao> lista = findForRecalcularComissaoNotPedidovenda(venda);
			bean = vendaService.loadForRecalcularcomissao(venda);
			bean.setListavendapagamento(vendapagamentoService.findVendaPagamentoByVenda(bean));
//			bean.setListavendamaterial(vendamaterialService.findByVenda(bean));
			
			boolean contaReceberVinculadaVenda = true;
//			if(bean != null && 
//					bean.getPedidovendatipo() != null &&
//					bean.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
//					!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_VENDAREALIZADA) &&
//					(!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_EMISSAONOTA) || 
//							(bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_EMISSAONOTA) 
//									&& Boolean.TRUE.equals(bean.getPedidovendatipo().getReserva()) 
//									&& bean.getPedidovenda() != null 
//									&& Pedidovendasituacao.CONFIRMADO.equals(bean.getPedidovenda().getPedidovendasituacao())
//									&& !pedidovendaService.existeVariasVendas(bean.getPedidovenda()))) 
//									&& !vendaService.existeDocumento(bean)){
//				contaReceberVinculadaVenda = false;		
//			}
			
			if(bean.getPedidovenda() != null && pedidovendapagamentoService.existeDocumento(bean.getPedidovenda())){
				contaReceberVinculadaVenda = false;
			}
			
			if(contaReceberVinculadaVenda && !documentocomissaovendaService.existeComissaoRepassadaParaColaborador(null, venda, null) 
					&& (bean.getPedidovenda() == null || !documentocomissaovendaService.existeComissaoRepassadaParaColaborador(null, null, bean.getPedidovenda()))){
				for (Vendapagamento vendapagamento : bean.getListavendapagamento()) {			
					vendaService.verificaComissionamento(vendapagamento, bean.getListavendapagamento().size(), lista, colaborador);
				}
			}
			
			bean = vendaService.loadWithPedidovenda(venda);
			
			if(bean != null && 
					bean.getPedidovenda() != null && 
					bean.getPedidovenda().getCdpedidovenda() != null &&
					!contaReceberVinculadaVenda){
				Venda vendaComComissaoVinculada = vendaService.loadUltimaVendaNaoCanceladaByPedido(bean.getPedidovenda());
				if(vendaComComissaoVinculada == null){
					vendaComComissaoVinculada = bean;
				}
				lista = findForRecalcularComissaoWithPedidovenda(vendaComComissaoVinculada);
				Pedidovenda pedidovenda = bean.getPedidovenda();
				
				pedidovenda.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(pedidovenda));
				pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(pedidovenda));
				
				if (!documentocomissaovendaService.existeComissaoRepassadaParaColaborador(null, null, pedidovenda)) {
					for (Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()) {			
						pedidovendaService.verificaComissionamento(pedidovendapagamento, pedidovenda.getListaPedidovendapagamento().size(), lista, colaborador, vendaComComissaoVinculada,false);
					}
					if(!bean.equals(vendaComComissaoVinculada)){//Se for a mesma venda, quer dizer que a venda n�o est� cancelada e o sistema j� fez o rec�lculo
						pedidovendaService.verificaCancelamentoOuRecalculoComissionamento(bean, true);
					}
				}
			}
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissao(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda) {
		return findForRecalcularComissaoNotPedidovenda(venda, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento)
	*
	* @param venda
	* @param documento
	* @return
	* @since 15/05/2015
	* @author Luiz Fernando
	*/
	public List<Documentocomissao> findForRecalcularComissaoNotPedidovenda(Venda venda, Documento documento) {
		return documentocomissaovendaDAO.findForRecalcularComissaoNotPedidovenda(venda, documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#findForRecalcularComissaoWithPedidovenda(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentocomissao> findForRecalcularComissaoWithPedidovenda(Venda venda) {
		return documentocomissaovendaDAO.findForRecalcularComissaoWithPedidovenda(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#deleteDocumentocomissaovenda(Documentocomissao documentocomissao)
	 *
	 * @param documentocomissao
	 * @author Luiz Fernando
	 */
	public void deleteDocumentocomissaovenda(Documentocomissao documentocomissao) {
		documentocomissaovendaDAO.deleteDocumentocomissaovenda(documentocomissao);		
	}
	public List<Documentocomissao> findForPDF(
			DocumentocomissaovendaFiltro filtro) {
		
		return documentocomissaovendaDAO.findForPDF(filtro);
	}	
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForTotalGeralDocumentocomissaovenda
	 * @param filtro
	 * @return
	 * @return Money
	 * @author Thiago Augusto
	 * @date 24/08/2012
	 *
	 */
	public Money findForTotalGeralDocumentocomissaovenda(DocumentocomissaovendaFiltro filtro){
		return documentocomissaovendaDAO.findForTotalGeralDocumentocomissaovenda(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentocomissaovendaDAO#existeComissaoRepassadaParaColaborador(Colaborador colaborador, Venda venda, Pedidovenda pedidovenda)
	*
	* @param colaborador
	* @param venda
	* @param pedidovenda
	* @return
	* @since 16/09/2015
	* @author Luiz Fernando
	*/
	public Boolean existeComissaoRepassadaParaColaborador(Colaborador colaborador, Venda venda, Pedidovenda pedidovenda) {
		return documentocomissaovendaDAO.existeComissaoRepassadaParaColaborador(colaborador, venda, pedidovenda);
	}
	
	public void recalcularComissaopedidovenda(Pedidovenda pedidovenda, Colaborador colaborador) {
		List<Documentocomissao> lista = findForRecalcularComissao(pedidovenda);
		Pedidovenda bean = pedidovendaService.loadForRecalcularcomissao(pedidovenda);
		List<Venda> listaVenda = vendaService.findByPedidovenda(pedidovenda);
		Venda vendaComissao = vendaService.loadUltimaVendaNaoCanceladaByPedido(pedidovenda);
		if(vendaComissao != null){
			vendaComissao = SinedUtil.isListNotEmpty(listaVenda) ? listaVenda.get(0) : null;
		}
		bean.setListaPedidovendapagamento(pedidovendapagamentoService.findPedidovendaPagamentoByPedidovenda(bean));
		bean.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidovenda(bean));
		
		boolean geranotaaposvenda = true;
//		if(bean != null && 
//				bean.getPedidovendatipo() != null &&
//				bean.getPedidovendatipo().getGeracaocontareceberEnum() != null &&
//				!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_VENDAREALIZADA) &&
//				(!bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_EMISSAONOTA) || 
//						(bean.getPedidovendatipo().getGeracaocontareceberEnum().equals(GeracaocontareceberEnum.APOS_EMISSAONOTA) 
//								&& Boolean.TRUE.equals(bean.getPedidovendatipo().getReserva()) 
//								&& bean != null 
//								&& Pedidovendasituacao.CONFIRMADO.equals(bean.getPedidovendasituacao())
//								&& !pedidovendaService.existeVariasVendas(bean))) 
//								&& !pedidovendaService.existeDocumento(bean)){
//			geranotaaposvenda = false;		
//		}
		
		boolean existeComissaoRepassadaParaColaborador = documentocomissaovendaService.existeComissaoRepassadaParaColaborador(null, null, pedidovenda);
		if(!existeComissaoRepassadaParaColaborador && SinedUtil.isListNotEmpty(listaVenda)){
			for(Venda venda : listaVenda){
				if(documentocomissaovendaService.existeComissaoRepassadaParaColaborador(null, venda, null)){
					existeComissaoRepassadaParaColaborador = true;
					break;
				}
			}
		}
		
		if(geranotaaposvenda && !existeComissaoRepassadaParaColaborador){
			for (Pedidovendapagamento pedidovendapagamento : bean.getListaPedidovendapagamento()) {
				pedidovendaService.verificaComissionamento(pedidovendapagamento, bean.getListaPedidovendapagamento().size(), lista, colaborador, vendaComissao,false);
			}
			if(SinedUtil.isListNotEmpty(listaVenda)){
				pedidovendaService.verificaCancelamentoOuRecalculoComissionamento(listaVenda.get(0), true);
			}
		}
		
//		lista = findForRecalcularComissaoPedidovenda(pedidovenda, SinedUtil.isListNotEmpty(listaVenda) ? CollectionsUtil.listAndConcatenate(listaVenda, "cdvenda", ",") : null);
//		bean = pedidovendaService.loadPedidovenda(pedidovenda);
//		
//		if(bean != null && 
//				bean != null && 
//				bean.getCdpedidovenda() != null &&
//				!geranotaaposvenda &&
//				!existeComissaoRepassadaParaColaborador){
//			pedidovenda.setListaPedidovendapagamento(pedidovendapagamentoService.findByPedidovenda(bean));
//			pedidovenda.setListaPedidovendamaterial(pedidovendamaterialService.findByPedidoVenda(bean));
//			
//			for (Pedidovendapagamento pedidovendapagamento : pedidovenda.getListaPedidovendapagamento()) {
//				pedidovendaService.verificaComissionamento(pedidovendapagamento, pedidovenda.getListaPedidovendapagamento().size(), lista, colaborador, vendaComissao);
//			}
//		}
	}
	
	public List<Documentocomissao> findForRecalcularComissaoPedidovenda(Pedidovenda pedidovenda, String whereInVenda) {
		return documentocomissaovendaDAO.findForRecalcularComissaoPedidovenda(pedidovenda, whereInVenda);
	}
	
	private List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda) {
		return findForRecalcularComissao(pedidovenda, null);
	}
	
	private List<Documentocomissao> findForRecalcularComissao(Pedidovenda pedidovenda, Documento documento) {
		return documentocomissaovendaDAO.findForRecalcularComissao(pedidovenda, documento);
	}

	public void processarListagem(ListagemResult<Documentocomissao> listagemResult, 
			DocumentocomissaovendaFiltro filtro) {
		List<Documentocomissao> list = listagemResult.list();
		
		Money totalDocumentos = new Money();
		Money totalComissao = new Money();
		List<Documento> listaDocumentosUtilizados = new ArrayList<Documento>();
		
		for (Documentocomissao documentocomissao : list) {
			if (documentocomissao.getDocumento() != null && documentocomissao.getDocumento().getAux_documento() != null && 
					documentocomissao.getDocumento().getAux_documento().getValoratual() != null){
				if(!listaDocumentosUtilizados.contains(documentocomissao.getDocumento())){
					totalDocumentos = totalDocumentos.add(documentocomissao.getDocumento().getAux_documento().getValoratual());
					listaDocumentosUtilizados.add(documentocomissao.getDocumento());
				}
			}
			if (documentocomissao.getValorcomissao() != null)
				totalComissao = totalComissao.add(documentocomissao.getValorcomissao());
		}
		
		filtro.setTotal(totalDocumentos);
		filtro.setTotalComissao(totalComissao);
		
		String whereInDocumentos = CollectionsUtil.listAndConcatenate(list, "documento.cddocumento", ",");
		
		if(whereInDocumentos != null && !whereInDocumentos.equals("")){
			List<Vdocumentonegociado> listaNegociados = vdocumentonegociadoService.findByDocumentos(whereInDocumentos);
			
			for (Documentocomissao documentocomissao : list) {
				if(documentocomissao.getDocumento() != null) {
					List<Documento> documentosNegociados = new ArrayList<Documento>();
					for (Vdocumentonegociado v : listaNegociados) {
						if(v.getCddocumento().equals(documentocomissao.getDocumento().getCddocumento())){
							documentosNegociados.add(new Documento(v.getCddocumentonegociado(), 
																	v.getCddocumentoacaonegociado(), 
																	v.getDtvencimentonegociado(), 
																	v.getDtemissaonegociado()));
						}
					}
					documentocomissao.setDocumentosNegociados(documentosNegociados);
				}
			}
		}
	}
	
	public Resource gerarListagemCSV(DocumentocomissaovendaFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Documentocomissao> listagemResult = findForExportacao(filtro);
		processarListagem(listagemResult, filtro);
		
		String cabecalho = "\"Cliente \";\"Pedido de Venda/Venda\";\"Parcela\";\"Valor da Conta a Receber\";\"Valor da Comiss�o\";\"Pago\";\"Repassado\";\"Valor Repassado\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Documentocomissao bean : listagemResult.list()) {

			if(bean.getPedidovenda() != null && bean.getPedidovenda().getCliente() != null) 
				csv.append("\"" + bean.getPedidovenda().getCliente().getNome() + "\";");
			else if(bean.getVenda() != null && bean.getVenda().getCliente() != null) 
				csv.append("\"" + bean.getVenda().getCliente().getNome() + "\";");
			else
				csv.append("\"\";");
			
			if(bean.getPedidovenda() != null && bean.getPedidovenda().getCdpedidovenda() != null)
				csv.append("\"" + bean.getPedidovenda().getCdpedidovenda() + "\";");
			else if(bean.getVenda() != null && bean.getVenda().getCdvenda() != null)
				csv.append("\"" + bean.getVenda().getCdvenda() + "\";");
			else
				csv.append("\"\";");
			
			csv.append("\"");
			if(bean.getDocumento() != null && bean.getDocumento().getCddocumento() != null) {
				csv.append(bean.getDocumento().getCddocumento());
				List<Documento> negociados = bean.getDocumentosNegociados();
				
				if(negociados != null && !negociados.isEmpty()) {
					csv.append("\nNegocia��o:\n");
					
					for(Documento documento : negociados) {
						if(negociados.indexOf(documento) != negociados.size() - 1)
							csv.append(documento.getCddocumento() + ", ");
						else
							csv.append(documento.getCddocumento() + "");
					}
				}
			}
			csv.append("\";");
			
			csv.append("\"" + bean.getDocumento().getAux_documento().getValoratual() + "\";");
			csv.append("\"" + bean.getValorcomissao() + "\";");
			csv.append("\"" + (bean.getPago() != null && bean.getPago() ? "Sim" : "N�o") + "\";");
			
			if(bean.getColaboradorcomissao() == null 
					|| bean.getColaboradorcomissao().getCdcolaboradorcomissao() == null
					|| bean.getColaboradorcomissao().getDocumento() == null 
					|| bean.getColaboradorcomissao().getDocumento().getDocumentoacao() == null
					|| bean.getColaboradorcomissao().getDocumento().getDocumentoacao().getCddocumentoacao() == 0) {
				
				csv.append("\"N�o\";");
				csv.append("\" - \";");
			} else {
				csv.append("\"Sim\";");
				csv.append("\"" + bean.getValorcomissao() + "\";");
			}
			
			csv.append("\n");
		}

		return new Resource("text/csv", "controlar_comissionamento_venda.csv", csv.toString().getBytes());
	}
}
