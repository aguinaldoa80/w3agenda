package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Garantiareformahistorico;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Garantiareformaitemconstatacao;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.OtrPneuTipoOtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.auxiliar.GarantiareformaClienteBean;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.geral.dao.GarantiareformaDAO;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ComprovanteGarantiareformaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaClienteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaEmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaPneuBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaResultadoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

import com.ibm.icu.text.SimpleDateFormat;

public class GarantiareformaService extends GenericService<Garantiareforma>{

	private GarantiareformaDAO garantiareformaDAO;
	private GarantiareformaitemService garantiareformaitemService;
	private GarantiareformahistoricoService garantiareformahistoricoService;
	private ProducaoordemService producaoordemService;
	private ColetaMaterialService coletaMaterialService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ColetahistoricoService coletahistoricoService;
	private ProducaoetapaService producaoetapaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private PneuService pneuService;
	private PedidovendaService pedidovendaService;
	private OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService;
	
	private static GarantiareformaService instance;
	
	public static GarantiareformaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(GarantiareformaService.class);
		}
		return instance;
	}
	
	public void setGarantiareformaDAO(GarantiareformaDAO garantiareformaDAO) {
		this.garantiareformaDAO = garantiareformaDAO;
	}
	
	public void updateSituacaoGarantia(String whereIn, Garantiasituacao garantiasituacao){
		garantiareformaDAO.updateSituacaoGarantia(whereIn, garantiasituacao);
	}
	
	public List<Garantiareforma> findForCalculargarantia(String whereIn){
		return garantiareformaDAO.findForCalculargarantia(whereIn, null);
	}
	
	public List<Garantiareforma> findByWherein(String whereIn){
		return garantiareformaDAO.findByWherein(whereIn);
	}
	
	public List<Garantiareforma> findForComprovanteGarantia(String whereIn){
		return garantiareformaDAO.findForComprovanteGarantia(whereIn);	
	}
	
	public void setGarantiareformaitemService(GarantiareformaitemService garantiareformaitemService) {
		this.garantiareformaitemService = garantiareformaitemService;
	}
	
	public void setGarantiareformahistoricoService(GarantiareformahistoricoService garantiareformahistoricoService) {
		this.garantiareformahistoricoService = garantiareformahistoricoService;
	}
	
	public void setProducaoordemService(ProducaoordemService producaoordemService) {
		this.producaoordemService = producaoordemService;
	}
	
	public void setColetaMaterialService(ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	
	public void setColetahistoricoService(ColetahistoricoService coletahistoricoService) {
		this.coletahistoricoService = coletahistoricoService;
	}
	
	public void setProducaoetapaService(ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setOtrPneuTipoOtrClassificacaoCodigoService(OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService) {
		this.otrPneuTipoOtrClassificacaoCodigoService = otrPneuTipoOtrClassificacaoCodigoService;
	}

	public List<ComprovanteGarantiareformaBean> montaListaComprovanteGarantia(String whereIn){
		List<Garantiareforma> listaGarantia = this.findForComprovanteGarantia(whereIn);
		List<ComprovanteGarantiareformaBean> listaRelatorio = new ArrayList<ComprovanteGarantiareformaBean>();
		Empresa empresaprincipal = empresaService.loadPrincipal();
		for(Garantiareforma garantiareforma: listaGarantia){
			for(Garantiareformaitem garantiareformaitem: garantiareforma.getListaGarantiareformaitem()){
				Pneu pneu = garantiareformaitem.getPneu();
				ComprovanteGarantiareformaBean bean = new ComprovanteGarantiareformaBean();
				bean.setCdgarantia(garantiareforma.getCdgarantiareforma().toString());
				bean.setGarantiasituacao(garantiareforma.getGarantiasituacao().toString());
				bean.setDtgarantia(new SimpleDateFormat("dd/MM/yyyy").format(garantiareforma.getDtgarantia()));
				bean.setCliente(garantiareforma.getCliente().getNome());
//				if(garantiareformaitem.getConstatacao() != null)
//					bean.setConstatacao(garantiareformaitem.getConstatacao().getDescricao());
				if(garantiareformaitem.getListaGarantiareformaitemconstatacao()!=null)
					bean.setConstatacao(CollectionsUtil.listAndConcatenate(garantiareformaitem.getListaGarantiareformaitemconstatacao(), "motivodevolucao.descricao", ","));
				bean.setPneu(pneu.getCdpneu().toString());
				bean.setDot(pneu.getDot());
				bean.setFogo(pneu.getSerie());
				if(pneu.getPneumedida() != null)
					bean.setMedidapneu(pneu.getPneumedida().getNome());
				if(pneu.getMaterialbanda() != null){
					if(pneu.getMaterialbanda().getProfundidadesulco() != null)
						bean.setProfundidadesulco(pneu.getMaterialbanda().getProfundidadesulco().toString());
					bean.setDesenhopneu(pneu.getMaterialbanda().getNome());
				}
				if(garantiareformaitem.getResiduobanda() != null)
					bean.setResiduobanda(garantiareformaitem.getResiduobanda().toString());
				if(garantiareforma.getPedidovenda() != null){
					bean.setOsreclamacao(garantiareforma.getPedidovenda().getCdpedidovenda().toString());
					
					if(garantiareforma.getPedidovenda().getEndereco() != null){
						bean.setEnderecocliente(garantiareforma.getPedidovenda().getEndereco().getLogradouroCompletoComBairro());
					}
				}else{
					
					if(garantiareforma.getCliente().getEndereco() != null){
						bean.setEnderecocliente(garantiareforma.getCliente().getEndereco().getLogradouroCompletoComBairro());
					}
				}
				if(Boolean.TRUE.equals(garantiareforma.getPedidoexterno())){
					if(garantiareforma.getIdentificadorexternopedidovenda() != null){
						bean.setOsreclamacao(garantiareforma.getIdentificadorexternopedidovenda());
					}
				}else if(garantiareforma.getPedidovendaorigem() != null){
					bean.setOsrelacionada(garantiareforma.getPedidovendaorigem().getCdpedidovenda().toString());
				}
				
				if(garantiareforma.getVendedor() != null){
					bean.setVendedor(garantiareforma.getVendedor().getNome());
				}
				
				bean.setServicogarantido(garantiareformaitem.getMaterial().getNome());
				bean.setCodigoservicogarantido(garantiareformaitem.getMaterial().getIdentificacao());
				if(pneu.getPneumarca() != null)
					bean.setMarcapneu(pneu.getPneumarca().getNome());
				
				if(garantiareforma.getGarantiareformaresultado() != null){
					String strPencent = garantiareformaitem.getGarantiatipopercentual() != null? garantiareformaitem.getGarantiatipopercentual().getPercentual().toString(): "";
					if(Util.strings.isNotEmpty(garantiareforma.getGarantiareformaresultado().getMensagem1())){
						bean.setResultado(garantiareforma.getGarantiareformaresultado().getMensagem1().replace(GarantiareformaresultadoService.VARIAVEL_PERCENTUAL, strPencent));
					}
					if(Util.strings.isNotEmpty(garantiareforma.getGarantiareformaresultado().getMensagem2())){
						bean.setMensagemDestino(garantiareforma.getGarantiareformaresultado().getMensagem2().replace(GarantiareformaresultadoService.VARIAVEL_PERCENTUAL, strPencent));
					}
				}else{
					if(garantiareformaitem.getGarantiatipopercentual() != null){
						bean.setResultado("Concess�o de cr�dito de "+garantiareformaitem.getGarantiatipopercentual().getPercentual().toString()+"% em futuras reformas.");
					}else{
						bean.setResultado("Sem concess�o de cr�dito em futuras reformas.");
					}
				}
				bean.setReimpressao(Boolean.TRUE.equals(garantiareforma.getImpresso()));
				Image logo = SinedUtil.getLogo(garantiareforma.getEmpresa());
				if(logo == null && !empresaprincipal.equals(garantiareforma.getEmpresa())){
					logo = SinedUtil.getLogo(empresaprincipal);
				}
				bean.setLogo(logo);
				
				listaRelatorio.add(bean);
			}
		}
		return listaRelatorio;
	}
	
	public IReport createComprovanteGarantiareforma(String whereIn){
		Report report = new Report("/producao/comprovanteGarantiareforma");
		report.setDataSource(this.montaListaComprovanteGarantia(whereIn));
		this.sinalizaGarantiareformaComoImpressa(whereIn);
		return report;
	}
	
	public List<Garantiareforma> findByNotInGarantiasituacao(String whereIn, Garantiasituacao...situacoes){
		return garantiareformaDAO.findByNotInGarantiasituacao(whereIn, situacoes);
	}
	
	public List<Garantiareforma> findDisponiveis(Cliente cliente, String whereNotInGarantiareformaitem){
		return garantiareformaDAO.findDisponiveis(cliente, whereNotInGarantiareformaitem);
	}
	
	public void sinalizaGarantiareformaComoImpressa(String whereIn){
		garantiareformaDAO.sinalizaGarantiareformaComoImpressa(whereIn);
	}
	
	/**
	 * @param producaoordem
	 * @param chaoFabrica
	 * @param devolucao
	 */
	public void saveProducao(Producaoordem producaoordem, boolean chaoFabrica, boolean devolucao){
		producaoordem = producaoordemService.loadForGarantia(producaoordem);
		
		if (producaoordem!=null){
			for (Producaoordemmaterial producaoordemmaterial: producaoordem.getListaProducaoordemmaterial()){
				if (!chaoFabrica){
					if (!Boolean.TRUE.equals(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome().getPermitirgerargarantia())){
						continue;
					}
				}
				
				if (!devolucao){
					if (!producaoetapaService.isUltimaEtapa(producaoordemmaterial.getProducaoetapaitem())){
						continue;
					}
				}
				
				Pedidovendamaterial pedidovendamaterial = producaoordemmaterial.getPedidovendamaterial();
				Pedidovenda pedidovenda = pedidovendamaterial.getPedidovenda();
				Garantiasituacao garantiasituacao = Boolean.TRUE.equals(producaoordemmaterial.getGarantiatipo().getGeragarantia()) ? Garantiasituacao.DISPONIVEL : Garantiasituacao.INCONFORME;
				String descricaoConstatacao = "";
				
				//TODO - Ir� ser alterado para N devolu��es nas pr�ximas vers�es
//				if (motivodevolucao==null){
//					if (producaoordem.getListaProducaoordemconstatacao()!=null && producaoordem.getListaProducaoordemconstatacao().size()==1){
//						motivodevolucao = new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class, producaoordem.getListaProducaoordemconstatacao()).get(0).getMotivodevolucao();
//					}
//				}
				
				Pedidovendamaterial pedidovendamaterialValidacao = pedidovendamaterialService.findByReformapneu(pedidovenda.getPedidovendaorigem(), pedidovendamaterial.getPedidovendamaterialgarantido().getMaterial(), producaoordemmaterial.getPneu(), null);
				if (pedidovendamaterialValidacao==null){
					continue;
				}
				
				Garantiareforma garantiareforma = new Garantiareforma();
				garantiareforma.setEmpresa(producaoordem.getEmpresa());
				garantiareforma.setDtgarantia(SinedDateUtils.currentDate());
				garantiareforma.setCliente(pedidovenda.getCliente());
				garantiareforma.setPedidovenda(pedidovenda);
				garantiareforma.setPedidovendaorigem(pedidovenda.getPedidovendaorigem());
				garantiareforma.setIdentificadorexternopedidovenda(pedidovenda.getIdentificacaoexterna());
				garantiareforma.setVendedor(pedidovenda.getColaborador());
				garantiareforma.setPedidoexterno(Boolean.FALSE);
				garantiareforma.setGarantiasituacao(garantiasituacao);
				saveOrUpdate(garantiareforma);
				
				Garantiareformaitem garantiareformaitem = new Garantiareformaitem();
				garantiareformaitem.setGarantiareforma(garantiareforma);
				garantiareformaitem.setMaterial(pedidovendamaterial.getPedidovendamaterialgarantido().getMaterial());
				garantiareformaitem.setPneu(producaoordemmaterial.getPneu());
				garantiareformaitem.setResiduobanda(producaoordemmaterial.getResiduobanda());
				garantiareformaitem.setConstatacao(null);
				garantiareformaitem.setGarantiatipo(producaoordemmaterial.getGarantiatipo());
				garantiareformaitem.setGarantiatipopercentual(producaoordemmaterial.getGarantiatipopercentual());
				garantiareformaitem.setProducaoordemmaterial(producaoordemmaterial);

				if (producaoordem.getListaProducaoordemconstatacao()!=null && !producaoordem.getListaProducaoordemconstatacao().isEmpty()){
					garantiareformaitem.setSaveListaGarantiareformaitemconstatacao(true);
					List<Garantiareformaitemconstatacao> listaGarantiareformaitemconstatacao = new ArrayList<Garantiareformaitemconstatacao>();
					
					for (Producaoordemconstatacao producaoordemconstatacao: producaoordem.getListaProducaoordemconstatacao()){
						Garantiareformaitemconstatacao garantiareformaitemconstatacao = new Garantiareformaitemconstatacao();
						garantiareformaitemconstatacao.setGarantiareformaitem(garantiareformaitem);
						garantiareformaitemconstatacao.setMotivodevolucao(producaoordemconstatacao.getMotivodevolucao());
						listaGarantiareformaitemconstatacao.add(garantiareformaitemconstatacao);
						
						descricaoConstatacao += producaoordemconstatacao.getMotivodevolucao().getDescricao() + ", ";
					}
					
					garantiareformaitem.setListaGarantiareformaitemconstatacao(listaGarantiareformaitemconstatacao);
					
					if (!descricaoConstatacao.equals("")){
						descricaoConstatacao = descricaoConstatacao.substring(0, descricaoConstatacao.length()-2);
					}
				}
				
				garantiareformaitemService.saveOrUpdate(garantiareformaitem);
				
				Garantiareformahistorico garantiareformahistorico = new Garantiareformahistorico();
				garantiareformahistorico.setGarantiareforma(garantiareforma);
				garantiareformahistorico.setAcao(garantiareforma.getGarantiasituacao());
				garantiareformahistorico.setObservacao(chaoFabrica ? "Garantia gerada via Ch�o de F�brica" : "Garantia gerada via Ordem de Produ��o");
				garantiareformahistoricoService.saveOrUpdate(garantiareformahistorico);
				
				if (!chaoFabrica){
					List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByPedidovendamaterial(pedidovendamaterial.getCdpedidovendamaterial().toString());
					Pedidovendahistorico pedidovendahistorico;
					Coletahistorico coletahistorico;
					
					String historicoPedidoVendaColeta = "Garantia: " + producaoordemmaterial.getGarantiatipo().getDescricao() + ", ";
					if (producaoordemmaterial.getGarantiatipopercentual()==null){
						historicoPedidoVendaColeta += "0%";
					}else {
						historicoPedidoVendaColeta += new DecimalFormat("###,##0.##").format(producaoordemmaterial.getGarantiatipopercentual().getPercentual()) + "%";
					}
					historicoPedidoVendaColeta += ", garantia <a href=\"javascript:visualizarGarantiaReforma(" + garantiareforma.getCdgarantiareforma() + ");\">" + garantiareforma.getCdgarantiareforma() + "</a>";
					
					//Registrar no hist�rico do pedido de venda �Garantia: tipo de garantia, percentual de garantia, link da garantia quando houver�.
					pedidovendahistorico = new Pedidovendahistorico();
					pedidovendahistorico.setPedidovenda(pedidovenda);
					pedidovendahistorico.setAcao("Garantia");
					pedidovendahistorico.setObservacao(historicoPedidoVendaColeta);
					pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
					
					//Registrar no hist�rico da coleta �Garantia: tipo de garantia, percentual de garantia, link da garantia quando houver�.
					for (ColetaMaterial coletaMaterial: listaColetaMaterial){
						coletahistorico = new Coletahistorico();
						coletahistorico.setColeta(coletaMaterial.getColeta());
						coletahistorico.setAcao(Coletaacao.GARANTIA);
						coletahistorico.setObservacao(historicoPedidoVendaColeta);
						coletahistoricoService.saveOrUpdate(coletahistorico);
					}
					
					//Registrar no hist�rico da coleta a constata��o definida na �ltima etapa da ordem de produ��o.
					if (!descricaoConstatacao.equals("")){
						for (ColetaMaterial coletaMaterial: listaColetaMaterial){
							coletahistorico = new Coletahistorico();
							coletahistorico.setColeta(coletaMaterial.getColeta());
							coletahistorico.setAcao(Coletaacao.CONSTATACAO);
							coletahistorico.setObservacao(descricaoConstatacao);
							coletahistoricoService.saveOrUpdate(coletahistorico);
						}
					}
					
					//Registrar no hist�rico da coleta e pedido venda "Item <id> com garantia negada"
					if (Garantiasituacao.INCONFORME.equals(garantiasituacao)){
						String historicoInconforme = "Item " + pedidovendamaterial.getCdpedidovendamaterial() + " com garantia negada";
						
						pedidovendahistorico = new Pedidovendahistorico();
						pedidovendahistorico.setPedidovenda(pedidovenda);
						pedidovendahistorico.setAcao("Garantia negada");
						pedidovendahistorico.setObservacao(historicoInconforme);
						pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
						
						for (ColetaMaterial coletaMaterial: listaColetaMaterial){
							coletahistorico = new Coletahistorico();
							coletahistorico.setColeta(coletaMaterial.getColeta());
							coletahistorico.setAcao(Coletaacao.GARANTIA_NEGADA);
							coletahistorico.setObservacao(historicoInconforme);
							coletahistoricoService.saveOrUpdate(coletahistorico);
						}
					}
				}
				boolean canceladoViaChaoDeFabrica = chaoFabrica && devolucao;//Para tratar o caso onde o cliente tem direito � garantia, mas a ordem de produ��o foi recusada devido � carca�a ter outro problema qu impossibilite a reforma.
				
				if (Garantiasituacao.DISPONIVEL.equals(garantiasituacao) && !canceladoViaChaoDeFabrica){
					try {
						garantiareformaitemService.calculaValorGarantia(garantiareformaitem, null, null);
					} catch (Exception e) {
						e.printStackTrace();
						
						try {
							String nomeMaquina = "N�o encontrado.";
							try {  
								InetAddress localaddr = InetAddress.getLocalHost();  
								nomeMaquina = localaddr.getHostName();  
							} catch (UnknownHostException e3) {}  
							
							
							String url = "N�o encontrado";
							try {  
								url = SinedUtil.getUrlWithContext();
							} catch (Exception e3) {}
							
							StringWriter stringWriter = new StringWriter();
							e.printStackTrace(new PrintWriter(stringWriter));
							
							String erro = "Erro ao calcular valor de garantia. (M�quina: " + nomeMaquina + ". URL: " + url + ") Veja a pilha de execu��o para mais detalhes:<br/><br/><pre>" + stringWriter.toString() + "</pre>";
							pedidovendaService.enviarEmailErro("[W3ERP] Erro ao criar garantia.",erro);
						} catch (Exception e2) {
							e.printStackTrace();
						}
					}
					if (garantiareformaitem.getValorCreditoGarantia()!=null && garantiareformaitem.getValorCreditoGarantia().getValue().doubleValue() > 0){
						Double preco = pedidovendamaterial.getPreco();
						Double quantidade = pedidovendamaterial.getQuantidade();
						Double desconto = pedidovendamaterial.getDesconto()!=null ? pedidovendamaterial.getDesconto().getValue().doubleValue() : 0D;
						Double totalSemDesconto = preco * quantidade;
						Double total = totalSemDesconto - desconto;
						
						if (garantiareformaitem.getValorCreditoGarantia().getValue().doubleValue() > total){
							desconto = total;
						} else {
							desconto = garantiareformaitem.getValorCreditoGarantia().getValue().doubleValue();
						}
						
						Double percentualdesconto = desconto * 100 / totalSemDesconto;
						pedidovendamaterialService.updateDesconto(pedidovendamaterial, new Money(desconto), percentualdesconto);
						pedidovendamaterialService.updateGarantiareformaitemAndDescontogarantiareforma(pedidovendamaterial, garantiareformaitem, garantiareformaitem.getValorCreditoGarantia());
						garantiareformaitemService.updatePedidovendamaterial(garantiareformaitem, pedidovendamaterial);
						this.updateSituacaoGarantia(garantiareforma.getCdgarantiareforma().toString(), Garantiasituacao.UTILIZADA);
					}
				}
			}
		}		
	}
	
	public GarantiareformaClienteBean findGarantiasCalculadasByCliente(Cliente cliente, String whereNotInGarantiareformaitem, Pedidovenda pedidovendadestino, Venda vendadestino, Pedidovendatipo pedidovendatipo, Prazopagamento prazopagamento){
		GarantiareformaClienteBean garantiasCalculadas = new GarantiareformaClienteBean();
		
		List<Garantiareforma> listaGarantiareforma = garantiareformaDAO.findForCalculargarantia(null, cliente, whereNotInGarantiareformaitem);
		if(pedidovendadestino != null)
			listaGarantiareforma.addAll(this.findGarantiautilizadaByPedido(pedidovendadestino, whereNotInGarantiareformaitem));
		
		if(vendadestino != null)
			listaGarantiareforma.addAll(this.findGarantiautilizadaByVenda(vendadestino, whereNotInGarantiareformaitem));
		
		for(Garantiareforma bean: listaGarantiareforma){
			for(Garantiareformaitem item: bean.getListaGarantiareformaitem()){
				item.setGarantiareforma(bean);
				garantiareformaitemService.calculaValorGarantia(item, pedidovendatipo, prazopagamento);
				garantiasCalculadas.setCliente(cliente);
				garantiasCalculadas.getListaGarantias().add(item);
			}
		}
		return garantiasCalculadas;
	}
	
	public ModelAndView abrePopupSelecaoGarantiareformaCliente(WebRequestContext request, Cliente cliente){
		Venda venda = null;
		Pedidovenda pedidoVenda = null;
		Pedidovendatipo pedidoVendaTipo = null;
		Prazopagamento prazoPagamento = null;
		
		String cdpedidovenda = request.getParameter("cdpedidovenda");
		String cdvenda = request.getParameter("cdvenda");
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		String cdprazopagamento = request.getParameter("cdprazopagamento");
		if(StringUtils.isNotBlank(cdvenda)){
			venda = new Venda(Integer.parseInt(cdvenda));
		}
		if(StringUtils.isNotBlank(cdpedidovenda)){
			pedidoVenda = new Pedidovenda(Integer.parseInt(cdpedidovenda));
		}
		if(StringUtils.isNotBlank(cdpedidovendatipo)){
			pedidoVendaTipo = new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo));
		}
		if(StringUtils.isNotBlank(cdprazopagamento)){
			prazoPagamento = new Prazopagamento(Integer.parseInt(cdprazopagamento));
		}
		request.setAttribute("whereInGarantiasJaSelecionadas", request.getParameter("whereInGarantiasJaSelecionadas"));
		GarantiareformaClienteBean garantia = this.getGarantiareformaClienteBean(request, cliente, pedidoVenda, venda, pedidoVendaTipo, prazoPagamento);
		return new ModelAndView("direct:/crud/popup/popupSelecaoGarantiareformaCliente", "bean", garantia);
	}
	
	public GarantiareformaClienteBean getGarantiareformaClienteBean(WebRequestContext request, Cliente cliente, Pedidovenda pedidoVenda, Venda venda, Pedidovendatipo pedidoVendaTipo, Prazopagamento prazoPagamento){
		String whereInGarantiasJaSelecionadas = request.getAttribute("whereInGarantiasJaSelecionadas") != null? request.getAttribute("whereInGarantiasJaSelecionadas").toString(): request.getParameter("whereInGarantiasJaSelecionadas");
		String indexDetalhe = request.getParameter("indexDetalhe");
		request.setAttribute("indexDetalhe", indexDetalhe);
		List<Garantiasituacao> situacoes = new ArrayList<Garantiasituacao>();
		situacoes.add(Garantiasituacao.DISPONIVEL);

		if(pedidoVenda != null || venda != null){
			situacoes.add(Garantiasituacao.UTILIZADA);
		}
		GarantiareformaClienteBean garantia = this.findGarantiasCalculadasByCliente(cliente, whereInGarantiasJaSelecionadas, pedidoVenda, venda, pedidoVendaTipo, prazoPagamento);
		return garantia;
	}
	
	public ModelAndView ajaxExistsGarantiareforma(WebRequestContext request, Cliente cliente){
		String cdpedidovenda = request.getParameter("cdpedidovenda");
		String cdvenda = request.getParameter("cdvenda");
		Pedidovenda pedidovenda = null;
		Venda venda = null;
		if(cdpedidovenda != null && cdpedidovenda != ""){
			pedidovenda = new Pedidovenda(Integer.parseInt(cdpedidovenda));
		}else if(cdvenda != null && cdvenda != ""){
			venda = new Venda(Integer.parseInt(cdvenda));
		}
		
		return this.ajaxExistsGarantiareforma(request, cliente, pedidovenda, venda, request.getParameter("whereInGarantiasJaSelecionadas"));
	}
	
	public ModelAndView ajaxExistsGarantiareforma(WebRequestContext request, Cliente cliente, Pedidovenda pedidoVenda, Venda venda, String whereInGarantiasJaSelecionadas){
		List<Garantiareforma> listagarantia = null;
		if(pedidoVenda != null){
			listagarantia = this.findBySelecaoPedidovenda(cliente, pedidoVenda, whereInGarantiasJaSelecionadas);
		}else if(venda != null){
			listagarantia = this.findBySelecaoVenda(cliente, venda, whereInGarantiasJaSelecionadas);
		}else{
			listagarantia = this.findDisponiveis(cliente, whereInGarantiasJaSelecionadas);
		}
		
		return new JsonModelAndView()
				.addObject("existsGarantias", !listagarantia.isEmpty());
	}
	
	public List<Garantiareforma> findBySelecaoPedidovenda(Cliente cliente, Pedidovenda pedidovendadestino, String whereNotInGarantiareformaitem){
		return garantiareformaDAO.findBySelecaoPedidovenda(cliente, pedidovendadestino, whereNotInGarantiareformaitem);
	}
	
	public List<Garantiareforma> findBySelecaoVenda(Cliente cliente, Venda vendadestino, String whereNotInGarantiareformaitem){
		return garantiareformaDAO.findBySelecaoVenda(cliente, vendadestino, whereNotInGarantiareformaitem);
	}
	
	public List<Garantiareforma> findGarantiautilizadaByPedido(Pedidovenda pedidovendadestino, String whereNotInGarantiareformaitem){
		return garantiareformaDAO.findGarantiautilizadaByPedido(pedidovendadestino, whereNotInGarantiareformaitem);
	}
	
	public List<Garantiareforma> findGarantiautilizadaByVenda(Venda vendadestino, String whereNotInGarantiareformaitem){
		return garantiareformaDAO.findGarantiautilizadaByVenda(vendadestino, whereNotInGarantiareformaitem);
	}
	
	public boolean validaObrigatoriedadePneu(Garantiareforma bean, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(SinedUtil.isRecapagem() && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA) &&
				bean != null && SinedUtil.isListNotEmpty(bean.getListaGarantiareformaitem())){
			for(Garantiareformaitem item : bean.getListaGarantiareformaitem()){
				if(item.getPneu() != null){
					if(!pneuService.validaObrigatoriedadePneu(item.getMaterial(), item.getPneu(), request, errors)){
						valido = false;
					}
				}
			}
		}
		return valido;
	}

	public List<GarantiaReformaBean> getGarantiaReformaBeanForTemplate(String selectedItens) {
		List<GarantiaReformaBean> garantiaReformaBeanList = new ArrayList<GarantiaReformaBean>();
		GarantiaReformaBean garantiaReformaBean = null;
		GarantiaReformaEmpresaBean garantiaReformaEmpresaBean = null;
		GarantiaReformaClienteBean garantiaReformaClienteBean = null;
		GarantiaReformaPneuBean garantiaReformaPneuBean = null;
		GarantiaReformaResultadoBean garantiaReformaResultadoBean = null;
		
		Garantiareforma garantiaReforma = null;
		
		if(!StringUtils.isEmpty(selectedItens)) {
			String[] ids = selectedItens.split(",");
			
			for (String id : ids) {
				garantiaReformaBean = new GarantiaReformaBean();
				
				garantiaReforma = garantiareformaDAO.findGarantiaReformaById(id);
				
				garantiaReformaBean.setCdGarantiaReforma(garantiaReforma.getCdgarantiareforma());
				garantiaReformaBean.setDtGarantia(garantiaReforma.getDtgarantia());
				garantiaReformaBean.setCdPedidoVenda(garantiaReforma.getPedidovenda() != null ? garantiaReforma.getPedidovenda().getCdpedidovenda() : null);
				garantiaReformaBean.setIdExternoPedidoVenda(garantiaReforma.getIdentificadorexternopedidovenda());
				garantiaReformaBean.setVendedorNome(garantiaReforma.getVendedor() != null ? garantiaReforma.getVendedor().getNome() : "");
				garantiaReformaBean.setCdPedidoVendaOrigem(garantiaReforma.getPedidovendaorigem() != null ? garantiaReforma.getPedidovendaorigem().getCdpedidovenda() : null);
				garantiaReformaBean.setPedidoExterno(garantiaReforma.getPedidoexterno() != null && garantiaReforma.getPedidoexterno() ? "Sim" : "N�o");
				garantiaReformaBean.setGarantiaSituacao(garantiaReforma.getGarantiasituacao() != null ? garantiaReforma.getGarantiasituacao().getNome() : "");
				
				if (garantiaReforma.getEmpresa() != null) {
					garantiaReformaEmpresaBean = new GarantiaReformaEmpresaBean();
					garantiaReformaEmpresaBean.setLogoEmpresa(SinedUtil.getLogoURLForReport(garantiaReforma.getEmpresa()));
					garantiaReformaEmpresaBean.setNome(garantiaReforma.getEmpresa().getNome());
					garantiaReformaEmpresaBean.setRazaoSocial(garantiaReforma.getEmpresa().getRazaosocial());
					garantiaReformaEmpresaBean.setCnpj(garantiaReforma.getEmpresa().getCpfCnpj());
					garantiaReformaEmpresaBean.setInscricaoEstadual(garantiaReforma.getEmpresa().getInscricaoestadual());
					garantiaReformaEmpresaBean.setInscricaoMunicipal(garantiaReforma.getEmpresa().getInscricaomunicipal());
					
					if (garantiaReforma.getEmpresa().getListaEndereco() != null && garantiaReforma.getEmpresa().getListaEndereco().size() > 0) {
						for (Endereco e : garantiaReforma.getEmpresa().getListaEndereco()) {
							garantiaReformaEmpresaBean.setEndereco(e.getLogradouro());
							garantiaReformaEmpresaBean.setBairro(e.getBairro());
							garantiaReformaEmpresaBean.setNumero(e.getNumero());
							garantiaReformaEmpresaBean.setComplemento(e.getComplemento());
							garantiaReformaEmpresaBean.setCep(e.getCep() != null ? e.getCep().getValue() : "");
							garantiaReformaEmpresaBean.setCidade(e.getMunicipio() != null ? e.getMunicipio().getNome() : "");
							garantiaReformaEmpresaBean.setEstado(e.getMunicipio() != null && e.getMunicipio().getUf() != null ? e.getMunicipio().getUf().getSigla() : "");
							
							break;
						}
					}
					
					if (garantiaReforma.getEmpresa().getListaTelefone() != null && garantiaReforma.getEmpresa().getListaTelefone().size() > 0) {
						if (garantiaReforma.getEmpresa().getTelefoneprincipal() != null && !garantiaReforma.getEmpresa().getTelefoneprincipal().equals("")) {
							garantiaReformaEmpresaBean.setTelefone(garantiaReforma.getEmpresa().getTelefoneprincipal());
						} else {
							for (Telefone e : garantiaReforma.getEmpresa().getListaTelefone()) {
								garantiaReformaEmpresaBean.setTelefone(e.getTelefone());
								
								break;
							}
						}
					}
					
					garantiaReformaBean.setGarantiaReformaEmpresaBean(garantiaReformaEmpresaBean);
				}
				
				if (garantiaReforma.getCliente() != null) {
					garantiaReformaClienteBean = new GarantiaReformaClienteBean();
					garantiaReformaClienteBean.setNome(garantiaReforma.getCliente().getNome());
					garantiaReformaClienteBean.setRazaoSocial(garantiaReforma.getCliente().getRazaosocial());
					garantiaReformaClienteBean.setCpfCnpj(garantiaReforma.getCliente().getCpfcnpj());
					garantiaReformaClienteBean.setInscricaoEstadual(garantiaReforma.getCliente().getInscricaoestadual());
					garantiaReformaClienteBean.setInscricaoMunicipal(garantiaReforma.getCliente().getInscricaomunicipal());
					
					if (garantiaReforma.getCliente().getListaEndereco() != null && garantiaReforma.getCliente().getListaEndereco().size() > 0) {
						for (Endereco e : garantiaReforma.getCliente().getListaEndereco()) {
							garantiaReformaClienteBean.setEndereco(e.getLogradouro());
							garantiaReformaClienteBean.setBairro(e.getBairro());
							garantiaReformaClienteBean.setNumero(e.getNumero());
							garantiaReformaClienteBean.setComplemento(e.getComplemento());
							garantiaReformaClienteBean.setCep(e.getCep() != null ? e.getCep().getValue() : "");
							garantiaReformaClienteBean.setCidade(e.getMunicipio() != null ? e.getMunicipio().getNome() : "");
							garantiaReformaClienteBean.setEstado(e.getMunicipio() != null && e.getMunicipio().getUf() != null ? e.getMunicipio().getUf().getSigla() : "");
							
							break;
						}
					}
					
					if (garantiaReforma.getCliente().getListaTelefone() != null && garantiaReforma.getCliente().getListaTelefone().size() > 0) {
						if (garantiaReforma.getCliente().getTelefoneprincipal() != null && !garantiaReforma.getCliente().getTelefoneprincipal().equals("")) {
							garantiaReformaClienteBean.setTelefone(garantiaReforma.getCliente().getTelefoneprincipal());
						} else {
							for (Telefone e : garantiaReforma.getCliente().getListaTelefone()) {
								garantiaReformaClienteBean.setTelefone(e.getTelefone());
								
								break;
							}
						}
					}
					
					garantiaReformaBean.setGarantiaReformaClienteBean(garantiaReformaClienteBean);
				}
				
				if (garantiaReforma.getListaGarantiareformaitem() != null && garantiaReforma.getListaGarantiareformaitem().size() > 0) {
					LinkedList<GarantiaReformaPneuBean> listaGarantiaReformaPneuBean = new LinkedList<GarantiaReformaPneuBean>();
					
					for (Garantiareformaitem gri : garantiaReforma.getListaGarantiareformaitem()) {
						garantiaReformaPneuBean = new GarantiaReformaPneuBean();
						garantiaReformaPneuBean.setId(gri.getCdgarantiareformaitem());
						garantiaReformaPneuBean.setMarca(gri.getPneu() != null && gri.getPneu().getPneumarca() != null ? gri.getPneu().getPneumarca().getNome() : "");
						garantiaReformaPneuBean.setModelo(gri.getPneu() != null && gri.getPneu().getPneumodelo() != null ? gri.getPneu().getPneumodelo().getNome() : "");
						garantiaReformaPneuBean.setMedida(gri.getPneu() != null && gri.getPneu().getPneumedida() != null ? gri.getPneu().getPneumedida().getNome() : "");
						garantiaReformaPneuBean.setQualificacao(gri.getPneu() != null && gri.getPneu().getPneuqualificacao() != null ? gri.getPneu().getPneuqualificacao().getNome() : "");
						garantiaReformaPneuBean.setSerieFogo(gri.getPneu() != null ? gri.getPneu().getSerie() : "");
						garantiaReformaPneuBean.setDot(gri.getPneu() != null ? gri.getPneu().getDot() : "");
						garantiaReformaPneuBean.setNumeroreforma(gri.getPneu() != null && gri.getPneu().getNumeroreforma() != null ? gri.getPneu().getNumeroreforma().getNome() : "");
						garantiaReformaPneuBean.setBanda(gri.getPneu() != null && gri.getPneu().getMaterialbanda() != null ? gri.getPneu().getMaterialbanda().getNome() : "");
						garantiaReformaPneuBean.setProfundidadeSulco(gri.getPneu() != null && gri.getPneu().getMaterialbanda() != null && gri.getPneu().getMaterialbanda().getProfundidadesulco() != null ? gri.getPneu().getMaterialbanda().getProfundidadesulco().toString() : "");
						garantiaReformaPneuBean.setDescricaoServicoGarantido(gri.getMaterial() != null ? gri.getMaterial().getNome() : "");
						garantiaReformaPneuBean.setResiduoBanda(gri.getResiduobanda() != null ? gri.getResiduobanda().toString() : "");
						if(gri.getPneu() != null && gri.getPneu().getAcompanhaRoda() != null){
							garantiaReformaPneuBean.setRoda(gri.getPneu().getAcompanhaRoda() == true ? "Sim": "N�o");
						}
						garantiaReformaPneuBean.setLonas(gri.getPneu() != null && gri.getPneu().getLonas() != null ? gri.getPneu().getLonas().toString() : "");
						garantiaReformaPneuBean.setDescricao(gri.getPneu() != null && gri.getPneu().getDescricao() != null ? gri.getPneu().getDescricao() : "");
						
						if(gri.getPneu() != null && gri.getPneu().getOtrPneuTipo() != null){
							String desenhos ="";
							String servicoTipo ="";
							
							List<String> listaOtrDesenhos = new ArrayList<String>();
							List<String> listaOtrServicoTipo = new ArrayList<String>();	
							List<OtrPneuTipoOtrClassificacaoCodigo> listaotrclassificacaocodigo = otrPneuTipoOtrClassificacaoCodigoService.findListOtrPneuTipoOtrClassificacaoCodigo(gri.getPneu().getOtrPneuTipo());
							
							if(listaotrclassificacaocodigo != null && !listaotrclassificacaocodigo.isEmpty()){
								for(OtrPneuTipoOtrClassificacaoCodigo otrPneuTipoOtrClassificacaoCodigo: listaotrclassificacaocodigo){
									if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho() != null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho() != null){	
										if(!listaOtrDesenhos.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho())) {
											if(listaOtrDesenhos.size() > 0) desenhos+= " | ";
											desenhos+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho();
											listaOtrDesenhos.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho());		
										}
									}
									if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo()!= null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico() != null){
										if(!listaOtrServicoTipo.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico())){
											if(listaOtrServicoTipo.size() > 0) servicoTipo+=" | ";
											servicoTipo+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico();
											listaOtrServicoTipo.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico());
										}
									}
								}				
								garantiaReformaPneuBean.setOtrPneuDesenho(desenhos);
								garantiaReformaPneuBean.setOtrPneuTipoServico(servicoTipo);
							}
						}
						if (gri.getListaMotivodevolucaoConstatacao() != null && gri.getListaMotivodevolucaoConstatacao().size() > 0) {
							LinkedList<String> listaMotivodevolucaoConstatacao = new LinkedList<String>();
							
							for (Motivodevolucao mdc : gri.getListaMotivodevolucaoConstatacao()) {
								listaMotivodevolucaoConstatacao.add(mdc.getDescricao());
							}
							
							garantiaReformaPneuBean.setListaMotivoDevolucao(listaMotivodevolucaoConstatacao);
						}
						
						garantiaReformaPneuBean.setGarantiaTipo(gri.getGarantiatipo() != null ? gri.getGarantiatipo().getDescricao() : "");
						garantiaReformaPneuBean.setGarantiaTipoPercentual(gri.getGarantiatipopercentual() != null ? 
								new Money(gri.getGarantiatipopercentual().getPercentual()).toString() : "");
						
						listaGarantiaReformaPneuBean.add(garantiaReformaPneuBean);
					}
					
					garantiaReformaBean.setListaGarantiaReformaPneuBean(listaGarantiaReformaPneuBean);
				}
				
				if (garantiaReforma.getGarantiareformaresultado() != null) {
					garantiaReformaResultadoBean = new GarantiaReformaResultadoBean();
					garantiaReformaResultadoBean.setNome(garantiaReforma.getGarantiareformaresultado().getNome());
					garantiaReformaResultadoBean.setMensagemResultado(garantiaReforma.getGarantiareformaresultado().getMensagem1());
					garantiaReformaResultadoBean.setMensagemDestino(garantiaReforma.getGarantiareformaresultado().getMensagem2());
					
					garantiaReformaBean.setGarantiaReformaResultadoBean(garantiaReformaResultadoBean);
				}
				
				garantiaReformaBeanList.add(garantiaReformaBean);
			}
		}
		
		return garantiaReformaBeanList;
	}

	public void processarListagem(ListagemResult<Garantiareforma> listagemResult, GarantiareformaFiltro filtro) {
		for(Garantiareforma bean: listagemResult.list()){
			
			for(Garantiareformaitem item: bean.getListaGarantiareformaitem()){
				bean.setServicogarantido(item.getMaterial());
				
				if(item.getPneu() != null && item.getPneu().getCdpneu() != null){
					bean.setPneu(item.getPneu().getCdpneu().toString());
				}
			}
		}
	}
	
	@Override
	protected ListagemResult<Garantiareforma> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Garantiareforma> listagemResult = super.findForExportacao(filtro);
		processarListagem(listagemResult, (GarantiareformaFiltro) filtro);
		
		return listagemResult;
	}
}

