package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.dao.ConcorrenteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConcorrenteService extends GenericService<Concorrente>{

	private ConcorrenteDAO concorrenteDAO;
	
	public void setConcorrenteDAO(ConcorrenteDAO concorrenteDAO) {
		this.concorrenteDAO = concorrenteDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name carregaConcorrente
	 * @param concorrente
	 * @return
	 * @return Concorrente
	 * @author Thiago Augusto
	 * @date 18/06/2012
	 *
	 */
	public Concorrente carregaConcorrente(Concorrente concorrente){
		return concorrenteDAO.carregaConcorrente(concorrente);
	}

	public Concorrente findByName(String campanhaStr) {
		return concorrenteDAO.findByName(campanhaStr);
	}
}
