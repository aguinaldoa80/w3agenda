package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.sined.geral.bean.Versao;
import br.com.linkcom.sined.geral.dao.VersaoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VersaoService extends GenericService<Versao> {

	private VersaoDAO versaoDAO;
	
	public void setVersaoDAO(VersaoDAO versaoDAO) {
		this.versaoDAO = versaoDAO;
	}
	
	public Versao getVersao(){
		return VersaoDAO.getInstance().getVersao();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Versao> findAllForFlex(){
		return versaoDAO.findAllForFlex();
	}
	
	/* singleton */
	private static VersaoService instance;
	public static VersaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VersaoService.class);
		}
		return instance;
	}
	
	private static final String VERSAO_VERIFICADA = "versaoVerificada";
	private static final String VERSAO_NUMERO = "versaoNumero";
	private static final String VERSAO_INFO = "informacoesbanco";
	private static final String DATABASE_VERSION = "databaseversion";

	public void doVerificaVersao(HttpServletRequest httpReq) throws Exception {
		// FAZ A VERIFICA��O DA VERS�O DO BANCO SOMENTE UMA VEZ POR SESS�O
		Object version = httpReq.getSession().getAttribute(VERSAO_VERIFICADA);
		if(version == null){
			
//			PEGA A VERS�O DO BANCO
			Versao versao = VersaoService.getInstance().getVersao();
			
//			PEGA AS PROPRIEDADES DO ARQUIVO QUE EST� NA APLICA��O
			Properties properties = getProperties();
			
			String databaseversion = properties.getProperty(DATABASE_VERSION);
			Double dbversion = Double.valueOf(databaseversion);
			
			httpReq.getSession().setAttribute(VERSAO_NUMERO, versao.getNumero() != null ? versao.getNumero().intValue() : "");
			if(versao == null || !versao.getNumero().equals(dbversion)){
				throw new SinedException("Vers�o do sistema inv�lida.");
			}
			
			httpReq.getSession().setAttribute(VERSAO_INFO, "Vers�o do banco aplica��o: "+databaseversion+"; Vers�o do banco de dados: "+versao.getNumero()+"; Descri��o do banco de dados: "+versao.getDescricao());
			httpReq.getSession().setAttribute(VERSAO_VERIFICADA, true);
		}
	}
	
	private Properties getProperties() throws IOException {
		InputStream is = NeoWeb.getRequestContext().getWebApplicationContext().getServletContext().getResourceAsStream("/WEB-INF/classes/version.properties");
		Properties props = new Properties();		 
		try {
			props.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return props;
	}
}
