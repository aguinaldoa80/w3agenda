package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Rateioapuracao;
import br.com.linkcom.sined.geral.dao.RateioapuracaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RateioapuracaoService extends GenericService<Rateioapuracao>{

	private RateioapuracaoDAO rateioapuracaoDAO;
	
	public void setRateioapuracaoDAO(RateioapuracaoDAO rateioapuracaoDAO) {
		this.rateioapuracaoDAO = rateioapuracaoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 	 
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/05/2018
	 */
	public Rateioapuracao findByNome(String nome){
		return rateioapuracaoDAO.findByNome(nome);
	}
}
