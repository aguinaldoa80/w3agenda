package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.dao.MaterialfornecedorDAO;
import br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemcompramaterialService extends GenericService<Ordemcompramaterial> {

	private OrdemcompramaterialDAO ordemcompramaterialDAO;
	private OrdemcompraentregamaterialService ordemcompraentregamaterialService;
	private MaterialService materialService;
	private EntregamaterialService entregamaterialService;
	private MaterialfornecedorService materialfornecedorService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setOrdemcompraentregamaterialService(
			OrdemcompraentregamaterialService ordemcompraentregamaterialService) {
		this.ordemcompraentregamaterialService = ordemcompraentregamaterialService;
	}
	public void setOrdemcompramaterialDAO(
			OrdemcompramaterialDAO ordemcompramaterialDAO) {
		this.ordemcompramaterialDAO = ordemcompramaterialDAO;
	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}	
	
	public void setMaterialfornecedorService(MaterialfornecedorService materialfornecedorService) {
		this.materialfornecedorService = materialfornecedorService;
	}
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findByOrdemcompra
	 * @param o
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Ordemcompramaterial> findByOrdemcompra(Ordemcompra o) {
		return ordemcompramaterialDAO.findByOrdemcompra(o);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findByOrdemcompraWherein(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/11/2012
	 */
	public List<Ordemcompramaterial> findByOrdemcompraWherein(String whereIn) {
		return ordemcompramaterialDAO.findByOrdemcompraWherein(whereIn);
	}

	/**
	 * Preenche a lista de materiais a partir de um pedido de venda.
	 *
	 * @param pedidovenda
	 * @return
	 * @since 12/04/2012
	 * @author Rodrigo Freitas
	 */
	public List<Ordemcompramaterial> getListaMaterialByPedidovenda(Pedidovenda pedidovenda) {
		List<Ordemcompramaterial> lista = new ArrayList<Ordemcompramaterial>();
		if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && pedidovenda.getListaPedidovendamaterial().size() > 0){
			List<Pedidovendamaterial> listaPedidovendamaterial = pedidovenda.getListaPedidovendamaterial();
			Ordemcompramaterial ordemcompramaterial = null;
			for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
				ordemcompramaterial = new Ordemcompramaterial();
				
				ordemcompramaterial.setMaterial(pedidovendamaterial.getMaterial());
				ordemcompramaterial.setQtdpedida(pedidovendamaterial.getQuantidade());
				ordemcompramaterial.setValorunitario(pedidovendamaterial.getPreco());
				ordemcompramaterial.setValor(pedidovendamaterial.getQuantidade() * pedidovendamaterial.getPreco());
				ordemcompramaterial.setFrequencia(new Frequencia(Frequencia.UNICA, "�nica"));
				ordemcompramaterial.setQtdefrequencia(1d);
				ordemcompramaterial.setUnidademedida(pedidovendamaterial.getUnidademedida());
				ordemcompramaterial.setContagerencial(pedidovendamaterial.getMaterial().getContagerencial());
				ordemcompramaterial.setIcmsissincluso(Boolean.FALSE);
				ordemcompramaterial.setIpiincluso(Boolean.FALSE);
				ordemcompramaterial.setObservacao(pedidovendamaterial.getObservacao());
				
				lista.add(ordemcompramaterial);
			}
		}
		return lista;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findUnidademedidaMaterial(Ordemcompra ordemcompra, Material material)
	 *
	 * @param ordemcompra
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompramaterial findUnidademedidaMaterial(Ordemcompra ordemcompra, Material material) {
		return ordemcompramaterialDAO.findUnidademedidaMaterial(ordemcompra, material);
	}
	
	/**
	 * Busca o �ltimo valor de compra de acordo com o material e fornecedor
	 *
	 * @see br.com.linkcom.sined.geral.service.OrdemcompramaterialService#findByFornecedorMaterial(Material material, Fornecedor fornecedor)
	 *
	 * @param cdmaterial
	 * @param cdfornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getUltimoValor(Integer cdmaterial, Integer cdfornecedor) {
		List<Ordemcompramaterial> listaOC = this.findByFornecedorMaterial(new Material(cdmaterial), new Fornecedor(cdfornecedor));
		List<Entregamaterial> listaEM = entregamaterialService.findByFornecedorMaterial(new Material(cdmaterial), new Fornecedor(cdfornecedor));
		Ordemcompramaterial ocm = null;
		Entregamaterial em = null;
		Date dtEM = null;
		Date dtOCM = null;
		if(listaOC != null && listaOC.size() > 0){
			ocm = listaOC.get(0);
			if(ocm.getOrdemcompra() != null){
				dtOCM = ocm.getOrdemcompra().getDtcriacao();
			}
		}
		if(listaEM != null && listaEM.size() > 0){
			em = listaEM.get(0);
			if(em.getEntregadocumento() != null){
				if(em.getEntregadocumento().getDtemissao() != null){
					dtEM = em.getEntregadocumento().getDtemissao();
				}else {
					dtEM = em.getEntregadocumento().getDtentrada();
				}
			}
		}
		
		Boolean considerarEntrega = null;
		if(dtEM != null && dtOCM == null){
			considerarEntrega = true;
		}else if(dtEM == null && dtOCM != null){
			considerarEntrega = false;
		}else {
			if(dtEM != null && dtOCM != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtEM, dtOCM)){
				considerarEntrega = true;
			}else {
				considerarEntrega = false;
			}
		}
		
		if(considerarEntrega != null){
			if(considerarEntrega){
				if(em != null){
					return em.getValorunitario();
				}
			}else {
				if(ocm != null){
					Double valorunitario;
					Double qtdepedida;
					if(ocm.getQtdpedida() != null && ocm.getQtdefrequencia() != null && 
							ocm.getValor() != null){
						qtdepedida = ocm.getQtdpedida() * ocm.getQtdefrequencia();
						valorunitario = ocm.getValor() / qtdepedida;
						return valorunitario;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findByFornecedorMaterial(Material material, Fornecedor fornecedor)
	 *
	 * @param material
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompramaterial> findByFornecedorMaterial(Material material, Fornecedor fornecedor) {
		return ordemcompramaterialDAO.findByFornecedorMaterial(material, fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findForCalcularrateio(Ordemcompramaterial ordemcompramaterial)
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcompramaterial findForCalcularrateio(Ordemcompramaterial ordemcompramaterial) {
		return ordemcompramaterialDAO.findForCalcularrateio(ordemcompramaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findForUltimovalorcompraByMaterial(Material material, Empresa empresa)
	 *
	 * @param material
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ordemcompramaterial> findForUltimovalorcompraByMaterial(Material material, Empresa empresa) {
		return ordemcompramaterialDAO.findForUltimovalorcompraByMaterial(material, empresa);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.service.OrdemcompramaterialService#getUltimoValor(Integer cdmaterial, Integer cdempresa)
	 *
	 * @param cdmaterial
	 * @param cdempresa
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getUltimoValorForVenda(Integer cdmaterial, Integer cdempresa) {
		List<Ordemcompramaterial> lista = this.findForUltimovalorcompraByMaterial(new Material(cdmaterial), (cdempresa != null ? new Empresa(cdempresa) : null));
		if(lista != null && lista.size() > 0){
			Double valorunitario;
			Double qtdepedida;
			Ordemcompramaterial ocm = lista.get(0);
			if(ocm.getQtdpedida() != null && ocm.getQtdefrequencia() != null && 
					ocm.getValor() != null){
				qtdepedida = ocm.getQtdpedida() * ocm.getQtdefrequencia();
				valorunitario = ocm.getValor() / qtdepedida;
				return valorunitario;
			}
		}
		return null;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#loadByOrdemcompramaterialForRomaneio(Ordemcompramaterial ordemcompramaterial)
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public Ordemcompramaterial loadByOrdemcompramaterialForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		return ordemcompramaterialDAO.loadByOrdemcompramaterialForRomaneio(ordemcompramaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Ordemcompramaterial> findByEntrega(Entrega entrega) {
		return ordemcompramaterialDAO.findByEntrega(entrega);
	}

	/**
	 * Busca a classe do material do item de ordem de compra.
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/12/2012
	 */
	public Materialclasse getMaterialclasseByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		List<Ordemcompraentregamaterial> lista = ordemcompraentregamaterialService.findByOrdemcompramaterial(ordemcompramaterial);
		
		if(lista != null && lista.size() > 0){
			for (Ordemcompraentregamaterial ordemcompraentregamaterial : lista) {
				Entregamaterial entregamaterial = ordemcompraentregamaterial.getEntregamaterial();
				if(entregamaterial != null && entregamaterial.getMaterialclasse() != null){
					return entregamaterial.getMaterialclasse();
				}
			}
		} else {
			Ordemcompramaterial ordemcompramaterial_aux = this.load(ordemcompramaterial, "ordemcompramaterial.cdordemcompramaterial, ordemcompramaterial.material");
			
			if(ordemcompramaterial_aux != null){
				List<Materialclasse> listaMaterialclasse = materialService.findClassesWithoutLoad(ordemcompramaterial_aux.getMaterial());
				if(listaMaterialclasse != null){
					if(listaMaterialclasse.size() == 1) return listaMaterialclasse.get(0);
					if(listaMaterialclasse.size() > 0){
						Collections.sort(listaMaterialclasse,new Comparator<Materialclasse>(){
							public int compare(Materialclasse o1, Materialclasse o2) {
								return o1.getCdmaterialclasse().compareTo(o2.getCdmaterialclasse());
							}
						});
						
						return listaMaterialclasse.get(0);
					}
				}
			}
		}
		
		return null;
	}
	
	public Material getMaterialByOrdemcompramaterial(String whereIn) {
		if(whereIn == null || whereIn.equals("")) return null;
		
		List<Ordemcompramaterial> lista = this.loadForConferenciaEntrega(whereIn);
		
		Material material = null;
		for (Ordemcompramaterial ordemcompramaterial : lista) {
			if(material == null){
				material = ordemcompramaterial.getMaterial();
			} else {
				if(!material.equals(ordemcompramaterial.getMaterial())){
					return null;
				}
			}
		}
		
		return material;
	}
	
	public List<Ordemcompramaterial> loadForConferenciaEntrega(String whereIn) {
		return ordemcompramaterialDAO.loadForConferenciaEntrega(whereIn);
	}


	public Ordemcompramaterial loadWithUnidademedida(Ordemcompramaterial ordemcompramaterial) {
		return ordemcompramaterialDAO.loadWithUnidademedida(ordemcompramaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findWithOrdemcompra(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 21/09/2015
	* @author Luiz Fernando
	*/
	public List<Ordemcompramaterial> findWithOrdemcompra(String whereIn) {
		return ordemcompramaterialDAO.findWithOrdemcompra(whereIn);
	}
	
	/**
	* M�todo que retorna a ordem de compra vinculado a ordemcompramaterial
	*
	* @param listaOrdemcompramaterial
	* @param ordemcompramaterial
	* @return
	* @since 21/09/2015
	* @author Luiz Fernando
	*/
	public Ordemcompra getOrdemcompra(List<Ordemcompramaterial> listaOrdemcompramaterial, Ordemcompramaterial ordemcompramaterial) {
		if(SinedUtil.isListNotEmpty(listaOrdemcompramaterial) && ordemcompramaterial != null && 
				ordemcompramaterial.getCdordemcompramaterial() != null){
			for(Ordemcompramaterial ocm : listaOrdemcompramaterial){
				if(ocm.getCdordemcompramaterial() != null && ocm.getCdordemcompramaterial().equals(ordemcompramaterial.getCdordemcompramaterial())){
					if(ordemcompramaterial.getUnidademedida() == null){
						ordemcompramaterial.setUnidademedida(ocm.getUnidademedida());
					}
					return ocm.getOrdemcompra();
				}
			}
		}
		return null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite)
	*
	* @param cdmaterial
	* @param cdempresa
	* @param limite
	* @return
	* @since 05/04/2016
	* @author Luiz Fernando
	*/
	public List<Ordemcompramaterial> findUltimascomprasByMaterial(Integer cdmaterial, Integer cdempresa, Integer limite) {
		return ordemcompramaterialDAO.findUltimascomprasByMaterial(cdmaterial, cdempresa, limite);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#getPrecoMaximoCompra(String cdmaterial, String cdempresa, Integer qtdeMeses)
	*
	* @param cdmaterial
	* @param cdempresa
	* @param qtdeMeses
	* @return
	* @since 05/04/2016
	* @author Luiz Fernando
	*/
	public Double getPrecoMaximoCompra(String cdmaterial, String cdempresa, Integer qtdeMeses) {
		return ordemcompramaterialDAO.getPrecoMaximoCompra(cdmaterial, cdempresa, qtdeMeses);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.OrdemcompramaterialDAO#getQtdeByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial)
	*
	* @param ordemcompramaterial
	* @return
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public Double getQtdeByOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		return ordemcompramaterialDAO.getQtdeByOrdemcompramaterial(ordemcompramaterial);
	}
	
	
	public void getCodigoAlternativo(List<Ordemcompramaterial> lista, Fornecedor fornecedor) {
		for (Ordemcompramaterial ordemcompramaterial : lista) {
			ordemcompramaterial.setCodigoalternativo(materialfornecedorService.getCodigoAlternativo(ordemcompramaterial.getMaterial(), fornecedor));
		}
		
	}
}
