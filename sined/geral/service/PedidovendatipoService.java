package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.dao.PedidovendatipoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.PedidovendatipoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.pedidovendatipo.PedidovendatipoRESTModel;

public class PedidovendatipoService extends GenericService<Pedidovendatipo> {
	
	private PedidovendatipoDAO pedidovendatipoDAO;
	private ParametrogeralService parametrogeralService;
	private DocumentoorigemService documentoorigemService;
	
	public void setPedidovendatipoDAO(PedidovendatipoDAO pedidovendatipoDAO) {this.pedidovendatipoDAO = pedidovendatipoDAO;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	
	/* singleton */
	private static PedidovendatipoService instance;
	public static PedidovendatipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PedidovendatipoService.class);
		}
		return instance;
	}

	public List<PedidovendatipoOfflineJSON> findForPVOffline() {
		List<PedidovendatipoOfflineJSON> lista = new ArrayList<PedidovendatipoOfflineJSON>();
		for(Pedidovendatipo pvt : pedidovendatipoDAO.findForPVOffline())
			lista.add(new PedidovendatipoOfflineJSON(pvt));
		
		return lista;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendatipoDAO#findReserva()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendatipo> findReserva() {
		return pedidovendatipoDAO.findReserva();
	}
	
	public Boolean existeVendaOrcamentoPedido(Integer cdpedidovendatipo) {
		return pedidovendatipoDAO.existeVendaOrcamentoPedido(cdpedidovendatipo);
	}
	
	/**
	 * M�todo para carregamento do PedidoVendaTipo espec�fico para OrdemServicoVeterinaria
	 *
	 * @author Marcos Lisboa
	 * @return Pedidovendatipo
	 */
	public Pedidovendatipo carregarTipoOSVeterinaria(){
		return pedidovendatipoDAO.carregarTipoOSVeterinaria();
	}
	
	public List<PedidovendatipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<PedidovendatipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<PedidovendatipoRESTModel> lista = new ArrayList<PedidovendatipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Pedidovendatipo bean : pedidovendatipoDAO.findForAndroid(whereIn))
				lista.add(new PedidovendatipoRESTModel(bean));
		}
		
		return lista;
	}

	/**
	* M�todo que retorna true se o tipo de pedido for representa��o
	*
	* @param pedidovendatipo
	* @return
	* @since 15/06/2015
	* @author Luiz Fernando
	*/
	public boolean tipoRepresentacao(Pedidovendatipo pedidovendatipo) {
		return pedidovendatipo != null && pedidovendatipo.getRepresentacao() != null && pedidovendatipo.getRepresentacao();
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendatipoDAO#existePedidovendatipoPrincipal(Pedidovendatipo pedidovendatipo)
	*
	* @param pedidovendatipo
	* @return
	* @since 20/10/2016
	* @author Luiz Fernando
	*/
	public Boolean existePedidovendatipoPrincipal(Pedidovendatipo pedidovendatipo){
		return pedidovendatipoDAO.existePedidovendatipoPrincipal(pedidovendatipo);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendatipoDAO#loadPrincipal()
	*
	* @return
	* @since 20/10/2016
	* @author Luiz Fernando
	*/
	public Pedidovendatipo loadPrincipal() {
		return pedidovendatipoDAO.loadPrincipal();
	}

	/**
	* M�todo que retorna a situa��o da conta a receber de acordo com o tipo de pedido de venda 
	*
	* @param pedidovendatipo
	* @return
	* @since 25/11/2016
	* @author Luiz Fernando
	*/
	public Documentoacao getSituacaoDocumento(Pedidovendatipo pedidovendatipo) {
		Documentoacao documentoacao = null;
		if(pedidovendatipo != null && pedidovendatipo.getCdpedidovendatipo() != null){
			Pedidovendatipo pvt = loadWithSituacaoDocumento(pedidovendatipo);
			if(pvt != null){
				documentoacao = pvt.getDocumentoacao();
			}
		}
		return documentoacao;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendatipoDAO#loadWithSituacaoDocumento(Pedidovendatipo pedidovendatipo)
	*
	* @param pedidovendatipo
	* @return
	* @since 25/11/2016
	* @author Luiz Fernando
	*/
	public Pedidovendatipo loadWithSituacaoDocumento(Pedidovendatipo pedidovendatipo) {
		return pedidovendatipoDAO.loadWithSituacaoDocumento(pedidovendatipo);
	}

	/**
	* M�todo que verifica se o tipo de pedido de venda est�a marcado para considerar o valor de custo do mateerial 
	*
	* @param pedidovendatipo
	* @return
	* @since 02/01/2017
	* @author Luiz Fernando
	*/
	public boolean considerarValorcustoMaterial(Pedidovendatipo pedidovendatipo) {
		boolean considerarValorcusto = false;
		if(pedidovendatipo != null){
			Pedidovendatipo pvt = load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.considerarvalorcusto");
			considerarValorcusto = pvt != null && pvt.getConsiderarvalorcusto() != null && pvt.getConsiderarvalorcusto(); 
		}
		return considerarValorcusto;
	}
	
	public boolean gerarExpedicaoVenda(Pedidovendatipo pedidovendatipo, boolean load){
		if(parametrogeralService.getBoolean(Parametrogeral.VENDA_COM_EXPEDICAO)){
			return true;
		}
		if(pedidovendatipo != null){
			Pedidovendatipo bean = load && pedidovendatipo.getCdpedidovendatipo() != null ? load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.gerarexpedicaovenda") : pedidovendatipo;
			return bean.getGerarexpedicaovenda() != null && bean.getGerarexpedicaovenda(); 
		}
		return false;
	}
	
	public boolean tipoVendaFutura(Pedidovendatipo pedidovendatipo, boolean load) {
		if(pedidovendatipo != null){
			Pedidovendatipo bean = load && pedidovendatipo.getCdpedidovendatipo() != null ? load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.permitirvendasemestoque") : pedidovendatipo;
			return bean != null && Boolean.TRUE.equals(bean.getPermitirvendasemestoque()); 
		}
		return false;
	}
	
	public Boolean verificarPedidoVendaTipoPermiteMaterialMestreVenda(Pedidovendatipo pedidovendatipo) {
		if (pedidovendatipo != null) {
			Pedidovendatipo pedidovendatipoFound = this.load(pedidovendatipo, 
					"pedidovendatipo.permitirMaterialMestreVenda, pedidovendatipo.baixaestoqueEnum");
			
			return (pedidovendatipoFound.getPermitirMaterialMestreVenda() != null && pedidovendatipoFound.getPermitirMaterialMestreVenda())
					&& (pedidovendatipoFound.getBaixaestoqueEnum() != null && !BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(pedidovendatipoFound.getBaixaestoqueEnum()));
		}
		
		return Boolean.FALSE;
	}
	
	public ModelAndView buscarPedidoVendaTipoAjax(Pedidovendatipo pedidovendatipo) {
		ModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("naoAbrirPopup", verificarPedidoVendaTipoPermiteMaterialMestreVenda(pedidovendatipo));
		
		if(pedidovendatipo != null){			
			pedidovendatipo = this.load(pedidovendatipo, "pedidovendatipo.obrigarLoteOrcamento, pedidovendatipo.obrigarLotePedidovenda, pedidovendatipo.obrigarLoteVenda");
		}
		
		jsonModelAndView.addObject("obrigarLoteOrcamento", pedidovendatipo != null && pedidovendatipo.getObrigarLoteOrcamento() != null ? pedidovendatipo.getObrigarLoteOrcamento() : false);
		jsonModelAndView.addObject("obrigarLotePedidovenda", pedidovendatipo != null && pedidovendatipo.getObrigarLotePedidovenda() != null ? pedidovendatipo.getObrigarLotePedidovenda() : false);
		jsonModelAndView.addObject("obrigarLoteVenda", pedidovendatipo != null && pedidovendatipo.getObrigarLoteVenda() != null ? pedidovendatipo.getObrigarLoteVenda() : false);
		
		return jsonModelAndView;
	}
	
	public boolean isRequerAprovacaoVenda(Pedidovendatipo pedidovendatipo) {
		Pedidovendatipo pedidovendatipoFound = this.load(pedidovendatipo, "pedidovendatipo.requerAprovacaoVenda");
		
		return (pedidovendatipoFound != null) && Boolean.TRUE.equals(pedidovendatipoFound.getRequerAprovacaoVenda());
	}
	
	public boolean isAtualizaEcommerceNaAprovacaoVenda(Pedidovendatipo pedidovendatipo) {
		Pedidovendatipo pedidovendatipoFound = this.load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.atualizarPedidoEcommerceStatusAoAprovar");
		
		return (pedidovendatipoFound != null) && Boolean.TRUE.equals(pedidovendatipoFound.getAtualizarPedidoEcommerceStatusAoAprovar());
	}
	
	public JsonModelAndView verificarPedidoVendaTipoBloqueioQuantidade(WebRequestContext request) {
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		
		JsonModelAndView retorno = new JsonModelAndView();
		if(!StringUtils.isBlank(cdpedidovendatipo)) {
			Pedidovendatipo pedidovendatipo = load(new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo)), 
					"pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bloqQuantidaMetragem,  pedidovendatipo.bloqAlteracaoPreco");
			
			Boolean bloquearQuantidade = BooleanUtils.isTrue(pedidovendatipo.getBloqQuantidaMetragem());
			retorno.addObject("bloquearQuantidade", bloquearQuantidade);
		} else {
			retorno.addObject("bloquearQuantidade", false);
		}
		
		return retorno;
	}
	
	public JsonModelAndView verificarPedidoVendaTipoBloqueioPreco(WebRequestContext request) {
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		String cdpedidovenda = request.getParameter("cdpedidovenda");
		
		Pedidovenda pedidoVenda = null;
		Pedidovendatipo pedidoVendaTipo = null;
		if(StringUtils.isNotBlank(cdpedidovendatipo)) {
			pedidoVendaTipo = new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo));
		}
		if(StringUtils.isNotBlank(cdpedidovenda)) {
			pedidoVenda = new Pedidovenda(Integer.parseInt(cdpedidovenda));
		}
		
		return this.verificarPedidoVendaTipoBloqueioPreco(request, pedidoVenda, pedidoVendaTipo);
	}
	
	public JsonModelAndView verificarPedidoVendaTipoBloqueioPreco(WebRequestContext request, Pedidovenda pedidoVenda, Pedidovendatipo pedidoVendaTipo) {
		JsonModelAndView retorno = new JsonModelAndView();
		if(Util.objects.isPersistent(pedidoVendaTipo)) {
			pedidoVendaTipo = load(pedidoVendaTipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bloqQuantidaMetragem,  pedidovendatipo.bloqAlteracaoPreco");
			
			Boolean pedidoVendaPossuiContas = false;
			if(Util.objects.isPersistent(pedidoVenda)) {
				pedidoVendaPossuiContas = SinedUtil.isListNotEmpty(documentoorigemService.getListaDocumentosDePedidovenda(pedidoVenda));
			}
			
			Boolean bloquearPreco = BooleanUtils.isTrue(pedidoVendaTipo.getBloqQuantidaMetragem());
			retorno.addObject("bloquearPreco", bloquearPreco || pedidoVendaPossuiContas);
			
			Boolean bloquearQuantidade = BooleanUtils.isTrue(pedidoVendaTipo.getBloqQuantidaMetragem());
			retorno.addObject("bloquearQuantidade", bloquearQuantidade);
		} else {
			retorno.addObject("bloquearPreco", false);
			retorno.addObject("bloquearQuantidade", false);
		}
		
		return retorno;
	}
	
	public Pedidovendatipo loadForConsiderarImpostos(Pedidovendatipo pedidovendatipo){
		return this.load(pedidovendatipo, "pedidovendatipo.cdpedidovendatipo, pedidovendatipo.bonificacao, pedidovendatipo.comodato, pedidovendatipo.consideraripiconta, " +
										"pedidovendatipo.consideraricmsstconta, pedidovendatipo.considerardesoneracaoconta, pedidovendatipo.considerarfcpstconta");
	}
	
	public void desmarcarCriarExpedicaoComConferencia() {
		pedidovendatipoDAO.desmarcarCriarExpedicaoComConferencia();
	}
	
	public List<Pedidovendatipo> findForComboWithoutVendaEcommerce(){
		List<Pedidovendatipo> lista = this.findForCombo("vendasEcommerce");
		List<Pedidovendatipo> listaRetorno = new ArrayList<Pedidovendatipo>();
		
		for(Pedidovendatipo tipo: lista){
			if(!Boolean.TRUE.equals(tipo.getVendasEcommerce())){
				listaRetorno.add(tipo);
			}
		}
		return listaRetorno;
	}
	
	public Boolean existsPedidovendaWithTicketMedio(){
		return pedidovendatipoDAO.existsPedidovendaWithTicketMedio();
	}
}
