package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.EventoPagamento;
import br.com.linkcom.sined.geral.dao.EventoPagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EventoPagamentoService extends GenericService<EventoPagamento>{

	private EventoPagamentoDAO eventoPagamentoDAO;
	
	public void setEventoPagamentoDAO(EventoPagamentoDAO eventoPagamentoDAO) {
		this.eventoPagamentoDAO = eventoPagamentoDAO;
	}
	
	public List<EventoPagamento> findByEmpresa(Empresa empresa) {
		return eventoPagamentoDAO.findByEmpresa(empresa);
	}
	
	public EventoPagamento loadWithTipo(EventoPagamento eventoPagamento) {
		return eventoPagamentoDAO.loadWithTipo(eventoPagamento);
	}
	
	public List<EventoPagamento> findByEmpresa(EventoPagamento evento, Empresa empresa) {
		return eventoPagamentoDAO.findByEmpresa(evento, empresa);
	}
	
	public List<EventoPagamento> findForImportacaoProvisionamento() {
		return eventoPagamentoDAO.findForImportacaoProvisionamento();
	}
	
	public EventoPagamento loadForImportarFolhaPagamento(String codigoIntegracao, Empresa empresa) {
		return eventoPagamentoDAO.loadForImportarFolhaPagamento(codigoIntegracao, empresa);
	}
}
