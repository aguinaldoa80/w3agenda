package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventos;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EventosFinaisCorreiosService extends GenericService<EventosFinaisCorreios>{

	private TipoEventosService tipoEventosService;
	
	public void setTipoEventosService(TipoEventosService tipoEventosService) {
		this.tipoEventosService = tipoEventosService;
	}
	
	public EventosFinaisCorreios loadByEvento(EventosCorreios eventoCorreios){
		if(Util.objects.isNotPersistent(eventoCorreios)){
			return null;
		}
		TipoEventos tipoEventos = tipoEventosService.loadEventoFinal(eventoCorreios);
		if(tipoEventos != null){
			EventosFinaisCorreios eventoFinaiCorreios = tipoEventos.getEventosFinaisCorreios();
			if(tipoEventos.getEventosCorreios() != null){
				eventoFinaiCorreios.setNome(tipoEventos.getEventosCorreios().getDescricao());
			}
			return eventoFinaiCorreios;
		}
		return null;
	}
}
