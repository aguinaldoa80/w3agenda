package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Vcategoria;
import br.com.linkcom.sined.geral.dao.CategoriaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.categoria.CategoriaRESTModel;

public class CategoriaService extends GenericService<Categoria>{
	
	private CategoriaDAO categoriaDAO;
	private static CategoriaService instance;
	
	public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
		this.categoriaDAO = categoriaDAO;
	}
	
	public static CategoriaService getInstance() {
		if (instance == null)
			instance = Neo.getObject(CategoriaService.class);
		
		return instance;
	}

	public List<Categoria> findByTipo(boolean isCliente, boolean isFornecedor) {

		return categoriaDAO.findByTipo(isCliente, isFornecedor);
	}

	public Categoria loadWithIdentificador(Categoria categoria) {

		return categoriaDAO.loadWithIdentificador(categoria);
	}

	public List<Categoria> findAutocomplete(String q, Boolean isCliente, Boolean isFornecedor) {
		return categoriaDAO.findAutocompleteTodos(q, isCliente, isFornecedor);
	}

	public List<Categoria> findTreeView(Boolean isCliente, Boolean isFornecedor) {
		List<Categoria> lista = this.findByTipo(isCliente, isFornecedor);
		
		List<Categoria> raiz = new ArrayList<Categoria>();
		for (Categoria cg : lista) {
			if(cg.getCategoriapai() == null || cg.getCategoriapai().getCdcategoria() == null){
				raiz.add(cg);
			}
		}
		for (Categoria pai : raiz) {
			pai.setFilhos(this.somenteFilhos(pai, lista));
		}
		
		return raiz;
	}
	
	private List<Categoria> somenteFilhos(Categoria pai, List<Categoria> lista) {
		List<Categoria> filhos = new ArrayList<Categoria>();
		for (Categoria cg : lista) {
			if(cg.getCategoriapai() != null && 
					cg.getCategoriapai().getCdcategoria() != null &&
					cg.getCategoriapai().getCdcategoria().equals(pai.getCdcategoria())){
				cg.setFilhos(somenteFilhos(cg, lista));
				filhos.add(cg);
			}
		}
		return filhos;
	}
	
	/**
	 * Prepara o bean para ser salvo no CRUD.
	 * 
	 * @see br.com.linkcom.sined.geral.service.CategoriaService#findFilhos
	 * @see br.com.linkcom.sined.geral.service.CategoriaService#carregaConta
	 * @param bean
	 * @author Giovane Freitas
	 */
	public void preparaParaSalvar(Categoria bean){

		if(bean.getCdcategoria() != null){
			Categoria categoria = loadForEntrada(bean);
			if(categoria != null && bean.getItem() != null && (
				(bean.getCategoriapai() == null && categoria.getCategoriapai() != null) ||
				(bean.getCategoriapai() != null && categoria.getCategoriapai() == null) ||
				(bean.getCategoriapai() != null && categoria.getCategoriapai() != null &&
						bean.getCategoriapai().getCdcategoria() != null &&
						categoria.getCategoriapai().getCdcategoria() != null &&
						!bean.getCategoriapai().getCdcategoria().equals(categoria.getCategoriapai().getCdcategoria())))){
				bean.setItem(null);
			}
		}
		if (bean.getItem() == null) {
			//setar o campo item para gera��o do identificador
			if (bean.getCategoriapai() != null && bean.getCategoriapai().getCdcategoria() != null) {
				List<Categoria> listaFilhos = categoriaDAO.findFilhos(bean.getCategoriapai());
				if (listaFilhos.size() > 0) {
					bean.setItem(listaFilhos.get(0).getItem() + 1);
				} else {
					bean.setItem(1);
				}
			} else {
				List<Categoria> listaPais = categoriaDAO.findRaiz();
				if (listaPais.size() > 0) {
					if (listaPais.get(0).getItem() != null)
						bean.setItem(listaPais.get(0).getItem() + 1);
					else
						bean.setItem(listaPais.size() + 1);
				} else {
					bean.setItem(1);
				}
			}
		}

	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 24/02/2012
	 * @param bean
	 * @return
	 */
	public Vcategoria carregarIdentificador(Categoria bean){
		return categoriaDAO.carregarIdentificador(bean);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CategoriaDAO#findByNome(String nome)
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/02/2013
	 */
	public Categoria findByNome(String nome) {
		return categoriaDAO.findByNome(nome);
	}
	
	public List<CategoriaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<CategoriaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<CategoriaRESTModel> lista = new ArrayList<CategoriaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Categoria bean : categoriaDAO.findForAndroid(whereIn))
				lista.add(new CategoriaRESTModel(bean));
		}
		
		return lista;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2014
	 */
	public List<Categoria> findByPessoa(Pessoa pessoa) {
		return categoriaDAO.findByPessoa(pessoa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Jo�o Vitor
	 * @since 27/05/2015
	 */
	public List<Categoria> findForMaterialgrupocomissao() {
		return categoriaDAO.findForMaterialgrupocomissao();
	}
}
