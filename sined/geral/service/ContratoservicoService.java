package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contratoservico;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratoservicoService extends GenericService<Contratoservico> {

	/* singleton */
	private static ContratoservicoService instance;
	
	public static ContratoservicoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContratoservicoService.class);
		}
		return instance;
	}
}
