package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatoemail;
import br.com.linkcom.sined.geral.dao.ContacrmcontatoemailDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmcontatoemailService extends GenericService<Contacrmcontatoemail>{
	
	protected ContacrmcontatoemailDAO contacrmcontatoemailDAO;
	
	public void setContacrmcontatoemailDAO(
			ContacrmcontatoemailDAO contacrmcontatoemailDAO) {
		this.contacrmcontatoemailDAO = contacrmcontatoemailDAO;
	}
	
	public List<Contacrmcontatoemail> findListEmailFromContacrmcontato(Contacrmcontato contacrmcontato){
		return contacrmcontatoemailDAO.findListEmailFromContacrmcontato(contacrmcontato);
	}
	
	public List<Contacrmcontatoemail> carregaContacrmcontatoemail(String whereIn) {
		return contacrmcontatoemailDAO.carregaContacrmcontatoemail(whereIn);
	}
}
