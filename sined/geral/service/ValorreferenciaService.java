package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.ValorreferenciaDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.AtualizarValorreferenciaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ValorreferenciaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ValorreferenciaService extends GenericService<Valorreferencia> {

	private TarefarecursogeralService tarefarecursogeralService;
	private TarefarecursohumanoService tarefarecursohumanoService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private PlanejamentorecursohumanoService planejamentorecursohumanoService;
	private ValorreferenciaDAO valorreferenciaDAO;
	private PlanejamentoService planejamentoService;
	private MaterialService materialService;
	private CargoService cargoService;
	
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setValorreferenciaDAO(ValorreferenciaDAO valorreferenciaDAO) {
		this.valorreferenciaDAO = valorreferenciaDAO;
	}
	public void setTarefarecursogeralService(
			TarefarecursogeralService tarefarecursogeralService) {
		this.tarefarecursogeralService = tarefarecursogeralService;
	}
	public void setTarefarecursohumanoService(
			TarefarecursohumanoService tarefarecursohumanoService) {
		this.tarefarecursohumanoService = tarefarecursohumanoService;
	}
	public void setPlanejamentorecursogeralService(
			PlanejamentorecursogeralService planejamentorecursogeralService) {
		this.planejamentorecursogeralService = planejamentorecursogeralService;
	}
	public void setPlanejamentorecursohumanoService(
			PlanejamentorecursohumanoService planejamentorecursohumanoService) {
		this.planejamentorecursohumanoService = planejamentorecursohumanoService;
	}
	
	/**
	 * M�todo que atualiza os valores de refer�ncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.TarefarecursohumanoService#valoresTarefaHumano
	 * @see br.com.linkcom.sined.geral.service.TarefarecursogeralService#valoresTarefaGeral
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursogeralService#valoresPlanejamentoGeral
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursohumanoService#valoresPlanejamentoHumano
	 * @param planejamento
	 * @param listaReferencia
	 * @param listaSemValorMaterial
	 * @param listaSemValorCargo
	 * @author Rodrigo Freitas
	 */
	public void atualizaValores(Planejamento planejamento, List<Valorreferencia> listaReferencia, List<Valorreferencia> listaSemValorMaterial, List<Valorreferencia> listaSemValorCargo) {
		List<Valorreferencia> listaTodas = this.findByPlanejamento(planejamento);
		
		tarefarecursohumanoService.valoresTarefaHumano(planejamento, listaReferencia, listaTodas);
		
		tarefarecursogeralService.valoresTarefaGeral(planejamento, listaReferencia, listaTodas);
		
		planejamentorecursogeralService.valoresPlanejamentoGeral(planejamento, listaReferencia, listaTodas);
		
		planejamentorecursohumanoService.valoresPlanejamentoHumano(planejamento, listaReferencia, listaTodas);
		
		List<Valorreferencia> listaSemValor = new ArrayList<Valorreferencia>();
		for (int i = 0; i < listaReferencia.size(); i++) {
			if(listaReferencia.get(i).getValor().compareTo(new Money()) == 0){
				listaSemValor.add(listaReferencia.get(i));
				listaReferencia.remove(i);
				i--;
			}
		}
		
		for (Valorreferencia vr : listaSemValor) {
			if (vr.getCargo() != null) {
				listaSemValorCargo.add(vr);
			} else {
				listaSemValorMaterial.add(vr);
			}
		}
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ValorreferenciaDAO#findByPlanejamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Valorreferencia> findByPlanejamento(Planejamento planejamento) {
		return valorreferenciaDAO.findByPlanejamento(planejamento);
	}
	
	/**
	 * Salva as tr�s listas para atualizar o valor de refr�ncia.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ValorreferenciaService#salvarLista
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void saveValores(AtualizarValorreferenciaBean bean) {
		this.salvarLista(bean.getListaSemValorCargo());
		this.salvarLista(bean.getListaSemValorMaterial());
		this.salvarLista(bean.getListaReferencia());
	}
	
	/**
	 * M�todo que salva a lista no banco, se der Exception de unique no banco
	 * passa para o pr�ximo valor sem lan�ar exce��o no banco.
	 *
	 * @param lista
	 * @author Rodrigo Freitas
	 */
	private void salvarLista(List<Valorreferencia> lista) {
		if (lista != null) {
			for (Valorreferencia vr : lista) {
				/*
				 * Feito um try/catch sem nenhum tratamento da exce��o pois de acordo com o UC,
				 * se der problema de unique no banco do valor de refer�ncia ao salvar, deve-se 
				 * passar para o outro bean se lan�ar exce��o alguma na tela do usu�rio.
				 */
				try {
					this.saveOrUpdate(vr);
				} catch (DataIntegrityViolationException e) {
					if (!DatabaseError.isKeyPresent(e, "IDX_VALORREFERENCIA_CUSTO")) {
						throw e;
					}	
				}
				
			}
		}
	}
	/**
	 * M�todo que gera um relat�rio de Valores de refer�ncia de acordo com a listagem.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ValorreferenciaService#findForReport(ValorreferenciaFiltro)
	 * @param filtro
	 * @return report
	 * @author Leandro Lima
	 */
	public Report gerarRelarorioValoresReferencia(ValorreferenciaFiltro filtro) {
		//setando arquivo do relat�rio
		Report report = new Report("/projeto/valorReferencia");		
		//carregando dados
		List<Valorreferencia> lista =  this.findForReport(filtro);
		//setando filtros
		if (filtro.getProjeto() != null)
			report.addParameter("projeto", ProjetoService.getInstance().load(filtro.getProjeto(), "projeto.nome").getNome());
		if (filtro.getPlanejamento() != null)
			report.addParameter("planejamento", planejamentoService.load(filtro.getPlanejamento(), "planejamento.descricao").getDescricao());
		if (filtro.getMaterial() != null)
			report.addParameter("material", materialService.load(filtro.getMaterial(), "material.nome").getNome());
		if (filtro.getCargo() != null)
			report.addParameter("funcao", cargoService.load(filtro.getCargo(), "cargo.nome").getNome());        
		//setando dados
		report.setDataSource(lista);
		return report;
	}
	
	/**
	 * 
	 * Metodo de referencia ao DAO.
	 * Montagem do relatorio
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ValorreferenciaDAO#findForReport(ValorreferenciaFiltro)
	 * @param filtro
	 * @return
	 * @author Leandro Lima
	 */
	public List<Valorreferencia> findForReport(ValorreferenciaFiltro filtro){		
		return valorreferenciaDAO.findForReport(filtro);
	}

}
