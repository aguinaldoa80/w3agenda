package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoempresa;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.Grupotributacaomaterialgrupo;
import br.com.linkcom.sined.geral.bean.Grupotributacaonaturezaoperacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoDIFALInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.grupotributacao.GrupotributacaoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.dao.GrupotributacaoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaoService extends GenericService<Grupotributacao>{

	private GrupotributacaoDAO grupotributacaoDAO;
	private MaterialService materialService;
	private EnderecoService enderecoService;
	private EmpresaService empresaService;
	private FornecedorService fornecedorService;
	private ClienteService clienteService;
	private UfService ufService;
	private GrupotributacaoimpostoService grupotributacaoimpostoService;
	private FaixaimpostoService faixaimpostoService;
	
	public void setFaixaimpostoService(FaixaimpostoService faixaimpostoService) {
		this.faixaimpostoService = faixaimpostoService;
	}
	public void setGrupotributacaoimpostoService(
			GrupotributacaoimpostoService grupotributacaoimpostoService) {
		this.grupotributacaoimpostoService = grupotributacaoimpostoService;
	}
	public void setGrupotributacaoDAO(GrupotributacaoDAO grupotributacaoDAO) {
		this.grupotributacaoDAO = grupotributacaoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	
	public GrupotributacaoVO createGrupotributacaoVOForTributacao(Operacao operacao, Empresa empresa, Endereco enderecoEmpresa, Naturezaoperacao naturezaoperacao, 
			Material material, Boolean tributadoicms, Cliente cliente, Endereco enderecoCliente, Uf ufCliente,
			Fornecedor fornecedor, Endereco enderecoFornecedor, Uf ufFornecedor, Localdestinonfe localDestinoNfe, Date notaDtEmissao) {
		
		GrupotributacaoVO grupotributacaoVO = new GrupotributacaoVO();
		
		if(material != null && material.getCdmaterial() != null){
			material = materialService.loadForTributacao(material);
			grupotributacaoVO.setMaterialgrupo(material.getMaterialgrupo());
			grupotributacaoVO.setNcmcompleto(material.getNcmcompleto());
			grupotributacaoVO.setMaterialTipoBean(material.getMaterialtipo());
		}
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithMunicipioFiscal(empresa);
			grupotributacaoVO.setEmpresa(empresa);
		}
		if(cliente != null && cliente.getCdpessoa() != null){
			cliente = clienteService.loadForTributacao(cliente);
			grupotributacaoVO.setClientecodigotributacao(cliente.getCodigotributacao() != null ? cliente.getCodigotributacao().getCodigo() : null);
			grupotributacaoVO.setClienteincideiriss(cliente.getIncidiriss());
			grupotributacaoVO.setClienteContribuinteICMS(cliente.getContribuinteICMS());
			grupotributacaoVO.setContribuinteIcmsTipoEnum(cliente.getContribuinteicmstipo());
		}
		if(ufCliente != null && ufCliente.getCduf() != null){
			Uf uf = ufService.load(ufCliente, "uf.cduf, uf.sigla");
			grupotributacaoVO.setClienteuf(uf.getSigla());
		}else if(enderecoCliente != null && enderecoCliente.getCdendereco() != null){
			Endereco endereco = enderecoService.loadEnderecoNota(enderecoCliente);
			if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
				grupotributacaoVO.setClienteuf(endereco.getMunicipio().getUf().getSigla());	
			}
		}
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			fornecedor = fornecedorService.loadForTributacao(fornecedor);
			Endereco endereco = null;
			if(ufFornecedor != null && ufFornecedor.getCduf() != null){
				Uf uf = ufService.load(ufFornecedor, "uf.cduf, uf.sigla");
				grupotributacaoVO.setFornecedoruf(uf.getSigla());
			}else {
				if(enderecoFornecedor != null && enderecoFornecedor.getCdendereco() != null){
					endereco = enderecoService.loadEnderecoNota(enderecoFornecedor);
				}else {
					endereco = fornecedor.getEndereco();
				}
				if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null){
					grupotributacaoVO.setFornecedoruf(endereco.getMunicipio().getUf().getSigla());	
				}
			}
		}
		if(localDestinoNfe != null) {
			grupotributacaoVO.setLocalDestinoNfe(localDestinoNfe);
		}
		if(notaDtEmissao != null){
			grupotributacaoVO.setNotaDtEmissao(notaDtEmissao);
		}
		
		return grupotributacaoVO;
	}
	
	public GrupotributacaoVO createGrupotributacaoVOForTributacao(
			Operacao operacao, 
			Empresa empresa, 
			Naturezaoperacao naturezaoperacao, 
			Materialgrupo materialgrupo,
			Cliente cliente,
			Boolean tributadoicms, 
			Material material, 
			String ncmcompleto, 
			Endereco clienteEndereco, 
			Endereco fornecedorEndereco,
			List<Categoria> listaCategoriaCliente,
			List<Categoria> listaCategoriaFornecedor,
			Operacaonfe operacaonfe,
			ModeloDocumentoFiscalEnum modelodocumentoEnum,
			Localdestinonfe localDestinoNfe,
			Date notaDtEmissao){
	
		return createGrupotributacaoVOForTributacao(operacao, 
				empresa, 
				naturezaoperacao, 
				materialgrupo,
				cliente,
				tributadoicms, 
				material, 
				ncmcompleto, 
				clienteEndereco, 
				fornecedorEndereco,
				listaCategoriaCliente,
				listaCategoriaFornecedor,
				operacaonfe,
				null,
				modelodocumentoEnum,
				localDestinoNfe,
				notaDtEmissao);
	}
	
	public GrupotributacaoVO createGrupotributacaoVOForTributacao(
			Operacao operacao, 
			Empresa empresa, 
			Naturezaoperacao naturezaoperacao, 
			Materialgrupo materialgrupo,
			Cliente cliente,
			Boolean tributadoicms, 
			Material material, 
			String ncmcompleto, 
			Endereco clienteEndereco, 
			Endereco fornecedorEndereco,
			List<Categoria> listaCategoriaCliente,
			List<Categoria> listaCategoriaFornecedor,
			Operacaonfe operacaonfe,
			Boolean considerarTributadoicms,
			ModeloDocumentoFiscalEnum modelodocumentoEnum,
			Localdestinonfe localDestinoNfe, 
			Date notaDtEmissao){
		
		Material auxMaterial = null;
		Empresa auxEmpresa = null;
		Cliente auxCliente = null;
		Endereco enderecoCliente = null;	
		Endereco enderecoFornecedor = null;
		
		if(material!= null && material.getCdmaterial() != null)
			auxMaterial = materialService.loadForTributacao(material);
		
		if(auxMaterial != null && material != null)
			auxMaterial.setModelodocumentofiscalICMS(material.getModelodocumentofiscalICMS());
		
		if(empresa!= null && empresa.getCdpessoa() != null)
			auxEmpresa = empresaService.loadWithMunicipioFiscal(empresa);
		
		if(cliente!= null && cliente.getCdpessoa() != null)
			auxCliente = clienteService.loadForTributacao(cliente);
		
		if(clienteEndereco!= null && clienteEndereco.getCdendereco() != null)
			enderecoCliente = enderecoService.loadEnderecoNota(clienteEndereco);
		
		if(fornecedorEndereco!= null && fornecedorEndereco.getCdendereco() != null)
			enderecoFornecedor = enderecoService.loadEnderecoNota(fornecedorEndereco);
		
		return new GrupotributacaoVO(operacao, 
									auxEmpresa, 
									naturezaoperacao, 
									materialgrupo,
									auxCliente,
									tributadoicms, 
									auxMaterial, 
									ncmcompleto, 
									enderecoCliente, 
									enderecoFornecedor,
									listaCategoriaCliente,
									listaCategoriaFornecedor,
									operacaonfe,
									considerarTributadoicms,
									modelodocumentoEnum,
									localDestinoNfe,
									notaDtEmissao);
	}
	
	public List<Grupotributacao> findGrupotributacaoCondicaoTrue(GrupotributacaoVO grupotributacaoVO) {
		List<Grupotributacao> lista = findGrupotributacao(grupotributacaoVO);
		List<Grupotributacao> listaCondicaoTrue = new ArrayList<Grupotributacao>();
		if(lista != null && !lista.isEmpty()){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			
			String startsWithJs = 	"if (typeof String.prototype.startsWith != 'function') { " +
									" 	String.prototype.startsWith = function (str){ " +
									"    	return this.indexOf(str) == 0; " +
									"  	}; " +
									"} \n" +
									"	function getData(data) {" +
									"		var arrayData = data.split('/');" +
									"		return new Date('' + (arrayData[1]) + '/' + arrayData[0] + '/' + arrayData[2]).getTime();" +
									" } ";

			engine.put("material_ncm", grupotributacaoVO.getNcmcompleto());
			engine.put("material_nome", grupotributacaoVO.getMaterialnome());
			engine.put("material_tipo", grupotributacaoVO.getMaterialtipo());
			engine.put("material_origem", grupotributacaoVO.getMaterialorigem());
			engine.put("empresa_uf", (grupotributacaoVO.getEmpresa() != null ? grupotributacaoVO.getEmpresa().getEnderecoSiglaEstado() : ""));
			engine.put("cliente_cnpj", grupotributacaoVO.getClientecnpj());
			engine.put("cliente_cpf", grupotributacaoVO.getClientecpf());
			engine.put("cliente_nomefantasia", grupotributacaoVO.getClientenomefantasia());
			engine.put("cliente_municipio", grupotributacaoVO.getClientemunicipio());
			engine.put("cliente_uf", grupotributacaoVO.getClienteuf());
			engine.put("cliente_regimetributacao", grupotributacaoVO.getClienteregimetributacao());
			engine.put("cliente_incidiriss", grupotributacaoVO.getClienteincideiriss());
			engine.put("cliente_codigotributacao", grupotributacaoVO.getClientecodigotributacao());
			engine.put("cliente_categoria", grupotributacaoVO.getClientecategoria());
			engine.put("cliente_contribuinteICMS", grupotributacaoVO.getClienteContribuinteICMS());
			engine.put("cliente_contribuinteicmstipo", grupotributacaoVO.getClienteContribuinteicmstipo());
			engine.put("cliente_consumidorfinal", grupotributacaoVO.getClienteConsumidorFinal() != null ? grupotributacaoVO.getClienteConsumidorFinal().getDescricao() : "");
			engine.put("fornecedor_uf", grupotributacaoVO.getFornecedoruf());
			engine.put("fornecedor_categoria", grupotributacaoVO.getFornecedorcategoria());
			engine.put("consumidor_final", grupotributacaoVO.getConsumidorfinal());
			engine.put("modelo", grupotributacaoVO.getModelo());
			engine.put("empresa_uf", "");
			engine.put("nota_dtemissao", SinedDateUtils.dateToTimestampInicioDia(grupotributacaoVO.getNotaDtEmissao() != null ? grupotributacaoVO.getNotaDtEmissao() : new Date(System.currentTimeMillis())).getTime());
			engine.put("nota_tipooperacao", grupotributacaoVO.getLocalDestinoNfe() != null ? grupotributacaoVO.getLocalDestinoNfe().name() : "");
			
			for(Grupotributacao gt : lista){
				Boolean resultado = true;
				if(gt.getCondicao() != null && !"".equals(gt.getCondicao())){
					try {
						Object obj;
						obj = engine.eval(startsWithJs + gt.getCondicao()); 
						
						if(obj != null){
							String resultadoStr = obj.toString();
							resultado = new Boolean(resultadoStr);
						}
					} catch (ScriptException e) {
						e.printStackTrace();
						resultado = false;
					}
				}
				
				if(resultado)
					listaCondicaoTrue.add(gt);
			}
		}
		return listaCondicaoTrue;
	}

	public List<Grupotributacao> findGrupotributacao(GrupotributacaoVO grupotributacaoVO) {
		if(grupotributacaoVO == null) return null;
		return findGrupotributacao(grupotributacaoVO.getEmpresa(), grupotributacaoVO.getOperacao(), grupotributacaoVO.getTributadoicms(), grupotributacaoVO.getNaturezaoperacao(), 
				grupotributacaoVO.getMaterialgrupo(), grupotributacaoVO.getConsiderarTributadoicms(), grupotributacaoVO.getNcmcompleto(),
				grupotributacaoVO.getMaterialTipoBean(), grupotributacaoVO.getLocalDestinoNfe(), grupotributacaoVO.getContribuinteIcmsTipoEnum(), grupotributacaoVO.getOperacaonfe());
	}
	
	public List<Grupotributacao> findGrupotributacao(Empresa empresa, Operacao operacao, Boolean tributadoicms, Naturezaoperacao naturezaoperacao, Materialgrupo materialgrupo, Boolean considerarTributadoicms, String ncm, Materialtipo materialTipo, Localdestinonfe localDestinoNfe, Contribuinteicmstipo contribuinteIcmsTipo, Operacaonfe operacaonfe) {
		return grupotributacaoDAO.findGrupotributacao(empresa, operacao, tributadoicms, naturezaoperacao, materialgrupo, considerarTributadoicms, ncm, materialTipo, localDestinoNfe, contribuinteIcmsTipo, operacaonfe);
	}
	
	public Grupotributacao getGrupotributacao(Operacao operacao, Boolean tributadoicms, Empresa empresa, Materialgrupo materialgrupo, 
												Naturezaoperacao naturezaoperacao, Grupotributacao grupotributacao, Boolean considerarTributadoicms, String ncm, Materialtipo materialTipo, Localdestinonfe localDestinoNfe, Contribuinteicmstipo contribuinteIcmsTipo, Operacaonfe operacaonfe) {
		List<Grupotributacao> lista = findGrupotributacao(empresa, operacao, tributadoicms, naturezaoperacao, materialgrupo, considerarTributadoicms, ncm, materialTipo, localDestinoNfe, contribuinteIcmsTipo, operacaonfe);
		if(lista != null && !lista.isEmpty()){
			if(grupotributacao != null){
				for(Grupotributacao gt : lista){
					if(gt.equals(grupotributacao)) return gt;
				}
			}
			return lista.get(0);
		}
		return null;
	}

	public Grupotributacao getSugestaoGrupotributacao(List<Grupotributacao> listaGrupotributacao, Grupotributacao grupoTributacaoPrioridade) {
		if(listaGrupotributacao == null || listaGrupotributacao.isEmpty()) return null;

		if(grupoTributacaoPrioridade != null && grupoTributacaoPrioridade.getCdgrupotributacao() != null && listaGrupotributacao.contains(grupoTributacaoPrioridade)){
			return listaGrupotributacao.get(listaGrupotributacao.indexOf(grupoTributacaoPrioridade));
		}
		
		for(Grupotributacao gt : listaGrupotributacao){
			if(gt.getCondicao() != null && !"".equals(gt.getCondicao()))
				return gt;
		}
		
		for(Grupotributacao gt : listaGrupotributacao){
			if(gt.getListaGrupotributacaoncm() != null && !gt.getListaGrupotributacaoncm().isEmpty())
				return gt;
		}
		
		return listaGrupotributacao.get(0);
	}
	
	public Grupotributacao getSugestaoGrupotributacao(List<Grupotributacao> listaGrupotributacao) {
		return getSugestaoGrupotributacao(listaGrupotributacao, null);
	}

	public Grupotributacao loadWithInfAdicional(Grupotributacao grupotributacao) {
		return grupotributacaoDAO.loadWithInfAdicional(grupotributacao);
	}
	
	public void limparReferenciasForCopiar(Grupotributacao grupotributacao) {
		if(grupotributacao != null){
			grupotributacao.setCdgrupotributacao(null);
			
			if(grupotributacao.getListaGrupotributacaoempresa() != null && !grupotributacao.getListaGrupotributacaoempresa().isEmpty()){
				for(Grupotributacaoempresa grupotributacaoempresa : grupotributacao.getListaGrupotributacaoempresa()){
					grupotributacaoempresa.setCdgrupotributacaoempresa(null);
				}
			}
			if(grupotributacao.getListaGrupotributacaomaterialgrupo() != null && !grupotributacao.getListaGrupotributacaomaterialgrupo().isEmpty()){
				for(Grupotributacaomaterialgrupo grupotributacaomaterialgrupo : grupotributacao.getListaGrupotributacaomaterialgrupo()){
					grupotributacaomaterialgrupo.setCdgrupotributacaomaterialgrupo(null);
				}
			}			
			if(grupotributacao.getListaGrupotributacaonaturezaoperacao() != null && !grupotributacao.getListaGrupotributacaonaturezaoperacao().isEmpty()){
				for(Grupotributacaonaturezaoperacao grupotributacaonaturezaoperacao : grupotributacao.getListaGrupotributacaonaturezaoperacao()){
					grupotributacaonaturezaoperacao.setCdgrupotributacaonaturezaoperacao(null);
				}
			}
			if(grupotributacao.getListaGrupotributacaoimposto() != null && !grupotributacao.getListaGrupotributacaoimposto().isEmpty()){
				for(Grupotributacaoimposto grupotributacaoimposto : grupotributacao.getListaGrupotributacaoimposto()){
					grupotributacaoimposto.setCdgrupotributacaoimposto(null);
				}
			}
		}
	}
	
	public List<Grupotributacao> findAtivosForImportarXMLNFSe(Empresa empresa){
		return grupotributacaoDAO.findAtivosForImportarXMLNFSe(empresa);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icmsst de acordo com a formula do grupo de tributa��o 
	*
	* @param item
	* @param grupotributacao
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public void calcularValoresFormulaIcms(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		calcularValorBCIcms(item, grupotributacao);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo e o valor de icmsst de acordo com a formula do grupo de tributa��o 
	*
	* @param item
	* @param grupotributacao
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValoresFormulaIcmsST(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		calcularValorBCIcmsst(item, grupotributacao);
		calcularValorIcmsst(item, grupotributacao);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo e o valor de icmsst de acordo com a formula do grupo de tributa��o 
	*
	* @param item
	* @param grupotributacao
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValoresFormulaIcmsST(Entregamaterial item, Grupotributacao grupotributacao){
		calcularValorBCIcmsst(item, grupotributacao);
		calcularValorIcmsst(item, grupotributacao);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icmsst de acordo com a formula do grupo de tributa��o 
	*
	* @param item
	* @param grupotributacao
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public void calcularValoresFormulaIcms(Entregamaterial item, Grupotributacao grupotributacao){
		calcularValorBCIcms(item, grupotributacao);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de ipi de acordo com a formula do grupo de tributa��o 
	*
	* @param item
	* @param grupotributacao
	* @since 15/05/2017
	* @author Luiz Fernando
	*/
	public void calcularValoresFormulaIpi(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		calcularValorBCIpi(item, grupotributacao);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icms de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public void calcularValorBCIcms(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcicms()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcicms()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcicms(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
			if(item.getIcms() != null){
				item.setValoricms(new Money((item.getValorbcicms().getValue().doubleValue() * item.getIcms())/100d));
			}
		}
	}
	
	public void calcularValorBCIcmsDestinatario(CalculoDIFALInterface item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcicmsdestinatario()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaDestinatarioGrupotributacao(engine, item, grupotributacao, 0d);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcicmsdestinatario()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcdestinatario(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icmsst de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValorBCIcmsst(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcst()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulabcst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcicmsst(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que calcula o valor de icmsst de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValorIcmsst(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulavalorst()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulavalorst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValoricmsst(new Money(resultado != null ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que calcula o valor da base de calculo do ipi
	*
	* @param item
	* @param grupotributacao
	* @since 15/05/2017
	* @author Luiz Fernando
	*/
	public void calcularValorBCIpi(CalculoImpostoInterface item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcipi()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacaoIPI(engine, item, grupotributacao, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcipi()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcipi(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
			if(item.getIpi() != null){
				item.setValoripi(new Money((item.getValorbcipi().getValue().doubleValue() * item.getIpi())/100d));
			}
		}
	}
	
	/**
	* M�todo que seta as variaveis da formula de base de calculo e valor de icmsst
	*
	* @param engine
	* @param item
	* @param grupotributacao
	* @param formulaBC
	* @since 20/01/2016
	* @author Luiz Fernando
	*/
	private void setVariaveisFormulaGrupotributacao(ScriptEngine engine, CalculoImpostoInterface item, Grupotributacao grupotributacao, boolean formulaBC) {
		setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, formulaBC, 0d, false);
	}
	
	/**
	* M�todo que seta as variaveis da formula de base de calculo de ipi
	*
	* @param engine
	* @param item
	* @param grupotributacao
	* @param formulaBC
	* @since 15/05/2017
	* @author Luiz Fernando
	*/
	private void setVariaveisFormulaGrupotributacaoIPI(ScriptEngine engine, CalculoImpostoInterface item, Grupotributacao grupotributacao, boolean formulaBC) {
		setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, formulaBC, 0d, true);
	}
	
	/**
	* M�todo que seta as variaveis da formula de base de calculo e valor de icmsst
	*
	* @param engine
	* @param item
	* @param grupotributacao
	* @param formulaBC
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	private void setVariaveisFormulaGrupotributacao(ScriptEngine engine, CalculoImpostoInterface item, Grupotributacao grupotributacao, boolean formulaBC, Double valorNulo, boolean ipi) {
		engine.put("vr_bruto", item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : valorNulo);
		
		if(!ipi){
			engine.put("alq_icms", item.getIcms() != null ? item.getIcms() : valorNulo);
			engine.put("bc_icms", item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : valorNulo);
			engine.put("vr_icms", item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : valorNulo);
		}
		
		engine.put("alq_ipi", item.getIpi() != null ? item.getIpi() : valorNulo);
		if(!ipi){
			engine.put("bc_ipi", item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : valorNulo);
			engine.put("vr_ipi", item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : valorNulo);
		}
		
		engine.put("alq_pis", item.getPis() != null ? item.getPis() : valorNulo);
		engine.put("bc_pis", item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : valorNulo);
		engine.put("vr_pis", item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : valorNulo);
		
		engine.put("alq_cofins", item.getCofins() != null ? item.getCofins() : valorNulo);
		engine.put("bc_cofins", item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : valorNulo);
		engine.put("vr_cofins", item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : valorNulo);
		
		if(!ipi){
			engine.put("alq_icms_st", item.getIcmsst() != null ? item.getIcmsst() : valorNulo);
			engine.put("vr_icms_st", item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : valorNulo);
			engine.put("mva", item.getMargemvaloradicionalicmsst() != null ? item.getMargemvaloradicionalicmsst() : valorNulo);
			engine.put("reducao_icms_st", grupotributacao.getReducaobcicmsst() != null ? grupotributacao.getReducaobcicmsst() : valorNulo);
			engine.put("uf_icms_st", grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "");
		}
		engine.put("frete", item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : valorNulo);
		engine.put("desconto", item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : valorNulo);
		engine.put("seguro", item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : valorNulo);
		engine.put("outras_despesas", item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : valorNulo);
		if(ipi){
			engine.put("consumidor_final", item.getOperacaonfe() != null && item.getOperacaonfe().equals(Operacaonfe.CONSUMIDOR_FINAL));
		}
		if(!ipi){
			engine.put("bc_icms_st", !formulaBC && item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : valorNulo);
			engine.put("credito_icms_aproveitado", item.getValorcreditoicms() != null ? item.getValorcreditoicms().getValue().doubleValue() : valorNulo);
		}
	}
	
	private void setVariaveisFormulaDestinatarioGrupotributacao(ScriptEngine engine, CalculoDIFALInterface item, Grupotributacao grupotributacao, Double valorNulo) {
		engine.put("vr_bruto", item.getValorbruto() != null ? item.getValorbruto().getValue().doubleValue() : valorNulo);
		engine.put("alq_icms", item.getIcms() != null ? item.getIcms() : valorNulo);
		engine.put("alq_fcp", item.getFcp() != null ? item.getFcp() : valorNulo);
		engine.put("vr_ipi", item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : valorNulo);
		engine.put("frete", item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : valorNulo);
		engine.put("desconto", item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : valorNulo);
		engine.put("seguro", item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : valorNulo);
		engine.put("outras_despesas", item.getOutrasdespesas() != null ? item.getOutrasdespesas().getValue().doubleValue() : valorNulo);
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icms de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public void calcularValorBCIcms(Entregamaterial item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcicms()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcicms()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcicms(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que calcula o valor da base de calculo de icmsst de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValorBCIcmsst(Entregamaterial item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcst()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulabcst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValorbcicmsst(new Money(resultado != null && !resultado.isNaN() && !resultado.isInfinite() ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que calcula o valor de icmsst de acordo com a formula do grupo de tributa��o
	*
	* @param item
	* @param grupotributacao
	* @since 25/06/2015
	* @author Luiz Fernando
	*/
	public void calcularValorIcmsst(Entregamaterial item, Grupotributacao grupotributacao){
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulavalorst()) && item != null){
			
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, item, grupotributacao, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulavalorst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			item.setValoricmsst(new Money(resultado != null ? resultado : 0d));
		}
	}
	
	/**
	* M�todo que seta as variaveis da formula de base de calculo e valor de icmsst
	*
	* @param engine
	* @param item
	* @param grupotributacao
	* @param formulaBC
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	private void setVariaveisFormulaGrupotributacao(ScriptEngine engine, Entregamaterial item, Grupotributacao grupotributacao, boolean formulaBC) {
		engine.put("vr_total", item.getQtde() != null && item.getValorunitario() != null ? item.getQtde() * item.getValorunitario() : 0d);
		engine.put("vr_bruto", item.getQtde() != null && item.getValorunitario() != null ? item.getQtde() * item.getValorunitario() : 0d);
		
		engine.put("alq_icms", item.getIcms() != null ? item.getIcms() : 0d);
		engine.put("bc_icms", item.getValorbcicms() != null ? item.getValorbcicms().getValue().doubleValue() : 0d);
		engine.put("vr_icms", item.getValoricms() != null ? item.getValoricms().getValue().doubleValue() : 0d);
		
		engine.put("alq_ipi", item.getIpi() != null ? item.getIpi() : 0d);
		engine.put("bc_ipi", item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0d);
		engine.put("vr_ipi", item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0d);
		
		engine.put("alq_pis", item.getPis() != null ? item.getPis() : 0d);
		engine.put("bc_pis", item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0d);
		engine.put("vr_pis", item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0d);
		
		engine.put("alq_cofins", item.getCofins() != null ? item.getCofins() : 0d);
		engine.put("bc_cofins", item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0d);
		engine.put("vr_cofins", item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0d);
		
		engine.put("alq_icms_st", item.getIcmsst() != null ? item.getIcmsst() : 0d);
		engine.put("mva", item.getValormva() != null ? item.getValormva() : 0d);
		
		engine.put("reducao_icms_st", grupotributacao.getReducaobcicmsst() != null ? grupotributacao.getReducaobcicmsst() : 0d);
		engine.put("uf_icms_st", grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "");
		engine.put("frete", item.getValorfrete() != null ? item.getValorfrete().getValue().doubleValue() : 0d);
		engine.put("desconto", item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d);
		engine.put("seguro", item.getValorseguro() != null ? item.getValorseguro().getValue().doubleValue() : 0d);
		engine.put("outras_despesas", item.getValoroutrasdespesas() != null ? item.getValoroutrasdespesas().getValue().doubleValue() : 0d);
		
		engine.put("bc_icms_st", !formulaBC && item.getValorbcicmsst() != null ? item.getValorbcicmsst().getValue().doubleValue() : 0d);
	}
	
	/**
	* M�todo que cria a fun��o de base de calculo de icms
	*
	* @param grupotributacao
	* @param formulabcicms
	* @param tela
	* @return
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public String criaFuncaoBaseCalculoICMSJavascript(Grupotributacao grupotributacao, String formulabcicms, String tela) {
		return "function (" + ("ENTRADAFISCAL".equals(tela) ? "index" : "")+ " ){ " +
					(grupotributacao != null ? criaVariaveisFuncaoICMSJavascript(grupotributacao, false, tela) : "") +
					"try {\n " +
					(StringUtils.isNotEmpty(formulabcicms) ? 
							( "ENTRADAFISCAL".equals(tela) ?
									("form['listaEntregamaterial['+index+'].valorbcicms'].value = $s.toMoney(" + formulabcicms + "); \nreturn true; ")
										:
									("form['valorbcicms'].value = $s.toMoney(" + formulabcicms + "); \nreturn true; ") 
							) 
							: " ;\nreturn false; ") + 
					" }catch(erro) {\n msgErroCalculo = 'Erro ao calcular base de calculo de icms: ' + erro.message;} if(msgErroCalculo != ''){alert(msgErroCalculo); return false;} " +
				"}; ";
	}
	
	/**
	* M�todo que cria a fun��o de base de calculo de icmsst
	*
	* @param grupotributacao
	* @param formulabcst
	* @param tela
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public String criaFuncaoBaseCalculoICMSSTJavascript(Grupotributacao grupotributacao, String formulabcst, String tela) {
		return "function (" + ("ENTRADAFISCAL".equals(tela) ? "index" : "")+ " ){ " +
					(grupotributacao != null ? criaVariaveisFuncaoICMSSTJavascript(grupotributacao, false, tela) : "") +
					"try {\n " +
					(StringUtils.isNotEmpty(formulabcst) ? 
							( "ENTRADAFISCAL".equals(tela) ?
									("form['listaEntregamaterial['+index+'].valorbcicmsst'].value = $s.toMoney(" + formulabcst + "); \nreturn true; ")
										:
									("form['valorbcicmsst'].value = $s.toMoney(" + formulabcst + "); \nreturn true; ") 
							) 
							: " ;\nreturn false; ") + 
					" }catch(erro) {\n msgErroCalculo = 'Erro ao calcular base de calculo de icmsst: ' + erro.message;} if(msgErroCalculo != ''){alert(msgErroCalculo); return false;} " +
				"}; ";
	}
	/**
	* M�todo que cria a fun��o de valor de icmsst
	*
	* @param grupotributacao
	* @param formulavalorst
	* @param tela
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public String criaFuncaoValorICMSSTJavascript(Grupotributacao grupotributacao, String formulavalorst, String tela) {
		return "function (" + ("ENTRADAFISCAL".equals(tela) ? "index" : "")+ " ){ " +
					(grupotributacao != null ? criaVariaveisFuncaoICMSSTJavascript(grupotributacao, true, tela) : "") +
					"try {\n " +
					(StringUtils.isNotEmpty(formulavalorst) ?  
							( "ENTRADAFISCAL".equals(tela) ? 
									("form['listaEntregamaterial['+index+'].valoricmsst'].value = $s.toMoney(" + formulavalorst + "); \nreturn true; ")
										:
									("form['valoricmsst'].value = $s.toMoney(" + formulavalorst + "); \nreturn true; ")
							)
							: " ;\nreturn false; ") + 
					" }catch(erro) {\n msgErroCalculo = 'Erro ao calcular valor de icmsst: ' + erro.message;} if(msgErroCalculo != ''){alert(msgErroCalculo); return false;} " +
				"}; ";
	}
	
	public String criaFuncaoBaseCalculoIPIJavascript(Grupotributacao grupotributacao, String formulabcipi, String tela) {
		return "function (" + ("ENTRADAFISCAL".equals(tela) ? "index" : "")+ " ){ " +
			(grupotributacao != null ? criaVariaveisFuncaoIPIJavascript(grupotributacao, false, tela) : "") +
			"try {\n " +
			(StringUtils.isNotEmpty(formulabcipi) ? 
					( "ENTRADAFISCAL".equals(tela) ?
							("form['listaEntregamaterial['+index+'].valorbcipi'].value = $s.toMoney(" + formulabcipi + "); \nreturn true; ")
								:
							("form['valorbcipi'].value = $s.toMoney(" + formulabcipi + "); \nreturn true; ") 
					) 
					: " ;\nreturn false; ") + 
			" }catch(erro) {\n msgErroCalculo = 'Erro ao calcular base de calculo de ipi: ' + erro.message;} if(msgErroCalculo != ''){alert(msgErroCalculo); return false;} " +
		"}; ";
	}
	
	/**
	* M�todo que criar as variaveis para a fun��o de icmsst
	*
	* @param grupotributacao
	* @param isFormulavalor
	* @param tela
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	private String criaVariaveisFuncaoICMSSTJavascript(Grupotributacao grupotributacao, boolean isFormulavalor, String tela) {
		if("NOTAFISCALPRODUTO".equals(tela)){
			return 
				" var vr_bruto = form['valorbruto'] && form['valorbruto'].value != '' ? $s.toFloat(form['valorbruto'].value) : 0; \n" +
				" var alq_icms = form['icms'] && form['icms'].value != '' ? $s.toFloat(form['icms'].value) : 0;  \n" +
				" var bc_icms = form['valorbcicms'] && form['valorbcicms'].value != '' ? $s.toFloat(form['valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['valoricms'] && form['valoricms'].value != '' ? $s.toFloat(form['valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['ipi'] && form['ipi'].value != '' ? $s.toFloat(form['ipi'].value) : 0;  \n" +
				" var bc_ipi = form['valorbcipi'] && form['valorbcipi'].value != '' ? $s.toFloat(form['valorbcipi'].value) : 0;  \n" +
				" var vr_ipi = form['valoripi'] && form['valoripi'].value != '' ? $s.toFloat(form['valoripi'].value) : 0;  \n" +
				" var alq_pis = form['pis'] && form['pis'].value != '' ? $s.toFloat(form['pis'].value) : 0;  \n" +
				" var bc_pis = form['valorbcpis'] && form['valorbcpis'].value != '' ? $s.toFloat(form['valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['valorpis'] && form['valorpis'].value != '' ? $s.toFloat(form['valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['cofins'] && form['cofins'].value != '' ? $s.toFloat(form['cofins'].value) : 0;  \n" +
				" var bc_cofins = form['valorbccofins'] && form['valorbccofins'].value != '' ? $s.toFloat(form['valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['valorcofins'] && form['valorcofins'].value != '' ? $s.toFloat(form['valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['icmsst'] && form['icmsst'].value != '' ? $s.toFloat(form['icmsst'].value) : 0;  \n" +
				" var mva = form['margemvaloradicionalicmsst'] && form['margemvaloradicionalicmsst'].value != '' ? $s.toFloat(form['margemvaloradicionalicmsst'].value) : 0;  \n" +
				" var reducao_icms_st = form['reducaobcicmsst'] && form['reducaobcicmsst'].value != '' ? $s.toFloat(form['reducaobcicmsst'].value) : 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['valorfrete'] && form['valorfrete'].value != '' ? $s.toFloat(form['valorfrete'].value) : 0;  \n" +
				" var desconto = form['valordesconto'] && form['valordesconto'].value != '' ? $s.toFloat(form['valordesconto'].value) : 0;  \n" +
				" var seguro = form['valorseguro'] && form['valorseguro'].value != '' ? $s.toFloat(form['valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['outrasdespesas'] && form['outrasdespesas'].value != '' ? $s.toFloat(form['outrasdespesas'].value) : 0;  \n" + 
				" var credito_icms_aproveitado = form['valorcreditoicms'] && form['valorcreditoicms'].value != '' ? $s.toFloat(form['valorcreditoicms'].value) : 0; \n" +
				" var bc_icms_st = " + (isFormulavalor ? " form['valorbcicmsst'] && form['valorbcicmsst'].value != '' ? $s.toFloat(form['valorbcicmsst'].value) : 0 " : " 0 ") + "; \n" +
				" var msgErroCalculo = ''; \n";
		}else if("ENTRADAFISCAL".equals(tela)){
			return 
				" var vr_bruto = form['listaEntregamaterial['+index+'].valortotal'] && form['listaEntregamaterial['+index+'].valortotal'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valortotal'].value) : 0; \n" +
				" var alq_icms = form['listaEntregamaterial['+index+'].icms'] && form['listaEntregamaterial['+index+'].icms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icms'].value) : 0;  \n" +
				" var bc_icms = form['listaEntregamaterial['+index+'].valorbcicms'] && form['listaEntregamaterial['+index+'].valorbcicms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['listaEntregamaterial['+index+'].valoricms'] && form['listaEntregamaterial['+index+'].valoricms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['listaEntregamaterial['+index+'].ipi'] && form['listaEntregamaterial['+index+'].ipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].ipi'].value) : 0;  \n" +
				" var bc_ipi = form['listaEntregamaterial['+index+'].valorbcipi'] && form['listaEntregamaterial['+index+'].valorbcipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcipi'].value) : 0;  \n" +
				" var vr_ipi = form['listaEntregamaterial['+index+'].valoripi'] && form['listaEntregamaterial['+index+'].valoripi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoripi'].value) : 0;  \n" +
				" var alq_pis = form['listaEntregamaterial['+index+'].pis'] && form['listaEntregamaterial['+index+'].pis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].pis'].value) : 0;  \n" +
				" var bc_pis = form['listaEntregamaterial['+index+'].valorbcpis'] && form['listaEntregamaterial['+index+'].valorbcpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['listaEntregamaterial['+index+'].valorpis'] && form['listaEntregamaterial['+index+'].valorpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['listaEntregamaterial['+index+'].cofins'] && form['listaEntregamaterial['+index+'].cofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].cofins'].value) : 0;  \n" +
				" var bc_cofins = form['listaEntregamaterial['+index+'].valorbccofins'] && form['listaEntregamaterial['+index+'].valorbccofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['listaEntregamaterial['+index+'].valorcofins'] && form['listaEntregamaterial['+index+'].valorcofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['listaEntregamaterial['+index+'].icmsst'] && form['listaEntregamaterial['+index+'].icmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icmsst'].value) : 0;  \n" +
				" var mva = form['listaEntregamaterial['+index+'].valormva'] && form['listaEntregamaterial['+index+'].valormva'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valormva'].value) : 0;  \n" +
				" var reducao_icms_st = 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['listaEntregamaterial['+index+'].valorfrete'] && form['listaEntregamaterial['+index+'].valorfrete'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorfrete'].value) : 0;  \n" +
				" var desconto = form['listaEntregamaterial['+index+'].valordesconto'] && form['listaEntregamaterial['+index+'].valordesconto'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valordesconto'].value) : 0;  \n" +
				" var seguro = form['listaEntregamaterial['+index+'].valorseguro'] && form['listaEntregamaterial['+index+'].valorseguro'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['listaEntregamaterial['+index+'].valoroutrasdespesas'] && form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value) : 0;  \n" + 
				" var bc_icms_st = " + (isFormulavalor ? " form['listaEntregamaterial['+index+'].valorbcicmsst'] && form['listaEntregamaterial['+index+'].valorbcicmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicmsst'].value) : 0 " : " 0 ") + "; \n" +
				" var msgErroCalculo = ''; \n";
		}else {
			return "";
		}
	}
	
	/**
	* M�todo que criar as variaveis para a fun��o de icms
	*
	* @param grupotributacao
	* @param isFormulavalor
	* @param tela
	* @return
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	private String criaVariaveisFuncaoICMSJavascript(Grupotributacao grupotributacao, boolean isFormulavalor, String tela) {
		if("NOTAFISCALPRODUTO".equals(tela)){
			return 
				" var vr_bruto = form['valorbruto'] && form['valorbruto'].value != '' ? $s.toFloat(form['valorbruto'].value) : 0; \n" +
				" var alq_icms = form['icms'] && form['icms'].value != '' ? $s.toFloat(form['icms'].value) : 0;  \n" +
				" var bc_icms = form['valorbcicms'] && form['valorbcicms'].value != '' ? $s.toFloat(form['valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['valoricms'] && form['valoricms'].value != '' ? $s.toFloat(form['valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['ipi'] && form['ipi'].value != '' ? $s.toFloat(form['ipi'].value) : 0;  \n" +
				" var bc_ipi = form['valorbcipi'] && form['valorbcipi'].value != '' ? $s.toFloat(form['valorbcipi'].value) : 0;  \n" +
				" var vr_ipi = form['valoripi'] && form['valoripi'].value != '' ? $s.toFloat(form['valoripi'].value) : 0;  \n" +
				" var alq_pis = form['pis'] && form['pis'].value != '' ? $s.toFloat(form['pis'].value) : 0;  \n" +
				" var bc_pis = form['valorbcpis'] && form['valorbcpis'].value != '' ? $s.toFloat(form['valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['valorpis'] && form['valorpis'].value != '' ? $s.toFloat(form['valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['cofins'] && form['cofins'].value != '' ? $s.toFloat(form['cofins'].value) : 0;  \n" +
				" var bc_cofins = form['valorbccofins'] && form['valorbccofins'].value != '' ? $s.toFloat(form['valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['valorcofins'] && form['valorcofins'].value != '' ? $s.toFloat(form['valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['icmsst'] && form['icmsst'].value != '' ? $s.toFloat(form['icmsst'].value) : 0;  \n" +
				" var mva = form['margemvaloradicionalicmsst'] && form['margemvaloradicionalicmsst'].value != '' ? $s.toFloat(form['margemvaloradicionalicmsst'].value) : 0;  \n" +
				" var reducao_icms_st = form['reducaobcicmsst'] && form['reducaobcicmsst'].value != '' ? $s.toFloat(form['reducaobcicmsst'].value) : 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['valorfrete'] && form['valorfrete'].value != '' ? $s.toFloat(form['valorfrete'].value) : 0;  \n" +
				" var desconto = form['valordesconto'] && form['valordesconto'].value != '' ? $s.toFloat(form['valordesconto'].value) : 0;  \n" +
				" var seguro = form['valorseguro'] && form['valorseguro'].value != '' ? $s.toFloat(form['valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['outrasdespesas'] && form['outrasdespesas'].value != '' ? $s.toFloat(form['outrasdespesas'].value) : 0;  \n" + 
				" var bc_icms_st = " + (isFormulavalor ? " form['valorbcicmsst'] && form['valorbcicmsst'].value != '' ? $s.toFloat(form['valorbcicmsst'].value) : 0 " : " 0 ") + "; \n" +
				" var msgErroCalculo = ''; \n";
		}else if("ENTRADAFISCAL".equals(tela)){
			return 
				" var vr_bruto = form['listaEntregamaterial['+index+'].valortotal'] && form['listaEntregamaterial['+index+'].valortotal'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valortotal'].value) : 0; \n" +
				" var alq_icms = form['listaEntregamaterial['+index+'].icms'] && form['listaEntregamaterial['+index+'].icms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icms'].value) : 0;  \n" +
				" var bc_icms = form['listaEntregamaterial['+index+'].valorbcicms'] && form['listaEntregamaterial['+index+'].valorbcicms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['listaEntregamaterial['+index+'].valoricms'] && form['listaEntregamaterial['+index+'].valoricms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['listaEntregamaterial['+index+'].ipi'] && form['listaEntregamaterial['+index+'].ipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].ipi'].value) : 0;  \n" +
				" var bc_ipi = form['listaEntregamaterial['+index+'].valorbcipi'] && form['listaEntregamaterial['+index+'].valorbcipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcipi'].value) : 0;  \n" +
				" var vr_ipi = form['listaEntregamaterial['+index+'].valoripi'] && form['listaEntregamaterial['+index+'].valoripi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoripi'].value) : 0;  \n" +
				" var alq_pis = form['listaEntregamaterial['+index+'].pis'] && form['listaEntregamaterial['+index+'].pis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].pis'].value) : 0;  \n" +
				" var bc_pis = form['listaEntregamaterial['+index+'].valorbcpis'] && form['listaEntregamaterial['+index+'].valorbcpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['listaEntregamaterial['+index+'].valorpis'] && form['listaEntregamaterial['+index+'].valorpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['listaEntregamaterial['+index+'].cofins'] && form['listaEntregamaterial['+index+'].cofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].cofins'].value) : 0;  \n" +
				" var bc_cofins = form['listaEntregamaterial['+index+'].valorbccofins'] && form['listaEntregamaterial['+index+'].valorbccofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['listaEntregamaterial['+index+'].valorcofins'] && form['listaEntregamaterial['+index+'].valorcofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['listaEntregamaterial['+index+'].icmsst'] && form['listaEntregamaterial['+index+'].icmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icmsst'].value) : 0;  \n" +
				" var mva = form['listaEntregamaterial['+index+'].valormva'] && form['listaEntregamaterial['+index+'].valormva'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valormva'].value) : 0;  \n" +
				" var reducao_icms_st = 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['listaEntregamaterial['+index+'].valorfrete'] && form['listaEntregamaterial['+index+'].valorfrete'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorfrete'].value) : 0;  \n" +
				" var desconto = form['listaEntregamaterial['+index+'].valordesconto'] && form['listaEntregamaterial['+index+'].valordesconto'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valordesconto'].value) : 0;  \n" +
				" var seguro = form['listaEntregamaterial['+index+'].valorseguro'] && form['listaEntregamaterial['+index+'].valorseguro'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['listaEntregamaterial['+index+'].valoroutrasdespesas'] && form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value) : 0;  \n" + 
				" var bc_icms_st = " + (isFormulavalor ? " form['listaEntregamaterial['+index+'].valorbcicmsst'] && form['listaEntregamaterial['+index+'].valorbcicmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicmsst'].value) : 0 " : " 0 ") + "; \n" +
				" var msgErroCalculo = ''; \n";
		}else {
			return "";
		}
	}
	
	private String criaVariaveisFuncaoIPIJavascript(Grupotributacao grupotributacao, boolean isFormulavalor, String tela) {
		if("NOTAFISCALPRODUTO".equals(tela)){
			return 
				" var vr_bruto = form['valorbruto'] && form['valorbruto'].value != '' ? $s.toFloat(form['valorbruto'].value) : 0; \n" +
				" var alq_icms = form['icms'] && form['icms'].value != '' ? $s.toFloat(form['icms'].value) : 0;  \n" +
				" var bc_icms = form['valorbcicms'] && form['valorbcicms'].value != '' ? $s.toFloat(form['valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['valoricms'] && form['valoricms'].value != '' ? $s.toFloat(form['valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['ipi'] && form['ipi'].value != '' ? $s.toFloat(form['ipi'].value) : 0;  \n" +
				" var vr_ipi = form['valoripi'] && form['valoripi'].value != '' ? $s.toFloat(form['valoripi'].value) : 0;  \n" +
				" var alq_pis = form['pis'] && form['pis'].value != '' ? $s.toFloat(form['pis'].value) : 0;  \n" +
				" var bc_pis = form['valorbcpis'] && form['valorbcpis'].value != '' ? $s.toFloat(form['valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['valorpis'] && form['valorpis'].value != '' ? $s.toFloat(form['valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['cofins'] && form['cofins'].value != '' ? $s.toFloat(form['cofins'].value) : 0;  \n" +
				" var bc_cofins = form['valorbccofins'] && form['valorbccofins'].value != '' ? $s.toFloat(form['valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['valorcofins'] && form['valorcofins'].value != '' ? $s.toFloat(form['valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['icmsst'] && form['icmsst'].value != '' ? $s.toFloat(form['icmsst'].value) : 0;  \n" +
				" var mva = form['margemvaloradicionalicmsst'] && form['margemvaloradicionalicmsst'].value != '' ? $s.toFloat(form['margemvaloradicionalicmsst'].value) : 0;  \n" +
				" var reducao_icms_st = form['reducaobcicmsst'] && form['reducaobcicmsst'].value != '' ? $s.toFloat(form['reducaobcicmsst'].value) : 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['valorfrete'] && form['valorfrete'].value != '' ? $s.toFloat(form['valorfrete'].value) : 0;  \n" +
				" var desconto = form['valordesconto'] && form['valordesconto'].value != '' ? $s.toFloat(form['valordesconto'].value) : 0;  \n" +
				" var seguro = form['valorseguro'] && form['valorseguro'].value != '' ? $s.toFloat(form['valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['outrasdespesas'] && form['outrasdespesas'].value != '' ? $s.toFloat(form['outrasdespesas'].value) : 0;  \n" + 
				" var bc_icms_st = form['valorbcicmsst'] && form['valorbcicmsst'].value != '' ? $s.toFloat(form['valorbcicmsst'].value) : 0;  \n" +
				" var consumidor_final = parent.form['operacaonfe'] && parent.form['operacaonfe'].value == 'CONSUMIDOR_FINAL';  \n" +
				" var msgErroCalculo = ''; \n";
		}else if("ENTRADAFISCAL".equals(tela)){
			return 
				" var vr_bruto = form['listaEntregamaterial['+index+'].valortotal'] && form['listaEntregamaterial['+index+'].valortotal'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valortotal'].value) : 0; \n" +
				" var alq_icms = form['listaEntregamaterial['+index+'].icms'] && form['listaEntregamaterial['+index+'].icms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icms'].value) : 0;  \n" +
				" var bc_icms = form['listaEntregamaterial['+index+'].valorbcicms'] && form['listaEntregamaterial['+index+'].valorbcicms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicms'].value) : 0;  \n" +
				" var vr_icms = form['listaEntregamaterial['+index+'].valoricms'] && form['listaEntregamaterial['+index+'].valoricms'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoricms'].value) : 0;  \n" +
				" var alq_ipi = form['listaEntregamaterial['+index+'].ipi'] && form['listaEntregamaterial['+index+'].ipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].ipi'].value) : 0;  \n" +
				" var bc_ipi = form['listaEntregamaterial['+index+'].valorbcipi'] && form['listaEntregamaterial['+index+'].valorbcipi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcipi'].value) : 0;  \n" +
				" var vr_ipi = form['listaEntregamaterial['+index+'].valoripi'] && form['listaEntregamaterial['+index+'].valoripi'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoripi'].value) : 0;  \n" +
				" var alq_pis = form['listaEntregamaterial['+index+'].pis'] && form['listaEntregamaterial['+index+'].pis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].pis'].value) : 0;  \n" +
				" var bc_pis = form['listaEntregamaterial['+index+'].valorbcpis'] && form['listaEntregamaterial['+index+'].valorbcpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcpis'].value) : 0;  \n" +
				" var vr_pis = form['listaEntregamaterial['+index+'].valorpis'] && form['listaEntregamaterial['+index+'].valorpis'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorpis'].value) : 0;  \n" +
				" var alq_cofins = form['listaEntregamaterial['+index+'].cofins'] && form['listaEntregamaterial['+index+'].cofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].cofins'].value) : 0;  \n" +
				" var bc_cofins = form['listaEntregamaterial['+index+'].valorbccofins'] && form['listaEntregamaterial['+index+'].valorbccofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbccofins'].value) : 0; \n" +
				" var vr_cofins = form['listaEntregamaterial['+index+'].valorcofins'] && form['listaEntregamaterial['+index+'].valorcofins'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorcofins'].value) : 0;  \n" +
				" var alq_icms_st = form['listaEntregamaterial['+index+'].icmsst'] && form['listaEntregamaterial['+index+'].icmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].icmsst'].value) : 0;  \n" +
				" var mva = form['listaEntregamaterial['+index+'].valormva'] && form['listaEntregamaterial['+index+'].valormva'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valormva'].value) : 0;  \n" +
				" var reducao_icms_st = 0;  \n" +
				" var uf_icms_st = '" + (grupotributacao.getUficmsst() != null && grupotributacao.getUficmsst().getSigla() != null ? grupotributacao.getUficmsst().getSigla() : "" ) + "';  \n" +
				" var frete = form['listaEntregamaterial['+index+'].valorfrete'] && form['listaEntregamaterial['+index+'].valorfrete'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorfrete'].value) : 0;  \n" +
				" var desconto = form['listaEntregamaterial['+index+'].valordesconto'] && form['listaEntregamaterial['+index+'].valordesconto'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valordesconto'].value) : 0;  \n" +
				" var seguro = form['listaEntregamaterial['+index+'].valorseguro'] && form['listaEntregamaterial['+index+'].valorseguro'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorseguro'].value) : 0;  \n" +
				" var outras_despesas = form['listaEntregamaterial['+index+'].valoroutrasdespesas'] && form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valoroutrasdespesas'].value) : 0;  \n" + 
				" var bc_icms_st = " + (isFormulavalor ? " form['listaEntregamaterial['+index+'].valorbcicmsst'] && form['listaEntregamaterial['+index+'].valorbcicmsst'].value != '' ? $s.toFloat(form['listaEntregamaterial['+index+'].valorbcicmsst'].value) : 0 " : " 0 ") + "; \n" +
				" var msgErroCalculo = ''; \n";
		}else {
			return "";
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.GrupotributacaoDAO#findForBaseCalculoICMSSTJavascript(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public List<Grupotributacao> findForBaseCalculoICMSSTJavascript(String whereIn){
		return grupotributacaoDAO.findForBaseCalculoICMSSTJavascript(whereIn);
	}
	
	/**
	* M�todo que valida a formula de base de calculo de icms
	*
	* @param grupotributacao
	* @return
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	public String validaFormulaBCICMS(Grupotributacao grupotributacao) {
		String mensagemErro = "";
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcicms())){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, new Notafiscalprodutoitem(), grupotributacao, true, 1d, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcicms()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}
			
			if(resultado.isInfinite()){
				mensagemErro = "resultado inv�lido. (Infinite)";
			}
			if(resultado.isNaN()){
				mensagemErro = "resultado inv�lido. (NaN)";
			}
		}
		return mensagemErro;
	}
	
	public String validaFormulaBCICMSDestinatario(Grupotributacao grupotributacao) {
		String mensagemErro = "";
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcicmsdestinatario())){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, new Notafiscalprodutoitem(), grupotributacao, true, 1d, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMS(grupotributacao.getFormulabcicmsdestinatario()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}
			
			if(resultado.isInfinite()){
				mensagemErro = "resultado inv�lido. (Infinite)";
			}
			if(resultado.isNaN()){
				mensagemErro = "resultado inv�lido. (NaN)";
			}
		}
		return mensagemErro;
	}
	
	/**
	* M�todo que valida a formula de base de calculo de icmsst
	*
	* @param grupotributacao
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public String validaFormulaBCST(Grupotributacao grupotributacao) {
		String mensagemErro = "";
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcst())){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, new Notafiscalprodutoitem(), grupotributacao, true, 1d, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulabcst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}
			
			if(resultado.isInfinite()){
				mensagemErro = "resultado inv�lido. (Infinite)";
			}
			if(resultado.isNaN()){
				mensagemErro = "resultado inv�lido. (NaN)";
			}
		}
		return mensagemErro;
	}
	
	/**
	* M�todo que valida a formula de valor de icmsst
	*
	* @param grupotributacao
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public String validaFormulaVALORST(Grupotributacao grupotributacao) {
		String mensagemErro = "";
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulavalorst())){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, new Notafiscalprodutoitem(), grupotributacao, true, 1d, false);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaICMSST(grupotributacao.getFormulavalorst()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}

			if(resultado.isInfinite()){
				mensagemErro = "resultado inv�lido (Infinite).";
			}
			if(resultado.isNaN()){
				mensagemErro = "resultado inv�lido (NaN). Obs: � preciso tratar a divis�o por zero.";
			}
		}
		return mensagemErro;
	}
	
	/**
	* M�todo que valida formula de ipi
	*
	* @param grupotributacao
	* @return
	* @since 15/05/2017
	* @author Luiz Fernando
	*/
	public String validaFormulaBCIPI(Grupotributacao grupotributacao) {
		String mensagemErro = "";
		if(grupotributacao != null && StringUtils.isNotEmpty(grupotributacao.getFormulabcipi())){
			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			setVariaveisFormulaGrupotributacao(engine, new Notafiscalprodutoitem(), grupotributacao, true, 1d, true);
			
			Double resultado = 0d;
			try {
				Object obj = engine.eval(getStringForEvalFormulaIPI(grupotributacao.getFormulabcipi()));
				if(obj != null){
					String resultadoStr = obj.toString();
					resultado = new Double(resultadoStr);
				}
			} catch (Exception e) {
				mensagemErro = e.getMessage();
			}
			
			if(resultado.isInfinite()){
				mensagemErro = "resultado inv�lido. (Infinite)";
			}
			if(resultado.isNaN()){
				mensagemErro = "resultado inv�lido. (NaN)";
			}
		}
		return mensagemErro;
	}
	
	/**
	* M�todo que retorna a formula sem o $s. e com as fun��es de javacript (toFloat, toMoney)
	*
	* @param formula
	* @return
	* @since 04/01/2017
	* @author Luiz Fernando
	*/
	private String getStringForEvalFormulaICMS(String formula){
		return SinedUtil.getFuncoesSinedJs() + formula.replaceAll("\\$s\\.", "");
	}
	
	/**
	* M�todo que retorna a formula sem o $s. e com as fun��es de javacript (toFloat, toMoney)
	*
	* @param formula
	* @return
	* @since 20/01/2016
	* @author Luiz Fernando
	*/
	private String getStringForEvalFormulaICMSST(String formula){
		return SinedUtil.getFuncoesSinedJs() + formula.replaceAll("\\$s\\.", "");
	}
	
	/**
	* M�todo que retorna a formula sem o $s. e com as fun��es de javacript (toFloat, toMoney)
	*
	* @param formula
	* @return
	* @since 12/05/2017
	* @author Luiz Fernando
	*/
	private String getStringForEvalFormulaIPI(String formula){
		return SinedUtil.getFuncoesSinedJs() + formula.replaceAll("\\$s\\.", "");
	}
	
	/**
	 * Busca a aliquota a partir do controle e grupo de tributa��o
	 *
	 * @param faixaimpostocontrole
	 * @param grupotributacao
	 * @param empresa
	 * @param ufOrigem
	 * @param ufDestino
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	public Double getAliquotaByGrupotributacao(Faixaimpostocontrole faixaimpostocontrole, Grupotributacao grupotributacao, Empresa empresa, Cliente cliente, Uf ufOrigem, Uf ufDestino) {
		Money aliquota = new Money();
		
		try{
			boolean existImpostoGrupo = false;
			boolean impostoGrupotributacao = false;
			boolean buscaraliquotafixafaixaimposto = false;
			
			Grupotributacaoimposto imposto = grupotributacaoimpostoService.findByGrupotributacaoControle(grupotributacao, faixaimpostocontrole);
			if (imposto != null){
				existImpostoGrupo = true;
				buscaraliquotafixafaixaimposto = imposto.getBuscaraliquotafixafaixaimposto() != null ? imposto.getBuscaraliquotafixafaixaimposto() : false;
				if (imposto.getAliquotafixa() != null){
					aliquota = imposto.getAliquotafixa();
					impostoGrupotributacao = true;
				}
			}
			if(existImpostoGrupo && (!impostoGrupotributacao || buscaraliquotafixafaixaimposto)){
				Faixaimposto faixaimposto = faixaimpostoService.findByControleAndUf(empresa, cliente, faixaimpostocontrole, ufOrigem, ufDestino);
				if (faixaimposto != null && faixaimposto.getValoraliquota() != null) {
					aliquota = faixaimposto.getValoraliquota();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return aliquota != null ? aliquota.getValue().doubleValue() : 0.0;
	}
	
	@Override
	public Resource generateCSV(FiltroListagem filtro, String[] fields) {
		String cabecalho = "\"Nome\";\"Utiliza��o\";\"CFOP\";\"Produto tributado pelo:\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Grupotributacao> listagemResult = findForExportacao(filtro);
		
		while (listagemResult != null){
			List<Grupotributacao> listagem = listagemResult.list();
			
			if (listagem.isEmpty())
				break;

			for (Grupotributacao bean : listagem){
				csv.append("\"" + (bean.getNome() != null ? bean.getNome() : "") + "\";");
				csv.append("\"" + (bean.getOperacao() != null ? bean.getOperacao() : "") + "\";");
				csv.append("\"" + (bean.getCfopgrupo() != null ? bean.getCfopgrupo().getDescricaoCodigo() : "") + "\";");
				
				if(bean.getTributadoicms() != null && bean.getTributadoicms()) {
					csv.append("\"ICMS\";");
				} else {
					csv.append("\"ISSQN\";");
				}
				
				csv.append("\n");
			}
			
			filtro.setCurrentPage(filtro.getCurrentPage() + 1);
			if (filtro.getCurrentPage() < filtro.getNumberOfPages())
				listagemResult = findForExportacao(filtro);
			else
				listagemResult = null;
		}
		
		Resource resource = new Resource("text/csv", getNomeArquivoExportacao(), csv.toString().getBytes());
		return resource;
	}
	public Boolean conferirExisteGrupoTributacaoImportacaoSped(Grupotributacao grupoTributacao) {
		List<Grupotributacao> grupoTributacaoList = grupotributacaoDAO.findGrupoTributacaoImportacaoSped(grupoTributacao);
		
		if (grupoTributacaoList != null && grupoTributacaoList.size() > 0) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	public boolean calculaDifalNaVenda(Grupotributacao bean){
		if(Util.objects.isPersistent(bean)){
			Grupotributacao grupoTrib = this.load(bean, "grupotributacao.cdgrupotributacao, grupotributacao.calcularDifal");
			return Util.objects.isPersistent(grupoTrib) && Boolean.TRUE.equals(grupoTrib.getCalcularDifal());
		}
		return false;
	}
}
