package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.dao.CargomaterialsegurancaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargomaterialsegurancaService extends GenericService<Cargomaterialseguranca> {
	
	private CargomaterialsegurancaDAO cargomaterialsegurancaDAO;
	private ColaboradorcargoService colaboradorcargoService;
	
	public void setCargomaterialsegurancaDAO(CargomaterialsegurancaDAO cargomaterialsegurancaDAO) {this.cargomaterialsegurancaDAO = cargomaterialsegurancaDAO;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}


	/***
	 * Busca a lista de material de seguranša dos cargos presentes em listaCargo
	 * 
	 * @see br.com.linkcom.sined.geral.dao.CargomaterialsegurancaDAO#findByCargo(Cargo)
	 * 
	 * @param listaCargo
	 * @return List<Cargomaterialseguranca>
	 * 
	 * @author Rodrigo Alvarenga
	 */
	public List<Cargomaterialseguranca> findByCargo(List<Cargo> listaCargo) {
		return cargomaterialsegurancaDAO.findByCargo(listaCargo);
	}
	
	public List<Cargomaterialseguranca> obtemListaCargomaterialseguranca(Colaborador colaborador){
		Colaboradorcargo colaboradorcargo = colaboradorcargoService.carregaDadosCargo(colaborador);
		List<Cargo> listaCargo = new ArrayList<Cargo>();
		listaCargo.add(colaboradorcargo.getCargo());
		return this.findByCargo(listaCargo);
	}
}
