package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.dao.ProjetodespesaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProjetodespesaService extends GenericService<Projetodespesa>{
	
	private ProjetodespesaDAO projetodespesaDAO;
	
	public void setProjetodespesaDAO(ProjetodespesaDAO projetodespesaDAO){
		this.projetodespesaDAO = projetodespesaDAO;
	}
	
	private static ProjetodespesaService instance;
	
	public static ProjetodespesaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetodespesaService.class);
		}
		return instance;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param projeto
	 * @return
	 * @author Marden Silva
	 */

	public List<Projetodespesa> findByProjetoData(Projeto projeto, Date dataAte) {
		return projetodespesaDAO.findByProjetoData(projeto, dataAte);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Projetodespesa> findAllForAtualizarRateio(){
		return projetodespesaDAO.findAllForAtualizarRateio();
	}
	
	/**
	* M�todo que agrupa as despesas do projeto de acordo com a conta gerencial
	*
	* @param listaTodosItens
	* @since 19/06/2015
	* @author Luiz Fernando
	*/
	public void agruparListaPD(List<Projetodespesa> listaTodosItens) {
		Projetodespesa ibean;
		Projetodespesa jbean;
		for (int i = 0; i < listaTodosItens.size(); i++) {
			ibean = listaTodosItens.get(i);
			for (int j = 0; j < listaTodosItens.size(); j++) {
				jbean = listaTodosItens.get(j);
				if (j != i && 
						ibean.getContagerencial() != null && jbean.getContagerencial() != null &&
						ibean.getContagerencial().getCdcontagerencial().equals(jbean.getContagerencial().getCdcontagerencial()) &&
						((ibean.getValorhora() != null && jbean.getValorhora() != null && 
						ibean.getValorhora().compareTo(jbean.getValorhora()) == 0) ||
						(ibean.getValorhora() == null || jbean.getValorhora() == null))
						) {
						ibean.setValortotal((ibean.getValortotal().add(jbean.getValortotal())));
						listaTodosItens.remove(j);
						j--;
				}
			}
		}
	}
}
