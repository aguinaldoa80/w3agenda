package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.dao.EnvioemailDAO;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.MailingEnviadosFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EnvioemailService extends GenericService<Envioemail>{
		
	private EnvioemailDAO envioemailDAO;
	public void setEnvioemailDAO(EnvioemailDAO envioemailDAO) {
		this.envioemailDAO = envioemailDAO;
	}
	
	/**
	 *  
	 * @param filtro
	 * @author Thiago Clemente
	 */
	public List<Envioemail> findForMailingEnviados(MailingEnviadosFiltro filtro){
		return envioemailDAO.findForMailingEnviados(filtro);
	}
	
	private static EnvioemailService instance;
	public static EnvioemailService getInstance(){
		if (instance==null){
			instance = Neo.getObject(EnvioemailService.class);
		}
		return instance;
	}

	/**
	 * Registra um envio de email.
	 * 
	 * @param emailRemetente
	 * @param assunto
	 * @param mensagem
	 * @param tipo
	 * @param pessoa 
	 * @param email 
	 * @param nomeContato 
	 * @param listaItens
	 * @param emailManager
	 */
	public Envioemail registrarEnvio(String emailRemetente, String assunto, String mensagem, Envioemailtipo tipo, Pessoa pessoa, String email, String nomeContato, EmailManager emailManager) {
		Envioemail envioemail = new Envioemail();
		
		if (email != null){		
			List<Envioemailitem> listaItens = new ArrayList<Envioemailitem>();
			listaItens.add(new Envioemailitem(pessoa, email, nomeContato));
	
			envioemail.setDtenvio(new Timestamp(System.currentTimeMillis()));
			envioemail.setRemetente(emailRemetente);
			envioemail.setAssunto(assunto);
			envioemail.setMensagem(mensagem);
			envioemail.setEnvioemailtipo(tipo);
			envioemail.setListaEnvioemailitem(listaItens);
			this.saveOrUpdate(envioemail);
			
			emailManager.setEmailItemId(listaItens.get(0).getCdenvioemailitem());
		}
			
		return envioemail;
	}		
	
	/**
	 * Registra um envio de email.
	 * 
	 * @param emailRemetente
	 * @param assunto
	 * @param mensagem
	 * @param tipo
	 */
	public Envioemail registrarEnvio(String emailRemetente, String assunto, String mensagem, Envioemailtipo tipo) {
		
		Envioemail envioemail = new Envioemail();
		envioemail.setDtenvio(new Timestamp(System.currentTimeMillis()));
		envioemail.setDtaltera(new Timestamp(System.currentTimeMillis()));
		envioemail.setRemetente(emailRemetente);
		envioemail.setAssunto(assunto);
		envioemail.setMensagem(mensagem);
		envioemail.setEnvioemailtipo(tipo);
		this.saveOrUpdate(envioemail);
		
		return envioemail;
	}
	
	public Envioemail registrarEnvioWithoutLog(String emailRemetente, String assunto, String mensagem, Envioemailtipo tipo) {
		
		Envioemail envioemail = new Envioemail();
		envioemail.setDtenvio(new Timestamp(System.currentTimeMillis()));
		envioemail.setRemetente(emailRemetente);
		envioemail.setAssunto(assunto);
		envioemail.setMensagem(mensagem);
		envioemail.setEnvioemailtipo(tipo);
		
		this.saveOrUpdateWithoutLog(envioemail);
		
		return envioemail;
	}
	
	public void updateStatusemail(Envioemail envioemail, EmailStatusEnum emailstatusenum){
		envioemailDAO.updateStatusemail(envioemail, emailstatusenum);
	}
	
	public void updateUltimaverificacao(Envioemail envioemail, Timestamp ultimaverificacao){
		envioemailDAO.updateUltimaverificacao(envioemail, ultimaverificacao);
	}
}