package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Materialvenda;
import br.com.linkcom.sined.geral.dao.MaterialvendaDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.MaterialvendaOfflineJSON;

public class MaterialvendaService extends GenericService<Materialvenda> {

	private MaterialvendaDAO materialvendaDAO;
	
	public void setMaterialvendaDAO(MaterialvendaDAO materialvendaDAO) {
		this.materialvendaDAO = materialvendaDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MaterialvendaDAO#findForSugestao(String codigos)
	 *
	 * @param codigos
	 * @return
	 * @since 01/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Sugestaovenda> findForSugestao(String codigos) {
		return materialvendaDAO.findForSugestao(codigos);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MaterialvendaDAO#findForPVOffline()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<MaterialvendaOfflineJSON> findForPVOffline() {
		List<MaterialvendaOfflineJSON> lista = new ArrayList<MaterialvendaOfflineJSON>();
		for(Materialvenda mv : materialvendaDAO.findForPVOffline()){
			lista.add(new MaterialvendaOfflineJSON(mv));
		}
		return lista;
	}

}
