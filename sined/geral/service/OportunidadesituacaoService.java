package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO;
import br.com.linkcom.sined.util.RedimensionaImagem;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OportunidadesituacaoService extends GenericService<Oportunidadesituacao>{
	private OportunidadesituacaoDAO oportunidadesituacaoDAO;
	
	
	public void setOportunidadesituacaoDAO(OportunidadesituacaoDAO oportunidadesituacaoDAO) {
		this.oportunidadesituacaoDAO = oportunidadesituacaoDAO;
	}
	
	/* singleton */
	private static OportunidadesituacaoService instance;
	
	public static OportunidadesituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(OportunidadesituacaoService.class);
		}
		return instance;
	}
	/**
	 * Referencia o DAO
	 * @author Taidson Santos
	 * @since 30/03/2010
	 * @param filtro
	 */
	public List<Oportunidadesituacao> populaComboSituacao (Oportunidade oportunidade){
		return oportunidadesituacaoDAO.populaComboSituacao(oportunidade);
	}
	
	@Override
	public void saveOrUpdate(Oportunidadesituacao bean) {		
		//carega o arquivo
		if(bean.getImagem() != null && bean.getImagem().getContent() != null){
			RedimensionaImagem redimensionaImagem = new RedimensionaImagem();
			try {
				Arquivo arquivoImgem = redimensionaImagem.redimensionaImagem(bean.getImagem(),25,25);
				if (arquivoImgem != null) bean.setImagem(arquivoImgem);
			} catch (IOException e) {
				e.printStackTrace();
			}catch(NullPointerException nullPointer){
				throw new SinedException("Imagem inv�lida.");
			}
		}
		super.saveOrUpdate(bean);
	}

	public Oportunidadesituacao carregaImagemarquivo(Oportunidadesituacao oportunidadesituacao) {
		return oportunidadesituacaoDAO.carregaImagemarquivo(oportunidadesituacao);
	}

	/**
	* M�todo com refer�ncia no DAO
	* Busca as situa��es marcadas para aparecer na listagem
	*
	* @see	br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO#findDefaultlistagem()
	* 
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Oportunidadesituacao> findDefaultlistagem() {
		return oportunidadesituacaoDAO.findDefaultlistagem();
	}
	/**
	* M�todo com refer�ncia no DAO
	* Busca a situa��o marcada como final
	* 
	* 
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaofinal() {
		return oportunidadesituacaoDAO.findSituacaofinal();
	}
	/**
	* M�todo com refer�ncia no DAO
	* Busca a situa��o marcada como antes da final
	*
	* @see	br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO#findSituacaoantesfinal()
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaoantesfinal() {
		return oportunidadesituacaoDAO.findSituacaoantesfinal();
	}
	/**
	* M�todo com refer�ncia no DAO
	* Busca a situa��o marcada como inicial
	*
	* @see	br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO#findSituacaoinicial()
	* 
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaoinicial() {
		return oportunidadesituacaoDAO.findSituacaoinicial();
	}
	/**
	* M�todo com refer�ncia no DAO
	* Busca a situa��o marcada como canelada
	* 
	* @see	br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO#findSituacaocancelada()
	*
	* @return
	* @since Sep 23, 2011
	* @author Luiz Fernando F Silva
	*/
	public Oportunidadesituacao findSituacaocancelada() {
		return oportunidadesituacaoDAO.findSituacaocancelada();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.OportunidadesituacaoDAO#existeOportunidadesituacao(Integer cdoportunidadesituacao, String situacao)
	 *
	 * @param cdoportunidadesituacao
	 * @param situacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeOportunidadesituacao(Integer cdoportunidadesituacao, String situacao){
		return oportunidadesituacaoDAO.existeOportunidadesituacao(cdoportunidadesituacao, situacao);
	}

}
