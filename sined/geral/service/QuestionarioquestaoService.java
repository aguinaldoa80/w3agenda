package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.geral.dao.QuestionarioquestaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class QuestionarioquestaoService extends GenericService<Questionarioquestao>{
	
	private QuestionarioquestaoDAO questionarioquestaoDAO;
	
	public void setQuestionarioquestaoDAO(QuestionarioquestaoDAO questionarioquestaoDAO) {
		this.questionarioquestaoDAO = questionarioquestaoDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 06/03/2012
	 * @param bean
	 * @return
	 */
	public Questionarioquestao carregarQuestionarioQuestao(Questionarioquestao bean){
		return questionarioquestaoDAO.carregarQuestionarioQuestao(bean);
	}
	
	public List<Questionarioquestao> loadQuestoes(Questionario questionario){
		return questionarioquestaoDAO.loadQuestoes(questionario);
	}
	
	public List<Questionarioquestao> carregarQuestionarioQuestaoForGrupoquestionario(Grupoquestionario grupoquestionario){
		return questionarioquestaoDAO.carregarQuestionarioQuestaoForGrupoquestionario(grupoquestionario);
	}
	
	public void findForDelete(String whereIn, Grupoquestionario grupoquestionario){
		questionarioquestaoDAO.findForDelete(whereIn, grupoquestionario);
	}
	
	public List<Questionarioquestao> carregarListaQuestionarioQuestao(String whereIn){
		return questionarioquestaoDAO.carregarListaQuestionarioQuestao(whereIn);
	}
}
