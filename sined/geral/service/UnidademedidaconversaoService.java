package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.dao.UnidademedidaconversaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class UnidademedidaconversaoService extends GenericService<Unidademedidaconversao> {

	private UnidademedidaconversaoDAO unidademedidaconversaoDAO;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;

	public void setUnidademedidaconversaoDAO(UnidademedidaconversaoDAO unidademedidaconversaoDAO) {
		this.unidademedidaconversaoDAO = unidademedidaconversaoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 * @since 30/12/2010
	 */
	public List<Unidademedidaconversao> conversoesByUnidademedida(Unidademedida unidademedida){
		return unidademedidaconversaoDAO.conversoesByUnidademedida(unidademedida);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param unidademedida
	 * @return
	 * @author Taidson
	 * @since 06/01/2011
	 */
	public List<Unidademedidaconversao> unidadeAndUnidadeRelacionada(Unidademedida unidademedida){
		return unidademedidaconversaoDAO.unidadeAndUnidadeRelacionada(unidademedida);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.UnidademedidaconversaoDAO#findUnidademedidarelacionadaByUnidademedida(Unidademedida unidademedida)
	 *
	 * @param unidademedida
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedidaconversao> findUnidademedidarelacionadaByUnidademedida(Unidademedida unidademedida) {
		return unidademedidaconversaoDAO.findUnidademedidarelacionadaByUnidademedida(unidademedida);
	}
	
	/**
	 * M�todo que cria uma lista de unidade de medida que tenham rela��o de convers�o
	 *
	 * @see br.com.linkcom.sined.geral.service.UnidademedidaconversaoService#findUnidademedidarelacionadaByUnidademedida(Unidademedida unidademedida)
	 *
	 * @param unidademedida
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Unidademedida> criarListaUnidademedidaRelacionadas(Unidademedida unidademedida, Material material) {
		List<Unidademedida> lista = new ArrayList<Unidademedida>();
		List<Unidademedidaconversao> listaUnidademedidaconversao = new ArrayList<Unidademedidaconversao>();
		Boolean existListaMaterialunidademedida = Boolean.FALSE;
		
		if(unidademedida != null && unidademedida.getCdunidademedida() != null){
			unidademedida = unidademedidaService.load(unidademedida, "unidademedida.cdunidademedida, unidademedida.nome");
		}
		
		if(unidademedida != null && unidademedida.getCdunidademedida() != null && material != null && material.getCdmaterial() != null){
			Material m = materialService.findListaMaterialunidademedida(material);
			if(m != null && m.getListaMaterialunidademedida() != null && !m.getListaMaterialunidademedida().isEmpty()){
				existListaMaterialunidademedida = Boolean.TRUE;
				lista.add(unidademedida);
				for(Materialunidademedida materialunidademedida : m.getListaMaterialunidademedida()){
					Unidademedida unidademedidaMaterial = materialunidademedida.getUnidademedida();
					if(unidademedidaMaterial != null){
						unidademedidaMaterial.setPrioritariacompra(materialunidademedida.getPrioridadecompra() != null && materialunidademedida.getPrioridadecompra());
						lista.add(unidademedidaMaterial);
					}
				}
			}
		}
		
		if(!existListaMaterialunidademedida && unidademedida != null && unidademedida.getCdunidademedida() != null){
			lista.add(unidademedida);
			listaUnidademedidaconversao = this.findUnidademedidarelacionadaByUnidademedida(unidademedida);
			if(listaUnidademedidaconversao != null && !listaUnidademedidaconversao.isEmpty()){
				for(Unidademedidaconversao unidademedidaconversao : listaUnidademedidaconversao){
					if(unidademedidaconversao.getUnidademedidarelacionada() != null && 
							unidademedidaconversao.getUnidademedidarelacionada().getCdunidademedida() != null){
						lista.add(unidademedidaconversao.getUnidademedidarelacionada());
					}
				}
			}
		}
		
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.UnidademedidaconversaoDAO#findForValidacaoConversao(Unidademedida unidademedida, Unidademedida unidademedidarelacionada)
	*
	* @param unidademedida
	* @param unidademedidarelacionada
	* @return
	* @since 12/04/2016
	* @author Luiz Fernando
	*/
	public List<Unidademedidaconversao> findForValidacaoConversao(Unidademedida unidademedida, Unidademedida unidademedidarelacionada){
		return unidademedidaconversaoDAO.findForValidacaoConversao(unidademedida, unidademedidarelacionada);
	}
}
