package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoprocesso;
import br.com.linkcom.sined.geral.bean.Documentoprocessomaterial;
import br.com.linkcom.sined.geral.dao.DocumentoprocessomaterialDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoprocessomaterialService extends GenericService<Documentoprocessomaterial>{

	private DocumentoprocessomaterialDAO documentoprocessomaterialDAO;
	
	public void setDocumentoprocessomaterialDAO(DocumentoprocessomaterialDAO documentoprocessomaterialDAO) {
		this.documentoprocessomaterialDAO = documentoprocessomaterialDAO;
	}

	public void saveOrUpdateListaDocumentoprocessomaterial(Documentoprocesso documentoprocesso){
		if(documentoprocesso == null || documentoprocesso.getCddocumentoprocesso() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser nula.");
		}
		if(documentoprocesso.getListadocumentoprocessomaterial() == null){
			return;
		}
		
		List<Documentoprocessomaterial> listaAtual = this.findByDocumentoprocesso(documentoprocesso);
		List<Documentoprocessomaterial> listaDelete = new ArrayList<Documentoprocessomaterial>();
		List<Documentoprocessomaterial> lista = documentoprocesso.getListadocumentoprocessomaterial();
		
		if(listaAtual != null && !listaAtual.isEmpty()){
			for (Documentoprocessomaterial documentoprocessoprojeto : listaAtual) {
				if(documentoprocessoprojeto.getCddocumentoprocessomaterial() != null && !lista.contains(documentoprocessoprojeto)){
					listaDelete.add(documentoprocessoprojeto);
				}
			}
			
			for (Documentoprocessomaterial documentoprocessomaterial : listaDelete) {
				this.delete(documentoprocessomaterial);
			}
		}
		
		for(Documentoprocessomaterial doc : lista){
			doc.setDocumentoprocesso(new Documentoprocesso(documentoprocesso.getCddocumentoprocesso()));
			this.saveOrUpdateNoUseTransaction(doc);
		}
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoprocessomaterialService#findByDocumentoprocesso(Documentoprocesso documentoprocesso)
	 *
	 * @param documentoprocesso
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentoprocessomaterial> findByDocumentoprocesso(Documentoprocesso documentoprocesso) {
		return documentoprocessomaterialDAO.findByDocumentoprocesso(documentoprocesso);
	}

}
