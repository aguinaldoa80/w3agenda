package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Comunicado;
import br.com.linkcom.sined.geral.bean.Comunicadodestinatario;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailitem;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.dao.ComunicadoDAO;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.EventoservicointernoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ComunicadoService extends GenericService<Comunicado>{
	
	private ComunicadoDAO comunicadoDao;
	private ColaboradorcargoService colaboradorcargoService;
	private EmpresaService empresaService;
	private EnvioemailService envioemailService;
	private EnvioemailitemService envioemailitemService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setColaboradorcargoService(
			ColaboradorcargoService colaboradorcargoService) {
		this.colaboradorcargoService = colaboradorcargoService;
	}
	public void setComunicadoDao(ComunicadoDAO comunicadoDao) {
		this.comunicadoDao = comunicadoDao;
	}

	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}
	
	public void setEnvioemailitemService(EnvioemailitemService envioemailitemService) {
		this.envioemailitemService = envioemailitemService;
	}

	/**
	 * Fun��o para a gera��o do relat�rio de eventos
	 * @param filtro
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.ComunicadoDAO#findForFiltroEvento(EventoservicointernoFiltro)
	 */
	public List<Comunicado> findForFiltroEvento(EventoservicointernoFiltro filtro){
		return comunicadoDao.findForFiltroEvento(filtro);
	}
	
	/**
	 * Envia emails
	 * @param bean
	 * @author C�ntia
	 * @throws Exception 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findCargoAtual(Colaborador)
	 */
	public void enviaEmail(Comunicado bean) throws Exception{
		String assunto = bean.getAssunto();
		Empresa empresa = empresaService.loadPrincipal();
		
		if(! bean.getListadestinatario().isEmpty()){
			Colaboradorcargo colaboradorCargo = colaboradorcargoService.findCargoAtual(bean.getRemetente());
			String template = null;
			try{
				template = new TemplateManager("/WEB-INF/template/templateMensagemEmailComunicado.tpl")
					.assign("descricao", bean.getDescricao() != null ? bean.getDescricao().replace("\n", "<br>") : "")
					.assign("remetente", bean.getRemetente().getNome())
					.assign("cargo", colaboradorCargo != null && colaboradorCargo.getCargo() != null ? colaboradorCargo.getCargo().getNome() : "")
					.assign("departamento", colaboradorCargo != null && colaboradorCargo.getDepartamento() != null ? colaboradorCargo.getDepartamento().getNome() : "")
					.assign("link", SinedUtil.getUrlWithContext() + "/servicointerno/crud/Comunicado?ACAO=consultar&cdcomunicado=" + bean.getCdcomunicado())
					.assign("codigo", bean.getCdcomunicado().toString())  
					.assign("nomeSistema", SinedUtil.isSined() ? "SINED" : (SinedUtil.isBriefcase() ? "W3ERP - BRIEFCASE" : "W3ERP"))
					.getTemplate();
			} catch (IOException e) {
				throw new SinedException("Erro no processamento do template.",e);
			}

			Envioemail envioemail = envioemailService.registrarEnvio(
					empresa.getEmail(),
					assunto,
					template,
					Envioemailtipo.COMUNICADO);
			
			if (bean.getListadestinatario()!=null){
				for(Comunicadodestinatario destinatario : bean.getListadestinatario()){
					if(destinatario.getEmail() != null && !destinatario.getEmail().equals("")){
						
						Envioemailitem envioemailitem = new Envioemailitem();
						envioemailitem.setPessoa(destinatario.getColaborador());
						envioemailitem.setEmail(destinatario.getEmail());
						envioemailitem.setNomecontato(destinatario.getColaborador().getNome());
						envioemailitem.setEnvioemail(envioemail);
						envioemailitemService.saveOrUpdate(envioemailitem);
						
						
						EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
						email.setFrom(empresa.getEmail());
						email.setSubject(assunto);
						email.setTo(destinatario.getEmail());
						email.setEmailItemId(envioemailitem.getCdenvioemailitem());
						
						email.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinatario.getEmail()));
						
						email.sendMessage();
					}
				}
			}
			
		} else throw new SinedException("N�o foi enviado nenhum e-mail.");
	}

}
