package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import net.sf.json.JSON;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentoreferenciado;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofrete;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregadocumentoreferenciado;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregamateriallote;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Formulacusto;
import br.com.linkcom.sined.geral.bean.Formulacustoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.geral.bean.NotaHistorico;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcompraentregamaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Referencia;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.FreteVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.MaterialVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.NotaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.formulacusto.OutrosVO;
import br.com.linkcom.sined.geral.bean.enumeration.Documentoreferenciadoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentoemitente;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Retencao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotitulocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.view.Vwentregadocumentoimposto;
import br.com.linkcom.sined.geral.dao.EntradafiscalDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialgradeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Rastro;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ConferenciaDadosFornecedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeDuplicataBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemAssociadoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportacaoXmlNfeItemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.ApuracaoimpostoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.EntradafiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SintegraFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedpiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoipiFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaopiscofinsFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.SagefiscalFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Entradafiscal;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntradafiscalService extends GenericService<Entregadocumento> {

	private EntradafiscalDAO entradafiscalDAO;
	private RateioService rateioService;
	private MaterialService materialService;
	private CfopService cfopService;
	private ProdutoService produtoService;
	private EmpresaService empresaService;
	private EntregapagamentoService entregapagamentoService;
	private EntregamaterialService entregamaterialService;
	private DocumentoService documentoService;
	private OrdemcompramaterialService ordemcompramaterialService;
	private OrdemcompraService ordemcompraService;
	private RateioitemService rateioitemService;
	private ContapagarService contapagarService;
	private DocumentoorigemService documentoorigemService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private UfService ufService;
	private UnidademedidaService unidademedidaService;
	private ProjetoService projetoService;
	private EnderecoService enderecoService;
	private FornecedorService fornecedorService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private GrupotributacaoService grupotributacaoService;
	private ParametrogeralService parametrogeralService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private EntregadocumentofreteService entregadocumentofreteService;
	private EntregadocumentofreterateioService entregadocumentofreterateioService;
	private DocumentohistoricoService documentohistoricoService;
	private FormulacustoService formulacustoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private CategoriaService categoriaService;
	private ModelodocumentofiscalService modelodocumentofiscalService;
	private NotaHistoricoService notaHistoricoService;
	private PrazopagamentoService prazopagamentoService;
	private LoteestoqueService loteestoqueService;
	private EntregamaterialloteService entregamaterialloteService;
	private DocumentoreferenciadoService documentoreferenciadoService;
	private PedidovendamaterialService pedidovendamaterialService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private CentrocustoService centrocustoService;
	private HistoricooperacaoService historicooperacaoService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private DocumentotipoService documentotipoService;
	private EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService;
	private ColaboradorFornecedorService colaboradorFornecedorService;
	private PedidovendaService pedidovendaService;
	private ColetaService coletaService;
	private FornecimentoService fornecimentoService;
	private LocalarmazenagemService localarmazenagemService;
	private ContagerencialService contagerencialService;
	
	public void setModelodocumentofiscalService(
			ModelodocumentofiscalService modelodocumentofiscalService) {
		this.modelodocumentofiscalService = modelodocumentofiscalService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setEntradafiscalDAO(EntradafiscalDAO entradafiscalDAO) {
		this.entradafiscalDAO = entradafiscalDAO;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}	
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEntregapagamentoService(EntregapagamentoService entregapagamentoService) {
		this.entregapagamentoService = entregapagamentoService;
	}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {
		this.entregamaterialService = entregamaterialService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setOrdemcompramaterialService(OrdemcompramaterialService ordemcompramaterialService) {
		this.ordemcompramaterialService = ordemcompramaterialService;
	}
	public void setOrdemcompraService(OrdemcompraService ordemcompraService) {
		this.ordemcompraService = ordemcompraService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setEntregadocumentohistoricoService(EntregadocumentohistoricoService entregadocumentohistoricoService) {
		this.entregadocumentohistoricoService = entregadocumentohistoricoService;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {
		this.naturezaoperacaocfopService = naturezaoperacaocfopService;
	}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {
		this.grupotributacaoService = grupotributacaoService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setEntregadocumentofreteService(EntregadocumentofreteService entregadocumentofreteService) {
		this.entregadocumentofreteService = entregadocumentofreteService;
	}
	public void setEntregadocumentofreterateioService(EntregadocumentofreterateioService entregadocumentofreterateioService) {
		this.entregadocumentofreterateioService = entregadocumentofreterateioService;
	}
	public void setDocumentohistoricoService( DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setFormulacustoService(FormulacustoService formulacustoService) {
		this.formulacustoService = formulacustoService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) {
		this.notaHistoricoService = notaHistoricoService;
	}
	public void setPrazopagamentoService(PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}
	public void setEntregamaterialloteService(EntregamaterialloteService entregamaterialloteService) {
		this.entregamaterialloteService = entregamaterialloteService;
	}
	public void setDocumentoreferenciadoService(DocumentoreferenciadoService documentoreferenciadoService) {
		this.documentoreferenciadoService = documentoreferenciadoService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	public void setHistoricooperacaoService(HistoricooperacaoService historicooperacaoService) {
		this.historicooperacaoService = historicooperacaoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {
		this.materialCustoEmpresaService = materialCustoEmpresaService;
	}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {
		this.documentotipoService = documentotipoService;
	}
	public void setEntregaDocumentoApropriacaoService(EntregaDocumentoApropriacaoService entregaDocumentoApropriacaoService) {
		this.entregaDocumentoApropriacaoService = entregaDocumentoApropriacaoService;
	}
	public void setColaboradorFornecedorService(ColaboradorFornecedorService colaboradorFornecedorService) {
		this.colaboradorFornecedorService = colaboradorFornecedorService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setContagerencialService(ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	@Override
	public void saveOrUpdate(final Entregadocumento bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				saveOrUpdateNoUseTransaction(bean);
				
				if(bean.getListaEntregamaterial() != null && !bean.getListaEntregamaterial().isEmpty()){
					for (Entregamaterial entregamaterial : bean.getListaEntregamaterial()){ 
						entregamaterialService.saveOrUpdateNoUseTransaction(entregamaterial);
					}
				}						
				
				return status;
			}
		});
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findEntregadocumentoByEntrega(Entrega entrega) {
		return entradafiscalDAO.findEntregadocumentoByEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vwentregadocumentoimposto> criaListaImposto(Entregadocumento entregadocumento){
		List<Vwentregadocumentoimposto> lista = new ArrayList<Vwentregadocumentoimposto>();
		if(entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
			lista = getListaVwentregadocumentoimpostogrupadaCFOP(entregadocumento.getListaEntregamaterial());			
		}		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param listaEntregamaterial
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vwentregadocumentoimposto> getListaVwentregadocumentoimpostogrupadaCFOP(Set<Entregamaterial> listaEntregamaterial){
		List<Vwentregadocumentoimposto> listaAgrupada = new ArrayList<Vwentregadocumentoimposto>();
		Vwentregadocumentoimposto imposto;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				imposto = new Vwentregadocumentoimposto(em.getCfop(), 
						em.getValorbcicms(), em.getIcms(), em.getValoricms(),
						em.getValorbcicmsst(), em.getIcmsst(), em.getValoricmsst(),
						em.getValorbcpis(), em.getPis(), em.getValorpis(),
						em.getValorbcipi(), em.getIpi(), em.getValoripi(),
						em.getValorbccofins(), em.getCofins(), em.getValorcofins(),
						em.getValorbciss(), em.getIss(), em.getValoriss(),
						em.getValorir(), em.getValorcsll(),
						em.getValorinss(), em.getValorimpostoimportacao(), 
						em.getValorbcfcp(), em.getFcp(), em.getValorfcp(),
						em.getValorbcfcpst(), em.getFcpst(), em.getValorfcpst(),
						em.getValorIcmsDesonerado()
						);
				
				adicionaImpostoCFOP(listaAgrupada, imposto);
			}
		}
		return listaAgrupada;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param listaAgrupada
	 * @param vwentregadocumentoimposto
	 * @author Luiz Fernando
	 */
	private void adicionaImpostoCFOP(List<Vwentregadocumentoimposto> listaAgrupada, Vwentregadocumentoimposto vwentregadocumentoimposto) {		
		boolean exist = false;
		if(listaAgrupada != null && !listaAgrupada.isEmpty() && vwentregadocumentoimposto != null){
			for(Vwentregadocumentoimposto vwedi : listaAgrupada){
				if(vwentregadocumentoimposto != null){
					if(vwentregadocumentoimposto.getCdcfop() != null){
						if(vwedi.getCdcfop() != null && vwentregadocumentoimposto.getCdcfop().equals(vwedi.getCdcfop())){
							exist = true;
						}
					}else if(vwedi.getCdcfop() == null){
						exist = true;					
						
					}
					if(exist){
						if(vwedi.getBcicms() != null){
							vwedi.setBcicms(vwedi.getBcicms().add(vwentregadocumentoimposto.getBcicms()));
						}else {
							vwedi.setBcicms(vwentregadocumentoimposto.getBcicms());
						}
						if(vwedi.getBcicmsst() != null){
							vwedi.setBcicmsst(vwedi.getBcicmsst().add(vwentregadocumentoimposto.getBcicmsst()));
						}else {
							vwedi.setBcicmsst(vwentregadocumentoimposto.getBcicmsst());
						}
						if(vwedi.getBcfcp() != null){
							vwedi.setBcfcp(vwedi.getBcfcp().add(vwentregadocumentoimposto.getBcfcp()));
						}else {
							vwedi.setBcfcp(vwentregadocumentoimposto.getBcfcp());
						}
						if(vwedi.getBcfcpst() != null){
							vwedi.setBcfcpst(vwedi.getBcfcpst().add(vwentregadocumentoimposto.getBcfcpst()));
						}else {
							vwedi.setBcfcpst(vwentregadocumentoimposto.getBcfcpst());
						}
						if(vwedi.getBcpis() != null){
							vwedi.setBcpis(vwedi.getBcpis().add(vwentregadocumentoimposto.getBcpis()));
						}else {
							vwedi.setBcpis(vwentregadocumentoimposto.getBcpis());
						}
						if(vwedi.getBcipi() != null){
							vwedi.setBcipi(vwedi.getBcipi().add(vwentregadocumentoimposto.getBcipi()));
						}else {
							vwedi.setBcipi(vwentregadocumentoimposto.getBcipi());
						}
						if(vwedi.getBccofins() != null){
							vwedi.setBccofins(vwedi.getBccofins().add(vwentregadocumentoimposto.getBccofins()));
						}else {
							vwedi.setBccofins(vwentregadocumentoimposto.getBccofins());
						}
						if(vwedi.getBciss() != null){
							vwedi.setBciss(vwedi.getBciss().add(vwentregadocumentoimposto.getBciss()));
						}else {
							vwedi.setBciss(vwentregadocumentoimposto.getBciss());
						}
						
						
						if(vwedi.getValoricms() != null){
							vwedi.setValoricms(vwedi.getValoricms().add(vwentregadocumentoimposto.getValoricms()));
						}else {
							vwedi.setValoricms(vwentregadocumentoimposto.getValoricms());
						}
						if(vwedi.getValoricmsst() != null){
							vwedi.setValoricmsst(vwedi.getValoricmsst().add(vwentregadocumentoimposto.getValoricmsst()));
						}else {
							vwedi.setValoricmsst(vwentregadocumentoimposto.getValoricmsst());
						}
						if(vwedi.getValorfcp() != null){
							vwedi.setValorfcp(vwedi.getValorfcp().add(vwentregadocumentoimposto.getValorfcp()));
						}else {
							vwedi.setValorfcp(vwentregadocumentoimposto.getValorfcp());
						}
						if(vwedi.getValorfcpst() != null){
							vwedi.setValorfcpst(vwedi.getValorfcpst().add(vwentregadocumentoimposto.getValorfcpst()));
						}else {
							vwedi.setValorfcpst(vwentregadocumentoimposto.getValorfcpst());
						}
						if(vwedi.getValorpis() != null){
							vwedi.setValorpis(vwedi.getValorpis().add(vwentregadocumentoimposto.getValorpis()));
						}else {
							vwedi.setValorpis(vwentregadocumentoimposto.getValorpis());
						}
						if(vwedi.getValoripi() != null){
							vwedi.setValoripi(vwedi.getValoripi().add(vwentregadocumentoimposto.getValoripi()));
						}else {
							vwedi.setValoripi(vwentregadocumentoimposto.getValoripi());
						}
						if(vwedi.getValoripi() != null){
							vwedi.setValorcofins(vwedi.getValorcofins().add(vwentregadocumentoimposto.getValorcofins()));
						}else {
							vwedi.setValorcofins(vwentregadocumentoimposto.getValorcofins());
						}
						if(vwedi.getValoriss() != null){
							vwedi.setValoriss(vwedi.getValoriss().add(vwentregadocumentoimposto.getValoriss()));
						}else {
							vwedi.setValoriss(vwentregadocumentoimposto.getValoriss());
						}
						
						
						if(vwedi.getValorir() != null){
							vwedi.setValorir(vwedi.getValorir().add(vwentregadocumentoimposto.getValorir() != null ? vwentregadocumentoimposto.getValorir() : new Money()));
						}else {
							vwedi.setValorir(vwentregadocumentoimposto.getValorir() != null ? vwentregadocumentoimposto.getValorir() : new Money());
						}
						if(vwedi.getValorcsll() != null){
							vwedi.setValorcsll(vwedi.getValorcsll().add(vwentregadocumentoimposto.getValorcsll() != null ? vwentregadocumentoimposto.getValorcsll() : new Money()));
						}else {
							vwedi.setValorcsll(vwentregadocumentoimposto.getValorcsll() != null ? vwentregadocumentoimposto.getValorcsll() : new Money());
						}
						if(vwedi.getValorinss() != null){
							vwedi.setValorinss(vwedi.getValorinss().add(vwentregadocumentoimposto.getValorinss() != null ? vwentregadocumentoimposto.getValorinss() : new Money()));
						}else {
							vwedi.setValorinss(vwentregadocumentoimposto.getValorinss() != null ? vwentregadocumentoimposto.getValorinss() : new Money());
						}
						if(vwedi.getValorimpostoimportacao() != null){
							vwedi.setValorimpostoimportacao(vwedi.getValorimpostoimportacao().add(vwentregadocumentoimposto.getValorimpostoimportacao() != null ? vwentregadocumentoimposto.getValorimpostoimportacao() : new Money()));
						}else {
							vwedi.setValorimpostoimportacao(vwentregadocumentoimposto.getValorimpostoimportacao() != null ? vwentregadocumentoimposto.getValorimpostoimportacao() : new Money());
						}
						
						if(vwedi.getValorIcmsDesonerado() != null) {
							vwedi.setValorIcmsDesonerado(vwedi.getValorIcmsDesonerado().add(vwentregadocumentoimposto.getValorIcmsDesonerado() != null ? vwentregadocumentoimposto.getValorIcmsDesonerado() : new Money()));
						} else {
							vwedi.setValorIcmsDesonerado(vwentregadocumentoimposto.getValorIcmsDesonerado() != null ? vwentregadocumentoimposto.getValorIcmsDesonerado() : new Money());
						}
						
						break;
					}
				}
			}
		}
		
		if(!exist){
			listaAgrupada.add(vwentregadocumentoimposto);
		}
	}	
	
	/**
	 * M�todo cria entregadocumentohistorico com origem da entrega
	 *
	 * @param entregadocumento
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumentohistorico criaEntregadocumentohistoricoOrigemEntrega(Entregadocumento entregadocumento, Entrega entrega){
		Entregadocumentohistorico edh = null;
		if(entrega != null && entrega.getCdentrega() != null && entregadocumento != null && entregadocumento.getCdentregadocumento() != null){
			edh = new Entregadocumentohistorico();
			edh.setEntregadocumento(entregadocumento);
			edh.setObservacao("Origem: Recebimento <a href=\"/w3erp/suprimento/crud/Entrega?ACAO=consultar&cdentrega=" + entrega.getCdentrega() +
					"\" >" + entrega.getCdentrega() + "</a>");
			edh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			edh.setDtaltera(new Timestamp(System.currentTimeMillis()));
		}
		
		return edh;
	}
	
	/**
	 * M�todo que cria o hist�rico da entregadocumento com a situa��o passada por par�metro
	 *
	 * @param entregadocumento
	 * @param entregadocumentosituacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumentohistorico criaEntregadocumentohistorico(Entregadocumento entregadocumento, Entregadocumentosituacao entregadocumentosituacao){
		Entregadocumentohistorico edh = null;
		edh = new Entregadocumentohistorico();
		edh.setEntregadocumento(entregadocumento);
		edh.setEntregadocumentosituacao(entregadocumentosituacao);
		edh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		edh.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		if(entregadocumento.getIdFornecimento() != null && !"".equals(entregadocumento.getIdFornecimento())){
			edh.setObservacao("Origem: Fornecimento <a href=\"/w3erp/suprimento/crud/Fornecimento?ACAO=consultar&cdfornecimento=" + entregadocumento.getIdFornecimento() +
					"\" >" + entregadocumento.getIdFornecimento() + "</a>");
		}
		if(entregadocumento.getIdDocumentoRegistrarEntrada() != null && !"".equals(entregadocumento.getIdDocumentoRegistrarEntrada())){
			String[] docs = entregadocumento.getListDocumentoRegistrarEntrada().split(",");
			edh.setObservacao("Origem: Conta � pagar: ");
			for (String str : docs) {
					edh.setObservacao(edh.getObservacao().concat("<a href=\"/w3erp/financeiro/crud/Contapagar?ACAO=consultar&cddocumento=" + entregadocumento.getIdDocumentoRegistrarEntrada() +
							"\" >" + str + "</a> (Registrar Entrada Fiscal) "));
			}
		}
		if(entregadocumento.getWhereInPedido() != null && !"".equals(entregadocumento.getWhereInPedido())){
			String[] ids = entregadocumento.getWhereInPedido().split(",");
			StringBuilder obs = new StringBuilder();
			if(ids != null && ids.length > 0){
				for(String cdpedidovenda : ids){
					String linkPedido = "";
					if(parametrogeralService.getBoolean(Parametrogeral.HABILITAR_CONFIGURACAO_OTR)) {
						linkPedido = "/w3erp/faturamento/crud/PedidovendaOtr";
					} else {
						linkPedido = "/w3erp/faturamento/crud/Pedidovenda";
					}
					
					if(!obs.toString().equals("")) obs.append(", ");
					obs.append("<a href=\"" + linkPedido + "?ACAO=consultar&cdpedidovenda=" + cdpedidovenda +
					"\" >" + cdpedidovenda + "</a>");
				}
			}
			edh.setObservacao("Coleta do pedido de venda: " + obs.toString());
		}
		if(entregadocumento.getWhereInColeta() != null && !"".equals(entregadocumento.getWhereInColeta())){
			String[] ids = entregadocumento.getWhereInColeta().split(",");
			StringBuilder obs = new StringBuilder();
			if(ids != null && ids.length > 0){
				for(String cdcoleta : ids){
					if(!obs.toString().equals("")) obs.append(", ");
					obs.append("<a href=\"/w3erp/faturamento/crud/Coleta?ACAO=consultar&cdcoleta=" + cdcoleta +
					"\" >" + cdcoleta + "</a>");
				}
			}
			if(edh.getObservacao() != null){
				edh.setObservacao(edh.getObservacao()+"<br>Coleta: " + obs.toString());
			} else{				
				edh.setObservacao("Coleta: " + obs.toString());
			}
		}
		return edh;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findEntregadocumentoComSaldo(Integer cdentregadocumento)
	 *
	 * @param cdentregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumento findEntregadocumentoComSaldo(Integer cdentregadocumento) {
		return entradafiscalDAO.findEntregadocumentoComSaldo(cdentregadocumento);
	}
	
	/**
	 * M�todo que carrega o rateio da entregadoumento
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> carregaListaEntregadocumentoForEntrega(Entrega entrega){
		List<Entregadocumento> listaEntregadocumento = null;
		if(entrega != null && entrega.getCdentrega() != null){
			listaEntregadocumento = findEntregadocumentoByEntrega(entrega);
			if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
				for(Entregadocumento ed : listaEntregadocumento){
					if(ed.getRateio() != null)
						ed.setRateio(rateioService.findRateio(ed.getRateio()));
					ed.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class, entregadocumentofreteService.findByEntregadocumento(ed)));
					ed.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class, entregadocumentofreterateioService.findByEntregadocumento(ed)));
					ed.setListaApropriacao(entregaDocumentoApropriacaoService.findByEntregaDocumento(ed));
					if(ed.getCdentregadocumento() != null){
						carregaListaEntregamateriallote(ed);
					}
				}
			}
		}
		return listaEntregadocumento;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForDominiointegracaoEntrada(Empresa empresa, Date dtinicio, Date dtfim)
	 *
	 * @param empresa
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForDominiointegracaoEntrada(Empresa empresa, Date dtinicio, Date dtfim){
		return entradafiscalDAO.findForDominiointegracaoEntrada(empresa, dtinicio, dtfim);
	}
	
	/**
	 * M�todo que calcula o rateio de acordo com os materias
	 *
	 * @param bean
	 * @author Luiz Fernando
	 */
	public void calculaRateio(Entregadocumento bean) {
		HashMap<String, Money> map = new HashMap<String, Money>();
		HashMap<Integer, Money> mapimpostos = new HashMap<Integer, Money>();
		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
		Double valortotal;
		Double valor = 0.0;
		if(bean.getValortotaldocumento() != null){
			valor = bean.getValortotaldocumento().getValue().doubleValue();
		}
		
		Empresa empresa = null;
		if(bean.getEmpresa() != null && bean.getEmpresa().getCdpessoa() != null){
			empresa = empresaService.loadComContagerenciaiscompra(bean.getEmpresa());
		}
		
		Contagerencial contagerencialicmsst = null;
		Contagerencial contagerencialipi = null;
		Contagerencial contagerencialfrete = null;
		Contagerencial contagerencialii = null;
		
		Money valortotalicmsst = bean.getValortotalicmsst();
		Money valortotalipi = bean.getValortotalipi();
		Money valortotalfrete = new Money();
		Money valortotalii = new Money();
		
		Ordemcompramaterial ordemcompramaterial; 
		Contagerencial contagerencial = null;
		Centrocusto centrocusto = null;
		Projeto projeto = null;
		
		Object attr = NeoWeb.getRequestContext().getSession().getAttribute("listaEntregamaterial" + bean.getIdentificadortela());
		if(attr != null) {
			bean.setListaEntregamaterial((Set<Entregamaterial>) attr);
		}
		
		for (Entregamaterial em : bean.getListaEntregamaterial()){
			if(em.getMaterial() != null && em.getQtde() != null && em.getQtde() > 0 && em.getValorunitario() != null && em.getValorunitario() > 0){
				contagerencial = null;
				centrocusto = em.getCentrocusto();
				projeto = em.getProjeto();
				
				Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = em.getListaOrdemcompraentregamaterial();
				for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
					Ordemcompramaterial ocm = ordemcompraentregamaterial.getOrdemcompramaterial();
					if(ocm != null && ocm.getCdordemcompramaterial() != null){
						ordemcompramaterial = ordemcompramaterialService.findForCalcularrateio(ocm);
						if(ordemcompramaterial != null){
							contagerencial = ordemcompramaterial.getContagerencial();
							centrocusto = ordemcompramaterial.getCentrocusto();
							projeto = ordemcompramaterial.getProjeto();
							break;
						}
					}
				}
				
				Material material = materialService.load(em.getMaterial(), "material.cdmaterial, material.nome, material.contagerencial");
				if(material != null){
					em.setMaterial(material);
					if(contagerencial == null)
						contagerencial = material.getContagerencial();
				}
				
				String whereInContagerencialCentrocustoProjeto = ordemcompraService.getwhereInContagerencialCentrocustoProjeto(contagerencial, centrocusto, projeto);
				valortotal = 0.0;
				if(em.getValorunitario() != null)
					valortotal = em.getValorunitario();
				
				valortotal = valortotal * (em.getQtde() != null ? em.getQtde() : 1.0);
				
				if(em.getValorpisretido() != null && em.getValorpisretido().getValue().doubleValue() > 0){
					valortotal = valortotal - em.getValorpisretido().getValue().doubleValue();
				}
				if(em.getValorcofinsretido() != null && em.getValorcofinsretido().getValue().doubleValue() > 0){
					valortotal = valortotal - em.getValorcofinsretido().getValue().doubleValue();
				}
				if(em.getValordesconto() != null){
					valortotal = valortotal - em.getValordesconto().getValue().doubleValue();
				}
				if(em.getValorir() != null){
					valortotal = valortotal - em.getValorir().getValue().doubleValue();
				}
				if(em.getValorcsll() != null){
					valortotal = valortotal - em.getValorcsll().getValue().doubleValue();
				}
				if(em.getValorinss() != null){
					valortotal = valortotal - em.getValorinss().getValue().doubleValue();
				}
				if(em.getValoriss() != null && em.getTipotributacaoiss() != null && Tipotributacaoiss.RETIDA.equals(em.getTipotributacaoiss())){
					valortotal = valortotal - em.getValoriss().getValue().doubleValue();
				}
				if(em.getValorseguro() != null){
					valortotal = valortotal + em.getValorseguro().getValue().doubleValue();
				}
				if (em.getValorIcmsDesonerado() != null) {
					valortotal = valortotal - em.getValorIcmsDesonerado().getValue().doubleValue();
				}
				if(em.getQtde() != null && em.getQtde() > 0){
					if(map.containsKey(whereInContagerencialCentrocustoProjeto)){
						map.put(whereInContagerencialCentrocustoProjeto, map.get(whereInContagerencialCentrocustoProjeto).add(new Money(valortotal)));
					} else{
						map.put(whereInContagerencialCentrocustoProjeto, new Money(valortotal));
					}
				}
				
				if(em.getValorfrete() != null && em.getValorfrete().getValue().doubleValue() > 0){
					valortotalfrete = valortotalfrete.add(em.getValorfrete());
				}
				if(em.getValorimpostoimportacao() != null && em.getValorimpostoimportacao().getValue().doubleValue() > 0){
					valortotalii = valortotalii.add(em.getValorimpostoimportacao());
				}
			}
		}
		
		if(empresa != null){
			contagerencialicmsst = empresa.getContagerencialicmsst();
			contagerencialipi = empresa.getContagerencialipi();
			contagerencialfrete = empresa.getContagerencialfrete();
			contagerencialii = empresa.getContagerencialii();
			
			if(contagerencialicmsst != null && contagerencialicmsst.getCdcontagerencial() != null && 
					valortotalicmsst != null && valortotalicmsst.getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialicmsst.getCdcontagerencial())){
					mapimpostos.put(contagerencialicmsst.getCdcontagerencial(), mapimpostos.get(contagerencialicmsst.getCdcontagerencial()).add(new Money(valortotalicmsst)));
				} else{
					mapimpostos.put(contagerencialicmsst.getCdcontagerencial(), new Money(valortotalicmsst));
				}
			}
			if(contagerencialipi != null && contagerencialipi.getCdcontagerencial() != null && 
					valortotalipi != null && valortotalipi.getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialipi.getCdcontagerencial())){
					mapimpostos.put(contagerencialipi.getCdcontagerencial(), mapimpostos.get(contagerencialipi.getCdcontagerencial()).add(valortotalipi));
				} else{
					mapimpostos.put(contagerencialipi.getCdcontagerencial(), valortotalipi);
				}
			}
			if(contagerencialfrete != null && contagerencialfrete.getCdcontagerencial() != null && 
					valortotalfrete != null && valortotalfrete.getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialfrete.getCdcontagerencial())){
					mapimpostos.put(contagerencialfrete.getCdcontagerencial(), mapimpostos.get(contagerencialfrete.getCdcontagerencial()).add(valortotalfrete));
				} else{
					mapimpostos.put(contagerencialfrete.getCdcontagerencial(), valortotalfrete);
				}
			}
			if(contagerencialii != null && contagerencialii.getCdcontagerencial() != null && 
					valortotalii != null && valortotalii.getValue().doubleValue() > 0){
				if(mapimpostos.containsKey(contagerencialii.getCdcontagerencial())){
					mapimpostos.put(contagerencialii.getCdcontagerencial(), mapimpostos.get(contagerencialii.getCdcontagerencial()).add(valortotalii));
				} else{
					mapimpostos.put(contagerencialii.getCdcontagerencial(), valortotalii);
				}
			}
		}
		
		for (String whereInContagerencialCentrocustoProjeto : map.keySet()) {
			Money valorContaGerencial = map.get(whereInContagerencialCentrocustoProjeto);
			
			String[] idsContagerencialCentrocustoProjeto = whereInContagerencialCentrocustoProjeto.split(",");
			
			contagerencial = null;
			centrocusto = null;
			projeto = null;
			if(!idsContagerencialCentrocustoProjeto[0].equals("<null>")) contagerencial = new Contagerencial(Integer.parseInt(idsContagerencialCentrocustoProjeto[0]));
			if(!idsContagerencialCentrocustoProjeto[1].equals("<null>")) centrocusto = new Centrocusto(Integer.parseInt(idsContagerencialCentrocustoProjeto[1]));
			if(!idsContagerencialCentrocustoProjeto[2].equals("<null>")) projeto = new Projeto(Integer.parseInt(idsContagerencialCentrocustoProjeto[2]));
			
			Rateioitem rateioitem = new Rateioitem(
					contagerencial, centrocusto, projeto, valorContaGerencial, 
					SinedUtil.truncate(valor == 0 ? 0 : valorContaGerencial.getValue().doubleValue() / valor * 100));
			
			itens.add(rateioitem);
		}
		
		for (Integer cg : mapimpostos.keySet()) {
			Money valorContaGerencial = mapimpostos.get(cg);
			
			Rateioitem rateioitem = new Rateioitem(
					new Contagerencial(cg), valorContaGerencial, 
					SinedUtil.truncate(valor == 0 ? 0 : valorContaGerencial.getValue().doubleValue() / valor * 100));
			
			itens.add(rateioitem);
		}
		
		Rateio rateio = new Rateio();
		rateio.setListaRateioitem(itens);
		bean.setRateio(rateio);
		rateioService.calculaValoresRateio(rateio, new Money(valor));
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param request
	 * @param entregadocumento
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void criaAtributoEntregamaterialParaCalculoRateio (WebRequestContext request, Entregadocumento entregadocumento){
		if(entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
			List<Entregamaterial> lista = new ArrayList<Entregamaterial>();
			lista.addAll(entregadocumento.getListaEntregamaterial());
			
			Collections.sort(lista,new Comparator<Entregamaterial>(){
				public int compare(Entregamaterial e1, Entregamaterial e2) {
					if(e1.getMaterial() == null) return 1;
					if(e2.getMaterial() == null) return -1;
					
					try{
						Integer o1Id = Integer.parseInt(e1.getMaterial().getIdentificacao());
						Integer o2Id = Integer.parseInt(e2.getMaterial().getIdentificacao());
						return o1Id.compareTo(o2Id);
					} catch (Exception e) {
						return e1.getMaterial().getAutocompleteDescription().compareTo(e2.getMaterial().getAutocompleteDescription());
					}
				}
			});
			
			entregadocumento.setListaEntregamaterialTrans(lista);
			
			if(request.getBindException().hasErrors()){
				Object attribute = null;
				if(entregadocumento.getCdentregadocumento() != null)
					attribute = request.getSession().getAttribute("listaEntregamaterial" + (entregadocumento.getCdentregadocumento()));
				else if(entregadocumento.getIndexlistaforrateio() != null)
					attribute = request.getSession().getAttribute("listaEntregamaterial" + entregadocumento.getIndexlistaforrateio());
					
				if (attribute != null) {
					entregadocumento.setListaEntregamaterialTrans((List<Entregamaterial>)attribute);		
				}
			}
		}
		if(entregadocumento != null){
			if(entregadocumento.getCdentregadocumento() != null){
				request.getSession().setAttribute("listaEntregamaterial"  + (entregadocumento.getCdentregadocumento() != null ? entregadocumento.getCdentregadocumento() : ""), entregadocumento.getListaEntregamaterialTrans());
			}else {
				request.getSession().setAttribute("listaEntregamaterial"  + (entregadocumento.getIndexlista() != null ? entregadocumento.getIndexlista() : ""), entregadocumento.getListaEntregamaterialTrans());
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro50(SintegraFiltro filtro) {
		return entradafiscalDAO.findForSIntegraRegistro50(filtro);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSIntegraRegistro70(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro70(SintegraFiltro filtro) {
		return entradafiscalDAO.findForSIntegraRegistro70(filtro);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSIntegraRegistro76(SintegraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSIntegraRegistro76(SintegraFiltro filtro) {
		return entradafiscalDAO.findForSIntegraRegistro76(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedRegD100
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregadocumento> findForSpedRegD100(SpedarquivoFiltro filtro) {
		return entradafiscalDAO.findForSpedRegD100(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedRegC100
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Entregadocumento> findForSpedRegC100(SpedarquivoFiltro filtro) {
		return entradafiscalDAO.findForSpedRegC100(filtro);
	}
	
	public List<Entregadocumento> findForSpedRegC500(SpedarquivoFiltro filtro) {
		return entradafiscalDAO.findForSpedRegC500(filtro);
	}
	
	public List<Entregadocumento> findForSpedRegD500(SpedarquivoFiltro filtro) {
		return entradafiscalDAO.findForSpedRegD500(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegC100(SpedpiscofinsFiltro filtro, boolean servico, Empresa empresa) {
		return entradafiscalDAO.findForSpedPiscofinsRegC100(filtro, servico, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedPiscofinsRegF100(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @param servico
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegF100(SpedpiscofinsFiltro filtro, boolean servico, Empresa empresa) {
		return entradafiscalDAO.findForSpedPiscofinsRegF100(filtro, servico, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedPiscofinsRegD100(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegD100(SpedpiscofinsFiltro filtro, Empresa empresa) {
		return entradafiscalDAO.findForSpedPiscofinsRegD100(filtro, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForSpedPiscofinsRegC500(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegC500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		return entradafiscalDAO.findForSpedPiscofinsRegC500(filtro, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForSpedPiscofinsRegD500(SpedpiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForSpedPiscofinsRegD500(SpedpiscofinsFiltro filtro, Empresa empresa) {
		return entradafiscalDAO.findForSpedPiscofinsRegD500(filtro, empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForSagefiscal(SagefiscalFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 19/01/2017
	* @author Luiz Fernando
	*/
	public List<Entregadocumento> findForSagefiscal(SagefiscalFiltro filtro) {
		return entradafiscalDAO.findForSagefiscal(filtro);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 19/01/2012
	 * @param bean
	 */
	public void updateRetornoWMS(Entregadocumento bean){
		entradafiscalDAO.updateRetornoWMS(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findRateioByEntregadocumento(Entregadocumento entregadocumento)
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumento findRateioByEntregadocumento(Entregadocumento entregadocumento) {
		return entradafiscalDAO.findRateioByEntregadocumento(entregadocumento);
	}
	
	/**
	 * M�todo que limpa as refer�ncias do rateio
	 *
	 * @param ed
	 * @author Luiz Fernando
	 */
	public void atualizaRateioReferencias(Entregadocumento ed) {
		if(ed != null && ed.getRateio() != null && ed.getEntrega() != null && ed.getEntrega().getHaveOrdemcompra()){
			Set<Ordemcompraentrega> listaOrdemcompraentrega = ed.getEntrega().getListaOrdemcompraentrega();
			for (Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega) {
				Ordemcompra oc = ordemcompraentrega.getOrdemcompra();
				if(oc != null && oc.getRateio() != null){
					oc.getRateio().setListaRateioitem(ed.getRateio().getListaRateioitem());
					rateioService.limpaReferenciaApenasRateioitem(oc.getRateio());
					rateioService.saveOrUpdate(oc.getRateio());
				}
			}
		}
	}
	
	/**
	 * M�todo que valida os campos obrigat�rios  da entrada fiscal no recebimento
	 *
	 * @param ed
	 * @author Luiz Fernando
	 */
	public void validaEntradafiscal(Entregadocumento ed) {
		boolean isSped = SinedUtil.isSpedFiscal();
		StringBuilder msg = new StringBuilder();
		if(ed == null){
			throw new SinedException("Entrada fiscal � obrigat�rio.");
		}
		
		Modelodocumentofiscal modelodocumentofiscal = ed.getModelodocumentofiscal();
		if(modelodocumentofiscal != null){
			modelodocumentofiscal = modelodocumentofiscalService.load(modelodocumentofiscal);
		}
		
		if(ed.getDocumentotipo() == null)
			msg.append("Tipo de documento � obrigat�rio.\n");
		if(ed.getFornecedor() == null)
			msg.append("Fornecedor � obrigat�rio.\n");
		if(ed.getNumero() == null || "".equals(ed.getNumero()))
			msg.append("N�mero � obrigat�rio.\n");
		if(ed.getDtemissao() == null)
			msg.append("Data de emiss�o � obrigat�rio.\n");
		if(modelodocumentofiscal == null)
			msg.append("Modelo da NF-E � obrigat�rio.\n");
		if(ed.getPrazopagamento() == null && (ed.getSimplesremessa() == null || !ed.getSimplesremessa()))
			msg.append("Prazo de pagmento � obrigat�rio.\n");
		
		String msgErroCfopescopo = "";
		if(isSped){
			if(modelodocumentofiscal != null){
				if(modelodocumentofiscal.getObrigarchaveacesso() != null && 
						modelodocumentofiscal.getObrigarchaveacesso() && 
						(ed.getChaveacesso() == null || "".equals(ed.getChaveacesso())))
					msg.append("Chave de acesso � obrigat�rio.\n");
				
				if(modelodocumentofiscal.getObrigartitulo() != null && modelodocumentofiscal.getObrigartitulo() && (ed.getSimplesremessa() == null || !ed.getSimplesremessa())){
					if(ed.getTipotitulo() == null)
						msg.append("O campo Tipo de t�tulo � obrigat�rio.\n");
					if(ed.getNumerotitulo() == null || "".equals(ed.getNumerotitulo()))
						msg.append("O campo n�mero do t�tulo � obrigat�rio.\n");
					
					if(ed.getTipotitulo() != null && ed.getTipotitulo().equals(Tipotitulocredito.OUTROS) && 
							(ed.getDescricaotitulo() == null || "".equals(ed.getDescricaotitulo()))){
						msg.append("O campo descri��o do t�tulo � obrigat�rio.\n");
					}
				}
			}
		}
		
		boolean modelosped = modelodocumentofiscal != null && ((modelodocumentofiscal.getEnviospedfiscal() != null && modelodocumentofiscal.getEnviospedfiscal()) || (modelodocumentofiscal.getEnviospedpiscofins() != null && modelodocumentofiscal.getEnviospedpiscofins()));
		
		if(ed.getListaEntregamaterial() == null || ed.getListaEntregamaterial().isEmpty()){
			msg.append("Entrada fiscal deve ter pelo menos 1 item.\n");
		}else {
			for(Entregamaterial em : ed.getListaEntregamaterial()){
				if(em.getMaterial() == null)
					msg.append("Campo material � obrigat�rio.\n");
				if(em.getQtde() == null)
					msg.append("Campo qtde entregue � obrigat�rio.\n");
				
				if(em.getMaterialclassetrans() == null && em.getMaterialclasse() == null)
					msg.append("Necessidade � obrigat�rio.\n");
				
				if(isSped && modelosped){
					if(em.getUnidademedidacomercial() == null)
						msg.append("Unidade de medidas � obrigat�rio.\n");
					if(em.getCfop() == null || em.getCfop().getCdcfop() == null){
						msg.append("CFOP � obrigat�rio.\n");
					}
					
					Integer operacaoCOFINS = 0;
					Integer operacaoPIS = 0;
					
					if(em.getCstcofins() != null){
						try{
							operacaoCOFINS = Integer.parseInt(em.getCstcofins().getCdnfe());
						} catch (Exception e) {	}
						
						if(em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_01) || 
								em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_02) || 
								em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_03) ||
								em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_50) ||
								em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_66) ||
								em.getCstcofins().equals(Tipocobrancacofins.OPERACAO_75)){
							if(em.getValorbccofins() == null || em.getValorbccofins().getValue().doubleValue() == 0){
								msg.append("Base de c�lculo do cofins � obrigat�rio.\n");
							}
							if(em.getCofins() == null){
								msg.append("Al�quota do cofins � obrigat�rio.\n");
							}
							if(em.getValorcofins() == null){
								msg.append("Valor do cofins � obrigat�rio.\n");
							}
						}
					}
					
					if(em.getCstpis() != null){
						try{
							operacaoPIS = Integer.parseInt(em.getCstpis().getCdnfe());
						} catch (Exception e) {}
						
						if(em.getCstpis().equals(Tipocobrancacofins.OPERACAO_01) || 
								em.getCstpis().equals(Tipocobrancacofins.OPERACAO_02) || 
								em.getCstpis().equals(Tipocobrancacofins.OPERACAO_03) ||
								em.getCstpis().equals(Tipocobrancacofins.OPERACAO_50) ||
								em.getCstpis().equals(Tipocobrancacofins.OPERACAO_66) ||
								em.getCstpis().equals(Tipocobrancacofins.OPERACAO_75)){
							if(em.getValorbcpis() == null || em.getValorbcpis().getValue().doubleValue() == 0){
								msg.append("Base de c�lculo do pis � obrigat�rio.\n");
							}
							if(em.getPis() == null || em.getPis() == 0){
								msg.append("Al�quota do pis � obrigat�rio.\n");
							}
							if(em.getValorpis() == null){
								msg.append("Valor do pis � obrigat�rio.\n");
							}
						}
					}
					
					if((operacaoCOFINS >= 50 && operacaoCOFINS <= 66) || (operacaoPIS >= 50 && operacaoCOFINS <= 66)){
						if(em.getNaturezabcc() == null){
							msg.append("Natureza da base de c�lculo do cr�dito � obrigat�rio.\n");
						}
					}
				}
				
				if(em.getCfop() != null && em.getCfop().getCdcfop() != null){
					em.setCfop(cfopService.carregarCfop(em.getCfop()));
					if(em.getCfop().getCodigo() != null){
//						if("5".equals(em.getCfop().getCodigo().subSequence(0, 1)) || "6".equals(em.getCfop().getCodigo().subSequence(0, 1)) || 
//								"7".equals(em.getCfop().getCodigo().subSequence(0, 1))){
//							msg.append("CFOP informado deve ser do tipo Entrada. " + (em.getMaterial() != null ? " Material " + materialService.load(em.getMaterial(), "material.nome").getNome(): "" ) + "<br>");
//						}
					}
					msgErroCfopescopo = cfopService.verificaCfopescopo(ed.getFornecedor(), ed.getEmpresa(), em.getCfop());
					if(msgErroCfopescopo != null && !"".equals(msgErroCfopescopo)){
						msg.append(msgErroCfopescopo + (em.getMaterial() != null ? " Material " + materialService.load(em.getMaterial(), "material.nome").getNome() : "") + "\n");
					}
				}
			}
		}		
		
		if(ed.getListadocumento() != null && !ed.getListadocumento().isEmpty()){
			for(Entregapagamento ep : ed.getListadocumento()){
				if(ep.getDtvencimento() == null){
					msg.append("Data de vencimento da(s) parcela(s) � obrigat�rio.\n");
					break;
				}					
			}
			if(ed.getFornecedor() != null && ed.getDocumentotipo() != null){
				if(!this.validaPagamentoAntecipado(ed)){
					msg.append("O valor da parcela deve ser menor ou igual ao saldo do pagamento adiantado escolhido.\n");
				}
			}
		}		
		if(isSped && modelosped){
			if(ed.getDtentrada() == null)
				msg.append("Data de entrada � obrigat�rio.\n");
			if(ed.getIndpag() == null){
				msg.append("Forma de pagamento � obrigat�rio.\n");
			}
			if(ed.getResponsavelfrete() == null)
				msg.append("Respons�vel pelo frete � obrigat�rio.\n");
		}
		
		if(ed.getListaEntregadocumentofrete() != null && ed.getListaEntregadocumentofrete().size() > 0){
			Money valorDocumento = ed.getValor();
			Money valorRateio = new Money(0);
			if (ed.getListaEntregadocumentofreterateio()!=null && !ed.getListaEntregadocumentofreterateio().isEmpty()){
				for (Entregadocumentofreterateio entregadocumentofreterateio: ed.getListaEntregadocumentofreterateio()){
					valorRateio = valorRateio.add(entregadocumentofreterateio.getValor());
				}
			}
			if (valorDocumento.toLong()!=valorRateio.toLong()){
				msg.append("O valor do rateio n�o pode diferente do valor total da nota.");
			}
		}
		
		if(!"".equals(msg.toString())){
			throw new SinedException(msg.toString());
		}
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#existFornecedorNumero(Entregadocumento ed)
	 *
	 * @param ed
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existFornecedorNumero(Entregadocumento ed) {
		if(Util.objects.isPersistent(ed.getModelodocumentofiscal()) && StringUtils.isBlank(ed.getModelodocumentofiscal().getCodigo())){
			Modelodocumentofiscal modelodocumentofiscal = modelodocumentofiscalService.load(ed.getModelodocumentofiscal());
			if(modelodocumentofiscal != null){
				ed.setModelodocumentofiscal(modelodocumentofiscal);
			}
		}
		return entradafiscalDAO.existFornecedorNumero(ed);		
	}
	
	/**
	 * M�todo que verifica se tem que exibir o campo comprimento na aba itens do entradafiscal
	 *
	 * @param entregadocumento
	 * @author Luiz Fernando
	 */
	public void verificaExibicaoComprimentoItemMaterial(Entregadocumento entregadocumento) {
		if(entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
			for(Entregamaterial em : entregadocumento.getListaEntregamaterial()){
				em.setShowcomprimento(Boolean.FALSE);
				if(em.getMaterial() != null && em.getMaterial().getCdmaterial() != null){
					if(em.getMaterial().getProduto() != null && em.getMaterial().getProduto()){
						Produto produto = produtoService.carregaProduto(em.getMaterial());
						if(produto != null) {
							if(produto.getComprimento() != null || produto.getLargura() != null || produto.getAltura() != null){
								em.setShowcomprimento(Boolean.TRUE);
							}
						}
					}
				}
			}
		}
		
	}	
	
	/**
	 * M�todo que verifica se existe entregadocumento removida do recebimento
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForDeleteManual(Entrega entrega, String notWhereInEntregadocumento)
	 * 
	 * @param entrega
	 * @param notWhereInEntregadocumento
	 * @author Luiz Fernando
	 */
	public void verificarAndexcluirEntregadocumentoremovido(Entrega entrega, String notWhereInEntregadocumento) {
		if(entrega != null && entrega.getCdentrega() != null){
			List<Entregadocumento> listaEntregadocumento = this.findForDeleteManual(entrega, notWhereInEntregadocumento);
			
			if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
				for(Entregadocumento entregadocumento : listaEntregadocumento){
					if(entregadocumento.getCdentregadocumento() != null){
						if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
							for(Entregapagamento entregapagamento : entregadocumento.getListadocumento()){
								if(entregapagamento.getCdentregapagamento() != null){
									entregapagamentoService.delete(entregapagamento);
								}
							}
							for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
								if(entregamaterial.getCdentregamaterial() != null){
									entregamaterialService.delete(entregamaterial);
								}
							}
						}
						this.delete(entregadocumento);
					}
				}
			}
		}		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForDeleteManual(Entrega entrega, String notWhereInEntregadocumento)
	 *
	 * @param entrega
	 * @param notWhereInEntregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Entregadocumento> findForDeleteManual(Entrega entrega,	String notWhereInEntregadocumento) {
		return entradafiscalDAO.findForDeleteManual(entrega, notWhereInEntregadocumento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForTotalListagem(EntradafiscalFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public List<Entregadocumento> findForTotalListagem(EntradafiscalFiltro filtro) {
		return entradafiscalDAO.findForTotalListagem(filtro);
	}
	
	/**
	 * M�todo que retorna o valor total das entradas fiscais 
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public void setTotalListagem(EntradafiscalFiltro filtro){
		Money totalValorDocumento = new Money(0);
		Money totalValorPagamento = new Money(0);
		List<Entregadocumento> lista = findForTotalListagem(filtro);
		if(lista != null && !lista.isEmpty()){
			for (Entregadocumento entregadocumento: lista){
				if (entregadocumento.getValor()!=null){
					totalValorDocumento = totalValorDocumento.add(entregadocumento.getValor());
				}
			}
			totalValorPagamento = entregapagamentoService.getTotalpagamento(CollectionsUtil.listAndConcatenate(lista, "cdentregadocumento", ","));
		}
		filtro.setTotalgeral(totalValorDocumento);
		filtro.setTotalpagamento(totalValorPagamento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForCsv(EntradafiscalFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForCsv(EntradafiscalFiltro filtro) {
		return entradafiscalDAO.findForCsv(filtro);
	}
	
	public List<Entregadocumento> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return entradafiscalDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findforApuracaopiscofins(ApuracaopiscofinsFiltro filtro) {
		return entradafiscalDAO.findforApuracaopiscofins(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForApuracaoipi(ApuracaoipiFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoipi(ApuracaoipiFiltro filtro) {
		return entradafiscalDAO.findForApuracaoipi(filtro);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForApuracaoicms(ApuracaoicmsFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoicms(ApuracaoicmsFiltro filtro) {
		return entradafiscalDAO.findForApuracaoicms(filtro);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#existEntradafiscalNotCanceladabyFornecedor(Fornecedor fornecedor)
	 *
	 * @param fornecedor
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradafiscalNotCancelada(Integer cdentregadocumento) {
		return entradafiscalDAO.existEntradafiscalNotCancelada(cdentregadocumento);
	}
	
	/**
	 * M�todo que valida pagamento com antecipa��o
	 *
	 * @param entregadocumento
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean validaPagamentoAntecipado(Entregadocumento entregadocumento){
		Set<Entregapagamento> lista = entregadocumento.getListadocumento();
		
		if (lista != null && !lista.isEmpty() && entregadocumento.getFornecedor() != null){			
			List<Documento> listaAntecipacao = new ArrayList<Documento>();
			listaAntecipacao = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			
			Map<Integer, Double> mapaAntecipacao = new HashMap<Integer, Double>();
			
			for (Documento antecipacao : listaAntecipacao){
				mapaAntecipacao.put(antecipacao.getCddocumento(), antecipacao.getValor().getValue().doubleValue());
			}
			
			for (Entregapagamento entregapagamento : lista){
				if (entregapagamento.getDocumentoantecipacao() != null && entregapagamento.getDocumentoantecipacao().getCddocumento() != null){
					Double valor = mapaAntecipacao.get(entregapagamento.getDocumentoantecipacao().getCddocumento());
					if (valor != null &&  entregapagamento.getValor() != null){
						valor = valor - entregapagamento.getValor().getValue().doubleValue();
						if (valor < 0){
							return false;
						} else {
							mapaAntecipacao.put(entregapagamento.getDocumentoantecipacao().getCddocumento(), valor);
						}
					}
				}
			}			
		}		
		
		return true;
	}
	
	/**
	 * M�todo que cria entrada fiscal a partir do documento
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Entregadocumento criaEntradafiscalByDocumento(List<Documento> listaDoc) {
		Entregadocumento entregadocumento = new Entregadocumento();
		Money valordocumento = new Money();
		Rateio rateio = new Rateio();
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		Documento documento = listaDoc.get(0);
		boolean numerosDiferentes= false;
		boolean datasDiferentes= false;
		if(documento != null && documento.getPessoa() != null && Tipopagamento.FORNECEDOR.equals(documento.getTipopagamento())){
			documentoService.calculaValores(documento);
			
			entregadocumento.setIdDocumentoRegistrarEntrada(documento.getCddocumento().toString());
			
			entregadocumento.setFornecedor(new Fornecedor(documento.getPessoa().getCdpessoa(), documento.getPessoa().getNome()));
			entregadocumento.setDocumentotipo(documento.getDocumentotipo());
			entregadocumento.setEmpresa(documento.getEmpresa());
			entregadocumento.setDtemissao(documento.getDtemissao());
			
			Set<Entregapagamento> listadocumento = new ListSet<Entregapagamento>(Entregapagamento.class);
					
			entregadocumento.setRateio(rateio);
			entregadocumento.setNumero(documento.getNumero());
			
			int parcela = 1;
			for (Documento doc : listaDoc) {
//				doc.getRateio().getListaRateioitem().get(0).setPercentual(100.00/porcentagem);
				listaRateioitem.addAll(doc.getRateio().getListaRateioitem());
				if(entregadocumento.getListDocumentoRegistrarEntrada() == null || entregadocumento.getListDocumentoRegistrarEntrada().equals("")){
					entregadocumento.setListDocumentoRegistrarEntrada(doc.getCddocumento().toString());
				}else{
					entregadocumento.setListDocumentoRegistrarEntrada(entregadocumento.getListDocumentoRegistrarEntrada().concat(","+doc.getCddocumento().toString()));
				}
				
				if(entregadocumento.getNumero() !=null){
					if(!numerosDiferentes && !entregadocumento.getNumero().equals(doc.getNumero())){
						entregadocumento.setNumero(null);
						numerosDiferentes =true; 
					}
				}
				if(entregadocumento.getDtemissao() !=null){
					if(!datasDiferentes && !entregadocumento.getDtemissao().equals(doc.getDtemissao())){
						entregadocumento.setDtemissao(null);
						datasDiferentes =true; 
					}
				}
				
				Entregapagamento entregapagamento;
				Prazopagamento prazo = null;
				
				entregapagamento = new Entregapagamento();
				entregapagamento.setParcela(parcela);
				
				entregapagamento.setDocumentoorigem(doc);
				entregapagamento.setValor(doc.getValor());
				entregapagamento.setDtvencimento(doc.getDtvencimento());
				listadocumento.add(entregapagamento);
				parcela++;
				valordocumento = valordocumento.add(doc.getValor());
				
				if(doc.getListaParcela() != null && !doc.getListaParcela().isEmpty()){
					for(Parcela p : doc.getListaParcela()){
						if(prazo == null && p.getParcelamento() != null && p.getParcelamento().getPrazopagamento() != null){
							prazo = p.getParcelamento().getPrazopagamento();
						}
					}
					if(prazo == null) prazo = Prazopagamento.UNICA;
					if(entregadocumento.getPrazopagamento() == null || entregadocumento.getPrazopagamento().getCdprazopagamento() == null)
						entregadocumento.setPrazopagamento(prazo);
				}else if(entregadocumento.getPrazopagamento() == null || entregadocumento.getPrazopagamento().getCdprazopagamento() == null){
					entregadocumento.setPrazopagamento(Prazopagamento.UNICA);
				}
				if(doc.getListaDocumento() != null && !doc.getListaDocumento().isEmpty()){
					for(Documento d : doc.getListaDocumento()){
						if(listaDoc.size() > 1 && parcela >1 && !numerosDiferentes){
							break;
						}
						if(d.getCddocumento() != null){
							entregapagamento = new Entregapagamento();
							entregapagamento.setParcela(parcela);
							entregapagamento.setDocumentoorigem(d);
							entregapagamento.setValor(d.getValor());
							entregapagamento.setDtvencimento(d.getDtvencimento());
							listadocumento.add(entregapagamento);
							parcela++;
							valordocumento = valordocumento.add(d.getValor());
							if(d.getRateio() != null && d.getRateio().getListaRateioitem() != null && !d.getRateio().getListaRateioitem().isEmpty()){
								listaRateioitem.addAll(d.getRateio().getListaRateioitem());
							}
						}
					}
				}
			}
			entregadocumento.setListadocumento(listadocumento);
			
			rateio.setListaRateioitem(listaRateioitem);
//			rateioService.calculaValoresRateio(rateio, valordocumento);
//			rateioService.atualizaValorRateio(rateio, valordocumento);
			rateioService.limpaReferenciaRateio(rateio);
			rateioService.limpaReferenciaApenasRateioitem(rateio);
			
			List<Material> listaMaterial = materialService.findServicoByFornecedor(entregadocumento.getFornecedor(), entregadocumento.getEmpresa());
			Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
			Entregamaterial entregamaterial = new Entregamaterial();
			setIdentificadorinterno(entregamaterial);
			
			if(listaMaterial != null && !listaMaterial.isEmpty()){
				entregamaterial.setMaterial(listaMaterial.get(0));
			}
			entregamaterial.setUnidademedidacomercial(entregamaterial.getMaterial() != null ? entregamaterial.getMaterial().getUnidademedida() : null);
			entregamaterial.setQtde(1.0);
			entregamaterial.setValorunitario(valordocumento.getValue().doubleValue());
			entregamaterial.setSequenciaitem(1);
			listaEntregamaterial.add(entregamaterial);
			
			entregadocumento.setListaEntregamaterial(listaEntregamaterial);
		}
		return entregadocumento;
	}
	
	public void setIdentificadorinterno(Entregamaterial entregamaterial) {
		setIdentificadorinterno(entregamaterial, null);
	}
	
	public void setIdentificadorinterno(Entregamaterial entregamaterial, Integer controle) {
		if(entregamaterial != null && entregamaterial.getIdentificadorinterno() == null){
			if(controle == null) controle = 0; 
			entregamaterial.setIdentificadorinterno(controle + System.currentTimeMillis() + "");
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#existEntradafiscalByDocumento(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradafiscalByDocumento(Documento documento) {
		return entradafiscalDAO.existEntradafiscalByDocumento(documento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForFaturar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isOrigemDocumentoOrEntrega(String whereIn) {
		return entradafiscalDAO.isOrigemDocumentoOrEntrega(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForFaturar(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForFaturar(String whereIn) {
		return entradafiscalDAO.findForFaturar(whereIn);
	}
	
	/**
	 * M�todo que fatura a entrada fiscal. Cria conta a pagar
	 *
	 * @param listaEntregadocumento
	 * @return
	 * @author Luiz Fernando
	 * @param nOTACOMPRA_GERA_CONTADEFINITIVA 
	 */
	public HashMap<Boolean, Documento> faturar(List<Entregadocumento> listaEntregadocumento, boolean abrirTelaContaPagar) {
		HashMap<Boolean, Documento> retorno = new HashMap<Boolean, Documento>();
		Documento documentoCriado = null;
		Boolean existDocumentoAntecipacao = false;
		Boolean NOTACOMPRA_GERA_CONTADEFINITIVA = parametrogeralService.getBoolean(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA);
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			Documento documento = new Documento();
			Documentoorigem documentoorigem;
			List<Documentoorigem> listaDocumentoorigemAntecipacao = new ArrayList<Documentoorigem>();
			List<Documento> financiamentos;
			Set<Entregapagamento> listaEntregaPagamento = null;
			Rateio rateio = new Rateio();
			boolean existDocumento = false;
			String whereIn = SinedUtil.listAndConcatenate(listaEntregadocumento, "cdentregadocumento", ",");
			
			if(listaEntregadocumento.size() > 1){
				int tam = listaEntregadocumento.size();
				int i = 1;
				Double valorTotal = 0.0;
				documentoService.defineDocumentoacaoParametroGeral(documento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
				for(Entregadocumento entregadocumento: listaEntregadocumento){				
					if( i == tam){
						valorTotal += entregadocumento.getValorTotalPagamento().getValue().doubleValue();
						documento.setEmpresa(entregadocumento.getEmpresa());
						
						if (listaEntregadocumento.size() == 1 && parametrogeralService.getBoolean("PREENCHE_NUMNF_CAMPO_DOCUMENTO")) {
							documento.setNumero(entregadocumento.getNumero());
						} else {
							documento.setNumero(null);
						}
						
						documento.setDocumentotipo(entregadocumento.getDocumentotipo());
						documento.setDescricao("Conta a pagar referente as entrada fiscal: " + whereIn);
						documento.setDtemissao(new Date(System.currentTimeMillis()));
						documento.setDtvencimento(getDtvencimentoMenor(listaEntregadocumento));
						documento.setValor(new Money(valorTotal));
						documento.setReferencia(Referencia.MES_ANO_CORRENTE);
						documento.setTipopagamento(Tipopagamento.FORNECEDOR);
						documento.setPrazo(null);
						documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
						
						Date dtCompetencia = null;
						if(entregadocumento.getDtentrada() != null) {
							dtCompetencia = entregadocumento.getDtentrada();
						} else if(entregadocumento.getDtemissao() != null) {
							dtCompetencia = entregadocumento.getDtemissao();
						}
						
						documento.setDtcompetencia(dtCompetencia);
					}else{
						valorTotal += entregadocumento.getValorTotalPagamento().getValue().doubleValue();
					}
					i++;					
				}			
							
				Entregapagamento entregapagamentoaux = new Entregapagamento();
				Set<Entregamaterial> listaEntregaMaterialaux = new ListSet<Entregamaterial>(Entregamaterial.class);
				double valorTotalEntregapagamento = 0.0;
				
				for(Entregadocumento ed : listaEntregadocumento){
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						if(ed.getEmpresa() != null && ed.getEmpresa().getCdpessoa() != null){
							documento.setEmpresa(ed.getEmpresa());
						}
						documento.setFornecedor(ed.getFornecedor());
						documento.setPessoa(ed.getFornecedor());
						documento.setDescricao(documento.getDescricao() + " - NF " + ed.getNumero());
						listaEntregaPagamento = ed.getListadocumento();	
						for(Entregapagamento entregapagamento : listaEntregaPagamento){
							if(entregapagamento.getDocumentoantecipacao() == null){
								valorTotalEntregapagamento += entregapagamento.getValor().getValue().doubleValue();
								existDocumento = true;
							}else {
								documentoorigem = new Documentoorigem();
								documentoorigem.setEntregadocumento(ed);
								documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
								if(!existeDocumentoantecipacaoEntregadocumento(documentoorigem, listaDocumentoorigemAntecipacao)){
									listaDocumentoorigemAntecipacao.add(documentoorigem);
								}
								existDocumentoAntecipacao = true;
							}
						}
						
						for(Entregamaterial entregamaterial : ed.getListaEntregamaterial()){
							listaEntregaMaterialaux.add(entregamaterial);
						}
					}
				}
					
				if(existDocumento){
					if(existDocumentoAntecipacao){
						documento.setValor(new Money(valorTotalEntregapagamento));
					}
					entregapagamentoaux.setValor(new Money(valorTotalEntregapagamento));
					List<Rateioitem> listaRateioitem = this.calculaRateioVariasEntradafiscal(listaEntregadocumento);
					rateioitemService.agruparItensRateio(listaRateioitem);
					rateio.setListaRateioitem(listaRateioitem);
					
					if(rateio != null)
						rateioService.limpaReferenciaRateio(rateio);
					documento.setRateio(rateio);						
					documento.setWhereInEntregadocumento(whereIn);
					if(!abrirTelaContaPagar){
						if(documento.getDescricao() != null && documento.getDescricao().length() > 500){
							documento.setDescricao(documento.getDescricao().substring(0, 500));
						}
						contapagarService.saveOrUpdate(documento);
						documento.setWhereInEntregadocumento(null);
					}
					documentoCriado = documento;
				}
			} else {

				Entregadocumento ed = listaEntregadocumento.get(0);
				if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
					financiamentos = new ListSet<Documento>(Documento.class);
					
					if(ed.getRateio() != null && ed.getRateio().getCdrateio() != null)
						ed.setRateio(rateioService.findRateio(ed.getRateio()));
					
					listaEntregaPagamento = ed.getListadocumento();				
					int parcelas = 1;				
					for (Entregapagamento entregapagamento : listaEntregaPagamento) {
						if(entregapagamento.getDocumentoantecipacao() == null){
								if(parcelas == 1){
									documentoService.defineDocumentoacaoParametroGeral(documento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
									this.preencheDocumento(ed.getDocumentotipo(), documento, ed,entregapagamento);
									rateioitemService.agruparItensRateio(ed.getRateio().getListaRateioitem());
									rateio.setListaRateioitem(ed.getRateio().getListaRateioitem());
									rateioService.atualizaValorRateio(rateio, entregapagamento.getValor());
								
								} else{
									Documento financiamento = this.preencheFinanciamento(entregapagamento, parcelas, ed);
									documentoService.defineDocumentoacaoParametroGeral(financiamento, Documentoacao.PREVISTA, NOTACOMPRA_GERA_CONTADEFINITIVA);
									financiamentos.add(financiamento);
								}
							
							parcelas++;
							existDocumento = true;
						}else {
							existDocumentoAntecipacao = true;
							documentoorigem = new Documentoorigem();
							documentoorigem.setEntregadocumento(ed);
							documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
							if(!this.existeDocumentoantecipacaoEntregadocumento(documentoorigem, listaDocumentoorigemAntecipacao)){
								listaDocumentoorigemAntecipacao.add(documentoorigem);
							}
						}
					}
					if(existDocumento){
						if(rateio != null)
							rateioService.limpaReferenciaRateio(rateio);
						documento.setRateio(rateio);
						documento.setListaDocumento(financiamentos);
						
						Money valorRateio = new Money();
						
						for (Rateioitem ri : ed.getRateio().getListaRateioitem()) {
							BigDecimal valor = SinedUtil.round(ri.getValor().getValue(), 2);
							valorRateio = valorRateio.add(new Money(valor));
						}
						
						valorRateio = documento.getValor().subtract(valorRateio);
						
						if (!valorRateio.toString().equals("0,00") && !valorRateio.toString().equals("-0,00")) {
							BigDecimal documentoValor = SinedUtil.round(documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).getValor().getValue(), 2);
							BigDecimal rateioValor = SinedUtil.round(valorRateio.getValue(), 2);
							documento.getRateio().getListaRateioitem().get(documento.getRateio().getListaRateioitem().size() - 1).setValor(new Money(documentoValor).add(new Money(rateioValor)));
						}
						documento.setEmpresa(ed.getEmpresa());
						if(!documento.getIsDescricaoHistorico()){
							documento.setDescricao("Conta a pagar referente a Entrada Fiscal: " + ed.getCdentregadocumento() + " - NF " + ed.getNumero());
						}
						documento.setFornecedor(ed.getFornecedor());
						documento.setPessoa(ed.getFornecedor());
						documento.setDtemissao(ed.getDtemissao());					
						documento.setFornecedor(ed.getFornecedor());
						documento.setDocumentotipo(ed.getDocumentotipo());
						if(ed.getDocumentotipo() != null)
							documento.setDocumentotipo(ed.getDocumentotipo());
						
						documento.setWhereInEntregadocumento(whereIn);
						
						documentoCriado = documento;
						if(!abrirTelaContaPagar){
							if(documento.getDescricao() != null && documento.getDescricao().length() > 500){
								documento.setDescricao(documento.getDescricao().substring(0, 500));
							}
							contapagarService.saveOrUpdate(documento);
							documento.setWhereInEntregadocumento(null);
						}
					}
				}
			}

			if(!abrirTelaContaPagar && listaDocumentoorigemAntecipacao != null && !listaDocumentoorigemAntecipacao.isEmpty()){
				for(Documentoorigem doco : listaDocumentoorigemAntecipacao){
					if(doco.getDocumento() != null && doco.getDocumento().getCddocumento() != null && 
							doco.getEntregadocumento() != null && doco.getEntregadocumento().getCdentregadocumento() != null &&
							!documentoorigemService.existeDocumentoorigemEntradafiscal(doco.getDocumento(), doco.getEntregadocumento())){
						documentoorigemService.saveOrUpdate(doco);
					}
				}
			}
			if(documentoCriado == null && existDocumentoAntecipacao){
				this.updateSituacaoEntradafiscal(whereIn, Entregadocumentosituacao.FATURADA);
			}
		}
		retorno.put(existDocumentoAntecipacao, documentoCriado);
			
		return retorno;
	}

	public Date getDtvencimentoMenor(List<Entregadocumento> listaEntregadocumento) {
		Date dtvencimento = null;
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				if(ed.getListadocumento() != null && !ed.getListadocumento().isEmpty()){
					for(Entregapagamento ep : ed.getListadocumento()){
						if(dtvencimento == null){
							dtvencimento = ep.getDtvencimento();
						}else if(ep.getDtvencimento() != null && SinedDateUtils.afterOrEqualsIgnoreHour(dtvencimento, ep.getDtvencimento())){
							dtvencimento = ep.getDtvencimento();
						}
					}
				}
			}
		}
		return dtvencimento;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#updateSituacaoEntradafiscal(Entregadocumento entregadocumento, Entregadocumentosituacao situacao)
	 *
	 * @param entregadocumento
	 * @param situacao
	 * @author Luiz Fernando
	 */
	public void updateSituacaoEntradafiscal(Entregadocumento entregadocumento, Entregadocumentosituacao situacao) {
		if(entregadocumento != null && entregadocumento.getCdentregadocumento() != null)
			entradafiscalDAO.updateSituacaoEntradafiscal(entregadocumento.getCdentregadocumento().toString(), situacao);
	}
	
	public void updateSituacaoEntradafiscal(String whereIn, Entregadocumentosituacao situacao) {
		entradafiscalDAO.updateSituacaoEntradafiscal(whereIn, situacao);
	}
	
	/**
	 * M�todo que preneche o documento de acordo com a entrada fiscal
	 *
	 * @param documentotipo
	 * @param documento
	 * @param entregadocumento
	 * @param entregapagamento
	 * @author Luiz Fernando
	 */
	public void preencheDocumento(Documentotipo documentotipo, Documento documento, Entregadocumento entregadocumento, Entregapagamento entregapagamento) {
		documento.setEmpresa(entregadocumento.getEmpresa());		
		
		if (parametrogeralService.getBoolean("PREENCHE_NUMNF_CAMPO_DOCUMENTO")) {
			documento.setNumero(entregadocumento.getNumero() != null ? entregadocumento.getNumero().toString() : null);
		} else {
			documento.setNumero(entregapagamento.getNumero() != null ? entregapagamento.getNumero().toString() : null);
		}
		
		documento.setDocumentotipo(documentotipo);
		documento.setDescricao("Conta a pagar referente a Entrada Fiscal: " + entregadocumento.getNumero());
		documento.setDtemissao(entregapagamento.getDtemissao());
		documento.setDtvencimento(entregapagamento.getDtvencimento());
		documento.setValor(entregapagamento.getValor());
		documento.setReferencia(Referencia.MES_ANO_CORRENTE);
		documento.setDtemissao(entregadocumento.getDtemissao());
		documento.setEntrega(entregadocumento.getEntrega());
		documento.setTipopagamento(Tipopagamento.FORNECEDOR);
		documento.setFornecedor(entregadocumento.getFornecedor());
		documento.setPessoa(entregadocumento.getFornecedor());
		documento.setPrazo(entregadocumento.getPrazopagamento());
		documento.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
		documento.setIsDescricaoHistorico(historicooperacaoService.addDescricaoEntregaByHistoricooperacao(documento, entregadocumento, entregapagamento));
		
		Date dtCompetencia = null;
		if(entregadocumento.getDtentrada() != null) {
			dtCompetencia = entregadocumento.getDtentrada();
		} else if(entregadocumento.getDtemissao() != null) {
			dtCompetencia = entregadocumento.getDtemissao();
		}
		
		documento.setDtcompetencia(dtCompetencia);
		
		if(entregadocumento.getListadocumento() != null && entregadocumento.getListadocumento().size() > 1){
			documento.setFinanciamento(Boolean.TRUE);
			documento.setRepeticoes(entregadocumento.getListadocumento().size()-1);
		}
	}
	
	/**
	 * M�todo que preenche o financiamento ao faturar entrada fiscal
	 *
	 * @param entregapagamento
	 * @param parcelas
	 * @return
	 * @author Luiz Fernando
	 */
	private Documento preencheFinanciamento(Entregapagamento entregapagamento, int parcelas, Entregadocumento entregadocumento) {
		Documento financiamento = new Documento();
		financiamento.setValor(entregapagamento.getValor());
		financiamento.setParcela(new Integer(parcelas));
		financiamento.setDtvencimento(entregapagamento.getDtvencimento());
		financiamento.setDtemissao(entregapagamento.getDtemissao());
		financiamento.setDocumentoacao(Documentoacao.PREVISTA);
		
		if (parametrogeralService.getBoolean("PREENCHE_NUMNF_CAMPO_DOCUMENTO")) {
			financiamento.setNumero(entregadocumento.getNumero() != null ? entregadocumento.getNumero().toString() : null);
		} else {
			financiamento.setNumero(entregapagamento.getNumero() != null ? entregapagamento.getNumero().toString() : null);
		}
		
		return financiamento;
	}
	
	/**
	 * M�todo que verifica se existe documento de antecipa��o em uma lista determinada
	 *
	 * @param documentoorigem
	 * @param listaDocumentoorigemAntecipacao
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existeDocumentoantecipacaoEntregadocumento(Documentoorigem documentoorigem, List<Documentoorigem> listaDocumentoorigemAntecipacao) {
		Boolean existe = Boolean.FALSE;		
		if(listaDocumentoorigemAntecipacao != null && !listaDocumentoorigemAntecipacao.isEmpty() && 
				documentoorigem != null && documentoorigem.getDocumento() != null && 
				documentoorigem.getDocumento().getCddocumento() != null && 
				documentoorigem.getEntregadocumento() != null && documentoorigem.getEntregadocumento().getCdentregadocumento() != null){
			for(Documentoorigem doco : listaDocumentoorigemAntecipacao){
				if(doco.getDocumento() != null && doco.getDocumento().getCddocumento() != null && 
						doco.getEntregadocumento() != null && doco.getEntregadocumento().getCdentregadocumento() != null)
				if(doco.getDocumento().getCddocumento().equals(documentoorigem.getCddocumentoorigem()) && 
						doco.getEntregadocumento().getCdentregadocumento().equals(documentoorigem.getEntregadocumento().getCdentregadocumento())){
					existe = Boolean.TRUE;
					break;
				}
			}
		}		
		return existe;
	}
	
	/**
	 * M�todo que c�lcula os valores de rateio das entrada fiscal
	 *
	 * @param listaEntregadocdumento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Rateioitem> calculaRateioVariasEntradafiscal(List<Entregadocumento> listaEntregadocdumento){
		List<Rateioitem> itens = new ListSet<Rateioitem>(Rateioitem.class);
		for(Entregadocumento ed : listaEntregadocdumento){
			ed.setRateio(rateioService.findRateio(ed.getRateio()));
			itens.addAll(ed.getRateio().getListaRateioitem());
		}			
		
		return itens;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#isFornecedorDiferente(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isFornecedorDiferente(String whereIn) {
		return entradafiscalDAO.isFornecedorDiferente(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForDevolucaoEntradafiscal(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForDevolucaoEntradafiscal(String whereIn) {
		return entradafiscalDAO.findForDevolucaoEntradafiscal(whereIn);
	}
	
	/**
	 * 
	 * @param filtro
	 * @author Thiago Clemente
	 * 
	 */
	public Report createReportEntradafiscalListagem(EntradafiscalFiltro filtro){
		
		List<Entregadocumento> listaListagem = findForCsv(filtro);
		
		if (listaListagem.isEmpty()){
			return null;
		}
		
		List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForPdfListagem(CollectionsUtil.listAndConcatenate(listaListagem, "cdentregadocumento", ","));
		Map<String, Entradafiscal> mapa = new HashMap<String, Entradafiscal>();
		
		for (Entregamaterial entregamaterial: listaEntregamaterial){
			
			Entregadocumento entregadocumento = entregamaterial.getEntregadocumento();
			Fornecedor fornecedor = entregadocumento.getFornecedor();
			
			Entradafiscal bean = new Entradafiscal();
			bean.setCdentregadocumento(entregadocumento.getCdentregadocumento());
			bean.setDtentrada(entregadocumento.getDtentrada());
			bean.setEspecie(entregadocumento.getModelodocumentofiscal() != null ? entregadocumento.getModelodocumentofiscal().getNome() : null);
			bean.setSerie(entregadocumento.getSerie() != null ? entregadocumento.getSerie() : "");
			bean.setNumero(entregadocumento.getNumero());
			bean.setDtdocumento(entregadocumento.getDtemissao());
			bean.setCodigoEmitente(fornecedor.getIdentificador());
			
			if (fornecedor.getListaEndereco()!=null && !fornecedor.getListaEndereco().isEmpty()){
				List<Endereco> listaEndereco = new ListSet<Endereco>(Endereco.class, fornecedor.getListaEndereco());
				if (listaEndereco.get(0).getMunicipio()!=null && listaEndereco.get(0).getMunicipio().getUf()!=null){
					bean.setUfOrigem(listaEndereco.get(0).getMunicipio().getUf().getSigla());
				}
			}
			
			Money valorContabil = new Money();
			if (entregamaterial.getQtde()!=null && entregamaterial.getValorunitario() != null){
				valorContabil = valorContabil.add(new Money(entregamaterial.getQtde() * entregamaterial.getValorunitario()));
				
				if(entregamaterial.getValoricmsst() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoricmsst());
				}
				if(entregamaterial.getValoripi() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoripi());
				}
				if(entregamaterial.getValoroutrasdespesas() != null){
					valorContabil = valorContabil.add(entregamaterial.getValoroutrasdespesas());
				}
				if(entregamaterial.getValordesconto() != null){
					valorContabil = valorContabil.subtract(entregamaterial.getValordesconto());
				}
				if(entregamaterial.getValorfrete() != null){
					valorContabil = valorContabil.add(entregamaterial.getValorfrete());
				}
				if(entregamaterial.getValorseguro() != null){
					valorContabil = valorContabil.add(entregamaterial.getValorseguro());
				}
			}
			bean.setValorContabil(valorContabil);
			
			
			String fornecedorStr = fornecedor.getNome();
			if (fornecedor.getRazaosocial()!=null && !fornecedor.getRazaosocial().trim().equals("")){
				fornecedorStr = fornecedor.getRazaosocial();
			}
			if (fornecedor.getCnpj()!=null && fornecedor.getCnpj().getValue()!=null){
				fornecedorStr += " - ";
				fornecedorStr += fornecedor.getCnpj().toString();
			}
			if (fornecedor.getInscricaoestadual()!=null && !fornecedor.getInscricaoestadual().trim().equals("")){
				fornecedorStr += " - ";
				fornecedorStr += fornecedor.getInscricaoestadual();
			}
			
			bean.setFornecedorStr(fornecedorStr);
			bean.setFiscal(entregamaterial.getCfop()!=null ? entregamaterial.getCfop().getCodigo() : null);
			bean.setCodigoValorFiscalIcms(entregamaterial.getValorFiscalIcms()!=null ? entregamaterial.getValorFiscalIcms().getCodigo() : null);
			bean.setValorBCIcms(entregamaterial.getValorbcicms()!=null ? entregamaterial.getValorbcicms() : new Money(0));
			bean.setAliquotaIcms(entregamaterial.getIcms()!=null ? entregamaterial.getIcms() : 0D);
			bean.setValorCreditadoIcms(entregamaterial.getValoricms()!=null ? entregamaterial.getValoricms() : new Money(0));
			bean.setCodigoValorFiscalIpi(entregamaterial.getValorFiscalIpi()!=null ? entregamaterial.getValorFiscalIpi().getCodigo() : null);
			bean.setValorBCIpi(entregamaterial.getValorbcipi()!=null ? entregamaterial.getValorbcipi() : new Money(0));
			bean.setAliquotaIpi(entregamaterial.getIpi()!=null ? entregamaterial.getIpi() : 0d);
			bean.setValorCreditadoIpi(entregamaterial.getValoripi()!=null ? entregamaterial.getValoripi(): new Money(0));
			
			Entradafiscal beanAux = mapa.get(bean.getKey());
			if (beanAux==null){
				mapa.put(bean.getKey(), bean);
			}else {
				beanAux.somarCampos(bean);
				mapa.put(bean.getKey(), beanAux);	
			}			
		}
		
		List<Entradafiscal> listaRelatorio = new ArrayList<Entradafiscal>(mapa.values());
		
		Collections.sort(listaRelatorio, new Comparator<Entradafiscal>() {
			@Override
			public int compare(Entradafiscal o1, Entradafiscal o2) {
				Date dtentrada1 = o1.getDtentrada();
				Date dtentrada2 = o2.getDtentrada();
				int compCdentregadocumento = o1.getCdentregadocumento().compareTo(o2.getCdentregadocumento());
				if (dtentrada1==null && dtentrada2==null){
					return compCdentregadocumento;
				}else if (dtentrada1!=null && dtentrada2==null){
					return -1;
				}else if (dtentrada1==null && dtentrada2!=null){
					return 1;
				}else {
					int compDtentrada = dtentrada1.compareTo(dtentrada2);
					if (compDtentrada==0){
						return compCdentregadocumento;
					}else {
						return compDtentrada;
					}
				}
			}
		});
		
		Report report = new Report("/fiscal/entradafiscal");
		
		Empresa empresa = filtro.getEmpresa();
		if(empresa != null){
			empresa = empresaService.loadComArquivo(empresa);
		} else {
			empresa = empresaService.loadPrincipal();
		}
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa.getRazaosocialOuNome());
		report.addParameter("CNPJ", empresa.getCnpj() != null ? empresa.getCnpj().toString() : "");
		report.addParameter("INSCRICAOESTADUAL", empresa.getInscricaoestadual());
		
		report.addParameter("TITULO", "ENTRADA FISCAL");
		report.addParameter("PERIODOENTRADA", SinedUtil.getDescricaoPeriodo(filtro.getDtentrada1(), filtro.getDtentrada2()));
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.setDataSource(listaRelatorio);
		
		return report;
	}
	
	/**
	 * M�todo que seta os valores do fornecedor ao importar xml
	 *
	 * @param f
	 * @param x
	 * @return
	 * @author Luiz Fernando
	 */
	public ConferenciaDadosFornecedorBean setaDadosConferenciaFornecedor(Fornecedor f, ImportacaoXmlNfeBean x) {
		ConferenciaDadosFornecedorBean bean = new ConferenciaDadosFornecedorBean();
		
		bean.setTipopessoa_xml(x.getCnpj() != null ? Tipopessoa.PESSOA_JURIDICA : Tipopessoa.PESSOA_FISICA);
		bean.setCnpj_xml(x.getCnpj());
		bean.setCpf_xml(x.getCpf());
		bean.setRazaosocial_xml(x.getRazaosocial());
		bean.setNomefantasia_xml(x.getNomefantasia());
		bean.setIe_xml(x.getIe());
		bean.setIm_xml(x.getIm());
		if (x.getCuf() != null)
			bean.setUf_xml(ufService.findForCodigo(x.getCuf()));
		
		if(f != null){
			bean.setCdpessoa(f.getCdpessoa());
			bean.setTipopessoa_bd(f.getTipopessoa());
			bean.setCnpj_bd(f.getCnpj());
			bean.setCpf_bd(f.getCpf());
			bean.setRazaosocial_bd(f.getRazaosocial());
			bean.setNomefantasia_bd(f.getNome());
			bean.setIe_bd(f.getInscricaoestadual());
			bean.setIm_bd(f.getInscricaomunicipal());
		}
		
		return bean;
	}
	
	public void setaValoresImportacao(Entregadocumento entregadocumento, ImportacaoXmlNfeBean bean) {
		this.setaValoresImportacao(entregadocumento, bean, true);
	}
	
	/**
	 * M�todo que seta os valores da entrada fiscal com origem de xml/chave de acesso
	 *
	 * @param entregadocumento
	 * @param bean
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void setaValoresImportacao(Entregadocumento entregadocumento, ImportacaoXmlNfeBean bean, Boolean geraPagamentoByXml) {
		entregadocumento.setEmitente(Entregadocumentoemitente.TERCEIROS);
		entregadocumento.setDtemissao(bean.getDtemissao());
//		entregadocumento.setValorbcicms(bean.getValorbcicms() != null ? bean.getValorbcicms() : new Money());
//		entregadocumento.setValoricms(bean.getValoricms() != null ? bean.getValoricms() : new Money());
//		entregadocumento.setValorbcicmsst(bean.getValorbcicmsst() != null ? bean.getValorbcicmsst() : new Money());
		entregadocumento.setValoricmsst(bean.getTotalicmsst() != null ? bean.getTotalicmsst() : new Money());
		entregadocumento.setValorseguro(bean.getTotalseguro() != null ? bean.getTotalseguro() : new Money());
		entregadocumento.setValorfrete(bean.getTotalfrete() != null ? bean.getTotalfrete() : new Money());
		entregadocumento.setValordesconto(bean.getTotaldesconto() != null ? bean.getTotaldesconto() : new Money());
		entregadocumento.setValoripi(bean.getTotalipi() != null ? bean.getTotalipi() : new Money());
//		entregadocumento.setValorpis(bean.getValorpis() != null ? bean.getValorpis() : new Money());
//		entregadocumento.setValorcofins(bean.getValorcofins() != null ? bean.getValorcofins() : new Money());
		entregadocumento.setValoroutrasdespesas(bean.getTotaloutrasdespesas() != null ? bean.getTotaloutrasdespesas() : new Money());
		entregadocumento.setChaveacesso(bean.getChaveacesso());
		entregadocumento.setTransportadora(bean.getTransportadora());
		entregadocumento.setPlacaveiculo(bean.getPlacaveiculo());
		if(entregadocumento.getEmpresa() == null && bean.getEmpresa() != null){
			entregadocumento.setEmpresa(bean.getEmpresa());
		}
		
		Fornecedor fornecedor = entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null ? 
				fornecedorService.loadForTributacao(entregadocumento.getFornecedor()) : null;
				
		if(entregadocumento.getNaturezaoperacao() == null && fornecedor != null && fornecedor.getNaturezaoperacao() != null){
			entregadocumento.setNaturezaoperacao(fornecedor.getNaturezaoperacao());
		}
		if(entregadocumento.getNaturezaoperacao() == null){
			entregadocumento.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
		}
				
		Endereco enderecoempresa = null; 
		if (entregadocumento.getEmpresa() != null && entregadocumento.getEmpresa().getCdpessoa() != null){
			Empresa empresa = empresaService.loadWithEndereco(entregadocumento.getEmpresa());
			enderecoempresa = empresa.getEndereco();
		} else {
			Empresa empresa = empresaService.loadPrincipalWithEndereco();
			if(empresa != null){
				enderecoempresa = empresa.getEndereco();
			}
		}		
		
		Endereco enderecofornecedor = null;
		if(fornecedor != null){
			enderecofornecedor = fornecedor.getEndereco();
		}
		
		List<Categoria> listaCategoriaFornecedor = null;
		if(fornecedor != null){
			listaCategoriaFornecedor = categoriaService.findByPessoa(fornecedor);
		}
		
		List<ImportacaoXmlNfeItemBean> listaItens = bean.getListaItens();
		Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
		
		Money valortotaldesconto = new Money();
		Money valortotalicmsst = new Money();
		Money valortotalfcpst = new Money();
		
		if(SinedUtil.isListNotEmpty(listaItens)){
			HashMap<Integer, Double> mapOCMQtde = new HashMap<Integer, Double>();
			Set<Entregamaterial> listaNova = new ListSet<Entregamaterial>(Entregamaterial.class);
			
			Collections.sort(listaItens, new Comparator<ImportacaoXmlNfeItemBean>(){
				public int compare(ImportacaoXmlNfeItemBean f1, ImportacaoXmlNfeItemBean f2) {
					if(f1.getQtde() == null || f2.getQtde() == null) return -1;
					return f2.getQtde().compareTo(f1.getQtde());
				}
			});
			
			Integer controle = 0;
			for (ImportacaoXmlNfeItemBean it : listaItens) {
				Entregamaterial entregamaterial = new Entregamaterial();
				setIdentificadorinterno(entregamaterial, controle);
				controle++;
				
				if(StringUtils.isNotEmpty(it.getWhereInOrdemcompramaterial())){
					for (Entregamaterial em : listaEntregamaterial) {
						boolean achou = false;
						if(bean.getAgrupamentoMaterial() != null && bean.getAgrupamentoMaterial()){ 
							if( (it.getWhereInOrdemcompramaterial() != null && it.getWhereInOrdemcompramaterial().equals(em.getWhereInOrdemcompramaterial())) || 
								  (it.getWhereInOrdemcompramaterial() == null && em.getSequencial() != null && em.getSequencial().equals(it.getSequencial())) ){
								achou = true;
							}
						}else if(em.getOcm() != null && em.getOcm().getCdordemcompramaterial() != null){
							for(String idOCM : it.getWhereInOrdemcompramaterial().split(",")){
								if(idOCM.equals(em.getOcm().getCdordemcompramaterial().toString())){
									achou = true;
									break;
								}
							}
						}
						
						if(achou){
							entregamaterial.setMaterial(em.getMaterial());
							if(bean.getAgrupamentoMaterial() == null || !bean.getAgrupamentoMaterial()){
									entregamaterial.setOcm(em.getOcm());
							}
							entregamaterial.setLocalarmazenagem(em.getLocalarmazenagem());
							entregamaterial.setProjeto(em.getProjeto());
							entregamaterial.setCentrocusto(em.getCentrocusto());
							entregamaterial.setValoripi(em.getValoripi());
							entregamaterial.setUnidademedidacomercial(em.getUnidademedidacomercial());
							entregamaterial.setFaturamentocliente(em.getFaturamentocliente());
							break;
						}
					}
				}
				
				if(SinedUtil.isListNotEmpty(it.getListaRastro())){
					if(it.getListaRastro().size() == 1){
						entregamaterial.setTipolote(true);
						Loteestoque loteestoque = it.getListaRastro().get(0).getLoteestoque();
						if(Util.objects.isPersistent(loteestoque)){
							entregamaterial.setLoteestoque(loteestoqueService.loadWithNumeroValidade(loteestoque, it.getMaterial(), false));
						}
					}else{
						entregamaterial.setTipolote(false);
						List<Entregamateriallote> listaLotes = new ArrayList<Entregamateriallote>();
						for(Rastro rastro: it.getListaRastro()){
							if(Util.objects.isPersistent(rastro.getLoteestoque())){
								Entregamateriallote lote = new Entregamateriallote();
								
								lote.setQtde(rastro.getqLote());
								lote.setLoteestoque(loteestoqueService.loadWithNumeroValidade(rastro.getLoteestoque(), it.getMaterial(), false));
								
								listaLotes.add(lote);
							}
						}
						entregamaterial.setListaEntregamateriallote(CollectionsUtil.listToSet(Entregamateriallote.class, listaLotes));
					}
				}
					
				entregamaterial.setMaterial(it.getMaterial());
				entregamaterial.setQtde(it.getQtde());
				if(entregamaterial.getOrigemMaterialmestregrade() == null || !entregamaterial.getOrigemMaterialmestregrade())
					entregamaterial.setValorunitario(it.getValorunitario());
				entregamaterial.setCprod(it.getCprod());
				entregamaterial.setXprod(Util.strings.truncate(it.getXprod(), 120));
				entregamaterial.setCfop(cfopService.verificaTrocaCfop(it.getCfop()));
				entregamaterial.setValortotal(SinedUtil.round(entregamaterial.getQtde() * entregamaterial.getValorunitario(), 2));
				entregamaterial.setValorfrete(MoneyUtils.zeroWhenNull(it.getValorfrete()));
				entregamaterial.setValordesconto(MoneyUtils.zeroWhenNull(it.getValordesconto()));
				if(it.getValordesconto() != null){
					valortotaldesconto = valortotaldesconto.add(it.getValordesconto());
				}
				entregamaterial.setValorseguro(MoneyUtils.zeroWhenNull(it.getValorseguro()));
				entregamaterial.setValoroutrasdespesas(it.getValoroutrasdespesas());
				entregamaterial.setValoriss(MoneyUtils.zeroWhenNull(entregamaterial.getValoriss()));
				
				boolean verificarConversaoQtde = false; 
				if(it.getUnidademedidaAssociada() != null){
					entregamaterial.setUnidademedidacomercial(it.getUnidademedidaAssociada());
					verificarConversaoQtde = true;
				}
				
				entregamaterial.setCsticms(it.getCsticms());
				entregamaterial.setValorbcicms(it.getValorbcicms());
				entregamaterial.setIcms(it.getIcms());
				entregamaterial.setValoricms(MoneyUtils.zeroWhenNull(it.getValoricms()));
				
				entregamaterial.setValorIcmsDesonerado(it.getValorIcmsDeson());
				entregamaterial.setMotivoDesoneracaoIcms(it.getMotDesICMS());
				
				if (it.getValorBcStRet() != null || it.getSt() != null || it.getValorIcmsStRet() != null) {
					entregamaterial.setValorbcicmsst(it.getValorBcStRet());
					entregamaterial.setIcmsst(it.getSt());
					entregamaterial.setValoricmsst(it.getValorIcmsStRet());
				} else {
					entregamaterial.setValorbcicmsst(it.getValorbcicmsst());
					entregamaterial.setIcmsst(it.getIcmsst());
					entregamaterial.setValoricmsst(it.getValoricmsst());
				}
				
				if(it.getValoricmsst() != null){
					valortotalicmsst = valortotalicmsst.add(it.getValoricmsst());
				}
				
				entregamaterial.setValormva(it.getValormva() != null ? it.getValormva().getValue().doubleValue() : null);
				
				entregamaterial.setValorbcfcp(it.getValorBcFcp());
				entregamaterial.setFcp(it.getFcp());
				entregamaterial.setValorfcp(it.getValorFcp());
				
				if (it.getValorBcFcpStRet() != null || it.getFcpStRet() != null || it.getValorFcpStRet() != null) {
					entregamaterial.setValorbcfcpst(it.getValorBcFcpStRet());
					entregamaterial.setFcpst(it.getFcpStRet());
					entregamaterial.setValorfcpst(it.getValorFcpStRet());
				} else {
					entregamaterial.setValorbcfcpst(it.getValorBcFcpSt());
					entregamaterial.setFcpst(it.getFcpSt());
					entregamaterial.setValorfcpst(it.getValorFcpSt());
				}
				
				if(it.getValorFcpSt() != null){
					valortotalfcpst = valortotalfcpst.add(it.getValorFcpSt());
				}
				
				entregamaterial.setCstipi(it.getCstipi());
				entregamaterial.setValorbcipi(it.getValorbcipi());
				entregamaterial.setIpi(it.getIpi());
				entregamaterial.setValoripi(MoneyUtils.zeroWhenNull(it.getValoripi()));

				entregamaterial.setCstpis(it.getCstpis());
				entregamaterial.setValorbcpis(it.getValorbcpis());
				entregamaterial.setPis(it.getPis());
				entregamaterial.setQtdebcpis(it.getQtdebcpis());
				entregamaterial.setValorunidbcpis(it.getValorunidbcpis());
				entregamaterial.setValorpis(MoneyUtils.zeroWhenNull(it.getValorpis()));
				
				entregamaterial.setCstcofins(it.getCstcofins());
				entregamaterial.setValorbccofins(it.getValorbccofins());
				entregamaterial.setCofins(it.getCofins());
				entregamaterial.setQtdebccofins(it.getQtdebccofins());
				entregamaterial.setValorunidbccofins(it.getValorunidbccofins());
				entregamaterial.setValorcofins(MoneyUtils.zeroWhenNull(it.getValorcofins()));
				
				entregamaterial.setValorbcii(it.getValorbcii());
				entregamaterial.setValoriiItem(it.getValorii());
				entregamaterial.setValorDespesasAduaneiras(it.getValorDespesasAduaneiras());
				entregamaterial.setIof(it.getIof());
				entregamaterial.setPedidovendamaterial(it.getPedidovendamaterial());
				entregamaterial.setSequenciaitem(controle);

				if(entregamaterial.getMaterial() != null){
					Material beanMaterial = materialService.loadForTributacao(entregamaterial.getMaterial());
					if(beanMaterial != null){
						if(entregamaterial.getMaterial().getUnidademedida() == null){
							entregamaterial.getMaterial().setUnidademedida(beanMaterial.getUnidademedida());
						}
						if(entregamaterial.getUnidademedidacomercial() == null && beanMaterial.getUnidademedida() != null){
							entregamaterial.setUnidademedidacomercial(beanMaterial.getUnidademedida());
						}
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
														entregadocumento.getEmpresa(), 
														entregadocumento.getNaturezaoperacao(), 
														beanMaterial.getMaterialgrupo(),
														null,
														(beanMaterial.getServico() == null || !beanMaterial.getServico()), 
														beanMaterial, 
														beanMaterial.getNcmcompleto(),
														null, 
														enderecofornecedor,
														null,
														listaCategoriaFornecedor,
														null,
														null, 
														null,
														entregadocumento.getDtemissao()));
						entregamaterial.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					}
					
					entregamaterial = loadTributacao(entregamaterial, entregadocumento.getEmpresa(), null, entregadocumento.getFornecedor(), enderecofornecedor, enderecoempresa);
					
					// caso a origem no grupo de tributa��o seja null, procura no xml. Caso n�o encontre, procura no cadastro de material
					if(entregamaterial.getOrigemProduto() == null) {
						if(it.getOrigemProduto() != null) {
							entregamaterial.setOrigemProduto(it.getOrigemProduto());
						} else if(beanMaterial.getOrigemproduto() != null) {
							entregamaterial.setOrigemProduto(beanMaterial.getOrigemproduto());
						}
					}
					
					grupotributacaoService.calcularValoresFormulaIcms(entregamaterial, entregamaterial.getMaterial().getGrupotributacaoNota() != null ? entregamaterial.getMaterial().getGrupotributacaoNota() : entregamaterial.getGrupotributacao());
					grupotributacaoService.calcularValoresFormulaIcmsST(entregamaterial, entregamaterial.getMaterial().getGrupotributacaoNota() != null ? entregamaterial.getMaterial().getGrupotributacaoNota() : entregamaterial.getGrupotributacao());
				}
				
				List<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
				if(SinedUtil.isListNotEmpty(it.getListaItemAssociado())){
					Double qtdeTotalEntrega = null;
					for(ImportacaoXmlNfeItemAssociadoBean itAssociado : it.getListaItemAssociado()){
						if(StringUtils.isNotEmpty(itAssociado.getWhereInOrdemcompramaterial())){
							List<Ordemcompramaterial> listaOrdemcompramaterial = ordemcompramaterialService.findWithOrdemcompra(itAssociado.getWhereInOrdemcompramaterial());
							for(String idOCM : itAssociado.getWhereInOrdemcompramaterial().split(",")){
								if(itAssociado.getQtdeassociada() != null){
									Ordemcompraentregamaterial ordemcompraentregamaterial = new Ordemcompraentregamaterial();
									ordemcompraentregamaterial.setOrdemcompramaterial(new Ordemcompramaterial(Integer.parseInt(idOCM)));
									ordemcompraentregamaterial.setQtde(itAssociado.getQtdeassociada());
									ordemcompraentregamaterial.getOrdemcompramaterial().setOrdemcompra(ordemcompramaterialService.getOrdemcompra(listaOrdemcompramaterial, ordemcompraentregamaterial.getOrdemcompramaterial()));
									listaOrdemcompraentregamaterial.add(ordemcompraentregamaterial);
									
									if(mapOCMQtde.get(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial()) == null){
										Entregamaterial em_aux = new Entregamaterial();
										em_aux.setOcm(ordemcompraentregamaterial.getOrdemcompramaterial());
										Double qtdeOCM = entregamaterialService.getQtdeByEntregaMaterial(em_aux);
										if(verificarConversaoQtde && qtdeOCM != null){
											if(entregamaterial.getCdentregamaterial() == null && entregamaterial.getUnidademedidacomercial() != null && entregamaterial.getMaterial() != null && 
													ordemcompraentregamaterial.getOrdemcompramaterial() != null &&
													ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida() != null && 
													!ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida().equals(entregamaterial.getUnidademedidacomercial())){
												Double fracao = unidademedidaService.getFatorconversao(ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida(), itAssociado.getQtdeassociada(), entregamaterial.getUnidademedidacomercial(), entregamaterial.getMaterial(), null, 1d);
												if(fracao != null){
													qtdeOCM = qtdeOCM * fracao;
												}
											}
										}
										mapOCMQtde.put(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial(), qtdeOCM);
										if(qtdeOCM != null){
											if(qtdeTotalEntrega == null) qtdeTotalEntrega = 0d;
											Double diferenca = entregamaterial.getQtde() != null ? qtdeTotalEntrega+qtdeOCM - entregamaterial.getQtde() : null;
											if(entregamaterial.getQtde() == null || (qtdeTotalEntrega+qtdeOCM <= entregamaterial.getQtde())){
												qtdeTotalEntrega+= qtdeOCM;
											}else if(qtdeTotalEntrega < entregamaterial.getQtde() && diferenca != null && diferenca > 0 && (qtdeOCM-diferenca) > 0){
												qtdeTotalEntrega+= qtdeOCM - diferenca;
											}
										}
										ordemcompraentregamaterial.setAux_qtde(qtdeOCM);
									}
								}
							}
						}
					}
					if(bean.getAgrupamentoMaterial() != null && bean.getAgrupamentoMaterial()){
						int qtdeItensAssociados = listaOrdemcompraentregamaterial.size();
						if(qtdeItensAssociados > 0){
							Collections.sort(listaOrdemcompraentregamaterial, new Comparator<Ordemcompraentregamaterial>(){
								public int compare(Ordemcompraentregamaterial f1, Ordemcompraentregamaterial f2) {
									if(f1.getAux_qtde() == null || f2.getAux_qtde() == null) return -1;
									return f2.getAux_qtde().compareTo(f1.getAux_qtde());
								}
							});
							
							Ordemcompraentregamaterial ordemcompraentregamaterial = listaOrdemcompraentregamaterial.iterator().next(); 
							if(ordemcompraentregamaterial.getQtde() != null){
								if(qtdeItensAssociados > 1){
									Double qtdeEntregamaterial = qtdeTotalEntrega != null ? qtdeTotalEntrega : ordemcompraentregamaterial.getQtde();
									if(qtdeEntregamaterial != null){
										for(Ordemcompraentregamaterial ocem : listaOrdemcompraentregamaterial){
											Double qtdeOCM = mapOCMQtde.get(ocem.getOrdemcompramaterial().getCdordemcompramaterial());
											if(qtdeOCM != null){
												if(qtdeEntregamaterial <= 0d){
													ocem.setQtde(0d);
												}else if(qtdeEntregamaterial >= qtdeOCM){
													ocem.setQtde(qtdeOCM);
													qtdeEntregamaterial = qtdeEntregamaterial - qtdeOCM;
													mapOCMQtde.put(ocem.getOrdemcompramaterial().getCdordemcompramaterial(), 0d);
												}else {
													ocem.setQtde(qtdeEntregamaterial);
													mapOCMQtde.put(ocem.getOrdemcompramaterial().getCdordemcompramaterial(), qtdeOCM-qtdeEntregamaterial);
													qtdeEntregamaterial -= qtdeOCM;
												}
											}
										}
									}
								}else if(mapOCMQtde.get(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial()) != null){
									mapOCMQtde.put(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial(), mapOCMQtde.get(ordemcompraentregamaterial.getOrdemcompramaterial().getCdordemcompramaterial())-ordemcompraentregamaterial.getQtde());
								}
							}
						}
					}
				}
				entregamaterial.setListaOrdemcompraentregamaterial(SinedUtil.listToSet(listaOrdemcompraentregamaterial, Ordemcompraentregamaterial.class));
				listaNova.add(entregamaterial);
			}
			entregadocumento.setListaEntregamaterial(listaNova);
			entregadocumento.setValordocumento(entregadocumento.getValortotaldocumento());
		}else {
			for (Entregamaterial em : listaEntregamaterial) {
				for (ImportacaoXmlNfeItemBean it : listaItens) {
					if( (it.getWhereInOrdemcompramaterial() != null && it.getWhereInOrdemcompramaterial().equals(em.getWhereInOrdemcompramaterial())) || 
						(it.getWhereInOrdemcompramaterial() == null && em.getSequencial() != null && em.getSequencial().equals(it.getSequencial())) ){
						
						if(it.getMaterial() != null){
							em.setMaterial(it.getMaterial());
						}
						em.setQtde(it.getQtde());
						if(em.getOrigemMaterialmestregrade() == null || !em.getOrigemMaterialmestregrade())
							em.setValorunitario(it.getValorunitario());
						em.setCprod(it.getCprod());
						em.setXprod(Util.strings.truncate(it.getXprod(), 120));
						em.setCfop(cfopService.verificaTrocaCfop(it.getCfop()));
						em.setValortotal(SinedUtil.round(em.getQtde() * em.getValorunitario(), 2));
						em.setValorfrete(it.getValorfrete());
						em.setValordesconto(it.getValordesconto());
						if(it.getValordesconto() != null){
							valortotaldesconto = valortotaldesconto.add(it.getValordesconto());
						}
						em.setValorseguro(it.getValorseguro());
						em.setValoroutrasdespesas(it.getValoroutrasdespesas());
						
						em.setUnidademedidacomercial(it.getUnidademedidaAssociada());
						
						em.setCsticms(it.getCsticms());
						em.setValorbcicms(it.getValorbcicms());
						em.setIcms(it.getIcms());
						em.setValoricms(it.getValoricms());
						
						if (it.getValorBcStRet() != null || it.getSt() != null || it.getValorIcmsStRet() != null) {
							em.setValorbcicmsst(it.getValorBcStRet());
							em.setIcmsst(it.getSt());
							em.setValoricmsst(it.getValorIcmsStRet());
						} else {
							em.setValorbcicmsst(it.getValorbcicmsst());
							em.setIcmsst(it.getIcmsst());
							em.setValoricmsst(it.getValoricmsst());
						}
						
						if(it.getValoricmsst() != null){
							valortotalicmsst = valortotalicmsst.add(it.getValoricmsst());
						}
						
						em.setValormva(it.getValormva() != null ? it.getValormva().getValue().doubleValue() : null);
						
						em.setValorbcfcp(it.getValorBcFcp());
						em.setFcp(it.getFcp());
						em.setValorfcp(it.getValorFcp());
						
						if (it.getValorBcFcpStRet() != null || it.getFcpStRet() != null || it.getValorFcpStRet() != null) {
							em.setValorbcfcpst(it.getValorBcFcpStRet());
							em.setFcpst(it.getFcpStRet());
							em.setValorfcpst(it.getValorFcpStRet());
						} else {
							em.setValorbcfcpst(it.getValorBcFcpSt());
							em.setFcpst(it.getFcpSt());
							em.setValorfcpst(it.getValorFcpSt());
						}
						
						if(it.getValorFcpSt() != null){
							valortotalfcpst = valortotalfcpst.add(it.getValorFcpSt());
						}
						
						em.setCstipi(it.getCstipi());
						em.setValorbcipi(it.getValorbcipi());
						em.setIpi(it.getIpi());
						em.setValoripi(it.getValoripi());

						em.setCstpis(it.getCstpis());
						em.setValorbcpis(it.getValorbcpis());
						em.setPis(it.getPis());
						em.setQtdebcpis(it.getQtdebcpis());
						em.setValorunidbcpis(it.getValorunidbcpis());
						em.setValorpis(it.getValorpis());
						
						em.setCstcofins(it.getCstcofins());
						em.setValorbccofins(it.getValorbccofins());
						em.setCofins(it.getCofins());
						em.setQtdebccofins(it.getQtdebccofins());
						em.setValorunidbccofins(it.getValorunidbccofins());
						em.setValorcofins(it.getValorcofins());
						
						em.setValorbcii(it.getValorbcii());
						em.setValoriiItem(it.getValorii());
						em.setValorDespesasAduaneiras(it.getValorDespesasAduaneiras());
						em.setIof(it.getIof());

						if(em.getMaterial() != null){
							Material beanMaterial = materialService.loadForTributacao(em.getMaterial());
							if(beanMaterial != null){
								if(em.getUnidademedidacomercial() == null && beanMaterial.getUnidademedida() != null){
									em.setUnidademedidacomercial(beanMaterial.getUnidademedida());
								}
								List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
										grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
																entregadocumento.getEmpresa(), 
																null, 
																beanMaterial.getMaterialgrupo(),
																null,
																(beanMaterial.getServico() == null || !beanMaterial.getServico()), 
																beanMaterial, 
																beanMaterial.getNcmcompleto(),
																null, 
																enderecofornecedor,
																null,
																listaCategoriaFornecedor,
																null,
																null,
																null,
																entregadocumento.getDtemissao()));
								em.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
							}
							
							em = loadTributacao(em, entregadocumento.getEmpresa(), null, entregadocumento.getFornecedor(), enderecofornecedor, enderecoempresa);
							
							grupotributacaoService.calcularValoresFormulaIcms(em, em.getMaterial().getGrupotributacaoNota() != null ? em.getMaterial().getGrupotributacaoNota() : em.getGrupotributacao());
							grupotributacaoService.calcularValoresFormulaIcmsST(em, em.getMaterial().getGrupotributacaoNota() != null ? em.getMaterial().getGrupotributacaoNota() : em.getGrupotributacao());
						}
						
						Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = em.getListaOrdemcompraentregamaterial();
						if(listaOrdemcompraentregamaterial != null && listaOrdemcompraentregamaterial.size() > 0){
							if(listaOrdemcompraentregamaterial.size() == 1){
								listaOrdemcompraentregamaterial.iterator().next().setQtde(it.getQtde());
							} else {
								Double qtde = 0d;
								for (Ordemcompraentregamaterial ordemcompraentregamaterial : listaOrdemcompraentregamaterial) {
									if(ordemcompraentregamaterial.getQtde() != null)
										qtde += ordemcompraentregamaterial.getQtde();
								}
								em.setQtde(qtde);
							}
						}
						
						break;
					}
				}
			}
		}
		
		entregadocumento.setValordesconto(valortotaldesconto);
		if(valortotalicmsst != null && valortotalicmsst.compareTo(entregadocumento.getValoricmsst()) > 0){
			entregadocumento.setValoricmsst(valortotalicmsst);
		}
		if(valortotalfcpst != null && entregadocumento.getValorfcpst() != null && valortotalfcpst.compareTo(entregadocumento.getValorfcpst()) > 0){
			entregadocumento.setValorfcpst(valortotalfcpst);
		}
		if(bean.getNatop() != null)
			entregadocumento.setNatop(bean.getNatop());
		entregadocumento.setIndpag(bean.getIndpag());
		entregadocumento.setModelodocumentofiscal(bean.getModelodocumentofiscal());
		entregadocumento.setSerie(bean.getSerie() != null ? String.valueOf(bean.getSerie()) : null);
		entregadocumento.setNumero(bean.getNumero());
		if(entregadocumento.getDtemissao() != null){
			entregadocumento.setDtentrada(entregadocumento.getDtemissao());
		}else {
			entregadocumento.setDtentrada(bean.getDsaient());
		}
		
		if(parametrogeralService.getBoolean(Parametrogeral.RECEBIMENTO_ENTRADA_DATA_CORRENTE) && entregadocumento.getCdentregadocumento() == null){
			entregadocumento.setDtentrada(SinedDateUtils.currentDate());
		}
		
		entregadocumento.setInfoadicionalfisco(bean.getInfadfisco());
		entregadocumento.setInfoadicionalcontrib(bean.getInfcpl());
		entregadocumento.setResponsavelfrete(bean.getResponsavelFrete());
		
		if(SinedUtil.isListNotEmpty(bean.getListaDuplicatas()) && Boolean.TRUE.equals(geraPagamentoByXml)){
			List<Entregapagamento> listaDocumento = new ArrayList<Entregapagamento>();
			Entregapagamento entregapagamento;
			Integer qtdeParcelas = null;
			Money totalDuplicatas = new Money();
			
			for(ImportacaoXmlNfeDuplicataBean item : bean.getListaDuplicatas()){
				qtdeParcelas = item.getSequencial() + 1;
				
				entregapagamento = new Entregapagamento();
				entregapagamento.setNumero(item.getNumero());
				entregapagamento.setDtvencimento(item.getDtvencimento());
				entregapagamento.setValor(item.getValor());
				listaDocumento.add(entregapagamento);
				
				totalDuplicatas = totalDuplicatas.add(item.getValor());
			}
			
			// ajuste na parcela gerada na importa��o para se adequar a valida��o que ocorre na tela, onde o valor da receita n�o pode ser maior do que o valor do pagamento.
			Money totalReceita = entregadocumento.getValortotaldocumentoreceita();
			if(totalDuplicatas != null && totalReceita != null && totalReceita.doubleValue() < totalDuplicatas.doubleValue()) {
				for(Entregapagamento parcela : listaDocumento) {
					parcela.setValor(totalReceita.divide(new Money(qtdeParcelas.doubleValue())));
				}
			}
			
			entregadocumento.setListadocumento(SinedUtil.listToSet(listaDocumento, Entregapagamento.class));
			
			if(qtdeParcelas != null && qtdeParcelas > 0){
				List<Prazopagamento> listaPrazopagamento = prazopagamentoService.findByQtdeParcelas(qtdeParcelas);
				if(SinedUtil.isListNotEmpty(listaPrazopagamento)){
					entregadocumento.setPrazopagamento(listaPrazopagamento.get(0));
				}
			}
		}
	}
	
	
	/**
	 * M�todo que ajusta a Entregadocumento com a qtde correta para registrar devolu��o.
	 *
	 * @param listaEd
	 * @author Luiz Fernando
	 */
	public void ajustaEntradaQtdeJaDevolvida(List<Entregadocumento> listaEd) {
		if(listaEd != null && !listaEd.isEmpty()){
			StringBuilder whereIn = new StringBuilder();
			for(Entregadocumento ed : listaEd){
				if(!"".equals(whereIn.toString())) whereIn.append(",");
				whereIn.append(ed.getCdentregadocumento());
			}
			if(!"".equals(whereIn.toString())){
				List<Entregadocumentohistorico> listaEdh = entregadocumentohistoricoService.findForDevolucaoEntregadocumento(whereIn.toString());
				if(listaEdh != null && !listaEdh.isEmpty()){
					for(Entregadocumento ed : listaEd){
						for(Entregadocumentohistorico edh : listaEdh){
							if(edh.getNotafiscalproduto() != null && ed.equals(edh.getEntregadocumento())){
								for(Entregamaterial em : ed.getListaEntregamaterial()){
									if(em.getMaterial() != null){
										for(Notafiscalprodutoitem notaFPI : edh.getNotafiscalproduto().getListaItens()){
											if(notaFPI.getMaterial() != null && em.getMaterial().equals(notaFPI.getMaterial())){
												em.setQtde(em.getQtde()-notaFPI.getQtde());
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}		
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForApuracaoimposto(ApuracaoimpostoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Entregadocumento> findForApuracaoimposto(ApuracaoimpostoFiltro filtro) {
		return entradafiscalDAO.findForApuracaoimposto(filtro);
	}
	
	public void ajustarListaAposIncluirItensGrade(Entregadocumento bean, Entregamaterial entregamaterial) {
		List<Entregamaterial> lista = new ArrayList<Entregamaterial>();
		if(bean != null && SinedUtil.isListNotEmpty(bean.getListaMaterialitemgrade())){
			if(bean.getListaEntregamaterial() == null) bean.setListaEntregamaterial(new ListSet<Entregamaterial>(Entregamaterial.class));
			Integer index = null;
			try {
				index = Integer.parseInt(bean.getListaMaterialitemgrade().get(0).getIndexEntregamaterial());
			} catch (Exception e) {}
			
			Integer posicao = 0;
			for(Entregamaterial em : bean.getListaEntregamaterial()){
				if(index == null || posicao < index){
					lista.add(em);
					posicao++;
				}else {
					if(posicao == index){
						entregamaterial = em;
					}
					break;
				}
			}
			
			for (Material material : bean.getListaMaterialitemgrade()){
				Entregamaterial emNovo = null;
				if(entregamaterial != null){
					emNovo = entregamaterialService.addInfMestregrade(entregamaterial, emNovo, material, material.getQuantidade());
					emNovo.setNovoItemGrade(false);
				}else {
					emNovo = new Entregamaterial();
					emNovo.setMaterial(material);
					emNovo.setQtde(material.getQuantidade());
					emNovo.setValorunitario(material.getPreco());
					
					if(Util.objects.isPersistent(material)){
						if(!Util.objects.isPersistent(material.getUnidademedida())){
							material.setUnidademedida(unidademedidaService.findByMaterial(material));
						}
						emNovo.setUnidademedidacomercial(material.getUnidademedida());
					}
					
					emNovo.setNovoItemGrade(false);
				}
				lista.add(emNovo);
			}
			
			if(index != null){
				posicao = 0;
				for(Entregamaterial em : bean.getListaEntregamaterial()){
					if(posicao > index){
						lista.add(em);
					}
					posicao++;
				}
			}
			
			int i = 1;
			for(Entregamaterial em: bean.getListaEntregamaterial()){
				em.setSequenciaitem(i);
				i++;
			}
			
			bean.setListaEntregamaterial(SinedUtil.listToSet(lista, Entregamaterial.class));
		}
	}
	
	public void ajustarListaAposIncluirItensGrade(Entregadocumento bean) {
		if(bean != null && bean.getListaEntregamaterial() != null && !bean.getListaEntregamaterial().isEmpty()){
			Entregamaterial entregamaterial = null;
			Set<Entregamaterial> listaCorrigida = new ListSet<Entregamaterial>(Entregamaterial.class);
			for (Entregamaterial em : bean.getListaEntregamaterial()){
				if(em.getRemoverItem() == null || !em.getRemoverItem()){
					listaCorrigida.add(em);
				}else {
					entregamaterial = em;
				}
			}
			
			if(entregamaterial != null && listaCorrigida != null && !listaCorrigida.isEmpty()){
				for (Entregamaterial em : listaCorrigida){
					if(em.getNovoItemGrade() != null && em.getNovoItemGrade()){
						entregamaterialService.addInfMestregrade(entregamaterial, em, em.getMaterial(), em.getQtde());
						em.setNovoItemGrade(false);
					}
				}
				bean.setListaEntregamaterial(listaCorrigida);
				int i = 1;
				for(Entregamaterial em: bean.getListaEntregamaterial()){
					em.setSequenciaitem(i);
					i++;
				}
			}
		}
	}
	
	public MaterialgradeBean selecionarItensGrade(WebRequestContext request) {
		MaterialgradeBean materialgradeBean = criaMaterialgradeBean(request);
		if(materialgradeBean != null && materialgradeBean.getListaMaterial() != null && 
				!materialgradeBean.getListaMaterial().isEmpty()){
			if(materialgradeBean.getMaterial() != null && materialgradeBean.getMaterial().getUnidademedida() != null &&
					materialgradeBean.getMaterial().getUnidademedida().getCasasdecimaisestoque() == null){
				materialgradeBean.getMaterial().getUnidademedida().setCasasdecimaisestoque(SinedUtil.getParametroCasasDecimaisArredondamento());
			}
			for(Material material : materialgradeBean.getListaMaterial()){				
				if(!materialService.isControleMaterialgrademestre(material)){	
					material.setListaMaterialitemgrade(materialService.findMaterialitemByMaterialmestregrade(material, material.getWhereInMaterialItenGrade()));
					
					if(material.getListaMaterialitemgrade() != null && !material.getListaMaterialitemgrade().isEmpty()){
						Double qtdeItem = null;
						if(material.getQuantidade() != null && material.getQuantidade() > 0){
							qtdeItem = material.getQuantidade() / material.getListaMaterialitemgrade().size();
						}
						for (Material item : material.getListaMaterialitemgrade()) {
							item.setIndexEntregamaterial(material.getIndexEntregamaterial());
							item.setCdordemcompramaterial(material.getCdordemcompramaterial());
							item.setQuantidade(qtdeItem);
							if(item.getQuantidade() != null){
								if(item.getUnidademedida() != null && item.getUnidademedida().getCasasdecimaisestoque() != null){
									item.setQuantidade(SinedUtil.roundByUnidademedida(item.getQuantidade(), item.getUnidademedida()));
									item.setCasasdecimaisestoqueTrans(item.getUnidademedida().getCasasdecimaisestoque());
								}else {
									item.setQuantidade(SinedUtil.roundByParametro(item.getQuantidade()));
									item.setCasasdecimaisestoqueTrans(SinedUtil.getParametroCasasDecimaisArredondamento());
								}
							}
						}
					}
				}
			}
		}
		return materialgradeBean;
	}
	
	public MaterialgradeBean atualizarItensGrade(WebRequestContext request) {
		request.setAttribute("DIMENSAOVENDA_1", parametrogeralService.getDimensaovenda(0));
		request.setAttribute("DIMENSAOVENDA_2", parametrogeralService.getDimensaovenda(1));
		request.setAttribute("DIMENSAOVENDA_3", parametrogeralService.getDimensaovenda(2));
		
		String calculometroquadrado = parametrogeralService.getValorPorNome(Parametrogeral.CALCULOMETROQUADRADO);
		MaterialgradeBean materialgradeBean = criaMaterialgradeBean(request);
		if(materialgradeBean != null && materialgradeBean.getListaMaterial() != null && 
				!materialgradeBean.getListaMaterial().isEmpty()){
			for(Material material : materialgradeBean.getListaMaterial()){				
				if(!materialService.isControleMaterialgrademestre(material) || 
						(material.getWhereInMaterialItenGrade() != null && !"".equals(material.getWhereInMaterialItenGrade()))){	
					material.setListaMaterialitemgrade(materialService.findMaterialitemByMaterialmestregrade(material, material.getWhereInMaterialItenGrade()));
					
					if(material.getListaMaterialitemgrade() != null && !material.getListaMaterialitemgrade().isEmpty()){
						String whereInItenGrade = CollectionsUtil.listAndConcatenate(material.getListaMaterialitemgrade(), "cdmaterial", ",");
						List<Produto> listaProduto = null;
						if(whereInItenGrade != null && !"".equals(whereInItenGrade)){
							listaProduto = produtoService.findWithAlturaComprimentoLargura(whereInItenGrade);
						}
						
						Double qtdeTotal = 0d;
						for (Material item : material.getListaMaterialitemgrade()) {
							Produto produto = produtoService.getProduto(listaProduto, item);
							if(produto != null && (produto.getAltura() != null || produto.getComprimento() != null || produto.getLargura() != null)){
								Double fatorconversao = 1000d;
								if(produto.getFatorconversao() != null) fatorconversao = produto.getFatorconversao();
								if(produto.getComprimento() != null && produto.getComprimento() > 0)
									item.setProduto_comprimento(produto.getComprimento()/fatorconversao);
								if(produto.getAltura() != null && produto.getAltura() > 0)
									item.setProduto_altura(produto.getAltura()/fatorconversao);
								if(produto.getLargura() != null && produto.getLargura() > 0)
									item.setProduto_largura(produto.getLargura()/fatorconversao);
								item.setQuantidade(materialService.getQtdeParamCalculometroquadrado(calculometroquadrado, 
																					item.getProduto_altura(), 
																					item.getProduto_largura(),
																					item.getProduto_comprimento()));
								
								if(item.getQuantidade() != null){
									if(item.getUnidademedida() != null && item.getUnidademedida().getCasasdecimaisestoque() != null){
										item.setQuantidade(SinedUtil.roundByUnidademedida(item.getQuantidade(), item.getUnidademedida()));
										item.setCasasdecimaisestoqueTrans(item.getUnidademedida().getCasasdecimaisestoque());
									}else {
										item.setQuantidade(SinedUtil.roundByParametro(item.getQuantidade()));
										item.setCasasdecimaisestoqueTrans(SinedUtil.getParametroCasasDecimaisArredondamento());
									}
									qtdeTotal += item.getQuantidade();
								}
							}
							item.setIndexEntregamaterial(material.getIndexEntregamaterial());
							item.setCdordemcompramaterial(material.getCdordemcompramaterial());
						}
						if(qtdeTotal != null){
							if(material.getUnidademedida() != null && material.getUnidademedida().getCasasdecimaisestoque() != null){
								qtdeTotal = (SinedUtil.roundByUnidademedida(qtdeTotal, material.getUnidademedida()));
								material.setCasasdecimaisestoqueTrans(material.getUnidademedida().getCasasdecimaisestoque());
							}else {
								qtdeTotal = (SinedUtil.roundByParametro(qtdeTotal));
								material.setCasasdecimaisestoqueTrans(SinedUtil.getParametroCasasDecimaisArredondamento());
							}
						}
						material.setQuantidade(qtdeTotal);
					}
				}
			}
		}
		request.setAttribute("calculometroquadrado", calculometroquadrado);
		return materialgradeBean;
	}
	
	public ModelAndView salvarAtualizacaoItensGrade(WebRequestContext request, MaterialgradeBean materialgradeBean) {
		if(materialgradeBean.getListaMaterial() == null || materialgradeBean.getListaMaterial().isEmpty()){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script type=\"text/javascript\">alert('Nenhum item informado!');parent.$.akModalRemove(true);</script>");
			return null;
		}
		
		Double qtde = 0d;
		String indexEntregamaterial = "";
		try {
			if(materialgradeBean.getListaMaterial() != null && !materialgradeBean.getListaMaterial().isEmpty()){
				for(Material material : materialgradeBean.getListaMaterial()){
					if(material.getListaMaterialitemgrade() != null && !material.getListaMaterialitemgrade().isEmpty()){
						indexEntregamaterial = material.getIndexEntregamaterial();
						for(Material item : material.getListaMaterialitemgrade()){
							if(item.getQuantidade() != null && item.getQuantidade() > 0) qtde += item.getQuantidade();
							if(item.getCdmaterial() != null){
								Produto produto = materialService.preencheProduto(item);
								produto.setCdmaterial(item.getCdmaterial());
								if(produto.getAltura() != null) produto.setAltura(produto.getAltura()*1000);
								if(produto.getLargura() != null) produto.setLargura(produto.getLargura()*1000);
								if(produto.getComprimento() != null) produto.setComprimento(produto.getComprimento()*1000);
								produtoService.updateDimensoesProduto(produto);
							}
						}
						if(qtde != null && qtde > 0 && material.getCasasdecimaisestoqueTrans() != null){
							qtde = SinedUtil.round(qtde, material.getCasasdecimaisestoqueTrans());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script type=\"text/javascript\">parent.setQdteTotalGrade('"+indexEntregamaterial +"','" +
				qtde+"');alert('Grade atualizada com sucesso!');parent.$.akModalRemove(true);</script>");
		return null;
	}
	
	private MaterialgradeBean criaMaterialgradeBean(WebRequestContext request) {
		MaterialgradeBean materialgradeBean = null;
		
		String cdmaterial = request.getParameter("cdmaterialmestregrade");
		String cdordemcompramaterial = request.getParameter("cdordemcompramaterial");
		String indexEntregamaterial = request.getParameter("indexEntregamaterial");
		String whereInMaterialItenGrade = request.getParameter("whereInMaterialItenGrade");
		String quantidade = request.getParameter("quantidade");
		String cdunidademedida = request.getParameter("cdunidademedida");
		
		Material material = null;
		
		try {
			if(cdmaterial != null && !"".equals(cdmaterial)){
				material = materialService.unidadeMedidaMaterial(new Material(Integer.parseInt(cdmaterial)));
				material.setIndexEntregamaterial(indexEntregamaterial);
				if(cdordemcompramaterial != null && !"".equals(cdordemcompramaterial)){
					material.setCdordemcompramaterial(Integer.parseInt(cdordemcompramaterial));
				}
				if(quantidade != null && !"".equals(quantidade)){
					material.setQuantidade(Double.parseDouble(quantidade.replace(".", "").replace(",", ".")));
				}
				if(whereInMaterialItenGrade != null && !"".equals(whereInMaterialItenGrade)){
					material.setWhereInMaterialItenGrade(whereInMaterialItenGrade);
				}
				if(cdunidademedida != null && !"".equals(cdunidademedida)){
					material.setUnidademedida(new Unidademedida(Integer.parseInt(cdunidademedida)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(material != null){
			materialgradeBean = new MaterialgradeBean();
			List<Material> listaMaterial = new ArrayList<Material>();
			if(material != null && (!materialService.isControleMaterialgrademestre(material) || 
					(whereInMaterialItenGrade != null && !"".equals(whereInMaterialItenGrade)))){
				materialgradeBean.setMaterial(material);
				listaMaterial.add(material);
			}
			
			materialgradeBean.setListaMaterial(listaMaterial);	
		}
		
		return materialgradeBean;
	}
	
	/**
	 * M�todo que adiciona informa��es de atributo para a cria��o/edi��o da entrada fiscal
	 *
	 * @param request
	 * @param entregadocumento
	 * @author Luiz Fernando
	 */
	public void addInfAtributo(WebRequestContext request, Entregadocumento entregadocumento) {
		if(request != null){
//			// preenche unidade de medida do material j� selecionado
//			Set<Material> materiais = new HashSet<Material>();
//			Map<Material, List<Unidademedida>> mapaUnidades = new HashMap<Material, List<Unidademedida>>();
//			if (entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && 
//					!entregadocumento.getListaEntregamaterial().isEmpty()) {
//				for (Entregamaterial entrega : entregadocumento.getListaEntregamaterial()) {
//					if(entrega.getMaterial() != null){
//						materiais.add(entrega.getMaterial());
//					}
//				}
//			}
//			if(materiais != null){
//				for (Material material : materiais) {
//					material = materialService.loadSined(material);
//					
//					if (material != null) {
//						mapaUnidades.put(material, unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material));
//					}
//				}
//			}
//			request.setAttribute("mapaUnidades", mapaUnidades);
			
			List<Projeto> projetos = new ArrayList<Projeto>();
			if(entregadocumento != null && entregadocumento.getRateio() != null && entregadocumento.getRateio().getListaRateioitem() != null){
				for(Rateioitem rateioitem : entregadocumento.getRateio().getListaRateioitem()){
					if(rateioitem.getProjeto() != null){
						projetos.add(rateioitem.getProjeto());
					}
				}
			}
			List<Projeto> listaProjeto = projetoService.findProjetosAbertos(projetos, true, null);
			request.setAttribute("listaProjeto", listaProjeto);
			request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(entregadocumento.getNaturezaoperacao(), NotaTipo.ENTRADA_FISCAL, Boolean.TRUE));
			request.setAttribute("listaProjetoUsuarioNotCancelado", projetoService.findForComboByUsuariologadoNotCancelado());
			
			if (entregadocumento.getCdentregadocumento() != null){			
				request.setAttribute("verificaNatop", entregadocumento.getNatop() != null && !entregadocumento.getNatop().isEmpty() && 
						entregadocumento.getNaturezaoperacao() == null);
			}
			
			if (entregadocumento != null && entregadocumento.getListaEntregamaterial() != null && 
					!entregadocumento.getListaEntregamaterial().isEmpty()) {
				for (Entregamaterial entrega : entregadocumento.getListaEntregamaterial()) {
					if(entrega.getMaterial() != null && entrega.getMaterial().getCdmaterial() != null){
						Material material = materialService.loadWithGrade(entrega.getMaterial());
						
						boolean exibirButtonGrade = false;
						boolean exibirButtonItemGrade = false;
						boolean exibirMontargrade = false;
						
						if(material.getMaterialgrupo() != null && material.getMaterialgrupo().getGradeestoquetipo() != null){
							if(material.getMaterialmestregrade() != null){
								exibirButtonGrade = true;
							}else {
								exibirMontargrade = true;
								List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
								exibirButtonItemGrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty();
							}
						}
						entrega.setExibirButtonGrade(exibirButtonGrade);
						entrega.setExibirButtonItemGrade(exibirButtonItemGrade);
						entrega.setExibirMontargrade(exibirMontargrade);
					}
				}
			}
			
//			Map<Material, List<Grupotributacao>> mapaGrupotributacao = new HashMap<Material, List<Grupotributacao>>();
//			if(materiais != null && materiais.size() > 0){
//				Endereco enderecofornecedor = null;
//				Fornecedor fornecedor = null;
//				if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null){
//					fornecedor = fornecedorService.loadForTributacao(entregadocumento.getFornecedor());
//					enderecofornecedor = fornecedor.getEndereco();
//				}
//				
//				List<Categoria> listaCategoriaFornecedor = null;
//				if(fornecedor != null){
//					listaCategoriaFornecedor = categoriaService.findByPessoa(fornecedor);
//				}
//				
//				for (Material material : materiais) {
//					material = materialService.carregaMaterial(material);
//					List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
//							grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
//													entregadocumento.getEmpresa(), 
//													entregadocumento.getNaturezaoperacao(), 
//													material.getMaterialgrupo(),
//													null, 
//													(material.getServico() == null || !material.getServico()), 
//													material, 
//													material.getNcmcompleto(),
//													null, 
//													enderecofornecedor,
//													null,
//													listaCategoriaFornecedor,
//													null,
//													null,
//													null,
//													entregadocumento.getDtemissao()));
//					mapaGrupotributacao.put(material, listaGrupotributacao);
//				}
//				
//				if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
//					for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
//						if(entregamaterial.getMaterial() != null && entregamaterial.getGrupotributacao() != null && 
//								entregamaterial.getGrupotributacao().getCdgrupotributacao() != null){
//							List<Grupotributacao> listaGrupotributacao = mapaGrupotributacao.get(entregamaterial.getMaterial());
//							if(listaGrupotributacao == null) listaGrupotributacao = new ArrayList<Grupotributacao>();
//							boolean adicionar = true;
//							for(Grupotributacao grupotributacao : listaGrupotributacao){
//								if(entregamaterial.getGrupotributacao().getCdgrupotributacao().equals(grupotributacao.getCdgrupotributacao())){
//									adicionar = false;
//									break;
//								}
//							}
//							if(adicionar){
//								listaGrupotributacao.add(entregamaterial.getGrupotributacao());
//							}
//							mapaGrupotributacao.put(entregamaterial.getMaterial(), listaGrupotributacao);
//						}
//					}
//				}
//			}
//			request.setAttribute("mapaGrupotributacao", mapaGrupotributacao);
			request.setAttribute("tipooperacao", Tipooperacao.TIPO_DEBITO);
			request.setAttribute("listaPrazopagamento", prazopagamentoService.findAtivosForCombo(entregadocumento.getPrazopagamento()));
			request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		}
	}
	
	public void addInfAtributo(WebRequestContext request, Entregadocumento entregadocumento, Entregamaterial entregamaterial) {
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		List<Grupotributacao> listaGrupotributacao = new ArrayList<Grupotributacao>();
		List<Materialclasse> listaMaterialclasse = new ArrayList<Materialclasse>();
		
		if(entregamaterial != null && Util.objects.isPersistent(entregamaterial.getMaterial())){
			Material material = materialService.loadWithGrade(entregamaterial.getMaterial());
			listaUnidademedida = unidademedidaconversaoService.criarListaUnidademedidaRelacionadas(material.getUnidademedida(), material);
			listaMaterialclasse = materialService.findClasses(material);
			
			boolean exibirButtonGrade = false;
			boolean exibirButtonItemGrade = false;
			boolean exibirMontargrade = false;
			
			if(material.getMaterialgrupo() != null && material.getMaterialgrupo().getGradeestoquetipo() != null){
				if(material.getMaterialmestregrade() != null){
					exibirButtonGrade = true;
				}else {
					exibirMontargrade = true;
					List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
					exibirButtonItemGrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty();
				}
			}
			entregamaterial.setExibirButtonGrade(exibirButtonGrade);
			entregamaterial.setExibirButtonItemGrade(exibirButtonItemGrade);
			entregamaterial.setExibirMontargrade(exibirMontargrade);
			
			Endereco enderecofornecedor = null;
			Fornecedor fornecedor = null;
			if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null){
				fornecedor = fornecedorService.loadForTributacao(entregadocumento.getFornecedor());
				enderecofornecedor = fornecedor.getEndereco();
			}
			
			List<Categoria> listaCategoriaFornecedor = null;
			if(fornecedor != null){
				listaCategoriaFornecedor = categoriaService.findByPessoa(fornecedor);
			}
				
			material = materialService.carregaMaterial(entregamaterial.getMaterial());
			listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
					grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
											entregadocumento.getEmpresa(), 
											entregadocumento.getNaturezaoperacao(), 
											material.getMaterialgrupo(),
											null, 
											(material.getServico() == null || !material.getServico()), 
											material, 
											material.getNcmcompleto(),
											null, 
											enderecofornecedor,
											null,
											listaCategoriaFornecedor,
											null,
											null,
											null,
											entregadocumento.getDtemissao()));
				
			if(entregamaterial.getMaterial() != null && entregamaterial.getGrupotributacao() != null && 
					entregamaterial.getGrupotributacao().getCdgrupotributacao() != null){
				if(listaGrupotributacao == null) listaGrupotributacao = new ArrayList<Grupotributacao>();
				boolean adicionar = true;
				for(Grupotributacao grupotributacao : listaGrupotributacao){
					if(entregamaterial.getGrupotributacao().getCdgrupotributacao().equals(grupotributacao.getCdgrupotributacao())){
						adicionar = false;
						break;
					}
				}
				if(adicionar){
					listaGrupotributacao.add(entregamaterial.getGrupotributacao());
				}
			}
		}
		request.setAttribute("listaMaterialclasse", listaMaterialclasse);
		request.setAttribute("listaGrupotributacao", listaGrupotributacao);
		request.setAttribute("listaUnidademedida", listaUnidademedida);
		
		String consultar = request.getParameter("consultar");
		request.setAttribute("consultar", consultar != null && consultar.equals("true") ? true : false);
		request.setAttribute("fromrecebimento", Boolean.TRUE.equals(entregadocumento.getFromRecebimento()));
		
		String haveOrdemCompra = request.getParameter("haveOrdemCompra");
		if(StringUtils.isNotBlank(haveOrdemCompra)) { 
			request.setAttribute("haveOrdemCompra", haveOrdemCompra); 
		}
	}
	
	public ModelAndView buscarMaterialmestregrade(WebRequestContext request) {
		Material material = null;
		if(request.getParameter("cdmaterial") != null && !"".equals(request.getParameter("cdmaterial"))){
			material = materialService.loadWithGrade(new Material(Integer.parseInt(request.getParameter("cdmaterial"))));
		}
		return new JsonModelAndView().addObject("material", material.getMaterialmestregrade());
	}
	
	public ModelAndView verificaButtonGrade(WebRequestContext request) {
		Material material = null;
		boolean exibirButtonGrade = false;
		boolean exibirButtonItemGrade = false;
		boolean exibirMontargrade = false;
		if(request.getParameter("cdmaterial") != null && !"".equals(request.getParameter("cdmaterial"))){
			material = materialService.loadWithGrade(new Material(Integer.parseInt(request.getParameter("cdmaterial"))));
			
			if(material.getMaterialgrupo() != null && material.getMaterialgrupo().getGradeestoquetipo() != null){
				if(material.getMaterialmestregrade() != null){
					exibirButtonGrade = true;
				}else {
					exibirMontargrade = true;
					List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
					exibirButtonItemGrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty();
				}
			}
		}
		return new JsonModelAndView()
					.addObject("exibirMontargrade", exibirMontargrade)			
					.addObject("exibirButtonGrade", exibirButtonGrade)
					.addObject("exibirButtonItemGrade", exibirButtonItemGrade);
	}
	
	public void trocaEmpresaOuClienteOuNaturezaoperacao(WebRequestContext request, Entregadocumento entregadocumento){
		Naturezaoperacao naturezaoperacao = entregadocumento.getNaturezaoperacao();
		Empresa empresa = entregadocumento.getEmpresa();
		Endereco enderecoempresa = null;
		Fornecedor fornecedor = entregadocumento.getFornecedor();
		Endereco enderecofornecedor = null;
		
		Uf ufEmpresa = null;
		Uf ufFornecedor = null;
		Cfop cfop = null; 
		
		if (empresa!=null){
			enderecoempresa = enderecoService.carregaEnderecoEmpresa(empresa);
		}else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if (empresa!=null && empresa.getEndereco()!=null){
				enderecoempresa = empresa.getEndereco();
			}
		}
		
		if (enderecoempresa!=null && enderecoempresa.getCdendereco()!=null){
			enderecoempresa = enderecoService.carregaEnderecoComUfPais(enderecoempresa);
			if (enderecoempresa.getMunicipio()!=null){
				ufEmpresa = enderecoempresa.getMunicipio().getUf();
			}
		}
		
		
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			fornecedor = fornecedorService.loadForTributacao(fornecedor);
			enderecofornecedor = fornecedor.getEndereco();
			if(enderecofornecedor != null && enderecofornecedor.getMunicipio() != null){
				ufFornecedor = enderecofornecedor.getMunicipio().getUf();
			}
		}
		
		List<Categoria> listaCategoriaFornecedor = null;
		if(fornecedor != null){
			listaCategoriaFornecedor = categoriaService.findByPessoa(fornecedor);
		}
		
		Cfopescopo cfopescopo = null;
		if (ufEmpresa!=null && ufFornecedor!=null){
			if (ufEmpresa.getCduf().equals(ufFornecedor.getCduf())){
				cfopescopo = Cfopescopo.ESTADUAL;
			}else {
				cfopescopo = Cfopescopo.FORA_DO_ESTADO;
			}
		}
		
		if (naturezaoperacao != null && naturezaoperacao.getCdnaturezaoperacao()!=null){
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, cfopescopo);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				cfop = listaNaturezaoperacaocfop.get(0).getCfop();
			}			
		}
		
		Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
		Object attr = request.getSession().getAttribute("listaEntregamaterial" + entregadocumento.getIdentificadortela());
		if(attr != null) {
			listaEntregamaterial = (Set<Entregamaterial>) attr;
		}
		
		if (listaEntregamaterial!=null){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				if(entregamaterial.getMaterial() != null){
					Material material = materialService.loadForTributacao(entregamaterial.getMaterial());
					if(entregamaterial.getMaterial() != null){
						List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
								grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
														empresa, 
														naturezaoperacao, 
														material.getMaterialgrupo(),
														null, 
														(material.getServico() == null || !material.getServico()), 
														material, 
														material.getNcmcompleto(),
														null, 
														enderecofornecedor,
														null,
														listaCategoriaFornecedor,
														null,
														null,
														null, 
														null));
						entregamaterial.setGrupotributacao(grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao));
					}
					
					entregamaterial = loadTributacao(entregamaterial, empresa, null, fornecedor, enderecofornecedor, enderecoempresa);
					
					if(entregamaterial.getCfop() != null && entregamaterial.getCfop().getCodigo() != null){
						material.getMaterialgrupo().setCfop(entregamaterial.getCfop());
						cfop = cfopService.findForEntradafiscal(material, enderecoempresa, enderecofornecedor);
						entregamaterial.setCfop(cfop);
					}else if (cfop != null){
						material.getMaterialgrupo().setCfop(cfop);
						cfop = cfopService.findForEntradafiscal(material, enderecoempresa, enderecofornecedor);
						entregamaterial.setCfop(cfop);
					}
					
					Material materialTributacao = material != null && material.getCdmaterial() != null ? new Material(material.getCdmaterial()) : null;  
						
					materialTributacao = materialService.loadTributacao(materialTributacao, empresa, null, material.getMaterialgrupo(), fornecedor,
							enderecofornecedor, enderecoempresa, entregamaterial.getGrupotributacao());
					
					Double valorbc = entregamaterial.getValortotaloperacao();
					if(materialTributacao != null && valorbc != null){
						if(materialTributacao.getPercentualBccofins() != null){
							entregamaterial.setValorbccofins(new Money(valorbc * materialTributacao.getPercentualBccofins() / 100d));
						}
						if(materialTributacao.getPercentualBcpis() != null){
							entregamaterial.setValorbcpis(new Money(valorbc * materialTributacao.getPercentualBcpis() / 100d));
						}
					}
					
					grupotributacaoService.calcularValoresFormulaIcms(entregamaterial, entregamaterial.getMaterial().getGrupotributacaoNota() != null ? entregamaterial.getMaterial().getGrupotributacaoNota() : entregamaterial.getGrupotributacao());
					grupotributacaoService.calcularValoresFormulaIcmsST(entregamaterial, entregamaterial.getMaterial().getGrupotributacaoNota() != null ? entregamaterial.getMaterial().getGrupotributacaoNota() : entregamaterial.getGrupotributacao());
					
				}
			}
			request.getSession().setAttribute("listaEntregamaterial" + entregadocumento.getIdentificadortela(), listaEntregamaterial);
		}
		
		entregadocumento.setListaEntregamaterial(listaEntregamaterial);
		request.setAttribute("RECALCULO_IMPOSTO_NOTA", Boolean.TRUE);
	}
	
	private Entregamaterial loadTributacao(Entregamaterial item, Empresa empresa, Cliente cliente, Fornecedor fornecedor, Endereco enderecoOrigem,
			Endereco enderecoDestino) {
		if (item.getMaterial() != null){		
			Material material = materialService.loadWithMatrialgrupo(item.getMaterial());
			material = materialService.loadTributacao(material, empresa, cliente, material.getMaterialgrupo(), fornecedor, 
					enderecoOrigem, enderecoDestino, item.getGrupotributacao());
			Cfop cfop = null;
			if (material.getGrupotributacaoNota() != null ){
				item.setGrupotributacaotrans(material.getGrupotributacaoNota());
				if(material.getGrupotributacaoNota().getCfopgrupo() != null){
					material.getMaterialgrupo().setCfop(material.getGrupotributacaoNota().getCfopgrupo());
					cfop = cfopService.findForEntradafiscal(material, material.getEnderecoTributacaoOrigem(), material.getEnderecoTributacaoDestino());
				}
				preencherDadosTributacao(item, material.getGrupotributacaoNota());
			}
			item.setCfop(cfop);			
			
//			item.setIcms(material.getAliquotaicms() != null ? material.getAliquotaicms().getValue().doubleValue() : null);
//			item.setIcmsst(material.getAliquotaicmsst() != null ? material.getAliquotaicmsst().getValue().doubleValue() : null);
//			item.setIpi(material.getAliquotaipi() != null ? material.getAliquotaipi().getValue().doubleValue() : null);
//			item.setPis(material.getAliquotapis() != null ? material.getAliquotapis().getValue().doubleValue() : null);
//			item.setCofins(material.getAliquotacofins() != null ? material.getAliquotacofins().getValue().doubleValue() : null);
//			item.setIss(material.getAliquotaiss() != null ? material.getAliquotaiss().getValue().doubleValue() : null);
			
			if(material.getAliquotaicms() != null && (material.getAliquotaicms().getValue().doubleValue() > 0 || 
					(material.getAliquotaicms().getValue().doubleValue() == 0 && 
					 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setIcms(material.getAliquotaicms().getValue().doubleValue());
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setIcms(null);
			}
			if(material.getAliquotaicmsst() != null && (material.getAliquotaicmsst().getValue().doubleValue() > 0 || 
						(material.getAliquotaicmsst().getValue().doubleValue() == 0 && 
						 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setIcmsst(material.getAliquotaicmsst().getValue().doubleValue());
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setIcmsst(null);
			}	
			boolean calcularIpi = true;
			if(material.getAliquotaipi() != null && (material.getAliquotaipi().getValue().doubleValue() > 0 || 
						(material.getAliquotaipi().getValue().doubleValue() == 0 && 
						 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setIpi(material.getAliquotaipi().getValue().doubleValue());
				calcularIpi = false;
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setIpi(null);
			}
			if(material.getAliquotapis() != null && (material.getAliquotapis().getValue().doubleValue() > 0 || 
						(material.getAliquotapis().getValue().doubleValue() == 0 && 
						 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setPis(material.getAliquotapis().getValue().doubleValue());
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setPis(null);
			}
			if(material.getAliquotacofins() != null && (material.getAliquotacofins().getValue().doubleValue() > 0 || 
					(material.getAliquotacofins().getValue().doubleValue() == 0 && 
					 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setCofins(material.getAliquotacofins().getValue().doubleValue());
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setCofins(null);
			}
			if(material.getAliquotaiss() != null && (material.getAliquotaiss().getValue().doubleValue() > 0 || 
					(material.getAliquotaiss().getValue().doubleValue() == 0 && 
					 (item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null)))){
				item.setIss(material.getAliquotaiss().getValue().doubleValue());
			}else if(item.getGrupotributacao() != null || material.getGrupotributacaoNota() != null){
				item.setIss(null);
			}
			if(item.getValormva() == null){
				item.setValormva(material.getAliquotamva() != null ? material.getAliquotamva() : null);
			}
			if(item.getMaterial() != null){
				item.getMaterial().setBasecalculost(material.getBasecalculost());
				item.getMaterial().setValorst(material.getValorst());
			}
			
			Double valorbc = item.getValortotaloperacao();
			if(valorbc != null){
				if(item.getValorbcicms() == null){
					item.setValorbcicms(new Money(valorbc));
				}
				if(item.getIcms() != null && item.getValorbcicms() != null){
					item.setValoricms(new Money(item.getValorbcicms().getValue().doubleValue()*item.getIcms()/100));
				}
				if(Retencao.NUNCA_RETER.equals(material.getRetencaoIpi()) && material.getPercentualBcipi() != null){
					item.setValorbcipi(new Money(valorbc).multiply(new Money(material.getPercentualBcipi())).divide(new Money(100d)));
				}
				if(item.getValorbcipi() == null){
					item.setValorbcipi(new Money(valorbc));
				}
				if(item.getIpi() != null && (item.getIpi() > 0 || !calcularIpi) && item.getValorbcipi() != null){
					item.setValoripi(new Money(item.getValorbcipi().getValue().doubleValue()*item.getIpi()/100));
				}else if(item.getValoripi() != null && item.getValoripi().getValue().doubleValue() > 0 && item.getValorbcipi() != null && item.getValorbcipi().getValue().doubleValue() > 0 && calcularIpi && (item.getIpi() == null || item.getIpi() == 0)){
					item.setIpi(SinedUtil.round(item.getValoripi().getValue().doubleValue() / item.getValorbcipi().getValue().doubleValue()*100, 2));
				}
				if(item.getValorbcpis() == null){
					item.setValorbcpis(new Money(valorbc));
				}
				if(item.getPis() != null && item.getValorbcpis() != null){
					item.setValorpis(new Money(item.getValorbcpis().getValue().doubleValue()*item.getPis()/100));
				}
				if(item.getValorbccofins() == null){
					item.setValorbccofins(new Money(valorbc));
				}
				if(item.getCofins() != null && item.getValorbccofins() != null){
					item.setValorcofins(new Money(item.getValorbccofins().getValue().doubleValue()*item.getCofins()/100));
				}
				if(material.getServico() != null && material.getServico()){
					if(item.getValorbciss() == null){
						item.setValorbciss(new Money(valorbc));
					}
					if(item.getIss() != null && item.getValorbciss() != null){
						item.setValoriss(new Money(valorbc*item.getIss()/100));
					}
				}
			}
		}
		
		return item;
	}
	
	private void preencherDadosTributacao(Entregamaterial item, Grupotributacao grupotributacao){
		item.setCsticms(grupotributacao.getTipocobrancaicms());
		item.setCstipi(grupotributacao.getTipocobrancaipi());
		item.setCstpis(grupotributacao.getTipocobrancapis());
		item.setCstcofins(grupotributacao.getTipocobrancacofins());
		item.setTipotributacaoiss(grupotributacao.getTipotributacaoiss());
		item.setValormva(grupotributacao.getMargemvaloradicionalicmsst());
		item.setValorFiscalIcms(grupotributacao.getValorFiscalIcms());
		item.setValorFiscalIpi(grupotributacao.getValorFiscalIpi());
		item.setNaturezabcc(grupotributacao.getNaturezabcc());
		item.setOrigemProduto(grupotributacao.getOrigemprodutoicms());
		item.setNaoconsideraricmsst(grupotributacao.getNaoconsideraricmsst());
	}
	
	public void ajaxOnChangeGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao) {
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		Material material = materialService.loadWithMatrialgrupo(grupotributacao.getMaterialTrans());
		Fornecedor fornecedor = grupotributacao.getFornecedorTrans() != null && grupotributacao.getFornecedorTrans().getCdpessoa() != null ? 
										fornecedorService.loadForTributacao(grupotributacao.getFornecedorTrans()) : null;
		Empresa empresa = null; 
		Endereco enderecoOrigem = null;
		if (grupotributacao.getEmpresaTributacaoOrigem() != null && grupotributacao.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(grupotributacao.getEmpresaTributacaoOrigem());
			enderecoOrigem = empresa.getEndereco();
		} else {
			empresa = empresaService.loadPrincipalWithEndereco();
			if(empresa != null){
				enderecoOrigem = empresa.getEndereco();
			}
		}		
		
		Endereco enderecoDestino = grupotributacao.getEnderecoTributacaoDestino();
		if(fornecedor != null) {
			enderecoDestino = fornecedor.getEndereco();
		}
		
		Grupotributacao gt = grupotributacaoService.loadForEntrada(grupotributacao);
		material = materialService.loadTributacao(material, empresa, null, material.getMaterialgrupo(), fornecedor,
													enderecoDestino, enderecoOrigem, gt);
		
		Cfop cfop = null;
		
		if(gt.getCfopgrupo() != null){
			material.getMaterialgrupo().setCfop(gt.getCfopgrupo());
			cfop = cfopService.findForEntradafiscal(material, enderecoOrigem, enderecoDestino);
		}
		
		view.println("var cfop = '" + (cfop != null && cfop.getCdcfop() != null ? "br.com.linkcom.sined.geral.bean.Cfop[cdcfop="+cfop.getCdcfop()+", codigo=" + cfop.getCodigo() + ", descricaoCodigo="+cfop.getDescricaoCodigo().replace(",", "")+"]" : "") + "';");
		view.println("var descricaoCfop = '" + (cfop != null && cfop.getCdcfop() != null ? cfop.getDescricaoCodigo() : "") + "';");
		
		view.println("var tipocobrancaicms = '" + (gt.getTipocobrancaicms() != null && gt.getTipocobrancaicms().getValue() != null ? gt.getTipocobrancaicms().name() : "") + "';");
		view.println("var icms = '" + (material.getAliquotaicms() != null ? SinedUtil.descriptionDecimal(material.getAliquotaicms().getValue().doubleValue()) : "") + "';");		
		view.println("var icmsst = '" + (material.getAliquotaicmsst() != null ? SinedUtil.descriptionDecimal(material.getAliquotaicmsst().getValue().doubleValue()) : "") + "';");
		view.println("var margemvaloradicionalicmsst = '" + (material.getAliquotamva() != null ? SinedUtil.descriptionDecimal(material.getAliquotamva()) : "") + "';");
		view.println("var naoconsideraricmsst = '" + (gt.getNaoconsideraricmsst() != null ? gt.getNaoconsideraricmsst() : "") + "';");
		
		view.println("var tipocobrancaipi = '" + (gt.getTipocobrancaipi() != null ? gt.getTipocobrancaipi().name() : "") + "';");
		view.println("var ipi = '" + (material.getAliquotaipi() != null ? SinedUtil.descriptionDecimal(material.getAliquotaipi().getValue().doubleValue()) : "") + "';");
		
		view.println("var tipocobrancapis = '" + (gt.getTipocobrancapis() != null ? gt.getTipocobrancapis().name() : "") + "';");
		view.println("var pis = '" + (material.getAliquotapis() != null ? SinedUtil.descriptionDecimal(material.getAliquotapis().getValue().doubleValue()) : (gt.getTipocobrancapis() != null ? "0,00" :"")) + "';");
		
		view.println("var tipocobrancacofins = '" + (gt.getTipocobrancacofins() != null ? gt.getTipocobrancacofins().name() : "") + "';");
		view.println("var cofins = '" + (material.getAliquotacofins() != null ? SinedUtil.descriptionDecimal(material.getAliquotacofins().getValue().doubleValue()) : (gt.getTipocobrancacofins() != null ? "0,00" :"")) + "';");	

		view.println("var iss = '" + (material.getAliquotaiss() != null ? SinedUtil.descriptionDecimal(material.getAliquotaiss().getValue().doubleValue()) : "") + "';");	
		view.println("var tipotributacaoiss = '" + (gt.getTipotributacaoiss() != null ? gt.getTipotributacaoiss().name() : "") + "';");
		
		view.println("var valorFiscalIcms = '" + (gt.getValorFiscalIcms() != null ? gt.getValorFiscalIcms().name() : "<null>") + "';");
		view.println("var valorFiscalIpi = '" + (gt.getValorFiscalIpi() != null ? gt.getValorFiscalIpi().name() : "<null>") + "';");
		view.println("var naturezabcc = '" + (gt.getNaturezabcc() != null ? gt.getNaturezabcc().name() : "<null>") + "';");
		view.println("var origemProdutoIcms = '" + (gt.getOrigemprodutoicms() != null ? gt.getOrigemprodutoicms().name() : "<null>") + "';");
		
		view.println("var percentualBcicms = '" + (material.getPercentualBcicms() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcicms()) : "") + "';");
		view.println("var percentualBcipi = '" + (material.getPercentualBcipi() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcipi()) : "") + "';");
		view.println("var percentualBcpis = '" + (material.getPercentualBcpis() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcpis()) : "") + "';");
		view.println("var percentualBccofins = '" + (material.getPercentualBccofins() != null ? SinedUtil.descriptionDecimal(material.getPercentualBccofins()) : "") + "';");
		view.println("var percentualBccsll = '" + (material.getPercentualBccsll() != null ? SinedUtil.descriptionDecimal(material.getPercentualBccsll()) : "") + "';");
		view.println("var percentualBcir = '" + (material.getPercentualBcir() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcir()) : "") + "';");
		view.println("var percentualBcinss = '" + (material.getPercentualBcinss() != null ? SinedUtil.descriptionDecimal(material.getPercentualBcinss()) : "") + "';");
		view.println("var percentualBciss = '" + (material.getPercentualBciss() != null ? SinedUtil.descriptionDecimal(material.getPercentualBciss()) : "") + "';");
		
		view.println("var funcaoBaseCalculoICMS = " + grupotributacaoService.criaFuncaoBaseCalculoICMSJavascript(gt, gt.getFormulabcicms(), "ENTRADAFISCAL"));
		view.println("var funcaoBaseCalculoICMSST = " + grupotributacaoService.criaFuncaoBaseCalculoICMSSTJavascript(gt, gt.getFormulabcst(), "ENTRADAFISCAL"));
		view.println("var funcaoValorICMSST = " + grupotributacaoService.criaFuncaoValorICMSSTJavascript(gt, gt.getFormulavalorst(), "ENTRADAFISCAL"));
	}
	
	public void ajaxCarregaGrupotributacao(WebRequestContext request, Grupotributacao grupotributacao){
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		String notaDtEmissaoString = request.getParameter("notaDtEmissao");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date notaDtEmissao = null;
		try {
			notaDtEmissao = new java.sql.Date(sdf.parse(notaDtEmissaoString).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Fornecedor fornecedor = grupotributacao.getFornecedorTrans() != null && grupotributacao.getFornecedorTrans().getCdpessoa() != null ? 
				fornecedorService.loadForTributacao(grupotributacao.getFornecedorTrans()) : null;
		Naturezaoperacao naturezaoperacao = null;
		if(grupotributacao.getNaturezaoperacaoTrans() != null && grupotributacao.getNaturezaoperacaoTrans().getCdnaturezaoperacao() != null){
			naturezaoperacao = grupotributacao.getNaturezaoperacaoTrans();
		}
		Empresa empresanota = null; 
		if (grupotributacao.getEmpresaTributacaoOrigem() != null && grupotributacao.getEmpresaTributacaoOrigem().getCdpessoa() != null){
			empresanota = grupotributacao.getEmpresaTributacaoOrigem();
			Empresa empresa = empresaService.loadWithEndereco(grupotributacao.getEmpresaTributacaoOrigem());
			grupotributacao.setEnderecoTributacaoOrigem(empresa.getEndereco());
		} else {
			Empresa empresa = empresaService.loadPrincipalWithEndereco();
			if(empresa != null){
				grupotributacao.setEnderecoTributacaoOrigem(empresa.getEndereco());
			}
		}		
		
		Endereco enderecoDestinoFornecedor = null;
		if(fornecedor != null){
			enderecoDestinoFornecedor = fornecedor.getEndereco();
		}
		
		boolean modelodocumentofiscalICMS = grupotributacao.getModelodocumentofiscalICMS() != null &&
			grupotributacao.getModelodocumentofiscalICMS(); 
		
		Material beanMaterial = materialService.loadForTributacao(grupotributacao.getMaterialTrans());
		if(beanMaterial != null && modelodocumentofiscalICMS){
			beanMaterial.setModelodocumentofiscalICMS(modelodocumentofiscalICMS);
		}
		
		List<Categoria> listaCategoriaFornecedor = null;
		if(fornecedor != null){
			listaCategoriaFornecedor = categoriaService.findByPessoa(fornecedor);
		}
		
		List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findGrupotributacaoCondicaoTrue(
				grupotributacaoService.createGrupotributacaoVOForTributacao(	Operacao.ENTRADA, 
																		empresanota, 
																		naturezaoperacao, 
																		beanMaterial.getMaterialgrupo(),
																		null, 
																		(beanMaterial.getServico() == null || !beanMaterial.getServico()), 
																		beanMaterial, 
																		beanMaterial.getNcmcompleto(),
																		null, 
																		enderecoDestinoFornecedor,
																		null,
																		listaCategoriaFornecedor,
																		null,
																		null,
																		null,
																		notaDtEmissao));
		
		JSON listaGrupotributacaoJSON = View.convertToJson(listaGrupotributacao);
		String jsonGrupotributacao = listaGrupotributacaoJSON.toString(0);
		view.println("var listaGrupotributacao = " + jsonGrupotributacao);
		
		grupotributacao = grupotributacaoService.getSugestaoGrupotributacao(listaGrupotributacao);
		if(grupotributacao != null){
			view.println("var grupotributacaoSelecionado = 'br.com.linkcom.sined.geral.bean.Grupotributacao[cdgrupotributacao=" + grupotributacao.getCdgrupotributacao() + "]';");
		} else {
			view.println("var materialOrigemProduto = '" + (beanMaterial.getOrigemproduto() != null ? beanMaterial.getOrigemproduto().name() : "") + "';");
		}
	}
	
	/**
	 * M�todo ajustar os itens do recebimento. (Enviar os itens de grade para o wms)
	 *
	 * @param bean
	 * @author Luiz Fernando
	 * @since 18/03/2014
	 */
	public void ajustaEntregamaterialForSincronizar(Entrega bean) {
		if(bean != null && bean.getListaEntregadocumento() != null && !bean.getListaEntregadocumento().isEmpty()){
			String paramCalculometroquadrado = parametrogeralService.getValorPorNome(Parametrogeral.CALCULOMETROQUADRADO);
			if(paramCalculometroquadrado == null || "".equals(paramCalculometroquadrado)){
				paramCalculometroquadrado = "LxAxC";
			}
			for(Entregadocumento ed : bean.getListaEntregadocumento()){
				if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
					Set<Entregamaterial> listaAjustada = new ListSet<Entregamaterial>(Entregamaterial.class);
					for(Entregamaterial em : ed.getListaEntregamaterial()){
						boolean adicionar = true;
						if(em.getMaterial() != null && em.getWhereInMaterialItenGrade() != null &&
								!"".equals(em.getWhereInMaterialItenGrade()) && 
								materialService.isControleMaterialitemgrade(em.getMaterial())){
							List<Material> listaMaterial = materialService.findMaterialitemByMaterialmestregrade(em.getMaterial(), em.getWhereInMaterialItenGrade());
							if(listaMaterial != null && !listaMaterial.isEmpty()){
								for(Material material : listaMaterial){
									adicionar = false;
									Entregamaterial itemNovo = new Entregamaterial();
									itemNovo.setMaterial(material);
									itemNovo.setCdentregamaterial(em.getCdentregamaterial());
									itemNovo.setConsiderarItemGradeForWms(true);
									itemNovo.setQtde(1d);
									itemNovo.setValorunitario(em.getValorunitario());
									listaAjustada.add(itemNovo);
								}
							}
						} else if(em.getMaterial() != null){
							em.setMaterial(materialService.carregaMaterialWMS(em.getMaterial()));
						}
						
						if(adicionar){
							listaAjustada.add(em);
						}
					}
					if(listaAjustada != null && !listaAjustada.isEmpty()){
						ed.setListaEntregamaterial(listaAjustada);
					}
				}
			}
		}
		
	}
	
	public List<Entregadocumento> findForCTRC(String whereIn, String whereNotIn) {
		return entradafiscalDAO.findForCTRC(whereIn, whereNotIn);
	}
	
	public void salvarVinculo(Entregadocumento bean){
		
		String whereNotIn = "";
		if (bean.getListaEntregadocumentofrete()!=null){
			for (Entregadocumentofrete entregadocumentofrete: bean.getListaEntregadocumentofrete()){
				if (entregadocumentofrete.getCdentregadocumentofrete()!=null){
					whereNotIn += entregadocumentofrete.getCdentregadocumentofrete() + ","; 
				}
			}
			if (whereNotIn.endsWith(",")){
				whereNotIn = whereNotIn.substring(0, whereNotIn.length()-1);
			}
		}
		
		entregadocumentofreteService.delete(bean.getCdentregadocumento(), whereNotIn);
		if (bean.getListaEntregadocumentofrete()!=null){
			for (Entregadocumentofrete entregadocumentofrete: bean.getListaEntregadocumentofrete()){
				entregadocumentofrete.setEntregadocumento(bean);
				entregadocumentofreteService.saveOrUpdate(entregadocumentofrete);
			}
		}
	}
	
	/**
	 * M�todo respons�vel por salvar a lista de novos documentos referenciados inclu�dos em um 'entregadocumento'.
	 * 
	 * @param request
	 * @param bean
	 * @author Rafael Salvio
	 */
	public void saveDocumentosReferenciados(WebRequestContext request, Entregadocumento bean){
		if(bean != null){
			Set<Entregadocumentoreferenciado> listaNovosDocumentos = bean.getListaEntregadocumentoreferenciadoTrans();
		
			if(listaNovosDocumentos != null && !listaNovosDocumentos.isEmpty()){
				StringBuilder erros = new StringBuilder();
				for(Entregadocumentoreferenciado edr : listaNovosDocumentos){
					if(edr.getInserircontapagar() != null && edr.getInserircontapagar()){
						Documento doc = new Documento();
						doc.setEmpresa(bean.getEmpresa());
						if("FALSE".equals(parametrogeralService.getValorPorNome(Parametrogeral.NOTACOMPRA_GERA_CONTADEFINITIVA)))
							doc.setDocumentoacao(edr.getContapaga() != null && edr.getContapaga() ? Documentoacao.AUTORIZADA : Documentoacao.DEFINITIVA);
						doc.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
						doc.setTipopagamento(Tipopagamento.FORNECEDOR);
						doc.setReferencia(Referencia.MES_ANO_CORRENTE);
						doc.setPessoa(edr.getFornecedor());
						doc.setDocumentotipo(edr.getDocumentotipo());
						doc.setDtemissao(bean.getDtemissao());
						doc.setDtvencimento(edr.getDtvencimento());
						doc.setValor(edr.getValor());
						doc.setValoratual(edr.getValor());
						doc.setDescricao("Documento referenciado na entrada fiscal " + bean.getCdentregadocumento());
						
						Rateio rateio = new Rateio();
						rateio.setListaRateioitem(new ListSet<Rateioitem>(Rateioitem.class));
						Rateioitem rateioitem = new Rateioitem();
						rateioitem.setRateio(rateio);
						rateioitem.setContagerencial(edr.getContagerencial());
						rateioitem.setCentrocusto(edr.getCentrocusto());
						rateioitem.setProjeto(edr.getProjeto());
						rateioitem.setPercentual(100.);
						rateioitem.setValor(edr.getValor());
						rateio.getListaRateioitem().add(rateioitem);
						doc.setRateio(rateio);
						
						documentoService.saveOrUpdate(doc);
						
						Documentohistorico dh = documentohistoricoService.geraHistoricoDocumento(doc);
						dh.setObservacao("Criado a partir do documento referenciado da entrada fiscal <a href=\"javascript:visualizaEntregadocumentoreferenciado("+bean.getCdentregadocumento()+");\">"+bean.getCdentregadocumento()+"</a>.");
						documentohistoricoService.saveOrUpdateNoUseTransaction(dh);
						
						Documentoorigem documentoorigem = new Documentoorigem();
						documentoorigem.setDocumento(doc);
						documentoorigem.setEntregadocumentoreferenciado(edr);
						documentoorigemService.saveOrUpdate(documentoorigem);
						
						if(edr.getContapaga() != null && edr.getContapaga()){
							BaixarContaBean bcb = new BaixarContaBean();
							bcb.setListaDocumento(new ListSet<Documento>(Documento.class));
							bcb.getListaDocumento().add(doc);
							bcb.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
							bcb.setVinculo(edr.getVinculo());
							bcb.setFormapagamento(edr.getFormapagamento());
							bcb.setRateio(doc.getRateio());
							bcb.setDtpagamento(edr.getDtpagamento());
							
							try{	
								contapagarService.transactionSaveBaixarConta(request, bcb);
							}catch (Exception e) {
								erros.append(doc.getCddocumento()+",");
							}
						}
					}
				}
				if(erros.length() > 0){
					request.addError("As seguintes contas n�o puderam ser baixadas automaticamente: " + erros.deleteCharAt(erros.length()-1).toString());
				}
			}
		}
	}
	
	/**
	* M�todo que calcula e atualiza o valor da entrada do material 
	*
	* @param entregadocumento
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public void calcularCustoEntradaMaterialByEntradafiscal(Entregadocumento entregadocumento) {
		if(entregadocumento != null && entregadocumento.getCdentregadocumento() != null){
			Entregadocumento bean = loadWithEntrega(entregadocumento);
			Boolean freteRateio = SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentofreterateio()); 
			if(bean != null && (bean.getEntrega() != null && bean.getEntrega().getCdentrega() != null) || (freteRateio != null && freteRateio)){
				calcularCustoEntradaMaterial(bean.getEntrega(), bean, freteRateio);
			}
		}
	}
	
	/**
	* M�todo que calcula e atualiza o valor da entrada do material
	*
	* @param entrega
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public void calcularCustoEntradaMaterial(Entrega entrega, Entregadocumento entregadocumento, Boolean freteRateio){
		if(formulacustoService.existsFormulaAtiva() && 
				"FALSE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.ENTREGA_NAOATUALIZACUSTO))){
			if((entrega != null && entrega.getCdentrega() != null && movimentacaoestoqueService.existEntradaByEntrega(entrega)) || (freteRateio != null && freteRateio)){
				List<Entregadocumento> listaEntregadocumento = findForCalcularEntradaMaterial(entrega, entregadocumento);
				if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
					Formulacusto formulacusto = formulacustoService.getFormulacustoForcalcularCustoEntradaMaterial();
					if(formulacusto != null && SinedUtil.isListNotEmpty(formulacusto.getListaFormulacustoitem())){
						Collections.sort(formulacusto.getListaFormulacustoitem(), new Comparator<Formulacustoitem>(){
							public int compare(Formulacustoitem f1, Formulacustoitem f2) {
								return f1.getOrdem().compareTo(f2.getOrdem());
							}
						});
						
						HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregamaterialFreterateio = new HashMap<Integer, List<Entregadocumentofreterateio>>();
						HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregadocumentoFreterateio = new HashMap<Integer, List<Entregadocumentofreterateio>>();
						String whereInEntregadocumentoVinculado = preencherEntregadocumentoComFreterateio(listaEntregadocumento, mapEntregamaterialFreterateio, mapEntregadocumentoFreterateio);
						
						List<Material> listaMaterial = new ArrayList<Material>();
						List<Entregamaterial> listaEntregamaterial = new ArrayList<Entregamaterial>();
						if(StringUtils.isNotBlank(whereInEntregadocumentoVinculado)){
							List<Entregadocumento> listaEntregadocumentoVinculado = findForCalcularEntradaMaterial(whereInEntregadocumentoVinculado);
							if(SinedUtil.isListNotEmpty(listaEntregadocumentoVinculado)){
								listaEntregadocumento.addAll(listaEntregadocumentoVinculado);
							}
						}
						
						for(Entregadocumento ed : listaEntregadocumento){
							if(ed.getEntrega() == null || !movimentacaoestoqueService.existEntradaByEntrega(ed.getEntrega())) continue;
							
							Money valorTotalFreteRateado = getValorTotalFreteRateo(ed, mapEntregadocumentoFreterateio);
							
//							if((ed.getSimplesremessa() == null || !ed.getSimplesremessa()) &&
//									(ed.getEntrega() == null || 
//									ed.getEntrega().getFaturamentocliente() == null ||
//									!ed.getEntrega().getFaturamentocliente())){
								ed.setValordocumento(ed.getValortotaldocumento());
								NotaVO notaVO = new NotaVO(ed);
								if(SinedUtil.isListNotEmpty(ed.getListaEntregamaterial())){
									for(Entregamaterial em : ed.getListaEntregamaterial()){
										if(em.getMaterial() != null && em.getMaterial().getCdmaterial() != null &&
												em.getCfop() != null && em.getCfop().getAtualizarvalor() != null && 
												em.getCfop().getAtualizarvalor()){
											
											if(em.getUnidademedidacomercial() != null && em.getMaterial() != null && 
													em.getMaterial().getUnidademedida() != null &&
													!em.getMaterial().getUnidademedida().equals(em.getUnidademedidacomercial())){
												em.setQtde_convertida(unidademedidaService.converteQtdeUnidademedida(
														em.getMaterial().getUnidademedida(), 
														em.getQtde(),
														em.getUnidademedidacomercial(),
														em.getMaterial(),null,1.0)
														);
											}
											
											MaterialVO materialVO = new MaterialVO(em);
											FreteVO freteVO = new FreteVO(valorTotalFreteRateado, getValorTotalFreteRateadoMaterial(em, mapEntregamaterialFreterateio));
											OutrosVO outrosVO = new OutrosVO();
											
											ScriptEngineManager manager = new ScriptEngineManager();
											ScriptEngine engine = manager.getEngineByName("JavaScript");
											
											engine.put("material", materialVO);
											engine.put("nota", notaVO);
											engine.put("frete", freteVO);
											engine.put("outros", outrosVO);
											
											Double valorEntrada = 0d;
											boolean erro = false;
											String msgm = "";
											for (Formulacustoitem formulacustoitem : formulacusto.getListaFormulacustoitem()) {
												try{
													Object obj = engine.eval(formulacustoitem.getFormula());
									
													Double resultado = 0d;
													if(obj != null){
														String resultadoStr = obj.toString();
														resultado = new Double(resultadoStr);
													}
													
													engine.put(formulacustoitem.getIdentificador(), resultado);
													if(!Double.isInfinite(resultado)){
														valorEntrada = SinedUtil.roundByParametro(resultado);
													}else {
														erro = true;
													}
												} catch (ScriptException e) {
													e.printStackTrace();
													erro = true;
												}
												if(erro){
													msgm = "Erro ao calcular o valor de custo. Frmula: " + 
													formulacusto.getNome() + ". (" +
													formulacustoitem.getIdentificador() + " - " +
													formulacustoitem.getFormula() + " )";
													break;
												}
											}
											
											if(erro){
												NeoWeb.getRequestContext().addError(msgm);
											}else {
												em.setValorEntrada(valorEntrada);
												listaEntregamaterial.add(em);
												if(!listaMaterial.contains(em.getMaterial())){
													listaMaterial.add(em.getMaterial());
												}
											}
										}
									}
								}
//							}
						}
						if(SinedUtil.isListNotEmpty(listaEntregamaterial) && SinedUtil.isListNotEmpty(listaMaterial)){
							List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByEntregamaterial(CollectionsUtil.listAndConcatenate(listaEntregamaterial, "cdentregamaterial", ","));
							if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
								for(Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque){
									if(movimentacaoestoque.getMovimentacaoestoqueorigem() != null && 
											movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial() != null &&
											movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial().getCdentregamaterial() != null){
										for(Entregamaterial entregamaterial : listaEntregamaterial){
											if(movimentacaoestoque.getMovimentacaoestoqueorigem().getEntregamaterial().getCdentregamaterial().equals(entregamaterial.getCdentregamaterial())){
												movimentacaoestoqueService.preencheValorCusto(entregamaterial.getValorEntrada(), movimentacaoestoque);
											
												listaMaterial.remove(movimentacaoestoque.getMaterial());
												break;
											}
										}
									}
								}
//								materialService.recalcularCustoMedio(CollectionsUtil.listAndConcatenate(listaMaterial, "cdmaterial", ","));
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param listaEntregadocumento
	* @param mapEntregamaterialFreterateio
	* @param mapEntregadocumentoFreterateio
	* @return
	* @since 30/03/2016
	* @author Luiz Fernando
	*/
	private String preencherEntregadocumentoComFreterateio(List<Entregadocumento> listaEntregadocumento, HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregamaterialFreterateio, HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregadocumentoFreterateio) {
		if(SinedUtil.isListNotEmpty(listaEntregadocumento)){
			List<Entregadocumentofreterateio> lista = entregadocumentofreterateioService.findForCalcularCusto(CollectionsUtil.listAndConcatenate(listaEntregadocumento, "cdentregadocumento", ","));
			if(SinedUtil.isListNotEmpty(lista)){
				List<Entregadocumento> listaED = new ArrayList<Entregadocumento>();
				if(mapEntregamaterialFreterateio == null) mapEntregamaterialFreterateio = new HashMap<Integer, List<Entregadocumentofreterateio>>();
				if(mapEntregadocumentoFreterateio == null) mapEntregadocumentoFreterateio = new HashMap<Integer, List<Entregadocumentofreterateio>>();
				
				for(Entregadocumentofreterateio edfr : lista){
					if(edfr.getEntregamaterial() != null && edfr.getEntregamaterial().getCdentregamaterial() != null){
						if(edfr.getEntregamaterial().getEntregadocumento() != null && edfr.getEntregamaterial().getEntregadocumento().getCdentregadocumento() != null){
							List<Entregadocumentofreterateio> listaEDFR = mapEntregadocumentoFreterateio.get(edfr.getEntregamaterial().getEntregadocumento().getCdentregadocumento());
							if(listaEDFR == null) listaEDFR = new ArrayList<Entregadocumentofreterateio>();
							listaEDFR.add(edfr);
							mapEntregadocumentoFreterateio.put(edfr.getEntregamaterial().getEntregadocumento().getCdentregadocumento(), listaEDFR);
							
							if(!listaED.contains(edfr.getEntregamaterial().getEntregadocumento())){
								listaED.add(edfr.getEntregamaterial().getEntregadocumento());
							}
						}
						
						List<Entregadocumentofreterateio> listaEDFR = mapEntregamaterialFreterateio.get(edfr.getEntregamaterial().getCdentregamaterial());
						if(listaEDFR == null) listaEDFR = new ArrayList<Entregadocumentofreterateio>();
						listaEDFR.add(edfr);
						mapEntregamaterialFreterateio.put(edfr.getEntregamaterial().getCdentregamaterial(), listaEDFR);
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaED))
					return CollectionsUtil.listAndConcatenate(listaED, "cdentregadocumento", ",");
			}
		}
		return null;
	}
	
	/**
	* M�todo que retorna o rateio frete do item da entrada fiscal 
	*
	* @param entregadocumento
	* @param entregamaterial
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	private Money getValorTotalFreteRateadoMaterial(Entregamaterial entregamaterial, HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregamaterialFreterateio) {
		Money valor = new Money();
		if(entregamaterial != null && entregamaterial.getCdentregamaterial() != null && mapEntregamaterialFreterateio != null && 
				mapEntregamaterialFreterateio.size() > 0){
			List<Entregadocumentofreterateio> lista = mapEntregamaterialFreterateio.get(entregamaterial.getCdentregamaterial());
			if(SinedUtil.isListNotEmpty(lista)){
				for(Entregadocumentofreterateio edfr : lista){
					if(edfr.getValor() != null){
						valor = valor.add(edfr.getValor());
					}
				}
			}
		}
		return valor;
	}
	
	/**
	* M�todo que retorna o total do rateio da entrada fiscal
	*
	* @param entregadocumento
	* @param mapEntregamaterialFreterateio
	* @return
	* @since 30/03/2016
	* @author Luiz Fernando
	*/
	private Money getValorTotalFreteRateo(Entregadocumento entregadocumento, HashMap<Integer, List<Entregadocumentofreterateio>> mapEntregamaterialFreterateio) {
		Money valor = new Money();
		if(entregadocumento != null && entregadocumento.getCdentregadocumento() != null && mapEntregamaterialFreterateio != null && 
				mapEntregamaterialFreterateio.size() > 0){
			List<Entregadocumentofreterateio> lista = mapEntregamaterialFreterateio.get(entregadocumento.getCdentregadocumento());
			if(SinedUtil.isListNotEmpty(lista)){
				for(Entregadocumentofreterateio edfr : lista){
					if(edfr.getValor() != null){
						valor = valor.add(edfr.getValor());
					}
				}
			}
		}
		return valor;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#findForCalcularEntradaMaterial(Entrega entrega)
	*
	* @param entrega
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	 * @param entregadocumento 
	*/
	private List<Entregadocumento> findForCalcularEntradaMaterial(Entrega entrega, Entregadocumento entregadocumento) {
		return entradafiscalDAO.findForCalcularEntradaMaterial(entrega, (entregadocumento != null && entregadocumento.getCdentregadocumento() != null ? entregadocumento.getCdentregadocumento().toString() : null));
	}
	
	private List<Entregadocumento> findForCalcularEntradaMaterial(String whereIn) {
		return entradafiscalDAO.findForCalcularEntradaMaterial(null, whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EntradafiscalDAO#loadWithEntrega(Entregadocumento entregadocumento)
	*
	* @param entregadocumento
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public Entregadocumento loadWithEntrega(Entregadocumento entregadocumento){
		return entradafiscalDAO.loadWithEntrega(entregadocumento);
	}
	
	public ModelAndView abrirPopUpApuracaoicms(WebRequestContext request, Entregamaterial entregamaterial, String modulo){
		request.setAttribute("consultar", Boolean.valueOf(request.getParameter("consultar") != null ? request.getParameter("consultar") : "false"));
		return new ModelAndView("direct:"+modulo+"/popup/apuracaoicms", "bean", entregamaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param chaveacesso
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Entregadocumento> findByChaveacesso(String chaveacesso){
		return entradafiscalDAO.findByChaveacesso(chaveacesso);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Entregadocumento> findAllForConferenciaCte(String whereIn){
		return entradafiscalDAO.findAllForConferenciaCte(whereIn);
	}
	
	/**
	* M�todo verifica se o ajuste na entrada fiscal foi excluido e registra no historico 
	*
	* @param listaAjusteFiscal
	* @param entregadocumento
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public void verificaExclusaoAjusteFiscal(Set<Ajustefiscal> listaAjusteFiscal, Entregadocumento entregadocumento) {
		if(SinedUtil.isListNotEmpty(listaAjusteFiscal) && entregadocumento != null && 
				entregadocumento.getCdentregadocumento() != null){
			boolean registrarHistorico = false;
			for(Ajustefiscal ajustefiscalAnterior : listaAjusteFiscal){
				boolean existeItem = false;
				if(SinedUtil.isListNotEmpty(entregadocumento.getListaAjustefiscal())){
					for(Ajustefiscal ajustefiscalAtual : entregadocumento.getListaAjustefiscal()){
						if(ajustefiscalAtual.getCdajustefiscal() != null && 
								ajustefiscalAtual.getCdajustefiscal().equals(ajustefiscalAnterior.getCdajustefiscal())){
							existeItem = true;
							break;
						}
					}
				}
				if(!existeItem){
					registrarHistorico = true;
					break;
				}
			}
			
			if(registrarHistorico){
				Entregadocumentohistorico entregadocumentohistorico = new Entregadocumentohistorico();
				entregadocumentohistorico.setEntregadocumento(entregadocumento);
				entregadocumentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				entregadocumentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
				entregadocumentohistorico.setObservacao("Removido o item de ajuste fiscal.");
				entregadocumentohistoricoService.saveOrUpdate(entregadocumentohistorico);
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param listaAjusteFiscal
	* @param notafiscalproduto
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	/**
	* M�todo verifica se o ajuste da nota fiscal foi excluido e registra no historico 
	*
	* @param listaAjusteFiscal
	* @param notafiscalproduto
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public void verificaExclusaoAjusteFiscal(Set<Ajustefiscal> listaAjusteFiscal, Notafiscalproduto notafiscalproduto) {
		if(SinedUtil.isListNotEmpty(listaAjusteFiscal) && notafiscalproduto != null && 
				notafiscalproduto.getCdNota() != null){
			boolean registrarHistorico = false;
			for(Ajustefiscal ajustefiscalAnterior : listaAjusteFiscal){
				boolean existeItem = false;
				if(SinedUtil.isListNotEmpty(notafiscalproduto.getListaAjustefiscal())){
					for(Ajustefiscal ajustefiscalAtual : notafiscalproduto.getListaAjustefiscal()){
						if(ajustefiscalAtual.getCdajustefiscal() != null && 
								ajustefiscalAtual.getCdajustefiscal().equals(ajustefiscalAnterior.getCdajustefiscal())){
							existeItem = true;
							break;
						}
					}
				}
				if(!existeItem){
					registrarHistorico = true;
					break;
				}
			}
			
			if(registrarHistorico){
				NotaHistorico notahistorico = new NotaHistorico();
				notahistorico.setNota(notafiscalproduto);
				notahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
				notahistorico.setNotaStatus(notafiscalproduto.getNotaStatus());
				notahistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdusuarioaltera());
				notahistorico.setObservacao("Removido o item de ajuste fiscal.");
				notaHistoricoService.saveOrUpdate(notahistorico);
			}
		}
	}
	
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.EntradafiscalDAO#carregarDocumentoParaAjusteFiscal(Entregadocumento entregadocumento)
	*
	* @param entregadocumento
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public Entregadocumento carregarDocumentoParaAjusteFiscal(Entregadocumento entregadocumento) {
		return entradafiscalDAO.carregarDocumentoParaAjusteFiscal(entregadocumento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EntregaDAO#findForSpedRegK200
	 *
	 * @param filtro
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Entregadocumento> findForSpedRegK200(Material material, SpedarquivoFiltro filtro) {
		return entradafiscalDAO.findForSpedRegK200(material, filtro);
	}
	
	/**
	* M�todo ajax para preencher o array de fun��o de icmsst
	*
	* @param request
	* @param entregadocumento
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView preencherArrayFuncaoICMSSTAjax(WebRequestContext request, Entregadocumento entregadocumento){
		ArrayList<String> funcaoBaseCalculoICMS = new ArrayList<String>();
		ArrayList<String> funcaoBaseCalculoICMSST = new ArrayList<String>();
		ArrayList<String> funcaoValorICMSST = new ArrayList<String>();
		
		if (entregadocumento != null && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())) {
			StringBuilder whereInGrupotributacao = new StringBuilder();
			for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
				if(entregamaterial.getGrupotributacao() != null && entregamaterial.getGrupotributacao().getCdgrupotributacao() != null){
					whereInGrupotributacao.append(entregamaterial.getGrupotributacao().getCdgrupotributacao()).append(",");
				}
			}
			if(whereInGrupotributacao.length() > 0){
				List<Grupotributacao> listaGrupotributacao = grupotributacaoService.findForBaseCalculoICMSSTJavascript(whereInGrupotributacao.substring(0, whereInGrupotributacao.length()-1));
				if(SinedUtil.isListNotEmpty(listaGrupotributacao)){
					for(Grupotributacao grupotributacao : listaGrupotributacao){
						if(grupotributacao.getCdgrupotributacao() != null){
							for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
								if(entregamaterial.getGrupotributacao() != null && entregamaterial.getGrupotributacao().getCdgrupotributacao() != null &&
										grupotributacao.getCdgrupotributacao().equals(entregamaterial.getGrupotributacao().getCdgrupotributacao())){
									entregamaterial.setGrupotributacao(grupotributacao);
								}
							}
						}
					}
				}
			}
		}
		if (entregadocumento != null && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())) {
			for (Entregamaterial entrega : entregadocumento.getListaEntregamaterial()) {
				funcaoBaseCalculoICMS.add(" fBCICMS = " + grupotributacaoService.criaFuncaoBaseCalculoICMSJavascript(entrega.getGrupotributacao(), entrega.getGrupotributacao() != null ? entrega.getGrupotributacao().getFormulabcicms() : null, "ENTRADAFISCAL"));
				funcaoBaseCalculoICMSST.add(" fBCICMSST = " + grupotributacaoService.criaFuncaoBaseCalculoICMSSTJavascript(entrega.getGrupotributacao(), entrega.getGrupotributacao() != null ? entrega.getGrupotributacao().getFormulabcst() : null, "ENTRADAFISCAL"));
				funcaoValorICMSST.add(" fVALORICMSST = " + grupotributacaoService.criaFuncaoValorICMSSTJavascript(entrega.getGrupotributacao(), entrega.getGrupotributacao() != null ? entrega.getGrupotributacao().getFormulavalorst() : null, "ENTRADAFISCAL"));
			}
		}
		
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		jsonModelAndView.addObject("funcaoBaseCalculoICMS", funcaoBaseCalculoICMS);
		jsonModelAndView.addObject("funcaoBaseCalculoICMSST", funcaoBaseCalculoICMSST);
		jsonModelAndView.addObject("funcaoValorICMSST", funcaoValorICMSST);
		return jsonModelAndView;
	}
	
	/**
	* M�todo que converte a quantidade restante ao trocar a unidade de medida
	*
	* @param request
	* @param entregamaterial
	* @return
	* @since 01/09/2015
	* @author Luiz Fernando
	*/
	public ModelAndView converteUnidademedida(WebRequestContext request, Entregamaterial entregamaterial){
		Double resto = entregamaterial.getResto();
		Unidademedida unidademedida = entregamaterial.getUnidademedidacomercial();
		Unidademedida unidademedidaAnterior = entregamaterial.getUnidademedidacomercialAnterior();
		
		String simbolo = "";
		if(unidademedida != null){
			Unidademedida umSimbolo = unidademedidaService.load(unidademedida, "unidademedida.cdunidademedida, unidademedida.simbolo");
			simbolo = umSimbolo != null && umSimbolo.getSimbolo() != null ? umSimbolo.getSimbolo().toUpperCase() : "";
		}
		
		Material material = materialService.findListaMaterialunidademedida(entregamaterial.getMaterial());
		
		Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedidaAnterior);
		Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedida);
		
		if(fracao1 != null && fracao2 != null){
			resto = (resto * fracao1) / fracao2;
			
			if(SinedUtil.isListNotEmpty(entregamaterial.getListaOrdemcompraentregamaterial())){
				for(Ordemcompraentregamaterial ocem : entregamaterial.getListaOrdemcompraentregamaterial()){
					if(ocem.getResto() != null){
						ocem.setResto((ocem.getResto() * fracao1) / fracao2);
					}
					if(ocem.getQtde() != null){
						ocem.setQtde((ocem.getQtde() * fracao1) / fracao2);
					}
				}
			}
		} else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && 
						item.getUnidademedida().getCdunidademedida() != null && 
						item.getFracao() != null){
						
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
		
			if(fracao != null && fracao > 0){
				resto = resto * fracao;
				
				if(SinedUtil.isListNotEmpty(entregamaterial.getListaOrdemcompraentregamaterial())){
					for(Ordemcompraentregamaterial ocem : entregamaterial.getListaOrdemcompraentregamaterial()){
						if(ocem.getResto() != null){
							ocem.setResto((ocem.getResto() / fracao1) * fracao2);
						}
						if(ocem.getQtde() != null){
							ocem.setQtde((ocem.getQtde() * fracao1) / fracao2);
						}
					}
				}
			}
		}
		
		List<Double> listaRestoOCEM = new ArrayList<Double>();
		List<Double> listaQtdeOCEM = new ArrayList<Double>();
		if(SinedUtil.isListNotEmpty(entregamaterial.getListaOrdemcompraentregamaterial())){
			for(Ordemcompraentregamaterial ocem : entregamaterial.getListaOrdemcompraentregamaterial()){
				if(ocem.getResto() != null){
					listaRestoOCEM.add(ocem.getResto());
				}
				if(ocem.getQtde() != null){
					listaQtdeOCEM.add(ocem.getQtde());
				}
			}
		}
		return new JsonModelAndView()
					.addObject("simboloUM", simbolo)
					.addObject("resto", SinedUtil.descriptionDecimal(resto))
					.addObject("listaRestoOCEM", listaRestoOCEM)
					.addObject("listaQtdeOCEM", listaQtdeOCEM);
	}
	
	public void calculaQtdeRestanteEntregadocumento(Entregadocumento entregadocumento, boolean verificarConversaoQtde) {
		if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()){
			for (Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()) {
				calculaQtdeRestanteEntregadocumento(entregamaterial, verificarConversaoQtde);
			}
		}
	}
	
	public void calculaQtdeRestanteEntregadocumento(Entregamaterial entregamaterial, boolean verificarConversaoQtde) {
		Double resto = 0d;
		if(entregamaterial.getListaOrdemcompraentregamaterial() != null && entregamaterial.getListaOrdemcompraentregamaterial().size() > 0){
			for (Ordemcompraentregamaterial ordemcompraentregamaterial : entregamaterial.getListaOrdemcompraentregamaterial()) {
				if(ordemcompraentregamaterial.getOrdemcompramaterial() != null && ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida() == null){
					Ordemcompramaterial ocm = ordemcompramaterialService.loadWithUnidademedida(ordemcompraentregamaterial.getOrdemcompramaterial());
					if(ocm != null){
						ordemcompraentregamaterial.getOrdemcompramaterial().setUnidademedida(ocm.getUnidademedida());
					}
				}
				entregamaterial.setOcm(ordemcompraentregamaterial.getOrdemcompramaterial());
				Double restoIt = entregamaterialService.getQtdeByEntregaMaterial(entregamaterial);
				if(entregamaterial.getUnidademedidacomercial() != null && entregamaterial.getMaterial() != null && 
						entregamaterial.getMaterial().getUnidademedida() != null && 
						!entregamaterial.getMaterial().getUnidademedida().equals(entregamaterial.getUnidademedidacomercial())){
					Double fracao = unidademedidaService.getFatorconversao(entregamaterial.getMaterial().getUnidademedida(), restoIt, entregamaterial.getUnidademedidacomercial(), entregamaterial.getMaterial(), null, 1d);
					if(fracao != null){
						restoIt = restoIt * fracao;
					}
				}
				if(verificarConversaoQtde && entregamaterial.getCdentregamaterial() == null && entregamaterial.getUnidademedidacomercial() != null && entregamaterial.getMaterial() != null && 
						ordemcompraentregamaterial.getOrdemcompramaterial() != null &&
						ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida() != null && 
						!ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida().equals(entregamaterial.getUnidademedidacomercial())){
					Double fracao = unidademedidaService.getFatorconversao(ordemcompraentregamaterial.getOrdemcompramaterial().getUnidademedida(), restoIt, entregamaterial.getUnidademedidacomercial(), entregamaterial.getMaterial(), null, 1d);
					if(fracao != null){
						ordemcompraentregamaterial.setQtde(ordemcompraentregamaterial.getQtde() * fracao);
					}
				}
				if(restoIt != null && ordemcompraentregamaterial.getQtde() != null){
					restoIt = SinedUtil.round(restoIt - ordemcompraentregamaterial.getQtde(), 10);
				}
				ordemcompraentregamaterial.setResto(restoIt);
				resto += restoIt;
			}
		}
		
		entregamaterial.setResto(resto);
	}
	
	/**
	* M�todo que verifica se o material est� no lote, caso contr�rio, inclui o item no lote
	*
	* @param entregadocumento
	* @since 23/09/2016
	* @author Luiz Fernando
	*/
	public void verificarMaterialLote(Entregadocumento entregadocumento) {
		if(entregadocumento != null && 
				entregadocumento.getCdentregadocumento() != null && 
				SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())/* && 
				entregadocumento.getFornecedor() != null &&
				parametrogeralService.getBoolean(Parametrogeral.RECEBIMENTO_LOTE_POR_FORNECEDOR)*/){
			for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
				if(SinedUtil.isListNotEmpty(entregamaterial.getListaEntregamateriallote())){
					for(Entregamateriallote entregamateriallote : entregamaterial.getListaEntregamateriallote()){
						if(entregamateriallote.getLoteestoque() != null && entregamaterial.getMaterial() != null){
							loteestoqueService.verificarEIncluirMaterialNoLote(entregamateriallote.getLoteestoque(), entregamaterial.getMaterial());
						}
					}
				}else if(entregamaterial.getLoteestoque() != null && entregamaterial.getMaterial() != null){
					loteestoqueService.verificarEIncluirMaterialNoLote(entregamaterial.getLoteestoque(), entregamaterial.getMaterial());
				}
			}
		}
		
	}
	
	/**
	* M�todo que abre a popup para escolher lote para os itens da entrada fiscal
	*
	* @param request
	* @param entregamaterial
	* @param modulo
	* @return
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public ModelAndView gerenciarLote(WebRequestContext request, Entregamaterial entregamaterial, String modulo) {
		if(entregamaterial.getListaEntregamateriallote() == null || entregamaterial.getListaEntregamateriallote().size() == 0){
			entregamaterial.getListaEntregamateriallote().add(new Entregamateriallote());
			entregamaterial.getListaEntregamateriallote().add(new Entregamateriallote());
		}
		request.setAttribute("consultaitemlote", Boolean.valueOf(request.getParameter("consultar") != null ? request.getParameter("consultar") : "false"));
		return new ModelAndView("direct:"+modulo+"/popup/popupEntregamateriallote","bean", entregamaterial);
	}
	
	/**
	* M�todo que carrega a lista de lote do item da entrada fiscal
	*
	* @param entregadocumento
	* @since 27/10/2016
	* @author Luiz Fernando
	*/
	public void carregaListaEntregamateriallote(Entregadocumento entregadocumento) {
		if(entregadocumento != null && entregadocumento.getCdentregadocumento() != null &&
				SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
			List<Entregamateriallote> lista = entregamaterialloteService.findByEntregadocumento(entregadocumento);
			if(SinedUtil.isListNotEmpty(lista)){
				for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
					entregamaterial.setListaEntregamateriallote(entregamaterialService.getListaEntregamateriallote(entregamaterial, lista));
				}
			}
		}
	}
	
	/**
	* M�todo que verifica se n�o existe documento em aberto para a entrada fiscal e executa update na situa��o da entrada fiscal
	*
	* @param whereInDocumento
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public void verificaEntradafiscalAposCancelamentoDocumento(String whereInDocumento) {
		List<Entregadocumento> lista = findByDocumento(whereInDocumento);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Entregadocumento entregadocumento : lista){
				if(!documentoorigemService.existeDocumentosNaoCanceladoEntradafiscal(entregadocumento)){
					updateSituacaoEntradafiscal(entregadocumento, Entregadocumentosituacao.REGISTRADA);
				}
			}
		}
		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.EntradafiscalService#findByDocumento(String whereInDocumento)
	*
	* @param whereInDocumento
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public List<Entregadocumento> findByDocumento(String whereInDocumento) {
		return entradafiscalDAO.findByDocumento(whereInDocumento);
	}
	
	public boolean haveEntradafiscalSituacao(String whereIn, boolean not, Entregadocumentosituacao... situacoes) {
		return entradafiscalDAO.haveEntradafiscalSituacao(whereIn, not, situacoes);
	}
	
	public ModelAndView ajaxBuscarDocumentoreferenciado(WebRequestContext request, Entregamaterial entregamaterial) {
		List<Documentoreferenciado> lista = buscarDocumentoreferenciado(entregamaterial, entregamaterial.getEmpresa());
		if(SinedUtil.isListNotEmpty(lista)){
			for(Documentoreferenciado dr : lista){
				if(dr.getFornecedor() != null && dr.getFornecedor().getNome() != null){
					dr.getFornecedor().setNome(new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(dr.getFornecedor().getNome()));
				}
			}
		}
		return new JsonModelAndView().addObject("listaDocumentoreferenciado", lista);
	}
	
	public List<Documentoreferenciado> buscarDocumentoreferenciado(Entregamaterial entregamaterial, Empresa empresa) {
		List<Documentoreferenciado> listaDocumentoreferenciado = new ArrayList<Documentoreferenciado>();
		if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getCdmaterial() != null){
			Material material = materialService.load(entregamaterial.getMaterial(), "material.cdmaterial, material.servico");
			if(material != null && Boolean.TRUE.equals(material.getServico())){
				listaDocumentoreferenciado = documentoreferenciadoService.findByImposto(getListaImpostoRetido(entregamaterial), empresa);
				setValorImpostoDocReferenciado(listaDocumentoreferenciado, entregamaterial);
			}
		}
		return listaDocumentoreferenciado;
	}
	
	private void setValorImpostoDocReferenciado(List<Documentoreferenciado> listaDocumentoreferenciado, Entregamaterial entregamaterial) {
		if(SinedUtil.isListNotEmpty(listaDocumentoreferenciado) && entregamaterial != null){
			for(Documentoreferenciado dr : listaDocumentoreferenciado){
				dr.setIdentificadorinternoentregamaterial(entregamaterial.getIdentificadorinterno());
				if(Documentoreferenciadoimposto.ISS.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValoriss());
				}else if(Documentoreferenciadoimposto.COFINS.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValorcofinsretido());
				}else if(Documentoreferenciadoimposto.IR.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValorir());
				}else if(Documentoreferenciadoimposto.PIS.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValorpisretido());
				}else if(Documentoreferenciadoimposto.CSLL.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValorcsll());
				}else if(Documentoreferenciadoimposto.INSS.equals(dr.getImposto())){
					dr.setValor(entregamaterial.getValorinss());
				}
			}
		}
		
	}
	
	private List<Documentoreferenciadoimposto> getListaImpostoRetido(Entregamaterial entregamaterial) {
		List<Documentoreferenciadoimposto> listaImposto = new ArrayList<Documentoreferenciadoimposto>();
		
		if(entregamaterial != null){
			if(entregamaterial.getValorir() != null && entregamaterial.getValorir().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.IR);
			}
			if(entregamaterial.getValorcsll() != null && entregamaterial.getValorcsll().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.CSLL);
			}
			if(entregamaterial.getValorinss() != null && entregamaterial.getValorinss().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.INSS);
			}
			if(entregamaterial.getValoriss() != null && entregamaterial.getValoriss().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.ISS);
			}
			if(entregamaterial.getValorpisretido() != null && entregamaterial.getValorpisretido().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.PIS);
			}
			if(entregamaterial.getValorcofinsretido() != null && entregamaterial.getValorcofinsretido().getValue().doubleValue() > 0){
				listaImposto.add(Documentoreferenciadoimposto.COFINS);
			}
		}
		return listaImposto;
	}
	
	public Set<Entregamaterial> getListaEntregamaterialByPedidovenda(String whereInPedidovenda, String whereInColeta) {
		Set<Entregamaterial> listaEntregamaterial = new ListSet<Entregamaterial>(Entregamaterial.class);
		
		List<Pedidovendamaterial> listaPedidovendamaterial = pedidovendamaterialService.findForEntradafiscal(whereInPedidovenda, whereInColeta);
		Integer controle = 0;
		for (Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial) {
			Entregamaterial entregamaterial = new Entregamaterial();
			setIdentificadorinterno(entregamaterial, controle);
			controle++;
			
			Material material = pedidovendamaterial.getMaterialcoleta() != null ? pedidovendamaterial.getMaterialcoleta() : pedidovendamaterial.getMaterial();
			List<Materialclasse> listaMaterialclasse = materialService.findClasses(material);
			
			entregamaterial.setMaterial(material);
			if(listaMaterialclasse != null && listaMaterialclasse.size() > 0){
				entregamaterial.setMaterialclasse(listaMaterialclasse.get(0));
			}
			entregamaterial.setQtde(pedidovendamaterial.getQuantidade());
			entregamaterial.setUnidademedidacomercial(pedidovendamaterial.getUnidademedida());
			if(pedidovendamaterial.getValorcoleta() != null){
				entregamaterial.setValorunitario(pedidovendamaterial.getValorcoleta());
			}else {
				entregamaterial.setValorunitario(material.getValorcusto());
			}
			entregamaterial.setPedidovendamaterial(pedidovendamaterial);
			
			listaEntregamaterial.add(entregamaterial);
		}
		return listaEntregamaterial;
	}
	
	public void setDocumentoreferenciado(Entrega entrega) {
		if(entrega != null && SinedUtil.isListNotEmpty(entrega.getListaEntregadocumento())){
			for(Entregadocumento entregadocumento : entrega.getListaEntregadocumento()){
				setDocumentoreferenciado(entregadocumento);
			}
		}
	}
	
	public void setDocumentoreferenciado(Entregadocumento entregadocumento) {
		if(entregadocumento != null && entregadocumento.getCdentregadocumento() == null && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())){
			for(Entregamaterial entregamaterial : entregadocumento.getListaEntregamaterial()){
				List<Documentoreferenciado> lista = buscarDocumentoreferenciado(entregamaterial, entregadocumento.getEmpresa());
				if(SinedUtil.isListNotEmpty(lista) && entregamaterial.getIdentificadorinterno() != null){
					List<Documentoreferenciadoimposto> listaImposto = new ArrayList<Documentoreferenciadoimposto>();
					if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentoreferenciado())){
						for(Entregadocumentoreferenciado edr : entregadocumento.getListaEntregadocumentoreferenciado()){
							if(entregamaterial.getIdentificadorinterno().equals(edr.getIdentificadorinternoentregamaterial()) && edr.getImposto() != null){
								listaImposto.add(edr.getImposto());
							}
						}
					}
					if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentoreferenciadoTrans())){
						for(Entregadocumentoreferenciado edr : entregadocumento.getListaEntregadocumentoreferenciadoTrans()){
							if(entregamaterial.getIdentificadorinterno().equals(edr.getIdentificadorinternoentregamaterial()) && edr.getImposto() != null){
								listaImposto.add(edr.getImposto());
							}
						}
					}
					
					for(Documentoreferenciado dr : lista){
						if(!listaImposto.contains(dr.getImposto())){
							if(entregadocumento.getListaEntregadocumentoreferenciadoTrans() == null){
								entregadocumento.setListaEntregadocumentoreferenciadoTrans(new ListSet<Entregadocumentoreferenciado>(Entregadocumentoreferenciado.class));
							}
							entregadocumento.getListaEntregadocumentoreferenciadoTrans().add(new Entregadocumentoreferenciado(dr));
						}
					}
				}
			}
		}
		
	}

	public boolean existeNotaDevolucao(Entregadocumento entregadocumento) {
		return entradafiscalDAO.existeNotaDevolucao(entregadocumento);
	}
	
	public Entregadocumento loadForCancelamento(Entregadocumento entregadocumento) {
		return entradafiscalDAO.loadForCancelamento(entregadocumento);
	}
		
	public ModelAndView ajaxInfoFornecedor(WebRequestContext request, Entregadocumento doc){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		if(doc.getFornecedor() != null && doc.getFornecedor().getCdpessoa() != null){
			Fornecedor fornecedor = fornecedorService.loadForOrdemcompraInfo(doc.getFornecedor());
			jsonModelAndView.addObject("cpfcnpj",  fornecedor.getCpfOuCnpj() );
			
			Naturezaoperacao naturezaoperacao = null;
			if (fornecedor.getNaturezaoperacao() != null) {
				naturezaoperacao = fornecedor.getNaturezaoperacao();
			} else {
				naturezaoperacao = naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL);
			}
			jsonModelAndView.addObject("naturezaoperacao",  naturezaoperacao);
		}
		
		return jsonModelAndView;
	}
	
	public void criarAvisoEntradaFiscalSemVinculoFinanceiro(Motivoaviso m, Date data, Date dateToSearch) {
		List<Entregadocumento> entregadocumentoList = entradafiscalDAO.findByEntradaFiscalSemVinculoFinanceiro(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Entregadocumento e : entregadocumentoList) {
			Aviso aviso = new Aviso("Entrada fiscal sem v�nculo a um registro financeiro", "C�digo da entrada fiscal: " + e.getCdentregadocumento(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), e.getCdentregadocumento(), e.getEmpresa() != null ? e.getEmpresa() : empresa, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, e.getCdentregadocumento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, e.getCdentregadocumento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public JsonModelAndView ajaxInfoNaturezaOperacao(Entregadocumento doc) {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(doc.getFornecedor() != null && doc.getFornecedor().getCdpessoa() != null){
			Fornecedor fornecedor = fornecedorService.loadForOrdemcompraInfo(doc.getFornecedor());
			
			if (fornecedor.getNaturezaoperacao() != null) {
				if(fornecedor.getNaturezaoperacao() != null){
					if(!Hibernate.isInitialized(fornecedor.getNaturezaoperacao().getListaNaturezaoperacaocfop())){
						fornecedor.getNaturezaoperacao().setListaNaturezaoperacaocfop(null);
					}
					if(!Hibernate.isInitialized(fornecedor.getNaturezaoperacao().getListaNaturezaoperacaocst())){
						fornecedor.getNaturezaoperacao().setListaNaturezaoperacaocst(null);
					}
					if(!Hibernate.isInitialized(fornecedor.getNaturezaoperacao().getNotaTipo())){
						fornecedor.getNaturezaoperacao().setNotaTipo(null);
					}
				}
				jsonModelAndView.addObject("naturezaoperacao", fornecedor.getNaturezaoperacao());
			} else {
				jsonModelAndView.addObject("naturezaoperacao", naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
			}
		} else {
			jsonModelAndView.addObject("naturezaoperacao", naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
		}
		
		return jsonModelAndView;
	}
	
	public Entregadocumento findEntradaFiscalByNumeroSerie(String numero, String serie) {
		return entradafiscalDAO.findEntradaFiscalByNumeroSerie(numero, serie);
	}
	
	public ModelAndView ajaxIsApropriacao(WebRequestContext request, Entregadocumento entregaDocumento){
		return new JsonModelAndView().addObject("isApropriacao", documentotipoService.isApropriacao(request, entregaDocumento.getDocumentotipo()));
	}
	
	public void ajaxMontaApropriacoes(WebRequestContext request, Entregadocumento entregaDocumento){
		List<DocumentoApropriacao> lista = new ArrayList<DocumentoApropriacao>();
		Prazopagamento prazo = null;
		if(entregaDocumento.getDocumentotipo() != null) {
			Documentotipo documentoTipo = documentotipoService.loadWithPrazoApropriacao(entregaDocumento.getDocumentotipo());
			prazo = documentoTipo!= null && documentoTipo.getPrazoApropriacao() != null? prazopagamentoService.loadForEntrada(documentoTipo.getPrazoApropriacao()): null;
			
			if(prazo != null && prazo.getListaPagamentoItem() != null) {
				for(Prazopagamentoitem item: prazo.getListaPagamentoItem()){
					Integer dias = item.getDias();
					DocumentoApropriacao bean = new DocumentoApropriacao();
					if(dias != null){
						Date dataApropriacao = SinedDateUtils.addDiasData(entregaDocumento.getDataForGeracaoContabil(), dias);
						dataApropriacao = SinedDateUtils.firstDateOfMonth(dataApropriacao);
						bean.setMesAno(dataApropriacao);
					}else{
						Date dataApropriacao = SinedDateUtils.addMesData(entregaDocumento.getDataForGeracaoContabil(), item.getMeses());
						dataApropriacao = SinedDateUtils.firstDateOfMonth(dataApropriacao);
						bean.setMesAno(dataApropriacao);
					}
					
					lista.add(bean);
				}
			}
		}
		
		View.getCurrent().convertObjectToJs(lista, "listaApropriacao");
		View.getCurrent().println("var parcelas = " + (prazo != null && prazo.getListaPagamentoItem() != null ? prazo.getListaPagamentoItem().size() : "0") +";");
	}
	
	public void carregarEntregadocumento(WebRequestContext request, Entregadocumento form, boolean fromRecebimento) {
		String identificadortela = new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis());
		if(form.getIdentificadortela() == null) {
			form.setIdentificadortela(identificadortela);
		}
		
		if(fromRecebimento && form.getIdentificadortelaRecebimento() == null) {
			form.setIdentificadortelaRecebimento(request.getParameter("identificadortela"));
		}
		
		boolean recalculoImpostoNota = request.getAttribute("RECALCULO_IMPOSTO_NOTA") != null ? (Boolean) request.getAttribute("RECALCULO_IMPOSTO_NOTA") : Boolean.FALSE;
		
		if(form.getCdentregadocumento() == null && !request.getBindException().hasErrors() && form.getNaturezaoperacao() == null) {
			form.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
		}
		
		if(request.getSession().getAttribute("whereInDevolucoes") != null) {			
			request.getSession().setAttribute("whereInDevolucoes", request.getParameter("whereInDevolucoes"));
		}
		
		if(form.getCdentregadocumento() != null && form.getFornecedor() != null) {
			colaboradorFornecedorService.validaPermissao(form.getFornecedor());
		}
		
		if(form.getCdentregadocumento() != null && !request.getBindException().hasErrors()){
			this.carregaListaEntregamateriallote(form);
			form.setListaApropriacao(entregaDocumentoApropriacaoService.findByEntregaDocumento(form));
		}
		
		request.setAttribute("isCriar", form.getCdentregadocumento() == null);
		
		String fromPedidoColetaParam = request.getParameter("fromPedidoColeta");
		String whereInPedidovenda = request.getParameter("whereInPedidovenda");
		if(!recalculoImpostoNota && fromPedidoColetaParam != null && fromPedidoColetaParam.trim().toUpperCase().equals("TRUE") && StringUtils.isNotEmpty(whereInPedidovenda)){
			if(SinedUtil.isListEmpty(form.getListaEntregamaterial())){
				form.setListaEntregamaterial(getListaEntregamaterialByPedidovenda(whereInPedidovenda, null));
			}
			
			form.setFromPedidoColeta(Boolean.TRUE);
			form.setWhereInPedido(whereInPedidovenda);
			form.setWhereInColeta(request.getParameter("whereInColeta"));
			form.setProducaoautomatica("true".equals(request.getParameter("producaoautomatica")));
			form.setEmpresa(pedidovendaService.getEmpresa(pedidovendaService.findForEntradafiscal(whereInPedidovenda)));
		}
		
		String fromColetaParam = request.getParameter("fromColeta");
		String whereInColeta = request.getParameter("whereInColeta");
		if(!recalculoImpostoNota && fromColetaParam != null && fromColetaParam.trim().equalsIgnoreCase("TRUE") && whereInColeta != null){
			form.setFromColeta(Boolean.TRUE);
			form.setWhereInColeta(whereInColeta);
			form.setProducaoautomatica("true".equals(request.getParameter("producaoautomatica")));

			List<Coleta> listaColeta = coletaService.loadForGerarNotafiscalproduto(whereInColeta);
			if(SinedUtil.isListNotEmpty(listaColeta)){
				form.setEmpresa(coletaService.getEmpresa(listaColeta));
				
				whereInPedidovenda = SinedUtil.listAndConcatenate(listaColeta, "pedidovenda.cdpedidovenda", ",");
				if(whereInPedidovenda != null && !"".equals(whereInPedidovenda)){
					form.setFromPedidoColeta(Boolean.TRUE);
					form.setWhereInPedido(whereInPedidovenda);
					
					if(SinedUtil.isListEmpty(form.getListaEntregamaterial())){
						form.setListaEntregamaterial(getListaEntregamaterialByPedidovenda(whereInPedidovenda, whereInColeta));
					}
				}
			}
		}
		
		if(!Boolean.TRUE.equals(request.getAttribute("consultar")) && !Entregadocumentosituacao.CANCELADA.equals(form.getEntregadocumentosituacao())){
			List<Entregadocumentosituacao> listaSituacao = new ArrayList<Entregadocumentosituacao>();
			listaSituacao.add(Entregadocumentosituacao.REGISTRADA);
			listaSituacao.add(Entregadocumentosituacao.ALTERADA);
			listaSituacao.add(Entregadocumentosituacao.FATURADA);
			request.setAttribute("listaSituacao", listaSituacao);
		}
		
		request.setAttribute("ignoreHackAndroidDownload", Boolean.TRUE);
		request.setAttribute("tamanholista", form.getListaEntregamaterial() != null ? form.getListaEntregamaterial().size() : 0);
		if(form.getListaEntregamaterial() != null && !form.getListaEntregamaterial().isEmpty()) {
			for(Entregamaterial e1 : form.getListaEntregamaterial()) {
				if(form.getCdentregadocumento() != null) {
					e1.setMaterialclassetrans(e1.getMaterialclasse());
				}
				
				if(Util.objects.isPersistent(e1.getMaterial())) {
					Material material = materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome, material.identificacao, material.produto, material.epi, material.servico, material.patrimonio");
					if(material != null) {
						e1.getMaterial().setNome(material.getNome());
						e1.getMaterial().setIdentificacao(material.getIdentificacao());
						
						if(e1.getMaterialclassetrans() == null || e1.getMaterialclasse() == null) {
							Materialclasse materialclasse = null;
							
							if(material.getProduto() != null && material.getProduto()){
								materialclasse = Materialclasse.PRODUTO;
							} else if(material.getEpi() != null && material.getEpi()){
								materialclasse = Materialclasse.EPI;
							} else if(material.getPatrimonio() != null && material.getPatrimonio()){
								materialclasse = Materialclasse.PATRIMONIO;
							} else if(material.getServico() != null && material.getServico()){
								materialclasse = Materialclasse.SERVICO;
							}
							
							if(e1.getMaterialclasse() == null) e1.setMaterialclasse(materialclasse);
							if(e1.getMaterialclassetrans() == null) e1.setMaterialclassetrans(materialclasse);
						}
					}
				}
				if(Util.objects.isPersistent(e1.getUnidademedidacomercial())) {
					Unidademedida unidadeComercial = unidademedidaService.load(e1.getUnidademedidacomercial(), "unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo, unidademedida.ativo");
					if(unidadeComercial != null) {
						e1.setUnidademedidacomercial(unidadeComercial);
					}
				}
				
				Material materialForgrupo = e1.getMaterial() != null ? materialService.load(e1.getMaterial(), "material.materialgrupo") : null;
				e1.setHabilitaPesoMedio(materialForgrupo != null && materialForgrupo.getMaterialgrupo() != null && Boolean.TRUE.equals(materialForgrupo.getMaterialgrupo().getRegistrarpesomedio()));
				
				if(Util.objects.isPersistent(e1.getCfop())) {
					e1.setCfop(cfopService.carregarCfop(e1.getCfop()));				
				}
			}
		}
		
		if(form.getRateio() != null && form.getRateio().getCdrateio() != null){
			form.setRateio(rateioService.findRateio(form.getRateio()));
		}
		
		form.setListaImposto(criaListaImposto(form));
		
		criaAtributoEntregamaterialParaCalculoRateio(request, form);
		
		if(request.getAttribute("existAlteracaoRateio") != null && "true".equals(request.getAttribute("existAlteracaoRateio"))){
			request.setAttribute("existAlteracaoRateio", "true");
		}
		
		if(form.getCdentregadocumento() != null){
			form.setListaEntregadocumentohistorico(entregadocumentohistoricoService.getLista(entregadocumentohistoricoService.findByEntregadocumento(form)));
		}
		
		verificaExibicaoComprimentoItemMaterial(form);
		
		String idFornecimento = request.getParameter("idFornecimento");
		if(idFornecimento != null && !idFornecimento.equals("") && !"<null>".equals(idFornecimento)) {
			form.setIdFornecimento(idFornecimento);
			Fornecimento fornecimento = fornecimentoService.findForRegistrarEntradafiscal(new Fornecimento(Integer.parseInt(idFornecimento)));
			if(fornecimento != null ){
				if(fornecimento.getFornecimentocontrato() != null){
					form.setFornecedor(fornecimento.getFornecimentocontrato().getFornecedor());
				}
				if(fornecimento.getDtcontrato() != null){
					form.setDtemissao(fornecimento.getDtcontrato());
				}
			}
		}
		
		Boolean existdocumentoorigem = false;
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if(form.getFornecedor() != null && form.getFornecedor().getCdpessoa() != null 
				&& form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null) {
			listaDocumento = documentoService.findForAntecipacaoEntrega(form.getFornecedor(), form.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(form.getFornecedor(), form.getDocumentotipo());
		}
		if(form.getListadocumento() != null && !form.getListadocumento().isEmpty()) {
			for(Entregapagamento ep : form.getListadocumento()) {
				if(ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())) {
					if(Boolean.TRUE.equals(request.getAttribute("consultar")) || !listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())) {
						listaDocumento.add(ep.getDocumentoantecipacao());
					}
				}
				if(!existdocumentoorigem && ep.getDocumentoorigem() != null && ep.getDocumentoorigem().getCddocumento() != null){
					existdocumentoorigem = true;
				}
			}
		}	
		request.setAttribute("listaDocumento", listaDocumento);	
		
		if(form.getIdDocumentoRegistrarEntrada() != null && !"".equals(form.getIdDocumentoRegistrarEntrada())){
			request.setAttribute("fromRegistrarentradafiscal", true);
			existdocumentoorigem = true;
		}
		request.setAttribute("existDocumentoorigem", existdocumentoorigem);
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));	
		request.setAttribute("listaLocais", localarmazenagemService.findAtivos());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		if(form.getHaveOrdemcompra()) {
			calculaQtdeRestanteEntregadocumento(form, false);
		}
		
		addInfAtributo(request, form);

		if(fromRecebimento && StringUtils.isNotBlank(form.getIdentificadortelaRecebimento())){
			Entregamaterial entregamaterial = null;
			Object attr = request.getSession().getAttribute("listaEntregamaterial" + request.getParameter("identificadortela"));
			if(attr != null) {
				form.setListaEntregamaterial((Set<Entregamaterial>) attr);
			}
			addInfAtributo(request, form, entregamaterial);
		} 
		
		
		if(form.getCdsentregadocumento() != null && !form.getCdsentregadocumento().trim().equals("")) {
			adicionarVinculoCTRC(request, form);
		} else if (form.getCdentregadocumentofreteRemover() != null && !form.getCdentregadocumentofreteRemover().trim().equals("")) {
			removerVinculoCTRC(request, form);
		} else {
			if(form.getCdentregadocumento() != null) {
				form.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class, entregadocumentofreteService.findByEntregadocumento(form)));
				form.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class, entregadocumentofreterateioService.findByEntregadocumento(form)));
			}
		}
		
		processarMateriaisCTRC(request, form);
		
		if(form.getListaEntregadocumentoreferenciado() != null && !form.getListaEntregadocumentoreferenciado().isEmpty()){
			String whereInEdr = SinedUtil.listAndConcatenateIDs(form.getListaEntregadocumentoreferenciado());
			List<Documentoorigem> listaDocOrigem = documentoorigemService.getlistaDocumentosReferenciados(whereInEdr);
			if(listaDocOrigem != null && !listaDocOrigem.isEmpty()){
				Map<Integer, Documento> mapaDocumentos = new HashMap<Integer, Documento>();
				for(Documentoorigem docOrigem : listaDocOrigem) {
					mapaDocumentos.put(docOrigem.getEntregadocumentoreferenciado().getCdentregadocumentoreferenciado(), docOrigem.getDocumento());
				}
				for(Entregadocumentoreferenciado edr : form.getListaEntregadocumentoreferenciado()) {
					edr.setDocumento(mapaDocumentos.get(edr.getCdentregadocumentoreferenciado()));
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(form.getListaEntregamaterial())){
			for(Entregamaterial entregamaterial : form.getListaEntregamaterial()){
				entregamaterial.setUnidademedidacomercialAnterior(entregamaterial.getUnidademedidacomercial());
			}
		}
		
		if(form.getCdentregadocumento() == null){
			this.setDocumentoreferenciado(form);
		}
		
		Boolean recebimentoEntradaDataCorrente = parametrogeralService.getBoolean("RECEBIMENTO_ENTRADA_DATA_CORRENTE");
		Boolean disableDtentrada = recebimentoEntradaDataCorrente && (form.getCdentregadocumento() == null || form.getDtentrada() != null);
		request.setAttribute("disableDtentrada", disableDtentrada);
		if(form.getCdentregadocumento() == null && recebimentoEntradaDataCorrente){
			form.setDtentrada(SinedDateUtils.currentDate());
		}

		String cddocumentotipo = form.getCdentregadocumento() != null && form.getDocumentotipo() != null && form.getDocumentotipo().getCddocumentotipo() != null? form.getDocumentotipo().getCddocumentotipo().toString(): null;
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
		request.setAttribute("listaDocumentotipo", documentotipoService.findForCompra(cddocumentotipo));
		
		if(form.getListaEntregamaterial() != null) {
			request.getSession().setAttribute("listaEntregamaterial" + identificadortela, form.getListaEntregamaterial());
		}
	}
	
	@SuppressWarnings("unchecked")
	public Entregadocumento carregarEntregadocumentoFromRecebimento(WebRequestContext request) {
		String identificadortelaRecebimento = request.getParameter("identificadortela");
		String indexLista = request.getParameter("indexLista");
		
		Entregadocumento entregadocumento = new Entregadocumento();
		if(StringUtils.isNotBlank(identificadortelaRecebimento)) {
			if(StringUtils.isNotBlank(indexLista)) {
				Object attrListaEntregadocumento = request.getSession().getAttribute("listaEntregadocumento" + identificadortelaRecebimento);
				if(attrListaEntregadocumento != null && !" ".equals(attrListaEntregadocumento)) {
					Integer index = Integer.parseInt(indexLista);
					List<Entregadocumento> lista = (List<Entregadocumento>) attrListaEntregadocumento;					
					entregadocumento = lista.get(index);
					entregadocumento.setIndexlista(index);
				}
			}
			entregadocumento.setIdentificadortelaRecebimento(identificadortelaRecebimento);
		}
		
		if(entregadocumento.getIdentificadortela() == null) {
			String identificadortela = new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(System.currentTimeMillis());
			entregadocumento.setIdentificadortela(identificadortela);
		}
		
		if(entregadocumento != null) {
			if(entregadocumento.getListaEntregamaterial() != null && !entregadocumento.getListaEntregamaterial().isEmpty()) {
				for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
					if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
						e1.setMaterial(materialService.load(e1.getMaterial(), "material.cdmaterial, material.nome"));
					if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
						e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
				}
				
				request.getSession().setAttribute("listaEntregamaterial" + entregadocumento.getIdentificadortela(), entregadocumento.getListaEntregamaterial());
			}
		}
		entregadocumento.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.ENTRADA_FISCAL));
		entregadocumento.setFromRecebimento(true);
		
		Object indexlistaforrateio = request.getParameter("posicaoListaForRateio");
		if(indexlistaforrateio == null) {
			indexlistaforrateio = entregadocumento.getPosicaoListaParam();
		}
		if(indexlistaforrateio != null){
			entregadocumento.setIndexlistaforrateio(Integer.parseInt((String) indexlistaforrateio));
		}
		
		criaAtributoEntregamaterialParaCalculoRateio(request, entregadocumento);
		
		String cddocumentotipo = null; 
		if(entregadocumento.getCdentregadocumento() != null && entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null) {
			cddocumentotipo = entregadocumento.getDocumentotipo().getCddocumentotipo().toString();
		}
		
		request.setAttribute("listaDocumentotipo", documentotipoService.findForCompra(cddocumentotipo));
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if(entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null 
				&& entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null) {
			listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		}
		if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
			for (Entregapagamento ep : entregadocumento.getListadocumento()){
				if (ep.getDocumentoantecipacao() != null 
						&& !listaDocumento.contains(ep.getDocumentoantecipacao())
						&& !listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
					listaDocumento.add(ep.getDocumentoantecipacao());
				}
			}
		}
		
		request.setAttribute("listaDocumento", listaDocumento);
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		addInfAtributo(request, entregadocumento);
		
		if (entregadocumento.getCdsentregadocumento() != null && !entregadocumento.getCdsentregadocumento().trim().equals("")){
			adicionarVinculoCTRC(request, entregadocumento, entregadocumento.getCdsentregadocumento());
		} else if (entregadocumento.getCdentregadocumentofreteRemover() != null && !entregadocumento.getCdentregadocumentofreteRemover().trim().equals("")){
			removerVinculoCTRC(request, entregadocumento, entregadocumento.getCdentregadocumentofreteRemover());
		}
		
		processarMateriaisCTRC(request, entregadocumento);
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
		
		Boolean recebimentoEntradaDataCorrente = parametrogeralService.getBoolean("RECEBIMENTO_ENTRADA_DATA_CORRENTE");
		request.setAttribute("RECEBIMENTO_ENTRADA_DATA_CORRENTE", recebimentoEntradaDataCorrente);
		
		Boolean disableDtentrada = recebimentoEntradaDataCorrente && (entregadocumento.getCdentregadocumento() == null || entregadocumento.getDtentrada() != null);
		request.setAttribute("disableDtentrada", disableDtentrada);
		
		if(entregadocumento.getDtentrada() == null && entregadocumento.getCdentregadocumento() == null && recebimentoEntradaDataCorrente){
			entregadocumento.setDtentrada(SinedDateUtils.currentDate());
		}
		
		String cdentrega = request.getParameter("cdentrega");
		if(StringUtils.isNotBlank(cdentrega)) {
			entregadocumento.setEntrega(new Entrega(Integer.parseInt(cdentrega)));
		}
		String cdempresaentrega = request.getParameter("cdempresaentrega");
		if(StringUtils.isNotBlank(cdempresaentrega)) {
			entregadocumento.setCdempresaentrega(cdempresaentrega);
			if(entregadocumento.getEmpresa() == null) {
				entregadocumento.setEmpresa(new Empresa(Integer.parseInt(cdempresaentrega)));
			}
		}
		
		return entregadocumento;
	}
	
	@SuppressWarnings("unchecked")
	public void editarEntregadocumentoFromRecebimento(WebRequestContext request, Entregadocumento entregadocumento) {
		if(entregadocumento == null) {
			entregadocumento = new Entregadocumento();
		}
		
		String index = request.getParameter("indexLista");
		if(index == null) {
			index = entregadocumento.getIndexlista() != null ? entregadocumento.getIndexlista().toString() : null;
		}
		
		String cdentregastr = request.getParameter("cdentrega");
		if(StringUtils.isNotBlank(index)) {
			Object attr = request.getSession().getAttribute("listaEntregadocumento" + request.getParameter("identificadortela"));
			List<Entregadocumento> listaEntregadocumento = null;
			if(attr != null) {
				listaEntregadocumento = (List<Entregadocumento>) attr;
				
				entregadocumento = listaEntregadocumento.get(Integer.parseInt(index));
				entregadocumento.setIndexlista(Integer.parseInt(index));
				entregadocumento.setAcao("editar");
				entregadocumento.setIdentificadortela(request.getParameter("identificadortela"));
				
				if(entregadocumento.getCdentregadocumento() != null && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())) {
					for (Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
						e1.setMaterialclassetrans(e1.getMaterialclasse());
					}
				}
				
				if (SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())) {
					Loteestoque loteestoque;
					for(Entregamaterial e1 : entregadocumento.getListaEntregamaterial()) {
						e1.setUnidademedidacomercialAnterior(e1.getUnidademedidacomercial());
						
						if(e1.getMaterial() != null && e1.getMaterial().getCdmaterial() != null)
							e1.setMaterial(materialService.loadWithGrade(e1.getMaterial()));
						
						if(e1.getCfop() != null && e1.getCfop().getCdcfop() != null)
							e1.setCfop(cfopService.carregarCfop(e1.getCfop()));
						
						if(e1.getLoteestoque() != null){
							if(e1.getLoteestoque().getNumero() == null){
								loteestoque = loteestoqueService.load(e1.getLoteestoque(), "loteestoque.cdloteestoque, loteestoque.numero");
								if(loteestoque != null) e1.getLoteestoque().setNumero(loteestoque.getNumero());
							}
						} else if(SinedUtil.isListNotEmpty(e1.getListaEntregamateriallote())){
							for(Entregamateriallote entregamateriallote : e1.getListaEntregamateriallote()){
								if(entregamateriallote.getLoteestoque() != null){
									if(entregamateriallote.getLoteestoque().getNumero() == null){
										loteestoque = loteestoqueService.load(entregamateriallote.getLoteestoque(), "loteestoque.cdloteestoque, loteestoque.numero");
										if(loteestoque != null) entregamateriallote.getLoteestoque().setNumero(loteestoque.getNumero());
									}
								}
							}
						}
						
						e1.setHabilitaPesoMedio(e1.getMaterial() != null 
								&& e1.getMaterial().getMaterialgrupo() != null 
								&& Boolean.TRUE.equals(e1.getMaterial().getMaterialgrupo().getRegistrarpesomedio()));
					}
				}
				
				criaAtributoEntregamaterialParaCalculoRateio(request, entregadocumento);
				calculaQtdeRestanteEntregadocumento(entregadocumento, false);
			}
		}
		
		if(SinedUtil.validaNumeros(cdentregastr)) {
			request.setAttribute("cdentrega", cdentregastr);
			if(entregadocumento != null) {
				entregadocumento.setCdentrega(cdentregastr);
			}
		}
		
		//Cria a lista de documentos antecipados utilizados nos pagamentos
		List<Documento> listaDocumento = new ArrayList<Documento>();
		List<Documento> listaDocumentoUtilizados = new ArrayList<Documento>();
		if (entregadocumento.getFornecedor() != null && entregadocumento.getFornecedor().getCdpessoa() != null && 
				entregadocumento.getDocumentotipo() != null && entregadocumento.getDocumentotipo().getCddocumentotipo() != null){
			listaDocumento = documentoService.findForAntecipacaoEntrega(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
			listaDocumentoUtilizados = documentoService.findForAntecipacaoConsideraEntregaUtilizados(entregadocumento.getFornecedor(), entregadocumento.getDocumentotipo());
		}
		if(entregadocumento.getListadocumento() != null && !entregadocumento.getListadocumento().isEmpty()){
			for (Entregapagamento ep : entregadocumento.getListadocumento()){
				if (ep.getDocumentoantecipacao() != null && !listaDocumento.contains(ep.getDocumentoantecipacao())){
					if(!listaDocumentoUtilizados.contains(ep.getDocumentoantecipacao())){
						listaDocumento.add(ep.getDocumentoantecipacao());
					}
				}
			}
		}
		
		addInfAtributo(request, entregadocumento);
		request.setAttribute("listaDocumento", listaDocumento);
		request.setAttribute("listaEmpresa", empresaService.findAtivos(entregadocumento.getEmpresa()));
//		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaResultadoCompensacaoOutras());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		
		if (entregadocumento != null){
			if (entregadocumento.getCdsentregadocumento()!=null && !entregadocumento.getCdsentregadocumento().trim().equals("")) {
				entregadocumento.setModelodocumentofiscal(entregadocumento.getModelodocumentofiscal());
				adicionarVinculoCTRC(request, entregadocumento, entregadocumento.getCdsentregadocumento());
			} else if (entregadocumento.getCdentregadocumentofreteRemover()!=null && !entregadocumento.getCdentregadocumentofreteRemover().trim().equals("")) {
				entregadocumento.setModelodocumentofiscal(entregadocumento.getModelodocumentofiscal());
				removerVinculoCTRC(request, entregadocumento, entregadocumento.getCdentregadocumentofreteRemover());
			} else {
				if (entregadocumento.getCdentregadocumento()!=null) {
					entregadocumento.setListaEntregadocumentofrete(new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class, entregadocumentofreteService.findByEntregadocumento(entregadocumento)));
					entregadocumento.setListaEntregadocumentofreterateio(new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class, entregadocumentofreterateioService.findByEntregadocumento(entregadocumento)));
				}
			}
			processarMateriaisCTRC(request, entregadocumento);
		}
		
		setDocumentoreferenciado(entregadocumento);
		request.setAttribute("VALOR_NOTA_PARCELADIVERGENTE", "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.VALOR_NOTA_PARCELADIVERGENTE)));
	}
	
	private void adicionarVinculoCTRC(WebRequestContext request, Entregadocumento form){
		adicionarVinculoCTRC(request, form, null);
	}
	
	private void adicionarVinculoCTRC(WebRequestContext request, Entregadocumento form, String cdsentregadocumento){
		String whereNotInCdentregadocumento = null; 
		if (form.getListaEntregadocumentofrete()!=null && !form.getListaEntregadocumentofrete().isEmpty()){
			whereNotInCdentregadocumento = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofrete(), "entregadocumentovinculo.cdentregadocumento", ",");
		}
		
		List<Entregadocumento> listaEntregadocumento = findForCTRC(cdsentregadocumento, whereNotInCdentregadocumento);
		if(cdsentregadocumento != null) {
			listaEntregadocumento = findForCTRC(cdsentregadocumento, whereNotInCdentregadocumento);
		} else {
			listaEntregadocumento = findForCTRC(form.getCdsentregadocumento(), whereNotInCdentregadocumento);
		}
		
		if (!listaEntregadocumento.isEmpty()){
			//Vinculo
			Set<Entregadocumentofrete> listaEntregadocumentofreteAdd = new ListSet<Entregadocumentofrete>(Entregadocumentofrete.class);
			for (Entregadocumento entregadocumentovinculo: listaEntregadocumento){
				listaEntregadocumentofreteAdd.add(new Entregadocumentofrete(entregadocumentovinculo));
			}
			if (form.getListaEntregadocumentofrete()==null){
				form.setListaEntregadocumentofrete(listaEntregadocumentofreteAdd);
			}else {
				form.getListaEntregadocumentofrete().addAll(listaEntregadocumentofreteAdd);
			}
			
			//Materiais
			String whereInCdentregadocumento = CollectionsUtil.listAndConcatenate(listaEntregadocumento, "cdentregadocumento", ",");
			String whereNotInCdentregamaterial = null; 
			if (form.getListaEntregadocumentofreterateio()!=null && !form.getListaEntregadocumentofreterateio().isEmpty()){
				whereNotInCdentregamaterial = CollectionsUtil.listAndConcatenate(form.getListaEntregadocumentofreterateio(), "entregamaterial.cdentregamaterial", ",");
			}
			List<Entregamaterial> listaEntregamaterial = entregamaterialService.findForCTRC(whereInCdentregadocumento, whereNotInCdentregamaterial);
			if (!listaEntregamaterial.isEmpty()){
				Set<Entregadocumentofreterateio> listaEntregadocumentofreterateioAdd = new ListSet<Entregadocumentofreterateio>(Entregadocumentofreterateio.class);
				for (Entregamaterial entregamaterial: listaEntregamaterial){
					listaEntregadocumentofreterateioAdd.add(new Entregadocumentofreterateio(entregamaterial));
				}
				if (form.getListaEntregadocumentofreterateio()==null){
					form.setListaEntregadocumentofreterateio(listaEntregadocumentofreterateioAdd);
				}else {
					form.getListaEntregadocumentofreterateio().addAll(listaEntregadocumentofreterateioAdd);
				}
			}
		}
	}
	
	private void removerVinculoCTRC(WebRequestContext request, Entregadocumento form) {
		removerVinculoCTRC(request, form, null);
	}
	
	private void removerVinculoCTRC(WebRequestContext request, Entregadocumento form, String cdentregadocumentofreteRemover){
		if(form.getListaEntregadocumentofrete() != null && !form.getListaEntregadocumentofrete().isEmpty()){
			for(Iterator<Entregadocumentofrete> iterator1 = form.getListaEntregadocumentofrete().iterator(); iterator1.hasNext();) {
				Entregadocumentofrete entregadocumentofrete = (Entregadocumentofrete) iterator1.next();
				
				Boolean achou = false;
				if(cdentregadocumentofreteRemover != null) {
					achou = cdentregadocumentofreteRemover.equals(entregadocumentofrete.getCdentregadocumentofrete().toString());
				} else {
					achou = form.getCdentregadocumentofreteRemover().equals(entregadocumentofrete.getEntregadocumentovinculo().getCdentregadocumento().toString());
				}
				
				if(achou) {
					if(form.getListaEntregadocumentofreterateio() != null) {
						for(Iterator<Entregadocumentofreterateio> iterator2 = form.getListaEntregadocumentofreterateio().iterator(); iterator2.hasNext();) {
							Entregadocumentofreterateio entregadocumentofreterateio = (Entregadocumentofreterateio) iterator2.next();
							
							if(entregadocumentofreterateio.getEntregamaterial().getEntregadocumento().getCdentregadocumento().equals(entregadocumentofrete.getEntregadocumentovinculo().getCdentregadocumento())) {
								iterator2.remove();
							}
						}
					}
					iterator1.remove();
				}
			}
		}
	}
	
	private void processarMateriaisCTRC(WebRequestContext request, Entregadocumento form){
		Set<Entregadocumentofreterateio> listaEntregadocumentofreterateio = form.getListaEntregadocumentofreterateio();
		Money valorTotalDocumentoCTRC = form.getValor()==null ?  new Money(0) : form.getValor();
		Money valorTotalRateioCTRC = new Money(0);
		
		if (listaEntregadocumentofreterateio!=null && !listaEntregadocumentofreterateio.isEmpty()){
			for (Entregadocumentofreterateio entregadocumentofreterateio: listaEntregadocumentofreterateio){
				if (entregadocumentofreterateio.getEntregamaterial().getQtde()==null){
					entregadocumentofreterateio.getEntregamaterial().setQtde(0D);
				}
				if (entregadocumentofreterateio.getEntregamaterial().getValorunitario()==null){
					entregadocumentofreterateio.getEntregamaterial().setValorunitario(0D);
				}
				Double qtde = entregadocumentofreterateio.getEntregamaterial().getQtde();
				Double valorunitario = entregadocumentofreterateio.getEntregamaterial().getValorunitario();
				double valortotal = valorunitario * qtde;
				entregadocumentofreterateio.getEntregamaterial().setValortotal(valortotal);
				
				if (entregadocumentofreterateio.getValor()!=null){
					valorTotalRateioCTRC = valorTotalRateioCTRC.add(entregadocumentofreterateio.getValor());
				}
			}
		}
		
		form.setValorTotalDocumentoCTRC(valorTotalDocumentoCTRC);
		form.setValorTotalRateioCTRC(valorTotalRateioCTRC);
		form.setValorRestanteCTRC(valorTotalDocumentoCTRC.subtract(valorTotalRateioCTRC));
	}
	
	public void preencheLotesW3(List<Rastro> listaRastro, Material material, Fornecedor fornecedor){
		if(Util.objects.isNotPersistent(fornecedor) || SinedUtil.isListEmpty(listaRastro) || Util.objects.isNotPersistent(material) || !materialService.obrigarLote(material)){
			return;
		}
		for(Rastro rastro: listaRastro){
			Loteestoque loteestoque = loteestoqueService.findByNumeroAndFornecedorForImportacaoXml(rastro.getnLote(), fornecedor);
			if(loteestoque != null){
				boolean exists = false;
				for(Lotematerial lm: loteestoque.getListaLotematerial()){
					if(lm.getMaterial().equals(material)){
						exists = true;
					}
				}
				if(!exists){
					Lotematerial lotematerial = new Lotematerial();
					lotematerial.setMaterial(material);
					lotematerial.setLoteestoque(loteestoque);
					if(rastro.getdVal() != null)
						lotematerial.setValidade(new Date(rastro.getdVal().getTime()));
					if(rastro.getdFab() != null)
						lotematerial.setFabricacao(new Date(rastro.getdFab().getTime()));
					lotematerial.setCodigoAgregacao(rastro.getcAgreg());
					LotematerialService.getInstance().saveOrUpdate(lotematerial);
				}
			}else{
				loteestoque = new Loteestoque();
				loteestoque.setNumero(rastro.getnLote());
				loteestoque.setFornecedor(fornecedor);
				loteestoque.setNumero(rastro.getnLote());
				
				Lotematerial lotematerial = new Lotematerial();
				lotematerial.setMaterial(material);
				lotematerial.setLoteestoque(loteestoque);
				if(rastro.getdVal() != null)
					lotematerial.setValidade(new Date(rastro.getdVal().getTime()));
				if(rastro.getdFab() != null)
					lotematerial.setFabricacao(new Date(rastro.getdFab().getTime()));
				lotematerial.setCodigoAgregacao(rastro.getcAgreg());
				loteestoque.setNumero(rastro.getnLote());
				loteestoque.setListaLotematerial(new ArrayList<Lotematerial>());
				loteestoque.getListaLotematerial().add(lotematerial);
				loteestoqueService.saveOrUpdate(loteestoque);
			}
		}
		List<Loteestoque> listaLoteestoque = loteestoqueService.findByMaterialAndFornecedor(material, fornecedor);
		for(Rastro rastro: listaRastro){
			for(Loteestoque le: listaLoteestoque){
				Date validade = SinedUtil.isListNotEmpty(le.getListaLotematerial())? le.getListaLotematerial().get(0).getValidade(): null;
				if(rastro.getNumeroLote().equalsIgnoreCase(le.getNumeroLoteSoNumeroAndLetra())){
					if(validade == null)
						throw new SinedException("A data de validade do(s) lote(s) do XML � diferente da data de validade do(s) registro(s) de lote(s).");
					Date validadeXml = new Date(rastro.getdVal().getTime());
					if(rastro.getdVal() != null && validade.compareTo(validadeXml) != 0){
						throw new SinedException("A data de validade do(s) lote(s) do XML � diferente da data de validade do(s) registro(s) de lote(s).");
					}
				}
			}
			
			rastro.getListaLotesW3().addAll(listaLoteestoque);
		}
	}
	
	public void preencheFornecedorFromImportacaoXml(Entregadocumento entregadocumento, ImportacaoXmlNfeBean bean){
		if (bean.getConferenciaDadosFornecedorBean().getCnpj_bd() != null){
			entregadocumento.setFornecedor(fornecedorService.getFornecedorByCpfCnpj(bean.getConferenciaDadosFornecedorBean().getCnpj_bd(), null));
		} else if (bean.getConferenciaDadosFornecedorBean().getCpf_bd() != null){
			entregadocumento.setFornecedor(fornecedorService.getFornecedorByCpfCnpj(null, bean.getConferenciaDadosFornecedorBean().getCpf_bd()));
		}
		
		Fornecedor fornecedorDaTela = Util.objects.isPersistent(entregadocumento.getFornecedor())? entregadocumento.getFornecedor(): fornecedorService.loadFornecedorForImportacaoXml(bean.getFornecedor());
		entregadocumento.setFornecedor(fornecedorDaTela);
	}
}