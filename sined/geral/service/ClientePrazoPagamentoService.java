package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClientePrazoPagamento;
import br.com.linkcom.sined.geral.dao.ClientePrazoPagamentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.clienteprazopagamento.ClienteprazopagamentoRESTModel;

public class ClientePrazoPagamentoService extends GenericService<ClientePrazoPagamento> {

	private ClientePrazoPagamentoDAO clientePrazoPagamentoDAO;
	public void setClientePrazoPagamentoDAO( ClientePrazoPagamentoDAO clientePrazoPagamentoDAO) { this.clientePrazoPagamentoDAO = clientePrazoPagamentoDAO;	}
	
	/* singleton */
	private static ClientePrazoPagamentoService instance;
	public static ClientePrazoPagamentoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ClientePrazoPagamentoService.class);
		}
		return instance;
	}
	
	/**
	 * Lista todos os Prazo Pagamento para um determinado Cliente.
	 * 
	 * @author Marcos Lisboa
	 */
	public List<ClientePrazoPagamento> findByCliente(Cliente cliente){
		return clientePrazoPagamentoDAO.findByCliente(cliente);
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.ClientePrazoPagamentoService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClienteprazopagamentoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ClientePrazoPagamentoDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<ClienteprazopagamentoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ClienteprazopagamentoRESTModel> lista = new ArrayList<ClienteprazopagamentoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(ClientePrazoPagamento bean : clientePrazoPagamentoDAO.findForAndroid(whereIn))
				lista.add(new ClienteprazopagamentoRESTModel(bean));
		}
		
		return lista;
	}
}
