package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradordeficiencia;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradordeficienciaService extends GenericService<Colaboradordeficiencia> {	
	
	/* singleton */
	private static ColaboradordeficienciaService instance;
	public static ColaboradordeficienciaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradordeficienciaService.class);
		}
		return instance;
	}
	
}
