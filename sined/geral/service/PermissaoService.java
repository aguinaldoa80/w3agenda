package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Permissao;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.dao.PermissaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PermissaoService extends GenericService<Permissao> {

	private PermissaoDAO permissaoDAO;
	
	public void setPermissaoDAO(PermissaoDAO permissaoDAO) {
		this.permissaoDAO = permissaoDAO;
	}
	
	/**
	 * Verifica se o papel possui permiss�o ao m�dulo.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PermissaoDAO#findPermissaoModulo(Papel, String)
	 * 
	 * @author JO�o Paulo Zica
	 * 		   Pedro Gon�alves
	 * @param listUsuariopapel
	 * @param path
	 * @return Lista com as permiss�es encontradas
	 */
	public Long findPermissaoModulo(List<Usuariopapel> listUsuariopapel, String path) {
		return permissaoDAO.findPermissaoModulo(listUsuariopapel, path);
	}

	/* singleton */
	private static PermissaoService instance;
	public static PermissaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PermissaoService.class);
		}
		return instance;
	}
}
