package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Periodo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.dao.PeriodoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PeriodoService extends GenericService<Periodo> {

	private PeriodoDAO periodoDAO;
	
	public void setPeriodoDAO(PeriodoDAO periodoDAO) {
		this.periodoDAO = periodoDAO;
	}
	
	public void deleteWherePlanejamento(Planejamento planejamento) {
		periodoDAO.deleteWherePlanejamento(planejamento);	
	}

	public List<Periodo> findByPlanejamento(Planejamento planejamento) {
		return periodoDAO.findByPlanejamento(planejamento);
	}

}
