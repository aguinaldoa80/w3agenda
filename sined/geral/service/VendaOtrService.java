package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.VendaOtrDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaOtrService extends GenericService<Venda> {

	private VendaOtrDAO vendaOtrDAO;
	private VendaService vendaService;
	
	public void setVendaOtrDAO(VendaOtrDAO vendaOtrDAO) {
		this.vendaOtrDAO = vendaOtrDAO;
	}
	
	
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	
	@Override
	public ListagemResult<Venda> findForListagem(FiltroListagem filtro) {
		return vendaService.findForListagem(filtro);
	}
	
	@Override
	public Venda loadForEntrada(Venda bean) {
		return vendaService.loadForEntrada(bean);
	}

}