package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.dao.TabelavalorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.tabelavalor.TabelavalorRESTModel;

public class TabelavalorService extends GenericService<Tabelavalor> {

	private TabelavalorDAO tabelavalorDAO;
	
	public void setTabelavalorDAO(TabelavalorDAO tabelavalorDAO) {
		this.tabelavalorDAO = tabelavalorDAO;
	}

	/* singleton */
	private static TabelavalorService instance;
	public static TabelavalorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TabelavalorService.class);
		}
		return instance;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TabelavalorDAO#findForCalcularValorvenda(String whereInIdentificador)
	*
	* @param whereInIdentificador
	* @return
	* @since 23/06/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalor> findForCalcularValorvenda(String whereInIdentificador) {
		return tabelavalorDAO.findForCalcularValorvenda(whereInIdentificador);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TabelavalorDAO#findByIdentificador(String whereInIdentificador)
	*
	* @param whereInIdentificador
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public List<Tabelavalor> findByIdentificador(String whereInIdentificador) {
		return tabelavalorDAO.findByIdentificador(whereInIdentificador);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.TabelavalorDAO#updateValorPorIdentificador(String identificador, Double valor)
	*
	* @param identificador
	* @param valor
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public void updateValorPorIdentificador(String identificador, Double valor) {
		tabelavalorDAO.updateValorPorIdentificador(identificador, valor);		
	}

	/**
	* M�todo que retorna o valor da tabela de valor de acordo com o identificador
	*
	* @param identificador
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public Double getValor(String identificador) {
		return getValor(identificador, findByIdentificador(identificador));
	}
	
	/**
	* M�todo que retorna o valor da tabela de valor de acordo com o identificador
	*
	* @param identificador
	* @param listaTabelavalor
	* @return
	* @since 23/09/2015
	* @author Luiz Fernando
	*/
	public Double getValor(String identificador, List<Tabelavalor> listaTabelavalor) {
		if(SinedUtil.isListNotEmpty(listaTabelavalor)){
			for(Tabelavalor tabelavalor : listaTabelavalor){
				if(tabelavalor.getIdentificador() != null){
					if(tabelavalor.getIdentificador().equals(identificador)){
						return tabelavalor.getValor();
					}
				}
			}
		}
		return 0d;
	}
	
	public List<TabelavalorRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<TabelavalorRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<TabelavalorRESTModel> lista = new ArrayList<TabelavalorRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Tabelavalor bean : tabelavalorDAO.findForAndroid(whereIn))
				lista.add(new TabelavalorRESTModel(bean));
		}
		
		return lista;
	}
}
