package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;

import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Comissionamentofaixadesconto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ComissionamentofaixadescontoService extends GenericService<Comissionamentofaixadesconto>{

	public Double getPercentualComissao(Comissionamento comissionamento, Double percentualDescontoVenda, Double preco, Double precoTabela) {
		if(comissionamento != null && Boolean.TRUE.equals(comissionamento.getConsiderardiferencapreco()) && preco != null && precoTabela != null && preco < precoTabela && precoTabela > 0){
			Double percentualDiferenca = (precoTabela - preco) / precoTabela * 100d;
			if(percentualDiferenca > 0){
				percentualDescontoVenda = percentualDescontoVenda != null ? percentualDescontoVenda + percentualDiferenca : percentualDiferenca;
			}
		}
		
		if(comissionamento != null && percentualDescontoVenda != null && SinedUtil.isListNotEmpty(comissionamento.getListaComissionamentofaixadesconto())){
			
			//ordem deve ser por maior valor
			Collections.sort(comissionamento.getListaComissionamentofaixadesconto(),new Comparator<Comissionamentofaixadesconto>(){
				public int compare(Comissionamentofaixadesconto o1, Comissionamentofaixadesconto o2) {
					try {
						return o1.getPercentualdescontoate().compareTo(o2.getPercentualdescontoate());
					}catch (Exception e) {return 0;}
				}
			});
			
			for(Comissionamentofaixadesconto item : comissionamento.getListaComissionamentofaixadesconto()){
				if(item.getPercentualdescontoate() != null && percentualDescontoVenda <= item.getPercentualdescontoate()){
					return item.getPercentualcomissao();
				}
			}
		}
		return null;
	}

}
