package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.NestableException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoTipo;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentodespesa;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Planejamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Planejamentosituacao;
import br.com.linkcom.sined.geral.bean.Producaodiaria;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetoarquivo;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.bean.Projetohistorico;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.ApontamentoDAO;
import br.com.linkcom.sined.geral.dao.DocumentoDAO;
import br.com.linkcom.sined.geral.dao.FornecimentocontratoDAO;
import br.com.linkcom.sined.geral.dao.OrdemcompraDAO;
import br.com.linkcom.sined.geral.dao.ProjetoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.CopiaProjeto;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ApontamentoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ProjetoFiltro;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.AcompanhamentoEconomicoListagemDetalheBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.HSSFRegionUtil;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.ProjetoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.projeto.ProjetoRESTModel;

public class ProjetoService extends GenericService<Projeto> {

	private ProjetoDAO projetoDAO;
	private PlanejamentoService planejamentoService;
	private ProjetoService projetoService;
	private TarefaService tarefaService;
	private PropostaService propostaService;
	private ProducaodiariaService producaodiariaService;
	private ApontamentoDAO apontamentoDAO;
	private DocumentoDAO documentoDAO;
	private OrdemcompraDAO ordemcompraDAO;
	private FornecimentocontratoDAO fornecimentocontratoDAO;
	private ContapagarService contapagarService;
	private OrcamentorecursohumanoService orcamentorecursohumanoService;
	private ProjetodespesaService projetodespesaService;
	
	public void setProjetodespesaService(
			ProjetodespesaService projetodespesaService) {
		this.projetodespesaService = projetodespesaService;
	}
	public void setOrcamentorecursohumanoService(
			OrcamentorecursohumanoService orcamentorecursohumanoService) {
		this.orcamentorecursohumanoService = orcamentorecursohumanoService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setProjetoDAO(ProjetoDAO projetoDAO) {
		this.projetoDAO = projetoDAO;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setTarefaService(TarefaService tarefaService) {
		this.tarefaService = tarefaService;
	}
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setProducaodiariaService(ProducaodiariaService producaodiariaService) {
		this.producaodiariaService = producaodiariaService;
	}
	public void setApontamentoDAO(ApontamentoDAO apontamentoDAO) {
		this.apontamentoDAO = apontamentoDAO;
	}
	public void setDocumentoDAO(DocumentoDAO documentoDAO) {
		this.documentoDAO = documentoDAO;
	}
	public void setOrdemcompraDAO(OrdemcompraDAO ordemcompraDAO) {
		this.ordemcompraDAO = ordemcompraDAO;
	}
	public void setFornecimentocontratoDAO(FornecimentocontratoDAO fornecimentocontratoDAO) {
		this.fornecimentocontratoDAO = fornecimentocontratoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * Atualiza a situa��o do projeto.
	 * 
	 * Obs: este m�todo ser� executado uma vez por dia
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#atualizaSituacaoProjeto()
	 *
	 * @author Luiz Fernando
	 */
	public void atualizaSituacaoProjeto() {
		projetoDAO.atualizaSituacaoProjeto();
	}
	
	/**
	 * Renorna uma lista de projeto contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Projeto> findDescricao(String whereIn) {
		return projetoDAO.findDescricao(whereIn);
	}
	
	/**
	 * Retorna os nomes dos Projetos, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 *  
	 * @see #findDescricao(String)
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeProjetos(List<ItemDetalhe> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(lista, "projeto.cdprojeto", ",");
		stCds = SinedUtil.removeValoresDuplicados(stCds);
		
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/**
	 * Retorna os itens de op��o que s�o mostrados nos filtros de relat�rio do Financeiro.
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<GenericBean> getListaProjetoFiltro() {
		List<GenericBean> listaItemProjeto = new ArrayList<GenericBean>();
		
		listaItemProjeto.add(new GenericBean("todosProjetos","Todos"));
		listaItemProjeto.add(new GenericBean("escolherProjeto","Escolher"));
		listaItemProjeto.add(new GenericBean("comProjeto","Apenas com projeto"));
		listaItemProjeto.add(new GenericBean("semProjeto","Apenas sem projeto"));
		
		return listaItemProjeto;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> findAllForFlex(){
		return projetoDAO.findAllForFlex();
	}
	
	/**
	 * M�todo para salvar uma c�pia de projeto.
	 * Prepara os beans e salva em uma transaction.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#loadForCopiaProjeto(Planejamento)
	 * @see #loadForEntrada(Projeto)
	 * @see #saveOrUpdateNoUseTransaction(Projeto)
	 * @see br.com.linkcom.sined.geral.service.PlanejamentoService#saveOrUpdateNoUseTransaction(Planejamento)
	 * @see br.com.linkcom.sined.geral.service.TarefaService#saveOrUpdateListNoUseTransaction(Collection)
	 * @param copiaProjeto
	 * @author Fl�vio Tavares
	 */
	public void saveCopiaProjeto(final CopiaProjeto copiaProjeto){
		
		final Planejamento planejamento = planejamentoService.loadForCopiaProjeto(copiaProjeto.getPlanejamento());
		planejamento.setCdplanejamento(null);
		planejamento.setListaPlanejamentodespesa(null);
		
		String nome = copiaProjeto.getNome();
		if(BooleanUtils.isTrue(copiaProjeto.getCopiarPlanejamentoNovoProjeto())){
			Projeto projeto = this.loadForEntrada(copiaProjeto.getProjeto());
			projeto.setNome(nome);
			projeto.setCdprojeto(null);
			projeto.setListaProjetohistorico(null);
			projeto.setListaProjetoarquivo(null);
			
			planejamento.setProjeto(projeto);
		}else{
			planejamento.setDescricao(nome);
		}
				
		// Copiar recursos gerais
		if(BooleanUtils.isTrue(copiaProjeto.getCopiarRecursosGerais())){
			Collection<Planejamentorecursogeral> listaPlanejamentorecursogeral = planejamento.getListaPlanejamentorecursogeral();
			SinedUtil.setListPropertyValue(listaPlanejamentorecursogeral, "cdplanejamentorecursogeral", null);
		}else planejamento.setListaPlanejamentorecursogeral(null);
		
		// Copiar recursos humanos
		if(BooleanUtils.isTrue(copiaProjeto.getCopiarRecursosHumanos())){
			Collection<Planejamentorecursohumano> listaPlanejamentorecursohumano = planejamento.getListaPlanejamentorecursohumano();
			SinedUtil.setListPropertyValue(listaPlanejamentorecursohumano, "cdplanejamentorecursohumano", null);
		}else planejamento.setListaPlanejamentorecursohumano(null);
		
		// Copiar apontamentos
		if(BooleanUtils.isTrue(copiaProjeto.getApontamentos())){
			Collection<Apontamento> listaApontamento = planejamento.getListaApontamento();
			SinedUtil.setListPropertyValue(listaApontamento, "cdapontamento", null);
		}else planejamento.setListaApontamento(null);
		
		// Copiar tarefas
		if(BooleanUtils.isTrue(copiaProjeto.getTarefas())){
			Collection<Tarefa> listaTarefa = planejamento.getListaTarefa();
			if(SinedUtil.isListNotEmpty(listaTarefa)){
				
				for (Tarefa tarefa : listaTarefa) {
					tarefa.setCdtarefa(null);
					
					// Copiar atividades
					if(BooleanUtils.isTrue(copiaProjeto.getAtividades())){
						Collection<Atividade> listaAtividade = tarefa.getListaAtividade();
						SinedUtil.setListPropertyValue(listaAtividade, "cdatividade", null);					
					}else tarefa.setListaAtividade(null);
					
					// Copiar medi��es
					if(BooleanUtils.isTrue(copiaProjeto.getMedicoes())){
						Collection<Producaodiaria> listaProducaodiaria = tarefa.getListaProducaodiaria();
						SinedUtil.setListPropertyValue(listaProducaodiaria, "cdproducaodiaria", null);
					}else tarefa.setListaProducaodiaria(null);
					
					// Ajuste para copiar a listaTarefarecursogeral
					Collection<Tarefarecursogeral> listaTarefarecursogeral = tarefa.getListaTarefarecursogeral();
					SinedUtil.setListPropertyValue(listaTarefarecursogeral, "cdtarefarecursogeral", null);
					
					// Ajuste para copiar a listaTarefarecursohumano
					Collection<Tarefarecursohumano> listaTarefarecursohumano = tarefa.getListaTarefarecursohumano();
					SinedUtil.setListPropertyValue(listaTarefarecursohumano, "cdtarefarecursohumano", null);
				}
			}
		}else planejamento.setListaTarefa(null);
		
		// Ajuste de planejamentodespesa para c�pia
		Set<Planejamentodespesa> listaPlanejamentodespesa = planejamento.getListaPlanejamentodespesa();
		SinedUtil.setListPropertyValue(listaPlanejamentodespesa, "cdplanejamentodespesa", null);
		
		try {
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					if(BooleanUtils.isTrue(copiaProjeto.getCopiarPlanejamentoNovoProjeto())){
						projetoService.saveOrUpdateNoUseTransaction(planejamento.getProjeto());
					}
					planejamentoService.saveOrUpdateNoUseTransaction(planejamento);
					tarefaService.saveOrUpdateListNoUseTransaction(planejamento.getListaTarefa());
					
					return null;
				}
			});
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_projeto_nome"))
				throw new SinedException("Projeto j� cadastrado no sistema.");
			else if (DatabaseError.isKeyPresent(e, "projeto_cnpj_key"))
				throw new SinedException("Cnpj j� cadastrado.");
			else if (DatabaseError.isKeyPresent(e, "projeto_cei_key"))
				throw new SinedException("Cei j� cadastrado.");
			else throw e;
		}
	}
	
	/* singleton */
	private static ProjetoService instance;
	public static ProjetoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetoService.class);
		}
		return instance;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findByPlanejamento 
	 * 
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Projeto findByPlanejamento(Planejamento planejamento) {
		return projetoDAO.findByPlanejamento(planejamento);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findCombo
	 * 
	 * @param projetoAberto
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> findCombo(Boolean projetoAberto){
		return projetoDAO.findCombo(projetoAberto);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findProjetosAbertos
	 *  
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertos(List<Projeto> projetos) {
		return projetoDAO.findProjetosAbertos(projetos, false, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findProjetosAbertos
	 *  
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertos(List<Projeto> projetos, boolean retirarCancelados, Boolean permissaoProjeto) {
		return projetoDAO.findProjetosAbertos(projetos, retirarCancelados, permissaoProjeto);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findProjetosAbertosSemPermissao
	 *  
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertosSemPermissao(List<Projeto> projetos) {
		return projetoDAO.findProjetosAbertosSemPermissao(projetos, false, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *  
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findProjetosAbertosSemPermissao
	 *  
	 * @param projetos
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Projeto> findProjetosAbertosSemPermissao(List<Projeto> projetos, boolean retirarCancelados, Boolean permissaoProjeto) {
		return projetoDAO.findProjetosAbertosSemPermissao(projetos, retirarCancelados, permissaoProjeto);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findByContrato 
	 * 
	 * @param contrato
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Projeto> findByContrato(Contrato contrato) {
		return projetoDAO.findByContrato(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findByContrato(Contrato contrato, Integer cdtarefa)
	 *
	 * @param contrato
	 * @param cdtarefa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findByContrato(Contrato contrato, Integer cdstarefa) {
		return this.findByContrato(contrato, (cdstarefa!=null ? cdstarefa.toString() : null));
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findByContrato(Contrato contrato, Integer cdtarefa)
	 *
	 * @param contrato
	 * @param cdtarefa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findByContrato(Contrato contrato, String cdstarefa) {
		return projetoDAO.findByContrato(contrato, cdstarefa, null);
	}
	
	public List<Projeto> findByContrato(Contrato contrato, String cdstarefa, Integer cdrequisicao) {
		return projetoDAO.findByContrato(contrato, cdstarefa, cdrequisicao);
	}

	public List<Projeto> findByContratoInteracao(Contrato contrato) {
		if(contrato != null && contrato.getCdcontrato() != null){
			return this.findByContrato(contrato);
		}
		return projetoDAO.findAll();
	}
	
	/**
	 * Faz refer�ncia a DAO.
	 * @param projeto
	 * @return
	 * @author Taidson
	 * @since 14/07/2010
	 */
	public Projeto arquivosEHistoricosPorProjeto(Projeto projeto){
		return projetoDAO.arquivosEHistoricosPorProjeto(projeto);
	}
	
	/**
	 * Verifica se existem arquivos a serem exclu�dos, para informar no hist�rico
	 * @param projetoarquivoatual
	 * @param listaArquivoBd
	 * @return
	 * @author Taidson
	 * @since 14/07/2010
	 */
	public Projetoarquivo arquivosExcluidos(Projetoarquivo projetoarquivoatual, List<Projetoarquivo> listaArquivo){
		if(!listaArquivo.contains(projetoarquivoatual)){
			return projetoarquivoatual;
		}
		return null;
	}
	/**
	  * Verifica se existem arquivos a serem modificados, para informar no hist�rico
	 * @param projetoarquivoatual
	 * @param listaArquivoBd
	 * @return
	 * @author Taidson
	 * @since 14/07/2010
	 */
	public Projetoarquivo arquivosModificados(Projetoarquivo projetoarquivoatual, List<Projetoarquivo> listaArquivoBd){
		if(listaArquivoBd.contains(projetoarquivoatual)){
			for (Projetoarquivo item : listaArquivoBd) {
				if(projetoarquivoatual.getCdprojetoarquivo().equals(item.getCdprojetoarquivo()) && !projetoarquivoatual.getDescricao().equalsIgnoreCase(item.getDescricao())){
					return projetoarquivoatual;
				}
			}
		}
		return null;
	}

	/**
	 * M�todo respons�vel por gerar hist�ricos de projeto
	 * @param bean
	 * @return
	 * @author Taidson
	 * @since 14/07/2010
	 */
	public Projeto gerarProjetohistorico(Projeto bean){
		
		Boolean novoArquivo = false;
		Projeto projeto = new Projeto();
		projeto = this.arquivosEHistoricosPorProjeto(bean);
		
		List<Projetoarquivo> listaArquivosExcluidos = new ListSet<Projetoarquivo>(Projetoarquivo.class); 
		List<Projetoarquivo> listaArquivosModificados = new ListSet<Projetoarquivo>(Projetoarquivo.class); 
		
		List<Projetohistorico> listaProjetohistorico = new ListSet<Projetohistorico>(Projetohistorico.class);
		
		if(projeto.getListaProjetohistorico() != null && projeto.getListaProjetohistorico().size() > 0){
			listaProjetohistorico = projeto.getListaProjetohistorico();
		}
		
		if(projeto.getListaProjetoarquivo() != null && projeto.getListaProjetoarquivo().size() > 0){
			if(bean.getListaProjetoarquivo() != null && bean.getListaProjetoarquivo().size() > 0){
				//Verifica se h� arquivos exclu�dos, se houver ser� incluido no hist�rico
				for(Projetoarquivo projetoarquivoatual : projeto.getListaProjetoarquivo()) {
					Projetoarquivo arquivoAtual = new Projetoarquivo();
					if(projetoarquivoatual.getCdprojetoarquivo() != null)
						arquivoAtual = this.arquivosExcluidos(projetoarquivoatual, bean.getListaProjetoarquivo());
					
					if(arquivoAtual != null && arquivoAtual.getCdprojetoarquivo() != null)
						listaArquivosExcluidos.add(arquivoAtual);
				}
				//Verifica se h� arquivos modificados, se houver ser� incluido no hist�rico
				for (Projetoarquivo projetoarquivoatual : bean.getListaProjetoarquivo()) {
					Projetoarquivo arquivoAtual = new Projetoarquivo();
					if(projetoarquivoatual.getCdprojetoarquivo() != null)
						arquivoAtual = this.arquivosModificados(projetoarquivoatual, projeto.getListaProjetoarquivo());
					
					if(arquivoAtual != null && arquivoAtual.getCdprojetoarquivo() != null){
						listaArquivosModificados.add(arquivoAtual);
					}
					
				}
			}else{
				for(Projetoarquivo projetoarquivobd : projeto.getListaProjetoarquivo()) {
					listaArquivosExcluidos.add(projetoarquivobd);
				}
			}
		}
		// Seta informa��es de arquivos exclu�dos, na lista de hist�rico
		if(listaArquivosExcluidos != null && listaArquivosExcluidos.size() > 0){
			for (Projetoarquivo arquivoExcluido : listaArquivosExcluidos) {
				Projetohistorico phExlcluido = new Projetohistorico();
				phExlcluido.setDthistorico(new Timestamp(System.currentTimeMillis()));
				phExlcluido.setObservacao("Arquivo: " + arquivoExcluido.getDescricao() + " exclu�do.");
				phExlcluido.setUsuarioresponsavel(SinedUtil.getUsuarioLogado());
				listaProjetohistorico.add(phExlcluido);
			}
			
		}
		// Seta informa��es de arquivos modificados, na lista de hist�rico
		if(listaArquivosModificados != null && listaArquivosModificados.size() > 0){
			for (Projetoarquivo arquivoAlterado : listaArquivosModificados) {
				Projetohistorico phAlterado = new Projetohistorico();
				phAlterado.setDthistorico(new Timestamp(System.currentTimeMillis()));
				phAlterado.setObservacao("Arquivo: " + arquivoAlterado.getDescricao() + " alterado.");
				phAlterado.setUsuarioresponsavel(SinedUtil.getUsuarioLogado());
				listaProjetohistorico.add(phAlterado);
			}
			
		}
		// Seta informa��es de arquivos inclu�dos, na lista de hist�rico
		if(bean.getListaProjetoarquivo() != null && bean.getListaProjetoarquivo().size() > 0){
			for (Projetoarquivo pa : bean.getListaProjetoarquivo()) {
				if(pa.getCdprojetoarquivo() == null){
					novoArquivo = true;
					Projetohistorico phNovo = new Projetohistorico();
					phNovo.setDthistorico(new Timestamp(System.currentTimeMillis()));
					phNovo.setObservacao("Arquivo: " + pa.getDescricao() + " inclu�do.");
					phNovo.setUsuarioresponsavel(SinedUtil.getUsuarioLogado());
					listaProjetohistorico.add(phNovo);
				}
			}
		}
		if((listaArquivosExcluidos == null || listaArquivosExcluidos.size() == 0)
				&& (listaArquivosModificados == null || listaArquivosModificados.size() == 0)
				&& (novoArquivo == false)){
			Projetohistorico ph = new Projetohistorico();
			ph.setDthistorico(new Timestamp(System.currentTimeMillis()));
			ph.setObservacao("Alterado.");
			ph.setUsuarioresponsavel(SinedUtil.getUsuarioLogado());
			listaProjetohistorico.add(ph);
		}
		
		bean.setListaProjetohistorico(listaProjetohistorico);
		
		return bean;
	}
	
	public Projeto loadForEmail(Projeto projeto) {
		return projetoDAO.loadForEmail(projeto);
	}
	
	public Projeto loadForDiarioObra(Projeto projeto) {
		return projetoDAO.loadForDiarioObra(projeto);
	}
	
	/**
	 * M�todo que gera relat�rio de acompanhamento econ�mico
	 * 
	 * @param filtro
	 * @return
	 * @author Giovane Freitas
	 */
	public Resource gerarAcompanhamentoEconomicoExcel(AcompanhamentoEconomicoReportFiltro filtro) throws IOException{
		try{ 
			Projeto projeto = projetoDAO.loadForAcompanhamentoEconomico(filtro.getProjeto());
			projeto.setListaProjetodespesa(projetodespesaService.findByProjetoData(projeto, filtro.getDtFim()));
			
			Proposta proposta = null;
			if (projeto.getProposta() != null && projeto.getProposta().getCdproposta() != null)
				proposta = propostaService.loadWithView(projeto.getProposta());
			
			SinedExcel wb = new SinedExcel();
	
			HSSFSheet planilha = wb.createSheet("Planilha");
			planilha.setDefaultColumnWidth((short) 15);
			planilha.setColumnWidth((short)0, (short) (3 * 256) );// Set the width (in units of 1/256th of a character width)
			planilha.setColumnWidth((short)1, (short) (33 * 256) );
			
			short border =  (short) 1;
			short linhaAtual = 0;
			
			//--------------
			//Estilo do t�tulo de se��o
			HSSFCellStyle secaoStyle = wb.createCellStyle();
			secaoStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			secaoStyle.setBorderTop(border);
			secaoStyle.setBorderBottom(border);
			secaoStyle.setBorderLeft(border);
			secaoStyle.setBorderRight(border);
			secaoStyle.setFillForegroundColor(HSSFColor.BLUE.index);
			secaoStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			secaoStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			HSSFFont font = wb.createFont();
			font.setFontHeightInPoints((short) 10);
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font.setFontName("Arial");
			secaoStyle.setFont(font);
			secaoStyle.setWrapText(true);
			//--------------
	
			//--------------
			//Estilo do t�tulo de subse��o
			HSSFCellStyle subSecaoStyle = wb.createCellStyle();
			subSecaoStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			subSecaoStyle.setBorderTop(border);
			subSecaoStyle.setBorderBottom(border);
			subSecaoStyle.setBorderLeft(border);
			subSecaoStyle.setBorderRight(border);
			subSecaoStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
			subSecaoStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			subSecaoStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			subSecaoStyle.setWrapText(true);
			//--------------
			
			//--------------
			//Estilo do detalhe
			HSSFCellStyle detalheStyle = wb.createCellStyle();
			detalheStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			detalheStyle.setBorderTop(border);
			detalheStyle.setBorderBottom(border);
			detalheStyle.setBorderLeft(border);
			detalheStyle.setBorderRight(border);
			detalheStyle.setFillForegroundColor(HSSFColor.WHITE.index);
			detalheStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			detalheStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
			detalheStyle.setWrapText(true);
			//--------------
			
			////////////////
			//Linha 1
			String titulo = "PROJETO: " + projeto.getNome() + "\n                      " + projeto.getLocalidade();
			criarCelula(wb, planilha, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT, titulo, 0, 3, linhaAtual, linhaAtual);
			
			titulo = "CLIENTE: " + (projeto.getCliente() != null ? projeto.getCliente().getNome() : "");
			criarCelula(wb, planilha, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_LEFT, titulo, 4, 5, linhaAtual, linhaAtual);
			
			HSSFRow projetoRow = planilha.getRow(linhaAtual);
			projetoRow.setHeight((short) (36*20) );// Set the height in "twips" or 1/20th of a point.
			/////////////////
			
			//===============
			//Linha 2
			String texto = "DATA DA ATUALIZA��O: ";
			criarCelula(wb, planilha, detalheStyle, texto, 0, 3, ++linhaAtual, linhaAtual);
			
			texto = SinedDateUtils.toString(new Date(System.currentTimeMillis()));
			criarCelula(wb, planilha, detalheStyle, texto, 4, 5, linhaAtual, linhaAtual);
			//===============
			
			////////////////
			//Linha 3
			criarCelula(wb, planilha, secaoStyle, "DADOS DO CONTRATO", 0, 5, ++linhaAtual, linhaAtual);
			/////////////////

			////////////////
			//Linhas 4 e 5 
			criarCelula(wb, planilha, detalheStyle, "IT", 0, 0, ++linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "DESCRI��O", 1, 1, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "UND", 2, 2, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "QTE", 3, 3, linhaAtual, linhaAtual+1);

			criarCelula(wb, planilha, detalheStyle, "INDICADOR", 4, 5, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "TIPO", 4, 4, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "VALOR", 5, 5, linhaAtual+1, linhaAtual+1);
			linhaAtual++;
			/////////////////
			
			//---------------
			//Linha 6
			criarCelula(wb, planilha, detalheStyle, "1", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Prazo", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Semana", 2, 2, linhaAtual, linhaAtual);
			
			HSSFCell celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			Date dtfimprojeto = projeto.getDtfimprojeto() != null ? projeto.getDtfimprojeto() : filtro.getDtFim();
			int semanas = SinedDateUtils.diferencaSemanas(dtfimprojeto, projeto.getDtprojeto());
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			celula.setCellValue(semanas);

			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------

			//---------------
			//Linha 7
			criarCelula(wb, planilha, detalheStyle, "2", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "M�o de obra", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "h", 2, 2, linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			
			List<Planejamento> planejamentos = planejamentoService.findByProjetoSituacao(projeto, Planejamentosituacao.AUTORIZADA);
			if(planejamentos != null && planejamentos.size() > 0){
				Planejamento planejamento = planejamentoService.loadWithOrcamento(planejamentos.get(0));
				if(planejamento.getOrcamento() != null && planejamento.getOrcamento().getCdorcamento() != null){
					Double totalhoras = orcamentorecursohumanoService.calculaTotalHorasMDO(planejamento.getOrcamento());
					celula.setCellValue(totalhoras);
				}
			}
			
//			if (proposta != null)
//				celula.setCellValue(proposta.getAux_proposta().getMdo().getValue().doubleValue());
			
			criarCelula(wb, planilha, detalheStyle, "h/semanas", 4, 4, linhaAtual, linhaAtual);
			
			HSSFCell indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D7/D6");
			//---------------
			
			//---------------
			//Linha 8
			criarCelula(wb, planilha, detalheStyle, "3", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Valor de venda", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			Money valorVenda = new Money();
			if (proposta != null)
				proposta.getAux_proposta().getValor();
			celula.setCellValue(valorVenda.getValue().doubleValue());
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D8/D7");
			//---------------
			
			//---------------
			//Linha 9
			criarCelula(wb, planilha, detalheStyle, "4", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Despesas", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			int numDespesas = projeto.getListaProjetodespesa().size();
			celula.setCellFormula("C" + (numDespesas + 41) );//Existem 41 linhas est�ticas at� o total
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);

			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D9/D7");
			//---------------
			
			//---------------
			//Linha 10
			criarCelula(wb, planilha, secaoStyle, "DESEMPENHO DO CONTRATO", 0, 5, ++linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 11 e 12
			criarCelula(wb, planilha, detalheStyle, "IT", 0, 0, ++linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "DESCRI��O", 1, 1, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "UND", 2, 2, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "QTE", 3, 3, linhaAtual, linhaAtual+1);

			criarCelula(wb, planilha, detalheStyle, "INDICADOR", 4, 5, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "TIPO", 4, 4, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "VALOR", 5, 5, linhaAtual+1, linhaAtual+1);
			linhaAtual++;
			//---------------
			
			//---------------
			//Linha 13
			criarCelula(wb, planilha, subSecaoStyle, "1", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, subSecaoStyle, "Faturamento", 1, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 14
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acumulado Faturamento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			Money faturamentoAcumulado = documentoDAO.getTotalProjeto(filtro, Documentoclasse.OBJ_RECEBER, null);
			celula.setCellValue(faturamentoAcumulado.getValue().doubleValue());
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D14/D32");
			//---------------
			
			//---------------
			//Linha 15
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acumulado Medi��es", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			//TODO: Total de medi��es
			criarCelula(wb, planilha, detalheStyle, "", 3, 3, linhaAtual, linhaAtual);

			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D15/D32");
			//---------------
			
			//---------------
			//Linha 16
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Se proporcional a MDO, seria:", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D32*F8");

			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D16/D32");
			//---------------
			
			//---------------
			//Linha 17
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Rendimento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D14-D16");

			criarCelula(wb, planilha, detalheStyle, "%", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D17/D15");
			//---------------
			
			//---------------
			//Linha 18
			criarCelula(wb, planilha, subSecaoStyle, "2", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, subSecaoStyle, "Despeas", 1, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 19
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acumulado da obra", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("E" + (numDespesas + 41) );
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D19/D32");
			//---------------
			
			//---------------
			//Linha 20
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Se proporcional ao faturamento, seria:", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D14/D15*F8*D32");
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D20/D32");
			//---------------
			
			//---------------
			//Linha 21
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Se proporcional �s medi��es, seria:", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D15/D16*F9*D32");
			
			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D21/D32");
			//---------------
			
			//---------------
			//Linha 22
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Rendimento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);

			indicador = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D21-D19");
			
			criarCelula(wb, planilha, detalheStyle, "%", 4, 4, linhaAtual, linhaAtual);
			
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D22/D21");
			//---------------
			
			//---------------
			//Linha 23
			criarCelula(wb, planilha, subSecaoStyle, "3", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, subSecaoStyle, "Prazo", 1, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 24
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Data Inicio", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "data", 2, 2, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, SinedDateUtils.toString(projeto.getDtprojeto()), 3, 3, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 25
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Data Fim", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "data", 2, 2, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, SinedDateUtils.toString(dtfimprojeto), 3, 3, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 26
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Semanas", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "qte", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("(D25-D24)/7");
			
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 27
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Avan�o do prazo", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "qte", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("(E2-D24)/7");
			
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 28
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Avan�o do prazo", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "%", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D27/D26");
			
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 29
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Avan�o real da obra", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "%", 2, 2, linhaAtual, linhaAtual);
			
			//TODO: Definir o valor
			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			celula.setCellValue(producaodiariaService.getMediaPercentualProducaoDiaria(projeto));
			
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 30
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Rendimento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "%", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D29/D28");
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 4, 4, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("IF(D30>100;\"Adiantamento (d)\";\"Atraso (d)\")");
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("(D30-100)*D27*7");
			//---------------
			
			//---------------
			//Linha 31
			criarCelula(wb, planilha, subSecaoStyle, "4", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, subSecaoStyle, "M�o de obra", 1, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 32
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acumulado da obra", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "h", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			ApontamentoFiltro apontamentoFiltro = new ApontamentoFiltro();
			apontamentoFiltro.setProjeto(projeto);
			apontamentoFiltro.setDtapontamentoAte(filtro.getDtFim());
			apontamentoFiltro.setApontamentoTipo(ApontamentoTipo.FUNCAO_HORA);
			celula.setCellValue(apontamentoDAO.getTotalRhMinutos(apontamentoFiltro )/60);
			
			criarCelula(wb, planilha, detalheStyle, "h/semana", 4, 4, linhaAtual, linhaAtual);
			indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			indicador.setCellFormula("D32/D27");
			//---------------
			
			//---------------
			//Linha 33
			criarCelula(wb, planilha, subSecaoStyle, "5", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, subSecaoStyle, "Estat�stica de seguran�a do trabalho", 1, 5, linhaAtual, linhaAtual);
			//---------------

			//---------------
			//Linha 34
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acidente sem afastamento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "qte", 2, 2, linhaAtual, linhaAtual);
			//---------------

			//---------------
			//Linha 35
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Acidente com afastamento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "qte", 2, 2, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 36
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Taxa de Frequ�ncia", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Vr", 2, 2, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 37
			criarCelula(wb, planilha, detalheStyle, null, 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Taxa de Gravidade", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Vr", 2, 2, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 38
			criarCelula(wb, planilha, secaoStyle, "COMPORTAMENTO DAS DESPESAS", 0, 5, ++linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha 39 e 40
			criarCelula(wb, planilha, detalheStyle, "IT", 0, 0, ++linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "DESCRI��O DA DESPESA", 1, 1, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "OR�AMENTO", 2, 3, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "REALIZADO", 4, 5, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "TOTAL R$", 2, 2, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "R$/h", 3, 3, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "ACUM R$", 4, 4, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "R$/h", 5, 5, linhaAtual+1, linhaAtual+1);
			linhaAtual++;
			//---------------
			
			
			//===============
			//Linhas de despesas
			if (projeto.getListaProjetodespesa() != null){
				int count = 1;
				for (Projetodespesa despesa : projeto.getListaProjetodespesa()){
					criarCelula(wb, planilha, detalheStyle, String.valueOf(count++), 0, 0, ++linhaAtual, linhaAtual);
					String descricaoConta = despesa.getContagerencial().getVcontagerencial().getNome();
					criarCelula(wb, planilha, detalheStyle, descricaoConta, 1, 1, linhaAtual, linhaAtual);
					
					celula = criarCelula(wb, planilha, detalheStyle, null, 2, 2, linhaAtual, linhaAtual);
					celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					celula.setCellValue(despesa.getValortotal().getValue().doubleValue());
					
					celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
					celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					celula.setCellValue(despesa.getValorhora().getValue().doubleValue());

					Money totalRealizado = this.getTotalRealizado(filtro, despesa.getContagerencial());
					celula = criarCelula(wb, planilha, detalheStyle, null, 4, 4, linhaAtual, linhaAtual);
					celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					celula.setCellValue(totalRealizado.getValue().doubleValue());
					
					indicador = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
					indicador.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					indicador.setCellFormula("E" + (linhaAtual +1) + "/$D$32");//linhaAtual � baseada em ZERO, excel em UM
				}
			}
			//===============
			
			//---------------
			//Linha NUM_DESEPESAS + 41
			criarCelula(wb, planilha, SinedExcel.STYLE_TOTAL_LEFT, "TOTAL DE DESPESAS", 0, 1, ++linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, SinedExcel.STYLE_TOTAL_LEFT, null, 2, 2, linhaAtual, linhaAtual);
			if (numDespesas > 0){
				celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
				celula.setCellFormula("SUM(C41:C" + (linhaAtual) + ")");//linhaAtual � baseada em ZERO, excel em UM
			}
			
			criarCelula(wb, planilha, SinedExcel.STYLE_TOTAL_LEFT, null, 3, 3, linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, SinedExcel.STYLE_TOTAL_LEFT, null, 4, 4, linhaAtual, linhaAtual);
			if (numDespesas > 0){
				celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
				celula.setCellFormula("SUM(E41:E" + (linhaAtual) + ")");//linhaAtual � baseada em ZERO, excel em UM
			}
			
			criarCelula(wb, planilha, SinedExcel.STYLE_TOTAL_LEFT, null, 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS + 42
			criarCelula(wb, planilha, secaoStyle, "PROJE��O DO CONTRATO", 0, 5, ++linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS +43 e +44
			criarCelula(wb, planilha, detalheStyle, "IT", 0, 0, ++linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "DESCRI��O", 1, 1, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "UND", 2, 2, linhaAtual, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "QTE", 3, 3, linhaAtual, linhaAtual+1);

			criarCelula(wb, planilha, detalheStyle, "INDICADOR", 4, 5, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "TIPO", 4, 4, linhaAtual+1, linhaAtual+1);
			criarCelula(wb, planilha, detalheStyle, "VALOR", 5, 5, linhaAtual+1, linhaAtual+1);
			linhaAtual++;
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS + 45
			criarCelula(wb, planilha, detalheStyle, "1", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Prazo", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Semana", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D26/D30");
			
			criarCelula(wb, planilha, detalheStyle, "X", 4, 4, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "X", 5, 5, linhaAtual, linhaAtual);
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS + 46
			criarCelula(wb, planilha, detalheStyle, "2", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "M�o de obra", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "h", 2, 2, linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("(D32/D30)/D27*D" + (numDespesas + 45) );

			celula = criarCelula(wb, planilha, detalheStyle, "h/semana", 4, 4, linhaAtual, linhaAtual);
			
			celula = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D" + (numDespesas + 46) + "/D" + (numDespesas + 45) );
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS + 47
			criarCelula(wb, planilha, detalheStyle, "3", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Faturamento", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			celula.setCellValue(valorVenda.subtract(faturamentoAcumulado).getValue().doubleValue());


			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D" + (numDespesas + 47) + "/D" + (numDespesas + 46) );
			//---------------
			
			//---------------
			//Linha NUM_DESEPESAS + 48
			criarCelula(wb, planilha, detalheStyle, "4", 0, 0, ++linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "Despesas", 1, 1, linhaAtual, linhaAtual);
			criarCelula(wb, planilha, detalheStyle, "R$", 2, 2, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 3, 3, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("D" + (numDespesas + 46) + "*F" + (numDespesas + 48) );

			criarCelula(wb, planilha, detalheStyle, "R$/h", 4, 4, linhaAtual, linhaAtual);

			celula = criarCelula(wb, planilha, detalheStyle, null, 5, 5, linhaAtual, linhaAtual);
			celula.setCellType(HSSFCell.CELL_TYPE_FORMULA);
			celula.setCellFormula("F19");
			//---------------
			
			return wb.getWorkBookResource("W3ERP_Acompanhamento_Economico.xls");
		}catch (Exception e) {
			throw new SinedException("Ocorreu um erro ao gerar a planilha.", e);
		}
	}
	
	/**
	 * M�todo que cria uma c�lula simples ou um conjunto de c�lulas mescladas.
	 * 
	 * @author Giovane Freitas
	 * @return 
	 */
	private HSSFCell criarCelula(SinedExcel wb, HSSFSheet planilha, HSSFCellStyle cellStyle, String texto, int colFrom, int colTo, int rowFrom, int rowTo)
			throws NestableException {
		
		short border = 1;
		
		HSSFRow detalhe1Row = planilha.getRow((short) rowFrom);
		if (detalhe1Row == null)
			detalhe1Row = planilha.createRow((short) rowFrom);
		
		HSSFCell detalhe1Cell = detalhe1Row.createCell((short) colFrom);
		detalhe1Cell.setCellStyle(cellStyle);
		
		if (texto != null)
			detalhe1Cell.setCellValue(texto);

		if (rowFrom != rowTo || colFrom != colTo){
			Region detalhe1Region = new Region((short)rowFrom, (short)colFrom, (short)rowTo, (short)colTo);
			planilha.addMergedRegion(detalhe1Region);
			
			// Set the border and border colors.
			HSSFRegionUtil.setBorderBottom(border, detalhe1Region, planilha, wb);
			HSSFRegionUtil.setBorderTop(border, detalhe1Region, planilha, wb);
			HSSFRegionUtil.setBorderLeft(border, detalhe1Region, planilha, wb);
			HSSFRegionUtil.setBorderRight(border, detalhe1Region, planilha, wb);
		}
		
		return detalhe1Cell;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o projeto pela sigla.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findBySigla(String sigla)
	 *
	 * @param sigla
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Projeto findBySigla(String sigla) {
		return projetoDAO.findBySigla(sigla);
	}
	
	
	/**
	* M�todo com refer�ncia no DAO
	* Busca o local de armzazenagem e o centro de custo do projeto
	* 
	* @see	br.com.linkcom.sined.geral.dao.ProjetoDAO.carregaLocalarmazenagemCentroCusto(Projeto projeto)
	*
	* @param projeto
	* @return
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public Projeto carregaLocalarmazenagemCentroCusto(Projeto projeto) {
		return projetoDAO.carregaLocalarmazenagemCentroCusto(projeto);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#buscaDetalheAcompanhamentoEconomico
	 *
	 * @param projeto
	 * @param dtfim
	 * @param contagerencial
	 * @return
	 * @since 13/10/2011
	 * @author Rodrigo Freitas
	 */
	public List<AcompanhamentoEconomicoListagemDetalheBean> buscaDetalheAcompanhamentoEconomico(Projeto projeto, Date dtfim, Contagerencial contagerencial) {
		return projetoDAO.buscaDetalheAcompanhamentoEconomico(projeto, dtfim, contagerencial);
	}
	
	/**
	 * Busca o valor realizado de um projeto at� uma data por contagerencial.
	 * Soma os seguintes valores:
	 * 	- Total de ordem de compra.
	 * 	- Total de fornecimento.
	 * 	- Total de conta a pagar que n�o veio de ordem de compra.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.OrdemcompraDAO#getValorTotalProjeto
	 * @see br.com.linkcom.sined.geral.dao.FornecimentocontratoDAO#getValorTotalProjeto
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#getValorTotalProjeto
	 *
	 * @param filtro
	 * @param contagerencial
	 * @return
	 * @since 13/10/2011
	 * @author Rodrigo Freitas
	 */
	public Money getTotalRealizado(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		
		Money totalOrdemCompra;
		if(filtro.getConsiderarFrequenciaOC() != null && filtro.getConsiderarFrequenciaOC()){
			totalOrdemCompra = ordemcompraDAO.getValorTotalProjetoFluxocaixa(filtro, contagerencial);
		} else {
			totalOrdemCompra = ordemcompraDAO.getValorTotalProjeto(filtro, contagerencial);
		}
		Money totalFornecimento = fornecimentocontratoDAO.getValorTotalProjeto(filtro, contagerencial);
		Money totalContapagar = contapagarService.getValorTotalProjeto(filtro, contagerencial);
		
		return totalOrdemCompra
					.add(totalFornecimento)
					.add(totalContapagar)
					;
	}
	
	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 */
	public Iterator<Projeto> findForArquivoGerencial() {
		return projetoDAO.findForArquivoGerencial();
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoService#findForGerarOrdemServico(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForGerarOrdemServico(String whereIn) {
		return projetoDAO.findForGerarOrdemServico(whereIn);
	}
	
	/**
	 * 
	 * M�todo que cria o CSV referente a listagem de Projeto.
	 *
	 * @name prepararArquivoClienteCSV
	 * @param filtro
	 * @return
	 * @return Resource
	 * @author Thiago Augusto
	 * @date 28/06/2012
	 *
	 */
	public Resource prepararProjetoCSV(ProjetoFiltro filtro) {
		StringBuilder csv = new StringBuilder();
//		ADICIONA OS CABE�ALHOS
		csv.append("Descri��o;CNPJ;CEI;Data de In�cio;Data de T�rmino;Total de Semana(s);Sigla;Local de Entrega de Compras; " +
				"Uf/Cidade;Respons�vel;Cliente;Empresa;Centro de Custo;Proposta;\n");
//		BUSCA A LISTA DO CSV
		List<Projeto>listaProjetosCVS = this.findListaProjetoForCSV(filtro);
//		MONTA O DETALHE DO CSV
		for (Projeto projeto : listaProjetosCVS) {
			csv.append(projeto.getNome() != null ? projeto.getNome() : "");
			csv.append(";");
			csv.append(projeto.getCnpj() != null ? projeto.getCnpj() : "");
			csv.append(";");
			csv.append(projeto.getCei() != null ? projeto.getCei() : "");
			csv.append(";");
			csv.append(projeto.getDtprojeto() != null ? projeto.getDtprojeto() : "");
			csv.append(";");
			csv.append(projeto.getDtfimprojeto() != null ? projeto.getDtfimprojeto() : "");
			csv.append(";");
//			TOTAL SEMANAS
			if (projeto.getDtprojeto() != null && projeto.getDtfimprojeto() != null)
				csv.append(SinedDateUtils.diferencaSemanas(projeto.getDtfimprojeto(), projeto.getDtprojeto()));
			csv.append(";");
			csv.append(projeto.getSigla() != null ? projeto.getSigla() : "");
			csv.append(";");
			csv.append(projeto.getLocalarmazenagem() != null ? projeto.getLocalarmazenagem().getNome() : "");
			csv.append(";");
			csv.append(projeto.getMunicipio() != null && projeto.getMunicipio().getUf() != null ? projeto.getMunicipio().getUf().getSigla() + "/" + projeto.getMunicipio().getNome() : "");
			csv.append(";");
			csv.append(projeto.getColaborador() != null ? projeto.getColaborador().getNome() : "");
			csv.append(";");
			csv.append(projeto.getCliente() != null ? projeto.getCliente().getNome() : "");
			csv.append(";");
			csv.append(projeto.getEmpresa() != null ? projeto.getEmpresa().getRazaosocialOuNome() : "");
			csv.append(";");
			csv.append(projeto.getCentrocusto() != null ? projeto.getCentrocusto().getNome() : "");
			csv.append(";");
			csv.append(projeto.getProposta() != null ? " " + projeto.getProposta().getDescricaoCombo() : "");
			csv.append(";");
			csv.append("\n");
		}
		Resource resource = new Resource("text/csv", "contrato_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;		
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListaProjetoForCSV
	 * @param filtro
	 * @return
	 * @return List<Projeto>
	 * @author Thiago Augusto
	 * @date 28/06/2012
	 *
	 */
	public List<Projeto> findListaProjetoForCSV(ProjetoFiltro filtro){
		return projetoDAO.findListaProjetoForCSV(filtro);
	}
	/**
	 * Metodo com refer�ncia no DAO
	 * @param q
	 * @return
	 * @author Thiers Euler
	 */
	public List<Projeto> findForAutocompleteByUsuariologado(String q) {
		return projetoDAO.findForAutocompleteByUsuariologado(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findForComboByUsuariologadoNotCancelado(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologadoNotCancelado(String q) {
		return projetoDAO.findForComboByUsuariologadoNotCancelado(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findForPVOffline()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<ProjetoOfflineJSON> findForPVOffline(){
		List<ProjetoOfflineJSON> lista = new ArrayList<ProjetoOfflineJSON>();
		for(Projeto p : projetoDAO.findForPVOffline())
			lista.add(new ProjetoOfflineJSON(p));
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ProjetoDAO#findForComboByUsuariologado()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologado() {
		return projetoDAO.findForComboByUsuariologado();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.ProjetoDAO#findForComboByUsuariologadoNotCancelado()
	 *
	 * @param situacaoprojeto
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Projeto> findForComboByUsuariologadoNotCancelado() {
		return projetoDAO.findForComboByUsuariologadoNotCancelado();
	}
	
	public List<Projeto> findForComboByUsuariologado(List<Projeto> projetos, boolean retirarCancelados) {
		return projetoDAO.findForComboByUsuariologado(projetos, retirarCancelados);
	}
	
	public List<Projeto> findForSolicitacaoCompra(List<Projeto> projetos, boolean retirarFinalizados) {
		return projetoDAO.findForSolicitacaoCompra(projetos, retirarFinalizados);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * 
	 *
	 * 
	 * @return lista de projetos
	 * @author Lucas Costa
	 */
	public List<Projeto> findNaoCancelados() {
		return projetoDAO.findNaoCancelados();
	}
	
	public List<Projeto> findByCentrocusto(Centrocusto centrocusto){
		return projetoDAO.findByCentrocusto(centrocusto);
	}

	public Projeto loadForRequisicao(Projeto projeto) {
		return projetoDAO.loadForRequisicao(projeto);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @param date
	 * @param whereIn
	 * @return
	 * @author Lucas Costa
	 */
	public void updateDataTerminio(String whereIn, Date date){
		projetoDAO.updateDataTerminio(whereIn, date);
	}
	
	public List<ProjetoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ProjetoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ProjetoRESTModel> lista = new ArrayList<ProjetoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Projeto bean : projetoDAO.findForAndroid(whereIn))
				lista.add(new ProjetoRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * M�tdo que busca o projeto para o combo 
	 * reload com o tipo de projeto e com a 
	 * valida��o aberto ou fechado
	 * @return
	 */
	public List<Projeto> findComboProjetoTipo(Boolean projetoAberto, Projetotipo projetotipo){
		return projetoDAO.findComboProjetoTipo(projetoAberto,projetotipo);
	}
	
	/**
	 * M�tdo que busca o projeto para o combo 
	 * reload com o tipo de projeto
	 * @return
	 */
	public List<Projeto> findComboProjetoTipo(Projetotipo projetotipo){
		return projetoDAO.findComboProjetoTipo(projetotipo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProjetoService#findProjetosAtivos(List<Projeto> projetos, Boolean permissaoProjeto)
	*
	* @param projeto
	* @param permissaoProjeto
	* @return
	* @since 22/07/2016
	* @author Luiz Fernando
	*/
	public List<Projeto> findProjetosAtivos(Projeto projeto, Boolean permissaoProjeto) {
		List<Projeto> listaProjeto = null;
		if(projeto != null){
			listaProjeto = new ArrayList<Projeto>();
			listaProjeto.add(projeto);
		}
		return findProjetosAtivos(listaProjeto, permissaoProjeto);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ProjetoService#findProjetosAtivos(List<Projeto> projetos, Boolean permissaoProjeto)
	*
	* @param projetos
	* @param permissaoProjeto
	* @return
	* @since 22/07/2016
	* @author Luiz Fernando
	*/
	public List<Projeto> findProjetosAtivos(List<Projeto> projetos, Boolean permissaoProjeto) {
		return projetoDAO.findProjetosAtivos(projetos, permissaoProjeto);
	}
	
	public List<Projeto> findByCdsprojetotipo(String cdsprojetotipo) {
		if (cdsprojetotipo==null || cdsprojetotipo.trim().equals("")){
			return new ArrayList<Projeto>();
		}else {
			return projetoDAO.findByCdsprojetotipo(cdsprojetotipo);
		}
	}
	public List<Projeto> buscarParaCusteio(Date dtInicio, Date dtFim) {
		return projetoDAO.buscarParaCusteio(dtInicio, dtFim);
	}
	public Projeto buscarParaProcessarCusteio(Integer id) {
		return projetoDAO.buscarParaProcessarCusteio(id);
	}
	
	public List<br.com.linkcom.sined.util.bean.GenericBean> findForWSVenda(Usuario usuario){
		return ObjectUtils.translateEntityListToGenericBeanList(ProjetoService.getInstance().findForCombo());
	}
	
}




















