package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.dao.IndiceDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class IndiceService extends GenericService<Indice> {

	private IndiceDAO indiceDAO;
	
	public void setIndiceDAO(IndiceDAO indiceDAO) {
		this.indiceDAO = indiceDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.IndiceDAO#loadRecursos(Indice)
	 * @param indice
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Indice loadRecursos(Indice indice){
		return indiceDAO.loadRecursos(indice);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.IndiceDAO#loadWithUnidade
	 * @param indice
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice loadWithUnidade(Indice indice) {
		return indiceDAO.loadWithUnidade(indice);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.IndiceDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Indice> findAllForFlex(){
		return indiceDAO.findAllForFlex();		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.IndiceDAO#findWithListasFlex
	 * @param indice
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice findWithListasFlex(Indice indice){
		return indiceDAO.findWithListasFlex(indice);
	}
	
	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.IndiceDAO#findForImportacaoTarefa
	 *
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Indice findForImportacaoTarefa(String identificador) {
		return indiceDAO.findForImportacaoTarefa(identificador);
	}
	
}
