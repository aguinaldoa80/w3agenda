package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Localarmazenagemempresa;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.LocalarmazenagemOfflineJSON;
import br.com.linkcom.sined.util.rest.android.localarmazenagem.LocalarmazenagemRESTModel;

public class LocalarmazenagemService extends GenericService<Localarmazenagem> {

	private LocalarmazenagemDAO localarmazenagemDAO;
	private EnderecoService enderecoService;
	private LocalarmazenagemempresaService localarmazenagemempresaService;
	private MunicipioService municipioService;
	
	public void setLocalarmazenagemDAO(LocalarmazenagemDAO localarmazenagemDAO) {
		this.localarmazenagemDAO = localarmazenagemDAO;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setLocalarmazenagemempresaService(
			LocalarmazenagemempresaService localarmazenagemempresaService) {
		this.localarmazenagemempresaService = localarmazenagemempresaService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}

	/* singleton */
	private static LocalarmazenagemService instance;
	public static LocalarmazenagemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LocalarmazenagemService.class);
		}
		return instance;
	}
	
	@Override
	public void saveOrUpdate(final Localarmazenagem bean) {
		if(bean.getEndereco()!= null && bean.getEndereco().getCdendereco() == null)
			bean.getEndereco().setEnderecotipo(Enderecotipo.UNICO);
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				enderecoService.saveOrUpdateNoUseTransaction(bean.getEndereco());
				saveOrUpdateNoUseTransaction(bean);
				return status;
			}
		});
	}
	
	/**
	* M�todo que busca os locais de armazenagem de acordo com a empresa
	*
	* @param q
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemEmpresaAutocomplete(String q) {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {} 
		
		return findLocalarmazenagemAutocomplete(q, null, cdempresa);
	}
	
	/**
	* M�todo que busca os locais de armazenagem ativos de acordo com a empresa
	*
	* @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#findLocalarmazenagemAutocomplete(String q, Boolean ativo, String whereInEmpresa)
	*
	* @param q
	* @return
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAtivoEmpresaAutocomplete(String q) {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {} 
		
		return findLocalarmazenagemAutocomplete(q, true, cdempresa);
	}
	
	/**
	* M�todo que busca os locais de armazenagem
	*
	* @param q
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAutocomplete(String q) {
		return findLocalarmazenagemAutocomplete(q, null, null);
	}
	
	/**
	* M�todo que busca os locais de armazenagem ativos
	*
	* @param q
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAtivoAutocomplete(String q) {
		return findLocalarmazenagemAutocomplete(q, true, null);
	}
	
	/**
	* M�todo que busca os locais de armazenagem de acordo com a empresa do usu�rio
	*
	* @param q
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemEmpresaUsuarioAutocomplete(String q) {
		return findLocalarmazenagemAutocomplete(q, null, new SinedUtil().getListaEmpresa());
	}
	
	/**
	* M�todo que busca os locais de armazenagem de acordo com a empresa da tela ou a empresa do usu�rio
	*
	* @param q
	* @return
	* @since 24/11/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemUsuarioAutocomplete(String q) {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {}
		
		if(StringUtils.isBlank(cdempresa)){
			cdempresa = new SinedUtil().getListaEmpresa();
		}
		return findLocalarmazenagemAutocomplete(q, null, cdempresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#findLocalarmazenagemAutocomplete(String q, Boolean ativo, String whereInEmpresa)
	*
	* @param q
	* @return
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAtivoEmpresaUsuarioAutocomplete(String q) {
		String cdempresa = null;
		try {
			cdempresa = NeoWeb.getRequestContext().getParameter("empresa");
		} catch (Exception e) {}
		
		if(StringUtils.isBlank(cdempresa)){
			cdempresa = new SinedUtil().getListaEmpresa();
		}
		return findLocalarmazenagemAutocomplete(q, true, cdempresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#findLocalarmazenagemAutocomplete(String q, Boolean ativo, String whereInEmpresa)
	*
	* @param q
	* @param ativo
	* @param whereInEmpresa
	* @return
	* @since 28/08/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findLocalarmazenagemAutocomplete(String q, Boolean ativo, String whereInEmpresa) {
		return localarmazenagemDAO.findLocalarmazenagemAutocomplete(q, ativo, whereInEmpresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#getPermitirestoquenegativo(Localarmazenagem localarmazenagem)
	*
	* @param localarmazenagem
	* @return
	* @since 22/06/2015
	* @author Luiz Fernando
	*/
	public boolean getPermitirestoquenegativo(Localarmazenagem localarmazenagem) {
		return localarmazenagemDAO.getPermitirestoquenegativo(localarmazenagem);
	}

	/**
	 * Preenche a lista de Localarmazenagemempresa do bean de material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#adicionaEmpresa
	 * @param request
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	@SuppressWarnings("unchecked")
	public void preencheLocalarmazenagemEmpresa(WebRequestContext request, Localarmazenagem bean) {
		Set<Localarmazenagemempresa> listaLocalempresa = new ListSet<Localarmazenagemempresa>(Localarmazenagemempresa.class);
		
		List<Empresa> listaEmpresa = bean.getListaEmpresa();
		this.adicionaEmpresa(listaLocalempresa, listaEmpresa);
		
		listaEmpresa = (List<Empresa>) request.getSession().getAttribute("listaEmpresa");
		if (listaEmpresa != null) {
			this.adicionaEmpresa(listaLocalempresa, listaEmpresa);
		}
		
		bean.setListaempresa(listaLocalempresa);
	}

	/**
	 * Prrenche a lista de Localarmazenagemempresa a partir de uma lista de Empresa.
	 * 
	 * @param listaLocalempresa
	 * @param listaEmpresa
	 * @author Tom�s Rabelo
	 */
	private void adicionaEmpresa(Set<Localarmazenagemempresa> listaLocalempresa, List<Empresa> listaEmpresa) {
		Localarmazenagemempresa localarmazenagemempresa;
		for (Empresa empresa : listaEmpresa) {
			localarmazenagemempresa = new Localarmazenagemempresa();
			localarmazenagemempresa.setEmpresa(empresa);
			listaLocalempresa.add(localarmazenagemempresa);
		}
	}
	
	/**
	 * Preenche a lista de empresas do bean de material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MaterialempresaService#findByMaterial
	 * @param form
	 * @param findAtivos
	 * @param lista
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Empresa> preencheListaEmpresa(Localarmazenagem form, List<Empresa> findAtivos, List<Empresa> lista) {
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		List<Localarmazenagemempresa> listaLocalarmazenagemempresa =  localarmazenagemempresaService.findByLocalarmazenagem(form);
		
		for (Localarmazenagemempresa localempresa : listaLocalarmazenagemempresa) {
			if (!findAtivos.contains(localempresa.getEmpresa())) {
				listaEmpresa.add(localempresa.getEmpresa());
			}
			lista.add(localempresa.getEmpresa());
		}
		return listaEmpresa;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#findAtivosForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Localarmazenagem> findAtivosForFlex(){
		return localarmazenagemDAO.findAtivosForFlex();
	
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#loadWithEndereco
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Localarmazenagem> loadWithEndereco(String whereIn) {
		return localarmazenagemDAO.loadWithEndereco(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @param empresa
	 * @return
	 * @author Marden Silva
	 */	
	public List<Localarmazenagem> loadForVenda(Empresa empresa) {
		return localarmazenagemDAO.loadForVenda(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#loadForEntrega(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Localarmazenagem> loadForEntrega(Empresa empresa) {
		return localarmazenagemDAO.loadForEntrega(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#findAtivosByEmpresa(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Localarmazenagem> findAtivosByEmpresa(Empresa empresa) {
		return localarmazenagemDAO.findAtivosByEmpresa(empresa);
	}
	
	public List<Localarmazenagem> findAtivos() {
		return findAtivosByEmpresa(null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* Carregar o municipio e uf do localarmazenagem
	* 
	* @see	br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#carregaUfMunicipio(Localarmazenagem localarmazenagem)
	*
	* @param localarmazenagem
	* @return
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public Localarmazenagem carregaUfMunicipio(Localarmazenagem localarmazenagem) {
		return localarmazenagemDAO.carregaUfMunicipio(localarmazenagem);
	}
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Luiz Fernando
	 * @date 03/09/2012
	 * 
	 */
	public List<LocalarmazenagemOfflineJSON> findForPVOffline() {
		List<LocalarmazenagemOfflineJSON> lista = new ArrayList<LocalarmazenagemOfflineJSON>();
		for(Localarmazenagem la : localarmazenagemDAO.findForPVOffline())
			lista.add(new LocalarmazenagemOfflineJSON(la));
		
		return lista;
	}

	public Localarmazenagem loadByNome(String nome) {
		List<Localarmazenagem> listaLocalarmazenagem = this.findByNome(nome);
		if(listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)	
			return listaLocalarmazenagem.get(0);
		else 
			return null;
	}
	
	private List<Localarmazenagem> findByNome(String nome) {
		return localarmazenagemDAO.findByNome(nome);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#loadByEmpresaAndClienteLocacao(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/10/2013
	 */
	public Localarmazenagem loadByEmpresaAndClienteLocacao(Empresa empresa) {
		return localarmazenagemDAO.loadByEmpresaAndClienteLocacao(empresa);
	}
	
	/**
	 *
	 * @param empresa
	 * @param clienteLocacao
	 * @param principal
	 * @param indenizacao
	 * @author Thiago Clemente
	 * 
	 */
	public Localarmazenagem loadForRomaneioFromContrato(Empresa empresa, Boolean clienteLocacao, Boolean principal, Boolean indenizacao) {
		return localarmazenagemDAO.loadForRomaneioFromContrato(empresa, clienteLocacao, principal, indenizacao);
	}
	
	/**
	 * 
	 * @param cdlocalarmazenagemExcecao
	 * @param empresa
	 * @param principal
	 * @param indenizacao
	 * @author Thiago Clemente
	 * 
	 */
	public boolean verificarPrincipalIndenizacao(Integer cdlocalarmazenagemExcecao, Empresa empresa, Boolean principal, Boolean indenizacao){
		return localarmazenagemDAO.verificarPrincipalIndenizacao(cdlocalarmazenagemExcecao, empresa, principal, indenizacao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#loadWithInformacoesEstoque(Localarmazenagem localarmazenagem)
	*
	* @param localarmazenagem
	* @return
	* @since 04/08/2014
	* @author Luiz Fernando
	*/
	public Localarmazenagem loadWithInformacoesEstoque(Localarmazenagem localarmazenagem){
		return localarmazenagemDAO.loadWithInformacoesEstoque(localarmazenagem);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInEmpresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/02/2015
	 */
	public List<Localarmazenagem> findByEmpresa(Localarmazenagem localarmazenagem, String whereInEmpresa) {
		return localarmazenagemDAO.findByEmpresa(localarmazenagem, whereInEmpresa);
	}
	
	/**
	* M�todo que retorna os locais que o usu�rio tem permiss�o de acordo com a empresa
	*
	* @return
	* @since 24/02/2015
	* @author Luiz Fernando
	*/
	public List<Localarmazenagem> findByUsuarioLogado() {
		String whereInEmpresaUsuarioLogado = new SinedUtil().getListaEmpresa();
		return findByEmpresa(null, whereInEmpresaUsuarioLogado);
	}
	
	/**
	* M�todo que retorna um whereIn dos locais que o usu�rio tem permiss�o de acordo com a empresa
	*
	* @return
	* @since 25/02/2015
	* @author Luiz Fernando
	*/
	public String getWhereInLocalRestricaoUsuarioLogado(){
		String whereIn = null;
		List<Localarmazenagem> lista = findByUsuarioLogado();
		if(SinedUtil.isListNotEmpty(lista)){
			whereIn = CollectionsUtil.listAndConcatenate(lista, "cdlocalarmazenagem", ",");
		}
		return whereIn;
	}
	
	/**
	* @see br.com.linkcom.sined.geral.service.LocalarmazenagemService#findForAndroid(String whereIn, boolean sincronizacaoInicial)
	*
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<LocalarmazenagemRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#findForAndroid(String whereIn)
	*
	* @param whereIn
	* @param sincronizacaoInicial
	* @return
	* @since 10/06/2016
	* @author Luiz Fernando
	*/
	public List<LocalarmazenagemRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<LocalarmazenagemRESTModel> lista = new ArrayList<LocalarmazenagemRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Localarmazenagem bean : localarmazenagemDAO.findForAndroid(whereIn))
				lista.add(new LocalarmazenagemRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	* M�todo ajax para carregar o municipio e uf do localarmazenagem
	*
	* @param request
	* @param localarmazenagem
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxCarregaUfMunicipio(WebRequestContext request, Localarmazenagem localarmazenagem){
		boolean sucesso = false;
		StringBuilder s = new StringBuilder();
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		if(localarmazenagem.getCdlocalarmazenagem() != null){
			localarmazenagem = carregaUfMunicipio(localarmazenagem);
			if(localarmazenagem != null && localarmazenagem.getEndereco() != null && localarmazenagem.getEndereco().getMunicipio() != null && 
						localarmazenagem.getEndereco().getMunicipio().getCdmunicipio() != null &&
						localarmazenagem.getEndereco().getMunicipio().getUf() != null &&
						localarmazenagem.getEndereco().getMunicipio().getUf().getCduf() != null){
				s.append("var uf = '" + localarmazenagem.getEndereco().getMunicipio().getUf().getCduf() + "';");
				s.append("var municipio = '" + localarmazenagem.getEndereco().getMunicipio().getCdmunicipio() + "';");
				
				String logradouroCompletoComBairroSemMunicipio = localarmazenagem.getEndereco().getLogradouroCompletoComBairroSemMunicipio();
				if(logradouroCompletoComBairroSemMunicipio != null && logradouroCompletoComBairroSemMunicipio.length() > 200){
					logradouroCompletoComBairroSemMunicipio = logradouroCompletoComBairroSemMunicipio.substring(0, 199);
				}
				s.append("var logradouroCompletoComBairroSemMunicipio = '" + logradouroCompletoComBairroSemMunicipio + "';");
				
				List<Municipio> listaMunicipio = municipioService.find(localarmazenagem.getEndereco().getMunicipio().getUf());
				if(listaMunicipio != null){
					s.append(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipio", ""));
				}
				sucesso = true;
			}
		}
		
		s.append("var sucesso = " + sucesso + ";");		
		view.println(s.toString());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#existePedidoOuVendaPrevista(Localarmazenagem localarmazenagem)
	*
	* @param localarmazenagem
	* @return
	* @since 18/11/2016
	* @author Luiz Fernando
	*/
	public boolean existePedidoOuVendaPrevista(Localarmazenagem localarmazenagem) {
		return localarmazenagemDAO.existePedidoOuVendaPrevista(localarmazenagem);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LocalarmazenagemDAO#isLocalEmpresa(Localarmazenagem localarmazenagem, Empresa empresa)
	*
	* @param localarmazenagem
	* @param empresa
	* @return
	* @since 10/04/2017
	* @author Luiz Fernando
	*/
	public boolean isLocalEmpresa(Localarmazenagem localarmazenagem, Empresa empresa) {
		return localarmazenagemDAO.isLocalEmpresa(localarmazenagem, empresa);
	}
	
	public Localarmazenagem loadPrincipalByEmpresa(Empresa empresa) {
		Localarmazenagem localPrincipal = localarmazenagemDAO.loadPrincipalByEmpresa(empresa);
		if(localPrincipal == null){
			localPrincipal = this.loadPrincipalSemEmpresa();
		}
		return localPrincipal;
	}
	
	public Localarmazenagem loadPrincipalSemEmpresa() {
		return localarmazenagemDAO.loadPrincipalSemEmpresa();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param localarmazenagem
	 * @return
	 * @since 14/10/2019
	 * @author Rodrigo Freitas
	 */
	public Localarmazenagem loadWithCentrocusto(Localarmazenagem localarmazenagem) {
		return localarmazenagemDAO.loadWithCentrocusto(localarmazenagem);
	}
	
	public List<Localarmazenagem> findLocalarmazenagemAtivoEmpresaAutocompleteWSVenda(String q, String cdEmpresa) {
		return findLocalarmazenagemAutocomplete(q, true, cdEmpresa);
	}
}