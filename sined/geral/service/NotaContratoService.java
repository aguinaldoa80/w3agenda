package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaContrato;
import br.com.linkcom.sined.geral.dao.NotaContratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotaContratoService extends GenericService<NotaContrato> {
	
	private NotaContratoDAO notaContratoDAO;
	
	public void setNotaContratoDAO(NotaContratoDAO notaContratoDAO) {
		this.notaContratoDAO = notaContratoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotaContratoDAO#findByNota
	 *
	 * @param nota
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<NotaContrato> findByNota(Nota nota){
		return notaContratoDAO.findByNota(nota);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.NotaContratoDAO#findForCobranca(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<NotaContrato> findForCobranca(String whereIn) {
		return notaContratoDAO.findForCobranca(whereIn);
	}
	
	public List<NotaContrato> findForRateioCobranca(String whereIn) {
		return notaContratoDAO.findForRateioCobranca(whereIn);
	}

}
