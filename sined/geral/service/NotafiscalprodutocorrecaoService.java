package br.com.linkcom.sined.geral.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutocorrecao;
import br.com.linkcom.sined.geral.bean.enumeration.Notafiscalprodutocorrecaosituacao;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutocorrecaoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.LkNfeUtil;

public class NotafiscalprodutocorrecaoService extends GenericService<Notafiscalprodutocorrecao> {

	private NotafiscalprodutocorrecaoDAO notafiscalprodutocorrecaoDAO;
	private ArquivoService arquivoService;
	private NotafiscalprodutoService notafiscalprodutoService;
	
	public void setNotafiscalprodutoService(
			NotafiscalprodutoService notafiscalprodutoService) {
		this.notafiscalprodutoService = notafiscalprodutoService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setNotafiscalprodutocorrecaoDAO(
			NotafiscalprodutocorrecaoDAO notafiscalprodutocorrecaoDAO) {
		this.notafiscalprodutocorrecaoDAO = notafiscalprodutocorrecaoDAO;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutocorrecaoDAO#findByNotafiscalproduto(Notafiscalproduto nf)
	 *
	 * @param nf
	 * @return
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalprodutocorrecao> findByNotafiscalproduto(Notafiscalproduto nf) {
		return notafiscalprodutocorrecaoDAO.findByNotafiscalproduto(nf);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutocorrecaoDAO#marcarFlag(String whereInCartacorrecao)
	 *
	 * @param whereInCartacorrecao
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public void marcarFlag(String whereInCartacorrecao) {
		notafiscalprodutocorrecaoDAO.marcarFlag(whereInCartacorrecao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutocorrecaoDAO#findNovasCorrecoes(Empresa empresa)
	 *
	 * @param empresa
	 * @return
	 * @since 17/09/2012
	 * @author Rodrigo Freitas
	 */
	public List<Notafiscalprodutocorrecao> findNovasCorrecoes(Empresa empresa) {
		return notafiscalprodutocorrecaoDAO.findNovasCorrecoes(empresa);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.NotafiscalprodutocorrecaoDAO#updateArquivoretorno(Notafiscalprodutocorrecao notafiscalprodutocorrecao)
	 *
	 * @param notafiscalprodutocorrecao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoretorno(Notafiscalprodutocorrecao notafiscalprodutocorrecao) {
		notafiscalprodutocorrecaoDAO.updateArquivoretorno(notafiscalprodutocorrecao);
	}

	/**
	 * Processa o retorno do webservice da receita na carta de corre��o.
	 *
	 * @param notafiscalprodutocorrecao
	 * @param content
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	public void processaRetornoCartacorrecaoSefaz(Notafiscalprodutocorrecao notafiscalprodutocorrecao, byte[] content) {
		try {
			String string = new String(content, "UTF-8");
			string = Util.strings.tiraAcento(string);
			
			SAXBuilder sb = new SAXBuilder();  
			Document d = sb.build(new ByteArrayInputStream(string.getBytes()));
			Element rootElement = d.getRootElement();
			
			Element cStatElt = SinedUtil.getChildElement("cStat", rootElement.getContent());
			if(cStatElt == null) throw new SinedException("Erro no processamento do retorno.");
			
			String cStatPrincipal = cStatElt.getText();
			if(cStatPrincipal.equals("128")){ // Lote de Evento Processado 
				
				Element retEventoElt = SinedUtil.getChildElement("retEvento", rootElement.getContent());
				if(retEventoElt == null) throw new SinedException("Erro no processamento do retorno.");
				
				Element infEventoElt = SinedUtil.getChildElement("infEvento", retEventoElt.getContent());
				if(infEventoElt == null) throw new SinedException("Erro no processamento do retorno.");
				
				Element cStatElt2 = SinedUtil.getChildElement("cStat", infEventoElt.getContent());
				if(cStatElt2 == null) throw new SinedException("Erro no processamento do retorno.");
				
				String cStat = cStatElt2.getText();
				
				if(cStat.equals("135") || cStat.equals("136")){
					this.updateSituacao(notafiscalprodutocorrecao, Notafiscalprodutocorrecaosituacao.PROCESSADA_COM_SUCESSO);
				} else {
					Element xMotivoElt = SinedUtil.getChildElement("xMotivo", infEventoElt.getContent());
					String motivo = xMotivoElt.getText();
					
					this.updateSituacao(notafiscalprodutocorrecao, Notafiscalprodutocorrecaosituacao.PROCESSADA_COM_ERRO);
					this.updateMotivo(notafiscalprodutocorrecao, motivo);
				}
			} else {
				Element xMotivoElt = SinedUtil.getChildElement("xMotivo", rootElement.getContent());
				String motivo = xMotivoElt.getText();
				
				this.updateSituacao(notafiscalprodutocorrecao, Notafiscalprodutocorrecaosituacao.PROCESSADA_COM_ERRO);
				this.updateMotivo(notafiscalprodutocorrecao, motivo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param notafiscalprodutocorrecao
	 * @param motivo
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	private void updateMotivo(Notafiscalprodutocorrecao notafiscalprodutocorrecao, String motivo) {
		notafiscalprodutocorrecaoDAO.updateMotivo(notafiscalprodutocorrecao, motivo);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param notafiscalprodutocorrecao
	 * @param situacao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	private void updateSituacao(Notafiscalprodutocorrecao notafiscalprodutocorrecao, Notafiscalprodutocorrecaosituacao situacao) {
		notafiscalprodutocorrecaoDAO.updateSituacao(notafiscalprodutocorrecao, situacao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param notafiscalprodutocorrecao
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public void updateArquivoxml(Notafiscalprodutocorrecao notafiscalprodutocorrecao) {
		notafiscalprodutocorrecaoDAO.updateArquivoxml(notafiscalprodutocorrecao);
	}

	@SuppressWarnings("unchecked")
	public IReport criarRelatorioCartacorrecao(Notafiscalprodutocorrecao notafiscalprodutocorrecao) throws JDOMException, IOException {
		
		notafiscalprodutocorrecao = this.loadForEntrada(notafiscalprodutocorrecao);
		
		String nome_cliente = "";
		String cpf_cnpj_cliente = "";
		String numero_nota = "";
		Notafiscalproduto notafiscalproduto = notafiscalprodutocorrecao.getNotafiscalproduto();
		if(notafiscalproduto != null){
			numero_nota = notafiscalproduto.getNumero();
			notafiscalproduto = notafiscalprodutoService.loadForEntrada(notafiscalproduto);
			if(notafiscalproduto.getCliente() != null){
				nome_cliente = notafiscalproduto.getCliente().getRazaoOrNomeByTipopessoa();
				cpf_cnpj_cliente = notafiscalproduto.getCliente().getCpfOuCnpj();
			}
		}
		
		Arquivo arquivoenvio = notafiscalprodutocorrecao.getArquivoenvio();		
		arquivoenvio = arquivoService.loadWithContents(arquivoenvio);
		
		SAXBuilder sb = new SAXBuilder();  
		Document doc = sb.build(new ByteArrayInputStream(arquivoenvio.getContent()));
		
		Element rootElement = doc.getRootElement();
		
		Element eventoElement = SinedUtil.getChildElement("evento", rootElement.getContent());
		
		Attribute versaoPrincipalAttr = eventoElement.getAttribute("versao");
		
		Element infEventoElement = SinedUtil.getChildElement("infEvento", eventoElement.getContent());
		
		Attribute idAttr = infEventoElement.getAttribute("Id");
		
		Element cOrgaoElement = SinedUtil.getChildElement("cOrgao", infEventoElement.getContent());
		Element tpAmbElement = SinedUtil.getChildElement("tpAmb", infEventoElement.getContent());
		Element cNPJElement = SinedUtil.getChildElement("CNPJ", infEventoElement.getContent());
		Element chNFeElement = SinedUtil.getChildElement("chNFe", infEventoElement.getContent());
		Element dhEventoElement = SinedUtil.getChildElement("dhEvento", infEventoElement.getContent());
		Element tpEventoElement = SinedUtil.getChildElement("tpEvento", infEventoElement.getContent());
		Element nSeqEventoElement = SinedUtil.getChildElement("nSeqEvento", infEventoElement.getContent());
		Element verEventoElement = SinedUtil.getChildElement("verEvento", infEventoElement.getContent());
		
		Element detEventoElement = SinedUtil.getChildElement("detEvento", infEventoElement.getContent());
		
		Attribute versaoCartaCorrecaoAttr = detEventoElement.getAttribute("versao");
		
		Element descEventoElement = SinedUtil.getChildElement("descEvento", detEventoElement.getContent());
		Element xCorrecaoElement = SinedUtil.getChildElement("xCorrecao", detEventoElement.getContent());
		Element xCondUsoElement = SinedUtil.getChildElement("xCondUso", detEventoElement.getContent());
		
		String versao_principal = versaoPrincipalAttr != null ? versaoPrincipalAttr.getValue() : "";
		String versao_evento = verEventoElement != null ? verEventoElement.getText() : "";
		String versao_cartacorrecao = versaoCartaCorrecaoAttr != null ? versaoCartaCorrecaoAttr.getValue() : "";
		String id = idAttr != null ? idAttr.getValue() : "";
		String orgao = cOrgaoElement != null ? cOrgaoElement.getText() : "";
		String ambiente = tpAmbElement != null ? tpAmbElement.getText() : "";
		String cnpj_cpf = cNPJElement != null ? cNPJElement.getText() : "";
		String chave_acesso = chNFeElement != null ? chNFeElement.getText() : "";
		String data_evento = dhEventoElement != null ? dhEventoElement.getText() : "";
		String codigo_evento = tpEventoElement != null ? tpEventoElement.getText() : "";
		String sequencial_evento = nSeqEventoElement != null ? nSeqEventoElement.getText() : "";
		String descricao_evento = descEventoElement != null ? descEventoElement.getText() : "";
		String texto = xCorrecaoElement != null ? xCorrecaoElement.getText() : "";
		String condicoes_uso = xCondUsoElement != null ? xCondUsoElement.getText() : "";
		
		if(data_evento != null && !data_evento.equals("")){
			try{
				data_evento = data_evento.substring(0, data_evento.length() - 6);
				Date date_evento = LkNfeUtil.FORMATADOR_DATA_HORA.parse(data_evento);
				data_evento = SinedDateUtils.toString(new java.sql.Date(date_evento.getTime()), "dd/MM/yyyy HH:mm:ss");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Report report = new Report("/faturamento/cartacorrecao");
		
		report.addParameter("VERSAO_PRINCIPAL", versao_principal);
		report.addParameter("VERSAO_EVENTO", versao_evento);
		report.addParameter("VERSAO_CARTACORRECAO", versao_cartacorrecao);
		report.addParameter("ID", id);
		report.addParameter("ORGAO", orgao);
		report.addParameter("AMBIENTE", ambiente);
		try{
			report.addParameter("CNPJ_CPF", new Cnpj(cnpj_cpf).toString());
		} catch (Exception e) {
			report.addParameter("CNPJ_CPF", cnpj_cpf);
		}
		report.addParameter("CHAVE_ACESSO", chave_acesso);
		report.addParameter("DATA_EVENTO", data_evento);
		report.addParameter("CODIGO_EVENTO", codigo_evento);
		report.addParameter("SEQUENCIAL_EVENTO", sequencial_evento);
		report.addParameter("DESCRICAO_EVENTO", descricao_evento);
		report.addParameter("TEXTO", texto);
		report.addParameter("CONDICOES_USO", condicoes_uso);
		report.addParameter("NOME_CLIENTE", nome_cliente);
		report.addParameter("CPF_CNPJ_CLIENTE", cpf_cnpj_cliente);
		report.addParameter("NUMERO_NOTA", numero_nota);
		
		if (ParametrogeralService.getInstance().getBoolean("EXIBIR_LOGO_LINKCOM")) {
			report.addParameter("LOGO_LINKCOM", SinedUtil.getLogoLinkCom());
		} else {
			report.addParameter("LOGO_LINKCOM", null);
		}
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param bean
	 * @return
	 * @since 18/09/2012
	 * @author Rodrigo Freitas
	 */
	public Notafiscalprodutocorrecao loadWithEmpresa(Notafiscalprodutocorrecao bean) {
		return notafiscalprodutocorrecaoDAO.loadWithEmpresa(bean);
	}
	
	public Long getProximoContadorCartaCorrecao(Integer cdnotafiscalproduto) {
		return notafiscalprodutocorrecaoDAO.getProximoContadorCartaCorrecao(cdnotafiscalproduto);
	}
	
	/**
	 * M�todo que verifica se a nota possui
	 * carta de corre��o
	 * @param cdNota
	 * @return
	 */
	public Boolean haveCartaCorrecao(Integer cdNota) {
		return notafiscalprodutocorrecaoDAO.haveCartaCorrecao(cdNota);
	}

}
