package br.com.linkcom.sined.geral.service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.axis2.AxisFault;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.bean.Materiallegendamaterialtipo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialsincronizacao;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterialunidademedidasincronizacao;
import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.WmsBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.wms.WmsWebServiceStub;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarClasseProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarCliente;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarEnderecoEntrega;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVenda;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarPedidoVendaProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarProdutoembalagem;
import br.com.linkcom.sined.wms.WmsWebServiceStub.CadastrarTransportador;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ClasseProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.ConfirmarExclusaoItemPedidoResponse;
import br.com.linkcom.sined.wms.WmsWebServiceStub.DesmarcarItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.DesmarcarItemPedidoResponse;
import br.com.linkcom.sined.wms.WmsWebServiceStub.EnderecoEntrega;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedido;
import br.com.linkcom.sined.wms.WmsWebServiceStub.MarcarItemPedidoResponse;
import br.com.linkcom.sined.wms.WmsWebServiceStub.PedidoVenda;
import br.com.linkcom.sined.wms.WmsWebServiceStub.PedidoVendaProduto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.Produto;
import br.com.linkcom.sined.wms.WmsWebServiceStub.Produtoembalagem;

public class PedidovendasincronizacaoService extends GenericService<Pedidovendasincronizacao> {
	
	private PedidovendasincronizacaoDAO pedidovendasincronizacaoDAO;
	private MaterialService materialService;
	private ClienteService clienteService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private PedidovendaService pedidovendaService;
	private PedidovendamaterialService pedidovendamaterialService;
	private PedidovendasincronizacaohistoricoService pedidovendasincronizacaohistoricoService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private MaterialunidademedidaService materialunidademedidaService;
	private FornecedorService fornecedorService;
	private MateriallegendaService materiallegendaService;
	
	public void setPedidovendasincronizacaoDAO(PedidovendasincronizacaoDAO pedidovendasincronizacaoDAO) {
		this.pedidovendasincronizacaoDAO = pedidovendasincronizacaoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setPedidovendasincronizacaohistoricoService(PedidovendasincronizacaohistoricoService pedidovendasincronizacaohistoricoService) {
		this.pedidovendasincronizacaohistoricoService = pedidovendasincronizacaohistoricoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {
		this.materialunidademedidaService = materialunidademedidaService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setMateriallegendaService(MateriallegendaService materiallegendaService) {
		this.materiallegendaService = materiallegendaService;
	}

	/* singleton */
	private static PedidovendasincronizacaoService instance;
	public static PedidovendasincronizacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PedidovendasincronizacaoService.class);
		}
		return instance;
	}
	
	
	/**
	 * M�todo que faz a sincroniza��o dos pedidos de vendas para o wms
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendasincronizacaoServic#findForSincronizacaoPedidoVendaPendente()
	 *
	 * @author Luiz Fernando
	 */
	public void sincronizarPedidovenda(){
		String contadorenviostr = parametrogeralService.getValorPorNome(Parametrogeral.CONTADORENVIO_PEDIDOVENDASINCRONIZACAO);
		Integer contadorenvio = null;
		if(contadorenviostr != null && !"".equals(contadorenviostr)){
			try {
				contadorenvio = Integer.parseInt(contadorenviostr);				
			} catch (Exception e) {
				SinedUtil.enviaEmailErroSincronizacaoWMS("pedido de venda", 0, e);
			}
		}
		if(contadorenvio != null && this.existPedidovendaNaoSincronizado(contadorenvio)){
			try {
				List<Pedidovendasincronizacao> lista = findForSincronizacaoPedidoVendaPendente(contadorenvio);
				if(lista != null && !lista.isEmpty()){
					Map<Integer, Integer> mapaCdarquivos = null;
					try {
						String param = parametrogeralService.buscaValorPorNome("GERAR_LEGENDAS_CONCATENADAS");
						if(param != null && param.equalsIgnoreCase("true")){
							mapaCdarquivos = generateImagensLegendas(lista, null, 20, 20);
						}
					} catch (Exception e) {
						e.printStackTrace();
						SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de gera��o de legendas)", 0, e);
					}
					for(Pedidovendasincronizacao pedidovendasincronizacao : lista){
						WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(pedidovendasincronizacao.getPedidovenda().getEmpresa()) + "/webservices/WmsWebService");
						updatePedidovendasincronizacaoTentativaDeSincronizacao(pedidovendasincronizacao);
						criarEnviarInfPedidoVendaWms(pedidovendasincronizacao, mapaCdarquivos, stub);
					}
				}
				
			} catch (AxisFault e) {
				e.printStackTrace();
				SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de busca de 'Pedidovendasincronizacao')", 0, e);
			} catch (RemoteException e) {
				e.printStackTrace();
				SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de busca de 'Pedidovendasincronizacao')", 0, e);
			}
		}
		
		if(this.existPedidovendaDesmarcaritens()){
			List<Pedidovendasincronizacao> lista = findForDesmarcaritensPedidoVenda();
				
			for(Pedidovendasincronizacao pedidovendasincronizacao : lista){
				if(pedidovendasincronizacao.getPedidovenda() != null && pedidovendasincronizacao.getPedidovenda().getCdpedidovenda() != null){
					this.desmarcarItensPedidovenda(pedidovendasincronizacao.getPedidovenda());
				}
			}
		}
	}
	
	private Map<Integer, Integer> generateImagensLegendas(List<Pedidovendasincronizacao> lista, Material material, Integer defaultWidth, Integer defaultHeight) throws Exception{		
		Map<Integer, Integer> mapa = new HashMap<Integer, Integer>();
		Map<Integer, List<Arquivo>> mapaArquivos = new HashMap<Integer, List<Arquivo>>();
		Set<Materialtipo> listaMT = new ListSet<Materialtipo>(Materialtipo.class);
		if(SinedUtil.isListNotEmpty(lista)){
			for(Pedidovendasincronizacao pvs : lista){
				if(pvs.getListaPedidovendamaterialsincronizacao() != null){
					for(Pedidovendamaterialsincronizacao pvms : pvs.getListaPedidovendamaterialsincronizacao()){
						if(pvms.getMaterial() != null && pvms.getMaterial().getMaterialtipo() != null
								&& pvms.getMaterial().getMaterialtipo().getCdmaterialtipo() != null
								&& !mapa.containsKey(pvms.getMaterial().getMaterialtipo().getCdmaterialtipo())){
							listaMT.add(pvms.getMaterial().getMaterialtipo());
						}
					}
				}
			}
		}else if(material != null && material.getMaterialtipo() != null && material.getMaterialtipo().getCdmaterialtipo() != null){
			listaMT.add(material.getMaterialtipo());
		}
		
		if(!listaMT.isEmpty()){
			String whereIn = SinedUtil.listAndConcatenate(listaMT, "cdmaterialtipo", ",");
			List<Materiallegenda> listaML = materiallegendaService.findByMaterialtipo(whereIn);
			if(listaML != null && !listaML.isEmpty()){
				for(Materiallegenda ml : listaML){
					if(ml.getSimbolo() != null && ml.getSimbolo().getCdarquivo() != null && ml.getListaMateriallegendamaterialtipo() != null){
						for(Materiallegendamaterialtipo mlmt : ml.getListaMateriallegendamaterialtipo()){
							if(!mapaArquivos.containsKey(mlmt.getMaterialtipo().getCdmaterialtipo())){
								mapaArquivos.put(mlmt.getMaterialtipo().getCdmaterialtipo(), new ListSet<Arquivo>(Arquivo.class));
							}
							mapaArquivos.get(mlmt.getMaterialtipo().getCdmaterialtipo()).add(ml.getSimbolo());
						}
					}
				}
			}
			mapa = SinedUtil.generateImagensByMaterialtipo(mapaArquivos, defaultWidth, defaultHeight);
		}
		return mapa;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#findForDesmarcaritensPedidoVenda()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	private List<Pedidovendasincronizacao> findForDesmarcaritensPedidoVenda() {
		return pedidovendasincronizacaoDAO.findForDesmarcaritensPedidoVenda();
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#existPedidovendaDesmarcaritens()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	private Boolean existPedidovendaDesmarcaritens() {
		return pedidovendasincronizacaoDAO.existPedidovendaDesmarcaritens();
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#existPedidovendaNaoSincronizado()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existPedidovendaNaoSincronizado(Integer contadorenvio){
		return pedidovendasincronizacaoDAO.existPedidovendaNaoSincronizado(contadorenvio);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#findForSincronizacaoPedidoVendaPendente()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendasincronizacao> findForSincronizacaoPedidoVendaPendente(Integer contadorenvio){
		return pedidovendasincronizacaoDAO.findForSincronizacaoPedidoVendaPendente(contadorenvio);
	}
	
	/**
	 * M�todo que cria e envia as informa��es do pedido de venda para o wms
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendasincronizacaoService#updatePedidovendasincronizacaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao)
	 *
	 * @param pedidovendasincronizacao
	 * @param stub
	 * @throws RemoteException
	 */
	public void criarEnviarInfPedidoVendaWms(Pedidovendasincronizacao pedidovendasincronizacao, Map<Integer, Integer> mapaCdarquivo, WmsWebServiceStub stub) throws RemoteException{		

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		Boolean erro = Boolean.FALSE;
		List<Pedidovendamaterialsincronizacao> listaItensSincronizados = new ArrayList<Pedidovendamaterialsincronizacao>();
		try {	
		
//		**********************************Cadastra o Cliente****************************************
			br.com.linkcom.sined.wms.WmsWebServiceStub.Cliente cliente = new br.com.linkcom.sined.wms.WmsWebServiceStub.Cliente();
			if (pedidovendasincronizacao.getCliente() != null){
				cliente.setId(pedidovendasincronizacao.getClienteId());
				cliente.setNome(pedidovendasincronizacao.getClienteNome());
				cliente.setNatureza(pedidovendasincronizacao.getClienteNatureza());
				cliente.setDocumento(StringUtils.soNumero(pedidovendasincronizacao.getClienteDocumento()));
				cliente.setFilial(pedidovendasincronizacao.getClienteFilial());
				cliente.setEndereco(pedidovendasincronizacao.getClienteEndereco());
				
				try {
					CadastrarCliente cadastrarCliente = new CadastrarCliente();
					cadastrarCliente.setIn0(cliente);
					stub.cadastrarCliente(cadastrarCliente);
					
				} catch (AxisFault e) {
//					e.printStackTrace();
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar cliente: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (RemoteException e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar cliente: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (Exception e){
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar cliente: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				}
			}
		
//		*******************************************************************************************
//		**********************************Cadastra a Filial****************************************
			if (!erro && pedidovendasincronizacao.getEmpresa() != null){
				br.com.linkcom.sined.wms.WmsWebServiceStub.Cliente filialWms = new br.com.linkcom.sined.wms.WmsWebServiceStub.Cliente();
				filialWms.setId(pedidovendasincronizacao.getFilialwmsId());
				filialWms.setNome(pedidovendasincronizacao.getFilialwmsNome());
				filialWms.setNatureza(pedidovendasincronizacao.getFilialwmsNatureza());
				filialWms.setDocumento(StringUtils.soNumero(pedidovendasincronizacao.getFilialwmsDocumento()));
				filialWms.setFilial(pedidovendasincronizacao.getFilialwmsFilial());
				
				try {
					CadastrarCliente cadastrarFilial = new CadastrarCliente();
					cadastrarFilial.setIn0(filialWms);
					stub.cadastrarCliente(cadastrarFilial);
				} catch (AxisFault e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar filial: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (RemoteException e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar filial: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (Exception e){
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar filial: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				}
				
//			*******************************************************************************************
//			**********************************Cadastra o Transportador*********************************
				if (!erro && pedidovendasincronizacao.getTransportador() != null ){
					br.com.linkcom.sined.wms.WmsWebServiceStub.Transportador transportador = 
						new br.com.linkcom.sined.wms.WmsWebServiceStub.Transportador();
					
					transportador.setDeposito(filialWms.getDocumento());
					transportador.setTransportadorId(pedidovendasincronizacao.getTransportadorId());
					transportador.setTransportadorNome(pedidovendasincronizacao.getTransportadorNome());
					transportador.setTransportadorNatureza(pedidovendasincronizacao.getTransportadorNatureza());
					transportador.setTransportadorCpfCnpj(pedidovendasincronizacao.getTransportadorCpfCnpj());
					
					try {
						CadastrarTransportador cadastrarTransportador = new CadastrarTransportador();
						cadastrarTransportador.setIn0(transportador);
						stub.cadastrarTransportador(cadastrarTransportador);
					} catch (AxisFault e) {
						e.printStackTrace();
						pedidovendasincronizacao.setErro("Erro ao cadastrar o transportador: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					} catch (RemoteException e) {
						e.printStackTrace(pw);
						pedidovendasincronizacao.setErro("Erro ao cadastrar o transportador: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					} catch (Exception e){
						e.printStackTrace(pw);
						pedidovendasincronizacao.setErro("Erro ao cadastrar o transportador: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					}
				}
			}
			
//			*******************************************************************************************
//			**********************************Cadastra o Endere�o**************************************
			EnderecoEntrega endereco = new EnderecoEntrega();
			if (!erro && pedidovendasincronizacao.getEndereco() != null){
				endereco.setId(pedidovendasincronizacao.getEnderecoId());
				endereco.setIdCliente(pedidovendasincronizacao.getEnderecoIdCliente());
				endereco.setLogradouro(pedidovendasincronizacao.getEnderecoLogradouro());
				endereco.setNumero(pedidovendasincronizacao.getEnderecoNumero());
				endereco.setComplemento(pedidovendasincronizacao.getEnderecoComplemento());
				endereco.setBairro(pedidovendasincronizacao.getEnderecoBairro());
				endereco.setCep(pedidovendasincronizacao.getEnderecoCep());
				endereco.setReferencia(pedidovendasincronizacao.getEnderecoPontoreferencia());
				endereco.setMunicipio(pedidovendasincronizacao.getEnderecoMunicipio());
				endereco.setUf(pedidovendasincronizacao.getEnderecoUf());
				
				try {
					CadastrarEnderecoEntrega enderecoEntrega = new CadastrarEnderecoEntrega();
					enderecoEntrega.setIn0(endereco);
					stub.cadastrarEnderecoEntrega(enderecoEntrega);
				} catch (AxisFault e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar endereco: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (RemoteException e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar endereco: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (Exception e){
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar endereco: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString().toString());
					erro = Boolean.TRUE;
				}
			}
		
//		*******************************************************************************************
//		*********************************Cadastra o PedidoVenda************************************
			if(!erro){
				PedidoVenda novoPedidoVenda = new PedidoVenda();
				novoPedidoVenda.setId(pedidovendasincronizacao.getPedidovendaId());
				novoPedidoVenda.setIdFilialVenda(pedidovendasincronizacao.getPedidovendaIdFilialVenda());
				novoPedidoVenda.setNumero(pedidovendasincronizacao.getPedidovendaNumero());
				novoPedidoVenda.setIdCliente(pedidovendasincronizacao.getPedidovendaIdCliente());
				novoPedidoVenda.setDataEmissao(SinedDateUtils.dateToCalendar(pedidovendasincronizacao.getPedidovendaDataEmissao()));
				novoPedidoVenda.setDataLancamento(SinedDateUtils.dateToCalendar(pedidovendasincronizacao.getPedidovendaDataLancamento()));
				novoPedidoVenda.setFlagTroca(pedidovendasincronizacao.getPedidovendaFlagTroca());
				novoPedidoVenda.setIdExterno(pedidovendasincronizacao.getPedidovendaIdExterno());
				novoPedidoVenda.setIdTransportador(pedidovendasincronizacao.getTransportadorId() != null ? pedidovendasincronizacao.getTransportadorId() : 0);
				novoPedidoVenda.setObservacao(pedidovendasincronizacao.getPedidovendaObservacaointerna());
//				if(pedidovendasincronizacao.getPedidovenda() != null && pedidovendasincronizacao.getPedidovenda().getObservacao() != null)
//					novoPedidoVenda.setObservacao(pedidovendasincronizacao.getPedidovenda().getObservacao());
				
				try {
					CadastrarPedidoVenda cadastrarPedidoVenda = new CadastrarPedidoVenda();
					cadastrarPedidoVenda.setIn0(novoPedidoVenda);
					stub.cadastrarPedidoVenda(cadastrarPedidoVenda);
				} catch (AxisFault e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar pedido de venda: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (RemoteException e) {
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar pedido de venda: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				} catch (Exception e){
					e.printStackTrace(pw);
					pedidovendasincronizacao.setErro("Erro ao cadastrar pedido de venda: " + e.getMessage());
					pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
					erro = Boolean.TRUE;
				}
			}
		
//		*******************************************************************************************
//		*****************************Cadastra o PedidoVendaMaterial********************************
			if(!erro){
				String paramMaterialIdentificacaoWMS = parametrogeralService.getValorPorNome(Parametrogeral.ENVIAR_IDENTIFICADOR_MATERIAL_WMS);
				PedidoVendaProduto prod;
				CadastrarPedidoVendaProduto cadastro;
				for (Pedidovendamaterialsincronizacao produto : pedidovendasincronizacao.getListaPedidovendamaterialsincronizacao()) {
					
	//			**************************Material Grupo e Material *****************************					
					WmsBean wmsBean = cadastrarProdutoWMS(produto, mapaCdarquivo, stub, paramMaterialIdentificacaoWMS);
					if(wmsBean.getErro() != null && wmsBean.getErro()){
						pedidovendasincronizacao.setErro(wmsBean.getDescricaoErro());
						pedidovendasincronizacao.setErrolog(wmsBean.getDescricaoErrolog());
						erro = Boolean.TRUE;
					}
				
	//			**************************PedidoVendaProduto**************************
					if(!erro){
						prod = new PedidoVendaProduto();
						
						prod.setId(produto.getPedidovendaprodutoId() != null ? produto.getPedidovendaprodutoId() : null);
						prod.setDeposito(StringUtils.soNumero(produto.getPedidovendaprodutoDeposito()));
						if(produto.getPedidovendaprodutoIdPedidoVenda() != null)
							prod.setIdPedidoVenda(produto.getPedidovendaprodutoIdPedidoVenda());
						else
							prod.setIdPedidoVenda(pedidovendasincronizacao.getPedidovendaId());
						prod.setIdProduto(produto.getPedidovendaprodutoIdProduto());
						prod.setQuantidade(produto.getPedidovendaprodutoQuantidade());
						prod.setPrevisaoEntrega(SinedDateUtils.dateToCalendar(produto.getPedidovendaprodutoPrevisaoEntrega()));
						prod.setTipo(produto.getPedidovendaprodutoTipo());
						if(produto.getPedidovendaprodutoIdEnderecoEntrega() != null)
							prod.setIdEnderecoEntrega(produto.getPedidovendaprodutoIdEnderecoEntrega());
						else
							prod.setIdEnderecoEntrega(pedidovendasincronizacao.getEnderecoId());
						prod.setValorUnitario(produto.getPedidovendaprodutoValorUnitario().floatValue());
						prod.setFlagReenvio(produto.getPedidovendaprodutoFlagReenvio());
						prod.setObservacao(produto.getPedidovendaprodutoObservacao());
						
						
						try {
							cadastro = new CadastrarPedidoVendaProduto();
							cadastro.setIn0(prod);
							stub.cadastrarPedidoVendaProduto(cadastro);
							
							listaItensSincronizados.add(produto);
						} catch (AxisFault e) {
							e.printStackTrace(pw);
							pedidovendasincronizacao.setErro("Erro ao cadastrar pedidovendaproduto: " + e.getMessage());
							pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
							erro = Boolean.TRUE;
						} catch (RemoteException e) {
							e.printStackTrace(pw);
							pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
							erro = Boolean.TRUE;
						} catch (Exception e){
							e.printStackTrace(pw);
							pedidovendasincronizacao.setErro("Erro ao cadastrar pedidovendaproduto: " + e.getMessage());
							pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
							erro = Boolean.TRUE;
						}
					}
				}
				
//				********************************************************************************************
//				***********************************Cadastro de embalagens***********************************
				if(!erro){		
					CadastrarProdutoembalagem cadastrarProdutoembalagem;
					try {
						if(pedidovendasincronizacao.getListaPedidovendamaterialunidademedidasincronizacao() != null){
							for(Pedidovendamaterialunidademedidasincronizacao embalagem : pedidovendasincronizacao.getListaPedidovendamaterialunidademedidasincronizacao()){								
								cadastrarProdutoembalagem = new CadastrarProdutoembalagem();
								
								Produtoembalagem produtoembalagem = new Produtoembalagem();
								produtoembalagem.setCdproduto(embalagem.getMaterial() != null && embalagem.getMaterial().getCdmaterial() != null 
																? embalagem.getMaterial().getCdmaterial() : 0);
								produtoembalagem.setCodigoerp(embalagem.getMaterialunidademedida() != null && embalagem.getMaterialunidademedida().getCdmaterialunidademedida() != null
																? embalagem.getMaterialunidademedida().getCdmaterialunidademedida() : 0);
								produtoembalagem.setDescricao(embalagem.getUnidademedida() != null && embalagem.getUnidademedida().getSimbolo() != null ? embalagem.getUnidademedida().getSimbolo() : "");
								produtoembalagem.setFator(embalagem.getFracao() != null ? embalagem.getFracao() : 0.);
								produtoembalagem.setQtde(embalagem.getQtdereferencia() != null ? embalagem.getQtdereferencia() : 0.);
								
								cadastrarProdutoembalagem.setIn0(produtoembalagem);
								stub.cadastrarProdutoembalagem(cadastrarProdutoembalagem);
							}
						}
					} catch (AxisFault e) {
						e.printStackTrace(pw);
						pedidovendasincronizacao.setErro("Erro ao cadastrar embalagem: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					} catch (RemoteException e) {
						e.printStackTrace(pw);
						pedidovendasincronizacao.setErro("Erro ao cadastrar embalagem: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					} catch (Exception e){
						e.printStackTrace(pw);
						pedidovendasincronizacao.setErro("Erro ao cadastrar embalagem: " + e.getMessage());
						pedidovendasincronizacao.setErrolog("StackTrace: " + sw.toString());
						erro = Boolean.TRUE;
					}
				}
			}
			
			if(!erro){
				updatePedidovendasincronizacaoSincronizado(pedidovendasincronizacao);
				pedidovendasincronizacaohistoricoService.insertPedidovendasincronizacaohistorico(pedidovendasincronizacao, "Pedido de venda sincronizado com sucesso.  Qtde de itens sincronizados: " + listaItensSincronizados.size(), "");
			}else {
				boolean erromarcaritem = false;
				boolean erroexclusaoitem = false;
				String erroMsg = null;
				String erroLog = null;
				try{
					StringBuilder itens = new StringBuilder();
					for (Pedidovendamaterialsincronizacao produto : pedidovendasincronizacao.getListaPedidovendamaterialsincronizacao()) {	
						if(produto.getPedidovendaprodutoId() != null){
							itens.append(!"".equals(itens.toString()) ? "," : "").append(produto.getPedidovendaprodutoId());
						}
					}
					MarcarItemPedido marcarItemPedido = new MarcarItemPedido();
					marcarItemPedido.setIn0(itens.toString());
					
					MarcarItemPedidoResponse marcarItemPedidoResponse = stub.marcarItemPedido(marcarItemPedido);
					if(!marcarItemPedidoResponse.getOut()){
						erromarcaritem = true;
					} else {
						ConfirmarExclusaoItemPedido confirmarExclusaoItemPedido = new ConfirmarExclusaoItemPedido();
						confirmarExclusaoItemPedido.setIn0(itens.toString());
						
						ConfirmarExclusaoItemPedidoResponse confirmarExclusaoItemPedidoResponse = stub.confirmarExclusaoItemPedido(confirmarExclusaoItemPedido);
						if(!confirmarExclusaoItemPedidoResponse.getOut()){
							erroexclusaoitem = true;
						}
					}
				} catch (Exception e) {
					erroMsg = e.getMessage();
					erroLog = " StackTrace: " + sw.toString();
				}
				
				if(erromarcaritem || erroexclusaoitem){
					pedidovendasincronizacaohistoricoService.insertPedidovendasincronizacaohistorico(pedidovendasincronizacao, "Erro na sincroniza��o de alguns itens.", "Ao tentar remover os itens enviados do wms ocorreu. " + erroMsg + " .StackTrace: \n" + erroLog);
				}else {
					pedidovendasincronizacaohistoricoService.insertPedidovendasincronizacaohistorico(pedidovendasincronizacao, "Erro na sincroniza��o de alguns itens. Os itens enviados foram removidos do wms.", "");
				}
				
				this.updatePedidovendasincronizacaoComErro(pedidovendasincronizacao);
				this.updatePedidovendasincronizacaoNaoSincronizado(pedidovendasincronizacao, false);
				
				if(pedidovendasincronizacao.getPedidovendaId() != null){					
					SinedUtil.enviaEmailErroSincronizacaoWMS("pedido de venda", pedidovendasincronizacao.getPedidovendaId(), sw.toString());
				} else{
					SinedUtil.enviaEmailErroSincronizacaoWMS("pedido de venda sincroniza��o", pedidovendasincronizacao.getCdpedidovendasincronizacao(), sw.toString());
				}
			}
		} catch (Exception e){
			e.printStackTrace();
			SinedUtil.enviaEmailErroSincronizacaoWMS("pedido de venda sincroniza��o", pedidovendasincronizacao.getCdpedidovendasincronizacao(), e);
		}
	}
	
	/**
	 * M�todo de preenchimento padr�o de um Produto a ser sincronizado a partir de um Pedidovendamaterialsincronizacao;
	 * 
	 * @param bean
	 * @param mapaCdarquivo
	 * @return
	 * @author Rafael Salvio
	 */
	private Produto montaProdutoSincronizacao(Pedidovendamaterialsincronizacao bean, Map<Integer, Integer> mapaCdarquivo){
		Produto produto = new Produto();
		produto.setId(bean.getMaterialId());
		produto.setCdprodutomestre(bean.getMaterialmestreId());
		produto.setDescricao(bean.getMaterialDescricao());
		produto.setReferencia(bean.getMaterialReferencia());
		produto.setCodigo(bean.getMaterialCodigo());
		produto.setClasseProduto(bean.getMaterialClasseproduto());
		produto.setDescricaoEmbalagem(bean.getMaterialDescricaoEmbalagem());
		produto.setQuantidadeVolume(bean.getMaterialQuantidadeVolume());
		produto.setPeso(bean.getMaterialPeso() != null ? bean.getMaterialPeso() : 0);
		produto.setAltura(bean.getMaterialAltura() != null ? bean.getMaterialAltura() : 0);
		produto.setLargura(bean.getMaterialLargura() != null ? bean.getMaterialLargura() : 0);
		produto.setProfundidade(bean.getMaterialComprimento() != null ? bean.getMaterialComprimento() : 0);
		produto.setCdmaterialcor(bean.getMaterialcorId() != null ? bean.getMaterialcorId() : 0);
		produto.setCdarquivolegenda(0);
		
		if(mapaCdarquivo != null && !mapaCdarquivo.isEmpty()
				&& bean.getMaterial() != null && bean.getMaterial().getMaterialtipo() != null 
				&& bean.getMaterial().getMaterialtipo().getCdmaterialtipo() != null){
			if(mapaCdarquivo.containsKey(bean.getMaterial().getMaterialtipo().getCdmaterialtipo()))
				produto.setCdarquivolegenda(mapaCdarquivo.get(bean.getMaterial().getMaterialtipo().getCdmaterialtipo()));
		}
		
		return produto;
	}
	
	/**
	 * M�todo de preenchimento padr�o de um Pedidovendamaterialsincronizacao a partir de um Material;
	 * 
	 * @param beanVendamaterial
	 * @param material
	 * @param materialmestre
	 * @return
	 * @author Rafael Salvio
	 */
	private Pedidovendamaterialsincronizacao preencheBeanVendamaterial(Pedidovendamaterialsincronizacao beanVendamaterial, Material material, Material materialmestre){
		if(beanVendamaterial == null){
			beanVendamaterial = new Pedidovendamaterialsincronizacao();
		}
		
		beanVendamaterial.setMaterial(material);
		beanVendamaterial.setMaterialId(material.getCdmaterial());
		if(materialmestre != null){
			beanVendamaterial.setMaterialmestre(materialmestre);
			beanVendamaterial.setMaterialmestreId(materialmestre.getMaterialmestregrade() != null 
													? materialmestre.getMaterialmestregrade().getCdmaterial() : null);
		} else{
			beanVendamaterial.setMaterialmestre(material.getMaterialmestregrade());
			beanVendamaterial.setMaterialmestreId(material.getMaterialmestregrade() != null 
													? material.getMaterialmestregrade().getCdmaterial() : 0);
		}
		beanVendamaterial.setMaterialDescricao(material.getNome());
		beanVendamaterial.setMaterialReferencia(material.getCodigofabricante());
		beanVendamaterial.setMaterialCodigo(material.getCdmaterial().toString());
		beanVendamaterial.setMaterialClasseproduto(material.getMaterialgrupo().getCdmaterialgrupo().toString());
		beanVendamaterial.setMaterialDescricaoEmbalagem(material.getUnidademedida() != null ? material.getUnidademedida().getSimbolo() : "UN");
		beanVendamaterial.setMaterialQuantidadeVolume(1);
		beanVendamaterial.setMaterialPeso(material.getPesobruto());
		beanVendamaterial.setMaterialAltura(material.getMaterialproduto() != null 
											&& material.getMaterialproduto().getAltura() != null
											? Math.round(material.getMaterialproduto().getAltura()/10) : 0);
		beanVendamaterial.setMaterialLargura(material.getMaterialproduto() != null 
											&& material.getMaterialproduto().getLargura() != null
											? Math.round(material.getMaterialproduto().getLargura()/10) : 0);
		beanVendamaterial.setMaterialComprimento(material.getMaterialproduto() != null 
											&& material.getMaterialproduto().getComprimento() != null
											? Math.round(material.getMaterialproduto().getComprimento()/10) : 0);
		beanVendamaterial.setMaterialcorId(material.getMaterialcor() != null 
											&& material.getMaterialcor().getCdmaterialcor() != null 
											? material.getMaterialcor().getCdmaterialcor() : 0);
		
		return beanVendamaterial;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updatePedidovendasincronizacaoNaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao, Boolean sincronizado)
	 *
	 * @param pedidovendasincronizacao
	 * @param sincronizado
	 * @author Luiz Fernando
	 */
	private void updatePedidovendasincronizacaoNaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao, Boolean sincronizado) {
		pedidovendasincronizacaoDAO.updatePedidovendasincronizacaoNaoSincronizado(pedidovendasincronizacao, sincronizado);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updatePedidovendasincronizacaoComErro(Pedidovendasincronizacao pedidovendasincronizacao)
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	private void updatePedidovendasincronizacaoComErro(Pedidovendasincronizacao pedidovendasincronizacao) {
		pedidovendasincronizacaoDAO.updatePedidovendasincronizacaoComErro(pedidovendasincronizacao);
	}
	/**
	 * M�todo que cria um Pedidovendasincronizacao
	 *
	 * @param pedidoVenda
	 * @param reenvio
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovendasincronizacao criarPedidovendasincronizacao(Pedidovenda pedidoVenda, boolean reenvio) {
		Pedidovendasincronizacao bean = new Pedidovendasincronizacao();
		bean.setSincronizado(Boolean.FALSE);
		bean.setReenvio(reenvio);
		List<Pedidovendamaterialsincronizacao> listaPedidovendamaterialsincronizacao = new ArrayList<Pedidovendamaterialsincronizacao>();
		
		bean.setPedidovendasituacao(pedidoVenda.getPedidovendasituacao());
		try {
			
		
//		**********************************Cadastra o Cliente****************************************
			if (pedidoVenda.getCliente() != null){
				bean.setCliente(clienteService.load(pedidoVenda.getCliente().getCdpessoa()));
				bean.setClienteId(bean.getCliente().getCdpessoa());
				bean.setClienteNome(bean.getCliente().getNome());
				if (bean.getCliente().getTipopessoa() == Tipopessoa.PESSOA_JURIDICA){
					bean.setClienteNatureza("J");
				} else {
					bean.setClienteNatureza("F");
				}
				bean.setClienteDocumento(StringUtils.soNumero(bean.getCliente().getCpfCnpj()));
				bean.setClienteFilial("F");
				Endereco enderecoCliente = enderecoService.loadEndereco(pedidoVenda.getEndereco());
				if(enderecoCliente != null)
					bean.setClienteEndereco(enderecoCliente.getLogradouroCompletoComBairro());
				
			}

//		**********************************Cadastra a Filial****************************************
			Empresa filial = new Empresa();
			if (pedidoVenda.getEmpresa() != null){
				bean.setEmpresa(empresaService.carregaEmpresa(pedidoVenda.getEmpresa()));
				filial = bean.getEmpresa();
				if(filial == null) 
					filial = empresaService.loadPrincipal();
				bean.setFilialwmsId(filial.getCdpessoa());
				bean.setFilialwmsNome(filial.getRazaosocialOuNome());
				bean.setFilialwmsNatureza("J");
				bean.setFilialwmsDocumento(StringUtils.soNumero(filial.getCnpj().toString()));
				bean.setFilialwmsFilial("V");
			}

//		**********************************Cadastra o Endere�o**************************************
			if (pedidoVenda.getEndereco() != null){
				bean.setEndereco(enderecoService.loadEndereco(pedidoVenda.getEndereco()));
				bean.setEnderecoId(bean.getEndereco().getCdendereco());
				bean.setEnderecoIdCliente(bean.getCliente().getCdpessoa());
				bean.setEnderecoLogradouro(bean.getEndereco().getLogradouro());
				if (bean.getEndereco().getNumero() != null)
					bean.setEnderecoNumero(bean.getEndereco().getNumero());
				if (bean.getEndereco().getComplemento() != null)
					bean.setEnderecoComplemento(bean.getEndereco().getComplemento());
				if (bean.getEndereco().getBairro() != null)
					bean.setEnderecoBairro(bean.getEndereco().getBairro());
				if(bean.getEndereco().getCep() != null)
					bean.setEnderecoCep(bean.getEndereco().getCep().toString().replace("-", ""));
				if (bean.getEndereco().getPontoreferencia() != null)
					bean.setEnderecoPontoreferencia(bean.getEndereco().getPontoreferencia());
				if (bean.getEndereco().getMunicipio() != null)
					bean.setEnderecoMunicipio(bean.getEndereco().getMunicipio().getNome());
				if (bean.getEndereco().getMunicipio() != null && bean.getEndereco().getMunicipio().getUf() != null)
					bean.setEnderecoUf(bean.getEndereco().getMunicipio().getUf().getSigla());
			}
			
//			**********************************Cadastra o Transportador**********************************
			if (pedidoVenda.getTerceiro() != null){
				bean.setTransportador(fornecedorService.load(pedidoVenda.getTerceiro().getCdpessoa()));
				bean.setTransportadorId(bean.getTransportador().getCdpessoa());
				bean.setTransportadorNome(bean.getTransportador().getNome());
				if (bean.getTransportador().getTipopessoa() == Tipopessoa.PESSOA_JURIDICA){
					bean.setTransportadorNatureza("J");
				} else {
					bean.setTransportadorNatureza("F");
				}
				bean.setTransportadorCpfCnpj(StringUtils.soNumero(bean.getTransportador().getCpfCnpj()));				
			}
			
//		*********************************Cadastra o PedidoVenda************************************
			bean.setPedidovenda(pedidoVenda);
			bean.setPedidovendaId(pedidoVenda.getCdpedidovenda());
			bean.setPedidovendaIdFilialVenda(filial.getCdpessoa());
			bean.setPedidovendaNumero(pedidoVenda.getCdpedidovenda().toString());
			bean.setPedidovendaIdCliente(pedidoVenda.getCliente().getCdpessoa());
			bean.setPedidovendaDataEmissao(pedidoVenda.getDtpedidovenda());
			bean.setPedidovendaDataLancamento(pedidoVenda.getDtpedidovenda());
			bean.setPedidovendaFlagTroca("F");
			bean.setPedidovendaIdExterno(pedidoVenda.getIdentificacaoexterna());
			bean.setPedidovendaObservacaointerna(pedidoVenda.getObservacaointerna());
			
//		*****************************Cadastra o PedidoVendaMaterial********************************
			Pedidovendamaterialsincronizacao beanVendamaterial;
			Set<String> whereInCdmaterial = new ListSet<String>(String.class);
			for (Pedidovendamaterial produto : pedidoVenda.getListaPedidovendamaterial()) {
				beanVendamaterial = new Pedidovendamaterialsincronizacao();
				beanVendamaterial.setMaterial(materialService.carregaMaterialWMS(produto.getMaterial()));
				if(materialService.existMaterialitemByMaterialgrademestre(beanVendamaterial.getMaterial())){
					beanVendamaterial.setMaterialmestre(beanVendamaterial.getMaterial());
				}
	//			**************************Material Grupo*****************************
				beanVendamaterial.setMaterialgrupo(beanVendamaterial.getMaterial().getMaterialgrupo());
				beanVendamaterial.setMaterialgrupoNumero(beanVendamaterial.getMaterial().getMaterialgrupo().getCdmaterialgrupo().toString());
				beanVendamaterial.setMaterialgrupoNome(beanVendamaterial.getMaterial().getMaterialgrupo().getNome());
							
	//			*****************************Material********************************
				beanVendamaterial = preencheBeanVendamaterial(beanVendamaterial, beanVendamaterial.getMaterial(), beanVendamaterial.getMaterialmestre());
				whereInCdmaterial.add(beanVendamaterial.getMaterialCodigo());

	//			**************************PedidoVendaProduto**************************			
				beanVendamaterial.setPedidovendaprodutoId(produto.getCdpedidovendamaterial());
				beanVendamaterial.setPedidovendaprodutoDeposito(StringUtils.soNumero(bean.getEmpresa().getCnpj().toString()));
				beanVendamaterial.setPedidovendaprodutoIdPedidoVenda(pedidoVenda.getCdpedidovenda());
				beanVendamaterial.setPedidovendaprodutoIdProduto(produto.getMaterial().getCdmaterial());
				beanVendamaterial.setPedidovendaprodutoQuantidade(getQtdeForSincronizar(produto));
				beanVendamaterial.setPedidovendaprodutoPrevisaoEntrega(produto.getDtprazoentrega() != null ? produto.getDtprazoentrega() : pedidoVenda.getDtpedidovenda());
				beanVendamaterial.setPedidovendaprodutoObservacao(produto.getObservacao() != null ? produto.getObservacao() : "");
				
				if(pedidoVenda.getPedidovendatipo() != null && 
						pedidoVenda.getPedidovendatipo().getSigla() != null && 
						!pedidoVenda.getPedidovendatipo().getSigla().equals("")){
					beanVendamaterial.setPedidovendaprodutoTipo(pedidoVenda.getPedidovendatipo().getSigla());
				} else beanVendamaterial.setPedidovendaprodutoTipo("C");
				
				if (pedidoVenda.getEndereco() != null){
					beanVendamaterial.setPedidovendaprodutoIdEnderecoEntrega(pedidoVenda.getEndereco().getCdendereco());
				} else {
					beanVendamaterial.setPedidovendaprodutoIdEnderecoEntrega(0);
				}
				beanVendamaterial.setPedidovendaprodutoValorUnitario(getPrecoUnitarioForSincronizar(produto, beanVendamaterial.getPedidovendaprodutoQuantidade()));
				
				if(reenvio)
					beanVendamaterial.setPedidovendaprodutoFlagReenvio("V");
				else
					beanVendamaterial.setPedidovendaprodutoFlagReenvio("F");
				
				beanVendamaterial.setPedidovendamaterial(produto);
				listaPedidovendamaterialsincronizacao.add(beanVendamaterial);
			}
			
			bean.setListaPedidovendamaterialsincronizacao(listaPedidovendamaterialsincronizacao);
		
		
//		*****************************Cadastra o PedidoVendaMaterialunidademedida********************************
			List<Pedidovendamaterialunidademedidasincronizacao> listaEmbalagens = new ListSet<Pedidovendamaterialunidademedidasincronizacao>(Pedidovendamaterialunidademedidasincronizacao.class);
			StringBuilder whereIn = new StringBuilder();
			Iterator<String> it = whereInCdmaterial.iterator();
			while (it.hasNext()) {
				whereIn.append(it.next());
				if(it.hasNext()){
					whereIn.append(",");
				}
			}
			List<Materialunidademedida> listaMaterialunidademedida = materialunidademedidaService.findByMaterialForSincronizacao(whereIn.toString());
			if(listaMaterialunidademedida != null){
				for(Materialunidademedida materialunidademedida : listaMaterialunidademedida){
					Pedidovendamaterialunidademedidasincronizacao embalagem = new Pedidovendamaterialunidademedidasincronizacao();
					embalagem.setPedidovendasincronizacao(bean);
					embalagem.setMaterial(materialunidademedida.getMaterial());
					embalagem.setUnidademedida(materialunidademedida.getUnidademedida());
					embalagem.setMaterialunidademedida(materialunidademedida);
					embalagem.setFracao(materialunidademedida.getFracao());
					embalagem.setQtdereferencia(materialunidademedida.getQtdereferencia());
					embalagem.setValorunitario(materialunidademedida.getValorunitario());
					
					listaEmbalagens.add(embalagem);
				}
				
				bean.setListaPedidovendamaterialunidademedidasincronizacao(listaEmbalagens);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	private Double getQtdeForSincronizar(Pedidovendamaterial pedidovendamaterial) {
		Double quantidade = 1d;
		if(pedidovendamaterial != null){
			quantidade = pedidovendamaterial.getQuantidade();
			if(pedidovendamaterial.getQuantidade() != null && pedidovendamaterial.getMaterial() != null && 
				pedidovendamaterial.getMaterial().getCdmaterial() != null && 
				pedidovendamaterial.getMaterial().getUnidademedida() != null && 
				pedidovendamaterial.getMaterial().getUnidademedida().getCdunidademedida() != null){
				if(pedidovendamaterial.getUnidademedida() != null && pedidovendamaterial.getUnidademedida().getCdunidademedida() != null && 
						!pedidovendamaterial.getUnidademedida().getCdunidademedida().equals(pedidovendamaterial.getMaterial().getUnidademedida().getCdunidademedida())){
					Double qtde = unidademedidaService.converteQtdeUnidademedida(pedidovendamaterial.getMaterial().getUnidademedida(), 
																				 pedidovendamaterial.getQuantidade(), 
																				 pedidovendamaterial.getUnidademedida(), 
																				 pedidovendamaterial.getMaterial(), 
																				 pedidovendamaterial.getFatorconversao(), 
																				 pedidovendamaterial.getQtdereferencia());
					if(qtde != null && qtde > 0){
						quantidade = qtde;
					}
				}
			}
		}
		return (quantidade != null ? quantidade : 1);
	}
	
	private Double getPrecoUnitarioForSincronizar(Pedidovendamaterial pedidovendamaterial, Double qtdeFinal) {
		if(pedidovendamaterial != null){
			if(pedidovendamaterial.getPreco() != null 
					&& pedidovendamaterial.getQuantidade() != null && pedidovendamaterial.getQuantidade() > 0. 
					&& qtdeFinal != null && qtdeFinal > 0.){
				return pedidovendamaterial.getPreco() * (pedidovendamaterial.getQuantidade() / qtdeFinal);
			}
			return pedidovendamaterial.getPreco();
		}
		
		return 0.;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updatePedidovendasincronizacaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao)
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoSincronizado(Pedidovendasincronizacao pedidovendasincronizacao) {
		pedidovendasincronizacaoDAO.updatePedidovendasincronizacaoSincronizado(pedidovendasincronizacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updatePedidovendasincronizacaoTentativaDeSincronizacao(Pedidovendasincronizacao pedidovendasincronizacao)
	 *
	 * @param pedidovendasincronizacao
	 * @author Luiz Fernando
	 */
	public void updatePedidovendasincronizacaoTentativaDeSincronizacao(Pedidovendasincronizacao pedidovendasincronizacao) {
		pedidovendasincronizacaoDAO.updatePedidovendasincronizacaoTentativaDeSincronizacao(pedidovendasincronizacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO.findByPedidovenda(Pedidovenda pedidoVenda)
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovendasincronizacao findByPedidovenda(Pedidovenda pedidoVenda, Pedidovendasituacao pedidovendasituacao) {
		return pedidovendasincronizacaoDAO.findByPedidovenda(pedidoVenda, pedidovendasituacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO.findByPedidovenda(Pedidovenda pedidoVenda)
	 *
	 * @param pedidoVenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Pedidovendasincronizacao findUpdatePedidovenda(Pedidovenda pedidoVenda) {
		return pedidovendasincronizacaoDAO.findUpdateByPedidovenda(pedidoVenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO.getCdpedidovendasincronizacaoByPedidovenda(Pedidovenda pedidovenda)
 	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Integer getCdpedidovendasincronizacaoByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendasincronizacaoDAO.getCdpedidovendasincronizacaoByPedidovenda(pedidovenda);
	}
	
	public void criarPedidovendasincronizacaoByWhereIn(String whereIn, String observacao) {
		if(whereIn != null && !"".equals(whereIn)){
			List<Pedidovenda> lista = pedidovendaService.findForSincronizacaowms(whereIn);
			if(lista != null && !lista.isEmpty()){
				for(Pedidovenda pedidoVenda : lista){
					pedidovendaService.integracaoWMS(pedidoVenda, observacao, false);
				}
			}
		}		
	}
	
	/**
	 * M�todo que desmarca os itens do pedido de venda no wms
	 *
	 * @param pedidovenda
	 * @author Luiz Fernando
	 */
	public void desmarcarItensPedidovenda(Pedidovenda pedidovenda){
		String mgs = "";
		Boolean erro = false;
		try{
			List<Pedidovendamaterial> lista = pedidovendamaterialService.findByPedidoVenda(pedidovenda);
			List<Pedidovendamaterial> listaIntegracao =  pedidovendaService.getListaIntegracaoWms(lista);
			if(listaIntegracao != null && !listaIntegracao.isEmpty()){
				WmsWebServiceStub stub = new WmsWebServiceStub("http://" + SinedUtil.getUrlIntegracaoWms(pedidovenda.getEmpresa()) + "/webservices/WmsWebService");
				
				DesmarcarItemPedido desmarcarItemPedido = new DesmarcarItemPedido();
				desmarcarItemPedido.setIn0(CollectionsUtil.listAndConcatenate(listaIntegracao, "cdpedidovendamaterial", ","));
				
				DesmarcarItemPedidoResponse desmarcarItemPedidoResponse = stub.desmarcarItemPedido(desmarcarItemPedido);
				if(!desmarcarItemPedidoResponse.getOut()){
					erro = true;
				}
			}
		} catch (Exception e) {
			mgs = e.getMessage();
		}
		
		Pedidovendasincronizacao pedidovendasincronizacao = this.findByPedidovenda(pedidovenda, Pedidovendasituacao.AGUARDANDO_APROVACAO);
		if(erro){			
			if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
				pedidovendasincronizacao.setErro("Erro ao tentar desmarcar itens " + mgs);
				updatePedidovendasincronizacaoComErro(pedidovendasincronizacao);
			}
		}else {
			if(pedidovendasincronizacao != null && pedidovendasincronizacao.getCdpedidovendasincronizacao() != null){
				pedidovendasincronizacao.setErro(mgs);
				updatePedidovendaItensdesmarcados(pedidovendasincronizacao, false);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updatePedidovendaItensdesmarcados(Pedidovendasincronizacao pedidovendasincronizacao, boolean desmarcaritens)
	 *
	 * @param pedidovendasincronizacao
	 * @param desmarcaritens
	 * @author Luiz Fernando
	 */
	private void updatePedidovendaItensdesmarcados(Pedidovendasincronizacao pedidovendasincronizacao, boolean desmarcaritens) {
		pedidovendasincronizacaoDAO.updatePedidovendaItensdesmarcados(pedidovendasincronizacao, desmarcaritens);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendasincronizacaoDAO#updateValorcontadorenvio(Integer novovalorcontadorenvio, String whereIn)
	 *
	 * @param novovalorcontadorenvio
	 * @param whereIn
	 * @author Luiz Fernando
	 */
	public void updateValorcontadorenvio(Integer novovalorcontadorenvio, String whereIn) {
		pedidovendasincronizacaoDAO.updateValorcontadorenvio(novovalorcontadorenvio, whereIn);		
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param material
	* @param empresa
	* @return
	* @throws RemoteException
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public WmsBean cadastrarProdutoWMS(Material material, WmsWebServiceStub stub, String paramMaterialIdentificacaoWMS){
		Pedidovendamaterialsincronizacao produto = preencheBeanVendamaterial(null, material, material.getMaterialmestregrade());
		
		produto.setMaterialgrupo(material.getMaterialgrupo());
		produto.setMaterialgrupoNumero(material.getMaterialgrupo().getCdmaterialgrupo().toString());
		produto.setMaterialgrupoNome(material.getMaterialgrupo().getNome());
		
		Map<Integer, List<Arquivo>> mapaArquivos = new HashMap<Integer, List<Arquivo>>();
		Map<Integer, Integer> mapaCdarquivos = null;
		if(material.getMaterialtipo() != null && material.getMaterialtipo().getCdmaterialtipo() != null){
			List<Materiallegenda> listaML = materiallegendaService.findByMaterialtipo(material.getMaterialtipo().getCdmaterialtipo().toString());
			if(listaML != null && !listaML.isEmpty()){
				for(Materiallegenda ml : listaML){
					if(ml.getSimbolo() != null && ml.getSimbolo().getCdarquivo() != null && ml.getListaMateriallegendamaterialtipo() != null){
						for(Materiallegendamaterialtipo mlmt : ml.getListaMateriallegendamaterialtipo()){
							if(!mapaArquivos.containsKey(mlmt.getMaterialtipo().getCdmaterialtipo())){
								mapaArquivos.put(mlmt.getMaterialtipo().getCdmaterialtipo(), new ListSet<Arquivo>(Arquivo.class));
							}
							mapaArquivos.get(mlmt.getMaterialtipo().getCdmaterialtipo()).add(ml.getSimbolo());
						}
					}
				}
			}
		}
		
		try {
			String param = parametrogeralService.buscaValorPorNome("GERAR_LEGENDAS_CONCATENADAS");
			if(param != null && param.equalsIgnoreCase("true")){
				mapaCdarquivos = generateImagensLegendas(null, material, 20, 20);
			}
		} catch (Exception e) {
			e.printStackTrace();
			SinedUtil.enviaEmailErroSincronizacaoWMS("(Etapa de gera��o de legendas)", 0, e);
		}
		
		return cadastrarProdutoWMS(produto, mapaCdarquivos, stub, paramMaterialIdentificacaoWMS);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param produto
	* @param mapaCdarquivo
	* @param stub
	* @return
	* @throws RemoteException
	* @since 28/09/2015
	* @author Luiz Fernando
	*/
	public WmsBean cadastrarProdutoWMS(Pedidovendamaterialsincronizacao produto, Map<Integer, Integer> mapaCdarquivo, WmsWebServiceStub stub, String paramMaterialIdentificacaoWMS){
		WmsBean wmsBean = new WmsBean();
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		
		CadastrarClasseProduto cadastrarClasseProduto;
		CadastrarProduto cadastrarProduto;
		
		ClasseProduto materialGrupo = new ClasseProduto();
		materialGrupo.setNumero(produto.getMaterialgrupoNumero());
		materialGrupo.setNome(produto.getMaterialgrupoNome());
		
		try {
			cadastrarClasseProduto = new CadastrarClasseProduto();
			cadastrarClasseProduto.setIn0(materialGrupo);
			stub.cadastrarClasseProduto(cadastrarClasseProduto);
		} catch (AxisFault e) {
			e.printStackTrace(pw);
			wmsBean.setDescricaoErro("Erro ao cadastrar classe do produto: " + e.getMessage());
			wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
			wmsBean.setErro(Boolean.TRUE);
		} catch (RemoteException e) {
			e.printStackTrace(pw);
			wmsBean.setDescricaoErro("Erro ao cadastrar classe do produto: " + e.getMessage());
			wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
			wmsBean.setErro(Boolean.TRUE);
		} catch (Exception e){
			e.printStackTrace(pw);
			wmsBean.setDescricaoErro("Erro ao cadastrar classe do produto: " + e.getMessage());
			wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
			wmsBean.setErro(Boolean.TRUE);
		}
		
		if(wmsBean.getErro() == null || !wmsBean.getErro()){
			Produto produto2 = montaProdutoSincronizacao(produto, mapaCdarquivo);
			
			if(org.apache.commons.lang.StringUtils.isNotBlank(paramMaterialIdentificacaoWMS) &&
					"TRUE".equalsIgnoreCase(paramMaterialIdentificacaoWMS)){
				try {
					produto2.setCodigo(produto.getMaterial().getIdentificacao());
				} catch (Exception e) {}
			}
			
			try {
				cadastrarProduto = new CadastrarProduto();
				cadastrarProduto.setIn0(produto2);
				stub.cadastrarProduto(cadastrarProduto);
				
				// Cadastra os itens da grade no wms.
				if(produto.getMaterialmestreId() > 0){
					List<Material> filhos = materialService.findMaterialitemByMaterialmestregrade(produto.getMaterial());
					if(filhos != null && !filhos.isEmpty()){
						Material materialmestre = produto.getMaterial();
						for(Material materialfilho : filhos){
							Pedidovendamaterialsincronizacao temp = preencheBeanVendamaterial(null, materialfilho, materialmestre);
							produto2 = montaProdutoSincronizacao(temp, mapaCdarquivo);
							
							if(org.apache.commons.lang.StringUtils.isNotBlank(paramMaterialIdentificacaoWMS) &&
									"TRUE".equalsIgnoreCase(paramMaterialIdentificacaoWMS)){
								try {
									produto2.setId(Integer.parseInt(produto.getMaterial().getIdentificacao()));
									produto2.setCodigo(produto.getMaterial().getIdentificacao());
								} catch (Exception e) {}
							}
							
							cadastrarProduto = new CadastrarProduto();
							cadastrarProduto.setIn0(produto2);
							stub.cadastrarProduto(cadastrarProduto);
						}
					}
				}
			} catch (AxisFault e) {
				e.printStackTrace(pw);
				wmsBean.setDescricaoErro("Erro ao cadastrar produto: " + e.getMessage());
				wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
				wmsBean.setErro(Boolean.TRUE);
			} catch (RemoteException e) {
				e.printStackTrace(pw);
				wmsBean.setDescricaoErro("Erro ao cadastrar produto: " + e.getMessage());
				wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
				wmsBean.setErro(Boolean.TRUE);
			} catch (Exception e){
				e.printStackTrace(pw);
				wmsBean.setDescricaoErro("Erro ao cadastrar produto: " + e.getMessage());
				wmsBean.setDescricaoErrolog("StackTrace: " + sw.toString());
				wmsBean.setErro(Boolean.TRUE);
			}
		}
		
		return wmsBean;
	}
}
