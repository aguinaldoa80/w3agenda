package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Radiushistorico;
import br.com.linkcom.sined.geral.dao.RadiushistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RadiushistoricoService extends GenericService<Radiushistorico>{	

	private RadiushistoricoDAO radiushistoricoDAO;
	public void setRadiushistoricoDAO(RadiushistoricoDAO radiushistoricoDAO) {
		this.radiushistoricoDAO = radiushistoricoDAO;
	}
	
	/**
	 * Preenche a data de fim da sess�o dos registros passados como par�metro
	 * @param lista
	 * @author Thiago Gon�alves
	 */
	public void confirmartempo(String lista){
		radiushistoricoDAO.confirmartempo(lista);
	}
	
}
