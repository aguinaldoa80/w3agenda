package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.dao.RequisicaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RequisicaohistoricoService extends GenericService<Requisicaohistorico> {

	private RequisicaohistoricoDAO requisicaohistoricoDAO;
	private PessoaService pessoaService;
	
	public void setRequisicaohistoricoDAO(
			RequisicaohistoricoDAO requisicaohistoricoDAO) {
		this.requisicaohistoricoDAO = requisicaohistoricoDAO;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param requisicao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaohistorico> findRequisicoesHistoricoByRequisicao(Requisicao requisicao) {
		List<Requisicaohistorico> lista =  requisicaohistoricoDAO.findRequisicoesHistoricoByRequisicao(requisicao);
		if(lista != null && !lista.isEmpty()){
			for (Requisicaohistorico requisicaohistorico : lista){
				if (requisicaohistorico.getCdusuarioaltera()!=null){
					requisicaohistorico.setPessoa(pessoaService.findPessoaByCodigo(requisicaohistorico.getCdusuarioaltera()));
				}
			}
		}
		
		return lista;
	}


	/**
	 * 
	 * M�todo que verifica se o usu�rio logado pode editar a Ordem de Servi�o:
	 * O primeiro item do hist�rico foi lan�ado por o usu�rio logado
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPodeEditarRequisicao(Requisicao requisicao){
		return requisicaohistoricoDAO.isPodeEditarRequisicao(requisicao);
	}
	
	/* singleton */
	private static RequisicaohistoricoService instance;
	public static RequisicaohistoricoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RequisicaohistoricoService.class);
		}
		return instance;
	}
}