package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.geral.bean.Metacomercialitem;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.dao.MetacomercialDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AcompanhamentometaAnaliticoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AcompanhamentometaAnaliticoItensBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.AcompanhamentometaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.EmitiracompanhamentometaFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MetacomercialFiltro;
import br.com.linkcom.sined.util.Grafico;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.dashboard.DashboardMetaComercialRESTModel;
import br.com.linkcom.sined.util.rest.android.dashboard.DashboardRESTWSBean;

public class MetacomercialService extends GenericService<Metacomercial> {

	private MetacomercialDAO metacomercialDAO;
	private VendaService vendaService;
	private CalendarioService calendarioService;
	private ContratoService contratoService;
	private ColaboradorService colaboradorService;
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMetacomercialDAO(MetacomercialDAO metacomercialDAO) {
		this.metacomercialDAO = metacomercialDAO;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setCalendarioService(CalendarioService calendarioService) {
		this.calendarioService = calendarioService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	public static MetacomercialService instance;
	public static MetacomercialService getInstance(){
		if (instance==null){
			return Neo.getObject(MetacomercialService.class);
		}else {
			return instance;
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MetacomercialDAO#existsMetaPeriodo(Metacomercial metacomercial)
	 *
	 * @param metacomercial
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsMetaPeriodo(Metacomercial metacomercial) {
		return metacomercialDAO.existsMetaPeriodo(metacomercial);
	}

	/**
	 * M�todo que gera o relat�rio de acompanhamento de meta
	 *
	 * @see br.com.linkcom.sined.geral.service.MetacomercialService#findForAcompanhamentometa(Date dtinicio, Date dtfim)
	 * @see br.com.linkcom.sined.geral.service.VendaService#findForAcompanhamentometa(EmitiracompanhamentometaFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public IReport gerarRelatorioAcompanhamentometaSintetico(EmitiracompanhamentometaFiltro filtro) throws Exception {
		Report report = new Report("/faturamento/emitiracompanhamentometasintetico");
		
		Empresa empresa = filtro.getEmpresa();
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		Calendario calendario = calendarioService.getCalendarioUnificado(uf, municipio);
		
		Date dtreferenciaInicio = filtro.getDtreferenciaInicio();
		Date dtreferenciaFim = filtro.getDtreferenciaFim();
		Date firstDateMonth = SinedDateUtils.firstDateOfMonth(dtreferenciaInicio);
		Date lastDateMonth = SinedDateUtils.lastDateOfMonth(dtreferenciaFim);
		Integer diasuteis = calendarioService.getQtdeDiasUteis(filtro.getDtreferenciaInicio(), dtreferenciaFim, calendario);
		Integer diascorridos = calendarioService.getQtdeDiasUteis(firstDateMonth, lastDateMonth, calendario);
		Integer diasrestantes = calendarioService.getQtdeDiasUteis(dtreferenciaFim, lastDateMonth, calendario);
		
		if(diasrestantes > 0) diasrestantes--;
		
		List<AcompanhamentometaBean> listaBean = getListaAcompanhamentoMetaBean(filtro);
		
		Money totalVendas = new Money();
		for (AcompanhamentometaBean acompanhamentometaBean : listaBean) {
			totalVendas = totalVendas.add(acompanhamentometaBean.getValorvenda());
			acompanhamentometaBean.setAux_dtreferencia(dtreferenciaFim);
			acompanhamentometaBean.setAux_firstDateMonth(firstDateMonth);
			acompanhamentometaBean.setAux_lastDateMonth(lastDateMonth);
			acompanhamentometaBean.setAux_diasuteis(diasuteis);
			acompanhamentometaBean.setAux_diascorridos(diascorridos);
			acompanhamentometaBean.setAux_diasrestantes(diasrestantes);
		}
		report.addParameter("dtreferencia", dtreferenciaFim != null ? dtreferenciaFim : 0);
		report.addParameter("diasuteis", diasuteis != null ? diasuteis : 0);
		report.addParameter("diascorridos", diascorridos != null ? diascorridos : 0);
		report.addParameter("diasrestantes", diasrestantes != null ? diasrestantes : 0);
		report.addParameter("totalvendas", totalVendas != null ? totalVendas.toString() : "0,00");
		
		
		if(listaBean != null && !listaBean.isEmpty())
			report.setDataSource(listaBean);	
		else throw new SinedException("N�o existem dados para gerar o relat�rio.");
		
		return report;
	}
	
	public List<AcompanhamentometaBean> getListaAcompanhamentoMetaBean(EmitiracompanhamentometaFiltro filtro) {
		Date dtreferenciaInicio = filtro.getDtreferenciaInicio();
		Date dtreferenciaFim = filtro.getDtreferenciaFim();
		Date firstDateMonth = SinedDateUtils.firstDateOfMonth(dtreferenciaInicio);
		HashMap<Colaborador, Metacomercial> mapColaboradorMetacomercial = new HashMap<Colaborador, Metacomercial>();
		List<Metacomercial> listaMetacomercial = this.findForAcompanhamentometa(firstDateMonth, dtreferenciaFim);
		List<Venda> listaVenda = vendaService.findForAcompanhamentometa(filtro);
		vendaService.listaVendaAjustadaWithQtdeDevolvida(listaVenda);
		List<Contrato> listaContrato = contratoService.findForAcompanhamentometa(filtro);
		List<AcompanhamentometaBean> listaBean = new ArrayList<AcompanhamentometaBean>();
		
		if(listaVenda != null && !listaVenda.isEmpty()){
			for (Venda venda : listaVenda) {
				Metacomercial metacomercial = this.getMetacomercialColaborador(venda.getColaborador(), mapColaboradorMetacomercial, listaMetacomercial);
				
				if(metacomercial != null){				
					Money totalVenda = venda.getTotalvenda(false, true);
					
					AcompanhamentometaBean acompanhamentometaBean = new AcompanhamentometaBean();
					acompanhamentometaBean.setNome_vendedor(venda.getColaborador().getNome());
					acompanhamentometaBean.setNome_meta(metacomercial.getNome());
					acompanhamentometaBean.setValorvenda(totalVenda);
					acompanhamentometaBean.setMeta(metacomercial.getMeta());
				
					this.addAcompanhamentometaBean(listaBean, acompanhamentometaBean);
				}
			}
		}
		
		if(listaContrato != null && !listaContrato.isEmpty()){
			for (Contrato contrato : listaContrato) {
				if(contrato.getVendedortipo() != null && Vendedortipo.COLABORADOR.equals(contrato.getVendedortipo()) && 
						contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null){
					Colaborador colaborador = colaboradorService.loadWithListaCargo(new Colaborador(contrato.getVendedor().getCdpessoa()));
					if(colaborador != null){
						Metacomercial metacomercial = this.getMetacomercialColaborador(colaborador, mapColaboradorMetacomercial, listaMetacomercial);
						
						if(metacomercial != null && contrato.getValor() != null){				
							AcompanhamentometaBean acompanhamentometaBean = new AcompanhamentometaBean();
							acompanhamentometaBean.setNome_vendedor(colaborador.getNome());
							acompanhamentometaBean.setNome_meta(metacomercial.getNome());
							acompanhamentometaBean.setValorvenda(contrato.getValor());
							acompanhamentometaBean.setMeta(metacomercial.getMeta());
						
							this.addAcompanhamentometaBean(listaBean, acompanhamentometaBean);
						}
					}
				}
			}
		}
		return listaBean;
	}
	
	/**
	 * M�todo que adiciona os dados de venda do colaborador a lista do relat�rio
	 *
	 * @param listaBean
	 * @param acompanhamentometaBean
	 * @author Luiz Fernando
	 */
	private void addAcompanhamentometaBean(List<AcompanhamentometaBean> listaBean, AcompanhamentometaBean acompanhamentometaBean) {
		boolean achou = false;
		if(!listaBean.isEmpty()){
			for(AcompanhamentometaBean bean : listaBean){
				if(bean.equals(acompanhamentometaBean)){
					bean.setValorvenda(bean.getValorvenda().add(acompanhamentometaBean.getValorvenda()));
					achou = true;
				}
			}
		}		
		if(!achou) listaBean.add(acompanhamentometaBean);
	}
	
	/**
	 * M�todo que retorna a meta do colaborador
	 *
	 * @param colaborador
	 * @param mapColaboradorMetacomercial
	 * @param listaMetacomercial
	 * @return
	 * @author Luiz Fernando
	 */
	private Metacomercial getMetacomercialColaborador(Colaborador colaborador, HashMap<Colaborador, 
														Metacomercial> mapColaboradorMetacomercial,
														List<Metacomercial> listaMetacomercial) {
		Cargo cargo = colaborador.getCargoAtualTrans();
		Metacomercial mc = mapColaboradorMetacomercial.get(colaborador);
		List<Metacomercial> listaMetaColaborador = new ArrayList<Metacomercial>();
		List<Metacomercial> listaMetaCargo = new ArrayList<Metacomercial>();
		List<Metacomercial> listaMetaGeral = new ArrayList<Metacomercial>();
		if(mc == null && listaMetacomercial != null && !listaMetacomercial.isEmpty()){
			for(Metacomercial metacomercial : listaMetacomercial){
				if(metacomercial.getListaMetacomercialitem() != null && !metacomercial.getListaMetacomercialitem().isEmpty()){
					for(Metacomercialitem metacomercialitem : metacomercial.getListaMetacomercialitem()){
						
						if(metacomercialitem.getColaborador() != null && metacomercialitem.getColaborador().equals(colaborador)){
							listaMetaColaborador.add(metacomercial);
						}else if(metacomercialitem.getCargo() != null && cargo != null && metacomercialitem.getCargo().equals(cargo)){
							listaMetaCargo.add(metacomercial);
						}
					}
				}else{
					listaMetaGeral.add(metacomercial);
				}
			}
		}
		/*N�o s�o permitidas duas metas cadastradas para o mesmo per�odo.*/
		if((listaMetaColaborador != null && listaMetaColaborador.size() > 1)||
			(listaMetaCargo != null && listaMetaCargo.size() > 1)||
			(listaMetaGeral != null && listaMetaGeral.size() > 1)){
			throw new SinedException("O per�odo informado possui metas diferentes.");
		}
		
		/*A meta considerada dever� seguir a seguinte regra:
		 * Se tiver meta cadastrada para o colaborador, a mesma ser� considerada.
		 * Caso n�o haja, ser� verificado se existe meta para o cargo do colaborador e caso haja, a mesma ser� considerada.
		 * Caso n�o se enquadre nessas situa��es, mas tenha meta geral cadastrada, a mesma ser� considerada.*/
		if(listaMetaColaborador.size() == 1){
			mc = new Metacomercial();
			mc.setMeta(listaMetaColaborador.iterator().next().getMeta());
		}else if(listaMetaCargo.size() == 1){
			mc = new Metacomercial();
			mc.setMeta(listaMetaCargo.iterator().next().getMeta());
		}else if(listaMetaGeral.size() == 1){
			mc = new Metacomercial();
			mc.setMeta(listaMetaGeral.iterator().next().getMeta());
		}
		return mc;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MetacomercialDAO#findForAcompanhamentometa(Date dtinicio, Date dtfim)
	 *
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Metacomercial> findForAcompanhamentometa(Date dtinicio, Date dtfim) {
		return metacomercialDAO.findForAcompanhamentometa(dtinicio, dtfim);
	}
	
	public List<Metacomercial> findForMetaColaboradores (Metacomercial metacomercial, String whereIn){
		return metacomercialDAO.findForMetaColaboradores(metacomercial, whereIn);
	}
	
	/**
	 * M�todo que cria relat�rio CSV, com os dados da listagem
	 * 
	 * @param filtro
	 * @author Danilo Guimar�es
	 */
	public Resource createMetacomercialCSVReport(MetacomercialFiltro filtro) {
		List<Metacomercial> metacomercial = metacomercialDAO.findForCsv(filtro);
		
		StringBuilder csv = new StringBuilder();
		StringBuilder csvColaboradores = new StringBuilder();
		
		csv.append("Nome;V�lido de;V�lido at�;Valor da Meta;Colaborador(es)\n");
		
		if (metacomercial != null && !metacomercial.isEmpty()) {
			for (Metacomercial mc : metacomercial) {
				csv
				.append(mc.getNome()).append(";")
				.append(mc.getDtinicio()).append(";")
				.append(mc.getDtfim()).append(";")
				.append(mc.getMeta()).append(";");
				
				if (mc.getListaMetacomercialitem() != null && !mc.getListaMetacomercialitem().isEmpty()) {
					csvColaboradores = new StringBuilder();
					for (Metacomercialitem mci : mc.getListaMetacomercialitem()) {
						if (mci.getColaborador() != null) {
							csvColaboradores
							.append(";;;;")
							.append(mci.getColaborador().getNome()).append(";\n");
						}
					}
				}
				csv
				.append("\n")
				.append(csvColaboradores.toString());
			}
		}
		Resource resource = new Resource("text/csv", "metacomercial_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public List<Metacomercial> findForMetaAtualizacao (String whereIn) {
		return metacomercialDAO.findForMetaAtualizacao(whereIn);
	}
	
	public IReport gerarRelatorioAcompanhamentometaAnalitico(EmitiracompanhamentometaFiltro filtro) {
//		report.addParameter("GRAFICO",Grafico.geraGraficoAcompanhamentoProjeto(listaAcompanhamentoProjeto));
		
		Empresa empresa = filtro.getEmpresa();
		Municipio municipio = null;
		Uf uf = null;
		if(empresa != null && empresa.getCdpessoa() != null){
			empresa = empresaService.loadWithEndereco(empresa);
			if(empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				for (Endereco endereco : empresa.getListaEndereco()) {
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getCdmunicipio() != null){
						municipio = endereco.getMunicipio();
						if(municipio.getUf() != null && municipio.getUf().getCduf() != null){
							uf = municipio.getUf();
						}
					}
				}
			}
		}
		
		Calendario calendario = calendarioService.getCalendarioUnificado(uf, municipio);
		
		Date dtreferenciaInicio = filtro.getDtreferenciaInicio();
		Date dtreferenciaFim = filtro.getDtreferenciaFim();
		Date firstDateMonth = SinedDateUtils.firstDateOfMonth(dtreferenciaInicio);
		Date lastDateMonth = SinedDateUtils.lastDateOfMonth(dtreferenciaFim);
		Integer diasuteis = calendarioService.getQtdeDiasUteis(filtro.getDtreferenciaInicio(), dtreferenciaFim, calendario);
		Integer diascorridos = calendarioService.getQtdeDiasUteis(firstDateMonth, lastDateMonth, calendario);
		Integer diasrestantes = calendarioService.getQtdeDiasUteis(dtreferenciaFim, lastDateMonth, calendario);
		
		if(diasrestantes > 0) diasrestantes--;
		
		HashMap<Colaborador, Metacomercial> mapColaboradorMetacomercial = new HashMap<Colaborador, Metacomercial>();
		List<Metacomercial> listaMetacomercial = this.findForAcompanhamentometa(firstDateMonth, dtreferenciaFim);
		Metacomercial metacomercial = null;
		List<Venda> listaVenda = vendaService.findForAcompanhamentometa(filtro);
		vendaService.listaVendaAjustadaWithQtdeDevolvida(listaVenda);
		List<Contrato> listaContrato = contratoService.findForAcompanhamentometa(filtro);
		List<AcompanhamentometaAnaliticoBean> listaBean = new ArrayList<AcompanhamentometaAnaliticoBean>();
		AcompanhamentometaAnaliticoBean bean;
		Money totalVendas = new Money();
		List<java.util.Date> listaDatas = new ArrayList<java.util.Date>();
		if(listaVenda != null && !listaVenda.isEmpty()){
			for (Venda venda : listaVenda) {
				metacomercial = this.getMetacomercialColaborador(venda.getColaborador(), mapColaboradorMetacomercial, listaMetacomercial);
				
				if(metacomercial != null){				
					Money valorTotalVenda  = venda.getTotalvenda(false, true);
					totalVendas = totalVendas.add(valorTotalVenda);
					
					bean = new AcompanhamentometaAnaliticoBean();
					bean.setNome_vendedor(venda.getColaborador().getNome());
					bean.setNome_meta(metacomercial.getNome());
					bean.setMeta(metacomercial.getMeta());
					
					bean.setAux_dtreferencia(dtreferenciaFim);
					bean.setAux_firstDateMonth(firstDateMonth);
					bean.setAux_lastDateMonth(lastDateMonth);
					bean.setAux_diasuteis(diasuteis);
					bean.setAux_diascorridos(diascorridos);
					bean.setAux_diasrestantes(diasrestantes);
				
					if(venda.getDtvenda() != null && !listaDatas.contains(venda.getDtvenda()))
						listaDatas.add(venda.getDtvenda());
					this.addAcompanhamentometaAnaliticoBean(listaBean, bean, venda.getDtvenda(), valorTotalVenda);
				}
			}
		}
		
		if(listaContrato != null && !listaContrato.isEmpty()){
			for (Contrato contrato : listaContrato) {
				if(contrato.getVendedortipo() != null && Vendedortipo.COLABORADOR.equals(contrato.getVendedortipo()) && 
						contrato.getVendedor() != null && contrato.getVendedor().getCdpessoa() != null){
					Colaborador colaborador = colaboradorService.loadWithListaCargo(new Colaborador(contrato.getVendedor().getCdpessoa()));
					if(colaborador != null){
						metacomercial = this.getMetacomercialColaborador(colaborador, mapColaboradorMetacomercial, listaMetacomercial);
						
						if(metacomercial != null && contrato.getValor() != null){				
							Money valorTotalVenda  = contrato.getValor();
							totalVendas = totalVendas.add(valorTotalVenda);
							
							bean = new AcompanhamentometaAnaliticoBean();
							bean.setNome_vendedor(colaborador.getNome());
							bean.setNome_meta(metacomercial.getNome());
							bean.setMeta(metacomercial.getMeta());
							
							bean.setAux_dtreferencia(dtreferenciaFim);
							bean.setAux_firstDateMonth(firstDateMonth);
							bean.setAux_lastDateMonth(lastDateMonth);
							bean.setAux_diasuteis(diasuteis);
							bean.setAux_diascorridos(diascorridos);
							bean.setAux_diasrestantes(diasrestantes);
						
							if(contrato.getDtinicio() != null && !listaDatas.contains(contrato.getDtinicio()))
								listaDatas.add(contrato.getDtinicio());
							this.addAcompanhamentometaAnaliticoBean(listaBean, bean, contrato.getDtinicio(), valorTotalVenda);
						}
					}
				}
			}
		}
		
		this.ajustaDadosAnalitico(listaBean, listaDatas, dtreferenciaInicio, dtreferenciaFim);
		
		if(listaBean != null && !listaBean.isEmpty()){
			for(AcompanhamentometaAnaliticoBean analiticoBean : listaBean){
				try {
					analiticoBean.setGrafico(Grafico.geraGraficoAcompanhamentometa(analiticoBean));
				} catch (Exception e) {
					e.printStackTrace();
//					System.out.println("Erro ao criar gr�fico: " + e.getMessage());
				}
			}
		}
		
		Report report = new Report("/faturamento/emitiracompanhamentometaanalitico");
		if(listaBean != null && !listaBean.isEmpty())
			report.setDataSource(listaBean);	
		else throw new SinedException("N�o existem dados para gerar o relat�rio.");
		
		report.addParameter("dtreferencia", dtreferenciaFim != null ? dtreferenciaFim : 0);
		report.addParameter("diasuteis", diasuteis != null ? diasuteis : 0);
		report.addParameter("diascorridos", diascorridos != null ? diascorridos : 0);
		report.addParameter("diasrestantes", diasrestantes != null ? diasrestantes : 0);
		
		Report sub_report = new Report("/faturamento/emitiracompanhamentometaanalitico_sub");
		report.addSubReport("EMITIRACOMPANHAMENTOMETAANALITICO_SUB", sub_report);
		return report;
	}

	private void ajustaDadosAnalitico(List<AcompanhamentometaAnaliticoBean> lista, List<java.util.Date> listaDatas, Date dtinicio, Date dtfim) {
		if(lista != null && !lista.isEmpty()){
			for(AcompanhamentometaAnaliticoBean analiticoBean : lista){
				if(analiticoBean != null && dtinicio != null && dtfim != null){
					Date data = dtinicio;
					while(SinedDateUtils.beforeOrEqualIgnoreHour(data, dtfim)){
						if(!listaDatas.contains(data)){
							AcompanhamentometaAnaliticoItensBean itemBean = new AcompanhamentometaAnaliticoItensBean();
							itemBean.setData(data);
							itemBean.setVendadiaria(new Money());
							itemBean.setMetadiaria(analiticoBean.getMetadiaria());
							if(analiticoBean.getListaItens() == null)
								analiticoBean.setListaItens(new ArrayList<AcompanhamentometaAnaliticoItensBean>());
							analiticoBean.getListaItens().add(itemBean);
						}
						data = SinedDateUtils.addDiasData(data, 1);
					}
					
					if(analiticoBean.getListaItens() != null && !analiticoBean.getListaItens().isEmpty()){
						Collections.sort(analiticoBean.getListaItens(), new Comparator<AcompanhamentometaAnaliticoItensBean>(){
							public int compare(AcompanhamentometaAnaliticoItensBean a1, AcompanhamentometaAnaliticoItensBean a2){
								try {
									return a1.getData().compareTo(a2.getData());
								} catch (Exception e) {
									return 0;
								}
							}
						});
					}
					
					boolean primeiroItem = true;
					Money valortotal = new Money();
					for(AcompanhamentometaAnaliticoItensBean item : analiticoBean.getListaItens()){
						if(primeiroItem){
							item.setPrimeiroitem(true);
							primeiroItem = false;
							if(item.getVendadiaria() != null){
								valortotal = item.getVendadiaria();
							}
							item.setVendatotal(new Money());
						}else {
							if(item.getVendadiaria() != null){
								valortotal = valortotal.add(item.getVendadiaria());
							}
							item.setVendatotal(valortotal);
						}
					}
				}
			}
		}
	}
	
	private void addAcompanhamentometaAnaliticoBean(List<AcompanhamentometaAnaliticoBean> listaBean, AcompanhamentometaAnaliticoBean bean, java.util.Date data, Money valor) {
		if(listaBean != null && bean != null){
			boolean addBeanNovo = true;
			for(AcompanhamentometaAnaliticoBean acompanhamentometaBean : listaBean){
				if(bean.equals(acompanhamentometaBean)){
					addBeanNovo = false;
					boolean addItemNovo = true;
					if(acompanhamentometaBean.getListaItens() != null && !acompanhamentometaBean.getListaItens().isEmpty()){
						for(AcompanhamentometaAnaliticoItensBean item : acompanhamentometaBean.getListaItens()){
							if(item.getData() != null && item.getData().equals(data)){
								addItemNovo = false;
								if(item.getVendadiaria() == null) item.setVendadiaria(new Money());
								item.setVendadiaria(item.getVendadiaria().add(valor));
								break;
							}
						}
					}
					if(addItemNovo){
						if(acompanhamentometaBean.getListaItens() == null)
							acompanhamentometaBean.setListaItens(new ArrayList<AcompanhamentometaAnaliticoItensBean>());
						
						AcompanhamentometaAnaliticoItensBean itemBean = new AcompanhamentometaAnaliticoItensBean();
						itemBean.setData(data);
						itemBean.setVendadiaria(valor);
						itemBean.setMetadiaria(bean.getMetadiaria());
						acompanhamentometaBean.getListaItens().add(itemBean);
					}
					break;
				}
			}
			if(addBeanNovo){
				if(bean.getListaItens() == null)
					bean.setListaItens(new ArrayList<AcompanhamentometaAnaliticoItensBean>());
				
				AcompanhamentometaAnaliticoItensBean itemBean = new AcompanhamentometaAnaliticoItensBean();
				itemBean.setData(data);
				itemBean.setVendadiaria(valor);
				itemBean.setMetadiaria(bean.getMetadiaria());
				bean.getListaItens().add(itemBean);
				listaBean.add(bean);
			}
		}		
	}
	
	public DashboardMetaComercialRESTModel getDashboardMetaComercial(DashboardRESTWSBean bean) {
		boolean isColaborador = colaboradorService.isColaborador(bean.getCdusuario());
		if(!isColaborador) return null;
		
		EmitiracompanhamentometaFiltro filtro = new EmitiracompanhamentometaFiltro();
		filtro.setColaborador(new Colaborador(bean.getCdusuario()));
		filtro.setDtreferenciaInicio(SinedDateUtils.firstDateOfMonth());
		filtro.setDtreferenciaFim(SinedDateUtils.lastDateOfMonth());
		filtro.setEmpresa(new Empresa(bean.getCdempresa()));
		filtro.setTiporelatorio("sintetico");
		
		List<AcompanhamentometaBean> listaAcompanhamentoMetaBean = this.getListaAcompanhamentoMetaBean(filtro);
		if(listaAcompanhamentoMetaBean != null && listaAcompanhamentoMetaBean.size() == 1){
			AcompanhamentometaBean acompanhamentometaBean = listaAcompanhamentoMetaBean.get(0);
			
			DashboardMetaComercialRESTModel model = new DashboardMetaComercialRESTModel();
			model.setMetaComercialTotal(acompanhamentometaBean.getMeta() != null ? acompanhamentometaBean.getMeta().doubleValue() : 0d);
			model.setMetaComercialAtingida(acompanhamentometaBean.getMeta() != null ? acompanhamentometaBean.getValorvenda().doubleValue() : 0d);
			return model;
		}
		
		
		return null;
	}
	
}
