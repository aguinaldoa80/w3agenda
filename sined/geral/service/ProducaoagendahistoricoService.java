package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.dao.ProducaoagendahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoagendahistoricoService extends GenericService<Producaoagendahistorico> {

	private ProducaoagendahistoricoDAO producaoagendahistoricoDAO;
	
	public void setProducaoagendahistoricoDAO(
			ProducaoagendahistoricoDAO producaoagendahistoricoDAO) {
		this.producaoagendahistoricoDAO = producaoagendahistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoagendahistoricoDAO#findByProducaoagenda(Producaoagenda producaoagenda)
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoagendahistorico> findByProducaoagenda(Producaoagenda producaoagenda) {
		return producaoagendahistoricoDAO.findByProducaoagenda(producaoagenda);
	}

}
