package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.OtrServicoTipo;
import br.com.linkcom.sined.geral.dao.OtrServicoTipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OtrServicoTipoService extends GenericService<OtrServicoTipo>{
	protected OtrServicoTipoDAO otrServicoTipoDao;

	public void setOtrServicoTipoDao(OtrServicoTipoDAO otrServicoTipoDao) {
		this.otrServicoTipoDao = otrServicoTipoDao;
	}
	
	public List<OtrServicoTipo> findTipoServico (){
		return otrServicoTipoDao.findTipoServico();
	}
}
