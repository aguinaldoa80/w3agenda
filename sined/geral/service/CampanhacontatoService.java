package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhacontato;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.dao.CampanhacontatoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CampanhacontatoService extends GenericService<Campanhacontato>{
	
	protected CampanhacontatoDAO campanhacontatoDAO;
	
	public void setCampanhacontatoDAO(CampanhacontatoDAO campanhacontatoDAO) {
		this.campanhacontatoDAO = campanhacontatoDAO;
	}
	
	
	public List<Campanhacontato> findByCampanha(Campanha campanha){
		return campanhacontatoDAO.findByCampanha(campanha);
	}
	
	public List<Campanhacontato> loadByContacrm(Contacrm contacrm){
		return campanhacontatoDAO.loadByContacrm(contacrm);
	}
	
	public void saveOrUpdateListContacrmcontato(Campanha campanha, List<Campanhacontato> listaCampanhacontato){
		if(listaCampanhacontato == null){
			return;
		}
		if(campanha == null || campanha.getCdcampanha() == null){
			throw new SinedException("A refer�ncia da lista n�o pode ser null.");
		}
		
		List<Campanhacontato> listaAtual = this.findByCampanha(campanha);
		List<Campanhacontato> listaDelete = new ArrayList<Campanhacontato>();
			
		for (Campanhacontato cc : listaAtual) {
			if(cc.getCdcampanhacontato() != null && !listaCampanhacontato.contains(cc)){
				listaDelete.add(cc);
			}
		}
		
		for (Campanhacontato cc : listaDelete) {
			this.delete(cc);
		}
		for (Campanhacontato cc : listaCampanhacontato) {
			cc.setCampanha(campanha);
			this.saveOrUpdateNoUseTransaction(cc);
		}
	}
	

}
