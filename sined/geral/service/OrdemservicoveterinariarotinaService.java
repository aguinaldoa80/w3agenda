package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Ordemservicoveterinariarotina;
import br.com.linkcom.sined.geral.dao.OrdemservicoveterinariarotinaDAO;
import br.com.linkcom.sined.util.IteracaoVeterinariaRetorno;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrdemservicoveterinariarotinaService extends GenericService<Ordemservicoveterinariarotina> {
	
	private OrdemservicoveterinariarotinaDAO ordemservicoveterinariarotinaDAO;
	private static OrdemservicoveterinariarotinaService instance;
	
	public void setOrdemservicoveterinariarotinaDAO(OrdemservicoveterinariarotinaDAO ordemservicoveterinariarotinaDAO) {
		this.ordemservicoveterinariarotinaDAO = ordemservicoveterinariarotinaDAO;
	}
	
	public static OrdemservicoveterinariarotinaService getInstance() {
		if (instance==null){
			instance = Neo.getObject(OrdemservicoveterinariarotinaService.class);
		}
		return instance;
	}	
	
	public List<Ordemservicoveterinariarotina> findByOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria){
		return ordemservicoveterinariarotinaDAO.findByOrdemservicoveterinaria(ordemservicoveterinaria);
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public List<Ordemservicoveterinariarotina> findForIteracao(){
		return ordemservicoveterinariarotinaDAO.findForIteracao();
	}
	
	/**
	 * 
	 * @author Thiago Clemente
	 * 
	 */
	public void updateIteracao(Ordemservicoveterinariarotina ordemservicoveterinariarotina, IteracaoVeterinariaRetorno retorno){
		ordemservicoveterinariarotinaDAO.updateIteracao(ordemservicoveterinariarotina, retorno);
	}	
}

