package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.dao.ContaempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.contaempresa.ContaempresaRESTModel;

public class ContaempresaService extends GenericService<Contaempresa>{

	private ContaempresaDAO contaempresaDAO;

	public void setContaempresaDAO(ContaempresaDAO contaempresaDAO) {this.contaempresaDAO = contaempresaDAO;}
	
	/* singleton */
	private static ContaempresaService instance;
	public static ContaempresaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ContaempresaService.class);
		}
		return instance;
	}
	
	public List<ContaempresaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ContaempresaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ContaempresaRESTModel> lista = new ArrayList<ContaempresaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Contaempresa bean : contaempresaDAO.findForAndroid(whereIn))
				lista.add(new ContaempresaRESTModel(bean));
		}
		
		return lista;
	}

	
}
