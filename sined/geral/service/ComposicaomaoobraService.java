package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Composicaomaoobraformula;
import br.com.linkcom.sined.geral.bean.Customaoobraitem;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.dao.ComposicaomaoobraDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ComposicaomaoobraService extends GenericService<Composicaomaoobra> {
	
	private ComposicaomaoobraDAO composicaomaoobraDAO;
	private CustomaoobraitemService customaoobraitemService;
	
	public void setComposicaomaoobraDAO(
			ComposicaomaoobraDAO composicaomaoobraDAO) {
		this.composicaomaoobraDAO = composicaomaoobraDAO;
	}
	public void setCustomaoobraitemService(
			CustomaoobraitemService customaoobraitemService) {
		this.customaoobraitemService = customaoobraitemService;
	}
	
	/**
	 * M�todo que retorna o valo por hora do cargo passado por par�metro.
	 *
	 * @param cargo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public Money getCustoporhoraByCargo(Cargo cargo){
		List<Composicaomaoobra> listaByCargo = this.findByCargo(cargo);
		if(listaByCargo == null || listaByCargo.size() == 0) return null;
		try {
			return this.calculaFormulaCustoporhora(listaByCargo.get(0));
		} catch (ScriptException e) {
			return null;
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cargo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Composicaomaoobra> findByCargo(Cargo cargo) {
		return composicaomaoobraDAO.findByCargo(cargo);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/01/2015
	 */
	public List<Composicaomaoobra> findAllForFlex(){
		List<Composicaomaoobra> lista = composicaomaoobraDAO.findAllForFlex();
		for (Composicaomaoobra composicaomaoobra : lista) {
			try{
				Money custoporhora = this.calculaFormulaCustoporhora(composicaomaoobra);
				composicaomaoobra.setCustoporhora(custoporhora);
			} catch (Exception e) {}
		}
		return lista;
	}

	/**
	 * M�todo que faz o c�lculo do custo por hora de uma composi��o de m�o de obra
	 *
	 * @param composicaomaoobra
	 * @return
	 * @author Rodrigo Freitas
	 * @throws ScriptException 
	 * @since 13/01/2015
	 */
	public Money calculaFormulaCustoporhora(Composicaomaoobra composicaomaoobra) throws ScriptException {
		Double custoporhora = 0d;
		
		if(composicaomaoobra != null && composicaomaoobra.getCargo() != null){
			if(composicaomaoobra.getListaComposicaomaoobraformula() != null && 
					composicaomaoobra.getListaComposicaomaoobraformula().size() > 0){
				List<Composicaomaoobraformula> listaComposicaomaoobraformula = composicaomaoobra.getListaComposicaomaoobraformula();
				
				for (Composicaomaoobraformula composicaomaoobraformula : listaComposicaomaoobraformula) {
					if(composicaomaoobraformula.getOrdem() == null ||
							composicaomaoobraformula.getIdentificador() == null ||
							composicaomaoobraformula.getFormula() == null){
						return new Money();
					}
				}
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				List<Customaoobraitem> listaCustomaoobraitem = customaoobraitemService.findByCargo(composicaomaoobra.getCargo());
				if(SinedUtil.isListNotEmpty(listaCustomaoobraitem)){
					for (Customaoobraitem customaoobraitem : listaCustomaoobraitem) {
						if(customaoobraitem.getIdentificador() != null && customaoobraitem.getCustovenda() != null){
							engine.put("_" +customaoobraitem.getIdentificador()+"_", customaoobraitem.getCustovenda().getValue().doubleValue());
						}
					}
				}
				engine.put("salario", composicaomaoobra.getSalario() != null ? composicaomaoobra.getSalario().getValue().doubleValue() : 0d);
				
				Collections.sort(listaComposicaomaoobraformula, new Comparator<Composicaomaoobraformula>(){
					public int compare(Composicaomaoobraformula o1, Composicaomaoobraformula o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Composicaomaoobraformula composicaomaoobraformula : listaComposicaomaoobraformula) {
					String formula = composicaomaoobraformula.getFormula().replace("\"", "_");
					Object obj = engine.eval(formula);
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(composicaomaoobraformula.getIdentificador(), resultado);
					custoporhora = resultado;
				}
			}
		}
		
		return new Money(custoporhora);
	}
	
	public Composicaomaoobra criaCopia(Composicaomaoobra origem) {
		Composicaomaoobra copia = new Composicaomaoobra();
		
		origem = this.loadForEntrada(origem);
		
		copia.setCargo(origem.getCargo());
		copia.setSalario(origem.getSalario());
		
		if(SinedUtil.isListNotEmpty(origem.getListaComposicaomaoobraformula())){
			for(Composicaomaoobraformula composicaomaoobraformula : origem.getListaComposicaomaoobraformula()){
				composicaomaoobraformula.setCdcomposicaomaoobraformula(null);
				composicaomaoobraformula.setComposicaomaoobra(null);
			}
			copia.setListaComposicaomaoobraformula(origem.getListaComposicaomaoobraformula());
		}
		return copia;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ComposicaomaoobraDAO#buscarComposicaomaoobraParaCopia(Orcamento orcamento)
	*
	* @param orcamento
	* @return
	* @since 22/04/2015
	* @author Luiz Fernando
	*/
	public List<Composicaomaoobra> buscarComposicaomaoobraParaCopia(Orcamento orcamento) {
		return composicaomaoobraDAO.buscarComposicaomaoobraParaCopia(orcamento);
	} 
}
