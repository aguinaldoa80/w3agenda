package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.SaldoIcms;
import br.com.linkcom.sined.geral.dao.SaldoIcmsDAO;

public class SaldoIcmsService extends GenericService<SaldoIcms> {

	private SaldoIcmsDAO saldoIcmsDAO;
	
	public void setSaldoIcmsDAO(SaldoIcmsDAO saldoIcmsDAO) {
		this.saldoIcmsDAO = saldoIcmsDAO;
	}
	
	public Double getLastMesAnoSaldoIcmsByEmpresa(Empresa empresa, String codAjApur) {
		SaldoIcms saldoIcms = saldoIcmsDAO.getLastMesAnoSaldoIcmsByEmpresa(empresa, codAjApur);
		
		if (saldoIcms != null) {
			return saldoIcms.getValor().getValue().doubleValue();
		} else {
			return 0d;
		}
	}

	public SaldoIcms getSaldoIcmsByMesAnoEmpresaTipoUtilizacaoCredito(SaldoIcms bean) {
		return saldoIcmsDAO.getSaldoIcmsByMesAnoEmpresaTipoUtilizacaoCredito(bean);
	}
}
