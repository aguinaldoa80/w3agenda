package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.ClienteDocumentoTipo;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.DocumentotipoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.DocumentotipoWSBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.DocumentotipoOfflineJSON;
import br.com.linkcom.sined.util.rest.android.documentotipo.DocumentotipoRESTModel;

public class DocumentotipoService extends GenericService<Documentotipo>{
	
	private DocumentotipoDAO documentotipoDAO;
	private ParametrogeralService parametrogeralService;
	private ClienteDocumentoTipoService clienteDocumentoTipoService;
	private PrazopagamentoitemService prazopagamentoitemService;
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setDocumentotipoDAO(DocumentotipoDAO documentotipoDAO) {
		this.documentotipoDAO = documentotipoDAO;
	}
	public void setClienteDocumentoTipoService(ClienteDocumentoTipoService clienteDocumentoTipoService) {
		this.clienteDocumentoTipoService = clienteDocumentoTipoService;
	}
	public void setPrazopagamentoitemService(
			PrazopagamentoitemService prazopagamentoitemService) {
		this.prazopagamentoitemService = prazopagamentoitemService;
	}

	/* singleton */
	private static DocumentotipoService instance;
	public static DocumentotipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentotipoService.class);
		}
		return instance;
	}
	
	/**
	 * Renorna uma lista de cliente contendo apenas o campo Nome
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#findDescricao(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Documentotipo> findDescricao(String whereIn) {
		return documentotipoDAO.findDescricao(whereIn);
	}
	
	
	@Override
	public Documentotipo loadForEntrada(Documentotipo bean) {
		bean = super.loadForEntrada(bean);
		if(bean != null){
			Documentotipo docComLista = documentotipoDAO.loadListaForEntrada(bean);
			bean.setListaPrazoPagamentoECF(docComLista.getListaPrazoPagamentoECF());
		}
		return bean;
	}
	
	
	/**
	 * Retorna os nomes dos tipos de documento, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os centros de custo nos cabe�alhos dos relat�rios
	 *  
	 * @see #findDescricao(String)
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomesDocumentotipo(List<Documentotipo> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		String stCds = CollectionsUtil.listAndConcatenate(lista, "cddocumentotipo", ",");
		return CollectionsUtil.listAndConcatenate(this.findDescricao(stCds), "nome", ", ");
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#findNotafiscal
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documentotipo findNotafiscal(){
		return documentotipoDAO.findNotafiscal();
	}
	
	public Documentotipo findBoleto(){
		return documentotipoDAO.findBoleto();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#haveAnotherNota
	 * 
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Boolean haveAnotherNota(Documentotipo bean) {
		return documentotipoDAO.haveAnotherNota(bean);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param documentotipo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentotipo> findDocumentoTipoOrdemCompra(Documentotipo documentotipo) {
		return documentotipoDAO.findDocumentoTipoOrdemCompra(documentotipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 */
	public List<Documentotipo> findAllForFlex(){
		return documentotipoDAO.findAllForFlex();
	}

	/**
	 * Busca o tipo de documento para o faturamento do contrato quando se gera uma conta a receber.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ParametrogeralService#getValorPorNome
	 * @see br.com.linkcom.sined.geral.service.DocumentotipoService#findNotafiscal
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Documentotipo findForFaturamentoContareceber() {
		try{
			String id = parametrogeralService.getValorPorNome("TipoDocumentoFaturamentoNaoGerarNota");
			if(id != null && !id.equals("")){
				Documentotipo documentotipo = new Documentotipo(Integer.parseInt(id.trim()));
				documentotipo = this.load(documentotipo);
				
				if(documentotipo != null){
					return documentotipo;
				}
			}
		} catch (Exception e) {
			// SE DER ALGUM PROBLEMA QUANDO TENTAR OBTER O DOCUMENTOTIPO FAZ O NORMAL.
		}
		return this.findNotafiscal();
	}
	
		
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 20/03/2012
	 * @param documento
	 * @return
	 */
	public Documentotipo findDocumentoTipo(Documentotipo documentotipo){
		return documentotipoDAO.findDocumentoTipo(documentotipo);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 23/03/2012
	 * @return
	 */
	public List<Documentotipo> getListaDocumentoTipoUsuario(){
		return documentotipoDAO.getListaDocumentoTipoUsuario(null, null);
	}
	
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<DocumentotipoOfflineJSON> findForPVOffline() {
		List<DocumentotipoOfflineJSON> lista = new ArrayList<DocumentotipoOfflineJSON>();
		for(Documentotipo dt : documentotipoDAO.findForPVOffline())
			lista.add(new DocumentotipoOfflineJSON(dt));
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#findDocumentoTipoBoleto(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentotipo> findDocumentoTipoBoleto(String whereIn) {
		return documentotipoDAO.findDocumentoTipoBoleto(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#findByFinalizadoraEmporium(Integer finalizadoraId)
	 *
	 * @param finalizadoraId
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/04/2014
	 */
	public Documentotipo findByFinalizadoraEmporium(Integer finalizadoraId) {
		return documentotipoDAO.findByFinalizadoraEmporium(finalizadoraId);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#findAntecipacao()
	 *
	 * @return
	 * @author Luiz Fernando
	 * @since 25/10/2013
	 */
	public List<Documentotipo> findAntecipacao() {
		return documentotipoDAO.findAntecipacao();
	}

	/**
	 * 
	 * M�todo com refer�ncia no DAO
	 *
	 *@author Lucas Costa
	 *@date 21/07/2014
	 *@param documentotipo
	 *@return
	 */
	public Documentotipo findForContaReceber (Documentotipo documentotipo){
		return documentotipoDAO.findForContaReceber (documentotipo);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#loadWithContadestino(Documentotipo documentotipo)
	*
	* @param documentotipo
	* @return
	* @since 28/04/2015
	* @author Luiz Fernando
	*/
	public Documentotipo loadWithContadestino (Documentotipo documentotipo){
		return documentotipoDAO.loadWithContadestino(documentotipo);
	}
	
	/**
	* M�todo que carrega o tipo de documento do cliente. Se n�o encontrar no cliente, 
	* busca todos os tipo de acordo com a permiss�o do usu�rio
	*
	* @param cliente
	* @return
	* @since 13/05/2015
	* @author Luiz Fernando
	*/
	public List<Documentotipo> findDocumentotipoUsuarioForVenda(Cliente cliente, String whereInDocumentotipo){
		List<Documentotipo> listDocumentotipo = new ArrayList<Documentotipo>();
		boolean existsDocumentotipoByCliente = false;
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(cliente != null && cliente.getCdpessoa() != null){
			List<ClienteDocumentoTipo> listClienteDocumentoTipo = clienteDocumentoTipoService.findByClienteUsuario(cliente, usuario);
			if(SinedUtil.isListNotEmpty(listClienteDocumentoTipo)){
				List<String> listaWhereIn = whereInDocumentotipo!=null ? Arrays.asList(whereInDocumentotipo.split(",")) : new ArrayList<String>();
				for (ClienteDocumentoTipo it : listClienteDocumentoTipo) {
					if(it.getDocumentotipo() != null){
						existsDocumentotipoByCliente = true;
						if(Boolean.TRUE.equals(it.getDocumentotipo().getExibirnavenda()) || listaWhereIn.contains(it.getDocumentotipo().getCddocumentotipo().toString())){
							listDocumentotipo.add(it.getDocumentotipo());
						}
					}
				}
			}
		}
		
		if(SinedUtil.isListEmpty(listDocumentotipo) && !existsDocumentotipoByCliente){
			listDocumentotipo = getListaDocumentoTipoUsuarioForVenda(whereInDocumentotipo);
		}
		return listDocumentotipo;
	}
	
	public List<Documentotipo> findDocumentotipoUsuarioLogadoForVenda(Cliente cliente, String whereInDocumentotipo){
		return this.findDocumentotipoUsuarioForVenda(cliente, whereInDocumentotipo);
	}
	
	public List<DocumentotipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<DocumentotipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<DocumentotipoRESTModel> lista = new ArrayList<DocumentotipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Documentotipo bean : documentotipoDAO.findForAndroid(whereIn))
				lista.add(new DocumentotipoRESTModel(bean));
		}
		
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/04/2016
	 */
	public List<DocumentotipoWSBean> findForWSSOAP() {
		List<DocumentotipoWSBean> lista = new ArrayList<DocumentotipoWSBean>();
		for(Documentotipo d: documentotipoDAO.findForWSSOAP())
			lista.add(new DocumentotipoWSBean(d));
		return lista;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentotipoDAO#isAntecipacao(Documentotipo documentotipo)
	*
	* @param documentotipo
	* @return
	* @since 06/05/2016
	* @author Luiz Fernando
	*/
	public Boolean isAntecipacao(Documentotipo documentotipo) {
		return documentotipoDAO.isAntecipacao(documentotipo);
	}
	
	/**
	 * Busca formas de pagamento do cliente
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 27/05/2016
	 * 
	 */
	public List<Documentotipo> findByCliente(Cliente cliente, boolean verificaPermissaoUsuarioLogado) {
		if (cliente==null || cliente.getCdpessoa()==null){
			return new ArrayList<Documentotipo>();
		}
		return documentotipoDAO.findByCliente(cliente, verificaPermissaoUsuarioLogado);	
	}
	
	/**
	 * Busca a forma de pagamento utilizada pelo cliente na �ltima venda. 
	 *
	 * @return
	 * @author Thiago Clemente
	 * @since 03/06/2016
	 * 
	 */
	public Documentotipo getUltimaVenda(Cliente cliente) {
		if (cliente==null || cliente.getCdpessoa()==null){
			return null;
		}
		return documentotipoDAO.getUltimaVenda(cliente);
	}
	
	/**
	 * Caso algum tipo de documento n�o permita sem analise de cr�dito retorna false
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/06/2016
	 */
	public boolean getPermiteVendaSemAnaliseByWhereIn(String whereIn) {
		if(whereIn == null || whereIn.trim().equals("")) 
			return false;
		
		List<Documentotipo> listaDocumentotipo = this.findWithPermiteVendaSemAnalise(whereIn);
		
		if(listaDocumentotipo == null || listaDocumentotipo.size() == 0) 
			return false;
		
		for (Documentotipo documentotipo : listaDocumentotipo) {
			if(documentotipo.getPermitirvendasemanalisecredito() == null || !documentotipo.getPermitirvendasemanalisecredito())
				return false;
		}
		
		return true;
	}
	
	public ModelAndView ajaxInfoDocumentotipo(WebRequestContext request) {
		String whereIn = request.getParameter("whereInDocumentoTipo");
		boolean boleto = false;
		if(StringUtils.isNotBlank(whereIn)){
			List<Documentotipo> lista = findDocumentoTipoBoleto(whereIn);
			boleto = SinedUtil.isListNotEmpty(lista);
		}
		return new JsonModelAndView().addObject("boleto", boleto);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/06/2016
	 */
	private List<Documentotipo> findWithPermiteVendaSemAnalise(String whereIn) {
		return documentotipoDAO.findWithPermiteVendaSemAnalise(whereIn);
	}
	
	public boolean isMovimentacaoTaxa(Documentotipo documentotipo){
		return documentotipoDAO.isMovimentacaoTaxa(documentotipo);
	}
	
	public List<Documentotipo> findForCompra(String whereInIgnoreFlagCompra) {
		return documentotipoDAO.findForCompra(whereInIgnoreFlagCompra);
	}
	
	public List<Documentotipo> getListaDocumentoTipoUsuarioForVenda(String whereInIgnoreFlagVenda){
		return documentotipoDAO.getListaDocumentoTipoUsuarioForVenda(whereInIgnoreFlagVenda);
	}
	
	public List<Documentotipo> getListaDocumentoTipoUsuarioForCompra(String whereInIgnoreFlagCompra){
		return documentotipoDAO.getListaDocumentoTipoUsuarioForCompra(whereInIgnoreFlagCompra);
	}
	/**
	 * @param codECF
	 * @param bean
	 * @return documentos diferentes com mesmo cod de ECF
	 */
	public List<Documentotipo> findByCodECF(Integer codECF, Documentotipo bean) {
		return documentotipoDAO.findByCodECF(codECF,bean);
	}
	
	public Documentotipo loadWithPrazoApropriacao(Documentotipo bean) {
		return documentotipoDAO.loadWithPrazoApropriacao(bean);
	}
	
	public Boolean isApropriacao(WebRequestContext request, Documentotipo documentoTipo){
		Documentotipo tipo = null;
		Integer parcelas = 0;
		if(documentoTipo != null && documentoTipo.getCddocumentotipo() != null){
			tipo = this.loadWithPrazoApropriacao(documentoTipo);
			if(tipo != null && tipo.getPrazoApropriacao() != null && tipo.getPrazoApropriacao().getCdprazopagamento() != null){
				List<Prazopagamentoitem> listaParcelas = prazopagamentoitemService.findByPrazo(tipo.getPrazoApropriacao());
				if(SinedUtil.isListNotEmpty(listaParcelas)){
					parcelas = listaParcelas.size();
				}
			}
		}
		
		return tipo != null && tipo.getPrazoApropriacao() != null && !Prazopagamento.UNICA.equals(tipo.getPrazoApropriacao()) && parcelas > 1;
	}
}