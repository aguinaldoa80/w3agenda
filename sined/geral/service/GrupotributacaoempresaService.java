package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaoempresa;
import br.com.linkcom.sined.geral.dao.GrupotributacaoempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class GrupotributacaoempresaService extends GenericService<Grupotributacaoempresa>{

	private GrupotributacaoempresaDAO grupotributacaoempresaDAO;
	
	public void setGrupotributacaoempresaDAO(GrupotributacaoempresaDAO grupotributacaoempresaDAO) {
		this.grupotributacaoempresaDAO = grupotributacaoempresaDAO;
	}

	public List<Grupotributacaoempresa> findByGrupotributacao(Grupotributacao grupotributacao){
		return grupotributacaoempresaDAO.findByGrupotributacao(grupotributacao);
	}
}
