package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.geral.dao.MenumoduloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MenumoduloService extends GenericService<Menumodulo> {

	private MenumoduloDAO menumoduloDAO;
	
	public void setMenumoduloDAO(MenumoduloDAO menumoduloDAO) {
		this.menumoduloDAO = menumoduloDAO;
	}
	
	/* singleton */
	private static MenumoduloService instance;
	public static MenumoduloService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MenumoduloService.class);
		}
		return instance;
	}
	
	public List<Menumodulo> findForTag() {
		return menumoduloDAO.findForTag();
	}
	
	public List<Menumodulo> findForTag(String contexto) {
		return menumoduloDAO.findForTag(contexto);
	}

	public Menumodulo findByPrefixo(String urlprefixo, String contexto) {
		return menumoduloDAO.findByPrefixo(urlprefixo,contexto);
	}
	
}
