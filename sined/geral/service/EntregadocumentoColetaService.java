package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.EntregadocumentoColeta;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.dao.EntregadocumentoColetaDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregadocumentoColetaService extends GenericService<EntregadocumentoColeta> {

	private EntregadocumentoColetaDAO entregadocumentoColetaDAO;
	
	public void setEntregadocumentoColetaDAO(EntregadocumentoColetaDAO entregadocumentoColetaDAO) {
		this.entregadocumentoColetaDAO = entregadocumentoColetaDAO;
	}
	
	public String getWhereInNotaColeta(String whereInColeta) {
		return entregadocumentoColetaDAO.getWhereInNotaColeta(whereInColeta);
	}

	public List<EntregadocumentoColeta> findForDevolucaoByEntradafiscal(Entregadocumento entregadocumento) {
		return entregadocumentoColetaDAO.findForDevolucaoByEntradafiscal(entregadocumento);
	}

	public ColetaMaterial getColetamaterial(List<EntregadocumentoColeta> listaEntregadocumentoColeta, Entregamaterial em) {
		if(SinedUtil.isListNotEmpty(listaEntregadocumentoColeta) && em != null ){
			for(EntregadocumentoColeta entregadocumentoColeta : listaEntregadocumentoColeta){
				if(entregadocumentoColeta.getColeta() != null && SinedUtil.isListNotEmpty(entregadocumentoColeta.getColeta().getListaColetaMaterial())){
					for(ColetaMaterial coletaMaterial : entregadocumentoColeta.getColeta().getListaColetaMaterial()){
						if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
								if(coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().equals(em.getMaterial()) && !Boolean.TRUE.equals(coletaMaterial.getUtilizado()) 
										&& (itemServico.getPedidovendamaterial() == null && em.getPedidovendamaterial() == null 
												|| itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().equals(em.getPedidovendamaterial()))){
									coletaMaterial.setUtilizado(Boolean.TRUE);
									return coletaMaterial;
								}
							}
						}
//						if(coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().equals(em.getMaterial()) && !Boolean.TRUE.equals(coletaMaterial.getUtilizado()) &&
//								(coletaMaterial.getPedidovendamaterial() == null && em.getPedidovendamaterial() == null ||
//								 coletaMaterial.getPedidovendamaterial() != null && coletaMaterial.getPedidovendamaterial().equals(em.getPedidovendamaterial()))
//								 ){
//							coletaMaterial.setUtilizado(Boolean.TRUE);
//							return coletaMaterial;
//						}
					}
				}
			}
		}
		return null;
	}
	
	public Boolean possuiColetamaterial(List<EntregadocumentoColeta> listaEntregadocumentoColeta, Entregamaterial em) {
		if(SinedUtil.isListNotEmpty(listaEntregadocumentoColeta) && em != null ){
			for(EntregadocumentoColeta entregadocumentoColeta : listaEntregadocumentoColeta){
				if(entregadocumentoColeta.getColeta() != null && SinedUtil.isListNotEmpty(entregadocumentoColeta.getColeta().getListaColetaMaterial())){
					for(ColetaMaterial coletaMaterial : entregadocumentoColeta.getColeta().getListaColetaMaterial()){
						if(coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().equals(em.getMaterial())){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
