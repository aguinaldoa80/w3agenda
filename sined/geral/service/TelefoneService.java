package br.com.linkcom.sined.geral.service;

import java.util.Collection;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.dao.TelefoneDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TelefoneService extends GenericService<Telefone>{

	private TelefoneDAO telefoneDAO;
	public void setTelefoneDAO(TelefoneDAO telefoneDAO) {
		this.telefoneDAO = telefoneDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TelefoneDAO#saveOrUpdateNoTransaction(Telefone)
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	public void saveOrUpdateNoTransaction(Telefone bean) {
		telefoneDAO.saveOrUpdateNoTransaction(bean);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega a lista de telefones de uma pessoa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TelefoneDAO#carregarListaTelefone(Pessoa)
	 * @param pessoa
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Telefone> carregarListaTelefone(Pessoa pessoa, Integer cdNota) {
		return telefoneDAO.carregarListaTelefone(pessoa, cdNota);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Carrega a lista de telefones de uma pessoa.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TelefoneDAO#carregarListaTelefone(Pessoa)
	 * @param pessoa
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Telefone> carregarListaTelefone(Pessoa pessoa) {
		return telefoneDAO.carregarListaTelefone(pessoa, null);
	}
	
	/**
	 * M�todo para criar um string concatenando os campos de telefone e tipo de telefone de uma lista.
	 * 
	 * @param listaTelefones
	 * @param separador
	 * @return
	 * @author Fl�vio Tavares
	 */
	public String createListaTelefone(Collection<Telefone> listaTelefones, String separador){
		StringBuilder sb = new StringBuilder();
		
		for (Telefone tel : listaTelefones) {
			sb.append(tel.getTelefone());
			if(tel.getTelefonetipo() != null && tel.getTelefonetipo().getNome() != null){
				sb.append(" " + tel.getTelefonetipo().getNome());
			}
			
			if(separador != null) sb.append(separador);
		}
		
		return sb.toString();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TelefoneDAO#findByPessoa
	 * @param pessoa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Telefone> findByPessoa(Pessoa pessoa) {
		return telefoneDAO.findByPessoa(pessoa);
	}

	/**
	 * M�todo que retorna telefone celular da pessoa
	 * 
	 * @param pessoa
	 * @return
	 * @author Tom�s Rabelo
	*/ 
	public Telefone getTelefoneCelular(Pessoa pessoa) {
		if(pessoa != null && pessoa.getCdpessoa() != null){
			List<Telefone> listaTelefones = findByPessoa(pessoa);
			if(listaTelefones != null && !listaTelefones.isEmpty())
				for (Telefone telefone : listaTelefones) 
					if(telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR))
						return telefone;
		}
		return null;
	}

	public Telefone loadTelefoneUnicoByPessoa(Pessoa pessoa) {
		return telefoneDAO.loadTelefoneUnicoByPessoa(pessoa);
	}
}
