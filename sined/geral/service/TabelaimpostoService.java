package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.geral.bean.Tabelaimpostoitem;
import br.com.linkcom.sined.geral.bean.enumeration.ImpostoEcfEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.dao.TabelaimpostoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TabelaimpostoService extends GenericService<Tabelaimposto> {

	private TabelaimpostoDAO tabelaimpostoDAO;
	
	public void setTabelaimpostoDAO(TabelaimpostoDAO tabelaimpostoDAO) {
		this.tabelaimpostoDAO = tabelaimpostoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public List<Tabelaimposto> findByEmpresa(Empresa empresa){
		return tabelaimpostoDAO.findByEmpresa(empresa);
	}

	/**
	 * Verifica se existe o alerta para vig�ncia de tabela.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	public int diasAlertaTabela() {
		Date maxDtvigenciafim = this.getMaxDtvigenciafim();
		if(maxDtvigenciafim == null) return Integer.MAX_VALUE;
		
		Date dataAtual = SinedDateUtils.currentDate();
		return SinedDateUtils.diferencaDias(maxDtvigenciafim, dataAtual);
	}

	/**
	 * Retorna a maior data de vig�ncia das tabelas de imposto
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 06/08/2015
	 */
	private Date getMaxDtvigenciafim() {
		return tabelaimpostoDAO.getMaxDtvigenciafim();
	}
	
	/**
	 * Retorna o item referente ao c�digo passado por par�metro
	 *
	 * @param listaTabelaimpostoitem
	 * @param codigo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public Tabelaimpostoitem getTabelaimpostoitemByCodigo(List<Tabelaimpostoitem> listaTabelaimpostoitem, String codigo){
		if(!StringUtils.isBlank(codigo)){
			if(listaTabelaimpostoitem != null && listaTabelaimpostoitem.size() > 0){
				for (Tabelaimpostoitem tabelaimpostoitem : listaTabelaimpostoitem) {
					if(tabelaimpostoitem.getCodigo() != null && tabelaimpostoitem.getCodigo().equals(codigo)){
						return tabelaimpostoitem;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Retorna o percentual do imposto da tabela de imposto
	 *
	 * @param empresa
	 * @param codigonbs
	 * @param codigoitem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/08/2015
	 */
	public Double getPercentualImpostoServico(Empresa empresa, String codigonbs, String codigoitem, ImpostoEcfEnum impostoEcfEnum){
		if(StringUtils.isBlank(codigonbs) && StringUtils.isBlank(codigoitem)) return null;
		
		if(!StringUtils.isBlank(codigoitem)) codigoitem = br.com.linkcom.neo.util.StringUtils.stringCheia(codigoitem, "0", 9, false);
		if(!StringUtils.isBlank(codigonbs)) codigonbs = br.com.linkcom.neo.util.StringUtils.stringCheia(codigonbs, "0", 9, false);
		
		Tabelaimposto tabelaimposto = this.getTabelaVigenteByEmpresa(empresa, new String[]{codigonbs, codigoitem}, null, true);
		if(tabelaimposto != null && 
				tabelaimposto.getListaTabelaimpostoitem() != null && 
				tabelaimposto.getListaTabelaimpostoitem().size() > 0){
			Tabelaimpostoitem tabelaimpostoitemNBS = this.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemServicoNBS(), codigonbs);
			
			if (impostoEcfEnum != null) {
				if (ImpostoEcfEnum.FEDERAL.equals(impostoEcfEnum)) {
					return tabelaimpostoitemNBS.getNacionalfederal();
				} else if (ImpostoEcfEnum.ESTADUAL.equals(impostoEcfEnum)) {
					return tabelaimpostoitemNBS.getEstadual();
				} else if (ImpostoEcfEnum.MUNICIPAL.equals(impostoEcfEnum)) {
					return tabelaimpostoitemNBS.getMunicipal();
				}
			}
			
			if(tabelaimpostoitemNBS != null){
				Double nacionalfederal = tabelaimpostoitemNBS.getNacionalfederal() != null ? tabelaimpostoitemNBS.getNacionalfederal() : 0d;
				Double municipal = tabelaimpostoitemNBS.getMunicipal() != null ? tabelaimpostoitemNBS.getMunicipal() : 0d;
				return nacionalfederal + municipal;
			}
			
			Tabelaimpostoitem tabelaimpostoitemItem = this.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemServicoItem(), codigoitem);
			if(tabelaimpostoitemItem != null){
				Double nacionalfederal = tabelaimpostoitemItem.getNacionalfederal() != null ? tabelaimpostoitemItem.getNacionalfederal() : 0d;
				Double municipal = tabelaimpostoitemItem.getMunicipal() != null ? tabelaimpostoitemItem.getMunicipal() : 0d;
				return nacionalfederal + municipal;
			}
		}
		return null;
	}

	/**
	 * Retorna o percentual do imposto da tabela de imposto
	 *
	 * @param empresa
	 * @param origemproduto
	 * @param ncmcompleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/08/2015
	 */
	public Double getPercentualImpostoProduto(Empresa empresa, Origemproduto origemproduto, String ncmcompleto, String extipi, ImpostoEcfEnum impostoEcfEnum) {
		if(StringUtils.isBlank(ncmcompleto)) return null;
		
		ncmcompleto = br.com.linkcom.neo.util.StringUtils.stringCheia(ncmcompleto, "0", 9, false);
		
		Tabelaimposto tabelaimposto = this.getTabelaVigenteByEmpresa(empresa, new String[]{ncmcompleto}, extipi, true);
		if(tabelaimposto != null && 
				tabelaimposto.getListaTabelaimpostoitem() != null && 
				tabelaimposto.getListaTabelaimpostoitem().size() > 0){
			Tabelaimpostoitem tabelaimpostoitemProduto = this.getTabelaimpostoitemByCodigo(tabelaimposto.getListaTabelaimpostoitemProduto(), ncmcompleto);
			if(tabelaimpostoitemProduto != null){
				if (impostoEcfEnum != null) {
					if (ImpostoEcfEnum.FEDERAL.equals(impostoEcfEnum)) {
						if(origemproduto != null && (
								origemproduto.equals(Origemproduto.ESTRAGEIRA_1) ||
								origemproduto.equals(Origemproduto.ESTRAGEIRA_2) ||
								origemproduto.equals(Origemproduto.ESTRAGEIRA_6) ||
								origemproduto.equals(Origemproduto.ESTRAGEIRA_7) ||
								origemproduto.equals(Origemproduto.NACIONAL_8)
								)){
							return tabelaimpostoitemProduto.getImportadofederal();
						} else {
							return tabelaimpostoitemProduto.getNacionalfederal();
						}
					} else if (ImpostoEcfEnum.ESTADUAL.equals(impostoEcfEnum)) {
						return tabelaimpostoitemProduto.getEstadual();
					} else if (ImpostoEcfEnum.MUNICIPAL.equals(impostoEcfEnum)) {
						return tabelaimpostoitemProduto.getMunicipal();
					}
				}
				
				Double nacionalfederal = tabelaimpostoitemProduto.getNacionalfederal() != null ? tabelaimpostoitemProduto.getNacionalfederal() : 0d;
				Double importadofederal = tabelaimpostoitemProduto.getImportadofederal() != null ? tabelaimpostoitemProduto.getImportadofederal() : 0d;
				Double estadual = tabelaimpostoitemProduto.getEstadual() != null ? tabelaimpostoitemProduto.getEstadual() : 0d;
				
				if(origemproduto != null && (
						origemproduto.equals(Origemproduto.ESTRAGEIRA_1) ||
						origemproduto.equals(Origemproduto.ESTRAGEIRA_2) ||
						origemproduto.equals(Origemproduto.ESTRAGEIRA_6) ||
						origemproduto.equals(Origemproduto.ESTRAGEIRA_7) ||
						origemproduto.equals(Origemproduto.NACIONAL_8)
						)){
					return importadofederal + estadual;
				} else {
					return nacionalfederal + estadual;
				}
			}
		}
		return null;
	}

	/**
	 * Retorna a tabela vigente por empresa
	 *
	 * @param empresa
	 * @param codigo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/08/2015
	 */
	public Tabelaimposto getTabelaVigenteByEmpresa(Empresa empresa, String[] codigos, String extipi, boolean includeItens) {
		List<Tabelaimposto> listaTabelaimposto = this.findVigentesByEmpresa(empresa, codigos, extipi, includeItens);
		if(listaTabelaimposto != null && listaTabelaimposto.size() > 0) return listaTabelaimposto.get(0);
		return null;
	}
	
	/**
	 * Retorna a tabela vigente pela empresa
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/09/2015
	 */
	public Tabelaimposto getTabelaVigenteByEmpresa(Empresa empresa) {
		return getTabelaVigenteByEmpresa(empresa, null, null, false);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @param codigoncm
	 * @param codigoitem 
	 * @param codigonbs 
	 * @since 18/08/2015
	 */
	private List<Tabelaimposto> findVigentesByEmpresa(Empresa empresa, String[] codigos, String extipi, boolean includeItens) {
		return tabelaimpostoDAO.findVigentesByEmpresa(empresa, codigos, extipi, includeItens);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/10/2015
	 */
	public Boolean haveTabelaimposto() {
		return tabelaimpostoDAO.haveTabelaimposto();
	}
	
}
