package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoacaoService extends GenericService<Movimentacaoacao>{
	
	/**
	 * Cria uma lista de Movimentacaoacao para montar um checklist
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Movimentacaoacao> createListaForChecklist(){
		List<Movimentacaoacao> lista = new ArrayList<Movimentacaoacao>();
		lista.add(Movimentacaoacao.NORMAL);
		lista.add(Movimentacaoacao.CONCILIADA);
		lista.add(Movimentacaoacao.CANCELADA);
		return lista;
	}
}
