package br.com.linkcom.sined.geral.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;
import br.com.linkcom.sined.geral.dao.BancoConfiguracaoRetornoCampoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class BancoConfiguracaoRetornoCampoService extends GenericService<BancoConfiguracaoRetornoCampo>{

	private BancoConfiguracaoRetornoCampoDAO bancoConfiguracaoRetornoCampoDAO;
	
	
	public void setBancoConfiguracaoRetornoCampoDAO(BancoConfiguracaoRetornoCampoDAO bancoConfiguracaoRetornoCampoDAO) {this.bancoConfiguracaoRetornoCampoDAO = bancoConfiguracaoRetornoCampoDAO;}

	@SuppressWarnings("unchecked")
	public Set<BancoConfiguracaoRetornoCampo> getListaBancoConfiguracaoRetornoCampo() {
		Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
		
		for (BancoConfiguracaoCampoEnum campo : BancoConfiguracaoCampoEnum.values()){
			if(campo != BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE && campo != BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER && campo != BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER){
				boolean encontrouCampo = false;
				
				for (BancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo : listaBancoConfiguracaoRetornoCampo){
					if (bancoConfiguracaoRetornoCampo.getCampo().equals(campo)){
						encontrouCampo = true;
						
						bancoConfiguracaoRetornoCampo.setObrigatorio(campo.isObrigatorio());
						bancoConfiguracaoRetornoCampo.setCampoData(campo.isData());
						bancoConfiguracaoRetornoCampo.setCampoValor(campo.isValor());
						bancoConfiguracaoRetornoCampo.setCampoCalculado(campo.isCalculado());
						
						break;
					}
				}
				
				if (!encontrouCampo){
					BancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo = new BancoConfiguracaoRetornoCampo();
					bancoConfiguracaoRetornoCampo.setCampo(campo);
					bancoConfiguracaoRetornoCampo.setObrigatorio(campo.isObrigatorio());
					bancoConfiguracaoRetornoCampo.setCampoData(campo.isData());
					bancoConfiguracaoRetornoCampo.setCampoValor(campo.isValor());
					bancoConfiguracaoRetornoCampo.setCampoCalculado(campo.isCalculado());
					listaBancoConfiguracaoRetornoCampo.add(bancoConfiguracaoRetornoCampo);
				}
			}
		}
		
		@SuppressWarnings("unchecked")
		List<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampoOrdencao = (List<BancoConfiguracaoRetornoCampo>) listaBancoConfiguracaoRetornoCampo;
		Collections.sort(listaBancoConfiguracaoRetornoCampoOrdencao, new Comparator<BancoConfiguracaoRetornoCampo>() {
			public int compare(BancoConfiguracaoRetornoCampo o1, BancoConfiguracaoRetornoCampo o2) {
				return o1.getCampo().getNome().compareTo(o2.getCampo().getNome());
			}
		});
		
		return SinedUtil.listToSet(listaBancoConfiguracaoRetornoCampoOrdencao, BancoConfiguracaoRetornoCampo.class);
	}
	
	public List<BancoConfiguracaoRetornoCampo> findBySegmento(BancoConfiguracaoRetornoSegmentoDetalhe bean){
		return bancoConfiguracaoRetornoCampoDAO.findBySegmento(bean);
	}
}
