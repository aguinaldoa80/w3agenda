package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoftp;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.ServicoftpDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicoftpService extends GenericService<Servicoftp>{
	
	private ServicoftpDAO servicoftpDAO;
	
	public void setServicoftpDAO(ServicoftpDAO servicoftpDAO) {
		this.servicoftpDAO = servicoftpDAO;
	}

	@Override
	public void saveOrUpdate(Servicoftp bean) {
		boolean novoRegistro = false;
		if(bean.getCdservicoftp() == null)
			novoRegistro = true;
		
		super.saveOrUpdate(bean);
		
		if(novoRegistro)
			SinedUtil.executaBriefCase(Servicoservidortipo.FTP, AcaoBriefCase.CADASTRAR, bean.getCdservicoftp());
	}
	
	@Override
	public void delete(Servicoftp bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.FTP, AcaoBriefCase.REMOVER, bean.getCdservicoftp());
		super.delete(bean);
	}

	public int countContratomaterial(Integer cdservicoftp, Contratomaterial contratomaterial) {
		return servicoftpDAO.countContratomaterial(cdservicoftp, contratomaterial);
	}

}
