package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Composicaoorcamento;
import br.com.linkcom.sined.geral.bean.Dependenciacargocomposicao;
import br.com.linkcom.sined.geral.bean.Dependenciafaixacomposicao;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Periodoorcamentocargo;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.geral.bean.Referenciacalculo;
import br.com.linkcom.sined.geral.bean.Tipodependenciarecurso;
import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.ComposicaoorcamentoDAO;
import br.com.linkcom.sined.modulo.projeto.controller.crud.filter.ComposicaoOrcamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ComposicaoorcamentoService extends GenericService<Composicaoorcamento>{
	
	private ComposicaoorcamentoDAO composicaoorcamentoDAO;
	private RecursocomposicaoService recursocomposicaoService;
	private PeriodoorcamentocargoService periodoorcamentocargoService;
	private OrcamentoService orcamentoService;
	
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	public void setComposicaoorcamentoDAO(ComposicaoorcamentoDAO composicaoorcamentoDAO) {
		this.composicaoorcamentoDAO = composicaoorcamentoDAO;
	}
	public void setRecursocomposicaoService(RecursocomposicaoService recursocomposicaoService) {
		this.recursocomposicaoService = recursocomposicaoService;
	}
	public void setPeriodoorcamentocargoService(PeriodoorcamentocargoService periodoorcamentocargoService) {
		this.periodoorcamentocargoService = periodoorcamentocargoService;
	}
	
	/**
	 * Retorna uma lista de composi��o (somente c�digo e nome) de um determinado or�amento 
	 * para ser utilizada nos combos de composi��o das telas em Flex
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComposicaoorcamentoDAO#findByOrcamento(Orcamento)
	 * @param orcamento
	 * @return lista de Composicaoorcamento
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Composicaoorcamento> findForComboFlex(Orcamento orcamento) {
		return composicaoorcamentoDAO.findByOrcamento(orcamento);
	}
	
	/**
	 * Retorna uma lista de composi��o (somente c�digo e nome) de um determinado or�amento
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComposicaoorcamentoDAO#findByOrcamento(Orcamento)
	 * @param orcamento
	 * @return lista de Composicaoorcamento
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Composicaoorcamento> findByOrcamento(Orcamento orcamento) {
		return composicaoorcamentoDAO.findByOrcamento(orcamento);
	}	
	
	/**
	 * Retorna uma lista de composi��o de um determinado or�amento.
	 * Caso o par�metro materialSeguranca seja verdadeiro, ser�o retornadas somente as composi��es que se referem a EPI
	 * Caso o par�metro materialSeguranca seja falso, ser�o retornadas somente as composi��es que n�o se referem a EPI
	 * Caso o par�metro materialSeguranca seja nulo, ser�o retornadas todas as composi��es
	 *
	 * @see br.com.linkcom.sined.geral.dao.ComposicaoorcamentoDAO#findByOrcamentoForFlex
	 * @param orcamento
	 * @return lista de Composicaoorcamento
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Composicaoorcamento> findByOrcamentoForFlex(Orcamento orcamento, Boolean materialSeguranca) {
		return composicaoorcamentoDAO.findByOrcamentoForFlex(orcamento, materialSeguranca);
	}	
	
	/**
	 * Busca as composi��es de or�amento a partir de um filtro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ComposicaoorcamentoDAO#findForListagemFlex(ComposicaoOrcamentoFiltro)
	 * 
	 * @param filtro
	 * @return List<Composicaoorcamento>
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public List<Composicaoorcamento> findForListagemFlex(ComposicaoOrcamentoFiltro filtro) {
		return composicaoorcamentoDAO.findForListagemFlex(filtro);
	}
	
	/* 
	 * Deixar sobrescrito mesmo sem nenhuma implementa��o para ser usado
	 * na parte em flex da aplica��o.
	 */
	@Override
	public Composicaoorcamento loadForEntrada(Composicaoorcamento bean) {
		return super.loadForEntrada(bean);
	}
	
	@Override
	public void saveOrUpdate(Composicaoorcamento bean) {
		if(bean != null && bean.getCdcomposicaoorcamento() != null && bean.getCdcomposicaoorcamento().intValue() == 0) {
			bean.setCdcomposicaoorcamento(null);
		}
		
		List<Recursocomposicao> listaRecursoComposicaoBanco;
		List<Recursocomposicao> listaRecursoComposicaoForDelete = new ArrayList<Recursocomposicao>();		
			
		//Busca a lista de RecursoComposicao do banco de dados
		listaRecursoComposicaoBanco = recursocomposicaoService.findByComposicaoOrcamentoForFlex(bean);			
		
		if (bean.getListaRecursocomposicao() != null) {
			
			//Verifica quais est�o no banco de dados e n�o est�o na aplica��o, para solicitar sua remo��o					
			for (Recursocomposicao recursoComposicaoBanco : listaRecursoComposicaoBanco) {
				if (!bean.getListaRecursocomposicao().contains(recursoComposicaoBanco)) {
					listaRecursoComposicaoForDelete.add(recursoComposicaoBanco);
				}
			}			
			
			for (Recursocomposicao recursoComposicaoApp : bean.getListaRecursocomposicao()) {
				
				//Como no flex os valores nulos para tipo inteiro s�o automaticamente convertidos para o valor default do primitivo (0),
				//deve-se fazer a convers�o aqui para que o registro seja inclu�do no banco e n�o atualizado...
				if (recursoComposicaoApp.getCdrecursocomposicao() != null && recursoComposicaoApp.getCdrecursocomposicao().intValue() == 0) {
					recursoComposicaoApp.setCdrecursocomposicao(null);
				}
				
				recursoComposicaoApp.setComposicaoorcamento(bean);
				recursoComposicaoApp.setOrcamento(bean.getOrcamento());
				
				if(recursoComposicaoApp.getReferenciacalculo() == null){
					recursoComposicaoApp.setReferenciacalculo(Referenciacalculo.QUANTIDADE);
				}
				
				//Como no flex os valores nulos para tipo number s�o automaticamente convertidos para o valor default do primitivo (0),
				//deve-se fazer a convers�o aqui para que o valor correto seja cadastrado no banco
				if (recursoComposicaoApp.getTiporelacaorecurso().equals(Tiporelacaorecurso.FAIXA_DE_VALORES)) {
					recursoComposicaoApp.setQuantidade(null);
					
					for (Dependenciafaixacomposicao dependenciaFaixaComposicaoApp : recursoComposicaoApp.getListaDependenciafaixacomposicao()) {
						//Como no flex os valores nulos para tipo inteiro s�o automaticamente convertidos para o valor default do primitivo (0),
						//deve-se fazer a convers�o aqui para que o registro seja inclu�do no banco e n�o atualizado...					
						if (dependenciaFaixaComposicaoApp.getCddependenciafaixacomposicao() != null && 
								dependenciaFaixaComposicaoApp.getCddependenciafaixacomposicao().intValue() == 0) {
							dependenciaFaixaComposicaoApp.setCddependenciafaixacomposicao(null);
						}
					}
				}

				if (recursoComposicaoApp.getTipodependenciarecurso().equals(Tipodependenciarecurso.SELECAO_CARGOS)) {
					for (Dependenciacargocomposicao dependenciaCargoComposicaoApp : recursoComposicaoApp.getListaDependenciacargocomposicao()) {
						//Como no flex os valores nulos para tipo inteiro s�o automaticamente convertidos para o valor default do primitivo (0),
						//deve-se fazer a convers�o aqui para que o registro seja inclu�do no banco e n�o atualizado...					
						if (dependenciaCargoComposicaoApp.getCddependenciacargocomposicao() != null &&
								dependenciaCargoComposicaoApp.getCddependenciacargocomposicao().intValue() == 0) {
							dependenciaCargoComposicaoApp.setCddependenciacargocomposicao(null);
						}
					}
				}
				//Recursos que n�o possuem depend�ncia tamb�m n�o possuir�o f�rmula de c�lculo das quantidades
				else if (recursoComposicaoApp.getTipodependenciarecurso().equals(Tipodependenciarecurso.SEM_DEPENDENCIA)) {
					recursoComposicaoApp.setFormulacomposicao(null);
				}
			}
		}
		
		composicaoorcamentoDAO.saveListaComposicaoOrcamentoForFlex(bean, listaRecursoComposicaoForDelete);		
		
		if (!Boolean.TRUE.equals(bean.getMaterialseguranca())) {
			Orcamento orcamento = orcamentoService.loadForEntrada(bean.getOrcamento());
			if(orcamento.getCalcularhistograma()){
				List<Periodoorcamentocargo> listaPeriodoOrcamentoCargo = periodoorcamentocargoService.findByOrcamentoFlex(bean.getOrcamento());		
				if (listaPeriodoOrcamentoCargo != null && !listaPeriodoOrcamentoCargo.isEmpty()) {
					//Atualiza as quantidades dos recursos gerais baseado no histograma.
					recursocomposicaoService.atualizaOrcamentoRecursoGeralIndiretoFlex(bean.getOrcamento(), bean.getListaRecursocomposicao(), listaPeriodoOrcamentoCargo);
				} else {
					recursocomposicaoService.atualizaMateriais(bean.getListaRecursocomposicao());
				}
			} else {
				Map<Cargo, Double> mapa = periodoorcamentocargoService.carregaListaCargoQuantidadeSemHistograma(orcamento);
				if(mapa.size() > 0){
					recursocomposicaoService.atualizaOrcamentoRecursoGeralIndiretoFlex(orcamento, bean.getListaRecursocomposicao(), mapa);
				} else {
					recursocomposicaoService.atualizaMateriais(bean.getListaRecursocomposicao());
				}
			}
		}
	}
	
	/**
	 * Deleta a composi��o do or�amento.
	 *
	 * @param composicaoorcamento
	 * @author Rodrigo Alvarenga
	 */
	public void deleteFlex(Composicaoorcamento composicaoorcamento){
		delete(composicaoorcamento);
	}
	
	/**
	 * Calcula o custo total dos recursos de uma determinada composi��o do or�amento
	 * 
	 * @see br.com.linkcom.sined.geral.service.RecursocomposicaoService#findRecursoByComposicao(Composicaoorcamento)
	 * 
	 * @param composicaoorcamento
	 * @return Double
	 * 
	 * @author Rodrigo Alvarenga
	 */	
	public Double calculaCustoTotalComposicao(Composicaoorcamento composicaoorcamento) {
		Double custoTotal = 0d;
		
		List<Recursocomposicao> listaRecursoComposicao = recursocomposicaoService.findRecursoByComposicao(composicaoorcamento);
		if (listaRecursoComposicao != null) {
			for (Recursocomposicao recursoComposicao : listaRecursoComposicao) {
				custoTotal += 
					(recursoComposicao.getQuantidadecalculada() != null ? recursoComposicao.getQuantidadecalculada() : 0d) * 
					(recursoComposicao.getNumocorrencia() != null ? recursoComposicao.getNumocorrencia() : 0d) * 
					(recursoComposicao.getCustounitario() != null ? recursoComposicao.getCustounitario().getValue().doubleValue() : 0d);
			}
		}		
		return custoTotal;
	}
	
	public List<Composicaoorcamento> findForCopiaOrcamento(String whereIn){
		return composicaoorcamentoDAO.findForCopiaOrcamento(whereIn);
	}
}
