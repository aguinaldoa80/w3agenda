package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Fornecedorempresa;
import br.com.linkcom.sined.geral.dao.FornecedorempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FornecedorempresaService extends GenericService<Fornecedorempresa> {

	private FornecedorempresaDAO fornecedorempresaDAO;
	
	public void setFornecedorempresaDAO(FornecedorempresaDAO fornecedorempresaDAO) {
		this.fornecedorempresaDAO = fornecedorempresaDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *  
	 * @see br.com.linkcom.sined.geral.dao.FornecedorempresaDAO#findByFornecedor
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Fornecedorempresa> findByFornecedor(Fornecedor form) {
		return fornecedorempresaDAO.findByFornecedor(form);
	}


}
