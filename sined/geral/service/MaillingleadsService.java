package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.dao.MaillingleadsDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingleadsFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaillingleadsService extends GenericService<Lead> {
	
	private MaillingleadsDAO maillingleadsDAO;
	
	public void setMaillingleadsDAO(MaillingleadsDAO maillingleadsDAO) {
		this.maillingleadsDAO = maillingleadsDAO;
	}
	
	@Override
	public void saveOrUpdate(Lead bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	public void delete(Lead bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/10/2012
	 */
	public List<Lead> findListagem(String whereIn, String orderBy, boolean asc) {
		return maillingleadsDAO.findListagem(whereIn, orderBy, asc);
	}

	public void processarListagem(ListagemResult<Lead> listagemResult, MaillingleadsFiltro filtro) {
		List<Lead> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdlead", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(findListagem(whereIn, filtro.getOrderBy(), filtro.isAsc()));
			
			for(Lead lead : list){
				if(lead.getListleademail()!= null){
					lead.setListaEmails(CollectionsUtil.listAndConcatenate(lead.getListleademail(), "email", "<br>"));
					if(StringUtils.isNotEmpty(lead.getEmail())){
						if(StringUtils.isNotEmpty(lead.getListaEmails())){
							lead.setListaEmails(lead.getEmail() + "<br>" + lead.getListaEmails());
						}else {
							lead.setListaEmails(lead.getEmail());
						}
					}
				}
			}
		}
	}
	
	@Override
	protected ListagemResult<Lead> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Lead> listagemResult = super.findForExportacao(filtro);
		processarListagem(listagemResult, (MaillingleadsFiltro) filtro);
		
		return listagemResult;
	}
}
