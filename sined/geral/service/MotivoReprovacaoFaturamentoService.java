package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.dao.MotivoReprovacaoFaturamentoDAO;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoReprovacaoFaturamentoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MotivoReprovacaoFaturamentoService extends GenericService<MotivoReprovacaoFaturamento> {

	private MotivoReprovacaoFaturamentoDAO motivoReprovacaoFaturamentoDAO;
	public void setMotivoReprovacaoFaturamentoDAO(
			MotivoReprovacaoFaturamentoDAO motivoReprovacaoFaturamentoDAO) {
		this.motivoReprovacaoFaturamentoDAO = motivoReprovacaoFaturamentoDAO;
	}
	
	
	public StringBuilder gerarCsvListagem(MotivoReprovacaoFaturamentoFiltro filtro) {

		List<MotivoReprovacaoFaturamento> listaMotivos = motivoReprovacaoFaturamentoDAO.findByFiltro(filtro);
		StringBuilder dados = new StringBuilder();
		dados = gerarTituloCSV();
		
		if(listaMotivos != null && !listaMotivos.isEmpty()){
			for (MotivoReprovacaoFaturamento motivo : listaMotivos) {
				dados.append(motivo.getDescricao()+";");
				if(motivo.getAtivo()){
					dados.append("sim;");
				}else {
					dados.append("n�o;");
				}
				dados.append("\n");
			}
		}
		return dados;
	}


	private StringBuilder gerarTituloCSV() {
		StringBuilder titulo = new StringBuilder();
		titulo.append("Descricao;");
		titulo.append("Ativo;");
		titulo.append("\n");
		return titulo;
	}


	public List<MotivoReprovacaoFaturamento> findAllAtivos() {
		return motivoReprovacaoFaturamentoDAO.findAllAtivos();
	}

}
