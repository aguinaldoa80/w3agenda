package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Atividade;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Diarioobra;
import br.com.linkcom.sined.geral.bean.Diarioobraarquivo;
import br.com.linkcom.sined.geral.bean.Diarioobramaterial;
import br.com.linkcom.sined.geral.bean.Diarioobramdo;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipocargo;
import br.com.linkcom.sined.geral.bean.enumeration.Tempo;
import br.com.linkcom.sined.geral.dao.DiarioobraDAO;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraAtividadesReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraRecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraTipoAtividadesReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DiarioobraService extends GenericService<Diarioobra> {

	private DiarioobraDAO diarioobraDAO;
	private AtividadeService atividadeService;
	private ApontamentoService apontamentoService;
	private ProjetoService projetoService;
	private ContratoService contratoService;
	private PlanejamentoService planejamentoService;
	private ReportTemplateService reportTemplateService;
	
	public void setReportTemplateService(
			ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	public void setPlanejamentoService(PlanejamentoService planejamentoService) {
		this.planejamentoService = planejamentoService;
	}
	public void setContratoService(ContratoService contratoService) {
		this.contratoService = contratoService;
	}
	public void setApontamentoService(ApontamentoService apontamentoService) {
		this.apontamentoService = apontamentoService;
	}
	public void setAtividadeService(AtividadeService atividadeService) {
		this.atividadeService = atividadeService;
	}
	public void setDiarioobraDAO(DiarioobraDAO diarioobraDAO) {
		this.diarioobraDAO = diarioobraDAO;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/01/2014
	 */
	public List<Diarioobra> findForReport(String whereIn){
		return diarioobraDAO.findForReport(whereIn);
	}

	/**
	 * Montagem do relatorio de diario de obra
	 *
	 * @param filtro
	 * @return report
	 * @author Rodrigo Freitas
	 */
	public LinkedList<ReportTemplateBean> createReportDiarioObraConfiguravel(WebRequestContext request) {
		LinkedList<ReportTemplateBean> lista = new LinkedList<ReportTemplateBean>();
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Diarioobra> listaDiarioobra = this.findForReport(whereIn);
		
		for (Diarioobra diarioobra : listaDiarioobra) {
			if(diarioobra.getProjeto() == null || diarioobra.getProjeto().getCdprojeto() == null)
				throw new SinedException("Projeto do di�rio de obra " + diarioobra.getCddiarioobra() + " n�o selecionado");
			
			Projeto projeto = projetoService.loadForDiarioObra(diarioobra.getProjeto());
			
			ReportTemplateBean modelodiarioobra = projeto.getModelodiarioobra();
			if(modelodiarioobra == null)
				throw new SinedException("Modelo do di�rio de obra do projeto " + projeto.getNome() + " n�o selecionado.");
			
			modelodiarioobra = reportTemplateService.load(modelodiarioobra);
			
			diarioobra.setProjeto(projeto);
			DiarioObraReportBean bean = this.createDiarioObraBean(diarioobra);
			
			LinkedHashMap<String, Object> datasource = new LinkedHashMap<String, Object>();
			datasource.put("diarioobra", bean);
			
			modelodiarioobra.setDatasource(datasource);
			
			lista.add(modelodiarioobra);
		}
		
		return lista;
	}
	
	private DiarioObraReportBean createDiarioObraBean(Diarioobra diarioobra) {
		DiarioObraReportBean bean = new DiarioObraReportBean();
		
		LinkedList<DiarioObraTipoAtividadesReportBean> listaTipoAtividadeBean = new LinkedList<DiarioObraTipoAtividadesReportBean>();
		DiarioObraTipoAtividadesReportBean tipoAtividadeBean;
		DiarioObraAtividadesReportBean atividadeBean;
		Integer idx;
		List<Atividade> listaAtividade = atividadeService.findByData(diarioobra.getDtdia(), diarioobra.getProjeto());
		
		Contrato contrato = contratoService.findLastByProjeto(diarioobra.getProjeto());
		if(contrato != null && contrato.getCdcontrato() != null){
			if(contrato.getIdentificador() != null && !"".equals(contrato.getIdentificador())){
				bean.setNumContrato(contrato.getIdentificador());
			}else {
				bean.setNumContrato(contrato.getCdcontrato().toString());
			}
			bean.setDtInicioContrato(contrato.getDtinicio());
			bean.setDtTerminoContrato(contrato.getDtfim());
		}
		
		Planejamento planejamento = null;
		List<Planejamento> findAutorizadosByProjeto = planejamentoService.findAutorizadosByProjeto(diarioobra.getProjeto());
		if(findAutorizadosByProjeto != null && findAutorizadosByProjeto.size() > 0)
			planejamento = findAutorizadosByProjeto.get(0);
		
		if(planejamento != null){
			bean.setDtMobilizacao(planejamento.getDtinicio());
		}
				
		
		//VERIFICA A LISTA DE MAO DE OBRA - SE N�O FOR VAZIA CONSIDERA A LISTA DE MAO DE OBRA SEN�O BUSCA OS DADOS DOS APONTAMENTOS
		if(diarioobra.getListaMaodeobra() != null && !diarioobra.getListaMaodeobra().isEmpty()){			
			LinkedList<DiarioObraRecursosReportBean> listaMoi = new LinkedList<DiarioObraRecursosReportBean>();
			LinkedList<DiarioObraRecursosReportBean> listaMod = new LinkedList<DiarioObraRecursosReportBean>();
			
			for(Diarioobramdo diarioobramdo : diarioobra.getListaMaodeobra()){
				if(diarioobramdo.getCargo().getTipocargo().equals(Tipocargo.MOD)){
					DiarioObraRecursosReportBean dorb = new DiarioObraRecursosReportBean();
					dorb.setCategoria(diarioobramdo.getCargo().getNome());
					dorb.setQte(diarioobramdo.getQuantidade());
					dorb.setHs(diarioobramdo.getTotalhora());
					listaMod.add(dorb);
					
				}else if(diarioobramdo.getCargo().getTipocargo().equals(Tipocargo.MOI)){
					DiarioObraRecursosReportBean dorb = new DiarioObraRecursosReportBean();
					dorb.setCategoria(diarioobramdo.getCargo().getNome());
					dorb.setQte(diarioobramdo.getQuantidade());
					dorb.setHs(diarioobramdo.getTotalhora());
					listaMoi.add(dorb);
				}
			}
			
			bean.setListaMod(listaMod);
			bean.setListaMoi(listaMoi);
		}else {
			bean.setListaMod(new LinkedList<DiarioObraRecursosReportBean>(apontamentoService.getListaDiarioObraMod(diarioobra.getDtdia(), diarioobra.getProjeto())));
			bean.setListaMoi(new LinkedList<DiarioObraRecursosReportBean>(apontamentoService.getListaDiarioObraMoi(diarioobra.getDtdia(), diarioobra.getProjeto())));
		}
		
		for (Atividade atividade : listaAtividade) {
			tipoAtividadeBean = new DiarioObraTipoAtividadesReportBean(atividade.getAtividadetipo().getNome());
			idx = null;
			if(listaTipoAtividadeBean.contains(tipoAtividadeBean)){
				idx = listaTipoAtividadeBean.indexOf(tipoAtividadeBean);
				tipoAtividadeBean = listaTipoAtividadeBean.get(idx);
			}
			atividadeBean = new DiarioObraAtividadesReportBean(atividade.getNome() + " - " + atividade.getDescricao());
			tipoAtividadeBean.getLista().add(atividadeBean);
			
			if(idx != null){
				listaTipoAtividadeBean.set(idx, tipoAtividadeBean);
			} else {
				listaTipoAtividadeBean.add(tipoAtividadeBean);
			}
		}
		
		tipoAtividadeBean = new DiarioObraTipoAtividadesReportBean("Fiscaliza��o / Cliente");
		tipoAtividadeBean.getLista().add(new DiarioObraAtividadesReportBean(" "));
		tipoAtividadeBean.getLista().add(new DiarioObraAtividadesReportBean(" "));
		tipoAtividadeBean.getLista().add(new DiarioObraAtividadesReportBean(" "));
		tipoAtividadeBean.getLista().add(new DiarioObraAtividadesReportBean(" "));
		tipoAtividadeBean.getLista().add(new DiarioObraAtividadesReportBean(" "));
		listaTipoAtividadeBean.add(tipoAtividadeBean);
		
		bean.setListaAtividade(listaTipoAtividadeBean);
		
		if(diarioobra.getListMaterial() != null && !diarioobra.getListMaterial().isEmpty()){
			LinkedList<DiarioObraRecursosReportBean> listMaterial = new LinkedList<DiarioObraRecursosReportBean>();
			
			for(Diarioobramaterial diarioobramaterial : diarioobra.getListMaterial()){
				DiarioObraRecursosReportBean dorb = new DiarioObraRecursosReportBean();
				dorb.setCategoria(diarioobramaterial.getMaterial().getNome());
				dorb.setQte(diarioobramaterial.getQuantidade());
				if(diarioobramaterial.getTotalhora() != null){
					dorb.setHs(diarioobramaterial.getTotalhora());
				}
				listMaterial.add(dorb);
			}
			
			bean.setListaEquip(listMaterial);
		}
		else if(bean.getListaEquip() != null && bean.getListaEquip().size() == 0){
			bean.getListaEquip().add(new DiarioObraRecursosReportBean());
			bean.getListaEquip().add(new DiarioObraRecursosReportBean());
			bean.getListaEquip().add(new DiarioObraRecursosReportBean());
		}
		
		if(bean.getListaMos() != null && bean.getListaMos().size() == 0){
			bean.getListaMos().add(new DiarioObraRecursosReportBean());
			bean.getListaMos().add(new DiarioObraRecursosReportBean());
			bean.getListaMos().add(new DiarioObraRecursosReportBean());
		}
		
		if(bean.getListaMod() != null && bean.getListaMod().size() == 0){
			bean.getListaMod().add(new DiarioObraRecursosReportBean());
			bean.getListaMod().add(new DiarioObraRecursosReportBean());
			bean.getListaMod().add(new DiarioObraRecursosReportBean());
		}
		
		if(bean.getListaMoi() != null && bean.getListaMoi().size() == 0){
			bean.getListaMoi().add(new DiarioObraRecursosReportBean());
			bean.getListaMoi().add(new DiarioObraRecursosReportBean());
			bean.getListaMoi().add(new DiarioObraRecursosReportBean());
		}
		
		bean.setSolManha(diarioobra.getTempomanha() != null && diarioobra.getTempomanha().equals(Tempo.BOM));
		bean.setChuvaManha(diarioobra.getTempomanha() != null && diarioobra.getTempomanha().equals(Tempo.CHUVA));
		bean.setSolTarde(diarioobra.getTempotarde() != null && diarioobra.getTempotarde().equals(Tempo.BOM));
		bean.setChuvaTarde(diarioobra.getTempotarde() != null && diarioobra.getTempotarde().equals(Tempo.CHUVA));
		bean.setSolNoite(diarioobra.getTemponoite() != null && diarioobra.getTemponoite().equals(Tempo.BOM));
		bean.setChuvaNoite(diarioobra.getTemponoite() != null && diarioobra.getTemponoite().equals(Tempo.CHUVA));
		
		bean.setHrInicioManha(diarioobra.getHrinicioparalizacaomanha() != null ? new Timestamp(diarioobra.getHrinicioparalizacaomanha().getTime()) : null);
		bean.setHrInicioTarde(diarioobra.getHrinicioparalizacaotarde() != null ? new Timestamp(diarioobra.getHrinicioparalizacaotarde().getTime()) : null);
		bean.setHrInicioNoite(diarioobra.getHrinicioparalizacaonoite() != null ? new Timestamp(diarioobra.getHrinicioparalizacaonoite().getTime()) : null);
		bean.setHrTerminoManha(diarioobra.getHrfimparalizacaomanha() != null ? new Timestamp(diarioobra.getHrfimparalizacaomanha().getTime()) : null);
		bean.setHrTerminoTarde(diarioobra.getHrfimparalizacaotarde() != null ? new Timestamp(diarioobra.getHrfimparalizacaotarde().getTime()) : null);
		bean.setHrTerminoNoite(diarioobra.getHrfimparalizacaonoite() != null ? new Timestamp(diarioobra.getHrfimparalizacaonoite().getTime()) : null);
		
		
		if(diarioobra.getHrinicioparalizacaomanha() != null && diarioobra.getHrfimparalizacaomanha() != null){
			long duracaomanha = diarioobra.getHrfimparalizacaomanha().getTime() - diarioobra.getHrinicioparalizacaomanha().getTime();
			bean.setDuracaoManha(this.formatarTimestamp(duracaomanha));
		}
		
		if(diarioobra.getHrinicioparalizacaotarde() != null && diarioobra.getHrfimparalizacaotarde() != null){
			long duracaotarde = diarioobra.getHrfimparalizacaotarde().getTime() - diarioobra.getHrinicioparalizacaotarde().getTime();
			bean.setDuracaoTarde(this.formatarTimestamp(duracaotarde));
		}
		
		if(diarioobra.getHrinicioparalizacaonoite() != null && diarioobra.getHrfimparalizacaonoite() != null){
			long duracaonoite = diarioobra.getHrfimparalizacaonoite().getTime() - diarioobra.getHrinicioparalizacaonoite().getTime();
			bean.setDuracaoNoite(this.formatarTimestamp(duracaonoite));
		}
		
		bean.setPrazoContratual(diarioobra.getDtprazocontratual());
		bean.setDiaSemana(SinedDateUtils.getDayOfWeek(diarioobra.getDtdia()));
		bean.setDiasDecorridos(diarioobra.getDiasdecorridos());
		bean.setDtDia(diarioobra.getDtdia());
		bean.setNumeroDocumento(diarioobra.getCddiarioobra());
		
		if(diarioobra.getListaDiarioobraarquivo() != null){
			LinkedList<String> listaItens = new LinkedList<String>();
			for(Diarioobraarquivo it : diarioobra.getListaDiarioobraarquivo()){
				if(it.getCddiarioobraarquivo() != null){
					listaItens.add(SinedUtil.getUrlWithContext() + "/DOWNLOADFILE/" + it.getArquivo().getCdarquivo());
				}
			}
			bean.setListaFotos(listaItens);
		}
		
		Projeto projeto = diarioobra.getProjeto();
		if(projeto != null && projeto.getCdprojeto() != null){
			bean.setObra(projeto.getNome());
			if(projeto.getMunicipio() != null){
				bean.setLocal(projeto.getMunicipio().getNomecompleto());
			}
		}
		
		Double totalEfetivo = this.getTotalEfetivo(bean);
		if(totalEfetivo != null){
			bean.setEfetivoTotalObra(totalEfetivo.intValue());
		}
		
		return bean;
	}

	private Double getTotalEfetivo(DiarioObraReportBean bean) {
		Double total = 0.0;
		if(bean.getListaMod() != null && bean.getListaMod().size() > 0){
			for (DiarioObraRecursosReportBean modBean : bean.getListaMod()) {
				if(modBean.getQte() != null && modBean.getQte() > 0){
					total += modBean.getQte();
				}
			}
		}
		if(bean.getListaMoi() != null && bean.getListaMoi().size() > 0){
			for (DiarioObraRecursosReportBean moiBean : bean.getListaMoi()) {
				if(moiBean.getQte() != null && moiBean.getQte() > 0){
					total += moiBean.getQte();
				}
			}
		}
		return total > 0 ? total : null;
	}
	
	private String formatarTimestamp(long time) {  
		StringBuffer resultado = new StringBuffer();  
		long dias = time/(1000*60*60*24);  
		time = time % (1000*60*60*24);  
		if(dias > 0) {  
			resultado.append(dias);  
			resultado.append(" DD ");  
		}  
		long horas =  time / (1000*60*60);  
		time = time % (1000*60*60);  
		if(horas < 10) {  
			resultado.append("0");  
		}  
		resultado.append(horas);  
		resultado.append(":");  
		long minutos = time/(1000*60);  
		time = time % (1000*60); 
		if(minutos < 10) {           
			resultado.append("0");  
		}  
		resultado.append(minutos);  
//		resultado.append(":");  
//		long segundos = time/1000;  
//		time = time % 1000;  
//		if(segundos < 10) {  
//			resultado.append("0");           
//		}  
//		resultado.append(segundos);  
		return resultado.toString();  
	}  
	
}
