package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.lkbanco.LkBanco;
import br.com.linkcom.lkbanco.payment.PaymentVO;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivobancario;
import br.com.linkcom.sined.geral.bean.Arquivobancariodocumento;
import br.com.linkcom.sined.geral.bean.Arquivobancariosituacao;
import br.com.linkcom.sined.geral.bean.Arquivobancariotipo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Despesaviagem;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentohistorico;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Gnre;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.NotaDocumento;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Parcela;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetodespesa;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalOrcamentoMes;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipolancamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.ContapagarDAO;
import br.com.linkcom.sined.geral.dao.DocumentoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.RelatorioMargemContribuicaoFiltro;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.BaixarContaBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.CopiaChequeReportBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.CopiaChequeSubReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.AcompanhamentoEconomicoReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarFolhaPagamentoBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ImportarFolhaPagamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContapagarService extends GenericService<Documento> {

	private static ContapagarService instance;
	private ContapagarDAO contapagarDAO;
	private DocumentoService documentoService;
	private MovimentacaoService movimentacaoService;
	private RateioService rateioService;
	private RateioitemService rateioitemService;
	private ArquivobancarioService arquivobancarioService;
	private ArquivobancariodocumentoService arquivobancariodocumentoService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private DocumentoorigemService documentoorigemService;
	private FornecimentoService fornecimentoService;
	private PessoaService pessoaService;
	private DespesaviagemService despesaviagemService;
	private EntregaService entregaService;
	private NotaDocumentoService notaDocumentoService;
	private VeiculodespesaService veiculodespesaService;
	private DocumentoDAO documentoDAO;
	private DocumentohistoricoService documentohistoricoService;
	private ColaboradorcomissaoService colaboradorcomissaoService;
	private ControleorcamentoService controleorcamentoService;
	private ControleorcamentoitemService controleorcamentoitemService;	
	private EntradafiscalService entradafiscalService;
	private EntregadocumentohistoricoService entregadocumentohistoricoService;
	private EmpresaService empresaService;
	private AvisousuarioService avisoUsuarioService;
	private AvisoService avisoService;
	
	public static ContapagarService getInstance() {
		if (instance == null)
			instance = Neo.getObject(ContapagarService.class);
		
		return instance;
	}
	
	public void setEntregaService(EntregaService entregaService) {
		this.entregaService = entregaService;
	}	
	public void setDespesaviagemService(
			DespesaviagemService despesaviagemService) {
		this.despesaviagemService = despesaviagemService;
	}
	public void setFornecimentoService(FornecimentoService fornecimentoService) {
		this.fornecimentoService = fornecimentoService;
	}
	public void setDocumentoorigemService(
			DocumentoorigemService documentoorigemService) {
		this.documentoorigemService = documentoorigemService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setArquivobancariodocumentoService(ArquivobancariodocumentoService arquivobancariodocumentoService) {
		this.arquivobancariodocumentoService = arquivobancariodocumentoService;
	}
	public void setArquivobancarioService(ArquivobancarioService arquivobancarioService) {
		this.arquivobancarioService = arquivobancarioService;
	}
	public void setRateioitemService(RateioitemService rateioitemService) {
		this.rateioitemService = rateioitemService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}	
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {
		this.movimentacaoService = movimentacaoService;
	}
	public void setContapagarDAO(ContapagarDAO contapagarDAO) {
		this.contapagarDAO = contapagarDAO;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setNotaDocumentoService(NotaDocumentoService notaDocumentoService) {
		this.notaDocumentoService = notaDocumentoService;
	}
	public void setVeiculodespesaService(
			VeiculodespesaService veiculodespesaService) {
		this.veiculodespesaService = veiculodespesaService;
	}
	public void setDocumentoDAO(DocumentoDAO documentoDAO) {
		this.documentoDAO = documentoDAO;
	}
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setColaboradorcomissaoService(
			ColaboradorcomissaoService colaboradorcomissaoService) {
		this.colaboradorcomissaoService = colaboradorcomissaoService;
	}	
	public void setControleorcamentoService(
			ControleorcamentoService controleorcamentoService) {
		this.controleorcamentoService = controleorcamentoService;
	}
	public void setControleorcamentoitemService(
			ControleorcamentoitemService controleorcamentoitemService) {
		this.controleorcamentoitemService = controleorcamentoitemService;
	}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {
		this.entradafiscalService = entradafiscalService;
	}
	public void setEntregadocumentohistoricoService(
			EntregadocumentohistoricoService entregadocumentohistoricoService) {
		this.entregadocumentohistoricoService = entregadocumentohistoricoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	
	public Documento loadForEntradaWtihDadosPessoa(Documento documento){
		Documento doc = this.loadForEntrada(documento);
		documentoService.carregaDadosPessoaDocumentoeForEntrada(doc);
		return doc;
	}

	/**
	 * Prepara a lista de NotaDocumento para salvar.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotaDocumentoService#findBy(Object, String...) 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#findByNota(br.com.linkcom.sined.geral.bean.Nota) 
	 * @param documento
	 * @author Fl�vio Tavares
	 */
	public void preparaListaNotaDocumento(Documento documento){
		List<NotaDocumento> listaNotaDocumento = new ArrayList<NotaDocumento>();
		if(SinedUtil.isObjectValid(documento)){
			listaNotaDocumento = notaDocumentoService.findBy(documento);
		}
		Integer ordem = Integer.valueOf(1);
		List<Documento> listaDocumentos = documentoService.findByNota(documento.getNota());
		if(listaDocumentos != null) ordem = listaDocumentos.size() + 1;
		
		NotaDocumento notaDocumento = new NotaDocumento();
		notaDocumento.setNota(documento.getNota());
		notaDocumento.setOrdem(ordem);
		notaDocumento.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		notaDocumento.setDtaltera(new Timestamp(System.currentTimeMillis()));
		
		listaNotaDocumento.add(notaDocumento);
		documento.setListaNotaDocumento(listaNotaDocumento);
	}
	
	@Override
	public void saveOrUpdate(Documento bean) {
		boolean financiado = bean.getFinanciamento() != null && bean.getListaDocumento() != null && bean.getListaDocumento().size() != 0 && bean.getCddocumento() == null; 
		
		if(financiado){
			String whereInEntregadocumento = bean.getWhereInEntregadocumento();
			documentoService.salvaDocumentoFinanciado(bean);
			if(Util.strings.isNotEmpty(whereInEntregadocumento)){
				List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForFaturar(whereInEntregadocumento);
				for(Entregadocumento ed : listaEntregadocumento){
					if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
						for (Entregapagamento entregapagamento : ed.getListadocumento()) {
							if(entregapagamento.getDocumentoantecipacao() != null){
								Documentoorigem documentoorigem = new Documentoorigem();
								
								documentoorigem.setEntregadocumento(ed);
								documentoorigem.setDocumento(entregapagamento.getDocumentoantecipacao());
								documentoorigemService.saveOrUpdate(documentoorigem);
							}
						}
					}
				}
				entradafiscalService.updateSituacaoEntradafiscal(whereInEntregadocumento, Entregadocumentosituacao.FATURADA);
			}
			
		}else{
			
			Entrega entrega = null;
			String whereInEntregas = bean.getWhereInEntrega();
			List<Entrega> listaEntregas = new ArrayList<Entrega>();
			if (bean.getEntrega() != null){
				entrega = bean.getEntrega();
				bean.setObservacaoHistorico("Origem entrega: <a href=\"javascript:visualizaEntrega("+ entrega.getCdentrega() +");\">"+ entrega.getCdentrega() +"</a>.");
			}
			if(whereInEntregas != null){
				listaEntregas = entregaService.listEntregaForFaturar(whereInEntregas);
				String obs = "Origem entrega: ";
				int tam = 0, i = 1;
				if(listaEntregas != null && !listaEntregas.isEmpty()){
					StringBuilder whereInEntregadocumento = new StringBuilder();
					tam = listaEntregas.size();					
					for(Entrega e : listaEntregas){
						if(e.getListaEntregadocumento() != null && !e.getListaEntregadocumento().isEmpty()){
							for(Entregadocumento ed : e.getListaEntregadocumento()){
								if(ed.getSimplesremessa() == null || !ed.getSimplesremessa()){
									if(!whereInEntregadocumento.toString().contains(ed.getCdentregadocumento().toString())){
										if(!whereInEntregadocumento.toString().equals("")) whereInEntregadocumento.append(",");
										whereInEntregadocumento.append(ed.getCdentregadocumento());
									}
								}
							}
						}
						if(i == tam){
							obs += "<a href=\"javascript:visualizaEntrega("+ e.getCdentrega() +");\">"+ e.getCdentrega() +"</a> ";
						}else{
							obs += "<a href=\"javascript:visualizaEntrega("+ e.getCdentrega() +");\">"+ e.getCdentrega() +"</a>, ";
						}
						i++;
					}
					bean.setObservacaoHistorico(obs);
					if(whereInEntregadocumento != null && !"".equals(whereInEntregadocumento.toString())){
						entradafiscalService.updateSituacaoEntradafiscal(whereInEntregadocumento.toString(), Entregadocumentosituacao.FATURADA);
					}
				}
			}
			
			List<Gnre> listaGnre = new ArrayList<Gnre>();
			String whereInGnre = bean.getWhereInGnre();
			if(whereInGnre != null && !"".equals(whereInGnre)){
				String obs = "Criada a partir do(s) registro(s) de GNRE ";
				int tam = 0, i = 1;
				if(whereInGnre.split(",").length > 0){
					for(String id : whereInGnre.split(",")){
						listaGnre.add(new Gnre(Integer.parseInt(id)));
						if(i == tam){
							obs += "<a href=\"javascript:visualizaGnre("+ id +");\">"+ id +"</a> ";
						}else{
							obs += "<a href=\"javascript:visualizaGnre("+ id +");\">"+ id +"</a>, ";
						}
						i++;
					}

					obs = obs.trim();
					if (obs.endsWith(",")){
						obs = obs.substring(0, obs.length()-1);
					}
					
					bean.setObservacaoHistorico(obs);
				}
			}
			
			String whereInEntregadocumento = bean.getWhereInEntregadocumento();
			if(whereInEntregadocumento != null && !"".equals(whereInEntregadocumento)){
				String obs = "Origem entrada fiscal: ";
				int tam = 0, i = 1;
				if(whereInEntregadocumento.split(",").length > 0){
					for(String idEntradafiscal : whereInEntregadocumento.split(",")){
						if(i == tam){
							obs += "<a href=\"javascript:visualizaEntradafiscal("+ idEntradafiscal +");\">"+ idEntradafiscal +"</a> ";
						}else{
							obs += "<a href=\"javascript:visualizaEntradafiscal("+ idEntradafiscal +");\">"+ idEntradafiscal +"</a>, ";
						}
						i++;
					}

					obs = obs.trim();
					if (obs.endsWith(",")){
						obs = obs.substring(0, obs.length()-1);
					}
					
					bean.setObservacaoHistorico(obs);
				}
			}
			
			String whereInOrdemcompra = bean.getWhereInOrdemcompra();
			if(StringUtils.isNotBlank(whereInOrdemcompra)){
				bean.setObservacaoHistorico("Origem de ordem de compra (Adiantamento) : <a href=\"javascript:visualizarOrdemcompra("+ bean.getWhereInOrdemcompra() +");\">"+ bean.getWhereInOrdemcompra() +"</a>.");
			}
			
			if(bean.getContaReceberDevolucao() != null){
				String itens[] = bean.getContaReceberDevolucao().split(",");
				//gera hist�rio na conta a pagar
				String observacao = "";
				for (String item : itens) {
					observacao += "<a href=\"Contareceber?ACAO=consultar&cddocumento="+item+"\">"+item+"</a> "; 
				}
				bean.setObservacaoHistorico("Devolu��o gerada de: "+observacao);
				
			}
			Coleta coleta = bean.getColeta();
			if(coleta != null){
				StringBuilder hist = new StringBuilder();
				hist.append("Pagamento da coleta: <a href=\"javascript:visualizarColeta(")
					.append(coleta.getCdcoleta())
					.append(");\">")
					.append(coleta.getCdcoleta())
					.append("</a>");
				bean.setObservacaoHistorico(hist.toString());
			}
			
			String whereInFornecimento = bean.getIdsFornecimento();
			if(whereInFornecimento != null && !"".equals(whereInFornecimento)){
				StringBuilder sb = new StringBuilder();
				sb.append("Origem Fornecimento: ");
				if(whereInFornecimento.split(",").length > 0){
					for(String idFornecimento : whereInFornecimento.split(",")){
						sb.append("<a href=\"javascript:visualizarFornecimento("+ idFornecimento +");\">"+ idFornecimento +"</a>, ");
					}
					sb.replace(sb.length()-2, sb.length(), "");
					bean.setObservacaoHistorico(sb.toString());
				}
			}
			
			if (bean.getFromDespesaVeiculo() != null && bean.getFromDespesaVeiculo()) {
				StringBuilder sb = new StringBuilder();
				sb.append("Origem: Despesa de ve�culo  ");
				if(bean.getIdsVeiculosDespesa() != null && bean.getIdsVeiculosDespesa().split(",").length > 0){
					for(String idVeiculoDespesa :  bean.getIdsVeiculosDespesa().split(",")){
						sb.append("<a href=\"javascript:visualizarVeiculodespesa("+ idVeiculoDespesa +");\">"+ idVeiculoDespesa +"</a>, ");
					}
					sb.replace(sb.length()-2, sb.length(), "");
					bean.setObservacaoHistorico(sb.toString());
				}
			}
			
			documentoService.atualizaHistoricoDocumento(bean, Boolean.TRUE);
			
			Boolean adiantamentoDespesa = null;
			String whereInDespesaviagem = null;
			if(bean.getWhereInDespesaviagem() != null && !bean.getWhereInDespesaviagem().equals("")) {
				whereInDespesaviagem = bean.getWhereInDespesaviagem();
				adiantamentoDespesa = bean.getAdiatamentodespesa();
			}
			
			String[] ids = null;
			if (bean.getIdsFornecimento() != null && !bean.getIdsFornecimento().equals("")) {
				ids = bean.getIdsFornecimento().split(",");
			}
			String[] idsDespesa = null;
			if (bean.getIdsVeiculosDespesa() != null && !bean.getIdsVeiculosDespesa().equals("")) {
				idsDespesa = bean.getIdsVeiculosDespesa().split(",");
			}
			
			super.saveOrUpdate(bean);
			
			if(bean.getContaReceberDevolucao() != null){
				String observacaoDevolucao = "Devolu��o gerada para: <a href=\"Contapagar?ACAO=consultar&cddocumento="+bean.getCddocumento()+"\">"+bean.getCddocumento()+"</a> ";
				String itens2[] = bean.getContaReceberDevolucao().split(",");
				for (String item2 : itens2) {
					Integer cddocumento = Integer.valueOf(item2);
					bean.setCddocumentorelacionado(bean.getCddocumento());
					documentoDAO.salvaContaDevolvida(cddocumento, observacaoDevolucao, bean.getCddocumentorelacionado());
					
					Documentohistorico documentohistorico = new Documentohistorico();
					Documento documento = documentoService.contaReceberDevolucao(cddocumento);
					
					documentohistorico.setDocumento(documento);
					documentohistorico.setDtaltera(documento.getDtaltera());
					documentohistorico.setCdusuarioaltera(documento.getCdusuarioaltera());
					documentohistorico.setDocumentoacao(documento.getDocumentoacao());
					documentohistorico.setObservacao(observacaoDevolucao);
					documentohistorico.setDocumentotipo(documento.getDocumentotipo());
					documentohistorico.setDescricao(documento.getDescricao());
					documentohistorico.setNumero(documento.getNumero());
					documentohistorico.setDtemissao(documento.getDtemissao());
					documentohistorico.setDtvencimento(documento.getDtvencimento());
					documentohistorico.setValor(documento.getValor());
					documentohistorico.setTipopagamento(Tipopagamento.CLIENTE);
					documentohistorico.setOutrospagamento(documento.getOutrospagamento());
					documentohistorico.setPessoa(documento.getPessoa());
					
					documentohistoricoService.saveOrUpdate(documentohistorico);
				}
			}
			
			Documentoorigem documentoorigem = null;
			if (entrega != null && SinedUtil.isListEmpty(listaEntregas)) {
				documentoorigem = new Documentoorigem();
				documentoorigem.setDocumento(bean);
				documentoorigem.setEntrega(entrega);
				documentoorigemService.saveOrUpdate(documentoorigem);
				
				entregaService.callProcedureAtualizaEntrega(entrega);
			}
			
			if(SinedUtil.isListNotEmpty(listaGnre)){
				for (Gnre gnre : listaGnre) {
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setGnre(gnre);
					documentoorigemService.saveOrUpdate(documentoorigem);
				}
			}

			if(coleta != null){
				documentoorigem = new Documentoorigem();
				documentoorigem.setDocumento(bean);
				documentoorigem.setColeta(coleta);
				documentoorigemService.saveOrUpdate(documentoorigem);
			}
			
			if(SinedUtil.isListNotEmpty(listaEntregas)){
				for(Entrega e: listaEntregas){
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setEntrega(e);
					documentoorigemService.saveOrUpdate(documentoorigem);
					
					entregaService.callProcedureAtualizaEntrega(e);
				}
			}
			
			if(whereInEntregadocumento != null && !"".equals(whereInEntregadocumento)){
				List<Entregadocumento> listaEntregadocumento = entradafiscalService.findForFaturar(whereInEntregadocumento);
				if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
					for(Entregadocumento ed : listaEntregadocumento){
						documentoorigem = new Documentoorigem();
						documentoorigem.setDocumento(bean);
						documentoorigem.setEntregadocumento(ed);
						documentoorigemService.saveOrUpdate(documentoorigem);
						
						Entregadocumentohistorico entregadocumentohistorico = new Entregadocumentohistorico();
						entregadocumentohistorico.setEntregadocumento(ed);
						entregadocumentohistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
						entregadocumentohistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
						entregadocumentohistorico.setEntregadocumentosituacao(Entregadocumentosituacao.FATURADA);
						entregadocumentohistorico.setObservacao("Conta a pagar: <a href=\"javascript:visualizaContapagar(" + bean.getCddocumento() +");\">"+ bean.getCddocumento() +"</a>");
						entregadocumentohistoricoService.saveOrUpdate(entregadocumentohistorico);
					}
				}
				entradafiscalService.updateSituacaoEntradafiscal(whereInEntregadocumento.toString(), Entregadocumentosituacao.FATURADA);
			}
			
			if(StringUtils.isNotBlank(whereInOrdemcompra)){
				for(String idOrdemcompra : whereInOrdemcompra.split(",")){
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setOrdemcompra(new Ordemcompra(Integer.parseInt(idOrdemcompra)));
					documentoorigemService.saveOrUpdate(documentoorigem);
				}
			}
			
			if(whereInDespesaviagem != null){
				String[] idsDespesas = whereInDespesaviagem.split(",");
				for (int i = 0; i < idsDespesas.length; i++) {
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					if (adiantamentoDespesa) {
						documentoorigem.setDespesaviagemadiantamento(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
					} else {
						documentoorigem.setDespesaviagemacerto(new Despesaviagem(Integer.parseInt(idsDespesas[i])));
						despesaviagemService.updateSituacao(idsDespesas[i], Situacaodespesaviagem.BAIXADA);
					}
					documentoorigemService.saveOrUpdate(documentoorigem);
				}
			}
			
			if (ids != null && ids.length > 0) {
				for (int i = 0; i < ids.length; i++) {
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setFornecimento(new Fornecimento(Integer.parseInt(ids[i])));
					documentoorigemService.saveOrUpdate(documentoorigem);
					
					fornecimentoService.updateBaixado(documentoorigem.getFornecimento());
				}
			}
			if (idsDespesa != null && idsDespesa.length > 0) {
				StringBuilder whereIn = new StringBuilder();
				for (int i = 0; i < idsDespesa.length; i++) {
					documentoorigem = new Documentoorigem();
					documentoorigem.setDocumento(bean);
					documentoorigem.setVeiculodespesa(new Veiculodespesa(Integer.parseInt(idsDespesa[i])));
					documentoorigemService.saveOrUpdate(documentoorigem);
					whereIn.append(idsDespesa[i]+",");
				}
				veiculodespesaService.updateFaturado(whereIn.delete(whereIn.length()-1, whereIn.length()).toString());
			}
			
		
			
			if( (bean.getFromPagamentoComissao() != null && bean.getFromPagamentoComissao()) || 
				(bean.getFromPagamentoComissaoVenda() != null && bean.getFromPagamentoComissaoVenda()) ||
				(bean.getFromPagamentoComissaoPorFaixas() != null && bean.getFromPagamentoComissaoPorFaixas()) ||
				(bean.getFromPagamentoComissaoDesempenho() != null && bean.getFromPagamentoComissaoDesempenho())) {
				
				colaboradorcomissaoService.salvarComissao(bean);
			}
		}
		documentoService.callProcedureAtualizaDocumento(bean);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#updateStatusConta
	 * @param whereIn
	 * @param documentoacao
	 * @param condicaoAcao
	 * @throws SinedException - se nenhuma conta foi atualizada.
	 * @author Rodrigo Freitas
	 */
	public void updateStatusConta(Documentoacao documentoacao, String whereIn, Documentoacao ... condicaoAcao)throws SinedException{
		contapagarDAO.updateStatusConta(documentoacao, whereIn, condicaoAcao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#updateDataVencimento
	 * @param whereIn
	 * @param documentoacao
	 * @author Rodrigo Freitas
	 */
	public void updateDataVencimento(String whereIn, Date data){
		contapagarDAO.updateDataVencimento(whereIn, data);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#updateStatusContaCartorio
	 * @param whereIn
	 * @param documentoacao
	 * @author Rodrigo Freitas
	 */
	public void updateCartorio(String whereIn, Date dtcartorio, Documentoacao acaoProtesto){
		contapagarDAO.updateCartorio(whereIn, dtcartorio, acaoProtesto);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#findContas
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findContas(String whereIn){
		return contapagarDAO.findContas(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#findContasPreechidas
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Documento> findContasPreenchidas(String whereIn){
		return contapagarDAO.findContasPreenchidas(whereIn);
	}
	
	
	/**
	 * M�todo para criar uma transaction para efetuar a baixa de uma conta a pagar/receber.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doBaixarConta(WebRequestContext, BaixarContaBean)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#callProcedureAtualizaMovimentacao(Movimentacao)
	 * @param request
	 * @param baixarContaBean
	 */
	public void transactionSaveBaixarConta(final WebRequestContext request, final BaixarContaBean baixarContaBean) {
		Movimentacao movimentacao = (Movimentacao)getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				return documentoService.doBaixarConta(request, baixarContaBean);
			}
		});
		if(movimentacao != null){
			movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
		}
	}
	
	/**
	 * M�todo para criar uma transaction para efetuar a baixa de uma conta a pagar/receber.
	 * Gerando v�rias movimenta��es
	 *
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#doBaixarContaVariasMovimentacoes(WebRequestContext, BaixarContaBean)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoService#callProcedureAtualizaMovimentacao(Movimentacao)
	 * 	 *
	 * @param request
	 * @param baixarContaBean
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	public void transactionSaveBaixarContaVariasMovimentacoes(final WebRequestContext request, final BaixarContaBean baixarContaBean, final boolean fromBaixaParcial) {
		List<Movimentacao> listaMovimentacao = (List<Movimentacao>) getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				return documentoService.doBaixarContaVariasMovimentacoes(request, baixarContaBean, fromBaixaParcial);
			}
		});
		if(listaMovimentacao != null && !listaMovimentacao.isEmpty()){
			for(Movimentacao movimentacao : listaMovimentacao){
				if(movimentacao != null){
					movimentacaoService.callProcedureAtualizaMovimentacao(movimentacao);
				}
			}
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#findListaDocumentoBaixarConta
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	protected List<Documento> findListaDocumentoBaixarConta(String whereIn) {
		return contapagarDAO.findListaDocumentoBaixarConta(whereIn);
	}
	
	/**
	 * Gera um arquivo banc�rio e salva no banco.
	 * 
	 * @param listaDocumento
	 * @param conta
	 * @author Rodrigo Freitas
	 * @throws Exception 
	 */
	protected Arquivo gerarArquivoBancario(List<Documento> listaDocumento, Conta conta) throws Exception {
		
		List<PaymentVO> listaPagamentos = criaListaPagamento(listaDocumento);
		byte[] bytes = LkBanco.generatePagamento(conta, listaPagamentos).generateResource();			
		
		
		String nome = new SimpleDateFormat("MMddHHmm").format(new Date(System.currentTimeMillis())) + ".REM";
		Arquivo arquivo = new Arquivo(bytes, nome, "text/plain");
		Arquivobancario arquivobancario = new Arquivobancario(nome, Arquivobancariotipo.REMESSA, arquivo, new Date(System.currentTimeMillis()),Arquivobancariosituacao.GERADO);
		
		
		arquivoDAO.saveFile(arquivobancario, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		arquivobancarioService.saveOrUpdate(arquivobancario);
		
		
		Arquivobancariodocumento ad = null;
		for (Documento documento: listaDocumento) {
			ad = new Arquivobancariodocumento();
			ad.setArquivobancario(arquivobancario);
			ad.setDocumento(documento);
			arquivobancariodocumentoService.saveOrUpdate(ad);
		}
	
		return arquivo;	
	}
	
	/**
	 * Cria uma lista de PaymentVO (Bean da biblioteca LkBanco).
	 * 
	 * @see br.com.linkcom.sined.geral.service.PessoaService#carregaPessoa(Pessoa)
	 * @see br.com.linkcom.sined.geral.service.PessoaService#ajustaPessoaToLoad(Pessoa)
	 * @param listaDocumento
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares
	 */
	protected List<PaymentVO> criaListaPagamento(List<Documento> listaDocumento) {
		
		PaymentVO paymentVO;
		List<PaymentVO> lista = new ArrayList<PaymentVO>();
		
		for (Documento documento : listaDocumento) {
			if(documento.getPessoa() == null){
				throw new SinedException("Sem dados banc�rios para gerar arquivo.");
			}
		
			Pessoa pessoa = pessoaService.carregaPessoa(documento.getPessoa());
			
			/**
			 * Transfere o dado bancario da lista de dados do fornecedor para a propriedade em pessoa 
			 */
			pessoaService.ajustaPessoaToLoad(pessoa);
			
			if (pessoa.getDadobancario() == null) {
				throw new SinedException("Dados banc�rios da pessoa '"+pessoa.getNome()+"' n�o informados.");
			}
			
			paymentVO = new PaymentVO(	pessoa,
										pessoa.getNome(),
										documento.getValor().getValue().doubleValue(),
										PaymentVO.TRANFERENCIA,
										documento.getCddocumento(), 
										SinedDateUtils.currentDate(),
										null,
										null,
										null,
										null,
										null);
			
			lista.add(paymentVO);
		}
		
		return lista;
	}
	
	/**
	 * M�todo que faz a jun��o dos itens de rateio das contas a pagar.
	 * - Regras:
	 * 		-> Centro de custo semelhante
	 * 		-> Projeto semelhante.
	 * 		-> Conta gerancial semelhante.
	 * 
	 * @see br.com.linkcom.sined.geral.service.RateioitemService#findByRateio(Rateio)
	 * @param listaConta
	 * @param listaTodosItens
	 * @author Rodrigo Freitas
	 */
	public void juncaoRateioItem(List<Documento> listaConta, List<Rateioitem> listaTodosItens, Map<Documento, Money> mapDocTotalTaxa) {
		
		List<Rateioitem> listaRateioItem = null;
		
		rateioService.setRateioDocumento(listaConta);
		
		for (Documento bean : listaConta) {
			//pegar todos os itens de rateio.
			listaRateioItem = bean.getRateio() != null && Hibernate.isInitialized(bean.getRateio().getListaRateioitem()) ? 
					bean.getRateio().getListaRateioitem() : null;
			
			Money valoratual = bean.getValoratualbaixaSemTaxa() != null ? bean.getValoratualbaixaSemTaxa() : bean.getValoratual()!=null && bean.getValoratual().toLong()>0 ? bean.getValoratual() : bean.getAux_documento().getValoratual();
			if(mapDocTotalTaxa != null && mapDocTotalTaxa.get(bean) != null){
				valoratual = valoratual.subtract(mapDocTotalTaxa.get(bean));
			}
			rateioService.calculaValorRateioitem(valoratual, listaRateioItem);
			
			listaTodosItens.addAll(listaRateioItem);	
		}
		rateioitemService.agruparItensRateio(listaTodosItens);
	}
	
	
	/**
	 * Seta os percentuais da lista de itens de rateio a serem listados.
	 * 
	 * @param valortotal
	 * @param listaTodosItens
	 * @author Rodrigo Freitas
	 */ 
	public void setaPercentual(Money valortotal,
			List<Rateioitem> listaTodosItens, Boolean formatarCasadecimal) {
		double doubleValue;
		Double totalRateio = 0.0;
		for (Rateioitem item : listaTodosItens) {
			totalRateio += item.getValor().getValue().doubleValue();
		}
		for (Rateioitem bean : listaTodosItens) {
			if (valortotal.getValue().doubleValue() == 0.0 || totalRateio == 0.0) {
				doubleValue = 0.0;
			} else {
				//Altera��o feita para que os valores dos itens de rateio sejam atualizados
				//de forma autom�tica na tela de baixa.
				//Quanto aos percentuais, estes n�o ser�o atualizados.
				Money total = new Money(totalRateio);
				doubleValue = new Money(100).multiply(bean.getValor()).divide(total).getValue().doubleValue();
				//C�lculo
				//doubleValue = new Money(100).multiply(bean.getValor()).divide(valortotal).getValue().doubleValue();
			}
			//Formata��o para somente duas casas decimais
			if(formatarCasadecimal != null && formatarCasadecimal)
				doubleValue = Double.parseDouble(new DecimalFormat("0.00").format(doubleValue).replace(",", "."));
			
			bean.setPercentual(doubleValue);
		}
	}
	
	@Override
	public ListagemResult<Documento> findForListagem(FiltroListagem filtro) {
		ListagemResult<Documento> listResult = super.findForListagem(filtro);
		
		//CALCULAR O SALDO ATUAL DAS CONTAS A PAGAR
//		this.calculaValorAtual(listResult.list());
		return listResult;
	}
	
	
	
	/**
	 * Calcula a taxa de juros proporcional ao valor autorizado.
	 * 
	 * TAXA * VALOR(AUTORIZADO) / VALOR(DOCUMENTO)
	 * 
	 * @param listaConta
	 * @author Rodrigo Freitas
	 */
	public void calculaTaxasProporcionais(List<Documento> listaConta) {
		if (listaConta == null) {
			throw new SinedException("N�o foi poss�vel calcular as taxas proporcionais.");
		}
		Double valorautorizado;
		Double taxa;
		Double valordocumento;
		Double taxaproporcional;
		
		for (Documento documento : listaConta) {
			valorautorizado = documento.getValorautorizado().getValue().doubleValue();
			taxa = documento.getMoneyTaxas().getValue().doubleValue();
			valordocumento = documento.getValor().getValue().doubleValue();
			taxaproporcional = (taxa * valorautorizado) / valordocumento;
			documento.setMoneyTaxas(new Money(taxaproporcional));
			documento.setValoratual(new Money(valorautorizado + taxa < 0 ? 0 : valorautorizado + taxa));
		}
		
	}
	
	/**
	 * Cria o relat�rio de c�pia de cheque de um s� documento.
	 * 
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	public IReport createCopiaChequeDOcumentoReport(WebRequestContext request) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		DecimalFormat valorformat = new DecimalFormat("R$ ###,###,##0.00");
		
		String whereIn = request.getParameter("selectedItens");
		if (whereIn.equals("")) {
			throw new SinedException("Nenhum item selecionado.");
		}
		
		List<Documento> listaConta = documentoService.findDocsCopiaCheque(whereIn);
		
		List<CopiaChequeReportBean> listaCopia = new ArrayList<CopiaChequeReportBean>();
		List<CopiaChequeSubReportBean> listaSub = null;
		CopiaChequeReportBean reportBean = null;
		CopiaChequeSubReportBean subReportBean = null;
		List<Rateioitem> listaItem = null;
		int size;
		
		for (Documento documento : listaConta) {
			reportBean = new CopiaChequeReportBean();
			
			String descricao = documento.getDescricao();
			if (SinedUtil.isListNotEmpty(documento.getListaParcela())) {
				Parcela parcela = documento.getListaParcela().iterator().next();
				if (parcela.getOrdem() != null && parcela.getParcelamento() != null && parcela.getParcelamento().getIteracoes() != null)
					descricao += " - Parc.: "
							+ parcela.getOrdem() + "/"
							+ parcela.getParcelamento().getIteracoes();
			}
			
			
			reportBean.setTipodocumento(documento.getDocumentotipo().getNome());
			reportBean.setFornecedor(documento.getPessoa() == null ? documento.getOutrospagamento() : documento.getPessoa().getNome());
			reportBean.setNumerodocumento(documento.getNumero() != null ? documento.getNumero() : "");
			reportBean.setDataemissao(format.format(documento.getDtemissao()));
			reportBean.setDatavenc(format.format(documento.getDtvencimento()));
			reportBean.setValordocumento(valorformat.format(documento.getValor().getValue().doubleValue()));
			reportBean.setDescricaodocumento(descricao);
			reportBean.setDigitadopor(((Usuario)Neo.getUser()).getNome());
			reportBean.setDataatual(format.format(new Date(System.currentTimeMillis())));
			reportBean.setCp(documento.getCddocumento().toString());
			
			if(documento.getCheque() != null){
				reportBean.setBancosacado(documento.getCheque().getBanco() != null ? documento.getCheque().getBanco().toString() : "");
				reportBean.setAgenciaconta(documento.getCheque().getAgenciacontaTrans());
				reportBean.setNumerocheque(documento.getCheque().getNumero());
			}
			
			reportBean.setEmpresa(null);
			reportBean.setLogo(null);
			
			if(documento.getEmpresa() != null && documento.getEmpresa().getRazaosocialOuNome() != null){
				reportBean.setEmpresa(documento.getEmpresa().getRazaosocialOuNome());
				
				if(documento.getEmpresa().getLogomarca() != null){
					try {
						Arquivo rubrica = ArquivoService.getInstance().loadWithContents(documento.getEmpresa().getLogomarca());
						Image image = ArquivoService.getInstance().loadAsImage(rubrica);
						reportBean.setLogo(image);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			listaSub = new ArrayList<CopiaChequeSubReportBean>();
			
//			listaItem = documento.getRateio().getListaRateioitem();
			
			Rateio rateio = documento.getRateio();
			if (rateio != null) {
				listaItem = rateioitemService.findByRateio(rateio);
			} else {
				listaItem = new ArrayList<Rateioitem>();
			}
			
			
			size = listaItem.size()/6;
			if (size>=0) {
				reportBean.setItensrateio1(CollectionsUtil.listAndConcatenate(listaItem.subList(0,listaItem.size() < 6 ? listaItem.size() : 6), "relatorio", "\n"));
			}
			if (size>=1) {
				reportBean.setItensrateio2(CollectionsUtil.listAndConcatenate(listaItem.subList(6,listaItem.size() < 12 ? listaItem.size() : 12), "relatorio", "\n"));
			}
			if (size>=2) {
				reportBean.setItensrateio3(CollectionsUtil.listAndConcatenate(listaItem.subList(12,listaItem.size() < 18 ? listaItem.size() : 18), "relatorio", "\n"));
			}
			
			
			for (Rateioitem rateioitem : listaItem) {
				subReportBean = new CopiaChequeSubReportBean();
				subReportBean.setContagerencial(rateioitem.getContagerencial().getNome());
				subReportBean.setProjeto(rateioitem.getProjeto() != null ? rateioitem.getProjeto().getNome() : "");
				subReportBean.setValor(valorformat.format(rateioitem.getValor().getValue().doubleValue()));
				listaSub.add(subReportBean);
			}
			
			reportBean.setListaItens(listaSub);

			listaCopia.add(reportBean);
		}
		
		
		Report report = new Report("financeiro/copiacheque");
		Report subreport1 = new Report("financeiro/sub_copiacheque");
		
		report.addSubReport("SUB_COPIACHEQUE", subreport1);
		report.setDataSource(listaCopia);
		
		return report;
	}
	
	/**
	 * Calcula o valor total de uma lista de documentos.
	 * 
	 * @param listaConta
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money calculaValorTotalDocumentos(List<Documento> listaConta, boolean considerarValorSemTaxa) {
		Money valortotal = new Money();
		boolean valorAdicionado = false;
		for (Documento documento : listaConta) {
			valorAdicionado = false;
			if(considerarValorSemTaxa){
				if(documento.getValoratualbaixaSemTaxa() != null){
					valortotal = valortotal.add(documento.getValoratualbaixaSemTaxa());
					valorAdicionado = true;
				}
			}
			if(!valorAdicionado) {
				valortotal = valortotal.add(documento.getValoratual()!=null ? documento.getValoratual() : documento.getValor());
			}
		}
		
		return valortotal;
	}
	
	
	/**
	 * Usado no BaixarContaProcess para manipula��o da lista de Documentos
	 * 
	 * @see #agruparItensRateio(List)
	 * @param request
	 * @param listaConta
	 * @param baixarContaBean 
	 * @return
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares - Desmembramento da parte do m�todo que agrupa os itens do rateio. 
	 */
	public BaixarContaBean manipulacaoContas(WebRequestContext request, List<Documento> listaConta, Boolean erro, BaixarContaBean baixarContaBean, Map<Documento, Money> mapDocTotalTaxa) {
		Money valortotal = this.calculaValorTotalDocumentos(listaConta, baixarContaBean.getGerarmovimentacaoparacadacontapagar() != null && baixarContaBean.getGerarmovimentacaoparacadacontapagar());
		
		if (!erro) {
			boolean formatarCasaDecimal = listaConta != null && listaConta.size() > 1;
			baixarContaBean.setRateio(this.agruparItensRateio(listaConta, formatarCasaDecimal, mapDocTotalTaxa));
		}
		request.setAttribute("valortotal", valortotal);
		
		baixarContaBean.setListaDocumento(listaConta);
		
		if(!Boolean.TRUE.equals(baixarContaBean.getGerarmovimentacaoparacadacontapagar())){
			baixarContaBean.setCheque(getChequeByConta(listaConta));
		}
		
		if(baixarContaBean.getFormapagamento() == null){
			baixarContaBean.setFormapagamento(getFormapagamentoByConta(listaConta, baixarContaBean.getDocumentoclasse()));
		}
		
		return baixarContaBean;
	}
	
	/**
	* M�todo que busca a forma de pagamento 
	*
	* @param listaConta
	* @return
	* @since 24/03/2017
	* @author Luiz Fernando
	 * @param documentoclasse 
	*/
	private Formapagamento getFormapagamentoByConta(List<Documento> listaConta, Documentoclasse documentoclasse) {
		Documentotipo documentotipo = null;
		if(listaConta != null && !listaConta.isEmpty()){
			for(Documento documento : listaConta){
				if(documento.getDocumentotipo() != null && documento.getDocumentotipo().getCddocumentotipo() != null){
					if(documentotipo != null){
						if(!documentotipo.getCddocumentotipo().equals(documento.getDocumentotipo()))
							return null;
					}else {
						documentotipo = documento.getDocumentotipo();
					}
				}
			}
		}
		
		Formapagamento formapagamento = null;
		if(documentotipo != null){
			if(Documentoclasse.OBJ_PAGAR.equals(documentoclasse)){
				formapagamento = documentotipo.getFormapagamentodebito();
			}else if(Documentoclasse.OBJ_RECEBER.equals(documentoclasse)){
				formapagamento = documentotipo.getFormapagamentocredito();
			}
		}
		return formapagamento;
	}

	/**
	 * M�todo que retorna o cheque da lista passada como par�metro caso tenha apenas um cheque
	 *  
	 * @param listaConta
	 * @return
	 * @author Luiz Fernando
	 */
	private Cheque getChequeByConta(List<Documento> listaConta) {
		Cheque cheque = null;
		if(listaConta != null && !listaConta.isEmpty()){
			for(Documento documento : listaConta){
				if(documento.getChequeBaixarconta() != null && documento.getChequeBaixarconta().getCdcheque() != null){
					if(cheque != null){
						if(!cheque.getCdcheque().equals(documento.getChequeBaixarconta().getCdcheque()))
							return null;
					}else {
						cheque = documento.getChequeBaixarconta();
					}
				}
			}
		}
		return cheque;
	}

	/**
	 * M�todo para criar um rateio com os itens de rateio agrupados dos documentos informados por par�metro.
	 * 
	 * @see #calculaValorTotalDocumentos(List)
	 * @see #juncaoRateioItem(List, List)
	 * @see #setaPercentual(Money, List)
	 * @see br.com.linkcom.sined.geral.service.RateioService#calculaValoresRateio(Rateio, Money)
	 * @param listaDocumento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Rateio agruparItensRateio(List<Documento> listaDocumento, Boolean formatarCasadecimal, Map<Documento, Money> mapDocTotalTaxa){
		Money valortotal = this.calculaValorTotalDocumentos(listaDocumento, listaDocumento.size() == 1);
		
		Rateio rateio = new Rateio();
		List<Rateioitem> listaTodosItens = new ArrayList<Rateioitem>();
		
		this.juncaoRateioItem(listaDocumento, listaTodosItens, mapDocTotalTaxa);
		
		this.setaPercentual(valortotal, listaTodosItens, formatarCasadecimal);
		rateio.setListaRateioitem(listaTodosItens);
		
		rateioService.calculaValoresRateio(rateio, valortotal);
		
		return rateio;
	}
	
	/**
	 * Calcula o valor atual de uma lista de documentos.
	 * 
	 * VALOR(DOCUMENTO) + TAXAS(JUROS)
	 *  
	 * @see br.com.linkcom.sined.geral.service.DocumentoService#obterListaValorAtual(List)
	 * @param lista
	 * @author Rodrigo Freitas
	 * @author Fl�vio Tavares - Altera��o do c�lculo do valor atual. 
	 */
	public void calculaValorAtual(List<Documento> lista){
		if (lista == null) {
			throw new SinedException("Lista de documentos n�o pode ser nulo.");
		}
//		for (Documento documento : lista) {
//			this.calculaValorAtualIndividual(documento);
//		}
		
		/**
		 * O valor atual � obtido com a fun��o VALORATUAL do banco.
		 */
		List<Documento> listaAux = documentoService.obterListaValorAtual(lista);
		for (Documento doc : listaAux) {
			Documento aux = lista.get(lista.indexOf(doc));
			aux.setValoratual(doc.getAux_documento().getValoratual());
			
			Money taxa = new Money();
			if(aux.getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
				taxa = aux.getValoratual().subtract(aux.getValorautorizado());
			}else{
				taxa = aux.getValoratual().subtract(aux.getValor());
			}
			aux.setMoneyTaxas(taxa);
			
		}
	}
	
//	/**
//	 * Calcula o valor atual e as taxas do documento passado.
//	 * 
//	 * VALOR(DOCUMENTO) + TAXAS(JUROS)
//	 * 
//	 * @see #pegarTaxas(Documento)
//	 * @param documento
//	 * @author Rodrigo Freitas
//	 */
//	public void calculaValorAtualIndividual(Documento documento) {
//		Double valoratual = null;
//		Double taxa = 0.0;
//		Taxa taxas = null;
//		Date agora = SinedDateUtils.currentDateToBeginOfDay();
//
//		if (documento.getMoneyTaxas() == null) {
//			documento.setValoratual(documento.getValor());
//		} else {
//			if (documento.getDtvencimento() == null) {
//				throw new SinedException("N�o foi poss�vel calcular o valor atual. Data de vencimento nula.");
//			}
//
//			taxas = this.pegarTaxas(documento);
//
//			if (documento.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
//				valoratual = documento.getValorautorizado().getValue().doubleValue();
//			} else {
//				valoratual = documento.getValor().getValue().doubleValue();
//			}
//
//			if (taxas != null) {
//				Set<Taxaitem> listaTaxas = taxas.getListaTaxaitem();
//
//				for (Taxaitem taxaItem : listaTaxas) {
//					if (taxaItem.getTipotaxa().equals(Tipotaxa.DESAGIO)) {
//						if (taxaItem.getDtlimite() != null && SinedDateUtils.afterIgnoreHour(taxaItem.getDtlimite(), agora)) {
//							if (taxaItem.isPercentual()) {
//								taxa -= ((taxaItem.getValor().getValue().doubleValue() / 100) * valoratual) * SinedDateUtils.calculaDiferencaDias(agora, taxaItem.getDtlimite());
//							} else {
//								taxa -= taxaItem.getValor().getValue().doubleValue() * SinedDateUtils.calculaDiferencaDias(agora, taxaItem.getDtlimite());
//							}
//						}
//					} else if (taxaItem.getTipotaxa().equals(Tipotaxa.DESCONTO)) {
//						if (taxaItem.getDtlimite() != null && SinedDateUtils.afterIgnoreHour(taxaItem.getDtlimite(), agora)) {
//							if (taxaItem.isPercentual()) {
//								taxa -= ((taxaItem.getValor().getValue().doubleValue() / 100) * valoratual);
//							} else {
//								taxa -= taxaItem.getValor().getValue().doubleValue();
//							}
//						}
//					} else if (taxaItem.getTipotaxa().equals(Tipotaxa.MULTA)) {
//						if (taxaItem.getDtlimite() != null && SinedDateUtils.beforeIgnoreHour(taxaItem.getDtlimite(), agora)) {
//							if (taxaItem.isPercentual()) {
//								taxa += ((taxaItem.getValor().getValue().doubleValue() / 100) * valoratual);
//							} else {
//								taxa += taxaItem.getValor().getValue().doubleValue();
//							}
//						}
//					} else if (taxaItem.getTipotaxa().equals(Tipotaxa.JUROS)) {
//						if (taxaItem.getDtlimite() != null && SinedDateUtils.beforeIgnoreHour(taxaItem.getDtlimite(), agora)) {
//							if (taxaItem.isPercentual()) {
//								taxa += ((taxaItem.getValor().getValue().doubleValue() / 100) * valoratual) * SinedDateUtils.calculaDiferencaDias(taxaItem.getDtlimite(), agora);
//							} else {
//								taxa += taxaItem.getValor().getValue().doubleValue() * SinedDateUtils.calculaDiferencaDias(taxaItem.getDtlimite(), agora);
//							}					
//						}
//					}
//				}
//			}
//
//
//			documento.setMoneyTaxas(new Money(taxa));
//			documento.setValoratual(new Money(valoratual+taxa < 0 ? 0 : valoratual+taxa));
//		}
//
//	}
//	
//	/**
//	 * Pega a lista de taxas do Documento, se houver. Caso contr�rio, recupera a lista do Banco.
//	 * 
//	 * @see br.com.linkcom.sined.geral.service.TaxaService#findByDocumento(Documento)
//	 * @param documento
//	 * @return
//	 * @author Hugo Ferreira
//	 */
//	private Taxa pegarTaxas(Documento documento) {
//		Taxa taxas;
//		if (documento.getTaxa() == null || documento.getTaxa().getListaTaxaitem() == null) {
//			taxas = taxaService.findByDocumento(documento);
//		} else {
//			taxas = documento.getTaxa();
//		}
//		return taxas;
//	}

	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosContaPagar(){
		Date ultimocadastro = documentoService.getDtUltimoCadastro(Documentoclasse.OBJ_PAGAR);
		Date ultimaBaixa = movimentacaoService.getDtUltimaContaPagarReceberBaixada(Documentoclasse.OBJ_PAGAR);
		Integer atrasadas = documentoService.getQtdeAtrasadas(Documentoclasse.OBJ_PAGAR);
		Integer total = documentoService.getQtdeTotalContas(Documentoclasse.OBJ_PAGAR);
		return new DadosEstatisticosBean(DadosEstatisticosBean.CONTA_PAGAR, ultimocadastro, ultimaBaixa, atrasadas, total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @autho Tom�s Rabelo
	 */
	public List<Documento> findContasPagarForBuscaGeral(String busca) {
		return contapagarDAO.findContasPagarForBuscaGeral(busca);
	}
	
	
	/**
	 * Verifica se existem mais de um cliente para a lista de itens
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Boolean validaClienteRepetido(String itens, Integer cdpessoa){
		List<Documento> documentos = documentoDAO.contaReceberDevolucao(itens);
		for (Documento item : documentos) {
			if(!item.getPessoa().getCdpessoa().equals(cdpessoa)){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verifica se existem mais de uma empresa para a lista de itens
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Boolean verificaEmpresasEmComun(String itens){
		List<Documento> documentos = documentoDAO.verificaEmpresasEmComum(itens);
		if(documentos.size() > 1){
			return false;
		}
		return true;
	}
	
	/**
	 * Verifica se existem mais de um documento para a lista de itens
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Boolean verificaDocumentosEmComun(String itens){
		List<Documento> documentos = documentoDAO.verificaDocumentosEmComun(itens);
		if(documentos.size() > 1){
			return false;
		}
		return true;
	}
	
	/**
	 * Verifica se existem mais de um tipo de documento para a lista de itens
	 * 
	 * @author Taidson
	 * @since 22/04/2010
	 */
	public Boolean verificaTiposDocumentosEmComun(String itens){
		List<Documento> documentos = documentoDAO.verificaTiposDocumentosEmComun(itens);
		if(documentos.size() > 1){
			return false;
		}
		return true;
	}

	/**
	 * Faz refer�ncia ao DAO
	 * @param usuario
	 * @return
	 * @author Taidson
	 * @since 09/06/2010
	 */
	public Boolean permiteAutorizarContaPagar(Usuario usuario){
		return contapagarDAO.permiteAutorizarContaPagar(usuario);
	}
	
	public Money getValorTotalProjeto(AcompanhamentoEconomicoReportFiltro filtro, Contagerencial contagerencial) {
		return contapagarDAO.getValorTotalProjeto(filtro, contagerencial);
	}
	
	/**
	* M�todo para calcular a comissao de acordo com a ordem de comiss�o (se tiver a ordem de comiss�o)
	*
	* @param listaContratocolaborador
	* @param pessoa
	* @param valorAtual
	* @return
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public Money calculaComissao(List<Contratocolaborador> listaContratocolaborador, Pessoa pessoa, Money valorAtual, Contrato contrato){
		if(listaContratocolaborador == null || listaContratocolaborador.isEmpty()){
			return new Money();
		}
		
		Money valordedutivocomissionamento = contrato.getValordedutivocomissionamento() != null ? contrato.getValordedutivocomissionamento() : new Money(0);
		Money valor = valorAtual.subtract(valordedutivocomissionamento);
		Money valorAntigo = valorAtual.subtract(valordedutivocomissionamento);
		int ordemComissaoAnterior = 0;		
		
		for(Contratocolaborador cc : listaContratocolaborador){
			Contrato c = cc.getContrato() != null ? cc.getContrato() : contrato;
			if(c != null && c.getComissionamento() != null && c.getComissionamento().getDeduzirvalor() != null && 
					c.getComissionamento().getDeduzirvalor()){
				if(c.getComissionamento().getPercentual() != null)
					valor = new Money(c.getComissionamento().getPercentual() * valor.getValue().doubleValue() / 100);
				else
					valor = new Money(c.getComissionamento().getValor());
				
				if(pessoa.equals(c.getVendedor()))
					return valor;
				else{
					valor = new Money(valorAtual.getValue().doubleValue() - valor.getValue().doubleValue());
					valorAntigo = valor;
					break;
				}					
			}
		}
		
		for(Contratocolaborador cc : listaContratocolaborador){
			//SE O COLABORADOR FOR A PESSOA PASSADO COMO PAR�METRO
			if(cc.getColaborador().getCdpessoa().equals(pessoa.getCdpessoa())){
				for(Contratocolaborador ccolaborador : listaContratocolaborador){
					//SE A PESSOA N�O FOR O COLABORADOR, VERIFICA SE HAVER� DEDU��O DO VALOR PARA A COMISS�O
					if(!ccolaborador.getColaborador().getCdpessoa().equals(pessoa.getCdpessoa()) && ccolaborador.getOrdemcomissao() != null && 
							cc.getOrdemcomissao() != null && ccolaborador.getOrdemcomissao() < cc.getOrdemcomissao()){
						if(ccolaborador.getComissionamento() != null && ccolaborador.getComissionamento().getDeduzirvalor() != null && 
								ccolaborador.getComissionamento().getDeduzirvalor()){
							//SE A ORDEMCOMISSAO FOR IGUAL, A DEDU��O TEM DE SER SOBRE O VALOR NAO DEDUZIDO ANTERIORMENTE
							if(ccolaborador.getOrdemcomissao() == ordemComissaoAnterior){
								if(ccolaborador.getComissionamento().getPercentual() != null)
									valor = new Money(valor.getValue().doubleValue() - (ccolaborador.getComissionamento().getPercentual() * valorAntigo.getValue().doubleValue() / 100));							
								else
									valor = new Money(valorAntigo.getValue().doubleValue() - ccolaborador.getComissionamento().getValor());
								
							}else {
								if(ccolaborador.getComissionamento() != null && ccolaborador.getComissionamento().getPercentual() != null)
									valor = new Money(valor.getValue().doubleValue() - (ccolaborador.getComissionamento().getPercentual() * valor.getValue().doubleValue() / 100));
								else
									valor = new Money(valor.getValue().doubleValue() - ccolaborador.getComissionamento().getValor());
								
								valorAntigo = valor;
							}
							ordemComissaoAnterior = ccolaborador.getOrdemcomissao();
						}
					}else if(ccolaborador.getColaborador().getCdpessoa().equals(pessoa.getCdpessoa())){
						if(ccolaborador.getComissionamento() != null){
							if(ccolaborador.getComissionamento().getPercentual() != null)
								valor = new Money(ccolaborador.getComissionamento().getPercentual() * valor.getValue().doubleValue() / 100);
							else
								valor = new Money(valor.getValue().doubleValue() - ccolaborador.getComissionamento().getValor());
						}else{
							valor = new Money(0.0);
						}
						return valor;
					}					
				}
			}
		}
		
		return valor;
		
		
	}

	/**
	 * Busca todos os registros para a exporta��o de arquivos gerenciais.
	 * @author Giovane Freitas <giovane.freitas@linkcom.com.br>
	 * @param documentoclasse 
	 */
	public Iterator<Object[]> findForArquivoGerencial(Documentoclasse documentoclasse) {
		return documentoDAO.findForArquivoGerencial(documentoclasse);
	}
	
	/**
	 * M�todo para verificar limite das Contas Gerenciais no cadastro de Conta a Pagar
	 * 
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public String verificaLimiteMesContagerencial(WebRequestContext request){
		String whereInContagerencial = request.getParameter("whereInContagerencial");
		String whereInCentrocusto = request.getParameter("whereInCentrocusto");
		String[] valores = request.getParameter("valoresContagerencial") != null ? request.getParameter("valoresContagerencial").replaceAll("\\.", "").replaceAll(",", ".").split("/") : null;
		Integer cdcontapagar = request.getParameter("cdcontapagar") != null &&  !request.getParameter("cdcontapagar").equals("") ? Integer.parseInt(request.getParameter("cdcontapagar")) : null;
		String vencimentoStr = request.getParameter("vencimento");
		Integer cdempresa = request.getParameter("cdempresa") != null && !request.getParameter("cdempresa").equals("") ? Integer.parseInt(request.getParameter("cdempresa")) : null;
		Date vencimento = null;
		try {
			vencimento = vencimentoStr != null && !vencimentoStr.equals("") ? SinedDateUtils.stringToDate(vencimentoStr) : null;
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		
		StringBuilder msgm = new StringBuilder();
		if(whereInContagerencial != null && 
				!whereInContagerencial.equals("") && 
				whereInCentrocusto != null && 
				!whereInCentrocusto.equals("") &&
				valores != null && 
				vencimento != null){
			String[] idsContagerencial = whereInContagerencial.split(",");	
			String[] idsCentrocusto = whereInCentrocusto.split(",");	
			
			HashMap<MultiKey, Double> mapContagerencialCentrocusto = new HashMap<MultiKey, Double>();
			HashMap<String, Double> mapContagerencial = new HashMap<String, Double>();
			
			for(int i = 0; i < idsContagerencial.length; i++){
				String idContegerencial = idsContagerencial[i];
				String idCentrocusto = idsCentrocusto[i];
				MultiKey multikey = new MultiKey(idContegerencial, idCentrocusto);
				Double valor = new Double(Double.parseDouble(valores[i]));
				
				if(mapContagerencialCentrocusto.containsKey(multikey)){
					mapContagerencialCentrocusto.put(multikey, mapContagerencialCentrocusto.get(multikey) + valor);
				} else {
					mapContagerencialCentrocusto.put(multikey, valor);
				}
				if(mapContagerencial.containsKey(idContegerencial)){
					mapContagerencial.put(idContegerencial, mapContagerencial.get(idContegerencial) + valor);
				} else {
					mapContagerencial.put(idContegerencial, valor);
				}
			}			
			
			List<Controleorcamentoitem> listaCOitem = controleorcamentoitemService.findByContagerencialWhereIn(
					whereInContagerencial, 
					whereInCentrocusto, 
//					Integer.parseInt(new SimpleDateFormat("yyyy").format(vencimento)),
					vencimento,
					cdempresa);			
			
			if(listaCOitem != null && !listaCOitem.isEmpty()){
				List<TotalOrcamentoMes> listaTotalOrcamentoMes = controleorcamentoService.findForControleorcamentoMes(whereInContagerencial, whereInCentrocusto, vencimento, cdcontapagar, cdempresa); 
				
				for(Controleorcamentoitem coitem : listaCOitem){
					Tipolancamento tipolancamento = coitem.getControleorcamento().getTipolancamento();
					
					boolean periodoValido = Tipolancamento.ANUAL.equals(tipolancamento) || (coitem.getMesano() != null && SinedDateUtils.equalsMesAno(coitem.getMesano(), vencimento));
					boolean haveCentrocusto = coitem.getCentrocusto() != null && coitem.getCentrocusto().getCdcentrocusto() != null;
					boolean haveContagerencial = coitem.getContagerencial() != null && coitem.getContagerencial().getCdcontagerencial() != null;
					
					if(periodoValido && haveContagerencial){
						Money valor = new Money();
						
						Money valorMes = new Money();
						if(Tipolancamento.MENSAL.equals(tipolancamento)){
							valorMes = coitem.getValor();
						}else {
							valorMes = coitem.getValor().divide(new Money(12.0));
						}
						
						Integer cdcontagerencial = coitem.getContagerencial().getCdcontagerencial();
						
						if(haveCentrocusto){
							Integer cdcentrocusto = coitem.getCentrocusto().getCdcentrocusto();
							Double valorTela = mapContagerencialCentrocusto.get(new MultiKey(cdcontagerencial.toString(), cdcentrocusto.toString()));
							if(valorTela != null){
								valor = valor.add(new Money(valorTela));
								
								if(listaTotalOrcamentoMes != null && !listaTotalOrcamentoMes.isEmpty()){
									for(TotalOrcamentoMes totalmes : listaTotalOrcamentoMes){
										if(totalmes.getCdcontagerencial() != null && 
												totalmes.getCdcontagerencial().equals(cdcontagerencial) && 
												totalmes.getCdcentrocusto() != null && 
												totalmes.getCdcentrocusto().equals(cdcentrocusto) && 
												totalmes.getValor() != null && 
												totalmes.getValor().getValue().doubleValue() > 0){
											valor = valor.add(totalmes.getValor());
										}
									}
								}
								
								if(valor != null && valorMes != null && valor.getValue().compareTo(valorMes.getValue()) > 0){
									this.montaMensagemOrcamento(msgm, coitem.getContagerencial().getNome(), coitem.getCentrocusto().getNome(), valorMes, valor);
								}
							}
						} else {
							Double valorTela = mapContagerencial.get(cdcontagerencial.toString());
							if(valorTela != null){
								valor = valor.add(new Money(valorTela));
								
								if(listaTotalOrcamentoMes != null && !listaTotalOrcamentoMes.isEmpty()){
									for(TotalOrcamentoMes totalmes : listaTotalOrcamentoMes){
										if(totalmes.getCdcontagerencial() != null && 
												totalmes.getCdcontagerencial().equals(cdcontagerencial) && 
												totalmes.getValor() != null && 
												totalmes.getValor().getValue().doubleValue() > 0){
											valor = valor.add(totalmes.getValor());
										}
									}
								}
								
								if(valor != null && valorMes != null && valor.getValue().compareTo(valorMes.getValue()) > 0){
									this.montaMensagemOrcamento(msgm, coitem.getContagerencial().getNome(), null, valorMes, valor);
								}
							}
						}
					}
				}
			}
		}
		
		return msgm.length() > 0 ? msgm.toString() + "</ul>" : "";
	}
	
	private void montaMensagemOrcamento(StringBuilder msgm, String descricaoContagerencial, String descricaoCentrocusto, Money valorMes, Money valor){
		if("".equals(msgm.toString())){
			msgm.append("<span class=\"messagetitle\">A(s) conta(s) gerencial(is) e centro(s) de custo(s) abaixo est�(�o) com o(s) valor(es) mensal(is) acima do planejamento or�ament�rio:</span><ul>");
		}
		
		msgm.append("<li class=\"globalerror\" style=\"padding-bottom: 5px;\">");
		
		if(descricaoContagerencial != null) 
			msgm.append("<u>Conta gerencial:</u> ").append(descricaoContagerencial).append("<BR/>");
		
		if(descricaoCentrocusto != null) 
			msgm.append("<u>Centro de custo:</u> ").append(descricaoCentrocusto).append("<BR/>");
		
		if(valorMes != null) 
			msgm.append("<u>Limite no M�s:</u> R$ ").append(valorMes).append("<BR/>");
		
		if(valor != null) 
			msgm.append("<u>Total das Contas a pagar:</u> R$ ").append(valor).append("<BR/>");
		
		msgm.append("</li>");
	}

	public Money getTotalDocumentoByCheque(Documento documento, Cheque cheque) {
		return contapagarDAO.getTotalDocumentoByCheque(documento, cheque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContapagarDAO#loadForRegistrarEntradafiscal(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Documento loadForRegistrarEntradafiscal(Documento documento){
		return contapagarDAO.loadForRegistrarEntradafiscal(documento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.ContapagarService#isOrigemEntregaOrEntradafiscalAvulsa(Documento documento)
	 *
	 * @param documento
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean isOrigemEntregaOrEntradafiscalAvulsa(Documento documento) {
		return contapagarDAO.isOrigemEntregaOrEntradafiscalAvulsa(documento);
	}
	
	public List<Documento> findForUpdateBaixaByDocumentoCancelado(String whereInDocumento) {
		return contapagarDAO.findForUpdateBaixaByDocumentoCancelado(whereInDocumento);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereInDocumento
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documento> findForAtualizarRateio(String whereInDocumento){
		return contapagarDAO.findForAtualizarRateio(whereInDocumento);
	}
	
	/**
	 * M�todo que monta um mapa de valores totais acumulados por projeto de acordo com as entradas em Projetodespesa.
	 * 
	 * @return
	 * @author Rafael Salvio
	 */
	public Map<Projeto, Double> montaMapaProjetosByPeso(){
		Map<Projeto, Double> mapa = new HashMap<Projeto, Double>();
		List<Projetodespesa> lista = ProjetodespesaService.getInstance().findAllForAtualizarRateio();
		if(lista != null && !lista.isEmpty()){
			for(Projetodespesa pd : lista){
				mapa.put(pd.getProjeto(), pd.getValortotal().getValue().doubleValue());
			}
		}
		return mapa;
	}
	
	/**
	 * M�todo que monta um mapa de horas totais acumulados por projeto de acordo com as entradas em Apontamentos.
	 * 
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 * @author Rafael Salvio
	 */
	public Map<Projeto, Double> montaMapaProjetosByApontamento(Date dtinicio, Date dtfim){
		Map<Projeto, Double> mapa = new HashMap<Projeto, Double>();
		List<Apontamento> lista = ApontamentoService.getInstance().findAllForAtualizarRateio(dtinicio, dtfim);
		if(lista != null && !lista.isEmpty()){
			Projeto pAnterior = lista.get(0).getPlanejamento().getProjeto();
			Double horas = 0.;
			for(Apontamento ap : lista){
				Projeto pAtual = ap.getPlanejamento().getProjeto();
				if(!pAtual.equals(pAnterior)){
					mapa.put(pAnterior, horas);
					horas = 0.;
					pAnterior = pAtual;
				}
				if(ap.getListaApontamentoHoras() != null && !ap.getListaApontamentoHoras().isEmpty()){
					for(ApontamentoHoras aph : ap.getListaApontamentoHoras()){
						horas += (aph.getHrfim().getTime() - aph.getHrinicio().getTime()) / 1000.0 / 60.0 / 60.0;
					}
				}
			}
			mapa.put(pAnterior, horas);
		}
		return mapa;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.ContapagarService#findAdiantamentoByDespesaviagem(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 16/02/2016
	* @author Luiz Fernando
	*/
	public List<Documento> findAdiantamentoByDespesaviagem(String whereIn) {
		return contapagarDAO.findAdiantamentoByDespesaviagem(whereIn);
	}
	
	public void saveTransactionImportarFolhaPagamento(final ImportarFolhaPagamentoFiltro filtro, final List<ImportarFolhaPagamentoBean> listaBean){
		contapagarDAO.saveTransactionImportarFolhaPagamento(filtro, listaBean);
	}

	public void criarAvisoContaPagarProximo(Motivoaviso m, Date data) {
		List<Documento> documentoList = contapagarDAO.findByDiasFaltantes(data);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		for (Documento d : documentoList) {
			Aviso aviso = new Aviso("Conta a pagar pr�ximo do vencimento", (!org.apache.commons.lang.StringUtils.isEmpty(d.getNumero()) ? "N�mero da conta a pagar: " + d.getNumero() : "C�digo da conta a pagar: " + d.getCddocumento()), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), d.getCddocumento(), d.getEmpresa() != null ? d.getEmpresa() : empresaService.loadPrincipal(), 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, d.getCddocumento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, d.getCddocumento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

	public void criarAvisoContaPagarAtrasado(Motivoaviso m, Date data, Date dateToSearch) {
		List<Documento> documentoList = contapagarDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		for (Documento d : documentoList) {
			Aviso aviso = new Aviso("Conta a pagar vencida", (!org.apache.commons.lang.StringUtils.isEmpty(d.getNumero()) ? "N�mero da conta a pagar: " + d.getNumero() : "C�digo da conta a pagar: " + d.getCddocumento()), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), d.getCddocumento(), d.getEmpresa() != null ? d.getEmpresa() : empresaPrincipal, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, d.getCddocumento());
			
			if (lastAvisoDate != null) {
				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, d.getCddocumento(), lastAvisoDate);
				
				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

	public boolean verificaContaCanceladaVeiculoDespesa(Veiculodespesa veiculodespesa) {
		return contapagarDAO.verificaContaCanceladaVeiculoDespesa(veiculodespesa);
	}

	public Money buscarGastoEmpresa(RelatorioMargemContribuicaoFiltro filtro) {
		return contapagarDAO.buscarGastoEmpresa(filtro);
	}
}