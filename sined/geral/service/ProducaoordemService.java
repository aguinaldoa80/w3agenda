package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Coletahistorico;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.OrdemProducaoSped;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterial;
import br.com.linkcom.sined.geral.bean.Producaoagendamaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaochaofabrica;
import br.com.linkcom.sined.geral.bean.Producaoetapa;
import br.com.linkcom.sined.geral.bean.Producaoetapaitem;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemconstatacao;
import br.com.linkcom.sined.geral.bean.Producaoordemequipamento;
import br.com.linkcom.sined.geral.bean.Producaoordemhistorico;
import br.com.linkcom.sined.geral.bean.Producaoordemitemadicional;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialcampo;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialmateriaprima;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialoperador;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialorigem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialperdadescarte;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialproduzido;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.auxiliar.ProducaoordemmaterialMaterialprimaPerdaBean;
import br.com.linkcom.sined.geral.bean.enumeration.BaixarEstoqueMateriasPrimasEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.SitucaoPneuEtiqueteEnum;
import br.com.linkcom.sined.geral.dao.ProducaoordemDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ConferenciaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ConsumoMateriaprimaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.MateriaprimaConferenciaProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.MateriaprimaConsumoProducaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoitemBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.filter.ProducaoordemFiltro;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.EmitirProducaoordemReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoordemmaterialReportBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemService extends GenericService<Producaoordem> {

	private ProducaoordemDAO producaoordemDAO;
	private ProducaoagendaService producaoagendaService;
	private ProducaoordemmaterialService producaoordemmaterialService;
	private ProducaoordemmaterialorigemService producaoordemmaterialorigemService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private ProducaoordemhistoricoService producaoordemhistoricoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialproducaoService materialproducaoService;
	private MaterialService materialService;
	private ProducaoetapaService producaoetapaService;
	private ProducaoetapaitemService producaoetapaitemService;
	private ColetaMaterialService coletaMaterialService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private ProducaoordemmaterialproduzidoService producaoordemmaterialproduzidoService;
	private ReservaService reservaService;
	private ColetaService coletaService; 
	private MotivodevolucaoService motivodevolucaoService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private ProducaoordemmaterialcampoService producaoordemmaterialcampoService;
	private ProducaoordemequipamentoService producaoordemequipamentoService;
	private ProducaoordemitemadicionalService producaoordemitemadicionalService;
	private ProducaoordemmaterialmateriaprimaService producaoordemmaterialmateriaprimaService;
	private LotematerialService lotematerialService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private PneuService pneuService;
	private PedidovendaService pedidovendaService;
	private ProducaochaofabricaService producaochaofabricaService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private ColetahistoricoService coletahistoricoService;
	private LocalarmazenagemService localarmazenagemService;
	private VendamaterialService vendamaterialService;
	private PedidovendamaterialService pedidovendamaterialService;
	private VendaService vendaService;
	private ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService;
	private ProducaoordemconstatacaoService producaoordemconstatacaoService;
	private GarantiareformaService garantiareformaService;
	private ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService;
	private ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService;
	private EmpresaService empresaService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService;
	
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setPedidovendamaterialService(
			PedidovendamaterialService pedidovendamaterialService) {
		this.pedidovendamaterialService = pedidovendamaterialService;
	}
	public void setColetahistoricoService(
			ColetahistoricoService coletahistoricoService) {
		this.coletahistoricoService = coletahistoricoService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setProducaochaofabricaService(
			ProducaochaofabricaService producaochaofabricaService) {
		this.producaochaofabricaService = producaochaofabricaService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setProducaoordemmaterialproduzidoService(
			ProducaoordemmaterialproduzidoService producaoordemmaterialproduzidoService) {
		this.producaoordemmaterialproduzidoService = producaoordemmaterialproduzidoService;
	}
	public void setProducaoagendamaterialService(
			ProducaoagendamaterialService producaoagendamaterialService) {
		this.producaoagendamaterialService = producaoagendamaterialService;
	}
	public void setColetaMaterialService(
			ColetaMaterialService coletaMaterialService) {
		this.coletaMaterialService = coletaMaterialService;
	}
	public void setProducaoetapaitemService(
			ProducaoetapaitemService producaoetapaitemService) {
		this.producaoetapaitemService = producaoetapaitemService;
	}
	public void setProducaoetapaService(
			ProducaoetapaService producaoetapaService) {
		this.producaoetapaService = producaoetapaService;
	}
	public void setMaterialproducaoService(
			MaterialproducaoService materialproducaoService) {
		this.materialproducaoService = materialproducaoService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setProducaoordemhistoricoService(
			ProducaoordemhistoricoService producaoordemhistoricoService) {
		this.producaoordemhistoricoService = producaoordemhistoricoService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setProducaoagendaService(
			ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setProducaoordemmaterialorigemService(
			ProducaoordemmaterialorigemService producaoordemmaterialorigemService) {
		this.producaoordemmaterialorigemService = producaoordemmaterialorigemService;
	}
	public void setProducaoordemmaterialService(
			ProducaoordemmaterialService producaoordemmaterialService) {
		this.producaoordemmaterialService = producaoordemmaterialService;
	}
	public void setProducaoordemDAO(ProducaoordemDAO producaoordemDAO) {
		this.producaoordemDAO = producaoordemDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setColetaService(ColetaService coletaService) {
		this.coletaService = coletaService;
	}
	public void setMotivodevolucaoService(MotivodevolucaoService motivodevolucaoService) {
		this.motivodevolucaoService = motivodevolucaoService;
	}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setProducaoordemmaterialcampoService(ProducaoordemmaterialcampoService producaoordemmaterialcampoService) {
		this.producaoordemmaterialcampoService = producaoordemmaterialcampoService;
	}
	public void setProducaoordemequipamentoService(ProducaoordemequipamentoService producaoordemequipamentoService) {
		this.producaoordemequipamentoService = producaoordemequipamentoService;
	}
	public void setProducaoordemitemadicionalService(ProducaoordemitemadicionalService producaoordemitemadicionalService) {
		this.producaoordemitemadicionalService = producaoordemitemadicionalService;
	}
	public void setProducaoordemmaterialmateriaprimaService(ProducaoordemmaterialmateriaprimaService producaoordemmaterialmateriaprimaService) {
		this.producaoordemmaterialmateriaprimaService = producaoordemmaterialmateriaprimaService;
	}
	public void setLotematerialService(LotematerialService lotematerialService) {
		this.lotematerialService = lotematerialService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setProducaoordemmaterialoperadorService(ProducaoordemmaterialoperadorService producaoordemmaterialoperadorService) {
		this.producaoordemmaterialoperadorService = producaoordemmaterialoperadorService;
	}
	public void setProducaoordemconstatacaoService(ProducaoordemconstatacaoService producaoordemconstatacaoService) {
		this.producaoordemconstatacaoService = producaoordemconstatacaoService;
	}
	public void setGarantiareformaService(GarantiareformaService garantiareformaService) {
		this.garantiareformaService = garantiareformaService;
	}
	public void setColetamaterialmotivodevolucaoService(ColetamaterialmotivodevolucaoService coletamaterialmotivodevolucaoService) {
		this.coletamaterialmotivodevolucaoService = coletamaterialmotivodevolucaoService;
	}
	public void setProducaoordemmaterialperdadescarteService(
			ProducaoordemmaterialperdadescarteService producaoordemmaterialperdadescarteService) {
		this.producaoordemmaterialperdadescarteService = producaoordemmaterialperdadescarteService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setMovimentacaoEstoqueHistoricoService(
			MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {
		this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;
	}
	public void setColetaMaterialPedidovendamaterialService(ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService) {
		this.coletaMaterialPedidovendamaterialService = coletaMaterialPedidovendamaterialService;
	}
	
	@Override
	public void saveOrUpdate(final Producaoordem bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				Set<Producaoordemmaterial> listaProducaoordemmaterial = bean.getListaProducaoordemmaterial();
				Set<Producaoordemequipamento> listaProducaoordemequipamento = bean.getListaProducaoordemequipamento();
				Set<Producaoordemitemadicional> listaProducaoordemitemadicional = bean.getListaProducaoordemitemadicional();
				Set<Producaoordemconstatacao> listaProducaoordemconstatacao = bean.getListaProducaoordemconstatacao();
				
				if(bean.getCdproducaoordem() != null){
					StringBuilder whereInPOMDelete = new StringBuilder();
					if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
						for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){	
							if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
								whereInPOMDelete.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
							}
							if(Hibernate.isInitialized(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()) && 
									SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima())){
								for(Producaoordemmaterialmateriaprima pommp : producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()){
									if(pommp.getMaterialproducao() != null && pommp.getMaterialproducao().getCdmaterialproducao() == null){
										pommp.setMaterialproducao(null);
									}
								}
							}
						}
					}
					if(whereInPOMDelete.length() > 0){
						List<Producaoordemmaterial> listaPAMDelete = producaoordemmaterialService.findForDelete(bean, whereInPOMDelete.substring(0, whereInPOMDelete.length()-1));
						if(SinedUtil.isListNotEmpty(listaPAMDelete)){
							for(Producaoordemmaterial producaoordemmaterial : listaPAMDelete){
								producaoordemmaterialService.delete(producaoordemmaterial);
							}
						}
					}
					
					StringBuilder whereInPOMEDelete = new StringBuilder();
					if(SinedUtil.isListNotEmpty(listaProducaoordemequipamento)){
						for(Producaoordemequipamento producaoordemequipamento : listaProducaoordemequipamento){	
							if(producaoordemequipamento.getCdproducaoordemequipamento() != null){
								whereInPOMEDelete.append(producaoordemequipamento.getCdproducaoordemequipamento()).append(",");
							}
						}
					}
					if(whereInPOMEDelete.length() > 0){
						List<Producaoordemequipamento> listaPOMEDelete = producaoordemequipamentoService.findForDelete(bean, whereInPOMEDelete.substring(0, whereInPOMEDelete.length()-1));
						if(SinedUtil.isListNotEmpty(listaPOMEDelete)){
							for(Producaoordemequipamento producaoordemequipamento : listaPOMEDelete){
								producaoordemequipamentoService.delete(producaoordemequipamento);
							}
						}
					}
					
					StringBuilder whereInPOIADelete = new StringBuilder();
					if(SinedUtil.isListNotEmpty(listaProducaoordemitemadicional)){
						for(Producaoordemitemadicional producaoordemitemadicional : listaProducaoordemitemadicional){	
							if(producaoordemitemadicional.getCdproducaoordemitemadicional() != null){
								whereInPOIADelete.append(producaoordemitemadicional.getCdproducaoordemitemadicional()).append(",");
							}
						}
					}
					if(whereInPOIADelete.length() > 0){
						List<Producaoordemitemadicional> listaPOIADelete = producaoordemitemadicionalService.findForDelete(bean, whereInPOIADelete.substring(0, whereInPOIADelete.length()-1));
						if(SinedUtil.isListNotEmpty(listaPOIADelete)){
							for(Producaoordemitemadicional producaoordemitemadicional : listaPOIADelete){
								producaoordemitemadicionalService.delete(producaoordemitemadicional);
							}
						}
					}
					
					StringBuilder whereInPOCDelete = new StringBuilder();
					if(SinedUtil.isListNotEmpty(listaProducaoordemconstatacao)){
						for(Producaoordemconstatacao producaoordemconstatacao: listaProducaoordemconstatacao){	
							if(producaoordemconstatacao.getCdproducaoordemconstatacao() != null){
								whereInPOCDelete.append(producaoordemconstatacao.getCdproducaoordemconstatacao()).append(",");
							}
						}
					}
					if(whereInPOCDelete.length() > 0){
						List<Producaoordemconstatacao> listaPOCDelete = producaoordemconstatacaoService.findForDelete(bean, whereInPOCDelete.substring(0, whereInPOCDelete.length()-1));
						if(SinedUtil.isListNotEmpty(listaPOCDelete)){
							for(Producaoordemconstatacao producaoordemconstatacao : listaPOCDelete){
								producaoordemconstatacaoService.delete(producaoordemconstatacao);
							}
						}
					}
				}
				
				saveOrUpdateNoUseTransaction(bean);	
				
				if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
					for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){	
						producaoordemmaterial.setProducaoordem(bean);
						producaoordemmaterialService.saveOrUpdateNoUseTransaction(producaoordemmaterial);
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaProducaoordemequipamento)){
					for(Producaoordemequipamento producaoordemequipamento : listaProducaoordemequipamento){	
						producaoordemequipamento.setProducaoordem(bean);
						producaoordemequipamentoService.saveOrUpdateNoUseTransaction(producaoordemequipamento);
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaProducaoordemitemadicional)){
					for(Producaoordemitemadicional producaoordemitemadicional : listaProducaoordemitemadicional){	
						producaoordemitemadicional.setProducaoordem(bean);
						producaoordemitemadicionalService.saveOrUpdateNoUseTransaction(producaoordemitemadicional);
					}
				}
				
				if(SinedUtil.isListNotEmpty(listaProducaoordemconstatacao)){
					for(Producaoordemconstatacao producaoordemconstatacao: listaProducaoordemconstatacao){	
						producaoordemconstatacao.setProducaoordem(bean);
						producaoordemconstatacaoService.saveOrUpdateNoUseTransaction(producaoordemconstatacao);
					}
				}
				return null;
			}
		});
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForCsv(ProducaoordemFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordem> findForCsv(ProducaoordemFiltro filtro) {
		return producaoordemDAO.findForCsv(filtro);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findWithSituacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 04/01/2013
	 */
	public List<Producaoordem> findWithSituacao(String whereIn) {
		return producaoordemDAO.findWithSituacao(whereIn);
	}
	
	/**
	 * Cria o arquivo CSV da Listagem de Ordem de produ��o
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public Resource makeCSVListagem(ProducaoordemFiltro filtro) {
		StringBuilder rel = new StringBuilder();
		
		rel.append("\"ORDEM DE PRODU��O\"").append(";");
		rel.append("\"DEPARTAMENTO\"").append(";");
		rel.append("\"DATA\"").append(";");
		rel.append("\"PREVIS�O DE ENTREGA\"").append(";");
		rel.append("\"SITUA��O\"").append(";");
		rel.append("\"MATERIAL\"").append(";");
		rel.append("\"QTDE.\"").append(";");
		rel.append("\"QTDE. PRODUZIDA\"").append(";");
		rel.append("\n");
		
		List<Producaoordem> listaProducaoordem = this.findForCsv(filtro);
		for (Producaoordem producaoordem : listaProducaoordem) {
			rel.append("\"" + producaoordem.getCdproducaoordem() + "\";");
			
			if(producaoordem.getDepartamento() != null) 
				rel.append("\"" + producaoordem.getDepartamento().getNome() + "\";");
			else
				rel.append("\"\";");
			
			if(producaoordem.getDtproducaoordem() != null) 
				rel.append("\"" + SinedDateUtils.toString(producaoordem.getDtproducaoordem()) + "\";");
			else
				rel.append("\"\";");
			
			rel.append("\"" + SinedDateUtils.toString(producaoordem.getDtprevisaoentrega()) + "\";");
			rel.append("\"" + producaoordem.getProducaoordemsituacao().getNome().toUpperCase() + "\";");
			rel.append("\n");
			
			Set<Producaoordemmaterial> listaProducaoordemmaterial = producaoordem.getListaProducaoordemmaterial();
			if(listaProducaoordemmaterial != null && listaProducaoordemmaterial.size() > 0){
				
				for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
					rel.append("\"\";\"\";\"\";\"\";\"\";");
					
					rel.append("\"" + producaoordemmaterial.getMaterial().getNome() + "\";");
					rel.append("\"" + producaoordemmaterial.getQtde() + "\";");
					rel.append("\"" + (producaoordemmaterial.getQtdeproduzido() != null ? producaoordemmaterial.getQtdeproduzido() : 0) + "\";");
					rel.append("\n");
				}
			}
			
		}
		
		return new Resource("text/csv","producaoordem_" + SinedUtil.datePatternForReport() + ".csv", rel.toString().replaceAll(";null;", ";;").getBytes());
	}
	
	/**
	 * Prepara a lista dos agendamentos para a confer�ncia da produ��o.
	 *
	 * @param whereIn
	 * @param mapCdproducaoagendaProducaoordem 
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public ConferenciaProducaoBean prepareForConferenciaProducao(String whereIn, String whereInProducaoordem, Boolean isAgendaproducao, List<Producaoordemmaterial> listaProducaoordemmaterialAux, HashMap<Integer, List<Integer>> mapCdproducaoagendaProducaoordem, Boolean isProducaoemlote) {
		List<Producaoordemmaterial> listaProducaoordemmaterialConferencia = new ArrayList<Producaoordemmaterial>();
		if(StringUtils.isNotBlank(whereIn)){
			List<Producaoagenda> listaProducaoagenda = producaoagendaService.findForProducao(whereIn);
			Date dataAtual = new Date(System.currentTimeMillis());
			
			boolean recapagem = SinedUtil.isRecapagem();
			
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				Set<Producaoagendamaterial> listaProducaoagendamaterial = producaoagenda.getListaProducaoagendamaterial();
				Producaoetapa producaoetapa_tipo = producaoagenda.getProducaoagendatipo() != null ? producaoagenda.getProducaoagendatipo().getProducaoetapa() : null;
				
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
					
					if(listaProducaoordemmaterialAux == null || producaoordemmaterialService.existeItem(producaoagendamaterial, listaProducaoordemmaterialAux)){
					
						StringBuilder departamentosPrevisoes = new StringBuilder(); 				
						Material material = producaoagendamaterial.getMaterial();
						List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima = producaoagendamaterial.getListaProducaoagendamaterialmateriaprima();
						Producaoetapa producaoetapa_material = producaoagendamaterial.getProducaoetapa();
						if(producaoetapa_material == null && material.getProducaoetapa() != null){
							producaoetapa_material = material.getProducaoetapa();
						} else if(producaoetapa_material == null && producaoagenda.getProducaoagendatipo() != null){
							producaoetapa_material = producaoagenda.getProducaoagendatipo().getProducaoetapa();
						}
						
						Date dtprevisaoentregaAgenda = new Date(producaoagenda.getDtentrega().getTime());
						
						if(material != null && (producaoetapa_material != null || producaoetapa_tipo != null)){
							Producaoordemmaterial producaoordemmaterial = new Producaoordemmaterial();
							
							Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = new ListSet<Producaoordemmaterialorigem>(Producaoordemmaterialorigem.class);
							listaProducaoordemmaterialorigem.add(new Producaoordemmaterialorigem(producaoagenda, producaoagendamaterial));
							producaoordemmaterial.setListaProducaoordemmaterialorigem(listaProducaoordemmaterialorigem);
							
							setInformacoesProducaoordemmaterial(producaoordemmaterial, listaProducaoordemmaterialAux, null, null);
							
							List<Producaoetapaitem> listaProducaoetapaitem = new ArrayList<Producaoetapaitem>();
							if(producaoetapa_material != null){
								listaProducaoetapaitem = producaoetapa_material.getListaProducaoetapaitem();
								producaoordemmaterial.setProducaoetapa(producaoetapa_material);
								producaoordemmaterial.setNaoagrupamaterial(producaoetapa_material.getNaoagruparmaterial());
							} else if(producaoetapa_tipo != null){
								listaProducaoetapaitem = producaoetapa_tipo.getListaProducaoetapaitem();
								producaoordemmaterial.setProducaoetapa(producaoetapa_tipo);
								producaoordemmaterial.setNaoagrupamaterial(producaoetapa_tipo.getNaoagruparmaterial());
							}
							
							if(listaProducaoetapaitem != null && listaProducaoetapaitem.size() > 0){
								Integer qtdediasatras = 0;
								for (Producaoetapaitem it : listaProducaoetapaitem) {
									if(it.getQtdediasprevisto() != null){
										qtdediasatras += it.getQtdediasprevisto();
									}
								}
								
								Date dtprevisao_aux = dataAtual;
								
								for (Producaoetapaitem it : listaProducaoetapaitem) {
									departamentosPrevisoes.append("<b>Etapa ").append(it.getOrdem()).append(":</b> ").append(it.getProducaoetapanome() != null ? it.getProducaoetapanome().getNome() : "");
									if(it.getDepartamento() != null){
										departamentosPrevisoes.append(" - <b>Departamento:</b> ").append(it.getDepartamento().getNome());
									}
									if(it.getQtdediasprevisto() != null){
										dtprevisao_aux = SinedDateUtils.addDiasData(dtprevisao_aux, it.getQtdediasprevisto());
									}
									departamentosPrevisoes.append(" - <b>Prev. Entrega:</b> ").append(SinedDateUtils.toString(dtprevisao_aux));
									departamentosPrevisoes.append("<BR>");
									
									if(producaoordemmaterial.getProducaoetapaitem() == null){
										producaoordemmaterial.setProducaoetapaitem(it);
										producaoordemmaterial.setDepartamento(it.getDepartamento());
										producaoordemmaterial.setDtprevisaoentrega(dtprevisao_aux);
									}
								}
							} else {
								departamentosPrevisoes.append("<b>Etapa �nica</b> - <b>Prev. Entrega:</b> ").append(SinedDateUtils.toString(dtprevisaoentregaAgenda));
								producaoordemmaterial.setDtprevisaoentrega(dtprevisaoentregaAgenda);
							}
							
							producaoordemmaterial.setEmpresa(producaoagenda.getEmpresa());
							producaoordemmaterial.setMaterial(material);
							producaoordemmaterial.setListaProducaoagendamaterialmateriaprima(listaProducaoagendamaterialmateriaprima);
							producaoordemmaterial.setDepartamentosprevisoes(departamentosPrevisoes.toString());
							producaoordemmaterial.setQtdeprevista(producaoagendamaterial.getQtde());
							producaoordemmaterial.setQtde(producaoagendamaterial.getQtde());
							if(producaoordemmaterial.getQtdeBaixaEstoque() == null){
								producaoordemmaterial.setQtdeBaixaEstoque(producaoagendamaterial.getQtde());	
							}
							producaoordemmaterial.setHaveCemobra(producaoagenda.getCemobra() != null);
							producaoordemmaterial.setObservacao(producaoagendamaterial.getObservacao());
							producaoordemmaterial.setUnidademedida(producaoagendamaterial.getUnidademedida());
							producaoordemmaterial.setProducaoetapa(producaoagendamaterial.getProducaoetapa());
							
							producaoordemmaterial.setPedidovendamaterial(producaoagendamaterial.getPedidovendamaterial());
							if(recapagem){
								producaoordemmaterial.setPneu(producaoagendamaterial.getPneu());
							}
							
							if(producaoagenda.getPedidovenda() != null && producaoagenda.getPedidovenda().getLocalarmazenagem() != null){
								producaoordemmaterial.setLocalarmazenagem(producaoagenda.getPedidovenda().getLocalarmazenagem());
							}else if(producaoagenda.getPedidovenda() != null && producaoagenda.getPedidovenda().getLocalarmazenagem() != null){
								producaoordemmaterial.setLocalarmazenagem(producaoagenda.getVenda().getLocalarmazenagem());
							}
							if(producaoordemmaterial.getLocalbaixamateriaprima() == null || producaoagenda.getLocalbaixamateriaprima() != null){
								producaoordemmaterial.setLocalbaixamateriaprima(producaoagenda.getLocalbaixamateriaprima());
							}
							listaProducaoordemmaterialConferencia.add(producaoordemmaterial);
						}
					}
				}
				
			}
		}
		if(StringUtils.isNotBlank(whereInProducaoordem)){
			List<Producaoordem> listaProducaoordem = findForProducao(whereInProducaoordem);
			Date dataAtual = new Date(System.currentTimeMillis());
			boolean recapagem = SinedUtil.isRecapagem();
			
			for (Producaoordem po : listaProducaoordem) {
				Set<Producaoordemmaterial> listaProducaoordemmaterial = po.getListaProducaoordemmaterial();
				for (Producaoordemmaterial pom : listaProducaoordemmaterial) {
						StringBuilder departamentosPrevisoes = new StringBuilder(); 				
						Material material = pom.getMaterial();
						Set<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima = pom.getListaProducaoordemmaterialmateriaprima();
						Producaoetapa producaoetapa_material = pom.getProducaoetapa();
						if(producaoetapa_material == null && material.getProducaoetapa() != null){
							producaoetapa_material = material.getProducaoetapa();
						}
						
						Date dtprevisaoentregaordem = new Date(po.getDtprevisaoentrega().getTime());
						
						if(material != null){
							Producaoordemmaterial producaoordemmaterial = new Producaoordemmaterial();
							producaoordemmaterial.setConsiderarProducaoordem(true);
							producaoordemmaterial.setProducaoetapaitem(pom.getProducaoetapaitem());
							
							setInformacoesProducaoordemmaterial(producaoordemmaterial, listaProducaoordemmaterialAux, whereInProducaoordem, pom.getCdproducaoordemmaterial().toString());
							
							boolean incluirEtapaUnica = true;
							if(pom.getProducaoetapa() != null){
								List<Producaoetapaitem> listaProducaoetapaitem = new ArrayList<Producaoetapaitem>();
								if(producaoetapa_material != null){
									listaProducaoetapaitem = producaoetapa_material.getListaProducaoetapaitem();
									producaoordemmaterial.setProducaoetapa(producaoetapa_material);
									producaoordemmaterial.setNaoagrupamaterial(producaoetapa_material.getNaoagruparmaterial());
								}
								if(listaProducaoetapaitem != null && listaProducaoetapaitem.size() > 0){
									incluirEtapaUnica = false;
									Integer qtdediasatras = 0;
									for (Producaoetapaitem it : listaProducaoetapaitem) {
										if(it.getQtdediasprevisto() != null){
											qtdediasatras += it.getQtdediasprevisto();
										}
									}
									
									Date dtprevisao_aux = dataAtual;
									
									for (Producaoetapaitem it : listaProducaoetapaitem) {
										departamentosPrevisoes.append("<b>Etapa ").append(it.getOrdem()).append(":</b> ").append(it.getProducaoetapanome() != null ? it.getProducaoetapanome().getNome() : "");
										if(it.getDepartamento() != null){
											departamentosPrevisoes.append(" - <b>Departamento:</b> ").append(it.getDepartamento().getNome());
										}
										if(it.getQtdediasprevisto() != null){
											dtprevisao_aux = SinedDateUtils.addDiasData(dtprevisao_aux, it.getQtdediasprevisto());
										}
										departamentosPrevisoes.append(" - <b>Prev. Entrega:</b> ").append(SinedDateUtils.toString(dtprevisao_aux));
										departamentosPrevisoes.append("<BR>");
										
										if(producaoordemmaterial.getProducaoetapaitem() == null || (pom.getProducaoetapaitem() != null && it.getCdproducaoetapaitem() != null && 
												it.getCdproducaoetapaitem().equals(pom.getProducaoetapaitem().getCdproducaoetapaitem()))){
											producaoordemmaterial.setProducaoetapaitem(it);
											producaoordemmaterial.setDepartamento(it.getDepartamento());
											producaoordemmaterial.setDtprevisaoentrega(dtprevisao_aux);
										}
									}
								}
							}
							if(incluirEtapaUnica) {
								departamentosPrevisoes.append("<b>Etapa �nica</b> - <b>Prev. Entrega:</b> ").append(SinedDateUtils.toString(dtprevisaoentregaordem));
								producaoordemmaterial.setDtprevisaoentrega(dtprevisaoentregaordem);
								
								if(producaoetapa_material != null){
									producaoordemmaterial.setBaixarmateriaprimacascata(producaoetapa_material.getBaixarmateriaprimacascata());
								}
							}
							
							producaoordemmaterial.setEmpresa(po.getEmpresa());
							producaoordemmaterial.setMaterial(material);
							producaoordemmaterial.setListaProducaoordemmaterialmateriaprima(listaProducaoordemmaterialmateriaprima);
							producaoordemmaterial.setDepartamentosprevisoes(departamentosPrevisoes.toString());
							producaoordemmaterial.setQtdeprevista(pom.getQtde());
							producaoordemmaterial.setQtde(pom.getQtde());
							if(producaoordemmaterial.getQtdeBaixaEstoque() == null){
								producaoordemmaterial.setQtdeBaixaEstoque(pom.getQtde());	
							}
							producaoordemmaterial.setObservacao(pom.getObservacao());
							producaoordemmaterial.setUnidademedida(pom.getUnidademedida());
							producaoordemmaterial.setProducaoetapa(pom.getProducaoetapa());
							producaoordemmaterial.setPedidovendamaterial(pom.getPedidovendamaterial());
							if(recapagem){
								producaoordemmaterial.setPneu(pom.getPneu());
							}
							
							if(SinedUtil.isListNotEmpty(pom.getListaProducaoordemmaterialorigem())){
								for (Producaoordemmaterialorigem producaoordemmaterialorigem : pom.getListaProducaoordemmaterialorigem()) {
									if(producaoordemmaterialorigem.getProducaoagenda() != null){
										if(producaoordemmaterialorigem.getProducaoagenda().getPedidovenda() != null && producaoordemmaterialorigem.getProducaoagenda().getPedidovenda().getLocalarmazenagem() != null){
											producaoordemmaterial.setLocalarmazenagem(producaoordemmaterialorigem.getProducaoagenda().getPedidovenda().getLocalarmazenagem());
										}else if(producaoordemmaterialorigem.getProducaoagenda().getPedidovenda() != null && producaoordemmaterialorigem.getProducaoagenda().getPedidovenda().getLocalarmazenagem() != null){
											producaoordemmaterial.setLocalarmazenagem(producaoordemmaterialorigem.getProducaoagenda().getVenda().getLocalarmazenagem());
										}
										if(producaoordemmaterialorigem.getProducaoagenda().getLocalbaixamateriaprima() != null){
											producaoordemmaterial.setLocalbaixamateriaprima(producaoordemmaterialorigem.getProducaoagenda().getLocalbaixamateriaprima());
										}
										break;
									}
								}
							}
							
							listaProducaoordemmaterialConferencia.add(producaoordemmaterial);
						}
					}
				}
		}
		
		ConferenciaProducaoBean bean = new ConferenciaProducaoBean();
		String valor = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAPRODUCAO);
		NeoWeb.getRequestContext().setAttribute("SOLICITARCOMPRAPRODUCAO", valor);
		
//		if(valor != null && !valor.trim().toUpperCase().equals("REQUISICAOMATERIAL")){
			List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = new ArrayList<MateriaprimaConferenciaProducaoBean>();
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialConferencia) {
				Producaoetapanome producaoetapanome = producaoordemmaterial.getProducaoetapaitem() != null ? producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome() : null;
				BaixarEstoqueMateriasPrimasEnum baixarEstoque = null;
				
				if(producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima() != null){
					baixarEstoque = producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima();
				}
				if(!isAgendaproducao || (isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)))){
					
					Boolean baixarmateriaprimacascata = (producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
															producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata()) ||
															(producaoordemmaterial.getProducaoetapa() == null && producaoordemmaterial.getBaixarmateriaprimacascata() != null && 
																	producaoordemmaterial.getBaixarmateriaprimacascata());
					if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoagendamaterialmateriaprima())){
						for (Producaoagendamaterialmateriaprima mp : producaoordemmaterial.getListaProducaoagendamaterialmateriaprima()) {
							Material material = mp.getMaterial();
							
							if((!isAgendaproducao && ((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(baixarEstoque) &&
									producaoetapanome != null && producaoetapanome.equals(mp.getProducaoetapanome())) ||
									(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(baixarEstoque) && producaoordemmaterial.getUltimaetapa() != null &&
									producaoordemmaterial.getUltimaetapa()) ||
									producaoordemmaterial.getProducaoetapa() == null)) || 
									(isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)))){
								boolean achou = false;
								if(isNotServicoEpiPatrimonio(material)){
									for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
										if(materiaprima.getMaterial().equals(material)){
											achou = true;
										}
									}
								}
								
								if(!achou){
									MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
									materiaprima.setMaterial(material);
									materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
									materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(material, Materialclasse.PRODUTO, null));
									materiaprima.setLocalarmazenagem(producaoordemmaterial.getLocalbaixamateriaprima() != null ? producaoordemmaterial.getLocalbaixamateriaprima() : producaoordemmaterial.getLocalarmazenagem());
									materiaprima.setUnidademedida(mp.getUnidademedida());
									materiaprima.setCasasdecimaisestoqueunidademedida(mp.getUnidademedida().getCasasdecimaisestoque());
									materiaprima.setLoteestoque(mp.getLoteestoque());
									materiaprima.setFracaounidademedida(mp.getFracaounidademedida());
									materiaprima.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
									materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
									
									String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
									materiaprima.setWhereInAgendaProducao(whereInProducaoagenda);
									
									if(!isAgendaproducao){
										setWhereInOrdemProducaoAgendaProducaoMateriaprima(materiaprima, mapCdproducaoagendaProducaoordem, whereInProducaoagenda);
									}
									
									addMateriaprima(materiaprima, listaMateriaprima, false, isNotServicoEpiPatrimonio(material));
								}
							}
						}
					}else if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima())){
						for (Producaoordemmaterialmateriaprima mp : producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()) {
							Material material = mp.getMaterial();
							if((!isAgendaproducao && ((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(baixarEstoque) &&
									producaoetapanome != null && producaoetapanome.equals(mp.getProducaoetapanome())) ||
									(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(baixarEstoque) && producaoordemmaterial.getUltimaetapa() != null &&
									producaoordemmaterial.getUltimaetapa()) ||
									producaoordemmaterial.getProducaoetapa() == null)) || 
									(isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)))){
								boolean achou = false;
								if(isNotServicoEpiPatrimonio(material)){
									for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
										if(materiaprima.getMaterial().equals(material)){
											achou = true;
										}
									}
								}
								
								if(!achou){
									MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
									materiaprima.setMaterial(material);
									materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
									materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(material, Materialclasse.PRODUTO, null));
									materiaprima.setLocalarmazenagem(producaoordemmaterial.getLocalbaixamateriaprima() != null ? producaoordemmaterial.getLocalbaixamateriaprima() : producaoordemmaterial.getLocalarmazenagem());
									materiaprima.setUnidademedida(mp.getUnidademedida());
									materiaprima.setCasasdecimaisestoqueunidademedida(mp.getUnidademedida().getCasasdecimaisestoque());
									materiaprima.setLoteestoque(mp.getLoteestoque());
									materiaprima.setFracaounidademedida(mp.getFracaounidademedida());
									materiaprima.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
									materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
									
									String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
									materiaprima.setWhereInAgendaProducao(whereInProducaoagenda);
									
									if(!isAgendaproducao){
										setWhereInOrdemProducaoAgendaProducaoMateriaprima(materiaprima, mapCdproducaoagendaProducaoordem, whereInProducaoagenda);
									}
									
									addMateriaprima(materiaprima, listaMateriaprima, false, isNotServicoEpiPatrimonio(material));
								}
							}
						}
					}else {
						List<Materialproducao> listaMaterialproducao = materialproducaoService.findListaProducao(producaoordemmaterial.getMaterial().getCdmaterial());
						
						for (Materialproducao materialproducao : listaMaterialproducao) {
							Material material = materialproducao.getMaterial();
							
							if((!isAgendaproducao && ((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(baixarEstoque) &&
									producaoetapanome != null && producaoetapanome.equals(materialproducao.getProducaoetapanome())) ||
									(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(baixarEstoque) && producaoordemmaterial.getUltimaetapa() != null &&
									producaoordemmaterial.getUltimaetapa()) ||
									producaoordemmaterial.getProducaoetapa() == null)) || 
									(isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)))){
								if(SinedUtil.isRecapagem() &&
										materialproducao.getExibirvenda() != null &&
										materialproducao.getExibirvenda() &&
										producaoordemmaterial.getPneu() != null &&
										producaoordemmaterial.getPneu().getMaterialbanda() != null){
									material = producaoordemmaterial.getPneu().getMaterialbanda();
								}
								
								boolean achou = false;
								if(isNotServicoEpiPatrimonio(material)){
									for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
										if(materiaprima.getMaterial().equals(material)){
											achou = true;
										}
									}
								}
								
								if(!achou){
									MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
									materiaprima.setMaterial(material);
									materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
									materiaprima.setLocalarmazenagem(producaoordemmaterial.getLocalbaixamateriaprima() != null ? producaoordemmaterial.getLocalbaixamateriaprima() : producaoordemmaterial.getLocalarmazenagem());
									materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(material, Materialclasse.PRODUTO, materiaprima.getLocalarmazenagem()));
									materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
									
									String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
									materiaprima.setWhereInAgendaProducao(whereInProducaoagenda);
									
									if(!isAgendaproducao){
										setWhereInOrdemProducaoAgendaProducaoMateriaprima(materiaprima, mapCdproducaoagendaProducaoordem, whereInProducaoagenda);
									}
									
									addMateriaprima(materiaprima, listaMateriaprima, false, isNotServicoEpiPatrimonio(material));
								}
							}
						}
					}
				}
			}
			
			Collections.sort(listaMateriaprima, new Comparator<MateriaprimaConferenciaProducaoBean>(){
				public int compare(MateriaprimaConferenciaProducaoBean o1, MateriaprimaConferenciaProducaoBean o2) {
					return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				}
			});
			
			bean.setListaMateriaprima(listaMateriaprima);
//		}
		
		// ORDENA PELO NOME DO MATERIAL
		Collections.sort(listaProducaoordemmaterialConferencia, new Comparator<Producaoordemmaterial>(){
			public int compare(Producaoordemmaterial o1, Producaoordemmaterial o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		bean.setListaProducaoordemmaterial(listaProducaoordemmaterialConferencia);
		bean.setProducaoEmLote(isProducaoemlote);
		
		return bean;
	}

	private void setWhereInOrdemProducaoAgendaProducaoMateriaprima(MateriaprimaConferenciaProducaoBean materiaprima, HashMap<Integer, List<Integer>> mapCdproducaoagendaProducaoordem, String whereInProducaoagenda) {
		if(mapCdproducaoagendaProducaoordem != null && whereInProducaoagenda != null){
			try {
				String[] idsProducaoagenda = whereInProducaoagenda.split(",");
				if(idsProducaoagenda.length == 1){
					List<Integer> listaCdordemproducao = mapCdproducaoagendaProducaoordem.get(Integer.parseInt(idsProducaoagenda[0]));
					if(listaCdordemproducao != null && listaCdordemproducao.size() == 1){
						materiaprima.setWhereInOrdemProducaoAgendaProducao(listaCdordemproducao.get(0).toString());
					}
				}
			} catch (Exception e) {}
		}
	}
	
	private void setWhereInOrdemProducaoAgendaProducaoMateriaprima(MateriaprimaConferenciaProducaoBean materiaprima, List<MateriaprimaConferenciaProducaoBean> lista, String whereInProducaoagenda) {
		if(SinedUtil.isListNotEmpty(lista) && whereInProducaoagenda != null){
			try {
				String[] idsProducaoagenda = whereInProducaoagenda.split(",");
				if(idsProducaoagenda.length == 1){
					for(MateriaprimaConferenciaProducaoBean materiaprimaConferenciaProducaoBean : lista){
						if(materiaprimaConferenciaProducaoBean.getMaterial() != null && materiaprimaConferenciaProducaoBean.getMaterial().equals(materiaprima.getMaterial()) && 
								whereInProducaoagenda.equals(materiaprima.getWhereInAgendaProducao())){
							materiaprima.setWhereInOrdemProducaoAgendaProducao(materiaprimaConferenciaProducaoBean.getWhereInOrdemProducaoAgendaProducao());
							break;
						}
					}
				}
			} catch (Exception e) {}
		}
	}
	
	private void addMateriaprima(MateriaprimaConferenciaProducaoBean materiaprima, List<MateriaprimaConferenciaProducaoBean> listaMateriaprima, Boolean calcularQtde, Boolean isNotServicoEpiPatrimonio) {
		boolean adicionar = true;
		
		List<MateriaprimaConferenciaProducaoBean> listaMateriaprimaCascata = new ArrayList<MateriaprimaConferenciaProducaoBean>();
		if(materiaprima.getBaixarmateriaprimacascata() != null &&  materiaprima.getBaixarmateriaprimacascata()){
			List<Materialproducao> listaProducao = materialproducaoService.getMateriaisProducao(materiaprima.getMaterial(), materiaprima.getQtde(), materiaprima.getBaixarmateriaprimacascata());
			if(listaProducao != null && !listaProducao.isEmpty()){
				adicionar = false;
				for(Materialproducao materialproducaoCascata : listaProducao){
					if(isNotServicoEpiPatrimonio(materialproducaoCascata.getMaterial())){
						
						boolean achou = false;
						for (MateriaprimaConferenciaProducaoBean it : listaMateriaprima) {
							if(it.getMaterial().equals(materialproducaoCascata.getMaterial())){
								if(calcularQtde){
									it.setQtde((it.getQtde() != null ? it.getQtde() : 0d) + (materialproducaoCascata.getConsumo() != null ? materialproducaoCascata.getConsumo() : 0d));
								}
								achou = true;
							}
						}
						
						if(!achou){
							MateriaprimaConferenciaProducaoBean materiaprimaCascata = new MateriaprimaConferenciaProducaoBean();
							materiaprimaCascata.setMaterial(materialproducaoCascata.getMaterial());
							materiaprimaCascata.setEmpresa(materiaprima.getEmpresa());
							materiaprimaCascata.setLocalarmazenagem(materiaprima.getLocalarmazenagem());
							materiaprimaCascata.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(materialproducaoCascata.getMaterial(), Materialclasse.PRODUTO, materiaprima.getLocalarmazenagem()));
							materiaprimaCascata.setBaixarmateriaprimacascata(materiaprima.getBaixarmateriaprimacascata());
							materiaprimaCascata.setWhereInAgendaProducao(materiaprima.getWhereInAgendaProducao());
							materiaprimaCascata.setWhereInOrdemProducaoAgendaProducao(materiaprima.getWhereInOrdemProducaoAgendaProducao());
							materiaprimaCascata.setWhereInOrdemProducao(materiaprima.getWhereInOrdemProducao());
							if(calcularQtde){
								materiaprimaCascata.setQtde(materialproducaoCascata.getConsumo());
							}
							
							listaMateriaprimaCascata.add(materiaprimaCascata);
						}
					}
				}
				listaMateriaprima.addAll(listaMateriaprimaCascata);
			}
		}
		if(adicionar && isNotServicoEpiPatrimonio){
			materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(materiaprima.getMaterial(), Materialclasse.PRODUTO, materiaprima.getLocalarmazenagem(), materiaprima.getEmpresa(), null, materiaprima.getLoteestoque()));
			listaMateriaprima.add(materiaprima);
		}
		
	}
	/**
	* M�todo que seta informa��es da ordem de produ��o no novo bean
	*
	* @param pom2
	* @param listaProducaoordemmaterialAux
	* @since 10/03/2016
	* @author Luiz Fernando
	*/
	private void setInformacoesProducaoordemmaterial(Producaoordemmaterial pom2, List<Producaoordemmaterial> listaProducaoordemmaterialAux, String whereInProducaoordem, String whereInProducaoordemmaterial) {
		if(pom2 != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterialAux)){
			for(Producaoordemmaterial pom : listaProducaoordemmaterialAux){
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = pom.getListaProducaoordemmaterialorigem();
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem2 = pom2.getListaProducaoordemmaterialorigem();
				
				boolean producaoagendaMatch = false;
				boolean producaoagendamaterialMatch = false;
				
				boolean producaoordemMatch = false;
				boolean producaoordemmaterialMatch = false;
				
				if(SinedUtil.isListNotEmpty(listaProducaoordemmaterialorigem2)){
					for (Producaoordemmaterialorigem producaoordemmaterialorigem2 : listaProducaoordemmaterialorigem2) {
						for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
							producaoagendaMatch = producaoordemmaterialorigem.getProducaoagenda().equals(producaoordemmaterialorigem2.getProducaoagenda());
							if(producaoagendaMatch){
								if(producaoordemmaterialorigem.getProducaoagendamaterial() == null || producaoordemmaterialorigem2.getProducaoagendamaterial() == null){
									producaoagendamaterialMatch = true;
								} else {
									producaoagendamaterialMatch = producaoordemmaterialorigem.getProducaoagendamaterial().equals(producaoordemmaterialorigem2.getProducaoagendamaterial());
								}
							}
							
							if(producaoagendaMatch && producaoagendamaterialMatch){
								break;
							} else {
								producaoagendaMatch = false;
								producaoagendamaterialMatch = false;
							}
						}
					}
				}else if(StringUtils.isNotBlank(whereInProducaoordem)){
					if(whereInProducaoordem.split(",").length > 1 && StringUtils.isNotBlank(whereInProducaoordemmaterial) && pom.getCdproducaoordemmaterial() != null){
						for(String cdPOM : whereInProducaoordemmaterial.split(",")){
							if(pom.getCdproducaoordemmaterial().toString().equals(cdPOM)){
								producaoordemMatch = true;
								producaoordemmaterialMatch = true;
								break;
							}
						}
					}else {
						producaoordemMatch = true;
						producaoordemmaterialMatch = true;
					}
				}
				
				if((producaoagendaMatch && producaoagendamaterialMatch) || (producaoordemMatch&& producaoordemmaterialMatch)){
					if(pom.getProducaoetapaitem() != null && pom.getProducaoetapaitem().getCdproducaoetapaitem() != null){
						pom2.setProducaoetapaitem(pom.getProducaoetapaitem());
					}
					pom2.setUltimaetapa(pom.getUltimaetapa());
					pom2.setCdproducaoordemmaterial(pom.getCdproducaoordemmaterial());
					pom2.setWhereInProducaoordem(StringUtils.isNotBlank(whereInProducaoordem) ? whereInProducaoordem : null);
					pom2.setWhereInProducaoordemmaterial(StringUtils.isNotBlank(whereInProducaoordemmaterial) ? whereInProducaoordemmaterial : null);
					pom2.setLocalbaixamateriaprima(pom.getLocalarmazenagem());
					
					if(pom.getQtderegistro() != null){
						if(pom2.getQtdeBaixaEstoque() == null) pom2.setQtdeBaixaEstoque(0d);
						pom2.setQtdeBaixaEstoque(pom2.getQtdeBaixaEstoque()+pom.getQtderegistro());
					}
				}
			}
		}
	}
	
	/**
	 * Cria a string com o link para a ordem de producao para o hist�rico.
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public String makeLinkHistoricoProducaoordem(Producaoordem producaoordem) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<a href=\"javascript:visualizarProducaoordem(")
			.append(producaoordem.getCdproducaoordem())
			.append(");\">")
			.append(producaoordem.getCdproducaoordem())
			.append("</a>");
		
		return sb.toString();
	}
	
	/**
	 * M�todo que salva as ordens de produ��o vindas da tela de confer�ncia da produ��o.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 11/01/2013
	 */
	public void conferenciaCriarProducao(ConferenciaProducaoBean bean) {
		List<Producaoordem> listaProducaoordemSalvar = new ArrayList<Producaoordem>();
		List<Producaoordemmaterial> listaProducaoordemmaterialTela = bean.getListaProducaoordemmaterial();
		StringBuilder listaWhereInAgendaProducao = new StringBuilder();
		
		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialTela) {
			if(producaoordemmaterial.getQtdeBaixaEstoque() <= 0 && producaoordemmaterial.getQtde() <= 0) continue;
			
			Empresa empresa = producaoordemmaterial.getEmpresa();
			Departamento departamento = producaoordemmaterial.getDepartamento();
			Date dtprevisaoentrega = producaoordemmaterial.getDtprevisaoentrega();
			Boolean naoagrupamaterial = producaoordemmaterial.getNaoagrupamaterial() != null && producaoordemmaterial.getNaoagrupamaterial();
			
			boolean achou = false;
			
			if(!naoagrupamaterial){
				for (Producaoordem producaoordem : listaProducaoordemSalvar) {
					boolean empresaIgual = (empresa != null && producaoordem.getEmpresa() != null && empresa.equals(producaoordem.getEmpresa())) 
											|| (empresa == null && producaoordem.getEmpresa() == null);
					boolean departamentoIgual = (departamento != null && producaoordem.getDepartamento() != null && departamento.equals(producaoordem.getDepartamento())) 
												|| (departamento == null && producaoordem.getDepartamento() == null);
					boolean dtprevisaoentregaIgual = SinedDateUtils.equalsIgnoreHour(dtprevisaoentrega, producaoordem.getDtprevisaoentrega());
					
					if(empresaIgual && departamentoIgual && dtprevisaoentregaIgual){
						producaoordem.getListaProducaoordemmaterial().add(producaoordemmaterial);
						achou = true;
						break;
					}
				}
			}
			
			if(!achou){
				Producaoordem producaoordem = new Producaoordem();
				producaoordem.setEmpresa(empresa);
				producaoordem.setDepartamento(departamento);
				producaoordem.setDtprevisaoentrega(dtprevisaoentrega);
				producaoordem.setDtproducaoordem(SinedDateUtils.currentDate());
				producaoordem.setProducaoordemsituacao(Producaoordemsituacao.EM_ESPERA);
				
				Set<Producaoordemmaterial> listaProducaoordemmaterial = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
				listaProducaoordemmaterial.add(producaoordemmaterial);
				
				producaoordem.setListaProducaoordemmaterial(listaProducaoordemmaterial);
				
				listaProducaoordemSalvar.add(producaoordem);
			}
		}
		
		if(listaProducaoordemSalvar.size() == 0) throw new SinedException("Nenhuma Ordem de Produ��o ser� criada.");
		
		// SALVA AS ORDENS DE PRODU��O
		this.saveListaOrdemproducaoMassa(listaProducaoordemSalvar);
		
		Map<Integer, Localarmazenagem> mapaArmazenagem = new HashMap<Integer, Localarmazenagem>();
		for(MateriaprimaConferenciaProducaoBean item : bean.getListaMateriaprima()){
			mapaArmazenagem.put(item.getMaterial().getCdmaterial(), item.getLocalarmazenagem());
		}
		
		for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialTela){
			if(producaoordemmaterial.getProducaoetapa() != null){
				Producaoetapa producaoetapa = producaoetapaService.loadBaixarEstoqueMateriaprima(producaoordemmaterial.getProducaoetapa());
				if(producaoetapa != null && producaoetapa.getBaixarEstoqueMateriaprima() != null 
						&& producaoetapa.getBaixarEstoqueMateriaprima().equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)){
					String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
					String whereInProducaoagendamaterial = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagendamaterial.cdproducaoagendamaterial", ",");
					baixarEstoqueMateriaPrimaProducaoOrdem(producaoordemmaterial, whereInProducaoagenda, whereInProducaoagendamaterial, bean.getListaMateriaprima(), bean.getProducaoEmLote());
				}
			}
		}
		
//		// SAIDA DE ESTOQUE DAS MAT�RIAS PRIMAS
//		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialTela) {
//			Producaoetapa producaoetapa = producaoordemmaterial.getProducaoetapa();
//			producaoetapa = producaoetapaService.loadBaixarEstoqueMateriaprima(producaoetapa);
//			if(producaoetapa != null && producaoetapa.getBaixarEstoqueMateriaprima() != null 
//					&& producaoetapa.getBaixarEstoqueMateriaprima().equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)){
//				
//				Boolean baixarmateriaprimacascata = producaoetapa.getBaixarmateriaprimacascata() != null && producaoetapa.getBaixarmateriaprimacascata();
//				
//				MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
//				materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
//				materiaprima.setMaterial(producaoordemmaterial.getMaterial());
//				
//				String whereInAgendaproducao = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
//				materiaprima.setWhereInAgendaProducao(whereInAgendaproducao);
//				
//				materiaprima.setQtde(producaoordemmaterial.getQtde());
//				List<Materialproducao> listaProducao = materialproducaoService.getMateriaisProducao(materiaprima.getMaterial(), materiaprima.getQtde(), baixarmateriaprimacascata);
//				
//				if(listaProducao != null && !listaProducao.isEmpty()){
//					for(Materialproducao materialproducao : listaProducao){
//						if(isNotServicoEpiPatrimonio(materialproducao.getMaterial())){
//							materiaprima.setQtde(materialproducao.getConsumo());
//							materiaprima.setMaterial(materialproducao.getMaterial());
//							materiaprima.setLocalarmazenagem(mapaArmazenagem.get(materiaprima.getMaterial().getCdmaterial()));
//							saidaEstoqueMateriasPrimas(materiaprima, listaWhereInAgendaProducao);
//						}
//					}
//				} else {
//					if(isNotServicoEpiPatrimonio(materiaprima.getMaterial())){
//						materiaprima.setLocalarmazenagem(mapaArmazenagem.get(materiaprima.getMaterial().getCdmaterial()));
//						saidaEstoqueMateriasPrimas(materiaprima, listaWhereInAgendaProducao);
//					}
//				}
//				
//			}
//		}
		
		String listaWhereInAgendaproducao = listaWhereInAgendaProducao.toString();
		
		if (StringUtils.isNotEmpty(listaWhereInAgendaproducao)) {
			reservaService.deleteReservaByProducaoagenda(listaWhereInAgendaproducao.substring(0, listaWhereInAgendaproducao.lastIndexOf(",")));
		}
		
	}
	
	/**
	* M�todo que verifica se o material n�o � servi�o, epi e patrim�nio 
	*
	* @param material
	* @return
	* @since 24/02/2016
	* @author Luiz Fernando
	*/
	private boolean isNotServicoEpiPatrimonio(Material material) {
		return (material.getServico() == null || !material.getServico()) &&
			   (material.getEpi() == null || !material.getEpi()) &&
			   (material.getPatrimonio() == null || !material.getPatrimonio());
		
	}
	/**
	 * M�todo que registra a movimenta��o do estoque de sa�da para baixa dos materiais de produ��o
	 * @param materiaprima
	 * @param listaWhereInAgendaProducao
	 * @since 09/06/2015
	 * @author Danilo Guimar�es
	 */
	public Movimentacaoestoque saidaEstoqueMateriasPrimas(MateriaprimaConferenciaProducaoBean materiaprima, StringBuilder listaWhereInAgendaProducao, Producaoordemmaterial producaoordemmaterial){
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		
		String whereInAgendaProducao = materiaprima.getWhereInAgendaProducao();
		String whereInOrdemProducaoAgendaProducao = materiaprima.getWhereInOrdemProducaoAgendaProducao();
		String whereInOrdemProducao = materiaprima.getWhereInOrdemProducao();
		boolean origemUsuario = true;
		if(StringUtils.isNotBlank(whereInOrdemProducao)){
			try {
				movimentacaoestoqueorigem.setProducaoordem(new Producaoordem(Integer.parseInt(whereInOrdemProducao)));
				movimentacaoestoqueorigem.setProducaoOrdemMaterial(materiaprima.getProducaoOrdemMaterial());
				movimentacaoestoqueorigem.setRegistrarproducao(true);
				origemUsuario = false;
			} catch (Exception e) {}
		}else if(whereInAgendaProducao != null && !whereInAgendaProducao.equals("")){
			if(whereInAgendaProducao.indexOf(",") != -1){
				whereInAgendaProducao = whereInAgendaProducao.substring(0, whereInAgendaProducao.indexOf(","));
				listaWhereInAgendaProducao.append(whereInAgendaProducao + ",");
			}
			
			try {
				movimentacaoestoqueorigem.setProducaoAgendaMaterial(materiaprima.getProducaoAgendaMaterial());
				movimentacaoestoqueorigem.setProducaoagenda(new Producaoagenda(Integer.parseInt(whereInAgendaProducao)));
				origemUsuario = false;
				if(StringUtils.isNotBlank(whereInOrdemProducaoAgendaProducao) && whereInOrdemProducaoAgendaProducao.indexOf(",") == -1){
					try {
						movimentacaoestoqueorigem.setProducaoOrdemMaterial(materiaprima.getProducaoOrdemMaterial());
						movimentacaoestoqueorigem.setProducaoordem(new Producaoordem(Integer.parseInt(whereInOrdemProducaoAgendaProducao)));
						movimentacaoestoqueorigem.setRegistrarproducao(true);
					} catch (Exception e) {}
				}
			} catch (Exception e) {}
		} 
		if(origemUsuario){
			movimentacaoestoqueorigem.setUsuario((Usuario)Neo.getUser());
		}
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setEmpresa(materiaprima.getEmpresa());
		movimentacaoestoque.setMaterial(materiaprima.getMaterial());
		if (materiaprima.getFracaounidademedida() != null && materiaprima.getFracaounidademedida() != 0 && materiaprima.getUnidademedida() != null && 
				materiaprima.getMaterial().getUnidademedida() != null && !materiaprima.getUnidademedida().equals(materiaprima.getMaterial().getUnidademedida())) {
			materiaprima.getUnidademedida().setCasasdecimaisestoque(materiaprima.getCasasdecimaisestoqueunidademedida());
			movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(materiaprima.getQtde() * (materiaprima.getFracaounidademedida()  / (materiaprima.getQtdereferenciaunidademedida() != null ? materiaprima.getQtdereferenciaunidademedida() : 1.0)), materiaprima.getUnidademedida()));
		} else {
			if(materiaprima.getMaterial().getUnidademedida() != null){
				movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(materiaprima.getQtde(), materiaprima.getMaterial().getUnidademedida()));
				if(movimentacaoestoque.getQtde() == 0d){
					movimentacaoestoque.setQtde(materiaprima.getQtde());
				}
			}else {
				movimentacaoestoque.setQtde(materiaprima.getQtde());
			}
		}

		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
		movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
		movimentacaoestoque.setLocalarmazenagem(materiaprima.getLocalarmazenagem());
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setLoteestoque(materiaprima.getLoteestoque());
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		Material mat = materialService.load(materiaprima.getMaterial(), "material.cdmaterial, material.valorcusto");
		if(mat != null)
			movimentacaoestoque.setValor(mat.getValorcusto());
		
		movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);			
		
		if(StringUtils.isNotBlank(whereInOrdemProducao)){
			movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA ORDEM DE PRODU��O " + SinedUtil.makeLinkHistorico(movimentacaoestoqueorigem.getProducaoordem().getCdproducaoordem().toString(), "visualizarOrdemDeProducao"), true);					
		}else if(whereInAgendaProducao != null && !whereInAgendaProducao.equals("")){
			movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA AGENDA DE PRODU��O " + SinedUtil.makeLinkHistorico(movimentacaoestoqueorigem.getProducaoagenda().getCdproducaoagenda().toString(), "visualizarAgendaDeProducao"), true);	
		}
		
		
		return movimentacaoestoque;
	}
	
	/**
	 * M�todo que salva as ordens de produ��o.
	 *
	 * @param listaProducaoordemSalvar
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	private void saveListaOrdemproducaoMassa(List<Producaoordem> listaProducaoordemSalvar) {
		for (Producaoordem producaoordem : listaProducaoordemSalvar) {
			Set<Producaoordemmaterial> listaProducaoordemmaterial = producaoordem.getListaProducaoordemmaterial();
			producaoordem.setListaProducaoordemmaterial(null);
			producaoordem.setListaProducaoordemequipamento(null);
			
			this.saveOrUpdate(producaoordem);
			
			List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
			
			Integer i = 1;
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				if(StringUtils.isBlank(producaoordemmaterial.getIdentificadorinterno())){
					producaoordemmaterial.setIdentificadorinterno(System.currentTimeMillis() + i.toString() + producaoordemmaterial.getMaterial().getCdmaterial().toString());
				}
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = producaoordemmaterial.getListaProducaoordemmaterialorigem();
				
				producaoordemmaterial.setProducaoordem(producaoordem);
				producaoordemmaterialService.saveOrUpdate(producaoordemmaterial);
				
				for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
					Producaoagenda producaoagenda = producaoordemmaterialorigem.getProducaoagenda();
					boolean salvarHistorico = false;
					if(!listaProducaoagenda.contains(producaoagenda)){
						listaProducaoagenda.add(producaoagenda);
						salvarHistorico = true;
					}
					
					producaoordemmaterialorigem.setProducaoordemmaterial(producaoordemmaterial);
					producaoordemmaterialorigemService.saveOrUpdate(producaoordemmaterialorigem);
					
					producaoagendaService.updateSituacao(producaoagenda, Producaoagendasituacao.EM_ANDAMENTO);
					
					if(salvarHistorico){
						Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
						producaoagendahistorico.setProducaoagenda(producaoagenda);
						producaoagendahistorico.setObservacao("Ordem de produ��o " + this.makeLinkHistoricoProducaoordem(producaoordem) + " criada");
						producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
					}
				}
			}
			
			Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
			producaoordemhistorico.setProducaoordem(producaoordem);
			producaoordemhistorico.setObservacao("Criado a partir da(s) agenda(s) de produ��o: " + producaoagendaService.makeLinkHistoricoProducaoagenda(listaProducaoagenda));
			producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
			
			producaoordem.setListaProducaoordemmaterial(listaProducaoordemmaterial);
			producaoordemequipamentoService.preencherListaEquipamentos(producaoordem);
			if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemequipamento())){
				for(Producaoordemequipamento producaoordemequipamento : producaoordem.getListaProducaoordemequipamento()){
					producaoordemequipamento.setProducaoordem(producaoordem);
					producaoordemequipamentoService.saveOrUpdate(producaoordemequipamento);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findWithSituacaoDepartamento(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public List<Producaoordem> findWithSituacaoDepartamento(String whereIn) {
		return producaoordemDAO.findWithSituacaoDepartamento(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForRegistrarProducao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	public List<Producaoordem> findForRegistrarProducao(String whereIn) {
		return producaoordemDAO.findForRegistrarProducao(whereIn);
	}
	
	/**
	 * Prepara os dados para a tela de confer�ncia do registro de produ��o.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/01/2013
	 */
	@SuppressWarnings("unchecked")
	public List<Producaoordemmaterial> prepareForRegistroProducao(String whereIn) {
		List<Producaoordemmaterial> listaProducaoordemmaterialTela = new ArrayList<Producaoordemmaterial>();
		List<Producaoordem> listaProducaoordem = this.findForRegistrarProducao(whereIn);
		
		for (Producaoordem producaoordem : listaProducaoordem) {
			Set<Producaoordemmaterial> listaProducaoordemmaterial = producaoordem.getListaProducaoordemmaterial();
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				if(producaoordemmaterial.getQtdeproduzido() == null){
					producaoordemmaterial.setQtdeproduzido(0d);
				}
				if(producaoordemmaterial.getQtdeperdadescarte() == null){
					producaoordemmaterial.setQtdeperdadescarte(0d);
				}
				
				List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.findAtivosByEmpresa(producaoordem.getEmpresa());
				if(SinedUtil.isListNotEmpty(listaLocalarmazenagem) && listaLocalarmazenagem.size() == 1){
					producaoordemmaterial.setLocalarmazenagem(listaLocalarmazenagem.get(0));
				}

				producaoordemmaterial.setEmpresa(producaoordem.getEmpresa());
				producaoordemmaterial.setDtprevisaoentrega(producaoordem.getDtprevisaoentrega());
				producaoordemmaterial.setQtderegistro(producaoordemmaterial.getQtde() - producaoordemmaterial.getQtdeproduzido() - producaoordemmaterial.getQtdeperdadescarte());
				producaoordemmaterial.setQtderegistroperda(0d);
				
				listaProducaoordemmaterialTela.add(producaoordemmaterial);
			}
		}
		
		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialTela) {
			Producaoetapaitem producaoetapaitem = producaoordemmaterial.getProducaoetapaitem();
			boolean ultimaetapa = true;
			if(producaoetapaitem != null){
				ultimaetapa = producaoetapaService.isUltimaEtapa(producaoetapaitem);
			}
			producaoordemmaterial.setUltimaetapa(ultimaetapa);
			if(producaoordemmaterial.getPedidovendamaterial() != null && producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
				producaoordemmaterial.setHaveColeta(coletaMaterialService.haveColetaMaterialByPedidovendamaterial(producaoordemmaterial.getPedidovendamaterial()));
			}
		}
		
		// ORDENA PELO NOME DO MATERIAL
		Collections.sort(listaProducaoordemmaterialTela, new Comparator<Producaoordemmaterial>(){
			public int compare(Producaoordemmaterial o1, Producaoordemmaterial o2) {
				if(o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
					if(o1.getDtprevisaoentrega().equals(o2.getDtprevisaoentrega())){
						return o1.getProducaoordem().getCdproducaoordem().compareTo(o2.getProducaoordem().getCdproducaoordem());
					} else return o1.getDtprevisaoentrega().compareTo(o2.getDtprevisaoentrega());
				} else return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterialTela)){
			producaoordemmaterialcampoService.preencherListaCamposAdicionais(SinedUtil.listToSet(listaProducaoordemmaterialTela, Producaoordemmaterial.class));
		}
		return listaProducaoordemmaterialTela;
	}
	
	@SuppressWarnings("unchecked")
	public void registroProducao(Producaoordem bean) {
		Set<Producaoordemmaterial> listaProducaoordemmaterial = bean.getListaProducaoordemmaterial();
		
		List<Producaoordem> listaProducaoordemSalvar = new ArrayList<Producaoordem>();
		Set<Producaoordem> listaProducaoordemVerificar = new ListSet<Producaoordem>(Producaoordem.class);
		List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
		
		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
			if(!listaProducaoordemVerificar.contains(producaoordemmaterial.getProducaoordem())){
				listaProducaoordemVerificar.add(producaoordemmaterial.getProducaoordem());
			}
			
			Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = producaoordemmaterial.getListaProducaoordemmaterialorigem();
			if(listaProducaoordemmaterialorigem != null && listaProducaoordemmaterialorigem.size() > 0){
				for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
					if(producaoordemmaterialorigem.getProducaoagenda() != null && 
							producaoordemmaterialorigem.getProducaoagenda().getCdproducaoagenda() != null){
						listaProducaoagenda.add(producaoordemmaterialorigem.getProducaoagenda());
					}
				}
			}
			
			// ATUALIZAR A QTDE PRODUZIDA E A QTDE PERDIDA/DESCARTADA
			if(producaoordemmaterial.getQtderegistro() != null && producaoordemmaterial.getQtderegistro() > 0){
				Producaoordemmaterialproduzido producaoordemmaterialproduzido = new Producaoordemmaterialproduzido();
				producaoordemmaterialproduzido.setData(SinedDateUtils.currentDate());
				producaoordemmaterialproduzido.setProducaoordemmaterial(producaoordemmaterial);
				producaoordemmaterialproduzido.setQuantidade(producaoordemmaterial.getQtderegistro());
				producaoordemmaterialproduzidoService.saveOrUpdate(producaoordemmaterialproduzido);
				
				producaoordemmaterialService.adicionaQtdeProduzido(producaoordemmaterial, producaoordemmaterial.getQtderegistro());
			}
			List<Motivodevolucao> listaMotivodevolucao = new ArrayList<Motivodevolucao>();
			for(Producaoordemmaterialoperador producaoordemmaterialoperador: producaoordemmaterial.getListaProducaoordemmaterialoperador()){
				if(producaoordemmaterialoperador.getCdproducaoordemmaterialoperador() == null){
					producaoordemmaterialoperador.setProducaoordemmaterial(producaoordemmaterial);
					if(SinedUtil.isListNotEmpty(producaoordemmaterialoperador.getListaProducaoordemmaterialperdadescarte())){
						for(Producaoordemmaterialperdadescarte pomp: producaoordemmaterialoperador.getListaProducaoordemmaterialperdadescarte()){
							pomp.setProducaoordemmaterial(producaoordemmaterial);
							if(pomp.getMotivodevolucao()!= null && !listaMotivodevolucao.contains(pomp.getMotivodevolucao())){
								listaMotivodevolucao.add(pomp.getMotivodevolucao());
							}
						}
					}
					if(producaoordemmaterialoperador.getQtdeproduzido() == null){
						producaoordemmaterialoperador.setQtdeproduzido(0d);
					}
					if(producaoordemmaterialoperador.getQtdeperdadescarte() == null){
						producaoordemmaterialoperador.setQtdeperdadescarte(0d);
					}
					producaoordemmaterialoperadorService.saveOrUpdate(producaoordemmaterialoperador);
				}
			}
			if(producaoordemmaterial.getQtderegistroperda() != null && producaoordemmaterial.getQtderegistroperda() > 0){
				producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, producaoordemmaterial.getQtderegistroperda());
				if(producaoordemmaterial.getListaProducaoordemmaterialperdadescarte() != null){
					for(Producaoordemmaterialperdadescarte beanPerdadescarte: producaoordemmaterial.getListaProducaoordemmaterialperdadescarte()){
						if(beanPerdadescarte.getCdproducaoordemmaterialperdadescarte() == null){
							beanPerdadescarte.setProducaoordemmaterial(producaoordemmaterial);
							producaoordemmaterialperdadescarteService.saveOrUpdate(beanPerdadescarte);
							
							if(beanPerdadescarte.getMotivodevolucao() != null && !listaMotivodevolucao.contains(beanPerdadescarte.getMotivodevolucao())){
								listaMotivodevolucao.add(beanPerdadescarte.getMotivodevolucao());
							}
						}
					}
				}
				/*PROCESSO COMENTADO AT� ESCLARECIMENTOS SOBRE A AT 189123, QUE PEDIU ESSA IMPLEMENTA��O
				 * if(bean.isSalvarRegistrarProducaoLote() && producaoordemmaterial.getPedidovendamaterial() != null &&
						producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null && Boolean.TRUE.equals(producaoordemmaterial.getHaveColeta())){
					List<ColetaMaterial> listaColetamaterial = coletaMaterialService.findByPedidovendamaterial(producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial().toString());
					if(SinedUtil.isListNotEmpty(listaColetamaterial)){
						
						ColetaMaterial coletamaterial = listaColetamaterial.get(0);
						
						Material material = coletamaterial.getMaterial();
						if(material.getServico() == null || !material.getServico()){
							Materialclasse materialclasse = Materialclasse.PRODUTO;
							if(material.getProduto() == null || !material.getProduto()){
								if(material.getEpi() != null && material.getEpi()){
									materialclasse = Materialclasse.EPI;
								} else materialclasse = null;
							}
							
							if(materialclasse != null){
								Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
								movimentacaoestoque.setMaterial(coletamaterial.getMaterial());
								movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
								movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
								movimentacaoestoque.setQtde(producaoordemmaterial.getQtderegistroperda());
								movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
								movimentacaoestoque.setLocalarmazenagem(producaoordemmaterial.getLocalarmazenagem());
								movimentacaoestoque.setEmpresa(producaoordemmaterial.getEmpresa());
								Material mat = materialService.load(coletamaterial.getMaterial(), "material.cdmaterial, material.valorcusto");
								if(mat != null)
									movimentacaoestoque.setValor(mat.getValorcusto());
								
								Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
								movimentacaoestoqueorigem.setProducaoordem(producaoordemmaterial.getProducaoordem());
								
								movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
								
								movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
							}
						}
						
						coletaMaterialService.adicionaQtdeDevolvida(coletamaterial, producaoordemmaterial.getQtderegistroperda());
						
						coletamaterialmotivodevolucaoService.delete(coletamaterial);
						for(Motivodevolucao motivodevolucao: listaMotivodevolucao){
							coletamaterialmotivodevolucaoService.save(coletamaterial, motivodevolucao, false);
						}
						
						StringBuilder obs = new StringBuilder();
						if(coletamaterial.getMaterial()!=null)
							obs.append("Devolu��o do material: " + coletamaterial.getMaterial().getNome());
						
						Coletahistorico coletahistorico = new Coletahistorico();
						coletahistorico.setAcao(Coletaacao.DEVOLVIDO);
						coletahistorico.setColeta(coletamaterial.getColeta());
						coletahistorico.setObservacao(obs.toString());
						coletahistoricoService.saveOrUpdate(coletahistorico);						
					}
				}*/
			}
			
			if(bean.isSalvarRegistrarProducaoLote() && producaoordemmaterial.getQtderegistroperda() != null && producaoordemmaterial.getQtderegistroperda() > 0 &&
					SinedUtil.isListNotEmpty(producaoordemmaterial.getListaMateriaprimaperda())){
				for(ProducaoordemmaterialMaterialprimaPerdaBean beanMateriaPerda: producaoordemmaterial.getListaMateriaprimaperda()){
					if(!Boolean.TRUE.equals(beanMateriaPerda.getSelected())){
						continue;
					}
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setProducaoordem(producaoordemmaterial.getProducaoordem());
					movimentacaoestoqueorigem.setProducaoOrdemMaterial(producaoordemmaterial);
					movimentacaoestoqueorigem.setRegistrarproducao(true);

					Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
					movimentacaoestoque.setEmpresa(producaoordemmaterial.getEmpresa());
					movimentacaoestoque.setMaterial(beanMateriaPerda.getMateriaprima());
					movimentacaoestoque.setQtde(beanMateriaPerda.getConsumo());
					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
					movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
					movimentacaoestoque.setLocalarmazenagem(beanMateriaPerda.getLocalarmazenagem());
					movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
					
					Material mat = materialService.load(beanMateriaPerda.getMateriaprima(), "material.cdmaterial, material.valorcusto");
					if(mat != null)
						movimentacaoestoque.setValor(mat.getValorcusto());
					
					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
				}
			}

			if(producaoordemmaterial.getQtderegistro() <= 0) continue;
			
			boolean isServico = producaoordemmaterial.getMaterial() != null && producaoordemmaterial.getMaterial().getServico() != null && producaoordemmaterial.getMaterial().getServico();
			
			Producaoetapa producaoetapa = producaoordemmaterial.getProducaoetapa();
			
			Boolean naogerarentradaconclusao = producaoetapa != null && producaoetapa.getNaogerarentradaconclusao() != null && producaoetapa.getNaogerarentradaconclusao();
			Boolean naoagruparmaterial = producaoetapa != null && producaoetapa.getNaoagruparmaterial() != null && producaoetapa.getNaoagruparmaterial();
			
			// CASO N�O TENHA MAIS ETAPAS REGISTRAR NO ESTOQUE A ENTRADA DOS MATERIAIS 
			if(!isServico && !naogerarentradaconclusao && producaoordemmaterial.getUltimaetapa()){
				Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
				movimentacaoestoqueorigem.setProducaoordem(producaoordemmaterial.getProducaoordem());
				movimentacaoestoqueorigem.setProducaoOrdemMaterial(producaoordemmaterial);
				movimentacaoestoqueorigem.setRegistrarproducao(true);
				
				Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
				movimentacaoestoque.setEmpresa(producaoordemmaterial.getEmpresa());
				movimentacaoestoque.setMaterial(producaoordemmaterial.getMaterial());
				movimentacaoestoque.setQtde(new Double(producaoordemmaterial.getQtderegistro()));

				if(listaProducaoordemmaterialorigem != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterialorigem)){
					Producaoagendamaterial producaoagendamaterial = listaProducaoordemmaterialorigem.iterator().next().getProducaoagendamaterial();
					if(producaoagendamaterial != null){
						producaoagendamaterial = producaoagendamaterialService.loadForRegistroproducao(producaoagendamaterial);
						if(producaoagendamaterial.getAltura() != null || producaoagendamaterial.getLargura() != null || producaoagendamaterial.getComprimento() != null){
							Double altura = producaoagendamaterial.getAltura() != null ? producaoagendamaterial.getAltura() : 1d;
							Double largura = producaoagendamaterial.getLargura() != null ? producaoagendamaterial.getLargura() : 1d;
							Double comprimento = producaoagendamaterial.getComprimento() != null ? producaoagendamaterial.getComprimento() : 1d;
							
							if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
									producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
									producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
								if(largura != null && producaoagendamaterial.getLargura() != null) largura = largura / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
								if(altura != null && producaoagendamaterial.getAltura() != null) altura = altura / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
								if(comprimento != null && producaoagendamaterial.getComprimento() != null) comprimento = comprimento / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
							}
									
							movimentacaoestoque.setQtde(altura * largura * comprimento * movimentacaoestoque.getQtde());
						}
					}
				}
				
				movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
				movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
				movimentacaoestoque.setLocalarmazenagem(producaoordemmaterial.getLocalarmazenagem());
				movimentacaoestoque.setLoteestoque(producaoordemmaterial.getLoteestoque());
				movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
				movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
				
				Material mat = materialService.load(producaoordemmaterial.getMaterial(), "material.cdmaterial, material.valorcusto");
				if(mat != null)
					movimentacaoestoque.setValor(mat.getValorcusto());
				
				movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
				
				// REGISTRA NO HIST�RICO
				Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
				producaoordemhistorico.setProducaoordem(producaoordemmaterial.getProducaoordem());
				producaoordemhistorico.setObservacao("Entrada no estoque criada: " + movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(movimentacaoestoque));
				producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
			}
			
			// VERIFICAR E CRIAR CASO TENHA UMA PR�XIMA ETAPA DA ORDEM DE PRODU��O AGRUPADAS POR DEPARTAMENTO
			Producaoetapaitem producaoetapaitem = producaoordemmaterial.getProducaoetapaitem();
			if((producaoordemmaterial.getUltimaetapa() == null || !producaoordemmaterial.getUltimaetapa()) && producaoetapaitem != null){
				List<Producaoetapaitem> listaProducaoetapaitem = producaoetapaitemService.findByProducaoetapa(producaoetapaitem.getProducaoetapa());
				if(listaProducaoetapaitem != null && listaProducaoetapaitem.size() > 1){
					Producaoetapaitem proximaetapa = null;
					for (Producaoetapaitem etapa : listaProducaoetapaitem) {
						if(etapa.getOrdem().equals(producaoetapaitem.getOrdem()+1)){
							proximaetapa = etapa;
							break;
						}
					}
					
					if(proximaetapa != null){
						Departamento departamento = proximaetapa.getDepartamento();
						Date dtprevisaoentrega = proximaetapa.getQtdediasprevisto()  != null ? SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), proximaetapa.getQtdediasprevisto()) : SinedDateUtils.currentDate();
						
						Producaoordemmaterial producaoordemmaterialCadastro = new Producaoordemmaterial();
						producaoordemmaterialCadastro.setProducaoetapa(producaoetapaitem.getProducaoetapa());
						producaoordemmaterialCadastro.setMaterial(producaoordemmaterial.getMaterial());
						producaoordemmaterialCadastro.setUnidademedida(producaoordemmaterial.getUnidademedida());
						producaoordemmaterialCadastro.setQtde(producaoordemmaterial.getQtderegistro());
						producaoordemmaterialCadastro.setProducaoetapaitem(proximaetapa);
						producaoordemmaterialCadastro.setPedidovendamaterial(producaoordemmaterial.getPedidovendamaterial());
						producaoordemmaterialCadastro.setPneu(producaoordemmaterial.getPneu());
						producaoordemmaterialCadastro.setListaProducaoordemmaterialorigem(listaProducaoordemmaterialorigem);
						producaoordemmaterialCadastro.setListaProducaoordemmaterialperdadescarte(producaoordemmaterial.getListaProducaoordemmaterialperdadescarte());
						
						boolean achou = false;
						for (Producaoordem producaoordem : listaProducaoordemSalvar) {
							boolean departamentoIgual = (departamento != null && producaoordem.getDepartamento() != null && departamento.equals(producaoordem.getDepartamento())) 
														|| (departamento == null && producaoordem.getDepartamento() == null);
							boolean dtprevisaoentregaIgual = SinedDateUtils.equalsIgnoreHour(dtprevisaoentrega, producaoordem.getDtprevisaoentrega());
							
							if(departamentoIgual && dtprevisaoentregaIgual && !naoagruparmaterial){
								producaoordem.getListaProducaoordemmaterial().add(producaoordemmaterialCadastro);
								achou = true;
								break;
							}
						}
						
						if(!achou){
							Producaoordem producaoordem = new Producaoordem();
							producaoordem.setEmpresa(producaoordemmaterial.getEmpresa());
							producaoordem.setDepartamento(departamento);
							producaoordem.setDtprevisaoentrega(dtprevisaoentrega);
							producaoordem.setDtproducaoordem(SinedDateUtils.currentDate());
							producaoordem.setProducaoordemsituacao(Producaoordemsituacao.EM_ESPERA);
							producaoordem.setProducaoordemanterior(producaoordemmaterial.getProducaoordem());
							
							Set<Producaoordemmaterial> listaProducaoordemmaterialCadastro = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
							listaProducaoordemmaterialCadastro.add(producaoordemmaterialCadastro);
							
							producaoordem.setListaProducaoordemmaterial(listaProducaoordemmaterialCadastro);
							
							listaProducaoordemSalvar.add(producaoordem);
						}
					}
				}
			}
		}
		
		// SALVA AS ORDENS DE PRODU��O
		this.saveListaOrdemproducaoMassa(listaProducaoordemSalvar);
		
		// VERIFICAR E ATUALIZAR A SITUA��O DA ORDEM DE PRODU��O
		for (Producaoordem producaoordem : listaProducaoordemVerificar) {
			boolean concluido = true;
			boolean emandamento = false;
			
			List<Producaoordemmaterial> listaProducaoordemmaterialVerificar = producaoordemmaterialService.findByProducaoordem(producaoordem);
			List<Producaoordemitemadicional> listaProducaoordemitemadicional = producaoordemitemadicionalService.findByProducaoordem(producaoordem);
			
			for (Producaoordemmaterial producaoordemmaterialVerificar : listaProducaoordemmaterialVerificar) {
				Double qtde = producaoordemmaterialVerificar.getQtde() != null ? producaoordemmaterialVerificar.getQtde() : 0d;
				Double qtdeproduzido = producaoordemmaterialVerificar.getQtdeproduzido() != null ? producaoordemmaterialVerificar.getQtdeproduzido() : 0d;
				Double qtdeperdadescarte = producaoordemmaterialVerificar.getQtdeperdadescarte() != null ? producaoordemmaterialVerificar.getQtdeperdadescarte() : 0d;
				
				if(qtdeproduzido > 0d || qtdeperdadescarte > 0d){
					emandamento = true;
				}
				if((!parametrogeralService.getBoolean(Parametrogeral.PERMITIRPRODUZIR_QTDE_SUP_PREVISTO) && !qtde.equals(qtdeproduzido + qtdeperdadescarte)) ||
				   (parametrogeralService.getBoolean(Parametrogeral.PERMITIRPRODUZIR_QTDE_SUP_PREVISTO) && qtde > (qtdeproduzido + qtdeperdadescarte))){
					concluido = false;
				}else {
					for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
						if(producaoordemmaterial.getCdproducaoordemmaterial() != null && 
								producaoordemmaterial.getCdproducaoordemmaterial().equals(producaoordemmaterialVerificar.getCdproducaoordemmaterial())){
							producaoordemmaterial.setConcluido(true);
						}
					}
				}
			}
			
			Producaoordemsituacao producaoordemsituacao = emandamento ? Producaoordemsituacao.EM_ANDAMENTO : Producaoordemsituacao.EM_ESPERA;
			if(concluido){
				producaoordemsituacao = Producaoordemsituacao.CONCLUIDA;
			}
			this.updateSituacao(producaoordem, producaoordemsituacao);
			
			// REGISTRA NO HIST�RICO
			Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
			producaoordemhistorico.setProducaoordem(producaoordem);
			producaoordemhistorico.setObservacao(bean.getObservacaoHistorico() != null ? bean.getObservacaoHistorico() : "Produ��o registrada");
			producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
			
			if(producaoordemsituacao.equals(Producaoordemsituacao.CONCLUIDA)){
				this.doBaixaEstoqueItemAdicional(null, producaoordem, SinedUtil.listToSet(listaProducaoordemitemadicional, Producaoordemitemadicional.class));
				Date dtConclusao = SinedDateUtils.currentDate();
				this.updateDataConclusaoOrdemProducao(producaoordem, dtConclusao);
			}
			
			garantiareformaService.saveProducao(producaoordem, false, false);
		}
		
		// VERIFICAR E ATUALIZAR A SITUA��O DAS AGENDAS DE PRODU��O DE ORIGEM
		if(listaProducaoagenda != null && listaProducaoagenda.size() > 0){
			producaoagendaService.verificaConclusaoProducaoagenda(CollectionsUtil.listAndConcatenate(listaProducaoagenda, "cdproducaoagenda", ","));
		}
	}
	
	private void updateDataConclusaoOrdemProducao(Producaoordem producaoordem, Date dtConclusao) {
		producaoordemDAO.updateDataConclusaoOrdemProducao(producaoordem, dtConclusao);
	}
	/**
	 * M�todo que registra sa�da das mat�rias primas de ordem de produ��o ao final da �ltima etapa de produ��o
	 * @param whereIn
	 * @since 05/06/2015
	 * @author Danilo Guimar�es
	 */
	public void baixarEstoqueMateriaPrimaProducaoOrdem(Producaoordemmaterial producaoordemmaterial, String whereInAgendaproducao, String whereInProducaoagendamaterial, List<MateriaprimaConferenciaProducaoBean> materiasPrimas, Boolean isProducaoEmLote){
		List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = new ListSet<MateriaprimaConferenciaProducaoBean>(MateriaprimaConferenciaProducaoBean.class);
		ConferenciaProducaoBean bean = new ConferenciaProducaoBean();
		List<Material> listaMaterialWithQtde = new ArrayList<Material>();
		String valor = ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.SOLICITARCOMPRAPRODUCAO);
		NeoWeb.getRequestContext().setAttribute("SOLICITARCOMPRAPRODUCAO", valor);
		
		Producaoetapaitem producaoetapaitem = producaoordemmaterial.getProducaoetapaitem();
		Producaoetapa producaoetapa = producaoordemmaterial.getProducaoetapa();

		if(Boolean.TRUE.equals(isProducaoEmLote) || (producaoordemmaterial.getQtdeBaixaEstoque() != null && producaoordemmaterial.getQtdeBaixaEstoque() > 0)){
			producaoordemmaterial.getMaterial().setQtdereferencia(producaoordemmaterial.getQtdeBaixaEstoque());
			listaMaterialWithQtde.add(producaoordemmaterial.getMaterial());
		}else if(producaoordemmaterial.getQtde() != null && producaoordemmaterial.getQtde() > 0){
			producaoordemmaterial.getMaterial().setQtdereferencia(producaoordemmaterial.getQtde());
			listaMaterialWithQtde.add(producaoordemmaterial.getMaterial());
		}
		
		if(producaoetapa == null || BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(producaoetapa.getBaixarEstoqueMateriaprima())){
			if(StringUtils.isNotBlank(producaoordemmaterial.getWhereInProducaoordem())){
				String cdproducaoordemmaterial = producaoordemmaterial.getCdproducaoordemmaterial()!=null? producaoordemmaterial.getCdproducaoordemmaterial().toString(): null;
				if(cdproducaoordemmaterial == null && producaoordemmaterial.getWhereInProducaoordemmaterial() != null){
					Integer idUnico = SinedUtil.getIdUnicoWhereIn(producaoordemmaterial.getWhereInProducaoordemmaterial());
					if(idUnico != null) cdproducaoordemmaterial = idUnico.toString();
				}
				List<Producaoordemmaterial> listaProducaoordemmaterial = producaoordemmaterialService.findByProducaoordem(producaoordemmaterial.getWhereInProducaoordem(), cdproducaoordemmaterial);
				for (Producaoordemmaterial pom : listaProducaoordemmaterial) {
					Material material = pom.getMaterial();
					if(!listaMaterialWithQtde.contains(material)) continue;
					
					Material mat_aux = listaMaterialWithQtde.get(listaMaterialWithQtde.indexOf(material));
					
					/*No registro de produ��o em lote, pode ser feita a produ��o de apenas parte da quantidade prevista. Da mesma forma, pode n�o haver produ��o, apenas perda.
					 * Dessa forma, quanfo houver perda, n�o teremos quantidade produzida e, consequentemente, n�o teremos mat�ria-prima pra baixar nesse momento, pois mat�ria-prima de perda
					 * � baixada em outro momento(Existe um popup pra definir quanto de mat�ria-prima foi usado nas perdas).*/
					if(Boolean.TRUE.equals(isProducaoEmLote) && (mat_aux.getQtdereferencia() == null || mat_aux.getQtdereferencia() <= 0)){
						continue;
					}
					
					Producaoordem producaoordem = pom.getProducaoordem();
					Empresa empresa = producaoordem != null ? producaoordem.getEmpresa() : null;
					Pneu pneu = pom.getPneu();
					
					Producaoetapa producaoetapa_material = pom.getProducaoetapa();
					if(producaoetapa_material == null && material.getProducaoetapa() != null){
						pom.setBaixarmateriaprimacascata(material.getProducaoetapa().getBaixarmateriaprimacascata());
					}
					
					
					Boolean baixarmateriaprimacascata = (pom.getProducaoetapa() != null && pom.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
														pom.getProducaoetapa().getBaixarmateriaprimacascata()) ||
														(pom.getProducaoetapa() == null && pom.getBaixarmateriaprimacascata() != null && 
																pom.getBaixarmateriaprimacascata());
					
					Double qtdeprevista = pom.getQtde();
					Double qtdeproduzir = mat_aux != null ? mat_aux.getQtdereferencia() : pom.getQtde();
					Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
					
					if(SinedUtil.isListNotEmpty(pom.getListaProducaoordemmaterialmateriaprima())){
						for (Producaoordemmaterialmateriaprima mp : pom.getListaProducaoordemmaterialmateriaprima()) {
							Material m = mp.getMaterial();
							if((producaoetapa == null && producaoetapaitem == null) ||
									(producaoetapa != null && producaoetapa.getBaixarEstoqueMateriaprima() != null && 
											BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) && 
											mp.getProducaoetapanome() == null) ||
									(producaoetapa != null && producaoetapa.getBaixarEstoqueMateriaprima() != null && 
											BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) &&
											pom.getUltimaetapa() != null && pom.getUltimaetapa()) ||
									(producaoetapaitem != null &&
											producaoetapaitem.getProducaoetapanome().equals(mp.getProducaoetapanome()))){
								Double consumo = ((mp.getQtdeprevista() != null ? mp.getQtdeprevista() : 1d) / qtdeprevista / qtdereferencia);
								if(qtdeproduzir != null){
									consumo = consumo * qtdeproduzir;
								}
								
								boolean achou = false;
								if(isNotServicoEpiPatrimonio(mp.getMaterial())){
									for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
										if(materiaprima.getMaterial().equals(m)){
											materiaprima.setQtde(materiaprima.getQtde() + consumo);
											achou = true;
										}
									}
								}
								
								if(!achou){
									MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
									materiaprima.setEmpresa(empresa);
									materiaprima.setMaterial(m);
									materiaprima.setQtde(consumo);
									materiaprima.setUnidademedida(mp.getUnidademedida());
									materiaprima.setFracaounidademedida(mp.getFracaounidademedida());
									materiaprima.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
									materiaprima.setLoteestoque(mp.getLoteestoque());
									materiaprima.setWhereInOrdemProducao(pom.getWhereInProducaoordem());
									materiaprima.setProducaoOrdemMaterial(producaoordemmaterial);
									if(StringUtils.isBlank(pom.getWhereInProducaoordem()) && producaoordem != null && producaoordem.getCdproducaoordem() != null){
										materiaprima.setWhereInOrdemProducao(producaoordem.getCdproducaoordem().toString());
									}
									materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
									
									if(mp.getUnidademedida() != null){
										materiaprima.setCasasdecimaisestoqueunidademedida(mp.getUnidademedida().getCasasdecimaisestoque());
									}
									
									addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(mp.getMaterial()));
								}
							}
						}
					}else {
						Producaoagendamaterial pam = producaoagendamaterialService.getProducaoagendamaterial(pom);
						boolean dimensoesProducao = pam != null && (pam.getAltura() != null || 
																	pam.getLargura() != null ||
																	pam.getComprimento() != null);
						boolean corteProducao = pom.getProducaoetapa() != null && pom.getProducaoetapa().getCorteproducao() != null ? pom.getProducaoetapa().getCorteproducao() : false;
						Double larguraProducao = pam != null && pam.getLargura() != null ? pam.getLargura() : 1d;
						Double alturaProducao = pam != null && pam.getAltura() != null ? pam.getAltura() : 1d;
						Double comprimentoProducao = pam != null && pam.getComprimento() != null ? pam.getComprimento() : 1d;
						Double qtdeProduzir = pom.getQtde() != null ? pom.getQtde() : 1d;
						Double b = (larguraProducao * alturaProducao * comprimentoProducao);
						
						material = materialService.loadMaterialComMaterialproducao(material);
						material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
						
						try{
							material.setProduto_altura(pam != null ? pam.getAltura() : null);
							material.setProduto_largura(pam != null ? pam.getLargura() : null);
							material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
							materialproducaoService.getValorvendaproducao(material);			
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						for (Materialproducao materialproducao : material.getListaProducao()) {
							Material m = materialproducao.getMaterial();
							if((producaoetapa == null && producaoetapaitem == null) ||
									(producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) && 
											materialproducao.getProducaoetapanome() == null) ||
									(producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) &&
											pom.getUltimaetapa() != null && pom.getUltimaetapa()) ||
									(producaoetapaitem != null && producaoetapaitem.getProducaoetapanome() != null &&
											producaoetapaitem.getProducaoetapanome().equals(materialproducao.getProducaoetapanome()))){
								if(SinedUtil.isRecapagem() &&
										materialproducao.getExibirvenda() != null &&
										materialproducao.getExibirvenda() &&
										pneu != null &&
										pneu.getMaterialbanda() != null){
									m = pneu.getMaterialbanda();
								}
								
								Double consumo = qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia);
								
								if(dimensoesProducao && corteProducao){
									Produto produto = pom.getMaterial().getMaterialproduto();
									if(produto != null){
										Double largura = produto.getLargura() != null ? produto.getLargura()/1000d : 1d;
										Double altura = produto.getAltura() != null ? produto.getAltura()/1000d : 1d;
										Double comprimento = produto.getComprimento() != null ? produto.getComprimento()/1000d : 1d;
										
										Double a = (largura * altura * comprimento);
										BigDecimal c = new BigDecimal(b/a);
										
										consumo = SinedUtil.roundUp(new BigDecimal(c.doubleValue() * qtdeProduzir), 0).doubleValue() * materialproducao.getConsumo();
									}
								}
								boolean achou = false;
								if(isNotServicoEpiPatrimonio(materialproducao.getMaterial())){
									for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
										if(materiaprima.getMaterial().equals(m)){
											materiaprima.setQtde(materiaprima.getQtde() + consumo);
											achou = true;
										}
									}
								}
								
								if(!achou){
									MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
									materiaprima.setEmpresa(empresa);
									materiaprima.setMaterial(m);
									materiaprima.setQtde(consumo);
									materiaprima.setWhereInOrdemProducao(pom.getWhereInProducaoordem());
									materiaprima.setProducaoOrdemMaterial(producaoordemmaterial);
									materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
									if(StringUtils.isBlank(pom.getWhereInProducaoordem()) && producaoordem != null && producaoordem.getCdproducaoordem() != null){
										materiaprima.setWhereInOrdemProducao(producaoordem.getCdproducaoordem().toString());
									}
									
									addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(materialproducao.getMaterial()));
								}
							}
						}
					}
				}
			}
		}else if(StringUtils.isNotBlank(whereInAgendaproducao)){
			List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findByProducaoagenda(whereInAgendaproducao, whereInProducaoagendamaterial);
			for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
				
				Material material = producaoagendamaterial.getMaterial();
				Producaoagenda producaoagenda = producaoagendamaterial.getProducaoagenda();
				Empresa empresa = producaoagenda != null ? producaoagenda.getEmpresa() : null;
				Pneu pneu = producaoagendamaterial.getPneu();
				
				if(!listaMaterialWithQtde.contains(material)) continue;
				
				Boolean baixarmateriaprimacascata = producaoagendamaterial.getProducaoetapa() != null && producaoagendamaterial.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
								producaoagendamaterial.getProducaoetapa().getBaixarmateriaprimacascata();
				Material mat_aux = listaMaterialWithQtde.get(listaMaterialWithQtde.indexOf(material));
				/*No registro de produ��o em lote, pode ser feita a produ��o de apenas parte da quantidade prevista. Da mesma forma, pode n�o haver produ��o, apenas perda.
				 * Dessa forma, quanfo houver perda, n�o teremos quantidade produzida e, consequentemente, n�o teremos mat�ria-prima pra baixar nesse momento, pois mat�ria-prima de perda
				 * � baixada em outro momento(Existe um popup pra definir quanto de mat�ria-prima foi usado nas perdas).*/
				if(Boolean.TRUE.equals(isProducaoEmLote) && (mat_aux.getQtdereferencia() == null || mat_aux.getQtdereferencia() <= 0)){
					continue;
				}
				
				Double qtdeprevista = producaoagendamaterial.getQtde() != null ? producaoagendamaterial.getQtde() : 1d;
				Double qtdeproduzir = mat_aux != null ? mat_aux.getQtdereferencia() : producaoagendamaterial.getQtde();
				Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
				
				if(SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
					for (Producaoagendamaterialmateriaprima mp : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
						Material m = mp.getMaterial();
						if((producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) && 
										mp.getProducaoetapanome() == null) ||
								(producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) &&
										producaoordemmaterial.getUltimaetapa() != null && producaoordemmaterial.getUltimaetapa()) ||
								(producaoetapaitem != null &&
										producaoetapaitem.getProducaoetapanome().equals(mp.getProducaoetapanome()))){
							Double consumo = ((mp.getQtdeprevista() != null ? mp.getQtdeprevista() : 1d) / qtdeprevista / qtdereferencia);
							if(qtdeproduzir != null){
								consumo = consumo * qtdeproduzir;
							}
							
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(mp.getMaterial())){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setUnidademedida(mp.getUnidademedida());
								materiaprima.setFracaounidademedida(mp.getFracaounidademedida());
								materiaprima.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
								materiaprima.setWhereInOrdemProducao(producaoordemmaterial.getWhereInProducaoordem());
								materiaprima.setWhereInAgendaProducao(whereInAgendaproducao);
								materiaprima.setProducaoAgendaMaterial(producaoagendamaterial);
								materiaprima.setProducaoOrdemMaterial(producaoordemmaterial);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								setWhereInOrdemProducaoAgendaProducaoMateriaprima(materiaprima, materiasPrimas, whereInAgendaproducao);
								
								if(mp.getUnidademedida() != null){
									materiaprima.setCasasdecimaisestoqueunidademedida(mp.getUnidademedida().getCasasdecimaisestoque());
								}
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(mp.getMaterial()));
							}
						}
					}
				}else {
					boolean dimensoesProducao = producaoagendamaterial.getAltura() != null || 
												producaoagendamaterial.getLargura() != null ||
												producaoagendamaterial.getComprimento() != null;
					boolean corteProducao = producaoagendamaterial.getProducaoetapa() != null &&
											producaoagendamaterial.getProducaoetapa().getCorteproducao() != null ?
											producaoagendamaterial.getProducaoetapa().getCorteproducao() : false;
					Double larguraProducao = producaoagendamaterial.getLargura() != null ? producaoagendamaterial.getLargura() : 1d;
					Double alturaProducao = producaoagendamaterial.getAltura() != null ? producaoagendamaterial.getAltura() : 1d;
					Double comprimentoProducao = producaoagendamaterial.getComprimento() != null ? producaoagendamaterial.getComprimento() : 1d;
					Double qtdeProduzir = producaoagendamaterial.getQtde() != null ? producaoagendamaterial.getQtde() : 1d;
					
					if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
						if(larguraProducao != null && producaoagendamaterial.getLargura() != null) larguraProducao = larguraProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(alturaProducao != null && producaoagendamaterial.getAltura() != null) alturaProducao = alturaProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(comprimentoProducao != null && producaoagendamaterial.getComprimento() != null) comprimentoProducao = comprimentoProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
					}
					
					Double b = (larguraProducao * alturaProducao * comprimentoProducao);
					
					material = materialService.loadMaterialComMaterialproducao(material);
					material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
					
					try{
						material.setProduto_altura(producaoagendamaterial.getAltura());
						material.setProduto_largura(producaoagendamaterial.getLargura());
						material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
						materialproducaoService.getValorvendaproducao(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					for (Materialproducao materialproducao : material.getListaProducao()) {
						Material m = materialproducao.getMaterial();
						if((producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) && 
										materialproducao.getProducaoetapanome() == null) ||
								(producaoetapa != null && BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(producaoetapa.getBaixarEstoqueMateriaprima()) &&
										producaoordemmaterial.getUltimaetapa() != null && producaoordemmaterial.getUltimaetapa()) ||
								(producaoetapaitem != null &&
										producaoetapaitem.getProducaoetapanome().equals(materialproducao.getProducaoetapanome()))){
							if(SinedUtil.isRecapagem() &&
									materialproducao.getExibirvenda() != null &&
									materialproducao.getExibirvenda() &&
									pneu != null &&
									pneu.getMaterialbanda() != null){
								m = pneu.getMaterialbanda();
							}
							
							Double consumo = qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia);
							
							if(dimensoesProducao && corteProducao){
								Produto produto = producaoagendamaterial.getMaterial().getMaterialproduto();
								if(produto != null){
									Double largura = produto.getLargura() != null && produto.getLargura() > 0 ? produto.getLargura() : 1d;
									Double altura = produto.getAltura() != null && produto.getAltura() > 0 ? produto.getAltura() : 1d;
									Double comprimento = produto.getComprimento() != null && produto.getComprimento() > 0 ? produto.getComprimento() : 1d;
									
									Double fatorconversaoproducao = 1000d;
									if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
											producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
											producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
										fatorconversaoproducao = producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
									}
									if(fatorconversaoproducao != null && fatorconversaoproducao > 0){
										if(largura != null && producaoagendamaterial.getLargura() != null) largura = largura / fatorconversaoproducao;
										if(altura != null && producaoagendamaterial.getAltura() != null) altura = altura / fatorconversaoproducao;
										if(comprimento != null && producaoagendamaterial.getComprimento() != null) comprimento = comprimento / fatorconversaoproducao;
									}
									
									Double a = (largura * altura * comprimento);
									BigDecimal c = new BigDecimal(b/a);
									
									consumo = SinedUtil.roundUp(new BigDecimal(c.doubleValue() * qtdeProduzir), 0).doubleValue() * materialproducao.getConsumo();
								}
							}
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(materialproducao.getMaterial())){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setWhereInOrdemProducao(producaoordemmaterial.getWhereInProducaoordem());
								materiaprima.setWhereInAgendaProducao(whereInAgendaproducao);
								materiaprima.setProducaoAgendaMaterial(producaoagendamaterial);
								materiaprima.setProducaoOrdemMaterial(producaoordemmaterial);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								setWhereInOrdemProducaoAgendaProducaoMateriaprima(materiaprima, materiasPrimas, whereInAgendaproducao);
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(materialproducao.getMaterial()));
							}
						}
					}
				}
			}
		}
		bean.setListaMateriaprima(listaMateriaprima);
		
		List<MateriaprimaConferenciaProducaoBean> listaMateriaprimaAux = bean.getListaMateriaprima();
		
		if(materiasPrimas != null && !materiasPrimas.isEmpty()){
			for (MateriaprimaConferenciaProducaoBean mp1 : materiasPrimas) {
				mp1.setQtde(SinedUtil.round(mp1.getQtde(), 10));
				for (MateriaprimaConferenciaProducaoBean mp2 : listaMateriaprimaAux) {
					mp2.setQtde(SinedUtil.round(mp2.getQtde(), 10));
					if(mp1.getMaterial().equals(mp2.getMaterial()) && mp1.getQtde().equals(mp2.getQtde())){
						mp2.setLocalarmazenagem(mp1.getLocalarmazenagem());
						mp2.setLoteestoque(mp1.getLoteestoque());
					}
				}
			}
		}
		
		List<Movimentacaoestoque> listaMovimentacaoestoqueMateriaprima = new ArrayList<Movimentacaoestoque>();
		StringBuilder listaWhereInAgendaProducao = new StringBuilder();
		for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprimaAux) {
			if(isNotServicoEpiPatrimonio(materiaprima.getMaterial())){
				listaMovimentacaoestoqueMateriaprima.add(saidaEstoqueMateriasPrimas(materiaprima, listaWhereInAgendaProducao, producaoordemmaterial));
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaMovimentacaoestoqueMateriaprima)){
			HashMap<Producaoordem, StringBuilder> mapProducaoordemHistorico = new HashMap<Producaoordem, StringBuilder>();
			HashMap<Producaoagenda, StringBuilder> mapProducaoagendaHistorico = new HashMap<Producaoagenda, StringBuilder>();
			
			String link;
			for(Movimentacaoestoque me : listaMovimentacaoestoqueMateriaprima){
				if(me.getMovimentacaoestoqueorigem() != null){
					link = "<a href=\"javascript:visualizarMovimentacaoestoque("+me.getCdmovimentacaoestoque()+");\">"+me.getCdmovimentacaoestoque()+"</a>,";
					if(me.getMovimentacaoestoqueorigem().getProducaoordem() != null && me.getMovimentacaoestoqueorigem().getProducaoagenda() == null){
						if(mapProducaoordemHistorico.get(me.getMovimentacaoestoqueorigem().getProducaoordem()) == null){
							mapProducaoordemHistorico.put(me.getMovimentacaoestoqueorigem().getProducaoordem(), new StringBuilder()); 
						}
						mapProducaoordemHistorico.get(me.getMovimentacaoestoqueorigem().getProducaoordem()).append(link);
					}else if(me.getMovimentacaoestoqueorigem().getProducaoagenda() != null){
						if(mapProducaoagendaHistorico.get(me.getMovimentacaoestoqueorigem().getProducaoagenda()) == null){
							mapProducaoagendaHistorico.put(me.getMovimentacaoestoqueorigem().getProducaoagenda(), new StringBuilder()); 
						}
						mapProducaoagendaHistorico.get(me.getMovimentacaoestoqueorigem().getProducaoagenda()).append(link);
					}
				}
			}
			
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			Integer cdusuarioaltera = null;
			Usuario usuario = SinedUtil.getUsuarioLogado();
			if(usuario != null){
				cdusuarioaltera = usuario.getCdpessoa();
			}
			if(mapProducaoordemHistorico.size() > 0){
				Producaoordemhistorico poh;
				for(Producaoordem producaoordem : mapProducaoordemHistorico.keySet()){
					poh = new Producaoordemhistorico();
					poh.setProducaoordem(producaoordem);
					poh.setCdusuarioaltera(cdusuarioaltera);
					poh.setDtaltera(dtaltera);
					
					link = mapProducaoordemHistorico.get(producaoordem).toString();
					poh.setObservacao("Sa�da de mat�ria-prima: " + link.substring(0, link.length()-1));
					
					producaoordemhistoricoService.saveOrUpdate(poh);
				}
			}
			if(mapProducaoagendaHistorico.size() > 0){
				Producaoagendahistorico pah;
				for(Producaoagenda producaoagenda : mapProducaoagendaHistorico.keySet()){
					pah = new Producaoagendahistorico();
					pah.setProducaoagenda(producaoagenda);
					pah.setCdusuarioaltera(cdusuarioaltera);
					pah.setDtaltera(dtaltera);
					
					link = mapProducaoagendaHistorico.get(producaoagenda).toString();
					pah.setObservacao("Sa�da de mat�ria-prima: " + link.substring(0, link.length()-1));
					
					producaoagendahistoricoService.saveOrUpdate(pah);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#updateSituacao(Producaoordem producaoordem, Producaoordemsituacao producaoordemsituacao)
	 *
	 * @param producaoordem
	 * @param producaoordemsituacao
	 * @author Rodrigo Freitas
	 * @since 15/01/2013
	 */
	public void updateSituacao(Producaoordem producaoordem, Producaoordemsituacao producaoordemsituacao) {
		producaoordemDAO.updateSituacao(producaoordem, producaoordemsituacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param producaoordemsituacao
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public void updateSituacao(String whereIn, Producaoordemsituacao producaoordemsituacao) {
		producaoordemDAO.updateSituacao(whereIn, producaoordemsituacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#loadWithLista(String whereIn, String orderBy, boolean asc)
	 *
	 * @param whereIn
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/01/2013
	 */
	public List<Producaoordem> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return producaoordemDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	/**
	 * Cria a lista de beans para a emiss�o de ordem de produ��o
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public LinkedList<EmitirProducaoordemReportBean> makeProducaoordemReportTemplate(String whereIn) {
		List<Producaoordem> listaProducaoordem = this.loadForReporTemplate(whereIn);
		
		LinkedList<EmitirProducaoordemReportBean> lista = new LinkedList<EmitirProducaoordemReportBean>();
		
		for (Producaoordem producaoordem : listaProducaoordem) {
			EmitirProducaoordemReportBean bean = new EmitirProducaoordemReportBean();
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			bean.setCdproducaoordem(producaoordem.getCdproducaoordem());
			bean.setDepartamento_nome(producaoordem.getDepartamento() != null ? producaoordem.getDepartamento().getNome() : "");
			bean.setDtprevisaoentrega(producaoordem.getDtprevisaoentrega() != null ? format.format(producaoordem.getDtprevisaoentrega()) : "");
			bean.setDtproducaoordem(producaoordem.getDtproducaoordem() != null ? format.format(producaoordem.getDtproducaoordem()) : "");
			bean.setHistorico(producaoordem.getHistorico());
			bean.setProducaoordemsituacao(producaoordem.getProducaoordemsituacao() != null ? producaoordem.getProducaoordemsituacao().getNome(): "");
			bean.setListaProducaoordemmaterialReportBean(makeProducaoordemmaterialReportTemplate(producaoordem));
			
			lista.add(bean);
		}
		
		return lista;
	}
	
	/**
	 * Faz a cria��o dos itens para a emiss�o de ordem de produ��o
	 *
	 * @param producaoordem
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public LinkedList<ProducaoordemmaterialReportBean> makeProducaoordemmaterialReportTemplate(Producaoordem producaoordem) {
		LinkedList<ProducaoordemmaterialReportBean> lista = new LinkedList<ProducaoordemmaterialReportBean>();
		if(producaoordem != null && producaoordem.getListaProducaoordemmaterial() != null && 
				!producaoordem.getListaProducaoordemmaterial().isEmpty()){
			for(Producaoordemmaterial item : producaoordem.getListaProducaoordemmaterial()){
				ProducaoordemmaterialReportBean bean = new ProducaoordemmaterialReportBean(item);
				lista.add(bean);
			}
		}
		return lista;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/07/2015
	 */
	public List<Producaoordem> loadForReporTemplate(String whereIn) {
		return producaoordemDAO.loadForReporTemplate(whereIn);
	}
	
	/**
	* M�todo que verifica se o material est� na lista dos itens com a qtde maior do que zero 
	*
	* @param listaProducaoagenda
	* @param producaoagenda
	* @param material
	* @return
	* @since 24/06/2014
	* @author Luiz Fernando
	*/
	public boolean existMaterial(List<Producaoagenda> listaProducaoagenda, Producaoagenda producaoagenda, Material material) {
		if(listaProducaoagenda != null && !listaProducaoagenda.isEmpty() && material != null){
			for (Producaoagenda bean : listaProducaoagenda) {
				if(bean.equals(producaoagenda) && bean.getMaterialmestreProduzir() != null && 
						bean.getMaterialProduzir().equals(material)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForDevolverColeta(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Producaoordem> findForDevolverColeta(String whereIn) {
		return producaoordemDAO.findForDevolverColeta(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public boolean haveProducaoordemSituacaoByProducaoagenda(String whereIn, Producaoordemsituacao... situacoes) {
		return producaoordemDAO.haveProducaoordemSituacaoByProducaoagenda(whereIn, situacoes);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public List<Producaoordem> findByProducaoagenda(String whereInProducaoagenda) {
		return producaoordemDAO.findByProducaoagenda(whereInProducaoagenda);
	}
	
	public boolean existeProducaoagenda(Producaoordem producaoordem) {
		return producaoordemDAO.existeProducaoagenda(producaoordem);
	}
	
	public Producaoordem findByPneuAndProducaoagendamaterial(Pneu pneu, Producaoagendamaterial producaoagendamaterial) {
		return producaoordemDAO.findByPneuAndProducaoagendamaterial(pneu, producaoagendamaterial);
	}
	
	/**
	 * Cria a lista de mnat�ria-prima para a conferencia de produ��o
	 *
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/08/2014
	 */
	public List<MateriaprimaConferenciaProducaoBean> makeListaMateriaprimaProducao(ConferenciaProducaoBean bean, Boolean isAgendaproducao) {
		List<MateriaprimaConferenciaProducaoBean> listaMateriaprima = new ListSet<MateriaprimaConferenciaProducaoBean>(MateriaprimaConferenciaProducaoBean.class);
		List<Producaoordemmaterial> listaMaterial = bean.getListaProducaoordemmaterial();
		List<Producaoordemmaterial> listaMaterialWithQtde = new ArrayList<Producaoordemmaterial>();
		
		StringBuilder whereInProducaoagenda = new StringBuilder();
		StringBuilder whereInProducaoagendamaterial = new StringBuilder();
		
		StringBuilder whereInProducaoordem = new StringBuilder();
		StringBuilder whereInProducaoordemmaterial = new StringBuilder();
		
		for (Producaoordemmaterial producaoordemmaterial : listaMaterial) {
			if(producaoordemmaterial.getConsiderarProducaoordem() != null && producaoordemmaterial.getConsiderarProducaoordem()){
				String whereInProducaoordemIt = producaoordemmaterial.getWhereInProducaoordem();
				String whereInOrdemproducaomaterialIt = producaoordemmaterial.getWhereInProducaoordemmaterial();
				
				if(StringUtils.isNotBlank(whereInProducaoordemIt) && !whereInProducaoordem.toString().contains(whereInProducaoordemIt)){
					if(!whereInProducaoordem.toString().equals("")) whereInProducaoordem.append(",");
					whereInProducaoordem.append(whereInProducaoordemIt);
				}
				if(StringUtils.isNotBlank(whereInOrdemproducaomaterialIt) && !whereInProducaoordemmaterial.toString().contains(whereInOrdemproducaomaterialIt)){
					if(!whereInProducaoordemmaterial.toString().equals("")) whereInProducaoordemmaterial.append(",");
					whereInProducaoordemmaterial.append(whereInOrdemproducaomaterialIt);
				}
			}else {
				if(producaoordemmaterial.getListaProducaoordemmaterialorigem() != null && producaoordemmaterial.getListaProducaoordemmaterialorigem().size() > 0){
					String whereInAgendaproducaoIt = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
					String whereInAgendaproducaomaterialIt = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagendamaterial.cdproducaoagendamaterial", ",");
					
					if(StringUtils.isNotBlank(whereInAgendaproducaoIt) && !whereInProducaoagenda.toString().contains(whereInAgendaproducaoIt)){
						if(!whereInProducaoagenda.toString().equals("")) whereInProducaoagenda.append(",");
						whereInProducaoagenda.append(whereInAgendaproducaoIt);
					}
					if(StringUtils.isNotBlank(whereInAgendaproducaomaterialIt) && !whereInProducaoagendamaterial.toString().contains(whereInAgendaproducaomaterialIt)){
						if(!whereInProducaoagendamaterial.toString().equals("")) whereInProducaoagendamaterial.append(",");
						whereInProducaoagendamaterial.append(whereInAgendaproducaomaterialIt);
					}
				}
			}
			if(producaoordemmaterial.getQtde() != null && producaoordemmaterial.getQtde() > 0 || (producaoordemmaterial.getQtdeBaixaEstoque() != null && producaoordemmaterial.getQtdeBaixaEstoque() > 0)){
				listaMaterialWithQtde.add(producaoordemmaterial);
			}
		}
		
		if(StringUtils.isNotBlank(whereInProducaoagenda.toString())){
			List<Producaoagendamaterial> listaProducaoagendamaterial = producaoagendamaterialService.findByProducaoagenda(whereInProducaoagenda.toString(), whereInProducaoagendamaterial.toString());
			for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
				Material material = producaoagendamaterial.getMaterial();
				Producaoagenda producaoagenda = producaoagendamaterial.getProducaoagenda();
				Empresa empresa = producaoagenda != null ? producaoagenda.getEmpresa() : null;
				
				Double qtdeprevista = producaoagendamaterial.getQtde();
				Double qtdeproduzir = producaoagendamaterial.getQtde();
				Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
				
				boolean achouOrdem = false;
				for (Producaoordemmaterial producaoordemmaterial : listaMaterialWithQtde) {
					Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = producaoordemmaterial.getListaProducaoordemmaterialorigem();
					for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
						if(producaoordemmaterialorigem.getProducaoagendamaterial() == null || producaoordemmaterialorigem.getProducaoagendamaterial().equals(producaoagendamaterial)){
							achouOrdem = true;
							if(producaoordemmaterial.getQtdeBaixaEstoque() != null){
								qtdeproduzir = producaoordemmaterial.getQtdeBaixaEstoque();
							}else {
								qtdeproduzir = producaoordemmaterial.getQtde();
							}
							break;
						}
					}
					if(achouOrdem) break;
				}
				if(!achouOrdem) continue;
				
				Boolean baixarmateriaprimacascata = producaoagendamaterial.getProducaoetapa() != null && producaoagendamaterial.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
													producaoagendamaterial.getProducaoetapa().getBaixarmateriaprimacascata();
				
				if(SinedUtil.isListNotEmpty(producaoagendamaterial.getListaProducaoagendamaterialmateriaprima())){
					for (Producaoagendamaterialmateriaprima mp : producaoagendamaterial.getListaProducaoagendamaterialmateriaprima()) {
						Material m = mp.getMaterial();
//						if(isNotServicoEpiPatrimonio(m)){
							Double consumo = ((mp.getQtdeprevista() != null ? mp.getQtdeprevista() : 1d) / qtdeprevista / qtdereferencia);
							if(qtdeproduzir != null){
								consumo = consumo * qtdeproduzir; 
							}
							
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(m)){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(m));
							}
//						}
					}
				}else {
					boolean dimensoesProducao = producaoagendamaterial.getAltura() != null || 
												producaoagendamaterial.getLargura() != null ||
												producaoagendamaterial.getComprimento() != null;
					boolean corteProducao = producaoagendamaterial.getProducaoetapa() != null &&
											producaoagendamaterial.getProducaoetapa().getCorteproducao() != null ?
											producaoagendamaterial.getProducaoetapa().getCorteproducao() : false;
					Double larguraProducao = producaoagendamaterial.getLargura() != null ? producaoagendamaterial.getLargura() : 1d;
					Double alturaProducao = producaoagendamaterial.getAltura() != null ? producaoagendamaterial.getAltura() : 1d;
					Double comprimentoProducao = producaoagendamaterial.getComprimento() != null ? producaoagendamaterial.getComprimento() : 1d;
					Double qtdeProduzir = producaoagendamaterial.getQtde() != null ? producaoagendamaterial.getQtde() : 1d;
					
					if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
						if(larguraProducao != null && producaoagendamaterial.getLargura() != null) larguraProducao = larguraProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(alturaProducao != null && producaoagendamaterial.getAltura() != null) alturaProducao = alturaProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(comprimentoProducao != null && producaoagendamaterial.getComprimento() != null) comprimentoProducao = comprimentoProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
					}
					Double b = (larguraProducao * alturaProducao * comprimentoProducao);
					
					material = materialService.loadMaterialComMaterialproducao(material);
					material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
					
					try{
						material.setProduto_altura(producaoagendamaterial.getAltura());
						material.setProduto_largura(producaoagendamaterial.getLargura());
						material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
						materialproducaoService.getValorvendaproducao(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					for (Materialproducao materialproducao : material.getListaProducao()) {
						Material m = materialproducao.getMaterial();
//						if(isNotServicoEpiPatrimonio(m)){
							Double consumo = qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia);
							
							if(dimensoesProducao && corteProducao){
								Produto produto = producaoagendamaterial.getMaterial().getMaterialproduto();
								if(produto != null){
									Double largura = produto.getLargura() != null && produto.getLargura() > 0 ? produto.getLargura() : 1d;
									Double altura = produto.getAltura() != null && produto.getAltura() > 0  ? produto.getAltura() : 1d;
									Double comprimento = produto.getComprimento() != null && produto.getComprimento() > 0  ? produto.getComprimento() : 1d;
									
									Double fatorconversaoproducao = 1000d;
									if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
											producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
											producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
										fatorconversaoproducao = producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
									}
									if(fatorconversaoproducao != null && fatorconversaoproducao > 0){
										if(largura != null && producaoagendamaterial.getLargura() != null) largura = largura / fatorconversaoproducao;
										if(altura != null && producaoagendamaterial.getAltura() != null) altura = altura / fatorconversaoproducao;
										if(comprimento != null && producaoagendamaterial.getComprimento() != null) comprimento = comprimento / fatorconversaoproducao;
									}
									
									Double a = (largura * altura * comprimento);
									BigDecimal c = new BigDecimal(b/a);
									
									consumo = SinedUtil.roundUp(new BigDecimal(c.doubleValue() * qtdeProduzir), 0).doubleValue() * materialproducao.getConsumo();
								}
							}
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(m)){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(m));
							}
//						}
					}
				}
			}
		}else if(StringUtils.isNotBlank(whereInProducaoordem.toString())){
			List<Producaoordemmaterial> listaProducaoordemmaterial = producaoordemmaterialService.findByProducaoordem(whereInProducaoordem.toString(), whereInProducaoordemmaterial.toString());
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				Material material = producaoordemmaterial.getMaterial();
				Producaoordem producaoordem = producaoordemmaterial.getProducaoordem();
				Empresa empresa = producaoordem != null ? producaoordem.getEmpresa() : null;
				
//				Double qtdeprevista = producaoordemmaterial.getQtde();
				Double qtdeproduzir = producaoordemmaterial.getQtde();
				Double qtdereferencia = material.getQtdereferencia() != null ? material.getQtdereferencia() : 1d;
				
				Producaoetapa producaoetapa_material = producaoordemmaterial.getProducaoetapa();
				if(producaoetapa_material == null && material.getProducaoetapa() != null){
					producaoordemmaterial.setBaixarmateriaprimacascata(material.getProducaoetapa().getBaixarmateriaprimacascata());
				}

				Producaoetapanome producaoetapanome = producaoordemmaterial.getProducaoetapaitem() != null ? producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome() : null;
				
				BaixarEstoqueMateriasPrimasEnum baixarEstoque = null;
				if(producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima() != null){
					baixarEstoque = producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima();
				}
				
				boolean achouOrdem = false;
				for (Producaoordemmaterial pom : listaMaterialWithQtde) {
					if(SinedUtil.isListNotEmpty(pom.getListaProducaoordemmaterialorigem())){
						for (Producaoordemmaterialorigem producaoordemmaterialorigem : pom.getListaProducaoordemmaterialorigem()) {
							if(producaoordemmaterialorigem.getProducaoordemmaterial() == null || producaoordemmaterialorigem.getProducaoordemmaterial().equals(producaoordemmaterial)){
								achouOrdem = true;
								if(Boolean.TRUE.equals(bean.getProducaoEmLote()) && producaoordemmaterial.getQtdeBaixaEstoque() != null){
									qtdeproduzir = producaoordemmaterial.getQtdeBaixaEstoque();
								}else {
									qtdeproduzir = producaoordemmaterial.getQtde();
								}
								break;
							}
						}
					}else if(StringUtils.isNotBlank(pom.getWhereInProducaoordem()) && StringUtils.isNotBlank(pom.getWhereInProducaoordemmaterial())){
						boolean producaoordemMatch = false;
						boolean producaoordemmaterialMatch = false;
						
						for(String idProducaoordem : pom.getWhereInProducaoordem().split(",")){
							if(producaoordemmaterial.getProducaoordem() != null && producaoordemmaterial.getProducaoordem().getCdproducaoordem() != null && 
									idProducaoordem.equals(producaoordemmaterial.getProducaoordem().getCdproducaoordem().toString())){
								producaoordemMatch = true;
								break;
							}
						}
						for(String idProducaoordemmaterial : pom.getWhereInProducaoordemmaterial().split(",")){
							if(producaoordemmaterial.getCdproducaoordemmaterial() != null && idProducaoordemmaterial.equals(producaoordemmaterial.getCdproducaoordemmaterial().toString())){
								producaoordemmaterialMatch = true;
								break;
							}
						}
						
						if(producaoordemMatch && producaoordemmaterialMatch){
							achouOrdem = true;
							if(Boolean.TRUE.equals(bean.getProducaoEmLote()) && pom.getQtdeBaixaEstoque() != null){
								qtdeproduzir = pom.getQtdeBaixaEstoque();
							}else {
								qtdeproduzir = pom.getQtde();
							}
						}
					}
					if(achouOrdem) break;
				}
				if(!achouOrdem) continue;
				
				Boolean baixarmateriaprimacascata = (producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
													producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata()) ||
													(producaoordemmaterial.getProducaoetapa() == null && producaoordemmaterial.getBaixarmateriaprimacascata() != null && 
															producaoordemmaterial.getBaixarmateriaprimacascata());
				
				if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima())){
					for (Producaoordemmaterialmateriaprima mp : producaoordemmaterial.getListaProducaoordemmaterialmateriaprima()) {
						Material m = mp.getMaterial();
						if(isNotServicoEpiPatrimonio(material) && (
								(!isAgendaproducao && ((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(baixarEstoque) &&
										producaoetapanome != null && producaoetapanome.equals(mp.getProducaoetapanome())) ||
										(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(baixarEstoque) && producaoordemmaterial.getUltimaetapa() != null &&
										producaoordemmaterial.getUltimaetapa()) ||
										producaoordemmaterial.getProducaoetapa() == null)) || 
								(isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA))))){
//							Double consumo = ((mp.getQtdeprevista() != null ? mp.getQtdeprevista() : 1d) / qtdeprevista / qtdereferencia);
							Double consumo = ((mp.getQtdeprevista() != null ? mp.getQtdeprevista() : 1d) / qtdereferencia);
							
							if(qtdeproduzir != null && producaoordemmaterial.getQtde() != null && producaoordemmaterial.getQtde() > 0 && !qtdeproduzir.equals(producaoordemmaterial.getQtde()) ){
								consumo = consumo / producaoordemmaterial.getQtde() * qtdeproduzir;
							}
							
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(m)){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(m));
							}
						}
					}
				}else {
					Producaoagendamaterial pam = producaoagendamaterialService.getProducaoagendamaterial(producaoordemmaterial);
					boolean dimensoesProducao = pam != null && (pam.getAltura() != null || 
																pam.getLargura() != null ||
																pam.getComprimento() != null);
					boolean corteProducao = producaoordemmaterial.getProducaoetapa() != null &&
											producaoordemmaterial.getProducaoetapa().getCorteproducao() != null ?
											producaoordemmaterial.getProducaoetapa().getCorteproducao() : false;
					Double larguraProducao = pam != null && pam.getLargura() != null ? pam.getLargura() : 1d;
					Double alturaProducao = pam != null && pam.getAltura() != null ? pam.getAltura() : 1d;
					Double comprimentoProducao = pam != null && pam.getComprimento() != null ? pam.getComprimento() : 1d;
					Double qtdeProduzir = producaoordemmaterial.getQtde() != null ? producaoordemmaterial.getQtde() : 1d;
					Double b = (larguraProducao * alturaProducao * comprimentoProducao);
					
					material = materialService.loadMaterialComMaterialproducao(material);
					material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
					
					try{
						material.setProduto_altura(pam != null ? pam.getAltura() : null);
						material.setProduto_largura(pam != null ? pam.getLargura() : null);
						material.setQuantidade(qtdeproduzir != null ? qtdeproduzir.doubleValue() : 1.0);
						materialproducaoService.getValorvendaproducao(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					for (Materialproducao materialproducao : material.getListaProducao()) {
						Material m = materialproducao.getMaterial();
						if((!isAgendaproducao && ((BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_CADA_ETAPA.equals(baixarEstoque) &&
										producaoetapanome != null && producaoetapanome.equals(materialproducao.getProducaoetapanome())) ||
										(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(baixarEstoque) && producaoordemmaterial.getUltimaetapa() != null &&
										producaoordemmaterial.getUltimaetapa()) ||
										producaoordemmaterial.getProducaoetapa() == null)) || 
								(isAgendaproducao && (baixarEstoque == null || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA)))){
							Double consumo = qtdeproduzir * (materialproducao.getConsumo() / qtdereferencia);
							
							if(dimensoesProducao && corteProducao){
								Produto produto = producaoordemmaterial.getMaterial().getMaterialproduto();
								if(produto != null){
									Double largura = produto.getLargura() != null ? produto.getLargura()/1000d : 1d;
									Double altura = produto.getAltura() != null ? produto.getAltura()/1000d : 1d;
									Double comprimento = produto.getComprimento() != null ? produto.getComprimento()/1000d : 1d;
									
									Double a = (largura * altura * comprimento);
									BigDecimal c = new BigDecimal(b/a);
									
									consumo = SinedUtil.roundUp(new BigDecimal(c.doubleValue() * qtdeProduzir), 0).doubleValue() * materialproducao.getConsumo();
								}
							}
							boolean achou = false;
							if(isNotServicoEpiPatrimonio(m)){
								for (MateriaprimaConferenciaProducaoBean materiaprima : listaMateriaprima) {
									if(materiaprima.getMaterial().equals(m)){
										materiaprima.setQtde(materiaprima.getQtde() + consumo);
										achou = true;
									}
								}
							}
							
							if(!achou){
								MateriaprimaConferenciaProducaoBean materiaprima = new MateriaprimaConferenciaProducaoBean();
								materiaprima.setEmpresa(empresa);
								materiaprima.setMaterial(m);
								materiaprima.setQtde(consumo);
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								
								addMateriaprima(materiaprima, listaMateriaprima, true, isNotServicoEpiPatrimonio(m));
							}
						}
					}
				}
			}
		}
		
		return listaMateriaprima;
	}
	
	/**
	* M�todo que verifica se a ordem de produ��o foi totalmente devolvida para cancelar as ordens de produ��o e a agenda de produ��o
	*
	* @param request
	* @param whereInProducaoordemmaterial
	* @since 08/07/2015
	* @author Luiz Fernando
	*/
	public void verificaCancelamentoProducaoordemQtdeDevolvida(WebRequestContext request, String whereInProducaoordemmaterial, boolean interromperProducao) {
		if(StringUtils.isNotEmpty(whereInProducaoordemmaterial)){
			List<Producaoordem> listaProducaoordem = findForCancelamentoQtdeDevolvida(whereInProducaoordemmaterial);
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			if(SinedUtil.isListNotEmpty(listaProducaoordem)){
				StringBuilder whereInPO = new StringBuilder();
				List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
				for(Producaoordem po : listaProducaoordem){
					if(po.getProducaoordemsituacao() != null && 
							!po.getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA) &&
							!po.getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA) &&
							SinedUtil.isListNotEmpty(po.getListaProducaoordemmaterial())){
						int qtdeItensDevolvidos = 0;
						for(Producaoordemmaterial pom : po.getListaProducaoordemmaterial()){
							if(pom.getQtde() != null && pom.getQtdeperdadescarte() != null && pom.getQtde().compareTo(pom.getQtdeperdadescarte()) == 0){
								qtdeItensDevolvidos++;
							}
							if(SinedUtil.isListNotEmpty(pom.getListaProducaoordemmaterialorigem())){
								for(Producaoordemmaterialorigem pomo : pom.getListaProducaoordemmaterialorigem()){
									if(pomo.getProducaoagenda() != null && pomo.getProducaoagenda().getProducaoagendasituacao() != null &&
											!Producaoagendasituacao.CANCELADA.equals(pomo.getProducaoagenda().getProducaoagendasituacao()) &&
											!Producaoagendasituacao.CONCLUIDA.equals(pomo.getProducaoagenda().getProducaoagendasituacao()) &&
											!listaProducaoagenda.contains(listaProducaoagenda)){
										listaProducaoagenda.add(new Producaoagenda(pomo.getProducaoagenda().getCdproducaoagenda()));
									}
								}
							}
						}
						if(qtdeItensDevolvidos == po.getListaProducaoordemmaterial().size()){
							updateSituacao(po, Producaoordemsituacao.CANCELADA);
							whereInPO.append(po.getCdproducaoordem()).append(",");
							Producaoordemhistorico poh = new Producaoordemhistorico();
							poh.setProducaoordem(po);
							poh.setObservacao("Cancelamento realizado ap�s a devolu��o de todos os itens.");
							poh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
							poh.setDtaltera(dtaltera);
							producaoordemhistoricoService.saveOrUpdate(poh);
						}
					}
				}
				
				if(StringUtils.isNotBlank(whereInPO.toString())){
					registrarHistoricoOrdemCanceladaPedidovenda(whereInPO.substring(0, whereInPO.length()-1));
				}
				
				if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
					List<Producaoordem> listaPO = findByProducaoagenda(CollectionsUtil.listAndConcatenate(listaProducaoagenda, "cdproducaoagenda", ","));
					if(SinedUtil.isListNotEmpty(listaPO)){
						StringBuilder whereInPACancelamento = new StringBuilder();
						for(Producaoagenda producaoagenda : listaProducaoagenda){
							boolean podeCancelar = true;
							boolean achouProducao = false;
							for(Producaoordem producaoordem : listaPO){
								if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
									for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
										if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialorigem())){
											for(Producaoordemmaterialorigem producaoordemmaterialorigem : producaoordemmaterial.getListaProducaoordemmaterialorigem()){
												if(producaoordemmaterialorigem.getProducaoagenda() != null && 
														producaoordemmaterialorigem.getProducaoagenda().equals(producaoagenda)){
													achouProducao = true;
													break;
												}
											}
											if(achouProducao){
												break;
											}
										}
									}
								}
								if(achouProducao && (producaoordem.getProducaoordemsituacao() == null || 
										!producaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA))){
									podeCancelar = false;
									break;
								}
							}
							if(podeCancelar && !whereInPACancelamento.toString().contains(producaoagenda.getCdproducaoagenda()+",")){
								whereInPACancelamento.append(producaoagenda.getCdproducaoagenda()).append(",");
							}
						}
						if(!whereInPACancelamento.toString().equals("")){
							producaoagendaService.cancelar(request, whereInPACancelamento.substring(0, whereInPACancelamento.length()-1), "Cancelamento realizado ap�s a devolu��o de todos os itens da ordem de produ��o.", false);
						}
					}
					
					producaoagendaService.verificaConclusaoProducaoagenda(CollectionsUtil.listAndConcatenate(listaProducaoagenda, "cdproducaoagenda", ","));
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForCancelamentoQtdeDevolvida(String whereInProducaoordemmaterial)
	*
	* @param whereInProducaoordemmaterial
	* @return
	* @since 08/07/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForCancelamentoQtdeDevolvida(String whereInProducaoordemmaterial) {
		return producaoordemDAO.findForCancelamentoQtdeDevolvida(whereInProducaoordemmaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pneu
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2015
	 */
	public List<Producaoordem> findByPneu(Pneu pneu) {
		return producaoordemDAO.findByPneu(pneu);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/08/2015
	 */
	public Date getMaxDateProducaoByProducaoagenda(Producaoagenda producaoagenda) {
		if(producaoagenda == null) 
			return null;
		
		return producaoordemDAO.getMaxDateProducaoByProducaoagenda(producaoagenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForDevolverColetaByColeta(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 15/09/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForDevolverColetaByColeta(String whereIn) {
		return producaoordemDAO.findForDevolverColetaByColeta(whereIn);
	}
	
	/**
	 * M�todo que retorna os itens adcionais a partir de um pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public List<Producaoordemitemadicional> getItensAdicionaisByPedidovenda(Pedidovenda pedidovenda) {
		List<Producaoordemitemadicional> listaProducaoordemitemadicional = new ArrayList<Producaoordemitemadicional>();
		
		List<Producaoordem> listaProducaoordem  = this.findByPedidovenda(pedidovenda);
		
		for (Producaoordem producaoordem : listaProducaoordem) {
			Pneu pneu = getPneuProducaoordem(producaoordem);
			if(producaoordem.getListaProducaoordemitemadicional() != null){
				for(Producaoordemitemadicional itemAdicional : producaoordem.getListaProducaoordemitemadicional()){
					if(itemAdicional.getPneu() == null){
						itemAdicional.setPneu(pneu);
					}
					listaProducaoordemitemadicional.add(itemAdicional);
				}
			}
		}
		
		

		if(SinedUtil.isListNotEmpty(listaProducaoordemitemadicional)){
			for(Producaoordemitemadicional itemAdicional : listaProducaoordemitemadicional){
				vendaService.preencheValorVendaComTabelaPreco(
						itemAdicional.getMaterial(), 
						null, 
						pedidovenda.getPedidovendatipo(), 
						pedidovenda.getCliente(), 
						pedidovenda.getEmpresa(), 
						itemAdicional.getMaterial().getUnidademedida(), 
						pedidovenda.getPrazopagamento(), 
						null);
				
				if(itemAdicional.getValorvenda() == null) itemAdicional.setValorvenda(0d);
			}
			
			List<Producaoordemitemadicional> listaAjustada = new ArrayList<Producaoordemitemadicional>();
			List<Vendamaterial> listaVendamaterial  = vendamaterialService.findByItensAdicionais(CollectionsUtil.listAndConcatenate(listaProducaoordemitemadicional, "cdproducaoordemitemadicional", ","), null);
			if(SinedUtil.isListNotEmpty(listaVendamaterial)){
				for(Producaoordemitemadicional itemAdicional : listaProducaoordemitemadicional){
					for(Vendamaterial vendamaterial : listaVendamaterial){
						if(vendamaterial.getCdproducaoordemitemadicional() != null && vendamaterial.getQuantidade() != null &&
								itemAdicional.getQuantidade() != null &&
								vendamaterial.getCdproducaoordemitemadicional().equals(itemAdicional.getCdproducaoordemitemadicional())){
							itemAdicional.setQuantidade(itemAdicional.getQuantidade()-vendamaterial.getQuantidade());
						}
					}
					if(itemAdicional.getQuantidade() != null && itemAdicional.getQuantidade() > 0){
						listaAjustada.add(itemAdicional);
					}
				}
				listaProducaoordemitemadicional = listaAjustada;
			}
			
		}
		return listaProducaoordemitemadicional;
	}
	
	/**
	* M�todo que retorna o pneu do primeiro item da ordem de produ��o
	*
	* @param producaoordem
	* @return
	* @since 04/10/2016
	* @author Luiz Fernando
	*/
	private Pneu getPneuProducaoordem(Producaoordem producaoordem) {
		Pneu pneu = null;
		if(producaoordem != null && producaoordem.getListaProducaoordemmaterial() != null && 
				producaoordem.getListaProducaoordemmaterial().size() == 1){
			pneu = producaoordem.getListaProducaoordemmaterial().iterator().next().getPneu();
		}
		return pneu;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pedidovenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public List<Producaoordem> findByPedidovenda(Pedidovenda pedidovenda) {
		return producaoordemDAO.findByPedidovenda(pedidovenda);
	}
	
	/**
	* M�todo que cria o bean dos itens de devolu��o
	*
	* @param listaProducaoordemmaterial
	* @param listaColetaMaterial
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<ProducaoordemdevolucaoitemBean> prepareForRegistrarDevolucao(List<Producaoordemmaterial> listaProducaoordemmaterial,
			List<ColetaMaterial> listaColetaMaterial) {
		List<ProducaoordemdevolucaoitemBean> listaItens = new ArrayList<ProducaoordemdevolucaoitemBean>();
		
		List<Pedidovendamaterial> listaPedidovendamaterial = new ArrayList<Pedidovendamaterial>();
		if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
			for(ColetaMaterial coletaMaterial : listaColetaMaterial){
				ProducaoordemdevolucaoitemBean item = new ProducaoordemdevolucaoitemBean();
				item.setColetaMaterial(coletaMaterial);
				item.setMaterial(coletaMaterial.getMaterial());
//				item.setPedidovendamaterial(coletaMaterial.getPedidovendamaterial());
				item.setQuantidade(coletaMaterial.getQuantidade());
				item.setQuantidadedevolvida(coletaMaterial.getQuantidadedevolvida());
				item.setObservacao(coletaMaterial.getObservacao());
				item.setLocalarmazenagem(coletaMaterial.getLocalarmazenagem());
				item.setEmpresa(coletaMaterial.getEmpresa());
//				item.setMotivodevolucao(coletaMaterial.getMotivodevolucao());
				item.setMotivodevolucao(Hibernate.isInitialized(coletaMaterial.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(coletaMaterial.getListaColetamaterialmotivodevolucao()) ? coletaMaterial.getListaColetamaterialmotivodevolucao().get(0).getMotivodevolucao() : null);
				item.setValorunitario(coletaMaterial.getValorunitario());
				item.setProducaoordemmaterial(coletaMaterial.getProducaoordemmaterial());
				item.setPneu(coletaMaterial.getPneu());
				
				if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
					for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
						if(itemServico.getPedidovendamaterial() != null 
								&& !listaPedidovendamaterial.contains(itemServico.getPedidovendamaterial())){
							if(item.getPedidovendamaterial() == null){
								item.setPedidovendamaterial(itemServico.getPedidovendamaterial());
							}
							listaPedidovendamaterial.add(itemServico.getPedidovendamaterial());
						}
					}
					item.setListaPedidovendamaterial(listaPedidovendamaterial);
				}
				
//				if(coletaMaterial.getPedidovendamaterial() != null && 
//						!listaPedidovendamaterial.contains(coletaMaterial.getPedidovendamaterial())){
//					listaPedidovendamaterial.add(coletaMaterial.getPedidovendamaterial());
//				}
				listaItens.add(item);
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
			for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
				if(producaoordemmaterial.getPedidovendamaterial() == null || 
						!listaPedidovendamaterial.contains(producaoordemmaterial.getPedidovendamaterial())){
					ProducaoordemdevolucaoitemBean item = new ProducaoordemdevolucaoitemBean();
					item.setProducaoordemmaterial(producaoordemmaterial);
					item.setMaterial(producaoordemmaterial.getMaterial());
					item.setQuantidade(producaoordemmaterial.getQtde());
					item.setQuantidadedevolvida(producaoordemmaterial.getQtdeperdadescarte());
					item.setObservacao(producaoordemmaterial.getObservacao());
					item.setEmpresa(producaoordemmaterial.getProducaoordem() != null ? producaoordemmaterial.getProducaoordem().getEmpresa() : null);
					item.setPneu(producaoordemmaterial.getPneu());
					
					listaItens.add(item);
				}
			}
		}
		
		
		return listaItens;
	}
	
	/**
	* M�todo que cria o bean de devolu��o
	*
	* @param listaProducaoordem
	* @param listaColeta
	* @param whereInProducaoordem
	* @param whereInColeta
	* @return
	* @throws Exception
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public ProducaoordemdevolucaoBean criaProducaoordemdevolucaoBean(List<Producaoordem> listaProducaoordem, List<Coleta> listaColeta, String whereInProducaoordem, String whereInColeta) throws Exception{
		ProducaoordemdevolucaoBean bean = new ProducaoordemdevolucaoBean();
		bean.setListaItens(prepareForRegistrarDevolucao(prepareForDevolverProducaoordem(listaProducaoordem), coletaService.prepareForDevolverColeta(listaColeta)));
		bean.setWhereInProducaoordem(whereInProducaoordem);
		bean.setWhereInColeta(whereInColeta);
		bean.setEmpresacoleta(coletaService.getEmpresa(listaColeta));
		bean.setClientecoleta(coletaService.getCliente(listaColeta));
		bean.setEmpresaproducaoordem(getEmpresa(listaProducaoordem));
		bean.setClienteproducaoordem(getCliente(listaProducaoordem));
		bean.setNaturezaoperacaosaidafiscal(coletaService.getNaturezaoperacaosaidafiscal(listaColeta));
		
		return bean;
	}
	
	public Cliente getCliente(List<Producaoordem> lista) {
		Cliente cliente = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Producaoordem bean : lista){
				if(SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterial())){
					for(Producaoordemmaterial producaoordemmaterial : bean.getListaProducaoordemmaterial()){
						if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoordemmaterialorigem())){
							for(Producaoordemmaterialorigem pomo : producaoordemmaterial.getListaProducaoordemmaterialorigem()){
								if(pomo.getProducaoagenda() != null && pomo.getProducaoagenda() .getCliente() != null){
									if(cliente == null){
										cliente = pomo.getProducaoagenda() .getCliente();
									}else if(!pomo.getProducaoagenda().getCliente().equals(cliente)){
										cliente = null;
										return null;
									}
								}
							}
						}
					}
				}
			}
		}
		return cliente;
	}
	
	public Empresa getEmpresa(List<Producaoordem> lista){
		Empresa empresa = null;
		if(SinedUtil.isListNotEmpty(lista)){
			for(Producaoordem bean : lista){
				if(bean.getEmpresa() != null){
					if(empresa == null){
						empresa = bean.getEmpresa();
					}else if(!bean.getEmpresa().equals(empresa)){
						empresa = null;
						break;
					}
				}
			}
		}
		return empresa;
	}
	
	/**
	* M�todo que retorna os itens da ordem de produ��o ajustados para devolu��o
	*
	* @param listaProducaoordem
	* @return
	* @throws Exception
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterial> prepareForDevolverProducaoordem(List<Producaoordem> listaProducaoordem) throws Exception {
		List<Producaoordemmaterial> listaMaterialTela = new ArrayList<Producaoordemmaterial>();
		
		if(SinedUtil.isListNotEmpty(listaProducaoordem)){
			for (Producaoordem po : listaProducaoordem) {
				if(SinedUtil.isListNotEmpty(po.getListaProducaoordemmaterial())){
					List<Producaoordemmaterial> listaColetaMaterial = new ArrayList<Producaoordemmaterial>();
					listaColetaMaterial.addAll(po.getListaProducaoordemmaterial());
					
					for (Producaoordemmaterial pom : listaColetaMaterial) {
						if(pom.getQtde() == null){
							pom.setQtde(0.);
						}
						if(pom.getQtdeperdadescarte() == null){
							pom.setQtdeperdadescarte(0.);
						}
						
						Double quantidadeColetada = pom.getQtde();
						Double quantidadeDevolvida = 0d;
						
						if(pom.getQtdeperdadescarte() != null){
							quantidadeDevolvida = pom.getQtdeperdadescarte();
						}
						
						Double qtdeDisponivel = quantidadeColetada - quantidadeDevolvida;
						
						pom.setQtde(qtdeDisponivel > 0. ? qtdeDisponivel : 0.);
						pom.setQtdeperdadescarte(0.);
						pom.setProducaoordem(po);
						
						listaMaterialTela.add(pom);
					}
				}
			}
			
			// ORDENA PELO NOME DO MATERIAL
			Collections.sort(listaMaterialTela, new Comparator<Producaoordemmaterial>(){
				public int compare(Producaoordemmaterial o1, Producaoordemmaterial o2) {
					if(o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
						return o1.getProducaoordem().getCdproducaoordem().compareTo(o2.getProducaoordem().getCdproducaoordem());
					} else return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				}
			});
		}
		
		return listaMaterialTela;
	}
	
	/**
	* M�todo que salva a devolu��o das ordens de produ��o
	*
	* @param request
	* @param whereInProducaoordem
	* @param listaProducaoordemdevolucaoitemBean
	* @param motivodevolucao
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public void salvarRegistrarDevolucao(WebRequestContext request, String whereInProducaoordem, List<ProducaoordemdevolucaoitemBean> listaProducaoordemdevolucaoitemBean, String motivodevolucao, boolean interromperProducao) {
		if(listaProducaoordemdevolucaoitemBean != null && !listaProducaoordemdevolucaoitemBean.isEmpty()){
			HashMap<Producaoordem, Producaoordemhistorico> mapProducaoordemHistorico = new HashMap<Producaoordem, Producaoordemhistorico>();
			List<Producaoordemmaterial> listaProducaoordemmaterial = new ArrayList<Producaoordemmaterial>();
			List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
			for(ProducaoordemdevolucaoitemBean item : listaProducaoordemdevolucaoitemBean){
				if(item.getColetaMaterial() == null && item.getProducaoordemmaterial() != null &&
						((item.getQuantidadedevolvida() != null && 
						item.getQuantidadedevolvida() > 0) || interromperProducao)){
					
					salvarRegistrarDevolucaoItem(item.getProducaoordemmaterial(), item.getMaterial(), 
							item.getLocalarmazenagem(), item.getEmpresa(), item.getQuantidadedevolvida(), item.getMotivodevolucao(), 
							listaProducaoordemmaterial, mapProducaoordemHistorico, listaMovimentacaoestoque, interromperProducao);
				}
			}
			
			salvarHistoricoDevolucao(mapProducaoordemHistorico, motivodevolucao);
			if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
				verificaCancelamentoProducaoordemQtdeDevolvida(request, SinedUtil.listAndConcatenate(listaProducaoordemmaterial, "cdproducaoordemmaterial", ","), interromperProducao);
			}
		}
	}
	
	public void salvarHistoricoDevolucao(HashMap<Producaoordem, Producaoordemhistorico> mapProducaoordemHistorico, String motivodevolucao) {
		if(mapProducaoordemHistorico.size() > 0){
			for(Producaoordem producaoordem : mapProducaoordemHistorico.keySet()){
				Producaoordemhistorico producaoordemhistorico = mapProducaoordemHistorico.get(producaoordem);
				if(StringUtils.isNotBlank(motivodevolucao)){
					producaoordemhistorico.setObservacao(producaoordemhistorico.getObservacao() + ". " + motivodevolucao);
				}
				producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
			}
		}
	}
	
	public void salvarRegistrarDevolucaoItem(Producaoordemmaterial producaoordemmaterial, Material material, Localarmazenagem localarmazenagem, 
			Empresa empresa, Double quantidadedevolvida, Motivodevolucao motivodevolucao, 
			List<Producaoordemmaterial> listaProducaoordemmaterial, HashMap<Producaoordem, Producaoordemhistorico> mapProducaoordemHistorico,
			List<Movimentacaoestoque> listaMovimentacaoestoque,
			boolean interromperProducao){
		if(material != null && producaoordemmaterial != null && ((quantidadedevolvida != null && quantidadedevolvida > 0) || interromperProducao)){
//			N�o registrar sa�da deestoque do produto de produ��o ao interromper produ��o
//			Movimentacaoestoque movimentacaoestoque = null;
//			if(material.getServico() == null || !material.getServico()){
//				Materialclasse materialclasse = Materialclasse.PRODUTO;
//				if(material.getProduto() == null || !material.getProduto()){
//					if(material.getEpi() != null && material.getEpi()){
//						materialclasse = Materialclasse.EPI;
//					} else materialclasse = null;
//				}
//				
//				if(materialclasse != null){
//					movimentacaoestoque = new Movimentacaoestoque();
//					movimentacaoestoque.setMaterial(material);
//					movimentacaoestoque.setMaterialclasse(materialclasse);
//					movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
//					movimentacaoestoque.setQtde(quantidadedevolvida);
//					movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
//					movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
//					movimentacaoestoque.setEmpresa(empresa);
//					
//					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
//					movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
//					movimentacaoestoqueorigem.setProducaoordem(producaoordemmaterial.getProducaoordem());
//					movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
//					
//					movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
//					
//					if(listaMovimentacaoestoque == null) listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
//					listaMovimentacaoestoque.add(movimentacaoestoque);
//				}
//			}
//			
//			Producaoordemhistorico producaoordemhistorico = mapProducaoordemHistorico.get(producaoordemmaterial.getProducaoordem()); 
//			if(producaoordemhistorico == null){
//				producaoordemhistorico = new Producaoordemhistorico();
//				producaoordemhistorico.setProducaoordem(producaoordemmaterial.getProducaoordem());
//				producaoordemhistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
//				producaoordemhistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
//				producaoordemhistorico.setObservacao("Devolu��o do material: ");
//			}
//			
//			producaoordemhistorico.setObservacao(producaoordemhistorico.getObservacao() + "\n" + material.getNome() + 
//					(motivodevolucao != null ? " Motivo da devolu��o: " + motivodevolucao.getDescricao() : "" ) + 
//					(movimentacaoestoque != null ? " Sa�da no estoque: <a href=\"javascript:visualizarMovimentacaoestoque("+movimentacaoestoque.getCdmovimentacaoestoque()+");\">"+movimentacaoestoque.getCdmovimentacaoestoque()+"</a>" : ""));
//			
//			mapProducaoordemHistorico.put(producaoordemmaterial.getProducaoordem(), producaoordemhistorico);
			
			if(quantidadedevolvida != null && quantidadedevolvida > 0){
				producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, quantidadedevolvida);
			}
			if(interromperProducao){
				Producaoordemmaterial pom = producaoordemmaterialService.load(producaoordemmaterial, "producaoordemmaterial.cdproducaoordemmaterial, producaoordemmaterial.qtde, producaoordemmaterial.qtdeperdadescarte, producaoordemmaterial.qtdeproduzido");
				if(pom != null && pom.getQtde() != null){
					Double qtdePerdaDescarteRestante = pom.getQtde();
					if(pom.getQtdeperdadescarte() != null){
						qtdePerdaDescarteRestante -= pom.getQtdeperdadescarte();
					}
					if(pom.getQtdeproduzido() != null){
						qtdePerdaDescarteRestante -= pom.getQtdeproduzido();
					}
					if(qtdePerdaDescarteRestante > 0){
						producaoordemmaterialService.adicionaQtdeperdadescarte(producaoordemmaterial, qtdePerdaDescarteRestante);
					}
				}
			}
			if(listaProducaoordemmaterial == null) listaProducaoordemmaterial = new ArrayList<Producaoordemmaterial>();
			if(!listaProducaoordemmaterial.contains(producaoordemmaterial)){
				listaProducaoordemmaterial.add(producaoordemmaterial);
			}
		}
	}
	
	/**
	* M�todo que ajusta o bean antes de salvar a devolu��o
	*
	* @param bean
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public void ajustaProducaoordemdevolucaoBean(ProducaoordemdevolucaoBean bean) {
		if(SinedUtil.isListNotEmpty(bean.getListaItens())){
			StringBuilder whereInMotivodevolucao = new StringBuilder();
			for(ProducaoordemdevolucaoitemBean item : bean.getListaItens()){
				if(item.getMotivodevolucao() != null && item.getMotivodevolucao().getCdmotivodevolucao() != null){
					whereInMotivodevolucao.append(item.getMotivodevolucao().getCdmotivodevolucao()).append(",");
				}
				if(item.getColetaMaterial() != null && item.getColetaMaterial().getCdcoletamaterial() == null){
					item.setColetaMaterial(null);
				}
			}
			if(StringUtils.isNotBlank(whereInMotivodevolucao.toString())){
				List<Motivodevolucao> listaMotivodevolucao = motivodevolucaoService.findByMotivodevolucao(whereInMotivodevolucao.substring(0, whereInMotivodevolucao.length()-1));
				if(SinedUtil.isListNotEmpty(listaMotivodevolucao)){
					for(Motivodevolucao motivodevolucao : listaMotivodevolucao){
						if(motivodevolucao.getCdmotivodevolucao() != null){
							for(ProducaoordemdevolucaoitemBean item : bean.getListaItens()){
								if(item.getMotivodevolucao() != null && motivodevolucao.getCdmotivodevolucao().equals(item.getMotivodevolucao().getCdmotivodevolucao())){
									item.getMotivodevolucao().setDescricao(motivodevolucao.getDescricao());
								}
							}
						}
					}
				}
				
			}
		}
		
	}
	
	/**
	* M�todo que salva as devolu��es da coleta/ordem de produ��o
	*
	* @param request
	* @param bean
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	 * @param isNaoRegistrarMovimentacao 
	*/
	public ModelAndView salvarRegistrarDevolucao(WebRequestContext request,	ProducaoordemdevolucaoBean bean, Boolean isNaoRegistrarMovimentacao) {
		Boolean interromperProducao = Boolean.valueOf(request.getParameter("interromperProducao") != null ? request.getParameter("interromperProducao") : "false");
		ajustaProducaoordemdevolucaoBean(bean);
		if(bean.getGerarNota() != null && bean.getGerarNota()){
			request.getSession().setAttribute("ProducaoordemdevolucaoBean", bean);
			return new ModelAndView("redirect:/faturamento/crud/Notafiscalproduto?ACAO=criar&devolucao=true&registrarDevolucaoColeta=true" + 
					(StringUtils.isNotBlank(bean.getWhereInProducaoordem()) ? "&whereInProducaoordem="+bean.getWhereInProducaoordem() : "") +
					(StringUtils.isNotBlank(bean.getWhereInColeta()) ? "&whereInColeta="+bean.getWhereInColeta() : "") +
					"&interromperProducao=" + interromperProducao);
		} else {
			coletaService.salvarRegistrarDevolucao(request, bean.getWhereInColeta(), bean.getListaItens(), bean.getMotivodevolucao(), interromperProducao,isNaoRegistrarMovimentacao);
			salvarRegistrarDevolucao(request, bean.getWhereInProducaoordem(), bean.getListaItens(), bean.getMotivodevolucao(), interromperProducao);
			
			if (bean.getWhereInProducaoordem()!=null){
				String whereInProducaoordem = bean.getWhereInProducaoordem().replace(" ", "");
				for (String id: whereInProducaoordem.split("\\,")){
					garantiareformaService.saveProducao(new Producaoordem(Integer.valueOf(id)), false, true);
				}
			}
			
			if(bean.getExibirPopupConsumoMateriaprima() != null && bean.getExibirPopupConsumoMateriaprima()){
				return abrirPopUpConsumoMateriaprima(bean, bean.getUrlretorno());
			}
			
			request.addMessage("Devolu��o registrada com sucesso.");
			return new ModelAndView("redirect:" + bean.getUrlretorno());
		}
	}
	
	/**
	* M�todo que abre uma popup para escolher a mat�ria-prima que foi consumida ap�s salvar a nota de devolu��o
	*
	* @param request
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpConsumoMateriaprimaAposCriarNotaDevolucao(WebRequestContext request) {
		try {
			String identificadortela = request.getParameter("identificadortelanota");
			ProducaoordemdevolucaoBean bean = (ProducaoordemdevolucaoBean) request.getSession().getAttribute("ProducaoordemdevolucaoBean" + identificadortela);
			request.getSession().removeAttribute("ProducaoordemdevolucaoBean" + identificadortela);
			return abrirPopUpConsumoMateriaprima(bean, bean.getUrlretorno());
		} catch (Exception e) {
			request.addError("Erro ao registrar consumo de mat�ria-prima: " + e.getMessage());
			SinedUtil.redirecionamento(request, "/producao/crud/Producaoordem");
			return null;
		}
	}
	
	/**
	* M�todo que abre uma popup para escolher a mat�ria-prima que foi consumida
	*
	* @param producaoordemdevolucaoBean
	* @param urlretorno
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView abrirPopUpConsumoMateriaprima(ProducaoordemdevolucaoBean producaoordemdevolucaoBean, String urlretorno) {
		ConsumoMateriaprimaProducaoBean bean = new ConsumoMateriaprimaProducaoBean();
		if(SinedUtil.isListNotEmpty(producaoordemdevolucaoBean.getListaItens())){
			List<Producaoordemmaterial> listaPO = new ArrayList<Producaoordemmaterial>();
			List<Producaoordemmaterial> listaAoConcluiUltimaEtapa = new ArrayList<Producaoordemmaterial>();
			List<Producaoordemmaterial> listaAoProduzirAgenda = new ArrayList<Producaoordemmaterial>();
			for(ProducaoordemdevolucaoitemBean item : producaoordemdevolucaoBean.getListaItens()){
				if(item.getQuantidadedevolvida() != null && item.getQuantidadedevolvida() > 0 && item.getProducaoordemmaterial() != null &&
						item.getProducaoordemmaterial().getProducaoetapa() != null && 
						(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(item.getProducaoordemmaterial().getProducaoetapa().getBaixarEstoqueMateriaprima()) ||
								BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(item.getProducaoordemmaterial().getProducaoetapa().getBaixarEstoqueMateriaprima()))){
					
					Producaoordemmaterial producaoordemmaterial = item.getProducaoordemmaterial();
					producaoordemmaterial.setQuantidadedevolvida(item.getQuantidadedevolvida());
					
					if(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA.equals(item.getProducaoordemmaterial().getProducaoetapa().getBaixarEstoqueMateriaprima())){
						listaAoConcluiUltimaEtapa.add(producaoordemmaterial);
					}else if(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA.equals(item.getProducaoordemmaterial().getProducaoetapa().getBaixarEstoqueMateriaprima())){
						listaAoProduzirAgenda.add(producaoordemmaterial);
					}
				}
			}
			
			listaPO.addAll(listaAoConcluiUltimaEtapa);
			listaPO.addAll(listaAoProduzirAgenda);
			
			bean = prepareForConsumoMaterialprimaProducao(listaPO);
		}
		
		bean.setUrlretorno(urlretorno);
		return new ModelAndView("process/registrarConsumoMateriaprima", "bean", bean);
	}
	
	/**
	* M�todo que salva os consumos de mat�ria-prima
	*
	* @param request
	* @param bean
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public ModelAndView saveConsumoMateriaPrima(WebRequestContext request, ConsumoMateriaprimaProducaoBean bean){
		if(SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterial())){
			List<Producaoagenda> listaProducaoagenda = new ArrayList<Producaoagenda>();
			StringBuilder whereInMaterial = new StringBuilder();
			StringBuilder whereInProducaoagenda = new StringBuilder();
			
			for(Producaoordemmaterial producaoordemmaterial : bean.getListaProducaoordemmaterial()){
				if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaMateriaprima())){
					for(MateriaprimaConsumoProducaoBean item : producaoordemmaterial.getListaMateriaprima()){
						if(producaoordemmaterial.getProducaoagenda() != null){
							whereInProducaoagenda.append(producaoordemmaterial.getProducaoagenda().getCdproducaoagenda()+",");
						}
						if(item.getMaterial() != null){
							whereInMaterial.append(item.getMaterial().getCdmaterial()+",");
						}
					}
				}
			}
			
			if(StringUtils.isNotBlank(whereInProducaoagenda.toString()) && StringUtils.isNotBlank(whereInMaterial.toString())){
				movimentacaoestoqueService.updateDataCancelamentoMateriaprimaByProducaoagenda(whereInProducaoagenda.substring(0, whereInProducaoagenda.length()-1) , whereInMaterial.substring(0, whereInMaterial.length()-1));
			}
			
			for(Producaoordemmaterial producaoordemmaterial : bean.getListaProducaoordemmaterial()){
				if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaMateriaprima())){
					for(MateriaprimaConsumoProducaoBean item : producaoordemmaterial.getListaMateriaprima()){
						if(item.getQtde() != null && item.getQtde() > 0){
							saidaEstoqueMateriasPrimas(item, listaProducaoagenda);
						}
					}
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaProducaoagenda)){
				Producaoagendahistorico pah;
				Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
				Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
				
				for(Producaoagenda producaoagenda : listaProducaoagenda){
					if(StringUtils.isNotBlank(producaoagenda.getWhereInSaidaMateriaprima())){
						StringBuilder observacao = new StringBuilder("Sa�da de mat�ria-prima:"); 
						for(String id : producaoagenda.getWhereInSaidaMateriaprima().split(",")){
							observacao.append(" <a href=\"javascript:visualizaMovimentacaoestoque(" + id + ");\">" + id +"</a>,");
						}
						pah = new Producaoagendahistorico();
						pah.setProducaoagenda(producaoagenda);
						pah.setCdusuarioaltera(cdusuarioaltera);
						pah.setDtaltera(dtaltera);
						pah.setObservacao(observacao.substring(0, observacao.length()-1));
						producaoagendahistoricoService.saveOrUpdate(pah);
					}
				}
			}
		}
		
		request.addMessage("Registro de consumo realizado com sucesso.");
		return new ModelAndView("redirect:" + bean.getUrlretorno());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#loadWithProducaoordemanterior(Producaoordem producaoordem)
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForEstornar(String whereIn) {
		return producaoordemDAO.findForEstornar(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#loadWithProducaoordemanterior(Producaoordem producaoordem)
	*
	* @param producaoordem
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public Producaoordem loadWithProducaoordemanterior(Producaoordem producaoordem){
		return producaoordemDAO.loadWithProducaoordemanterior(producaoordem);
	}
	
	/**
	* M�todo que retorna a ordem de produ��o anterior da ordem de produ��o
	*
	* @param bean
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public Producaoordem getProducaoordemanterior(Producaoordem bean) {
		Producaoordem producaoordem = loadWithProducaoordemanterior(bean);
		return producaoordem != null ? producaoordem.getProducaoordemanterior() : null;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemDAO#findForCancelarProximasEtapas(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForCancelarProximasEtapas(String whereIn) {
		return producaoordemDAO.findForCancelarProximasEtapas(whereIn);
	}
	
	/**
	* M�todo que estorna a ordem de produ��o
	*
	* @param listaProducaoordem
	* @since 22/12/2015
	* @author Luiz Fernando
	 * @param mapOrdemColeta 
	*/
	public void estornarOrdemproducao(final List<Producaoordem> listaProducaoordem, final HashMap<Producaoordemmaterial, List<ColetaMaterial>> mapOrdemColeta) {
		List<Movimentacaoestoque> listaEntrada = movimentacaoestoqueService.findByProducaoordem(SinedUtil.listAndConcatenate(listaProducaoordem, "cdproducaoordem", ","), Movimentacaoestoquetipo.ENTRADA, Boolean.TRUE, Boolean.FALSE);
		List<Movimentacaoestoque> listaSaida = movimentacaoestoqueService.findByProducaoordem(SinedUtil.listAndConcatenate(listaProducaoordem, "cdproducaoordem", ","), Movimentacaoestoquetipo.SAIDA, Boolean.FALSE, Boolean.TRUE);
		
		if(SinedUtil.isListNotEmpty(listaEntrada) && SinedUtil.isListNotEmpty(listaSaida)){
			boolean remover = false;
			for (Iterator<Movimentacaoestoque> iterEntrada = listaEntrada.iterator(); iterEntrada.hasNext();) {
				remover = false;
				Movimentacaoestoque movEntrada = (Movimentacaoestoque) iterEntrada.next();
				if(movEntrada.getMaterial() != null && movEntrada.getQtde() != null){
					for (Iterator<Movimentacaoestoque> iterSaida = listaSaida.iterator(); iterSaida.hasNext();) {
						Movimentacaoestoque movSaida = (Movimentacaoestoque) iterSaida.next();
						if(movEntrada.getMaterial().equals(movSaida.getMaterial()) && movSaida.getQtde() != null && movEntrada.getQtde().compareTo(movSaida.getQtde()) == 0){
							iterSaida.remove();
							remover = true;
							break;
						}
					}
				}
				if(remover){
					iterEntrada.remove();
				}
			}
		}
		
		final List<Movimentacaoestoque> lista = listaEntrada;
		final List<Movimentacaoestoque> listaCancelar = movimentacaoestoqueService.findByProducaoordem(SinedUtil.listAndConcatenate(listaProducaoordem, "cdproducaoordem", ","), Movimentacaoestoquetipo.SAIDA, Boolean.TRUE, Boolean.FALSE);
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				Integer cdusuarioaltera = SinedUtil.getUsuarioLogado().getCdpessoa();
				Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
				for(Producaoordem producaoordem : listaProducaoordem){
					updateSituacao(producaoordem, Producaoordemsituacao.EM_ESPERA);
					if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
						producaoordemmaterialService.zerarQtdeProduzido(SinedUtil.listAndConcatenate(producaoordem.getListaProducaoordemmaterial(), "cdproducaoordemmaterial", ","));
						producaoordemmaterialService.zerarQtdePerdaDescarte(SinedUtil.listAndConcatenate(producaoordem.getListaProducaoordemmaterial(), "cdproducaoordemmaterial", ","));
						producaoordemmaterialperdadescarteService.delete(producaoordem);
						producaoordemmaterialoperadorService.delete(producaoordem);
						if(mapOrdemColeta != null){
							for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
								List<ColetaMaterial> listaColetaMaterial = mapOrdemColeta.get(producaoordemmaterial);
								if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
									for(ColetaMaterial coletaMaterial : listaColetaMaterial){
										if(coletaMaterial.getCdcoletamaterial() != null && coletaMaterial.getQtdeDevolvidaEstonar() != null){
											coletaMaterialService.updateQtdeDevolvidaAposEstornoDevolucao(coletaMaterial, coletaMaterial.getQtdeDevolvidaEstonar());
										}
									}
								}
							}
						}
					}
					registrarSaidaAposEstornoNoUseTransaction(lista, producaoordem);
					if(SinedUtil.isListNotEmpty(listaCancelar)){
						movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaCancelar, "cdmovimentacaoestoque", ","));
						for (Movimentacaoestoque movimentacaoestoque : listaCancelar) {
							movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CANCELAR, "CANCELADO VIA ESTORNO DE ORDEM DE PRODU��O " + SinedUtil.makeLinkHistorico(producaoordem.getCdproducaoordem().toString(), "visualizarOrdemDeProducao") , false);
						}
					}
					
					Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
					producaoordemhistorico.setProducaoordem(producaoordem);
					producaoordemhistorico.setObservacao("Estorno no registro da Produ��o");
					producaoordemhistoricoService.saveOrUpdateNoUseTransaction(producaoordemhistorico);
					
					producaoordemmaterialoperadorService.delete(producaoordem);
					
					List<Producaoordem> listaPOCancelar = findForCancelarProximasEtapas(producaoordem.getCdproducaoordem().toString());
					if(SinedUtil.isListNotEmpty(listaPOCancelar)){
						String whereInProducaoordemCancelar = SinedUtil.listAndConcatenate(listaPOCancelar, "cdproducaoordem", ",");
						updateSituacao(whereInProducaoordemCancelar, Producaoordemsituacao.CANCELADA);
						registrarHistoricoOrdemCanceladaPedidovenda(whereInProducaoordemCancelar);
						
						List<Movimentacaoestoque> listaMovEntrada = movimentacaoestoqueService.findByProducaoordem(whereInProducaoordemCancelar, Movimentacaoestoquetipo.ENTRADA, Boolean.TRUE, Boolean.FALSE);
						List<Movimentacaoestoque> listaMovSaida = movimentacaoestoqueService.findByProducaoordem(whereInProducaoordemCancelar, Movimentacaoestoquetipo.SAIDA, Boolean.FALSE, Boolean.TRUE);
						
						if(SinedUtil.isListNotEmpty(listaMovEntrada) && SinedUtil.isListNotEmpty(listaMovSaida)){
							boolean remover = false;
							for (Iterator<Movimentacaoestoque> iterEntrada = listaMovEntrada.iterator(); iterEntrada.hasNext();) {
								remover = false;
								Movimentacaoestoque movEntrada = (Movimentacaoestoque) iterEntrada.next();
								if(movEntrada.getMaterial() != null && movEntrada.getQtde() != null){
									for (Iterator<Movimentacaoestoque> iterSaida = listaMovSaida.iterator(); iterSaida.hasNext();) {
										Movimentacaoestoque movSaida = (Movimentacaoestoque) iterSaida.next();
										if(movEntrada.getMaterial().equals(movSaida.getMaterial()) && movSaida.getQtde() != null && movEntrada.getQtde().compareTo(movSaida.getQtde()) == 0){
											iterSaida.remove();
											remover = true;
											break;
										}
									}
								}
								if(remover){
									iterEntrada.remove();
								}
							}
						}
						
						List<Movimentacaoestoque> listaMovRegistrarSaida = listaMovEntrada;
						List<Movimentacaoestoque> listaMovCancelar = movimentacaoestoqueService.findByProducaoordem(whereInProducaoordemCancelar, Movimentacaoestoquetipo.SAIDA, Boolean.TRUE, Boolean.FALSE);
						
						for(Producaoordem po : listaPOCancelar){
							if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
								producaoordemmaterialService.zerarQtdeProduzido(SinedUtil.listAndConcatenate(producaoordem.getListaProducaoordemmaterial(), "cdproducaoordemmaterial", ","));
							}
							registrarSaidaAposEstornoNoUseTransaction(listaMovRegistrarSaida, po);
							if(SinedUtil.isListNotEmpty(listaMovCancelar)){
								movimentacaoestoqueService.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovCancelar, "cdmovimentacaoestoque", ","));
							}
							
							Producaoordemhistorico poh = new Producaoordemhistorico();
							poh.setProducaoordem(po);
							poh.setCdusuarioaltera(cdusuarioaltera);
							poh.setDtaltera(dtaltera);
							poh.setObservacao("Ordem de produ��o cancelada ap�s o estorno da ordem de produ��o " +
											"<a href=\"javascript:visualizarProducaoordem(" +
											producaoordem.getCdproducaoordem() +
											");\">" +
											producaoordem.getCdproducaoordem() +
											"</a>");
							
							producaoordemhistoricoService.saveOrUpdateNoUseTransaction(poh);
							producaoordemmaterialoperadorService.delete(po);
						}
					}
					
					List<Producaoagenda> listaProducaoagendaConcluida = producaoagendaService.findProducaoagendaForEstornoProducaoordem(producaoordem.getCdproducaoordem().toString());
					if(SinedUtil.isListNotEmpty(listaProducaoagendaConcluida)){
						for(Producaoagenda producaoagenda : listaProducaoagendaConcluida){
							producaoagendaService.updateSituacao(producaoagenda, Producaoagendasituacao.EM_ANDAMENTO);
							
							Producaoagendahistorico pah = new Producaoagendahistorico();
							pah.setProducaoagenda(producaoagenda);
							pah.setCdusuarioaltera(cdusuarioaltera);
							pah.setDtaltera(dtaltera);
							pah.setObservacao("Estorno no registro da Produ��o " +
												"<a href=\"javascript:visualizarProducaoordem(" +
												producaoordem.getCdproducaoordem() +
												");\">" +
												producaoordem.getCdproducaoordem() +
												"</a>");
							
							producaoagendahistoricoService.saveOrUpdateNoUseTransaction(pah);
						}
					}
				}
				
				return status;
			}
		});
		
	}
	
	/**
	* M�todo que registra o cancelamento da ordem de produ��o no pedido de venda
	*
	* @param whereInProducaoordem
	* @since 18/04/2016
	* @author Luiz Fernando
	*/
	public void registrarHistoricoOrdemCanceladaPedidovenda(String whereInProducaoordem) {
		if(StringUtils.isNotBlank(whereInProducaoordem)){
			List<Producaoordemmaterial> listaProducaoordemmaterial = producaoordemmaterialService.findOrdemCanceladaPedidovenda(whereInProducaoordem);
			if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
				HashMap<Pedidovenda, List<Producaoordem>> mapPedidovendaOrdem = new HashMap<Pedidovenda, List<Producaoordem>>();
				for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
					if(producaoordemmaterial.getPedidovendamaterial() != null && producaoordemmaterial.getPedidovendamaterial().getPedidovenda() != null &&
							producaoordemmaterial.getProducaoordem() != null && producaoordemmaterial.getProducaoordem().getCdproducaoordem() != null){
						List<Producaoordem> listaProducaoordem = mapPedidovendaOrdem.get(producaoordemmaterial.getPedidovendamaterial().getPedidovenda());
						if(listaProducaoordem == null){
							listaProducaoordem = new ArrayList<Producaoordem>();
						}
						if(!listaProducaoordem.contains(producaoordemmaterial.getProducaoordem())){
							listaProducaoordem.add(producaoordemmaterial.getProducaoordem());
							mapPedidovendaOrdem.put(producaoordemmaterial.getPedidovendamaterial().getPedidovenda(), listaProducaoordem);
						}
					}
				}
				
				if(mapPedidovendaOrdem.size() > 0){
					Integer cdusuarioaltera = SinedUtil.getCdUsuarioLogado();
					Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
					for(Pedidovenda pedidovenda : mapPedidovendaOrdem.keySet()){
						Pedidovendahistorico pvh = new Pedidovendahistorico();
						pvh.setPedidovenda(pedidovenda);
						pvh.setCdusuarioaltera(cdusuarioaltera);
						pvh.setDtaltera(dtaltera);
						pvh.setObservacao("Ordem de produ��o cancelada: " + SinedUtil.makeLinkHistorico(CollectionsUtil.listAndConcatenate(mapPedidovendaOrdem.get(pedidovenda), "cdproducaoordem", ","), "visualizarProducaoordem"));
						pedidovendahistoricoService.saveOrUpdate(pvh);
						
					}
				}
			}
		}
	}
	
	/**
	* M�todo que registra a sa�da das ordens de produ��o que geraram entrada de estoque
	*
	* @param lista
	* @param producaoordem
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	private void registrarSaidaAposEstornoNoUseTransaction(List<Movimentacaoestoque> lista,	Producaoordem producaoordem) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Movimentacaoestoque me : lista){
				if(me.getMovimentacaoestoqueorigem() != null && me.getMovimentacaoestoqueorigem().getProducaoordem() != null &&
						me.getMovimentacaoestoqueorigem().getProducaoordem().equals(producaoordem)){
					
					Movimentacaoestoqueorigem meo = new Movimentacaoestoqueorigem();
					meo.setEstornoproducao(true);
					meo.setProducaoordem(producaoordem);
					meo.setProducaoOrdemMaterial(me.getMovimentacaoestoqueorigem().getProducaoOrdemMaterial());
					
					Movimentacaoestoque saida = new Movimentacaoestoque();
					saida.setMaterial(me.getMaterial());
					saida.setMaterialclasse(me.getMaterialclasse());
					saida.setEmpresa(me.getEmpresa());
					saida.setLocalarmazenagem(me.getLocalarmazenagem());
					saida.setLoteestoque(me.getLoteestoque());
					saida.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
					saida.setProjeto(me.getProjeto());
					saida.setQtde(me.getQtde());
					saida.setValor(me.getValor());
					saida.setMovimentacaoestoqueorigem(meo);
					saida.setDtmovimentacao(new Date(System.currentTimeMillis()));
					
					movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(meo);
					movimentacaoestoqueService.saveOrUpdateNoUseTransaction(saida);
				}
			}
		}
	}
	
	
	
	/**
	* M�todo que carrega as ordens de produ��o com as materias-primas
	*
	* @param producaoordem
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public void carregarProducaoordemmaterialmateriaprima(Producaoordem producaoordem){
		if(producaoordem != null && SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
			StringBuilder whereInProducaoordemmaterial = new StringBuilder();
			for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
				if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
					if(!Hibernate.isInitialized(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima())){
						producaoordemmaterial.setListaProducaoordemmaterialmateriaprima(new ListSet<Producaoordemmaterialmateriaprima>(Producaoordemmaterialmateriaprima.class));
					}
					whereInProducaoordemmaterial.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
				}
			}
			if(StringUtils.isNotEmpty(whereInProducaoordemmaterial.toString())){
				List<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima = producaoordemmaterialmateriaprimaService.findByProducaoordemmaterial(whereInProducaoordemmaterial.substring(0, whereInProducaoordemmaterial.length()-1));
				if(SinedUtil.isListNotEmpty(listaProducaoordemmaterialmateriaprima)){
					for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
						if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
							for(Producaoordemmaterialmateriaprima item : listaProducaoordemmaterialmateriaprima){
								if(item.getProducaoordemmaterial() != null &&
										item.getProducaoordemmaterial().getCdproducaoordemmaterial() != null &&
										producaoordemmaterial.getCdproducaoordemmaterial().equals(item.getProducaoordemmaterial().getCdproducaoordemmaterial())){
									if(producaoordemmaterial.getListaProducaoordemmaterialmateriaprima() == null){
										producaoordemmaterial.setListaProducaoordemmaterialmateriaprima(new ListSet<Producaoordemmaterialmateriaprima>(Producaoordemmaterialmateriaprima.class));
									}
									producaoordemmaterial.getListaProducaoordemmaterialmateriaprima().add(item);
								}
								
								if(item.getMaterial() != null && item.getLoteestoque() != null){
									List<Lotematerial> listaLotematerial = lotematerialService.findAllByLoteestoqueMaterial(item.getLoteestoque(), item.getMaterial());
									if(listaLotematerial != null && listaLotematerial.size() > 0){
										Lotematerial lotematerial = listaLotematerial.get(0);
										item.getLoteestoque().setValidade(lotematerial.getValidade());
									}
								}
							}
						}
						
					}
				}
			}
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.service.ProducaoordemService#findForProducao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 04/04/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordem> findForProducao(String whereIn) {
		return producaoordemDAO.findForProducao(whereIn);
	}
	
	/**
	* M�todo que salva os campos adicionais dos itens da ordem de produ��o
	*
	* @param listaProducaoordemmaterial
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public void salvarCamposAdicionais(final Set<Producaoordemmaterial> listaProducaoordemmaterial) {
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
			getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
				public Object doInTransaction(TransactionStatus status) {
					for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
						if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
							if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaCampo())){
								for(Producaoordemmaterialcampo producaoordemmaterialcampo : producaoordemmaterial.getListaCampo()){
									producaoordemmaterialcampo.setProducaoordemmaterial(producaoordemmaterial);
									producaoordemmaterialcampoService.saveOrUpdateNoUseTransaction(producaoordemmaterialcampo);
								}
							}
						}
					}
					return null;
				}});
		}
	}
	
	/**
	* M�todo que seta informa��es do item da ordem no item da coleta
	*
	* @param listaProducaoordem
	* @param listaColeta
	* @since 11/03/2016
	* @author Luiz Fernando
	*/
	public void setInformacoesProducaoordemmaterial(List<Producaoordem> listaProducaoordem, List<Coleta> listaColeta) {
		if(SinedUtil.isListNotEmpty(listaProducaoordem) && SinedUtil.isListNotEmpty(listaColeta)){
			for(Producaoordem po : listaProducaoordem){
				if(SinedUtil.isListNotEmpty(po.getListaProducaoordemmaterial())){
					for(Producaoordemmaterial pom : po.getListaProducaoordemmaterial()){
						if(pom.getPedidovendamaterial() != null){
							for (Coleta coleta : listaColeta) {
								if(SinedUtil.isListNotEmpty(coleta.getListaColetaMaterial())){
									for (ColetaMaterial coletaMaterial : coleta.getListaColetaMaterial()) {
										if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
											for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
												if(itemServico.getPedidovendamaterial() != null && pom.getPedidovendamaterial().equals(itemServico.getPedidovendamaterial())){
													pom.setWhereInProducaoagendamaterial(producaoordemmaterialService.getWhereInProducaoagendamaterialOrigem(pom));
													pom.setProducaoordem(po);
													coletaMaterial.setProducaoordemmaterial(pom);
												}				
											}
										}
//										if(coletaMaterial.getPedidovendamaterial() != null && pom.getPedidovendamaterial().equals(coletaMaterial.getPedidovendamaterial())){
//											pom.setWhereInProducaoagendamaterial(producaoordemmaterialService.getWhereInProducaoagendamaterialOrigem(pom));
//											pom.setProducaoordem(po);
//											coletaMaterial.setProducaoordemmaterial(pom);
//										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	* M�todo que cria o bean de consumo de mat�ria-prima
	*
	* @param listaProducaoordemmaterial
	* @return
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public ConsumoMateriaprimaProducaoBean prepareForConsumoMaterialprimaProducao(List<Producaoordemmaterial> listaProducaoordemmaterial) {
		String whereInProducaoordem = CollectionsUtil.listAndConcatenate(listaProducaoordemmaterial, "producaoordem.cdproducaoordem", ",");
		List<Producaoordemmaterial> listaProducaoordemmaterialConferencia = new ArrayList<Producaoordemmaterial>();
		
		if(StringUtils.isNotBlank(whereInProducaoordem)){
			List<Producaoagenda> listaProducaoagenda = producaoagendaService.findForConsumoMateriaprima(whereInProducaoordem);
			
			for (Producaoagenda producaoagenda : listaProducaoagenda) {
				Set<Producaoagendamaterial> listaProducaoagendamaterial = producaoagenda.getListaProducaoagendamaterial();
				Producaoetapa producaoetapa_tipo = producaoagenda.getProducaoagendatipo() != null ? producaoagenda.getProducaoagendatipo().getProducaoetapa() : null;
				
				for (Producaoagendamaterial producaoagendamaterial : listaProducaoagendamaterial) {
					
					Material material = producaoagendamaterial.getMaterial();
					List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima = producaoagendamaterial.getListaProducaoagendamaterialmateriaprima();
					Producaoetapa producaoetapa_material = producaoagendamaterial.getProducaoetapa();
					if(producaoetapa_material == null && material.getProducaoetapa() != null){
						producaoetapa_material = material.getProducaoetapa();
					} else if(producaoetapa_material == null && producaoagenda.getProducaoagendatipo() != null){
						producaoetapa_material = producaoagenda.getProducaoagendatipo().getProducaoetapa();
					}
					
					if(material != null && (producaoetapa_material != null || producaoetapa_tipo != null) && producaoordemmaterialService.existeProducaoagendamaterial(producaoagendamaterial, listaProducaoordemmaterial)){
						Producaoordemmaterial producaoordemmaterial = new Producaoordemmaterial();
						
						Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = new ListSet<Producaoordemmaterialorigem>(Producaoordemmaterialorigem.class);
						listaProducaoordemmaterialorigem.add(new Producaoordemmaterialorigem(producaoagenda, producaoagendamaterial));
						producaoordemmaterial.setListaProducaoordemmaterialorigem(listaProducaoordemmaterialorigem);
						
						setInformacoesProducaoordemmaterial(producaoordemmaterial, listaProducaoordemmaterial, null, null);
						
						producaoordemmaterial.setEmpresa(producaoagenda.getEmpresa());
						producaoordemmaterial.setMaterial(material);
						producaoordemmaterial.setListaProducaoagendamaterialmateriaprima(listaProducaoagendamaterialmateriaprima);
						producaoordemmaterial.setProducaoetapa(producaoagendamaterial.getProducaoetapa());
						producaoordemmaterial.setProducaoagenda(producaoagenda);
						producaoordemmaterial.setQuantidadedevolvida(producaoordemmaterial.getQuantidadedevolvida());
						producaoordemmaterial.setQtdeprevista(producaoordemmaterial.getQuantidadedevolvida());
						producaoordemmaterial.setProducaoagendamaterial(producaoagendamaterial);
						
						if(producaoagenda.getPedidovenda() != null && producaoagenda.getPedidovenda().getLocalarmazenagem() != null){
							producaoordemmaterial.setLocalarmazenagem(producaoagenda.getPedidovenda().getLocalarmazenagem());
						}else if(producaoagenda.getPedidovenda() != null && producaoagenda.getPedidovenda().getLocalarmazenagem() != null){
							producaoordemmaterial.setLocalarmazenagem(producaoagenda.getVenda().getLocalarmazenagem());
						}
						if(producaoagenda.getLocalbaixamateriaprima() != null){
							producaoordemmaterial.setLocalbaixamateriaprima(producaoagenda.getLocalbaixamateriaprima());
						}
						listaProducaoordemmaterialConferencia.add(producaoordemmaterial);
					}
				}
			}
		}
		
		ConsumoMateriaprimaProducaoBean bean = new ConsumoMateriaprimaProducaoBean();
		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialConferencia) {
			Material material = producaoordemmaterial.getMaterial();
			BaixarEstoqueMateriasPrimasEnum baixarEstoque = null;
			Double qtdereferencia = producaoordemmaterial.getQtdereferencia() != null ? producaoordemmaterial.getQtdereferencia() : 1d;
			Double quantidadedevolvida = producaoordemmaterial.getQuantidadedevolvida() != null ? producaoordemmaterial.getQuantidadedevolvida() : 1d;
			producaoordemmaterial.setQtdeprevista(quantidadedevolvida);
			
			if(producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima() != null){
				baixarEstoque = producaoordemmaterial.getProducaoetapa().getBaixarEstoqueMateriaprima();
			}
			if(baixarEstoque != null && (baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_PRODUZIR_AGENDA) || baixarEstoque.equals(BaixarEstoqueMateriasPrimasEnum.AO_CONCLUIR_ULTIMA_ETAPA))){
				Boolean baixarmateriaprimacascata = producaoordemmaterial.getProducaoetapa() != null && producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata() != null && 
														producaoordemmaterial.getProducaoetapa().getBaixarmateriaprimacascata();
				if(producaoordemmaterial.getListaMateriaprima() == null) producaoordemmaterial.setListaMateriaprima(new ArrayList<MateriaprimaConsumoProducaoBean>());
				
				if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaProducaoagendamaterialmateriaprima())){
					for (Producaoagendamaterialmateriaprima mp : producaoordemmaterial.getListaProducaoagendamaterialmateriaprima()) {
						Material m = mp.getMaterial();
						
						if(isNotServicoEpiPatrimonio(m)){
							boolean achou = false;
							for (MateriaprimaConsumoProducaoBean materiaprima : producaoordemmaterial.getListaMateriaprima()) {
								if(materiaprima.getMaterial().equals(m)){
									achou = true;
								}
							}
							
							if(!achou){
								Double consumo = quantidadedevolvida * (mp.getQtdeprevista() / qtdereferencia);
								
								MateriaprimaConsumoProducaoBean materiaprima = new MateriaprimaConsumoProducaoBean();
								materiaprima.setMaterial(m);
								materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
								materiaprima.setLocalarmazenagem(producaoordemmaterial.getLocalbaixamateriaprima() != null ? producaoordemmaterial.getLocalbaixamateriaprima() : producaoordemmaterial.getLocalarmazenagem());
								materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(m, Materialclasse.PRODUTO, materiaprima.getLocalarmazenagem()));
								materiaprima.setUnidademedida(mp.getUnidademedida());
								materiaprima.setCasasdecimaisestoqueunidademedida(mp.getUnidademedida().getCasasdecimaisestoque());
								materiaprima.setLoteestoque(mp.getLoteestoque());
								materiaprima.setFracaounidademedida(mp.getFracaounidademedida());
								materiaprima.setQtdereferenciaunidademedida(mp.getQtdereferenciaunidademedida());
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								materiaprima.setQtde(consumo);
								
								String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
								materiaprima.setWhereInAgendaProducao(whereInProducaoagenda);
								
								producaoordemmaterial.getListaMateriaprima().add(materiaprima);
							}
						}
					}
				} else {
					Producaoagendamaterial producaoagendamaterial = producaoordemmaterial.getProducaoagendamaterial();
					
					boolean dimensoesProducao = producaoagendamaterial.getAltura() != null || 
												producaoagendamaterial.getLargura() != null ||
												producaoagendamaterial.getComprimento() != null;
					boolean corteProducao = producaoagendamaterial.getProducaoetapa() != null &&
											producaoagendamaterial.getProducaoetapa().getCorteproducao() != null ?
											producaoagendamaterial.getProducaoetapa().getCorteproducao() : false;
					Double larguraProducao = producaoagendamaterial.getLargura() != null ? producaoagendamaterial.getLargura() : 1d;
					Double alturaProducao = producaoagendamaterial.getAltura() != null ? producaoagendamaterial.getAltura() : 1d;
					Double comprimentoProducao = producaoagendamaterial.getComprimento() != null ? producaoagendamaterial.getComprimento() : 1d;
					Double qtdeProduzir = producaoagendamaterial.getQtde() != null ? producaoagendamaterial.getQtde() : 1d;
					
					if(producaoagendamaterial.getMaterial() != null && producaoagendamaterial.getMaterial().getMaterialproduto() != null &&
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
							producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
						if(larguraProducao != null && producaoagendamaterial.getLargura() != null) larguraProducao = larguraProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(alturaProducao != null && producaoagendamaterial.getAltura() != null) alturaProducao = alturaProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
						if(comprimentoProducao != null && producaoagendamaterial.getComprimento() != null) comprimentoProducao = comprimentoProducao / producaoagendamaterial.getMaterial().getMaterialproduto().getFatorconversaoproducao();
					}
					
					Double b = (larguraProducao * alturaProducao * comprimentoProducao);
					
					material = materialService.loadMaterialComMaterialproducao(material);
					material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
					
					try{
					material.setProduto_altura(producaoagendamaterial.getAltura());
					material.setProduto_largura(producaoagendamaterial.getLargura());
					material.setQuantidade(qtdeProduzir != null ? qtdeProduzir : 1.0);
					materialproducaoService.getValorvendaproducao(material);			
					
					} catch (Exception e) {
					e.printStackTrace();
					}
					
					for (Materialproducao materialproducao : material.getListaProducao()) {
						Material m = materialproducao.getMaterial();
						
						if(isNotServicoEpiPatrimonio(m)){
							boolean achou = false;
							for (MateriaprimaConsumoProducaoBean materiaprima : producaoordemmaterial.getListaMateriaprima()) {
								if(materiaprima.getMaterial().equals(m)){
									achou = true;
								}
							}
							
							if(!achou){
								Double consumo = quantidadedevolvida * (materialproducao.getConsumo() / qtdereferencia);
								
								if(dimensoesProducao && corteProducao){
									Produto produto = producaoagendamaterial.getMaterial().getMaterialproduto();
									if(produto != null){
										Double largura = produto.getLargura() != null ? produto.getLargura()/1000d : 1d;
										Double altura = produto.getAltura() != null ? produto.getAltura()/1000d : 1d;
										Double comprimento = produto.getComprimento() != null ? produto.getComprimento()/1000d : 1d;
										
										Double a = (largura * altura * comprimento);
										BigDecimal c = new BigDecimal(b/a);
										
										consumo = SinedUtil.roundUp(new BigDecimal(c.doubleValue() * qtdeProduzir), 0).doubleValue() * materialproducao.getConsumo();
									}
								}
								
								MateriaprimaConsumoProducaoBean materiaprima = new MateriaprimaConsumoProducaoBean();
								materiaprima.setMaterial(m);
								materiaprima.setEmpresa(producaoordemmaterial.getEmpresa());
								materiaprima.setLocalarmazenagem(producaoordemmaterial.getLocalbaixamateriaprima() != null ? producaoordemmaterial.getLocalbaixamateriaprima() : producaoordemmaterial.getLocalarmazenagem());
								materiaprima.setQtdedisponivel(movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico(m, Materialclasse.PRODUTO, materiaprima.getLocalarmazenagem()));
								materiaprima.setBaixarmateriaprimacascata(baixarmateriaprimacascata);
								materiaprima.setQtde(consumo);
								
								String whereInProducaoagenda = CollectionsUtil.listAndConcatenate(producaoordemmaterial.getListaProducaoordemmaterialorigem(), "producaoagenda.cdproducaoagenda", ",");
								materiaprima.setWhereInAgendaProducao(whereInProducaoagenda);
								
								producaoordemmaterial.getListaMateriaprima().add(materiaprima);
							}
						}
					}
				}
			}
			
			Collections.sort(producaoordemmaterial.getListaMateriaprima(), new Comparator<MateriaprimaConsumoProducaoBean>(){
				public int compare(MateriaprimaConsumoProducaoBean o1, MateriaprimaConsumoProducaoBean o2) {
					return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
				}
			});
		}
		
		// ORDENA PELO NOME DO MATERIAL
		Collections.sort(listaProducaoordemmaterialConferencia, new Comparator<Producaoordemmaterial>(){
			public int compare(Producaoordemmaterial o1, Producaoordemmaterial o2) {
				return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		bean.setListaProducaoordemmaterial(listaProducaoordemmaterialConferencia);
		
		return bean;
	}
	
	/**
	* M�todo que registra a sa�da de mat�ria-prima
	*
	* @param materiaprima
	* @param listaProducaoagenda
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public void saidaEstoqueMateriasPrimas(MateriaprimaConsumoProducaoBean materiaprima, List<Producaoagenda> listaProducaoagenda){
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		
		Producaoagenda producaoagenda = null;
		String whereInAgendaProducao = materiaprima.getWhereInAgendaProducao();
		boolean origemUsuario = true;
		if(StringUtils.isNotBlank(whereInAgendaProducao)){
			try {
				producaoagenda = new Producaoagenda(Integer.parseInt(whereInAgendaProducao));
				movimentacaoestoqueorigem.setProducaoagenda(producaoagenda);
				origemUsuario = false;
				if(listaProducaoagenda == null) listaProducaoagenda = new ArrayList<Producaoagenda>();
				if(!listaProducaoagenda.contains(producaoagenda)){
					listaProducaoagenda.add(producaoagenda);
				}else {
					producaoagenda = listaProducaoagenda.get(listaProducaoagenda.indexOf(producaoagenda));
				}
			} catch (Exception e) {}
		} 
		if(origemUsuario){
			movimentacaoestoqueorigem.setUsuario((Usuario)Neo.getUser());
		}
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setEmpresa(materiaprima.getEmpresa());
		movimentacaoestoque.setMaterial(materiaprima.getMaterial());
		if (materiaprima.getFracaounidademedida() != null && materiaprima.getFracaounidademedida() != 0 && materiaprima.getUnidademedida() != null && 
				materiaprima.getMaterial().getUnidademedida() != null && !materiaprima.getUnidademedida().equals(materiaprima.getMaterial().getUnidademedida())) {
			materiaprima.getUnidademedida().setCasasdecimaisestoque(materiaprima.getCasasdecimaisestoqueunidademedida());
			movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(materiaprima.getQtde() * (materiaprima.getFracaounidademedida()  / (materiaprima.getQtdereferenciaunidademedida() != null ? materiaprima.getQtdereferenciaunidademedida() : 1.0)), materiaprima.getUnidademedida()));
		} else {
			movimentacaoestoque.setQtde(materiaprima.getQtde());
		}

		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
		movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
		movimentacaoestoque.setLocalarmazenagem(materiaprima.getLocalarmazenagem());
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setLoteestoque(materiaprima.getLoteestoque());
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		Material mat = materialService.load(materiaprima.getMaterial(), "material.cdmaterial, material.valorcusto");
		if(mat != null)
			movimentacaoestoque.setValor(mat.getValorcusto());
		
		movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
		
		if(producaoagenda != null){
			if(StringUtils.isNotBlank(producaoagenda.getWhereInSaidaMateriaprima())){
				producaoagenda.setWhereInSaidaMateriaprima(producaoagenda.getWhereInSaidaMateriaprima() + "," + movimentacaoestoque.getCdmovimentacaoestoque());
			}else {
				producaoagenda.setWhereInSaidaMateriaprima(movimentacaoestoque.getCdmovimentacaoestoque().toString());
			}
		}
	}
	
	public void saveListaProducaoordemForChaofabrica(
			Producaochaofabrica producaochaofabrica, 
			Producaoagenda producaoagenda, 
			Producaoagendamaterial producaoagendamaterial, 
			Pedidovendamaterial pedidovendamaterial, 
			List<Producaoordem> listaProducaoordem) {
		
		List<Pedidovendamaterial> listaPedidovendamaterialBandaAlterada = new ArrayList<Pedidovendamaterial>();
		Map<Integer, Integer> mapEtapaProducaoordem = new HashMap<Integer, Integer>();
		Map<Integer, Integer> mapEtapa = new HashMap<Integer, Integer>();
		Map<Integer, Integer> mapEtapaAnterior = new HashMap<Integer, Integer>();
		
		Integer cdlocalarmazenagem = parametrogeralService.getIntegerNullable(Parametrogeral.LOCALARMAZENAGEM_CHAO_DE_FABRICA);
		if(cdlocalarmazenagem == null){
			throw new SinedException("N�o foi encontrado o local de armazenagem.");
		}
		Localarmazenagem localarmazenagem_padrao = new Localarmazenagem(cdlocalarmazenagem);
		
		Empresa empresa = producaoagenda.getEmpresa();
		Localarmazenagem localarmazenagem = empresa != null ? empresa.getLocalarmazenagemmateriaprima() : null;
		
		if(producaoagendamaterial.getMaterial() != null &&
				producaoagendamaterial.getMaterialChaofabrica() != null &&
				!producaoagendamaterial.getMaterialChaofabrica().equals(producaoagendamaterial.getMaterial())){
			producaoagendamaterialService.atualizaMaterial(producaoagendamaterial, producaoagendamaterial.getMaterialChaofabrica());
			if(pedidovendamaterial != null){
				pedidovendamaterialService.atualizaMaterial(pedidovendamaterial, producaoagendamaterial.getMaterialChaofabrica());
				pedidovendamaterialService.updateServicoAlterado(pedidovendamaterial.getCdpedidovendamaterial() + "");
			}
		}
		
		Localarmazenagem localarmazenagemprodutofinal = null;
		for (Producaoordem producaoordem : listaProducaoordem) {
			Set<Producaoordemequipamento> listaProducaoordemequipamento = producaoordem.getListaProducaoordemequipamento();
			List<Producaoordemhistorico> listaProducaoordemhistorico = producaoordem.getListaProducaoordemhistorico();
			List<Producaoagendahistorico> listaProducaoagendahistorico = producaoordem.getListaProducaoagendahistorico();
			Set<Producaoordemitemadicional> listaProducaoordemitemadicional = producaoordem.getListaProducaoordemitemadicional();
			Set<Producaoordemmaterial> listaProducaoordemmaterial = producaoordem.getListaProducaoordemmaterial();
			Set<Producaoordemconstatacao> listaProducaoordemconstatacao = producaoordem.getListaProducaoordemconstatacao();
			Integer etapa_id = producaoordem.getEtapa_id();
			Integer etapaanterior_id = producaoordem.getEtapaanterior_id();
			Producaoordemsituacao producaoordemsituacao = producaoordem.getProducaoordemsituacao();
			
			if(producaoordemsituacao.equals(Producaoordemsituacao.CANCELADA) && (listaProducaoordemhistorico == null || listaProducaoordemhistorico.size() == 0)){
				continue;
			}
			
			producaoordem.setListaProducaoordemequipamento(null);
			producaoordem.setListaProducaoordemhistorico(null);
			producaoordem.setListaProducaoordemitemadicional(null);
			producaoordem.setListaProducaoordemmaterial(null);
			producaoordem.setListaProducaoordemconstatacao(null);
			this.saveOrUpdate(producaoordem);
			
			localarmazenagemprodutofinal = localarmazenagem_padrao;
			if(empresa != null){
				Empresa empresaLocal = empresaService.loadWithLocalproducao(empresa);
				if(empresaLocal != null && empresaLocal.getLocalarmazenagemprodutofinal() != null){
					localarmazenagemprodutofinal = empresaLocal.getLocalarmazenagemprodutofinal();
				}
			}
			
			mapEtapa.put(etapa_id, producaoordem.getCdproducaoordem());
			mapEtapaProducaoordem.put(producaoordem.getCdproducaoordem(), etapa_id);
			mapEtapaAnterior.put(etapa_id, etapaanterior_id);
			
			if(producaoordemsituacao.equals(Producaoordemsituacao.CONCLUIDA)){
				for (Producaoordemequipamento producaoordemequipamento : listaProducaoordemequipamento) {
					producaoordemequipamento.setProducaoordem(producaoordem);
					producaoordemequipamentoService.saveOrUpdate(producaoordemequipamento);
				}
				
				this.doBaixaEstoqueItemAdicional(producaoagenda, producaoordem, listaProducaoordemitemadicional);
			}
			
			for (Producaoordemhistorico producaoordemhistorico : listaProducaoordemhistorico) {
				producaoordemhistorico.setProducaoordem(producaoordem);
				producaoordemhistoricoService.saveOrUpdateWithoutLog(producaoordemhistorico);
			}
			producaoordem.setListaProducaoordemhistorico(listaProducaoordemhistorico);
			
			for (Producaoagendahistorico producaoagendahistorico : listaProducaoagendahistorico) {
				producaoagendahistorico.setProducaoagenda(producaoagenda);
				producaoagendahistorico.setObservacao(producaoagendahistorico.getObservacao() + " Ordem de produ��o " + this.makeLinkHistoricoProducaoordem(producaoordem));
				producaoagendahistoricoService.saveOrUpdateWithoutLog(producaoagendahistorico);
			}
			
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				Set<Producaoordemmaterialcampo> listaProducaoordemmaterialcampo = producaoordemmaterial.getListaCampo();
				Set<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima = producaoordemmaterial.getListaProducaoordemmaterialmateriaprima();
				Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = producaoordemmaterial.getListaProducaoordemmaterialorigem();
				Set<Producaoordemmaterialproduzido> listaProducaoordemmaterialproduzido = producaoordemmaterial.getListaProducaoordemmaterialproduzido();
				
				Pneu pneu = producaoordemmaterial.getPneu();
				if(pneu != null && pneu.getCdpneu() != null && pneu.getCdmaterialbanda() != null){
					Material materialbanda = new Material(pneu.getCdmaterialbanda());
					boolean pneuAlterado = pneuService.bandaAlterada(pneu, materialbanda);
					if(pneuAlterado){
						pneuService.updateBanda(pneu, materialbanda);
						if(producaoordemmaterial.getPedidovendamaterial() != null){
							listaPedidovendamaterialBandaAlterada.add(producaoordemmaterial.getPedidovendamaterial());
						}
					}
				}
				
				producaoordemmaterial.setListaCampo(null);
				producaoordemmaterial.setListaProducaoordemmaterialmateriaprima(null);
				producaoordemmaterial.setListaProducaoordemmaterialorigem(null);
				producaoordemmaterial.setListaProducaoordemmaterialproduzido(null);
				producaoordemmaterial.setProducaoordem(producaoordem);
				producaoordemmaterialService.saveOrUpdate(producaoordemmaterial);
				
				for (Producaoordemmaterialorigem producaoordemmaterialorigem : listaProducaoordemmaterialorigem) {
					producaoordemmaterialorigem.setProducaoordemmaterial(producaoordemmaterial);
					producaoordemmaterialorigemService.saveOrUpdate(producaoordemmaterialorigem);
				}
				
				if(producaoordemsituacao.equals(Producaoordemsituacao.CONCLUIDA)){
					for (Producaoordemmaterialcampo producaoordemmaterialcampo : listaProducaoordemmaterialcampo) {
						producaoordemmaterialcampo.setProducaoordemmaterial(producaoordemmaterial);
						producaoordemmaterialcampoService.saveOrUpdate(producaoordemmaterialcampo);
					}
					
					for (Producaoordemmaterialmateriaprima producaoordemmaterialmateriaprima : listaProducaoordemmaterialmateriaprima) {
						producaoordemmaterialmateriaprima.setProducaoordemmaterial(producaoordemmaterial);
						producaoordemmaterialmateriaprimaService.saveOrUpdate(producaoordemmaterialmateriaprima);
						
						if(Boolean.TRUE.equals(producaoordemmaterialmateriaprima.getBanda()) || Boolean.TRUE.equals(producaoordemmaterialmateriaprima.getAcompanhabanda())){
							if(Boolean.TRUE.equals(producaoagendamaterial.getBandaenviada())){
								continue;
							}
						}
						
						registrarSaidaMateriaPrimaChaofabrica(producaoordemmaterialmateriaprima, producaoordem, producaoordemmaterial, producaoagenda, producaoagendamaterial, localarmazenagem, localarmazenagem_padrao);
					}
					
					for (Producaoordemmaterialproduzido producaoordemmaterialproduzido : listaProducaoordemmaterialproduzido) {
						producaoordemmaterialproduzido.setProducaoordemmaterial(producaoordemmaterial);
						producaoordemmaterialproduzidoService.saveOrUpdate(producaoordemmaterialproduzido);
					}
					
					boolean isServico = producaoordemmaterial.getMaterial() != null && materialService.isServico(producaoordemmaterial.getMaterial());
					
					Producaoetapa producaoetapa = producaoordemmaterial.getProducaoetapa() != null ? producaoetapaService.load(producaoordemmaterial.getProducaoetapa(), "producaoetapa.cdproducaoetapa, producaoetapa.naogerarentradaconclusao") : null;
					Producaoetapaitem producaoetapaitem = producaoordemmaterial.getProducaoetapaitem() != null ? producaoetapaitemService.loadForProducao(producaoordemmaterial.getProducaoetapaitem()) : null;
					Boolean naogerarentradaconclusao = producaoetapa != null && producaoetapa.getNaogerarentradaconclusao() != null && producaoetapa.getNaogerarentradaconclusao();
					producaoordemmaterial.setUltimaetapa(producaoetapaitem != null ? producaoetapaService.isUltimaEtapa(producaoetapaitem) : Boolean.FALSE);
					
					// CASO N�O TENHA MAIS ETAPAS REGISTRAR NO ESTOQUE A ENTRADA DOS MATERIAIS 
					if(!isServico && !naogerarentradaconclusao && Boolean.TRUE.equals(producaoordemmaterial.getUltimaetapa())){
						Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
						movimentacaoestoqueorigem.setProducaoordem(producaoordem);
						movimentacaoestoqueorigem.setProducaoOrdemMaterial(producaoordemmaterial);
						movimentacaoestoqueorigem.setRegistrarproducao(true);
						
						Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
						movimentacaoestoque.setEmpresa(producaoordem.getEmpresa());
						movimentacaoestoque.setMaterial(producaoordemmaterial.getMaterial());
						movimentacaoestoque.setQtde(producaoordemmaterial.getQtdeproduzido());

						if(listaProducaoordemmaterialorigem != null && SinedUtil.isListNotEmpty(listaProducaoordemmaterialorigem) && producaoagendamaterial != null){
							Producaoagendamaterial pam = producaoagendamaterialService.loadForRegistroproducao(producaoagendamaterial);
							if(pam != null){
								if(pam.getAltura() != null || pam.getLargura() != null || pam.getComprimento() != null){
									Double altura = pam.getAltura() != null ? pam.getAltura() : 1d;
									Double largura = pam.getLargura() != null ? pam.getLargura() : 1d;
									Double comprimento = pam.getComprimento() != null ? pam.getComprimento() : 1d;
									
									if(pam.getMaterial() != null && pam.getMaterial().getMaterialproduto() != null &&
											pam.getMaterial().getMaterialproduto().getFatorconversaoproducao() != null && 
											pam.getMaterial().getMaterialproduto().getFatorconversaoproducao() > 0){
										if(largura != null && pam.getLargura() != null) largura = largura / pam.getMaterial().getMaterialproduto().getFatorconversaoproducao();
										if(altura != null && pam.getAltura() != null) altura = altura / pam.getMaterial().getMaterialproduto().getFatorconversaoproducao();
										if(comprimento != null && pam.getComprimento() != null) comprimento = comprimento / pam.getMaterial().getMaterialproduto().getFatorconversaoproducao();
									}
											
									movimentacaoestoque.setQtde(altura * largura * comprimento * movimentacaoestoque.getQtde());
								}
							}
						}
						
						movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
						movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
						movimentacaoestoque.setLocalarmazenagem(localarmazenagemprodutofinal);
						movimentacaoestoque.setLoteestoque(producaoordemmaterial.getLoteestoque());
						movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
						movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
						
						Material mat = materialService.load(producaoordemmaterial.getMaterial(), "material.cdmaterial, material.valorcusto");
						if(mat != null)
							movimentacaoestoque.setValor(mat.getValorcusto());
						
						movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
						
						// REGISTRA NO HIST�RICO
						Producaoordemhistorico producaoordemhistorico = new Producaoordemhistorico();
						producaoordemhistorico.setProducaoordem(producaoordem);
						producaoordemhistorico.setObservacao("Entrada no estoque criada: " + movimentacaoestoqueService.makeLinkHistoricoMovimentacaoestoque(movimentacaoestoque));
						producaoordemhistoricoService.saveOrUpdate(producaoordemhistorico);
					}
				}
			}
			
			for (Producaoordemconstatacao producaoordemconstatacao : listaProducaoordemconstatacao) {
				producaoordemconstatacao.setProducaoordem(producaoordem);
				producaoordemconstatacaoService.saveOrUpdateWithoutLog(producaoordemconstatacao);
			}			
		}
		
//		List<Producaoagendaitemadicional> listaProducaoagendaitemadicional = producaoagendaitemadicionalService.findByProducaoagenda(producaoagenda);
//		if(SinedUtil.isListNotEmpty(listaProducaoagendaitemadicional)){
//			producaoagendaService.doBaixaEstoqueItemAdicional(producaoagenda, listaProducaoagendaitemadicional);
//		}
		
		boolean lastCancelada = false;
		if (!listaProducaoordem.isEmpty()){
			Timestamp timestampUltimaProducao = null;
			Producaoordem lastProducaoordem = null;
			for (Producaoordem producaoordem : listaProducaoordem) {
				if(producaoordem.getCdproducaoordem() != null){
					List<Producaoordemhistorico> listaProducaoordemhistorico = producaoordem.getListaProducaoordemhistorico();
					if(listaProducaoordemhistorico != null && listaProducaoordemhistorico.size() > 0){
						for (Producaoordemhistorico producaoordemhistorico : listaProducaoordemhistorico) {
							if(producaoordemhistorico != null && producaoordemhistorico.getCdproducaoordemhistorico() != null && producaoordemhistorico.getDtaltera() != null){
								if(timestampUltimaProducao == null || producaoordemhistorico.getDtaltera().getTime() > timestampUltimaProducao.getTime()){
									timestampUltimaProducao = producaoordemhistorico.getDtaltera();
									lastProducaoordem = producaoordem;
								}
							}
						}
					}
				}
			}
			if(lastProducaoordem.getProducaoordemsituacao() != null && lastProducaoordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA)){
				lastCancelada = true;
			}
			garantiareformaService.saveProducao(lastProducaoordem, true, lastCancelada);
		}
		
		
		for (Producaoordem producaoordem : listaProducaoordem) {
			Integer etapa_id = mapEtapaProducaoordem.get(producaoordem.getCdproducaoordem());
			if(etapa_id != null){
				Integer etapaanterior_id = mapEtapaAnterior.get(etapa_id);
				if(etapaanterior_id != null){
					Integer cdproducaoordemanterior = mapEtapa.get(etapaanterior_id);
					if(cdproducaoordemanterior != null){
						this.updateProducaoordemanterior(producaoordem, new Producaoordem(cdproducaoordemanterior));
					}
				}
			}
		}
		
		
		if(lastCancelada){
			String observacaorecusa = producaoagenda.getObservacaorecusa();
			Motivodevolucao motivodevolucao = null;
			if(producaoagenda.getCdmotivodevolucao() != null){
				motivodevolucao = new Motivodevolucao();
				motivodevolucao.setCdmotivodevolucao(producaoagenda.getCdmotivodevolucao());
			}
			
			if(producaoagendamaterial != null && producaoagendamaterial.getPedidovendamaterial() != null && producaoagendamaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
				List<ColetaMaterial> listaColetaMaterial = coletaMaterialService.findByPedidovendamaterial(producaoagendamaterial.getPedidovendamaterial().getCdpedidovendamaterial() + "");
				for (ColetaMaterial cm : listaColetaMaterial) {
					coletaMaterialPedidovendamaterialService.updateRecusado(cm, pedidovendamaterial);
					if(parametrogeralService.getBoolean(Parametrogeral.HABILITAR_CONFIGURACAO_OTR)
						&& !coletaMaterialService.isTodosServicosRecusados(cm)){//S� devolve a carca�a quando todos os servi�os do pneu form recusados
						continue;
					}
					if(cm.getMaterial().getServico() == null || !cm.getMaterial().getServico()){
						Materialclasse materialclasse = Materialclasse.PRODUTO;
						if(cm.getMaterial().getProduto() == null || !cm.getMaterial().getProduto()){
							if(cm.getMaterial().getEpi() != null && cm.getMaterial().getEpi()){
								materialclasse = Materialclasse.EPI;
							} else materialclasse = null;
						}
						
						if(materialclasse != null){
							Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
							movimentacaoestoque.setMaterial(cm.getMaterial());
							movimentacaoestoque.setMaterialclasse(materialclasse);
							movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
							movimentacaoestoque.setQtde(cm.getQuantidade());
							movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
							movimentacaoestoque.setLocalarmazenagem(cm.getLocalarmazenagem());
							movimentacaoestoque.setEmpresa(empresa);
							movimentacaoestoque.setValor(cm.getValorunitario());
							
							Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
							movimentacaoestoqueorigem.setColeta(cm.getColeta());
							movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
							
							movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
						}
					}
					
					coletaMaterialService.adicionaQtdeDevolvida(cm, cm.getQuantidade());
					
//					if(motivodevolucao != null)
//						coletaMaterialService.adicionaMotivodevolucao(cm, motivodevolucao);
					coletamaterialmotivodevolucaoService.saveLista(cm, producaoagenda.getListaMotivodevolucao(), true);
					
					StringBuilder obs = new StringBuilder();
					obs.append("Devolu��o do material: " + cm.getMaterial().getNome());
					if(observacaorecusa != null && !observacaorecusa.trim().equals("")){
						obs.append(" (").append(observacaorecusa).append(")");
					}
					if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
						for(ColetaMaterialPedidovendamaterial itemServico : cm.getListaColetaMaterialPedidovendamaterial()) {
							if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
								obs.append(" (").append(itemServico.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
							}			
						}
					}
//					if(cm.getPedidovendamaterial() != null && cm.getPedidovendamaterial().getCdpedidovendamaterial() != null){
//						obs.append(" (").append(cm.getPedidovendamaterial().getCdpedidovendamaterial()).append(")");
//					}
					
					Coletahistorico coletahistorico = new Coletahistorico();
					coletahistorico.setAcao(Coletaacao.DEVOLVIDO);
					coletahistorico.setColeta(cm.getColeta());
					coletahistorico.setObservacao(obs.toString());
					coletahistoricoService.saveOrUpdate(coletahistorico);
				}
			}
		}

		Producaochaofabricasituacao producaochaofabricasituacaoAtual = lastCancelada ? Producaochaofabricasituacao.CANCELADA : Producaochaofabricasituacao.CONCLUIDA;
		producaochaofabricaService.updateSituacao(producaochaofabrica, producaochaofabricasituacaoAtual);

		try {
			if(parametrogeralService.getBoolean(Parametrogeral.EMITIR_ETIQUETA_PNEU_AUTOMATICA)){
				producaochaofabricaService.updateEmitirEtiquetaAutomatico(producaochaofabrica, Boolean.TRUE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean haveConcluida = false;
		boolean haveNotConcluidaCancelada = false;
		
		List<Producaochaofabrica> listaProducaochaofabrica = producaochaofabricaService.findByProducaoagenda(producaoagenda, true);
		for (Producaochaofabrica producaochaofabricaIt : listaProducaochaofabrica) {
			Producaochaofabricasituacao producaochaofabricasituacao = producaochaofabricaIt.getProducaochaofabricasituacao();
			
			try {
				if(Util.objects.isPersistent(producaochaofabricaIt) && producaochaofabricaIt.getCdproducaochaofabrica().equals(producaochaofabrica.getCdproducaochaofabrica())){
					if(!producaochaofabricasituacaoAtual.equals(producaochaofabricasituacao)){
						System.out.println("*** teste_pauliceia: situacao java: " + producaochaofabricasituacaoAtual.toString() + " - situacao origem banco de dados: " + producaochaofabricasituacao.toString());
						producaochaofabricasituacao = producaochaofabricasituacaoAtual;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			switch (producaochaofabricasituacao) {
				case CONCLUIDA:
					haveConcluida = true;
					break;
				case CANCELADA:
					break;
				default:
					haveNotConcluidaCancelada = true;
					break;
			}
		}
		
		if(!haveNotConcluidaCancelada){
			if(!haveConcluida){
				producaoagendaService.updateSituacao(producaoagenda, Producaoagendasituacao.CANCELADA);
			} else {
				producaoagendaService.updateSituacao(producaoagenda, Producaoagendasituacao.CONCLUIDA);
			}
		}
		
		pedidovendaService.updateBandaAlterada(listaPedidovendamaterialBandaAlterada);
	}
	
	public void registrarSaidaMateriaPrimaChaofabrica(Producaoordemmaterialmateriaprima producaoordemmaterialmateriaprima, Producaoordem producaoordem, Producaoordemmaterial producaoOrdemMaterial,
			Producaoagenda producaoagenda,  Producaoagendamaterial producaoAgendaMaterial, Localarmazenagem localarmazenagem, Localarmazenagem localarmazenagem_padrao) {

		if(producaoordemmaterialmateriaprima.getQtdeprevista() != null && producaoordemmaterialmateriaprima.getQtdeprevista() > 0){
			Material material = materialService.unidadeMedidaMaterial(producaoordemmaterialmateriaprima.getMaterial());
			Double quantidade = producaoordemmaterialmateriaprima.getQtdeprevista();
			Unidademedida unidademedida = producaoordemmaterialmateriaprima.getUnidademedida();
			if(material != null && 
					material.getUnidademedida() != null && 
					material.getUnidademedida().getCdunidademedida() != null &&
					unidademedida != null &&
					unidademedida.getCdunidademedida() != null &&
					!material.getUnidademedida().getCdunidademedida().equals(unidademedida.getCdunidademedida())){
				quantidade = unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), quantidade, unidademedida, material, null, null);
			}
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setProducaoordem(producaoordem);
			movimentacaoestoqueorigem.setProducaoOrdemMaterial(producaoOrdemMaterial);
			movimentacaoestoqueorigem.setProducaoagenda(producaoagenda);
			movimentacaoestoqueorigem.setProducaoAgendaMaterial(producaoAgendaMaterial);
			movimentacaoestoqueorigem.setRegistrarproducao(true);
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setEmpresa(producaoordem != null ? producaoordem.getEmpresa() : (producaoagenda != null && producaoagenda.getEmpresa() != null ? producaoagenda.getEmpresa() : null));
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setQtde(quantidade);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoque.setLocalarmazenagem(localarmazenagem != null ? localarmazenagem : localarmazenagem_padrao);
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			
			Material mat = materialService.load(material, "material.cdmaterial, material.valorcusto");
			if(mat != null) movimentacaoestoque.setValor(mat.getValorcusto());
			
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
		}
	}
	
	/**
	 * Realiza a baixa de estoque dos itens adicionais
	 *
	 * @param producaoagenda
	 * @param producaoordem
	 * @param listaProducaoordemitemadicional
	 * @author Rodrigo Freitas
	 * @since 30/01/2017
	 */
	private void doBaixaEstoqueItemAdicional(Producaoagenda producaoagenda, Producaoordem producaoordem, Set<Producaoordemitemadicional> listaProducaoordemitemadicional) {
		for (Producaoordemitemadicional producaoordemitemadicional : listaProducaoordemitemadicional) {
			producaoordemitemadicional.setProducaoordem(producaoordem);
			producaoordemitemadicionalService.saveOrUpdate(producaoordemitemadicional);
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setProducaoordem(producaoordem);
			movimentacaoestoqueorigem.setProducaoagenda(producaoagenda);
			movimentacaoestoqueorigem.setRegistrarproducao(true);
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setEmpresa(producaoordem.getEmpresa());
			movimentacaoestoque.setMaterial(producaoordemitemadicional.getMaterial());
			movimentacaoestoque.setQtde(producaoordemitemadicional.getQuantidade());
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoque.setLocalarmazenagem(producaoordemitemadicional.getLocalarmazenagem());
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			
			Material mat = materialService.load(producaoordemitemadicional.getMaterial(), "material.cdmaterial, material.valorcusto");
			if(mat != null) movimentacaoestoque.setValor(mat.getValorcusto());
			
			movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
			
			if(producaoordem != null) {
				movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA ORDEM DE PRODU��O " + SinedUtil.makeLinkHistorico(movimentacaoestoqueorigem.getProducaoordem().getCdproducaoordem().toString(), "visualizarOrdemDeProducao"), true);					
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param producaoordem
	 * @param producaoordemanterior
	 * @author Rodrigo Freitas
	 * @since 08/07/2016
	 */
	public void updateProducaoordemanterior(Producaoordem producaoordem, Producaoordem producaoordemanterior) {
		producaoordemDAO.updateProducaoordemanterior(producaoordem, producaoordemanterior);
	}
	
	public Producaoordem loadForGarantia(Producaoordem producaoordem){
		if (producaoordem!=null && producaoordem.getCdproducaoordem()!=null){
			return producaoordemDAO.loadForGarantia(producaoordem);
		}else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Producaoordemmaterial> prepareForRegistroProducaoemlote(String whereIn) {
		List<Producaoordemmaterial> listaProducaoordemmaterialTela = new ArrayList<Producaoordemmaterial>();
		List<Producaoordem> listaProducaoordem = this.findForRegistrarProducao(whereIn);
		
		for (Producaoordem producaoordem : listaProducaoordem) {
			Set<Producaoordemmaterial> listaProducaoordemmaterial = producaoordem.getListaProducaoordemmaterial();
			for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial) {
				if(producaoordemmaterial.getQtdeproduzido() == null){
					producaoordemmaterial.setQtdeproduzido(0d);
				}
				if(producaoordemmaterial.getQtdeperdadescarte() == null){
					producaoordemmaterial.setQtdeperdadescarte(0d);
				}
				
				List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.findAtivosByEmpresa(producaoordem.getEmpresa());
				if(SinedUtil.isListNotEmpty(listaLocalarmazenagem) && listaLocalarmazenagem.size() == 1){
					producaoordemmaterial.setLocalarmazenagem(listaLocalarmazenagem.get(0));
				}

				producaoordemmaterial.setEmpresa(producaoordem.getEmpresa());
				producaoordemmaterial.setDtprevisaoentrega(producaoordem.getDtprevisaoentrega());
				producaoordemmaterial.setQtderegistro(producaoordemmaterial.getQtde() - producaoordemmaterial.getQtdeproduzido() - producaoordemmaterial.getQtdeperdadescarte());
				producaoordemmaterial.setQtderegistroperda(0d);
				
				listaProducaoordemmaterialTela.add(producaoordemmaterial);
			}
		}
		
		for (Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterialTela) {
			Producaoetapaitem producaoetapaitem = producaoordemmaterial.getProducaoetapaitem();
			boolean ultimaetapa = true;
			if(producaoetapaitem != null){
				ultimaetapa = producaoetapaService.isUltimaEtapa(producaoetapaitem);
			}
			producaoordemmaterial.setUltimaetapa(ultimaetapa);
			if(producaoordemmaterial.getPedidovendamaterial() != null && producaoordemmaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null){
				producaoordemmaterial.setHaveColeta(coletaMaterialService.haveColetaMaterialByPedidovendamaterial(producaoordemmaterial.getPedidovendamaterial()));
			}
			producaoordemmaterial.setListaProducaoordemmaterialperdadescarte(producaoordemmaterialperdadescarteService.findByProducaoordemmaterial(producaoordemmaterial));
			producaoordemmaterial.setListaProducaoordemmaterialoperador(SinedUtil.listToSet(producaoordemmaterialoperadorService.findByProducaoordemmaterial(producaoordemmaterial), Producaoordemmaterialoperador.class));
		}
		
		// ORDENA PELO NOME DO MATERIAL
		Collections.sort(listaProducaoordemmaterialTela, new Comparator<Producaoordemmaterial>(){
			public int compare(Producaoordemmaterial o1, Producaoordemmaterial o2) {
				if(o1.getMaterial().getNome().equals(o2.getMaterial().getNome())){
					if(o1.getDtprevisaoentrega().equals(o2.getDtprevisaoentrega())){
						return o1.getProducaoordem().getCdproducaoordem().compareTo(o2.getProducaoordem().getCdproducaoordem());
					} else return o1.getDtprevisaoentrega().compareTo(o2.getDtprevisaoentrega());
				} else return o1.getMaterial().getNome().compareTo(o2.getMaterial().getNome());
			}
		});
		
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterialTela)){
			producaoordemmaterialcampoService.preencherListaCamposAdicionais(SinedUtil.listToSet(listaProducaoordemmaterialTela, Producaoordemmaterial.class));
		}
		return listaProducaoordemmaterialTela;
	}

	public boolean validaObrigatoriedadePneu(Producaoordem bean, WebRequestContext request, BindException errors) {
		boolean valido = true;
		if(SinedUtil.isRecapagem() && parametrogeralService.getBoolean(Parametrogeral.BANDA_OBRIGATORIA_QUANDO_SERVICO_POSSUIR_BANDA) &&
				bean != null && SinedUtil.isListNotEmpty(bean.getListaProducaoordemmaterial())){
			for(Producaoordemmaterial item : bean.getListaProducaoordemmaterial()){
				if(item.getPneu() != null){
					if(!pneuService.validaObrigatoriedadePneu(item.getMaterial(), item.getPneu(), request, errors)){
						valido = false;
					}
				}
			}
		}
		return valido;
	}
	
	public List<Producaoordem> findForDevolverColeta(String whereIn, String whereInProducaoordemmaterial) {
		return producaoordemDAO.findForDevolverColeta(whereIn, whereInProducaoordemmaterial);
	}
	public Producaoordem findByPneuAndEtapa(Pneu pneu,Producaoetapaitem ultimaEtapa){
		return producaoordemDAO.findByPneuAndEtapa(pneu,ultimaEtapa);
	}
	
	public SitucaoPneuEtiqueteEnum pneuProduzidoOuCancelado(Pneu pneu,Producaoetapaitem ultimaEtapa) {
		Producaoordem ordem = this.findByPneuAndEtapa(pneu,ultimaEtapa);
		
		if(ordem != null && ordem.getProducaoordemsituacao() != null){
			if(ordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CANCELADA)){
				return SitucaoPneuEtiqueteEnum.CANCELADO;
			}else if(ordem.getProducaoordemsituacao().equals(Producaoordemsituacao.CONCLUIDA)){
				return SitucaoPneuEtiqueteEnum.PRODUZIDO;
			}
		}
		return SitucaoPneuEtiqueteEnum.INCOMPLETO;
	}

	public List<OrdemProducaoSped> findOrdensAvulsasForRegistroK230(SpedarquivoFiltro filtro) {
		return producaoordemDAO.findOrdensAvulsasForRegistroK230(filtro);
	}
	public List<OrdemProducaoSped> findOrdemOrigemAgendaForRegistroK230(SpedarquivoFiltro filtro, String whereIn) {
		return producaoordemDAO.findOrdemOrigemAgendaForRegistroK230(filtro, whereIn);
	}
	
	@Override
	public Producaoordem loadForEntrada(Producaoordem bean) {
		bean = super.loadForEntrada(bean);
		bean.setListaProducaoordemitemadicional(CollectionsUtil.listToSet(Producaoordemitemadicional.class, producaoordemitemadicionalService.findForProducaoordemEntrada(bean)));
		bean.setListaProducaoordemconstatacao(CollectionsUtil.listToSet(Producaoordemconstatacao.class, producaoordemconstatacaoService.findByProducaoordem(bean)));
		for(Producaoordemmaterial item: bean.getListaProducaoordemmaterial()){
			if(Util.objects.isPersistent(item.getPedidovendamaterial()) && Util.objects.isPersistent(item.getPedidovendamaterial().getPedidovendamaterialgarantido())
				&& Util.objects.isPersistent(item.getPedidovendamaterial().getPedidovendamaterialgarantido().getMaterial())){
				Integer cdmaterial = item.getPedidovendamaterial().getPedidovendamaterialgarantido().getMaterial().getCdmaterial();
				item.getPedidovendamaterial().getPedidovendamaterialgarantido().getMaterial().setListaProducao(CollectionsUtil.listToSet(Materialproducao.class, materialproducaoService.findListaProducao(cdmaterial)));
			}
		}
		return bean;
	}
}
