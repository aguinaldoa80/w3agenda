package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.view.Vwrelatorioinspecao;
import br.com.linkcom.sined.geral.dao.VwrelatorioinspecaoDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.report.filter.FormularioinspecaoFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VwrelatorioinspecaoService extends GenericService<Vwrelatorioinspecao>{
	
	private VwrelatorioinspecaoDAO vwrelatorioinspecaoDAO;
	
	public void setVwrelatorioinspecaoDAO(VwrelatorioinspecaoDAO vwrelatorioinspecaoDAO) {
		this.vwrelatorioinspecaoDAO = vwrelatorioinspecaoDAO;
	}
	/**
	 * M�todo que faz referencia ao DAO para buscar uma lista da vwrelatorioinspecao
	 * 
	 * @author Ramon Brazil
	 * @param veiculo,filtro
	 * @return List<Vwrelatorioinspecao>
	 * @see br.com.linkcom.w3auto.geral.dao.VwrelatorioinspecaoDAO#findForRelatorio(veiculo, filtro)
	 */
	/*public List<Vwrelatorioinspecao> findForRelatorio(Veiculo veiculo,FormularioinspecaoFiltro filtro ){
		return vwrelatorioinspecaoDAO.findForRelatorio(veiculo, filtro);
	}*/
	/**
	 * M�todo que faz referencia ao DAO para buscar uma lista da vwrelatorioinspecao
	 * 
	 * @author Fernando Boldrini
	 * @param filtro, order
	 * @return List<Vwrelatorioinspecao>
	 * @see br.com.linkcom.w3auto.geral.dao.VwrelatorioinspecaoDAO#findForVeiculoParaInspecao(filtro, order)
	 */
	public List<Vwrelatorioinspecao> findForVeiculoParaInspecao(
			FormularioinspecaoFiltro filtro, String order) {
		return vwrelatorioinspecaoDAO.findForVeiculoParaInspecao(filtro, order);
	}
}