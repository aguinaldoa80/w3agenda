package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognrerateio;
import br.com.linkcom.sined.geral.dao.ConfiguracaognrerateioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ConfiguracaognrerateioService extends GenericService<Configuracaognrerateio>{
	
	private ConfiguracaognrerateioDAO configuracaognrerateioDAO;

	public void setConfiguracaognrerateioDAO(
			ConfiguracaognrerateioDAO configuracaognrerateioDAO) {
		this.configuracaognrerateioDAO = configuracaognrerateioDAO;
	}
	
	public List<Configuracaognrerateio> findByConfiguracaognre(Configuracaognre configuracaognre) {
		return configuracaognrerateioDAO.findByConfiguracaognre(configuracaognre);
	}

}
