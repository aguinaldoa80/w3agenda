package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacaodocumento;
import br.com.linkcom.sined.geral.dao.ContratofaturalocacaodocumentoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratofaturalocacaodocumentoService extends GenericService<Contratofaturalocacaodocumento> {

	private ContratofaturalocacaodocumentoDAO contratofaturalocacaodocumentoDAO;
	
	public void setContratofaturalocacaodocumentoDAO(
			ContratofaturalocacaodocumentoDAO contratofaturalocacaodocumentoDAO) {
		this.contratofaturalocacaodocumentoDAO = contratofaturalocacaodocumentoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaodocumentoDAO#findForValidacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/09/2013
	 */
	public List<Contratofaturalocacaodocumento> findForValidacao(String whereIn) {
		return contratofaturalocacaodocumentoDAO.findForValidacao(whereIn);
	}
	
}
