package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaomaterial;
import br.com.linkcom.sined.geral.dao.ContratofaturalocacaomaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratofaturalocacaomaterialService extends GenericService<Contratofaturalocacaomaterial>{
	
	private ContratofaturalocacaomaterialDAO contratofaturalocacaomaterialDAO;
	
	public void setContratofaturalocacaomaterialDAO(
			ContratofaturalocacaomaterialDAO contratofaturalocacaomaterialDAO) {
		this.contratofaturalocacaomaterialDAO = contratofaturalocacaomaterialDAO;
	}

	public List<Contratofaturalocacaomaterial> findByContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		return contratofaturalocacaomaterialDAO.findByContratofaturalocacao(contratofaturalocacao);
	}
	

}
