package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicobanco;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.ServicobancoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicobancoService extends GenericService<Servicobanco>{
	
	private ServicobancoDAO servicobancoDAO;
	
	public void setServicobancoDAO(ServicobancoDAO servicobancoDAO) {
		this.servicobancoDAO = servicobancoDAO;
	}
	
	@Override
	public void saveOrUpdate(Servicobanco bean) {
		boolean novoRegistro = false;
		Servicobanco servicobancoAnterior = null;
		if(bean.getCdservicobanco() == null)
			novoRegistro = true;
		else 
			servicobancoAnterior = load(bean);
		
		super.saveOrUpdate(bean);
		SinedUtil.executaBriefCase(Servicoservidortipo.BANCO_DADOS, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCdservicobanco());
		if(bean.getAtivo() && (servicobancoAnterior == null || !servicobancoAnterior.getAtivo()))
			SinedUtil.executaBriefCase(Servicoservidortipo.BANCO_DADOS, AcaoBriefCase.ATIVAR, bean.getCdservicobanco());
		else if(!bean.getAtivo() && (servicobancoAnterior == null || servicobancoAnterior.getAtivo()))
			SinedUtil.executaBriefCase(Servicoservidortipo.BANCO_DADOS, AcaoBriefCase.DESATIVAR, bean.getCdservicobanco());
	}
	
	@Override
	public void delete(Servicobanco bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.BANCO_DADOS, AcaoBriefCase.REMOVER, bean.getCdservicobanco());
		super.delete(bean);
	}

	public int countContratomaterial(Integer cdservicobanco, Contratomaterial contratomaterial) {
		return servicobancoDAO.countContratomaterial(cdservicobanco, contratomaterial);
	}
	
}
