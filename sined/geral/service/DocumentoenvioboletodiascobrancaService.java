package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletodiascobranca;
import br.com.linkcom.sined.geral.dao.DocumentoenvioboletodiascobrancaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoenvioboletodiascobrancaService extends GenericService<Documentoenvioboletodiascobranca> {
	
	private DocumentoenvioboletodiascobrancaDAO documentoenvioboletodiascobrancaDAO;
	
	public void setDocumentoenvioboletodiascobrancaDAO(
			DocumentoenvioboletodiascobrancaDAO documentoenvioboletodiascobrancaDAO) {
		this.documentoenvioboletodiascobrancaDAO = documentoenvioboletodiascobrancaDAO;
	}

	/**
	 * Faz referÍncia ao DAO.
	 * 	 
	 * @param documentoenvioboleto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/07/2018
	 */
	public List<Documentoenvioboletodiascobranca> findByDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		return documentoenvioboletodiascobrancaDAO.findByDocumentoenvioboleto(documentoenvioboleto);
	}


}