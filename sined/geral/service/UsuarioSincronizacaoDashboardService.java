package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.UsuarioSincronizacaoDashboard;
import br.com.linkcom.sined.geral.dao.UsuarioSincronizacaoDashboardDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class UsuarioSincronizacaoDashboardService extends GenericService<UsuarioSincronizacaoDashboard>{

	private UsuarioSincronizacaoDashboardDAO usuarioSincronizacaoDashboardDAO;
	public void setUsuarioSincronizacaoDashboardDAO(UsuarioSincronizacaoDashboardDAO usuarioSincronizacaoDashboardDAO) {this.usuarioSincronizacaoDashboardDAO = usuarioSincronizacaoDashboardDAO;}
	
	/* singleton */
	private static UsuarioSincronizacaoDashboardService instance;
	public static UsuarioSincronizacaoDashboardService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuarioSincronizacaoDashboardService.class);
		}
		return instance;
	}
	
	public List<UsuarioSincronizacaoDashboard> findForSincronizacao(){
		return usuarioSincronizacaoDashboardDAO.findForSincronizacao();	
	}
	
	public List<UsuarioSincronizacaoDashboard> findForSincronizacaoDelete(){
		return usuarioSincronizacaoDashboardDAO.findForSincronizacaoDelete();	
	}

	public void updateError(Integer idusuario, Integer codigo, String mensagem) {
		usuarioSincronizacaoDashboardDAO.updateError(idusuario, codigo, mensagem);
	}

	public void updateSincronizacao(Integer idusuario, String token, String idusuariodashboard, Integer codigo, String mensagem ) {
		usuarioSincronizacaoDashboardDAO.updateSincronizacao(idusuario, token,idusuariodashboard, codigo, mensagem);
	}
	
	public void updateSincronizacao(Integer idusuario, Integer codigo, String mensagem ) {
		usuarioSincronizacaoDashboardDAO.updateSincronizacaodelete(idusuario,codigo, mensagem);
	}
	
	public UsuarioSincronizacaoDashboard findByUsuario(Usuario usuario){
		return usuarioSincronizacaoDashboardDAO.findByUsuario(usuario);
	}
}
