package br.com.linkcom.sined.geral.service;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Faturamentocontratohistorico;
import br.com.linkcom.sined.geral.dao.FaturamentocontratohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FaturamentocontratohistoricoService extends GenericService<Faturamentocontratohistorico> {

	private FaturamentocontratohistoricoDAO faturamentocontratohistoricoDAO;
	
	public void setFaturamentocontratohistoricoDAO(
			FaturamentocontratohistoricoDAO faturamentocontratohistoricoDAO) {
		this.faturamentocontratohistoricoDAO = faturamentocontratohistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInFaturamentohitorico
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public void updateFinalizado(String whereInFaturamentohitorico) {
		faturamentocontratohistoricoDAO.updateFinalizado(whereInFaturamentohitorico);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInContrato
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public boolean haveFaturamentoNaoFinalizadoByContrato(String whereInContrato) {
		return faturamentocontratohistoricoDAO.haveFaturamentoNaoFinalizadoByContrato(whereInContrato);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @param dtvencimento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/06/2014
	 */
	public boolean haveFaturamentoByContratoDtvencimento(Contrato contrato, Date dtvencimento) {
		return faturamentocontratohistoricoDAO.haveFaturamentoByContratoDtvencimento(contrato, dtvencimento);
	}

}
