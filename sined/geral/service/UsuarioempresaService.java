package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.dao.UsuarioempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.usuarioempresa.UsuarioempresaRESTModel;

public class UsuarioempresaService extends GenericService<Usuarioempresa> {

	private UsuarioService usuarioService;
	private UsuarioempresaDAO usuarioempresaDAO;
	
	public void setUsuarioempresaDAO(UsuarioempresaDAO usuarioempresaDAO) {
		this.usuarioempresaDAO = usuarioempresaDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	/**
	 * M�todo para carregar as empresas selecionadas para o usu�rio
	 * 
	 * @param usuario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Empresa> carregaEmpresa(Usuario usuario){
		List<Usuarioempresa> listaUsuarioempresa = null;
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if (usuario.getListaUsuarioempresa() == null || usuario.getListaUsuarioempresa().isEmpty() && usuario.getCdpessoa() != null) {
			Usuario usuarioTmp = usuarioService.carregaUsuarioWithEmpresa(usuario);
			listaUsuarioempresa = usuarioTmp.getListaUsuarioempresa();
		} else {
			listaUsuarioempresa = usuario.getListaUsuarioempresa();
		}
		for (Usuarioempresa usuarioempresa : listaUsuarioempresa) {
			listaEmpresa.add(usuarioempresa.getEmpresa());
		}
		return listaEmpresa;
	}	

	/* singleton */
	private static UsuarioempresaService instance;
	public static UsuarioempresaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(UsuarioempresaService.class);
		}
		return instance;
	}

	public List<Usuarioempresa> findByEmpresa(String itens) {
		return usuarioempresaDAO.findByEmpresa(itens);
	}
	
	public List<UsuarioempresaRESTModel> findForAndroid(String whereInUsuarioempresa, Boolean excluido, String whereInUsuario) {
		List<UsuarioempresaRESTModel> lista = new ArrayList<UsuarioempresaRESTModel>();
		if(StringUtils.isNotEmpty(whereInUsuario) && StringUtils.isNotEmpty(whereInUsuarioempresa)){
			for(Usuarioempresa bean : usuarioempresaDAO.findForAndroid(whereInUsuarioempresa, whereInUsuario))
				lista.add(new UsuarioempresaRESTModel(bean));
		}
		return lista;
	}
	

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param usuario
	 * @param empresa
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/12/2016
	 */
	public boolean haveRegistro(Usuario usuario, Empresa empresa) {
		return usuarioempresaDAO.haveRegistro(usuario, empresa);
	}
}
