package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendapagamentorepresentacao;
import br.com.linkcom.sined.geral.dao.PedidovendapagamentorepresentacaoDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaPagamentoRepresentacaoWSBean;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendapagamentorepresentacaoService extends GenericService<Pedidovendapagamentorepresentacao> {

	private PedidovendapagamentorepresentacaoDAO pedidovendapagamentorepresentacaoDAO;
	
	public void setPedidovendapagamentorepresentacaoDAO(PedidovendapagamentorepresentacaoDAO pedidovendapagamentorepresentacaoDAO) {
		this.pedidovendapagamentorepresentacaoDAO = pedidovendapagamentorepresentacaoDAO;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendapagamentorepresentacaoDAO#findPedidovendaPagamentoRepresentacaoByPedidovenda(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 13/02/2015
	* @author Luiz Fernando
	*/
	public List<Pedidovendapagamentorepresentacao> findPedidovendaPagamentoRepresentacaoByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendapagamentorepresentacaoDAO.findPedidovendaPagamentoRepresentacaoByPedidovenda(pedidovenda);
	}
	
	public List<PedidoVendaPagamentoRepresentacaoWSBean> toWSList(List<Pedidovendapagamentorepresentacao> listaPedidoVendaPagamentoRepresentacao){
		List<PedidoVendaPagamentoRepresentacaoWSBean> lista = new ArrayList<PedidoVendaPagamentoRepresentacaoWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidoVendaPagamentoRepresentacao)){
			for(Pedidovendapagamentorepresentacao pvr: listaPedidoVendaPagamentoRepresentacao){
				PedidoVendaPagamentoRepresentacaoWSBean bean = new PedidoVendaPagamentoRepresentacaoWSBean();
				bean.setCdpedidovendapagamentorepresentacao(pvr.getCdpedidovendapagamentorepresentacao());
				bean.setDataparcela(pvr.getDataparcela());
				bean.setDocumento(ObjectUtils.translateEntityToGenericBean(pvr.getDocumento()));
				bean.setDocumentotipo(ObjectUtils.translateEntityToGenericBean(pvr.getDocumentotipo()));
				bean.setFornecedor(ObjectUtils.translateEntityToGenericBean(pvr.getFornecedor()));
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvr.getPedidovenda()));
				bean.setValororiginal(pvr.getValororiginal());
				
				lista.add(bean);
			}
		}
		return lista;
	}
}
