package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Materialcor;
import br.com.linkcom.sined.geral.dao.MaterialcorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialcorService extends GenericService<Materialcor> {

	private MaterialcorDAO materialcorDAO;
	
	public void setMaterialcorDAO(MaterialcorDAO materialcorDAO) {
		this.materialcorDAO = materialcorDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param nome
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public Materialcor findByNomeForImportacao(String nome) {
		return materialcorDAO.findByNomeForImportacao(nome);
	}


}
