package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ClienteFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContatosclientesService extends GenericService<Cliente>{
	
	ClienteService clienteService;
	
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	
	public void processarListagem(ListagemResult<Cliente> listagemResult, ClienteFiltro filtro) {
		List<Cliente> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdpessoa", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(clienteService.findListagem(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}		
	}
	
	@Override
	protected ListagemResult<Cliente> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Cliente> listagemResult = super.findForExportacao(filtro);
		processarListagem(listagemResult, (ClienteFiltro) filtro);
		
		return listagemResult;
	}
}
