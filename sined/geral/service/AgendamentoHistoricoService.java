package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.AgendamentoHistorico;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.state.AgendamentoAcao;
import br.com.linkcom.sined.geral.dao.AgendamentoHistoricoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AgendamentoHistoricoService extends GenericService<AgendamentoHistorico>{

	AgendamentoHistoricoDAO agendamentoHistoricoDAO;

	public void setAgendamentoHistoricoDAO(AgendamentoHistoricoDAO agendamentoHistoricoDAO) {
		this.agendamentoHistoricoDAO = agendamentoHistoricoDAO;
	}
	
	/**
	 * Gera uma linha de hist�rico de agendamento de a cordo com a a��o (criado, alterado, finalizado, etc...)
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoHistoricoDAO#carregarListaHistorico(Agendamento)
	 * @param bean
	 * @param documento
	 * @author Hugo Ferreira
	 */
	public void gerenciaHistorico(Agendamento bean, Object vinculo) {
		AgendamentoHistorico  historico = null;
		AgendamentoHistorico historico2  = null;
		Set<AgendamentoHistorico> listaAgendamentoHistorico;
		String observacao = null;

		if (bean.getCdagendamento() == null) { //Criando...
			listaAgendamentoHistorico = new ListSet<AgendamentoHistorico>(AgendamentoHistorico.class);
			historico = new AgendamentoHistorico(null, null, new AgendamentoAcao(AgendamentoAcao.CRIADO), null, 
					SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
		} else { //Editando...
			listaAgendamentoHistorico = new ListSet<AgendamentoHistorico>(AgendamentoHistorico.class, this.carregarListaHistorico(bean));

			if (vinculo == null) { //A��o n�o � consolidar:
				historico = new AgendamentoHistorico(null, bean, new AgendamentoAcao(AgendamentoAcao.ALTERADO), null, 
						SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
			} else { //A��o � consolidar:
				if (vinculo instanceof Documento) {
					Documento documento = (Documento)vinculo;

					if (documento.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR)) {
						observacao = "Foi gerada a conta a pagar <a href=\"javascript:visualizarContapagar(" + 
						documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a>";
					} else {
						observacao = "Foi gerada a conta a receber <a href=\"javascript:visualizarContareceber(" + 
						documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a>";
					}
				} else if (vinculo instanceof Movimentacao) {
					Movimentacao movimentacao = (Movimentacao)vinculo;
					
					observacao = "Foi gerada a movimenta��o <a href=\"javascript:visualizarMovimentacao(" + 
					movimentacao.getCdmovimentacao() + ");\">" + movimentacao.getCdmovimentacao() + "</a>";
				} else {
					throw new SinedException("N�o foi poss�vel consolidar o agendamento " + bean.getCdagendamento() + 
							" pois ele n�o n�o gera um registro de " + vinculo.getClass().getName());
				}
				historico = new AgendamentoHistorico(null, bean, new AgendamentoAcao(AgendamentoAcao.CONSOLIDADO), observacao, 
						SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
				
				if (bean.getDtfim() != null && SinedDateUtils.afterIgnoreHour(bean.getDtproximo(), bean.getDtfim())) { //Alcan�ou a �ltima itera��o
					observacao = "T�rmino do agendamento.";
					historico2 = new AgendamentoHistorico(null, bean, new AgendamentoAcao(AgendamentoAcao.FINALIZADO), observacao,
							SinedUtil.getUsuarioLogado().getCdpessoa(), new Timestamp(System.currentTimeMillis()));
				}
			}
		}

		listaAgendamentoHistorico.add(historico);
		if (historico2 != null) {
			listaAgendamentoHistorico.add(historico2);
		}
		bean.setListaAgendamentoHistorico(listaAgendamentoHistorico);
	}

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Fornece o hist�rico de um agendamento ordenado de forma com que as �ltimas
	 * a��es apare�am primeiro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AgendamentoHistoricoDAO.#carregarHistorico(Agendamento)
	 * @param agendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<AgendamentoHistorico> carregarListaHistorico(Agendamento agendamento) {
		return agendamentoHistoricoDAO.carregarListaHistorico(agendamento);
	}
	
	/**
	 * M�todo para gerar um Agendamentohistorico com os dados de um bean de Agendamento.
	 * 
	 * @param agendamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public AgendamentoHistorico geraHistoricoAgendamento(Agendamento agendamento){
		AgendamentoHistorico agendamentohistorico = new AgendamentoHistorico();
		agendamentohistorico.setAgendamento(agendamento);
		agendamentohistorico.setCdusuarioaltera(agendamento.getCdusuarioaltera());
		agendamentohistorico.setDtaltera(agendamento.getDtaltera());
		agendamentohistorico.setObservacao(agendamento.getObservacao());
		return agendamentohistorico;
	}
	
	/**
	 * M�todo para gerar um Agendamentohistorico com os dados de um bean de Agendamento.
	 * 
	 * @param agendamento
	 * @return
	 * @author Fl�vio Tavares
	 */
	public void salvarHistoricoAgendamento(Agendamento agendamento, AgendamentoAcao agendamentoAcao, String observacao){
		AgendamentoHistorico agendamentohistorico = new AgendamentoHistorico();
		agendamentohistorico.setAgendamento(agendamento);
		agendamentohistorico.setAgendamentoAcao(agendamentoAcao);
		agendamentohistorico.setCdusuarioaltera(agendamento.getCdusuarioaltera());
		agendamentohistorico.setDtaltera(agendamento.getDtaltera());
		agendamentohistorico.setObservacao(observacao);
		this.saveOrUpdate(agendamentohistorico);
	}
}
