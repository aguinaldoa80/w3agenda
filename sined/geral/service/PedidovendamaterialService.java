package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PedidoVendaMaterialWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PneuWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendamaterialService extends GenericService<Pedidovendamaterial>{
	
	private PedidovendamaterialDAO pedidovendamaterialDAO;
	private ReservaService reservaService;
	private MaterialService materialService;
	private MaterialrelacionadoService materialrelacionadoService;
	private ProducaoagendamaterialService producaoagendamaterialService;
	private ProducaoagendaService producaoagendaService;
	
	public void setPedidovendamaterialDAO(PedidovendamaterialDAO pedidovendamaterialDAO) {this.pedidovendamaterialDAO = pedidovendamaterialDAO;}
	public void setReservaService(ReservaService reservaService) {this.reservaService = reservaService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {this.materialrelacionadoService = materialrelacionadoService;}
	public void setProducaoagendamaterialService(ProducaoagendamaterialService producaoagendamaterialService) {this.producaoagendamaterialService = producaoagendamaterialService;}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {this.producaoagendaService = producaoagendaService;	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#findByPedidoVenda(Pedidovenda pedidovenda)
	 * 
	 * @param pedidovenda
	 * @return
	 * @author Thiago Augusto
	 */
	public List<Pedidovendamaterial> findByPedidoVenda(Pedidovenda pedidovenda){
		return pedidovendamaterialDAO.findByPedidoVenda(pedidovenda, null, false);
	}
	
	public List<Pedidovendamaterial> findByPedidoVenda(Pedidovenda pedidovenda, Boolean fromCopiar){
		return pedidovendamaterialDAO.findByPedidoVenda(pedidovenda, null, fromCopiar);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#findByPedidoVenda
	 *
	 * @param pedidovenda
	 * @param orderBy
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public List<Pedidovendamaterial> findByPedidoVenda(Pedidovenda pedidovenda, String orderBy){
		return pedidovendamaterialDAO.findByPedidoVenda(pedidovenda, orderBy, false);
	}
	
	public List<Pedidovendamaterial> findByPedidoVenda(Pedidovenda pedidovenda, String orderBy, Boolean fromCopiar){
		return pedidovendamaterialDAO.findByPedidoVenda(pedidovenda, orderBy, fromCopiar);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#getQtdeRestante(Pedidovendamaterial pedidovendamaterial)
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @since 09/11/2011
	 * @author Rodrigo Freitas
	 */
	public Double getQtdeRestante(Pedidovendamaterial pedidovendamaterial, boolean considerarDevolucaoProducaoOtr) {
		return pedidovendamaterialDAO.getQtdeRestante(pedidovendamaterial, considerarDevolucaoProducaoOtr);
	}
	
	public Double getQtdeRestante(Pedidovendamaterial pedidovendamaterial) {
		return pedidovendamaterialDAO.getQtdeRestante(pedidovendamaterial, true);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#findByPedidoVenda(String itens)
	 *
	 * @param itens
	 * @return
	 * @since 14/11/2011
	 * @author Rodrigo Freitas
	 */
	public List<Pedidovendamaterial> findByPedidoVenda(String whereIn) {
		return pedidovendamaterialDAO.findByPedidoVenda(whereIn);
	}
	
	public List<Pedidovendamaterial> findByPedidoVendaWithMaterial(String whereIn) {
		return pedidovendamaterialDAO.findByPedidoVendaWithMaterial(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#findForMontarGrade(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 16/01/2014
	 */
	public List<Pedidovendamaterial> findForMontarGrade(String whereIn) {
		return pedidovendamaterialDAO.findForMontarGrade(whereIn);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#loadComMaterialfornecedor(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Pedidovendamaterial> loadComMaterialfornecedor(Pedidovenda pedidovenda) {
		return pedidovendamaterialDAO.loadComMaterialfornecedor(pedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.PedidovendamaterialService#getQtdeReservada(Material material, Empresa empresa)
	 *
	 * @param material
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 * @param empresa 
	 */
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque){
		return reservaService.getQtdeReservada(material, empresa, localarmazenagem, loteestoque, null);
	}
	
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem){
		return reservaService.getQtdeReservada(material, empresa, localarmazenagem);
	}
	
	public Double getQtdeReservada(Material material, Pedidovenda pedidovenda) {
		String whereInNotReservas = reservaService.montaWhereInReservas(pedidovenda, material);
		return reservaService.getQtdeReservada(material, null, null, null, whereInNotReservas);
	}
	
	public Double getQtdeReservada(Material material, Pedidovenda pedidovenda, Loteestoque loteestoque) {
		String whereInNotReservas = reservaService.montaWhereInReservas(pedidovenda, material);
		return reservaService.getQtdeReservada(material, null, null, loteestoque, whereInNotReservas);
	}
	
	public Double getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque, Pedidovenda pedidovenda) {
		String whereInNotReservas = reservaService.montaWhereInReservas(pedidovenda, material);
		return reservaService.getQtdeReservada(material, empresa, localarmazenagem, loteestoque, whereInNotReservas);
	}
	
	public Double getQtdeReservada(Material material, Pedidovenda pedidovenda, Loteestoque loteestoque, String whereInNotReserva) {
		Material mat = materialService.loadMaterialPromocionalComMaterialrelacionado(material.getCdmaterial());
		if(mat != null && mat.getVendapromocional() != null && mat.getVendapromocional()){
			Integer qtde = null;
			if(SinedUtil.isListNotEmpty(mat.getListaMaterialrelacionado())){
				Localarmazenagem localarmazenagem = null;
				if(pedidovenda != null && pedidovenda.getLocalarmazenagem() != null){
					localarmazenagem = pedidovenda.getLocalarmazenagem();
				}
				try{
					materialrelacionadoService.calculaQuantidadeKit(mat);			
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Double qtdeReservadaItemKit;
				for(Materialrelacionado materialrelacionado : mat.getListaMaterialrelacionado()){
					if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null && materialrelacionado.getQuantidade() > 0 &&
							(materialrelacionado.getMaterialpromocao().getServico() == null || !materialrelacionado.getMaterialpromocao().getServico())){
						String whereInNotReservasPromo = reservaService.montaWhereInReservas(pedidovenda, materialrelacionado.getMaterialpromocao());
						if(Util.strings.isEmpty(whereInNotReserva)){
							whereInNotReserva = whereInNotReservasPromo;
						}else if(Util.strings.isNotEmpty(whereInNotReservasPromo)){
							whereInNotReserva += (","+whereInNotReservasPromo);
						}
						qtdeReservadaItemKit = reservaService.getQtdeReservada(materialrelacionado.getMaterialpromocao(), null, localarmazenagem, loteestoque, whereInNotReserva);
						
						if(qtdeReservadaItemKit == null) qtdeReservadaItemKit = 0d;
						
						qtdeReservadaItemKit = qtdeReservadaItemKit/materialrelacionado.getQuantidade();
						if(qtde == null){
							qtde = qtdeReservadaItemKit.intValue();
						}else if(qtdeReservadaItemKit.intValue() < qtde){
							qtde = qtdeReservadaItemKit.intValue();
						}
					}
				}
			
			}
			return qtde != null ? qtde.doubleValue() : 0d;
		}else {
			return reservaService.getQtdeReservada(material, null, pedidovenda != null ? pedidovenda.getLocalarmazenagem() : null, loteestoque, whereInNotReserva);
		}
	}
	
	public Double getQtdeReservadaVenda(Material material, Pedidovenda pedidovenda, Loteestoque loteestoque){
		return pedidovendamaterialDAO.getQtdeReservada(material, pedidovenda, loteestoque);
	}
	
	public List<Pedidovendamaterial> findByVendaProducao(String whereIn) {
		return findByVendaProducao(whereIn, null);
	}
	
	public List<Pedidovendamaterial> findByVendaProducao(String whereIn, String whereInPedidovendamaterial) {
		return pedidovendamaterialDAO.findByVendaProducao(whereIn, whereInPedidovendamaterial);
	}
	
	public List<Pedidovendamaterial> findForEntradafiscal(String whereIn, String whereInColeta) {
		return pedidovendamaterialDAO.findForEntradafiscal(whereIn, whereInColeta);
	}

	public void updateSaldo(Pedidovendamaterial pvm, Money saldo) {
		pedidovendamaterialDAO.updateSaldo(pvm, saldo);
	}
	
	public Map<Integer, Double> montaMapaQuantidadeDisponivelForProducao(List<ColetaMaterial> listaColetaMaterial){
		Map<Integer, Double> mapa = new HashMap<Integer, Double>();
		if(listaColetaMaterial != null && !listaColetaMaterial.isEmpty()){
			for(ColetaMaterial cm : listaColetaMaterial){
//				if(cm.getPedidovendamaterial() != null && cm.getPedidovendamaterial().getCdpedidovendamaterial() != null){
				Double qtde = 0d;
				if(SinedUtil.isListNotEmpty(cm.getListaColetaMaterialPedidovendamaterial())) {
					for(ColetaMaterialPedidovendamaterial itemServico : cm.getListaColetaMaterialPedidovendamaterial()) {
						if(itemServico.getPedidovendamaterial() != null && itemServico.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							qtde = itemServico.getPedidovendamaterial().getQuantidade() != null ? itemServico.getPedidovendamaterial().getQuantidade() : 0.;
							qtde -= cm.getQuantidadedevolvida() != null ? cm.getQuantidadedevolvida() : 0.;
							if(qtde < 0.){
								qtde = 0.;
							}
							mapa.put(itemServico.getPedidovendamaterial().getCdpedidovendamaterial(), qtde);
						}							
					}
				}
					
//					Double qtde = cm.getPedidovendamaterial().getQuantidade() != null ? cm.getPedidovendamaterial().getQuantidade() : 0.;
//					qtde -= cm.getQuantidadedevolvida() != null ? cm.getQuantidadedevolvida() : 0.;
//					if(qtde < 0.){
//						qtde = 0.;
//					}
//					mapa.put(cm.getPedidovendamaterial().getCdpedidovendamaterial(), qtde);
//				}
			}
		}
		return mapa;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Pedidovendamaterial> findForExistenciaColetaProducao(String whereIn) {
		return pedidovendamaterialDAO.findForExistenciaColetaProducao(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param cotacao
	 * @return
	 * @author Lucas Costa
	 * @since 31/10/2014
	 */
	public List<Pedidovendamaterial> findByCotacao (Cotacao cotacao){
		return pedidovendamaterialDAO.findByCotacao(cotacao);
	}
	
	public List<Pedidovendamaterial> findForConferenciaByPedidovenda(String whereIn) {
		return pedidovendamaterialDAO.findForConferenciaByPedidovenda(whereIn);
	}
	public List<Pedidovendamaterial> findForConferenciaByColeta(String whereIn) {
		return pedidovendamaterialDAO.findForConferenciaByColeta(whereIn);
	}
	
	/**
	 * M�todo que identifica o fornecedor do material no pedido de venda
	 *
	 * @param listaPedidovendamaterial
	 * @param fornecedor
	 * @return
	 * @author Jo�o Vitor
	 */
	public Boolean isFornecedorPedidovendamaterial(List<Pedidovendamaterial> listaPedidovendamaterial, Fornecedor fornecedor){
		Boolean exist = Boolean.FALSE;
		if(SinedUtil.isListNotEmpty(listaPedidovendamaterial)){
			for(Pedidovendamaterial pedidovendamaterial : listaPedidovendamaterial){
				if(pedidovendamaterial.getFornecedor() != null){
					if(pedidovendamaterial.getFornecedor() != null && pedidovendamaterial.getFornecedor().equals(fornecedor)){
						return Boolean.TRUE;
					}
				}
			}
		}
		return exist;
	}	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param pedidovendamaterial
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public void updateQuantidadeWms(Pedidovendamaterial pedidovendamaterial) throws Exception{
		pedidovendamaterialDAO.updateQuantidadeWms(pedidovendamaterial);
	}
	
	public void updateValorcustomaterial(Pedidovendamaterial pedidovendamaterial, Double valorcustomaterial) {
		pedidovendamaterialDAO.updateValorcustomaterial(pedidovendamaterial, valorcustomaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#updateQtdeVolumeWms(Pedidovendamaterial pedidovendamaterial) throws Exception
	*
	* @param pedidovendamaterial
	* @throws Exception
	* @since 19/10/2015
	* @author Luiz Fernando
	*/
	public void updateQtdeVolumeWms(Pedidovendamaterial pedidovendamaterial) throws Exception{
		pedidovendamaterialDAO.updateQtdeVolumeWms(pedidovendamaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 03/11/2015
	 */
	public Pedidovendamaterial loadWithPedidovenda(Pedidovendamaterial pedidovendamaterial) {
		return pedidovendamaterialDAO.loadWithPedidovenda(pedidovendamaterial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#findMaterialproducaoInterrompido(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 08/01/2016
	* @author Luiz Fernando
	*/
	public List<Pedidovendamaterial> findMaterialproducaoInterrompido(Pedidovenda pedidovenda) {
		return pedidovendamaterialDAO.findMaterialproducaoInterrompido(pedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#updateInformacoesImposto(Pedidovendamaterial pedidovendamaterial)
	*
	* @param pedidovendamaterial
	* @since 22/01/2016
	* @author Luiz Fernando
	*/
	public void updateInformacoesImposto(Pedidovendamaterial pedidovendamaterial) {
		pedidovendamaterialDAO.updateInformacoesImposto(pedidovendamaterial);
	}
	
	public List<Pedidovendamaterial> findForGerarProducao(String whereIn) {
		return pedidovendamaterialDAO.findForGerarProducao(whereIn);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.PedidovendamaterialDAO#updateBandaAlterada(String whereIn)
	*
	* @param whereIn
	* @since 21/06/2016
	* @author Luiz Fernando
	*/
	public void updateBandaAlterada(String whereIn) {
		pedidovendamaterialDAO.updateBandaAlterada(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 10/04/2017
	 */
	public void updateServicoAlterado(String whereIn) {
		pedidovendamaterialDAO.updateServicoAlterado(whereIn);
	}
	
	/**
	 * Atualiza o material do item do pedido de venda
	 *
	 * @param pedidovendamaterial
	 * @param materialChaofabrica
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void atualizaMaterial(Pedidovendamaterial pedidovendamaterial, Material materialChaofabrica) {
		this.updateMaterial(pedidovendamaterial, materialChaofabrica);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param pedidovendamaterial
	 * @param material
	 * @author Rodrigo Freitas
	 * @since 07/04/2017
	 */
	public void updateMaterial(Pedidovendamaterial pedidovendamaterial, Material material) {
		pedidovendamaterialDAO.updateMaterial(pedidovendamaterial, material);
	}
	
	public void setPercentualDesconto(Pedidovendamaterial item) {
		if(item != null && item.getQuantidade() != null && item.getQuantidade() > 0 && item.getPreco() != null && item.getPreco() > 0){
			Double multiplicador = item.getMultiplicador() != null ? item.getMultiplicador() : 1d; 
			if(item.getDesconto() != null && item.getDesconto().getValue().doubleValue() > 0 && multiplicador > 0){
				Double valorbruto = item.getQuantidade() * item.getPreco() * multiplicador;
				item.setPercentualdesconto(item.getDesconto().getValue().doubleValue() * 100 / valorbruto);
			}
		}
		
	}
	
	public Pedidovendamaterial findByReformapneu(Pedidovenda pedidovenda, Material servico, Pneu pneu, Pedidovendamaterial pedidovendamaterial){
		return pedidovendamaterialDAO.findByReformapneu(pedidovenda, servico, pneu, pedidovendamaterial);
	}
	
	public void limpaGarantiareformaitem(Pedidovendamaterial pedidovendamaterial){
		pedidovendamaterialDAO.limpaGarantiareformaitem(pedidovendamaterial);
	}
	
	public void updateDesconto(Pedidovendamaterial pedidovendamaterial, Money desconto, Double percentualdesconto) {
		pedidovendamaterialDAO.updateDesconto(pedidovendamaterial, desconto, percentualdesconto);
	}
	
	public void updateGarantiareformaitemAndDescontogarantiareforma(Pedidovendamaterial pedidovendamaterial, Garantiareformaitem garantiareformaitem, Money descontogarantiareforma) {
		pedidovendamaterialDAO.updateGarantiareformaitemAndDescontogarantiareforma(pedidovendamaterial, garantiareformaitem, descontogarantiareforma);
	}
	
	public Double getQtdeVendasrelacionadas(Pedidovendamaterial pedidovendamaterial, Vendamaterial vendamaterialIgnorar) {
		return pedidovendamaterialDAO.getQtdeVendasrelacionadas(pedidovendamaterial, vendamaterialIgnorar);
	}
	public List<Pedidovendamaterial> findByPedidovenda(Pedidovenda pedidovenda) {
		return pedidovendamaterialDAO.findByPedidoVenda(pedidovenda, null, Boolean.FALSE);
	}
	
	public Pedidovendamaterial loadWithMaterialMestre(Pedidovendamaterial pedidovendamaterial){
		return pedidovendamaterialDAO.loadWithMaterialMestre(pedidovendamaterial);
	}
	
	public void updateFaixaMarkup(Pedidovendamaterial vm, FaixaMarkupNome faixaMarkup) {
		pedidovendamaterialDAO.updateFaixaMarkup(vm, faixaMarkup);
	}
	
	public List<PedidoVendaMaterialWSBean> toWSList(List<Pedidovendamaterial> listaPedidovendamaterial){
		List<PedidoVendaMaterialWSBean> lista = new ArrayList<PedidoVendaMaterialWSBean>();
		if(SinedUtil.isListNotEmpty(listaPedidovendamaterial)){
			for(Pedidovendamaterial pvm: listaPedidovendamaterial){
				PedidoVendaMaterialWSBean bean = new PedidoVendaMaterialWSBean();
				bean.setAliquotareaisipi(pvm.getAliquotareaisipi());
				bean.setAltura(pvm.getAltura());
				bean.setBandaalterada(pvm.getBandaalterada());
				bean.setCdpedidovendamaterial(pvm.getCdpedidovendamaterial());
				bean.setComissionamento(ObjectUtils.translateEntityToGenericBean(pvm.getComissionamento()));
				bean.setComprimento(pvm.getComprimento());
				bean.setComprimentooriginal(pvm.getComprimentooriginal());
				bean.setCustooperacional(pvm.getCustooperacional());
				bean.setDesconto(pvm.getDesconto());
				bean.setDescontogarantiareforma(pvm.getDescontogarantiareforma());
				bean.setDtprazoentrega(pvm.getDtprazoentrega());
				bean.setFaixaMarkupNome(ObjectUtils.translateEntityToGenericBean(pvm.getFaixaMarkupNome()));
				bean.setFatorconversao(pvm.getFatorconversao());
				bean.setFornecedor(ObjectUtils.translateEntityToGenericBean(pvm.getFornecedor()));
				bean.setGarantiareformaitem(ObjectUtils.translateEntityToGenericBean(pvm.getGarantiareformaitem()));
				bean.setGrupotributacao(ObjectUtils.translateEntityToGenericBean(pvm.getGrupotributacao()));
				bean.setIdentificadorespecifico(pvm.getIdentificadorespecifico());
				bean.setIdentificadorinterno(pvm.getIdentificadorinterno());
				bean.setIdentificadorintegracao(pvm.getIdentificadorintegracao());
				bean.setIpi(pvm.getIpi());
				bean.setLargura(pvm.getLargura());
				bean.setLoteestoque(ObjectUtils.translateEntityToGenericBean(pvm.getLoteestoque()));
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(pvm.getMaterial()));
				bean.setMaterialFaixaMarkup(ObjectUtils.translateEntityToGenericBean(pvm.getMaterialFaixaMarkup()));
				bean.setMaterialmestre(ObjectUtils.translateEntityToGenericBean(pvm.getMaterialmestre()));
				bean.setMultiplicador(pvm.getMultiplicador());
				bean.setObservacao(pvm.getObservacao());
				bean.setOrdem(pvm.getOrdem());
				bean.setPedidovenda(ObjectUtils.translateEntityToGenericBean(pvm.getPedidovenda()));
				bean.setPedidovendamaterialgarantido(ObjectUtils.translateEntityToGenericBean(pvm.getPedidovendamaterialgarantido()));
				bean.setPercentualcomissaoagencia(pvm.getPercentualcomissaoagencia());
				bean.setPercentualdesconto(pvm.getPercentualdesconto());
				bean.setPercentualrepresentacao(pvm.getPercentualrepresentacao());
				bean.setPeso(pvm.getPeso());
				bean.setPesomedio(pvm.getPesomedio());
				bean.setPreco(pvm.getPreco());
				bean.setProducaosemestoque(pvm.getProducaosemestoque());
				bean.setQtdereferencia(pvm.getQtdereferencia());
				bean.setQtdevolumeswms(pvm.getQtdevolumeswms());
				bean.setQuantidade(pvm.getQuantidade());
				bean.setSaldo(pvm.getSaldo());
				bean.setServicoalterado(pvm.getServicoalterado());
				bean.setTipocalculoipi(EnumUtils.translateEnum(pvm.getTipocalculoipi()));
				bean.setTipocobrancaipi(EnumUtils.translateEnum(pvm.getTipocobrancaipi()));
				bean.setUnidademedida(ObjectUtils.translateEntityToGenericBean(pvm.getUnidademedida()));
				bean.setValorcoleta(pvm.getValorcoleta());
				bean.setValorcustomaterial(pvm.getValorcustomaterial());
				bean.setValordescontosemvalecompra(pvm.getValordescontosemvalecompra());
				bean.setValorimposto(pvm.getValorimposto());
				bean.setValoripi(pvm.getValoripi());
				bean.setValorMinimo(pvm.getValorMinimo());
				bean.setValorvalecomprapropocional(pvm.getValorvalecomprapropocional());
				bean.setValorvendamaterial(pvm.getValorvendamaterial());
				
				if(pvm.getPneu() != null){
					bean.setPneu(new PneuWSBean(pvm.getPneu()));
				}
				
				lista.add(bean);
			}
		}
		return lista;
	}
	
	public void setaProducaoGerada(Pedidovendamaterial pvm, Pedidovenda pedidoVenda){
		if(Util.objects.isNotPersistent(pvm)){
			return;
		}
		boolean isItemSemProducao = producaoagendamaterialService.existeItemSemProducao(pvm.getCdpedidovendamaterial().toString());
		pvm.setAgendaProducaoGerada(!isItemSemProducao);
		if(Util.objects.isPersistent(pvm.getPneu())){
			boolean isAgendaProducaoGeradaPneu = SinedUtil.isListNotEmpty(producaoagendaService.findByPneu(pvm.getPneu(), pedidoVenda));
			pvm.getPneu().setAgendaProducaoGerada(isAgendaProducaoGeradaPneu);
		}
	}

	public Date findMaiorDataPrazoEntrega(Pedidovenda pedidovenda, Pneu pneu) {
		return pedidovendamaterialDAO.findMaiorDataPrazoEntrega(pedidovenda, pneu);
	}

}
