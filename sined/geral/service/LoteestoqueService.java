package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.auxiliar.LoteestoqueVO;
import br.com.linkcom.sined.geral.dao.LoteestoqueDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.LoteestoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.PosicaoEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RelatorioLoteEstoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.PosicaoEstoqueBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.RelatorioLoteEstoqueBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LoteestoqueService extends GenericService<Loteestoque> {
	
	private LoteestoqueDAO loteestoqueDAO;
	private MaterialService materialService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private LotematerialService lotematerialService;
	private LocalarmazenagemService localarmazenagemService;
	private ReservaService reservaService;
	
	public void setLoteestoqueDAO(LoteestoqueDAO loteestoqueDAO) {this.loteestoqueDAO = loteestoqueDAO;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setLotematerialService(LotematerialService lotematerialService) {this.lotematerialService = lotematerialService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setReservaService(ReservaService reservaService) {this.reservaService = reservaService;}

	/* singleton */
	private static LoteestoqueService instance;
	public static LoteestoqueService getInstance() {
		if(instance == null){
			instance = Neo.getObject(LoteestoqueService.class);
		}
		return instance;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#findForAutocomplete(String q)
	 *
	 * @param q
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Loteestoque> findAutocomplete(String q){
		return loteestoqueDAO.findAutocomplete(q, null, null);
	}
	
	public List<Loteestoque> findAutocompleteVinculoMaterial(String q){
		String cdmaterial = null;
		try {
			cdmaterial = NeoWeb.getRequestContext().getParameter("material");
		} catch (Exception e) {}
		
		List<Loteestoque> lista = null;
		
		String cdfornecedor = null;
		try {
			if(parametrogeralService.getBoolean(Parametrogeral.RECEBIMENTO_LOTE_POR_FORNECEDOR)){
				cdfornecedor = NeoWeb.getRequestContext().getParameter("fornecedor");
				lista = loteestoqueDAO.findAutocomplete(q, null, cdfornecedor);
			}else{
				lista = loteestoqueDAO.findAutocomplete(q, cdmaterial, cdfornecedor);
			}
		} catch (Exception e) {} 
		
		
		
		if(StringUtils.isNotBlank(cdmaterial) && CollectionsUtil.isListNotEmpty(lista)){
			Material materialAux = new Material(Integer.parseInt(cdmaterial));
			for(Loteestoque le: lista){
				le.setValidade(this.getValidadeByMaterialLote(le, materialAux));
			}
		}
		return lista;
	}
	
	public List<Loteestoque> findAutocompleteQtdeDisponivelVinculoMaterial(String q){
		Material material = null;
		try {
			material = new Material(Integer.parseInt(NeoWeb.getRequestContext().getParameter("material")));
		} catch (Exception e) {} 
		
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		} catch (Exception e) {} 
		
		Localarmazenagem localarmazenagem = null;
		try {
			localarmazenagem = new Localarmazenagem(Integer.parseInt(NeoWeb.getRequestContext().getParameter("localarmazenagemorigem")));
		} catch (Exception e) {} 
		
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		return loteestoqueDAO.findQtdeLoteestoqueForAutocomplete(q, material, localarmazenagem, empresa, true, considerarEmpresa);
	}
	
	public List<Loteestoque> findAutocompleteQtdeDisponivelVinculoMaterialRomaneio(String q){
		Material material = null;
		try {
			material = new Material(Integer.parseInt(NeoWeb.getRequestContext().getParameter("material")));
		} catch (Exception e) {} 
		
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		} catch (Exception e) {} 
		
		Localarmazenagem localarmazenagem = null;
		boolean considerarQtdeDisponivel = true;
		try {
			localarmazenagem = localarmazenagemService.load(new Localarmazenagem(Integer.parseInt(NeoWeb.getRequestContext().getParameter("localarmazenagemorigem"))), 
															"localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
			considerarQtdeDisponivel = localarmazenagem == null || localarmazenagem.getPermitirestoquenegativo() == null || !localarmazenagem.getPermitirestoquenegativo();
		} catch (Exception e) {} 
		
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		return loteestoqueDAO.findQtdeLoteestoqueForAutocomplete(q, material, localarmazenagem, empresa, considerarQtdeDisponivel, considerarEmpresa);
	}
	
	/**
	* M�todo autocomplete de lote
	*
	* @param lote
	* @param material
	* @param empresa
	* @param localarmazenagem
	* @return
	* @since 17/04/2017
	* @author Luiz Fernando
	*/
	public List<Loteestoque> findAutocompleteVinculoMaterialComponent(String lote, Material material, Empresa empresa, Localarmazenagem localarmazenagem){
		boolean considerarQtdeDisponivel = true;
		try {
			localarmazenagem = localarmazenagemService.load(localarmazenagem, "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
			considerarQtdeDisponivel = localarmazenagem == null || localarmazenagem.getPermitirestoquenegativo() == null || !localarmazenagem.getPermitirestoquenegativo();
		} catch (Exception e) {} 
		
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		return loteestoqueDAO.findQtdeLoteestoqueForAutocomplete(lote, material, localarmazenagem, empresa, considerarQtdeDisponivel, considerarEmpresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#findAutocompleteDisponiveis(String q, Movimentacaoestoque movimentacaoestoque)
	 *
	 * @param q
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findAutocompleteDisponiveis(String q){
		Material material = getMaterialSessao();
		
		List<Loteestoque> lista = loteestoqueDAO.findAutocompleteDisponiveis(q, material);
		if(lista != null && !lista.isEmpty()){
			for(Loteestoque loteestoque : lista){
				loteestoque.montaDescriptionAutocomplete(LoteestoqueService.getMaterialSessao());
			}
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.LoteestoqueService#findQtdeLoteestoque(Material material, Localarmazenagem localarmazenagem, Empresa empresa)
	 *
	 * @param material
	 * @param localarmazenagem
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Loteestoque> findQtdeLoteestoque(Material material, Localarmazenagem localarmazenagem, Empresa empresa){
		if(material == null || material.getCdmaterial() == null) return null;
		return findQtdeLoteestoque(material.getCdmaterial().toString(), localarmazenagem, empresa, true, null);
	}
	
	public List<Loteestoque> findQtdeLoteestoque(Material material, Localarmazenagem localarmazenagem, Empresa empresa, boolean considerarQtdeDisponivel){
		if(material == null || material.getCdmaterial() == null) return null;
		return findQtdeLoteestoque(material.getCdmaterial().toString(), localarmazenagem, empresa, considerarQtdeDisponivel, null);
	}
	
	public List<Loteestoque> findQtdeLoteestoque(String whereInMaterial, Localarmazenagem localarmazenagem, Empresa empresa, boolean considerarQtdeDisponivel, String whereInLoteestoque){
		Boolean considerarEmpresa = true;
		Boolean considerarQtdeReservada = false;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
			considerarQtdeReservada = parametrogeralService.getBoolean(Parametrogeral.QTDE_DISPONIVEL_CONSIDERAR_RESERVADO);
		} catch (Exception e) {}
		return loteestoqueDAO.findQtdeLoteestoque(whereInMaterial, localarmazenagem, empresa, considerarQtdeDisponivel, considerarEmpresa, whereInLoteestoque, considerarQtdeReservada, false);
	}
	
	public List<Loteestoque> findQtdeLoteestoque(String whereInMaterial, Localarmazenagem localarmazenagem, Empresa empresa, boolean considerarQtdeDisponivel, String whereInLoteestoque, Boolean agruparPorLocalAndEmpresa){
		Boolean considerarEmpresa = true;
		Boolean considerarQtdeReservada = false;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
			considerarQtdeReservada = true;
		} catch (Exception e) {}
		return loteestoqueDAO.findQtdeLoteestoque(whereInMaterial, localarmazenagem, empresa, considerarQtdeDisponivel, considerarEmpresa, whereInLoteestoque, considerarQtdeReservada, agruparPorLocalAndEmpresa);
	}
	
	public List<Loteestoque> findForAutocompleteTrocaLote(String str){
		String cdmaterial = null;
		try {
			cdmaterial = NeoWeb.getRequestContext().getParameter("material");
		} catch (Exception e) {} 
		
		Empresa empresa = null;
		try {
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		} catch (Exception e) {}
		
		Expedicaoitem expedicaoitem = null;
		try {
			expedicaoitem = new Expedicaoitem(Integer.parseInt(NeoWeb.getRequestContext().getParameter("expedicaoitem")));
		} catch (Exception e) {}
		
		Localarmazenagem localarmazenagem = null;
		try {
			localarmazenagem = new Localarmazenagem(Integer.parseInt(NeoWeb.getRequestContext().getParameter("localarmazenagem")));
		} catch (Exception e) {}
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		Unidademedida unidademedida = null;
		try {
			unidademedida = new Unidademedida(Integer.parseInt(NeoWeb.getRequestContext().getParameter("unidademedida")));
		} catch (Exception e) {}
		
		return findForAutocompleteTrocaLote(str, cdmaterial, localarmazenagem, empresa, expedicaoitem, unidademedida, considerarEmpresa);
	}
	
	public List<Loteestoque> findForAutocompleteTrocaLote(String str, String cdmaterial, Localarmazenagem localarmazenagem, Empresa empresa, Expedicaoitem expedicaoitem, Unidademedida unidadeMedida, Boolean considerarEmpresa){
		Expedicaoitem expItem = ExpedicaoitemService.getInstance().load(expedicaoitem);
		Loteestoque loteEstoque = this.load(expItem.getLoteestoque());
		Double qtdeReserva = reservaService.getReservaByVendamaterialOrExpedicaoitem(expItem.getVendamaterial(), expItem, expItem.getMaterial(), loteEstoque);
		List<Loteestoque> lista = loteestoqueDAO.findForAutocompleteTrocaLote(str, cdmaterial, localarmazenagem, empresa, considerarEmpresa);
		for(Loteestoque le: lista){
			if(le.equals(loteEstoque)){
				le.setQtde(le.getQtde()+qtdeReserva);
				break;
			}
		}
		if(Util.objects.isPersistent(unidadeMedida) && SinedUtil.isListNotEmpty(lista)){
			Material material_aux = materialService.unidadeMedidaMaterial(new Material(Integer.parseInt(cdmaterial)));
			if(material_aux != null && Util.objects.isPersistent(material_aux.getUnidademedida()) &&
					!unidadeMedida.equals(material_aux.getUnidademedida())){
				Double fatorconversao = unidademedidaService.getFatorconversao(material_aux.getUnidademedida(), 0d, unidadeMedida, material_aux, null, null);
				if(fatorconversao != null && fatorconversao != 0){
					for(Loteestoque loteestoque: lista){
						if(loteestoque.getQtde() != null){
							loteestoque.setQtde(loteestoque.getQtde()*fatorconversao);
						}
					}
				}
			}
		}
		return lista;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#getQtdeLoteestoque(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Loteestoque loteestoque)
	 *
	 * @param material
	 * @param localarmazenagem
	 * @param empresa
	 * @param loteestoque
	 * @return
	 * @author Luiz Fernando
	 * @param filtroMaterialmestregrade 
	 */
	public Double getQtdeLoteestoque(Material material, Localarmazenagem localarmazenagem, Empresa empresa,	Loteestoque loteestoque, Boolean filtroMaterialmestregrade) {
		return loteestoqueDAO.getQtdeLoteestoque(material, localarmazenagem, empresa, loteestoque, filtroMaterialmestregrade);
	}
	
	public ModelAndView getLoteestoque(WebRequestContext request, Material material){
		return getLoteestoque(request, material, null);
	}
	
	/**
	 * M�todo que retorna json de loteestoque
	 *
	 * @param request
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView getLoteestoque(WebRequestContext request, Material material, List<Loteestoque> listaLoteVinculoMaterial){
		String cdempresa = request.getParameter("cdempresa");
		String cdlocalarmazenagem = request.getParameter("cdlocalarmazenagem");
		String cdunidademedida = request.getParameter("cdunidademedida");
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		Unidademedida unidademedida = null;
		try {
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
			}
			if(cdunidademedida != null && !"".equals(cdunidademedida)){
				unidademedida = new Unidademedida(Integer.parseInt(cdunidademedida));
			}
		} catch (NumberFormatException e) {}
		
		return this.getLoteestoque(request, material, listaLoteVinculoMaterial, empresa, unidademedida, localarmazenagem);
	}
	
	public ModelAndView getLoteestoque(WebRequestContext request, Material material, List<Loteestoque> listaLoteVinculoMaterial, Empresa empresa, Unidademedida unidadeMedida, Localarmazenagem localArmazenagem){
		List<Loteestoque> listaLoteestoque = getLoteestoque(request, material, empresa, localArmazenagem);
		verificaUnidademedida(unidadeMedida, material, listaLoteestoque);
		setQtdeDisponivel(listaLoteVinculoMaterial, listaLoteestoque);
		
		return new JsonModelAndView()
					.addObject("loteestoque", createLoteestoqueVO(SinedUtil.isListNotEmpty(listaLoteVinculoMaterial) ? listaLoteVinculoMaterial : listaLoteestoque))
					.addObject("index", request.getParameter("index") != null ? request.getParameter("index") : "");
	}
	
	public List<Loteestoque> findComboVinculoMaterial(WebRequestContext request, List<Loteestoque> listaLoteVinculoMaterial, Material material, Empresa empresa, Localarmazenagem localarmazenagem, Unidademedida unidademedida){
		if(SinedUtil.isListNotEmpty(listaLoteVinculoMaterial)){
			List<Loteestoque> listaLoteestoque = getLoteestoque(request, material, empresa, localarmazenagem);
			verificaUnidademedida(unidademedida, material, listaLoteestoque);
			setQtdeDisponivel(listaLoteVinculoMaterial, listaLoteestoque);
		}
		return listaLoteVinculoMaterial;
	}
	
	public void setQtdeDisponivel(List<Loteestoque> listaLoteVinculoMaterial, List<Loteestoque> listaLoteestoque) {
		if(SinedUtil.isListNotEmpty(listaLoteVinculoMaterial) && SinedUtil.isListNotEmpty(listaLoteestoque)){
			for(Loteestoque loteestoque : listaLoteVinculoMaterial){
				for(Loteestoque loteView : listaLoteestoque){
					if(loteView.getCdloteestoque() != null && loteView.getCdloteestoque().equals(loteestoque.getCdloteestoque()) &&
							loteView.getCdmaterial() != null && loteView.getCdmaterial().equals(loteestoque.getCdmaterial()) &&
							loteView.getNumero() != null && loteView.getNumero().equals(loteestoque.getNumero()) &&
							((loteView.getValidade() == null && loteestoque.getValidade() == null) || 
							  loteView.getValidade() != null && loteestoque.getValidade() != null && SinedDateUtils.equalsIgnoreHour(loteView.getValidade(), loteestoque.getValidade()))){
						loteestoque.setQtde(loteView.getQtde());
					}
				}
			}
		}
	}
	
	public void verificaUnidademedida(Unidademedida unidademedida, Material material, List<Loteestoque> listaLoteestoque) {
		if(unidademedida != null && unidademedida.getCdunidademedida() != null && material != null && SinedUtil.isListNotEmpty(listaLoteestoque)){
			Material material_aux = materialService.unidadeMedidaMaterial(material);
			if(material_aux != null && material_aux.getUnidademedida() != null && material_aux.getUnidademedida().getCdunidademedida() != null &&
					!unidademedida.equals(material_aux.getUnidademedida())){
				Double fatorconversao = unidademedidaService.getFatorconversao(material_aux.getUnidademedida(), 0d, unidademedida, material, null, null);
				if(fatorconversao != null && fatorconversao != 0){
					for(Loteestoque loteestoque: listaLoteestoque){
						if(loteestoque.getQtde() != null){
							loteestoque.setQtde(loteestoque.getQtde()*fatorconversao);
						}
					}
				}
			}
		}
		
	}
	public List<Loteestoque> getLoteestoque(WebRequestContext request, Material material, Empresa empresa, Localarmazenagem localarmazenagem){
		return getLoteestoque(request, material, empresa, localarmazenagem, null);
	}
	
	/**
	* M�todo que retorna uma lista de loteestoque
	*
	* @param request
	* @param material
	* @param empresa
	* @param localarmazenagem
	* @return
	* @since 15/05/2015
	* @author Luiz Fernando
	*/
	public List<Loteestoque> getLoteestoque(WebRequestContext request, Material material, Empresa empresa, Localarmazenagem localarmazenagem, String whereInLote){
		List<Loteestoque> loteestoque = new ArrayList<Loteestoque>();
		
		boolean considerarQtdeDisponivel = true;
		if(request.getAttribute("naoConsiderarQtde") != null){
			considerarQtdeDisponivel = !(Boolean) request.getAttribute("naoConsiderarQtde");
		}
		
		boolean existeitemgrade = false;
		if(material != null && material.getCdmaterial() != null){
			material = materialService.loadWithMaterialMestre(material);
			List<Material> listaMaterialitemgrade = materialService.findMaterialitemByMaterialmestregrade(material);
			existeitemgrade = listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty() && !materialService.isControleMaterialgrademestre(material);
			
			if("true".equals(request.getAttribute("origemVenda")) && existeitemgrade){
				String whereInMaterial = CollectionsUtil.listAndConcatenate(listaMaterialitemgrade, "cdmaterial", ",");
				List<Loteestoque> loteestoqueItem = findQtdeLoteestoque(whereInMaterial, localarmazenagem, empresa, considerarQtdeDisponivel, null);
				
				if(loteestoqueItem != null && !loteestoqueItem.isEmpty()){
					for(Loteestoque loteestoqueItemGrade : loteestoqueItem){
						if(!loteestoque.contains(loteestoqueItemGrade)){
							Loteestoque beanLoteestoque = new Loteestoque(loteestoqueItemGrade.getCdloteestoque(), loteestoqueItemGrade.getNumero());
							beanLoteestoque.setNaoExibirQtde(true);
							loteestoque.add(beanLoteestoque);
						}
					}
				}
			}
		}
		
		if(loteestoque == null || loteestoque.isEmpty()){
			loteestoque = this.findQtdeLoteestoque(material, localarmazenagem, empresa, considerarQtdeDisponivel);
		}
		return loteestoque;
	}
	
	public List<LoteestoqueVO> createLoteestoqueVO(List<Loteestoque> listaLoteestoque) {
		List<LoteestoqueVO> lista = new ArrayList<LoteestoqueVO>();
		if(SinedUtil.isListNotEmpty(listaLoteestoque)){
			for(Loteestoque loteestoque : listaLoteestoque){
				lista.add(new LoteestoqueVO(loteestoque.getCdloteestoque(), loteestoque.getNumerovalidade(), loteestoque.getNumerovalidadeqtde()));
			}
		}
		return lista;
	}
	
	public List<Loteestoque> getLoteestoque(List<Loteestoque> listaLoteestoque, Material material) {
		if(listaLoteestoque == null || listaLoteestoque.isEmpty() || material == null || material.getCdmaterial() == null)
			return null;
		
		List<Loteestoque> lista = new ArrayList<Loteestoque>();
		for(Loteestoque loteestoque : listaLoteestoque){
			if(loteestoque.getCdmaterial() != null && loteestoque.getCdmaterial().equals(material.getCdmaterial()))
				lista.add(loteestoque);
		}
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#carregaLoteestoque(Loteestoque loteestoque)
	 *
	 * @param loteestoque
	 * @return
	 * @author Luiz Fernando
	 */
	public Loteestoque carregaLoteestoque(Loteestoque loteestoque){
		return loteestoqueDAO.carregaLoteestoque(loteestoque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param numero
	 * @param fornecedor
	 * @return
	 * @author Rafael Salvio
	 */
	public Loteestoque findByNumeroAndFornecedor(String numero, Fornecedor fornecedor){
		return loteestoqueDAO.findByNumeroAndFornecedor(numero, fornecedor);
	}
	
	public Loteestoque findByNumeroAndFornecedorForImportacaoXml(String numero, Fornecedor fornecedor){
		return loteestoqueDAO.findByNumeroAndFornecedorForImportacaoXml(numero, fornecedor);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findByMaterial(Material material){
		return loteestoqueDAO.findByMaterial(material);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompleteVendaOrcamento(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "VENDAORCAMENTO", true);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompletePedidovenda(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "PEDIDOVENDA", true);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompleteVenda(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "VENDA", true);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompleteProducaoagenda(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "PRODUCAOAGENDA", null);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompleteProducaoordem(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "PRODUCAOORDEM", null);
	}
	
	public List<Loteestoque> findQtdeLoteestoqueForAutocompleteExpedicao(String lote){
		return findQtdeLoteestoqueForAutocomplete(lote, "EXPEDICAO", true);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 *
	 * @param lote
	 * @param material
	 * @param localarmazenagem
	 * @param empresa
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findQtdeLoteestoqueForAutocomplete(String lote, String tela, Boolean considerarQtdeDisponivel){
		WebRequestContext request = NeoWeb.getRequestContext();
		
		Material material = null;
		Object materialObj = request.getSession().getAttribute(tela + "_MATERIAL_LOTE_AUTOCOMPLETE");
		if(materialObj != null){
			material = (Material)materialObj;
		}else{
			return new ArrayList<Loteestoque>();
//			throw new SinedException("Material inv�lido ou nulo para consulta de lotes.");
		}
		
		Localarmazenagem localarmazenagem = null;
		Object localarmazenagemObj = request.getSession().getAttribute(tela + "_LOCALARMAZENAGEM_LOTE_AUTOCOMPLETE");
		if(localarmazenagemObj != null){
			localarmazenagem = (Localarmazenagem)localarmazenagemObj;
		}
		
		Empresa empresa = null;
		Object empresaObj = request.getSession().getAttribute(tela + "_EMPRESA_LOTE_AUTOCOMPLETE");
		if(empresaObj != null){
			empresa = (Empresa)empresaObj;
		}
		
		Unidademedida unidademedida = null;
		Object unidademedidaObj = request.getSession().getAttribute(tela + "_UNIDADEMEDIDA_LOTE_AUTOCOMPLETE");
		if(unidademedidaObj != null){
			unidademedida = (Unidademedida)unidademedidaObj;
		}
		
		List<Loteestoque> listaLoteestoque = this.findQtdeLoteestoqueForAutocomplete(lote, considerarQtdeDisponivel, material, localarmazenagem, empresa, unidademedida);
		
		for(Loteestoque le: listaLoteestoque){
			le.setValidade(this.getValidadeByMaterialLote(le, material));
		}
		
		return listaLoteestoque;
	}
	public List<Loteestoque> findQtdeLoteestoqueForAutocomplete(String lote, Boolean considerarQtdeDisponivel, Material material, Localarmazenagem localArmazenagem, Empresa empresa, Unidademedida unidadeMedida){
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {
			
		}
		List<Loteestoque> listaLoteestoque = loteestoqueDAO.findQtdeLoteestoqueForAutocomplete(lote, material, localArmazenagem, empresa, considerarQtdeDisponivel != null && considerarQtdeDisponivel, considerarEmpresa);
		if(unidadeMedida != null && unidadeMedida.getCdunidademedida() != null && SinedUtil.isListNotEmpty(listaLoteestoque)){
			Material material_aux = materialService.unidadeMedidaMaterial(material);
			if(material_aux != null && material_aux.getUnidademedida() != null && material_aux.getUnidademedida().getCdunidademedida() != null &&
					!unidadeMedida.equals(material_aux.getUnidademedida())){
				Double fatorconversao = unidademedidaService.getFatorconversao(material_aux.getUnidademedida(), 0d, unidadeMedida, material, null, null);
				if(fatorconversao != null && fatorconversao != 0){
					for(Loteestoque loteestoque: listaLoteestoque){
						if(loteestoque.getQtde() != null){
							loteestoque.setQtde(loteestoque.getQtde()*fatorconversao);
						}
					}
				}
			}
		}
		
		return listaLoteestoque;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param whereInCnpj
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Loteestoque> findByWhereInNumeroAndFornecedor(String whereIn, String whereInCnpj){		
		return loteestoqueDAO.findByWhereInNumeroAndFornecedor(whereIn, whereInCnpj);
	}
	
	public List<Loteestoque> findWithLotematerial(String whereIn){
		return loteestoqueDAO.findWithLotematerial(whereIn);
	}
	
	public static Material getMaterialSessao(){
		Material material = null;
		Object obj = NeoWeb.getRequestContext().getSession().getAttribute("MATERIAL_LOTE");
		
		if(obj != null){
			try {
				material = (Material)obj;
			} catch (Exception e) {}
			
		}
		
		return material;
	}
	/**
	* M�todo que verifica se o material est� cadastrado no lote, caso contr�rio, inclui o material no lote
	*
	* @param loteestoque
	* @param material
	* @since 23/09/2016
	* @author Luiz Fernando
	*/
	public void verificarEIncluirMaterialNoLote(Loteestoque loteestoque, Material material) {
		if(loteestoque != null && loteestoque.getCdloteestoque() != null && material != null && material.getCdmaterial() != null && !lotematerialService.existeMaterialNoLote(material, loteestoque)){
			Lotematerial lotematerial = new Lotematerial();
			lotematerial.setLoteestoque(new Loteestoque(loteestoque.getCdloteestoque()));
			lotematerial.setMaterial(new Material(material.getCdmaterial()));
			lotematerialService.saveOrUpdate(lotematerial);
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#findLoteByMaterial(String whereInMaterial, String numero)
	*
	* @param whereInMaterial
	* @return
	* @since 28/12/2016
	* @author Luiz Fernando
	*/
	public List<Loteestoque> findLoteByMaterial(String whereInMaterial){
		return loteestoqueDAO.findLoteByMaterial(whereInMaterial, null, false);
	}
	
	/**
	* M�todo que retorna os lotes vinculados ao material
	*
	* @param material
	* @return
	* @since 28/12/2016
	* @author Luiz Fernando
	*/
	public List<Loteestoque> findLoteByMaterial(Material material){
		if(material == null || material.getCdmaterial() == null)
			return null;
		return findLoteByMaterial(material.getCdmaterial().toString());
	}
	
	public List<Loteestoque> findAutocompleteVinculoLoteMaterialCompra(String q){
		WebRequestContext request = NeoWeb.getRequestContext();
		request.setAttribute("naoConsiderarQtde", Boolean.TRUE);
		return findAutocompleteVinculoLoteMaterial(q);
	}
	
	public List<Loteestoque> findAutocompleteVinculoLoteMaterial(String q){
		WebRequestContext request = NeoWeb.getRequestContext();
		String cdmaterial = request.getParameter("material");
		String cdempresa = request.getParameter("empresa");
		String cdlocalarmazenagem = request.getParameter("localarmazenagem");
		String cdunidademedida = request.getParameter("unidademedida");
		Material material = null;
		Empresa empresa = null;
		Localarmazenagem localarmazenagem = null;
		Unidademedida unidademedida = null;
		try {
			if(cdmaterial != null && !"".equals(cdmaterial)){
				material = new Material(Integer.parseInt(cdmaterial));
			}
			if(cdempresa != null && !"".equals(cdempresa)){
				empresa = new Empresa(Integer.parseInt(cdempresa));
			}
			if(cdlocalarmazenagem != null && !"".equals(cdlocalarmazenagem)){
				localarmazenagem = new Localarmazenagem(Integer.parseInt(cdlocalarmazenagem));
			}
			if(cdunidademedida != null && !"".equals(cdunidademedida)){
				unidademedida = new Unidademedida(Integer.parseInt(cdunidademedida));
			}
		} catch (NumberFormatException e) {}
		
		List<Loteestoque> listaLoteVinculoMaterial = new ArrayList<Loteestoque>();
		if(material != null && material.getCdmaterial() != null){
			listaLoteVinculoMaterial = loteestoqueDAO.findLoteByMaterial(material.getCdmaterial().toString(), q, true);
			if(SinedUtil.isListNotEmpty(listaLoteVinculoMaterial)){
				List<Loteestoque> listaLoteestoque = getLoteestoque(request, material, empresa, localarmazenagem, CollectionsUtil.listAndConcatenate(listaLoteVinculoMaterial, "cdloteestoque", ","));
				if(unidademedida != null && unidademedida.getCdunidademedida() != null && SinedUtil.isListNotEmpty(listaLoteestoque)){
					Material material_aux = materialService.unidadeMedidaMaterial(material);
					if(material_aux != null && material_aux.getUnidademedida() != null && material_aux.getUnidademedida().getCdunidademedida() != null &&
							!unidademedida.equals(material_aux.getUnidademedida())){
						Double fatorconversao = unidademedidaService.getFatorconversao(material_aux.getUnidademedida(), 0d, unidademedida, material, null, null);
						if(fatorconversao != null && fatorconversao != 0){
							for(Loteestoque loteestoque: listaLoteestoque){
								if(loteestoque.getQtde() != null){
									loteestoque.setQtde(loteestoque.getQtde()*fatorconversao);
								}
							}
						}
					}
				}
				if(SinedUtil.isListNotEmpty(listaLoteVinculoMaterial) && SinedUtil.isListNotEmpty(listaLoteestoque)){
					for(Loteestoque loteestoque : listaLoteVinculoMaterial){
						for(Loteestoque loteView : listaLoteestoque){
							if(loteView.getCdloteestoque() != null && loteView.getCdloteestoque().equals(loteestoque.getCdloteestoque()) &&
									loteView.getCdmaterial() != null && loteView.getCdmaterial().equals(loteestoque.getCdmaterial()) &&
									loteView.getNumero() != null && loteView.getNumero().equals(loteestoque.getNumero()) &&
									((loteView.getValidade() == null && loteestoque.getValidade() == null) || 
									  loteView.getValidade() != null && loteestoque.getValidade() != null && SinedDateUtils.equalsIgnoreHour(loteView.getValidade(), loteestoque.getValidade()))){
								loteestoque.setQtde(loteView.getQtde());
							}
						}
					}
				}
			}
		}
		return listaLoteVinculoMaterial;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.LoteestoqueDAO#loadWithNumeroValidade(Loteestoque loteestoque, Material material)
	*
	* @param loteestoque
	* @param material
	* @return
	* @since 17/04/2017
	* @author Luiz Fernando
	*/
	public Loteestoque loadWithNumeroValidade(Loteestoque loteestoque, Material material) {
		return loteestoqueDAO.loadWithNumeroValidade(loteestoque, material, true);
	}
	
	public Loteestoque loadWithNumeroValidade(Loteestoque loteestoque, Material material, boolean menorValidade) {
		return loteestoqueDAO.loadWithNumeroValidade(loteestoque, material, false);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.LoteestoqueService#getLoteestoque(WebRequestContext request, Material material, Empresa empresa, Localarmazenagem localarmazenagem)
	*
	* @param request
	* @param material
	* @param empresa
	* @param localarmazenagem
	* @return
	* @since 17/04/2017
	* @author Luiz Fernando
	*/
	public List<Loteestoque> findTreeView(WebRequestContext request, Material material, Empresa empresa, Localarmazenagem localarmazenagem) {
		return getLoteestoque(request, material, empresa, localarmazenagem);
	}
	
	public List<Loteestoque> findLoteestoqueForCsvReport(LoteestoqueFiltro filtro) {
		return loteestoqueDAO.findLoteestoqueForCsvReport(filtro);
	}
	
	public List<Loteestoque> carregarComLoteMaterial(List<Loteestoque> lista) {
		if(lista != null && !lista.isEmpty()) {
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdloteestoque", ",");
			List<Loteestoque> listaComMaterial = findWithLotematerial(whereIn);
			
			if(SinedUtil.isListNotEmpty(lista)) {
				for(Loteestoque comMaterial : listaComMaterial) {
					for(Loteestoque lote : lista) {
						if(comMaterial.equals(lote)) {
							lote.setListaLotematerial(comMaterial.getListaLotematerial());
						}
					}
				}
			}
		}
		
		return lista;
	}
	
	public Resource generateCsvReport(LoteestoqueFiltro filtro, String nomeArquivo) {
		List<Loteestoque> lista = carregarComLoteMaterial(findLoteestoqueForCsvReport(filtro));
		
		StringBuilder csv = new StringBuilder("\"N�mero\";\"Fornecedor\";\"Materiais\";\"Fabrica��es\";\"Validades\"\n");
		String [] fields = {"numero", "fornecedor", "materialStr", "fabricacaoStr", "validadeStr"};
		
		for(Loteestoque bean : lista) {
			beanToCSV(fields, csv, bean);
		}
		
		Resource resource = new Resource("text/csv", nomeArquivo, csv.toString().replace("<br>", "\n").getBytes());
		return resource;
	}
	
	public Loteestoque findWithLotematerial(Loteestoque loteestoque){
		return loteestoqueDAO.findWithLotematerial(loteestoque);
	}
	public Date buscarDtVencimentoLote(Integer cdmaterial, Integer cdpessoa, Integer cdlocalarmazenagem) {
		return loteestoqueDAO.buscarDtVencimentoLote(cdmaterial, cdpessoa, cdlocalarmazenagem);
	}
	public List<Loteestoque> buscarLotesMaterialDisponivel(Integer cdmaterial, Integer cdlocalarmazenagem, Integer cdpessoa) {
		return loteestoqueDAO.buscarLotesMaterialDisponivel(cdmaterial, cdlocalarmazenagem, cdpessoa);
	}
	public Double buscarQtdeDisponivelMaterialLotes(Integer cdmaterial, Integer cdlocalarmazenagem, Integer cdpessoa) {
		return this.buscarQtdeDisponivelMaterialLotes(cdmaterial, cdlocalarmazenagem, cdpessoa, null);
		
	}
	public Double buscarQtdeDisponivelMaterialLotes(Integer cdmaterial, Integer cdlocalarmazenagem, Integer cdpessoa, Integer cdLoteEstoque) {
		return loteestoqueDAO.buscarQtdeDisponivelMaterialLotes(cdmaterial, cdlocalarmazenagem, cdpessoa, cdLoteEstoque);
	}
	
	public List<Loteestoque> findByMaterialAndFornecedor(Material material, Fornecedor fornecedor){
		return loteestoqueDAO.findByMaterialAndFornecedor(material, fornecedor);
	}
	public ModelAndView ajaxBuscaLotes(Entregamaterial bean){
		ModelAndView jsonModelAndView = new JsonModelAndView();
		List<Loteestoque> lista = this.findByMaterialAndFornecedor(bean.getMaterial(), null);
		
		boolean obrigarLote = false;
		if(Util.objects.isPersistent(bean.getMaterial())){
			obrigarLote = materialService.obrigarLote(bean.getMaterial());
		}
		return jsonModelAndView.addObject("lista", lista).addObject("obrigarLote", obrigarLote);
	}
	public List<RelatorioLoteEstoqueBean> findForRelatorioEstoque(RelatorioLoteEstoqueFiltro filtro) {
		return loteestoqueDAO.findForRelatorioEstoque(filtro);
	}
	public Resource gerarRelatorioLoteEstoqueCsv(List<RelatorioLoteEstoqueBean> listaRelatorioEstoque, RelatorioLoteEstoqueFiltro filtro) {
		StringBuilder csv = new StringBuilder();
		Double totalQtde = 0D;
		Double totalQtdeReservada = 0D;
		Double totalQtdeDisponivel = 0D;
		List<Loteestoque> listaLotes = new ArrayList<Loteestoque>(); 
		csv.append("C�d. Produto.;Identificador;Produto;Unidade de Medida;C�d. Lote;N�mero Lote;Validade Lote;Fornecedor;Tipo de Material;Local de Armazenagem;Qtde Atual;Qtde Reservada;Qtde Dispon�vel; Empresa; Nome; Fabricante\n");
		
		for (RelatorioLoteEstoqueBean relatorioLoteEstoqueBean : listaRelatorioEstoque) {
			csv
			.append(relatorioLoteEstoqueBean.getCdmaterial() + ";");
			if(relatorioLoteEstoqueBean.getIdentificadorMaterial() != null){
				csv.append(relatorioLoteEstoqueBean.getIdentificadorMaterial() + ";");				
			}else {
				csv.append(";");
			}
			
			csv
			.append(relatorioLoteEstoqueBean.getNomeMaterial() + ";")
			.append(relatorioLoteEstoqueBean.getUnidadeMedida() +";")
			.append(relatorioLoteEstoqueBean.getCdlote() + ";");
			if(relatorioLoteEstoqueBean.getNumeroLote()!= null){
				csv.append(relatorioLoteEstoqueBean.getNumeroLote() + ";");				
			}else {
				csv.append(";");
			}			
			if(relatorioLoteEstoqueBean.getDtValidade() != null){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String dataValidade = sdf.format(relatorioLoteEstoqueBean.getDtValidade());
				csv.append(dataValidade + ";");			
			}else {
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getFornecedorLote() != null){
				csv.append(relatorioLoteEstoqueBean.getFornecedorLote() + ";");			
			}else {
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getMaterialTipo() != null){
				csv.append(relatorioLoteEstoqueBean.getMaterialTipo() + ";");			
			}else {
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getLocalarmazenagem() != null){
				csv.append(relatorioLoteEstoqueBean.getLocalarmazenagem() + ";");			
			}else {
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getQtde() != null){
				csv.append(relatorioLoteEstoqueBean.getQtde() + ";");			
			}else {
				csv.append(";");
			}
			
			if(relatorioLoteEstoqueBean.getQtdeReservada()  != null){
				csv.append(relatorioLoteEstoqueBean.getQtdeReservada()  + ";");			
			}else {
				csv.append(";");
			}
			
			if(relatorioLoteEstoqueBean.getQtdeDisponivel()  != null){
				csv.append(relatorioLoteEstoqueBean.getQtdeDisponivel()  + ";");			
			}else {
				csv.append(";");
			}
			
			if(relatorioLoteEstoqueBean.getEmpresa()  != null){
				csv.append(relatorioLoteEstoqueBean.getEmpresa()  + ";");			
			}else {
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getFabricanteMaterial()  != null){
				csv.append(relatorioLoteEstoqueBean.getFabricanteMaterial()  + ";");			
			}else {
				csv.append(";");
			}
			
			
			//totalQtde += BigDecimal.valueOf(DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtde())).doubleValue();
			//totalQtde += BigDecimal.valueOf(DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtde())).doubleValue();
			totalQtde += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtde());
			totalQtdeReservada += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtdeReservada());
			totalQtdeDisponivel += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtdeDisponivel());
			
			Loteestoque lote = new Loteestoque(relatorioLoteEstoqueBean.getCdlote());
			if(!listaLotes.contains(lote)){
				listaLotes.add(lote);
			}
			
			csv.append("\n\n");
		}
		
		csv.append("Total Qtde;Total Qtd Reservada;Total Qtde Dispon�vel;Total de lotes;\n");
		csv.append(totalQtde).append(";").append(totalQtdeReservada).append(";").append(totalQtdeDisponivel).append(";").append(listaLotes.size()).append(";");
		
		Resource resource = new Resource("text/csv", "relatorio_lote_estoque_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public Resource gerarRelatorioPosicaoEstoqueCsv(List<PosicaoEstoqueBean> listaRelatorioEstoque, PosicaoEstoqueFiltro filtro) {
		StringBuilder csv = new StringBuilder();
		csv.append("C�d. Produto;Identificador;Produto;Tipo de Material;Local;Fabricante;C�digo do fabricante;C�digo de barras;Quantidade Atual;Quantidade Reservada;Quantidade Dispon�vel;Unidade de Medida;Categoria;Valor Unit�rio de custo;Valor total de custo\n");
		Double totalQtde = 0D;
		Double totalQtdeReservada = 0D;
		Double totalQtdeDisponivel = 0D;
		Double totalCusto = 0D;
		for (PosicaoEstoqueBean relatorioLoteEstoqueBean : listaRelatorioEstoque) {
			csv
			.append(relatorioLoteEstoqueBean.getCdmaterial() + ";");
			if(relatorioLoteEstoqueBean.getIdentificadorMaterial() != null){
				csv.append(relatorioLoteEstoqueBean.getIdentificadorMaterial() + ";");				
			}else {
				csv.append(";");
			}
			
			csv
			.append(relatorioLoteEstoqueBean.getNomeMaterial() + ";")
			.append(relatorioLoteEstoqueBean.getMaterialTipo()).append(";")
			.append(relatorioLoteEstoqueBean.getLocalarmazenagem()).append(";")
			.append(relatorioLoteEstoqueBean.getFabricanteMaterial()).append(";")
			.append(relatorioLoteEstoqueBean.getCodigoFabricante()).append(";")
			.append(relatorioLoteEstoqueBean.getCodigoBarra()).append(";");
			if(relatorioLoteEstoqueBean.getQtde() != null){
				csv.append(relatorioLoteEstoqueBean.getQtde()).append(";");
			}else{
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getQtdeReservada() != null){
				csv.append(relatorioLoteEstoqueBean.getQtdeReservada()).append(";");
			}else{
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getQtdeDisponivel() != null){
				csv.append(relatorioLoteEstoqueBean.getQtdeDisponivel()).append(";");
			}else{
				csv.append(";");
			}
			csv.append(relatorioLoteEstoqueBean.getUnidadeMedida()).append(";")
			.append(relatorioLoteEstoqueBean.getMaterialCategoria()).append(";");
			if(relatorioLoteEstoqueBean.getCustoUnitario() != null){
				csv.append(relatorioLoteEstoqueBean.getCustoUnitario()).append(";");
			}else{
				csv.append(";");
			}
			if(relatorioLoteEstoqueBean.getCustoTotal() != null){
				csv.append(relatorioLoteEstoqueBean.getCustoTotal()).append(";");
			}else{
				csv.append(";");
			}
			csv.append("\n");
			
			totalQtde += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtde());
			totalQtdeReservada += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtdeReservada());
			totalQtdeDisponivel += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getQtdeDisponivel());
			totalCusto += DoubleUtils.zeroWhenNull(relatorioLoteEstoqueBean.getCustoTotal());
		}
		csv.append("\n\n");
		csv.append("Total Qtde;Total Qtd Reservada;Total Qtde Dispon�vel;Total Custo\n");
		
		csv.append(totalQtde).append(";").append(totalQtdeReservada).append(";").append(totalQtdeDisponivel).append(";").append(totalCusto).append(";");
		
		Resource resource = new Resource("text/csv", "relatorio_lote_estoque_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public List<PosicaoEstoqueBean> findForRelatorioPosicaoEstoque(PosicaoEstoqueFiltro filtro) {
		return loteestoqueDAO.findForRelatorioPosicaoEstoque(filtro);
	}
	
	public String getValidadeMaterialLote(Loteestoque loteestoque, Material material) {
		if(loteestoque != null && material != null && SinedUtil.isListNotEmptyAndInitialized(loteestoque.getListaLotematerial())){
			for(Lotematerial lotematerial : loteestoque.getListaLotematerial()){
				if(lotematerial.getValidade() != null && Util.objects.isPersistent(lotematerial.getMaterial()) && material.equals(lotematerial.getMaterial())){
					return SinedDateUtils.toString(lotematerial.getValidade());
				}
			}
		}
		return null;
	}
	
	public Date getValidadeByMaterialLote(Loteestoque loteestoque, Material material) {
		if(loteestoque != null && material != null){
			List<Lotematerial> lista = loteestoque.getListaLotematerial();
			if(!SinedUtil.isListNotEmptyAndInitialized(lista)){
				lista = lotematerialService.findAllByLoteestoque(loteestoque);
			}
			if(SinedUtil.isListNotEmptyAndInitialized(lista)){
				for(Lotematerial lotematerial : lista){
					if(lotematerial.getValidade() != null && Util.objects.isPersistent(lotematerial.getMaterial()) && material.equals(lotematerial.getMaterial())){
						return lotematerial.getValidade();
					}
				}
			}
		}
		return null;
	}
}
