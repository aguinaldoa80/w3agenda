package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Coleta.TipoColeta;
import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.ColetaMaterialPedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.dao.ColetaMaterialDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColetaMaterialService extends GenericService<ColetaMaterial> {
	
	private ColetaMaterialDAO coletaMaterialDAO;
	private ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService;
	
	public void setColetaMaterialDAO(ColetaMaterialDAO coletaMaterialDAO) {
		this.coletaMaterialDAO = coletaMaterialDAO;
	}
	public void setColetaMaterialPedidovendamaterialService(ColetaMaterialPedidovendamaterialService coletaMaterialPedidovendamaterialService) {
		this.coletaMaterialPedidovendamaterialService = coletaMaterialPedidovendamaterialService;
	}

	/**
	 * M�todo com refer�ncia no DAO.
 	 * 
	 * @param coletaMaterial
	 * @param qtde
	 * @author Rafael Salvio
	 */
	public void adicionaQtdeDevolvida(ColetaMaterial coletaMaterial, Double qtde) {
		coletaMaterialDAO.adicionaQtdeDevolvida(coletaMaterial, qtde);
	}
	
	public void updateQtdeDevolvidaNota(ColetaMaterial coletaMaterial, Double qtde) {
		coletaMaterialDAO.updateQtdeDevolvidaNota(coletaMaterial, qtde);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coletaMaterial
	 * @param qtde
	 * @author Rodrigo Freitas
	 * @since 20/08/2014
	 */
	public void adicionaQtdeComprada(ColetaMaterial coletaMaterial, Double qtde) {
		coletaMaterialDAO.adicionaQtdeComprada(coletaMaterial, qtde);
	}
	
	public List<ColetaMaterial> findByPedidovendaForProducao(String whereInPV) throws Exception{
		return findByPedidovendaForProducao(whereInPV, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereInPV
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<ColetaMaterial> findByPedidovendaForProducao(String whereInPV, String whereInPVM) throws Exception{
		return coletaMaterialDAO.findByPedidovendaForProducao(whereInPV, whereInPVM);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coletaMaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public ColetaMaterial loadForDevolucao(ColetaMaterial coletaMaterial) {
		return coletaMaterialDAO.loadForDevolucao(coletaMaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param pedidovendamaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 25/07/2014
	 */
	public Boolean haveColetaMaterialByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		return coletaMaterialDAO.haveColetaMaterialByPedidovendamaterial(pedidovendamaterial);
	}
	
	public Boolean haveColetaMaterialByPedidovendamaterialOtr(Pedidovendamaterial pedidovendamaterial) {
		return coletaMaterialDAO.haveColetaMaterialByPedidovendamaterialOtr(pedidovendamaterial);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInColeta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<ColetaMaterial> findByColeta(String whereInColeta) {
		return coletaMaterialDAO.findByColeta(whereInColeta);
	}
	
	/**
	 * Cria o bean de coleta para a pop-up de devolu��o e compra de cliente 
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/08/2014
	 */
	public Coleta makeListaColetaMaterialForCompra(String whereIn) {
		List<ColetaMaterial> listaColetaMaterial = this.findByColeta(whereIn);
		List<ColetaMaterial> listaColetaMaterialNova = new ArrayList<ColetaMaterial>();
		Money valorCustoMaterial = new Money();
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		List<Contagerencial> listaContagerencialcompra = new ArrayList<Contagerencial>();
		for (ColetaMaterial coletaMaterial : listaColetaMaterial) {
			if(coletaMaterial.getColeta() != null && 
					coletaMaterial.getColeta().getTipo() != null &&
					coletaMaterial.getColeta().getTipo().equals(TipoColeta.FORNECEDOR)){
				throw new SinedException("N�o � poss�vel realizar a compra de cliente de uma coleta com fornecedor.");
			}
			
			if(coletaMaterial.getQuantidadedevolvida() == null){
				coletaMaterial.setQuantidadedevolvida(0d);
			}
			
			Double quantidadeColetada = coletaMaterial.getQuantidade();
			Double quantidadeVenda = 0d;
			Double quantidadeDevolvida = 0d;
			Double quantidadeComprada = 0d;
			
			if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
				for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
					Pedidovendamaterial pedidovendamaterial = itemServico.getPedidovendamaterial();
					if(pedidovendamaterial != null){
						Set<Vendamaterial> listaVendamaterial = pedidovendamaterial.getListaVendamaterial();
						if(listaVendamaterial != null && listaVendamaterial.size() > 0){
							for (Vendamaterial vendamaterial : listaVendamaterial) {
								if(vendamaterial != null && 
										vendamaterial.getVenda() != null && 
										vendamaterial.getVenda().getVendasituacao() != null &&
										!vendamaterial.getVenda().getVendasituacao().equals(Vendasituacao.CANCELADA)){
									quantidadeVenda += vendamaterial.getQuantidade();
								}
							}
						}
					}				
				}
			}
//			Pedidovendamaterial pedidovendamaterial = coletaMaterial.getPedidovendamaterial();
//			if(pedidovendamaterial != null){
//				Set<Vendamaterial> listaVendamaterial = pedidovendamaterial.getListaVendamaterial();
//				if(listaVendamaterial != null && listaVendamaterial.size() > 0){
//					for (Vendamaterial vendamaterial : listaVendamaterial) {
//						if(vendamaterial != null && 
//								vendamaterial.getVenda() != null && 
//								vendamaterial.getVenda().getVendasituacao() != null &&
//								!vendamaterial.getVenda().getVendasituacao().equals(Vendasituacao.CANCELADA)){
//							quantidadeVenda += vendamaterial.getQuantidade();
//						}
//					}
//				}
//			}
			
			if(coletaMaterial.getQuantidadedevolvida() != null){
				quantidadeDevolvida = coletaMaterial.getQuantidadedevolvida();
			}
			if(coletaMaterial.getQuantidadecomprada() != null){
				quantidadeComprada = coletaMaterial.getQuantidadecomprada();
			}
			
			Double quantidadeRestante = quantidadeColetada - quantidadeDevolvida - quantidadeComprada - quantidadeVenda;
			if(quantidadeRestante > 0){
				coletaMaterial.setQuantidaderestante(quantidadeRestante);
				if(coletaMaterial.getValorunitario() != null){
					valorCustoMaterial = valorCustoMaterial.add(new Money(coletaMaterial.getValorunitario() * quantidadeRestante));
				}else if(coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().getValorcusto() != null){
					valorCustoMaterial = valorCustoMaterial.add(new Money(coletaMaterial.getMaterial().getValorcusto() * quantidadeRestante));
				}
				listaColetaMaterialNova.add(coletaMaterial);
				
				if(coletaMaterial.getColeta() != null && coletaMaterial.getColeta().getCliente() != null && !listaCliente.contains(coletaMaterial.getColeta().getCliente())){
					listaCliente.add(coletaMaterial.getColeta().getCliente());
				}
				if(coletaMaterial.getMaterial() != null && coletaMaterial.getMaterial().getContagerencial() != null && !listaContagerencialcompra.contains(coletaMaterial.getMaterial().getContagerencial())){
					listaContagerencialcompra.add(coletaMaterial.getMaterial().getContagerencial());
				}
				if(coletaMaterial.getColeta() != null && coletaMaterial.getColeta().getEmpresa() != null && !listaEmpresa.contains(coletaMaterial.getColeta().getEmpresa())){
					listaEmpresa.add(coletaMaterial.getColeta().getEmpresa());
				}
			}
		}
		
		if(listaColetaMaterialNova.size() == 0){
			throw new SinedException("Nenhum item a ser comprado do cliente.");
		}
		
		Coleta coleta = new Coleta();
		coleta.setListaColetaMaterial(listaColetaMaterialNova);
		coleta.setWhereIn(whereIn);
		coleta.setValor(valorCustoMaterial);
		coleta.setCliente(listaCliente.size() == 1 ? listaCliente.get(0) : null);
		coleta.setEmpresa(listaEmpresa.size() == 1 ? listaEmpresa.get(0) : null);
		coleta.setContagerencial(listaContagerencialcompra.size() == 1 ? listaContagerencialcompra.get(0) : null);
		return coleta;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.ColetaMaterialDAO#findByPedidovendamaterial(String whereInPedidovendamaterial)
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public List<ColetaMaterial> findByPedidovendamaterial(String whereInPedidovendamaterial) {
		return coletaMaterialDAO.findByPedidovendamaterial(whereInPedidovendamaterial);
	}
	
	/**
	* M�todo que verifica se existe item do pedido de venda sem registro de coleta
	*
	* @see br.com.linkcom.sined.geral.service.ColetaMaterialService#findByPedidovendamaterial(String whereInPedidovendamaterial)
	*
	* @param whereInPedidovendamaterial
	* @return
	* @since 17/06/2015
	* @author Luiz Fernando
	*/
	public boolean existeItemSemColeta(List<Pedidovendamaterial> listaPedidoVendaMaterial) {
		boolean itemSemColeta = false;
		if(SinedUtil.isListNotEmpty(listaPedidoVendaMaterial)){
			String whereInPedidovendamaterial = CollectionsUtil.listAndConcatenate(listaPedidoVendaMaterial, "cdpedidovendamaterial", ",");
			List<ColetaMaterial> listaColetaMaterial = findByPedidovendamaterial(whereInPedidovendamaterial);
			if(SinedUtil.isListNotEmpty(listaColetaMaterial)){
				for(Pedidovendamaterial pvm: listaPedidoVendaMaterial){
					boolean achou = false;
					for(ColetaMaterial coletaMaterial : listaColetaMaterial){
						if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial())) {
							if(Util.objects.isPersistent(pvm.getPneu()) && Util.objects.isPersistent(pvm.getMaterial()) &&
								pvm.getPneu().equals(coletaMaterial.getPneu()) &&
								pvm.getMaterialcoleta().equals(coletaMaterial.getMaterial())){
								achou = true;
								break;
							}
							
							/*for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
								if(Util.objects.isPersistent(itemServico.getPedidovendamaterial()) &&
										id.equals(itemServico.getPedidovendamaterial().getCdpedidovendamaterial().toString()) ||
										(itemServico.getPedidovendamaterial().getPneu().equals(coletaMaterial.getPneu()) &&
										itemServico.getPedidovendamaterial().getMaterial().equals(coletaMaterial.getMaterial()))){
									achou = true;
									break;
								}
							}
							if(achou){
								break;
							}*/
						}
//						if(coletaMaterial.getPedidovendamaterial() != null && 
//								coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial() != null &&
//								id.equals(coletaMaterial.getPedidovendamaterial().getCdpedidovendamaterial().toString())){
//							achou = true;
//							break;
//						}
					}
					if(!achou){
						itemSemColeta = true;
						break;
					}
				}
			}else {
				itemSemColeta = true;
			}
		}
		return itemSemColeta;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ColetaMaterialDAO#adicionaMotivodevolucao(ColetaMaterial coletaMaterial, Motivodevolucao motivodevolucao)
	*
	* @param coletaMaterial
	* @param motivodevolucao
	* @since 08/07/2015
	* @author Luiz Fernando
	*/
//	public void adicionaMotivodevolucao(ColetaMaterial coletaMaterial, Motivodevolucao motivodevolucao) {
//		coletaMaterialDAO.adicionaMotivodevolucao(coletaMaterial, motivodevolucao);
//	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ColetaMaterialDAO#updateQtdeDevolvidaAposEstornoDevolucao(ColetaMaterial coletaMaterial, Double quantidadeestorno)
	*
	* @param coletaMaterial
	* @param quantidadeestorno
	* @since 15/10/2015
	* @author Luiz Fernando
	*/
	public void updateQtdeDevolvidaAposEstornoDevolucao(ColetaMaterial coletaMaterial, Double quantidadeestorno) {
		coletaMaterialDAO.updateQtdeDevolvidaAposEstornoDevolucao(coletaMaterial, quantidadeestorno);
		
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param coletaMaterial
	* @param listaProducaoordem
	* @return
	* @since 16/10/2015
	* @author Luiz Fernando
	*/
	public String getDescricaoEstornoDevolucao(ColetaMaterial coletaMaterial, List<Producaoordem> listaProducaoordem) {
		String observacao = coletaMaterial.getMaterial().getNome();
		if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial()) && SinedUtil.isListNotEmpty(listaProducaoordem)){
			for(Producaoordem producaoordem : listaProducaoordem){
				if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
					for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
						for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
							if(producaoordemmaterial.getPedidovendamaterial() != null && 
									itemServico.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
								return observacao + SinedUtil.makeLinkHistorico(producaoordem.getCdproducaoordem().toString(), "("+producaoordem.getCdproducaoordem().toString()+")", "visualizarProducaoordem");
							}
						}
//						if(producaoordemmaterial.getPedidovendamaterial() != null && 
//								coletaMaterial.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
//							return observacao + SinedUtil.makeLinkHistorico(producaoordem.getCdproducaoordem().toString(), "("+producaoordem.getCdproducaoordem().toString()+")", "visualizarProducaoordem");
//						}
					}
				}
			}
		}
		return observacao;
	}

	/**
	* M�todo que retorna a Ordem de produ��o do item da coleta
	*
	* @param coletaMaterial
	* @param listaProducaoordem
	* @return
	* @since 16/10/2015
	* @author Luiz Fernando
	*/
	public Producaoordem getProducaoordemByColetaMaterial(ColetaMaterial coletaMaterial, List<Producaoordem> listaProducaoordem) {
		if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial()) && SinedUtil.isListNotEmpty(listaProducaoordem)){
			for(Producaoordem producaoordem : listaProducaoordem){
				if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
					for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
						for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
							if(producaoordemmaterial.getPedidovendamaterial() != null && 
									itemServico.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
								return producaoordem;
							}					
						}
//						if(producaoordemmaterial.getPedidovendamaterial() != null && 
//								coletaMaterial.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
//							return producaoordem;
//						}
					}
				}
			}
		}
		return null;
	}
	/**
	* M�todo que retorna o item da Ordem de produ��o do item da coleta
	*
	* @param coletaMaterial
	* @param listaProducaoordem
	* @return
	* @since 29/10/2015
	* @author Luiz Fernando
	*/
	public Producaoordemmaterial getProducaoordemmaterialByColetaMaterial(ColetaMaterial coletaMaterial, List<Producaoordem> listaProducaoordem) {
		if(SinedUtil.isListNotEmpty(coletaMaterial.getListaColetaMaterialPedidovendamaterial()) && SinedUtil.isListNotEmpty(listaProducaoordem)){
			for(Producaoordem producaoordem : listaProducaoordem){
				if(SinedUtil.isListNotEmpty(producaoordem.getListaProducaoordemmaterial())){
					for(Producaoordemmaterial producaoordemmaterial : producaoordem.getListaProducaoordemmaterial()){
						for(ColetaMaterialPedidovendamaterial itemServico : coletaMaterial.getListaColetaMaterialPedidovendamaterial()) {
							if(producaoordemmaterial.getPedidovendamaterial() != null && 
									itemServico.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
								return producaoordemmaterial;
							}					
						}
//						if(producaoordemmaterial.getPedidovendamaterial() != null && 
//								coletaMaterial.getPedidovendamaterial().equals(producaoordemmaterial.getPedidovendamaterial())){
//							return producaoordemmaterial;
//						}
					}
				}
			}
		}
		return null;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * @param cdPedidoMaterial
	 * @return
	 * @since 03/02/2026
	 * @author C�sar
	 */
	public List<ColetaMaterial> findByIensInterropidos (String WhereIn) {
		return coletaMaterialDAO.findByIensInterropidos (WhereIn);
	}

	public  ColetaMaterial findForExpedicao(Integer cdColetaMaterial) {
		return coletaMaterialDAO.findForExpedicao(cdColetaMaterial);
	}

	public ColetaMaterial loadColetaByPneuAndMaterialcoleta(Pedidovenda pedidoVenda, Pneu pneu, Material materialColeta){
		return coletaMaterialDAO.loadColetaByPneuAndMaterialcoleta(pedidoVenda, pneu, materialColeta);
	}
	
	public boolean isTodosServicosRecusados(ColetaMaterial coletaMaterial){
		return SinedUtil.isListEmpty(coletaMaterialPedidovendamaterialService.findNaoRecusados(coletaMaterial));
	}
	
	public void updateValorUnitario(ColetaMaterial coletaMaterial, Double valorunitario) {
		coletaMaterialDAO.updateValorUnitario(coletaMaterial, valorunitario);
	}
}
