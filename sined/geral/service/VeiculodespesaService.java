package br.com.linkcom.sined.geral.service;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.sined.geral.bean.Tipokmajuste;
import br.com.linkcom.sined.geral.bean.Veiculodespesa;
import br.com.linkcom.sined.geral.bean.Veiculodespesaitem;
import br.com.linkcom.sined.geral.bean.Veiculodespesatipo;
import br.com.linkcom.sined.geral.bean.Veiculokm;
import br.com.linkcom.sined.geral.dao.VeiculodespesaDAO;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculodespesaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VeiculodespesaService extends GenericService<Veiculodespesa> {

	private VeiculokmService veiculokmService;
	private VeiculodespesaDAO veiculodespesaDAO;
	
	public void setVeiculokmService(VeiculokmService veiculokmService) {
		this.veiculokmService = veiculokmService;
	}
	public void setVeiculodespesaDAO(VeiculodespesaDAO veiculodespesaDAO) {
		this.veiculodespesaDAO = veiculodespesaDAO;
	}
	
	@Override
	public void saveOrUpdate(final Veiculodespesa bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				boolean isFirstTime = false;
				if(bean.getCdveiculodespesa() == null)
					isFirstTime = true;
				
				saveOrUpdateNoUseTransaction(bean);
				
				if(isFirstTime && bean.getKm() != null)
					veiculokmService.saveOrUpdateNoUseTransaction(new Veiculokm(bean.getVeiculo(), new Date(), bean.getKm(), Tipokmajuste.OUTRO));
				
				return status;
			}
		});
	}
	
	/**
	 * M�todo que gera relat�rio padr�o da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(VeiculodespesaFiltro filtro) {
		Report report = new Report("/veiculo/veiculodespesa");
		
		List<Veiculodespesa> veiculos = this.findForReport(filtro);
		if(veiculos != null && veiculos.size() > 0){
			StringBuilder whereIn = new StringBuilder();
			for (Veiculodespesa vd : veiculos) {
				if (vd.getVeiculodespesatipo().getCdveiculodespesatipo().equals(Veiculodespesatipo.ABASTECIMENTO.getCdveiculodespesatipo())){
					whereIn.append(vd.getCdveiculodespesa()).append(",");
				}
			}
			
			if(whereIn.length() > 0){
				List<Veiculodespesa> listaVeiculos = this.findForCalculoKmPorLitro(whereIn.substring(0, whereIn.length()-1));
				if(SinedUtil.isListNotEmpty(listaVeiculos)){
					for(Veiculodespesa veiculodespesa : listaVeiculos){
						if(veiculodespesa.getCdveiculodespesa() != null && veiculodespesa.getKmAnterior() != null && veiculodespesa.getKmAnterior() >= 0){
							for (Veiculodespesa vdListagem : veiculos) {
								if (veiculodespesa.getCdveiculodespesa().equals(vdListagem.getCdveiculodespesa())){
									vdListagem.setKmAnterior(veiculodespesa.getKmAnterior());
								}
							}
						}
					}
				}
			}
			
			for (Veiculodespesa vd : veiculos) {
				if (vd.getVeiculodespesatipo().getCdveiculodespesatipo().equals(Veiculodespesatipo.ABASTECIMENTO.getCdveiculodespesatipo()) && vd.getKmAnterior() != null && vd.getKmAnterior() >= 0){
					double quantidade = 0.0;
					for (Veiculodespesaitem vdi : vd.getListaveiculodespesaitem()) {
						if (vdi.getQuantidade() != null)
							quantidade += vdi.getQuantidade();
					}
					if (quantidade > 0 && vd.getKm() != null)
						vd.setKmLitro(SinedUtil.truncate((vd.getKm() - vd.getKmAnterior()) / quantidade));
				}
			}
			report.setDataSource(veiculos);
		}
		return report;
	}
	
	public List<Veiculodespesa> findForCalculoKmPorLitro(String whereIn) {
		return veiculodespesaDAO.findForCalculoKmPorLitro(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param idsVeiculosDespesa
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Veiculodespesa> findForFaturar(String idsVeiculosDespesa) {
		return veiculodespesaDAO.findForFaturar(idsVeiculosDespesa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void updateFaturado(String whereIn) {
		veiculodespesaDAO.updateFaturado(whereIn);
	}

	public void updateBaixado(String whereIn) {
		veiculodespesaDAO.updateBaixado(whereIn);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findForReport
	 * @param filtro
	 * @return
	 * @return List<Veiculodespesa>
	 * @author Thiago Augusto
	 * @date 11/07/2012
	 *
	 */
	public List<Veiculodespesa> findForReport(VeiculodespesaFiltro filtro){
		return veiculodespesaDAO.findForReport(filtro);
	}
}