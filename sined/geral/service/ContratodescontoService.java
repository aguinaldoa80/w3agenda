package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratodesconto;
import br.com.linkcom.sined.geral.bean.enumeration.NotaFiscalTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipodesconto;
import br.com.linkcom.sined.geral.dao.ContratodescontoDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratodescontoService extends GenericService<Contratodesconto>{

	private ContratodescontoDAO contratodescontoDAO;
	
	public void setContratodescontoDAO(ContratodescontoDAO contratodescontoDAO) {
		this.contratodescontoDAO = contratodescontoDAO;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratodescontoDAO#findByContrato(Contrato contrato)
	 *
	 * @param contrato
	 * @return
	 * @since 06/08/2012
	 * @author Rodrigo Freitas
	 */
	public List<Contratodesconto> findByContrato(Contrato contrato) {
		return contratodescontoDAO.findByContrato(contrato);
	}

	public Map<NotaFiscalTipoEnum, Money> getDescontosContrato(Date dtproximovencimento, List<Contratodesconto> listaContratodesconto) {
		
		Map<NotaFiscalTipoEnum, Money> mapDescontos = new HashMap<NotaFiscalTipoEnum, Money>();
		
		if (listaContratodesconto != null && listaContratodesconto.size() > 0){
			for (Contratodesconto cd : listaContratodesconto) {
				if(cd.getTipodesconto() != null && 
						cd.getTiponota() != null &&
						cd.getTipodesconto().equals(Tipodesconto.NF_SERVICO) && 
						SinedDateUtils.afterOrEqualsIgnoreHour(dtproximovencimento, cd.getDtinicio()) &&
						(cd.getDtfim() == null || SinedDateUtils.beforeOrEqualIgnoreHour(dtproximovencimento, cd.getDtfim()))){
					Money valor = new Money(cd.getValor());
					if(mapDescontos.containsKey(cd.getTiponota())){
						valor = valor.add(mapDescontos.get(cd.getTiponota()));
					}
					mapDescontos.put(cd.getTiponota(), valor);
				}
			}
		}
		
		return mapDescontos;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratodescontoDAO#findByContratoComissinamento(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 21/12/2012
	 */
	public List<Contratodesconto> findByContratoComissinamento(String whereIn) {
		return contratodescontoDAO.findByContratoComissinamento(whereIn);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param contrato
	 * @param data
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/02/2014
	 */
	public boolean haveDescontoContratoData(Contrato contrato, Date data) {
		return contratodescontoDAO.haveDescontoContratoData(contrato, data);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContratodescontoDAO#findForCobranca(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 07/04/2014
	 */
	public List<Contratodesconto> findForCobranca(String whereIn) {
		return contratodescontoDAO.findForCobranca(whereIn);
	}
	
}
