package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.bean.Ecommaterialidentificador;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.dao.EcommaterialidentificadorDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EcommaterialidentificadorService extends GenericService<Ecommaterialidentificador>{
	
	private EcommaterialidentificadorDAO ecommaterialidentificadorDAO;
	
	public void setEcommaterialidentificadorDAO(
			EcommaterialidentificadorDAO ecommaterialidentificadorDAO) {
		this.ecommaterialidentificadorDAO = ecommaterialidentificadorDAO;
	}
	
	/* singleton */
	private static EcommaterialidentificadorService instance;
	public static EcommaterialidentificadorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(EcommaterialidentificadorService.class);
		}
		return instance;
	}
	
	/**
	 * Salva ou atualiza o identificador a partir do material.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EcommaterialidentificadorService#findByMaterial(Material material)
	 *
	 * @param material
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void saveOrUpdateIdentificador(Material material, String identificador, Configuracaoecom configuracaoecom) {
		Ecommaterialidentificador ecommaterialidentificador = this.findByMaterial(material, configuracaoecom);
		
		if(ecommaterialidentificador == null) ecommaterialidentificador = new Ecommaterialidentificador();
		
		ecommaterialidentificador.setMaterial(material);
		ecommaterialidentificador.setIdentificador(identificador);
		ecommaterialidentificador.setConfiguracaoecom(configuracaoecom);
		this.saveOrUpdate(ecommaterialidentificador);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialidentificadorDAO#findByMaterial(Material material)
	 *
	 * @param material
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public Ecommaterialidentificador findByMaterial(Material material, Configuracaoecom configuracaoecom) {
		return ecommaterialidentificadorDAO.findByMaterial(material, configuracaoecom);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialidentificadorDAO#deleteByIdentificador(String identificador)
	 *
	 * @param identificador
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public void deleteByIdentificador(String identificador, Configuracaoecom configuracaoecom) {
		ecommaterialidentificadorDAO.deleteByIdentificador(identificador, configuracaoecom);
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EcommaterialidentificadorDAO#findByIdentificador(String identificador)
	 *
	 * @param identificador
	 * @return
	 * @author Rodrigo Freitas
	 * @param configuracaoecom 
	 * @since 18/07/2013
	 */
	public Ecommaterialidentificador findByIdentificador(String identificador, Configuracaoecom configuracaoecom) {
		return ecommaterialidentificadorDAO.findByIdentificador(identificador, configuracaoecom);
	}
	
}
