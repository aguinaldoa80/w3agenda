package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.templateengine.GroovyTemplateEngine;
import br.com.linkcom.geradorrelatorio.templateengine.ITemplateEngine;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Agendainteracao;
import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialrequisicao;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaFiscalServico;
import br.com.linkcom.sined.geral.bean.NotaFiscalServicoItem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.bean.Requisicaohistorico;
import br.com.linkcom.sined.geral.bean.Requisicaoitem;
import br.com.linkcom.sined.geral.bean.Requisicaonota;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.dao.RequisicaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DescricaoDeServicoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemServicoCamposAdicionaisBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemServicoItemBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.RequisicaoFiltro;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;

public class RequisicaoService extends br.com.linkcom.sined.util.neo.persistence.GenericService<Requisicao> {

	private RequisicaoDAO requisicaoDAO;
	private RequisicaohistoricoService requisicaohistoricoService;
	private EmpresaService empresaService;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private EnvioemailService envioemailService;
	private ParametrogeralService parametrogeralService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private NotaHistoricoService notaHistoricoService;
	private VendaService vendaService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private UsuarioService usuarioService;
	private ColaboradorService colaboradorService;
	private MaterialrequisicaoService materialrequisicaoService;
	private ContratoService contratoService;
	private MaterialService materialService;
	private RequisicaonotaService requisicaonotaService;
	private GrupotributacaoService grupotributacaoService;
	private ConfiguracaonfeService configuracaonfeService;
	private AvisoService avisoService;
	private DepartamentoService departamentoService;
	private DocumentoService documentoService;
	private ClienteService clienteService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	public void setConfiguracaonfeService(ConfiguracaonfeService configuracaonfeService) {this.configuracaonfeService = configuracaonfeService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {this.grupotributacaoService = grupotributacaoService;}
	public void setRequisicaonotaService(RequisicaonotaService requisicaonotaService) {this.requisicaonotaService = requisicaonotaService;}
	public void setEmpresaService(EmpresaService empresaService) { this.empresaService = empresaService; }
	public void setRequisicaoDAO(RequisicaoDAO requisicaoDAO) { this.requisicaoDAO = requisicaoDAO;	}
	public void setRequisicaohistoricoService(RequisicaohistoricoService requisicaohistoricoService) { this.requisicaohistoricoService = requisicaohistoricoService;	}
	public void setEnderecoService(EnderecoService enderecoService) { this.enderecoService = enderecoService; }
	public void setTelefoneService(TelefoneService telefoneService) { this.telefoneService = telefoneService; }
	public void setEnvioemailService(EnvioemailService envioemailService) { this.envioemailService = envioemailService;	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) { this.parametrogeralService = parametrogeralService;	}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) { this.notaFiscalServicoService = notaFiscalServicoService;	}
	public void setNotaHistoricoService(NotaHistoricoService notaHistoricoService) { this.notaHistoricoService = notaHistoricoService; }
	public void setVendaService(VendaService vendaService) { this.vendaService = vendaService;	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) { this.movimentacaoestoqueService = movimentacaoestoqueService;	}
	public void setUsuarioService(UsuarioService usuarioService) { this.usuarioService = usuarioService; }
	public void setColaboradorService(ColaboradorService colaboradorService) { this.colaboradorService = colaboradorService; }
	public void setMaterialrequisicaoService(MaterialrequisicaoService materialrequisicaoService) { this.materialrequisicaoService = materialrequisicaoService;	}
	public void setContratoService(ContratoService contratoService) { this.contratoService = contratoService;	}
	public void setMaterialService(MaterialService materialService) { this.materialService = materialService;	}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setDepartamentoService(DepartamentoService departamentoService) {this.departamentoService = departamentoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setClienteService(ClienteService clienteService) {this.clienteService = clienteService;}
	public void setMovimentacaoEstoqueHistoricoService(MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;}
	/* singleton */
	private static RequisicaoService instance;
	public static RequisicaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RequisicaoService.class);
		}
		return instance;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * M�todo para carregar as requisi��es pelos PK's passados como par�metro
	 * 
	 * @author Jo�o Paulo Zica
	 * @param whereIn
	 * @return
	 */
	public List<Requisicao> carregaRequisicao(String whereIn){
		return requisicaoDAO.carregaRequisicao(whereIn);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * M�todo para carregar a requisi��o
	 * 
	 * @author Jo�o Paulo Zica
	 * @param requisicao
	 * @return
	 */
	public Requisicao carregaRequisicao(Requisicao requisicao){
		return requisicaoDAO.carregaRequisicao(requisicao);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO.</p>
	 * Faz update no estado da requisi��o.
	 * 
	 * @param requisicaoestado
	 * @param cd
	 * @author Jo�o Paulo Zica
	 */
	public void updateEstado(Requisicaoestado requisicaoestado, String cd) {
		requisicaoDAO.updateEstado(requisicaoestado, cd, null);
	}
	
	public void updateEstado(Requisicaoestado requisicaoestado, String whereIn, Date dtconclusao) {
		requisicaoDAO.updateEstado(requisicaoestado, whereIn, dtconclusao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @param requisicaoestado
	 * @author Tom�s Rabelo
	 */
	public void updateEstados(String whereIn, Requisicaoestado requisicaoestado) {
		 	requisicaoDAO.updateEstados(whereIn, requisicaoestado);
	}

	public List<Requisicao> carregaDadosRequisicaoForCSV(RequisicaoFiltro filtro){
		return requisicaoDAO.carregaDadosRequisicaoForCSV(filtro);
	}
	
	public Map<Integer, String> buscaSituacoesAutorizacoes(String whereIn){
		return requisicaoDAO.buscaSituacoesAutorizacoes(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param requisicao
	 * @author Marden Silva
	 */	
	public Requisicao carregaDadosRequisicaoForEmail(Requisicao requisicao) {
		return requisicaoDAO.carregaDadosRequisicaoForEmail(requisicao);
	}
	
	public void enviaEmailRequisicao(WebRequestContext request, Requisicao requisicao, boolean concluida, boolean criada, boolean cancelada, boolean paramCliente, String observacao, Boolean observacaoInterna) throws Exception {
		requisicao = this.carregaRequisicao(requisicao);

		Contrato contrato = requisicao.getContrato();
		Empresa empresa = empresaService.loadPrincipal();
		Colaborador colaborador = requisicao.getColaboradorresponsavel();
		
		String codigo = requisicao.getCdrequisicao() != null ? requisicao.getCdrequisicao().toString() : "";
		String assunto = null;

		if(criada){
			assunto = "W3erp - Ordem de Servi�o " + codigo + " Cadastrada";
		} else if(concluida) {
			assunto = "W3erp - Ordem de Servi�o " + codigo + " Conclu�da";
		} else if(cancelada){
			assunto = "W3erp - Ordem de Servi�o " + codigo + " Cancelada";			
		} else {
			assunto = "W3erp - Ordem de Servi�o " + codigo + " Alterada";
		}
		
		// CONFEC��O DO TEMPLATE
		TemplateManager templateEmail = null;

		if(criada && !concluida){
			templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioRequisicao.tpl");
		} else if(concluida) {
			templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioRequisicaoConcluida.tpl");
		} else {
			templateEmail = new TemplateManager("/WEB-INF/template/templateEnvioRequisicaoAlterada.tpl");
		}
		
		String urlResponsavel = SinedUtil.getUrlWithContext() + "/servicointerno/crud/Requisicao" + (!codigo.equals("") ? "?ACAO=consultar&cdrequisicao=" + codigo : "");
		
		String emailRemetente = empresa.getEmail();
		
		String destinoResponsavel = colaborador.getEmail();
		
		String corpoEmailResponsavel = "";
		
		if (colaborador == null || colaborador.getEmail() == null || colaborador.getEmail().trim().equals("")){
//			throw new SinedException("Endere�o de e-mail n�o cadastrado.");
			request.addError("N�o foi poss�vel enviar o e-mail para o Respons�vel. Endere�o de e-mail n�o cadastrado.");
		}else {
			Colaborador responsaveldepartamento = colaboradorService.getResponsavelDepartamento(colaborador);

			String descricao = requisicao.getDescricao();

			if (descricao!=null){
				descricao = descricao.replaceAll("\n", "<br>").trim();
			}

			if (observacao!=null){
				observacao = observacao.replaceAll("\n", "<br>").trim();
			}

			templateEmail = templateEmail
			.assign("codigo", codigo)
			.assign("data", new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis())))
			.assign("hora", new SimpleDateFormat("HH:mm").format(new Date(System.currentTimeMillis())))
			.assign("cliente", requisicao.getCliente() != null ? requisicao.getCliente().getNome() : "")
			.assign("contrato", contrato != null && contrato.getDescricao() != null ? contrato.getDescricao() : "")
			.assign("situacao", requisicao.getRequisicaoestado() != null ? requisicao.getRequisicaoestado().getDescricao() : "")
			.assign("prioridade", requisicao.getRequisicaoprioridade() != null ? requisicao.getRequisicaoprioridade().getDescricao() : "")
			.assign("descricao", descricao)
			.assign("observacao", observacao == null || observacao.equals("") ? "" : ("<b>�ltima observa��o lan�ada: </b>" + observacao));

			corpoEmailResponsavel = templateEmail.assign("url", "Por favor, verifique no <a href='" + urlResponsavel + "'>W3ERP</a>").getTemplate();
			
			// MONTAGEM DO OBJECTO DE E-MAIL PARA ENVIO
			String emailsuporte = parametrogeralService.getValorPorNome(Parametrogeral.EMAILSUPORTE);
			if(emailsuporte != null && !"".equals(emailsuporte)){
				emailRemetente = emailsuporte;
			}
			
			if(destinoResponsavel != null && !destinoResponsavel.equals("")){
				EmailManager emailResponsavel = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
				.setSubject(assunto)
				.setFrom(emailRemetente)
				.setTo(destinoResponsavel);

				if(responsaveldepartamento != null) {
					emailResponsavel.setCc(responsaveldepartamento.getEmail());
				}

				if(requisicao.getAtividadetipo() != null){
					if((requisicao.getAtividadetipo().getEmailextraenvioos() != null && !requisicao.getAtividadetipo().getEmailextraenvioos().isEmpty())){
						emailResponsavel.setListCC(Arrays.asList(requisicao.getAtividadetipo().getEmailextraenvioos().split(";|,")));
					}
					
					corpoEmailResponsavel = templateEmail
					.assign("mensagemTipoAtividade", requisicao.getAtividadetipo().getMensagememail() != null ? requisicao.getAtividadetipo().getMensagememail() : "")
					.getTemplate();
				}

				Envioemail envioemail = envioemailService.registrarEnvio(emailRemetente, assunto, corpoEmailResponsavel, Envioemailtipo.AVISO_ORDEM_SERVICO_CONCLUIDA, colaborador, destinoResponsavel, colaborador.getNome(), emailResponsavel);
				
				try {
					emailResponsavel.addHtmlText(corpoEmailResponsavel + 
							EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinoResponsavel) + 
							usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), emailResponsavel))
							.sendMessage();
				} catch (Exception e) {
					request.addError("N�o foi poss�vel enviar o e-mail para o Respons�vel.");
				}
			}
		}				

		if(paramCliente){
			if(observacaoInterna != null && observacaoInterna){
				templateEmail.assign("observacao", "");
			}
			//Envio de email para o cliente
			String corpoEmailCliente = templateEmail
			.assign("url","")
			.getTemplate();
			
			String destinoCliente = requisicao.getCliente().getEmail();
			Pessoa pessoaCliente = requisicao.getCliente();
			String nomeCliente = requisicao.getCliente().getNome();

			if(destinoResponsavel != null && !destinoResponsavel.equals("")){
				emailRemetente = destinoResponsavel;
			}
			
			if (destinoCliente==null || destinoCliente.trim().equals("")){
				request.addError("N�o foi poss�vel enviar o e-mail para o Cliente. Endere�o de e-mail n�o cadastrado.");
			}else {
				EmailManager emailCliente = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
				.setSubject(assunto)
				.setFrom(emailRemetente)
				.setTo(destinoCliente);
				
				Envioemail envioemail = envioemailService.registrarEnvio(emailRemetente, assunto, corpoEmailResponsavel, Envioemailtipo.AVISO_ORDEM_SERVICO_CONCLUIDA, pessoaCliente, destinoCliente, nomeCliente, emailCliente);
				try {
					emailCliente.addHtmlText(corpoEmailCliente + 
							EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinoCliente) +
							usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), emailCliente)).sendMessage();
				} catch (Exception e) {
					request.addError("N�o foi poss�vel enviar o e-mail para o Cliente.");
				}
			}
			
			//Envio de email para o contato
			String urlContato = "http://" + SinedUtil.getUrlWithoutContext() + "/w3cliente";
			String corpoEmailContato = templateEmail
			.assign("url","Por favor, verifique no <a href='" + urlContato + "'>W3CLIENTE</a>")
			.getTemplate();
			String destinoContato = null;
			Pessoa pessoaContato = null;
			String nomeContato = null;

			if (requisicao.getContato() != null && 
					requisicao.getContato().getEmailcontato() != null && 
					!requisicao.getContato().getEmailcontato().equals("")){
				destinoContato = requisicao.getContato().getEmailcontato();
				pessoaContato = requisicao.getContato();
				nomeContato = requisicao.getContato().getNome();
			}

			if(destinoContato != null && !destinoContato.equals("") && (colaborador == null || colaborador.getEmail() == null || !destinoContato.trim().equals(colaborador.getEmail().trim())) &&
					(destinoCliente == null || !destinoContato.trim().equals(destinoCliente.trim()))){
				EmailManager emailCliente = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME))
				.setSubject(assunto)
				.setFrom(emailRemetente)
				.setTo(destinoContato);
				
				Envioemail envioemail = envioemailService.registrarEnvio(emailRemetente, assunto, corpoEmailResponsavel, Envioemailtipo.AVISO_ORDEM_SERVICO_CONCLUIDA, pessoaContato, destinoContato, nomeContato, emailCliente);
				try {
					emailCliente.addHtmlText(corpoEmailContato + EmailUtil.getHtmlConfirmacaoEmail(envioemail, destinoContato)).sendMessage();
				} catch (Exception e) {
					request.addError("N�o foi poss�vel enviar o e-mail para o Contato.");
				}
			}
		}		
	}
	
	public List<Requisicao> findConcluidaByContrato(Contrato contrato, Integer cdrequisicao){
		return requisicaoDAO.findConcluidaByContrato(contrato, cdrequisicao);
	}
	
	public List<Requisicao> findConcluidaByContrato(Contrato contrato){
		return this.findConcluidaByContrato(contrato, null);
	}

	public List<Requisicao> findByCliente(Cliente cliente, Atividadetipo atividadetipo) {
		return requisicaoDAO.findByCliente(cliente, atividadetipo, null);
	}
	
	public List<Requisicao> findByCliente(Cliente cliente) {
		return requisicaoDAO.findByCliente(cliente, null, null);
	}
	
	public List<Requisicao> findByCliente(Cliente cliente, Atividadetipo atividadetipo, Empresa empresa) {
		return requisicaoDAO.findByCliente(cliente, atividadetipo, empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @author Taidson
	 * @since 24/05/2010
	 */
	public void atualizaHoras (Requisicao requisicao, Double qtdeHoras){
		requisicaoDAO.atualizaHoras(requisicao, qtdeHoras);
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param selectedItens
	 * @return
	 * @author Administrador
	 * @since 23/08/2010
	 */
	public List<Requisicao> historicoCliente(String selectedItens, Empresa empresa, Date dtinicio, Date dtfim){
		return requisicaoDAO.historicoCliente(selectedItens, empresa, dtinicio, dtfim);
	}
	 
	/**
	 * M�todo que gera relat�rio de fichas da ordem de servi�o
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorioFichas(String whereIn) {
		
		Report report = new Report("servicointerno/emitirRequisicao");
		List<Requisicao> list = loadRequisicoesForReport(whereIn);
		
		Empresa empresaPrincipal = empresaService.loadPrincipal();
		Endereco enderecoEmpresaPrincipal = enderecoService.carregaEnderecoEmpresa(empresaPrincipal);
		
		String mensagemAceiteClienteTemplate = "TERMO DE RECEBIMENTO DO SERVI�O\n";
		mensagemAceiteClienteTemplate += "Pelo presente termo, aceito os servi�os prestados<NOME_EMPRESA>referente � descri��o acima.\n";
		mensagemAceiteClienteTemplate += "<CIDADE><DATA>";
		
		for (Requisicao requisicao : list) {
			
			Empresa empresaRequisicao;
			Endereco enderecoEmpresaRequisicao;
			
			if (requisicao.getEmpresa()!=null){
				empresaRequisicao = requisicao.getEmpresa();
				enderecoEmpresaRequisicao = enderecoService.carregaEnderecoEmpresa(requisicao.getEmpresa());
			}else if (requisicao.getContrato() != null && requisicao.getContrato().getEmpresa()!= null){
				empresaRequisicao = requisicao.getContrato().getEmpresa();
				enderecoEmpresaRequisicao = enderecoService.carregaEnderecoEmpresa(requisicao.getContrato().getEmpresa());
			}else {
				empresaRequisicao = empresaPrincipal;
				enderecoEmpresaRequisicao = enderecoEmpresaPrincipal;
			}
			
			empresaService.setInformacoesPropriedadeRural(empresaRequisicao);
			
			requisicao.setEmpresa(empresaRequisicao);
			requisicao.setEnderecoEmpresa(enderecoEmpresaRequisicao);
			requisicao.setLogo(SinedUtil.getLogo(empresaRequisicao));
			
			if (requisicao.getEndereco() != null && requisicao.getEndereco().getCdendereco() != null)
				requisicao.getCliente().setEnderecoAux(requisicao.getEndereco());
			else
				enderecoService.carregaEnderecoCliente(requisicao.getCliente());
			requisicao.setListaRequisicaohistorico(requisicaohistoricoService.findRequisicoesHistoricoByRequisicao(requisicao));
			
			List<Telefone> telefonesContato = new ArrayList<Telefone>();
			if(requisicao.getContato() != null && requisicao.getContato().getCdpessoa() != null){
				telefonesContato = telefoneService.findByPessoa(requisicao.getContato());
				if(telefonesContato != null && !telefonesContato.isEmpty()) {
					for (Telefone telefone : telefonesContato) {
						if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.RESIDENCIAL) 
								|| telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL))
							requisicao.getContato().setTelefonePrincipal(telefone);
						else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR))
							requisicao.getContato().setCelular(telefone);
					}
				}
			}
			
			List<Telefone> telefonesCliente = telefoneService.findByPessoa(requisicao.getCliente());
			if (telefonesContato == null || telefonesContato.size() <= 0){
				if (requisicao.getCliente() != null && requisicao.getCliente().getCdpessoa() != null){
					if(telefonesCliente != null && !telefonesCliente.isEmpty()) {
						for (Telefone telefone : telefonesCliente) {
							if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.RESIDENCIAL)
									|| telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL))
								requisicao.getCliente().setTelefonePrincipal(telefone);
							else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR))
								requisicao.getCliente().setCelular(telefone);
						}
					}
				}
			}
			
			if (telefonesCliente == null || telefonesCliente.size() <= 0){
				if (requisicao.getEmpresa() != null && requisicao.getEmpresa().getCdpessoa() != null){
					List<Telefone> telefonesEmpresa = telefoneService.findByPessoa(requisicao.getEmpresa());
					if(telefonesEmpresa != null && !telefonesEmpresa.isEmpty()) {
						for (Telefone telefone : telefonesEmpresa) {
							if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.RESIDENCIAL)
									|| telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL))
								requisicao.getEmpresa().setTelefonePrincipal(telefone);
							else if (telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR))
								requisicao.getEmpresa().setCelular(telefone);
						}
					}
				}
			}
			
			if(requisicao.getListaRequisicaohistorico() != null && !requisicao.getListaRequisicaohistorico().isEmpty()){
				for(Requisicaohistorico rh : requisicao.getListaRequisicaohistorico()){
					if(rh.getObservacao() != null && rh.getObservacao().contains("Origem: Venda")){
						try {
							rh.setObservacao(rh.getObservacaovendaTrans());
						} catch (Exception e) {}
					}
				}
			}
			
			String mensagemAceiteCliente = mensagemAceiteClienteTemplate;
			if (empresaRequisicao!=null){
				mensagemAceiteCliente = mensagemAceiteCliente.replace("<NOME_EMPRESA>", " pela " + empresaRequisicao.getRazaosocialOuNome() + " ");
			}else {
				mensagemAceiteCliente = mensagemAceiteCliente.replace("<NOME_EMPRESA>", " ");
			}
			if (enderecoEmpresaRequisicao!=null && enderecoEmpresaRequisicao.getMunicipio()!=null){
				mensagemAceiteCliente = mensagemAceiteCliente.replace("<CIDADE>", enderecoEmpresaRequisicao.getMunicipio().getNome() + ", ");
			}else {
				mensagemAceiteCliente = mensagemAceiteCliente.replace("<CIDADE>", "");
			}
			mensagemAceiteCliente = mensagemAceiteCliente.replace("<DATA>", new SimpleDateFormat("dd' de 'MMMM' de 'yyyy").format(SinedDateUtils.currentDate()));
			requisicao.setMensagemAceiteCliente(mensagemAceiteCliente);
		}
		
		report.addSubReport("SUB_REPORT", new Report("/servicointerno/emitirRequisicaoSub"));
		report.addSubReport("SUB_HISTORICO", new Report("/servicointerno/emitirRequisicaoHistoricoSub"));
		report.setDataSource(list);
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicao> loadRequisicoesForReport(String whereIn) {
		return requisicaoDAO.loadRequisicoesForReport(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#updateResponsavel(Colaborador colaboradorresponsavel, Integer cdrequisicao)
	 *
	 * @param colaboradorresponsavel
	 * @param cdrequisicao
	 * @author Luiz Fernando
	 */	
	public void updateResponsavel(Colaborador colaboradorresponsavel, Integer cdrequisicao){
		requisicaoDAO.updateResponsavel(colaboradorresponsavel, cdrequisicao);
	}
	
	/**
	 * M�todo que envia email para o respons�vel da ordem de servi�o que est�o em espera
	 * Obs: Email s� � enviado se parametro geral ENVIAREMAILRESPONSAVELREQUISICAOEMESPERA estiver como TRUE
	 * Obs: Este m�todo � executado toda vez que o primeiro usu�rio loga no sistema
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaoService#findForEnviaEmailResponsavelRequisicaoEmEspera()
	 * @see br.com.linkcom.sined.geral.service.RequisicaoService#enviaEmailOSEmAndamento(Empresa empresa, String emailresponsavel, String htmltext)
	 *
	 * @author Luiz Fernando
	 */
	public void enviaEmailResponsavelRequisicaoEmEspera() {		
		String enviar = parametrogeralService.buscaValorPorNome(Parametrogeral.ENVIAREMAILRESPONSAVELREQUISICAOEMESPERA);
		
		if(enviar != null && !"".equals(enviar) && Boolean.valueOf(enviar)){
			List<Requisicao> listaRequisicao = this.findForEnviaEmailResponsavelRequisicaoEmEspera();
			if(listaRequisicao != null && !listaRequisicao.isEmpty()){
				StringBuilder htmltxt = new StringBuilder();			
				Integer cdresponsavel = new Integer(0);
				String emailresponsavel = "";
				Empresa empresa = empresaService.loadPrincipal();
				int i = 0;
				for(Requisicao r : listaRequisicao){				
					if(r.getColaboradorresponsavel() != null && r.getColaboradorresponsavel().getCdpessoa() != null){
						if(cdresponsavel != r.getColaboradorresponsavel().getCdpessoa()){						
							cdresponsavel = r.getColaboradorresponsavel().getCdpessoa();	
							emailresponsavel = r.getColaboradorresponsavel().getEmail();
							htmltxt = new StringBuilder(" A(s) seguinte(s) Ordem de Servi�o est�(�o) Em Espera: <P>");
						}
					}
					
					htmltxt
						.append("OS: " + r.getCdrequisicao()).append(r.getDescricao() != null ? " - " + r.getDescricao() : "" )
						.append("<BR>")
						.append("Cliente: " + (r.getCliente() != null && r.getCliente().getNome() != null ?  r.getCliente().getNome() : " - "))
						.append("<BR>")
						.append("Contrato: " + (r.getContrato() != null && r.getContrato().getDescricao() != null ?  r.getContrato().getDescricao().trim() : " - "))
						.append("<BR>")
						.append("Data de Previs�o: " + (r.getDtprevisao() != null ? r.getDtprevisao() : " - "))
						.append("<BR><BR>");
					
					if(listaRequisicao.size()-1 == i){
						this.enviaEmailOSEmAndamento(empresa, emailresponsavel, htmltxt.toString());
					}else if(cdresponsavel != listaRequisicao.get(i+1).getColaboradorresponsavel().getCdpessoa()){
						this.enviaEmailOSEmAndamento(empresa, emailresponsavel, htmltxt.toString());
					}
					i++;
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * busca as requisi��es que est�o em espera
	 * obs: Este m�todo � executado toda vez que o primeiro usu�rio loga no sistema
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForEnviaEmailResponsavelRequisicaoEmEspera(){
		return requisicaoDAO.findForEnviaEmailResponsavelRequisicaoEmEspera();
	}
	
	/**
	 * M�todo para enviar email para o respons�vel da requisi��o de acordo com o par�metro
	 *
	 * @param empresa
	 * @param emailresponsavel
	 * @param htmltext
	 * @author Luiz Fernando
	 */
	private void enviaEmailOSEmAndamento(Empresa empresa, String emailresponsavel, String htmltext){		
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			try {
				email
					.setFrom(empresa.getEmail())
					.setSubject("W3erp - OS Em Andamento")
					.setTo(emailresponsavel)
					.addHtmlText(htmltext)
					.sendMessage();				
				
			} catch (Exception e) {
//				System.out.println("E-mail n�o enviado.");
				e.printStackTrace();
			}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see  br.com.linkcom.sined.geral.dao.RequisicaoDAO#findForPainel(Integer prioridade)
	 *
	 * @param prioridade
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForPainel(Integer prioridade){
		return requisicaoDAO.findForPainel(prioridade);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeEmEspera() {
		return requisicaoDAO.getQtdeEmEspera();
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @return
	 * @since 23/04/2012
	 * @author Rodrigo Freitas
	 */
	public Integer getQtdeAtrasadas() {
		return requisicaoDAO.getQtdeAtrasadas();
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name carregaRequisicaoParaUpdateDaVenda
	 * @param whereIn
	 * @return
	 * @return List<Requisicao>
	 * @author Thiago Augusto
	 * @date 30/05/2012
	 *
	 */
	public List<Requisicao> carregaRequisicaoParaUpdateDaVenda(String whereIn){
		return requisicaoDAO.carregaRequisicaoParaUpdateDaVenda(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param q
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public List<Requisicao> findRequisicaoRelacionadaAutocomplete(String q){
		return requisicaoDAO.findRequisicaoRelacionadaAutocomplete(q);
	}
	
	/**
	 * M�todo que v�lida requisi��es para faturamento
	 * 
	 * @param listaRequisicoes
	 * @param errors
	 * @author Rafael Salvio
	 */
	public String validaRequisicoesParaFaturar(List<Requisicao> listaRequisicoes) {
		for (Requisicao requisicao : listaRequisicoes) {
			if(!requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CONCLUIDA) 
					&& !requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.VISTO)
					&& !requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.FATURADO_PARCIALMENTE)){
				
				return "V�lido somente para requisi��es com situa��o \"Conclu�da\" ou \"Visto (Requisitante)\"";
			}
		}
		
		return "";
	}
	
	public String validaRequisicoesParaFaturarAutorizacao(List<Requisicao> listaRequisicao) {
		if(listaRequisicao == null || listaRequisicao.isEmpty())
			return "Nenhum item selecionado.";
		
		for(Requisicao requisicao : listaRequisicao){
			if(requisicao.getRequisicaoestado() == null || 
					requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.FATURADO) || 
					requisicao.getRequisicaoestado().getCdrequisicaoestado().equals(Requisicaoestado.CANCELADA)){
				return "V�lido somente para requisi��es com situa��o diferente de \"Cancelada\" e \"Faturada\"";
			}			
			if(requisicao.getListaMateriaisrequisicao() == null || requisicao.getListaMateriaisrequisicao().isEmpty())
				return "Nenhum item selecionado para a OS " + requisicao.getCdrequisicao() + ".";
		}
		
		return "";
	}

	/**
	 * <p>M�todo com refer�ncia no DAO</p>
	 * 
	 * @param whereIn
	 * @return	
	 * @author Rafael Salvio
	 */
	public List<Requisicao> carregaRequisicaoForFaturar(String whereIn){
		return requisicaoDAO.carregaRequisicaoForFaturar(whereIn);
	}
	
	public List<Requisicao> carregaRequisicaoForFaturarAutorizacao(String whereIn, boolean medicao){
		List<Requisicao> listaRequisicao = requisicaoDAO.carregaRequisicaoForFaturarAutorizacao(whereIn);
		if(medicao){
			ajustaListaComPercentualfaturado(listaRequisicao);
		}
		List<Requisicao> listaRequisicaoNova = new ArrayList<Requisicao>();
		for (Requisicao requisicao : listaRequisicao) {
			List<Materialrequisicao> listaMateriaisrequisicao = requisicao.getListaMateriaisrequisicao();
			List<Materialrequisicao> listaMateriaisrequisicaoNova = new ArrayList<Materialrequisicao>();
			for (Materialrequisicao materialrequisicao : listaMateriaisrequisicao) {
				if(materialrequisicao.getFaturado() == null || !materialrequisicao.getFaturado()){
					if(medicao){
						listaMateriaisrequisicaoNova.add(materialrequisicao);
					} else {
						/*if(materialrequisicao.getAutorizacaoFaturamento() != null && 
								!"".equals(materialrequisicao.getAutorizacaoFaturamento()) &&
								(materialrequisicao.getPercentualfaturado() == null || materialrequisicao.getPercentualfaturado() <= 0)){
							listaMateriaisrequisicaoNova.add(materialrequisicao);
						}*/
						if(materialrequisicao.getPercentualfaturado() == null || materialrequisicao.getPercentualfaturado() <= 0){
							listaMateriaisrequisicaoNova.add(materialrequisicao);
						}
					}
				}
			}
			requisicao.setListaMateriaisrequisicao(listaMateriaisrequisicaoNova);
			if(listaMateriaisrequisicaoNova.size() > 0){
				listaRequisicaoNova.add(requisicao);
			}
		}
		return listaRequisicaoNova;
	}
	
	/**
	* M�todo que ajusta o percentual faturado do item da requisi��o
	*
	* @param listaRequisicao
	* @since 15/10/2015
	* @author Luiz Fernando
	*/
	private void ajustaListaComPercentualfaturado(List<Requisicao> listaRequisicao) {
		for (Requisicao requisicao : listaRequisicao) {
			if(SinedUtil.isListNotEmpty(requisicao.getListaMateriaisrequisicao())){
				String whereInRequisicaomaterial = CollectionsUtil.listAndConcatenate(requisicao.getListaMateriaisrequisicao(), "cdmaterialrequisicao", ",");
				if(StringUtils.isNotBlank(whereInRequisicaomaterial)){
					List<Requisicaonota> listaRequisicaonota = requisicaonotaService.findForPercentualfaturado(requisicao, whereInRequisicaomaterial);
					for(Materialrequisicao materialrequisicao : requisicao.getListaMateriaisrequisicao()){
						Double percentualfaturado = 0d;
						if(SinedUtil.isListNotEmpty(listaRequisicaonota)){
							for(Requisicaonota requisicaonota : listaRequisicaonota){
								if(requisicaonota.getPercentualfaturado() != null &&
										requisicaonota.getPercentualfaturado() > 0 &&
										requisicaonota.getMaterialrequisicao() != null &&
										requisicaonota.getMaterialrequisicao().getCdmaterialrequisicao() != null &&
										requisicaonota.getMaterialrequisicao().getCdmaterialrequisicao().equals(materialrequisicao.getCdmaterialrequisicao())){
									percentualfaturado += requisicaonota.getPercentualfaturado();
								}
							}
						}
						materialrequisicao.setPercentualfaturado(percentualfaturado);
					}
				}
			}
		}
	}
	
	/**
	 * Cria nota fiscal de servi�o, agrupadas por cliente ou individuais(definido por notaUnica), a partir de uma ou v�rias requisi��es
	 * 
	 * @param listaRequisicoes
	 * @return
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public List<NotaFiscalServico> criaNotas(List<Requisicao> listaRequisicoes, boolean notaUnica) {
		WebRequestContext request = NeoWeb.getRequestContext();
		List<NotaFiscalServico> listaNotaFS = new ListSet<NotaFiscalServico>(NotaFiscalServico.class);
		
		Requisicao requisicaoPrimeira = listaRequisicoes.get(0);
		Cliente clientePrimeiro = requisicaoPrimeira.getCliente();
		
		NotaFiscalServico nota = new NotaFiscalServico();
		nota.setCliente(clientePrimeiro);
		nota.setDtEmissao(new Date(System.currentTimeMillis()));
		nota.setFromRequisicao(true);
		nota.setListaItens(new ArrayList<NotaFiscalServicoItem>());
		nota.setBasecalculo(new Money(0.));
		nota.setBasecalculoiss(new Money(0.));
		nota.setBasecalculoinss(new Money(0.));
		nota.setProjeto(requisicaoPrimeira.getProjeto());
		if(requisicaoPrimeira.getEndereco() != null){
			nota.setEnderecoCliente(requisicaoPrimeira.getEndereco());
		} else {
			List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(clientePrimeiro);
			clientePrimeiro.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
			
			nota.setEnderecoCliente(clientePrimeiro.getEndereco());
		}
		
		Empresa empresa = null;
		if(requisicaoPrimeira.getEmpresa() != null){
			empresa = requisicaoPrimeira.getEmpresa();
		}else if(requisicaoPrimeira.getContrato() != null && requisicaoPrimeira.getContrato().getEmpresa() != null){
			empresa = requisicaoPrimeira.getContrato().getEmpresa();
		}else {
			empresa = (Empresa) request.getSession().getAttribute("empresaSelecionada");
		}
		
		empresa = empresa != null && empresa.getCdpessoa() != null ? empresa : empresaService.loadPrincipal();
		notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, nota);
		
		NotaFiscalServicoItem notaFSI = null;
		Iterator<Requisicao> iter = listaRequisicoes.iterator();
		Cliente ultimoCliente = new Cliente(clientePrimeiro.getCdpessoa());
		while(iter.hasNext()){
			Requisicao req = iter.next();
			Cliente cliente = req.getCliente();
			if(ultimoCliente.getCdpessoa() != cliente.getCdpessoa()){
				if(nota.getDadosAdicionais() != null && !nota.getDadosAdicionais().isEmpty())
					nota.setDadosAdicionais(nota.getDadosAdicionais().substring(0, nota.getDadosAdicionais().length()-1));
				
				listaNotaFS.add(nota);
				ultimoCliente.setCdpessoa(cliente.getCdpessoa());
				
				nota = new NotaFiscalServico();
				nota.setCliente(cliente);
				
				nota.setProjeto(req.getProjeto());
				if(req.getEndereco() != null){
					nota.setEnderecoCliente(req.getEndereco());
				} else {
					List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
					cliente.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
					
					nota.setEnderecoCliente(cliente.getEndereco());
				}
				
				nota.setDtEmissao(new Date(System.currentTimeMillis()));
				nota.setFromRequisicao(true);
				nota.setListaItens(new ArrayList<NotaFiscalServicoItem>());
				nota.setBasecalculo(new Money(0.));
				nota.setBasecalculoiss(new Money(0.));
				nota.setBasecalculoinss(new Money(0.));
				
				if(requisicaoPrimeira.getContrato() != null && requisicaoPrimeira.getContrato().getEmpresa() != null)
					empresa = requisicaoPrimeira.getContrato().getEmpresa();
				else {
					empresa = (Empresa) request.getSession().getAttribute("empresaSelecionada");
				}
				
				empresa = empresa != null && empresa.getCdpessoa() != null ? empresa : empresaService.loadPrincipal();
				notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, nota);
			}
			
			if (req.getListaMateriaisrequisicao() != null && !req.getListaMateriaisrequisicao().isEmpty()){
				for(Materialrequisicao mr : req.getListaMateriaisrequisicao()){
					if(mr.getMaterial() != null && mr.getMaterial().getServico() != null && mr.getMaterial().getServico()){
						if(mr.getFaturado() == null || !mr.getFaturado()){
							notaFSI = new NotaFiscalServicoItem();
							
							notaFSI.setQtde(mr.getQuantidade().doubleValue());
							if(mr.getValortotal() != null)
								notaFSI.setPrecoTotal(new Money(mr.getValortotal()));
							else
								notaFSI.setPrecoTotal(new Money(0.));
							if(mr.getValorunitario() != null)
								notaFSI.setPrecoUnitario(new Money(mr.getValorunitario()).getValue().doubleValue());
							else
								notaFSI.setPrecoUnitario(0D);
							
							notaFSI.setCdmaterialrequisicao(mr.getCdmaterialrequisicao());
							
							StringBuilder desc = new StringBuilder();
							if(req.getContrato() != null && req.getContrato().getDescricao() != null)
								desc.append("CONTRATO: " + req.getContrato().getDescricao() + "\n");
							desc.append("PEDIDO: " + (req.getOrdemservico() != null ? req.getOrdemservico() : req.getCdrequisicao()) + "\n");
							desc.append("ITEM: " + mr.getItem() + "\n" + mr.getMaterial().getNome() + "\n" + 
									(mr.getObservacao() != null && !"".equals(mr.getObservacao()) ? mr.getObservacao() : req.getDescricao()));
							notaFSI.setDescricao(desc.toString());
							
							nota.setBasecalculo( nota.getBasecalculo().add(notaFSI.getPrecoTotal()));
							if(mr.getMaterial().getServico() != null && mr.getMaterial().getServico()){
								nota.setBasecalculoiss(nota.getBasecalculoiss().add(notaFSI.getPrecoTotal()));
								nota.setBasecalculoinss(nota.getBasecalculoinss().add(notaFSI.getPrecoTotal()));
							}
							
							nota.getListaItens().add(notaFSI);
						}
					}
				}
			}
			else{
				notaFSI = new NotaFiscalServicoItem();
				
				notaFSI.setQtde(req.getQtde() != null ? req.getQtde() : 1.);
				notaFSI.setDescricao(req.getDescricao() != null ? req.getDescricao() : "");
				
				nota.getListaItens().add(notaFSI);
			}
			
			if(req.getListaItem() != null && !req.getListaItem().isEmpty()){
				List<Requisicaoitem> listaItem = new ListSet<Requisicaoitem>(Requisicaoitem.class);
				for(Requisicaoitem ri : req.getListaItem()){
					if(ri.getAtividadetipoitem().getOrdem() != null 
							&& ri.getAtividadetipoitem().getImprimeNota() != null 
							&& ri.getAtividadetipoitem().getImprimeNota())
						listaItem.add(ri);
				}
				if(listaItem != null && !listaItem.isEmpty()){
					Collections.sort(listaItem, new Comparator<Requisicaoitem>(){
						public int compare(Requisicaoitem r1, Requisicaoitem r2){
							if(r1.getAtividadetipoitem().getOrdem() > r2.getAtividadetipoitem().getOrdem())
								return -1;
							else if(r1.getAtividadetipoitem().getOrdem() < r2.getAtividadetipoitem().getOrdem())
								return 1;
							else return 0;
						}
					});
					StringBuilder camposAdicionais = new StringBuilder();
					for(Requisicaoitem ri : listaItem){
						if(ri.getValor() != null
								&& !ri.getValor().equals("") 
								&& ri.getAtividadetipoitem().getImprimeNota() != null
								&& ri.getAtividadetipoitem().getImprimeNota()){
							camposAdicionais.append(ri.getValor() + ", ");
						}
					}
					if(camposAdicionais.length() > 0){
						nota.setDadosAdicionais( (nota.getDadosAdicionais()!=null ? nota.getDadosAdicionais() : "") + camposAdicionais.toString());
					}
				}
			}
			nota.setWhereIn((nota.getWhereIn()!=null ? nota.getWhereIn() : "") + req.getCdrequisicao() + ",");
			if(!notaUnica)
				ultimoCliente.setCdpessoa(0);
		}
		if(nota.getDadosAdicionais() != null && !nota.getDadosAdicionais().isEmpty())
			nota.setDadosAdicionais(nota.getDadosAdicionais().substring(0, nota.getDadosAdicionais().length()-1));
		listaNotaFS.add(nota);
		return listaNotaFS;
	}
	
	/**
	 * M�todo que fatura as requisi��es de acordo com o parametro de autorizacaoFaturamento fornecido.
	 * 
	 * @param listaRequisicoes
	 * @param autorizacaoFaturamento
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	@SuppressWarnings("unchecked")
	public List<NotaFiscalServico> criaNotasByAutorizacao(List<Requisicao> listaRequisicoes, boolean medicao){
		WebRequestContext request = NeoWeb.getRequestContext();
		List<NotaFiscalServico> listaNotaFS = new ListSet<NotaFiscalServico>(NotaFiscalServico.class);
		for(Requisicao req : listaRequisicoes){
			NotaFiscalServico nota = new NotaFiscalServico();
			Cliente cliente = req.getCliente();
			
			nota.setListaItens(new ListSet<NotaFiscalServicoItem>(NotaFiscalServicoItem.class));
			nota.setCliente(cliente);
			nota.setDtEmissao(new Date(System.currentTimeMillis()));
			nota.setFromRequisicao(true);
			nota.setProjeto(req.getProjeto());
			
			if(req.getEndereco() != null){
				nota.setEnderecoCliente(req.getEndereco());
			} else {
				List<Endereco> listaEndereco = enderecoService.carregarListaEndereco(cliente);
				cliente.setListaEndereco(SinedUtil.listToSet(listaEndereco, Endereco.class));
				
				nota.setEnderecoCliente(cliente.getEndereco());
			}
			
			Empresa empresa = null;
			if(req.getEmpresa() != null){
				empresa = req.getEmpresa();
			}else if(req.getContrato() != null && req.getContrato().getEmpresa() != null){
				empresa = req.getContrato().getEmpresa();
			}else {
				empresa = (Empresa) request.getSession().getAttribute("empresaSelecionada");
			}
			
			empresa = empresa != null && empresa.getCdpessoa() != null ? empresa : empresaService.loadPrincipal();
			nota.setEmpresa(empresa);
			notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, nota);
			
			if(req.getListaItem() != null && !req.getListaItem().isEmpty()){
				List<Requisicaoitem> listaItem = new ListSet<Requisicaoitem>(Requisicaoitem.class);
				for(Requisicaoitem ri : req.getListaItem()){
					if(ri.getAtividadetipoitem().getOrdem() != null 
							&& ri.getAtividadetipoitem().getImprimeNota() != null 
							&& ri.getAtividadetipoitem().getImprimeNota())
						listaItem.add(ri);
				}
				if(listaItem != null && !listaItem.isEmpty()){
					Collections.sort(listaItem, new Comparator<Requisicaoitem>(){
						public int compare(Requisicaoitem r1, Requisicaoitem r2){
							if(r1.getAtividadetipoitem().getOrdem() > r2.getAtividadetipoitem().getOrdem())
								return -1;
							else if(r1.getAtividadetipoitem().getOrdem() < r2.getAtividadetipoitem().getOrdem())
								return 1;
							else return 0;
						}
					});
					StringBuilder camposAdicionais = new StringBuilder();
					for(Requisicaoitem ri : listaItem){
						if(ri.getValor() != null 
								&& !ri.getValor().equals("") 
								&& ri.getAtividadetipoitem().getImprimeNota() != null
								&& ri.getAtividadetipoitem().getImprimeNota()){
							camposAdicionais.append(ri.getValor() + ", ");
						}
					}
					if(camposAdicionais.length() > 0){
						nota.setDadosAdicionais(camposAdicionais.toString());
					}
				}
			}
			nota.setWhereIn(req.getCdrequisicao().toString());
			
			if (req.getListaMateriaisrequisicao() != null && !req.getListaMateriaisrequisicao().isEmpty()){
				for(Materialrequisicao mr : req.getListaMateriaisrequisicao()){
					if(mr.getMaterial() != null && ((mr.getMaterial().getServico() != null && mr.getMaterial().getServico()) || 
							(mr.getMaterial().getProduto() != null && mr.getMaterial().getProduto() && mr.getMaterial().getTributacaomunicipal() != null &&
							mr.getMaterial().getTributacaomunicipal()))) {
						boolean checado = mr.getChecado() != null && mr.getChecado();
						boolean havePercentual = mr.getPercentualafaturar() != null && mr.getPercentualafaturar() > 0;
						
						if((mr.getFaturado() == null || !mr.getFaturado()) && (checado || (havePercentual && medicao))){
							NotaFiscalServico nfs = new NotaFiscalServico();
							nfs.setDadosAdicionais(nota.getDadosAdicionais()!=null ? nota.getDadosAdicionais() : "");
							nfs.setWhereIn(nota.getWhereIn());
							nfs.setListaItens(new ListSet<NotaFiscalServicoItem>(NotaFiscalServicoItem.class));
							nfs.setCliente(nota.getCliente());
							nfs.setEnderecoCliente(nota.getEnderecoCliente());
							nfs.setDtEmissao(new Date(System.currentTimeMillis()));
							nfs.setFromRequisicao(true);
							nfs.setProjeto(nota.getProjeto());
							nfs.setMaterialrequisicao(mr);
							if(mr.getDtvencimento() != null)
								nfs.setDtVencimento(mr.getDtvencimento());
							
							if(nota.getEmpresa() != null) 
								nfs.setEmpresa(nota.getEmpresa());
							
							notaFiscalServicoService.preencheValoresPadroesEmpresa(empresa, nfs);
							
							if(empresa.getTributacaomunicipiocliente() != null && empresa.getTributacaomunicipiocliente()){
								if(nota.getEnderecoCliente() != null && 
										nota.getEnderecoCliente().getMunicipio() != null && 
										nota.getMunicipioissqn() != null &&
										!nota.getMunicipioissqn().equals(nota.getEnderecoCliente().getMunicipio())){
									nfs.setMunicipioissqn(nota.getEnderecoCliente().getMunicipio());
									nfs.setNaturezaoperacao(Naturezaoperacao.TRIBUTACAO_FORA_MUNICIPIO);
								}
							}
							
							Double valorunitario = mr.getValorunitario();
							Double valortotal = mr.getValortotal();
							Double quantidade = mr.getQuantidade().doubleValue();
							if(havePercentual){
								valortotal = valortotal * (mr.getPercentualafaturar() / 100d);
								//valorunitario = valortotal / quantidade;
							}
							
							NotaFiscalServicoItem notaFSI = new NotaFiscalServicoItem();
							
							notaFSI.setQtde(quantidade);
							
							if(valortotal != null) notaFSI.setPrecoTotal(new Money(valortotal));
							else notaFSI.setPrecoTotal(new Money(0.));
							
							if(valorunitario != null) notaFSI.setPrecoUnitario(valorunitario);
							else notaFSI.setPrecoUnitario(0D);
							
							StringBuilder desc = new StringBuilder();
							if(StringUtils.isNotEmpty(req.getDescricao())){
								desc.append(req.getDescricao());
							}
							desc.append("\nITEM: " + mr.getItem());
							if(StringUtils.isNotEmpty(mr.getAutorizacaoFaturamento())){
								desc.append("\nFOLHA: " + mr.getAutorizacaoFaturamento());
							}
							desc.append("\n" + mr.getMaterial().getNome());
							desc.append("\n" +(StringUtils.isNotEmpty(mr.getObservacao()) ? mr.getObservacao() : req.getDescricao()));
							if(req.getContrato() != null && req.getContrato().getDescricao() != null)
								desc.append("\nCONTRATO: " + req.getContrato().getDescricao() + "\n");
							desc.append("\nPEDIDO: " + (req.getOrdemservico() != null ? req.getOrdemservico() : req.getCdrequisicao()) + "\n");
							notaFSI.setDescricao(desc.toString());
							
							notaFSI.setItem(mr.getItem());
							notaFSI.setMaterial(mr.getMaterial());
							notaFSI.setNumeroautorizacaofaturamentoitemos(mr.getAutorizacaoFaturamento());
							notaFSI.setNomematerial(mr.getMaterial().getNome());
							notaFSI.setNomematerialnf(mr.getMaterial().getNomenf());
							
							nfs.setBasecalculo(notaFSI.getPrecoTotal());
							if(mr.getMaterial().getServico() == null || !mr.getMaterial().getServico()){
								nfs.setBasecalculoiss(new Money(0.));
								nfs.setBasecalculoinss(new Money(0.));
							}
							nfs.getListaItens().add(notaFSI);
							
							if(mr.getMaterial() != null && mr.getMaterial().getGrupotributacao() != null){
								if(nfs.getEnderecoCliente() != null){
									Endereco enderecoCliente = enderecoService.loadEndereco(nfs.getEnderecoCliente());
									nfs.setEnderecoCliente(enderecoCliente);
								}
								
								Grupotributacao grupotributacao = grupotributacaoService.loadForEntrada(mr.getMaterial().getGrupotributacao());
								nfs.setGrupotributacao(grupotributacao);
								if(nfs.getGrupotributacao() != null){
									if(nfs.getGrupotributacao().getCodigocnae() != null){
										nfs.setCodigocnaeBean(nfs.getGrupotributacao().getCodigocnae());
									}
									if(nfs.getGrupotributacao().getCodigotributacao() != null){
										nfs.setCodigotributacao(nfs.getGrupotributacao().getCodigotributacao());
									}
									if(nfs.getGrupotributacao().getItemlistaservico() != null){
										nfs.setItemlistaservicoBean(nfs.getGrupotributacao().getItemlistaservico());
									}
									if(StringUtils.isNotBlank(nfs.getGrupotributacao().getInfoadicionalcontrib())){
										if(StringUtils.isBlank(nfs.getDadosAdicionais()) || !nfs.getDadosAdicionais().contains(nfs.getGrupotributacao().getInfoadicionalcontrib())){
											if(StringUtils.isBlank(nfs.getDadosAdicionais())){
												nfs.setDadosAdicionais(nfs.getGrupotributacao().getInfoadicionalcontrib()); 
											}else if(!nfs.getDadosAdicionais().contains(nfs.getGrupotributacao().getInfoadicionalcontrib())){
												nfs.setDadosAdicionais(nfs.getDadosAdicionais() + ". " + nfs.getGrupotributacao().getInfoadicionalcontrib());
											}
										}
									}
									if(StringUtils.isNotBlank(nfs.getGrupotributacao().getInfoadicionalfisco())){
										if(StringUtils.isBlank(nfs.getInfocomplementar()) || !nfs.getDadosAdicionais().contains(nfs.getGrupotributacao().getInfoadicionalfisco())){
											if(StringUtils.isBlank(nfs.getInfocomplementar())){
												nfs.setInfocomplementar(nfs.getGrupotributacao().getInfoadicionalfisco()); 
											}else if(!nfs.getInfocomplementar().contains(nfs.getGrupotributacao().getInfoadicionalfisco())){
												nfs.setInfocomplementar(nfs.getInfocomplementar() + ". " + nfs.getGrupotributacao().getInfoadicionalfisco());
											}
										}
									}
								}
								notaFiscalServicoService.calculaTributacaoNota(nfs, grupotributacao, nfs.getBasecalculo());
							}
							nfs.setRequisicao(req);
							listaNotaFS.add(nfs);
						}
					}
				}
			} else {
				NotaFiscalServicoItem notaFSI = new NotaFiscalServicoItem();
				
				notaFSI.setQtde(req.getQtde() != null ? req.getQtde() : 1.);
				notaFSI.setDescricao(req.getDescricao() != null ? req.getDescricao() : "");
				
				nota.getListaItens().add(notaFSI);
				nota.setRequisicao(req);
				listaNotaFS.add(nota);
			}
		}
		
		if (SinedUtil.isListNotEmpty(listaNotaFS)) {
			for (NotaFiscalServico notaFiscalServico : listaNotaFS) {
				this.verificaTemplateDiscriminacao(notaFiscalServico);
			}
		}
		
		return listaNotaFS;
	}
	
	public void verificaTemplateDiscriminacao(NotaFiscalServico notaFiscalServico) {
		if(notaFiscalServico != null && 
				notaFiscalServico.getRequisicao() != null && 
				notaFiscalServico.getRequisicao().getCliente() != null &&
				notaFiscalServico.getRequisicao().getCliente().getLayoutdiscriminacaoservico() != null) {
			DescricaoDeServicoBean descricaoDeServicoBean = new DescricaoDeServicoBean();
			
			descricaoDeServicoBean.setProjetoSigla(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getSigla() != null ? notaFiscalServico.getRequisicao().getProjeto().getSigla() : "");
			descricaoDeServicoBean.setProjetoNome(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getNome() != null ? notaFiscalServico.getRequisicao().getProjeto().getNome() : "");
			descricaoDeServicoBean.setProjetoCEI(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getCei() != null ? notaFiscalServico.getRequisicao().getProjeto().getCei() : "");
			descricaoDeServicoBean.setProjetoMunicipio(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getNome() != null ? notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getNome() : "");
			descricaoDeServicoBean.setProjetoUf(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf().getNome() != null ? notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf().getNome() : "");
			descricaoDeServicoBean.setProjetoEndereco(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getEndereco() != null ? notaFiscalServico.getRequisicao().getProjeto().getEndereco() : "");
			descricaoDeServicoBean.setProjetoUfSigla(notaFiscalServico.getRequisicao().getProjeto() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf() != null && notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf().getSigla() != null ? notaFiscalServico.getRequisicao().getProjeto().getMunicipio().getUf().getSigla() : "");
			descricaoDeServicoBean.setOrdemServico(notaFiscalServico.getRequisicao().getOrdemservico() != null ? notaFiscalServico.getRequisicao().getOrdemservico() : "");
			descricaoDeServicoBean.setDescricaoOS(notaFiscalServico.getRequisicao().getDescricao() != null ? notaFiscalServico.getRequisicao().getDescricao() : "");
			descricaoDeServicoBean.setItemListaServico(notaFiscalServico.getItemlistaservicoBean() != null && notaFiscalServico.getItemlistaservicoBean().getCodigo() != null ? notaFiscalServico.getItemlistaservicoBean().getCodigo() : "");
			descricaoDeServicoBean.setVencimentoInformadoFaturar(notaFiscalServico.getDtVencimento() != null ? SinedDateUtils.toString(notaFiscalServico.getDtVencimento()) : "");

			if (SinedUtil.isListNotEmpty(notaFiscalServico.getRequisicao().getListaItem())) {

				LinkedList<OrdemServicoCamposAdicionaisBean> listaOrdemServicoCamposAdicionaisBeans = new LinkedList<OrdemServicoCamposAdicionaisBean>();

				for (Requisicaoitem requisicaoitem : notaFiscalServico.getRequisicao().getListaItem()) {
					if (StringUtils.isNotBlank(requisicaoitem.getAtividadetipoitem().getNome()) && StringUtils.isNotBlank(requisicaoitem.getValor())) {
						OrdemServicoCamposAdicionaisBean ordemServicoCamposAdicionaisBean = new OrdemServicoCamposAdicionaisBean();
						ordemServicoCamposAdicionaisBean.setCampo(requisicaoitem.getAtividadetipoitem().getNome());
						ordemServicoCamposAdicionaisBean.setValor(requisicaoitem.getValor());
						listaOrdemServicoCamposAdicionaisBeans.add(ordemServicoCamposAdicionaisBean);
					}
				}
				descricaoDeServicoBean.setCamposAdicionais(listaOrdemServicoCamposAdicionaisBeans);
			}

			descricaoDeServicoBean.setNotaValorServicos(notaFiscalServico.getValorTotalServicos() != null ? SinedUtil.truncateFormat(notaFiscalServico.getValorTotalServicos().getValue().doubleValue()): 0);
			descricaoDeServicoBean.setNotaBaseCalculoIR(notaFiscalServico.getBasecalculoir() != null ? notaFiscalServico.getBasecalculoir().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceIR(notaFiscalServico.getIr() != null ? notaFiscalServico.getIr() : 0);
			descricaoDeServicoBean.setNotaBaseCalculoPIS(notaFiscalServico.getBasecalculopis() != null ? notaFiscalServico.getBasecalculopis().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndicePIS(notaFiscalServico.getPis() != null ? notaFiscalServico.getPis() : 0);
			descricaoDeServicoBean.setNotaBaseCalculoISS(notaFiscalServico.getBasecalculoiss() != null ? notaFiscalServico.getBasecalculoiss().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceISS(notaFiscalServico.getIss() != null ? notaFiscalServico.getIss(): 0);
			descricaoDeServicoBean.setNotaBaseCalculoCofins(notaFiscalServico.getBasecalculocofins() != null ? notaFiscalServico.getBasecalculocofins().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceCofins(notaFiscalServico.getCofins() != null ? notaFiscalServico.getCofins() : 0);
			descricaoDeServicoBean.setNotaBaseCalculoCSLL(notaFiscalServico.getBasecalculocsll() != null ? notaFiscalServico.getBasecalculocsll().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceCSLL(notaFiscalServico.getCsll() != null ? notaFiscalServico.getCsll() : 0);
			descricaoDeServicoBean.setNotaBaseCalculoINSS(notaFiscalServico.getBasecalculoinss() != null ? notaFiscalServico.getBasecalculoinss().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceINSS(notaFiscalServico.getInss() != null ? notaFiscalServico.getInss(): 0);
			descricaoDeServicoBean.setNotaBaseCalculoICMS(notaFiscalServico.getBasecalculoicms() != null ? notaFiscalServico.getBasecalculoicms().getValue().doubleValue() : 0);
			descricaoDeServicoBean.setNotaIndiceICMS(notaFiscalServico.getIcms() != null ? notaFiscalServico.getIcms() : 0);

			if(notaFiscalServico.getRequisicao().getColaboradorresponsavel() != null && StringUtils.isNotBlank(notaFiscalServico.getRequisicao().getColaboradorresponsavel().getNome())){
				descricaoDeServicoBean.setResponsavelProjeto(notaFiscalServico.getRequisicao().getColaboradorresponsavel().getNome());
			} else {
				descricaoDeServicoBean.setResponsavelProjeto("");
			}
			
			
			
			if (SinedUtil.isListNotEmpty(notaFiscalServico.getListaItens())) {
				for (NotaFiscalServicoItem notaFiscalServicoItem : notaFiscalServico.getListaItens()) {
					if (notaFiscalServicoItem != null) {
						LinkedList<OrdemServicoItemBean> listaNumeroAutorizacaoFaturamentoItemOS = new LinkedList<OrdemServicoItemBean>();
						
						OrdemServicoItemBean ordemServicoItemBean = new OrdemServicoItemBean();
						ordemServicoItemBean.setNumeroAutorizacaoFaturamentoItemOS(StringUtils.isNotBlank(notaFiscalServicoItem.getNumeroautorizacaofaturamentoitemos()) ? notaFiscalServicoItem.getNumeroautorizacaofaturamentoitemos() : "");
						ordemServicoItemBean.setItem(StringUtils.isNotBlank(notaFiscalServicoItem.getItem()) ? notaFiscalServicoItem.getItem() : "");
						ordemServicoItemBean.setNome(StringUtils.isNotBlank(notaFiscalServicoItem.getNomematerial()) ? notaFiscalServicoItem.getNomematerial() : "");
						ordemServicoItemBean.setNomenf(StringUtils.isNotBlank(notaFiscalServicoItem.getNomematerialnf()) ? notaFiscalServicoItem.getNomematerialnf() : "");
						ordemServicoItemBean.setQuantidade(notaFiscalServicoItem.getQtde() != null ? notaFiscalServicoItem.getQtde() : 0d);
						ordemServicoItemBean.setValorunitario(notaFiscalServicoItem.getPrecoUnitario() != null ? notaFiscalServicoItem.getPrecoUnitario() : 0d);
						
						listaNumeroAutorizacaoFaturamentoItemOS.add(ordemServicoItemBean);
						
						descricaoDeServicoBean.setListaOrdemServicoBean(listaNumeroAutorizacaoFaturamentoItemOS);
						
						ITemplateEngine engine = new GroovyTemplateEngine().build(notaFiscalServico.getRequisicao().getCliente().getLayoutdiscriminacaoservico().getLeiaute());
						
						Map<String,Object> datasource = new HashMap<String, Object>();
						datasource.put("bean", descricaoDeServicoBean);
						
						String relatorioHTML = engine.make(datasource);
						notaFiscalServicoItem.setDescricao(relatorioHTML);
					}
				}
				
			}
		}
	}
	
	/**
	 * M�todo que realiza o salvamento das notas fiscais e as altera��es dos estados das requisi��es selecionadas.
	 * 
	 * @param listaRequisicoes
	 * @param listaNotas
	 * 
	 * @author Rafael Salvio
	 */
	public void saveFaturarByOS(List<Requisicao> listaRequisicoes, List<NotaFiscalServico> listaNotas){
		MaterialrequisicaoService.getInstance().updateFaturadoByRequisicao(SinedUtil.listAndConcatenateIDs(listaRequisicoes));
		Integer proximoNumero = null;
		for(NotaFiscalServico nfs : listaNotas){
			
			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
			if(!haveVotorantim){
				proximoNumero = empresaService.carregaProxNumNF(nfs.getEmpresa());
				if(proximoNumero == null){
					proximoNumero = 1;
				}
				nfs.setNumero(proximoNumero.toString());
				proximoNumero++;
	
				empresaService.updateProximoNumNF(nfs.getEmpresa(), proximoNumero);
			}
			
			notaFiscalServicoService.saveOrUpdate(nfs);	
			
			for(Requisicao r : listaRequisicoes){
				Requisicaonota requisicaonota = new Requisicaonota();
				requisicaonota.setRequisicao(r);
				requisicaonota.setNota(nfs);
				requisicaonotaService.saveOrUpdate(requisicaonota);
			}
			
			StringBuilder observacaoNota = new StringBuilder();
			observacaoNota.append("Gerada a partir da(s) ordem(ns) de servi�o: ");
			String[] requisicoes = nfs.getWhereIn().split(",");
			
			for(int i = 0; i<requisicoes.length; i++){
				observacaoNota.append("<a href='/w3erp/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao=" + requisicoes[i] + "'>" +  requisicoes[i] + "</a>, ");
				Requisicao requisicaoRemover = null;
				
				for(Requisicao r : listaRequisicoes){
					if(r.getCdrequisicao() == Integer.parseInt(requisicoes[i])){
						this.updateEstado(new Requisicaoestado(Requisicaoestado.FATURADO), r.getCdrequisicao().toString());
						String observacaoReq = "Nota de Servi�o Gerada: <a href='/w3erp/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=" + nfs.getCdNota() + "'>" + nfs.getCdNota() + "</a>";
						this.updateNotaFiscalServico(r.getCdrequisicao(), nfs.getCdNota());
						requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, observacaoReq, r));
						requisicaoRemover = r;
						break;
					}
				}
				
				if(requisicaoRemover != null)
					listaRequisicoes.remove(requisicaoRemover);
				
			}
			
			for(NotaFiscalServicoItem notaFSI : nfs.getListaItens()){
				if(notaFSI.getCdmaterialrequisicao() != null && notaFSI.getCdNotaFiscalServicoItem() != null){
					materialrequisicaoService.updateItemMaterialrequisicaoByItemNota(notaFSI.getCdNotaFiscalServicoItem(), notaFSI.getCdmaterialrequisicao());
				}
			}
			observacaoNota = observacaoNota.deleteCharAt(observacaoNota.lastIndexOf(","));
			notaHistoricoService.adicionaHistoricoEmitidaObservacao(nfs, observacaoNota.toString());
			notaHistoricoService.saveOrUpdate(nfs.getListaNotaHistorico().get(0));
		}
		vendaService.updateStatusVenda(CollectionsUtil.listAndConcatenate(listaRequisicoes, "cdrequisicao", ","));
	}
	
	/**
	 * M�todo que realiza o salvamento das notas fiscais e as altera��es dos estados das requisi��es selecionadas.
	 * 
	 * @param listaRequisicoes
	 * @param listaNotas
	 * 
	 * @author Rafael Salvio
	 */
	public void saveFaturarByAutorizacao(String whereIn, List<NotaFiscalServico> listaNotas, List<Requisicao> listaRequisicoes, boolean medicao){
		Integer proximoNumero = null;
		for(NotaFiscalServico nfs : listaNotas){
			Requisicao requisicao = nfs.getRequisicao();
			Materialrequisicao materialrequisicao = nfs.getMaterialrequisicao();
			
			boolean haveVotorantim = configuracaonfeService.havePrefixoativo(nfs.getEmpresa(), Prefixowebservice.VOTORANTIMISS_PROD, Prefixowebservice.MOGIMIRIMISS_PROD);
			if(!haveVotorantim){
				proximoNumero = empresaService.carregaProxNumNF(nfs.getEmpresa());
				if(proximoNumero == null){
					proximoNumero = 1;
				}
				nfs.setNumero(proximoNumero.toString());
				proximoNumero++;
	
				empresaService.updateProximoNumNF(nfs.getEmpresa(), proximoNumero);
			}
			
			notaFiscalServicoService.saveOrUpdate(nfs);	
			
			StringBuilder observacaoNota = new StringBuilder();
			observacaoNota.append("Gerada a partir da ordem de servi�o: ");
			observacaoNota.append("<a href='/w3erp/servicointerno/crud/Requisicao?ACAO=consultar&cdrequisicao=" + nfs.getWhereIn() + "'>" +  nfs.getWhereIn() + "</a>");
			for(Requisicao r : listaRequisicoes){
				if(r.getCdrequisicao() == Integer.parseInt(nfs.getWhereIn())){
					r.setObservacao( (r.getObservacao()!=null ? r.getObservacao() : "") + nfs.getCdNota() + ",");
					this.updateNotaFiscalServico(r.getCdrequisicao(), nfs.getCdNota());
					break;
				}
			}
			notaHistoricoService.adicionaHistoricoEmitidaObservacao(nfs, observacaoNota.toString());
			notaHistoricoService.saveOrUpdate(nfs.getListaNotaHistorico().get(0));
			
			Requisicaonota requisicaonota = new Requisicaonota();
			requisicaonota.setRequisicao(requisicao);
			requisicaonota.setNota(nfs);
			requisicaonota.setMaterialrequisicao(materialrequisicao);
			requisicaonota.setPercentualfaturado(materialrequisicao.getPercentualafaturar());
			requisicaonotaService.saveOrUpdate(requisicaonota);
		}
		
		for(Requisicao r : listaRequisicoes){
			for(Materialrequisicao mr : r.getListaMateriaisrequisicao()){
				boolean faturado = mr.getFaturado() != null && mr.getFaturado();
				if(!faturado){
					boolean checado = mr.getChecado() != null && mr.getChecado();
					if(medicao){
						Double percentualafaturar = mr.getPercentualafaturar();
						Double percentualfaturado = mr.getPercentualfaturado() != null ? mr.getPercentualfaturado() : 0d;
						
						percentualfaturado += percentualafaturar;
						if(SinedUtil.round(percentualfaturado, 5) >= 100d){
							materialrequisicaoService.updateFaturado(mr.getCdmaterialrequisicao().toString());
						}
						materialrequisicaoService.updatePercentualFaturado(mr, percentualfaturado);
					} else {
						if(checado){
							materialrequisicaoService.updateFaturado(mr.getCdmaterialrequisicao().toString());
						}
					}
				}
			}
			
			
			
			StringBuilder observacaoReq = new StringBuilder();
			observacaoReq.append("Nota(s) Gerada(s): ");
			String[] notas = r.getObservacao().split(",");
			
			for(int i=0; i<notas.length; i++)
				observacaoReq.append("<a href='/w3erp/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=" + notas[i] + "'>" + notas[i] + "</a>, ");
			
			observacaoReq = observacaoReq.deleteCharAt(observacaoReq.lastIndexOf(","));
			requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, observacaoReq.toString(), r));
		}
		
		List<Requisicao> listaRequisicaoUpdate = this.carregaRequisicaoForFaturar(CollectionsUtil.listAndConcatenate(listaRequisicoes, "cdrequisicao", ","));
		for (Requisicao r : listaRequisicaoUpdate) {
			Boolean hasMaterialNaoFaturado = Boolean.FALSE;
			for(Materialrequisicao mr : r.getListaMateriaisrequisicao()){
				if(mr.getFaturado() == null || !mr.getFaturado()){
					if(mr.getPercentualfaturado() != null){
						if(mr.getPercentualfaturado() < 100d){
							hasMaterialNaoFaturado = Boolean.TRUE;
						}
					} else {
						hasMaterialNaoFaturado = Boolean.TRUE;
					}
				}
			}
			
			if(!hasMaterialNaoFaturado)
				this.updateEstado(new Requisicaoestado(Requisicaoestado.FATURADO), r.getCdrequisicao().toString());
		}
		
		vendaService.updateStatusVenda(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#existsMaterial(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existsMaterial(String whereIn) {
		return requisicaoDAO.existsMaterial(whereIn);
	}
	
	public void registrarSaidaAposConclusao(String whereIn, Localarmazenagem localarmazenagem, List<Requisicao> listaRequisicao) {
		List<Requisicao> lista = SinedUtil.isListNotEmpty(listaRequisicao) ? listaRequisicao : this.findForRegistrarsaida(whereIn);
		
		if(SinedUtil.isListNotEmpty(lista) && localarmazenagem != null){
			Movimentacaoestoque movimentacaoestoque;
			Movimentacaoestoqueorigem movimentacaoestoqueorigem;
			Date dtmovimentacao = new Date(System.currentTimeMillis());
			for(Requisicao requisicao : lista){
				List<Integer> idsMoviemtnacaoestoque = new ArrayList<Integer>();
				if(requisicao.getListaMateriaisrequisicao() != null && !requisicao.getListaMateriaisrequisicao().isEmpty()){
					for(Materialrequisicao item : requisicao.getListaMateriaisrequisicao()){
						Material material = materialService.getMaterialmestreGradeOrItemGrade(item.getMaterial());
						if(material != null && (material.getServico() == null || !material.getServico())){
							movimentacaoestoque = new Movimentacaoestoque();
							
							movimentacaoestoque.setEmpresa(requisicao.getEmpresa());
							movimentacaoestoque.setMaterial(material);
							movimentacaoestoque.setValor(item.getValorunitario());
							movimentacaoestoque.setQtde(item.getQuantidade() != null ? item.getQuantidade().doubleValue() : 1.0);
							movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
							movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
							movimentacaoestoque.setDtmovimentacao(dtmovimentacao);
							
							if(item.getMaterial().getProduto() != null && item.getMaterial().getProduto()){
								movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
							}else if(item.getMaterial().getEpi() != null && item.getMaterial().getEpi()){
								movimentacaoestoque.setMaterialclasse(Materialclasse.EPI);
							}else if(item.getMaterial().getPatrimonio() != null && item.getMaterial().getPatrimonio()){
								movimentacaoestoque.setMaterialclasse(Materialclasse.PATRIMONIO);
							}
							
							movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
							
							movimentacaoestoqueorigem.setRequisicao(requisicao);
							movimentacaoestoqueorigem.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
							movimentacaoestoqueorigem.setDtaltera(new Timestamp(System.currentTimeMillis()));						
							
							movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
							movimentacaoestoqueService.saveOrUpdate(movimentacaoestoque);
							
							idsMoviemtnacaoestoque.add(movimentacaoestoque.getCdmovimentacaoestoque());
							
							movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CRIAR, "CRIADO VIA ORDEM DE SERVI�O " + SinedUtil.makeLinkHistorico(movimentacaoestoqueorigem.getRequisicao().getCdrequisicao().toString(), "visualizarOrdemDeServico"), true);
						}
					}
				}
				
				if(idsMoviemtnacaoestoque != null && idsMoviemtnacaoestoque.size() > 0){
					List<String> listaLinks = new ArrayList<String>();
					for (Integer id : idsMoviemtnacaoestoque) {
						listaLinks.add("<a href=\"javascript:visualizaMovimentacaoestoque(" + id + ")\">" + id + "</a>");
					}
					
					requisicaohistoricoService.saveOrUpdate(new Requisicaohistorico(null, "Baixa no estoque: " + CollectionsUtil.concatenate(listaLinks, ", "), requisicao));
				}
			}
		}		
	}
	
	/**
	 * M�todo abre a expedi��o caso a venda tenha expedi��o pendente
	 *
	 * @param request
	 * @param whereIn
	 * @param localarmazenagem
	 * @author Luiz Fernando
	 */
	public void gerarExpedicaoAposConclusao(WebRequestContext request, String whereIn, Localarmazenagem localarmazenagem, List<Requisicao> lista) {
//		List<Requisicao> lista = this.findForRegistrarsaida(whereIn);
		if(lista != null && !lista.isEmpty()){
			StringBuilder whereInVenda = new StringBuilder(); 
			for(Requisicao requisicao : lista){
				if(requisicao.getVenda() != null && requisicao.getVenda().getCdvenda() != null){
					if(!"".equals(whereInVenda.toString())) whereInVenda.append(",");
					whereInVenda.append(requisicao.getVenda().getCdvenda());
				}
			}
			if(!"".equals(whereInVenda.toString())){
				boolean notValido = vendaService.haveVendaNotInExpedicaoSituacao(whereInVenda.toString(), Expedicaovendasituacao.EXPEDICAO_PENDENTE, Expedicaovendasituacao.EXPEDICAO_PARCIAL);
				if(notValido){
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
					return;
				}
				
				List<Venda> listaVendaPedidocomodata = vendaService.findPedidovendacomodato(whereInVenda.toString());
				List<Venda> listaVendaPedidovendaorigem = vendaService.findPedidovendaorigem(whereInVenda.toString());
				boolean erro = false;
				if(listaVendaPedidocomodata != null && !listaVendaPedidocomodata.isEmpty()){
					boolean existvendapedidovendacomodato = false;
					for(Venda venda : listaVendaPedidocomodata){
						existvendapedidovendacomodato = false;
						if(listaVendaPedidovendaorigem != null && !listaVendaPedidovendaorigem.isEmpty()){
							for(Venda venda2 : listaVendaPedidovendaorigem){
								if(venda.getPedidovenda().getPedidovendaorigem().getCdpedidovenda().equals(venda2.getPedidovenda().getCdpedidovenda())){
									existvendapedidovendacomodato = true;
									break;
								}
							}
						}
						
						if(!existvendapedidovendacomodato){
							erro = true;
							request.addError("A venda " + venda.getCdvenda() + " foi criada a partir de um pedido de comodato, " +
									"portanto a venda criada a partir do pedido de origem " + venda.getPedidovenda().getPedidovendaorigem().getCdpedidovenda() + 
									" deve pertencer � mesma expedi��o.");
						}
					}
				}else if(listaVendaPedidovendaorigem != null && !listaVendaPedidovendaorigem.isEmpty()){
					erro = true;
					for(Venda venda : listaVendaPedidovendaorigem){
						request.addError("A venda " + venda.getCdvenda() + " foi criada a partir de um pedido informado " +
								"como pedido de origem. Portanto, a venda originada do pedido " + venda.getPedidovenda().getCdpedidovenda() + " deve pertencer � mesma expedi��o.");
					}
				}
				
				if(erro){
					SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
					return;
				}
				
				SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao?ACAO=criar&vendas=" + whereInVenda.toString() + (localarmazenagem != null && localarmazenagem.getCdlocalarmazenagem() != null ? "&cdlocalarmazenagem=" + localarmazenagem.getCdlocalarmazenagem() : "") );
				return;
			} else {
				this.registrarSaidaAposConclusao(whereIn, localarmazenagem, null);
			}
		}		
		SinedUtil.redirecionamento(request, "/servicointerno/crud/Requisicao");
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#findForRegistrarsaida(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForRegistrarsaida(String whereIn) {
		return requisicaoDAO.findForRegistrarsaida(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#findForCancelamentoNFS(String whereInNFSI)
	 *
	 * @param whereInNFSI
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicao> findForCancelamentoNFS(String whereIn) {
		return requisicaoDAO.findForCancelamentoNFS(whereIn);
	}
	
	/**
	 * M�todo que atualiza a situa��o da requisi��o quando a nota � cancelada
	 *
	 * @param whereInNota
	 * @author Luiz Fernando
	 */
	public void atualizaSituacaoRequisicaoCancelamentoNFS(String whereInNota){
		List<Requisicaonota> listaRequisicaonota = requisicaonotaService.findByNota(whereInNota);
		
		if(listaRequisicaonota != null && listaRequisicaonota.size() > 0){
			String whereInMaterialrequisicao = SinedUtil.listAndConcatenate(listaRequisicaonota, "materialrequisicao.cdmaterialrequisicao", ",");
			String whereInRequisicao = SinedUtil.listAndConcatenate(listaRequisicaonota, "requisicao.cdrequisicao", ",");
			
			if(whereInMaterialrequisicao != null && !whereInMaterialrequisicao.trim().equals("")){
				materialrequisicaoService.updateMaterialrequisicaoFaturado(whereInMaterialrequisicao);
			}
			
			if(whereInRequisicao != null && !whereInRequisicao.trim().equals("")){
				List<Requisicao> listaRequisicao = this.findForCancelamentoNFS(whereInRequisicao);
				
				if(listaRequisicao != null){
					for(Requisicao requisicao : listaRequisicao){
						if(requisicao.getListaMateriaisrequisicao() != null && !requisicao.getListaMateriaisrequisicao().isEmpty()){
							Integer qtde = 0;
							for(Materialrequisicao materialrequisicao : requisicao.getListaMateriaisrequisicao()){
								if(materialrequisicao.getFaturado() == null || !materialrequisicao.getFaturado()){
									qtde++;
								}
							}
							if(qtde > 0){
								if(qtde < requisicao.getListaMateriaisrequisicao().size()){
									this.updateEstado(new Requisicaoestado(Requisicaoestado.FATURADO_PARCIALMENTE), requisicao.getCdrequisicao().toString());
								}else {
									this.updateEstado(new Requisicaoestado(Requisicaoestado.CONCLUIDA), requisicao.getCdrequisicao().toString());
								}
							}
						}
					}
				}
			}
		}
	}
	
	public List<Requisicao> findForApontamento(String autocomplete){
		return requisicaoDAO.findForApontamento(autocomplete);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Requisicao> findForEmitirOS(String whereIn){
		return requisicaoDAO.findForEmitirOS(whereIn);
	}
	
	/**
	 * M�todo que retorna true caso a OS esteja faturada e tem origem de contrato que fatura por hora trabalhada
	 *
	 * @param requisicao
	 * @return
	 * @author Luiz Fernando
	 * @since 13/12/2013
	 */
	public Boolean isFaturadaByContrato(Requisicao requisicao) {
		Boolean retorno = false;
		if(requisicao != null && requisicao.getCdrequisicao() != null && requisicao.getRequisicaoestado() != null &&
				requisicao.getRequisicaoestado().getCdrequisicaoestado() != null &&
				Requisicaoestado.FATURADO.equals(requisicao.getRequisicaoestado().getCdrequisicaoestado()) &&
				requisicao.getContrato() != null && requisicao.getContrato().getCdcontrato() != null){
			Contrato contrato = contratoService.loadWithContratotipo(requisicao.getContrato());
			if(contrato != null && contrato.getContratotipo() != null && contrato.getContratotipo().getFaturarhoratrabalhada() != null &&
					contrato.getContratotipo().getFaturarhoratrabalhada()){
				retorno = true;
			}			
		}
		return retorno;
	}

	public Money calculaTotal(RequisicaoFiltro filtro) {
		return requisicaoDAO.calculaTotal(filtro); 
	}
	
	public Double calculaTotalHorasTrabalhadas(RequisicaoFiltro filtro){
		return requisicaoDAO.calculaTotalHorasTrabalhadas(filtro);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @param cdrequisicao
	 * @param cdnota
	 * @author Lucas Costa
	 */
	public void updateNotaFiscalServico(Integer cdrequisicao, Integer cdnota) {
		requisicaoDAO.updateNotaFiscalServico(cdrequisicao, cdnota);
	}
	
	public void updateQtde(Requisicao requisicao) {
		requisicaoDAO.updateQtde(requisicao);
	}
	
	public void limparReferenciasForCopiar(Requisicao requisicao) {
		if(requisicao != null){
			requisicao.setCdrequisicao(null);
			requisicao.setRequisicaoestado(new Requisicaoestado(Requisicaoestado.EMESPERA));
			
			if(requisicao.getListaItem() != null && !requisicao.getListaItem().isEmpty()){
				for(Requisicaoitem ri : requisicao.getListaItem()){
					ri.setCdrequisicaoitem(null);
				}
			}
			if(requisicao.getListaMateriaisrequisicao() != null && !requisicao.getListaMateriaisrequisicao().isEmpty()){
				for(Materialrequisicao mr : requisicao.getListaMateriaisrequisicao()){
					mr.setCdmaterialrequisicao(null);
					mr.setFaturado(null);
					mr.setPercentualfaturado(null);
				}
			}
			requisicao.setListaApontamento(new ListSet<Apontamento>(Apontamento.class));
			requisicao.setListaApontamento2(new ListSet<Apontamento>(Apontamento.class));
			requisicao.setListaRequisicaohistorico(null);
		}
		
	}
	
	public boolean unicoCliente(String whereIn) {
		if(whereIn != null && !whereIn.isEmpty()){
			Map<Integer, String> mapaCliente = new HashMap<Integer, String>();
			for(String item : whereIn.split(",")){
				Integer cdcliente = Integer.parseInt(item);
				if (mapaCliente.containsKey(cdcliente)) {
					return false;
				} else{
					mapaCliente.put(cdcliente, item);
				}
			}
		}
		return true;
	}
	
	/**
	 * M�todo de valida��o de requisi��es para faturamento de produto/servi�o
	 * 
	 * @param whereIn
	 * @param tipofaturamento
	 * @return
	 * @author Rafael Salvio
	 */
	public String validaRequisicaoForFaturarNotaFiscal(String whereIn, String tipofaturamento) {
		String error = null;
		if(whereIn != null && !whereIn.trim().isEmpty()){
			List<Requisicao> lista = loadWithListaMaterial(whereIn);
			Boolean hasProduto = Boolean.FALSE;
			Boolean hasServico = Boolean.FALSE;
			if(lista != null && !lista.isEmpty()){
				Cliente cliente = lista.get(0).getCliente();
				for(Requisicao requisicao : lista){
					if(!requisicao.getCliente().equals(cliente)){
						error = "N�o � poss�vel faturar os " + tipofaturamento.toLowerCase() + "s de ordens de servi�o de clientes diferentes.";
						break;
					}
					if(!(Requisicaoestado.VISTO.equals(requisicao.getRequisicaoestado().getCdrequisicaoestado()) 
									|| Requisicaoestado.CONCLUIDA.equals(requisicao.getRequisicaoestado().getCdrequisicaoestado()))){
						error = "Somente � poss�vel faturar ordens de servi�o cujo estado seja 'VISTO' ou 'CONCLU�DO'.";
						break;
					}
					if(requisicao.getListaMateriaisrequisicao() != null && !requisicao.getListaMateriaisrequisicao().isEmpty()){
						for(Materialrequisicao mr : requisicao.getListaMateriaisrequisicao()){
							if(mr.getMaterial() != null){
								if(mr.getMaterial().getProduto() != null && mr.getMaterial().getProduto()){
									hasProduto = Boolean.TRUE;
								}
								if((mr.getMaterial().getServico() != null && mr.getMaterial().getServico()) || 
										(mr.getMaterial().getProduto() != null && mr.getMaterial().getProduto() && mr.getMaterial().getTributacaomunicipal() != null && mr.getMaterial().getTributacaomunicipal())){
									hasServico = Boolean.TRUE;
								}
							}
						}
					}
				}
			}
			if(error == null){
				if(("produto".equalsIgnoreCase(tipofaturamento) && !hasProduto)){
					error = "Para faturar a ordem de servi�o, ela deve possuir pelo menos um " + tipofaturamento.toLowerCase() + ".";
				}
				if(("servico".equalsIgnoreCase(tipofaturamento) && !hasServico)){
					error = "Para faturar a ordem de servi�o, ela deve possuir pelo menos um servi�o.";
				}
			}
		}
		return error;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @author Rafael Salvio
	 */
	public List<Requisicao> loadWithListaMaterial(String whereIn){
		return requisicaoDAO.loadWithListaMaterial(whereIn);
	}
	
	/**
	 * Carrega a Lista de Requisi��es para Emiss�o de Nota Fiscal de Produtos
	 * 
	 * @param whereIn
	 * @author Marcos Lisboa
	 */
	public List<Requisicao> loadForEmitirNotaFiscalProduto(String whereIn, String tipoFaturamento){
		return requisicaoDAO.loadForEmitirNotaFiscalProduto(whereIn, tipoFaturamento);
	}
	
	/**
	 * M�todo que persiste o Hist�rico de Faturamento para Lista Requisicao 
	 *
	 * @param whereIn Requisicao
	 * @param cdNota
	 * @author Marcos Lisboa
	 */
	public void saveHistoricoRequisicao(String whereInOSV, Integer cdNota, boolean servico) {
		if (whereInOSV != null && cdNota != null && !whereInOSV.isEmpty()) {
			for (String strIdOS : whereInOSV.split(",")) {
				Requisicaohistorico historico = new Requisicaohistorico();
				historico.setRequisicao(new Requisicao(Integer.parseInt(strIdOS)));
				if(servico){
					historico.setObservacao("Nota de servi�o gerada: <a href='http://" + SinedUtil.getUrlWithoutContext() + "/w3erp/faturamento/crud/NotaFiscalServico?ACAO=consultar&cdNota=" + cdNota + "'>" +  cdNota + "</a> ");
				}else {
					historico.setObservacao("Nota de produto gerada: <a href='http://" + SinedUtil.getUrlWithoutContext() + "/w3erp/faturamento/crud/Notafiscalproduto?ACAO=consultar&cdNota=" + cdNota + "'>" +  cdNota + "</a> ");
				}
				requisicaohistoricoService.saveOrUpdate(historico);
			}
		}
	}
	
	/**
	 * Atualiza as vendas como Faturadas na Emiss�o de Nota Fiscal de Produto da Requisi��o 
	 *
	 * @param whereIn Requisicao
	 * @author Marcos Lisboa
	 */
	public void updateVendaFaturado(String whereInRequisicao) {
		List<Requisicao> lista = loadForEmitirNotaFiscalProduto(whereInRequisicao, null);
		if(lista != null){
			for(Requisicao requisicao: lista){
				if (requisicao != null && requisicao.getVenda() != null) {
					vendaService.updateStatusVenda(requisicao.getVenda(), Vendasituacao.FATURADA);
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 */
	public List<Requisicao> findForEnviarEmail(String whereIn) {
		return requisicaoDAO.findForEnviarEmail(whereIn);
	}
	
	public List<Requisicao> getListaRequisicoesFaturarPorAutorizacao(List<Requisicao> listaRequisicoes,	List<Requisicao> listaRequisicoesFaturarPorAutorizacao, boolean medicao) {
		List<Requisicao> listaNova = new ArrayList<Requisicao>();
		
		if(SinedUtil.isListNotEmpty(listaRequisicoes) &&  SinedUtil.isListNotEmpty(listaRequisicoesFaturarPorAutorizacao)){
			for(Requisicao bean : listaRequisicoes){
				if(bean.getCdrequisicao() != null && SinedUtil.isListNotEmpty(bean.getListaMateriaisrequisicao())){
					List<Materialrequisicao> listaItem = new ArrayList<Materialrequisicao>();
					for(Materialrequisicao itemBean : bean.getListaMateriaisrequisicao()){
						if(itemBean.getCdmaterialrequisicao() != null){
							for(Requisicao requisicaoPorAutorizacao : listaRequisicoesFaturarPorAutorizacao){
								if(requisicaoPorAutorizacao.getCdrequisicao() != null && 
										requisicaoPorAutorizacao.getCdrequisicao().equals(bean.getCdrequisicao()) &&
										SinedUtil.isListNotEmpty(requisicaoPorAutorizacao.getListaMateriaisrequisicao())){
									for(Materialrequisicao itemPorAutorizacao : requisicaoPorAutorizacao.getListaMateriaisrequisicao()){
										if(itemPorAutorizacao.getCdmaterialrequisicao() != null && 
												itemPorAutorizacao.getCdmaterialrequisicao().equals(itemBean.getCdmaterialrequisicao())){
											boolean checado = itemPorAutorizacao.getChecado() != null && itemPorAutorizacao.getChecado();
											boolean havePercentual = itemPorAutorizacao.getPercentualafaturar() != null;
											if((medicao && havePercentual) || checado){
												itemBean.setChecado(itemPorAutorizacao.getChecado());
												itemBean.setDtvencimento(itemPorAutorizacao.getDtvencimento());
												itemBean.setPercentualafaturar(itemPorAutorizacao.getPercentualafaturar());
												listaItem.add(itemBean);
												break;
											}
										}
									}
								}
							}
						}
					}
					if(SinedUtil.isListNotEmpty(listaItem)){
						bean.setListaMateriaisrequisicao(listaItem);
						listaNova.add(bean);
					}
				}
			}
		}
		return listaNova;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.RequisicaoService#findAutocompleteForAgendaServico(String whereIn, List<Requisicaoestado> listaRequisicaoestado)
	*
	* @param whereIn
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findAutocompleteForAgendaServicoEntrada(String whereIn) {
		List<Requisicaoestado> listaRequisicaoestado = new ArrayList<Requisicaoestado>();
		listaRequisicaoestado.add(new Requisicaoestado(Requisicaoestado.CONCLUIDA));
		listaRequisicaoestado.add(new Requisicaoestado(Requisicaoestado.CANCELADA));
		return findAutocompleteForAgendaServico(whereIn, listaRequisicaoestado);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.RequisicaoService#findAutocompleteForAgendaServico(String whereIn, List<Requisicaoestado> listaRequisicaoestado)
	*
	* @param whereIn
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findAutocompleteForAgendaServico(String whereIn) {
		return findAutocompleteForAgendaServico(whereIn, null);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.RequisicaoService#findAutocompleteForAgendaServico(String whereIn, Cliente cliente, Colaborador colaborador, Empresa empresa, List<Requisicaoestado> listaRequisicaoestado)
	*
	* @param whereIn
	* @param listaRequisicaoestado
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findAutocompleteForAgendaServico(String whereIn, List<Requisicaoestado> listaRequisicaoestado) {
		Empresa empresa = null;
		Cliente cliente = null;
		Colaborador colaborador = null;
		
		if (NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_AGENDASERVICO") != null){
			try {
				empresa = new Empresa((Integer) NeoWeb.getRequestContext().getSession().getAttribute("EMPRESA_FILTRO_AGENDASERVICO"));
			} catch (Exception e) {}
		}
		if (NeoWeb.getRequestContext().getSession().getAttribute("CLIENTE_FILTRO_AGENDASERVICO") != null){
			try {
				cliente = new Cliente((Integer) NeoWeb.getRequestContext().getSession().getAttribute("CLIENTE_FILTRO_AGENDASERVICO"));
			} catch (Exception e) {}
		}
		if (NeoWeb.getRequestContext().getSession().getAttribute("COLABORADOR_FILTRO_AGENDASERVICO") != null){
			try {
				colaborador = new Colaborador((Integer) NeoWeb.getRequestContext().getSession().getAttribute("COLABORADOR_FILTRO_AGENDASERVICO"));
			} catch (Exception e) {}
		}
		
		return findAutocompleteForAgendaServico(whereIn, cliente, colaborador, empresa, listaRequisicaoestado);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#findAutocompleteForAgendaServico(String whereIn, Cliente cliente, Colaborador colaborador, Empresa empresa, List<Requisicaoestado> listaRequisicaoestado)
	*
	* @param whereIn
	* @param cliente
	* @param colaborador
	* @param empresa
	* @param listaRequisicaoestado
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findAutocompleteForAgendaServico(String whereIn, Cliente cliente, Colaborador colaborador, Empresa empresa, List<Requisicaoestado> listaRequisicaoestado) {
		return requisicaoDAO.findAutocompleteForAgendaServico(whereIn, cliente, colaborador, empresa, listaRequisicaoestado);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaoDAO#findForAgendaServicoListagemFlex(RequisicaoFiltro filtro)
	*
	* @param requisicaoFiltro
	* @return
	* @since 01/07/2015
	* @author Luiz Fernando
	*/
	public List<Requisicao> findForAgendaServicoListagemFlex(RequisicaoFiltro requisicaoFiltro) {
		return requisicaoDAO.findForAgendaServicoListagemFlex(requisicaoFiltro); 
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param agendainteracao
	 * @return
	 * @author C�sar
	 */
	public List<Requisicao> findForAgendaInteracaoHistorico(Agendainteracao agendainteracao){
		return requisicaoDAO.findForAgendaInteracaoHistorico(agendainteracao);
	}
	
	public void preencheRequisicaoByContrato(Requisicao bean, Contrato contrato){
		
		Contrato contratoBean = contratoService.loadContratoByCriarRequisicao(contrato);
		if(contratoBean != null){
			bean.setDescricao(contratoBean.getDescricao());
			bean.setCliente(contratoBean.getCliente());
			bean.setEndereco(contratoBean.getEndereco());
			bean.setEmpresa(contratoBean.getEmpresa());
			bean.setColaboradorresponsavel(contratoBean.getResponsavel());
			bean.setListaMateriaisrequisicao(new ArrayList<Materialrequisicao>());
			
			if(contratoBean.getCliente() != null && SinedUtil.isListNotEmpty(contratoBean.getCliente().getListaContato())){
				for(PessoaContato pessoaContato : contratoBean.getCliente().getListaContato()) {
					Contato contato = pessoaContato.getContato();
					
					if(Boolean.TRUE.equals(contato.getResponsavelos())){
						bean.setContato(contato);
						break;
					}
				}
			}
			Integer count = 1;
			for(Contratomaterial contratomaterial: contratoBean.getListaContratomaterial()){
				Materialrequisicao item = new Materialrequisicao();
				
				item.setItem(count.toString());
				item.setMaterial(contratomaterial.getServico());
				item.setQuantidade(contratomaterial.getQtde());
				if(contratomaterial.getQtde() > 0)
					item.setValorunitario(contratomaterial.getValortotal().getValue().doubleValue() / contratomaterial.getQtde());
				item.setValortotal(contratomaterial.getValortotal().getValue().doubleValue());
				item.setObservacao(contratomaterial.getObservacao());
				
				bean.getListaMateriaisrequisicao().add(item);
				
				count++;
			}
		}
	}
	public void criarAvisoRequisicaoAtrasada(Motivoaviso m, Date data, Date dateToSearch) {
		List<Requisicao> requisicaoList = requisicaoDAO.findByAtrasado(data, dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
//		List<Avisousuario> avisoUsuarioList = null;
		List<Usuario> supervisores =  new ArrayList<Usuario>();
		Empresa empresa = empresaService.loadPrincipal();
		for (Requisicao r : requisicaoList) {
			Usuario responsavel  = null;
			Aviso aviso = new Aviso("Ordem de servi�o atrasada", "C�digo da ordem de servi�o: " + r.getCdrequisicao(), 
					m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), r.getCdrequisicao(), r.getEmpresa() != null ? r.getEmpresa() : empresa, 
					SinedDateUtils.currentDate(), m, Boolean.FALSE);
			Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, r.getCdrequisicao());
			
			Usuario supervisor =null;
			if(r.getColaboradorresponsavel() !=null){
				Colaborador colaborador = r.getColaboradorresponsavel();
				responsavel = usuarioService.findByCdpessoa(colaborador.getCdpessoa());
				colaborador = colaboradorService.carregaColaboradorCargoAtual(colaborador);
				Departamento dep = colaborador.getDepartamentotransient();
				if(dep.getResponsavel() ==null){
					dep = departamentoService.load(dep);
				}
				if(dep.getResponsavel() !=null){
					dep = departamentoService.retornarResponsavel(dep.getCddepartamento());
					supervisor = usuarioService.findByCdpessoa(dep.getResponsavel().getCdpessoa());
					supervisores.add(supervisor);
				}
			}
			List<Usuario> listaUsuario = new ArrayList<Usuario>();
			if(supervisor !=null){
				listaUsuario.add(supervisor);
			}
			if(responsavel !=null){
				listaUsuario.add(responsavel);
			}
			if (lastAvisoDate != null) {
//				avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, r.getCdrequisicao(), lastAvisoDate);
//				List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);	
				
				if (SinedUtil.isListNotEmpty(listaUsuario)) {
					aviso.setListaEnvolvidos(listaUsuario);
					avisoService.salvarAvisos(aviso, listaUsuario, false);
				}
			} else {
				aviso.setListaEnvolvidos(listaUsuario);
				avisoList.add(aviso);
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	public void processarListagem(ListagemResult<Requisicao> listagemResult, RequisicaoFiltro filtro) {
		List<Requisicao> list = listagemResult.list();
		
		String param = parametrogeralService.getValorPorNome(Parametrogeral.W3MPSBR_INTEGRACAO);
		if(param != null && param.toUpperCase().equals("TRUE")){
			String whereIn = CollectionsUtil.listAndConcatenate(list, "cdrequisicao", ",");
			
			if(whereIn != null && !whereIn.equals("")){
				Map<Integer, String> map = buscaSituacoesAutorizacoes(whereIn);
				
				if(map != null){
					for (Requisicao requisicao : list) {
						if(map.containsKey(requisicao.getCdrequisicao())){
							requisicao.setSituacao(map.get(requisicao.getCdrequisicao()));
						}
					}
				}
			}
		}
		
		for (Requisicao requisicao : list) {
			if(requisicao.getCliente() != null && requisicao.getCliente().getCdpessoa() != null){
				Date dtvencimento = documentoService.getPrimeiroVencimentoDocumentoAtrasado(requisicao.getCliente());	
				requisicao.setPendenciafinanceira(dtvencimento != null);
			}
		}
		
		if(filtro.getExibirTotais() != null && filtro.getExibirTotais()){
			Double horasTrabalhadas = calculaTotalHorasTrabalhadas(filtro); 
			if(horasTrabalhadas == null || horasTrabalhadas == 0.00){
				filtro.setTotalHorasTrabalhadas("00:00");
			} else {
				filtro.setTotalHorasTrabalhadas(SinedDateUtils.doubleToFormatoHora(horasTrabalhadas));
			}
			filtro.setTotal(calculaTotal(filtro));
		}
	}
	
	@Override
	protected ListagemResult<Requisicao> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Requisicao> listagemResult = super.findForExportacao(filtro);
		processarListagem(listagemResult, (RequisicaoFiltro) filtro);
		
		return listagemResult;
	}
	
	public ModelAndView abrirPopUpInformacaoClienteOrdemServico(WebRequestContext request) {
		Cliente cliente = null;
		String cdpessoa = request.getParameter("cdpessoa");
		
		if(cdpessoa != null && !cdpessoa.equals("")) {
			cliente = new Cliente(Integer.parseInt(cdpessoa));
			cliente = clienteService.load(cliente, "cliente.cdpessoa, cliente.informacaoOrdemServico");
		}
		return new ModelAndView("direct:/crud/popup/popUpInformacaoClienteOrdemServico", "bean", cliente);
	}
	
	public ModelAndView ajaxBuscaInformacaoClienteOrdemServico(WebRequestContext request) {
		String informacaoOrdemServico = "";
		String cdpessoa = request.getParameter("cdpessoa");
		
		if(cdpessoa != null && !cdpessoa.equals("")) {
			Cliente cliente = clienteService.load(new Cliente(Integer.parseInt(cdpessoa)), "cliente.cdpessoa, cliente.informacaoOrdemServico");
			if(cliente.getInformacaoOrdemServico() !=  null) {
				informacaoOrdemServico = cliente.getInformacaoOrdemServico();
			}
		}
		
		informacaoOrdemServico = new br.com.linkcom.neo.util.StringUtils().addScapesToDescription(informacaoOrdemServico);
		return new JsonModelAndView().addObject("informacaoOrdemServico", informacaoOrdemServico);
	}
}