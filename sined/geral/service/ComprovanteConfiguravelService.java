package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel.TipoComprovante;
import br.com.linkcom.sined.geral.dao.ComprovanteConfiguravelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ComprovanteConfiguravelService extends GenericService<ComprovanteConfiguravel> {

	private ComprovanteConfiguravelDAO comprovanteConfiguravelDAO;
	public void setComprovanteConfiguravelDAO(ComprovanteConfiguravelDAO comprovanteConfiguravelDAO) {this.comprovanteConfiguravelDAO = comprovanteConfiguravelDAO;}
	
	private static ComprovanteConfiguravelService instance;
	public static ComprovanteConfiguravelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ComprovanteConfiguravelService.class);
		}
		return instance;
	}
	
	public List<ComprovanteConfiguravel> findAtivosByPedidovenda(){
		return comprovanteConfiguravelDAO.findAtivosByPedidovenda();
	}
	
	public List<ComprovanteConfiguravel> findAtivosByVenda(){
		return comprovanteConfiguravelDAO.findAtivosByVenda();
	} 
	
	public List<ComprovanteConfiguravel> findAtivosByOrcamento(){
		return comprovanteConfiguravelDAO.findAtivosByOrcamento();
	} 
	
	public List<ComprovanteConfiguravel> findAtivosByColeta(){
		return comprovanteConfiguravelDAO.findAtivosByColeta();
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ComprovanteConfiguravelDAO#findComprovanteAlternativo(String whereIn, TipoComprovante tipoComprovante)
	*
	* @param whereIn
	* @param tipoComprovante
	* @return
	* @since 31/01/2017
	* @author Luiz Fernando
	*/
	public List<ComprovanteConfiguravel> findComprovanteAlternativo(String whereIn, TipoComprovante tipoComprovante) {
		return comprovanteConfiguravelDAO.findComprovanteAlternativo(whereIn, tipoComprovante);
	}	
}
