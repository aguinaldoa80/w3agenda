package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Processojuridicoquestionario;
import br.com.linkcom.sined.geral.dao.ProcessojuridicoquestionarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessojuridicoquestionarioService extends GenericService<Processojuridicoquestionario> {

	private ProcessojuridicoquestionarioDAO processojuridicoquestionarioDAO;
	
	public void setProcessojuridicoquestionarioDAO(ProcessojuridicoquestionarioDAO processojuridicoquestionarioDAO) {
		this.processojuridicoquestionarioDAO = processojuridicoquestionarioDAO;
	}
	
	/**
	 * 
	 * @param whereIn
	 * @author Thiago Clemente
	 * 
	 */
	public List<Processojuridicoquestionario> findForFichaProcessoJuridico(String whereIn) {
		return processojuridicoquestionarioDAO.findForFichaProcessoJuridico(whereIn);
	}
}
