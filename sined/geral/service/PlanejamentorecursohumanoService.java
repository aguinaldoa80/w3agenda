package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Planejamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Tarefarecursohumano;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.dao.PlanejamentorecursohumanoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.AnaliseRecDespBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.filter.OrcamentoAnaliticoReportFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PlanejamentorecursohumanoService extends GenericService<Planejamentorecursohumano>{

	private PlanejamentorecursohumanoDAO planejamentorecursohumanoDAO;
	
	public void setPlanejamentorecursohumanoDAO(
			PlanejamentorecursohumanoDAO planejamentorecursohumanoDAO) {
		this.planejamentorecursohumanoDAO = planejamentorecursohumanoDAO;
	}
	
	public List<Planejamentorecursohumano> preencheListaPlanRecHumano(Collection<Tarefarecursohumano> lista){
		List<Planejamentorecursohumano> listaPlan = new ArrayList<Planejamentorecursohumano>();
		Planejamentorecursohumano pg = null;
		for (Tarefarecursohumano tg : lista) {
			pg = new Planejamentorecursohumano();
			pg.setCdplanejamentorecursohumano(tg.getCdtarefarecursohumano().equals(0) ? null : tg.getCdtarefarecursohumano());
			pg.setCargo(tg.getCargo());
			pg.setQtde(tg.getQtde());
			
			listaPlan.add(pg);
		}
		return listaPlan;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentorecursohumanoDAO#findByPlanejamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursohumano> findByPlanejamento(Planejamento planejamento) {
		return planejamentorecursohumanoDAO.findByPlanejamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de referÍncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursohumanoService#findByPlanejamento
	 * @param planejamento
	 * @param listaReferencia
	 * @param listaTodas
	 * @author Rodrigo Freitas
	 */
	public void valoresPlanejamentoHumano(Planejamento planejamento, List<Valorreferencia> listaReferencia, List<Valorreferencia> listaTodas) {
		Valorreferencia valorreferencia;
		List<Planejamentorecursohumano> listaPlanejamentoHumano = this.findByPlanejamento(planejamento);
		for (Planejamentorecursohumano planejamentorecursohumano : listaPlanejamentoHumano) {
			valorreferencia = new Valorreferencia();
			valorreferencia.setCargo(planejamentorecursohumano.getCargo());
			valorreferencia.setPlanejamento(planejamento);
			valorreferencia.setValor(planejamentorecursohumano.getCargo().getCustohora());
			
			if (!listaTodas.contains(valorreferencia) && !listaReferencia.contains(valorreferencia)) {
				listaReferencia.add(valorreferencia);
			}
		}
	}

	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.PlanejamentorecursohumanoDAO#findForOrcamento
	 * @param planejamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Planejamentorecursohumano> findForOrcamento(Planejamento planejamento) {
		return planejamentorecursohumanoDAO.findForOrcamento(planejamento);
	}
	
	/**
	 * Preenche a lista de valores de refrÍncia a partir de uma lista de planejamentoRecursoHumano.
	 *
	 * @see br.com.linkcom.sined.geral.service.PlanejamentorecursohumanoService#findForOrcamento
	 * @see br.com.linkcom.sined.geral.bean.Valorreferencia#getValor
	 * @param filtro
	 * @param listaFolhas
	 * @param listaOutrosCargo
	 * @param somacargo
	 * @param listaValorReferencia
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Money preencheListaVRPlanejamentoCar(OrcamentoAnaliticoReportFiltro filtro, List<AnaliseRecDespBean> listaFolhas,
			List<AnaliseRecDespBean> listaOutrosCargo, Money somacargo, List<Valorreferencia> listaValorReferencia) {
		AnaliseRecDespBean bean;
		Money valor;
		List<Planejamentorecursohumano> listaPlanejamentoHumano = this.findForOrcamento(filtro.getPlanejamento());
		for (Planejamentorecursohumano planejamentorecursohumano : listaPlanejamentoHumano) {
			bean = new AnaliseRecDespBean();
			if (planejamentorecursohumano.getCargo().getContagerencial() != null) {
				bean.setIdentificador(planejamentorecursohumano.getCargo().getContagerencial().getVcontagerencial().getIdentificador());
				bean.setNome(planejamentorecursohumano.getCargo().getContagerencial().getNome());
				bean.setOperacao(planejamentorecursohumano.getCargo().getContagerencial().getTipooperacao().getSigla());
				valor = Valorreferencia.getValor(planejamentorecursohumano.getCargo(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(planejamentorecursohumano.getQtde())) : new Money());
				
				listaFolhas.add(bean);
			} else {
				bean.setIdentificador("XX.XX.XX");
				bean.setNome(planejamentorecursohumano.getCargo().getNome());
				bean.setOperacao("D");
				valor = Valorreferencia.getValor(planejamentorecursohumano.getCargo(), listaValorReferencia);
				bean.setValor(valor != null ? valor.multiply(new Money(planejamentorecursohumano.getQtde())) :  new Money());
				somacargo = somacargo.add(valor != null ? valor.multiply(new Money(planejamentorecursohumano.getQtde())) :  new Money()); 
				
				listaOutrosCargo.add(bean);
			}
		}
		return somacargo;
	}

}
