package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.dao.DocumentoacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoacaoService extends GenericService<Documentoacao>{

	private DocumentoacaoDAO documentoacaoDAO;
	public void setDocumentoacaoDAO(DocumentoacaoDAO documentoacaoDAO) {
		this.documentoacaoDAO = documentoacaoDAO;
	}
	
	/**
	 * M�todo para obter uma lista de <code>Documentoacao</code> para ser utilizada no filtro de Conta pagar.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoacaoDAO#findAcoesForDocumento()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> findForContapagar(){
		List<Documentoacao> findAcoesForDocumento = documentoacaoDAO.findAcoesForDocumento();
//		findAcoesForDocumento.remove(Documentoacao.NEGOCIADA);
		findAcoesForDocumento.remove(Documentoacao.BAIXADA_PARCIAL);
		return findAcoesForDocumento;
	}
	
	/**
	 * M�todo para obter uma lista de <code>Documentoacao</code> para ser utilizada no filtro de Conta receber.
	 * 
	 * @see #findAcoesForDocumento()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> findForContareceber(){
		List<Documentoacao> findAcoesForDocumento = this.findAcoesForDocumento();
		
		findAcoesForDocumento.remove(Documentoacao.AUTORIZADA);
		findAcoesForDocumento.remove(Documentoacao.AUTORIZADA_PARCIAL);
		findAcoesForDocumento.remove(Documentoacao.NAO_AUTORIZADA);
		
		return findAcoesForDocumento;
	}

	/**
	 * M�todo para montar uma lista de <code>Documentoacao</code> para ser utilizada
	 *  no filtro do relat�rio de fluxo de caixa.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 * @author Hugo Ferreira
	 */
	public List<Documentoacao> findAcoesContaPagar(){
		List<Documentoacao> findAcoesForDocumento = new ArrayList<Documentoacao>();
		
		findAcoesForDocumento.add(Documentoacao.PREVISTA);
		findAcoesForDocumento.add(Documentoacao.DEFINITIVA);
		findAcoesForDocumento.add(Documentoacao.AUTORIZADA);
		findAcoesForDocumento.add(Documentoacao.AUTORIZADA_PARCIAL);
		findAcoesForDocumento.add(Documentoacao.PROTESTADA);
		findAcoesForDocumento.add(Documentoacao.NEGOCIADA);
		
		return findAcoesForDocumento;
	}
	
	/**
	 * M�todo para montar uma lista de <code>Documentoacao</code> para ser utilizada
	 *  no filtro do relat�rio de fluxo de caixa.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 * @author Hugo Ferreira
	 */
	public List<Documentoacao> findAllAcoes(){
		List<Documentoacao> lista = new ArrayList<Documentoacao>();
		
		lista.add(Documentoacao.CANCELADA);
		lista.add(Documentoacao.PREVISTA);
		lista.add(Documentoacao.DEFINITIVA);
		lista.add(Documentoacao.AUTORIZADA);
		lista.add(Documentoacao.AUTORIZADA_PARCIAL);
		lista.add(Documentoacao.NAO_AUTORIZADA);
		lista.add(Documentoacao.BAIXADA);
		lista.add(Documentoacao.BAIXADA_PARCIAL);
		lista.add(Documentoacao.ALTERADA);
		lista.add(Documentoacao.ESTORNADA);
		lista.add(Documentoacao.PROTESTADA);
		lista.add(Documentoacao.CANCEL_MOVIMENTACAO);
		lista.add(Documentoacao.REAGENDAMENTO);
		lista.add(Documentoacao.ATRASADA);
		lista.add(Documentoacao.DEVOLVIDA);
		lista.add(Documentoacao.NEGOCIADA);

		return lista;
	}
	
	/**
	 * M�todo para montar uma lista de <code>Documentoacao</code> para ser utilizada
	 *  no filtro do relat�rio de fluxo de caixa.
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Documentoacao> findAcoesContaReceber(){
		List<Documentoacao> findAcoesForDocumento = new ArrayList<Documentoacao>();
		
		findAcoesForDocumento.add(Documentoacao.PREVISTA);
		findAcoesForDocumento.add(Documentoacao.DEFINITIVA);
		findAcoesForDocumento.add(Documentoacao.PROTESTADA);
		
		return findAcoesForDocumento;
	}
	
	/**
	 * Retorna os nomes das A��es de Conta, separados por v�rgula, presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 * 
	 * @see #findAllAcoes()
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public String getNomeAcoes(List<Documentoacao> lista) {
		if (lista == null || lista.isEmpty()) return null;
		
		List<Documentoacao> listaCheia = this.findAllAcoes();
		List<Documentoacao> listaRetornar = new ArrayList<Documentoacao>();
		
		for (Documentoacao documentoacao : listaCheia) {
			if (lista.contains(documentoacao)) {
				listaRetornar.add(documentoacao);
			}
		}
		
		return CollectionsUtil.listAndConcatenate(listaRetornar, "nome", ", ");
	}
	
	/**
	 * M�todo para obter as a��es que ser�o exibidas no cadastro de conta a pagar/receber.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoacaoDAO#findAcoesForDocumento()
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> findAcoesForDocumento(){
		return documentoacaoDAO.findAcoesForDocumento();
	}
	
	public String createExceptionMessage(Collection<Documentoacao> listaCondicao){
		StringBuilder message = new StringBuilder();
		
		message.append("Nenhum documento atualizado com as a��es: ");
		message.append(CollectionsUtil.listAndConcatenate(listaCondicao, "nome", ", "));
		
		return message.toString();
	}
	
	/**
	 * M�todo para obter uma lista de Documentoacao carregada com os ID's dos par�metros
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoacaoDAO#loadAsList(String, String)
	 * @param listaDocumentoacao
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Documentoacao> loadAsList(Collection<Documentoacao> listaDocumentoacao, String campos){
		String whereIn = CollectionsUtil.listAndConcatenate(listaDocumentoacao, "cddocumentoacao", ",");
		return documentoacaoDAO.loadAsList(whereIn, campos);
	}
	
	/* singleton */
	private static DocumentoacaoService instance;
	public static DocumentoacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(DocumentoacaoService.class);
		}
		return instance;
	}
}
