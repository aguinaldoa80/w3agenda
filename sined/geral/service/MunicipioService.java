package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.MunicipioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.MunicipioOfflineJSON;
import br.com.linkcom.sined.util.rest.android.municipio.MunicipioRESTModel;

public class MunicipioService extends GenericService<Municipio> {
	
	private MunicipioDAO municipioDAO;
	public void setMunicipioDAO(MunicipioDAO municipioDAO) {
		this.municipioDAO = municipioDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m todos os municipios de um UF.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#findByUf(Uf)
	 * @param uf
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Municipio> findByUf(Uf uf){
		return municipioDAO.findByUf(uf);
	}
	public List<Municipio> findByUfProcJuridico(Uf uf){
		return municipioDAO.findByUfProcJuridico(uf);
	}
	
	/**
	 * Faz refer�ncia ao m�todo findByUfFLex
	 *
	 * @param uf
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Municipio> findByUfFlex(Uf uf){
		return this.findByUf(uf);
	}
	
	
	/* singleton */
	private static MunicipioService instance;
	public static MunicipioService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MunicipioService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#carregaMunicipio
	 * @param municipio
	 * @return
	 * @author Rodrigo Freitas]
	 */
	public Municipio carregaMunicipio(Municipio municipio) {
		return municipioDAO.carregaMunicipio(municipio);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Usado na tela de nota fiscal de produto.
	 *
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#loadListaWithUfAutocomplete
	 * 
	 * @param s
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Municipio> loadListaWithUfAutocomplete(String s){
		String uf = null;
		Integer cduf = null;
		try {
			uf = NeoWeb.getRequestContext().getParameter("cduf");
			if(Util.strings.isNotEmpty(uf)){
				cduf = Integer.parseInt(uf);
			}
		} catch (Exception e) {}
		return municipioDAO.loadListaWithUfAutocomplete(s, cduf);
	}
	
	/**
	 * 
	 *<p>M�todo respons�vel em carregar inform��es do municipo para o webService de cep
	 *
	 * @param nomeMunicipio {@link Municipio}.getNome()
	 * @param siglaUf {@link Uf}.getSigla()
	 * @return {@link Municipio}
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#getByNomeAndUf(String nomeMunicipio, String siglaUf)
	 * 
	 * @author Taidson
	 * @since 25/04/2010
	 */
	public Municipio getByNomeAndUf(String nomeMunicipio, String siglaUf) {
		return municipioDAO.getByNomeAndUf(nomeMunicipio,siglaUf);
	}
	
	/**
	 * 
	 *<p>M�todo respons�vel em carregar dados do Uf
	 *
	 * @param uf {@link Uf}
	 * @return {@link List} Municip�o
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#find(Uf uf)
	 * 
	 * @author Taidson
	 * @since 25/04/2010
	 */
	public List<Municipio> find(Uf uf) {
		return municipioDAO.find(uf);
	}

	public Municipio findByNomeSigla(String nome, String siglauf) {
		return municipioDAO.findByNomeSigla(nome, siglauf);
	}
	
	/**
	 * Busca o munic�pio de acordo com seu c�digo IBGE. 
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MunicipioDAO#findByCdibge(String cdibge)
	 *
	 * @param cdibge
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Municipio findByCdibge(String cdibge) {
		return municipioDAO.findByCdibge(cdibge);
	}

	public List<MunicipioOfflineJSON> findForPVOffline() {
		List<MunicipioOfflineJSON> lista = new ArrayList<MunicipioOfflineJSON>();
		for(Municipio m : municipioDAO.findForPVOffline())
			lista.add(new MunicipioOfflineJSON(m));
		
		return lista;
	}
	
	public List<MunicipioRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<MunicipioRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MunicipioRESTModel> lista = new ArrayList<MunicipioRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Municipio m : municipioDAO.findForAndroid(whereIn))
				lista.add(new MunicipioRESTModel(m));
		}
		
		return lista;
	}
	
	public Municipio getByMunicipioNome(String nome) {
		return municipioDAO.getByMunicipioNome(nome);
	}
}
