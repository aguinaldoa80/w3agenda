package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Apontamento;
import br.com.linkcom.sined.geral.bean.ApontamentoHoras;
import br.com.linkcom.sined.geral.dao.ApontamentoHorasDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ApontamentoHorasService extends GenericService<ApontamentoHoras>{

	private ApontamentoHorasDAO apontamentoHorasDAO;
	
	public void setApontamentoHorasDAO(ApontamentoHorasDAO apontamentoHorasDAO) {
		this.apontamentoHorasDAO = apontamentoHorasDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ApontamentoHorasDAO#findByApontamento
	 * 
	 * @param apontamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<ApontamentoHoras> findByApontamento(Apontamento apontamento) {
		return apontamentoHorasDAO.findByApontamento(apontamento);
	}

}
