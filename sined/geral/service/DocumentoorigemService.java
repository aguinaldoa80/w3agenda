package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Colaboradordespesa;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Despesaavulsarh;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Fornecimento;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.dao.DocumentoorigemDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoorigemService extends GenericService<Documentoorigem>{

	private DocumentoorigemDAO documentoorigemDAO;
	
	public void setDocumentoorigemDAO(DocumentoorigemDAO documentoorigemDAO) {
		this.documentoorigemDAO = documentoorigemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.DocumentoorigemDAO#getListaDocumentosDaEntrega(Entrega)
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentoorigem> getListaDocumentosDaEntrega(Entrega entrega) {		
		return documentoorigemDAO.getListaDocumentosDaEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoorigemDAO#existDocumentosByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existDocumentosByEntrega(Entrega entrega) {
		return documentoorigemDAO.existDocumentosByEntrega(entrega);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Documentoorigem> getListaDocumentosDoFornecimento(Fornecimento form) {
		return documentoorigemDAO.getListaDocumentosDoFornecimento(form);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param entrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean existeDocumentoComOrigemEntrega(Entrega entrega) {
		return documentoorigemDAO.existeDocumentoComOrigemEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Documentoorigem> getListaDocumentosDeVenda(Venda venda) {
		return documentoorigemDAO.getListaDocumentosDeVenda(venda, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param venda
	 * @param whereInDocumento
	 * @return
	 * @author Mairon Cezar
	 */
	public List<Documentoorigem> getListaDocumentosDeVenda(Venda venda, String whereInDocumento) {
		return documentoorigemDAO.getListaDocumentosDeVenda(venda, whereInDocumento);
	}
	
	public List<Documentoorigem> findDocumentoByVenda(String whereInVenda) {
		return documentoorigemDAO.findDocumentoByVenda(whereInVenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.DocumentoorigemDAO#getListaDocumentosDePedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Documentoorigem> getListaDocumentosDePedidovenda(Pedidovenda pedidovenda) {
		return documentoorigemDAO.getListaDocumentosDePedidovenda(pedidovenda);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 24/11/2010
	 */
	public List<Documentoorigem> contasContrato(Contrato contrato){
		return documentoorigemDAO.contasContrato(contrato);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param documento
	 * @return
	 * @authro Tom�s Rabelo
	 */
	public List<Documentoorigem> findByDocumento(Documento documento) {
		return documentoorigemDAO.findByDocumento(documento);
	}

	public List<Documentoorigem> findByDocumento(String whereIn) {
		return documentoorigemDAO.findByDocumento(whereIn);
	}
	
	public Documentoorigem contaColaboradordespesa(Colaboradordespesa colaboradordespesa){
		return documentoorigemDAO.contaColaboradordespesa(colaboradordespesa);
		
	}

	/**
	 * Recupera as contas geradas a partir de uma {@link Despesaavulsarh}.
	 * 
	 * @author <a mailto="giovane.freitas@linkcom.com.br">Giovane Freitas</a>
	 * @since Oct 22, 2011
	 * @param despesaavulsarh
	 * @return
	 */
	public Documentoorigem contaDespesaavulsarh(Despesaavulsarh despesaavulsarh){
		return documentoorigemDAO.contaDespesaavulsarh(despesaavulsarh);
	}
	
	/**M�todo que busca o n�mero, valor e data de vencimento para a lista de duplicatas da NF.
	 * @author Thiago Augusto
	 * @param venda
	 * @return
	 */
	public List<Documentoorigem> buscarListaParaDuplicatas(Venda venda){
		return documentoorigemDAO.buscarListaParaDuplicatas(venda);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findDocumentoOrigemByDocumento
	 * @param documento
	 * @return
	 * @return Documentoorigem
	 * @author Thiago Augusto
	 * @date 11/05/2012
	 *
	 */
	public Documentoorigem findDocumentoOrigemByDocumento(Documento documento){
		return documentoorigemDAO.findDocumentoOrigemByDocumento(documento);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @since 21/09/2012
	 * @author Rodrigo Freitas
	 */
	public boolean isOrigemVenda(Documento documento) {
		return documentoorigemDAO.isOrigemVenda(documento);
	}
	
	public List<Documentoorigem> getListaDocumentosDaEntradaFiscal(String whereIn) {
		return documentoorigemDAO.getListaDocumentosDaEntradaFiscal(whereIn);
	}	
	
	public List<Documentoorigem> getListaDocumentosDaEntradaFiscalCancelar(String whereIn) {
		return documentoorigemDAO.getListaDocumentosDaEntradaFiscalCancelar(whereIn);
	}	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Documentoorigem> getlistaDocumentosReferenciados(String whereIn){
		return documentoorigemDAO.getlistaDocumentosReferenciados(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/07/2014
	 */
	public boolean haveDocumentoByColetaNotCancelada(Coleta coleta) {
		return documentoorigemDAO.haveDocumentoByColetaNotCancelada(coleta);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param documento
	 * @return
	 * @author Joao Vitor
	 * @since 10/10/2014
	 */
	public Agendamento findDocumentoOrigemAgendamentoByDocumento(Documento documento){
		return documentoorigemDAO.findDocumentoOrigemAgendamentoByDocumento(documento);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.DocumentoorigemService#findDocumentoWithDespesaviagem(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 21/03/2016
	* @author Luiz Fernando
	*/
	public List<Documentoorigem> findDocumentoWithDespesaviagem(String whereIn) {
		return documentoorigemDAO.findDocumentoWithDespesaviagem(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.DocumentoorigemDAO#existeDocumentosNaoCanceladoEntradafiscal(Entregadocumento entregadocumento)
	*
	* @param entregadocumento
	* @return
	* @since 01/11/2016
	* @author Luiz Fernando
	*/
	public Boolean existeDocumentosNaoCanceladoEntradafiscal(Entregadocumento entregadocumento) {
		return documentoorigemDAO.existeDocumentosNaoCanceladoEntradafiscal(entregadocumento);
	}
	
	public List<Documentoorigem> findByOrdemcompra(String whereIn) {
		return documentoorigemDAO.findByOrdemcompra(whereIn);
	}

	public boolean existeDocumentosNaoCancelado(List<Documentoorigem> listaDocOrigem) {
		if(SinedUtil.isListNotEmpty(listaDocOrigem)){
			for(Documentoorigem documentoorigem : listaDocOrigem){
				if(documentoorigem.getDocumento() != null && documentoorigem.getDocumento().getDocumentoacao() != null &&
						!Documentoacao.CANCELADA.equals(documentoorigem.getDocumento().getDocumentoacao())){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean existeDocumentoorigemEntradafiscal(Documento documento, Entregadocumento entregadocumento) {
		return documentoorigemDAO.existeDocumentoorigemEntradafiscal(documento, entregadocumento);
	}
	
	public boolean existeDocumentoorigemVenda(Venda venda) {
		return documentoorigemDAO.existeDocumentoorigemVenda(venda);
	}

	public List<Documentoorigem> findDocumentoWithGnre(String whereInDocumento) {
		return documentoorigemDAO.findDocumentoWithGnre(whereInDocumento);
	}
}