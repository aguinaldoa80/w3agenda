package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetipoitem;
import br.com.linkcom.sined.geral.dao.AtividadetipoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AtividadetipoitemService extends GenericService<Atividadetipoitem>{
	private AtividadetipoitemDAO atividadetipoitemDAO;
	
	public void setAtividadetipoitemDAO(AtividadetipoitemDAO atividadetipoitemDAO) {
		this.atividadetipoitemDAO = atividadetipoitemDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO
	 * @param tipo
	 * @return
	 * @author Taidson
	 * @since 01/10/2010
	 */
	public List<Atividadetipoitem> findByTipo(Atividadetipo tipo){
		return atividadetipoitemDAO.findByTipo(tipo);
	}
}
