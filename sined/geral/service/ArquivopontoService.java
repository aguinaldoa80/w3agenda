package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Arquivoponto;
import br.com.linkcom.sined.geral.dao.ArquivopontoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivopontoService extends GenericService<Arquivoponto>{

	private ArquivopontoDAO arquivopontoDAO;
	
	public void setArquivopontoDAO(ArquivopontoDAO arquivopontoDAO) {
		this.arquivopontoDAO = arquivopontoDAO;
	}
	
	/**
	 * Faz referência ao DAO.
	 * Busca a lista de Arquivoponto para o processamento do Arquivo. 
	 *
	 * @see br.com.linkcom.sined.geral.dao.ArquivopontoDAO#findForProcessamento(String whereIn)
	 * 
	 * @param whereIn
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public List<Arquivoponto> findForProcessamento(String whereIn) {
		return arquivopontoDAO.findForProcessamento(whereIn);
	}

	/**
	 * Faz referência ao DAO.
	 * Atualiza as informações de um arquivo de ponto para o processamento.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ArquivopontoDAO#updateProcessamento(String ids)
	 *
	 * @param ids
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public void updateProcessamento(String ids) {
		arquivopontoDAO.updateProcessamento(ids);
	}
	

}
