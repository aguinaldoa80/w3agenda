package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.PrazoPagamentoECF;
import br.com.linkcom.sined.geral.dao.PrazoPagamentoECFDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PrazoPagamentoECFService extends GenericService<PrazoPagamentoECF>{
	
	private PrazoPagamentoECFDAO prazoPagamentoECFDAO;
	public void setPrazoPagamentoECFDAO(PrazoPagamentoECFDAO prazoPagamentoECFDAO) {this.prazoPagamentoECFDAO = prazoPagamentoECFDAO;}
}
