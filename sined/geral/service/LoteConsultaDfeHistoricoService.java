package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.dao.LoteConsultaDfeHistoricoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LoteConsultaDfeHistoricoService extends GenericService<LoteConsultaDfeHistorico> {

	private LoteConsultaDfeHistoricoDAO loteConsultaDfeHistoricoDAO;
	
	public void setLoteConsultaDfeHistoricoDAO(LoteConsultaDfeHistoricoDAO loteConsultaDfeHistoricoDAO) {
		this.loteConsultaDfeHistoricoDAO = loteConsultaDfeHistoricoDAO;
	}
	
	public List<LoteConsultaDfeHistorico> procurarPorLoteConsultaDfe(LoteConsultaDfe form) {
		return loteConsultaDfeHistoricoDAO.procurarPorLoteConsultaDfe(form);
	}
}
