package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.dao.EmporiumtotalizadoresDAO;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumtotalizadoresService extends GenericService<Emporiumtotalizadores>{
	
	private EmporiumtotalizadoresDAO emporiumtotalizadoresDAO;
	
	public void setEmporiumtotalizadoresDAO(
			EmporiumtotalizadoresDAO emporiumtotalizadoresDAO) {
		this.emporiumtotalizadoresDAO = emporiumtotalizadoresDAO;
	}

	public List<Emporiumtotalizadores> findForSped(Emporiumpdv emporiumpdv, Date dtinicio, Date dtfim) {
		return emporiumtotalizadoresDAO.findForSped(emporiumpdv, dtinicio, dtfim);
	}

	public void deleteTotalizadores(Date data, Emporiumpdv emporiumpdv) {
		emporiumtotalizadoresDAO.deleteTotalizadores(data, emporiumpdv);
	}

	public List<Emporiumtotalizadores> findForAcerto(Date dtinicio, Date dtfim) {
		return emporiumtotalizadoresDAO.findForAcerto(dtinicio, dtfim);
	}

	public List<Emporiumtotalizadores> getEmporiumtotalizadoresByDataPDV(List<Emporiumtotalizadores> listaEmporiumtotalizadores, Emporiumpdv emporiumpdv, Date data) {
		List<Emporiumtotalizadores> lista = new ArrayList<Emporiumtotalizadores>();
		for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadores) {
			if(emporiumtotalizadores.getEmporiumpdv() != null &&
					emporiumtotalizadores.getEmporiumpdv().equals(emporiumpdv) &&
					SinedDateUtils.equalsIgnoreHour(data, emporiumtotalizadores.getData())){
				lista.add(emporiumtotalizadores);
			}
		}
		return lista;
	}
	
	public List<Emporiumtotalizadores> getEmporiumtotalizadoresByDataMovimento(Date datamovimento, List<Emporiumtotalizadores> listaEmporiumtotalizadores) {
		List<Emporiumtotalizadores> listaFinal = new ArrayList<Emporiumtotalizadores>();
		
		for (Emporiumtotalizadores emporiumtotalizadores : listaEmporiumtotalizadores) {
			if(emporiumtotalizadores.getData() != null &&
					SinedDateUtils.equalsIgnoreHour(emporiumtotalizadores.getData(), datamovimento)){
				listaFinal.add(emporiumtotalizadores);
			}
		}
		
		return listaFinal;
	}
	
	

}
