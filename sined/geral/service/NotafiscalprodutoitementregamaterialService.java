package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitementregamaterial;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoitementregamaterialDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoitementregamaterialService extends GenericService<Notafiscalprodutoitementregamaterial>{
	
	private NotafiscalprodutoitementregamaterialDAO notafiscalprodutoitementregamaterialDAO;

	public void setNotafiscalprodutoitementregamaterialDAO(
			NotafiscalprodutoitementregamaterialDAO notafiscalprodutoitementregamaterialDAO) {
		this.notafiscalprodutoitementregamaterialDAO = notafiscalprodutoitementregamaterialDAO;
	}
	
	@Deprecated
	public Notafiscalprodutoitementregamaterial loadByNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem){
		return notafiscalprodutoitementregamaterialDAO.loadByNotafiscalprodutoitem(notafiscalprodutoitem);
	}
	
	public List<Notafiscalprodutoitementregamaterial> loadListaByNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem){
		return notafiscalprodutoitementregamaterialDAO.loadListaByNotafiscalprodutoitem(notafiscalprodutoitem);
	}

}
