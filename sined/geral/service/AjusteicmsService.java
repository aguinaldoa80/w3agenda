package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ajusteicms;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.dao.AjusteicmsDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.ApuracaoicmsFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AjusteicmsService extends GenericService<Ajusteicms>{

	private AjusteicmsDAO ajusteicmsDAO;
	
	public void setAjusteicmsDAO(AjusteicmsDAO ajusteicmsDAO) {
		this.ajusteicmsDAO = ajusteicmsDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	public List<Ajusteicms> findForSPEDRegE111(SpedarquivoFiltro filtro, String siglaUf) {
		return ajusteicmsDAO.findForSPEDRegE111(filtro, siglaUf);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see  br.com.linkcom.sined.geral.dao.AjusteicmsDAO#findForSPEDRegE200(SpedarquivoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @since 17/03/2014
	 */
	public List<Ajusteicms> findForSPEDRegE220(SpedarquivoFiltro filtro, Uf uf) {
		return ajusteicmsDAO.findForSPEDRegE220(filtro, uf);
	}
	
	public List<Ajusteicms> findForSPEDRegE311(SpedarquivoFiltro filtro, String siglaUf) {
		return ajusteicmsDAO.findForSPEDRegE311(filtro, siglaUf);
	}

	public List<Ajusteicms> findForApuracaoICMSDebito(ApuracaoicmsFiltro filtro){
		return ajusteicmsDAO.findForApuracaoICMSDebito(filtro);
	}
	
	public List<Ajusteicms> findForApuracaoICMSCredito(ApuracaoicmsFiltro filtro){
		return ajusteicmsDAO.findForApuracaoICMSCredito(filtro);
	}
	
	public List<Ajusteicms> findForApuracaoICMSCredito09(SpedarquivoFiltro filtro){
		return ajusteicmsDAO.findForApuracaoICMSCredito09(filtro);
	}
}
