package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Arquivoconciliacaooperadora;
import br.com.linkcom.sined.geral.bean.AtributosMaterial;
import br.com.linkcom.sined.geral.dao.AtributosMaterialDAO;

public class AtributosMaterialService extends GenericService<AtributosMaterial>{
	AtributosMaterialDAO atributosMaterialDAO;
		
	public void setAtributosMaterialDAO(AtributosMaterialDAO atributosMaterialDAO) {this.atributosMaterialDAO = atributosMaterialDAO;}



	public Boolean existeAtributo (AtributosMaterial atributoMaterial){
		return atributosMaterialDAO.existeAtributo(atributoMaterial);
	}
}
