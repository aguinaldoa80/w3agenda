package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialformulavendaminimo;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.FormulatabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.OportunidadeVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda.TabelavalorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavendaminimo.FormulaVO;
import br.com.linkcom.sined.geral.bean.auxiliar.materialformulavendaminimo.MaterialVO;
import br.com.linkcom.sined.geral.dao.MaterialformulavendaminimoDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialformulavendaminimoService extends GenericService<Materialformulavendaminimo>{

	private MaterialService materialService;
	private MaterialformulavendaminimoDAO materialformulavendaminimoDAO;
	private TabelavalorService tabelavalorService;
	
	public void setMaterialformulavendaminimoDAO(MaterialformulavendaminimoDAO materialformulavendaminimoDAO) {
		this.materialformulavendaminimoDAO = materialformulavendaminimoDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	
	/**
	* M�todo que calcula o vendaminimo do material
	*
	* @param material
	* @return
	* @throws ScriptException
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public Double calculavendaminimoMaterial(Material material) throws ScriptException {
		Double vendaminimo = 0d;
		
		if(material != null){
			if(material.getListaMaterialformulavendaminimo() != null && !material.getListaMaterialformulavendaminimo().isEmpty()){
				List<Materialformulavendaminimo> listaMaterialformulavendaminimo = material.getListaMaterialformulavendaminimo();
				
				Pattern pattern_material = Pattern.compile("material\\.[a-zA-Z]+\\(([0-9]+)\\)");
				List<String> idsMateriais = new ArrayList<String>();
				idsMateriais.add(material.getCdmaterial().toString());
				if(material.getBanda() != null && material.getBanda().getCdmaterial() != null){
					idsMateriais.add(material.getBanda().getCdmaterial().toString());
				}
				
				Pattern pattern_tabelavalor = Pattern.compile("tabelavalor\\.[a-zA-Z]+\\((['a-zA-Z0-9_']+)\\)");
				List<String> identificadoresTabelavalor = new ArrayList<String>();
				
				for (Materialformulavendaminimo materialformulavendaminimo : listaMaterialformulavendaminimo) {
					if(materialformulavendaminimo.getOrdem() == null ||
							materialformulavendaminimo.getIdentificador() == null ||
							materialformulavendaminimo.getFormula() == null){
						return 0d;
					}else {
						Matcher m_material = pattern_material.matcher(materialformulavendaminimo.getFormula());
						while(m_material.find()){
							idsMateriais.add(m_material.group(1));
						}
						
						Matcher m_tabelavalor = pattern_tabelavalor.matcher(materialformulavendaminimo.getFormula());
						while(m_tabelavalor.find()){
							identificadoresTabelavalor.add(m_tabelavalor.group(1));
						}
					}
				}
				
				FormulaVO formulaVO = new FormulaVO();
				
				if(idsMateriais.size() > 0){
					List<Material> listaMaterial = materialService.findForCalcularValorvendaminimo(CollectionsUtil.concatenate(idsMateriais, ","));
					for (Material m : listaMaterial) {
						MaterialVO materialVO = new MaterialVO();
						materialVO.setCdmaterial(m.getCdmaterial());
						materialVO.setPesobruto(m.getPesobruto() != null ? m.getPesobruto() : 0.0);
						materialVO.setFrete(m.getValorfrete() != null ? m.getValorfrete().getValue().doubleValue() : 0.0);
						if(m.equals(material) && material.getValorcustoByCalculoProducao() != null){
							materialVO.setValorcusto(material.getValorcustoByCalculoProducao());
						}else {
							materialVO.setValorcusto(m.getValorcusto() != null ? m.getValorcusto() : 0.0);
						}
						materialVO.setValorvenda(m.getValorvenda() != null ? m.getValorvenda() : 0.0);
						if(m.getMaterialproduto() != null){
							materialVO.setAltura(m.getMaterialproduto().getAltura() != null ? m.getMaterialproduto().getAltura() : 0.0);
							materialVO.setLargura(m.getMaterialproduto().getLargura() != null ? m.getMaterialproduto().getLargura() : 0.0);
							materialVO.setComprimento(m.getMaterialproduto().getComprimento() != null ? m.getMaterialproduto().getComprimento() : 0.0);
						}
						if(m.equals(material) && material.getBanda() != null){
							materialVO.setCdmaterialbanda(material.getBanda().getCdmaterial());
						}
						formulaVO.addMaterial(materialVO);
						
						if(m.getCdmaterial().equals(material.getCdmaterial())){
							formulaVO.setAux_materialVO(materialVO);
							if(m.getMaterialgrupo() != null && m.getMaterialgrupo().getCdmaterialgrupo() != null){
								formulaVO.setCdgrupomaterial(m.getMaterialgrupo().getCdmaterialgrupo());
							}
							if(m.getOrigemproduto() != null){
								formulaVO.setOrigem(m.getOrigemproduto().ordinal());
							}
						}
					}
				}
				
				FormulatabelavalorVO formulatabelavalorVO = new FormulatabelavalorVO();
				if(identificadoresTabelavalor.size() > 0){
					List<Tabelavalor> listaTabelavalor = tabelavalorService.findForCalcularValorvenda(CollectionsUtil.concatenate(identificadoresTabelavalor, ","));
					for (Tabelavalor tabelavalor : listaTabelavalor) {
						formulatabelavalorVO.addTabelavalor(new TabelavalorVO(tabelavalor));
					}
				}
				
				ScriptEngineManager manager = new ScriptEngineManager();
				ScriptEngine engine = manager.getEngineByName("JavaScript");
				
				OportunidadeVO oportunidadeVO = new OportunidadeVO();
				oportunidadeVO.setRevendedor(material.getRevendedor() != null ? material.getRevendedor() : false);
				oportunidadeVO.setQuantidade(material.getQuantidadeOportunidade() != null ? material.getQuantidadeOportunidade() : 0d);
				
				engine.put("material", formulaVO);
				engine.put("tabelavalor", formulatabelavalorVO);
				
				Collections.sort(listaMaterialformulavendaminimo, new Comparator<Materialformulavendaminimo>(){
					public int compare(Materialformulavendaminimo o1, Materialformulavendaminimo o2) {
						return o1.getOrdem().compareTo(o2.getOrdem());
					}
				});
				
				for (Materialformulavendaminimo materialformulavendaminimo : listaMaterialformulavendaminimo) {
					Object obj = engine.eval(materialformulavendaminimo.getFormula());
	
					Double resultado = 0d;
					if(obj != null){
						String resultadoStr = obj.toString();
						resultado = new Double(resultadoStr);
					}
					
					engine.put(materialformulavendaminimo.getIdentificador(), resultado);
					vendaminimo = SinedUtil.roundByParametro(resultado);
				}
				material.setIsValorvendaminimoFormula(true);
			}
		}
		
		return vendaminimo;
	}
	
	public boolean setVendaminimoByFormula(Material material, Double valorcustoProducao, boolean carregarMaterial){
		boolean isFormula = false; 
		if(material != null && material.getCdmaterial() != null){
			try {
				Material bean = carregarMaterial ? materialService.loadForCalcularValorvendaminimo(material) : material;
				
				if(SinedUtil.isRecapagem() && material.getBanda() == null){
					Material mateiral_aux = carregarMaterial ? bean : materialService.loadForCalcularValorvendaminimo(material);
					if(mateiral_aux != null && SinedUtil.isListNotEmpty(mateiral_aux.getListaProducao())){
						for (Materialproducao materialproducao : mateiral_aux.getListaProducao()){
							if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
								bean.setBanda(materialproducao.getMaterial());
								break;
							}
						}
					}
				}else {
					bean.setBanda(material.getBanda());
				}
				
				bean.setValorcustoByCalculoProducao(valorcustoProducao);
				if(material.getValorvenda() != null && material.getConsiderarValorvendaCalculado() != null && 
						material.getConsiderarValorvendaCalculado()){
					bean.setValorvenda(material.getValorvenda());
				}
				Double valorCalculado = calculavendaminimoMaterial(bean);
				if(valorCalculado != null && valorCalculado > 0d){
					material.setValorvendaminimo(valorCalculado);
				}
				isFormula = bean.getIsValorvendaminimoFormula() != null ? bean.getIsValorvendaminimoFormula() : false; 
			} catch (Exception e) {e.printStackTrace();}
		}
		return isFormula;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MaterialformulavendaminimoDAO#findByMaterial(Material material)
	*
	* @param material
	* @return
	* @since 18/05/2015
	* @author Luiz Fernando
	*/
	public List<Materialformulavendaminimo> findByMaterial(Material material) {
		return materialformulavendaminimoDAO.findByMaterial(material);
	}
	
	public void saveReplicarFormulavendaminimo(List<Materialformulavendaminimo> listaMaterialformulavendaminimo, Material material) {
		if(material != null && material.getCdmaterial() != null && listaMaterialformulavendaminimo != null && 
				!listaMaterialformulavendaminimo.isEmpty()){
			if(material.getListaMaterialformulavendaminimo() != null && !material.getListaMaterialformulavendaminimo().isEmpty()){
				deleteFormulavendaminimoByMaterial(material);
			}
			List<Materialformulavendaminimo> listaMf = new ArrayList<Materialformulavendaminimo>();
			for(Materialformulavendaminimo materialformulavendaminimo : listaMaterialformulavendaminimo){
				Materialformulavendaminimo mfc = new Materialformulavendaminimo();
				mfc.setCdmaterialformulavendaminimo(null);
				mfc.setOrdem(materialformulavendaminimo.getOrdem());
				mfc.setIdentificador(materialformulavendaminimo.getIdentificador());
				mfc.setFormula(materialformulavendaminimo.getFormula());
				mfc.setMaterial(material);
				saveOrUpdate(mfc);
				listaMf.add(mfc);
			}
			material.setListaMaterialformulavendaminimo(listaMf);
		}
		
	}
	
	private void deleteFormulavendaminimoByMaterial(Material material) {
		materialformulavendaminimoDAO.deleteFormulavendaminimoByMaterial(material);
	}
}
