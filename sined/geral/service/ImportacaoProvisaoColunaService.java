package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ImportacaoProvisaoColuna;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoProvisaoCampo;
import br.com.linkcom.sined.geral.dao.ImportacaoProvisaoColunaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ImportacaoProvisaoColunaService extends GenericService<ImportacaoProvisaoColuna> {
	private ImportacaoProvisaoColunaDAO importacaoProvisaoColunaDAO;
	
	public void setImportacaoProvisaoColunaDAO(ImportacaoProvisaoColunaDAO importacaoProvisaoColunaDAO) {this.importacaoProvisaoColunaDAO = importacaoProvisaoColunaDAO;}
	
	public boolean existeColunaCadastrada(ImportacaoProvisaoColuna bean) {
		return importacaoProvisaoColunaDAO.existeColunaCadastrada(bean);
	}

	public List<ImportacaoProvisaoColuna> findByTipo(ImportacaoProvisaoCampo importacaoProvisaoCampo) {
		return importacaoProvisaoColunaDAO.findByTipo(importacaoProvisaoCampo);
	}
}
