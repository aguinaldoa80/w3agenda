package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.dao.FatorajusteDAO;

public class FatorajusteService extends GenericService<Fatorajuste>{

	private FatorajusteDAO fatorajusteDAO;
		
	public void setFatorajusteDAO(FatorajusteDAO fatorajusteDAO) {this.fatorajusteDAO = fatorajusteDAO;}

	
	/* singleton */
	private static FatorajusteService instance;
	
	public static FatorajusteService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FatorajusteService.class);
		}
		return instance;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	* carrega os dados principais do fator de ajuste
	* 
	* @see	br.com.linkcom.sined.geral.service.FatorajusteService.carregaFatorajuste(Fatorajuste fatorajuste)
	*
	* @param fatorajuste
	* @return
	* @since Oct 14, 2011
	* @author Luiz Fernando F Silva
	*/
	public Fatorajuste carregaFatorajuste(Fatorajuste fatorajuste) {
		return fatorajusteDAO.carregaFatorajuste(fatorajuste);
	}
}
