package br.com.linkcom.sined.geral.service;


import java.util.List;

import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterialmestre;
import br.com.linkcom.sined.geral.dao.VendaorcamentomaterialmestreDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VendaorcamentomaterialmestreService extends GenericService<Vendaorcamentomaterialmestre>{

	private VendaorcamentomaterialmestreDAO vendaorcamentomaterialmestreDAO;
	
	public void setVendaorcamentomaterialmestreDAO(VendaorcamentomaterialmestreDAO vendaorcamentomaterialmestreDAO){
		this.vendaorcamentomaterialmestreDAO = vendaorcamentomaterialmestreDAO;
	}

	/**
	 * 
	 * @param form
	 * @return
	 */
	public List<Vendaorcamentomaterialmestre> findByVendaOrcamento(Vendaorcamento vendaorcamento) {
		return vendaorcamentomaterialmestreDAO.findByVendaOrcamento(vendaorcamento);
	}
	
}
