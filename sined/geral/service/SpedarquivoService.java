package br.com.linkcom.sined.geral.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.AjusteIpi;
import br.com.linkcom.sined.geral.bean.AjusteIpiDocumento;
import br.com.linkcom.sined.geral.bean.Ajustefiscal;
import br.com.linkcom.sined.geral.bean.Ajusteicms;
import br.com.linkcom.sined.geral.bean.Ajusteicmsdocumento;
import br.com.linkcom.sined.geral.bean.Ajusteicmsprocesso;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Arquivonfnota;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacao;
import br.com.linkcom.sined.geral.bean.DeclaracaoExportacaoNota;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendaitem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentoreferenciado;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Entregapagamento;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Inutilizacaonfe;
import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.Inventariomaterial;
import br.com.linkcom.sined.geral.bean.LancamentosOperacaoCartao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Obrigacaoicmsrecolher;
import br.com.linkcom.sined.geral.bean.Operacaocontabildebitocredito;
import br.com.linkcom.sined.geral.bean.OrdemProducaoSped;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.ProcessoReferenciado;
import br.com.linkcom.sined.geral.bean.SaldoIcms;
import br.com.linkcom.sined.geral.bean.Spedarquivo;
import br.com.linkcom.sined.geral.bean.Speddocumento;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivoinventario;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDocumentoDeclaracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoNaturezaDaDeclaracao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.dao.SpedarquivoDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ParticipanteSpedBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.RegistroAnaliticoSped;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.RegistroE510Apoio;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.SPEDFiscalApoio;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0000;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0005;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0150;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0190;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0200;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0210;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0400;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0450;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0460;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro0990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1010;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1105;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1110;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1200;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1210;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1600;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro1990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro9001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro9900;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro9990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.Registro9999;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroB001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroB990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC101;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC110;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC112;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC140;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC141;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC170;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC190;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC195;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC197;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC400;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC405;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC410;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC420;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC425;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC490;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC500;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC590;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroC990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD190;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD195;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD197;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD500;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD590;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroD990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE110;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE111;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE112;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE113;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE116;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE200;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE210;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE220;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE230;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE240;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE250;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE300;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE310;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE311;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE316;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE500;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE510;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE520;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE530;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE531;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroE990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroG001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroG990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroH001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroH005;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroH010;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroH990;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK001;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK100;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK200;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK210;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK215;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK230;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK235;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK250;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK255;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK280;
import br.com.linkcom.spedfiscal.spedicmsipi.registros.RegistroK990;
import br.com.linkcom.spedfiscal.spedicmsipi.sped.SPEDFiscal;
import br.com.linkcom.spedfiscal.spedicmsipi.sped.VersaoFiscal;
import br.com.linkcom.utils.SPEDUtil;

public class SpedarquivoService extends GenericService<Spedarquivo> {
	
	private NotafiscalprodutoService notafiscalprodutoService;
	private EmpresaService empresaService;
	private FornecedorService fornecedorService;
	private PessoaService pessoaService;
	private UnidademedidaService unidademedidaService;
	private MaterialService materialService;
	private ArquivoDAO arquivoDAO;
	private ArquivoService arquivoService;
	private SpedarquivoDAO spedarquivoDAO;
	private SpeddocumentoService speddocumentoService;
	private EntregapagamentoService entregapagamentoService;
	private InventarioService inventarioService;
	private EntradafiscalService entradafiscalService;
	private ArquivonfnotaService arquivonfnotaService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumtotalizadoresService emporiumtotalizadoresService;
	private EmporiumvendaService emporiumvendaService;
	private AjusteicmsService ajusteicmsService;
	private ObrigacaoicmsrecolherService obrigacaoicmsrecolherService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private EntregamaterialService entregamaterialService;
	private MaterialproducaoService materialproducaoService; 
	private EmporiumvendaitemService emporiumvendaitemService;
	private AvisoService avisoService;
	private AvisousuarioService avisoUsuarioService;
	private SaldoIcmsService saldoIcmsService;
//	private SaldoIpiService saldoIpiService;
	private AjusteIpiService ajusteIpiService;
	private DeclaracaoExportacaoService declaracaoExportacaoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private ProducaoordemService producaoordemService;
	private ParametrogeralService parametrogeralService;
	private LancamentosOperacaoCartaoService lancamentosOperacaoCartaoService;
	private InutilizacaonfeService inutilizacaonfeService;
	private ProcessoReferenciadoService processoReferenciadoService;
	
	public void setInutilizacaonfeService(InutilizacaonfeService inutilizacaonfeService) {this.inutilizacaonfeService = inutilizacaonfeService;}
	public void setDeclaracaoExportacaoService(DeclaracaoExportacaoService declaracaoExportacaoService) {this.declaracaoExportacaoService = declaracaoExportacaoService;}
	public void setEmporiumvendaitemService(EmporiumvendaitemService emporiumvendaitemService) {this.emporiumvendaitemService = emporiumvendaitemService;}
	public void setAjusteicmsService(AjusteicmsService ajusteicmsService) {this.ajusteicmsService = ajusteicmsService;}
	public void setEmporiumvendaService(EmporiumvendaService emporiumvendaService) {this.emporiumvendaService = emporiumvendaService;}
	public void setEmporiumtotalizadoresService(EmporiumtotalizadoresService emporiumtotalizadoresService) {this.emporiumtotalizadoresService = emporiumtotalizadoresService;}
	public void setEmporiumreducaozService(EmporiumreducaozService emporiumreducaozService) {this.emporiumreducaozService = emporiumreducaozService;}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {this.emporiumpdvService = emporiumpdvService;}
	public void setArquivonfnotaService(ArquivonfnotaService arquivonfnotaService) {this.arquivonfnotaService = arquivonfnotaService;}
	public void setEntregapagamentoService(EntregapagamentoService entregapagamentoService) {this.entregapagamentoService = entregapagamentoService;}
	public void setSpeddocumentoService(SpeddocumentoService speddocumentoService) {this.speddocumentoService = speddocumentoService;}
	public void setSpedarquivoDAO(SpedarquivoDAO spedarquivoDAO) {this.spedarquivoDAO = spedarquivoDAO;}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {this.arquivoDAO = arquivoDAO;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {this.unidademedidaService = unidademedidaService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setInventarioService(InventarioService inventarioService) {this.inventarioService = inventarioService;}
	public void setEntradafiscalService(EntradafiscalService entradafiscalService) {this.entradafiscalService = entradafiscalService;}
	public void setObrigacaoicmsrecolherService(ObrigacaoicmsrecolherService obrigacaoicmsrecolherService) {this.obrigacaoicmsrecolherService = obrigacaoicmsrecolherService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setEntregamaterialService(EntregamaterialService entregamaterialService) {this.entregamaterialService = entregamaterialService;}
	public void setMaterialproducaoService(MaterialproducaoService materialproducaoService) {this.materialproducaoService = materialproducaoService;}
	public void setAvisoService(AvisoService avisoService) {this.avisoService = avisoService;}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {this.avisoUsuarioService = avisoUsuarioService;}
	public void setSaldoIcmsService(SaldoIcmsService saldoIcmsService) {this.saldoIcmsService = saldoIcmsService;}
//	public void setSaldoIpiService(SaldoIpiService saldoIpiService) {this.saldoIpiService = saldoIpiService;}
	public void setAjusteIpiService(AjusteIpiService ajusteIpiService) {this.ajusteIpiService = ajusteIpiService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {this.materialCustoEmpresaService = materialCustoEmpresaService;}
	public void setProducaoordemService(ProducaoordemService producaoordemService) {this.producaoordemService = producaoordemService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setLancamentosOperacaoCartaoService(LancamentosOperacaoCartaoService lancamentosOperacaoCartaoService) {this.lancamentosOperacaoCartaoService = lancamentosOperacaoCartaoService;}
	
	public void setProcessoReferenciadoService(ProcessoReferenciadoService processoReferenciadoService) {this.processoReferenciadoService = processoReferenciadoService;}


	//Constantes
	private final static String VALOR_NFE = "55";
	
	/**
	 * M�todo que faz a manipula��o da lista de entrega para o registro D100.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForSpedRegD100(SpedarquivoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Entregadocumento> getListaEntregaD100(SpedarquivoFiltro filtro) {
		List<Entregadocumento> listaEntrega = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaEntregaNormal = entradafiscalService.findForSpedRegD100(filtro);
			for (Entregadocumento e1 : listaEntregaNormal) {
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntrega.add(e1);
			}
		}
		
		return listaEntrega;
	}
	
	/**
	 * M�todo que faz a manipula��o da lista de entrega para o registro C100.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntregaService#findForSpedRegC100
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Entregadocumento> getListaEntregaC100(SpedarquivoFiltro filtro) {
		List<Entregadocumento> listaEntrega = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregamaterial> listaMateriaisKit = new ArrayList<Entregamaterial>();
			List<Entregadocumento> listaEntregaNormal = entradafiscalService.findForSpedRegC100(filtro);
			for (Entregadocumento e1 : listaEntregaNormal) {
				//adiciona todos os materiais que s�o KIT a uma lista para ser usada no registro K210
				for (Entregamaterial entregamaterial : e1.getListaEntregamaterial()) {
					if(Boolean.TRUE.equals(entregamaterial.getMaterial().getVendapromocional()) &&
							entregamaterial.getMaterial().getTipoitemsped() != null && 
							(Tipoitemsped.MERCADORIA_REVENDA.equals(entregamaterial.getMaterial().getTipoitemsped()) ||		
							Tipoitemsped.MATERIA_PRIMA.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.EMBALAGEM.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_PROCESSO.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_ACABADO.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.SUBPRODUTO.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_INTERMEDIARIO.equals(entregamaterial.getMaterial().getTipoitemsped()) ||
							Tipoitemsped.OUTROS_INSUMOS.equals(entregamaterial.getMaterial().getTipoitemsped()))){
						listaMateriaisKit.add(entregamaterial);
					}
				}
				filtro.setListaEntregamaterialKit(listaMateriaisKit);
				
				if(e1.getSituacaodocumento() != null){
					e1.setSituacaosped(e1.getSituacaodocumento().getValue());
				}else {
					e1.setSituacaosped("00");
				}
				listaEntrega.add(e1);
			}
		}
		
		return listaEntrega;
	}
	
	/**
	 * M�todo que faz a manipula��o da lista de entrada fiscal para o registro D500.
	 * 
	 * @see  br.com.linkcom.sined.geral.service.EntradafiscalService#findForSpedRegD500(SpedarquivoFiltro filtro)
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Entregadocumento> getListaDocumentoD500(SpedarquivoFiltro filtro) {
		List<Entregadocumento> listaDocumento = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaDocumentoNormal = entradafiscalService.findForSpedRegD500(filtro);
			for (Entregadocumento dt1 : listaDocumentoNormal) {
				if(dt1.getSituacaodocumento() != null){
					dt1.setSituacaosped(dt1.getSituacaodocumento().getValue());
				}else {
					dt1.setSituacaosped("00");
				}
				listaDocumento.add(dt1);
			}
		}
		
		return listaDocumento;
	}
	
	
	/**
	 * M�todo que faz a manipula��o da lista de entrada fsical para o registro C500.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EntradafiscalService#findForSpedRegC500(SpedarquivoFiltro filtro)
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Entregadocumento> getListaDocumentoC500(SpedarquivoFiltro filtro) {
		List<Entregadocumento> listaEntradafiscal = new ArrayList<Entregadocumento>();
		
		boolean entradas = filtro.getBuscarentradas() != null && filtro.getBuscarentradas();
		if(entradas){
			List<Entregadocumento> listaDocumentoNormal = entradafiscalService.findForSpedRegC500(filtro);
			for (Entregadocumento dt1 : listaDocumentoNormal) {
				if(dt1.getSituacaodocumento() != null){
					dt1.setSituacaosped(dt1.getSituacaodocumento().getValue());
				}else {
					dt1.setSituacaosped("00");
				}
				listaEntradafiscal.add(dt1);
			}
		}
		
		return listaEntradafiscal;
	}
	
	
	/**
	 * M�todo que faz a manipula��o da lista de notafiscalproduto para o registro C100.
	 * 
	 * @see br.com.linkcom.sined.geral.service.NotafiscalprodutoService#findForSpedRegC100
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<Notafiscalproduto> getListaNotaC100(SpedarquivoFiltro filtro) {
		List<Notafiscalproduto> listaNotafiscalproduto = new ArrayList<Notafiscalproduto>();
		
		List<Notafiscalproduto> listaNotafiscalprodutoNormal = notafiscalprodutoService.findForSpedRegC100(filtro);
		for (Notafiscalproduto nf1 : listaNotafiscalprodutoNormal) {
			if(nf1.getNotaStatus() != null && NotaStatus.CANCELADA.equals(nf1.getNotaStatus())){
				nf1.setSituacaosped("02");
			}else if(nf1.getNotaStatus() != null && NotaStatus.NFE_DENEGADA.equals(nf1.getNotaStatus())){
				nf1.setSituacaosped("04");
			}else { 
				nf1.setSituacaosped("00");
			}
			listaNotafiscalproduto.add(nf1);
		}
		
		return listaNotafiscalproduto;
	}
	
	/**
	 * M�todo principal que gera arquivo SPED Fiscal.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0000
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro9999
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#resumoProcessamentoSped
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#saveArquivoSped
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo, Rodrigo Freitas
	 * @param request 
	 */
	public Resource geraArquivo(SpedarquivoFiltro filtro, WebRequestContext request) {
		SPEDFiscal spedFiscal = new SPEDFiscal();
		
		filtro.setApoio(new SPEDFiscalApoio());
		
		spedFiscal.setRegistro0000(this.createRegistro0000(filtro));
		spedFiscal.setRegistro9999(this.createRegistro9999(filtro));
		
		this.resumoProcessamentoSped(spedFiscal, filtro);
		
		List<String> listaErro = filtro.getListaErro();
		if(listaErro != null && listaErro.size() > 0){
			for (String erro : listaErro) {
				request.addError(erro);
			}
			throw new SinedException("A gera��o do arquivo cont�m erros. Favor corrig�-los.");
		}
		
		String nome = "spedfiscal_" + SinedUtil.datePatternForReport() + ".txt";
		byte[] bytes = spedFiscal.toString().getBytes();
		
		this.saveArquivoSped(filtro, nome, bytes);
		
		return new Resource("text/plain", nome, bytes);
	}
	
	/**
	 * Deleta o arquivo antigo da Empresa/MesAno e salva um arquivo novo.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService.deleteByEmpresaMesano
	 *
	 * @param filtro
	 * @param nome
	 * @param bytes
	 * @author Rodrigo Freitas
	 */
	private void saveArquivoSped(SpedarquivoFiltro filtro, String nome, byte[] bytes) {
		Arquivo arquivo = new Arquivo(bytes, nome, "text/plain");
		
		this.deleteByEmpresaMesano(filtro.getEmpresa(), filtro.getMesano());
		
		Spedarquivo spedarquivo = new Spedarquivo();
		spedarquivo.setMesano(filtro.getMesano());
		spedarquivo.setArquivo(arquivo);
		spedarquivo.setEmpresa(filtro.getEmpresa());
		
		arquivoDAO.saveFile(spedarquivo, "arquivo");
		arquivoService.saveOrUpdate(arquivo);
		this.saveOrUpdate(spedarquivo);
		
		Speddocumento speddocumento;
		
//		List<Integer> listaIdDocumento = filtro.getApoio().getListaIdDocumento();
//		for (Integer id : listaIdDocumento) {
//			speddocumento = new Speddocumento();
//			speddocumento.setSpedarquivo(spedarquivo);
//			speddocumento.setDocumento(new Documento(id));
//			
//			speddocumentoService.saveOrUpdate(speddocumento);
//		}
		
		List<Integer> listaIdEntregadocumento = filtro.getApoio().getListaIdEntrega();
		for (Integer id : listaIdEntregadocumento) {
			speddocumento = new Speddocumento();
			speddocumento.setSpedarquivo(spedarquivo);
			speddocumento.setEntregadocumento(new Entregadocumento(id));
			
			speddocumentoService.saveOrUpdate(speddocumento);
		}
		
		List<Integer> listaIdNota = filtro.getApoio().getListaIdNota();
		for (Integer id : listaIdNota) {
			speddocumento = new Speddocumento();
			speddocumento.setSpedarquivo(spedarquivo);
			speddocumento.setNota(new Nota(id));
			
			speddocumentoService.saveOrUpdate(speddocumento);
		}
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SpedarquivoDAO#deleteByEmpresaMesano
	 *
	 * @param empresa
	 * @param mesano
	 * @author Rodrigo Freitas
	 */
	private void deleteByEmpresaMesano(Empresa empresa, Date mesano) {
		spedarquivoDAO.deleteByEmpresaMesano(empresa, mesano);
	}
	
	/**
	 * Gera os registros que precisam ser depois de todo o processamento.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0200
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0150
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0190
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0400
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0450
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroE100
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#existeRegistro0001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#existeRegistroC001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#existeRegistroD001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#existeRegistroE001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#qtdeLinhasRegistro0990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#qtdeLinhasRegistroC990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#qtdeLinhasRegistroD990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#qtdeLinhasRegistroE990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro9900
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#qtdeLinhasRegistro9990
	 *
	 * @param spedFiscal
	 * @param filtro
	 * @author Rodrigo Freitas
	 */
	private void resumoProcessamentoSped(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		SPEDFiscalApoio apoio = filtro.getApoio();
		
		if(apoio != null && spedFiscal != null){
			if(spedFiscal.getRegistro0000() != null) {
				this.createRegistroE100(spedFiscal, filtro);
				spedFiscal.getRegistro0000().getRegistroE001().setListaRegistroE200(filtro.getListaRegistroE200());
				this.createRegistroE300(spedFiscal, filtro);
				this.createRegistroE500(spedFiscal, filtro);
				
				if(spedFiscal.getRegistro0000().getRegistro0001() != null){
					this.createRegistro0200(filtro, spedFiscal, apoio); // ANTES DO 0190
					this.createRegistro0150(filtro, spedFiscal, apoio.getMapParticipante());
					this.createRegistro0190(filtro, spedFiscal, apoio.getMapUnidade());
					this.createRegistro0400(filtro, spedFiscal, apoio.getMapNatureza());
					this.createRegistro0450(filtro, spedFiscal, apoio.getMapInfoComplementar());
					this.createRegistro0460(filtro, spedFiscal, apoio.getMapObsLancamentoFiscal());
				}
				
				this.existeRegistro0001(spedFiscal);
				this.existeRegistroC001(spedFiscal);
				this.existeRegistroD001(spedFiscal);
				this.existeRegistroE001(spedFiscal);
				this.existeRegistroK001(spedFiscal);
				
				this.qtdeLinhasRegistro0990(spedFiscal);
				this.qtdeLinhasRegistroB990(spedFiscal);
				this.qtdeLinhasRegistroC990(spedFiscal);
				this.qtdeLinhasRegistroD990(spedFiscal);
				this.qtdeLinhasRegistroE990(spedFiscal);
				this.qtdeLinhasRegistroG990(spedFiscal);
				this.qtdeLinhasRegistroH990(spedFiscal);
				this.qtdeLinhasRegistroK990(spedFiscal);
				this.qtdeLinhasRegistro1990(spedFiscal);
				
				int qtdeTotalLinhas = this.createRegistro9900(filtro, spedFiscal);
				this.qtdeLinhasRegistro9990(spedFiscal);
				
				if(spedFiscal.getRegistro9999() != null) spedFiscal.getRegistro9999().setQtd_lin(qtdeTotalLinhas);
			}
		}
	}
	
	private void qtdeLinhasRegistro1990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistro1001() != null){
			qtdeLinhas++; // Registro 1001
			
			if(spedFiscal.getRegistro0000().getRegistro1001().getRegistro1010() != null) {
				qtdeLinhas++; // Registro 1010
				if(spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1200() != null){
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1200().size(); // Registro 1200
					for (Registro1200 r1200 : spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1200()) {
						if(r1200.getListaRegistro1210() != null)
							qtdeLinhas += r1200.getListaRegistro1210().size(); // Registro 1210
					}
				}
				if(spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1600() != null){
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1600().size(); // Registro 1600
				}
			}
			if(spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1100() != null){
				for(Registro1100 r1100 : spedFiscal.getRegistro0000().getRegistro1001().getListaRegistro1100()){
					qtdeLinhas++;
					if(r1100.getListaRegistro1105() !=null ){	
						for (Registro1105 r1105 : r1100.getListaRegistro1105()) {
							if (r1105.getRegistro1110() !=null){
								qtdeLinhas++;
							}
						}
						qtdeLinhas += r1100.getListaRegistro1105().size();
					}
					
				}
			}
			
		}
		
		if(spedFiscal.getRegistro0000().getRegistro1990() != null) {
			qtdeLinhas++; // Registro 0990
		
			spedFiscal.getRegistro0000().getRegistro1990().setQtd_lin_1(qtdeLinhas);
		}
	}
	/**
	 * Cria o registro 9900 do arquivo do SPED.
	 *
	 * @param filtro
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private int createRegistro9900(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal) {
		int countLinhaTotal = 0;
		
		if(spedFiscal.getRegistro0000().getRegistro9001() != null){
			List<Registro9900> lista = new ArrayList<Registro9900>();
			Registro9900 registro9900;
			
			Map<String, Integer> map = filtro.getApoio().getMapTotalizadorRegistros();
			Set<Entry<String, Integer>> entrySet = map.entrySet();
			
			
			int countLinha = 0;
			
			for (Entry<String, Integer> entry : entrySet) {
				registro9900 = new Registro9900();
				
				registro9900.setReg(Registro9900.REG);
				registro9900.setReg_blc(entry.getKey());
				registro9900.setQtd_reg_blc(entry.getValue());
				
				countLinhaTotal += entry.getValue();
				
				lista.add(registro9900);
				
				countLinha++;
			}
			
			registro9900 = new Registro9900();
			
			countLinha++;
			
			registro9900.setReg(Registro9900.REG);
			registro9900.setReg_blc(Registro9900.REG);
			registro9900.setQtd_reg_blc(countLinha);
			
			countLinhaTotal += countLinha;
			
			lista.add(registro9900);
			
			spedFiscal.getRegistro0000().getRegistro9001().setListaRegistro9900(lista);
		}
		
		return countLinhaTotal;
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco 9 do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistro9990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistro9001() != null){
			qtdeLinhas++; // Registro 9001
			
			if(spedFiscal.getRegistro0000().getRegistro9001().getListaRegistro9900() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro9001().getListaRegistro9900().size(); // Registro 9900
		}
		
		if(spedFiscal.getRegistro0000().getRegistro9990() != null){
			qtdeLinhas++; // Registro 9990
			qtdeLinhas++; // Registro 9999
			
			spedFiscal.getRegistro0000().getRegistro9990().setQtd_lin_9(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco E do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroE990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistroE001() != null){
			qtdeLinhas++; // Registro E001
		
			if(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE100() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE100().size();
				
				for (RegistroE100 rE100 : spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE100()) {
					if(rE100.getRegistroE110() != null){
						qtdeLinhas++;  // Registro E110
						
						if(rE100.getRegistroE110().getListaRegistroE111() != null){
							qtdeLinhas += rE100.getRegistroE110().getListaRegistroE111().size(); // Registro E111
							for (RegistroE111 rE111 : rE100.getRegistroE110().getListaRegistroE111()) {
								qtdeLinhas += rE111.getListaRegistroE112().size(); // Registro E112
								qtdeLinhas += rE111.getListaRegistroE113().size(); // Registro E113
							}
						}
						
						if(rE100.getRegistroE110().getListaRegistroE116() != null)
							qtdeLinhas += rE100.getRegistroE110().getListaRegistroE116().size(); // Registro E116
						
						if(rE100.getRegistroE110().getListaRegistroE250() != null)
							qtdeLinhas += rE100.getRegistroE110().getListaRegistroE250().size(); // Registro E250
					}
				}
			}
			
			if(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE200() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE200().size();
				
				for (RegistroE200 rE200 : spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE200()) {
					if(rE200.getRegistroE210() != null){
						qtdeLinhas ++; // Registro E210
						
						if(rE200.getRegistroE210().getListaRegistroE220() != null){
							qtdeLinhas += rE200.getRegistroE210().getListaRegistroE220().size(); //Registro E220
							
							for(RegistroE220 registroE220 : rE200.getRegistroE210().getListaRegistroE220()){
								if(registroE220.getListaRegistroE230() != null){
									qtdeLinhas += registroE220.getListaRegistroE230().size(); // Registro E230
								}
								if(registroE220.getListaRegistroE240() != null){
									qtdeLinhas += registroE220.getListaRegistroE240().size(); // Registro E240
								}
							}
						}
						if(rE200.getRegistroE210().getListaRegistroE250() != null){
							qtdeLinhas += rE200.getRegistroE210().getListaRegistroE250().size(); //Registro E250
						}
					}
				}
			}
			
			if(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE300() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE300().size(); //Registro E300
				
				for (RegistroE300 rE300 : spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE300()) {
					if(rE300.getListaRegistroE310() != null){
						qtdeLinhas += rE300.getListaRegistroE310().size(); //Registro E310
						
						for(RegistroE310 rE310 : rE300.getListaRegistroE310()){
							if(rE310.getListaRegistroE311() != null){
								qtdeLinhas += rE310.getListaRegistroE311().size(); //Registro E311
							}
							if(rE310.getListaRegistroE316() != null){
								qtdeLinhas += rE310.getListaRegistroE316().size(); //Registro E316
							}
						}
					}
				}
			}
			
			if(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE500() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE500().size(); // Registro E500
				
				for (RegistroE500 rE500 : spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE500()) {
					if(rE500.getListaRegistrosE510() != null)
						qtdeLinhas += rE500.getListaRegistrosE510().size(); // Registro E510
					
					if(rE500.getRegistroE520() != null) {
						qtdeLinhas++;  // Registro E520
						
						if (rE500.getRegistroE520().getListaRegistro530() != null) {
							for (RegistroE530 registroE530 : rE500.getRegistroE520().getListaRegistro530()) {
								qtdeLinhas++;
								
								if (registroE530.getListaRegistroE531() != null) {
									qtdeLinhas += registroE530.getListaRegistroE531().size();
								}
							}
						}
					}
				}
			}
		}
		
		if(spedFiscal.getRegistro0000().getRegistroE990() != null){
			qtdeLinhas++; // Registro E990
			
			spedFiscal.getRegistro0000().getRegistroE990().setQtd_lin_e(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco H do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroH990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistroH001() != null){
			qtdeLinhas++; // Registro H001
			
			if(spedFiscal.getRegistro0000().getRegistroH001().getListaRegistroH005() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroH001().getListaRegistroH005().size(); // Registro H005
				for(RegistroH005 registroH005 : spedFiscal.getRegistro0000().getRegistroH001().getListaRegistroH005()){
					if(registroH005.getListaRegistroH010() != null)
						qtdeLinhas += registroH005.getListaRegistroH010().size(); // Registro H010
				}
				
			}
		}
		
		if(spedFiscal.getRegistro0000().getRegistroH990() != null){
			qtdeLinhas++; // Registro H990
			
			spedFiscal.getRegistro0000().getRegistroH990().setQtd_lin_h(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco G do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroG990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistroG001() != null){
			qtdeLinhas++; // Registro G001
		}
		
		if(spedFiscal.getRegistro0000().getRegistroG990() != null){
			qtdeLinhas++; // Registro G990
			
			spedFiscal.getRegistro0000().getRegistroG990().setQtd_lin_g(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco D do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroD990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistroD001() != null){
			qtdeLinhas++; // Registro D001
		
			if(spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100().size(); // Registro D100
				
				for (RegistroD100 rD100 : spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100()) {
					if(rD100.getListaRegistroD190() != null)
						qtdeLinhas += rD100.getListaRegistroD190().size(); // Registro D190
					
					if(rD100.getListaRegistroD195() != null){
						qtdeLinhas += rD100.getListaRegistroD195().size(); // Registro D195
						
						for(RegistroD195 registroD195 : rD100.getListaRegistroD195()){
							if(registroD195.getListaRegistroD197() != null)
								qtdeLinhas += registroD195.getListaRegistroD197().size(); // Registro D197
						}
					}
				}
			}
			
			if(spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD500() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD500().size(); // Registro D500
				

				for (RegistroD500 rD500 : spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD500()) {
					if(rD500.getListaRegistroD590() != null)
						qtdeLinhas += rD500.getListaRegistroD590().size(); // Registro D590
				}
			}
			
		}
		
		if(spedFiscal.getRegistro0000().getRegistroD990() != null){
			qtdeLinhas++; // Registro D990
		
			spedFiscal.getRegistro0000().getRegistroD990().setQtd_lin_d(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco C do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroC990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null){
			qtdeLinhas++; // Registro C001
		
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100().size(); // Registro C100
				
				for (RegistroC100 rC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()) {
					if(rC100.getRegistroC101() != null)
						qtdeLinhas++; // Registro C101
					
					if(rC100.getListaRegistroC110() != null){
						qtdeLinhas += rC100.getListaRegistroC110().size(); // Registro C110
						
						for (RegistroC110 rc110 : rC100.getListaRegistroC110()) {
							if(rc110.getListaRegistroC112() != null){
								qtdeLinhas += rc110.getListaRegistroC112().size(); // Registro C112
							}
						}
					}
						
					if(rC100.getRegistroC140() != null){
						qtdeLinhas++; // Registro C140
						
						if(rC100.getRegistroC140().getListaRegistrosC141() != null)
							qtdeLinhas += rC100.getRegistroC140().getListaRegistrosC141().size(); // Registro C141
					}
					
					if(rC100.getRegistroC160() != null) 
						qtdeLinhas++; // Registro C160
						
					if(rC100.getListaRegistroC170() != null)
						qtdeLinhas += rC100.getListaRegistroC170().size(); // Registro C170
						
					if(rC100.getListaRegistroC190() != null)
						qtdeLinhas += rC100.getListaRegistroC190().size(); // Registro C190
					
					if(rC100.getListaRegistroC195() != null){
						qtdeLinhas += rC100.getListaRegistroC195().size(); // Registro C195
						
						for(RegistroC195 registroC195 : rC100.getListaRegistroC195()){
							if(registroC195.getListaRegistroC197() != null)
								qtdeLinhas += registroC195.getListaRegistroC197().size(); // Registro C197
						}
					}
				}	
			}
			
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC400() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC400().size(); // Registro C400
				
				for (RegistroC400 rC400 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC400()) {
					if(rC400.getListaRegistroC405() != null){
						qtdeLinhas += rC400.getListaRegistroC405().size(); // Registro C405
						
						for (RegistroC405 rC405 : rC400.getListaRegistroC405()) {
							if(rC405.getRegistroC410() != null){
								qtdeLinhas ++; // Registro C410
							}
							if(rC405.getListaRegistroC420() != null){
								qtdeLinhas += rC405.getListaRegistroC420().size(); // Registro C420
								
								for (RegistroC420 rC420 : rC405.getListaRegistroC420()) {
									if(rC420.getListaRegistroC425() != null)
										qtdeLinhas += rC420.getListaRegistroC425().size(); // Registro C425
								}
							}
							
							if(rC405.getListaRegistroC490() != null)
								qtdeLinhas += rC405.getListaRegistroC490().size(); // Registro C490
						}
						
					}
				}
			}
			
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500().size(); // Registro C500
				
				for (RegistroC500 rC500 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500()) {
					if(rC500.getListaRegistrosC590() != null)
						qtdeLinhas += rC500.getListaRegistrosC590().size(); // Registro C590
				}
			}
			
		}
		
		if(spedFiscal.getRegistro0000().getRegistroC990() != null){
			qtdeLinhas++; // Registro C990
		
			spedFiscal.getRegistro0000().getRegistroC990().setQtd_lin_c(qtdeLinhas);
		}
	}
	
	
	
	/**
	 * Calcula o n�mero de linhas do Bloco K do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Jo�o Vitor
	 */
	private void qtdeLinhasRegistroK990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;

		if(spedFiscal.getRegistro0000().getRegistroK001() != null){
			qtdeLinhas++; // Registro K001

			if(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100() != null){
				qtdeLinhas ++; //Registro K100

				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK200())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK200().size(); //Registro K200
				}
				
				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK280())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK280().size(); //Registro K280
				}
				
				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK220())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK220().size(); // Registro K220
				}

				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230().size(); // Registro K230
					for (RegistroK230 k230 : spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230()) {
						if (SinedUtil.isListNotEmpty(k230.getListaRegistroK235())) {
							qtdeLinhas += k230.getListaRegistroK235().size(); // Registro K235
						}
					}
				}

				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK250())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK250().size();
					for (RegistroK250 k250 : spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK250()) {
						if (SinedUtil.isListNotEmpty(k250.getListaRegistroK255())) {
							qtdeLinhas += k250.getListaRegistroK255().size(); // Registro K255
						}
					}
				}
				
				if (SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK210())) {
					qtdeLinhas += spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK210().size(); //Registro K210
					for (RegistroK210 k210 : spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK210()) {
						if(SinedUtil.isListNotEmpty(k210.getListaRegistroK215())){
							qtdeLinhas += k210.getListaRegistroK215().size(); // Registro K215
						}
					}
				}
			}


			if(spedFiscal.getRegistro0000().getRegistroK990() != null){
				qtdeLinhas++; // Registro K990
				spedFiscal.getRegistro0000().getRegistroK990().setQtd_lin_k(qtdeLinhas);
			}
			
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco 0 do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistro0990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		qtdeLinhas++; // Registro 0000
		
		if(spedFiscal.getRegistro0000().getRegistro0001() != null){
			qtdeLinhas++; // Registro 0001
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getRegistro0005() != null) 
				qtdeLinhas++; // Registro 0005
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getRegistro0100() != null)
				qtdeLinhas++; // Registro 0100
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0150() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0150().size(); // Registro 0150
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0190() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0190().size(); // Registro 0190
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0200() != null){
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0200().size(); // Registro 0200


				for (Registro0200 r200 : spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0200()) {
					if(r200.getListaRegistro0210() != null)
						qtdeLinhas += r200.getListaRegistro0210().size(); // Registro 0210
					
					if(r200.getListaRegistro0220() != null)
						qtdeLinhas += r200.getListaRegistro0220().size(); // Registro 0220
				}
			}
				
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0400() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0400().size(); // Registro 0400
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0450() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0450().size(); // Registro 0450
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0460() != null)
				qtdeLinhas += spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0460().size(); // Registro 0460
		}
		
		if(spedFiscal.getRegistro0000().getRegistro0990() != null){
			qtdeLinhas++; // Registro 0990
		
			spedFiscal.getRegistro0000().getRegistro0990().setQtd_lin_0(qtdeLinhas);
		}
	}
	
	/**
	 * Calcula o n�mero de linhas do Bloco 0 do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void qtdeLinhasRegistroB990(SPEDFiscal spedFiscal) {
		int qtdeLinhas = 0;
		
		qtdeLinhas++; // Registro B001
		qtdeLinhas++; // Registro B990
		
		if (spedFiscal != null && spedFiscal.getRegistro0000() != null && spedFiscal.getRegistro0000().getRegistroB990() != null) {
			spedFiscal.getRegistro0000().getRegistroB990().setQtd_lin_b(qtdeLinhas);
		}
	}
	
	/**
	 * Preenche o arquivo do SPED com a informa��o se o bloco E tem registro.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#listaComRegistro
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void existeRegistroE001(SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000().getRegistroE001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE100()) || 
					this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE200()) ||
					this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroE001().getListaRegistroE300())) ind_mov = 0;
			
			spedFiscal.getRegistro0000().getRegistroE001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 * Preenche o arquivo do SPED com a informa��o se o bloco D tem registro.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#listaComRegistro
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void existeRegistroD001(SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000().getRegistroD001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD500())) ind_mov = 0;
			
			spedFiscal.getRegistro0000().getRegistroD001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 * Preenche o arquivo do SPED com a informa��o se o bloco C tem registro.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#listaComRegistro
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void existeRegistroC001(SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000().getRegistroC001() != null){
			int ind_mov = 1;
			
			if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500())) ind_mov = 0;
			
			spedFiscal.getRegistro0000().getRegistroC001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 * Preenche o arquivo do SPED com a informa��o se o bloco K tem registro.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#listaComRegistro
	 *
	 * @param spedFiscal
	 * @author Jo�o Vitor
	 */
	private void existeRegistroK001(SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000().getRegistroK001() != null){
			int ind_mov = 1;
			
			if(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100() != null) ind_mov = 0;
			spedFiscal.getRegistro0000().getRegistroK001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 * Preenche o arquivo do SPED com a informa��o se o bloco 0 tem registro.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#listaComRegistro
	 *
	 * @param spedFiscal
	 * @author Rodrigo Freitas
	 */
	private void existeRegistro0001(SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000().getRegistro0001() != null){
			int ind_mov = 1;
			
			if(spedFiscal.getRegistro0000().getRegistro0001().getRegistro0005() != null) ind_mov = 0;
			else if(spedFiscal.getRegistro0000().getRegistro0001().getRegistro0100() != null) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0150())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0190())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0200())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0400())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0450())) ind_mov = 0;
			else if(this.listaComRegistro(spedFiscal.getRegistro0000().getRegistro0001().getListaRegistro0460())) ind_mov = 0;
			
			spedFiscal.getRegistro0000().getRegistro0001().setInd_mov(ind_mov);
		}
	}
	
	/**
	 * Verifica se a lista de registro tem algum registro informado.
	 *
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean listaComRegistro(List<? extends Object> lista){
		return lista != null && lista.size() > 0;
	}
	
	/**
	 * Cria o registro 0450 do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @param mapInfoComplementar
	 * @author Rodrigo Freitas
	 */
	private void createRegistro0450(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Map<String, String> mapInfoComplementar) {
		List<Registro0450> lista = new ArrayList<Registro0450>();
		Registro0450 registro0450;
		
		Set<Entry<String, String>> entrySet = mapInfoComplementar.entrySet();
		for (Entry<String, String> entry : entrySet) {
			registro0450 = new Registro0450();
			
			registro0450.setReg(Registro0450.REG);
			registro0450.setCod_inf(entry.getValue());
			registro0450.setTxt(entry.getKey() != null ? entry.getKey().replace("\n", " ").replace("\r", " ") : null);
			
			lista.add(registro0450);
			
			filtro.getApoio().addRegistros(Registro0450.REG);
		}
		
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0450(lista);
	}
	
	private void createRegistro0460(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Map<String, String> mapObsLancamentoFiscal) {
		List<Registro0460> lista = new ArrayList<Registro0460>();
		Registro0460 registro0460;
		
		Set<Entry<String, String>> entrySet = mapObsLancamentoFiscal.entrySet();
		for (Entry<String, String> entry : entrySet) {
			registro0460 = new Registro0460();
			
			registro0460.setReg(Registro0460.REG);
			registro0460.setCod_obs(entry.getValue());
			registro0460.setTxt(entry.getKey() != null ? entry.getKey().replace("\n", " ").replace("\r", " ") : null);
			
			lista.add(registro0460);
			
			filtro.getApoio().addRegistros(Registro0460.REG);
		}
		
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0460(lista);
	}
	
	/**
	 * Cria o registro 0400 do arquivo do SPED.
	 *
	 * @param spedFiscal
	 * @param mapNatureza
	 * @author Rodrigo Freitas
	 */
	private void createRegistro0400(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Map<String, String> mapNatureza) {
		List<Registro0400> lista = new ArrayList<Registro0400>();
		Registro0400 registro0400;
		
		Set<Entry<String, String>> entrySet = mapNatureza.entrySet();
		for (Entry<String, String> entry : entrySet) {
			registro0400 = new Registro0400();
			
			registro0400.setReg(Registro0400.REG);
			registro0400.setCod_nat(entry.getValue());
			registro0400.setDescr_nat(entry.getKey());
			
			lista.add(registro0400);
			
			filtro.getApoio().addRegistros(Registro0400.REG);
		}
		
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0400(lista);
	}
	
	
	
	
	
	
	
	
	public void completaMAt(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Material mat){
		if(spedFiscal.getRegistro0000() != null && 
				spedFiscal.getRegistro0000().getRegistroK001() != null && 
				spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100() != null && 
				SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230())){
			List<Materialproducao> listaMaterialproducao;
			for(RegistroK230 registroK230 : spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230()){
				//alterei aqui
				if(mat.getCdmaterial().toString().equals(registroK230.getCod_item()) &&
						registroK230.getCdmaterial() != null ){
					listaMaterialproducao = materialproducaoService.findForSped(registroK230.getCdmaterial().toString());
					if(SinedUtil.isListNotEmpty(listaMaterialproducao)){
						for(Materialproducao materialproducao : listaMaterialproducao){
							if(materialproducao.getMaterial() != null && materialproducao.getConsumo() != null && 
									materialproducao.getConsumo() > 0){
								filtro.getApoio().addProdutoServico(materialproducao.getMaterial().getCdmaterial(), materialproducao.getMaterial().getIdentificacaoOuCdmaterial());			
							}
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * Cria o Registro 0200 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#getIdsMap
	 * @see br.com.linkcom.sined.geral.service.MaterialService#findForSpedReg0200
	 *
	 * @param spedFiscal
	 * @param apoio
	 * @author Rodrigo Freitas
	 */
	private void createRegistro0200(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, SPEDFiscalApoio apoio) {
		Map<String, Map<String, String>> mapProdutoServico = apoio.getMapProdutoServico();
		Map<String, Map<String, String>> mapMaterialProducao = apoio.getMapMaterialProducao();
		
		StringBuilder sb = this.getIdsMaps(mapProdutoServico);
		if(sb.length() == 0) return;	
		StringBuilder sbMaterialproducao = this.getIdsMaps(mapMaterialProducao);
		
		
		
		List<Registro0200> lista = new ArrayList<Registro0200>();
		Registro0200 registro0200;

		
		List<Material> listaMaterial = materialService.findForSpedReg0200(sb.toString(), sbMaterialproducao.toString());
		for (Material material : listaMaterial) {
			completaMAt(filtro, spedFiscal,material);
		}
		mapProdutoServico = apoio.getMapProdutoServico();
		mapMaterialProducao = apoio.getMapMaterialProducao();
		sb = this.getIdsMaps(mapProdutoServico);
		sbMaterialproducao = this.getIdsMaps(mapMaterialProducao);
		listaMaterial = materialService.findForSpedReg0200(sb.toString(), sbMaterialproducao.toString());
		
		
		for (Material mat : listaMaterial) {

			Map<String, String> mapProd = mapProdutoServico.get(mat.getCdmaterial().toString());
			if(mapProd != null){
					registro0200 = new Registro0200();
					
					registro0200.setReg(Registro0200.REG);
					registro0200.setVersaoFiscal(filtro.getVersaoFiscal());
					
					registro0200.setCod_item(mat.getCdmaterial().toString());
					
					if(mat.getNome().length() > 255){
						registro0200.setDescr_item(mat.getNome().substring(0, 255));
					} else {
						registro0200.setDescr_item(mat.getNome());
					}
					registro0200.setCod_barra(mat.getCodigobarras());
		//			registro0200.setCod_ant_item();
					registro0200.setUnid_inv(mat.getUnidademedida() != null ? apoio.addUnidade(mat.getUnidademedida().getCdunidademedida()) : null);
					registro0200.setTipo_item(mat.getTipoitemsped() != null ? mat.getTipoitemsped().getValue() : null);
					registro0200.setCod_ncm(mat.getNcmcompleto());
					registro0200.setEx_ipi(mat.getExtipi());
					registro0200.setCod_gen(mat.getNcmcapitulo() != null ? mat.getNcmcapitulo().getCodeFormat() : null);
					registro0200.setCod_lst(mat.getCodlistaservico());
		//			registro0200.setAliq_icms();
		//			registro0200.setCest();
					
					createRegistro0210(registro0200, filtro, spedFiscal);
					
					lista.add(registro0200);
					
					filtro.getApoio().addRegistros(Registro0200.REG);
//				}
			}
		}
		
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0200(lista);
	}
	
	private void createRegistro0210(Registro0200 registro0200, SpedarquivoFiltro filtro, SPEDFiscal spedFiscal) {
		if(spedFiscal.getRegistro0000() != null && 
				spedFiscal.getRegistro0000().getRegistroK001() != null && 
				spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100() != null && 
				SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230())){
			List<Registro0210> lista = new ArrayList<Registro0210>();
			List<Materialproducao> listaMaterialproducao;
			Registro0210 registro0210;
			for(RegistroK230 registroK230 : spedFiscal.getRegistro0000().getRegistroK001().getRegistroK100().getListaRegistroK230()){
				//alterei aqui
				if(registro0200.getCod_item().equals(registroK230.getCod_item()) &&
						registroK230.getCdmaterial() != null ){
					listaMaterialproducao = materialproducaoService.findForSped(registroK230.getCdmaterial().toString());
					if(SinedUtil.isListNotEmpty(listaMaterialproducao)){
						for(Materialproducao materialproducao : listaMaterialproducao){
							if(materialproducao.getMaterial() != null && materialproducao.getConsumo() != null && 
									materialproducao.getConsumo() > 0){
								registro0210 = new Registro0210();
								registro0210.setReg(Registro0210.REG);
								registro0210.setCod_item_comp(filtro.getApoio().addProdutoServico(materialproducao.getMaterial().getCdmaterial(), materialproducao.getMaterial().getIdentificacaoOuCdmaterial()));
								registro0210.setQtd_comp(materialproducao.getConsumo());
								registro0210.setPerda(0d);
								
								lista.add(registro0210);
								filtro.getApoio().addRegistros(Registro0210.REG);
								
							}
						}
					}
				}
			}
			if(SinedUtil.isListNotEmpty(lista)){
				registro0200.setListaRegistro0210(lista);
			}
		}
	}
	
	/**
	 * Cria o Registro 0190 do arquivo do SPED.
	 *  14186 14417 13708 16855 16855 17915 16855 16855
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#getIdsMap
	 * @see br.com.linkcom.sined.geral.service.UnidademedidaService#findForSpedReg0190
	 *
	 * @param spedFiscal
	 * @param mapUnidade
	 * @author Rodrigo Freitas
	 */
	private void createRegistro0190(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Map<Integer, String> mapUnidade) {
		StringBuilder sb = this.getIdsMap(mapUnidade);
		if(sb.length() == 0) return;
		
		List<Registro0190> lista = new ArrayList<Registro0190>();
		Registro0190 registro0190;
		
		List<Unidademedida> listaUnidade = unidademedidaService.findForSpedReg0190(sb.toString());
		for (Unidademedida um : listaUnidade) {
			registro0190 = new Registro0190();
			
			registro0190.setReg(Registro0190.REG);
			registro0190.setUnid(mapUnidade.get(um.getCdunidademedida()));
			registro0190.setDescr(um.getNome());
			
			lista.add(registro0190);
			
			filtro.getApoio().addRegistros(Registro0190.REG);
		}
		
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0190(lista);
	}
	
	/**
	 * Cria o Registro 0150 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#getIdsMap
	 * @see br.com.linkcom.sined.geral.service.PessoaService#findForSpedReg0150
	 *
	 * @param spedFiscal
	 * @param mapParticipante
	 * @author Rodrigo Freitas
	 */
	private void createRegistro0150(SpedarquivoFiltro filtro, SPEDFiscal spedFiscal, Map<MultiKey, String> mapParticipante) {
		StringBuilder sb = this.getIdsMap(mapParticipante);
		if(sb.length() == 0) return;
		
		List<Registro0150> lista = new ArrayList<Registro0150>();
		
		List<ParticipanteSpedBean> listaBean = pessoaService.findForSpedReg0150(sb.toString());
		
		for (ParticipanteSpedBean bean : listaBean) {
			if(StringUtils.isBlank(bean.getCpf()) && StringUtils.isBlank(bean.getCnpj())){
				//continue;
			}
			String codPart = mapParticipante.get(new MultiKey(bean.getCdpessoa(), bean.getCdendereco()));
			if(codPart != null){
				if(bean.getCdendereco() != null){
					lista.add(createRegistro0150ByParticipanteBean(bean, codPart + "-" + bean.getCdendereco()));
				} else {
					lista.add(createRegistro0150ByParticipanteBean(bean, codPart));
				}
				filtro.getApoio().addRegistros(Registro0150.REG);
			}
		}
			
		spedFiscal.getRegistro0000().getRegistro0001().setListaRegistro0150(lista);
	}
	
	/**
	 * Cria o registro 0150 a partir do bean de ParticipanteSpedBean
	 *
	 * @param bean
	 * @param codPart
	 * @return
	 * @author Rodrigo Freitas
	 * @since 07/07/2015
	 */
	private Registro0150 createRegistro0150ByParticipanteBean(ParticipanteSpedBean bean, String codPart) {
		Registro0150 registro0150 = new Registro0150();
		registro0150.setReg(Registro0150.REG);
		registro0150.setCod_part(codPart);
		registro0150.setNome(bean.getNome());
		registro0150.setCnpj(bean.getCnpj());
		registro0150.setCpf(bean.getCpf());
		registro0150.setIe(SPEDUtil.soNumero(bean.getIe()));
		registro0150.setEnd(bean.getEnd());
		registro0150.setNum(bean.getNum());
		registro0150.setCompl(bean.getCompl());
		registro0150.setBairro(bean.getBairro());
		registro0150.setCod_mun(bean.getCod_municipio());
		registro0150.setCod_pais(StringUtils.isNotEmpty(bean.getCod_pais()) ? bean.getCod_pais() : "1058");
		return registro0150;
	}
	
	/**
	 * Pega todos os ids do Map passado por par�metro para uma string no formato do whereIn.
	 *
	 * @param map
	 * @return
	 * @author Rodrigo Freitas
	 */
	private StringBuilder getIdsMap(Map<?, String> map) {
		StringBuilder sb = new StringBuilder();
		for (Entry<?, String> entry : map.entrySet()) {
			Object key = entry.getKey();
			if(key != null){
				if(key instanceof MultiKey){
					sb.append(((MultiKey)key).getKey(0)).append(",");
				} else {
					sb.append(key).append(",");
				}
			}
		}
		
		if(sb.length() > 0){
			sb.delete(sb.length() - 1, sb.length());
		}
		
		return sb;
	}
	
	private StringBuilder getIdsMaps(Map<String, Map<String, String>> map) {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, Map<String, String>> entry : map.entrySet()) {
			Object key = entry.getKey();
			if(key != null){
				if(key instanceof MultiKey){
					sb.append(((MultiKey)key).getKey(0)).append(",");
				} else {
					sb.append(key).append(",");
				}
			}
		}
		
		if(sb.length() > 0){
			sb.delete(sb.length() - 1, sb.length());
		}
		
		return sb;
	}
	
	/**
	 * Cria o Registro 9999 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro9999 createRegistro9999(SpedarquivoFiltro filtro) {
		Registro9999 registro9999 = new Registro9999();
		
		registro9999.setReg(Registro9999.REG);
		
		filtro.getApoio().addRegistros(Registro9999.REG);
		
		return registro9999;
	}
	
	/**
	 * Cria o Registro 0000 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadForSpedReg0000
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroD001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroD990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroE001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroE990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro1001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro1990
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro9001
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro9990
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro0000 createRegistro0000(SpedarquivoFiltro filtro) {
		Registro0000 registro0000 = new Registro0000();
		
		Empresa empresa = empresaService.loadForSpedReg0000(filtro.getEmpresa());
		filtro.setUfSigla(empresa.getUfsped() != null ? empresa.getUfsped().getSigla() : null);
		
		registro0000.setReg(Registro0000.REG);

		int mes = SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.MONTH) + 1;
		int ano = SinedDateUtils.getDateProperty(filtro.getDtinicio(), Calendar.YEAR);
		
		registro0000.setAno(ano);
		
		filtro.setMes(mes);
		filtro.setAno(ano);
		
		if(ano <= 2012){
			if(mes <= 6){
				registro0000.setCod_ver(VersaoFiscal.COD_VER_005.getDescricao());
				filtro.setVersaoFiscal(VersaoFiscal.COD_VER_005);
			} else {
				registro0000.setCod_ver(VersaoFiscal.COD_VER_006.getDescricao());
				filtro.setVersaoFiscal(VersaoFiscal.COD_VER_006);
			}
		} else if(ano <= 2013){
			registro0000.setCod_ver(VersaoFiscal.COD_VER_007.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_007);
		} else if(ano <= 2014){
			registro0000.setCod_ver(VersaoFiscal.COD_VER_008.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_008);
		} else if(ano <= 2015){
			registro0000.setCod_ver(VersaoFiscal.COD_VER_009.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_009);
		} else if(ano <= 2016){
			registro0000.setCod_ver(VersaoFiscal.COD_VER_010.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_010);
		}else if(ano <= 2017){
			registro0000.setCod_ver(VersaoFiscal.COD_VER_011.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_011);
		} else if (ano <= 2018) {
			registro0000.setCod_ver(VersaoFiscal.COD_VER_012.getDescricao());
			filtro.setVersaoFiscal(VersaoFiscal.COD_VER_012);
		} else if (ano <= 2019) {
            registro0000.setCod_ver(VersaoFiscal.COD_VER_013.getDescricao());
            filtro.setVersaoFiscal(VersaoFiscal.COD_VER_013);
        } else if (ano <= 2020) {
            registro0000.setCod_ver(VersaoFiscal.COD_VER_014.getDescricao());
            filtro.setVersaoFiscal(VersaoFiscal.COD_VER_014);
        } else{
        	registro0000.setCod_ver(VersaoFiscal.COD_VER_015.getDescricao());
            filtro.setVersaoFiscal(VersaoFiscal.COD_VER_015);
        }
		
		registro0000.setCod_fin(0);
		registro0000.setDt_ini(filtro.getDtinicio());
		registro0000.setDt_fin(filtro.getDtfim());
		registro0000.setNome(empresa.getRazaosocialOuNome());
		registro0000.setCnpj(empresa.getCnpj() != null ? empresa.getCnpj().getValue() : null);
//		registro0000.setCpf(null);
		registro0000.setUf(filtro.getUfSigla());
		registro0000.setIe(SPEDUtil.soNumero(empresa.getInscricaoestadual()));
		registro0000.setCod_mun(empresa.getMunicipiosped() != null ? empresa.getMunicipiosped().getCdibge() : null);
		registro0000.setIm(empresa.getInscricaoestadualst());
//		registro0000.setSuframa(null);
		registro0000.setInd_perfil(empresa.getIndiceperfilsped() != null ? empresa.getIndiceperfilsped().getValue() : null);
		registro0000.setInd_ativ(empresa.getIndiceatividadesped() != null ? empresa.getIndiceatividadesped().ordinal() : null);
		
		registro0000.setRegistro0001(this.createRegistro0001(filtro));
		registro0000.setRegistro0990(this.createRegistro0990(filtro));
		
		if (filtro.getAno() >= 2019) {
			registro0000.setRegistroB001(this.createRegistroB001(filtro));
			registro0000.setRegistroB990(this.createRegistroB990(filtro));
		}
		
		registro0000.setRegistroC001(this.createRegistroC001(filtro));
		registro0000.setRegistroC990(this.createRegistroC990(filtro));
		registro0000.setRegistroD001(this.createRegistroD001(filtro));
		registro0000.setRegistroD990(this.createRegistroD990(filtro));
		registro0000.setRegistroE001(this.createRegistroE001(filtro));
		registro0000.setRegistroE990(this.createRegistroE990(filtro));
		registro0000.setRegistroG001(this.createRegistroG001(filtro));
		registro0000.setRegistroG990(this.createRegistroG990(filtro));
		registro0000.setRegistroH001(this.createRegistroH001(filtro));
		registro0000.setRegistroH990(this.createRegistroH990(filtro));
		registro0000.setRegistroK001(this.createRegistroK001(filtro));
		registro0000.setRegistroK990(this.createRegistroK990(filtro));
		registro0000.setRegistro1001(this.createRegistro1001(filtro));
		registro0000.setRegistro1990(this.createRegistro1990(filtro));
		registro0000.setRegistro9001(this.createRegistro9001(filtro));
		registro0000.setRegistro9990(this.createRegistro9990(filtro));
		
		filtro.getApoio().addRegistros(Registro0000.REG);
		
		return registro0000;
	}
	
	/**
	 * Cria o Registro H990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroH990 createRegistroH990(SpedarquivoFiltro filtro) {
		RegistroH990 registroH990 = new RegistroH990();
		
		registroH990.setReg(RegistroH990.REG);
		registroH990.setQtd_lin_h(2);
		
		filtro.getApoio().addRegistros(RegistroH990.REG);
		
		return registroH990;
	}
	
	/**
	 * Cria o Registro H001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroH001 createRegistroH001(SpedarquivoFiltro filtro) {
		RegistroH001 registroH001 = new RegistroH001();
		
		registroH001.setReg(RegistroH001.REG);
		
		if(filtro.getGerarinventario() != null && filtro.getGerarinventario()){
			registroH001.setInd_mov(0);
			registroH001.setListaRegistroH005(this.createListaRegistroH005(filtro));
		} else {
			registroH001.setInd_mov(1);
		}
		
		filtro.getApoio().addRegistros(RegistroH001.REG);
		
		return registroH001;
	}
	
	/**
	 * Cria a Lista de Registro H005 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/12/2011
	 * @author Rodrigo Freitas
	 */
	private List<RegistroH005> createListaRegistroH005(SpedarquivoFiltro filtro) {
		
		RegistroH005 registroH005 = new RegistroH005();
		registroH005.setReg(RegistroH005.REG);
		
		if(filtro.getInventario() != null && filtro.getInventario().size() > 0){
			List<Inventario> listaInventario = inventarioService.findForSpedRegistroH010(filtro);
			Motivoinventario motivoinventario = null;
			Integer mesinventario = null;
			Integer anoinventario = null;
			Date dtinventario = null;
			for (Inventario inventario : listaInventario) {
				if(motivoinventario == null || mesinventario == null || anoinventario == null){
					motivoinventario = inventario.getMotivoinventario();
					mesinventario = SinedDateUtils.getMes(inventario.getDtinventario());
					anoinventario = SinedDateUtils.getAno(inventario.getDtinventario());
					dtinventario = inventario.getDtinventario();
				} else {
					if(!motivoinventario.equals(inventario.getMotivoinventario())) filtro.addError("N�o � poss�vel selecionar invent�rios com motivos diferentes.");
					if(!mesinventario.equals(SinedDateUtils.getMes(inventario.getDtinventario()))) filtro.addError("N�o � poss�vel selecionar invent�rios com datas diferentes.");
					if(!anoinventario.equals(SinedDateUtils.getAno(inventario.getDtinventario()))) filtro.addError("N�o � poss�vel selecionar invent�rios com datas diferentes.");
				}
			}
			registroH005.setListaRegistroH010(this.createListaRegistroH010(filtro, listaInventario));
			registroH005.setDt_inv(SinedDateUtils.lastDateOfMonth(dtinventario));
			registroH005.setMot_inv(motivoinventario != null ? motivoinventario.getCdSped() : null);
			if(registroH005.getListaRegistroH010() != null && !registroH005.getListaRegistroH010().isEmpty()){
				Double total = new Double(0.0);
				for(RegistroH010 registroH010 : registroH005.getListaRegistroH010()){
					if(registroH010.getVl_item() != null)
						total += registroH010.getVl_item();
				}
				registroH005.setVl_inv(total);
			}
		} else {
			filtro.addError("Selecionar pelo menos um invent�rio para a gera��o correta do arquivo do SPED.");
		}
		
		List<RegistroH005> listaRegistroH005 = new ArrayList<RegistroH005>();
		listaRegistroH005.add(registroH005);
		
		filtro.getApoio().addRegistros(RegistroH005.REG);
		
		return listaRegistroH005;
	}
	
	/**
	 * Cria a Lista de Registro H010 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 19/12/2011
	 * @author Luiz Fernando
	 * @param listaInventario 
	 */
	private List<RegistroH010> createListaRegistroH010(SpedarquivoFiltro filtro, List<Inventario> listaInventario){
		List<RegistroH010> listaRegistroH010 = new ArrayList<RegistroH010>();
		
		Map<String, String> mapMaterialFornecedor = null;
		for (Inventario inventario : listaInventario) {
			inventario = inventario.ajustaBeanToLoad();
			if(inventario.getListaInventariomaterial() != null && inventario.getListaInventariomaterial().size() > 0){
				for (Inventariomaterial inventariomaterial : inventario.getListaInventariomaterial()){
					if(inventariomaterial.getMaterial() == null) continue;
					if(inventariomaterial.getGerarBlocoH()== null || inventariomaterial.getGerarBlocoH().equals(Boolean.FALSE))continue;
					if(inventariomaterial.getGerarBlocoK()==null || inventariomaterial.getGerarBlocoK().equals(Boolean.FALSE))continue;
					
					Material material = inventariomaterial.getMaterial();
					Indicadorpropriedade indicadorpropriedade = inventariomaterial.getIndicadorpropriedade();
					if(inventariomaterial.getPessoa()==null){
						mapMaterialFornecedor = this.makeMapMaterialFornecedor(filtro, mapMaterialFornecedor, material, indicadorpropriedade);
					}else{
						if(indicadorpropriedade.equals(Indicadorpropriedade.PROPRIEDADE_DO_INFORMANTE__POSSE_TERCEIROS) || 
						   indicadorpropriedade.equals(Indicadorpropriedade.PROPRIEDADE_DE_TERCEIROS__POSSE_INFORMANTE)){
							mapMaterialFornecedor = new HashMap<String, String>();
							mapMaterialFornecedor.put(inventariomaterial.getMaterial().getCdmaterial().toString(), filtro.getApoio().addParticipante(inventariomaterial.getPessoa().getCdpessoa()));
						}
					}
					
					if(material.getTipoitemsped() != null && 
							(Tipoitemsped.MERCADORIA_REVENDA.equals(material.getTipoitemsped()) ||		
							Tipoitemsped.MATERIA_PRIMA.equals(material.getTipoitemsped()) ||
							Tipoitemsped.EMBALAGEM.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_PROCESSO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_ACABADO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.SUBPRODUTO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_INTERMEDIARIO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.OUTROS_INSUMOS.equals(material.getTipoitemsped()))){
						
						String cod_item = filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial());
						Integer ind_prop = indicadorpropriedade != null ? indicadorpropriedade.getValue() : null;
						String cod_part = mapMaterialFornecedor != null ? mapMaterialFornecedor.get(filtro.getApoio().addProdutoServico(inventariomaterial.getMaterial().getCdmaterial(), inventariomaterial.getMaterial().getIdentificacaoOuCdmaterial())) : null;
						
						RegistroH010 registroH010 = null;
						for (RegistroH010 registroH010Aux : listaRegistroH010) {
							boolean cod_itemIgual = (cod_item != null && registroH010Aux.getCod_item() != null && cod_item.equals(registroH010Aux.getCod_item())) || (cod_item == null && registroH010Aux.getCod_item() == null);
							boolean ind_propIgual = (ind_prop != null && registroH010Aux.getInd_prop() != null && ind_prop.equals(registroH010Aux.getInd_prop())) || (ind_prop == null && registroH010Aux.getInd_prop() == null);
							boolean cod_partIgual = (cod_part != null && registroH010Aux.getCod_part() != null && cod_part.equals(registroH010Aux.getCod_part())) || (cod_part == null && registroH010Aux.getCod_part() == null);
							
							if(cod_itemIgual && ind_propIgual && cod_partIgual){
								registroH010 = registroH010Aux;
								break;
							}
						}
						
						if(registroH010 != null){
							registroH010.setQtd(registroH010.getQtd() + inventariomaterial.getQtde());
						} else {
							registroH010 = new RegistroH010();
							registroH010.setReg(RegistroH010.REG);
							registroH010.setVersaoFiscal(filtro.getVersaoFiscal());
							registroH010.setCod_item(cod_item);
							registroH010.setUnid(filtro.getApoio().addUnidade(inventariomaterial.getMaterial().getUnidademedida().getCdunidademedida()));
							
							Double qtde = 0d;
							if(inventariomaterial.getQtde() != null){
								qtde = inventariomaterial.getQtde();
							}
							registroH010.setQtd(qtde);
							
							Double vl_unit = 0d;
							if(inventariomaterial.getValorUnitario() != null && inventariomaterial.getValorUnitario() > 0){
								vl_unit = inventariomaterial.getValorUnitario();
							} else if(inventariomaterial.getMaterial().getValorcusto() != null){
								vl_unit = inventariomaterial.getMaterial().getValorcusto();
							}
							
							registroH010.setVl_unit(vl_unit);
							registroH010.setVl_item(vl_unit * qtde);
							registroH010.setInd_prop(ind_prop);
							registroH010.setCod_part(cod_part);
							
							if(inventariomaterial.getMaterial().getProduto_anotacoes() != null)
								registroH010.setTxt_compl(inventariomaterial.getMaterial().getProduto_anotacoes());
							
							//verificar (era buscada a conta contabil do inventariomaterial ou do inventario, acrescentei a do materialcustoempresa)
							String contaContabil = "";
							if(inventariomaterial.getContaanaliticacontabil() != null){
								contaContabil = inventariomaterial.getContaanaliticacontabil();
							}else if (inventario.getCodigocontaanalitica() != null){
								contaContabil = inventario.getCodigocontaanalitica();
							}else if(getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), material, inventario.getEmpresa()) != null){
								ContaContabil contaContabilBean = getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), material, inventario.getEmpresa());
								if(contaContabilBean.getVcontacontabil() != null && contaContabilBean.getVcontacontabil().getIdentificador() != null){
									contaContabil = contaContabilBean.getVcontacontabil().getIdentificador();									
								}
							}
							registroH010.setCod_cta(contaContabil);
							
							listaRegistroH010.add(registroH010);
							
							filtro.getApoio().addRegistros(RegistroH010.REG);
						}
					}
				}
			}
		}
		
		return listaRegistroH010;
	}
	
	/**
	 * Cria o Registro K001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Jo�o Vitor
	 */
	private RegistroK001 createRegistroK001(SpedarquivoFiltro filtro) {
		RegistroK001 registroK001 = new RegistroK001();
		registroK001.setReg(RegistroK001.REG);
		if(filtro.getGerarblocok() != null && filtro.getGerarblocok() || Boolean.TRUE.equals(filtro.getGerarRegistroK280())){
			registroK001.setRegistroK100(this.createRegistroK100(filtro));
		}
		filtro.getApoio().addRegistros(RegistroK001.REG);
		return registroK001;
	}
	
	/**
	 * Cria o Registro K100 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/01/2015
	 * @author Jo�o Vitor
	 */
	private RegistroK100 createRegistroK100(SpedarquivoFiltro filtro) {
		
		RegistroK100 registroK100 = new RegistroK100();
		
		registroK100.setReg(RegistroK100.REG);
		registroK100.setDt_ini(filtro.getDtinicio());
		registroK100.setDt_fin(filtro.getDtfim());
		
		if(Boolean.TRUE.equals(filtro.getGerarblocok()) && filtro.getInventarioEstoque() != null && filtro.getInventarioEstoque().size() > 0){
			registroK100.setListaRegistroK200(this.createListaRegistroK200(filtro));
		}
		
		if(Boolean.TRUE.equals(parametrogeralService.getBoolean(Parametrogeral.GERAR_SPED_COMPOSICAO_KIT))){
			registroK100.setListaRegistroK210(this.createListaRegistroK210(filtro));
		}
		
		if(Boolean.TRUE.equals(filtro.getGerarRegistroK280()) && filtro.getInventarioCorrecao() != null && filtro.getInventarioCorrecao().size() > 0){
			registroK100.setListaRegistroK280(this.createListaRegistroK280(filtro));
		}
		
//		if(filtro.getInventario() != null || Boolean.TRUE.equals(filtro.getGerarRegistroK280())){
//			if(Boolean.TRUE.equals(filtro.getGerarRegistroK280())){
//				filtro = ajutarfiltro(filtro);
//				registroK100.setListaRegistroK200(this.createListaRegistroK200(filtro));
//				registroK100.setListaRegistroK280(this.createListaRegistroK280(filtro));
//			}else{
//				registroK100.setListaRegistroK200(this.createListaRegistroK200(filtro));
//			}
//		}
		
		registroK100.setListaRegistroK230(this.createListaRegistroK230(filtro));
		registroK100.setListaRegistroK250(this.createListaRegistroK250(filtro));
		
		filtro.getApoio().addRegistros(RegistroK100.REG);
		
		return registroK100;
	}
	
	/**
	 * Cria a Lista do Registro K280 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/01/2015
	 * @author Jo�o Vitor
	 */
	private List<RegistroK280> createListaRegistroK280(SpedarquivoFiltro filtro) {
		List<RegistroK280> listaRegistroK280 = new ArrayList<RegistroK280>();
		
		Map <String, String> mapMaterialFornecedor = null;
		List<Inventario> listaInventario = inventarioService.findForSpedRegistroK280(filtro);
		for (Inventario inventario : listaInventario) {
			inventario = inventario.ajustaBeanToLoad();
			if(inventario.getListaInventariomaterial() != null && inventario.getListaInventariomaterial().size() > 0){
				for (Inventariomaterial inventariomaterial : inventario.getListaInventariomaterial()){
					if(inventariomaterial.getMaterial() == null) continue;
					if(inventariomaterial.getGerarBlocoH()== null || inventariomaterial.getGerarBlocoH().equals(Boolean.FALSE))continue;
					if(inventariomaterial.getGerarBlocoK()==null || inventariomaterial.getGerarBlocoK().equals(Boolean.FALSE))continue;
					
					Material material = inventariomaterial.getMaterial();
					Indicadorpropriedade indicadorpropriedade = inventariomaterial.getIndicadorpropriedade();
					mapMaterialFornecedor = this.makeMapMaterialFornecedor(filtro, mapMaterialFornecedor, material, indicadorpropriedade);
					
					if(material.getTipoitemsped() != null && 
							(Tipoitemsped.MERCADORIA_REVENDA.equals(material.getTipoitemsped()) ||		
							Tipoitemsped.MATERIA_PRIMA.equals(material.getTipoitemsped()) ||
							Tipoitemsped.EMBALAGEM.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_PROCESSO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_ACABADO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.SUBPRODUTO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_INTERMEDIARIO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.OUTROS_INSUMOS.equals(material.getTipoitemsped()))){
						String cod_item = filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial());
						String ind_est = indicadorpropriedade != null ? indicadorpropriedade.getValue().toString() : null;
						String cod_part = mapMaterialFornecedor != null ? mapMaterialFornecedor.get(filtro.getApoio().addProdutoServico(inventariomaterial.getMaterial().getCdmaterial(), inventariomaterial.getMaterial().getIdentificacaoOuCdmaterial())) : null;
						
						RegistroK280 registroK280 = null;
						for (RegistroK280 registroK280Aux : listaRegistroK280) {
							boolean cod_itemIgual = (cod_item != null && registroK280Aux.getCod_item() != null && cod_item.equals(registroK280Aux.getCod_item())) || (cod_item == null && registroK280Aux.getCod_item() == null);
							boolean ind_estIgual = (ind_est != null && registroK280Aux.getInd_est() != null && ind_est.equals(registroK280Aux.getInd_est())) || (ind_est == null && registroK280Aux.getInd_est() == null);
							boolean cod_partIgual = (cod_part != null && registroK280Aux.getCod_part() != null && cod_part.equals(registroK280Aux.getCod_part())) || (cod_part == null && registroK280Aux.getCod_part() == null);
							
							if(cod_itemIgual && ind_estIgual && cod_partIgual){
								registroK280 = registroK280Aux;
								break;
							}
						}
						
						Double atual  = 0.0;
						Double origem = 0.0;
						if (inventariomaterial != null && (inventariomaterial.getQtde() != null || inventariomaterial.getQtdeOrigem() !=null)) {
							atual = inventariomaterial.getQtde();
							origem = inventariomaterial.getQtdeOrigem();
							if(atual == null){
								atual = 0.0;
							}
							if(origem == null){
								origem = 0.0;
							}
						}
						
						Double qtd = atual - origem;
						if(registroK280 != null){
							registroK280.setQtd(registroK280.getQtd() + qtd);
						} else {
							registroK280 = new RegistroK280();
							registroK280.setReg(RegistroK200.REG);
							registroK280.setDt_est(filtro.getDtfim());
							registroK280.setCod_item(cod_item);
							registroK280.setQtd(qtd);
							registroK280.setInd_est(ind_est);
							registroK280.setCod_part(cod_part);
							listaRegistroK280.add(registroK280);
							filtro.getApoio().addRegistros(RegistroK280.REG);
						}
					}
				}
			}
		}
		return listaRegistroK280;
	}
	
	
	
	/**
	 * Cria a Lista do Registro K200 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/01/2015
	 * @author Jo�o Vitor
	 */
	private List<RegistroK200> createListaRegistroK200(SpedarquivoFiltro filtro) {
		List<RegistroK200> listaRegistroK200 = new ArrayList<RegistroK200>();
		
		Map<String, String> mapMaterialFornecedor = null;
		List<Inventario> listaInventario = inventarioService.findForSpedRegistroK200(filtro);
		for (Inventario inventario : listaInventario) {
			inventario = inventario.ajustaBeanToLoad();
			if(inventario.getListaInventariomaterial() != null && inventario.getListaInventariomaterial().size() > 0){
				for (Inventariomaterial inventariomaterial : inventario.getListaInventariomaterial()){
					if(inventariomaterial.getMaterial() == null) continue;
					
					Material material = inventariomaterial.getMaterial();
					Indicadorpropriedade indicadorpropriedade = inventariomaterial.getIndicadorpropriedade();
					mapMaterialFornecedor = this.makeMapMaterialFornecedor(filtro, mapMaterialFornecedor, material, indicadorpropriedade);
					
					if(material.getTipoitemsped() != null && 
							(Tipoitemsped.MERCADORIA_REVENDA.equals(material.getTipoitemsped()) ||		
							Tipoitemsped.MATERIA_PRIMA.equals(material.getTipoitemsped()) ||
							Tipoitemsped.EMBALAGEM.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_PROCESSO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_ACABADO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.SUBPRODUTO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.PRODUTO_INTERMEDIARIO.equals(material.getTipoitemsped()) ||
							Tipoitemsped.OUTROS_INSUMOS.equals(material.getTipoitemsped()))){
						
						String cod_item = filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial());
						String ind_est = indicadorpropriedade != null ? indicadorpropriedade.getValue().toString() : null;
						String cod_part = mapMaterialFornecedor != null ? mapMaterialFornecedor.get(filtro.getApoio().addProdutoServico(inventariomaterial.getMaterial().getCdmaterial(), inventariomaterial.getMaterial().getIdentificacaoOuCdmaterial())) : null;
						
						RegistroK200 registroK200 = null;
						for (RegistroK200 registroK200Aux : listaRegistroK200) {
							boolean cod_itemIgual = (cod_item != null && registroK200Aux.getCod_item() != null && cod_item.equals(registroK200Aux.getCod_item())) || (cod_item == null && registroK200Aux.getCod_item() == null);
							boolean ind_estIgual = (ind_est != null && registroK200Aux.getInd_est() != null && ind_est.equals(registroK200Aux.getInd_est())) || (ind_est == null && registroK200Aux.getInd_est() == null);
							boolean cod_partIgual = (cod_part != null && registroK200Aux.getCod_part() != null && cod_part.equals(registroK200Aux.getCod_part())) || (cod_part == null && registroK200Aux.getCod_part() == null);
							
							if(cod_itemIgual && ind_estIgual && cod_partIgual){
								registroK200 = registroK200Aux;
								break;
							}
						}
						
						if(registroK200 != null){
							registroK200.setQtd(registroK200.getQtd() + inventariomaterial.getQtde());
						} else {
							registroK200 = new RegistroK200();
							registroK200.setReg(RegistroK200.REG);
							registroK200.setDt_est(filtro.getDtfim());
							registroK200.setCod_item(cod_item);
							registroK200.setQtd(inventariomaterial.getQtde());
							registroK200.setInd_est(ind_est);
							registroK200.setCod_part(cod_part);
							listaRegistroK200.add(registroK200);
							filtro.getApoio().addRegistros(RegistroK200.REG);
						}
					}
				}
			}
		}

		return listaRegistroK200;
	}
	
	private Map<String, String> makeMapMaterialFornecedor(SpedarquivoFiltro filtro, Map<String, String> mapMaterialFornecedor, Material material, Indicadorpropriedade indicadorpropriedade) {
		if (indicadorpropriedade != null) {
			if (indicadorpropriedade.equals(Indicadorpropriedade.PROPRIEDADE_DO_INFORMANTE__POSSE_TERCEIROS)) {
				List<Notafiscalproduto> listaNotaFiscalProduto = notafiscalprodutoService.findForSpedRegK200(material, filtro);
				if (SinedUtil.isListNotEmpty(listaNotaFiscalProduto)) {
					mapMaterialFornecedor = new HashMap<String, String>();
					for (Notafiscalproduto notafiscalproduto: listaNotaFiscalProduto) {
						if (notafiscalproduto != null && SinedUtil.isListNotEmpty(notafiscalproduto.getListaItens())) {
							for (Notafiscalprodutoitem notafiscalprodutoitem:notafiscalproduto.getListaItens()) {
								if (notafiscalprodutoitem != null && notafiscalprodutoitem.getMaterial() != null 
										&& notafiscalprodutoitem.getMaterial().getCdmaterial() != null
										&& filtro.getApoio() != null && filtro.getApoio().getMapParticipante() != null 
										&& notafiscalproduto.getCliente() != null
										&& notafiscalproduto.getCliente().getCdpessoa() != null) {
									mapMaterialFornecedor.put(notafiscalprodutoitem.getMaterial().getCdmaterial().toString(), filtro.getApoio().addParticipante(notafiscalproduto.getCliente().getCdpessoa()));
								}
							}
						}
					}
				}
			} else if(indicadorpropriedade.equals(Indicadorpropriedade.PROPRIEDADE_DE_TERCEIROS__POSSE_INFORMANTE)) {
				List<Entregadocumento> listaEntregaDocumento = entradafiscalService.findForSpedRegK200(material, filtro);
				if (SinedUtil.isListNotEmpty(listaEntregaDocumento)) {
					mapMaterialFornecedor = new HashMap<String, String>();
					for (Entregadocumento entregadocumento : listaEntregaDocumento) {
						if (entregadocumento != null && SinedUtil.isListNotEmpty(entregadocumento.getListaEntregamaterial())) {
							for (Entregamaterial entregamaterial: entregadocumento.getListaEntregamaterial()) {
								if (entregamaterial != null && entregamaterial.getMaterial() != null 
										&& entregamaterial.getMaterial().getCdmaterial() != null
										&& filtro.getApoio() != null && filtro.getApoio().getMapParticipante() != null 
										&& entregadocumento.getFornecedor() != null
										&& entregadocumento.getFornecedor().getCdpessoa() != null) {
									mapMaterialFornecedor.put(entregamaterial.getMaterial().getCdmaterial().toString(), filtro.getApoio().addParticipante(entregadocumento.getFornecedor().getCdpessoa()));
								}
							}
						}
					}
				}
			}
		}
		
		return mapMaterialFornecedor;
	}
	
//	/**
//	 * Cria a Lista do Registro K220 do arquivo do SPED.
//	 *
//	 * @param filtro
//	 * @return
//	 * @since 15/01/2015
//	 * @author Jo�o Vitor
//	 */
//	private List<RegistroK220> createListaRegistroK220(SpedarquivoFiltro filtro, List<Object> movimentacaointerna) {
//		
//		List<RegistroK220> listaRegistroK220 = new ArrayList<RegistroK220>();
//		
//		if (SinedUtil.isListNotEmpty(movimentacaointerna)) {
//			for (Object movimentacao : movimentacaointerna) {
//				RegistroK220 registroK220 = new RegistroK220();
//				registroK220.setReg(RegistroK220.REG);
//				registroK220.setCod_item_dest(null);
//				registroK220.setCod_item_ori(null);
//				registroK220.setDt_mov(null);
//				registroK220.setQtd(null);
//				
//				listaRegistroK220.add(registroK220);
//				filtro.getApoio().addRegistros(RegistroK220.REG);
//			}
//		}
//		return listaRegistroK220;
//	}
	
	private List<RegistroK230> createListaRegistroK230(SpedarquivoFiltro filtro) {
		List<RegistroK230> listaRegistroK230 = new ArrayList<RegistroK230>();
		
		//busca as materias primas utilizadas com origem em ordem de produ��o
		List<Movimentacaoestoque> movimentacaoestoques = movimentacaoestoqueService.findMovimentacoesSaidaMateriasProducao(filtro);
		String whereIn = "";
		//whereIn do cdproducao das materias primas gastas, para ser usado na consulta que busca a listaOrdensAgenda
		if(SinedUtil.isListNotEmpty(movimentacaoestoques)){
			for (Movimentacaoestoque movimentacaoestoque : movimentacaoestoques) {
				if(movimentacaoestoque.getMovimentacaoestoqueorigem() != null && movimentacaoestoque.getMovimentacaoestoqueorigem().getProducaoordem() != null && movimentacaoestoque.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem() != null){
					whereIn = whereIn + movimentacaoestoque.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem().toString() + ", ";
				}
			}
			if(whereIn != null){
				whereIn = whereIn.substring(0, whereIn.length()-2);
			}			
		}
		
		List<OrdemProducaoSped> listaOrdemAvulsas = producaoordemService.findOrdensAvulsasForRegistroK230(filtro);
		List<OrdemProducaoSped> listaOrdensAgenda = producaoordemService.findOrdemOrigemAgendaForRegistroK230(filtro, whereIn);
		
		if(SinedUtil.isListNotEmpty(listaOrdemAvulsas)){
			for (OrdemProducaoSped ordemAvulsa : listaOrdemAvulsas) {
				RegistroK230 registroK230 = new RegistroK230();
				
				registroK230.setListaRegistroK235(this.createListaRegistroK235(filtro, ordemAvulsa.getCodigo(), ordemAvulsa.getCdmaterial(), true));
				
				//o registro k230 s� ser� enviado caso tenha algum registro k235
				if(SinedUtil.isListNotEmpty(registroK230.getListaRegistroK235())){
					
					registroK230.setReg(RegistroK230.REG);
					
					if(ordemAvulsa.getDtInicio() != null){
						registroK230.setDt_ini_op(ordemAvulsa.getDtInicio());
					}
					if(ordemAvulsa.getDtConclusao() != null && SinedDateUtils.beforeOrEqualIgnoreHour(ordemAvulsa.getDtConclusao(), filtro.getDtfim()) && ordemAvulsa.getSituacao() == 2){
						registroK230.setDt_fin_op(ordemAvulsa.getDtConclusao());
					}
					if(ordemAvulsa.getCodigo() != null){
						//O concatenado para indicar que � uma ordem avulsa
						registroK230.setCod_doc_op("A" + ordemAvulsa.getCodigo().toString());
					}
					if((ordemAvulsa.getCdmaterial() != null || ordemAvulsa.getIdentificacao() != null) && filtro.getApoio() != null && filtro.getApoio().getMapProdutoServico() != null){
						registroK230.setCod_item(filtro.getApoio().addProdutoServico(ordemAvulsa.getCdmaterial(), ordemAvulsa.getIdentificacaoOuCdmaterial()));
					}
					if(ordemAvulsa.getQuantidadeMaterial() != null){
						registroK230.setQtd_enc(ordemAvulsa.getQuantidadeMaterial());
					}
				
					filtro.getApoio().addRegistros(RegistroK230.REG);
					
					listaRegistroK230.add(registroK230);
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaOrdensAgenda)){
			for (OrdemProducaoSped ordemAgenda : listaOrdensAgenda) {
				RegistroK230 registroK230 = new RegistroK230();
				
				registroK230.setListaRegistroK235(this.createListaRegistroK235(filtro, ordemAgenda.getCodigo(), ordemAgenda.getCdmaterial(), false));
				
				//o registro k230 s� ser� enviado caso tenha algum registro k235
				if(SinedUtil.isListNotEmpty(registroK230.getListaRegistroK235())){
				
					registroK230.setReg(RegistroK230.REG);
					
					if(ordemAgenda.getDtInicio() != null){
						registroK230.setDt_ini_op(ordemAgenda.getDtInicio());
					}
					if(ordemAgenda.getDtConclusao() != null && SinedDateUtils.beforeOrEqualIgnoreHour(ordemAgenda.getDtConclusao(), filtro.getDtfim()) && ordemAgenda.getSituacao() == 2){
						registroK230.setDt_fin_op(ordemAgenda.getDtConclusao());
					}
					if(ordemAgenda.getCodigo() != null){
						registroK230.setCod_doc_op(ordemAgenda.getCodigo().toString());
					}
					if((ordemAgenda.getCdmaterial() != null || ordemAgenda.getIdentificacao() != null) && filtro.getApoio() != null && filtro.getApoio().getMapProdutoServico() != null){
						registroK230.setCod_item(filtro.getApoio().addProdutoServico(ordemAgenda.getCdmaterial(), ordemAgenda.getIdentificacaoOuCdmaterial()));
					}
					if(ordemAgenda.getQuantidadeMaterial() != null){
						registroK230.setQtd_enc(ordemAgenda.getQuantidadeMaterial());
					}
				
					filtro.getApoio().addRegistros(RegistroK230.REG);
					
					listaRegistroK230.add(registroK230);
				}
			}
		}
		
		return listaRegistroK230;
	}
	
	private List<RegistroK235> createListaRegistroK235(SpedarquivoFiltro filtro, Integer cdProducaoOrdemOuAgenda, Integer cdMaterialProducao, Boolean ordemAvulsa) {
		List<RegistroK235> listaRegistroK235 = new ArrayList<RegistroK235>();
		
		List<Tipoitemsped> listaTipoitemsped = new ArrayList<Tipoitemsped>();
		listaTipoitemsped.add(Tipoitemsped.MERCADORIA_REVENDA);
		listaTipoitemsped.add(Tipoitemsped.MATERIA_PRIMA);
		listaTipoitemsped.add(Tipoitemsped.EMBALAGEM);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_PROCESSO);
		listaTipoitemsped.add(Tipoitemsped.PRODUTO_ACABADO);
		listaTipoitemsped.add(Tipoitemsped.SUBPRODUTO);
		listaTipoitemsped.add(Tipoitemsped.OUTROS_INSUMOS); 
		
		if(Boolean.TRUE.equals(ordemAvulsa)){
			List<Movimentacaoestoque> listaMateriaPrimaOrdensAvulsas = movimentacaoestoqueService.findMateriasPrimasOrdemAvulsaForK235(filtro, cdProducaoOrdemOuAgenda, cdMaterialProducao, listaTipoitemsped);
			
			if(SinedUtil.isListNotEmpty(listaMateriaPrimaOrdensAvulsas)){
				for (Movimentacaoestoque movimentacaoestoque : listaMateriaPrimaOrdensAvulsas) {
					RegistroK235 registroK235 = new RegistroK235();
					registroK235.setReg(RegistroK235.REG);
					
					if(movimentacaoestoque.getDtmovimentacao() != null){
						registroK235.setDt_saida(movimentacaoestoque.getDtmovimentacao());
					}
					if(movimentacaoestoque.getCdmaterial() != null){
						registroK235.setCod_item(filtro.getApoio().addProdutoServico(movimentacaoestoque.getCdmaterial(), movimentacaoestoque.getIdentificacaoOuCdmaterial()));
					}
					if(movimentacaoestoque.getQtde() != null){
						registroK235.setQtd(movimentacaoestoque.getQtde());
					}
					
					listaRegistroK235.add(registroK235);
					filtro.getApoio().addRegistros(RegistroK235.REG);
				}				
			}
		}else {
			List<Movimentacaoestoque> listaMateriaPrimaAgenda = movimentacaoestoqueService.findMateriasPrimasOrdemAgenda(filtro, cdProducaoOrdemOuAgenda, cdMaterialProducao, listaTipoitemsped);
			
			if(SinedUtil.isListNotEmpty(listaMateriaPrimaAgenda)){
				for (Movimentacaoestoque movimentacaoestoque : listaMateriaPrimaAgenda) {
					RegistroK235 registroK235 = new RegistroK235();
					registroK235.setReg(RegistroK235.REG);
					
					if(movimentacaoestoque.getDtmovimentacao() != null){
						registroK235.setDt_saida(movimentacaoestoque.getDtmovimentacao());
					}
					if(movimentacaoestoque.getCdmaterial() != null){
						registroK235.setCod_item(filtro.getApoio().addProdutoServico(movimentacaoestoque.getCdmaterial(), movimentacaoestoque.getIdentificacaoOuCdmaterial()));
					}
					if(movimentacaoestoque.getQtde() != null){
						registroK235.setQtd(movimentacaoestoque.getQtde());
					}
					
					listaRegistroK235.add(registroK235);
					filtro.getApoio().addRegistros(RegistroK235.REG);
				}
			}
		}
		
		return listaRegistroK235;
	}

	/**
	 * Cria a Lista do Registro K250 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/01/2015
	 * @author Jo�o Vitor
	 */
	private List<RegistroK250> createListaRegistroK250(SpedarquivoFiltro filtro) {
		
		List<RegistroK250> listaRegistroK250 = new ArrayList<RegistroK250>();
		List<Entregamaterial> listaEntregaMaterial = entregamaterialService.findForSpedRegistroK250(filtro);

		if (SinedUtil.isListNotEmpty(listaEntregaMaterial)) {
				for (Entregamaterial entregaMaterial : listaEntregaMaterial) {
					RegistroK250 registroK250 = new RegistroK250();
					registroK250.setReg(RegistroK250.REG);
					
					if (entregaMaterial != null && entregaMaterial.getEntregadocumento() != null && entregaMaterial.getEntregadocumento().getDtemissao() != null) {
						registroK250.setDt_prod(entregaMaterial.getEntregadocumento().getDtemissao());
					}
					
					if (entregaMaterial != null && entregaMaterial.getMaterial() != null && entregaMaterial.getMaterial().getCdmaterial() != null) {
						registroK250.setCod_item(filtro.getApoio().addProdutoServico(entregaMaterial.getMaterial().getCdmaterial(), entregaMaterial.getMaterial().getIdentificacaoOuCdmaterial()));
					}
					
					if (entregaMaterial != null && entregaMaterial.getQtde() != null) {
						registroK250.setQtd(entregaMaterial.getQtde());
					}
					
					if (entregaMaterial != null && entregaMaterial.getMaterial() != null && SinedUtil.isListNotEmpty(entregaMaterial.getMaterial().getListaProducao())) {
					registroK250.setListaRegistroK255(this.createListaRegistroK255(filtro, entregaMaterial.getMaterial().getListaProducao()));
					
					listaRegistroK250.add(registroK250);
					
					filtro.getApoio().addRegistros(RegistroK250.REG);
				}
			}
		}
		return listaRegistroK250;
	}
	
	/**
	 * Cria a Lista do Registro K255 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @since 15/01/2015
	 * @author Jo�o Vitor
	 */
	private List<RegistroK255> createListaRegistroK255(SpedarquivoFiltro filtro, Set<Materialproducao> listaMaterialProducao) {
		
		List<RegistroK255> listaRegistroK255 = new ArrayList<RegistroK255>();
		
		if (SinedUtil.isListNotEmpty(listaMaterialProducao)) {
			for (Materialproducao materialproducao: listaMaterialProducao) {
				RegistroK255 registroK255 = new RegistroK255();
				registroK255.setReg(RegistroK255.REG);
				if(materialproducao != null && materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() != null ){
					registroK255.setCod_item(filtro.getApoio().addProdutoServico(materialproducao.getMaterial().getCdmaterial(), materialproducao.getMaterial().getIdentificacaoOuCdmaterial()));
				}
				if(materialproducao != null && materialproducao.getConsumo() != null){
					registroK255.setQtd(materialproducao.getConsumo());
				}
				
				registroK255.setDt_cons(null);
				
				registroK255.setCod_ins_subst(null);
			
				listaRegistroK255.add(registroK255);
				filtro.getApoio().addRegistros(RegistroK255.REG);
			}
		}
		return listaRegistroK255;
	}
	
	/**
	 * Cria o Registro K990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Jo�o Vitor
	 */
	private RegistroK990 createRegistroK990(SpedarquivoFiltro filtro) {
		RegistroK990 registrok990 = new RegistroK990();
		registrok990.setReg(RegistroK990.REG);
		filtro.getApoio().addRegistros(RegistroK990.REG);
		return registrok990;
	}
	

	/**
	 * Cria o Registro G990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroG990 createRegistroG990(SpedarquivoFiltro filtro) {
		RegistroG990 registroG990 = new RegistroG990();
		
		registroG990.setReg(RegistroG990.REG);
		
		filtro.getApoio().addRegistros(RegistroG990.REG);
		
		return registroG990;
	}
	
	/**
	 * Cria o Registro G001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroG001 createRegistroG001(SpedarquivoFiltro filtro) {
		RegistroG001 registroG001 = new RegistroG001();
		
		registroG001.setReg(RegistroG001.REG);
		registroG001.setInd_mov(1);
		
		filtro.getApoio().addRegistros(RegistroG001.REG);
		
		return registroG001;
	}
	
	/**
	 * Cria o Registro 9990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro9990 createRegistro9990(SpedarquivoFiltro filtro) {
		Registro9990 registro9990 = new Registro9990();
		
		registro9990.setReg(Registro9990.REG);
		
		filtro.getApoio().addRegistros(Registro9990.REG);
		
		return registro9990;
	}
	
	/**
	 * Cria o Registro 9001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro9001 createRegistro9001(SpedarquivoFiltro filtro) {
		Registro9001 registro9001 = new Registro9001();
		
		registro9001.setReg(Registro9001.REG);
		registro9001.setInd_mov(0);
		
		filtro.getApoio().addRegistros(Registro9001.REG);
		
		return registro9001;
	}
	
	/**
	 * Cria o Registro E990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroE990 createRegistroE990(SpedarquivoFiltro filtro) {
		RegistroE990 registroE990 = new RegistroE990();
		
		registroE990.setReg(RegistroE990.REG);
		
		filtro.getApoio().addRegistros(RegistroE990.REG);
		
		return registroE990;
	}
	
	/**
	 * Cria o Registro 1990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro1990 createRegistro1990(SpedarquivoFiltro filtro) {
		Registro1990 registro1990 = new Registro1990();
		
		registro1990.setReg(Registro1990.REG);
		
		filtro.getApoio().addRegistros(Registro1990.REG);
		
		return registro1990;
	}
	
	/**
	 * Cria o Registro 1001 do arquivo do SPED.
	 * 
	 * OBS: N�o foram informados os registros 1600 e 1700 pois n�o precisam ser informados.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro1001 createRegistro1001(SpedarquivoFiltro filtro) {
		Registro1001 registro1001 = new Registro1001();
		
		registro1001.setReg(Registro1001.REG);
		registro1001.setInd_mov(0);
		
		List<Ajusteicms> listaAjusteIcms = ajusteicmsService.findForApuracaoICMSCredito09(filtro);
		Boolean simIndCart = listaAjusteIcms != null && listaAjusteIcms.size() > 0;
		
		registro1001.setRegistro1010(this.createRegistro1010(filtro, simIndCart));
		if(Boolean.TRUE.equals(filtro.getGerarRegistro1100())){
			registro1001.setListaRegistro1100(this.createRegistra1100(filtro));
		}
		if (simIndCart) {
			registro1001.setListaRegistro1200(createRegistro1200(filtro, listaAjusteIcms));
		}
		
		registro1001.setListaRegistro1600(this.createRegistro1600(filtro));
		
		
		filtro.getApoio().addRegistros(Registro1001.REG);
		
		return registro1001;
	}
	
	
	private List<Registro1100> createRegistra1100(SpedarquivoFiltro filtro) {
		StringBuilder whereIn = new StringBuilder();
		String buscar = "";
		if(SinedUtil.isListNotEmpty(filtro.getListaDeclaracaoExportacao())){
			for (DeclaracaoExportacao ids : filtro.getListaDeclaracaoExportacao()) {
				whereIn.append(ids.getCdDeclaracaoExportacao().toString() + ",");
			}
			buscar = whereIn.substring(0, whereIn.length() - 1);
		} else {
			throw new SinedException("N�o foi encontrado nenhuma declara��o de exporta��o");
		}
		List<Registro1100> listaRegistro1100 = new ArrayList<Registro1100>();
		List<DeclaracaoExportacao> listaDeclaracao = declaracaoExportacaoService.findForSped(buscar);

		for (DeclaracaoExportacao declaracao : listaDeclaracao) {
			Registro1100  registro1100 = new Registro1100();
			if(declaracao.getTipoDocumentoDeclaracao() != null){
				registro1100.setInd_doc(declaracao.getTipoDocumentoDeclaracao().getId());
			}
			registro1100.setNro_de(declaracao.getNumeroDeclaracao());
			registro1100.setDt_de(declaracao.getDtDeclaracao());
			registro1100.setNat_exp(declaracao.getTipoNaturezaDeclaracao().getId());
			registro1100.setNro_re(declaracao.getNumeroRE());
			registro1100.setDt_re(declaracao.getDtRE());
			registro1100.setChc_emb(declaracao.getNumeroEmbarque());
			registro1100.setDt_emb(declaracao.getDtConhecimentoEmbarque());
			registro1100.setDt_avb(declaracao.getDtAverbacaoDE());
			if(declaracao.getTipoConhecimentoEmbarque() != null){
				registro1100.setTp_chc(declaracao.getTipoConhecimentoEmbarque().getDescricao());
			}
			if(declaracao.getPais() !=null && declaracao.getPais().getCdbacen() != null){
				registro1100.setPais(declaracao.getPais().getCdbacen().substring(0,3));	
			}
			registro1100.setListaRegistro1105(createRegistro1105(declaracao,filtro));
			filtro.getApoio().addRegistros(Registro1100.REG);
			listaRegistro1100.add(registro1100);
		}
		
		return listaRegistro1100;
	}
	
	private List<Registro1105> createRegistro1105(DeclaracaoExportacao declaracao, SpedarquivoFiltro filtro){
		HashMap<String, String> matDeclarado = new HashMap<String, String>();
		List<Registro1105> listaRegistro1105 = new ArrayList<Registro1105>();
		if(declaracao.getListaDeclaracaoExportacaoNota() != null){
			for (DeclaracaoExportacaoNota declaracaoNota : declaracao.getListaDeclaracaoExportacaoNota()) {
				Notafiscalproduto nota = declaracaoNota.getNotaFiscalProduto();
				if(SinedUtil.isListNotEmpty(nota.getListaItens())){
					for (Notafiscalprodutoitem item : nota.getListaItens()) {
						Registro1105 r1105 = new Registro1105();
						
						Arquivonfnota arquivonfnota = arquivonfnotaService.findByNotaProduto(nota);			
						if(arquivonfnota != null){
							
							if(arquivonfnota.getArquivonf() !=null && arquivonfnota.getArquivonf().getConfiguracaonfe()!= null){
								r1105.setCod_mod(arquivonfnota.getArquivonf().getConfiguracaonfe().getModelonfe());
							}
							
							r1105.setSerie(nota.getSerienfe());
							String nDoc = nota.getNumero();
							if(nDoc !=null){
								nDoc = br.com.linkcom.neo.util.StringUtils.stringCheia(nDoc, "0", 9, false);
								r1105.setNum_doc(nDoc);
							}
							
							if(VALOR_NFE.equals(r1105.getCod_mod())){
								r1105.setChv_nfe(arquivonfnota.getChaveacesso());
							}
							
							r1105.setDt_doc(nota.getDtEmissao());
							r1105.setCod_item(filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(),item.getMaterial().getIdentificacaoOuCdmaterial()));
							
							if(!matDeclarado.containsKey(r1105.getCod_item().concat(r1105.getNum_doc()))){
								if(TipoNaturezaDaDeclaracao.INDIRETA.equals(declaracao.getTipoNaturezaDeclaracao())){
									r1105.setRegistro1110(createRegistro1110(filtro,item,r1105,nota,arquivonfnota,declaracao));	
								}else{
									r1105.setRegistro1110(null);
								}
								if(filtro.getApoio() != null && filtro.getApoio().getMapMaterialProducao() != null){
									filtro.getApoio().addMaterialProducao(item.getMaterial().getCdmaterial(),item.getMaterial().getIdentificacaoOuCdmaterial());
								}
								matDeclarado.put(r1105.getCod_item().concat(r1105.getNum_doc()), r1105.getCod_item());
								filtro.getApoio().addRegistros(Registro1105.REG);
								listaRegistro1105.add(r1105);
							}
						}
					}
				}
			}
		}
		return listaRegistro1105;
	}
	
	private Registro1110 createRegistro1110(SpedarquivoFiltro filtro, Notafiscalprodutoitem item, Registro1105 r1105, Notafiscalproduto nota, Arquivonfnota arquivonfnota, DeclaracaoExportacao declaracao){
		Registro1110 r1110 = new Registro1110();
		if(nota.getTransportador() != null){
			Fornecedor fornecedor = nota.getTransportador();
			Map <Integer, String> mapMaterialFornecedor = new HashMap<Integer, String>();
			mapMaterialFornecedor.put(item.getMaterial().getCdmaterial(), filtro.getApoio().addParticipante(fornecedor.getCdpessoa()));
			if (mapMaterialFornecedor != null && filtro.getApoio() != null && filtro.getApoio().getMapProdutoServico() != null) {
				String chave = filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(),item.getMaterial().getIdentificacaoOuCdmaterial());
				String valor = mapMaterialFornecedor.get(Integer.parseInt(chave));
				r1110.setCod_part(valor);
			}		
		}
		r1110.setCod_mod(r1105.getCod_mod());
		r1110.setSer(nota.getSerienfe());
		r1110.setNum_doc(r1105.getNum_doc());
		r1110 .setDt_doc(nota.getDtEmissao());
		r1110.setCod_mod(r1105.getCod_mod());
		r1110.setChv_nfe(r1105.getChv_nfe());
		r1110.setNr_memo(null);
		Integer quantidade = item.getQtde().intValue();
		r1110.setQtd(quantidade.toString());
		if(item.getUnidademedida() !=null){
			r1110.setUnid(filtro.getApoio().addUnidade(item.getUnidademedida().getCdunidademedida()));
		}
		filtro.getApoio().addRegistros(Registro1110.REG);
		return r1110;
	}
	
	/**
	 * Cria o Registro 1010 do arquivo do SPED.
	 * 
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro1010 createRegistro1010(SpedarquivoFiltro filtro, Boolean simIndCart) {
		Registro1010 registro1010 = new Registro1010();
		
		registro1010.setReg(Registro1010.REG);
		registro1010.setAno(filtro.getAno());
		if(filtro.getGerarRegistro1100()){
			if(filtro.getListaDeclaracaoExportacao()!=null){
				boolean isDtAverbacao = false;
				for (DeclaracaoExportacao declaracao : filtro.getListaDeclaracaoExportacao()) {
				 	declaracao = declaracaoExportacaoService.load(declaracao);
					if(TipoDocumentoDeclaracao.DECLARACAO_EXPORTACAO.equals(declaracao.getTipoDocumentoDeclaracao())){
						isDtAverbacao= true;
						break;
					}
				}
				if(isDtAverbacao){
					registro1010.setInd_exp("S");
				}else{
					registro1010.setInd_exp("N");
				}	
			}else{
				registro1010.setInd_exp("N");
			}
		}else{
			registro1010.setInd_exp("N");
		}
		registro1010.setInd_ccrf(simIndCart ? "S" : "N");
		registro1010.setInd_comb("N");
		registro1010.setInd_usina("N");
		registro1010.setInd_va("N");
		registro1010.setInd_ee("N");
		if(lancamentosOperacaoCartaoService.existeRegistroPer�odo(filtro)){
			registro1010.setInd_cart("S");
		}else {
			registro1010.setInd_cart("N");			
		}
		registro1010.setInd_form("N");
		registro1010.setInd_aer("N");
		
		if (filtro.getAno() >= 2019) {
			registro1010.setInd_giaf1("N");
			registro1010.setInd_giaf3("N");
			registro1010.setInd_giaf4("N");
		}
        if(filtro.getAno() >= 2020){
            registro1010.setInd_rest_ressarc_compl_icms("N");
        }		
		filtro.getApoio().addRegistros(Registro1010.REG);
		
		return registro1010;
	}
	
	private List<Registro1200> createRegistro1200(SpedarquivoFiltro filtro, List<Ajusteicms> listaAjusteIcms) {
		List<Registro1200> listaRegistro1200 = new ArrayList<Registro1200>();
		
		Double sldCred = 0d, credApr = 0d, credReceb = 0d, credUtil = 0d;
		
		for (Ajusteicms ajusteicms : listaAjusteIcms) {
			Registro1200 registro1200 = new Registro1200();
			registro1200.setReg(Registro1200.REG);
			registro1200.setCod_aj_apur(ajusteicms.getAjusteicmstipo() != null && filtro.getEmpresa() != null ? 
					ajusteicms.getAjusteicmstipo().getCodigoSPED(filtro.getUfSigla()) : "");
			
			List<Notafiscalproduto> listaNfp = new ArrayList<Notafiscalproduto>();
			List<Entregadocumento> listaEd = new ArrayList<Entregadocumento>();
			
			sldCred = saldoIcmsService.getLastMesAnoSaldoIcmsByEmpresa(filtro.getEmpresa(), registro1200.getCod_aj_apur());
			
			registro1200.setSld_cred(SinedUtil.descriptionDecimal(sldCred, true));
			
			registro1200.setListaRegistro1210(createRegistro1210(filtro, listaAjusteIcms, listaNfp, listaEd, ajusteicms));
			
			for (Ajusteicms ajusteIcms : listaAjusteIcms) {
				for (Ajusteicmsdocumento ajusteicmsdocumento : ajusteIcms.getListaAjusteicmsdocumento()) {
					credUtil += ajusteicmsdocumento.getValorajuste() != null && ajusteicmsdocumento.getValorajuste() > 0 ? ajusteicmsdocumento.getValorajuste() : 0d;
				}
			}
			
			for (Notafiscalproduto nfp : listaNfp) {
				for (Notafiscalprodutoitem nfpi : nfp.getListaItens()) {
					if (nfpi.getCfop() != null && ("1601".equals(nfpi.getCfop().getCodigo()) || "1602".equals(nfpi.getCfop().getCodigo()) ||
							"1605".equals(nfpi.getCfop().getCodigo()))) {
						credReceb += nfpi.getValoricms().getValue().doubleValue();
					} else {
						credApr += nfpi.getValoricms().getValue().doubleValue();
					}
				}
			}
			
			for (Entregadocumento ed : listaEd) {
				for (Entregamaterial em : ed.getListaEntregamaterial()) {
					if (em.getCfop() != null && ("1601".equals(em.getCfop().getCodigo()) || "1602".equals(em.getCfop().getCodigo()) ||
							"1605".equals(em.getCfop().getCodigo()))) {
						credReceb += em.getValoricms().getValue().doubleValue();
					} else {
						credApr += em.getValoricms().getValue().doubleValue();
					}
				}
			}
			
			credUtil = SinedUtil.truncateFormat(credUtil);
			credApr = SinedUtil.truncateFormat(credApr);
			credReceb = SinedUtil.truncateFormat(credReceb);
			
			registro1200.setCred_apr(SinedUtil.descriptionDecimal(credApr, true));
			registro1200.setCred_receb(SinedUtil.descriptionDecimal(credReceb, true));
			registro1200.setCred_util(SinedUtil.descriptionDecimal(credUtil, true));
			
			Double sldCredFim = SinedUtil.truncateFormat(sldCred + credApr + credReceb - credUtil);
			registro1200.setSld_cred_fim(SinedUtil.descriptionDecimal(sldCredFim, true));
			
			listaRegistro1200.add(registro1200);
			
			//Salva saldo icms
			SaldoIcms saldoIcms = new SaldoIcms();
			saldoIcms.setMesAno(filtro.getMesano());
			saldoIcms.setMesAnoAux(saldoIcms.getMesAnoAux());
			saldoIcms.setEmpresa(filtro.getEmpresa());
			saldoIcms.setTipoUtilizacaoCredito(registro1200.getCod_aj_apur());
			
			SaldoIcms saldoIcmsFound = saldoIcmsService.getSaldoIcmsByMesAnoEmpresaTipoUtilizacaoCredito(saldoIcms);
			
			if (saldoIcmsFound == null) {
				saldoIcms.setValor(new Money(sldCredFim));
				saldoIcmsService.saveOrUpdate(saldoIcms);
			} else {
				saldoIcmsFound.setValor(new Money(sldCredFim));
				saldoIcmsService.saveOrUpdate(saldoIcmsFound);
			}
			
			filtro.getApoio().addRegistros(Registro1200.REG);
		}
		
		return listaRegistro1200;
	}
	
	private List<Registro1210> createRegistro1210(SpedarquivoFiltro filtro, List<Ajusteicms> listaAjusteIcms,
			List<Notafiscalproduto> listaNfp, List<Entregadocumento> listaEd, Ajusteicms ajusteicms) {
		List<Registro1210> listaRegistro1210 = new ArrayList<Registro1210>();
		
		for (Ajusteicms ajusteIcms : listaAjusteIcms) {
			if (listaAjusteIcms != null && listaAjusteIcms.size() > 0) {
				for (Ajusteicmsdocumento ajusteicmsdocumento : ajusteIcms.getListaAjusteicmsdocumento()) {
					if (ajusteicms.getCdajusteicms().equals(ajusteIcms.getCdajusteicms())) {
						Registro1210 registro1210 = new Registro1210();
						registro1210.setReg(Registro1210.REG);
						registro1210.setTipo_util(ajusteicmsdocumento.getTipoUtilizacaoCredito());
						registro1210.setNr_doc(ajusteicmsdocumento.getNumero());
						
						Double vlCredUtil = SinedUtil.truncateFormat(ajusteicmsdocumento.getValorajuste() != null && ajusteicmsdocumento.getValorajuste() > 0 ? ajusteicmsdocumento.getValorajuste() : 0d);
						registro1210.setVl_cred_util(SinedUtil.descriptionDecimal(vlCredUtil, true));
						
						String chaveAcesso = null;
						
						if (ajusteicmsdocumento.getModelonf() != null && ajusteicmsdocumento.getModelonf().getCodigo() != null && ajusteicmsdocumento.getModelonf().getCodigo().equals("55")) {
							if (!StringUtils.isEmpty(ajusteicmsdocumento.getNumero()) && !StringUtils.isEmpty(ajusteicmsdocumento.getSerie())) {
								Notafiscalproduto notaFiscalProduto = null;
								Integer serie = Integer.parseInt(ajusteicmsdocumento.getSerie());
								
								if (serie != null) {
									notaFiscalProduto = notafiscalprodutoService.findNotaByNumeroSerie(ajusteicmsdocumento.getNumero(), serie);
								}
								
								if (notaFiscalProduto != null) {
									listaNfp.add(notaFiscalProduto);
									
									for (Arquivonfnota arquivonfnota : notaFiscalProduto.getListaArquivonfnota()) {
										if((arquivonfnota.getDtcancelamento() == null && 
												arquivonfnota.getArquivonf() != null && 
												arquivonfnota.getArquivonf().getArquivonfsituacao() != null &&
												arquivonfnota.getArquivonf().getArquivonfsituacao().equals(Arquivonfsituacao.PROCESSADO_COM_SUCESSO)) ||
												arquivonfnota.getDtcancelamento() != null && NotaStatus.CANCELADA.equals(notaFiscalProduto.getNotaStatus())){
											chaveAcesso = arquivonfnota.getChaveacesso();
											break;
										}
									}
								} else {
									Entregadocumento entregadocumento = entradafiscalService.findEntradaFiscalByNumeroSerie(ajusteicmsdocumento.getNumero(), ajusteicmsdocumento.getSerie());
									
									if (entregadocumento != null) {
										listaEd.add(entregadocumento);
										
										if (!StringUtils.isEmpty(entregadocumento.getChaveacesso())) {
											chaveAcesso = entregadocumento.getChaveacesso();
										}
									}
								}
							}
						}
						
						registro1210.setChv_doce(chaveAcesso);
						
						listaRegistro1210.add(registro1210);
						
						filtro.getApoio().addRegistros(Registro1210.REG);
					}
				}
			}
		}
		
		return listaRegistro1210;
	}
	
	/**
	 * Cria o Registro E001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroE001 createRegistroE001(SpedarquivoFiltro filtro) {
		RegistroE001 registroE001 = new RegistroE001();
		
		registroE001.setReg(RegistroE001.REG);
		
		filtro.getApoio().addRegistros(RegistroE001.REG);
		
		return registroE001;
	}
	
	/**
	 * Cria o Registro D990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroD990 createRegistroD990(SpedarquivoFiltro filtro) {
		RegistroD990 registroD990 = new RegistroD990();
		
		registroD990.setReg(RegistroD990.REG);
		
		filtro.getApoio().addRegistros(RegistroD990.REG);
		
		return registroD990;
	}
	
	/**
	 * Cria o Registro D001 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroD100
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroD500
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroD001 createRegistroD001(SpedarquivoFiltro filtro) {
		RegistroD001 registroD001 = new RegistroD001();
		
		registroD001.setReg(RegistroD001.REG);
		
		registroD001.setListaRegistroD100(this.createRegistroD100(filtro));
		registroD001.setListaRegistroD500(this.createRegistroD500(filtro));
		
		filtro.getApoio().addRegistros(RegistroD001.REG);
		
		return registroD001;
	}
	
	/**
	 * Cria o Registro D500 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#getListaDocumentoD500
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#creteRegistroD590
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroD500> createRegistroD500(SpedarquivoFiltro filtro) {
		List<RegistroD500> lista = new ArrayList<RegistroD500>();
		RegistroD500 registroD500;
		Entregamaterial entregamaterial;
		
		List<Entregadocumento> listaDocumento = this.getListaDocumentoD500(filtro);
		for (Entregadocumento ed : listaDocumento) {
			registroD500 = new RegistroD500();
			
			entregamaterial = ed.getListaEntregamaterial().iterator().next();
			
			registroD500.setReg(RegistroD500.REG);
			registroD500.setInd_oper(0);
			registroD500.setInd_emit(1);
			registroD500.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa()) : null);
			registroD500.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroD500.setCod_sit(ed.getSituacaosped());
//			registroD500.setSer(ser)
//			registroD500.setSub(sub)
			registroD500.setNum_doc(ed.getNumero());
			registroD500.setDt_doc(ed.getDtemissao());
			registroD500.setDt_a_p(ed.getDtentrada());
			registroD500.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
//			registroD500.setVl_desc(vl_desc)
			registroD500.setVl_serv(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
//			registroD500.setVl_serv_nt(vl_serv_nt)
//			registroD500.setVl_terc(vl_terc)
//			registroD500.setVl_da(vl_da)
			registroD500.setVl_bc_icms(entregamaterial.getValorbcicms() != null ? entregamaterial.getValorbcicms().getValue().doubleValue() : null);
			registroD500.setVl_icms(entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().getValue().doubleValue() : null);
//			registroD500.setCod_inf(cod_inf)
			registroD500.setVl_pis(entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().getValue().doubleValue() : null);
			registroD500.setVl_cofins(entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().getValue().doubleValue() : null);
			ContaContabil contaContabil = getContagerencialSped(filtro.getApoio(), ed.getNaturezaoperacao(), entregamaterial.getMaterial() != null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), entregamaterial.getMaterial(), ed.getEmpresa()) : null);
			registroD500.setCod_cta(contaContabil !=null && contaContabil.getVcontacontabil()!=null ? contaContabil.getVcontacontabil().getIdentificador() : null);
//			registroD500.setTp_assinante(tp_assinante)
			
			registroD500.setListaRegistroD590(this.createRegistroD590(filtro, entregamaterial, ed));
			
			lista.add(registroD500);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroD500.REG);
		}
		
		return lista;
	}
	
	/**
	 * Cria o Registro D590 do arquivo do SPED.
	 *
	 * @param filtro
	 * @param em
	 * @return
	 * @author Rodrigo Freitas
	 * @param ed 
	 */
	private List<RegistroD590> createRegistroD590(SpedarquivoFiltro filtro, Entregamaterial em, Entregadocumento ed) {
		List<RegistroD590> lista = new ArrayList<RegistroD590>();
		RegistroD590 registroD590 = new RegistroD590();
		
		registroD590.setReg(RegistroD590.REG);
		registroD590.setCst_icms(em.getCsticms() != null ? SPEDUtil.stringCheia(em.getCsticms().getCdnfe(), "0", 3, false) : null);
		registroD590.setCfop(em.getCfop() != null ? em.getCfop().getCodigo() : null);
		registroD590.setAliq_icms(em.getIcms() != null ? em.getIcms() : null);
		registroD590.setVl_opr(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : 0d);
		registroD590.setVl_bc_icms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue() : 0d);
		registroD590.setVl_icms(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue() : 0d);
		registroD590.setVl_bc_icms_st(em.getValorbcicmsst() != null ? em.getValorbcicmsst().getValue().doubleValue() : 0d);
		registroD590.setVl_icms_st(em.getValoricmsst() != null ? em.getValoricmsst().getValue().doubleValue() : 0d);
		if(em.getValoricmsst() != null && em.getValoricmsst().getValue().doubleValue() > 0){
			em.setGerarE200(Boolean.TRUE);
		}
		registroD590.setVl_red_bc(0d);
//		registroD590.setCod_obs(cod_obs)
		
		filtro.getApoio().addRegistros(RegistroD590.REG);
		
		lista.add(registroD590);
		
		return lista;
	}
	
	/**
	 * Cria o Registro D100 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#getListaEntregaD100
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#createRegistroD190
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroD100> createRegistroD100(SpedarquivoFiltro filtro) {
		List<RegistroD100> listaDesagrupada = new ArrayList<RegistroD100>();
		
		RegistroD100 registroD100;
		
		List<Entregadocumento> listaEntregadocumento = this.getListaEntregaD100(filtro);
		for (Entregadocumento ed : listaEntregadocumento) {
			registroD100 = new RegistroD100();		
			
			registroD100.setReg(RegistroD100.REG);
			registroD100.setInd_oper(0);
			registroD100.setInd_emit(1);
			registroD100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa()) : null);
			registroD100.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroD100.setCod_sit(ed.getSituacaosped());
			registroD100.setSer(ed.getSerie() != null ? ed.getSerie().toString() : null);
//			registroD100.setSub(sub)
			registroD100.setNum_doc(ed.getNumero());
			registroD100.setChv_cte(ed.getChaveacesso());
			registroD100.setDt_doc(ed.getDtemissao());
			registroD100.setDt_a_p(ed.getDtentrada());
//			registroD100.setTp_ct_e(tp_ct_e)
//			registroD100.setChv_cte_ref(chv_cte_ref)
			registroD100.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
			registroD100.setVl_desc(ed.getValordesconto() != null ? ed.getValordesconto().getValue().doubleValue() : null);
			registroD100.setInd_frt(ed.getResponsavelfrete() != null ? ed.getResponsavelfrete().getCdnfe() : null);
			registroD100.setVl_serv(ed.getValor() != null ? ed.getValor().getValue().doubleValue() : null);
			registroD100.setVl_bc_icms(ed.getValortotalbcicms() != null ? ed.getValortotalbcicms().getValue().doubleValue() : null);
			registroD100.setVl_icms(ed.getValortotalicms() != null ? ed.getValortotalicms().getValue().doubleValue() : null);
//			registroD100.setVl_nt(vl_nt)
			
			//verificar (nao tem o material)
			ContaContabil contaContabil = getContagerencialSped(filtro.getApoio(), ed.getNaturezaoperacao(), null);
			registroD100.setCod_cta(contaContabil !=null && contaContabil.getVcontacontabil() != null ? contaContabil.getVcontacontabil().getIdentificador() : "");
			registroD100.setCod_mun_or_ig(Boolean.TRUE.equals(ed.getMunicipioorigemexterior()) ? "9999999" : (ed.getMunicipioorigem() != null ? ed.getMunicipioorigem().getCdibge() : null));
			registroD100.setCod_mun_de_st(Boolean.TRUE.equals(ed.getMunicipiodestinoexterior()) ? "9999999" : (ed.getMunicipiodestino() != null ? ed.getMunicipiodestino().getCdibge() : null));
			
			if(ed.getInfoadicionalcontrib() != null && !ed.getInfoadicionalcontrib().trim().equals("") && ed.getInfoadicionalfisco() != null && !ed.getInfoadicionalfisco().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalcontrib() + " " + ed.getInfoadicionalfisco()));
			} else if(ed.getInfoadicionalcontrib() != null && !ed.getInfoadicionalcontrib().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalcontrib()));
			} else if(ed.getInfoadicionalfisco() != null && !ed.getInfoadicionalfisco().trim().equals("")){
				registroD100.setCod_inf(filtro.getApoio().addInfoComplementar(ed.getInfoadicionalfisco()));
			}
			
			registroD100.setListaRegistroD190(this.createRegistroD190(filtro, ed));
			registroD100.setListaRegistroD195(this.createRegistroD195(filtro, ed.getListaAjustefiscal()));
			
			listaDesagrupada.add(registroD100);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroD100.REG);
		}
		
		List<RegistroD100> listaAgrupada = this.joinListaRegistroD100(listaDesagrupada, filtro);
		
		return listaAgrupada;
	}
	
	/**
	 * Agrupa os registros C100.
	 *
	 * @param listaDesagrupada
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	private List<RegistroD100> joinListaRegistroD100(List<RegistroD100> listaDesagrupada, SpedarquivoFiltro filtro) {
		List<RegistroD100> listaAgrupada = new ArrayList<RegistroD100>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroD100 registroD100 : listaDesagrupada) {
				if(listaAgrupada.contains(registroD100)){
					int idx = listaAgrupada.indexOf(registroD100);
					RegistroD100 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroD100(aux, registroD100, filtro);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroD100.REG);
				} else {
					listaAgrupada.add(registroD100);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	/**
	 * Junta dois registros D100 passados por par�metros.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#joinRegistroC140(RegistroC140 ori, RegistroC140 copia)
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#joinListaRegistroC190(List<RegistroC190> ori, List<RegistroC190> copia)
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 */
	private RegistroD100 joinRegistroD100(RegistroD100 ori, RegistroD100 copia, SpedarquivoFiltro filtro) {
		
		ori.setVl_doc((ori.getVl_doc() != null ? ori.getVl_doc() : 0d) + (copia.getVl_doc() != null ? copia.getVl_doc() : 0d));
		ori.setVl_desc((ori.getVl_desc() != null ? ori.getVl_desc() : 0d) + (copia.getVl_desc() != null ? copia.getVl_desc() : 0d));
		ori.setVl_bc_icms((ori.getVl_bc_icms() != null ? ori.getVl_bc_icms() : 0d) + (copia.getVl_bc_icms() != null ? copia.getVl_bc_icms() : 0d));
		ori.setVl_icms((ori.getVl_icms() != null ? ori.getVl_icms() : 0d) + (copia.getVl_icms() != null ? copia.getVl_icms() : 0d));
		ori.setVl_serv((ori.getVl_serv() != null ? ori.getVl_serv() : 0d) + (copia.getVl_serv() != null ? copia.getVl_serv() : 0d));
		ori.setVl_nt((ori.getVl_nt() != null ? ori.getVl_nt() : 0d) + (copia.getVl_nt() != null ? copia.getVl_nt() : 0d));

		// REGISTRO 190
		List<RegistroD190> listaRegistroD190Ori = ori.getListaRegistroD190();
		List<RegistroD190> listaRegistroD190Copia = copia.getListaRegistroD190();
		
		if(listaRegistroD190Ori != null && listaRegistroD190Ori.size() > 0 &&
				listaRegistroD190Copia != null && listaRegistroD190Copia.size() > 0){
			ori.setListaRegistroD190(this.joinListaRegistroD190(listaRegistroD190Ori, listaRegistroD190Copia, filtro));
		} else if(listaRegistroD190Ori != null && listaRegistroD190Ori.size() > 0){
			ori.setListaRegistroD190(listaRegistroD190Ori);
		} else if(listaRegistroD190Copia != null && listaRegistroD190Copia.size() > 0){
			ori.setListaRegistroD190(listaRegistroD190Copia);
		}
		
		return ori;
	}
	
	/**
	 * Junta a lista de Registro D190.
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	private List<RegistroD190> joinListaRegistroD190(List<RegistroD190> ori, List<RegistroD190> copia, SpedarquivoFiltro filtro) {
		
		for (RegistroD190 registroD190 : copia) {
			if(ori.contains(registroD190)){
				int idx = ori.indexOf(registroD190);
				RegistroD190 aux = ori.get(idx);
				
				aux.setVl_opr((aux.getVl_opr() != null ? aux.getVl_opr() : 0d) + (registroD190.getVl_opr() != null ? registroD190.getVl_opr() : 0d));
				aux.setVl_bc_icms((aux.getVl_bc_icms() != null ? aux.getVl_bc_icms() : 0d) + (registroD190.getVl_bc_icms() != null ? registroD190.getVl_bc_icms() : 0d));
				aux.setVl_icms((aux.getVl_icms() != null ? aux.getVl_icms() : 0d) + (registroD190.getVl_icms() != null ? registroD190.getVl_icms() : 0d));
				aux.setVl_red_bc((aux.getVl_red_bc() != null ? aux.getVl_red_bc() : 0d) + (registroD190.getVl_red_bc() != null ? registroD190.getVl_red_bc() : 0d));
				
				ori.set(idx, aux);
				
				filtro.getApoio().removeRegistro(RegistroD190.REG);
			} else {
				ori.add(registroD190);
			}
		}		
		
		return ori;
	}
	
	
	/**
	 * Cria o Registro D190 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#makeRegistroD190
	 *
	 * @param filtro
	 * @param dt
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroD190> createRegistroD190(SpedarquivoFiltro filtro, Entregadocumento ed) {
		List<RegistroAnaliticoSped> listaAnalitico = new ArrayList<RegistroAnaliticoSped>();
		RegistroAnaliticoSped reg;
		Money valorbruto;
		int idx;
		
		Money zero = new Money();
		
		Set<Entregamaterial> lista = ed.getListaEntregamaterial();
		for (Entregamaterial item : lista) {
			reg = new RegistroAnaliticoSped(item.getCstIcmsSPED(), item.getCfop(), 0d);
			
			if(listaAnalitico.contains(reg)){
				idx = listaAnalitico.indexOf(reg);
				reg = listaAnalitico.get(idx);
				listaAnalitico.remove(idx);
			}
			
			if(item.getValortotalmaterial() != null) valorbruto = new Money(item.getValortotalmaterial());
			else valorbruto = new Money();
			
			reg.addValoroperacao(valorbruto);
			reg.addValorbcicms(item.getValorbcicms() != null ? item.getValorbcicms() : zero);
			reg.addValoricms(item.getValoricms() != null ? item.getValoricms() : zero);
//			reg.addValorbcicmsst(item.getValorbcicmsst() != null ? item.getValorbcicmsst() : zero);
//			reg.addValoricmsst(item.getValoricmsst() != null ? item.getValoricmsst() : zero);
//			reg.addValorredbc(valorbruto != null && item.getValorbcicms() != null ? valorbruto.subtract(item.getValorbcicms()) : zero);
			reg.addValoripi(item.getValoripi() != null ? item.getValoripi() : zero);
			
			listaAnalitico.add(reg);
		}
		
		return this.makeRegistroD190(filtro, listaAnalitico);
	}
	
	/**
	 * Faz o registro D190 gen�rico.
	 *
	 * @param listaAnalitico
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroD190> makeRegistroD190(SpedarquivoFiltro filtro, List<RegistroAnaliticoSped> listaAnalitico) {
		
		List<RegistroD190> lista = new ArrayList<RegistroD190>();
		RegistroD190 registroD190;
		
		for (RegistroAnaliticoSped analitico : listaAnalitico) {
			if(analitico.getCst() == null || analitico.getCfop() == null) continue;
			if(analitico.getAliq_icms() == null) analitico.setAliq_icms(0d);
			
			registroD190 = new RegistroD190();
			
			registroD190.setReg(RegistroD190.REG);
			registroD190.setCst_icms(SPEDUtil.stringCheia(analitico.getCst(), "0", 3, false));
			registroD190.setCfop(analitico.getCfop().getCodigo());
			registroD190.setAliq_icms(analitico.getAliq_icms());
			registroD190.setVl_opr(analitico.getValoroperacao().getValue().doubleValue());
			registroD190.setVl_bc_icms(analitico.getValorbcicms().getValue().doubleValue());
			registroD190.setVl_icms(analitico.getValoricms().getValue().doubleValue());
			registroD190.setVl_red_bc(analitico.getValorredbc().getValue().doubleValue());
//			registroD190.setCod_obs(cod_obs)
			
			lista.add(registroD190);
			
			filtro.getApoio().addRegistros(RegistroD190.REG);
		}
		
		return lista;
	}
	
	private List<RegistroD195> createRegistroD195(SpedarquivoFiltro filtro, Set<Ajustefiscal> listaAjustefiscal) {
		List<RegistroD195> lista = new ArrayList<RegistroD195>();
		RegistroD195 registroD195;
		
		if(SinedUtil.isListNotEmpty(listaAjustefiscal)){
			for(Ajustefiscal ajustefiscal : listaAjustefiscal){
				if(ajustefiscal.getObslancamentofiscal() != null){
					registroD195 = new RegistroD195();
					
					registroD195.setReg(RegistroD195.REG);
					registroD195.setCod_obs(filtro.getApoio().addObsLancamentoFiscal(ajustefiscal.getObslancamentofiscal().getDescricao()));
					registroD195.setListaRegistroD197(createRegistroD197(filtro, ajustefiscal));
					
					lista.add(registroD195);
					
					filtro.getApoio().addRegistros(RegistroD195.REG);
				}
			}
		}
		
		return lista;
	}
	
	private List<RegistroD197> createRegistroD197(SpedarquivoFiltro filtro, Ajustefiscal ajustefiscal) {
		List<RegistroD197> lista = new ArrayList<RegistroD197>();
		
		if(ajustefiscal != null){
			RegistroD197 registroD197 = new RegistroD197();
			registroD197.setReg(RegistroD197.REG);
			registroD197.setCod_aj(ajustefiscal.getCodigoajuste());
			registroD197.setDesc_compl_aj(ajustefiscal.getDescricaocomplementar());
			if(ajustefiscal.getMaterial() != null && ajustefiscal.getMaterial().getCdmaterial() != null){
				if(filtro.getApoio() != null && filtro.getApoio().getMapProdutoServico() != null){
					registroD197.setCod_item(filtro.getApoio().addProdutoServico(ajustefiscal.getMaterial().getCdmaterial(), ajustefiscal.getMaterial().getIdentificacaoOuCdmaterial()));
				}
			}
			registroD197.setVl_bc_icms(ajustefiscal.getBasecalculoicms());
			registroD197.setAliq_icms(ajustefiscal.getAliquotaicms());
			registroD197.setVl_icms(ajustefiscal.getValoricms() != null ? ajustefiscal.getValoricms() : 0d);
			registroD197.setVl_outros(ajustefiscal.getOutrosvalores() != null ? ajustefiscal.getOutrosvalores() : 0d);
				
			lista.add(registroD197);
				
			filtro.getApoio().addRegistros(RegistroD197.REG);
		}
		
		return lista;
	}
	
	/**
	 * Cria o Registro C990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroC990 createRegistroC990(SpedarquivoFiltro filtro) {
		RegistroC990 registroC990 = new RegistroC990();
		
		registroC990.setReg(RegistroC990.REG);
		
		filtro.getApoio().addRegistros(RegistroC990.REG);
		
		return registroC990;
	}
	
	/**
	 * Cria o Registro C001 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC100
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC500
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroC001 createRegistroC001(SpedarquivoFiltro filtro) {
		RegistroC001 registroC001 = new RegistroC001();
		
		registroC001.setReg(RegistroC001.REG);
		
		registroC001.setListaRegistroC100(this.createRegistroC100(filtro));
		registroC001.setListaRegistroC400(this.createRegistroC400(filtro));
		registroC001.setListaRegistroC500(this.createRegistroC500(filtro));
		
		filtro.getApoio().addRegistros(RegistroC001.REG);
		
		return registroC001;
	}
	
	private List<RegistroC400> createRegistroC400(SpedarquivoFiltro filtro) {
		List<RegistroC400> lista = new ArrayList<RegistroC400>();
		
		boolean saidas = filtro.getBuscarsaidas() != null && filtro.getBuscarsaidas();
		if(saidas){
			RegistroC400 registroC400;
			Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.integracaopdv");
			if(empresa != null){
				filtro.getEmpresa().setIntegracaopdv(empresa.getIntegracaopdv());
			}
			List<Emporiumpdv> listaEmporiumpdv = emporiumpdvService.findByEmpresa(filtro.getEmpresa());
			for(Emporiumpdv emporiumpdv: listaEmporiumpdv){
				if(Boolean.TRUE.equals(emporiumpdv.getEquipamentoNaoFiscal())){//Para equipamento n�o fiscal, � teoria vai ser gerado o registro C175, que � referente a NFC-e.
					continue;
				}
				List<Emporiumreducaoz> listaEmporiumreducaoz = emporiumreducaozService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
				
				if(listaEmporiumreducaoz != null && listaEmporiumreducaoz.size() > 0){
					registroC400 = new RegistroC400();
					
					registroC400.setReg(RegistroC400.REG);
					registroC400.setCod_mod(emporiumpdv.getModelodocfiscal());
					registroC400.setEcf_mod(emporiumpdv.getModeloequipamento());
					registroC400.setEcf_fab(emporiumpdv.getNumeroserie());
					registroC400.setEcf_cx(emporiumpdv.getCodigoemporium());
					
					registroC400.setListaRegistroC405(this.createRegistroC405(filtro, emporiumpdv, listaEmporiumreducaoz));
					
					filtro.getApoio().addRegistros(RegistroC400.REG);
					
					lista.add(registroC400);
				}
			}
		}
		
		return lista;
	}
	
	private List<RegistroC405> createRegistroC405(SpedarquivoFiltro filtro, Emporiumpdv emporiumpdv, List<Emporiumreducaoz> listaEmporiumreducaoz) {
		List<RegistroC405> lista = new ArrayList<RegistroC405>();
		RegistroC405 registroC405;
		
		List<Emporiumtotalizadores> listaEmporiumtotalizadores = emporiumtotalizadoresService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
		List<Emporiumvenda> listaEmporiumvenda = emporiumvendaService.findForSped(emporiumpdv, filtro.getDtinicio(), filtro.getDtfim());
		List<Emporiumvendaitem> listaEmporiumvendaitem = emporiumvendaService.getEmporiumvendaitemForSpedAcerto(listaEmporiumvenda);
		
		StringBuilder sb = new StringBuilder();
		for(Emporiumvendaitem it: listaEmporiumvendaitem){
			if(it.getMaterial() == null){
				sb.append("Material " + it.getProduto_id() + " n�o encontrado.\n");
			}
		}
		if(sb.length() > 0){
			throw new SinedException(sb.toString());
		}
		
		for(Emporiumreducaoz emporiumreducaoz: listaEmporiumreducaoz){
			registroC405 = new RegistroC405();
			
			registroC405.setReg(RegistroC405.REG);
			registroC405.setDt_doc(emporiumreducaoz.getData());
			registroC405.setCro(emporiumreducaoz.getCro());
			registroC405.setCrz(emporiumreducaoz.getCrz());
			registroC405.setNum_coo_fin(emporiumreducaoz.getNumerocoofinal());
			registroC405.setGt_fin(emporiumreducaoz.getGtfinal());
			registroC405.setVl_brt(emporiumreducaoz.getValorvendabruta());
			
			if(filtro.getGerarregistroc410() != null && filtro.getGerarregistroc410()){
				registroC405.setRegistroC410(this.createRegistroC410(filtro, emporiumreducaoz.getData(), listaEmporiumvendaitem)); 
			}
			registroC405.setListaRegistroC420(this.createRegistroC420(filtro, emporiumreducaoz, emporiumreducaoz.getData(), listaEmporiumvendaitem, listaEmporiumtotalizadores));
			registroC405.setListaRegistroC490(this.createRegistroC490(filtro, emporiumreducaoz.getData(), listaEmporiumtotalizadores));
			
			filtro.getApoio().addRegistros(RegistroC405.REG);
			
			lista.add(registroC405);
		}
		
		return lista;
	}
	
	private List<RegistroC490> createRegistroC490(SpedarquivoFiltro filtro, Date datamovimento, List<Emporiumtotalizadores> listaEmporiumtotalizadores) {
		
		List<RegistroAnaliticoSped> listaAnalitico = new ArrayList<RegistroAnaliticoSped>();
		RegistroAnaliticoSped reg;
		int idx;
		
		List<Emporiumtotalizadores> listaEmporiumtotalizadoresData = emporiumtotalizadoresService.getEmporiumtotalizadoresByDataMovimento(datamovimento, listaEmporiumtotalizadores);
		
		for(Emporiumtotalizadores emporiumtotalizadores: listaEmporiumtotalizadoresData){
			
			Tributacaoecf tributacaoecf = emporiumtotalizadores.getTributacaoecf();
			if(tributacaoecf == null) continue;
			
			reg = new RegistroAnaliticoSped((tributacaoecf.getCsticms() != null ? tributacaoecf.getCsticms().getCdnfe() : null), 
											tributacaoecf.getCfop(), 
											emporiumtotalizadores.getAliqicms());
			
			if(listaAnalitico.contains(reg)){
				idx = listaAnalitico.indexOf(reg);
				reg = listaAnalitico.get(idx);
				listaAnalitico.remove(idx);
			}
			
			reg.addValoroperacao(emporiumtotalizadores.getValorbcicms() != null ? new Money(emporiumtotalizadores.getValorbcicms()) : new Money());
			reg.addValorbcicms(emporiumtotalizadores.getValorbcicms() != null ? new Money(emporiumtotalizadores.getValorbcicms()) : new Money());
			reg.addValoricms(emporiumtotalizadores.getValoricms() != null ? new Money(emporiumtotalizadores.getValoricms()) : new Money());
			
			listaAnalitico.add(reg);
		}
		
		
		return this.makeRegistroC490(filtro, listaAnalitico);
	}
	
	private List<RegistroC490> makeRegistroC490(SpedarquivoFiltro filtro, List<RegistroAnaliticoSped> listaAnalitico) {
		List<RegistroC490> lista = new ArrayList<RegistroC490>();
		RegistroC490 registroC490;
		
		for (RegistroAnaliticoSped analitico : listaAnalitico) {
			if(analitico.getValoroperacao() != null && 
					analitico.getValoroperacao().getValue().doubleValue() > 0){
				registroC490 = new RegistroC490();
				
				registroC490.setReg(RegistroC490.REG);
				registroC490.setCst_icms(analitico.getCst() != null ? SPEDUtil.stringCheia(analitico.getCst(), "0", 3, false) : null);
				registroC490.setCfop(analitico.getCfop() != null ? analitico.getCfop().getCodigo() : null);
				registroC490.setAliq_icms(analitico.getAliq_icms());
				registroC490.setVl_opr(analitico.getValoroperacao().getValue().doubleValue());
				registroC490.setVl_bc_icms(analitico.getValorbcicms().getValue().doubleValue());
				registroC490.setVl_icms(analitico.getValoricms().getValue().doubleValue());
	//			registroC490.setCod_obs(codObs);
				
				filtro.getApoio().addRegistros(RegistroC490.REG);
				
				lista.add(registroC490);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC420> createRegistroC420(SpedarquivoFiltro filtro, Emporiumreducaoz emporiumreducaoz, Date datamovimento, List<Emporiumvendaitem> listaEmporiumvendaitem, List<Emporiumtotalizadores> listaEmporiumtotalizadores) {
		List<RegistroC420> lista = new ArrayList<RegistroC420>();
		RegistroC420 registroC420;
		
		List<Emporiumtotalizadores> listaEmporiumtotalizadoresData = emporiumtotalizadoresService.getEmporiumtotalizadoresByDataMovimento(datamovimento, listaEmporiumtotalizadores);
		List<Emporiumvendaitem> listaEmporiumvendaitemData = emporiumvendaitemService.getEmporiumvendaitemByDataMovimento(datamovimento, listaEmporiumvendaitem, filtro.getEmpresa());
		
		if(emporiumreducaoz.getValorcancelados() != null && emporiumreducaoz.getValorcancelados() > 0){
			registroC420 = new RegistroC420();
			
			registroC420.setReg(RegistroC420.REG);
			registroC420.setCod_tot_par("Can-T");
			registroC420.setVlr_acum_tot(emporiumreducaoz.getValorcancelados());
			
			filtro.getApoio().addRegistros(RegistroC420.REG);
			
			lista.add(registroC420);
		}
		
		if(emporiumreducaoz.getValoracrescimo() != null && emporiumreducaoz.getValoracrescimo() > 0){
			registroC420 = new RegistroC420();
			
			registroC420.setReg(RegistroC420.REG);
			registroC420.setCod_tot_par("AT");
			registroC420.setVlr_acum_tot(emporiumreducaoz.getValoracrescimo());
			
			filtro.getApoio().addRegistros(RegistroC420.REG);
			
			lista.add(registroC420);
		}
		
		if(emporiumreducaoz.getValordesconto() != null && emporiumreducaoz.getValordesconto() > 0){
			registroC420 = new RegistroC420();
			
			registroC420.setReg(RegistroC420.REG);
			registroC420.setCod_tot_par("DT");
			registroC420.setVlr_acum_tot(emporiumreducaoz.getValordesconto());
			
			filtro.getApoio().addRegistros(RegistroC420.REG);
			
			lista.add(registroC420);
		}
		
		for(Emporiumtotalizadores emporiumtotalizadores: listaEmporiumtotalizadoresData){
			Tributacaoecf tributacaoecf = emporiumtotalizadores.getTributacaoecf();
			if(tributacaoecf == null) continue;
			
			if(emporiumtotalizadores.getValorbcicms() > 0){
				registroC420 = new RegistroC420();
				
				registroC420.setReg(RegistroC420.REG);
				registroC420.setCod_tot_par(tributacaoecf.getCodigototalizador());
				registroC420.setVlr_acum_tot(emporiumtotalizadores.getValorbcicms());
				registroC420.setNr_tot(tributacaoecf.getNumerototalizador());
				
				registroC420.setListaRegistroC425(this.createRegistroC425(filtro, tributacaoecf, listaEmporiumvendaitemData));
				
				filtro.getApoio().addRegistros(RegistroC420.REG);
				
				lista.add(registroC420);
			}
		}
		
		return this.joinListaRegistroC420(lista, filtro);
	}
	
	private List<RegistroC420> joinListaRegistroC420(List<RegistroC420> listaDesagrupada, SpedarquivoFiltro filtro) {
		List<RegistroC420> listaAgrupada = new ArrayList<RegistroC420>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroC420 registroC420 : listaDesagrupada) {
				if(listaAgrupada.contains(registroC420)){
					int idx = listaAgrupada.indexOf(registroC420);
					RegistroC420 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroC420(aux, registroC420, filtro);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroC420.REG);
				} else {
					listaAgrupada.add(registroC420);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	private RegistroC420 joinRegistroC420(RegistroC420 ori, RegistroC420 copia, SpedarquivoFiltro filtro) {
		
		ori.setVlr_acum_tot((ori.getVlr_acum_tot() != null ? ori.getVlr_acum_tot() : 0d) + (copia.getVlr_acum_tot() != null ? copia.getVlr_acum_tot() : 0d));
		
		// REGISTRO C425
		List<RegistroC425> listaRegistroC425Ori = ori.getListaRegistroC425();
		List<RegistroC425> listaRegistroC425Copia = copia.getListaRegistroC425();
		
		if(listaRegistroC425Ori != null && listaRegistroC425Ori.size() > 0 &&
				listaRegistroC425Copia != null && listaRegistroC425Copia.size() > 0){
			ori.setListaRegistroC425(this.joinListaRegistroC425(listaRegistroC425Ori, listaRegistroC425Copia, filtro));
		} else if(listaRegistroC425Ori != null && listaRegistroC425Ori.size() > 0){
			ori.setListaRegistroC425(listaRegistroC425Ori);
		} else if(listaRegistroC425Copia != null && listaRegistroC425Copia.size() > 0){
			ori.setListaRegistroC425(listaRegistroC425Copia);
		}
		
		return ori;
	}
	
	private List<RegistroC425> joinListaRegistroC425(List<RegistroC425> ori, List<RegistroC425> copia, SpedarquivoFiltro filtro) {
		
		for (RegistroC425 registroC425 : copia) {
			if(ori.contains(registroC425)){
				int idx = ori.indexOf(registroC425);
				RegistroC425 aux = ori.get(idx);
				
				aux.setQtd((aux.getQtd() != null ? aux.getQtd() : 0d) + (registroC425.getQtd() != null ? registroC425.getQtd() : 0d));
				aux.setVl_item((aux.getVl_item() != null ? aux.getVl_item() : 0d) + (registroC425.getVl_item() != null ? registroC425.getVl_item() : 0d));
				aux.setVl_cofins((aux.getVl_cofins() != null ? aux.getVl_cofins() : 0d) + (registroC425.getVl_cofins() != null ? registroC425.getVl_cofins() : 0d));
				aux.setVl_pis((aux.getVl_pis() != null ? aux.getVl_pis() : 0d) + (registroC425.getVl_pis() != null ? registroC425.getVl_pis() : 0d));
				
				ori.set(idx, aux);
				
				filtro.getApoio().removeRegistro(RegistroC425.REG);
			} else {
				ori.add(registroC425);
			}
		}		
		
		return ori;
	}
	
	private List<RegistroC425> createRegistroC425(SpedarquivoFiltro filtro, Tributacaoecf tributacaoecf, List<Emporiumvendaitem> listaEmporiumvendaitemData) {
		final int POS_QTDE = 0;
		final int POS_VALOR = 1;
		
		List<RegistroC425> lista = new ArrayList<RegistroC425>();
		RegistroC425 registroC425;
		
		Map<Material, Double[]> mapMaterial = new HashMap<Material, Double[]>();
		for(Emporiumvendaitem it: listaEmporiumvendaitemData){
			if(it.getMaterial() == null || it.getLegenda() == null){
				continue;
			}
			
			if(it.getLegenda().equals(tributacaoecf.getCodigoecf())){
				if(mapMaterial.containsKey(it.getMaterial())){
					Double[] valores = mapMaterial.get(it.getMaterial());
					
					Double qtde = valores[POS_QTDE] + it.getQuantidade();
					Double valor = valores[POS_VALOR] + it.getValorComDesconto();
					
					mapMaterial.put(it.getMaterial(), new Double[]{qtde, valor});
				} else {
					mapMaterial.put(it.getMaterial(), new Double[]{it.getQuantidade(), it.getValorComDesconto()});
				}
			}
		}
		
		Set<Entry<Material, Double[]>> entrySetMaterial = mapMaterial.entrySet();
		
		List<Entry<Material, Double[]>> listaEntryMaterial = new ArrayList<Entry<Material,Double[]>>(entrySetMaterial);
		Collections.sort(listaEntryMaterial,new Comparator<Entry<Material, Double[]>>(){
			public int compare(Entry<Material, Double[]> o1, Entry<Material, Double[]> o2) {
				return o1.getKey().getNome().compareTo(o2.getKey().getNome());
			}
		});
		
		for (Entry<Material, Double[]> entry : listaEntryMaterial) {
			Material material = entry.getKey();
			Double[] valores = entry.getValue();
			
			Double qtde = valores[POS_QTDE];
			Double valor = valores[POS_VALOR];
			Unidademedida unidademedida = material.getUnidademedida();
			
			
			if(valor > 0){
				registroC425 = new RegistroC425();
				
				registroC425.setReg(RegistroC425.REG);
				registroC425.setCod_item(filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial()));
				registroC425.setQtd(qtde);
				registroC425.setUnid(unidademedida != null ? filtro.getApoio().addUnidade(unidademedida.getCdunidademedida()) : null);
				registroC425.setVl_item(valor);
				if(filtro.getGerarregistroc410() != null && filtro.getGerarregistroc410()){
					if(tributacaoecf.getAliqpis() != null && tributacaoecf.getAliqpis() > 0){
						registroC425.setVl_pis((tributacaoecf.getAliqpis()/100d) * valor);
					}
					if(tributacaoecf.getAliqcofins() != null && tributacaoecf.getAliqcofins() > 0){
						registroC425.setVl_cofins((tributacaoecf.getAliqcofins()/100d) * valor);
					}
				}
				
				filtro.getApoio().addRegistros(RegistroC425.REG);
				
				lista.add(registroC425);
			}
		}
		
		return lista;
	}
	
	private RegistroC410 createRegistroC410(SpedarquivoFiltro filtro, Date datamovimento, List<Emporiumvendaitem> listaEmporiumvendaitem) {
		List<Emporiumvendaitem> listaEmporiumvendaitemData = emporiumvendaitemService.getEmporiumvendaitemByDataMovimento(datamovimento, listaEmporiumvendaitem, filtro.getEmpresa());
		
		Double vlPis = 0d;
		Double vlCofins = 0d;
		
		for (Emporiumvendaitem it : listaEmporiumvendaitemData) {
			if(it.getMaterial() != null &&
					it.getMaterial().getGrupotributacao() != null){
				Tributacaoecf tributacaoecf = it.getMaterial().getTributacaoecf();
				
				if(tributacaoecf.getAliqpis() != null && tributacaoecf.getAliqpis() > 0){
					vlPis += (tributacaoecf.getAliqpis()/100d) * it.getValorComDesconto();
				}
				if(tributacaoecf.getAliqcofins() != null && tributacaoecf.getAliqcofins() > 0){
					vlCofins += (tributacaoecf.getAliqcofins()/100d) * it.getValorComDesconto();
				}
			}
		}
		
		RegistroC410 registroC410 = new RegistroC410();
		
		registroC410.setReg(RegistroC410.REG);
		registroC410.setVl_cofins(vlCofins);
		registroC410.setVl_pis(vlPis);
		
		filtro.getApoio().addRegistros(RegistroC410.REG);
		
		return registroC410;
	}
	
	/**
	 * Cria o Registro C500 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#createRegistroC590
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#getListaDocumentoC500
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC500> createRegistroC500(SpedarquivoFiltro filtro) {
		List<RegistroC500> lista = new ArrayList<RegistroC500>();
		RegistroC500 registroC500;
		Entregamaterial entregamaterial;
		
		List<Entregadocumento> listaDocumento = this.getListaDocumentoC500(filtro);
		for (Entregadocumento ed : listaDocumento) {
			
			registroC500 = new RegistroC500();
			
			entregamaterial = ed.getListaEntregamaterial().iterator().next();
			
			registroC500.setReg(RegistroC500.REG);
			registroC500.setInd_oper(0);
			registroC500.setInd_emit(1);
			registroC500.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa()) : null);
			registroC500.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroC500.setCod_sit(ed.getSituacaosped());
//			registroC500.setSer()
//			registroC500.setSub(sub)
//			registroC500.setCod_cons();
			registroC500.setNum_doc(ed.getNumero());
			registroC500.setDt_doc(ed.getDtemissao());
			registroC500.setDt_e_s(ed.getDtentrada());
			registroC500.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
//			registroC500.setVl_desc(vl_desc)
			registroC500.setVl_forn(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
//			registroC500.setVl_serv_nt(vl_serv_nt)
//			registroC500.setVl_terc(vl_terc)
//			registroC500.setVl_da(vl_da)
			registroC500.setVl_bc_icms(entregamaterial.getValorbcicms() != null ? entregamaterial.getValorbcicms().getValue().doubleValue() : null);
			registroC500.setVl_icms(entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().getValue().doubleValue() : null);
			registroC500.setVl_bc_icms_st(entregamaterial.getValorbcicmsst() != null ? entregamaterial.getValorbcicmsst().getValue().doubleValue() : null);
			registroC500.setVl_icms_st(entregamaterial.getValoricmsst() != null ? entregamaterial.getValoricmsst().getValue().doubleValue() : null);
//			registroC500.setCod_inf(dt.get)
			registroC500.setVl_pis(entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().getValue().doubleValue() : null);
			registroC500.setVl_cofins(entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().getValue().doubleValue() : null);
			if("66".equals(registroC500.getCod_mod()) && ed.getChaveacesso() != null){
				registroC500.setChv_doce(ed.getChaveacesso());
			}
			if("66".equals(registroC500.getCod_mod())){
				registroC500.setFin_doce(1);				
			}
			
//			registroC500.setTp_ligacao();
//			registroC500.setCod_grupo_tensao();
			
			registroC500.setListaRegistrosC590(this.createRegistroC590(filtro, entregamaterial, ed));
			
			lista.add(registroC500);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroC500.REG);
		}
		
		return lista;
	}

	
	/**
	 * Cria o Registro C590 do arquivo do SPED.
	 *
	 * @param filtro
	 * @param em
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC590> createRegistroC590(SpedarquivoFiltro filtro, Entregamaterial em, Entregadocumento ed) {
		List<RegistroC590> lista = new ArrayList<RegistroC590>();
		RegistroC590 registroC590 = new RegistroC590();
		
		registroC590.setReg(RegistroC590.REG);
		registroC590.setCst_icms(em.getCsticms() != null ? SPEDUtil.stringCheia(em.getCsticms().getCdnfe(), "0", 3, false) : null);
		registroC590.setCfop(em.getCfop() != null ? em.getCfop().getCodigo() : null);
		registroC590.setAliq_icms(em.getIcms() != null ? em.getIcms() : null);
		registroC590.setVl_opr(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : 0d);
		registroC590.setVl_bc_icms(em.getValorbcicms() != null ? em.getValorbcicms().getValue().doubleValue() : 0d);
		registroC590.setVl_icms(em.getValoricms() != null ? em.getValoricms().getValue().doubleValue() : 0d);
		registroC590.setVl_bc_icms_st(em.getValorbcicmsst() != null ? em.getValorbcicmsst().getValue().doubleValue() : 0d);
		registroC590.setVl_icms_st(em.getValoricmsst() != null ? em.getValoricmsst().getValue().doubleValue() : 0d);
		registroC590.setVl_red_bc(0d);
//		registroC590.setCod_obs(cod_obs)
		
		filtro.getApoio().addRegistros(RegistroC590.REG);
		
		lista.add(registroC590);
		
		return lista;
	}
	
	/**
	 * Cria o Registro C100 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#getListaNotaC100
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#getListaEntregaC100
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC110
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC140
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC160
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC170
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistroC190
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC100> createRegistroC100(SpedarquivoFiltro filtro) {
		List<RegistroC100> lista = new ArrayList<RegistroC100>();
		RegistroC100 registroC100;
		
		List<Notafiscalproduto> listaNotafiscalproduto = this.getListaNotaC100(filtro);
		List<Entregadocumento> listaEntregadocumento = this.getListaEntregaC100(filtro);
		List<Inutilizacaonfe> listaInutilizacaonfe = inutilizacaonfeService.findForSpedRegC100(filtro);
		
		for (Inutilizacaonfe inut : listaInutilizacaonfe) {
			Integer numero = inut.getNuminicio();
			Integer numfim = inut.getNumfim();
			
			while(numero <= numfim){
				registroC100 = new RegistroC100();
				registroC100.setReg(RegistroC100.REG);
				registroC100.setInd_oper(1);
				registroC100.setInd_emit(RegistroC100.EMISSAO_PROPRIA);
				registroC100.setCod_mod("55");
				registroC100.setCod_sit("05");
				registroC100.setSer(inut.getSerienfe() != null ? inut.getSerienfe().toString() : "");
				registroC100.setNum_doc(numero+"");
				
				lista.add(registroC100);
				filtro.getApoio().addRegistros(RegistroC100.REG);
				
				numero++;
			}
		}
		
		
		int ano = SinedDateUtils.getDateProperty(filtro.getMesano(), Calendar.YEAR);
		
		Arquivonfnota arquivonfnota;
		String ufsigla = null;
		for (Notafiscalproduto notafiscalproduto : listaNotafiscalproduto) {
			boolean isNfce = ModeloDocumentoFiscalEnum.NFCE.equals(notafiscalproduto.getModeloDocumentoFiscalEnum());
					
			ufsigla = null;
			if(notafiscalproduto.getEnderecoCliente() != null && notafiscalproduto.getEnderecoCliente().getMunicipio() != null && 
					notafiscalproduto.getEnderecoCliente().getMunicipio().getUf() != null &&
					notafiscalproduto.getEnderecoCliente().getMunicipio().getUf().getSigla() != null){
				ufsigla = notafiscalproduto.getEnderecoCliente().getMunicipio().getUf().getSigla();
			}
			
			registroC100 = new RegistroC100();
			
			registroC100.setReg(RegistroC100.REG);
			registroC100.setInd_oper(notafiscalproduto.getTipooperacaonota() != null ? notafiscalproduto.getTipooperacaonota().getValue() : null);
			registroC100.setCod_sit(notafiscalproduto.getSituacaosped());
			registroC100.setInd_emit(RegistroC100.EMISSAO_PROPRIA);
			registroC100.setNum_doc(notafiscalproduto.getNumero());
			
			arquivonfnota = null;
			if(NotaStatus.CANCELADA.equals(notafiscalproduto.getNotaStatus()) || 
					NotaStatus.NFE_DENEGADA.equals(notafiscalproduto.getNotaStatus())){
				List<Arquivonfnota> listaArquivonfnota = arquivonfnotaService.findByNotaAndCancelada(notafiscalproduto.getCdNota().toString());
				if(listaArquivonfnota != null && listaArquivonfnota.size() > 0){
					arquivonfnota = listaArquivonfnota.get(0);
				}
			}else {
				arquivonfnota = arquivonfnotaService.findByNotaProduto(notafiscalproduto);
			}
			if(arquivonfnota != null){
				registroC100.setChv_nfe(arquivonfnota.getChaveacesso());
				if(arquivonfnota.getArquivonf() != null){
					if(arquivonfnota.getArquivonf().getConfiguracaonfe() != null){
						registroC100.setCod_mod(arquivonfnota.getArquivonf().getConfiguracaonfe().getModelonfe());
					}
				}
				if(arquivonfnota.getNota() != null){
					registroC100.setSer(arquivonfnota.getNota().getSerienfe() != null ? arquivonfnota.getNota().getSerienfe().toString() : null);
				}
				
			}
			if(Boolean.TRUE.equals(notafiscalproduto.getOrigemCupom()) && isNfce){
				registroC100.setCod_mod(ModeloDocumentoFiscalEnum.NFCE.getValue());
			}
			if(StringUtils.isBlank(registroC100.getSer()) && isNfce){
				registroC100.setSer("000");
			}
			
			if(notafiscalproduto.getNotaStatus() == null || (!NotaStatus.CANCELADA.equals(notafiscalproduto.getNotaStatus()) && 
					!NotaStatus.NFE_DENEGADA.equals(notafiscalproduto.getNotaStatus()))){
			
				Integer cdpessoa = notafiscalproduto.getCliente().getCdpessoa();
				
				String codPart = "";
				
				if(!isNfce){
					if(notafiscalproduto.getEnderecoCliente() != null && 
							notafiscalproduto.getEnderecoCliente().getInscricaoestadual() != null &&
							!"".equals(notafiscalproduto.getEnderecoCliente().getInscricaoestadual())){
						codPart = notafiscalproduto.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa, notafiscalproduto.getEnderecoCliente().getCdendereco()) : null;
						codPart = codPart + "-" + notafiscalproduto.getEnderecoCliente().getCdendereco();
					} else {
						codPart = notafiscalproduto.getCliente() != null ? filtro.getApoio().addParticipante(cdpessoa) : null;
					}
					registroC100.setCod_part(codPart);					
				}
				
				
				registroC100.setDt_doc(notafiscalproduto.getDtEmissao());
				registroC100.setDt_e_s(SinedDateUtils.timestampToDate(notafiscalproduto.getDatahorasaidaentrada()));
				registroC100.setVl_doc(notafiscalproduto.getValor() != null ? notafiscalproduto.getValor().getValue().doubleValue() : null);
				
				
				if(notafiscalproduto.getFormapagamentonfe() != null){
					if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.A_VISTA)){
						registroC100.setInd_pgto(0);
					} else if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.A_PRAZO)){
						registroC100.setInd_pgto(1);
					} else if(notafiscalproduto.getFormapagamentonfe().equals(Formapagamentonfe.OUTROS)){
						registroC100.setInd_pgto(2);
					}
				}
				
				if(notafiscalproduto.getResponsavelfrete() != null){
					if(ano >= 2012){
						registroC100.setInd_frt(notafiscalproduto.getResponsavelfrete().getCdnfe());
					} else {
						int cdnfe = notafiscalproduto.getResponsavelfrete().getCdnfe();
						switch (cdnfe) {
							case 0:
								registroC100.setInd_frt(1);
								break;
							case 1:
								registroC100.setInd_frt(2);
								break;
							case 2:
								registroC100.setInd_frt(0);
								break;
							case 9:
								registroC100.setInd_frt(9);
								break;
						}
					}
				}
				
				registroC100.setVl_desc(notafiscalproduto.getValordesconto() != null ? notafiscalproduto.getValordesconto().getValue().doubleValue() : null);
				registroC100.setVl_merc(notafiscalproduto.getValorprodutos() != null ? notafiscalproduto.getValorprodutos().getValue().doubleValue() : null);
				registroC100.setVl_frt(notafiscalproduto.getValorfrete() != null ? notafiscalproduto.getValorfrete().getValue().doubleValue() : null);
				registroC100.setVl_seg(notafiscalproduto.getValorseguro() != null ? notafiscalproduto.getValorseguro().getValue().doubleValue() : null);
				registroC100.setVl_out_da(notafiscalproduto.getOutrasdespesas() != null ? notafiscalproduto.getOutrasdespesas().getValue().doubleValue() : null);
				
				registroC100.setVl_bc_icms(notafiscalproduto.getValorbcicms() != null && notafiscalproduto.getValorbcicms().getValue().doubleValue() > 0 ? notafiscalproduto.getValorbcicms().getValue().doubleValue() : null);
				if(!"65".equals(registroC100.getCod_mod())){
					registroC100.setVl_bc_icms_st(notafiscalproduto.getValorbcicmsst() != null && notafiscalproduto.getValorbcicmsst().getValue().doubleValue() > 0 ? notafiscalproduto.getValorbcicmsst().getValue().doubleValue() : null);
					registroC100.setVl_ipi(notafiscalproduto.getValoripi() != null && notafiscalproduto.getValoripi().getValue().doubleValue() > 0 ? notafiscalproduto.getValoripi().getValue().doubleValue() : null);
					registroC100.setVl_pis(notafiscalproduto.getValorpis() != null && notafiscalproduto.getValorpis().getValue().doubleValue() > 0 ? notafiscalproduto.getValorpis().getValue().doubleValue() : null);
					registroC100.setVl_cofins(notafiscalproduto.getValorcofins() != null && notafiscalproduto.getValorcofins().getValue().doubleValue() > 0 ? notafiscalproduto.getValorcofins().getValue().doubleValue() : null);
				}
				
				registroC100.setVl_icms(notafiscalproduto.getValoricms() != null && notafiscalproduto.getValoricms().getValue().doubleValue() > 0 ? notafiscalproduto.getValoricms().getValue().doubleValue() : null);
				registroC100.setVl_icms_st(notafiscalproduto.getValoricmsst() != null && notafiscalproduto.getValoricmsst().getValue().doubleValue() > 0 ? notafiscalproduto.getValoricmsst().getValue().doubleValue() : null);
				
				if(!isNfce){
					registroC100.setRegistroC101(this.createRegistroC101(filtro, notafiscalproduto, ufsigla));
					registroC100.setListaRegistroC110(this.createRegistroC110(filtro, notafiscalproduto));
				}
				registroC100.setListaRegistroC190(this.createRegistroC190(filtro, notafiscalproduto));
				registroC100.setListaRegistroC195(this.createRegistroC195(filtro, notafiscalproduto.getListaAjustefiscal()));
			}
			
			lista.add(registroC100);
			
			filtro.getApoio().addNota(notafiscalproduto.getCdNota());			
			filtro.getApoio().addRegistros(RegistroC100.REG);
		}
		
		List<RegistroC100> listaDesagrupada = new ArrayList<RegistroC100>();
		Endereco endereco;
		for (Entregadocumento ed : listaEntregadocumento) {
			ufsigla = null;
			if(ed.getFornecedor() != null && ed.getFornecedor().getListaEndereco() != null && 
					ed.getFornecedor().getListaEndereco().size() > 0){
				endereco = ed.getFornecedor().getListaEndereco().iterator().next();
				if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null &&
						endereco.getMunicipio().getUf().getSigla() != null){
					ufsigla = endereco.getMunicipio().getUf().getSigla();
				}
			}
			
			registroC100 = new RegistroC100();
			
			registroC100.setReg(RegistroC100.REG);
			registroC100.setInd_oper(Tipooperacaonota.ENTRADA.getValue());
			registroC100.setInd_emit(RegistroC100.TERCEIROS);
			registroC100.setCod_mod(ed.getModelodocumentofiscal() != null && ed.getModelodocumentofiscal().getCodigo() != null ? ed.getModelodocumentofiscal().getCodigo() : null);
			registroC100.setCod_sit(ed.getSituacaosped());
			registroC100.setNum_doc(SPEDUtil.soNumero(ed.getNumero()));
			registroC100.setChv_nfe(ed.getChaveacesso());
			registroC100.setDt_e_s(ed.getDtentrada());
			registroC100.setVl_doc(ed.getValortotaldocumento() != null ? ed.getValortotaldocumento().getValue().doubleValue() : null);
			
			if(ed.getIndpag() != null){
				if(ed.getIndpag().equals(Formapagamentonfe.A_VISTA)){
					registroC100.setInd_pgto(0);
				} else if(ed.getIndpag().equals(Formapagamentonfe.A_PRAZO)){
					registroC100.setInd_pgto(1);
				} else if(ed.getIndpag().equals(Formapagamentonfe.OUTROS)){
					registroC100.setInd_pgto(2);
				}
			}
			
			if(ed.getResponsavelfrete() != null){
				if(ano >= 2012){
					registroC100.setInd_frt(ed.getResponsavelfrete().getCdnfe());
				} else {
					int cdnfe = ed.getResponsavelfrete().getCdnfe();
					switch (cdnfe) {
						case 0:
							registroC100.setInd_frt(1);
							break;
						case 1:
							registroC100.setInd_frt(2);
							break;
						case 2:
							registroC100.setInd_frt(0);
							break;
						case 9:
							registroC100.setInd_frt(9);
							break;
					}
				}
			}
			
			registroC100.setSer(ed.getSerie() != null ? ed.getSerie().toString() : null);
			registroC100.setCod_part(ed.getFornecedor() != null ? filtro.getApoio().addParticipante(ed.getFornecedor().getCdpessoa()) : null);
			registroC100.setDt_doc(ed.getDtemissao());
			registroC100.setVl_desc(ed.getValordesconto() != null ? ed.getValordesconto().getValue().doubleValue() : null);
			registroC100.setVl_merc(ed.getValormercadoriaSPEDPiscofins() != null ? ed.getValormercadoriaSPEDPiscofins().getValue().doubleValue() : null);
			registroC100.setVl_frt(ed.getValorfrete() != null ? ed.getValorfrete().getValue().doubleValue() : null);
			registroC100.setVl_seg(ed.getValorseguro() != null ? ed.getValorseguro().getValue().doubleValue() : null);
			registroC100.setVl_out_da(ed.getValoroutrasdespesas() != null ? ed.getValoroutrasdespesas().getValue().doubleValue() : null);

			Set<Entregamaterial> listaEntregamaterial = ed.getListaEntregamaterial();
			Money valorbcicms = new Money();
			Money valoricms = new Money();
			Money valorbcicmsst = new Money();
			Money valoricmsst = new Money();
			Money valoripi = new Money();
			
			for (Entregamaterial item : listaEntregamaterial) {
				if(item.getCsticms() != null &&
						!item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO) &&
						!item.getCsticms().equals(Tipocobrancaicms.ISENTA) &&
						!item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA) &&
						!item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO) &&
						!item.getCsticms().equals(Tipocobrancaicms.SUSPENSAO) &&
						!item.getCsticms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO)){
					valorbcicms = valorbcicms.add(item.getValorbcicms());
					valoricms = valoricms.add(item.getValoricms());
					valorbcicmsst = valorbcicmsst.add(item.getValorbcicmsst());
					valoricmsst = valoricmsst.add(item.getValoricmsst());
//					if(item.getCstipi() != null && item.getCstipi().equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO))
				}
				if(item.getValoripi() != null) valoripi = valoripi.add(item.getValoripi());
			}
			
			registroC100.setVl_bc_icms(valorbcicms.getValue().doubleValue());
			registroC100.setVl_icms(valoricms.getValue().doubleValue());
			registroC100.setVl_bc_icms_st(valorbcicmsst.getValue().doubleValue());
			registroC100.setVl_icms_st(valoricmsst.getValue().doubleValue());
			registroC100.setVl_ipi(valoripi.getValue().doubleValue());
			
			registroC100.setVl_pis(ed.getValortotalpis() != null && ed.getValortotalpis().getValue().doubleValue() > 0 ? ed.getValortotalpis().getValue().doubleValue() : null);
			registroC100.setVl_cofins(ed.getValortotalcofins() != null && ed.getValortotalcofins().getValue().doubleValue() > 0 ? ed.getValortotalcofins().getValue().doubleValue() : null);
			
			registroC100.setListaRegistroC110(this.createRegistroC110(filtro, ed));
			if(ed.getSimplesremessa() == null || !ed.getSimplesremessa())	registroC100.setRegistroC140(this.createRegistroC140(filtro, ed)); 
			registroC100.setListaRegistroC170(this.createRegistroC170(filtro, ed));
			registroC100.setListaRegistroC190(this.createRegistroC190(filtro, ed));
			registroC100.setListaRegistroC195(this.createRegistroC195(filtro, ed.getListaAjustefiscal()));
			
			listaDesagrupada.add(registroC100);
			
			filtro.getApoio().addEntrega(ed.getCdentregadocumento());
			filtro.getApoio().addRegistros(RegistroC100.REG);
		}
		
		List<RegistroC100> listaAgrupada = this.joinListaRegistroC100(listaDesagrupada, filtro);
		lista.addAll(listaAgrupada);
		
		filtro.setListaRegistroE200(this.createRegistroE200(filtro, listaEntregadocumento, listaNotafiscalproduto));
		
		return lista;
	}
	
	/**
	 * Cria o registro C101
	 *
	 * @param filtro
	 * @param notafiscalproduto
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/06/2016
	 */
	private RegistroC101 createRegistroC101(SpedarquivoFiltro filtro, Notafiscalproduto notafiscalproduto, String ufsigla) {
		RegistroC101 registroC101 = null;
		
		Money valorfcpdestinatario = notafiscalproduto.getValorfcpdestinatario();
		Money valoricmsdestinatario = notafiscalproduto.getValoricmsdestinatario();
		Money valoricmsremetente = notafiscalproduto.getValoricmsremetente();
		
		if((valorfcpdestinatario != null && valorfcpdestinatario.getValue().doubleValue() > 0) ||
				(valoricmsdestinatario != null && valoricmsdestinatario.getValue().doubleValue() > 0) ||
				(valoricmsremetente != null && valoricmsremetente.getValue().doubleValue() > 0)){
			registroC101 = new RegistroC101();
			registroC101.setReg(RegistroC101.REG);
			registroC101.setVl_fcp_uf_dest(valorfcpdestinatario.getValue().doubleValue());
			registroC101.setVl_icms_uf_dest(valoricmsdestinatario.getValue().doubleValue());
			registroC101.setVl_icms_uf_rem(valoricmsremetente.getValue().doubleValue());
			registroC101.setUfsigla(ufsigla);
		
			filtro.getApoio().addRegistros(RegistroC101.REG);
		}
		
		return registroC101;
	}
	
	/**
	 * Agrupa os registros C100.
	 *
	 * @param listaDesagrupada
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	private List<RegistroC100> joinListaRegistroC100(List<RegistroC100> listaDesagrupada, SpedarquivoFiltro filtro) {
		List<RegistroC100> listaAgrupada = new ArrayList<RegistroC100>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroC100 registroC100 : listaDesagrupada) {
				if(listaAgrupada.contains(registroC100)){
					int idx = listaAgrupada.indexOf(registroC100);
					RegistroC100 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroC100(aux, registroC100, filtro);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroC100.REG);
				} else {
					listaAgrupada.add(registroC100);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	/**
	 * Junta dois registros C100 passados por par�metros.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#joinRegistroC140(RegistroC140 ori, RegistroC140 copia)
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#joinListaRegistroC190(List<RegistroC190> ori, List<RegistroC190> copia)
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 */
	private RegistroC100 joinRegistroC100(RegistroC100 ori, RegistroC100 copia, SpedarquivoFiltro filtro) {
		
		ori.setVl_doc((ori.getVl_doc() != null ? ori.getVl_doc() : 0d) + (copia.getVl_doc() != null ? copia.getVl_doc() : 0d));
		ori.setVl_desc((ori.getVl_desc() != null ? ori.getVl_desc() : 0d) + (copia.getVl_desc() != null ? copia.getVl_desc() : 0d));
		ori.setVl_abat_nt((ori.getVl_abat_nt() != null ? ori.getVl_abat_nt() : 0d) + (copia.getVl_abat_nt() != null ? copia.getVl_abat_nt() : 0d));
		ori.setVl_merc((ori.getVl_merc() != null ? ori.getVl_merc() : 0d) + (copia.getVl_merc() != null ? copia.getVl_merc() : 0d));
		ori.setVl_frt((ori.getVl_frt() != null ? ori.getVl_frt() : 0d) + (copia.getVl_frt() != null ? copia.getVl_frt() : 0d));
		ori.setVl_seg((ori.getVl_seg() != null ? ori.getVl_seg() : 0d) + (copia.getVl_seg() != null ? copia.getVl_seg() : 0d));
		ori.setVl_out_da((ori.getVl_out_da() != null ? ori.getVl_out_da() : 0d) + (copia.getVl_out_da() != null ? copia.getVl_out_da() : 0d));
		ori.setVl_bc_icms((ori.getVl_bc_icms() != null ? ori.getVl_bc_icms() : 0d) + (copia.getVl_bc_icms() != null ? copia.getVl_bc_icms() : 0d));
		ori.setVl_icms((ori.getVl_icms() != null ? ori.getVl_icms() : 0d) + (copia.getVl_icms() != null ? copia.getVl_icms() : 0d));
		ori.setVl_bc_icms_st((ori.getVl_bc_icms_st() != null ? ori.getVl_bc_icms_st() : 0d) + (copia.getVl_bc_icms_st() != null ? copia.getVl_bc_icms_st() : 0d));
		ori.setVl_icms_st((ori.getVl_icms_st() != null ? ori.getVl_icms_st() : 0d) + (copia.getVl_icms_st() != null ? copia.getVl_icms_st() : 0d));
		ori.setVl_ipi((ori.getVl_ipi() != null ? ori.getVl_ipi() : 0d) + (copia.getVl_ipi() != null ? copia.getVl_ipi() : 0d));
		ori.setVl_pis((ori.getVl_pis() != null ? ori.getVl_pis() : 0d) + (copia.getVl_pis() != null ? copia.getVl_pis() : 0d));
		ori.setVl_cofins((ori.getVl_cofins() != null ? ori.getVl_cofins() : 0d) + (copia.getVl_cofins() != null ? copia.getVl_cofins() : 0d));
		ori.setVl_pis_st((ori.getVl_pis_st() != null ? ori.getVl_pis_st() : 0d) + (copia.getVl_pis_st() != null ? copia.getVl_pis_st() : 0d));
		ori.setVl_cofins_st((ori.getVl_cofins_st() != null ? ori.getVl_cofins_st() : 0d) + (copia.getVl_cofins_st() != null ? copia.getVl_cofins_st() : 0d));
		
		// REGISTRO C110
		List<RegistroC110> listaRegistroC110 = new ArrayList<RegistroC110>();
		if(ori.getListaRegistroC110() != null && ori.getListaRegistroC110().size() > 0){
			listaRegistroC110 = ori.getListaRegistroC110();
			if(copia.getListaRegistroC110() != null && copia.getListaRegistroC110().size() > 0){
				listaRegistroC110.addAll(copia.getListaRegistroC110());
			}
		} else if(copia.getListaRegistroC110() != null && copia.getListaRegistroC110().size() > 0){
			listaRegistroC110 = copia.getListaRegistroC110();
		}
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		for (RegistroC110 registroC110 : listaRegistroC110) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		ori.setListaRegistroC110(listaAgrupada);
		
		// REGISTRO C140
		RegistroC140 oriC140 = ori.getRegistroC140();
		RegistroC140 copiaC140 = copia.getRegistroC140();
		if(oriC140 != null && copiaC140 != null){
			ori.setRegistroC140(this.joinRegistroC140(oriC140, copiaC140, filtro));
		} else if(oriC140 != null){
			ori.setRegistroC140(oriC140);
		} else if(copiaC140 != null){
			ori.setRegistroC140(copiaC140);
		}
		
		// REGISTRO 170
		List<RegistroC170> listaRegistroC170 = null;
		if(ori.getListaRegistroC170() != null){
			listaRegistroC170 = ori.getListaRegistroC170();
			if(copia.getListaRegistroC170() != null){
				listaRegistroC170.addAll(copia.getListaRegistroC170());
			}
		} else if(copia.getListaRegistroC170() != null){
			listaRegistroC170 = copia.getListaRegistroC170();
		}
		if(listaRegistroC170 != null){
			int num_item = 1;
			for (RegistroC170 registroC170 : listaRegistroC170) {
				registroC170.setNum_item(num_item);
				num_item++;
			}
		}
		ori.setListaRegistroC170(listaRegistroC170);
		
		// REGISTRO 190
		List<RegistroC190> listaRegistroC190Ori = ori.getListaRegistroC190();
		List<RegistroC190> listaRegistroC190Copia = copia.getListaRegistroC190();
		
		if(listaRegistroC190Ori != null && listaRegistroC190Ori.size() > 0 &&
				listaRegistroC190Copia != null && listaRegistroC190Copia.size() > 0){
			ori.setListaRegistroC190(this.joinListaRegistroC190(listaRegistroC190Ori, listaRegistroC190Copia, filtro));
		} else if(listaRegistroC190Ori != null && listaRegistroC190Ori.size() > 0){
			ori.setListaRegistroC190(listaRegistroC190Ori);
		} else if(listaRegistroC190Copia != null && listaRegistroC190Copia.size() > 0){
			ori.setListaRegistroC190(listaRegistroC190Copia);
		}
		
		return ori;
	}
	
	/**
	 * Junta a lista de Registro C190.
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	private List<RegistroC190> joinListaRegistroC190(List<RegistroC190> ori, List<RegistroC190> copia, SpedarquivoFiltro filtro) {
		
		for (RegistroC190 registroC190 : copia) {
			if(ori.contains(registroC190)){
				int idx = ori.indexOf(registroC190);
				RegistroC190 aux = ori.get(idx);
				
				aux.setVl_opr((aux.getVl_opr() != null ? aux.getVl_opr() : 0d) + (registroC190.getVl_opr() != null ? registroC190.getVl_opr() : 0d));
				aux.setVl_bc_icms((aux.getVl_bc_icms() != null ? aux.getVl_bc_icms() : 0d) + (registroC190.getVl_bc_icms() != null ? registroC190.getVl_bc_icms() : 0d));
				aux.setVl_icms((aux.getVl_icms() != null ? aux.getVl_icms() : 0d) + (registroC190.getVl_icms() != null ? registroC190.getVl_icms() : 0d));
				aux.setVl_bc_icms_st((aux.getVl_bc_icms_st() != null ? aux.getVl_bc_icms_st() : 0d) + (registroC190.getVl_bc_icms_st() != null ? registroC190.getVl_bc_icms_st() : 0d));
				aux.setVl_icms_st((aux.getVl_icms_st() != null ? aux.getVl_icms_st() : 0d) + (registroC190.getVl_icms_st() != null ? registroC190.getVl_icms_st() : 0d));
				aux.setVl_red_bc((aux.getVl_red_bc() != null ? aux.getVl_red_bc() : 0d) + (registroC190.getVl_red_bc() != null ? registroC190.getVl_red_bc() : 0d));
				aux.setVl_ipi((aux.getVl_ipi() != null ? aux.getVl_ipi() : 0d) + (registroC190.getVl_ipi() != null ? registroC190.getVl_ipi() : 0d));
				
				ori.set(idx, aux);
				
				filtro.getApoio().removeRegistro(RegistroC190.REG);
			} else {
				ori.add(registroC190);
			}
		}		
		
		return ori;
	}
	/**
	 * Junta os registros C140 passados por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SpedarquivoService#joinListaRegistroC141(List<RegistroC141> ori, List<RegistroC141> copia)
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 * @param filtro 
	 */
	private RegistroC140 joinRegistroC140(RegistroC140 ori, RegistroC140 copia, SpedarquivoFiltro filtro) {
		
		ori.setQtd_parc((ori.getQtd_parc() != null ? ori.getQtd_parc() : 0) + (copia.getQtd_parc() != null ? copia.getQtd_parc() : 0));
		ori.setVl_tit((ori.getVl_tit() != null ? ori.getVl_tit() : 0d) + (copia.getVl_tit() != null ? copia.getVl_tit() : 0d));
		
		List<RegistroC141> listaRegistrosC141Ori = ori.getListaRegistrosC141();
		List<RegistroC141> listaRegistrosC141Copia = copia.getListaRegistrosC141();
		
		if(listaRegistrosC141Ori != null && listaRegistrosC141Copia != null){
			ori.setListaRegistrosC141(this.joinListaRegistroC141(listaRegistrosC141Ori, listaRegistrosC141Copia));
		} else if(listaRegistrosC141Ori != null) {
			ori.setListaRegistrosC141(listaRegistrosC141Ori);
		} else if(listaRegistrosC141Copia != null) {
			ori.setListaRegistrosC141(listaRegistrosC141Copia);
		}
		
		filtro.getApoio().removeRegistro(RegistroC140.REG);
		
		return ori;
	}
	
	/**
	 * Junta as listas de Registro C141 passadas por par�metro.
	 *
	 * @param ori
	 * @param copia
	 * @return
	 * @since 29/11/2011
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC141> joinListaRegistroC141(List<RegistroC141> ori, List<RegistroC141> copia) {
		List<RegistroC141> listaRegistroC141 = new ArrayList<RegistroC141>();
		
		listaRegistroC141.addAll(ori);
		listaRegistroC141.addAll(copia);
		
		Collections.sort(listaRegistroC141, new Comparator<RegistroC141>(){
			public int compare(RegistroC141 o1, RegistroC141 o2) {
				return o1.getDt_vcto().compareTo(o2.getDt_vcto());
			}
		});
		
		int num_parc = 1;
		for (RegistroC141 registroC141 : listaRegistroC141) {
			registroC141.setNum_parc(num_parc);
			num_parc++;
		}
		
		return listaRegistroC141;
	}
	
	/**
	 *  Cria o Registro C190 do arquivo do SPED, a partir de uma Entrega.
	 *
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#makeRegistroC190
	 *
	 * @param filtro
	 * @param entregadocumento
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC190> createRegistroC190(SpedarquivoFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroAnaliticoSped> listaAnalitico = new ArrayList<RegistroAnaliticoSped>();
		RegistroAnaliticoSped reg;
		Money valorbruto;
		int idx;
		
		Money zero = new Money();
		
		Set<Entregamaterial> lista = entregadocumento.getListaEntregamaterial();
		for (Entregamaterial item : lista) {
			Money valorbcicms = item.getValorbcicms() != null ? item.getValorbcicms() : new Money();
			double aliqIcms = item.getIcms() != null ? item.getIcms() : 0.0;
			Money valoricms = item.getValoricms() != null ? item.getValoricms() : new Money();
			Money valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst() : new Money();
			Money valoricmsst = item.getValoricmsst() != null ? item.getValoricmsst() : new Money();
			Money valoripi = item.getValoripi() != null ? item.getValoripi() : new Money();
			Money valorredbc = new Money();
			
			if(item.getValortotalmaterial_com_icmsst() != null) {
				valorbruto = new Money(item.getValortotalmaterial_com_icmsst());
				if(item.getValordesconto() != null)
					valorbruto = valorbruto.subtract(item.getValordesconto());
				if(item.getValorfrete() != null)
					valorbruto = valorbruto.add(item.getValorfrete());
				if(item.getValorseguro() != null)
					valorbruto = valorbruto.add(item.getValorseguro());
				if(item.getValoroutrasdespesas() != null)
					valorbruto = valorbruto.add(item.getValoroutrasdespesas());
			} else {
				valorbruto = new Money();
			}
			
			if(item.getCsticms() != null){
				if(item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO) ||
					item.getCsticms().equals(Tipocobrancaicms.ISENTA) ||
					item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA) ||
					item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO) ||
					item.getCsticms().equals(Tipocobrancaicms.SUSPENSAO) ||
					item.getCsticms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO)){
					valorbcicms = zero;
					aliqIcms = 0d;
					valoricms = zero;
					valorbcicmsst = zero;
					valoricmsst = zero;
				}
				
				if(item.getCsticms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC) ||
					item.getCsticms().equals(Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO)){
					valorredbc = valorbruto.subtract(valorbcicms);
				}
			}
			
			reg = new RegistroAnaliticoSped(item.getCstIcmsSPED(), item.getCfop(), aliqIcms);
			
			if(listaAnalitico.contains(reg)){
				idx = listaAnalitico.indexOf(reg);
				reg = listaAnalitico.get(idx);
				listaAnalitico.remove(idx);
			}
			
			reg.addValoroperacao(valorbruto);
			reg.addValorbcicms(valorbcicms);
			reg.addValoricms(valoricms);
			reg.addValorbcicmsst(valorbcicmsst);
			reg.addValoricmsst(valoricmsst);
			if(valoricmsst != null && valoricmsst.getValue().doubleValue() > 0){
				item.setGerarE200(Boolean.TRUE);
			}
//			if(item.getCstipi() != null && item.getCstipi().equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO))
			reg.addValoripi(valoripi);
			reg.addValorredbc(valorredbc);
			
			listaAnalitico.add(reg);
		}
		
		return this.makeRegistroC190(filtro, listaAnalitico);
	}
	
	/**
	 * Cria o Registro C190 do arquivo do SPED, a partir de uma Notafiscalproduto.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#makeRegistroC190
	 *
	 * @param filtro
	 * @param notafiscalproduto
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC190> createRegistroC190(SpedarquivoFiltro filtro, Notafiscalproduto notafiscalproduto) {
		List<RegistroAnaliticoSped> listaAnalitico = new ArrayList<RegistroAnaliticoSped>();
		RegistroAnaliticoSped reg;
		int idx;
		
		Money zero = new Money();
		
		List<Notafiscalprodutoitem> listaItens = notafiscalproduto.getListaItens();
		for (Notafiscalprodutoitem item : listaItens) {
			Money valorbcicmsst = item.getValorbcicmsst() != null ? item.getValorbcicmsst() : zero;
			Money valoricmsst = item.getValoricmsst() != null ? item.getValoricmsst() : zero;
			
			reg = new RegistroAnaliticoSped(item.getCstIcmsSPED(), item.getCfop(), item.getIcms());
			
			if(listaAnalitico.contains(reg)){
				idx = listaAnalitico.indexOf(reg);
				reg = listaAnalitico.get(idx);
				listaAnalitico.remove(idx);
			}
			
			reg.addValoroperacao(item.getValorBasecalculoC190() != null ? item.getValorBasecalculoC190() : zero);
			
			boolean naoTemIcms = item.getTipocobrancaicms() != null &&
				(
				Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SEM_PERMISSAO_CREDITO_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
				);
			
			boolean naoTemIcmsSt =  item.getTipocobrancaicms() != null && 
				(
				Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
				Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
				);
			
			if(naoTemIcms){
				reg.addValorbcicms(zero);
				reg.addValoricms(zero);
			} else {
				reg.addValorbcicms(item.getValorbcicms() != null ? item.getValorbcicms() : zero);
				reg.addValoricms(item.getValoricms() != null ? item.getValoricms() : zero);
			}
			
			if(naoTemIcmsSt){
				reg.addValorbcicmsst(zero);
				reg.addValoricmsst(zero);
			} else {
				reg.addValorbcicmsst(valorbcicmsst);
				reg.addValoricmsst(valoricmsst);
				if(valoricmsst != null && valoricmsst.getValue().doubleValue() > 0){
					item.setGerarE200(Boolean.TRUE);
				}
			}
			
			reg.addValorredbc(item.getValorbruto() != null ? item.getValorbruto().subtract(item.getValorbcicms() != null ? item.getValorbcicms() : zero) : zero);
			reg.addValoripi(item.getValoripi() != null ? item.getValoripi() : zero);
			
			reg.setSaida(true);
			if(item.getTipocobrancaipi() != null)
				reg.setCst_ipi_aux(item.getTipocobrancaipi().getCdnfe());
			
			listaAnalitico.add(reg);
		}
		
		return this.makeRegistroC190(filtro, listaAnalitico);
	}
	
	/**
	 * Faz o registro C190 gen�rico.
	 *
	 * @param listaAnalitico
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC190> makeRegistroC190(SpedarquivoFiltro filtro, List<RegistroAnaliticoSped> listaAnalitico) {
		
		List<RegistroC190> lista = new ArrayList<RegistroC190>();
		RegistroC190 registroC190;
		
		for (RegistroAnaliticoSped analitico : listaAnalitico) {
			if(analitico.getCst() == null || analitico.getCfop() == null) continue;
			if(analitico.getAliq_icms() == null) analitico.setAliq_icms(0d);
			
			registroC190 = new RegistroC190();
			registroC190.setSaida(analitico.getSaida());
			registroC190.setCst_ipi_aux(analitico.getCst_ipi_aux());
			
			registroC190.setReg(RegistroC190.REG);
			registroC190.setCst_icms(SPEDUtil.stringCheia(analitico.getCst(), "0", 3, false));
			registroC190.setCfop(analitico.getCfop().getCodigo());
			registroC190.setAliq_icms(analitico.getAliq_icms());
			registroC190.setVl_opr(analitico.getValoroperacao().getValue().doubleValue());
			registroC190.setVl_bc_icms(analitico.getValorbcicms().getValue().doubleValue());
			registroC190.setVl_icms(analitico.getValoricms().getValue().doubleValue());
			registroC190.setVl_bc_icms_st(analitico.getValorbcicmsst().getValue().doubleValue());
			registroC190.setVl_icms_st(analitico.getValoricmsst().getValue().doubleValue());
			registroC190.setVl_red_bc(analitico.getValorredbc().getValue().doubleValue());
			registroC190.setVl_ipi(analitico.getValoripi().getValue().doubleValue());
//			registroC190.setCod_obs(cod_obs)
			
			if(addRegistroC190(lista, registroC190)){
				lista.add(registroC190);
				filtro.getApoio().addRegistros(RegistroC190.REG);
			}
		}
		
		return lista;
	}
	
	private boolean addRegistroC190(List<RegistroC190> lista, RegistroC190 registroC190){
		boolean adicionar = true;
		
		if(lista != null && !lista.isEmpty()){
			for(RegistroC190 bean : lista){
				boolean cst = false;
				boolean cfop = false;
				boolean aliquota = false;
				
				if(bean.getCst_icms() == null && registroC190.getCst_icms() == null){
					cst = true;
				}else if(bean.getCst_icms() != null && registroC190.getCst_icms() != null && 
						bean.getCst_icms().equals(registroC190.getCst_icms())){
					cst = true;
				}
				
				if(bean.getCfop() == null && registroC190.getCfop() == null){
					cfop = true;
				}else if(bean.getCfop() != null && registroC190.getCfop() != null && 
						bean.getCfop().equals(registroC190.getCfop())){
					cfop = true;
				}
				
				if(bean.getAliq_icms() == null && registroC190.getAliq_icms() == null){
					aliquota = true;
				}else if(bean.getAliq_icms() != null && registroC190.getAliq_icms() != null && 
						bean.getAliq_icms().equals(registroC190.getAliq_icms())){
					aliquota = true;
				}
				
				if(cst && cfop && aliquota){
					if(registroC190.getVl_opr() != null)
						bean.setVl_opr(bean.getVl_opr()+registroC190.getVl_opr());
					if(registroC190.getVl_bc_icms() != null)
						bean.setVl_bc_icms(bean.getVl_bc_icms()+registroC190.getVl_bc_icms());
					if(registroC190.getVl_icms() != null)
						bean.setVl_icms(bean.getVl_icms()+registroC190.getVl_icms());
					if(registroC190.getVl_bc_icms_st() != null)
						bean.setVl_bc_icms_st(bean.getVl_bc_icms_st()+registroC190.getVl_bc_icms_st());
					if(registroC190.getVl_icms_st() != null)
						bean.setVl_icms_st(bean.getVl_icms_st()+registroC190.getVl_icms_st());
					if(registroC190.getVl_red_bc() != null)
						bean.setVl_red_bc(bean.getVl_red_bc()+registroC190.getVl_red_bc());
					if(registroC190.getVl_ipi() != null)
						bean.setVl_ipi(bean.getVl_ipi()+registroC190.getVl_ipi());
					
					if(bean.getSaida() == null){
						bean.setSaida(registroC190.getSaida());
						if(bean.getCst_ipi_aux() == null)
							bean.setCst_ipi_aux(registroC190.getCst_ipi_aux());
					}
					
					adicionar = false;
					break;
				}
			}
		}
		return adicionar;
	}
	
	/**
	 * Cria o Registro C170 do arquivo do SPED, a partir de uma Entrega.
	 *
	 * @param filtro
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC170> createRegistroC170(SpedarquivoFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroC170> lista = new ArrayList<RegistroC170>();
		RegistroC170 registroC170;
		
		int countItem = 1;
		
		Set<Entregamaterial> listaEntregamaterial = entregadocumento.getListaEntregamaterial();
		if(listaEntregamaterial != null && listaEntregamaterial.size() > 0){
			
			BigDecimal valor_it, valor_tot = BigDecimal.ZERO;
			for (Entregamaterial item : listaEntregamaterial) {
				if(item.getQtde() <= 0d) continue;
				
				registroC170 = new RegistroC170();
				registroC170.setAno(filtro.getAno());
				
				valor_it = new BigDecimal(item.getValortotalmaterial());
				
//				if(item.getValorfrete() != null)
//					valor_it = valor_it.add(item.getValorfrete().getValue());
//				if(item.getValordesconto() != null)
//					valor_it = valor_it.subtract(item.getValordesconto().getValue());
				
				valor_tot = valor_tot.add(valor_it);
				
				registroC170.setReg(RegistroC170.REG);
				registroC170.setNum_item(countItem);
				registroC170.setCod_item(item.getMaterial() != null ? filtro.getApoio().addProdutoServico(item.getMaterial().getCdmaterial(), item.getMaterial().getIdentificacaoOuCdmaterial()) : null);
//				registroC170.setDescr_compl(descr_compl)
				registroC170.setQtd(item.getQtde());
				registroC170.setUnid(item.getUnidademedidacomercial() != null ? filtro.getApoio().addUnidade(item.getUnidademedidacomercial().getCdunidademedida()) : null);
				registroC170.setVl_item(valor_it.doubleValue());
				registroC170.setVl_desc(item.getValordesconto() != null ? item.getValordesconto().getValue().doubleValue() : 0d);
				registroC170.setInd_mov(0);
				registroC170.setCst_icms(item.getCstIcmsSPED() != null ? SPEDUtil.stringCheia(item.getCstIcmsSPED(), "0", 3, false) : null);
				registroC170.setCfop(item.getCfop() != null ? item.getCfop().getCodigo() : null);
				
				if (entregadocumento.getNaturezaoperacao() != null) {
					registroC170.setCod_nat(filtro.getApoio().addNatureza(entregadocumento.getNaturezaoperacao().getNome()));
				}
				
				double valorbcicms = item.getValorbcicms() != null && item.getValorbcicms().getValue().doubleValue() > 0 ? item.getValorbcicms().getValue().doubleValue() : 0;
				double icms = item.getIcms() != null && item.getIcms() > 0 ? item.getIcms() : 0;
				double valoricms = item.getValoricms() != null && item.getValoricms().getValue().doubleValue() > 0 ? item.getValoricms().getValue().doubleValue() : 0;
				double valorbcicmsst = item.getValorbcicmsst() != null && item.getValorbcicmsst().getValue().doubleValue() > 0 ? item.getValorbcicmsst().getValue().doubleValue() : 0;
				double icmsst = item.getIcmsst() != null && item.getIcmsst() > 0 ? item.getIcmsst() : 0;
				double valoricmsst = item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0 ? item.getValoricmsst().getValue().doubleValue() : 0;
				double valorbcipi = item.getValorbcipi() != null ? item.getValorbcipi().getValue().doubleValue() : 0;
				double ipi = item.getIpi() != null ? item.getIpi() : 0;
				double valoripi = item.getValoripi() != null ? item.getValoripi().getValue().doubleValue() : 0;
				
				if(item.getCsticms() != null){
					if(item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_COM_SUBSTITUICAO) ||
						item.getCsticms().equals(Tipocobrancaicms.ISENTA) ||
						item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA) ||
						item.getCsticms().equals(Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO) ||
						item.getCsticms().equals(Tipocobrancaicms.SUSPENSAO) ||
						item.getCsticms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO)){
						valorbcicms = 0d;
						icms = 0d;
						valoricms = 0d;
						valorbcicmsst = 0d;
						icmsst = 0d;
						valoricmsst = 0d;
					}
				}
				
				registroC170.setVl_bc_icms(valorbcicms);
				registroC170.setAliq_icms(icms);
				registroC170.setVl_icms(valoricms);
				registroC170.setVl_bc_icms_st(valorbcicmsst);
				registroC170.setAliq_st(icmsst);
				registroC170.setVl_icms_st(valoricmsst);
//				registroC170.setInd_apur(ind_apur)
				registroC170.setCst_ipi(item.getCstipi() != null ? item.getCstipi().getCdnfe() : null);
//				registroC170.setCod_enq()
				registroC170.setVl_bc_ipi(valorbcipi);
				registroC170.setAliq_ipi(ipi);
//				if(item.getCstipi() != null && item.getCstipi().equals(Tipocobrancaipi.ENTRADA_RECUPERACAO_CREDITO))
					registroC170.setVl_ipi(valoripi);
				registroC170.setCst_pis(item.getCstpis() != null ? item.getCstpis().getCdnfe() : null);
				registroC170.setVl_bc_pis(item.getValorbcpis() != null ? item.getValorbcpis().getValue().doubleValue() : 0);
				registroC170.setPerc_aliq_pis(item.getPis() != null ? item.getPis() : 0);
				registroC170.setQuant_bc_pis(item.getQtdebcpis() != null ? item.getQtdebcpis() : 0);
				registroC170.setVl_aliq_pis(item.getValorunidbcpis() != null ? item.getValorunidbcpis() : 0);
				registroC170.setVl_pis(item.getValorpis() != null ? item.getValorpis().getValue().doubleValue() : 0);
				registroC170.setCst_cofins(item.getCstcofins() != null ? item.getCstcofins().getCdnfe() : null);
				registroC170.setVl_bc_cofins(item.getValorbccofins() != null ? item.getValorbccofins().getValue().doubleValue() : 0);
				registroC170.setPerc_aliq_cofins(item.getCofins() != null ? item.getCofins() : 0);
				registroC170.setQuant_bc_cofins(item.getQtdebccofins() != null ? item.getQtdebccofins() : 0);
				registroC170.setVl_aliq_cofins(item.getValorunidbccofins() != null ? item.getValorunidbccofins() : 0);
				registroC170.setVl_cofins(item.getValorcofins() != null ? item.getValorcofins().getValue().doubleValue() : 0);
				ContaContabil contaContabil = getContagerencialSped(filtro.getApoio(), entregadocumento.getNaturezaoperacao(), item.getMaterial() != null ? materialCustoEmpresaService.getContaContabilCustoEmpresa(filtro.getApoio().getMapEmpresaMaterialContaContabil(), item.getMaterial(), entregadocumento.getEmpresa()) : null);
				registroC170.setCod_cta(contaContabil !=null && contaContabil.getVcontacontabil()!=null ? contaContabil.getVcontacontabil().getIdentificador() : null);
				
				countItem++;
				
				lista.add(registroC170);
				
				filtro.getApoio().addRegistros(RegistroC170.REG);
			}
		}
		
		return lista;
	}
	
	/**
	 * Cria o Registro C140 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.DocumentoorigemService#getListaDocumentosDaEntrega
	 *
	 * @param filtro
	 * @param entrega
	 * @param entregadocumento
	 * @return
	 * @author Rodrigo Freitas
	 */
	private RegistroC140 createRegistroC140(SpedarquivoFiltro filtro, Entregadocumento entregadocumento) {
		if(entregadocumento.getModelodocumentofiscal() == null ||
				entregadocumento.getModelodocumentofiscal().getSpedfiscal_c140() == null || 
				!entregadocumento.getModelodocumentofiscal().getSpedfiscal_c140()){
			return null;
		}
		
		List<Entregapagamento> listaEntregapagamento = entregapagamentoService.findByEntregadocumento(entregadocumento);
		
		RegistroC140 registroC140 = new RegistroC140();
		
		registroC140.setReg(RegistroC140.REG);
		registroC140.setInd_emit(RegistroC140.TERCEIROS);
		registroC140.setInd_tit(entregadocumento.getTipotitulo() != null ? entregadocumento.getTipotitulo().getValue() : null);
		registroC140.setDesc_tit(entregadocumento.getDescricaotitulo());
		registroC140.setNum_tit(entregadocumento.getNumerotitulo());
		registroC140.setQtd_parc(listaEntregapagamento != null ? listaEntregapagamento.size() : 0);
		registroC140.setVl_tit(entregadocumento.getValor() != null ? entregadocumento.getValor().getValue().doubleValue() : null);
		
		if(listaEntregapagamento != null) registroC140.setListaRegistrosC141(this.createRegistroC141(filtro, listaEntregapagamento));
		
		filtro.getApoio().addRegistros(RegistroC140.REG);
		
		return registroC140;
	}
	
	/**
	 * Cria o Registro C141 do arquivo do SPED.
	 *
	 * @param filtro
	 * @param listaEntregapagamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC141> createRegistroC141(SpedarquivoFiltro filtro, List<Entregapagamento> listaEntregapagamento) {
		List<RegistroC141> lista = new ArrayList<RegistroC141>();
		RegistroC141 registroC141;
		
		int parcNum = 1;
		for (Entregapagamento ep : listaEntregapagamento) {
			if(ep != null && ep.getDtvencimento() != null && ep.getValor() != null){
				registroC141 = new RegistroC141();
				
				registroC141.setReg(RegistroC141.REG);
				registroC141.setNum_parc(parcNum);
				registroC141.setDt_vcto(ep.getDtvencimento());
				registroC141.setVl_parc(ep.getValor().getValue().doubleValue());
				
				lista.add(registroC141);
				parcNum++;
				
				filtro.getApoio().addRegistros(RegistroC141.REG);
			}
		}
		
		return lista;
	}
	
	private List<RegistroC195> createRegistroC195(SpedarquivoFiltro filtro, Set<Ajustefiscal> listaAjustefiscal) {
		List<RegistroC195> listaAgrupada = new ArrayList<RegistroC195>();
		RegistroC195 registroC195;
		
		if(SinedUtil.isListNotEmpty(listaAjustefiscal)){
			List<RegistroC195> listaDesagrupada = new ArrayList<RegistroC195>();
			for(Ajustefiscal ajustefiscal : listaAjustefiscal){
				if(ajustefiscal.getObslancamentofiscal() != null){
					registroC195 = new RegistroC195();
					
					registroC195.setReg(RegistroC195.REG);
					registroC195.setCod_obs(filtro.getApoio().addObsLancamentoFiscal(ajustefiscal.getObslancamentofiscal().getDescricao()));
					registroC195.setListaRegistroC197(createRegistroC197(filtro, ajustefiscal));
					
					listaDesagrupada.add(registroC195);
					filtro.getApoio().addRegistros(RegistroC195.REG);
				}
			}
			listaAgrupada = this.joinListaRegistroC195(listaDesagrupada, filtro);
		}
		
		return listaAgrupada;
	}
	
	private List<RegistroC195> joinListaRegistroC195(List<RegistroC195> listaDesagrupada, SpedarquivoFiltro filtro) {
		List<RegistroC195> listaAgrupada = new ArrayList<RegistroC195>();
		
		if(listaDesagrupada != null && listaDesagrupada.size() > 0){
			for (RegistroC195 registroC195 : listaDesagrupada) {
				if(listaAgrupada.contains(registroC195)){
					int idx = listaAgrupada.indexOf(registroC195);
					RegistroC195 aux = listaAgrupada.get(idx);
					aux = this.joinRegistroC195(aux, registroC195);
					listaAgrupada.set(idx, aux);
					
					filtro.getApoio().removeRegistro(RegistroC195.REG);
				} else {
					listaAgrupada.add(registroC195);
				}
			}
		}
		
		return listaAgrupada;
	}
	
	private RegistroC195 joinRegistroC195(RegistroC195 ori, RegistroC195 copia) {
		
		// REGISTRO 197
		List<RegistroC197> listaRegistroC197Ori = ori.getListaRegistroC197();
		List<RegistroC197> listaRegistroC197Copia = copia.getListaRegistroC197();
		
		if((SinedUtil.isListNotEmpty(listaRegistroC197Ori)) || (SinedUtil.isListNotEmpty(listaRegistroC197Copia))){
			ori.setListaRegistroC197(new ArrayList<RegistroC197>());
			if(SinedUtil.isListNotEmpty(listaRegistroC197Ori)){
				ori.getListaRegistroC197().addAll(listaRegistroC197Ori);
			} 
			if(SinedUtil.isListNotEmpty(listaRegistroC197Copia)){
				ori.getListaRegistroC197().addAll(listaRegistroC197Copia);
			}
		}
		
		return ori;
	}
	
	private List<RegistroC197> createRegistroC197(SpedarquivoFiltro filtro, Ajustefiscal ajustefiscal) {
		List<RegistroC197> lista = new ArrayList<RegistroC197>();
		
		if(ajustefiscal != null){
			RegistroC197 registroC197 = new RegistroC197();
			registroC197.setReg(RegistroC197.REG);
			registroC197.setCod_aj(ajustefiscal.getCodigoajuste());
			registroC197.setDesc_compl_aj(ajustefiscal.getDescricaocomplementar());
			if(ajustefiscal.getMaterial() != null && ajustefiscal.getMaterial().getCdmaterial() != null){
				if(filtro.getApoio() != null && filtro.getApoio().getMapProdutoServico() != null){
					registroC197.setCod_item(filtro.getApoio().addProdutoServico(ajustefiscal.getMaterial().getCdmaterial(), ajustefiscal.getMaterial().getIdentificacaoOuCdmaterial()));
				}
			}
			registroC197.setVl_bc_icms(ajustefiscal.getBasecalculoicms());
			registroC197.setAliq_icms(ajustefiscal.getAliquotaicms());
			registroC197.setVl_icms(ajustefiscal.getValoricms() != null ? ajustefiscal.getValoricms() : 0d);
			registroC197.setVl_outros(ajustefiscal.getOutrosvalores() != null ? ajustefiscal.getOutrosvalores() : 0d);
				
			lista.add(registroC197);
				
			filtro.getApoio().addRegistros(RegistroC197.REG);
		}
		
		return lista;
	}
	
	/**
	 * Cria o Registro C110 do arquivo do SPED, a partir de uma Entregadocumento.
	 *
	 * @param filtro
	 * @param notafiscalproduto
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC110> createRegistroC110(SpedarquivoFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroC110> listaDesagrupada = new ArrayList<RegistroC110>();
		
		String info = entregadocumento.getInfoadicionalfisco();
		if(info != null && !info.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(info));
			registroC110.setListaRegistroC112(createRegistroC112(filtro, entregadocumento));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		String infoContrib = entregadocumento.getInfoadicionalcontrib();
		if(infoContrib != null && !infoContrib.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(infoContrib));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		
		for (RegistroC110 registroC110 : listaDesagrupada) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		
		return listaDesagrupada;
	}
	
	/**
	 * Cria o Registro C110 do arquivo do SPED, a partir de uma Notafiscalproduto.
	 *
	 * @param filtro
	 * @param notafiscalproduto
	 * @return
	 * @author Rodrigo Freitas
	 */
	private List<RegistroC110> createRegistroC110(SpedarquivoFiltro filtro, Notafiscalproduto notafiscalproduto) {
		List<RegistroC110> listaDesagrupada = new ArrayList<RegistroC110>();
		
		String info = notafiscalproduto.getInfoadicionalfisco();
		if(info != null && !info.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(info));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		String infoContrib = notafiscalproduto.getInfoadicionalcontrib();
		if(infoContrib != null && !infoContrib.trim().equals("")){
			RegistroC110 registroC110 = new RegistroC110();
			
			registroC110.setReg(RegistroC110.REG);
			registroC110.setCod_inf(filtro.getApoio().addInfoComplementar(infoContrib));
			
			listaDesagrupada.add(registroC110);
			
			filtro.getApoio().addRegistros(RegistroC110.REG);
		}
		
		List<RegistroC110> listaAgrupada = new ArrayList<RegistroC110>();
		
		for (RegistroC110 registroC110 : listaDesagrupada) {
			if(!listaAgrupada.contains(registroC110)){
				listaAgrupada.add(registroC110);
			} else filtro.getApoio().removeRegistro(RegistroC110.REG);
		}		
		
		return listaDesagrupada;
	}
	
	/**
	* Cria o Registro C112 do arquivo do SPED, a partir de uma Entrada Fiscal.
	*
	* @param filtro
	* @param entregadocumento
	* @return
	* @since 31/07/2014
	* @author Luiz Fernando
	*/
	private List<RegistroC112> createRegistroC112(SpedarquivoFiltro filtro, Entregadocumento entregadocumento) {
		List<RegistroC112> listaRegistroC112 = new ArrayList<RegistroC112>();
		
		if(SinedUtil.isListNotEmpty(entregadocumento.getListaEntregadocumentoreferenciado())){
			for(Entregadocumentoreferenciado edr : entregadocumento.getListaEntregadocumentoreferenciado()){
				if (!ModelodocumentoEnum.DAE.equals(edr.getModelodocumento()) && !ModelodocumentoEnum.GNRE.equals(edr.getModelodocumento())){
					continue;
				}
				
				RegistroC112 registroC112 = new RegistroC112();
				
				registroC112.setReg(RegistroC112.REG);
				registroC112.setCod_da(edr.getModelodocumento() != null ? (edr.getModelodocumento().ordinal()+"") : null);
				registroC112.setUf(edr.getUf() != null ? edr.getUf().getSigla() : null);
				registroC112.setNum_da(edr.getNumero());
				registroC112.setCod_aut(edr.getAutenticacaobancaria());
				registroC112.setVl_da(edr.getValor() != null ? edr.getValor().getValue().doubleValue() : 0d);
				registroC112.setDt_vcto(edr.getDtvencimento());
				registroC112.setDt_pgto(edr.getDtpagamento());
				
				listaRegistroC112.add(registroC112);
				
				filtro.getApoio().addRegistros(RegistroC112.REG);
			}
		}
		
		return listaRegistroC112;
	}
	
	
	/**
	 * Cria o Registro 0990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro0990 createRegistro0990(SpedarquivoFiltro filtro) {
		Registro0990 registro0990 = new Registro0990();
		
		registro0990.setReg(Registro0990.REG);
		
		filtro.getApoio().addRegistros(Registro0990.REG);
		
		return registro0990;
	}
	
	/**
	 * Cria o Registro B990 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Fabricio Berg
	 */
	private RegistroB990 createRegistroB990(SpedarquivoFiltro filtro) {
		RegistroB990 registroB990 = new RegistroB990();
		
		registroB990.setReg(RegistroB990.REG);
		
		filtro.getApoio().addRegistros(RegistroB990.REG);
		
		return registroB990;
	}
	
	/**
	 * Cria o Registro 0001 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0005
	 * @see br.com.linkcom.sined.modulo.financeiro.controller.process.SPEDFiscalProcess#createRegistro0100
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro0001 createRegistro0001(SpedarquivoFiltro filtro) {
		Registro0001 registro0001 = new Registro0001();
		
		registro0001.setReg(Registro0001.REG);
		
		registro0001.setRegistro0005(this.createRegistro0005(filtro));
		registro0001.setRegistro0100(this.createRegistro0100(filtro));
		
		filtro.getApoio().addRegistros(Registro0001.REG);
		
		return registro0001;
	}
	
	/**
	 * Cria o Registro B001 do arquivo do SPED.
	 *
	 * @param filtro
	 * @return
	 * @author Fabricio Berg
	 */
	private RegistroB001 createRegistroB001(SpedarquivoFiltro filtro) {
		RegistroB001 registroB001 = new RegistroB001();
		
		registroB001.setReg(RegistroB001.REG);
		
		registroB001.setInd_dad(1);
		
		filtro.getApoio().addRegistros(RegistroB001.REG);
		
		return registroB001;
	}

	/**
	 * Cria o Registro 0100 do arquivo do SPED.
	 *
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadForSpedReg0100
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#loadForSpedReg0100Contabilista
	 * @see br.com.linkcom.sined.geral.service.FornecedorService#loadForSpedReg0100Escritorio
	 * @see br.com.linkcom.sined.util.SinedUtil#getTelefoneTipo
	 * @see br.com.linkcom.sined.geral.bean.Pessoa#getEndereco
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro0100 createRegistro0100(SpedarquivoFiltro filtro) {
		Registro0100 registro0100 = new Registro0100();
		
		Empresa empresa = empresaService.loadForSpedReg0100(filtro.getEmpresa());
		Fornecedor contabilista = empresa.getColaboradorcontabilista() != null  ? fornecedorService.loadForSpedReg0100Contabilista(empresa.getColaboradorcontabilista()) : null;
		Fornecedor escritorio = empresa.getEscritoriocontabilista() != null ? fornecedorService.loadForSpedReg0100Escritorio(empresa.getEscritoriocontabilista()) : null;
		
		registro0100.setReg(Registro0100.REG);
		registro0100.setCrc(empresa.getCrccontabilista());
		
		if(contabilista != null){
			registro0100.setNome(contabilista.getNome());
			registro0100.setCpf(contabilista.getCpf() != null ? contabilista.getCpf().getValue() : null);
			registro0100.setEmail(contabilista.getEmail());
			
			if(contabilista.getCpf() == null){
				filtro.addError("O campo CPF do contabilista � obrigat�rio.");
			}
		}
		
		if(escritorio != null){
			registro0100.setCnpj(escritorio.getCnpj() != null ? escritorio.getCnpj().getValue() : null);
			
			Endereco endereco = escritorio.getEndereco();
			Telefone telefone = SinedUtil.getTelefoneTipo(escritorio.getListaTelefone(), new Telefonetipo(Telefonetipo.PRINCIPAL), new Telefonetipo(Telefonetipo.COMERCIAL), new Telefonetipo(Telefonetipo.RESIDENCIAL));
			Telefone fax = SinedUtil.getTelefoneTipo(escritorio.getListaTelefone(), new Telefonetipo(Telefonetipo.FAX));
			
			if(endereco != null){
				registro0100.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
				registro0100.setEnd(endereco.getLogradouro());
				registro0100.setNum(endereco.getNumero());
				registro0100.setCompl(endereco.getComplemento());
				registro0100.setBairro(endereco.getBairro());
				registro0100.setCod_mun(endereco.getMunicipio() != null ? endereco.getMunicipio().getCdibge() : null);
			}
			registro0100.setFone(telefone != null ? SPEDUtil.soNumero(telefone.getTelefone()) : null);
			registro0100.setFax(fax != null ? SPEDUtil.soNumero(fax.getTelefone()) : null);
		}
		
		filtro.getApoio().addRegistros(Registro0100.REG);
		
		return registro0100;
	}

	/**
	 * Cria o Registro 0005 do arquivo do SPED.
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadForSpedReg0005
	 * @see br.com.linkcom.sined.util.SinedUtil#getTelefoneTipo
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	private Registro0005 createRegistro0005(SpedarquivoFiltro filtro) {
		Registro0005 registro0005 = new Registro0005();
		
		Empresa empresa = empresaService.loadForSpedReg0005(filtro.getEmpresa());
		Endereco endereco = empresa.getListaEndereco() != null ? empresa.getListaEndereco().iterator().next() : null;
		Telefone telefone = SinedUtil.getTelefoneTipo(empresa.getListaTelefone(), new Telefonetipo(Telefonetipo.PRINCIPAL), new Telefonetipo(Telefonetipo.COMERCIAL), new Telefonetipo(Telefonetipo.RESIDENCIAL));
		Telefone fax = SinedUtil.getTelefoneTipo(empresa.getListaTelefone(), new Telefonetipo(Telefonetipo.FAX));
		
		registro0005.setReg(Registro0005.REG);
		registro0005.setFantasia(empresa.getNomefantasia());
		if(endereco != null){
			registro0005.setCep(endereco.getCep() != null ? endereco.getCep().getValue() : null);
			registro0005.setEnd(endereco.getLogradouro());
			registro0005.setNum(endereco.getNumero());
			registro0005.setCompl(endereco.getComplemento());
			registro0005.setBairro(endereco.getBairro());
		}
		registro0005.setFone(telefone != null ? SPEDUtil.soNumero(telefone.getTelefone()) : null);
		registro0005.setFax(fax != null ? SPEDUtil.soNumero(fax.getTelefone()) : null);
		registro0005.setEmail(empresa.getEmail());
		
		filtro.getApoio().addRegistros(Registro0005.REG);
		
		return registro0005;
	}
	
	/**
	 * M�todo que cria registro E100
	 * 
	 * @param spedFiscal
	 * @param filtro
	 * @author Tom�s Rabelo
	 */
	private void createRegistroE100(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		List<RegistroE100> lista = new ArrayList<RegistroE100>();
		
		RegistroE100 registroE100 = new RegistroE100();
		
		registroE100.setReg(RegistroE100.REG);
		registroE100.setDt_ini(filtro.getDtinicio());
		registroE100.setDt_fin(filtro.getDtfim());
		
		registroE100.setRegistroE110(this.createRegistroE110(spedFiscal, filtro));
		
		filtro.getApoio().addRegistros(RegistroE100.REG);
		
		lista.add(registroE100);
		spedFiscal.getRegistro0000().getRegistroE001().setListaRegistroE100(lista);
	}
	
	/**
	 * M�todo que cria registro E110
	 * 
	 * @param spedFiscal
	 * @author Tom�s Rabelo
	 */
	private RegistroE110 createRegistroE110(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		double somaValores = 0.0;
		double icmsRecolher = 0.0;
		
		RegistroE110 registroE110 = new RegistroE110();
		registroE110.setReg(RegistroE110.REG);
		
		String siglaUf = spedFiscal.getRegistro0000() != null ? spedFiscal.getRegistro0000().getUf() : null;
		List<RegistroE111> listaRegistroE111 = this.createRegistroE111(filtro, siglaUf);
		registroE110.setListaRegistroE111(listaRegistroE111);
		
		Double vl_tot_aj_debitos = 0.0;
		Double vl_estornos_cred = 0.0;
		Double vl_tot_aj_creditos = 0.0;
		Double vl_estornos_deb = 0.0;
		Double vl_tot_deb = 0.0;
		Double deb_esp = 0.0;
		if(listaRegistroE111 != null && listaRegistroE111.size() > 0){
			for (RegistroE111 registroE111 : listaRegistroE111) {
				String codAjApur = registroE111.getCod_aj_apur();
				if(codAjApur != null && codAjApur.length() > 3){
					char[] charArrayCod = codAjApur.toCharArray();
					if(charArrayCod[2] == '0' && charArrayCod[3] == '0'){
						vl_tot_aj_debitos += registroE111.getVl_aj_apur();
					} else if(charArrayCod[2] == '0' && charArrayCod[3] == '1'){
						vl_estornos_cred += registroE111.getVl_aj_apur();
					} else if(charArrayCod[2] == '0' && charArrayCod[3] == '2'){
						vl_tot_aj_creditos += registroE111.getVl_aj_apur();
					} else if(charArrayCod[2] == '0' && charArrayCod[3] == '3'){
						vl_estornos_deb += registroE111.getVl_aj_apur();
					} else if(charArrayCod[2] == '0' && charArrayCod[3] == '4'){
						vl_tot_deb += registroE111.getVl_aj_apur();
					} else if(charArrayCod[2] == '0' && charArrayCod[3] == '5'){
						deb_esp += registroE111.getVl_aj_apur();
					}
				}
			}
		}
//		if(listaRegistroE116 != null && listaRegistroE116.size() > 0){
//			for (RegistroE116 registroE116 : listaRegistroE116) {
//				if(registroE116.getVl_or() != null){
//					deb_esp += registroE116.getVl_or();
//				}
//			}
//		}
		
		if(spedFiscal.getRegistro0000() != null){
			if(spedFiscal.getRegistro0000().getRegistroC001() != null &&
					spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
				for(RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()){
					if(registroC100.getListaRegistroC195() != null){
						for(RegistroC195 registroC195 : registroC100.getListaRegistroC195()){
							if(registroC195.getListaRegistroC197() != null){
								for(RegistroC197 registroC197 : registroC195.getListaRegistroC197()){
									if(registroC197.getCod_aj() != null && registroC197.getCod_aj().length() > 3 && 
											registroC197.getVl_icms() != null){
										char[] charArrayCod = registroC197.getCod_aj().toCharArray();
										if(charArrayCod[2] == '7' && (charArrayCod[3] == '0' || charArrayCod[3] == '2')){
											deb_esp += registroC197.getVl_icms();
										}
									}
								}
							}
						}
					}
				}
			}
			if(spedFiscal.getRegistro0000().getRegistroD001() != null &&
					spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100() != null){
				for(RegistroD100 registroD100 : spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100()){
					if(registroD100.getListaRegistroD195() != null){
						for(RegistroD195 registroD195 : registroD100.getListaRegistroD195()){
							if(registroD195.getListaRegistroD197() != null){
								for(RegistroD197 registroD197 : registroD195.getListaRegistroD197()){
									if(registroD197.getCod_aj() != null && registroD197.getCod_aj().length() > 3 && 
											registroD197.getVl_icms() != null){
										char[] charArrayCod = registroD197.getCod_aj().toCharArray();
										if(charArrayCod[2] == '7' && (charArrayCod[3] == '0' || charArrayCod[3] == '2')){
											deb_esp += registroD197.getVl_icms();
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		registroE110.setVl_tot_debitos(createSomatorioTotalDebitosCreditosE110(spedFiscal, 1));
		registroE110.setVl_aj_debitos(createSomatorioAjusteIcmsDebitosCreditosE110(spedFiscal, false)); //REGISTRO C197
		registroE110.setVl_tot_aj_debitos(vl_tot_aj_debitos); //REGISTRO E111
		registroE110.setVl_estornos_cred(vl_estornos_cred); //REGISTRO E111
		
		registroE110.setVl_tot_creditos(createSomatorioTotalDebitosCreditosE110(spedFiscal, 0));
		registroE110.setVl_aj_creditos(createSomatorioAjusteIcmsDebitosCreditosE110(spedFiscal, true)); //REGISTRO C197
		registroE110.setVl_tot_aj_creditos(vl_tot_aj_creditos); //REGISTRO E111
		registroE110.setVl_estornos_deb(vl_estornos_deb); //REGISTRO E111
		
		registroE110.setVl_sld_credor_ant(0.0);
		
		somaValores = (registroE110.getVl_tot_debitos() + registroE110.getVl_aj_debitos() + registroE110.getVl_tot_aj_debitos() + registroE110.getVl_estornos_cred()) -
					  (registroE110.getVl_tot_creditos() + registroE110.getVl_aj_creditos() + registroE110.getVl_tot_aj_creditos() + registroE110.getVl_estornos_deb() + registroE110.getVl_sld_credor_ant());
		
		registroE110.setVl_sld_apurado(somaValores < 0.0 ? 0.0 : somaValores);
		registroE110.setVl_tot_ded(vl_tot_deb); //REGISTRO C197 e E111
		
		icmsRecolher = somaValores - registroE110.getVl_tot_ded();
		
		registroE110.setVl_icms_recolher(icmsRecolher < 0.0 ? 0.0 : icmsRecolher);
		registroE110.setVl_sld_credor_transportar(somaValores >= 0.0 ? 0.0 : (somaValores * -1.0));
		registroE110.setDeb_esp(deb_esp);
		
		if(icmsRecolher > 0 || (deb_esp != null && deb_esp > 0)){
			List<RegistroE116> listaRegistroE116 = this.createRegistroE116(filtro, siglaUf);
			registroE110.setListaRegistroE116(listaRegistroE116);
		}
		
		filtro.getApoio().addRegistros(RegistroE110.REG);
		
		return registroE110;
	}
	
	/**
	 * M�todo que cria registro E111
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	private List<RegistroE111> createRegistroE111(SpedarquivoFiltro filtro, String siglaUf) {
		List<RegistroE111> listaRegistroE111 = new ArrayList<RegistroE111>();
		
		List<Ajusteicms> listaAjusteicms = ajusteicmsService.findForSPEDRegE111(filtro, siglaUf);
		if(listaAjusteicms != null && listaAjusteicms.size() > 0){
			for (Ajusteicms ajusteicms : listaAjusteicms) {
				RegistroE111 registroE111 = new RegistroE111();
				registroE111.setReg(RegistroE111.REG);
				registroE111.setCod_aj_apur(ajusteicms.getAjusteicmstipo() != null ? ajusteicms.getAjusteicmstipo().getCodigoSPED() : null);
				registroE111.setDescr_compl_aj(ajusteicms.getDescricaocomplementar() != null ? ajusteicms.getDescricaocomplementar() : null);
				registroE111.setVl_aj_apur(ajusteicms.getValor() != null ? ajusteicms.getValor() : 0d);
				
				registroE111.setListaRegistroE112(this.createRegistroE112(filtro, ajusteicms));
				registroE111.setListaRegistroE113(this.createRegistroE113(filtro, ajusteicms));
				
				filtro.getApoio().addRegistros(RegistroE111.REG);
				listaRegistroE111.add(registroE111);
			}
		}
		
		return listaRegistroE111;
	}
	
	/**
	 * M�todo que cria registro E113
	 *
	 * @param filtro
	 * @param ajusteicms
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	private List<RegistroE113> createRegistroE113(SpedarquivoFiltro filtro, Ajusteicms ajusteicms) {
		List<RegistroE113> listaRegistroE113 = new ArrayList<RegistroE113>();
		
		Set<Ajusteicmsdocumento> listaAjusteicmsdocumento = ajusteicms.getListaAjusteicmsdocumento();
		if(listaAjusteicmsdocumento != null && listaAjusteicmsdocumento.size() > 0){
			for (Ajusteicmsdocumento ajusteicmsdocumento : listaAjusteicmsdocumento) {
				RegistroE113 registroE113 = new RegistroE113();
				registroE113.setReg(RegistroE113.REG);
				registroE113.setCod_part(ajusteicmsdocumento.getPessoa() != null && ajusteicmsdocumento.getPessoa().getCdpessoa() != null ? filtro.getApoio().addParticipante(ajusteicmsdocumento.getPessoa().getCdpessoa()) : null);
				registroE113.setCod_mod(ajusteicmsdocumento.getModelonf() != null && ajusteicmsdocumento.getModelonf().getCodigo() != null ? ajusteicmsdocumento.getModelonf().getCodigo() : null);
				registroE113.setSer(ajusteicmsdocumento.getSerie());
				registroE113.setSub(ajusteicmsdocumento.getSubserie());
				registroE113.setNum_doc(ajusteicmsdocumento.getNumero());
				registroE113.setDt_doc(ajusteicmsdocumento.getDtemissao());
				registroE113.setCod_item(ajusteicmsdocumento.getMaterial() != null && ajusteicmsdocumento.getMaterial().getCdmaterial() != null ? filtro.getApoio().addProdutoServico(ajusteicmsdocumento.getMaterial().getCdmaterial(), ajusteicmsdocumento.getMaterial().getIdentificacaoOuCdmaterial()) : null);
				registroE113.setVl_aj_item(ajusteicmsdocumento.getValorajuste() != null ? ajusteicmsdocumento.getValorajuste() : 0d);
				
				filtro.getApoio().addRegistros(RegistroE113.REG);
				listaRegistroE113.add(registroE113);
			}
		}
		
		return listaRegistroE113;
	}
	
	private List<RegistroE116> createRegistroE116(SpedarquivoFiltro filtro, String siglaUf) {
		List<RegistroE116> listaRegistroE116 = new ArrayList<RegistroE116>();
		
		List<Obrigacaoicmsrecolher> listaObrigacaoicmsrecolher = obrigacaoicmsrecolherService.findForRegistroE116(filtro, siglaUf);
		if(listaObrigacaoicmsrecolher != null && listaObrigacaoicmsrecolher.size() > 0){
			for (Obrigacaoicmsrecolher obrigacaoicmsrecolher : listaObrigacaoicmsrecolher) {
				RegistroE116 registroE116 = new RegistroE116();
				registroE116.setReg(RegistroE116.REG);
				registroE116.setCod_or(obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher() != null ? 
						obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher().getCodigo() : null);
				registroE116.setVl_or(obrigacaoicmsrecolher.getValorobrigacao() != null ? obrigacaoicmsrecolher.getValorobrigacao().getValue().doubleValue() : null);
				registroE116.setDt_vcto(obrigacaoicmsrecolher.getDtvencimento());
				registroE116.setCod_rec(obrigacaoicmsrecolher.getCodigoreceita());
				registroE116.setNum_proc(obrigacaoicmsrecolher.getNumeroprocesso());
				registroE116.setInd_proc(obrigacaoicmsrecolher.getOrigemprocessosped() != null ? 
						obrigacaoicmsrecolher.getOrigemprocessosped().getCodigo() : null);
				registroE116.setProc(obrigacaoicmsrecolher.getProcesso());
				registroE116.setTxt_compl(obrigacaoicmsrecolher.getTextocomplementar());
				registroE116.setMes_ref(obrigacaoicmsrecolher.getMesreferencia() != null ? obrigacaoicmsrecolher.getMesreferencia().replace("/", "") : null);
				
				filtro.getApoio().addRegistros(RegistroE116.REG);
				listaRegistroE116.add(registroE116);
			}
		}
		
		return listaRegistroE116;
	}
	
	/**
	 * M�todo que cria registro E112
	 *
	 * @param filtro
	 * @param ajusteicms
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/02/2014
	 */
	private List<RegistroE112> createRegistroE112(SpedarquivoFiltro filtro, Ajusteicms ajusteicms) {
		List<RegistroE112> listaRegistroE112 = new ArrayList<RegistroE112>();
		
		Set<Ajusteicmsprocesso> listaAjusteicmsprocesso = ajusteicms.getListaAjusteicmsprocesso();
		if(listaAjusteicmsprocesso != null && listaAjusteicmsprocesso.size() > 0){
			for (Ajusteicmsprocesso ajusteicmsprocesso : listaAjusteicmsprocesso) {
				RegistroE112 registroE112 = new RegistroE112();
				registroE112.setReg(RegistroE112.REG);
				registroE112.setNum_da(ajusteicmsprocesso.getNumerodocumento());
				registroE112.setNum_proc(ajusteicmsprocesso.getNumeroprocesso());
				registroE112.setInd_proc(ajusteicmsprocesso.getOrigemprocesso() != null ? ajusteicmsprocesso.getOrigemprocesso().getId().toString() : null);
				registroE112.setProc(ajusteicmsprocesso.getDescricao());
				registroE112.setTxt_compl(ajusteicmsprocesso.getDescricaocomplementar());
				
				filtro.getApoio().addRegistros(RegistroE112.REG);
				listaRegistroE112.add(registroE112);
			}
		}
		
		return listaRegistroE112;
	}
	
	/**
	 * M�todo que cria somat�rio de debitos ou creditos
	 * 
	 * @param spedFiscal
	 * @param ind_ope
	 * @return
	 * @author Tom�s Rabelo
	 */
	private Double createSomatorioTotalDebitosCreditosE110(SPEDFiscal spedFiscal, int ind_ope) {
		Double total = 0.0;
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null){
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null)
				for (RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()) 
					if(registroC100.getInd_oper().equals(ind_ope) && registroC100.getListaRegistroC190() != null)
						for (RegistroC190 registroC190 : registroC100.getListaRegistroC190()) 
							if(registroC190.getVl_icms() != null && (registroC190.getCfop() == null || !registroC190.getCfop().equals("5605")))
								total += registroC190.getVl_icms();
			
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500() != null)
				for (RegistroC500 registroC500 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC500()) 
					if(registroC500.getInd_oper().equals(ind_ope) && registroC500.getListaRegistrosC590() != null)
						for (RegistroC590 registroC590 : registroC500.getListaRegistrosC590())
							if(registroC590.getVl_icms() != null && (registroC590.getCfop() == null || !registroC590.getCfop().equals("5605")))
								total += registroC590.getVl_icms();
			
			if(ind_ope == 0 && spedFiscal.getRegistro0000().getRegistroD001() != null && 
					spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100() != null)
				for (RegistroD100 registroD100 : spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100()) 
						if(registroD100.getVl_icms() != null)
							total += registroD100.getVl_icms();
					
			if(ind_ope == 1 && spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC400() != null){
				for (RegistroC400 registroC400 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC400()){
					if(registroC400.getListaRegistroC405() != null){
						for (RegistroC405 registroC405 : registroC400.getListaRegistroC405()){
							if(registroC405.getListaRegistroC490() != null){
								for (RegistroC490 registroC490 : registroC405.getListaRegistroC490()) {
									if(registroC490.getVl_icms() != null && (registroC490.getCfop() == null || !registroC490.getCfop().equals("5605"))){
										total += registroC490.getVl_icms();
									}
								}
							}
						}
					}
				}
			}
		}
		
		return total;
	}
	
	private Double createSomatorioAjusteIcmsDebitosCreditosE110(SPEDFiscal spedFiscal, boolean credito) {
		Double total = 0.0;
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null){
			if(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
				for (RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()){ 
					if(registroC100.getListaRegistroC195() != null){
						for (RegistroC195 registroC195 : registroC100.getListaRegistroC195()){
							if(registroC195.getListaRegistroC197() != null){
								for (RegistroC197 registroC197 : registroC195.getListaRegistroC197()){
									if(registroC197.getCod_aj() != null && registroC197.getCod_aj().length() > 3){
										String codigo = registroC197.getCod_aj().substring(2, 4);
										if(registroC197.getVl_icms() != null && ( 
												(!credito && 
													(
														("30".equals(codigo) || "33".equals(codigo) || "34".equals(codigo) ||" 35".equals(codigo)) || 
														("40".equals(codigo) || "43".equals(codigo) || "44".equals(codigo) ||" 45".equals(codigo)) ||
														("50".equals(codigo) || "53".equals(codigo) || "54".equals(codigo) ||" 55".equals(codigo))
													)
												) ||
												(credito && 
													(
														("00".equals(codigo) || "03".equals(codigo) || "04".equals(codigo) ||" 05".equals(codigo)) || 
														("10".equals(codigo) || "13".equals(codigo) || "14".equals(codigo) ||" 15".equals(codigo)) ||
														("20".equals(codigo) || "23".equals(codigo) || "24".equals(codigo) ||" 25".equals(codigo))
													)
												)
											)){
											total += registroC197.getVl_icms();
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return total;
	}
	
	private List<RegistroE200> createRegistroE200(SpedarquivoFiltro filtro, List<Entregadocumento> listaEntregadocumento, List<Notafiscalproduto> listaNfp) {
		List<RegistroE200> lista = new ArrayList<RegistroE200>();
		RegistroE200 registroE200;
		
		String ufsigla = null;
		List<Uf> listaUf = new ArrayList<Uf>();
		Endereco endereco;
		for(Notafiscalproduto nfp : listaNfp){
			if(!NotaStatus.CANCELADA.equals(nfp.getNotaStatus())){
				ufsigla = null;
				if(nfp.getListaItens() != null && !nfp.getListaItens().isEmpty()){
					if(nfp.getEnderecoCliente() != null && nfp.getEnderecoCliente().getMunicipio() != null && 
							nfp.getEnderecoCliente().getMunicipio().getUf() != null &&
							nfp.getEnderecoCliente().getMunicipio().getUf().getSigla() != null){
						ufsigla = nfp.getEnderecoCliente().getMunicipio().getUf().getSigla();
						
						if(!listaUf.contains(nfp.getEnderecoCliente().getMunicipio().getUf()))
							listaUf.add(nfp.getEnderecoCliente().getMunicipio().getUf());
					}
					if(ufsigla != null){
						for(Notafiscalprodutoitem item : nfp.getListaItens()){
//							boolean naoTemIcmsSt =  item.getTipocobrancaicms() != null && 
//							(
//							Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.ISENTA.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.SUSPENSAO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.DIFERIMENTO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.IMUNE.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getTipocobrancaicms()) ||
//							Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getTipocobrancaicms())
//							);
							
							if(Boolean.TRUE.equals(item.getGerarE200()) && item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0){
								registroE200 = new RegistroE200();
								
								registroE200.setReg(RegistroE200.REG);
								registroE200.setUf(ufsigla);
								registroE200.setDt_ini(filtro.getDtinicio());
								registroE200.setDt_fin(filtro.getDtfim());
								
								registroE200.setRegistroE210(this.createRegistroE210(item));
								this.addRegistroE200(filtro, lista, registroE200);							
							}
						}
					}
				}
			}
		}
		
		for(Entregadocumento ed : listaEntregadocumento){
			ufsigla = null;
			if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
				if(ed.getFornecedor() != null && ed.getFornecedor().getListaEndereco() != null && 
						ed.getFornecedor().getListaEndereco().size() > 0){
					endereco = ed.getFornecedor().getListaEndereco().iterator().next();
					if(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null &&
							endereco.getMunicipio().getUf().getSigla() != null){
						ufsigla = endereco.getMunicipio().getUf().getSigla();
						if(!listaUf.contains(endereco.getMunicipio().getUf()))
							listaUf.add(endereco.getMunicipio().getUf());
					}
				}
				if(ufsigla != null){
					for(Entregamaterial item : ed.getListaEntregamaterial()){
//						boolean naoTemIcmsSt =  item.getCsticms() != null && 
//						(
//						Tipocobrancaicms.TRIBUTADA_INTEGRALMENTE.equals(item.getCsticms()) ||
//						Tipocobrancaicms.TRIBUTADA_COM_REDUCAO_BC.equals(item.getCsticms()) ||
//						Tipocobrancaicms.ISENTA.equals(item.getCsticms()) ||
//						Tipocobrancaicms.NAO_TRIBUTADA.equals(item.getCsticms()) ||
//						Tipocobrancaicms.NAO_TRIBUTADA_ICMSST_DEVENDO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.SUSPENSAO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.DIFERIMENTO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO.equals(item.getCsticms()) ||
//						Tipocobrancaicms.ISENCAO_ICMS_SIMPLES_NACIONAL.equals(item.getCsticms()) ||
//						Tipocobrancaicms.IMUNE.equals(item.getCsticms()) ||
//						Tipocobrancaicms.NAO_TRIBUTADA_SIMPLES_NACIONAL.equals(item.getCsticms()) ||
//						Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE.equals(item.getCsticms())
//						);
						
						if(Boolean.TRUE.equals(item.getGerarE200()) && item.getValoricmsst() != null && item.getValoricmsst().getValue().doubleValue() > 0){
							registroE200 = new RegistroE200();
							
							registroE200.setReg(RegistroE200.REG);
							registroE200.setUf(ufsigla);
							registroE200.setDt_ini(filtro.getDtinicio());
							registroE200.setDt_fin(filtro.getDtfim());
							
							registroE200.setRegistroE210(this.createRegistroE210(item));
							this.addRegistroE200(filtro, lista, registroE200);							
						}
					}
				}
			}
		}
		
		if(listaUf != null && !listaUf.isEmpty() && lista != null && !lista.isEmpty()){
			for(RegistroE200 regE200 : lista){
				if(regE200.getRegistroE210() != null){
					Uf uf = getUf(regE200.getUf(), listaUf);
					if(uf != null){
						regE200.getRegistroE210().setListaRegistroE220(createRegistroE220(filtro, uf, regE200.getRegistroE210()));
						regE200.getRegistroE210().setListaRegistroE250(createRegistroE250(filtro, uf));
					}
				}
			}
		}
		
		return lista;
	}
	
	private Uf getUf(String sigla, List<Uf> listaUf){
		if(listaUf != null && !listaUf.isEmpty() && sigla != null){
			for(Uf uf : listaUf){
				if(uf.getSigla() != null && uf.getSigla().equals(sigla))
					return uf;
			}
		}
		return null;
	}
	
	private void addRegistroE200(SpedarquivoFiltro filtro, List<RegistroE200> lista, RegistroE200 registroE200) {
		boolean adicionar = true;
		
		if(lista == null) lista = new ArrayList<RegistroE200>();
		
		if(registroE200 != null && registroE200.getUf() != null){
			for(RegistroE200 e200 : lista){
				if(e200.getUf() != null && e200.getUf().equals(registroE200.getUf())){
					adicionar = false;
					
					RegistroE210 registroE210 = e200.getRegistroE210();
					
					if(registroE210 != null){
						registroE210.setVl_sld_credor_ant_st(0.0);
						registroE210.setVl_devolv_st(registroE210.getVl_devolv_st()+registroE200.getRegistroE210().getVl_devolv_st());
						registroE210.setVl_ressarc_st(registroE210.getVl_ressarc_st()+registroE200.getRegistroE210().getVl_ressarc_st());
						registroE210.setVl_out_cred_st(registroE210.getVl_out_cred_st()+registroE200.getRegistroE210().getVl_out_cred_st());
						registroE210.setVl_aj_creditos_st(registroE210.getVl_aj_creditos_st()+registroE200.getRegistroE210().getVl_aj_creditos_st());
						registroE210.setVl_retencao_st(registroE210.getVl_retencao_st()+registroE200.getRegistroE210().getVl_retencao_st());
						registroE210.setVl_out_deb_st(registroE210.getVl_out_deb_st()+registroE200.getRegistroE210().getVl_out_deb_st());
						registroE210.setVl_aj_debitos_st(registroE210.getVl_aj_debitos_st()+registroE200.getRegistroE210().getVl_aj_debitos_st());
						registroE210.setVl_deducoes_st(registroE210.getVl_deducoes_st()+registroE200.getRegistroE210().getVl_deducoes_st());
						registroE210.setDeb_esp_st(registroE210.getDeb_esp_st()+registroE200.getRegistroE210().getDeb_esp_st());
					} else {
						registroE210 = registroE200.getRegistroE210();
					}
					
					Double saldo = 
						(registroE210.getVl_retencao_st() + 
							registroE210.getVl_out_deb_st() + 
							registroE210.getVl_aj_debitos_st())
						-
						(registroE210.getVl_sld_credor_ant_st() + 
							registroE210.getVl_devolv_st() + 
							registroE210.getVl_ressarc_st() +
							registroE210.getVl_out_cred_st() + 
							registroE210.getVl_aj_creditos_st());
					
					if(saldo >= 0){
						registroE210.setVl_sld_dev_ant_st(saldo);
						registroE210.setVl_sld_cred_st_transportar(0.0);
					} else {
						registroE210.setVl_sld_dev_ant_st(0.0);
						registroE210.setVl_sld_cred_st_transportar(saldo * -1);
					}
					registroE210.setVl_icms_recol_st(registroE210.getVl_sld_dev_ant_st() - registroE210.getVl_deducoes_st());
					
					e200.setRegistroE210(registroE210);
					break;
				}
			}
		}
		
		if(adicionar){
			filtro.getApoio().addRegistros(RegistroE200.REG);
			filtro.getApoio().addRegistros(RegistroE210.REG);
			lista.add(registroE200);
		}
	}
	
	public RegistroE210 createRegistroE210(Notafiscalprodutoitem item){
		RegistroE210 registroE210 = new RegistroE210();
		registroE210.setReg(RegistroE210.REG);
		registroE210.setInd_mov_st("0");
		registroE210.setVl_sld_credor_ant_st(0.0);
		registroE210.setVl_aj_creditos_st(0.0);
		registroE210.setVl_out_deb_st(0.0);
		registroE210.setVl_aj_debitos_st(0.0);
		registroE210.setVl_deducoes_st(0.0);
		registroE210.setDeb_esp_st(0.0);
		
		String codigoCFOP = item.getCfop() != null && item.getCfop().getCodigo() != null ? item.getCfop().getCodigo() : null;
		
		String[] campo4CFOPs = {"1410", "1411", "1414", "1415", "1660", "1661", "1662", "2410", "2411", "2414", "2415", "2660", "2661", "2662"};
		if(codigoCFOP != null && ArrayUtils.contains(campo4CFOPs, codigoCFOP)){
			registroE210.setVl_devolv_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_devolv_st(0.0);
		}
		
		String[] campo5CFOPs = {"1603", "2603"};
		if(codigoCFOP != null && ArrayUtils.contains(campo5CFOPs, codigoCFOP)){
			registroE210.setVl_ressarc_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_ressarc_st(0.0);
		}
		
		if(codigoCFOP != null && (codigoCFOP.startsWith("1") || codigoCFOP.startsWith("2")) && !ArrayUtils.contains(campo4CFOPs, codigoCFOP)){
			registroE210.setVl_out_cred_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		}else {
			registroE210.setVl_out_cred_st(0.0);
		}
		
		if(codigoCFOP != null && (codigoCFOP.startsWith("5") || codigoCFOP.startsWith("6"))){
			registroE210.setVl_retencao_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_retencao_st(0.0);
		}
		
		return registroE210;
	}
	
	public RegistroE210 createRegistroE210(Entregamaterial item){
		RegistroE210 registroE210 = new RegistroE210();
		registroE210.setReg(RegistroE210.REG);
		registroE210.setInd_mov_st("0");
		registroE210.setVl_sld_credor_ant_st(0.0);
		registroE210.setVl_aj_creditos_st(0.0);
		registroE210.setVl_out_deb_st(0.0);
		registroE210.setVl_aj_debitos_st(0.0);
		registroE210.setVl_deducoes_st(0.0);
		registroE210.setDeb_esp_st(0.0);
		
		String codigoCFOP = item.getCfop() != null && item.getCfop().getCodigo() != null ? item.getCfop().getCodigo() : null;
		
		String[] campo4CFOPs = {"1410", "1411", "1414", "1415", "1660", "1661", "1662", "2410", "2411", "2414", "2415", "2660", "2661", "2662"};
		if(codigoCFOP != null && ArrayUtils.contains(campo4CFOPs, codigoCFOP)){
			registroE210.setVl_devolv_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_devolv_st(0.0);
		}
		
		String[] campo5CFOPs = {"1603", "2603"};
		if(codigoCFOP != null && ArrayUtils.contains(campo5CFOPs, codigoCFOP)){
			registroE210.setVl_ressarc_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_ressarc_st(0.0);
		}
		
		if(codigoCFOP != null && (codigoCFOP.startsWith("1") || codigoCFOP.startsWith("2")) && !ArrayUtils.contains(campo4CFOPs, codigoCFOP)){
			registroE210.setVl_out_cred_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		}else {
			registroE210.setVl_out_cred_st(0.0);
		}
		
		if(codigoCFOP != null && (codigoCFOP.startsWith("5") || codigoCFOP.startsWith("6"))){
			registroE210.setVl_retencao_st(item.getValoricmsst() != null ? item.getValoricmsst().getValue().doubleValue() : 0.0);
		} else {
			registroE210.setVl_retencao_st(0.0);
		}
		
		return registroE210;			
	}
	
	private List<RegistroE220> createRegistroE220(SpedarquivoFiltro filtro, Uf uf, RegistroE210 registroE210) {
		List<RegistroE220> listaRegistroE220 = new ArrayList<RegistroE220>();
		
		List<Ajusteicms> listaAjusteicms = ajusteicmsService.findForSPEDRegE220(filtro, uf);
		Double vlOutCredSt = registroE210.getVl_out_cred_st() != null ? registroE210.getVl_out_cred_st() : 0d;
		Double vlOutDebSt = registroE210.getVl_out_deb_st() != null ? registroE210.getVl_out_deb_st() : 0d;
		if(listaAjusteicms != null && listaAjusteicms.size() > 0){
			for (Ajusteicms ajusteicms : listaAjusteicms) {
				RegistroE220 registroE220 = new RegistroE220();
				registroE220.setReg(RegistroE220.REG);
				registroE220.setCod_aj_apur(ajusteicms.getAjusteicmstipo() != null ? ajusteicms.getAjusteicmstipo().getCodigoSPED() : null);
				registroE220.setDescr_compl_aj(ajusteicms.getDescricaocomplementar() != null ? ajusteicms.getDescricaocomplementar() : null);
				registroE220.setVl_aj_apur(ajusteicms.getValor() != null ? ajusteicms.getValor() : 0d);
				
				if(registroE220.getVl_aj_apur() != null && ajusteicms.getAjusteicmstipo() != null && 
						ajusteicms.getAjusteicmstipo().getTipoajuste() != null){
					if(TipoAjuste.OUTROS_DEBITOS.equals(ajusteicms.getAjusteicmstipo().getTipoajuste()) ||
						 TipoAjuste.ESTORNO_CREDITOS.equals(ajusteicms.getAjusteicmstipo().getTipoajuste())){
						vlOutDebSt += registroE220.getVl_aj_apur();
					}else if(TipoAjuste.OUTROS_CREDITOS.equals(ajusteicms.getAjusteicmstipo().getTipoajuste()) ||
						 TipoAjuste.ESTORNO_DEBITOS.equals(ajusteicms.getAjusteicmstipo().getTipoajuste())){
						vlOutCredSt += registroE220.getVl_aj_apur();
					}
				}
				
				registroE220.setListaRegistroE230(this.createRegistroE230(filtro, ajusteicms));
				registroE220.setListaRegistroE240(this.createRegistroE240(filtro, ajusteicms));
				
				filtro.getApoio().addRegistros(RegistroE220.REG);
				listaRegistroE220.add(registroE220);
			}
		}
		
		registroE210.setVl_out_cred_st(vlOutCredSt);
		registroE210.setVl_out_deb_st(vlOutDebSt);
		
		Double saldo = 
			(registroE210.getVl_retencao_st() + 
				registroE210.getVl_out_deb_st() + 
				registroE210.getVl_aj_debitos_st())
			-
			(registroE210.getVl_sld_credor_ant_st() + 
				registroE210.getVl_devolv_st() + 
				registroE210.getVl_ressarc_st() +
				registroE210.getVl_out_cred_st() + 
				registroE210.getVl_aj_creditos_st());
		
		if(saldo >= 0){
			registroE210.setVl_sld_dev_ant_st(saldo);
			registroE210.setVl_sld_cred_st_transportar(0.0);
		} else {
			registroE210.setVl_sld_dev_ant_st(0.0);
			registroE210.setVl_sld_cred_st_transportar(saldo * -1);
		}
		registroE210.setVl_icms_recol_st(registroE210.getVl_sld_dev_ant_st() - registroE210.getVl_deducoes_st());
		
		return listaRegistroE220;
	}
	
	private List<RegistroE230> createRegistroE230(SpedarquivoFiltro filtro, Ajusteicms ajusteicms) {
		List<RegistroE230> listaRegistroE230 = new ArrayList<RegistroE230>();
		
		Set<Ajusteicmsprocesso> listaAjusteicmsprocesso = ajusteicms.getListaAjusteicmsprocesso();
		if(listaAjusteicmsprocesso != null && listaAjusteicmsprocesso.size() > 0){
			for (Ajusteicmsprocesso ajusteicmsprocesso : listaAjusteicmsprocesso) {
				RegistroE230 registroE230 = new RegistroE230();
				registroE230.setReg(RegistroE230.REG);
				registroE230.setNum_da(ajusteicmsprocesso.getNumerodocumento());
				registroE230.setNum_proc(ajusteicmsprocesso.getNumeroprocesso());
				registroE230.setInd_proc(ajusteicmsprocesso.getOrigemprocesso() != null ? ajusteicmsprocesso.getOrigemprocesso().getId().toString() : null);
				registroE230.setProc(ajusteicmsprocesso.getDescricao());
				registroE230.setTxt_compl(ajusteicmsprocesso.getDescricaocomplementar());
				
				filtro.getApoio().addRegistros(RegistroE230.REG);
				listaRegistroE230.add(registroE230);
			}
		}
		
		return listaRegistroE230;
	}
	
	private List<RegistroE240> createRegistroE240(SpedarquivoFiltro filtro, Ajusteicms ajusteicms) {
		List<RegistroE240> listaRegistroE240 = new ArrayList<RegistroE240>();
		
		Set<Ajusteicmsdocumento> listaAjusteicmsdocumento = ajusteicms.getListaAjusteicmsdocumento();
		if(listaAjusteicmsdocumento != null && listaAjusteicmsdocumento.size() > 0){
			for (Ajusteicmsdocumento ajusteicmsdocumento : listaAjusteicmsdocumento) {
				RegistroE240 registroE240 = new RegistroE240();
				registroE240.setReg(RegistroE240.REG);
				registroE240.setCod_part(ajusteicmsdocumento.getPessoa() != null && ajusteicmsdocumento.getPessoa().getCdpessoa() != null ? filtro.getApoio().addParticipante(ajusteicmsdocumento.getPessoa().getCdpessoa()) : null);
				registroE240.setCod_mod(ajusteicmsdocumento.getModelonf() != null && ajusteicmsdocumento.getModelonf().getCodigo() != null ? ajusteicmsdocumento.getModelonf().getCodigo() : null);
				registroE240.setSer(ajusteicmsdocumento.getSerie());
				registroE240.setSub(ajusteicmsdocumento.getSubserie());
				registroE240.setNum_doc(ajusteicmsdocumento.getNumero());
				registroE240.setDt_doc(ajusteicmsdocumento.getDtemissao());
				registroE240.setCod_item(ajusteicmsdocumento.getMaterial() != null && ajusteicmsdocumento.getMaterial().getCdmaterial() != null ? filtro.getApoio().addProdutoServico(ajusteicmsdocumento.getMaterial().getCdmaterial(), ajusteicmsdocumento.getMaterial().getIdentificacaoOuCdmaterial()) : null);
				registroE240.setVl_aj_item(ajusteicmsdocumento.getValorajuste() != null ? ajusteicmsdocumento.getValorajuste() : 0d);
				
				filtro.getApoio().addRegistros(RegistroE240.REG);
				listaRegistroE240.add(registroE240);
			}
		}
		
		return listaRegistroE240;
	}
	
	private List<RegistroE250> createRegistroE250(SpedarquivoFiltro filtro, Uf uf) {
		List<RegistroE250> listaRegistroE250 = new ArrayList<RegistroE250>();
		if(uf != null){
			List<Obrigacaoicmsrecolher> listaObrigacaoicmsrecolher = obrigacaoicmsrecolherService.findForRegistroE250(filtro.getEmpresa(), uf, filtro.getMesano());
			if(SinedUtil.isListNotEmpty(listaObrigacaoicmsrecolher)){
				for (Obrigacaoicmsrecolher obrigacaoicmsrecolher : listaObrigacaoicmsrecolher) {
					RegistroE250 registroE250 = new RegistroE250();
					registroE250.setReg(RegistroE250.REG);
					registroE250.setCod_or(obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher() != null ? obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher().getCodigo() : null);
					registroE250.setVl_or(obrigacaoicmsrecolher.getValorobrigacao() != null ? obrigacaoicmsrecolher.getValorobrigacao().getValue().doubleValue() : null);
					registroE250.setDt_vcto(obrigacaoicmsrecolher.getDtvencimento());
					registroE250.setCod_rec(obrigacaoicmsrecolher.getCodigoreceita());
					registroE250.setNum_proc(obrigacaoicmsrecolher.getNumeroprocesso());
					registroE250.setInd_proc(obrigacaoicmsrecolher.getOrigemprocessosped() != null ? obrigacaoicmsrecolher.getOrigemprocessosped().getCodigo() : null);
					registroE250.setProc(obrigacaoicmsrecolher.getProcesso());
					registroE250.setTxt_compl(obrigacaoicmsrecolher.getTextocomplementar());
					registroE250.setMes_ref(obrigacaoicmsrecolher.getMesreferencia() != null ? obrigacaoicmsrecolher.getMesreferencia().replace("/", "") : null);
					
					filtro.getApoio().addRegistros(RegistroE250.REG);
					listaRegistroE250.add(registroE250);
				}
			}
		}
		
		return listaRegistroE250;
	}
	
	private void createRegistroE300(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro){
		if(spedFiscal.getRegistro0000() != null && spedFiscal.getRegistro0000().getRegistro0001() != null && spedFiscal.getRegistro0000().getRegistroE001() != null &&
				((spedFiscal.getRegistro0000().getRegistroC001() != null && SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100())) ||
				(spedFiscal.getRegistro0000().getRegistroD001() != null && SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroD001().getListaRegistroD100())))){
			
			List<RegistroE300> listaRegistroE300 = new ArrayList<RegistroE300>();
			Boolean primeiroPeriodoApuracao = SinedDateUtils.getMes(spedFiscal.getRegistro0000().getDt_ini()) == 0;
			if(spedFiscal.getRegistro0000().getRegistroC001() != null && SinedUtil.isListNotEmpty(spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100())){
				for(RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()){
					if(registroC100.getRegistroC101() != null && registroC100.getRegistroC101().getUfsigla() != null && 
							((registroC100.getRegistroC101().getVl_fcp_uf_dest() != null &&  registroC100.getRegistroC101().getVl_fcp_uf_dest() > 0) ||
							 (registroC100.getRegistroC101().getVl_icms_uf_dest() != null &&  registroC100.getRegistroC101().getVl_icms_uf_dest() > 0) ||
							 (registroC100.getRegistroC101().getVl_icms_uf_rem() != null &&  registroC100.getRegistroC101().getVl_icms_uf_rem() > 0))){
						
						if(spedFiscal.getRegistro0000().getUf() != null){
							createRegistroE310(spedFiscal, filtro, listaRegistroE300, registroC100, primeiroPeriodoApuracao, spedFiscal.getRegistro0000().getUf());
							createRegistroE310(spedFiscal, filtro, listaRegistroE300, registroC100, primeiroPeriodoApuracao, registroC100.getRegistroC101().getUfsigla());
						}
					}
				}
			}
			
			for(RegistroE300 registroE300 : listaRegistroE300){
				filtro.getApoio().addRegistros(RegistroE300.REG);
				
				if(SinedUtil.isListNotEmpty(registroE300.getListaRegistroE310())){
					for(RegistroE310 registroE310 : registroE300.getListaRegistroE310()){
						if(registroE310 != null){
							registroE310.setListaRegistroE311(createRegistroE311(filtro, registroE300.getUf(), registroE310));
							
							Double vl_sld_dev_ant_difal = (registroE310.getVl_tot_debitos_difal() + registroE310.getVl_out_deb_difal()) - 
														  (registroE310.getVl_sld_cred_ant_difal() + registroE310.getVl_tot_creditos_difal() + registroE310.getVl_out_cred_difal());
							registroE310.setVl_sld_dev_ant_difal(vl_sld_dev_ant_difal > 0 ? vl_sld_dev_ant_difal : 0d);
							
							Double vl_recol_difal = registroE310.getVl_sld_dev_ant_difal() - registroE310.getVl_deducoes_difal();
							registroE310.setVl_recol_difal(vl_recol_difal > 0 ? vl_recol_difal : 0d);
							
							Double vl_sld_dev_ant_fcp = (registroE310.getVl_tot_deb_fcp() + registroE310.getVl_out_deb_fcp()) - 
							  							  (registroE310.getVl_sld_cred_ant_fcp() + registroE310.getVl_tot_cred_fcp() + registroE310.getVl_out_cred_fcp());
							registroE310.setVl_sld_dev_ant_fcp(vl_sld_dev_ant_fcp > 0 ? vl_sld_dev_ant_fcp : 0d);
							
							Double vl_recol_fcp = registroE310.getVl_sld_dev_ant_fcp() - registroE310.getVl_deducoes_fcp();
							registroE310.setVl_recol_fcp(vl_recol_fcp > 0 ? vl_recol_fcp : 0d);
							
							Double vl_op = (vl_recol_difal != null ? vl_recol_difal : 0d) + (registroE310.getDeb_esp_difal() != null ? registroE310.getDeb_esp_difal() : 0d) + 
												(vl_recol_fcp != null ? vl_recol_fcp : 0d) + (registroE310.getDeb_esp_fcp() != null ? registroE310.getDeb_esp_fcp() : 0d);
 							if(vl_op != null && vl_op > 0){
								registroE310.setListaRegistroE316(createRegistroE316(filtro, registroE300.getUf(), registroE310));
							}
							
							filtro.getApoio().addRegistros(RegistroE310.REG);
						}
					}
				}
			}
			
			spedFiscal.getRegistro0000().getRegistroE001().setListaRegistroE300(listaRegistroE300);
		}
	}
	
	private List<RegistroE316> createRegistroE316(SpedarquivoFiltro filtro, String uf, RegistroE310 registroE310) {
		List<RegistroE316> listaRegistroE316 = new ArrayList<RegistroE316>();
		
		List<Obrigacaoicmsrecolher> listaObrigacaoicmsrecolher = obrigacaoicmsrecolherService.findForRegistroE316(filtro.getEmpresa(), uf, filtro.getMesano());
		if(listaObrigacaoicmsrecolher != null && listaObrigacaoicmsrecolher.size() > 0){
			for (Obrigacaoicmsrecolher obrigacaoicmsrecolher : listaObrigacaoicmsrecolher) {
				RegistroE316 registroE316 = new RegistroE316();
				registroE316.setReg(RegistroE316.REG);
				registroE316.setCod_or(obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher() != null ? obrigacaoicmsrecolher.getCodobrigacaoicmsrecolher().getCodigo() : null);
				registroE316.setVl_or(obrigacaoicmsrecolher.getValorobrigacao() != null ? obrigacaoicmsrecolher.getValorobrigacao().getValue().doubleValue() : null);
				registroE316.setDt_vcto(obrigacaoicmsrecolher.getDtvencimento());
				registroE316.setCod_rec(obrigacaoicmsrecolher.getCodigoreceita());
				registroE316.setNum_proc(obrigacaoicmsrecolher.getNumeroprocesso());
				registroE316.setInd_proc(obrigacaoicmsrecolher.getOrigemprocessosped() != null ? obrigacaoicmsrecolher.getOrigemprocessosped().getCodigo() : null);
				registroE316.setProc(obrigacaoicmsrecolher.getProcesso());
				registroE316.setTxt_compl(obrigacaoicmsrecolher.getTextocomplementar());
				registroE316.setMes_ref(obrigacaoicmsrecolher.getMesreferencia() != null ? obrigacaoicmsrecolher.getMesreferencia().replace("/", "") : null);
				
				filtro.getApoio().addRegistros(RegistroE316.REG);
				listaRegistroE316.add(registroE316);
			}
		}
		
		return listaRegistroE316;
	}
	
	private void createRegistroE310(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro, List<RegistroE300> listaRegistroE300, RegistroC100 registroC100, Boolean primeiroPeriodoApuracao, String uf) {
		String uf0000 = spedFiscal.getRegistro0000().getUf();
		RegistroE300 registroE300 = getRegistroE300(listaRegistroE300, uf, filtro);
		if(registroE300 == null){
			registroE300 = new RegistroE300();
			registroE300.setReg(RegistroE300.REG);
			registroE300.setDt_ini(spedFiscal.getRegistro0000().getDt_ini());
			registroE300.setDt_fin(spedFiscal.getRegistro0000().getDt_fin());
			registroE300.setUf(uf);
			
			listaRegistroE300.add(registroE300);
		}
		
		RegistroE310 registroE310 = getRegistroE310(registroE300);
		registroE310.setInd_mov_difal("0");
		
		if(new Integer(1).equals(registroC100.getInd_oper())){
			if(uf.equals(uf0000) && registroC100.getRegistroC101() != null && registroC100.getRegistroC101().getVl_icms_uf_rem() != null){
				registroE310.addVl_tot_debitos_difal(registroC100.getRegistroC101().getVl_icms_uf_rem());
				if(primeiroPeriodoApuracao && ("01".equals(registroC100.getCod_sit()) || "07".equals(registroC100.getCod_sit()))){
					registroE310.addDeb_esp_difal(registroC100.getRegistroC101().getVl_icms_uf_rem());
				}
			}else if(registroC100.getRegistroC101() != null && registroC100.getRegistroC101().getVl_icms_uf_dest() != null){
				registroE310.addVl_tot_debitos_difal(registroC100.getRegistroC101().getVl_icms_uf_dest());
				if(primeiroPeriodoApuracao && ("01".equals(registroC100.getCod_sit()) || "07".equals(registroC100.getCod_sit()))){
					registroE310.addDeb_esp_difal(registroC100.getRegistroC101().getVl_icms_uf_dest());
				}
			}
		}
		
		if(new Integer(0).equals(registroC100.getInd_oper())){
			if(uf.equals(uf0000) && registroC100.getRegistroC101().getVl_icms_uf_dest() != null){
				registroE310.addVl_tot_creditos_difal(registroC100.getRegistroC101().getVl_icms_uf_dest());
			}else if(registroC100.getRegistroC101().getVl_icms_uf_rem() != null){
				registroE310.addVl_tot_creditos_difal(registroC100.getRegistroC101().getVl_icms_uf_rem());
			}
		}
		
		if(uf.equals(uf0000)){
			registroE310.addVl_tot_deb_fcp(0d);
		}else {
			if(new Integer(1).equals(registroC100.getInd_oper()) && registroC100.getRegistroC101().getVl_fcp_uf_dest() != null){
				registroE310.addVl_tot_deb_fcp(registroC100.getRegistroC101().getVl_fcp_uf_dest());
				if(primeiroPeriodoApuracao && ("01".equals(registroC100.getCod_sit()) || "07".equals(registroC100.getCod_sit()))){
					registroE310.addDeb_esp_fcp(registroC100.getRegistroC101().getVl_fcp_uf_dest());
				}
			}
		}
		
		if(uf.equals(uf0000)){
			registroE310.addVl_tot_cred_fcp(0d);
		}else {
			if(new Integer(0).equals(registroC100.getInd_oper()) && registroC100.getRegistroC101().getVl_fcp_uf_dest() != null){
				registroE310.addVl_tot_cred_fcp(registroC100.getRegistroC101().getVl_fcp_uf_dest());
			}
		}
	}
	
	private RegistroE310 getRegistroE310(RegistroE300 registroE300) {
		if(registroE300.getListaRegistroE310() == null) registroE300.setListaRegistroE310(new ArrayList<RegistroE310>());
		
		for(RegistroE310 registroE310 : registroE300.getListaRegistroE310()){
			return registroE310;
		}
		
		RegistroE310 registroE310 = new RegistroE310();
		registroE310.setReg(RegistroE310.REG);
		registroE310.setVl_sld_cred_ant_difal(0d);
		registroE310.setVl_tot_debitos_difal(0d);
		registroE310.setVl_out_deb_difal(0d);
		registroE310.setVl_tot_creditos_difal(0d);
		registroE310.setVl_out_cred_difal(0d);
		registroE310.setVl_sld_dev_ant_difal(0d);
		registroE310.setVl_deducoes_difal(0d);
		registroE310.setVl_recol_difal(0d);
		registroE310.setVl_sld_cred_transportar_difal(0d);
		registroE310.setDeb_esp_difal(0d);
		registroE310.setVl_sld_cred_ant_fcp(0d);
		registroE310.setVl_tot_deb_fcp(0d);
		registroE310.setVl_out_deb_fcp(0d);
		registroE310.setVl_tot_cred_fcp(0d);
		registroE310.setVl_out_cred_fcp(0d);
		registroE310.setVl_sld_dev_ant_fcp(0d);
		registroE310.setVl_deducoes_fcp(0d);
		registroE310.setVl_recol_fcp(0d);
		registroE310.setVl_sld_cred_transportar_fcp(0d);
		registroE310.setDeb_esp_fcp(0d);
		
		registroE300.getListaRegistroE310().add(registroE310);
		
		return registroE310;
	}
	
	private RegistroE300 getRegistroE300(List<RegistroE300> listaRegistroE300, String ufsigla, SpedarquivoFiltro filtro) {
		for(RegistroE300 registroE300 : listaRegistroE300){
			if(registroE300.getUf().equals(ufsigla)){
				return registroE300;
			}
		}
		return null;
	}
	
	private List<RegistroE311> createRegistroE311(SpedarquivoFiltro filtro, String siglaUf, RegistroE310 registroE310) {
		List<RegistroE311> listaRegistroE311 = new ArrayList<RegistroE311>();
				
		List<Ajusteicms> listaAjusteicms = ajusteicmsService.findForSPEDRegE311(filtro, siglaUf);
		Double vlOutDebDifal = registroE310.getVl_out_deb_difal() != null ? registroE310.getVl_out_deb_difal() : 0d;
		Double vlOutCredDifal = registroE310.getVl_out_cred_difal() != null ? registroE310.getVl_out_cred_difal() : 0d;
		Double vlDeducoesDifal = registroE310.getVl_deducoes_difal() != null ? registroE310.getVl_deducoes_difal() : 0d;
		Double debEspDifal = registroE310.getDeb_esp_difal() != null ? registroE310.getDeb_esp_difal() : 0d;
		Double vlOutDebFcp = registroE310.getVl_out_deb_fcp() != null ? registroE310.getVl_out_deb_fcp() : 0d;
		Double vlOutCredFcp = registroE310.getVl_out_cred_fcp() != null ? registroE310.getVl_out_cred_fcp() : 0d;
		Double vlDeducoesFcp = registroE310.getVl_deducoes_fcp() != null ? registroE310.getVl_deducoes_fcp() : 0d;
		Double debEspFcp = registroE310.getDeb_esp_fcp() != null ? registroE310.getDeb_esp_fcp() : 0d;
		
		if(listaAjusteicms != null && listaAjusteicms.size() > 0){
			for (Ajusteicms ajusteicms : listaAjusteicms) {
				RegistroE311 registroE311 = new RegistroE311();
				registroE311.setReg(RegistroE311.REG);
				registroE311.setCod_aj_apur(ajusteicms.getAjusteicmstipo() != null ? ajusteicms.getAjusteicmstipo().getCodigoSPED(siglaUf) : null);
				registroE311.setDescr_compl_aj(ajusteicms.getDescricaocomplementar() != null ? ajusteicms.getDescricaocomplementar() : null);
				registroE311.setVl_aj_apur(ajusteicms.getValor() != null ? ajusteicms.getValor() : 0d);
				
				if(registroE311.getVl_aj_apur() != null && registroE311.getCod_aj_apur() != null && registroE311.getCod_aj_apur().length() > 3){
					char[] charArrayCod = registroE311.getCod_aj_apur().toCharArray();
					if(charArrayCod[2] == '2' && (charArrayCod[3] == '0' || charArrayCod[3] == '1')){
						vlOutDebDifal += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '2' && (charArrayCod[3] == '2' || charArrayCod[3] == '3')){
						vlOutCredDifal += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '2' && charArrayCod[3] == '4'){
						vlDeducoesDifal += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '2' && charArrayCod[3] == '5'){
						debEspDifal += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '3' && (charArrayCod[3] == '0' || charArrayCod[3] == '1')){
						vlOutDebFcp += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '3' && (charArrayCod[3] == '2' || charArrayCod[3] == '3')){
						vlOutCredFcp += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '3' && charArrayCod[3] == '4'){
						vlDeducoesFcp += registroE311.getVl_aj_apur();
					}else if(charArrayCod[2] == '3' && charArrayCod[3] == '5'){
						debEspFcp += registroE311.getVl_aj_apur();
					}
				}
				
				filtro.getApoio().addRegistros(RegistroE311.REG);
				listaRegistroE311.add(registroE311);
			}
		}
		
		registroE310.setVl_out_deb_difal(vlOutDebDifal);
		registroE310.setVl_out_cred_difal(vlOutCredDifal);
		registroE310.setVl_deducoes_difal(vlDeducoesDifal);
		registroE310.setDeb_esp_difal(debEspDifal);
		registroE310.setVl_out_deb_fcp(vlOutDebFcp);
		registroE310.setVl_out_cred_fcp(vlOutCredFcp);
		registroE310.setVl_deducoes_fcp(vlDeducoesFcp);
		registroE310.setDeb_esp_fcp(debEspFcp);
		
		return listaRegistroE311;
	}
	
	/**
	 * M�todo que cria registro E500
	 * 
	 * @param spedFiscal
	 * @param filtro
	 * @author Tom�s Rabelo
	 */
	private void createRegistroE500(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		if(spedFiscal.getRegistro0000() != null && spedFiscal.getRegistro0000().getInd_ativ() != null && spedFiscal.getRegistro0000().getInd_ativ().equals(0)){
			List<RegistroE500> lista = new ArrayList<RegistroE500>();
			RegistroE500 registroE500 = new RegistroE500();
			
			registroE500.setReg(RegistroE500.REG);
			registroE500.setInd_apur(0); // 0-Mensal / 1-Decendial
			registroE500.setDt_ini(filtro.getDtinicio());
			registroE500.setDt_fin(filtro.getDtfim());
			
			registroE500.setListaRegistrosE510(this.createListaRegistrosE510(spedFiscal, filtro));
			registroE500.setRegistroE520(this.createRegistroE520(spedFiscal, filtro));
			
			filtro.getApoio().addRegistros(RegistroE500.REG);
			
			lista.add(registroE500);
			spedFiscal.getRegistro0000().getRegistroE001().setListaRegistroE500(lista);
		}
	}
	
	/**
	 * M�todo que cria registro E510
	 * 
	 * @param spedFiscal
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<RegistroE510> createListaRegistrosE510(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		List<RegistroE510> lista = new ArrayList<RegistroE510>();

		List<RegistroE510Apoio> listaApoio = new ArrayList<RegistroE510Apoio>();
		int idx;
		RegistroE510Apoio reg;
		Double zero = 0.0;
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null && spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
			for (RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()) {
				if(registroC100.getListaRegistroC170() != null){
					for (RegistroC170 registroC170 : registroC100.getListaRegistroC170()) {
						if(registroC170.getCfop() != null && registroC170.getCst_ipi() != null){
							reg = new RegistroE510Apoio(registroC170.getCfop(), registroC170.getCst_ipi());
							if(listaApoio.contains(reg)){
								idx = listaApoio.indexOf(reg);
								reg = listaApoio.get(idx);
								listaApoio.remove(idx);
							}
							
							reg.addValorbcipi(registroC170.getVl_bc_ipi() != null ? registroC170.getVl_bc_ipi() : zero);
							reg.addValoripi(registroC170.getVl_ipi() != null ? registroC170.getVl_ipi() : zero);
							listaApoio.add(reg);
						}
					}
				}
			}
		}
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null && spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
			for (RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()) {
				if(registroC100.getListaRegistroC190() != null){
					for (RegistroC190 registroC190 : registroC100.getListaRegistroC190()) {
						if(registroC190.getSaida() != null && registroC190.getSaida() && registroC190.getVl_ipi() != null && registroC190.getVl_ipi() > 0){
							if(registroC190.getCfop() != null && registroC190.getCst_ipi_aux() != null){
								reg = new RegistroE510Apoio(registroC190.getCfop(), registroC190.getCst_ipi_aux());
								if(listaApoio.contains(reg)){
									idx = listaApoio.indexOf(reg);
									reg = listaApoio.get(idx);
									listaApoio.remove(idx);
								}
								
								reg.addValorbcipi(registroC190.getVl_opr() != null ? registroC190.getVl_opr() : zero);
								reg.addValoripi(registroC190.getVl_ipi() != null ? registroC190.getVl_ipi() : zero);
								listaApoio.add(reg);
							}
						}
					}
				}
			}
		}
		
		if(listaApoio != null){
			for (RegistroE510Apoio registroE510Apoio : listaApoio) {
				RegistroE510 registroE510 = new RegistroE510();
				
				registroE510.setReg(RegistroE510.REG);
				registroE510.setCfop(registroE510Apoio.getCfop());
				registroE510.setCst_ipi(registroE510Apoio.getCst_ipi());
				registroE510.setVl_cont_ipi(0.0);
				registroE510.setVl_bc_ipi(registroE510Apoio.getVl_bc_ipi());
				registroE510.setVl_ipi(registroE510Apoio.getVl_ipi());
				
				filtro.getApoio().addRegistros(RegistroE510.REG);
				
				lista.add(registroE510);
			}
		}
		
		return lista;
	}
	
	/**
	 * M�todo que cria Registro E520
	 * 
	 * @param spedFiscal
	 * @return
	 * @author Tom�s Rabelo
	 * @param filtro 
	 */
	private RegistroE520 createRegistroE520(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro) {
		RegistroE520 registroE520 = new RegistroE520();
		registroE520.setReg(RegistroE520.REG);
		registroE520.setVl_sd_ant_ipi(0.0);

		double somatorioDeb = 0.0;
		double somatorioCred = 0.0;
		
		if(spedFiscal.getRegistro0000().getRegistroC001() != null && 
				spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100() != null){
			for (RegistroC100 registroC100 : spedFiscal.getRegistro0000().getRegistroC001().getListaRegistroC100()) {
				if(registroC100.getListaRegistroC190() != null){
					for (RegistroC190 registroC190 : registroC100.getListaRegistroC190()) {
						if(registroC190.getCfop() != null && (registroC190.getCfop().charAt(0) == '5' || registroC190.getCfop().charAt(0) == '6'))
							somatorioDeb += registroC190.getVl_ipi();
						if(registroC190.getCfop() != null && (registroC190.getCfop().charAt(0) == '1' || registroC190.getCfop().charAt(0) == '2'|| registroC190.getCfop().charAt(0) == '3'))
							somatorioCred += registroC190.getVl_ipi();
					}
				}
			}
		}
		
		List<RegistroE530> listaRegistroE530 = new ArrayList<RegistroE530>();
		List<AjusteIpi> listaAjusteIpi = ajusteIpiService.findForApuracaoIpi(filtro);
		
		Money sumOdIpi = new Money(0);
		Money sumOcIpi = new Money(0);
		
		for (AjusteIpi ajusteIpi : listaAjusteIpi) {
			listaRegistroE530.add(this.createRegistroE530(spedFiscal, filtro, ajusteIpi));
			
			if (ajusteIpi.getTipoAjuste() != null) {
				if (ajusteIpi.getTipoAjuste().getValue() == 0) {
					sumOdIpi = sumOdIpi.add(ajusteIpi.getValor());
				} else if (ajusteIpi.getTipoAjuste().getValue() == 1) {
					sumOcIpi = sumOcIpi.add(ajusteIpi.getValor());
				}
			}
		}
		
		registroE520.setVl_od_ipi(sumOdIpi.getValue().doubleValue());
		registroE520.setVl_oc_ipi(sumOcIpi.getValue().doubleValue());
		registroE520.setVl_deb_ipi(somatorioDeb);
		registroE520.setVl_cred_ipi(somatorioCred);
		
		double somatorio = registroE520.getVl_deb_ipi() + registroE520.getVl_od_ipi() - (registroE520.getVl_sd_ant_ipi() + registroE520.getVl_cred_ipi() + registroE520.getVl_oc_ipi());
		
		if (somatorio < 0) {
			registroE520.setVl_sc_ipi(somatorio);
			registroE520.setVl_sd_ipi(0.0);
		} else {
			registroE520.setVl_sc_ipi(0.0);
			registroE520.setVl_sd_ipi(somatorio);
		}
		
		registroE520.setListaRegistro530(listaRegistroE530);
		
		filtro.getApoio().addRegistros(RegistroE520.REG);
		
		return registroE520;
	}
	
	private RegistroE530 createRegistroE530(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro, AjusteIpi ajusteIpi) {
//		Double valor = saldoIpiService.getLastMesAnoSaldoIpiByEmpresa(filtro.getEmpresa(), ajusteIpi.getTipoAjuste());
		
		RegistroE530 registroE530;
		registroE530 = new RegistroE530();
		registroE530.setReg(RegistroE530.REG);
		
		registroE530.setCod_aj(ajusteIpi.getCodigoAjuste() != null ? ajusteIpi.getCodigoAjuste().getCodigo() : null);
		registroE530.setDescr_aj(ajusteIpi.getDescricaoComplementar());
		registroE530.setInd_aj(ajusteIpi.getTipoAjuste() != null ? ajusteIpi.getTipoAjuste().getValue() : null);
		registroE530.setInd_doc(ajusteIpi.getOrigemDocumento() != null ? ajusteIpi.getOrigemDocumento().getValue() : null);
		registroE530.setNum_doc(ajusteIpi.getNumeroDocumento());
		registroE530.setVl_aj(ajusteIpi.getValor() != null ? ajusteIpi.getValor().getValue().doubleValue() : 0d);
		
		if (ajusteIpi.getAjusteIpiDocumentoList() != null && ajusteIpi.getAjusteIpiDocumentoList().size() > 0) {
			registroE530.setListaRegistroE531(this.createRegistroE531(spedFiscal, filtro, ajusteIpi.getAjusteIpiDocumentoList()));
		}
		
//		SaldoIpi saldoIpi = new SaldoIpi();
//		saldoIpi.setMesAno(filtro.getMesano());
//		saldoIpi.setMesAnoAux(saldoIpi.getMesAnoAux());
//		saldoIpi.setEmpresa(filtro.getEmpresa());
//		saldoIpi.setTipoUtilizacao(ajusteIpi.getTipoAjuste());
		
//		SaldoIpi saldoIpiFound = saldoIpiService.getSaldoIpiByMesAnoEmpresaTipoUtilizacao(saldoIpi);
//		
//		if (saldoIpiFound == null) {
//			saldoIpi.setValor(new Money(valor));
//			saldoIpiService.saveOrUpdate(saldoIpi);
//		} else {
//			saldoIpiFound.setValor(new Money(valor));
//			saldoIpiService.saveOrUpdate(saldoIpiFound);
//		}
		
		filtro.getApoio().addRegistros(RegistroE530.REG);
		
		return registroE530;
	}
	
	private List<RegistroE531> createRegistroE531(SPEDFiscal spedFiscal, SpedarquivoFiltro filtro, List<AjusteIpiDocumento> listaAjusteIpiDocumento) {
		List<RegistroE531> listaRegistroE531 = new ArrayList<RegistroE531>();
		RegistroE531 registroE531;
		
		for (AjusteIpiDocumento ajusteIpiDocumento : listaAjusteIpiDocumento) {
			registroE531 = new RegistroE531();
			registroE531.setReg(RegistroE531.REG);
			
			registroE531.setChv_nfe(ajusteIpiDocumento.getChaveAcesso());
			registroE531.setCod_item(filtro.getApoio().addProdutoServico(ajusteIpiDocumento.getMaterial().getCdmaterial(), ajusteIpiDocumento.getMaterial().getIdentificacaoOuCdmaterial()));
			registroE531.setCod_mod(ajusteIpiDocumento.getModeloDocumento() != null ? ajusteIpiDocumento.getModeloDocumento().getValue() : null);
			registroE531.setDt_doc(ajusteIpiDocumento.getDtEmissao());
			registroE531.setNum_doc(ajusteIpiDocumento.getNumero());
			
			String codPart = null;
			
			if (ajusteIpiDocumento.getFornecedor() != null) {
				codPart = filtro.getApoio().addParticipante(ajusteIpiDocumento.getFornecedor().getCdpessoa(), ajusteIpiDocumento.getFornecedor().getEndereco() != null ? ajusteIpiDocumento.getFornecedor().getEndereco().getCdendereco() : null);
			} else if (ajusteIpiDocumento.getCliente() != null) {
				codPart = filtro.getApoio().addParticipante(ajusteIpiDocumento.getCliente().getCdpessoa(), ajusteIpiDocumento.getCliente().getEndereco() != null ? ajusteIpiDocumento.getCliente().getEndereco().getCdendereco() : null);
			}
			
			registroE531.setCod_part(codPart);
			
			if (ModeloDocumentoFiscalEnum.NFE.equals(ajusteIpiDocumento.getModeloDocumento()) && StringUtils.isEmpty(ajusteIpiDocumento.getSerie())) {
				registroE531.setSer("000");
			} else {
				registroE531.setSer(ajusteIpiDocumento.getSerie());
			}
			
			registroE531.setSub(ajusteIpiDocumento.getSubserie());
			registroE531.setVl_aj_item(ajusteIpiDocumento.getValor() != null ? ajusteIpiDocumento.getValor().getValue().doubleValue() : null);
			
			listaRegistroE531.add(registroE531);
			
			filtro.getApoio().addRegistros(RegistroE531.REG);
		}
		
		
		return listaRegistroE531;
	}
	
	public void criarAvisoSped(Motivoaviso m, Date data) {
		List<Empresa> empresaList = empresaService.findAtivos();
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		List<Spedarquivo> spedArquivoList = null;
		
		for (Empresa e : empresaList) {
			if (e.getGeracaospedicmsipi() != null && e.getGeracaospedicmsipi() && e.getDiageracaospedicmsipi() != null) {
				spedArquivoList = spedarquivoDAO.getSpedMesAnterior(e);
				if (spedArquivoList != null && spedArquivoList.size() == 0 && SinedDateUtils.getDia(SinedDateUtils.currentDate()) == e.getDiageracaospedicmsipi()) {
					Aviso aviso = new Aviso("SPED Fiscal n�o emitido", "Nome da empresa: " + e.getNome(), m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), 
							e.getCdpessoa(), e, SinedDateUtils.currentDate(), m, Boolean.FALSE);
					Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, e.getCdpessoa());
					
					if (lastAvisoDate != null) {
						avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, e.getCdpessoa(), lastAvisoDate);
						
						List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
						
						if (SinedUtil.isListNotEmpty(listaUsuario)) {
							avisoService.salvarAvisos(aviso, listaUsuario, false);
						}
					} else {
						avisoList.add(aviso);
					}
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}
	
	private ContaContabil getContagerencialSped(SPEDFiscalApoio apoio, Naturezaoperacao naturezaoperacao, ContaContabil contagerencialContabilSPED) {
		if(naturezaoperacao != null){
			if(!apoio.getMapNaturezaContacontabil().containsKey(naturezaoperacao.getCdnaturezaoperacao())){
				Naturezaoperacao naturezaoperacao2 = naturezaoperacaoService.loadForSped(naturezaoperacao);
				ContaContabil contacontabil = null;
				if(naturezaoperacao2 != null && naturezaoperacao2.getOperacaocontabilpagamento() != null){
					contacontabil = getContaContabilSped(naturezaoperacao2.getOperacaocontabilpagamento().getListaCredito());
					if(contacontabil == null){
						contacontabil = getContaContabilSped(naturezaoperacao2.getOperacaocontabilpagamento().getListaDebito());
					}
				}
				apoio.getMapNaturezaContacontabil().put(naturezaoperacao.getCdnaturezaoperacao(), contacontabil);
			}
			
			ContaContabil contacontabil = apoio.getMapNaturezaContacontabil().get(naturezaoperacao.getCdnaturezaoperacao());
			if(contacontabil != null){
				return contacontabil;
			}
		}
		
		return contagerencialContabilSPED;
	}
	
	private ContaContabil getContaContabilSped(List<Operacaocontabildebitocredito> lista) {
		if(SinedUtil.isListNotEmpty(lista)){
			for(Operacaocontabildebitocredito item : lista){
				if(Boolean.TRUE.equals(item.getSped())) return item.getContaContabil();
			}
		}
		return null;
	}	
	
	private ContaContabil getContaContabilCustoEmpresa(Map<String, ContaContabil> mapaEmpresaMaterialContaContabil, Material material, Empresa empresa) {
		String chave = empresa.getCdpessoa() + "-" + material.getCdmaterial();
		
		if(mapaEmpresaMaterialContaContabil.containsKey(chave)){
			return mapaEmpresaMaterialContaContabil.get(chave);
		}else{			
			MaterialCustoEmpresa materialCustoEmpresa = materialCustoEmpresaService.loadForSpedPisCofins(material, empresa);
			
			if(materialCustoEmpresa != null && materialCustoEmpresa.getContaContabil() != null){
				mapaEmpresaMaterialContaContabil.put(chave, materialCustoEmpresa.getContaContabil());
				return materialCustoEmpresa.getContaContabil();
			}
		}		
		return null;
	}
	
	private List<RegistroK210> createListaRegistroK210(SpedarquivoFiltro filtro) {
		List<RegistroK210> listaRegistroK210 = new ArrayList<RegistroK210>();
		
		HashMap<Material, Double> mapaMaterialQuantidade = new HashMap<Material, Double>();
		
		String whereIn = "";
		if(SinedUtil.isListNotEmpty(filtro.getListaEntregamaterialKit())){
			for (Entregamaterial entregamaterial : filtro.getListaEntregamaterialKit()) {
				whereIn = whereIn + entregamaterial.getMaterial().getCdmaterial() + ", ";
			}
			whereIn = whereIn.substring(0, whereIn.length()-2);

			List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findForRegistroK210(filtro, whereIn); 
			if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
				for (Entregamaterial entregamaterial : filtro.getListaEntregamaterialKit()) {

					if(mapaMaterialQuantidade.containsKey(entregamaterial.getMaterial())){
						Double quantidade = mapaMaterialQuantidade.get(entregamaterial.getMaterial());
						quantidade += entregamaterial.getQtde();
						mapaMaterialQuantidade.put(entregamaterial.getMaterial(), quantidade);
					}else {
						mapaMaterialQuantidade.put(entregamaterial.getMaterial(), entregamaterial.getQtde());
					}
				}
				
				for (Map.Entry<Material, Double> entry : mapaMaterialQuantidade.entrySet()) {
					RegistroK210 registroK210 = new RegistroK210();
					registroK210.setReg(RegistroK210.REG);
					
					Material material = entry.getKey();
					registroK210.setCod_item_ori(filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial()));
				   
					registroK210.setQtd_ori(entry.getValue());

					registroK210.setListaRegistroK215(this.createListaRegistroK215(material.getCdmaterial(), listaMovimentacaoestoque, filtro));
					
					listaRegistroK210.add(registroK210);
					
					filtro.getApoio().addRegistros(RegistroK210.REG);	
				}				
			}
		}
		
		return listaRegistroK210;
	}
	
	private List<RegistroK215> createListaRegistroK215(Integer cdmaterial, List<Movimentacaoestoque> listaMovimentacaoestoque, SpedarquivoFiltro filtro) {
		List<RegistroK215> listaRegistroK215 = new ArrayList<RegistroK215>();
		
		HashMap<Material, Double> mapaMaterialQuantidade = new HashMap<Material, Double>();
		
		for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
			if(cdmaterial.equals(movimentacaoestoque.getMaterialOrigem().getCdmaterial())){
				if(mapaMaterialQuantidade.containsKey(movimentacaoestoque.getMaterial())){
					Double quantidade = mapaMaterialQuantidade.get(movimentacaoestoque.getMaterial());
					quantidade += movimentacaoestoque.getQtde();
					mapaMaterialQuantidade.put(movimentacaoestoque.getMaterial(), quantidade);
				}else {
					mapaMaterialQuantidade.put(movimentacaoestoque.getMaterial(), movimentacaoestoque.getQtde());
				}
				
			}
		}
		
		for (Map.Entry<Material, Double> entry : mapaMaterialQuantidade.entrySet()) {
			
			RegistroK215 registroK215 = new RegistroK215();
			registroK215.setReg(RegistroK215.REG);
			
			Material material = entry.getKey();
			
			registroK215.setCod_item_des(filtro.getApoio().addProdutoServico(material.getCdmaterial(), material.getIdentificacaoOuCdmaterial()));
			registroK215.setQtd_des(entry.getValue());
			
			listaRegistroK215.add(registroK215);
			
			filtro.getApoio().addRegistros(RegistroK215.REG);
		}
		
		return listaRegistroK215;
	}

	private List<Registro1600> createRegistro1600(SpedarquivoFiltro filtro) {
		List<Registro1600> listaRegistro1600 = new ArrayList<Registro1600>();
		List<LancamentosOperacaoCartao> listaLancamentos = lancamentosOperacaoCartaoService.findForSpedFiscal1600(filtro);
		
		if(SinedUtil.isListNotEmpty(listaLancamentos)){
			for(LancamentosOperacaoCartao lanc: listaLancamentos){
				Registro1600 registro1600 = new Registro1600();
				
				registro1600.setReg(Registro1600.REG);
				
				if(Util.objects.isPersistent(lanc.getCredenciadora())){
					registro1600.setCod_part(filtro.getApoio().addParticipante(lanc.getCredenciadora().getCdpessoa()));
				}
				
				registro1600.setTot_credito(MoneyUtils.moneyToDouble(lanc.getValorCredito()));
				registro1600.setTot_debito(MoneyUtils.moneyToDouble(lanc.getValorDebito()));
				
				listaRegistro1600.add(registro1600);
				
				filtro.getApoio().addRegistros(Registro1600.REG);
			}			
		}
		
		return listaRegistro1600;
	}
}
