package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.service.GenericService;
import br.com.linkcom.sined.geral.bean.Ecom_Cliente;
import br.com.linkcom.sined.geral.dao.Ecom_ClienteDAO;

public class Ecom_ClienteService extends GenericService<Ecom_Cliente>{

	private Ecom_ClienteDAO ecom_ClienteDAO;
	private Ecom_EnderecoService ecom_EnderecoService;
	
	public void setEcom_ClienteDAO(Ecom_ClienteDAO ecom_ClienteDAO) {
		this.ecom_ClienteDAO = ecom_ClienteDAO;
	}
	public void setEcom_EnderecoService(Ecom_EnderecoService ecom_EnderecoService) {
		this.ecom_EnderecoService = ecom_EnderecoService;
	}
	
	public List<Ecom_Cliente> findForPedido(Ecom_Cliente bean){
		List<Ecom_Cliente> lista = ecom_ClienteDAO.findForPedido(bean);
		for(Ecom_Cliente cliente: lista){
			cliente.setListaEnderecos(ecom_EnderecoService.findByCliente(cliente));
		}
		return lista;
	}
}
