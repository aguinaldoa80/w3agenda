package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Referenciacalculo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ReferenciacalculoService extends GenericService<Referenciacalculo>{

	/**
	 * Find All.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Referenciacalculo> findAllForFlex(){
		return this.findAllForFlex(null);
	}	
	
	public List<Referenciacalculo> findAllForFlex(Orcamento orcamento){
		if(orcamento == null || orcamento.getCalcularhistograma()){
			return Referenciacalculo.findAll();
		} else {
			List<Referenciacalculo> lista = new ArrayList<Referenciacalculo>();
			lista.add(Referenciacalculo.HORAS);
			
			return lista;
		}
	}	
	
}
