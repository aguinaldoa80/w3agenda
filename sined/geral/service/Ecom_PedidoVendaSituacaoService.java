package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVendaSituacao;
import br.com.linkcom.sined.geral.dao.Ecom_PedidoVendaSituacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class Ecom_PedidoVendaSituacaoService extends GenericService<Ecom_PedidoVendaSituacao>{

	private Ecom_PedidoVendaSituacaoDAO ecom_PedidoVendaSituacaoDAO;
	private static Ecom_PedidoVendaSituacaoService instance;
	
	public static Ecom_PedidoVendaSituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(Ecom_PedidoVendaSituacaoService.class);
		}
		return instance;
	}
	
	public void setEcom_PedidoVendaSituacaoDAO(Ecom_PedidoVendaSituacaoDAO ecom_PedidoVendaSituacaoDAO) {
		this.ecom_PedidoVendaSituacaoDAO = ecom_PedidoVendaSituacaoDAO;
	}
	
	public List<Ecom_PedidoVendaSituacao> findByEcomPedidoVenda(Ecom_PedidoVenda bean){
		return ecom_PedidoVendaSituacaoDAO.findByEcomPedidoVenda(bean);
	}
}
