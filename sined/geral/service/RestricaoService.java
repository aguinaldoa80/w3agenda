package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Restricao;
import br.com.linkcom.sined.geral.dao.RestricaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RestricaoService extends GenericService<Restricao>{
	private RestricaoDAO restricaoDAO;
	
	public void setRestricaoDAO(RestricaoDAO restricaoDAO) {this.restricaoDAO = restricaoDAO;}
	
	/* singleton */
	private static RestricaoService instance;
	public static RestricaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RestricaoService.class);
		}
		return instance;
	}
	
	public Restricao restricaoDesbloquear(Contrato contrato){
		return restricaoDAO.restricaoDesbloquear(contrato);
	}
	
	public List<Restricao> findByCliente(Cliente cliente){
		return restricaoDAO.findByCliente(cliente);
	}
	
	/**
	 * M�todo que verifica se o cliente possui restri��es sem data de libera��o e retorna true caso existam e false cc.
	 * 
	 * @param cliente
	 * @return Boolean
	 * @autor Rafael Salvio
	 */
	public Boolean verificaRestricaoSemDtLiberacao(Cliente cliente){
		if(!Util.objects.isPersistent(cliente)){
			return Boolean.FALSE;
		}
		List<Restricao> listaRestricao = findByCliente(cliente);
		for(Restricao restricao : listaRestricao){
			if(restricao.getDtliberacao() == null)
				return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RestricaoDAO#isClienteWithRestricaoByOportunidade(String whereInOportunidade)
	 *
	 * @param whereInOportunidade
	 * @return
	 * @author Luiz Fernando
	 * @since 15/10/2013
	 */
	public Boolean isClienteWithRestricaoByOportunidade(String whereInOportunidade) {
		return restricaoDAO.isClienteWithRestricaoByOportunidade(whereInOportunidade);
	}
	
	/**
	 * M�todo centralizado para verificar a restri��o de cliente nas telas de venda, pedido e or�amento
	 *
	 * @param request
	 * @author Rodrigo Freitas
	 * @since 04/09/2014
	 */
	public void verificaRestricaoCliente(WebRequestContext request) {
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		String hasRestricao;
		String cdcliente = request.getParameter("cdcliente");
		
		if(cdcliente != null && cdcliente != "")
			hasRestricao = "var hasRestricao = '" + this.verificaRestricaoSemDtLiberacao(new Cliente(Integer.parseInt(cdcliente))).toString() + "';";
		else
			hasRestricao = "var hasRestricao = '<null>';";

		view.println(hasRestricao);
	}
	
	public Boolean VerificaClienteEmRestri�ao (Cliente cliente, Date data){
		if(!Util.objects.isPersistent(cliente)){
			return Boolean.FALSE;
		}
		List<Restricao> listaRestricao = this.findByCliente(cliente);
		for(Restricao restricao : listaRestricao){
			if(restricao.getDtrestricao()== null || ((data.after(restricao.getDtrestricao()) || data.equals(restricao.getDtrestricao())) && (data.before(restricao.getDtliberacao())))){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
}
