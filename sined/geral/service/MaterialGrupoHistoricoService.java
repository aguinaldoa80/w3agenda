package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.sined.geral.bean.MaterialGrupoHistorico;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialGrupoEnum;
import br.com.linkcom.sined.geral.dao.MaterialGrupoHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialGrupoHistoricoService extends GenericService<MaterialGrupoHistorico>{

	private MaterialGrupoHistoricoDAO materialGrupoHistoricoDao;

	public void setMaterialGrupoHistoricoDao(
			MaterialGrupoHistoricoDAO materialGrupoHistoricoDao) {
		this.materialGrupoHistoricoDao = materialGrupoHistoricoDao;
	}
	
	
	public List< MaterialGrupoHistorico> findByMaterialGrupoHistorico (Materialgrupo materialGrupo){
		return materialGrupoHistoricoDao.findByMaterialGrupoHistorico(materialGrupo);
	}
	
	public void preencherHistorico(Materialgrupo materialGrupo, MaterialGrupoEnum acao, String observacao, Boolean useTransaction,Boolean blocoH, Boolean blocoK) {
		MaterialGrupoHistorico novoHistorico = new MaterialGrupoHistorico();
		
		novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		try {
			novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {}
		if(acao == MaterialGrupoEnum.ALTERADO){
			novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			novoHistorico.setCdMaterialGrupo(materialGrupo);
			if(blocoH!=null){
				if(blocoH){
					novoHistorico.setAcao(MaterialGrupoEnum.GERARBLOCOH);
				}else{
					novoHistorico.setAcao(MaterialGrupoEnum.NAOGERARBLOCOH);
				}
			}
			if(blocoK!=null){
				if(blocoK){
					novoHistorico.setAcao(MaterialGrupoEnum.GERARBLOCOK);
				}else{
					novoHistorico.setAcao(MaterialGrupoEnum.NAOGERARBLOCOK);
				}
			}
			novoHistorico.setObservacao(observacao);
		}else{
		novoHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		novoHistorico.setCdMaterialGrupo(materialGrupo);
		novoHistorico.setAcao(acao);
		novoHistorico.setObservacao(observacao);
		}
		if(Boolean.TRUE.equals(useTransaction)){
			saveOrUpdate(novoHistorico);			
		} else {
			saveOrUpdateNoUseTransaction(novoHistorico);
		}
	}
}
