package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.exception.NotInNeoContextException;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.DoubleUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Ecom_PedidoVenda;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Envioemail;
import br.com.linkcom.sined.geral.bean.Envioemailtipo;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.Expedicaoiteminspecao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialinspecaoitem;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaStatus;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.OtrPneuTipoOtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Produto;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.TipoEventos;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.geral.dao.ExpedicaoDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.CancelamentoExpedicaoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ExpedicaoTrocaLoteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.ExpedicaoFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeLegendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoClienteItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ExpedicaoEtiquetaBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.EmailUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.TemplateManager;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

import com.ibm.icu.text.SimpleDateFormat;

public class ExpedicaoService extends GenericService<Expedicao> {

	private ExpedicaoDAO expedicaoDAO;
	private VendaService vendaService;
	private ExpedicaoitemService expedicaoitemService;
	private RegiaoService regiaoService;
	private ExpedicaoiteminspecaoService expedicaoiteminspecaoService;
	private MaterialinspecaoitemService materialinspecaoitemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MaterialService materialService;
	private MateriallegendaService materiallegendaService;
	private ParametrogeralService parametrogeralService;
	private UnidademedidaService unidademedidaService;
	private VendamaterialService vendamaterialService;
	private NotaVendaService notaVendaService;
	private ExpedicaohistoricoService expedicaohistoricoService;
	private ReservaService reservaService;
	private PedidovendatipoService pedidovendatipoService;
	private ProdutoService produtoService;
	private OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService;
	private LoteestoqueService loteestoqueService;
	private ReportTemplateService reportTemplateService;
	private EnvioemailService envioemailService;
	private EmpresaService empresaService;
	private TipoEventosService tipoEventosService;
	private Ecom_PedidoVendaService ecom_PedidoVendaService;
	
	private static ExpedicaoService instance;
	public static ExpedicaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExpedicaoService.class);
		}
		return instance;
	}
	
	public void setMateriallegendaService(
			MateriallegendaService materiallegendaService) {
		this.materiallegendaService = materiallegendaService;
	}
	public void setRegiaoService(RegiaoService regiaoService) {
		this.regiaoService = regiaoService;
	}
	public void setExpedicaoitemService(ExpedicaoitemService expedicaoitemService) {
		this.expedicaoitemService = expedicaoitemService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setExpedicaoDAO(ExpedicaoDAO expedicaoDAO) {
		this.expedicaoDAO = expedicaoDAO;
	}
	public void setExpedicaoiteminspecaoService(ExpedicaoiteminspecaoService expedicaoiteminspecaoService) {
		this.expedicaoiteminspecaoService = expedicaoiteminspecaoService;
	}
	public void setMaterialinspecaoitemService(MaterialinspecaoitemService materialinspecaoitemService) {
		this.materialinspecaoitemService = materialinspecaoitemService;
	}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setVendamaterialService(VendamaterialService vendamaterialService) {
		this.vendamaterialService = vendamaterialService;
	}
	public void setNotaVendaService(NotaVendaService notaVendaService) {
		this.notaVendaService = notaVendaService;
	}
	public void setExpedicaohistoricoService(ExpedicaohistoricoService expedicaohistoricoService) {
		this.expedicaohistoricoService = expedicaohistoricoService;
	}
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setPedidovendatipoService(
			PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	public void setProdutoService(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}
	public void setOtrPneuTipoOtrClassificacaoCodigoService(
			OtrPneuTipoOtrClassificacaoCodigoService otrPneuTipoOtrClassificacaoCodigoService) {
		this.otrPneuTipoOtrClassificacaoCodigoService = otrPneuTipoOtrClassificacaoCodigoService;
	}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {
		this.loteestoqueService = loteestoqueService;
	}	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	public void setEnvioemailService(EnvioemailService envioemailService) {
		this.envioemailService = envioemailService;
	}	
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setTipoEventosService(TipoEventosService tipoEventosService) {
		this.tipoEventosService = tipoEventosService;
	}
	public void setEcom_PedidoVendaService(Ecom_PedidoVendaService ecom_PedidoVendaService) {
		this.ecom_PedidoVendaService = ecom_PedidoVendaService;
	}
	
	@Override
	public void saveOrUpdate(final Expedicao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				boolean isFirstSave = bean.getCdexpedicao() == null ? true : false;
				
				List<Expedicaoitem> listaExpedicaoitem = bean.getListaExpedicaoitem();
				
				saveOrUpdateNoUseTransaction(bean);				
				
				if(isFirstSave){
					if(listaExpedicaoitem != null && !listaExpedicaoitem.isEmpty()){
						for (Expedicaoitem expedicaoitem : listaExpedicaoitem){ 
							expedicaoitem.setExpedicao(bean);
							expedicaoitemService.saveExpedicaoitemMaisListaInspecao(expedicaoitem);
						}
					}
				} else{
					if(bean.getExpedicaosituacao()!=null && bean.getExpedicaosituacao().equals(Expedicaosituacao.EM_ABERTO)){
						List<Expedicaoitem> listaExpedicaoitemAntigos = expedicaoitemService.findByExpedicaoForDelete(bean,listaExpedicaoitem);
						if(listaExpedicaoitemAntigos != null && !listaExpedicaoitemAntigos.isEmpty()){
							for (Expedicaoitem expedicaoitem : listaExpedicaoitemAntigos) {
								expedicaoitemService.delete(expedicaoitem);
							}
						}
					}
					
					if(listaExpedicaoitem != null && !listaExpedicaoitem.isEmpty()){
						for (Expedicaoitem expedicaoitem : listaExpedicaoitem){ 
							expedicaoitem.setExpedicao(bean);
							expedicaoitemService.saveOrUpdateNoUseTransaction(expedicaoitem);
						}
					}						
				}					
				
				return status;
			}
		});
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#updateSituacao(String whereIn, Expedicaosituacao situacao)
	 *
	 * @param whereIn
	 * @param situacao
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public void updateSituacao(String whereIn, Expedicaosituacao situacao) {
		expedicaoDAO.updateSituacao(whereIn, situacao, null);
	}
	
	public void updateSituacao(String whereIn, Expedicaosituacao situacao, Integer volumes) {
		expedicaoDAO.updateSituacao(whereIn, situacao, volumes);
	}
	
	public void updateSituacaoentrega(String whereIn, Expedicaosituacaoentrega situacao) {
		expedicaoDAO.updateSituacaoentrega(whereIn, situacao);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#findForConfirmacao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Expedicao> findForConfirmacao(String whereIn) {
		return expedicaoDAO.findForConfirmacao(whereIn);
	}
	
	public List<Expedicao> findForConfereincia(String whereIn) {
		return expedicaoDAO.findForConfereincia(whereIn);
	}
	
	/**
	 * Cria o resgitro de Movimentacaoestoque na a��o de confirma��o da expedi��o.
	 *
	 * @param listaMovimentacaoestoque
	 * @param expedicao
	 * @param expedicaoitem
	 * @param material
	 * @param qtde
	 * @param movimentacaoestoquetipo
	 * @since 16/07/2012
	 * @author Rodrigo Freitas
	 */
	public void makeMovimentacaoestoqueByExpedicao(List<Movimentacaoestoque> listaMovimentacaoestoque, Expedicao expedicao, 
			Expedicaoitem expedicaoitem, Material material, Double qtde, Movimentacaoestoquetipo movimentacaoestoquetipo, boolean isMateriaPrima) {
		
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setDtmovimentacao(expedicaoitem.getDtexpedicaoitem());
		movimentacaoestoque.setEmpresa(expedicao.getEmpresa());
		movimentacaoestoque.setMaterial(material);
		movimentacaoestoque.setMovimentacaoestoquetipo(movimentacaoestoquetipo);
		movimentacaoestoque.setQtde(qtde);
		movimentacaoestoque.setLocalarmazenagem(expedicaoitem.getLocalarmazenagem());
		movimentacaoestoque.setLoteestoque(expedicaoitem.getLoteEstoque());
		
		if(!isMateriaPrima){
			if(expedicaoitem.getLoteEstoque() != null){
				movimentacaoestoque.setLoteestoque(expedicaoitem.getLoteEstoque());
			}else if(expedicaoitem.getVendamaterial() != null){
				movimentacaoestoque.setLoteestoque(expedicaoitem.getVendamaterial().getLoteestoque());
			}
		}
		
		if(expedicaoitem.getVendamaterial() != null){
			if(expedicaoitem.getVendamaterial().getVenda() != null && expedicaoitem.getVendamaterial().getVenda().getProjeto() != null &&
					expedicaoitem.getVendamaterial().getVenda().getProjeto().getCdprojeto() != null){
				movimentacaoestoque.setProjeto(expedicaoitem.getVendamaterial().getVenda().getProjeto());
			}
		}
		
		if(expedicaoitem != null && expedicaoitem.getVendamaterial() != null){
			movimentacaoestoque.setComprimento(expedicaoitem.getVendamaterial().getComprimento());
		}
		
		if(material.getProduto() != null && material.getProduto()){
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		} else if(material.getEpi() != null && material.getEpi()){
			movimentacaoestoque.setMaterialclasse(Materialclasse.EPI);
		} else {
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		}
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setExpedicao(expedicao);
		
		if(expedicaoitem.getVendamaterial() != null){
			movimentacaoestoque.setValor(expedicaoitem.getVendamaterial().getPreco());
			
			vendaService.verificaUnidademedidaMaterialDiferenteByVenda(movimentacaoestoque, expedicaoitem.getVendamaterial(), expedicaoitem.getVendamaterial().getUnidademedida(), expedicaoitem.getQtdeexpedicao());
			movimentacaoestoqueorigem.setVenda(expedicaoitem.getVendamaterial().getVenda());	
			if(movimentacaoestoque.getLocalarmazenagem() == null)
				movimentacaoestoque.setLocalarmazenagem(expedicaoitem.getVendamaterial().getVenda().getLocalarmazenagem());
		}
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		listaMovimentacaoestoque.add(movimentacaoestoque);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao[] situacoes)
	 *
	 * @param whereIn
	 * @param situacoes
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public boolean haveExpedicaoNotInSituacao(String whereIn, Expedicaosituacao... situacoes) {
		return expedicaoDAO.haveExpedicaoNotInSituacao(whereIn, situacoes);
	}
	
	public boolean haveExpedicaoBySituacao(String whereIn, Expedicaosituacao situacoes) {
		return expedicaoDAO.haveExpedicaoBySituacao(whereIn, situacoes);
	}

	/**
	 * Preenche a expedi��o a partir das vendas passadas por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.VendaService#findForExpedicao(String vendas)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService#getTransportadoraByVendas(List<Venda> listaVenda)
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService#getEmpresaByVendas(List<Venda> listaVenda)
	 *
	 * @param expedicao
	 * @param vendas
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public void preencheExpedicaoByVenda(Expedicao expedicao, String vendas, Localarmazenagem localarmazenagem) {
		List<Venda> listaVenda = vendaService.findForExpedicao(vendas);
		
		expedicao.setExpedicaosituacao(Expedicaosituacao.EM_ABERTO);
		expedicao.setDtexpedicao(SinedDateUtils.currentDate());
		expedicao.setFromVenda(Boolean.TRUE);
		expedicao.setVendas(vendas);
		
		List<Expedicaoitem> listaExpedicaoitem = new ArrayList<Expedicaoitem>();
		Expedicaoitem expedicaoitem;
		
		Fornecedor transportadora = this.getTransportadoraByVendas(listaVenda);
		Empresa empresa = this.getEmpresaByVendas(listaVenda);
		
		expedicao.setTransportadora(transportadora);
		expedicao.setEmpresa(empresa);
		
		boolean existeVendaForConferirMateriais = false; 
		Integer count = 1;
		for (Venda venda : listaVenda) {
			if(!parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO) || (venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getCriarExpedicaoComConferencia()))){
				existeVendaForConferirMateriais = true;
			}
			if(venda.getValorfrete() != null){
				expedicao.setValorCustoFrete(venda.getValorfrete().add(expedicao.getValorCustoFrete()));
			}
			if(venda.getDtprazoentregamax() != null){
				expedicao.setDataPrevisaoEntrega(new java.sql.Date(venda.getDtprazoentregamax().getTime()));
			}
			
			vendamaterialService.verificaVendaKitDiscriminarnota(venda);
			List<Vendamaterial> listavendamaterial = venda.getListavendamaterial();
			if(listavendamaterial != null && listavendamaterial.size() > 0){
				for (Vendamaterial vendamaterial : listavendamaterial) {
					Double qtdeJaExpedida = 0d;
					if(vendamaterial.getVendamaterialMaterialmestre() != null){
						qtdeJaExpedida = this.getQtdeExpedidaVendamaterial(vendamaterial.getVendamaterialMaterialmestre(), vendamaterial.getMaterial());
					}else {
						qtdeJaExpedida = this.getQtdeExpedidaVendamaterial(vendamaterial, null);
					}
					Double qtdeRestante = vendamaterial.getQuantidade() - qtdeJaExpedida;
					
					if(qtdeRestante > 0){
						expedicaoitem = new Expedicaoitem();
						expedicaoitem.setCliente(venda.getCliente());
						expedicaoitem.setContatocliente(venda.getContato());
						expedicaoitem.setCodigovenda(venda.getCdvenda());
						expedicaoitem.setDtexpedicaoitem(SinedDateUtils.currentDate());
						expedicaoitem.setMaterial(vendamaterial.getMaterial());
						expedicaoitem.setQtdeexpedicao(qtdeRestante);
						expedicaoitem.setQtderestante(qtdeRestante);
						expedicaoitem.setQtdevendida(vendamaterial.getQuantidade());
						expedicaoitem.setVendamaterial(vendamaterial);
						expedicaoitem.setLocalarmazenagem(venda.getLocalarmazenagem());
						expedicaoitem.setContatocliente(venda.getContato());
						expedicaoitem.setLoteEstoque(vendamaterial.getLoteestoque());
						expedicaoitem.setObservacao(venda.getObservacao());
						
						if(expedicaoitem.getUnidademedida() == null && vendamaterial.getUnidademedida()!=null){
							expedicaoitem.setUnidademedida(vendamaterial.getUnidademedida());
						}
						
						if(expedicaoitem.getLocalarmazenagem() == null && localarmazenagem != null){
							expedicaoitem.setLocalarmazenagem(localarmazenagem);
						}
						
						expedicaoitem.setOrdementrega(count);
						count++;
						
						listaExpedicaoitem.add(expedicaoitem);
					}
				}
			}
		}
		expedicao.setListaExpedicaoitem(listaExpedicaoitem);
		expedicao.setConferirExpedicaoFromVenda(existeVendaForConferirMateriais);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#getQtdeExpedidaVendamaterial(Vendamaterial vendamaterial)
	 *
	 * @param vendamaterial
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public Double getQtdeExpedidaVendamaterial(Vendamaterial vendamaterial, Material material) {
		return expedicaoDAO.getQtdeExpedidaVendamaterial(vendamaterial, material);
	}
	
	/**
	 * Retorna a empresa da lista de Vendas passadas por par�metro.
	 * Caso tenha empresa diferente retorna null.
	 *
	 * @param listaVenda
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	private Empresa getEmpresaByVendas(List<Venda> listaVenda) {
		Empresa empresa = null;
		
		boolean passou = false;
		for (Venda venda : listaVenda) {
			if(empresa == null && !passou){
				empresa = venda.getEmpresa();
			} else if(passou){
				boolean empresaIgual = (empresa == null && venda.getEmpresa() == null) ||
												(empresa != null && venda.getEmpresa() != null && 
														empresa.getCdpessoa() != null && venda.getEmpresa().getCdpessoa() != null &&
														empresa.getCdpessoa().equals(venda.getEmpresa().getCdpessoa()));
				
				if(!empresaIgual){
					empresa = null;
					break;
				}
			}
			passou = true;
		}
		
		return empresa;
	}
	
	/**
	 * Retorna a transportadora da lista de Vendas passadas por par�metro.
	 * Caso tenha transportadora diferente retorna null.
	 *
	 * @param listaVenda
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	private Fornecedor getTransportadoraByVendas(List<Venda> listaVenda) {
		Fornecedor transportadora = null;
		
		boolean passou = false;
		for (Venda venda : listaVenda) {
			if(transportadora == null && !passou){
				transportadora = venda.getTerceiro();
			} else if(passou){
				boolean transportadoraIgual = (transportadora == null && venda.getTerceiro() == null) ||
												(transportadora != null && venda.getTerceiro() != null && 
														transportadora.getCdpessoa() != null && venda.getTerceiro().getCdpessoa() != null &&
														transportadora.getCdpessoa().equals(venda.getTerceiro().getCdpessoa()));
				
				if(!transportadoraIgual){
					transportadora = null;
					break;
				}
			}
			passou = true;
		}
		
		return transportadora;
	}
	
	/**
	 * Monta a observa��o com os links para as vendas
	 *
	 * @param vendas
	 * @return
	 * @since 18/07/2012
	 * @author Rodrigo Freitas
	 */
	public String makeObservacaoVenda(String vendas) {
		StringBuilder obs = new StringBuilder();
		obs.append("Criada a partir da(s) venda(s): ");
		
		String[] ids = vendas.split(",");
		for (int i = 0; i < ids.length; i++) {
			if(i != 0) obs.append(", ");
			
			obs
				.append("<a href='javascript:visualizarVenda(")
				.append(ids[i])
				.append(")'>")
				.append(ids[i])
				.append("</a>");
		}
		
		return obs.toString();
	}
	
	/**
	 * Monta a observa��o com os links para as vendas
	 *
	 * @param vendas
	 * @return
	 * @since 21/01/2014
	 * @author Filipe Santos
	 */
	public String makeObservacaoUnificao(String whereIn) {
		StringBuilder obs = new StringBuilder();
		obs.append("Expedi��es unificadas: ");
		
		String[] ids = whereIn.split(",");
		for (int i = 0; i < ids.length; i++) {
			if(i != 0) obs.append(", ");
			
			obs
				.append("<a href='javascript:visualizarExpedicao(")
				.append(ids[i])
				.append(")'>")
				.append(ids[i])
				.append("</a>");
		}
		
		return obs.toString();
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param expedicao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public String makeObservacaoExpedicao(String expedicao) {
		StringBuilder obs = new StringBuilder();
		
		String[] ids = expedicao.split(",");
		for (int i = 0; i < ids.length; i++) {
			if(i != 0) obs.append(", ");
			
			obs
			.append("<a href='javascript:visualizarExpedicao(")
			.append(ids[i])
			.append(")'>")
			.append(ids[i])
			.append("</a>");
		}
		
		return obs.toString();
	}
	
	/**
	 * Cria o relat�rio da listagem de Expedi��o
	 * 
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService#findForGerarPDF(ExpedicaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorioListagem(ExpedicaoFiltro filtro) {
		Report report = new Report("/faturamento/expedicao");
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		
		List<Expedicao> lista = this.findForGerarPDF(filtro);
		report.setDataSource(lista);
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#findForGerarPDF(ExpedicaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 23/07/2012
	 * @author Rodrigo Freitas
	 */
	private List<Expedicao> findForGerarPDF(ExpedicaoFiltro filtro) {
		return expedicaoDAO.findForGerarPDF(filtro);
	}
	
	/**
	 * Gera as etiquetas das expedi��es listadas.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ExpedicaoService#findForEtiquetas(ExpedicaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 24/07/2012
	 * @author Rodrigo Freitas
	 */
	public IReport gerarRelatorioEtiqueta(ExpedicaoFiltro filtro) {
		if(filtro.getRegiao() != null){
			filtro.setRegiao(regiaoService.loadFetch(filtro.getRegiao(), "listaRegiaolocal"));
		}
		
		List<ExpedicaoEtiquetaBean> listaBean = new ArrayList<ExpedicaoEtiquetaBean>();
		List<Expedicao> lista = this.findForEtiquetas(filtro);

		ExpedicaoEtiquetaBean bean;
		for (Expedicao expedicao : lista) {
			List<Expedicaoitem> listaExpedicaoitem = expedicao.getListaExpedicaoitem();
			if(listaExpedicaoitem != null && listaExpedicaoitem.size() > 0){
				for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
					bean = new ExpedicaoEtiquetaBean();
					if(expedicao.getEmpresa() != null) 
						bean.setNome_empresa(expedicao.getEmpresa().getRazaosocialOuNome());
					
					bean.setCodigo_venda(expedicaoitem.getCodigovenda());
					bean.setQtde(SinedUtil.descriptionDecimal(expedicaoitem.getQtdeexpedicao()));
					
					if(expedicaoitem.getCliente() != null)
						bean.setNome_cliente(expedicaoitem.getCliente().getNome());
					if(expedicaoitem.getContatocliente() != null)
						bean.setContato_cliente(expedicaoitem.getContatocliente().getNome());
					if(expedicaoitem.getEnderecocliente() != null)
						bean.setEndereco_cliente(expedicaoitem.getEnderecocliente().getLogradouroCompletoComBairro());
					
					if(expedicaoitem.getMaterial() != null){
						String identificacaoOuCdmaterial = expedicaoitem.getMaterial().getIdentificacaoOuCdmaterial();
						if(expedicaoitem.getMaterial().getMaterialmestregrade() != null){
							identificacaoOuCdmaterial = expedicaoitem.getMaterial().getMaterialmestregrade().getIdentificacaoOuCdmaterial() + " - " + identificacaoOuCdmaterial;
						}
						
						bean.setIdentificador_produto(expedicaoitem.getMaterial().getIdentificacaoOuCdmaterial());
						
						String identificacao = new String(identificacaoOuCdmaterial);
						if(identificacao == null || identificacao.trim().equals("")) identificacao = "";
						else identificacao += " - ";
						
						String dimensao = "";
						if(expedicaoitem.getVendamaterial() != null && (
								(expedicaoitem.getVendamaterial().getAltura() != null && expedicaoitem.getVendamaterial().getAltura() > 0) ||
								(expedicaoitem.getVendamaterial().getLargura() != null && expedicaoitem.getVendamaterial().getLargura() > 0) ||
								(expedicaoitem.getVendamaterial().getComprimento() != null && expedicaoitem.getVendamaterial().getComprimento() > 0)
						)){
							dimensao =  
								(expedicaoitem.getVendamaterial().getAltura() != null && expedicaoitem.getVendamaterial().getAltura() > 0 ? SinedUtil.descriptionDecimal(expedicaoitem.getVendamaterial().getAltura()) : "0") + 
								" x " +
								(expedicaoitem.getVendamaterial().getLargura() != null && expedicaoitem.getVendamaterial().getLargura() > 0 ? SinedUtil.descriptionDecimal(expedicaoitem.getVendamaterial().getLargura()) : "0") + 
								" x " +
								(expedicaoitem.getVendamaterial().getComprimento() != null && expedicaoitem.getVendamaterial().getComprimento() > 0 ? SinedUtil.descriptionDecimal(expedicaoitem.getVendamaterial().getComprimento()) : "0");
						}
						
						String nome = expedicaoitem.getMaterial().getNome();
							
						bean.setIdentificador_nome_produto(identificacao + nome);
						bean.setDimensoes(dimensao);
						
						try{
							List<Materiallegenda> listaMateriallegenda = materiallegendaService.findByMaterial(expedicaoitem.getMaterial().getCdmaterial().toString());
							if(listaMateriallegenda != null && listaMateriallegenda.size() > 0){
								DanfeLegendaBean leg = new DanfeLegendaBean();
								leg.setCodigo(expedicaoitem.getMaterial().getCdmaterial().toString());
								
								for (Materiallegenda materiallegenda : listaMateriallegenda) {
									Arquivo simbolo = ArquivoService.getInstance().loadWithContents( materiallegenda.getSimbolo()); //aqui ele da erro logomarca t� nula
									if(simbolo != null){
										Image imageSimbolo = ArquivoService.getInstance().loadAsImage(simbolo);
										if(imageSimbolo != null){
											bean.getListaImagem().add(imageSimbolo);
										}
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					listaBean.add(bean);
				}
			}
		}
		
		Report report = new Report("/faturamento/expedicaoEtiqueta" + filtro.getTipoetiqueta().getSufix());
		report.setDataSource(listaBean);
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#findForEtiquetas(ExpedicaoFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @since 24/07/2012
	 * @author Rodrigo Freitas
	 */
	private List<Expedicao> findForEtiquetas(ExpedicaoFiltro filtro) {
		return expedicaoDAO.findForEtiquetas(filtro);
	}
	
	/**
	 * Verifica se todos os itens da expedi��o est�o faturados.
	 * Se estiver fatura a expedi��o tamb�m.
	 *
	 * @param expedicao
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public void verificaSituacaoFaturada(Expedicao expedicao) {
		if(!expedicaoitemService.haveNotFaturada(expedicao)){
			this.updateSituacao(expedicao.getCdexpedicao().toString(), Expedicaosituacao.FATURADA);
		}
	}
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#haveExpedicaoNaoFaturada(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 19/02/2013
	 */
	public boolean haveExpedicaoNaoFaturada(Venda venda) {
		return expedicaoDAO.haveExpedicaoNaoFaturada(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#haveExpedicaoByVenda(Venda venda, Expedicaosituacao expedicaosituacao)
	 *
	 * @param venda
	 * @param expedicaosituacao
	 * @return
	 * @author Luiz Fernando
	 * @since 29/01/2014
	 */
	public boolean haveExpedicaoByVenda(Venda venda, Expedicaosituacao expedicaosituacao) {
		return this.haveExpedicaoByVenda(venda, null, expedicaosituacao);
	}
	public boolean haveExpedicaoByVenda(Venda venda, Integer cdExpedicaoExcecao, Expedicaosituacao expedicaosituacao) {
		return expedicaoDAO.haveExpedicaoByVenda(venda, cdExpedicaoExcecao, expedicaosituacao);
	}
	
	public boolean haveExpedicaoByVendaNotInSituacao(Venda venda, Expedicaosituacao... expedicaosituacao) {
		return expedicaoDAO.haveExpedicaoByVendaNotInSituacao(venda, null, expedicaosituacao);
	}
	
	public boolean haveExpedicaoByVendaNotInSituacao(Venda venda, Expedicao expedicaoExcecao, Expedicaosituacao... expedicaosituacao){
		return expedicaoDAO.haveExpedicaoByVendaNotInSituacao(venda, expedicaoExcecao, expedicaosituacao);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#haveExpedicaoByVendaWithMovimentacaoestoque(Expedicaoitem expedicaoitem)
	 *
	 * @param expedicaoitem
	 * @return
	 * @author Luiz Fernando
	 * @since 29/01/2014
	 */
	public boolean haveExpedicaoByVendaWithMovimentacaoestoque(Expedicaoitem expedicaoitem, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		return expedicaoDAO.haveExpedicaoByVendaWithMovimentacaoestoque(expedicaoitem, movimentacaoestoquetipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#carregaExpedicaoParaInspecao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public List<Expedicao> carregaExpedicaoParaInspecao(String whereIn) {
		return expedicaoDAO.carregaExpedicaoParaInspecao(whereIn);
	}
	
	/**
	 * M�todo que verifica a inspe��o da expedi��o se est� completa
	 *
	 * @param bean
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void verificaInspecaoCompleta(Expedicao bean) {
		boolean inspecaoCompleta = true;
		if(bean.getListaExpedicao() != null && !bean.getListaExpedicao().isEmpty()){
			for(Expedicao expedicao : bean.getListaExpedicao()){
				boolean inspecaoExpedicaoCompleta = true;
				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
					for (Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){ 
						for (Expedicaoiteminspecao inspecao : expedicaoitem.getListaInspecao()) 
							if(inspecao.getConforme() == null){
								inspecaoExpedicaoCompleta = false;
								break;
							}
						if(!inspecaoExpedicaoCompleta)
							break;
					}
				}
				expedicao.setInspecaocompleta(inspecaoExpedicaoCompleta);
				if(!inspecaoExpedicaoCompleta){
					inspecaoCompleta = inspecaoExpedicaoCompleta;
				}
			}			
		}
		bean.setInspecaocompleta(inspecaoCompleta);
	}
	
	/**
	 * M�todo que salva a inspe��o da expedi��o
	 *
	 * @param bean
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void salvaInspecao(final Expedicao bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(bean.getListaExpedicao() != null && !bean.getListaExpedicao().isEmpty()){
					for(Expedicao expedicao : bean.getListaExpedicao()){
						if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
							for (Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()) 
								for (Expedicaoiteminspecao inspecao : expedicaoitem.getListaInspecao()) 
									expedicaoiteminspecaoService.saveOrUpdateNoUseTransaction(inspecao);
						}
						if(expedicao.getInspecaocompleta())
							doUpdateInspecaoExpedicao(expedicao);
					}
				}
				
				return status;
			}
		});	
	}
	
	/**
	 * M�todo que p�repara a lista de inspe��o da expedi��o
	 *
	 * @param bean
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void preparaListaInspecao(Expedicao bean) {
		if(bean != null && bean.getListaExpedicaoitem() != null && !bean.getListaExpedicaoitem().isEmpty()){
			String whereIn = CollectionsUtil.listAndConcatenate(bean.getListaExpedicaoitem(), "material.cdmaterial", ",");
			List<Materialinspecaoitem> lista = materialinspecaoitemService.findItensDosMateriais(whereIn);
			
			if(lista != null && lista.size() > 0){
				bean.setInspecaocompleta(Boolean.FALSE);
				for (Expedicaoitem expedicaoitem : bean.getListaExpedicaoitem()) {
					List<Expedicaoiteminspecao> listaInspecao = new ArrayList<Expedicaoiteminspecao>();
					
					for (Materialinspecaoitem materialinspecaoitem : lista) 
						if(expedicaoitem.getMaterial().getCdmaterial().equals(materialinspecaoitem.getMaterial().getCdmaterial())){
							Expedicaoiteminspecao inspecao = new Expedicaoiteminspecao(expedicaoitem, materialinspecaoitem.getInspecaoitem());
							listaInspecao.add(inspecao);
						}
					
					if(listaInspecao != null && listaInspecao.size() > 0)
						expedicaoitem.setListaInspecao(listaInspecao);
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#doUpdateInspecaoExpedicao(Expedicao expedicao)
	 *
	 * @param expedicao
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public void doUpdateInspecaoExpedicao(Expedicao expedicao) {
		expedicaoDAO.doUpdateInspecaoExpedicao(expedicao);
	}
	
	/**
	 * M�todo que gera o relat�rio de inspe��o
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @param  
	 * @param filtro 
	 * @since 28/11/2013
	 */
	public IReport gerarRelatorioInspecaoExpedicao(ExpedicaoFiltro filtro, String whereIn) {
		Report report = new Report("/faturamento/inspecaoexpedicao");
		
		Report subreport1 = new Report("faturamento/sub_inspecaoexpedicao");
		Report subreport2 = new Report("faturamento/sub_inspecaoexpedicao1");
		
		report.addSubReport("SUB_INSPECAOEXPEDICAO", subreport1);
		report.addSubReport("SUB_INSPECAOEXPEDICAO1", subreport2);
		
		List<Expedicao> listaExpedicao = this.findForRelatorioInspecaoExpedicao(whereIn);
		
		Empresa empresa = null;
		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getEmpresa() != null){
					if(empresa == null){
						empresa = expedicao.getEmpresa();
					}else if(!empresa.equals(expedicao.getEmpresa())){
						empresa = null;
						break;
					}
				}
			}
		}
		
		if(empresa != null){
			filtro.setEmpresa(empresa);
		}
		report.setDataSource(listaExpedicao);
		
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#findForRelatorioInspecaoExpedicao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 * @since 28/11/2013
	 */
	public List<Expedicao> findForRelatorioInspecaoExpedicao(String whereIn) {
		return expedicaoDAO.findForRelatorioInspecaoExpedicao(whereIn);
	}
	
	/**
	 * 
	 * @param whereIn
	 * @return
	 */
	public List<Expedicao> findByUnificacao(String whereIn) {
		return expedicaoDAO.findByUnificacao(whereIn);
	}
	
	/**
	 * 
	 * @param expedicao
	 */
	public Expedicao preencheExpedicaoByUnificar(Expedicao expedicao, List<Expedicao> listaExpedicao, String whereIn) {
		
		expedicao.setCdexpedicao(null);
		expedicao.setExpedicaosituacao(Expedicaosituacao.EM_ABERTO);
		expedicao.setDtexpedicao(SinedDateUtils.currentDate());
		expedicao.setFromVenda(Boolean.FALSE);		
		
		List<Expedicaoitem> listaExpedicaoitem = new ArrayList<Expedicaoitem>();
		int ordem = 1;
		for (Expedicao expedicaoAux : listaExpedicao){
			for (Expedicaoitem expedicaoitem : expedicaoAux.getListaExpedicaoitem()) {
				expedicaoitem.setCdexpedicaoitem(null);
				Double qtdeExpedida = this.getQtdeExpedidaVendamaterialExceto(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), expedicaoAux);
				if(qtdeExpedida==null){
					qtdeExpedida = 0d;
				}
				Double qtdeRestante = 0d;
				if(expedicaoitem.getVendamaterial()!=null && expedicaoitem.getVendamaterial().getQuantidade() != null){
					qtdeRestante = expedicaoitem.getVendamaterial().getQuantidade() - qtdeExpedida;
				}
				 
				expedicaoitem.setCodigovenda(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null?
											expedicaoitem.getVendamaterial().getVenda().getCdvenda(): null);
				expedicaoitem.setQtderestante(qtdeRestante);
				expedicaoitem.setOrdementrega(ordem);
				listaExpedicaoitem.add(expedicaoitem);
				ordem++;
			}			
		}
		
		expedicao.setListaExpedicaoitem(listaExpedicaoitem);
		expedicao.setWhereIn(whereIn);
		
		return expedicao;
	}
	
	public List<Expedicao> carregaListaPorId(String whereIn){
		return expedicaoDAO.carregaListaPorId(whereIn);
	}
	
	private List<Expedicao> findForEmissaoExpedicao(String whereIn){
		return expedicaoDAO.findForEmissaoExpedicao(whereIn);
	}
	
	public LinkedList<EmitirExpedicaoBean> gerarEmissaoExpedicaoBean(String whereIn) {
		List<Expedicao> listaExpedicao = this.findForEmissaoExpedicao(whereIn);
		LinkedList<EmitirExpedicaoBean> listaRelatorio = new LinkedList<EmitirExpedicaoBean>();
		
		for(Expedicao expedicao : listaExpedicao){
			EmitirExpedicaoBean bean = new EmitirExpedicaoBean();
			
			bean.setId(expedicao.getCdexpedicao());
			bean.setData(expedicao.getDtexpedicao());
			bean.setTransportadora(expedicao.getTransportadora() != null && expedicao.getTransportadora().getNome() != null ? expedicao.getTransportadora().getNome() : "");
			bean.setObservacao(expedicao.getObservacao() != null ? expedicao.getObservacao() : "");
			bean.setListaCliente(this.gerarItensEmissaoExpedicao(expedicao.getListaExpedicaoitem()));
			
			if(bean.getListaCliente() != null && !bean.getListaCliente().isEmpty()){
				Double qtde_total_vendida = 0d;
				Double qtde_total_expedicao = 0d;
				Double peso_total_bruto = 0d;
				for(EmitirExpedicaoClienteBean clienteBean : bean.getListaCliente()){
					if(clienteBean.getQtde_total_vendida() != null)
						qtde_total_vendida += clienteBean.getQtde_total_vendida();
					if(clienteBean.getQtde_total_expedicao() != null)
						qtde_total_expedicao += clienteBean.getQtde_total_expedicao();
					if(clienteBean.getPeso_total_bruto() != null)
						peso_total_bruto += clienteBean.getPeso_total_bruto();
				}
				bean.setQtde_total_vendida(qtde_total_vendida);
				bean.setQtde_total_expedicao(qtde_total_expedicao);
				bean.setPeso_total_bruto(peso_total_bruto);
			}
			
			listaRelatorio.add(bean);
		}
		
		return listaRelatorio;
	}
	
	private LinkedList<EmitirExpedicaoClienteBean> gerarItensEmissaoExpedicao(List<Expedicaoitem> listaExpedicaoitem) {
		
		LinkedList<EmitirExpedicaoClienteBean> listaCliente = new LinkedList<EmitirExpedicaoClienteBean>();
		
		for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
			EmitirExpedicaoClienteBean clienteBean = new EmitirExpedicaoClienteBean();
			clienteBean.setCliente(expedicaoitem.getCliente() != null && expedicaoitem.getCliente().getNome() != null ? expedicaoitem.getCliente().getNome() : "");
			clienteBean.setContato(expedicaoitem.getContatocliente() != null && expedicaoitem.getContatocliente().getNome() != null ? expedicaoitem.getContatocliente().getNome() : "");
			clienteBean.setEndereco(expedicaoitem.getEnderecocliente() != null ? expedicaoitem.getEnderecocliente().getLogradouroCompletoComBairro() : "");
			clienteBean.setTelefone(expedicaoitem.getCliente() != null ? expedicaoitem.getCliente().getTelefones() : "");
			
			String observacaoVendaPedido = "";
			LinkedList<EmitirExpedicaoClienteItemBean> listaItem = new LinkedList<EmitirExpedicaoClienteItemBean>();
			if(listaCliente.contains(clienteBean)){
				EmitirExpedicaoClienteBean bean_aux = listaCliente.get(listaCliente.indexOf(clienteBean));
				
				listaItem = bean_aux.getListaItem();
				observacaoVendaPedido = bean_aux.getObservacaoVendaPedido();
			}
			
			EmitirExpedicaoClienteItemBean itBean = new EmitirExpedicaoClienteItemBean();
			
			String numeroNf = "";
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getVenda() != null &&
					expedicaoitem.getVendamaterial().getVenda().getListaNotavenda() != null){
				for (NotaVenda nv : expedicaoitem.getVendamaterial().getVenda().getListaNotavenda()) {
					if(nv.getNota() != null &&
							nv.getNota().getNumero() != null &&
							nv.getNota().getNotaStatus() != null &&
							!nv.getNota().getNotaStatus().equals(NotaStatus.CANCELADA) &&
							!nv.getNota().getNotaStatus().equals(NotaStatus.EM_ESPERA) &&
							!nv.getNota().getNotaStatus().equals(NotaStatus.NFE_CANCELANDO)){
						numeroNf += nv.getNota().getNumero() + "<BR/>";
					}
				}
			}
			itBean.setNumero_nf(numeroNf);
			
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getVenda() != null){
					itBean.setId_venda(expedicaoitem.getVendamaterial().getVenda().getCdvenda());
					if(expedicaoitem.getVendamaterial().getVenda().getPedidovenda() != null){
						itBean.setId_pedidovenda(expedicaoitem.getVendamaterial().getVenda().getPedidovenda().getCdpedidovenda());
					}
			}
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getVenda() != null &&
					expedicaoitem.getVendamaterial().getVenda().getDtvenda() != null){
				itBean.setData_venda(expedicaoitem.getVendamaterial().getVenda().getDtvenda());
			}
			if(expedicaoitem.getPneu() !=null && expedicaoitem.getColetaMaterial() != null && (expedicaoitem.getColetaMaterial().getQuantidadedevolvida() ==null || expedicaoitem.getColetaMaterial().getQuantidadedevolvida() <=0)){
				itBean.setId_pneu(expedicaoitem.getPneu().getCdpneu());
				if(expedicaoitem.getPneu().getPneumarca() !=null){
					itBean.setPneu_marca(expedicaoitem.getPneu().getPneumarca().getNome());
				}
				if(expedicaoitem.getPneu().getPneumodelo() !=null){
					itBean.setPneu_modelo(expedicaoitem.getPneu().getPneumodelo().getNome());
				}
				if(expedicaoitem.getPneu().getPneumedida() !=null){
					itBean.setPneu_medida(expedicaoitem.getPneu().getPneumedida().getNome());
				}
				if(expedicaoitem.getPneu().getSerie() !=null){
					itBean.setPneu_seriefogo(expedicaoitem.getPneu().getSerie());
				}
				if(expedicaoitem.getPneu().getDot()!=null){
					try{
						Integer valor = Integer.parseInt(expedicaoitem.getPneu().getDot());
						itBean.setPneu_dot(valor);	
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(expedicaoitem.getPneu().getNumeroreforma() !=null){
					itBean.setPneu_numeroreforma(expedicaoitem.getPneu().getNumeroreforma().getNome());
				}
				if(expedicaoitem.getPneu().getMaterialbanda() !=null){
					itBean.setPneu_banda(expedicaoitem.getPneu().getMaterialbanda().getNome());
				}
				if(expedicaoitem.getPneu().getPneuqualificacao() !=null){
					itBean.setPneu_qualificacao(expedicaoitem.getPneu().getPneuqualificacao().getNome());
				}
				if(expedicaoitem.getPneu().getDescricao() != null){
					itBean.setPneu_descricao(expedicaoitem.getPneu().getDescricao());
				}
				if(expedicaoitem.getPneu().getAcompanhaRoda() != null){
					itBean.setPneu_roda(expedicaoitem.getPneu().getAcompanhaRoda() == true ? "Sim": "N�o");
				}
				if(expedicaoitem.getPneu().getLonas() != null){
					itBean.setPneu_lonas(expedicaoitem.getPneu().getLonas().toString());
				}
				if(expedicaoitem.getPneu().getOtrPneuTipo() != null){
					String desenhos ="";
					String servicoTipo ="";
					
					List<String> listaOtrDesenhos = new ArrayList<String>();
					List<String> listaOtrServicoTipo = new ArrayList<String>();	
					List<OtrPneuTipoOtrClassificacaoCodigo> listaotrclassificacaocodigo = otrPneuTipoOtrClassificacaoCodigoService.findListOtrPneuTipoOtrClassificacaoCodigo(expedicaoitem.getPneu().getOtrPneuTipo());
					
					if(listaotrclassificacaocodigo != null && !listaotrclassificacaocodigo.isEmpty()){
						for(OtrPneuTipoOtrClassificacaoCodigo otrPneuTipoOtrClassificacaoCodigo: listaotrclassificacaocodigo){
							if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho() != null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho() != null){	
								if(!listaOtrDesenhos.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho())) {
									if(listaOtrDesenhos.size() > 0) desenhos+= " | ";
									desenhos+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho();
									listaOtrDesenhos.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho());		
								}
							}
							if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo()!= null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico() != null){
								if(!listaOtrServicoTipo.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico())){
									if(listaOtrServicoTipo.size() > 0) servicoTipo+=" | ";
									servicoTipo+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico();
									listaOtrServicoTipo.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico());
								}
							}
						}				
						itBean.setOtrPneuDesenho(desenhos);
						itBean.setOtrPneuTipoServico(servicoTipo);
					}
					
				}
				
			} else if (expedicaoitem.getPneu() !=null){
				itBean.setId_pneu_recusado(expedicaoitem.getPneu().getCdpneu());
				if(expedicaoitem.getPneu().getPneumarca() !=null){
					itBean.setPneu_marca_recusado(expedicaoitem.getPneu().getPneumarca().getNome());
				}
				if(expedicaoitem.getPneu().getPneumodelo() !=null){
					itBean.setPneu_modelo_recusado(expedicaoitem.getPneu().getPneumodelo().getNome());
				}
				if(expedicaoitem.getPneu().getPneumedida() !=null){
					itBean.setPneu_medida_recusado(expedicaoitem.getPneu().getPneumedida().getNome());
				}
				if(expedicaoitem.getPneu().getSerie() !=null){
					itBean.setPneu_seriefogo_recusado(expedicaoitem.getPneu().getSerie());
				}
				if(expedicaoitem.getPneu().getDot()!=null){
					try{
						Integer valor = Integer.parseInt(expedicaoitem.getPneu().getDot());
						itBean.setPneu_dot_recusado(valor);	
					}catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(expedicaoitem.getPneu().getNumeroreforma() !=null){
					itBean.setPneu_numeroreforma_recusado(expedicaoitem.getPneu().getNumeroreforma().getNome());
				}
				if(expedicaoitem.getPneu().getMaterialbanda() !=null){
					itBean.setPneu_banda_recusado(expedicaoitem.getPneu().getMaterialbanda().getNome());
				}
				if(expedicaoitem.getPneu().getPneuqualificacao() !=null){
					itBean.setPneu_qualificacao_recusado(expedicaoitem.getPneu().getPneuqualificacao().getNome());
				}
				if(expedicaoitem.getPneu().getDescricao() != null){
					itBean.setPneu_descricao_recusado(expedicaoitem.getPneu().getDescricao());
				}
				if(expedicaoitem.getPneu().getAcompanhaRoda() != null){
					itBean.setPneu_roda_recusado(expedicaoitem.getPneu().getAcompanhaRoda() == true ? "Sim": "N�o");
				}
				if(expedicaoitem.getPneu().getLonas() != null){
					itBean.setPneu_lonas_recusado(expedicaoitem.getPneu().getLonas().toString());
				}
				if(expedicaoitem.getPneu().getOtrPneuTipo() != null){
					String desenhos ="";
					String servicoTipo ="";
					
					List<String> listaOtrDesenhos = new ArrayList<String>();
					List<String> listaOtrServicoTipo = new ArrayList<String>();	
					List<OtrPneuTipoOtrClassificacaoCodigo> listaotrclassificacaocodigo = otrPneuTipoOtrClassificacaoCodigoService.findListOtrPneuTipoOtrClassificacaoCodigo(expedicaoitem.getPneu().getOtrPneuTipo());
					
					if(listaotrclassificacaocodigo != null && !listaotrclassificacaocodigo.isEmpty()){
						for(OtrPneuTipoOtrClassificacaoCodigo otrPneuTipoOtrClassificacaoCodigo: listaotrclassificacaocodigo){
							if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho() != null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho() != null){	
								if(!listaOtrDesenhos.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho())) {
									if(listaOtrDesenhos.size() > 0) desenhos+= " | ";
									desenhos+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho();
									listaOtrDesenhos.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrDesenho().getDesenho());		
								}
							}
							if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo()!= null && otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico() != null){
								if(!listaOtrServicoTipo.contains(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico())){
									if(listaOtrServicoTipo.size() > 0) servicoTipo+=" | ";
									servicoTipo+= otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico();
									listaOtrServicoTipo.add(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo().getOtrServicoTipo().getTipoServico());
								}
							}
						}				
						itBean.setOtrPneuDesenho_recusado(desenhos);
						itBean.setOtrPneuTipoServico_recusado(servicoTipo);
					}
					
				}
			}

			if(expedicaoitem.getMaterial() != null){
				String material = (expedicaoitem.getMaterial().getIdentificacao() != null && !expedicaoitem.getMaterial().getIdentificacao().equals("") ? (expedicaoitem.getMaterial().getIdentificacao() + " - ") : "");
				material += expedicaoitem.getMaterial().getNome();
				itBean.setMaterial(material);
			}
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getQuantidade() != null){
				itBean.setQtde_vendida(expedicaoitem.getVendamaterial().getQuantidade());
			}
			
			if (expedicaoitem.getLoteEstoque() != null) {
				itBean.setLote(expedicaoitem.getLoteEstoque().getNumerovalidade());
			}
			
			if(expedicaoitem.getOrdementrega()!=null){
				itBean.setOrdementrega(expedicaoitem.getOrdementrega());
			}
			
			if (expedicaoitem.getMaterial() != null && expedicaoitem.getMaterial().getMaterialmestregrade() != null) {
				itBean.setMaterial_codigo_mestre_da_grade(expedicaoitem.getMaterial().getMaterialmestregrade().getCdmaterial());
				itBean.setMaterial_nome_mestre_da_grade(expedicaoitem.getMaterial().getMaterialmestregrade().getNome());
			}
			
			if (expedicaoitem.getMaterial() != null) {
				Produto produto = produtoService.carregaProduto(expedicaoitem.getMaterial());
						
				if (produto != null && produto.getAltura() != null) {
					itBean.setMaterial_altura(produto.getAltura());
				}
				
				if (produto != null && produto.getLargura() != null) {
					itBean.setMaterial_largura(produto.getLargura());
				}
				
				if (produto != null && produto.getComprimento() != null) {
					itBean.setMaterial_comprimento(produto.getComprimento());
				}
			}
			
			itBean.setQtde_expedicao(expedicaoitem.getQtdeexpedicao());
			if(expedicaoitem.getMaterial() != null &&
					expedicaoitem.getMaterial().getPesobruto() != null &&
					expedicaoitem.getQtdeexpedicao() != null){
				
				Double qtdeexpedicao = expedicaoitem.getQtdeexpedicao();
				if(expedicaoitem.getVendamaterial() != null){
					Double fatorconversaoQtdereferencia = expedicaoitem.getVendamaterial().getFatorconversaoQtdereferencia();
					qtdeexpedicao = qtdeexpedicao/fatorconversaoQtdereferencia;
				} else {
					if(expedicaoitem.getUnidademedida() != null &&
							expedicaoitem.getMaterial() != null &&
							expedicaoitem.getMaterial().getUnidademedida() != null &&
							expedicaoitem.getMaterial().getListaMaterialunidademedida() != null && 
							expedicaoitem.getMaterial().getListaMaterialunidademedida().size() > 0 && 
							!expedicaoitem.getUnidademedida().equals(expedicaoitem.getMaterial().getUnidademedida())){
						for (Materialunidademedida materialunidademedida : expedicaoitem.getMaterial().getListaMaterialunidademedida()) {
							if(materialunidademedida.getUnidademedida() != null && 
									materialunidademedida.getFracao() != null &&
									materialunidademedida.getUnidademedida().equals(expedicaoitem.getUnidademedida())){
								Double fracaoQtdereferencia = materialunidademedida.getFracaoQtdereferencia();
								qtdeexpedicao = qtdeexpedicao/fracaoQtdereferencia;
								break;
							}
						}
					}
				}
				
				
				
				itBean.setPeso_bruto(SinedUtil.roundByParametro(expedicaoitem.getMaterial().getPesobruto() * qtdeexpedicao));
			}
			
			listaItem.add(itBean);
			
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getObservacao() != null){
				observacaoVendaPedido += expedicaoitem.getVendamaterial().getObservacao() + " ";
			}
			
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getVenda() != null &&
					expedicaoitem.getVendamaterial().getVenda().getObservacao() != null){
				observacaoVendaPedido += expedicaoitem.getVendamaterial().getVenda().getObservacao() + " ";
			}
			if(expedicaoitem.getVendamaterial() != null &&
					expedicaoitem.getVendamaterial().getVenda() != null &&
					expedicaoitem.getVendamaterial().getVenda().getPedidovenda() != null &&
					expedicaoitem.getVendamaterial().getVenda().getPedidovenda().getObservacao() != null){
				observacaoVendaPedido += expedicaoitem.getVendamaterial().getVenda().getPedidovenda().getObservacao() + " ";
			}
			
			clienteBean.setObservacaoVendaPedido(observacaoVendaPedido);
			clienteBean.setListaItem(listaItem);
			
			if(!listaCliente.contains(clienteBean)){
				listaCliente.add(clienteBean);
			}
		}
		
		if(listaCliente != null && !listaCliente.isEmpty()){
			for(EmitirExpedicaoClienteBean clienteBean : listaCliente){
				Double qtde_total_vendida = 0d;
				Double qtde_total_expedicao = 0d;
				Double peso_total_bruto = 0d;
				
				if(clienteBean.getListaItem() != null && !clienteBean.getListaItem().isEmpty()){
					for(EmitirExpedicaoClienteItemBean itBean : clienteBean.getListaItem()){
						if(itBean.getQtde_vendida() != null)
							qtde_total_vendida += itBean.getQtde_vendida();
						if(itBean.getQtde_expedicao() != null)
							qtde_total_expedicao += itBean.getQtde_expedicao();
						if(itBean.getPeso_bruto() != null)
							peso_total_bruto += itBean.getPeso_bruto();
					}
				}
				clienteBean.setQtde_total_vendida(qtde_total_vendida);
				clienteBean.setQtde_total_expedicao(qtde_total_expedicao);
				clienteBean.setPeso_total_bruto(peso_total_bruto);
			}
		}
		
		return listaCliente;
	}
	
	/**
	 * 
	 * @param material
	 * @param produto
	 * @param localarmazenagem
	 * @param qtdeProducao
	 */
	public void validaQtdeEstoque(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque, Double qtdeProducao) {
		Double entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(material, localarmazenagem, empresa, null, loteestoque);
		Double saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(material, localarmazenagem, empresa, null, loteestoque);
		
		if(entrada==null)
			entrada = 0D;
		if(saida==null)
			saida = 0D;
		
		Double qtdeEstoque = SinedUtil.getQtdeEntradaSubtractSaida(entrada,saida);
		if((material.getServico()==null || !material.getServico()) && (qtdeEstoque - qtdeProducao) < 0){
			throw new SinedException("O material '" + material.getNome() + "' n�o tem quantidade suficiente para a confirma��o da expedi��o.");
		}
	}
	
	public List<Expedicaoitem> montaMaterialitemmestregrade(List<Material> listaMaterialitemgrade) {
		List<Expedicaoitem> lista = new ArrayList<Expedicaoitem>();
		
		if(listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty()){
			for(Material material : listaMaterialitemgrade){
				Expedicaoitem itemBean = new Expedicaoitem();
				itemBean.setMaterial(material);
				lista.add(itemBean);
			}
		}
		return lista;
	}
	
	public List<Expedicaoitem> getListaCorrigidaConfirmacao(List<Expedicaoitem> listaExpedicaoitem) {
		if(listaExpedicaoitem == null || listaExpedicaoitem.isEmpty())
			return listaExpedicaoitem;
		
		List<Expedicaoitem> lista = new ArrayList<Expedicaoitem>();
		HashMap<Material, Expedicaoitem> mapMaterial = new HashMap<Material, Expedicaoitem>();
		
		for(Expedicaoitem item : listaExpedicaoitem){
			if(item.getMaterial() != null){
				if(materialService.isControleMaterialgrademestre(item.getMaterial())){
					Material materialWithMestre = materialService.loadWithGrade(item.getMaterial());
					if(materialWithMestre != null && materialWithMestre.getMaterialmestregrade() != null){
						if(mapMaterial.get(materialWithMestre.getMaterialmestregrade()) == null){
							Expedicaoitem itemMestreGradeForBaixa = new Expedicaoitem(item);
							itemMestreGradeForBaixa.setMaterial(item.getMaterial().getMaterialmestregrade());
							itemMestreGradeForBaixa.setQuantidadePrincipal(item.getQuantidadePrincipal());
							if(item.getQuantidadePrincipal() != null){
								itemMestreGradeForBaixa.setQtdeexpedicao(item.getQuantidadePrincipal());
							}else if(item.getQtdeexpedicao() != null){
								itemMestreGradeForBaixa.setQtdeexpedicao(item.getQtdeexpedicao());
							}
							mapMaterial.put(materialWithMestre.getMaterialmestregrade(),itemMestreGradeForBaixa);
						}else {
							Expedicaoitem itemMestreGradeForBaixa = mapMaterial.get(materialWithMestre.getMaterialmestregrade());
							
							if(item.getQuantidadePrincipal() != null){
								if(itemMestreGradeForBaixa.getQuantidadePrincipal() != null){
									itemMestreGradeForBaixa.setQtdeexpedicao(itemMestreGradeForBaixa.getQuantidadePrincipal()+item.getQuantidadePrincipal());
								}else {
									itemMestreGradeForBaixa.setQtdeexpedicao(item.getQuantidadePrincipal());
								}
							}else if(item.getQtdeexpedicao() != null){
								if(itemMestreGradeForBaixa.getQtdeexpedicao() != null){
									itemMestreGradeForBaixa.setQtdeexpedicao(itemMestreGradeForBaixa.getQtdeexpedicao()+item.getQtdeexpedicao());
								}else {
									itemMestreGradeForBaixa.setQtdeexpedicao(item.getQtdeexpedicao());
								}
							}
						}
					}else {
						lista.add(item);
					}
				}else {
					lista.add(item);
				}
			}
		}
		
		if(mapMaterial.size() > 0){
			for(Material material : mapMaterial.keySet()){ 
				lista.add(mapMaterial.get(material));
			}
		}
		
		return lista;
	}
	
	/**
	 * 
	 * @param expedicao
	 * @return
	 */
	public Integer getOrdemEntregaByExpedicao(Expedicao expedicao) {
		return expedicaoDAO.getOrdemEntregaByExpedicao(expedicao);
	}
	
	public Boolean existExpedicaoNotConfirmada(String whereIn) {
		return expedicaoDAO.existExpedicaoNotConfirmada(whereIn);
	}
	
	public boolean mesmaMatrizExpedicao(String whereIn) {
		return expedicaoDAO.mesmaMatrizExpedicao(whereIn);
	}
	
	public List<Expedicao> findForNota(String whereInexpedicao) {
		return expedicaoDAO.findForNota(whereInexpedicao);
	}
	
	public Double verificaUnidademedidaMaterialDiferenteForNota(Notafiscalprodutoitem item, Expedicaoitem expedicaoitem, Vendamaterial vendamaterial) {
		Double fatorconversao = null;
		if("TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.NOTAFISCAL_UNIDADEPRINCIPAL))){ 
			if(item != null && expedicaoitem != null && expedicaoitem.getMaterial() != null && 
					expedicaoitem.getMaterial().getCdmaterial() != null){
				Material material = materialService.unidadeMedidaMaterial(expedicaoitem.getMaterial());
				Unidademedida un = expedicaoitem.getUnidademedida();
				
				if(un != null && material != null && material.getUnidademedida() != null && un.getCdunidademedida() != null && 
						material.getUnidademedida().getCdunidademedida() != null && 
						!un.getCdunidademedida().equals(material.getUnidademedida().getCdunidademedida())){
					Double qtde = unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), expedicaoitem.getQtdeexpedicao(), un, material, expedicaoitem.getFatorconversao(), expedicaoitem.getQtdereferencia());
					fatorconversao = unidademedidaService.getFatorconversao(material.getUnidademedida(), expedicaoitem.getQtdeexpedicao(), un, material, expedicaoitem.getFatorconversao(), expedicaoitem.getQtdereferencia());
					if(qtde != null && qtde > 0){
						item.setQtde(qtde);
						if(material.getUnidademedida() != null){
							item.setUnidademedida(material.getUnidademedida());
						}
						if(fatorconversao != null && fatorconversao > 0 && vendamaterial.getPreco() != null && vendamaterial.getPreco() > 0){
							item.setValorunitario(vendamaterial.getPreco() * fatorconversao);
						}
					}
				}
			}
			
			item.setQtde(SinedUtil.roundByParametro(item.getQtde(), 4));
			item.setValorunitario(SinedUtil.roundByParametro(item.getValorunitario(), 10));
		}
		
		return fatorconversao;
	}
	
	public boolean existVariasVendas(List<Expedicao> listaExpedicao) {
		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
			Venda venda = null;
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
						if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null && 
								expedicaoitem.getVendamaterial().getVenda().getCdvenda() != null){
							if(venda == null){
								venda = expedicaoitem.getVendamaterial().getVenda();
							}else if(!venda.getCdvenda().equals(expedicaoitem.getVendamaterial().getVenda().getCdvenda())){
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	public List<Venda> findVendasByExpedicao(Expedicao expedicao){
		List<Venda> lista = new ArrayList<Venda>();
		Expedicao bean = this.loadWithVendas(expedicao);
		if(bean != null){
			for(Expedicaoitem item: bean.getListaExpedicaoitem()){
				if(item.getVendamaterial() != null && item.getVendamaterial().getVenda() != null
					&& !lista.contains(item.getVendamaterial().getVenda())){
					lista.add(item.getVendamaterial().getVenda());
				}
			}
		}
		return lista;
	}
	
	public Expedicao loadWithVendas(Expedicao expedicao){
		return expedicaoDAO.loadWithVendas(expedicao);
	}
	
	public Venda getVendaByVendaExpedicao(List<Venda> listaVenda, Venda venda) {
		if(listaVenda == null || listaVenda.isEmpty() || venda == null || venda.getCdvenda() == null) return null;
		
		for(Venda bean : listaVenda){
			if(bean .getCdvenda() != null && venda.getCdvenda().equals(bean.getCdvenda())){
				return bean;
			}
		}
		return null;
	}
	
	public List<Venda> getVendaForExpedicaoNota(List<Expedicao> listaExpedicao) {
		StringBuilder whereInVenda = new StringBuilder();
		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
						if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null && 
								expedicaoitem.getVendamaterial().getVenda().getCdvenda() != null){
							if(!whereInVenda.toString().contains(expedicaoitem.getVendamaterial().getVenda().getCdvenda().toString())){
								if(!whereInVenda.toString().equals("")) whereInVenda.append(",");
								whereInVenda.append(expedicaoitem.getVendamaterial().getVenda().getCdvenda().toString());
							}
						}
					}
				}
			}
			
		}
		
		List<Venda> listaVenda = null;
		if(!whereInVenda.toString().equals("")){
			listaVenda = vendaService.findForCobranca(whereInVenda.toString());
		}
		return listaVenda;
	}
	
	public List<Expedicao> createListaExpedicaoForNota(List<Expedicao> listaExpedicao) {
		List<Expedicao> listaNova = new ArrayList<Expedicao>();
		List<Venda> listaVenda = new ArrayList<Venda>(); 
		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
			StringBuilder whereInVenda = new StringBuilder();
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
						if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null && 
								expedicaoitem.getVendamaterial().getVenda().getCdvenda() != null){
							if(!whereInVenda.toString().contains(expedicaoitem.getVendamaterial().getVenda().getCdvenda().toString())){
								whereInVenda.append(expedicaoitem.getVendamaterial().getVenda().getCdvenda()).append(",");
							}
						}
					}
				}
			}
			if(!whereInVenda.toString().equals("")){
				listaVenda = vendaService.findForCobranca(whereInVenda.toString().substring(0, whereInVenda.length()-1));
			}
		}
		
		if(listaExpedicao != null && !listaExpedicao.isEmpty()){
			HashMap<Venda, Expedicao> mapVendaEexpedicao = new HashMap<Venda, Expedicao>();
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getListaExpedicaoitem() != null && !expedicao.getListaExpedicaoitem().isEmpty()){
					for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
						if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null){
							if(mapVendaEexpedicao.get(expedicaoitem.getVendamaterial().getVenda()) == null){
								Expedicao beanExpedicao = new Expedicao();
								beanExpedicao.setCdexpedicao(expedicao.getCdexpedicao());
								beanExpedicao.setCliente(expedicaoitem.getCliente());
								beanExpedicao.setEmpresa(expedicao.getEmpresa());
								beanExpedicao.setObservacao(expedicao.getObservacao());
								beanExpedicao.setTransportadora(expedicao.getTransportadora());
								beanExpedicao.setVenda(getVenda(expedicaoitem.getVendamaterial().getVenda(), listaVenda));
								beanExpedicao.setExisteNotaVendaEmitida(beanExpedicao.getVenda() != null ? notaVendaService.existeNotaEmitida(beanExpedicao.getVenda()) : Boolean.FALSE);
								
								beanExpedicao.setQuantidadevolumes(expedicao.getQuantidadevolumes());
								
								mapVendaEexpedicao.put(expedicaoitem.getVendamaterial().getVenda(), beanExpedicao);
							}
							
							Expedicao beanExpedicao = mapVendaEexpedicao.get(expedicaoitem.getVendamaterial().getVenda());
							if(beanExpedicao.getListaExpedicaoitem() == null)
								beanExpedicao.setListaExpedicaoitem(new ArrayList<Expedicaoitem>());
							
							Expedicaoitem beanExpedicaoitem = new Expedicaoitem(expedicaoitem);
							beanExpedicao.getListaExpedicaoitem().add(beanExpedicaoitem);
						}
					}
				}
			}
			if(mapVendaEexpedicao.size() > 0){
				listaNova.addAll(mapVendaEexpedicao.values());
			}
		}
		return listaNova;
	}
	
	private Venda getVenda(Venda bean, List<Venda> listaVenda) {
		if(listaVenda != null && !listaVenda.isEmpty() && bean != null && bean.getCdvenda() != null){
			for(Venda venda : listaVenda){
				if(venda.getCdvenda() != null && venda.getCdvenda().equals(bean.getCdvenda()))
					return venda;
			}
		}
		return null;
	}
	
	public void updateConferenciamateriaisExpedicao(String whereIn){
		expedicaoDAO.updateConferenciamateriaisExpedicao(whereIn);
	}
	
	public boolean isNaoPossuiConferenciaMateriais(String whereIn) {
		return expedicaoDAO.isNaoPossuiConferenciaMateriais(whereIn);
	}
	
	/**
	* M�todo que verifica se existe a expedi��o n�o tem nota emitida para alterar a situa��o para 'CONFIRMADA'
	*
	* @param nota
	* @since 24/03/2017
	* @author Luiz Fernando
	*/
	public void verificaSituacaoExpedicaoAposCancelamentoNota(Nota nota) {
		if(nota != null && nota.getCdNota() != null){
			List<NotaVenda> listaNotaVenda = notaVendaService.findByNota(nota.getCdNota().toString());
			if(SinedUtil.isListNotEmpty(listaNotaVenda)){
				List<Expedicao> listaExpedicao = new ArrayList<Expedicao>();
				for (NotaVenda nv : listaNotaVenda) {
					if(nv.getExpedicao() != null && Expedicaosituacao.FATURADA.equals(nv.getExpedicao().getExpedicaosituacao()) && !listaExpedicao.contains(nv.getExpedicao())){
						listaExpedicao.add(nv.getExpedicao());
					}
				}
				if(SinedUtil.isListNotEmpty(listaExpedicao)){
					verificaSituacaoExpedicaoAposCancelamentoNota(listaExpedicao, nota.getCdNota().toString());
				}
				
			}
		}
	}
	
	/**
	* M�todo que verifica se existe a expedi��o n�o tem nota emitida para alterar a situa��o para 'CONFIRMADA'
	*
	* @param listaExpedicao
	* @since 18/01/2017
	* @author Luiz Fernando
	*/
	public void verificaSituacaoExpedicaoAposCancelamentoNota(List<Expedicao> listaExpedicao, String whereInNota) {
		if(SinedUtil.isListNotEmpty(listaExpedicao)){
			Timestamp dtaltera = new Timestamp(System.currentTimeMillis());
			Usuario usuario = SinedUtil.getUsuarioLogado();
			for(Expedicao expedicao : listaExpedicao){
				if(expedicao.getCdexpedicao() != null && !notaVendaService.existeNotaEmitida(expedicao, null)){
					updateSituacao(expedicao.getCdexpedicao().toString(), Expedicaosituacao.CONFIRMADA);
					Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
					expedicaohistorico.setExpedicao(expedicao);
					expedicaohistorico.setExpedicaoacao(Expedicaoacao.CONFIRMADA);
					expedicaohistorico.setDtaltera(dtaltera);
					expedicaohistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
					expedicaohistorico.setObservacao("Nota fiscal " + SinedUtil.makeLinkHistorico(whereInNota, "visualizaNota") + " cancelada.");
					expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
				}
				/*if(expedicao.getCdexpedicao() != null){
					Expedicao exp = this.loadForRefazerReservaNoCancelamentoDaNota(expedicao, whereInNota);
					
					if(exp != null && SinedUtil.isListNotEmpty(exp.getListaExpedicaoitem())){
						for(Expedicaoitem expedicaoitem: exp.getListaExpedicaoitem()){
							if(expedicaoitem.getVendamaterial() != null){
								Venda venda = expedicaoitem.getVendamaterial().getVenda();
								if(venda != null && venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva()) &&
									BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum())){
									reservaService.createUpdateReserva(expedicaoitem, exp, expedicaoitem.getMaterial(), expedicaoitem.getLocalarmazenagem(),
																	exp.getEmpresa(), null, expedicaoitem.getQtdeexpedicao());
								}
							}
						}
					}
				}*/
			}
		}
		
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExpedicaoDAO#getQtdeExpedidaVendamaterialExceto(Vendamaterial vendamaterial)
	 *
	 * @param vendamaterial
	 * @param expedicao
	 * @return
	 * @since 15/05/2017
	 * @author Mairon Cezar
	 */
	public Double getQtdeExpedidaVendamaterialExceto(Vendamaterial vendamaterial, Material material, Expedicao expedicaoExcecao) {
		return expedicaoDAO.getQtdeExpedidaVendamaterialExceto(vendamaterial, material, expedicaoExcecao);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 *
	 * @param whereIn
	 * @param filtro
	 * @param orderBy
	 * @param asc
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/06/2017
	 */
	public List<Expedicao> loadForListagem(String whereIn, ExpedicaoFiltro filtro, String orderBy, boolean asc) {
		return expedicaoDAO.loadForListagem(whereIn, filtro, orderBy, asc);
	}

	public void gerarReservaAoSalvar(Expedicao bean, BindException errors){
		List<Expedicaoitem> listaExpedicaoitem = bean.getListaExpedicaoitem();
		for (Expedicaoitem expedicaoitem : listaExpedicaoitem) {
			if(expedicaoitem.getVendamaterial() != null){
				Vendamaterial vm = vendamaterialService.load(expedicaoitem.getVendamaterial());
				Venda venda = vendaService.loadForGerarReserva(vm.getVenda());
				if(venda != null && venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva()) &&
						((BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) && 
						pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false)) ||
					(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum())))){
					
					//n�o remover isso, para que n�o fa�a reserva ao passar pelo validateBean
					if(errors != null){
						if(expedicaoitem.getLoteestoque() == null && materialService.obrigarLote(vm.getMaterial())){
							errors.reject("001", "O material "+expedicaoitem.getMaterial().getNome()+" n�o possui lote informado.");
						}
						continue;
					}
					
					Double quantidadeExpedicao = unidademedidaService.getQtdeUnidadePrincipal(expedicaoitem.getMaterial(),
																							expedicaoitem.getUnidademedida(),
																							expedicaoitem.getQtdeexpedicao());
					if(reservaService.existsReserva(expedicaoitem, expedicaoitem.getMaterial())){
						Reserva reserva = reservaService.loadByExpedicaoitem(expedicaoitem.getCdexpedicaoitem(), expedicaoitem.getMaterial());
						reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), reserva.getQuantidade());
						reservaService.createUpdateReserva(vm, venda, expedicaoitem.getMaterial(), expedicaoitem.getLocalarmazenagem(), bean.getEmpresa(), vm.getLoteestoque(), reserva.getQuantidade());
					}
					reservaService.desfazerReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), quantidadeExpedicao);
					reservaService.createUpdateReserva(expedicaoitem, bean, expedicaoitem.getMaterial(), expedicaoitem.getLocalarmazenagem(), bean.getEmpresa(), expedicaoitem.getLoteestoque(), quantidadeExpedicao);
				}
			}
		}
	}
	
	public void ajustaReservaByCancelamento(Venda venda, Expedicao expedicao, List<Vendamaterial> listaNaoCriarRserva){
		venda = vendaService.loadForGerarReservaNaExpedicao(venda, expedicao);
		Expedicaoacao ultimaacao = expedicaohistoricoService.ultimaAcao(expedicao, Expedicaoacao.ESTORNADA, Expedicaoacao.CONFIRMADA, Expedicaoacao.CONFERENCIA_REALIZADA, Expedicaoacao.SEPARACAO_INICIADA, Expedicaoacao.SEPARACAO_FINALIZADA);
		
		if (venda != null) {
			for(Vendamaterial vendamaterial: venda.getListavendamaterial()){
				for(Expedicaoitem expedicaoitem: vendamaterial.getListaExpedicaoitem()){
					Double quantidadeExpedicao = unidademedidaService.getQtdeUnidadePrincipal(expedicaoitem.getMaterial(),
																							expedicaoitem.getUnidademedida(),
																							expedicaoitem.getQtdeexpedicao());
					reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), quantidadeExpedicao);
					if(listaNaoCriarRserva != null && listaNaoCriarRserva.contains(vendamaterial)){
						continue;
					}
					if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva())){
						if((BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) && 
								pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false) && 
								!Expedicaoacao.ESTORNADA.equals(ultimaacao)) ||
							(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()))){
							Double quantidade = this.getQtdeAjustadaForEstornoReserva(expedicaoitem, vendamaterial);
							reservaService.createUpdateReserva(vendamaterial, venda, expedicaoitem.getMaterial(),
																venda.getLocalarmazenagem(), venda.getEmpresa(), vendamaterial.getLoteestoque(), quantidade);
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean validateReservaByCancelamento(WebRequestContext request, Venda venda, Expedicao expedicao, String idTelaSession){
		venda = vendaService.loadForGerarReservaNaExpedicao(venda, expedicao);
		Expedicaoacao ultimaacao = expedicaohistoricoService.ultimaAcao(expedicao, Expedicaoacao.ESTORNADA, Expedicaoacao.CONFIRMADA, Expedicaoacao.CONFERENCIA_REALIZADA, Expedicaoacao.SEPARACAO_FINALIZADA, Expedicaoacao.SEPARACAO_INICIADA);
		
		Object obj = request.getSession().getAttribute(idTelaSession);
		List<Vendamaterial> lista = null;
		if(obj != null){
			lista = (List<Vendamaterial>)obj;
		}else{
			lista = new ArrayList<Vendamaterial>();
		}

		if (venda != null) {
			List<CancelamentoExpedicaoItemBean> listaBean = new ArrayList<CancelamentoExpedicaoItemBean>();
			for(Vendamaterial vendamaterial: venda.getListavendamaterial()){
				for(Expedicaoitem expedicaoitem: vendamaterial.getListaExpedicaoitem()){
					if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva())){
						if((BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) && 
								pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false) && 
								!Expedicaoacao.ESTORNADA.equals(ultimaacao)) ||
							(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()))){
							
						}
						
						Double quantidade = this.getQtdeAjustadaForEstornoReserva(expedicaoitem, vendamaterial);

						addInListCancelamentoExpedicaoItem(listaBean, venda.getLocalarmazenagem(), vendamaterial.getMaterial(), vendamaterial.getUnidademedida(), vendamaterial.getLoteestoque(), vendamaterial, expedicaoitem, quantidade);
					}
				}
			}
			for(CancelamentoExpedicaoItemBean bean: listaBean){
				Double entrada = 0D;
				Double saida = 0D;
				entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(bean.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), venda.getProjeto(),
						bean.getLoteEstoque());
				if (entrada == null) {
					entrada = 0d;
				}
				saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(bean.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), venda.getProjeto(), bean.getLoteEstoque());
				if (saida == null) {
					saida = 0d;
				}
				List<Integer> listWhereInReservas = new ArrayList<Integer>();
				for(Expedicaoitem expItem: bean.getListaExpedicaoItem()){
					Reserva reserva = reservaService.loadByExpedicaoitem(expItem.getCdexpedicaoitem(), bean.getMaterial());
					if(reserva != null){
						listWhereInReservas.add(reserva.getCdreserva());
					}
				}
				String whereNotInReservas = CollectionsUtil.concatenate(listWhereInReservas, ",");
				
				Double qtdeReservada = reservaService.getQtdeReservada(bean.getMaterial(), venda.getEmpresa(), venda.getLocalarmazenagem(), bean.getLoteEstoque(), whereNotInReservas);
				
				Double qtdeDisponivel = (vendaService.getQtdeEntradaSubtractSaida(entrada, saida) - qtdeReservada);
				Unidademedida unidademedida = CollectionsUtil.isListNotEmpty(bean.getListaExpedicaoItem())? bean.getListaExpedicaoItem().get(0).getUnidademedida(): null;
				Material material = materialService.loadMaterialunidademedida(bean.getMaterial());
				Unidademedida unidadeMedidaMaterial = material != null? material.getUnidademedida(): null;
				if(Util.objects.isPersistent(unidademedida) && !unidademedida.equals(unidadeMedidaMaterial)){
					qtdeDisponivel = unidademedidaService.getQtdeConvertida(bean.getMaterial(), unidademedida, unidadeMedidaMaterial, qtdeDisponivel, false);
				}
				
				if(qtdeDisponivel.compareTo(bean.getQuantidade()) < 0){
					System.out.println("Entrou para setar popup de cancelamento");
					lista.addAll(bean.getListaVendaMaterial());
				}
				
				
			}
			/*for(Vendamaterial vendamaterial: venda.getListavendamaterial()){
				for(Expedicaoitem expedicaoitem: vendamaterial.getListaExpedicaoitem()){
					Double quantidadeExpedicao = unidademedidaService.getQtdeUnidadePrincipal(expedicaoitem.getMaterial(),
																							expedicaoitem.getUnidademedida(),
																							expedicaoitem.getQtdeexpedicao());
					//reservaService.desfazerReserva(expedicaoitem, expedicaoitem.getMaterial(), quantidadeExpedicao);
					if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva())){
						if((BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) && 
								pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false) && 
								!Expedicaoacao.ESTORNADA.equals(ultimaacao)) ||
							(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()))){
							Double quantidade = this.getQtdeAjustadaForEstornoReserva(expedicaoitem, vendamaterial);
							
							Double entrada = 0D;
							Double saida = 0D;
							entrada = movimentacaoestoqueService.getQuantidadeDeMaterialEntrada(expedicaoitem.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), venda.getProjeto(),
									vendamaterial.getLoteestoque());
							if (entrada == null) {
								entrada = 0d;
							}
							saida = movimentacaoestoqueService.getQuantidadeDeMaterialSaida(expedicaoitem.getMaterial(), venda.getLocalarmazenagem(), venda.getEmpresa(), venda.getProjeto(), vendamaterial.getLoteestoque());
							if (saida == null) {
								saida = 0d;
							}
							
							Double qtdeReservada = reservaService.getQtdeReservada(expedicaoitem.getMaterial(), venda.getEmpresa(), venda.getLocalarmazenagem(), vendamaterial.getLoteestoque(), null);
							
							Double qtdeDisponivel = (vendaService.getQtdeEntradaSubtractSaida(entrada, saida) - qtdeReservada);
							
							if(qtdeDisponivel < quantidade){
								lista.add(vendamaterial);
							}
							
							//reservaService.createUpdateReserva(vendamaterial, venda, expedicaoitem.getMaterial(),
								//								venda.getLocalarmazenagem(), venda.getEmpresa(), vendamaterial.getLoteestoque(), quantidade);
						}
					}
				}
			}*/
		}
		if(SinedUtil.isListNotEmpty(lista)){
			request.getSession().setAttribute(idTelaSession, lista);
		
			request.getSession().setAttribute("exibeMensagemVendaSemEstoque", true);
		}
		return true;
	}
	
	private CancelamentoExpedicaoItemBean addInListCancelamentoExpedicaoItem(List<CancelamentoExpedicaoItemBean> lista, Localarmazenagem localArmazenagem, Material material, Unidademedida unidadeMedida, Loteestoque loteEstoque, Vendamaterial vendamaterial, Expedicaoitem expedicaoitem, Double quantidade){
		CancelamentoExpedicaoItemBean retorno = null;
		for(CancelamentoExpedicaoItemBean bean: lista){
			if(bean.getLocalArmazenagem().equals(localArmazenagem) &&
				bean.getMaterial().equals(material) &&
				bean.getUnidadeMedida().equals(unidadeMedida) &&
				((bean.getLoteEstoque() == null && loteEstoque == null) || bean.getLoteEstoque().equals(loteEstoque))){
				retorno = bean;
				break;
			}
		}
		if(retorno == null){
			retorno = new CancelamentoExpedicaoItemBean(localArmazenagem, material, unidadeMedida, loteEstoque);
			retorno.setQuantidade(quantidade);
			lista.add(retorno);
		}else{
			retorno.setQuantidade(retorno.getQuantidade() + quantidade);
		}
		if(!retorno.getListaVendaMaterial().contains(vendamaterial)){
			retorno.getListaVendaMaterial().add(vendamaterial);
		}
		if(!retorno.getListaExpedicaoItem().contains(expedicaoitem)){
			retorno.getListaExpedicaoItem().add(expedicaoitem);
		}
		return retorno;
	}
	
	public void ajustaReservaByEstorno(Venda venda, Expedicao expedicao){
		if(!Expedicaosituacao.CONFIRMADA.equals(expedicao.getExpedicaosituacao())){
			return;
		}
		venda = vendaService.loadForGerarReservaNaExpedicao(venda, expedicao);
		for(Vendamaterial vendamaterial: venda.getListavendamaterial()){
			for(Expedicaoitem expedicaoitem: vendamaterial.getListaExpedicaoitem()){
				if(venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva())){
					if((BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()))){
						Double quantidadeExpedicao = unidademedidaService.getQtdeUnidadePrincipal(expedicaoitem.getMaterial(),
																								expedicaoitem.getUnidademedida(),
																								expedicaoitem.getQtdeexpedicao());
						if(Expedicaoacao.CANCELADA.equals(expedicao)){
							reservaService.desfazerReserva(expedicaoitem.getVendamaterial(), expedicaoitem.getMaterial(), quantidadeExpedicao);
						}
						reservaService.createUpdateReserva(expedicaoitem, expedicaoitem.getExpedicao(), expedicaoitem.getMaterial(),
															expedicaoitem.getLocalarmazenagem(), expedicaoitem.getExpedicao().getEmpresa(), expedicaoitem.getLoteestoque(), quantidadeExpedicao);
					}
					if((BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum()) &&
						pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false))){
						Double quantidade = this.getQtdeAjustadaForEstornoReserva(expedicaoitem, vendamaterial);
						reservaService.createUpdateReserva(vendamaterial, venda, expedicaoitem.getMaterial(),
															venda.getLocalarmazenagem(), venda.getEmpresa(), vendamaterial.getLoteestoque(), quantidade);
					}
				}
			}
		}
	}
	public Double getQtdeAjustadaForEstornoReserva(Expedicaoitem expedicaoitem, Vendamaterial vendamaterial){
		Double quantidade = unidademedidaService.getQtdeUnidadePrincipal(expedicaoitem.getMaterial(),
																		expedicaoitem.getUnidademedida(),
																		expedicaoitem.getQtdeexpedicao());
		if(Boolean.TRUE.equals(vendamaterial.getMaterial().getVendapromocional())){
			Double quantidadeVenda = unidademedidaService.getQtdeUnidadePrincipal(vendamaterial.getMaterial(),
																				vendamaterial.getUnidademedida(),
																				vendamaterial.getQuantidade());
			Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendamaterial.getMaterial().getCdmaterial());
			Double qtde = 0D;
			for(Materialrelacionado mr: material.getListaMaterialrelacionado()){
				if(expedicaoitem.getMaterial().equals(mr.getMaterialpromocao())){
					qtde = mr.getQuantidade() * quantidadeVenda;
					break;
				}
			}
			if(qtde > 0 && quantidade > qtde){
				return qtde;
			}
		}
		return quantidade;
	}
	
	public List<Expedicao> findReservaByExpedicoes(String whereInExpedicao){
		return expedicaoDAO.findReservaByExpedicoes(whereInExpedicao);
	}
	public Expedicao findByCdExpedicao(String id) {
		return expedicaoDAO.findByCdExpedicao(Integer.parseInt(id));
	}
	public Expedicao findByCdExpedicao(Integer id) {
		return expedicaoDAO.findByCdExpedicao(id);
	}

	public void processarListagem(ListagemResult<Expedicao> listagemResult, ExpedicaoFiltro filtro) {
		List<Expedicao> lista = listagemResult.list();
		
		if(lista != null && lista.size() > 0){
			String whereIn = CollectionsUtil.listAndConcatenate(lista, "cdexpedicao", ",");
			
			lista.removeAll(lista);
			lista.addAll(loadForListagem(whereIn, filtro, filtro.getOrderBy(), filtro.isAsc()));
		}
	}
	
	public Expedicao findByCdExpedicaoForEmail(Integer id) {
		return expedicaoDAO.findByCdExpedicaoForEmail(id);
	}
	
	public Resource gerarListagemCSV(ExpedicaoFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Expedicao> listagemResult = findForExportacao(filtro);
		processarListagem(listagemResult, filtro);
		List<Expedicao> lista = listagemResult.list();
		
		SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
		String cabecalho = "\"Id da expedi��o\";\"Id do carregamento\";\"Data\";\"Transportadora\";\"Vendas\";\"Situa��o\";\"Outros\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Expedicao bean : lista) {
			csv.append("\"" + bean.getCdexpedicao() + "\";")
				.append("\"" + (bean.getIdentificadorcarregamento() != null ? bean.getIdentificadorcarregamento() : "") + "\";")
				.append("\"" + (bean.getDtexpedicao() != null ? pattern.format(bean.getDtexpedicao()) : "") + "\";")
				.append("\"" + (bean.getTransportadora() != null ? bean.getTransportadora().getNome() : "") + "\";")
				.append("\"" + (bean.getVendasTrans() != null ? bean.getVendasTrans() : "") + "\";")
				.append("\"" + (bean.getExpedicaosituacao() != null ? bean.getExpedicaosituacao() : "") + "\";");
			
			if(bean.getInspecaocompleta() != null && bean.getInspecaocompleta()) {
				csv.append("\"Inspe��o completa\";");
			} else {
				csv.append("\"Inspe��o pendente\";");
			}
			
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "expedicao.csv", csv.toString().getBytes());
		return resource;		
	}
	/**
	 * 
	 * @param listaExpedicao
	 * @return true quando existe vinculo, falso quando n�o existe
	 */
	public boolean verificarVinculoNotaProduco(List<Expedicao> listaExpedicao) {
		if(SinedUtil.isListNotEmpty(listaExpedicao)){
			for (Expedicao expedicao : listaExpedicao) {
				if(SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
					for (Expedicaoitem item: expedicao.getListaExpedicaoitem()){
						if(item.getNotaFiscalProdutoItem() != null && item.getNotaFiscalProdutoItem().getCdnotafiscalprodutoitem() !=null){
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean isUsaSeparacaoConferenciaMateriais(){
		return parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO);
	}
	public boolean existsIdentificadorCarregamentoEmpresa(Expedicao bean, Integer idProximoCarregamento, Empresa empresa) {
		return expedicaoDAO.existsIdentificadorCarregamentoEmpresa(bean, idProximoCarregamento, empresa);
	}
	
	public List<Expedicao> findByExpedicaosituacao(String whereIn, Expedicaosituacao situacao){
		return expedicaoDAO.findByExpedicaosituacao(whereIn, situacao);
	}
	
	public List<Expedicao> findForEstornar(String whereIn){
		return expedicaoDAO.findForEstornar(whereIn);
	}
	
	public void limparQuantidadeVolume(Expedicao expedicao) {
		expedicaoDAO.limparQuantidadeVolume(expedicao);
	}
	
	public List<Expedicao> findExpedicaoSituacaoByVenda(Venda venda, Expedicaosituacao expedicaosituacao) {
		return expedicaoDAO.findExpedicaoSituacaoByVenda(venda, expedicaosituacao);
	}
	
	public List<Expedicao> findForImprimirSeparacao(String whereIn){
		List<Expedicao> lista = expedicaoDAO.findForImprimirSeparacao(whereIn);
		List<Expedicao> listaAjustada = new ArrayList<Expedicao>();
		Expedicao expedicaoNovo = null;
		for(Expedicao expedicao : lista){
			Integer cdvenda = null;
			Integer cdcliente = null;
			if(SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
				List<Expedicaoitem> listaExpedicaoitemNovo = new ArrayList<Expedicaoitem>();
				for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
					expedicaoitem.setValidadeMaterialLote(loteestoqueService.getValidadeMaterialLote(expedicaoitem.getLoteestoque(), expedicaoitem.getMaterial()));
					if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null && expedicaoitem.getVendamaterial().getVenda().getCdvenda() != null){
						if(cdvenda == null || cdvenda.equals(expedicaoitem.getVendamaterial().getVenda().getCdvenda())){
							if(cdvenda == null) {
								expedicaoNovo = criaCopiaExpedicaoRelatorio(expedicao, null);
								expedicaoNovo.setListaExpedicaoitem(listaExpedicaoitemNovo);
								listaAjustada.add(expedicaoNovo);
							}
							listaExpedicaoitemNovo.add(expedicaoitem);
						}else {
							listaExpedicaoitemNovo = new ArrayList<Expedicaoitem>();
							listaExpedicaoitemNovo.add(expedicaoitem);
							
							expedicaoNovo = criaCopiaExpedicaoRelatorio(expedicao, null);
							expedicaoNovo.setListaExpedicaoitem(listaExpedicaoitemNovo);
							listaAjustada.add(expedicaoNovo);
						}
						cdvenda = expedicaoitem.getVendamaterial().getVenda().getCdvenda();						
					}else if(expedicaoitem.getCliente() != null){
						if(cdcliente == null || cdcliente.equals(expedicaoitem.getCliente().getCdpessoa())){
							if(cdcliente == null) {
								expedicaoNovo = criaCopiaExpedicaoRelatorio(expedicao, expedicaoitem);
								expedicaoNovo.setListaExpedicaoitem(listaExpedicaoitemNovo);
								listaAjustada.add(expedicaoNovo);
							}
							listaExpedicaoitemNovo.add(expedicaoitem);
						}else {
							listaExpedicaoitemNovo = new ArrayList<Expedicaoitem>();
							listaExpedicaoitemNovo.add(expedicaoitem);
							
							expedicaoNovo = criaCopiaExpedicaoRelatorio(expedicao, expedicaoitem);
							expedicaoNovo.setListaExpedicaoitem(listaExpedicaoitemNovo);
							listaAjustada.add(expedicaoNovo);
						}
						cdcliente = expedicaoitem.getCliente().getCdpessoa();	
					}
				}
			}
		}
		
		for(Expedicao expedicao : listaAjustada){
			if(SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
				expedicao.getListaExpedicaoitem().get(expedicao.getListaExpedicaoitem().size()-1).setUltimoitem(Boolean.TRUE);
			}
		}
		return listaAjustada;
	}
	
	private Expedicao criaCopiaExpedicaoRelatorio(Expedicao expedicao, Expedicaoitem expedicaoitem) {
		Expedicao expedicaoNovo = new Expedicao(); 
		expedicaoNovo.setCdexpedicao(expedicao.getCdexpedicao());
		expedicaoNovo.setEmpresa(expedicao.getEmpresa());
		expedicaoNovo.setObservacao(expedicao.getObservacao());
		expedicaoNovo.setIdentificadorcarregamento(expedicao.getIdentificadorcarregamento());
		expedicaoNovo.setTransportadora(expedicao.getTransportadora());
		expedicaoNovo.setDtexpedicao(expedicao.getDtexpedicao());
		expedicaoNovo.setQuantidadevolumes(expedicao.getQuantidadevolumes());
		
		if(expedicaoitem != null){
			expedicaoNovo.setVenda(new Venda());
			expedicaoNovo.getVenda().setCliente(expedicaoitem.getCliente());
			expedicaoNovo.getVenda().setEndereco(expedicaoitem.getEnderecocliente());
		}
		return expedicaoNovo;
	}
	
	public boolean validaQtdeDisponivelLoteExpedicaoitem(WebRequestContext request, ExpedicaoTrocaLoteBean bean){
		Map<Loteestoque, Double> mapaLoteValor = new HashMap<Loteestoque, Double>();
		for(Expedicaoitem ei: bean.getListaExpedicaoItem()){
			if(Util.objects.isPersistent(ei.getLoteestoque())){
				if(mapaLoteValor.containsKey(ei.getLoteestoque())){
					Double qtde = mapaLoteValor.get(ei.getLoteestoque());
					mapaLoteValor.put(ei.getLoteestoque(), DoubleUtils.zeroWhenNull(ei.getQtdeexpedicao()) + qtde);
				}else{
					mapaLoteValor.put(ei.getLoteestoque(), DoubleUtils.zeroWhenNull(ei.getQtdeexpedicao()));
				}
			}
			
		}
		Expedicaoitem expedicaoItem = bean.getExpedicaoItem();
		boolean errorEstoque = false;
		for(Entry<Loteestoque, Double> entry: mapaLoteValor.entrySet()){
			Loteestoque loteEstoque = entry.getKey();
			
			Double qtdeDisponivel = loteestoqueService.buscarQtdeDisponivelMaterialLotes(expedicaoItem.getMaterial().getCdmaterial(), expedicaoItem.getLocalarmazenagem().getCdlocalarmazenagem(),
																						expedicaoItem.getEmpresa().getCdpessoa(), loteEstoque.getCdloteestoque());
			
			qtdeDisponivel += reservaService.getReservaByVendamaterialOrExpedicaoitem(expedicaoItem.getVendamaterial(), expedicaoItem, expedicaoItem.getMaterial(), loteEstoque);
			Material material = materialService.loadMaterialunidademedida(expedicaoItem.getMaterial());
			qtdeDisponivel = unidademedidaService.getQtdeConvertida(expedicaoItem.getMaterial(), expedicaoItem.getUnidademedida(), material.getUnidademedida(), qtdeDisponivel, false);
			if(DoubleUtils.zeroWhenNull(qtdeDisponivel) < entry.getValue()){
				Loteestoque le = loteestoqueService.loadWithNumeroValidade(loteEstoque, expedicaoItem.getMaterial());
				request.addError("A quantidade informada para o lote "+le.getNumero()+" � menor que a quantidade dispon�vel em estoque.");
				errorEstoque = true;
			}
		}
		
		return !errorEstoque;
	}
	

	public List<ReportTemplateBean> templatesEstiquetaDisponiveis() {
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO);
		return templates;
	}

	public Venda getVendaForReport(Expedicao expedicao){
		if(expedicao != null && SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
			for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
				if(expedicaoitem.getVendamaterial() != null) return expedicaoitem.getVendamaterial().getVenda();
				if(expedicao.getVenda() != null) return expedicao.getVenda();
			}
		}
		return null;
	}
	
	public ModelAndView ajaxTemplatesEstiquetaDisponiveis(WebRequestContext request) {
		List<ReportTemplateBean> templates = templatesEstiquetaDisponiveis();
		ModelAndView retorno = new JsonModelAndView().addObject("qtdeTemplates", templates.size());
		if (templates.size() == 1) {
			retorno.addObject("cdtemplate", templates.get(0).getCdreporttemplate());
		}
		return retorno;
	}
	
	public ModelAndView abrirSelecaoTemplateEtiquetaExpedicao(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
//		String emitidoPelaExpedicao = request.getParameter("emitidoPelaExpedicao");
		if (org.apache.commons.lang.StringUtils.isEmpty(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/faturamento/crud/Expedicao");	
			return null;
		}
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_EPEDICAO);
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("listaTemplatesEtiqueta", templates);

		return new ModelAndView("direct:relatorio/emitirEtiquetaExpedicao");
	}
	
	public ModelAndView emitirEtiquetaExpedicao(WebRequestContext request) {
		String whereIn = SinedUtil.getItensSelecionados(request);
//		String emitidoPelaExpedicao = request.getParameter("emitidoPelaExpedicao");
		String cdReportTemplate = request.getParameter("template.cdreporttemplate");
		if (org.apache.commons.lang.StringUtils.isEmpty(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request,"/faturamento/crud/Expedicao");
			return null;
		}
		return new ModelAndView("redirect:/faturamento/relatorio/EmitirEtiquetaExpedicaoReport?ACAO=gerar&selectedItens=" + whereIn + "&template.cdreporttemplate=" + cdReportTemplate);
	}
	
	public List<Expedicao> findForEtiquetaExpedicao(String whereIn) {
		return expedicaoDAO.findForEtiquetaExpedicao(whereIn);
	}
	public boolean existeVariasVendas(List<Expedicaoitem> listExpedicaoItem) {
		List<Integer> listaCdvenda = new ArrayList<Integer>();
		if(SinedUtil.isListNotEmpty(listExpedicaoItem)){
			for(Expedicaoitem expedicaoitem : listExpedicaoItem){
				if(expedicaoitem.getVendamaterial() != null && Util.objects.isPersistent(expedicaoitem.getVendamaterial().getVenda()) && 
						!listaCdvenda.contains(expedicaoitem.getVendamaterial().getVenda().getCdvenda())){
					listaCdvenda.add(expedicaoitem.getVendamaterial().getVenda().getCdvenda());
				}
			}
		}
		return listaCdvenda.size() > 1;
	}
	
	public List<Expedicao> getListaExpedicaoBySituacao(List<Expedicao> listaExpedicao, Expedicaosituacao expedicaosituacao) {
		List<Expedicao> lista = new ArrayList<Expedicao>();
		if(expedicaosituacao != null && SinedUtil.isListNotEmpty(listaExpedicao)){
			for(Expedicao expedicao : listaExpedicao){
				if(expedicaosituacao.equals(expedicao.getExpedicaosituacao())){
					lista.add(expedicao);
				}
			}
		}
		return lista;
	}
	

	public Expedicaosituacao getExpedicaoSituacaoByVenda(Venda venda) {
		Expedicaosituacao expedicaosituacao = null;
		
		if(venda != null && venda.getCdvenda() != null){
			List<Expedicao> listaExpedicao = findExpedicaoSituacaoByVenda(venda, Expedicaosituacao.CANCELADA);
			if(listaExpedicao != null && !listaExpedicao.isEmpty()){
				for(Expedicao expedicao : listaExpedicao){
					if(expedicao.getExpedicaosituacao() != null){
						if(expedicaosituacao == null){
							expedicaosituacao = expedicao.getExpedicaosituacao();
						}else if(Expedicaosituacao.EM_ABERTO.equals(expedicao.getExpedicaosituacao())){
							expedicaosituacao = expedicao.getExpedicaosituacao();
						}else if(Expedicaosituacao.EM_SEPARACAO.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CONFIRMADA) || expedicaosituacao.equals(Expedicaosituacao.CANCELADA) ||
									expedicaosituacao.equals(Expedicaosituacao.FATURADA) || expedicaosituacao.equals(Expedicaosituacao.SEPARACAO_REALIZADA) ||
									expedicaosituacao.equals(Expedicaosituacao.EM_CONFERENCIA) || expedicaosituacao.equals(Expedicaosituacao.CONFERENCIA_REALIZADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}else if(Expedicaosituacao.SEPARACAO_REALIZADA.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CONFIRMADA) || expedicaosituacao.equals(Expedicaosituacao.CANCELADA) ||
									expedicaosituacao.equals(Expedicaosituacao.FATURADA) || expedicaosituacao.equals(Expedicaosituacao.EM_CONFERENCIA) || 
									expedicaosituacao.equals(Expedicaosituacao.CONFERENCIA_REALIZADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}else if(Expedicaosituacao.EM_CONFERENCIA.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CONFIRMADA) || expedicaosituacao.equals(Expedicaosituacao.CANCELADA) ||
									expedicaosituacao.equals(Expedicaosituacao.FATURADA) || expedicaosituacao.equals(Expedicaosituacao.CONFERENCIA_REALIZADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}else if(Expedicaosituacao.CONFERENCIA_REALIZADA.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CONFIRMADA) || expedicaosituacao.equals(Expedicaosituacao.CANCELADA) ||
									expedicaosituacao.equals(Expedicaosituacao.FATURADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}else if(Expedicaosituacao.CONFIRMADA.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CANCELADA) ||expedicaosituacao.equals(Expedicaosituacao.FATURADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}else if(Expedicaosituacao.FATURADA.equals(expedicao.getExpedicaosituacao())){
							if(expedicaosituacao.equals(Expedicaosituacao.CANCELADA)){
								expedicaosituacao = expedicao.getExpedicaosituacao();
							}
						}
					}
				}
			}
		}
		
		return expedicaosituacao;
	}
	
	public JsonModelAndView enviarCodigodeRastreioPorEmail(WebRequestContext request, Expedicao expedicao){
		JsonModelAndView view = new JsonModelAndView();			
		Cliente cliente = null;
				
		for(Expedicaoitem ei: expedicao.getListaExpedicaoitem()){
			if(cliente == null){
				cliente = ei.getCliente();
			}else if(ei.getCliente() != null && !cliente.equals(ei.getCliente())){
				view.addObject("success", false);
				view.addObject("message", "A expedi��o possui clientes diferentes");
				return view;
			}
		}
		
		Empresa empresa = empresaService.loadPrincipal();
		
		/** @todo: adicionar c�digo do pedido e c�digo de rastreio*/
		String assunto = "C�digo para rastrear seu pedido "+ expedicao.getCdexpedicao() +" - "+ expedicao.getRastreamento();
		
		StringBuilder mensagem = new StringBuilder("Ol� Caro(a) ");
		mensagem.append(cliente.getNome()); 
		mensagem.append(",<br/><br/> Estamos entrando em contato para informar que seu pedido ");
		/** @todo: adicionar numero do pedido*/
		mensagem.append("<n�mero_do_pedido>");
		mensagem.append(" foi enviado.<br/><br/>Detalhes do pedido:<br/>");
		if(expedicao.getTransportadora() != null){
			mensagem.append("Transportadora: ");
			mensagem.append(expedicao.getTransportadora().getNome());
			mensagem.append("<br/>");
		}
		mensagem.append("<div style='margin-left: 32px;'>");
		for(Expedicaoitem ei: expedicao.getListaExpedicaoitem()){
			mensagem.append("<br/> - ");
			mensagem.append(ei.getMaterial().getNome());
		}
		mensagem.append("</div>");
		mensagem.append("<br/><br/>O c�digo de rastreio � ");
		/** @todo: adicionar c�digo de rastreio como hiperlink*/
		mensagem.append("<a href='"+expedicao.getUrlRastreamento()+"'>"+expedicao.getRastreamento()+"</a>");
		
		EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
		
	
		try {
			email.setFrom(empresa.getEmail());
			email.setSubject(assunto);
			email.setTo(cliente.getEmail());
			email.addHtmlText(mensagem.toString());
			email.sendMessage();			
		} catch (Exception e) {
			view.addObject("success", false);
			view.addObject("message", "Falha ao enviar email");
			return view;
		} 
		
		view.addObject("success", true);
		view.addObject("message", "Email enviado com sucesso");
		
		envioemailService.registrarEnvio(empresa.getEmail(), 
				assunto, 
				mensagem.toString(), 
				Envioemailtipo.ENVIO_RASTREAMENTO_ECOMMERCE, 
				new Pessoa(cliente.getCdpessoa()), 
				cliente.getEmail(), 
				cliente.getNome(), 
				email);
		
		Expedicaohistorico historico = new Expedicaohistorico();
		historico.setExpedicao(expedicao);
		historico.setExpedicaoacao(Expedicaoacao.RASTREIO);
		historico.setObservacao("Enviado e-mail com c�digo de rastreio para " + cliente.getEmail());
		expedicaohistoricoService.saveOrUpdate(historico);
		
		return view;				
	}

	public Venda getVendaUnica(Expedicao expedicao){
		List<Venda> listaVenda = new ArrayList<Venda>();
		if(expedicao != null && SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
			for(Expedicaoitem expedicaoitem : expedicao.getListaExpedicaoitem()){
				if(expedicaoitem.getVendamaterial() != null && expedicaoitem.getVendamaterial().getVenda() != null && 
						!listaVenda.contains(expedicaoitem.getVendamaterial().getVenda())){
					listaVenda.add(expedicaoitem.getVendamaterial().getVenda());
				}
			}
		}
		return listaVenda.size() == 1 ? listaVenda.get(0) : null;
	}
	
	public void insertExpedicaoCriadaTabelaSincronizacaoEcommerce(Venda venda) {
		expedicaoDAO.insertExpedicaoCriadaTabelaSincronizacaoEcommerce(venda);
	}	
	
	public void insertVendaEntregueTabelaSincronizacaoEcommerce(Ecom_PedidoVenda bean) {
		expedicaoDAO.insertVendaEntregueTabelaSincronizacaoEcommerce(bean);
	}

	public List<Expedicao> findForRastreamentoCorreios(Empresa empresa){
		return this.findForRastreamentoCorreios(empresa, null);
	}
	
	public List<Expedicao> findForRastreamentoCorreios(Empresa empresa, Expedicao expedicao){
		return expedicaoDAO.findForRastreamentoCorreios(empresa, expedicao);
	}
	
	public Expedicao loadForRastreio(Expedicao expedicao){
		return expedicaoDAO.loadForRastreio(expedicao);
	}
	
	public void updateEventoCorreios(Expedicao expedicao, EventosCorreios eventoCorreios, String jobName){
		if(Util.objects.isNotPersistent(expedicao) || Util.objects.isNotPersistent(eventoCorreios)){
			return;
		}
		expedicaoDAO.updateEventoCorreios(expedicao, eventoCorreios);
		if(Boolean.TRUE.equals(eventoCorreios.getEventoFinal())){
			TipoEventos tipoEventos = tipoEventosService.loadEventoFinal(eventoCorreios);
			if(tipoEventos != null && tipoEventos.getEventosFinaisCorreios() != null){
				this.updateEventosFinaisCorreios(expedicao, tipoEventos.getEventosFinaisCorreios());
				if(Boolean.TRUE.equals(tipoEventos.getEventosFinaisCorreios().getInsucesso())){
					this.updateSituacaoentrega(expedicao.getCdexpedicao().toString(), Expedicaosituacaoentrega.IMPEDIDA);
					if(Boolean.TRUE.equals(tipoEventos.getEventosFinaisCorreios().getEnviaemailinsucesso())){
						this.enviarEmailInsucessoEntrega(expedicao, tipoEventos.getEventosFinaisCorreios(), jobName);
					}
				}else if(Boolean.TRUE.equals(tipoEventos.getEventosFinaisCorreios().getFinalizadoSucesso())){
					this.updateSituacaoentrega(expedicao.getCdexpedicao().toString(), Expedicaosituacaoentrega.REALIZADA);
					List<Venda> listaVenda = this.findVendasByExpedicao(expedicao);
					if(SinedUtil.isListNotEmpty(listaVenda) && listaVenda.size() == 1){
						this.finalizarEntregue(expedicao, listaVenda.get(0));
					}
				}
			}
		}
	}
	
	public void updateEventosFinaisCorreios(Expedicao expedicao, EventosFinaisCorreios eventoFinaisCorreios){
		expedicaoDAO.updateEventoFinaisCorreios(expedicao, eventoFinaisCorreios);
	}
	
	public boolean finalizarEntregue(Expedicao expedicao, Venda venda){
		this.updateSituacaoentrega(expedicao.getCdexpedicao().toString(), Expedicaosituacaoentrega.REALIZADA);		
		
		if(Util.objects.isPersistent(venda)){
			if(vendaService.vendaPedidoVendaTipoIsEcommerce(venda) && SinedUtil.isIntegracaoEcompleto()){
				Ecom_PedidoVenda ecom_PedidoVenda = ecom_PedidoVendaService.loadByVenda(venda);
				if(ecom_PedidoVenda != null)
					this.insertVendaEntregueTabelaSincronizacaoEcommerce(ecom_PedidoVenda);	
				else
					return false;
			}		
		}
		
	
		expedicao.setExpedicaosituacaoentrega(Expedicaosituacaoentrega.REALIZADA);
		Expedicaohistorico expedicaohistorico = new Expedicaohistorico();
		expedicaohistorico.setExpedicaoacao(Expedicaoacao.FINALIZAR_ENTREGA_EXPEDICAO_ECOMMERCE);
		expedicaohistorico.setExpedicao(expedicao);
		expedicaohistorico.getExpedicao().setExpedicaosituacaoentrega(Expedicaosituacaoentrega.REALIZADA);
		expedicaohistoricoService.saveOrUpdate(expedicaohistorico);
		return true;
	}
	
	public void enviarEmailInsucessoEntrega(Expedicao expedicao, EventosFinaisCorreios eventosFinaisCorreios, String jobName){
		if(Util.strings.isEmpty(eventosFinaisCorreios.getEmail())){
			return;
		}
		expedicao = this.loadForEntrada(expedicao);
		String nomeCliente = "";
		if(SinedUtil.isListNotEmpty(expedicao.getListaExpedicaoitem())){
			Cliente cliente = expedicao.getListaExpedicaoitem().get(0).getCliente();
			if(cliente != null){
				nomeCliente = cliente.getNome();
			}
		}
		List<Venda> listaVenda = this.findVendasByExpedicao(expedicao);
		String numeroVenda = "";
		
		if(SinedUtil.isListNotEmpty(listaVenda)){
			numeroVenda = CollectionsUtil.listAndConcatenate(listaVenda, "identificadorOrCdvenda", ",");
		}
		String emailRemetente = "w3erp@w3erp.com.br";
		
		for(String email: eventosFinaisCorreios.getEmail().split(";")){
			email = email.trim();
			String assunto = "Entrega com impedimentos - Expedi��o "+expedicao.getCdexpedicao().toString();
			EmailManager emailmanager = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
			try{
				emailmanager
						.setFrom(emailRemetente)
						.setSubject(assunto)
						.setTo(email);
			} catch (Exception e) {
				e.printStackTrace();
				//listaMensagemErro.add("Erro ao criar e-mail da nota " + numero + ": " + e.getMessage());
				continue;
			}
			String urlWithContext = "";
			if(StringUtils.isNotBlank(jobName)){
				String jobNameAux = jobName.replace("Job_consulta_eventos_correios_", "").replace("_w3erp_PostgreSQLDS", "");
				urlWithContext = jobNameAux;
				if(SinedUtil.isAmbienteDesenvolvimento()){
					urlWithContext += ":8080/w3erp";
				}
			}else{
				try {
					urlWithContext = SinedUtil.getUrlWithContext();
				} catch (NotInNeoContextException e) {
					if(jobName != null){
						String jobNameAux = jobName.replace("Job_consulta_eventos_correios_", "").replace("_w3erp_PostgreSQLDS", "");
						urlWithContext = jobNameAux + "/w3erp";
					}
				}
			}
			String template = "";
			try {
				template = new TemplateManager("/WEB-INF/template/templateInsucessoEntregaExpedicao.tpl")
							.assign("numero_expedicao", expedicao.getCdexpedicao().toString())
							.assign("link", urlWithContext + "/faturamento/crud/Expedicao?ACAO=consultar&cdexpedicao=" + expedicao.getCdexpedicao())
							.assign("nome_cliente", nomeCliente)
							.assign("numero_venda", numeroVenda)
							.getTemplate();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String nomeEmpresa = "";
			if(expedicao.getEmpresa() != null){
				nomeEmpresa = expedicao.getEmpresa().getNome();
			}
			//REGISTRO DO ENVIO DE E-MAIL
			Envioemail envioemail = null;
			try{
				envioemail = envioemailService.registrarEnvio(
				emailRemetente, 
				assunto, 
				template, 
				Envioemailtipo.ENVIO_ENTREGA_COM_IMPEDIMENTOS_ECOMMERCE, 
				expedicao.getEmpresa(), 
				email, 
				nomeEmpresa,
				emailmanager);
			}catch(Exception e){
				e.printStackTrace();
			}
			
	//		ENVIO DE E-MAIL
			try{
				emailmanager.addHtmlText(template + EmailUtil.getHtmlConfirmacaoEmail(envioemail, email));
			
				emailmanager.sendMessage();
			} catch (Exception e) {
				e.printStackTrace();
				//listaMensagemErro.add("Erro no envio de e-mail da nota " + numero + ": " + e.getMessage());
				continue;
			}
		}
	}
	
	public Expedicao loadWithExpedicaoEntregaSituacao(Expedicao expedicao) {
		return expedicaoDAO.loadWithExpedicaoEntregaSituacao(expedicao);
	}
}