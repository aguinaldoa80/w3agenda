package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.geral.dao.ExametipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ExametipoService extends GenericService<Exametipo> {	
	
	private ExametipoDAO exametipoDAO;
	
	public void setExametipoDAO(ExametipoDAO exametipoDAO) {
		this.exametipoDAO = exametipoDAO;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ExametipoDAO#findExameTipoByFk(Integer) 
	 * @param cdexametipo
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Exametipo findExameTipoByPk(Integer cdexametipo){
		return exametipoDAO.findExameTipoByPk(cdexametipo);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ExametipoDAO#findAll(List<Exametipo>) 
	 * @param List<Exametipo>
	 * @return
	 * @author Orestes
	 */
	public List<Exametipo> findAll(List<Exametipo> lista){
		return exametipoDAO.findAll(lista);
	}

	
	/* singleton */
	private static ExametipoService instance;
	public static ExametipoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ExametipoService.class);
		}
		return instance;
	}
	
}
