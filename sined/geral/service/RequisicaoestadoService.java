package br.com.linkcom.sined.geral.service;

import java.util.Iterator;
import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.geral.bean.Requisicaoestado;
import br.com.linkcom.sined.geral.dao.RequisicaoestadoDAO;

public class RequisicaoestadoService extends GenericService<Requisicaoestado> {
		
	public Requisicaoestado findByCodigo(int cdcodigo) {
		return RequisicaoestadoDAO.getInstance().findByCodigo(cdcodigo);
	}
	
	public List<Requisicaoestado> findAllEstadoLiberado() {
		String rejeitados = "6,3";
		List<Requisicaoestado> lista = findAll();
		for (Iterator<Requisicaoestado> iterator = lista.iterator(); iterator.hasNext();) {
			Requisicaoestado requisicaoestado = (Requisicaoestado) iterator.next();
			if(rejeitados.contains(requisicaoestado.getCdrequisicaoestado().toString()))
				iterator.remove();
		}	
		return lista;
	}
	
	/*singleton*/
	public static RequisicaoestadoService instance;
	
	public static RequisicaoestadoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RequisicaoestadoService.class);
		}
		return instance;
	}

}
