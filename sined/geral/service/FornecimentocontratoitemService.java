package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Fornecimentocontratoitem;
import br.com.linkcom.sined.geral.dao.FornecimentocontratoitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FornecimentocontratoitemService extends GenericService<Fornecimentocontratoitem> {
	private FornecimentocontratoitemDAO fornecimentocontratoitemDAO;
	
	public void setFornecimentocontratoitemDAO(FornecimentocontratoitemDAO fornecimentocontratoitemDAO) {
		this.fornecimentocontratoitemDAO = fornecimentocontratoitemDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author Thiers Euller
	 */
	public List<Fornecimentocontratoitem> findForCSV(String whereIn){
		return fornecimentocontratoitemDAO.findForCSV(whereIn);
	}
}
