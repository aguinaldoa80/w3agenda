package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.geral.bean.Coleta;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Epi;
import br.com.linkcom.sined.geral.bean.Expedicao;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Lotematerial;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialCustoEmpresa;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Nota;
import br.com.linkcom.sined.geral.bean.NotaVenda;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Producaoordem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Rateioitem;
import br.com.linkcom.sined.geral.bean.Requisicao;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialRateioEstoqueTipoContaGerencial;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.geral.bean.enumeration.OrigemMovimentacaoEstoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.DadosEstatisticosBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.MateriaprimaBean;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.SpedarquivoFiltro;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.ConsultarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.BaixarEstoqueBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ConsultarEstoqueWSBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.RegistroMovimentacaoAnimalMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.TransferenciaCategoriaBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.TransferenciaCategoriaMaterialBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.TotalizadorEntradaSaida;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.DinamicaRebanhoFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.MovimentacaoestoqueFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.DinamicaRebanhoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MovimentacaoestoqueService extends GenericService<Movimentacaoestoque> {

	private MovimentacaoestoqueDAO movimentacaoestoqueDAO;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private MaterialService materialService;
	private LocalarmazenagemService localarmazenagemService;
	private MaterialunidademedidaService materialunidademedidaService;
	private NotaVendaService notaVendaService;
	private NotafiscalprodutoService notafiscalprodutoService;
	private EntregadocumentoService entregadocumentoService;
	private ParametrogeralService parametrogeralService;
	private FornecedorService fornecedorService;
	private LoteestoqueService loteestoqueService;
	private PedidovendamaterialService pedidovendamaterialService; 
	private PedidovendaService pedidovendaService;
	private EmpresaService empresaService;
	private RateioService rateioService;
	private MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService;
	private MaterialCustoEmpresaService materialCustoEmpresaService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	
	public void setMovimentacaoEstoqueHistoricoService(MovimentacaoEstoqueHistoricoService movimentacaoEstoqueHistoricoService) {this.movimentacaoEstoqueHistoricoService = movimentacaoEstoqueHistoricoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {this.pedidovendaService = pedidovendaService;}
	public void setMovimentacaoestoqueorigemService(MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;}
	public void setMovimentacaoestoqueDAO(MovimentacaoestoqueDAO movimentacaoestoqueDAO) {this.movimentacaoestoqueDAO = movimentacaoestoqueDAO;}
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setMaterialunidademedidaService(MaterialunidademedidaService materialunidademedidaService) {this.materialunidademedidaService = materialunidademedidaService;}
	public void setNotaVendaService(NotaVendaService notaVendaService) {this.notaVendaService = notaVendaService;}
	public void setNotafiscalprodutoService(NotafiscalprodutoService notafiscalprodutoService) {this.notafiscalprodutoService = notafiscalprodutoService;}
	public void setEntregadocumentoService(EntregadocumentoService entregadocumentoService) {this.entregadocumentoService = entregadocumentoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setLoteestoqueService(LoteestoqueService loteestoqueService) {this.loteestoqueService = loteestoqueService;}
	public void setPedidovendamaterialService(PedidovendamaterialService pedidovendamaterialService) {this.pedidovendamaterialService = pedidovendamaterialService;}
	public void setMaterialCustoEmpresaService(MaterialCustoEmpresaService materialCustoEmpresaService) {this.materialCustoEmpresaService = materialCustoEmpresaService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	
	
	@Override
	public Movimentacaoestoque loadForEntrada(Movimentacaoestoque bean) {
		Movimentacaoestoque movimentacaoestoque = super.loadForEntrada(bean);
		if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE)) && movimentacaoestoque.getRateio() != null){
			if(movimentacaoestoque != null){
				movimentacaoestoque.setRateio(rateioService.findRateio(movimentacaoestoque.getRateio()));
			}
		}
		return movimentacaoestoque;
	}
	@Override
	public void saveOrUpdate(final Movimentacaoestoque bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(bean.getMovimentacaoestoqueorigem() != null){
					movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(bean.getMovimentacaoestoqueorigem());
				}
				
				if("TRUE".equals(parametrogeralService.getValorPorNome(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE))){
					if (bean.getRateio() != null) {
						rateioService.limpaReferenciaRateio(bean.getRateio());
						rateioService.saveOrUpdateNoUseTransaction(bean.getRateio());
					}
				}else {
					bean.setRateio(null);
				}
				saveOrUpdateNoUseTransaction(bean);
				return status;
			}
		});
	}
	
	
	private static MovimentacaoestoqueService instance;
	public static MovimentacaoestoqueService getInstance(){
		if(instance == null){
			instance = NeoWeb.getObject(MovimentacaoestoqueService.class);
		}
		return instance;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacaoestoque> findMovimentacoes(String whereIn) {
		return movimentacaoestoqueDAO.findMovimentacoes(whereIn);
	}
	
	public List<Movimentacaoestoque> findMovimentacoesForQtdedisponivel(String whereIn) {
		return movimentacaoestoqueDAO.findMovimentacoesForQtdedisponivel(whereIn);
	}
		
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findMovimentacoesEntrega(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaoestoque> findMovimentacoesEntrega(String whereIn) {
		return movimentacaoestoqueDAO.findMovimentacoesEntrega(whereIn);
	}
	
	/**
	 * Busca todas as movimenta��es n�o canceladas relacionadas a uma entregamaterial.
	 * @param entregamaterial
	 * @return
	 */
	public List<Movimentacaoestoque> findMovimentacoesNaoCanceladasPorEntrega(Entregamaterial entregamaterial) {
		return movimentacaoestoqueDAO.findMovimentacoesNaoCanceladasPorEntrega(entregamaterial);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public void doUpdateMovimentacoes(String whereIn) {
		movimentacaoestoqueDAO.doUpdateMovimentacoes(whereIn);
	}
	
	/**
	 * Gera relat�rio com valores iguais o da listagem.
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(MovimentacaoestoqueFiltro filtro) {
		Report report = new Report("/suprimento/entradasaida");
		Report subreport = new Report("/suprimento/sub_totaisunidademateriais");
		
		filtro.setPageSize(Integer.MAX_VALUE);
		List<Movimentacaoestoque> movimentacoes = this.findForReport(filtro);
		if(movimentacoes != null && movimentacoes.size() > 0){
			report.setDataSource(movimentacoes);
		}
		
		String whereIn = new String();	
		Map<Integer,Double> mapaMateriais = new HashMap<Integer,Double>();
		whereIn = CollectionsUtil.listAndConcatenate(movimentacoes, "material.cdmaterial", ",");
		for (Movimentacaoestoque movimentacaoestoque : movimentacoes) {			
			mapaMateriais = getMapaMateriais(movimentacaoestoque.getMaterial().getCdmaterial(),mapaMateriais,movimentacaoestoque);
			if (movimentacaoestoque.getMovimentacaoestoqueorigem() != null && movimentacaoestoque.getMovimentacaoestoqueorigem().getNotafiscalproduto() == null){
				if (movimentacaoestoque.getMovimentacaoestoqueorigem() != null && movimentacaoestoque.getMovimentacaoestoqueorigem().getVenda() != null){
					List<NotaVenda> vendaNota = notaVendaService.findByVenda(movimentacaoestoque.getMovimentacaoestoqueorigem().getVenda());
					for (int i=0; i < vendaNota.size(); i++){
						Notafiscalproduto notafiscalproduto = new Notafiscalproduto();
						notafiscalproduto.setCdNota(vendaNota.get(i).getNota().getCdNota());
						notafiscalproduto = notafiscalprodutoService.load(notafiscalproduto);
						if (notafiscalproduto !=null){
							movimentacaoestoque.getMovimentacaoestoqueorigem().setNotafiscalproduto(notafiscalproduto);
							break;
						}
					}
				}
				else if (movimentacaoestoque.getMovimentacaoestoqueorigem() != null && movimentacaoestoque.getMovimentacaoestoqueorigem().getEntrega() != null){
					List<Entregadocumento> listaEntregadocumento = entregadocumentoService.findListEntregaDocumentoByEntrega(movimentacaoestoque.getMovimentacaoestoqueorigem().getEntrega());
					Notafiscalproduto notafiscalproduto = new Notafiscalproduto();
					StringBuilder sb = new StringBuilder ();
					for (Entregadocumento entregadocumento : listaEntregadocumento) {
						
						sb.append(entregadocumentoService.load(entregadocumento, "numero")+", ");
					}
					notafiscalproduto.setNumero(sb.toString());
					movimentacaoestoque.getMovimentacaoestoqueorigem().setNotafiscalproduto(notafiscalproduto);
					
				}
			}
		}
		report.addSubReport("SUB_TOTAISUNIDADEMATERIAIS", subreport);
		report.addParameter("LISTATOTAISUNITARIOS", materialunidademedidaService.getTotaisUnidadeMedida(whereIn, mapaMateriais));
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findForReport(MovimentacaoestoqueFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Movimentacaoestoque> findForReport(MovimentacaoestoqueFiltro filtro){
		return movimentacaoestoqueDAO.findForReport(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param requisicaomaterial
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Long getQuantidadeEntregueDaRequisicaoMaterial(Requisicaomaterial requisicaomaterial) {
		return movimentacaoestoqueDAO.getQuantidadeEntregueDaRequisicaoMaterial(requisicaomaterial);
	}
	
	/**
	 * M�todo monta a origem dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Movimentacaoestoque form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getMovimentacaoestoqueorigem() == null){
			return listaBean;
		}
		
		if(form.getMovimentacaoestoqueorigem().getColeta() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.COLETA, "COLETA", form.getMovimentacaoestoqueorigem().getColeta().getCdcoleta().toString(), 
					form.getMovimentacaoestoqueorigem().getColeta().getCdcoleta().toString(), null);

			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getUsuario() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.USUARIO, "USU�RIO", form.getMovimentacaoestoqueorigem().getUsuario().getNome(), 
															  form.getMovimentacaoestoqueorigem().getUsuario().getCdpessoa() != null ? form.getMovimentacaoestoqueorigem().getUsuario().getCdpessoa().toString() : "", null);
		
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getRequisicaomaterial() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.REQUISICAO, "REQUISI��O DE MATERIAL", 
					  form.getMovimentacaoestoqueorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null ?  form.getMovimentacaoestoqueorigem().getRequisicaomaterial().getIdentificador().toString() : "", 
					  form.getMovimentacaoestoqueorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null ? form.getMovimentacaoestoqueorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getEntrega() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ENTREGA, "ENTREGA", 
					  form.getMovimentacaoestoqueorigem().getEntrega().getCdentrega() != null ? form.getMovimentacaoestoqueorigem().getEntrega().getCdentrega().toString() : "", 
					  form.getMovimentacaoestoqueorigem().getEntrega().getCdentrega() != null ? form.getMovimentacaoestoqueorigem().getEntrega().getCdentrega().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		}
		if((form.getMovimentacaoestoqueorigem().getRomaneiomanual() != null && form.getMovimentacaoestoqueorigem().getRomaneiomanual()) ||
				(form.getMovimentacaoestoqueorigem().getRomaneio() != null)){
			if(form.getMovimentacaoestoqueorigem().getRomaneio() != null){
				origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ROMANEIO, "ROMANEIO", 
						form.getMovimentacaoestoqueorigem().getRomaneio().getCdromaneio() != null ? form.getMovimentacaoestoqueorigem().getRomaneio().getCdromaneio().toString() : "", 
						form.getMovimentacaoestoqueorigem().getRomaneio().getCdromaneio() != null ? form.getMovimentacaoestoqueorigem().getRomaneio().getCdromaneio().toString() : "", 
								null);
				
				listaBean.add(origemsuprimentosBean);
			}
		} 
		if(form.getMovimentacaoestoqueorigem().getNotafiscalproduto() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.NOTA, "NOTA FISCAL DE PRODUTO", 
					form.getMovimentacaoestoqueorigem().getNotafiscalproduto().getCdNota() != null ? form.getMovimentacaoestoqueorigem().getNotafiscalproduto().getCdNota().toString() : "", 
							form.getMovimentacaoestoqueorigem().getNotafiscalproduto().getCdNota() != null ? form.getMovimentacaoestoqueorigem().getNotafiscalproduto().getCdNota().toString() : "", 
									null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getRequisicao() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ORDEMSERVICO, "ORDEM DE SERVI�O", 
					form.getMovimentacaoestoqueorigem().getRequisicao().getCdrequisicao() != null ? form.getMovimentacaoestoqueorigem().getRequisicao().getCdrequisicao().toString() : "", 
							form.getMovimentacaoestoqueorigem().getRequisicao().getCdrequisicao() != null ? form.getMovimentacaoestoqueorigem().getRequisicao().getCdrequisicao().toString() : "", 
									null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ORDEMSERVICOVETERINARIA, "ORDEM DE SERVI�O VETERIN�RIA", 
					form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria().getCdordemservicoveterinaria() != null ? form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria().getCdordemservicoveterinaria().toString() : "", 
							form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria().getCdordemservicoveterinaria() != null ? form.getMovimentacaoestoqueorigem().getOrdemservicoveterinaria().getCdordemservicoveterinaria().toString() : "", 
									null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getConsumo() != null && form.getMovimentacaoestoqueorigem().getConsumo()){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.GERENCIAMENTOESTOQUE, "REGISTRAR CONSUMO", "", "", 
					null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getProducaoagenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PRODUCAOAGENDA, "AGENDA DE PRODU��O", 
					form.getMovimentacaoestoqueorigem().getProducaoagenda().getCdproducaoagenda() != null ? form.getMovimentacaoestoqueorigem().getProducaoagenda().getCdproducaoagenda().toString() : "", 
					form.getMovimentacaoestoqueorigem().getProducaoagenda().getCdproducaoagenda() != null ? form.getMovimentacaoestoqueorigem().getProducaoagenda().getCdproducaoagenda().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getProducaoordem() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PRODUCAOORDEM, "ORDEM DE PRODU��O", 
					form.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem() != null ? form.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem().toString() : "", 
					form.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem() != null ? form.getMovimentacaoestoqueorigem().getProducaoordem().getCdproducaoordem().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		} 
		if(form.getMovimentacaoestoqueorigem().getExpedicaoproducao() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.EXPEDICAOPRODUCAO, "EXPEDI��O DE PRODU��O", 
					form.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao() != null ? form.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao().toString() : "", 
					form.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao() != null ? form.getMovimentacaoestoqueorigem().getExpedicaoproducao().getCdexpedicaoproducao().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		}
		if(form.getMovimentacaoestoqueorigem().getPedidovenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PEDIDOVENDA, "PEDIDO DE VENDA", 
					form.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda() != null ? form.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda().toString() : "", 
					form.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda() != null ? form.getMovimentacaoestoqueorigem().getPedidovenda().getCdpedidovenda().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		}
		if(form.getMovimentacaoestoqueorigem().getColaborador() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.COLABORADOR, "COLABORADOR", form.getMovimentacaoestoqueorigem().getColaborador().getCdpessoa().toString(), 
					form.getMovimentacaoestoqueorigem().getColaborador().getCdpessoa().toString(), null);

			listaBean.add(origemsuprimentosBean);
		}
		if(form.getMovimentacaoestoqueorigem().getVenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.VENDA, "VENDA", 
					form.getMovimentacaoestoqueorigem().getVenda().getCdvenda() != null ? form.getMovimentacaoestoqueorigem().getVenda().getCdvenda().toString() : "", 
					form.getMovimentacaoestoqueorigem().getVenda().getCdvenda() != null ? form.getMovimentacaoestoqueorigem().getVenda().getCdvenda().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		}
		if(form.getMovimentacaoestoqueorigem().getExpedicao() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.EXPEDICAO, "EXPEDI��O", 
					form.getMovimentacaoestoqueorigem().getExpedicao().getCdexpedicao() != null ? form.getMovimentacaoestoqueorigem().getExpedicao().getCdexpedicao().toString() : "", 
					form.getMovimentacaoestoqueorigem().getExpedicao().getCdexpedicao() != null ? form.getMovimentacaoestoqueorigem().getExpedicao().getCdexpedicao().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		}
		
		if (form.getMovimentacaoestoqueorigem().getVeiculodespesa()!=null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.DESPESAVEICULO, "DESPESA DE VE�CULO", 
							form.getMovimentacaoestoqueorigem().getVeiculodespesa().getCdveiculodespesa()!=null ? form.getMovimentacaoestoqueorigem().getVeiculodespesa().getCdveiculodespesa().toString() : "", 
							form.getMovimentacaoestoqueorigem().getVeiculodespesa().getCdveiculodespesa()!=null ? form.getMovimentacaoestoqueorigem().getVeiculodespesa().getCdveiculodespesa().toString() : "", 
							null);
			
			listaBean.add(origemsuprimentosBean);
		}
		
		if(form.getMovimentacaoestoqueorigem().getInventario() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.INVENTARIO, "INVENT�RIO", 
					form.getMovimentacaoestoqueorigem().getInventario().getCdinventario()!=null ? form.getMovimentacaoestoqueorigem().getInventario().getCdinventario().toString() : "", 
					form.getMovimentacaoestoqueorigem().getInventario().getCdinventario()!=null ? form.getMovimentacaoestoqueorigem().getInventario().getCdinventario().toString() : "", 
					null);
	
			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @param materialclasse
	 * @param localarmazenagem
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, null, null, null, null, false);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, null, null, false);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Loteestoque loteestoque) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, null, loteestoque);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Loteestoque loteestoque, Boolean naoConsiderarLote) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, null, loteestoque, naoConsiderarLote);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @param materialclasse
	 * @param localarmazenagem
	 * @return
	 * @author Rafael Salvio
	 */
	public Double getQtdDisponivelProdutoEpiServicoFromConferencia(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Loteestoque loteestoque, Empresa empresa) {
		return getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, null, loteestoque, empresa);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Boolean naoconsiderargrade, Loteestoque loteestoque, Empresa empresa) {
		return movimentacaoestoqueDAO.getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, null, naoconsiderargrade, loteestoque, false);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Boolean naoconsiderargrade)
	*
	* @param material
	* @param materialclasse
	* @param localarmazenagem
	* @param naoconsiderargrade
	* @param loteestoque
	* @return
	* @since 01/09/2014
	* @author Luiz Fernando
	*/
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Boolean naoconsiderargrade, Loteestoque loteestoque) {
		return movimentacaoestoqueDAO.getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, null, null, naoconsiderargrade, loteestoque, false);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Boolean naoconsiderargrade, Loteestoque loteestoque) {
		return movimentacaoestoqueDAO.getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, naoconsiderargrade, loteestoque, false);
	}
	
	public Double getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Boolean naoconsiderargrade, Loteestoque loteestoque, Boolean naoConsiderarLote) {
		return movimentacaoestoqueDAO.getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, naoconsiderargrade, loteestoque, naoConsiderarLote);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacaoestoque> getListaEntradaDaEntrega(Entrega entrega) {
		return movimentacaoestoqueDAO.getListaEntradaDaEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#existEntradaByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEntradaByEntrega(Entrega entrega) {
		return movimentacaoestoqueDAO.existEntradaByEntrega(entrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @return
	 * @author Ramon Brazil
	 */
	public Double getQuantidadeDeMaterialEntrada(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialEntradaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialEntrada(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Loteestoque loteestoque) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialEntradaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, loteestoque);
	}
	
	public Double getQuantidadeDeMaterialEntrada(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Boolean origemvenda, Projeto projeto) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialEntradaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, origemvenda, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialEntradaWhereInEmpresa(Material material, Localarmazenagem localarmazenagem, String whereInEmpresa, Projeto projeto) {
		return getQuantidadeDeMaterialEntradaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialEntradaWhereInEmpresa(Material material, Localarmazenagem localarmazenagem, String whreInEmpresa, Boolean origemvenda, Projeto projeto, Loteestoque loteestoque) {
		if(material == null || material.getCdmaterial() == null)
			return 0d;
		
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		Boolean considerarProjeto = false;
		try {
			String estoqueporprojeto = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUE_POR_PROJETO);
			if(estoqueporprojeto != null && "TRUE".equalsIgnoreCase(estoqueporprojeto)){
				considerarProjeto = true;
			}
		} catch (Exception e) {}
		
		Material bean = materialService.loadWithGrade(material);
		boolean whereMaterialmestregrade = false;
		if(bean != null && bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getGradeestoquetipo() != null){
			if(Gradeestoquetipo.MESTRE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
				if(bean.getMaterialmestregrade() != null){
					whereMaterialmestregrade = true;
				}else {
					whereMaterialmestregrade = false;
				}
			}else if(Gradeestoquetipo.ITEMGRADE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
				if(bean.getMaterialmestregrade() != null){
					whereMaterialmestregrade = false;
				}else {
					/*verifica se o material est� sendo controlado por item de grade e n�o
					existe filhos vinculados a ele. Se retornar true, o cadastro do material est� errado ( material n�o � de grade) 
					e a quantidade retornada em estoque deve ser zero.*/
					if(materialService.isControleItemgradeSemMaterialgrademestre(material)){
						return 0d;
					}else {
						whereMaterialmestregrade = true;
					}
				}
			}
		}
		return movimentacaoestoqueDAO.getQuantidadeDeMaterial(Movimentacaoestoquetipo.ENTRADA, bean, localarmazenagem, whreInEmpresa, considerarEmpresa, whereMaterialmestregrade, origemvenda, projeto, considerarProjeto, loteestoque);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @return
	 * @author Ramon Brazil
	 */
	public Double getQuantidadeDeMaterialSaida(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialSaidaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialSaida(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Loteestoque loteestoque) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialSaidaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, loteestoque);
	}
	
	public Double getQuantidadeDeMaterialSaida(Material material, Localarmazenagem localarmazenagem, Empresa empresa, Boolean origemvenda, Projeto projeto) {
		String whereInEmpresa = empresa != null && empresa.getCdpessoa() != null ? empresa.getCdpessoa().toString() : null;
		return getQuantidadeDeMaterialSaidaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, origemvenda, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialSaidaWhereInEmpresa(Material material, Localarmazenagem localarmazenagem, String whereInEmpresa, Projeto projeto) {
		return getQuantidadeDeMaterialSaidaWhereInEmpresa(material, localarmazenagem, whereInEmpresa, false, projeto, null);
	}
	
	public Double getQuantidadeDeMaterialSaidaWhereInEmpresa(Material material, Localarmazenagem localarmazenagem, String whereInEmpresa, Boolean origemvenda, Projeto projeto, Loteestoque loteestoque) {
		if(material == null || material.getCdmaterial() == null)
			return 0d;
		
		Boolean considerarEmpresa = true;
		try {
			String estoquesemempresa = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUESEMEMPRESA);
			if(estoquesemempresa != null && "TRUE".equalsIgnoreCase(estoquesemempresa)){
				considerarEmpresa = false;
			}
		} catch (Exception e) {}
		
		Boolean considerarProjeto = false;
		try {
			String estoqueporprojeto = parametrogeralService.getValorPorNome(Parametrogeral.ESTOQUE_POR_PROJETO);
			if(estoqueporprojeto != null && "TRUE".equalsIgnoreCase(estoqueporprojeto)){
				considerarProjeto = true;
			}
		} catch (Exception e) {}
		
		Material bean = materialService.loadWithGrade(material);
		boolean whereMaterialmestregrade = false;
		if(bean != null && bean.getMaterialgrupo() != null && bean.getMaterialgrupo().getGradeestoquetipo() != null){
			if(Gradeestoquetipo.MESTRE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
				if(bean.getMaterialmestregrade() != null){
					whereMaterialmestregrade = true;
				}else {
					whereMaterialmestregrade = false;
				}
			}else if(Gradeestoquetipo.ITEMGRADE.equals(bean.getMaterialgrupo().getGradeestoquetipo())){
				if(bean.getMaterialmestregrade() != null){
					whereMaterialmestregrade = false;
				}else {
					/*verifica se o material est� sendo controlado por item de grade e n�o
					existe filhos vinculados a ele. Se retornar true, o cadastro do material est� errado ( material n�o � de grade) 
					e a quantidade retornada em estoque deve ser zero.*/
					if(materialService.isControleItemgradeSemMaterialgrademestre(material)){
						return 0d;
					}else {
						whereMaterialmestregrade = true;
					}
				}
			}
		}
		return movimentacaoestoqueDAO.getQuantidadeDeMaterial(Movimentacaoestoquetipo.SAIDA, bean, localarmazenagem, whereInEmpresa, considerarEmpresa, whereMaterialmestregrade, origemvenda, projeto, considerarProjeto, loteestoque);
	}
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param material
	 * @return
	 * @author Ramon Brazil
	 */
	public Movimentacaoestoque findbyVenda(Venda venda, Material material) {
		return movimentacaoestoqueDAO.findbyVenda(venda, material);
	}
	
	public List<Movimentacaoestoque> findByPedidovenda(Pedidovenda pedidovenda, Material material) {
		return movimentacaoestoqueDAO.findByPedidovenda(pedidovenda, material);
	}
	
	/**
	 * M�todo invocado pela tela flex que retorna os dados estatisticos
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public DadosEstatisticosBean getDadosEstatisticosMovimentacao(){
		Date ultimoCadastro = getDtUltimoCadastro();
		Integer qtdeTotalEntrada = getQtdeTotalEntradaSaida(Movimentacaoestoquetipo.ENTRADA);
		Integer qtdeTotalSaida = getQtdeTotalEntradaSaida(Movimentacaoestoquetipo.SAIDA);
		Integer total = getQtdeTotalMovimentacoes();
		return new DadosEstatisticosBean(DadosEstatisticosBean.ESTOQUE, ultimoCadastro, qtdeTotalEntrada, qtdeTotalSaida, total);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalMovimentacoes() {
		return movimentacaoestoqueDAO.getQtdeTotalMovimentacoes();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Integer getQtdeTotalEntradaSaida(Movimentacaoestoquetipo tipo) {
		return movimentacaoestoqueDAO.getQtdeTotalEntradaSaida(tipo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @Author Tom�s Rabelo
	 */
	public Date getDtUltimoCadastro() {
		return movimentacaoestoqueDAO.getDtUltimoCadastro();
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListMovimentacaoEstoqueByWhereInMaterial
	 * @param whereIn
	 * @return
	 * @return List<Movimentacaoestoque>
	 * @author Thiago Augusto
	 * @date 03/07/2012
	 *
	 */
	public List<Movimentacaoestoque> findListMovimentacaoEstoqueByWhereInMaterial(List<String> listaCdMaterial, List<String> listaCdLocalArmazenagem, List<String> listaCdMaterialClasse){
		return movimentacaoestoqueDAO.findListMovimentacaoEstoqueByWhereInMaterial(listaCdMaterial, listaCdLocalArmazenagem, listaCdMaterialClasse);
	}
	
	/**
	 * Cancelar as Movimentacaoestoque a partir de expedi��es.
	 *
	 * @param whereIn
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public void cancelarByExpedicao(String whereIn, String motivo) {
		List<Movimentacaoestoque> listaMovimentacaoestoque = this.findByExpedicao(whereIn);
		if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
			this.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
			for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
				if ("cancelamento".equals(motivo)){
					movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CANCELAR, "CANCELADO VIA CANCELAMENTO DE EXPEDI��O DE VENDA", true);					
				}else if ("estorno".equals(motivo)) {
					movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CANCELAR, "CANCELADO VIA ESTORNO DE EXPEDI��O DE VENDA", true);	
				}else if ("unificar".equals(motivo)) {
					movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CANCELAR, "CANCELADO VIA UNIFICA��O DE EXPEDI��ES DE VENDA", true);	
				}
			}			
		}
	}
	
	/**
	 * Cancela as movimenta��es de estoque referente a nota passada por par�metro.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 * @since 05/09/2013
	 */
	public void cancelarByNota(String whereIn) {
		List<Movimentacaoestoque> listaMovimentacaoestoque = this.findByNota(whereIn);
		if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
			this.doUpdateMovimentacoes(CollectionsUtil.listAndConcatenate(listaMovimentacaoestoque, "cdmovimentacaoestoque", ","));
			for(Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque){
				if(movimentacaoestoque.getMovimentacaoestoqueorigem() != null){
					Notafiscalproduto nota = movimentacaoestoque.getMovimentacaoestoqueorigem().getNotafiscalproduto();
					salvarHisotirico(movimentacaoestoque, "Cancelado via cancelamento da nota <a href=\"javascript:visualizarNota("+ nota.getCdNota()+");\">"+
															nota.getNumeroOrCdNota() +"</a>", MovimentacaoEstoqueAcao.CANCELAR);
				}else{
					salvarHisotirico(movimentacaoestoque, "Cancelado via cancelamento da nota", MovimentacaoEstoqueAcao.CANCELAR);
				}
			}
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByNota(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/09/2013
	 */
	public List<Movimentacaoestoque> findByNota(String whereIn) {
		return movimentacaoestoqueDAO.findByNota(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByExpedicao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @since 17/07/2012
	 * @author Rodrigo Freitas
	 */
	public List<Movimentacaoestoque> findByExpedicao(String whereIn) {
		return movimentacaoestoqueDAO.findByExpedicao(whereIn);
	}
	
	/**
	 * M�todo que gera a sa�da de material da nota fiscal de produto
	 *
	 * @see br.com.linkcom.sined.geral.service.NotafiscalprodutoService#findForMovimentacaoestoque(Nota nota)
	 * @see br.com.linkcom.sined.geral.service.MaterialService#carregaMaterialForMovimentacaoVenda(Material material)
	 * 
	 * @param notafiscalproduto
	 * @author Luiz Fernando
	 */
	public List<Movimentacaoestoque> gerarMovimentacaoNota(Notafiscalproduto notafiscalproduto, boolean origemVenda) {
		List<Movimentacaoestoque> listaMovimentacaoestoque = new ArrayList<Movimentacaoestoque>();
		
		if(notafiscalproduto != null && 
				notafiscalproduto.getCdNota() != null && 
				notafiscalproduto.getListaItens() != null && 
				!notafiscalproduto.getListaItens().isEmpty()){
			
			Movimentacaoestoque movimentacaoestoque;
			for(Notafiscalprodutoitem item : notafiscalproduto.getListaItens()){
				if(item.getMaterial() != null){
					Material materialEntradaSaida = item.getMaterial();
					if(materialEntradaSaida != null && Materialclasse.PRODUTO.equals(item.getMaterialclasse()) || Materialclasse.EPI.equals(item.getMaterialclasse())){
						Materialclasse materialclasse = new Materialclasse();
						
						movimentacaoestoque = new Movimentacaoestoque();
						movimentacaoestoque.setDtmovimentacao(new Date(System.currentTimeMillis()));
						movimentacaoestoque.setEmpresa(notafiscalproduto.getEmpresa());
						movimentacaoestoque.setProjeto(notafiscalproduto.getProjeto());
						movimentacaoestoque.setMaterial(materialEntradaSaida);
						movimentacaoestoque.setLoteestoque(item.getLoteestoque());
						movimentacaoestoque.setNotafiscalproduto(notafiscalproduto);
						movimentacaoestoque.setNotaFiscalProdutoItem(item);
						movimentacaoestoque.setContaGerencial(item.getCfop() != null ? item.getCfop().getContaGerencial() : null);
						
						if(notafiscalproduto.getProjeto() != null){
							movimentacaoestoque.setProjetoRateio(notafiscalproduto.getProjeto());
						}else if(materialEntradaSaida != null && materialEntradaSaida.getMaterialRateioEstoque() != null){
							movimentacaoestoque.setProjetoRateio(materialEntradaSaida.getMaterialRateioEstoque().getProjeto());
						}
						
						Boolean registrarSaidaMaterialproducao = false;
						Material material = materialService.carregaMaterialForMovimentacaoVenda(materialEntradaSaida);
						if(origemVenda && material != null && material.getProducao() != null && material.getProducao()){		
							registrarSaidaMaterialproducao = true;
							movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
							if(material.getListaProducao() != null && !material.getListaProducao().isEmpty()){
								Movimentacaoestoque me;
								for(Materialproducao mp : material.getListaProducao()){
									me = this.criaMovimentacaoMaterialproducao(notafiscalproduto, item, mp);
									if(me != null){
										me.setNotafiscalproduto(notafiscalproduto);
										listaMovimentacaoestoque.add(me);
									}
								}
							}
							
						}else {
							if(notafiscalproduto.getTipooperacaonota() != null && Tipooperacaonota.ENTRADA.equals(notafiscalproduto.getTipooperacaonota()))
								movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
							else
								movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
						}
						
						movimentacaoestoque.setQtde(item.getQtde());
								
						if(parametrogeralService.getBoolean(Parametrogeral.MOVIMENTACAOESTOQUE_VALORCUSTOMEDIO)){
							movimentacaoestoque.setValor(materialEntradaSaida.getValorcusto());
						}else {
							if(item.getValorunitario() != null && item.getQtde() != null){
								movimentacaoestoque.setValor(this.getValorcustoEntrada(item.getValorunitario(), item.getQtde(), item));
							}
						}
						
						if(item.getMaterialclasse().equals(Materialclasse.PRODUTO)){
							materialclasse = Materialclasse.PRODUTO;	
						}else if(item.getMaterialclasse().equals(Materialclasse.EPI)){
							materialclasse = Materialclasse.EPI;	
						}
						
						if(item.getLocalarmazenagem() != null){
							movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
						}else if (notafiscalproduto.getEmpresa() != null) {
							List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(notafiscalproduto.getEmpresa());
							if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
								movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
						}
						movimentacaoestoque.setMaterialclasse(materialclasse);
						movimentacaoestoque.setNotafiscalproduto(notafiscalproduto);
						listaMovimentacaoestoque.add(movimentacaoestoque);
						
						if(registrarSaidaMaterialproducao && (notafiscalproduto.getTipooperacaonota() == null || Tipooperacaonota.SAIDA.equals(notafiscalproduto.getTipooperacaonota()))){
							Movimentacaoestoque msaida = new Movimentacaoestoque();
							msaida.setMaterial(movimentacaoestoque.getMaterial());
							msaida.setQtde(movimentacaoestoque.getQtde());
							msaida.setDtmovimentacao(movimentacaoestoque.getDtmovimentacao());
							msaida.setEmpresa(movimentacaoestoque.getEmpresa());
							msaida.setProjeto(movimentacaoestoque.getProjeto());
							msaida.setLoteestoque(movimentacaoestoque.getLoteestoque());
							msaida.setNotafiscalproduto(movimentacaoestoque.getNotafiscalproduto());
							msaida.setValor(movimentacaoestoque.getValor());
							msaida.setMaterialclasse(movimentacaoestoque.getMaterialclasse());
							msaida.setLocalarmazenagem(movimentacaoestoque.getLocalarmazenagem());
							msaida.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
							msaida.setContaGerencial(movimentacaoestoque.getContaGerencial() != null ? movimentacaoestoque.getContaGerencial() : null);
							listaMovimentacaoestoque.add(msaida);
						}
						
//						materialEntradaSaida.setListaMaterialitemgrade(materialService.findMaterialitemByMaterialmestregrade(item.getMaterial()));
//						
//						if(!materialService.isControleMaterialgrademestre(item.getMaterial()) && materialEntradaSaida.getListaMaterialitemgrade() != null && !materialEntradaSaida.getListaMaterialitemgrade().isEmpty()){
//							List<Movimentacaoestoque> listaMovEstoqueItensGrade = new ArrayList<Movimentacaoestoque>();
//							for (Material itemGrade : materialEntradaSaida.getListaMaterialitemgrade()) {
//								Movimentacaoestoque me = new Movimentacaoestoque();
//								me.setMovimentacaoestoquetipo(movimentacaoestoque.getMovimentacaoestoquetipo());
//								me.setMaterial(itemGrade);
//								me.setDtmovimentacao(movimentacaoestoque.getDtmovimentacao());
//								me.setEmpresa(movimentacaoestoque.getEmpresa());
//								me.setProjeto(movimentacaoestoque.getProjeto());
//								me.setLoteestoque(movimentacaoestoque.getLoteestoque());
//								me.setMovimentacaoestoquetipo(movimentacaoestoque.getMovimentacaoestoquetipo());
//								me.setValor(movimentacaoestoque.getValor());
//								me.setLocalarmazenagem(movimentacaoestoque.getLocalarmazenagem());
//								me.setMaterialclasse(movimentacaoestoque.getMaterialclasse());
//								me.setNotafiscalproduto(movimentacaoestoque.getNotafiscalproduto());
//								
//								listaMovEstoqueItensGrade.add(me);
//							}
//							if(listaMovEstoqueItensGrade != null && !listaMovEstoqueItensGrade.isEmpty()){
//								movimentacaoestoque.setListaMovimentacaoEstoqueGradeItem(listaMovEstoqueItensGrade);
//							}
//						}
					}
				}
			}
			
		}
		
		return listaMovimentacaoestoque;
	}
	
	/**
	 * M�todo que cria a sa�da de um material de produ��o
	 *
	 * @param notafiscalproduto
	 * @param item
	 * @param mp
	 * @return
	 * @author Luiz Fernando
	 */
	private Movimentacaoestoque criaMovimentacaoMaterialproducao(Notafiscalproduto notafiscalproduto, Notafiscalprodutoitem item, Materialproducao mp) {
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		
		movimentacaoestoqueorigem.setNotafiscalproduto(notafiscalproduto);
		movimentacaoestoqueorigem.setNotaFiscalProdutoItem(item);
		Materialclasse materialclasse = new Materialclasse();
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setDtmovimentacao(new Date(System.currentTimeMillis()));
		movimentacaoestoque.setEmpresa(notafiscalproduto.getEmpresa());
		movimentacaoestoque.setMaterial(mp.getMaterial());
		movimentacaoestoque.setQtde(mp.getConsumo()*item.getQtde());
		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
		movimentacaoestoque.setValor(mp.getValorconsumo() * item.getQtde());
		movimentacaoestoque.setLocalarmazenagem(item.getLocalarmazenagem());
		if(mp.getMaterial().getProduto() != null && mp.getMaterial().getProduto()){
			materialclasse = Materialclasse.PRODUTO;	
		}else if(mp.getMaterial().getServico() != null && mp.getMaterial().getServico()){
			materialclasse = Materialclasse.SERVICO;	
		}else if(mp.getMaterial().getPatrimonio() != null && mp.getMaterial().getPatrimonio()){
			materialclasse = Materialclasse.PATRIMONIO;	
		}else {
			materialclasse = Materialclasse.EPI;	
		}
		if (notafiscalproduto.getEmpresa() != null && item.getLocalarmazenagem() == null) {
			List<Localarmazenagem> listaLocalarmazenagem = localarmazenagemService.loadForVenda(notafiscalproduto.getEmpresa());
			if (listaLocalarmazenagem != null && listaLocalarmazenagem.size() == 1)
				movimentacaoestoque.setLocalarmazenagem(listaLocalarmazenagem.get(0));				
		}
		movimentacaoestoque.setMaterialclasse(materialclasse);
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		
		return movimentacaoestoque;
	}
	
	
	public List<Movimentacaoestoque> findForCsv(MovimentacaoestoqueFiltro filtro) {

		return movimentacaoestoqueDAO.findForCsv(filtro);
	}
	
	public List<Movimentacaoestoque> findForGerarLancamentoContabilContaGerencial(GerarLancamentoContabilFiltro filtro, Contagerencial contagerencial, String whereNotIn, String whereNotInRatioitem, OrigemMovimentacaoEstoqueEnum origemMovimentacaoEstoque, boolean considerarRateio) {
		return movimentacaoestoqueDAO.findForGerarLancamentoContabilContaGerencial(filtro, contagerencial, whereNotIn, whereNotInRatioitem, origemMovimentacaoEstoque, considerarRateio);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#getTotalValoresEntradaSaida(MovimentacaoestoqueFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<TotalizadorEntradaSaida> getTotalValoresEntradaSaida(MovimentacaoestoqueFiltro filtro){
		return movimentacaoestoqueDAO.getTotalValoresEntradaSaida(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#existMovimentacaoestoqueByNotas(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public boolean existMovimentacaoestoqueByNotas(String whereIn) {
		return movimentacaoestoqueDAO.existMovimentacaoestoqueByNotas(whereIn);
	}	
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByRomaneio(Romaneio romaneio)
	 *
	 * @param romaneio
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<Movimentacaoestoque> findByRomaneio(Romaneio romaneio) {
		return this.findByRomaneio(romaneio, null);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param romaneio
	 * @param movimentacaoestoquetipo
	 * @return
	 * @author Rodrigo Freitas
	 * @since 05/06/2014
	 */
	public List<Movimentacaoestoque> findByRomaneio(Romaneio romaneio, Movimentacaoestoquetipo movimentacaoestoquetipo) {
		return movimentacaoestoqueDAO.findByRomaneio(romaneio, movimentacaoestoquetipo);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByEntrega(Entrega entrega)
	 *
	 * @param entrega
	 * @return
	 * @author Rodrigo Freitas
	 * @since 12/12/2012
	 */
	public List<Movimentacaoestoque> findByEntrega(Entrega entrega) {
		return movimentacaoestoqueDAO.findByEntrega(entrega);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByEntregamaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByEntregamaterial(String whereIn) {
		return movimentacaoestoqueDAO.findByEntregamaterial(whereIn);
	}
	
	/**
	 * Monta o link para o hist�rico.
	 *
	 * @param movimentacaoestoque
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/01/2013
	 */
	public String makeLinkHistoricoMovimentacaoestoque(Movimentacaoestoque movimentacaoestoque) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<a href=\"javascript:visualizarMovimentacaoestoque(")
			.append(movimentacaoestoque.getCdmovimentacaoestoque())
			.append(");\">")
			.append(movimentacaoestoque.getCdmovimentacaoestoque())
			.append("</a>");
		
		return sb.toString();
	}
	
	/**
	 * Monta o link para o hist�rico.
	 *
	 * @param whereInMovimentacaoestoque
	 * @return
	 * @author Rodrigo Freitas
	 * @since 01/10/2015
	 */
	public String makeLinkHistoricoMovimentacaoestoque(String whereInMovimentacaoestoque) {
		if(whereInMovimentacaoestoque == null || whereInMovimentacaoestoque.trim().equals("")) return "";
		
		StringBuilder sb = new StringBuilder();
		
		String[] idsMovimentacaoestoque = whereInMovimentacaoestoque.split(",");
		for (String idStr : idsMovimentacaoestoque) {
			sb.append(this.makeLinkHistoricoMovimentacaoestoque(new Movimentacaoestoque(Integer.parseInt(idStr)))).append(", ");
		}
		
		return sb.length() > 0 ? sb.substring(0, sb.length() - 2) : sb.toString();
	}
	
	public void deleteAllAnteriorIgual(Date dtestoque) {
		movimentacaoestoqueDAO.deleteAllAnteriorIgual(dtestoque);
	}
	
	public void deleteAllAnteriorIgualEmpresa(Date dtestoque, Empresa empresa) {
		movimentacaoestoqueDAO.deleteAllAnteriorIgualEmpresa(dtestoque, empresa);
	}
	
	/**
	 * M�todo que retorna o mapa com as quantidades definidas pelo tipo de movimenta��o.
	 * 
	 * @see gerarRelatorio(MovimentacaoestoqueFiltro)
	 * @param cdmaterial
	 * @param mapaMateriais
	 * @param movimentacaoestoque
	 * @return
	 */
	private Map<Integer, Double> getMapaMateriais(Integer cdmaterial, Map<Integer, Double> mapaMateriais, Movimentacaoestoque movimentacaoestoque) {		
		if(mapaMateriais.get(cdmaterial)==null){
			if(movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
				mapaMateriais.put(movimentacaoestoque.getMaterial().getCdmaterial(), movimentacaoestoque.getQtde());
			}else if(movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.SAIDA)){
				mapaMateriais.put(movimentacaoestoque.getMaterial().getCdmaterial(), movimentacaoestoque.getQtde()*(-1));
			}						
		}else{
			if(movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
				Double qtde = mapaMateriais.get(movimentacaoestoque.getMaterial().getCdmaterial()) + movimentacaoestoque.getQtde();
				mapaMateriais.put(movimentacaoestoque.getMaterial().getCdmaterial(), qtde);
			}else if(movimentacaoestoque.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.SAIDA)){
				Double qtde = mapaMateriais.get(movimentacaoestoque.getMaterial().getCdmaterial()) - movimentacaoestoque.getQtde();
				mapaMateriais.put(movimentacaoestoque.getMaterial().getCdmaterial(), qtde);
			}						
		}		
		return mapaMateriais;
	}
	
	/**
	 * M�todo que retorna o valor de custo de entrada
	 *
	 * @param valor
	 * @param qtd_ent
	 * @param entregamaterial
	 * @return
	 * @author Luiz Fernando
	 * @since 30/10/2013
	 */
	public Double getValorcustoEntrada(Double valor, Double qtd_ent, Entregamaterial entregamaterial) {
		if(entregamaterial != null && entregamaterial.getCfop() != null && entregamaterial.getCfop().getCodigo() != null && 
				(entregamaterial.getCfop().getCodigo().equals("1910") || entregamaterial.getCfop().getCodigo().equals("2910"))){
			valor = 0d; 
		}else if(valor != null && entregamaterial != null && qtd_ent != null && qtd_ent > 0){
			//caso seja alterado, verificar no banco o mesmo calcula (tg_entregamaterial_insert)
			valor = valor 
				+ (entregamaterial.getValoripi() != null ? (entregamaterial.getValoripi().getValue().doubleValue()/qtd_ent) : 0d)
	            + (entregamaterial.getValorfrete() != null ? (entregamaterial.getValorfrete().getValue().doubleValue()/qtd_ent) : 0d)
	            + (entregamaterial.getValoricmsst() != null ? (entregamaterial.getValoricmsst().getValue().doubleValue()/qtd_ent) : 0d)
	            + (entregamaterial.getValorseguro() != null ? (entregamaterial.getValorseguro().getValue().doubleValue()/qtd_ent) : 0d)
	            + (entregamaterial.getValoroutrasdespesas() != null ? (entregamaterial.getValoroutrasdespesas().getValue().doubleValue()/qtd_ent) : 0d)
	            - (entregamaterial.getValordesconto() != null ? (entregamaterial.getValordesconto().getValue().doubleValue()/qtd_ent) : 0d);
		}
		
		return valor;
	}
	
	/**
	 * M�todo que retorna o valor de custo de entrada
	 *
	 * @param valor
	 * @param qtd_ent
	 * @param item
	 * @return
	 * @author Luiz Fernando
	 * @since 12/11/2013
	 */
	public Double getValorcustoEntrada(Double valor, Double qtd_ent, Notafiscalprodutoitem item) {
		if(valor != null && item != null && qtd_ent != null && qtd_ent > 0){
			valor = valor 
				+ (item.getValoripi() != null ? (item.getValoripi().getValue().doubleValue()/qtd_ent) : 0d)
	            + (item.getValorfrete() != null ? (item.getValorfrete().getValue().doubleValue()/qtd_ent) : 0d)
	            + (item.getValoricmsst() != null ? (item.getValoricmsst().getValue().doubleValue()/qtd_ent) : 0d)
	            + (item.getValorseguro() != null ? (item.getValorseguro().getValue().doubleValue()/qtd_ent) : 0d)
	            + (item.getOutrasdespesas() != null ? (item.getOutrasdespesas().getValue().doubleValue()/qtd_ent) : 0d)
	            - (item.getValordesconto() != null ? (item.getValordesconto().getValue().doubleValue()/qtd_ent) : 0d);
		}
		
		return valor;
	}
	
	/**
	 * M�todo que faz atualiza��o do estoque.
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 14/02/2014
	 */
	public void atualizarEstoque(AtualizacaoEstoqueBean bean, Map<String, List<Material>> mapaLotes) {
		Date dtestoque = bean.getDtestoque();
		Empresa empresa = bean.getEmpresa();
		Localarmazenagem localarmazenagem = bean.getLocalarmazenagem();
		List<AtualizacaoEstoqueMaterialBean> lista = bean.getListaMaterial();
		
		if(lista == null || lista.size() == 0){
			throw new SinedException("Nenhum material encontrado para a atualiza��o.");
		}
		
		//Processamento de lotes do arquivo de entrada
		if(mapaLotes != null && !mapaLotes.isEmpty()){
			StringBuilder whereInLote = new StringBuilder();
			StringBuilder whereInCnpj = new StringBuilder();
			String[] lotecnpj = null;
			Set<String> lotes = mapaLotes.keySet();
			
			//Prepara��o de dados para busca de loteestoque
			Iterator<String> iterator = lotes.iterator();
			try {
				while(iterator.hasNext()){
					lotecnpj = iterator.next().split("\\|");
					
					String cnpjStr = lotecnpj[1];
					if(cnpjStr != null){
						cnpjStr = br.com.linkcom.neo.util.StringUtils.stringCheia(cnpjStr, "0", 14, false);
					}
					
					whereInLote.append("'"+lotecnpj[0]+"'");
					whereInCnpj.append("'"+new Cnpj(cnpjStr).getValue()+"'");
					if(iterator.hasNext()){
						whereInLote.append(",");
						whereInCnpj.append(",");
					}
				}					
			} catch (Exception e) {
//				System.out.println("Erro ao converter o lotecnpj.");
//				System.out.println("Lote: " + lotecnpj[0]);
//				System.out.println("Cnpj: " + lotecnpj[1]);
				e.printStackTrace();
			}
			
			//Carrega os loteestoques que atendem aos requisitos de numero e fornecedor
			List<Loteestoque> listaLotes = LoteestoqueService.getInstance().findByWhereInNumeroAndFornecedor(whereInLote.toString(), whereInCnpj.toString());
			iterator = lotes.iterator();
			try {
				
				//Itera por todos os numeros de lotes fornecidos no arquivo de entrada
				while(iterator.hasNext()){
					String key = iterator.next();
					lotecnpj = key.split("\\|");
					String numero = lotecnpj[0];
					String cnpjStr = lotecnpj[1];
					if(cnpjStr != null){
						cnpjStr = br.com.linkcom.neo.util.StringUtils.stringCheia(cnpjStr, "0", 14, false);
					}
					
					Cnpj cnpj = new Cnpj(cnpjStr);
					Loteestoque loteencontrado = null;
					
					//Tenta localizar um lote com respectivo numero e fornecedor da lista carregada do banco
					if(listaLotes != null && !listaLotes.isEmpty()){
						for(Loteestoque loteestoque : listaLotes){
							if(loteestoque.getNumero().equalsIgnoreCase(numero) && loteestoque.getFornecedor().getCnpj().equals(cnpj)){
								loteencontrado = loteestoque;
								break;
							}
						}
					}
					//Caso o lote ainda n�o exista, este ser� criado
					if(loteencontrado == null){
						loteencontrado = new Loteestoque();
						loteencontrado.setNumero(numero);
						Fornecedor fornecedor = fornecedorService.findByCnpj(cnpj);
						if(fornecedor == null){
							continue;
						} else{
							loteencontrado.setFornecedor(fornecedor);
						}
					}
					if(loteencontrado.getListaLotematerial() == null){
						loteencontrado.setListaLotematerial(new ListSet<Lotematerial>(Lotematerial.class));
					}
					
					//Itera pelos materiais a serem cadastrados para o lote a ser atualizado/inserido
					List<Material> listaMateriais = mapaLotes.get(key);
					if(listaMateriais != null && !listaMateriais.isEmpty()){
						for(Material m : listaMateriais){
							
							//Verifica se o lote a ser atualizado/inserido j� possui o material em quest�o
							Boolean possuiMaterial = Boolean.FALSE;
							for(Lotematerial lotematerial : loteencontrado.getListaLotematerial()){
								if(lotematerial.getMaterial().equals(m)){
									possuiMaterial = Boolean.TRUE;
									break;
								}
							}
							//Caso n�o possua, o insere/atualiza
							if(!possuiMaterial){
								Lotematerial lotematerial = new Lotematerial();
								lotematerial.setBloqueado(Boolean.FALSE);
								lotematerial.setLoteestoque(loteencontrado);
								lotematerial.setMaterial(m);
								loteencontrado.getListaLotematerial().add(lotematerial);
							}
						}
						
						loteestoqueService.saveOrUpdate(loteencontrado);
						
						if(StringUtils.isNotEmpty(loteencontrado.getNumero()) && SinedUtil.isListNotEmpty(lista)){
							for (AtualizacaoEstoqueMaterialBean it : lista) {
								if(it.getLoteestoque() == null && StringUtils.isNotEmpty(it.getLote()) && loteencontrado.getNumero().equals(it.getLote()) && loteencontrado.getFornecedor().getCnpj().equals(cnpj)){
									it.setLoteestoque(loteencontrado);
								}
							}
						}
					}
				}					
			} catch (Exception e) {}
		}
		
		for (AtualizacaoEstoqueMaterialBean it : lista) {
			materialService.validateObrigarLote(it);
		}
		
		String dataAtualStr = SinedDateUtils.toString(SinedDateUtils.currentDate());
		for (AtualizacaoEstoqueMaterialBean it : lista) {
			Double qtdeSistema = it.getQtdeSistema();
			Double qtdeAtual = it.getQtdeAtual();
			
			
			Double quantidade = 0d;
			Movimentacaoestoquetipo movimentacaoestoquetipo = null;
			if(bean.getGerarSomenteEntrada() != null && bean.getGerarSomenteEntrada()){
				movimentacaoestoquetipo = Movimentacaoestoquetipo.ENTRADA;
				quantidade = qtdeAtual;
			}else {
				// SE FOR IGUAL A QUANTIDADE DO SISTEMA COM A QUANTIDADE ATUAL N�O FAZ NADA
				if(qtdeSistema.equals(qtdeAtual)){
					continue;
				}
				
				if(qtdeSistema > qtdeAtual && (bean.getGerarSomenteEntrada() == null || !bean.getGerarSomenteEntrada())){
					movimentacaoestoquetipo = Movimentacaoestoquetipo.SAIDA;
					quantidade = new BigDecimal(qtdeSistema).subtract(new BigDecimal(qtdeAtual)).doubleValue();
				} else if(qtdeSistema < qtdeAtual){
					movimentacaoestoquetipo = Movimentacaoestoquetipo.ENTRADA;
					quantidade = new BigDecimal(qtdeAtual).subtract(new BigDecimal(qtdeSistema)).doubleValue();
				}
			}
			
			if(movimentacaoestoquetipo == null){
				continue;
			}
			
			Material material = it.getMaterial();
			Materialclasse materialclasse = Materialclasse.PRODUTO;
			if((material.getProduto() == null || !material.getProduto()) && (material.getEpi() != null && material.getEpi())){
				materialclasse = Materialclasse.EPI;
			}
			
			//Verifica se as contas gerenciais de ajuste estao preenchidas 
			if (SinedUtil.isRateioMovimentacaoEstoque()){
				if (Movimentacaoestoquetipo.ENTRADA.equals(movimentacaoestoquetipo) && (material.getMaterialRateioEstoque() == null 
						|| material.getMaterialRateioEstoque().getContaGerencialAjusteEntrada() == null)){
					throw new SinedException("� necess�rio o preenchimento do campo Conta Gerencial de ajuste (Entrada) na aba Rateio de Estoque do material: " + material.getNome());
				}
				if (Movimentacaoestoquetipo.SAIDA.equals(movimentacaoestoquetipo) && (material.getMaterialRateioEstoque() == null  
						|| material.getMaterialRateioEstoque().getContaGerencialAjusteEntrada() == null)){
				throw new SinedException("� necess�rio o preenchimento do campo Conta Gerencial de ajuste (Sa�da) na aba Rateio de Estoque do material: " + material.getNome());
				}
			}
	
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setMovimentacaoestoquetipo(movimentacaoestoquetipo);
			movimentacaoestoque.setDtmovimentacao(dtestoque);
			movimentacaoestoque.setQtde(quantidade);
			movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
			movimentacaoestoque.setEmpresa(empresa);
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoque.setMaterialclasse(materialclasse);
			movimentacaoestoque.setObservacao("Acerto de estoque no dia " + dataAtualStr);
			movimentacaoestoque.setLoteestoque(it.getLoteestoque());

			Money valorItemRateio =  new Money();
			if(material.getValorcusto() != null){
				valorItemRateio = new Money(material.getValorcusto());
			}
			
			//Cria o rateio na movimentacao do estoque
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				if (Movimentacaoestoquetipo.ENTRADA.equals(movimentacaoestoquetipo)){
					movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, movimentacaoestoque.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.AJUSTE_ENTRADA, it.getCentroCusto(), material.getProjeto(), valorItemRateio));					
				} else{
					movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, movimentacaoestoque.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.AJUSTE_SAIDA, it.getCentroCusto(), material.getProjeto(), valorItemRateio));
				}
				if(movimentacaoestoque.getRateio() != null && SinedUtil.isListNotEmpty(movimentacaoestoque.getRateio().getListaRateioitem())){
					boolean removerRateio = false;
					for(Rateioitem rateioitem : movimentacaoestoque.getRateio().getListaRateioitem()){
						if(rateioitem.getCentrocusto() == null){
							removerRateio = true;
						}
					}
					if(removerRateio){
						movimentacaoestoque.setRateio(null);
					}
				}
			}
			
			if(Boolean.TRUE.equals(it.getRegistrarpesomedio())){
				movimentacaoestoque.setPesomedio(it.getPesomedio());
				materialService.updatePesoBruto(material, it.getPesomedio());
			}
			
			this.saveOrUpdate(movimentacaoestoque);
			
			movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, MovimentacaoEstoqueAcao.CRIAR, "Acerto de estoque no dia " + dataAtualStr, true);
			
		}
	}
	
	public List<Movimentacaoestoque> findListMovimentacaoEstoqueByMaterialInData(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Date dataDe, Date dataAte){
		return movimentacaoestoqueDAO.findListMovimentacaoEstoqueByMaterialInData(material, empresa, localarmazenagem, dataDe, dataAte);
	}
	
	public List<Movimentacaoestoque> findForComprovanteColeta(Pedidovenda pedidovenda) {
		return movimentacaoestoqueDAO.findForComprovanteColeta(pedidovenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#findByPedidovenda(String whereInPedidovenda)
	 *
	 * @param whereInPedidovenda
	 * @return
	 * @author Luiz Fernando
	 * @since 02/04/2014
	 */
	public List<Movimentacaoestoque> findByPedidovenda(String whereInPedidovenda) {
		return movimentacaoestoqueDAO.findByPedidovenda(whereInPedidovenda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#existeMovimentacaoEstoque(Material material)
	*
	* @param material
	* @return
	* @since 14/07/2014
	* @author Luiz Fernando
	*/
	public boolean existeMovimentacaoEstoque(Material material) {
		return movimentacaoestoqueDAO.existeMovimentacaoEstoque(material);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#existeMovimentacaoEstoque(Material material)
	*
	* @param material
	* @return
	* @since 03/04/2020
	* @author Diogo Souza
	*/
	public boolean existeMovimentacaoEstoqueNaoCancelada(Material material) {
		return movimentacaoestoqueDAO.existeMovimentacaoEstoqueNaoCancelada(material);
	}
	
	/**
	 * 
	 * Faz uma chamada em geraSaidaEntradaEquipamentoColaborador para cara elemento da listaColaboradorequipamento
	 * O m�todo leva em considera��o o transient cdlocalarmazenagem de colaboradorequipamento para definir o local da movimenta��o 
	 * 
	 * @param listaColaboradorequipamento, movimentacaoestoquetipo
	 * @since 23/07/2014
	 * @author Rafael Patr�cio
	 * 
	 
	 */
	public void geraSaidaEntradaEquipamentoColaborador(List<Colaboradorequipamento> listaColaboradorequipamento, Movimentacaoestoquetipo movimentacaoestoquetipo){
		
		if(listaColaboradorequipamento!=null && !listaColaboradorequipamento.isEmpty()){
			for (Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento) {
				
				if(colaboradorequipamento.getCdlocalarmazenagem()==null)
					throw new SinedException("O cd do local de armazenagem n�o pode ser null");
				
				geraSaidaEntradaEquipamentoColaborador(colaboradorequipamento, new Localarmazenagem(colaboradorequipamento.getCdlocalarmazenagem()), movimentacaoestoquetipo);
			}
		}
	}
	
	/**
	 * 
	 * @param colaboradorequipamento, localarmazenagem, movimentacaoestoquetipo
	 * @since 23/07/2014
	 * @author Rafael Patr�cio
	 * 
	 * Gera uma movimenta��o de estoque com base nos parametros passados
	 * 
	 */
	public void geraSaidaEntradaEquipamentoColaborador(Colaboradorequipamento colaboradorequipamento, Localarmazenagem localarmazenagem, 
			Movimentacaoestoquetipo movimentacaoestoquetipo){
		
		if(colaboradorequipamento == null || localarmazenagem == null || movimentacaoestoquetipo == null)
			throw new SinedException("Par�metros inv�lidos");
		
		if(colaboradorequipamento.getQtde()!=null && colaboradorequipamento.getQtde() > 0){
			//Gera a sa�da dos materiais no estoque
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			if(colaboradorequipamento.getColaborador() != null){
				movimentacaoestoqueorigem.setColaborador(colaboradorequipamento.getColaborador());
			}else {
				movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			}
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque(colaboradorequipamento.getEpi(), movimentacaoestoquetipo, 
					new Double(colaboradorequipamento.getQtde()), localarmazenagem, null, movimentacaoestoqueorigem, Materialclasse.EPI);
			if(Movimentacaoestoquetipo.ENTRADA.equals(movimentacaoestoquetipo) && colaboradorequipamento.getDtdevolucao() != null){
				movimentacaoestoque.setDtmovimentacao(colaboradorequipamento.getDtdevolucao());
			}else if(Movimentacaoestoquetipo.SAIDA.equals(movimentacaoestoquetipo) && colaboradorequipamento.getDtentrega() != null){
				movimentacaoestoque.setDtmovimentacao(colaboradorequipamento.getDtentrega());
			}
			this.saveOrUpdate(movimentacaoestoque);		
		}
	}
	
	public void validaEntregaEPI(Localarmazenagem localarmazenagem, Epi epi, Double quantidade) {
		
		Double quantidadedisponivel = this.getQtdDisponivelProdutoEpiServico(epi, Materialclasse.EPI, localarmazenagem);
		
		if(quantidadedisponivel - quantidade < 0)
			throw new SinedException("Estoque insuficiente no local indicado");
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInProducaoagenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 29/07/2014
	 */
	public List<Movimentacaoestoque> findByProducaoagenda(String whereInProducaoagenda) {
		return movimentacaoestoqueDAO.findByProducaoagenda(whereInProducaoagenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param coleta
	 * @return
	 * @author Rodrigo Freitas
	 * @since 30/07/2014
	 */
	public List<Movimentacaoestoque> findByColeta(Coleta coleta) {
		return movimentacaoestoqueDAO.findByColeta(coleta);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#getTotalQtdeValor(Material material, Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial)
	*
	* @param material
	* @param dtcustoinicial
	* @param qtdecustoinicial
	* @param valorcustoinicial
	* @return
	* @since 27/08/2014
	* @author Luiz Fernando
	*/
	public Movimentacaoestoque getTotalQtdeValor(Material material,	Date dtcustoinicial, Double qtdecustoinicial, Double valorcustoinicial) {
		return movimentacaoestoqueDAO.getTotalQtdeValor(material, dtcustoinicial, qtdecustoinicial, valorcustoinicial);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#updateValorEntradaByMovimentacaoestoque(Double valorEntrada, Movimentacaoestoque movimentacaoestoque)
	*
	* @param valorcusto
	* @param movimentacaoestoque
	* @since 28/08/2014
	* @author Luiz Fernando
	*/
	public void updateValorEntradaByMovimentacaoestoque(Double valorcusto, Movimentacaoestoque movimentacaoestoque) {
		movimentacaoestoqueDAO.updateValorEntradaByMovimentacaoestoque(valorcusto, movimentacaoestoque);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#updateValorCustoEntradaByMovimentacaoestoque(Double valorCusto, Movimentacaoestoque movimentacaoestoque)
	*
	* @param valorcusto
	* @param movimentacaoestoque
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public void updateValorCustoEntradaByMovimentacaoestoque(Double valorcusto, Movimentacaoestoque movimentacaoestoque) {
		movimentacaoestoqueDAO.updateValorCustoEntradaByMovimentacaoestoque(valorcusto, movimentacaoestoque);
	}
	
	public void updateValorCustoPorEmpresaByMovimentacaoestoque(Double valorCustoPorEmpresa, Movimentacaoestoque movimentacaoestoque) {
		movimentacaoestoqueDAO.updateValorCustoPorEmpresaByMovimentacaoestoque(valorCustoPorEmpresa, movimentacaoestoque);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findMovimentacoesWithMaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 29/08/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findMovimentacoesWithMaterial(String whereIn) {
		return movimentacaoestoqueDAO.findMovimentacoesWithMaterial(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByMaterial(Material material, Date dtinicio, Movimentacaoestoque movimentacaoestoque)
	*
	* @param material
	* @param dtinicio
	* @param movimentacaoestoque
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByMaterial(Material material, Date dtinicio, Movimentacaoestoque movimentacaoestoque, Empresa empresa) {
		return movimentacaoestoqueDAO.findByMaterial(material, dtinicio, movimentacaoestoque, empresa);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#getMovimentacaoAnterior(Material material, Movimentacaoestoque meAtual)
	*
	* @param material
	* @param meAtual
	* @return
	* @since 24/09/2014
	* @author Luiz Fernando
	*/
	public Movimentacaoestoque getMovimentacaoAnterior(Material material, Movimentacaoestoque meAtual) {
		return movimentacaoestoqueDAO.getMovimentacaoAnterior(material, meAtual);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#existeMovimentacaoestoqueByRequisicao(Requisicao requisicao)
	*
	* @param requisicao
	* @return
	* @since 20/01/2015
	* @author Luiz Fernando
	*/
	public Boolean existeMovimentacaoestoqueByRequisicao(Requisicao requisicao) {
		return movimentacaoestoqueDAO.existeMovimentacaoestoqueByRequisicao(requisicao);
	}
		
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQtdDisponivelProdutoEpiServico(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Boolean naoconsiderargrade, Loteestoque loteestoque)
	* @see br.com.linkcom.sined.geral.service.PedidovendamaterialService#getQtdeReservada(Material material, Empresa empresa, Localarmazenagem localarmazenagem, Loteestoque loteestoque)
	* 
	* @param material
	* @param materialclasse
	* @param localarmazenagem
	* @param loteestoque
	* @return
	* @since 20/03/2015
	* @author Luiz Fernando
	*/
	public Double getQtdDisponivelProdutoEpiServicoFromConferenciaComReserva(Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagem, Empresa empresa, Projeto projeto, Loteestoque loteestoque) {
		Double qtdeDispoinivel = getQtdDisponivelProdutoEpiServico(material, materialclasse, localarmazenagem, empresa, projeto, null, loteestoque, false);
		Double qtdeReservada = pedidovendamaterialService.getQtdeReservada(material, empresa, localarmazenagem, loteestoque);
		
		if(qtdeDispoinivel == null)
			qtdeDispoinivel = 0d;
		if(qtdeReservada == null)
			qtdeReservada = 0d;
				
		return qtdeDispoinivel - qtdeReservada;
	}
	
	/**
	 * Busca os registros de invent�rio para o SPED
	 *
	 * @param filtro
	 * @return
	 */
	public List<Movimentacaoestoque> findForSpedRegistroK230(SpedarquivoFiltro filtro) {
		return movimentacaoestoqueDAO.findForSpedRegistroK230(filtro);
	}
	
	public List<MateriaprimaBean> findForSpedRegistroK235(SpedarquivoFiltro filtro, Material material, Producaoordem producaoordem) {
		return movimentacaoestoqueDAO.findForSpedRegistroK235(filtro, material, producaoordem);
	}
	
	/**
	 * @param entrega
	 * @return
	 * @since 09/06/2015
	 * @author Andrey Leonardo
	 */
	public Boolean existeMovimentacaoestoqueByEntrega(Entrega entrega, Boolean notCancelada){
		return movimentacaoestoqueDAO.existeMovimentacaoestoqueByEntrega(entrega, notCancelada);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 * @param whereIn
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public List<Movimentacaoestoque> findByColeta(String whereIn) {
		return movimentacaoestoqueDAO.findByColeta(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 * @param coleta
	 * @param material
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public Movimentacaoestoque findByColetaAvulsaAndMaterial(Coleta coleta, Material material) {
		return movimentacaoestoqueDAO.findByColetaAvulsaAndMaterial(coleta, material);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 * @param cdmovimentacaoestoque
	 * @param dtCancelamento
	 * @param observacao
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public void updateDataCancelamentoMovimentacao(Integer cdmovimentacaoestoque, Date dtCancelamento, String observacao){
		movimentacaoestoqueDAO.updateDataCancelamentoMovimentacao(cdmovimentacaoestoque, dtCancelamento, observacao);
	}
	
	/**
	 * M�todo com refer�ncia ao DAO
	 * @param cdmovimentacaoestoque
	 * @param quantidade
	 * @since 30/06/2015
	 * @author Danilo Guimar�es
	 */
	public void updateQuantidadeMovimentacao(Integer cdmovimentacaoestoque, Double quantidade, Double valor){
		movimentacaoestoqueDAO.updateQuantidadeMovimentacao(cdmovimentacaoestoque, quantidade, valor);
	}
	
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByRequisicaomaterial(String whereIn, boolean somenteRequisicao)
	*
	* @param requisicaomaterial
	* @param somenteRequisicao
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByRequisicaomaterial(Requisicaomaterial requisicaomaterial, boolean somenteRequisicao){
		return movimentacaoestoqueDAO.findByRequisicaomaterial(requisicaomaterial.getCdrequisicaomaterial().toString(), somenteRequisicao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByRequisicaomaterial(String whereIn, boolean somenteRequisicao)
	*
	* @param whereIn
	* @param somenteRequisicao
	* @return
	* @since 04/09/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByRequisicaomaterial(String whereIn, boolean somenteRequisicao){
		return movimentacaoestoqueDAO.findByRequisicaomaterial(whereIn, somenteRequisicao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see 
	*
	* @param venda
	* @return
	* @since 07/10/2015
	* @author Luiz Fernando
	*/
	public Boolean existeMovimentacaoestoque(Venda venda) {
		return movimentacaoestoqueDAO.existeMovimentacaoestoque(venda, null);
	}
	
	public Boolean existeMovimentacaoestoque(Venda venda, Expedicao expedicao) {
		return movimentacaoestoqueDAO.existeMovimentacaoestoque(venda, expedicao);
	}
	
	public Boolean existeMovimentacaoestoqueSomenteVenda(Venda venda) {
		return movimentacaoestoqueDAO.existeMovimentacaoestoqueSomenteVenda(venda);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#findByProducaoordem(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 22/12/2015
	* @author Luiz Fernando
	*/
	public List<Movimentacaoestoque> findByProducaoordem(String whereIn, Movimentacaoestoquetipo movimentacaoestoquetipo, Boolean registrarproducao, Boolean estornoproducao){
		return movimentacaoestoqueDAO.findByProducaoordem(whereIn, movimentacaoestoquetipo, registrarproducao, estornoproducao);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.MovimentacaoestoqueDAO#updateDataCancelamentoMateriaprimaByProducaoagenda(String whereInProducaoagenda, String whereInMaterial)
	*
	* @param whereInProducaoagenda
	* @param whereInMaterial
	* @since 15/04/2016
	* @author Luiz Fernando
	*/
	public void updateDataCancelamentoMateriaprimaByProducaoagenda(String whereInProducaoagenda, String whereInMaterial) {
		movimentacaoestoqueDAO.updateDataCancelamentoMateriaprimaByProducaoagenda(whereInProducaoagenda, whereInMaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param empresa
	 * @param localarmazenagem 
	 * @return
	 * @author Rodrigo Freitas
	 * @param produtos 
	 * @since 27/04/2016
	 */
	public List<ConsultarEstoqueWSBean> findForWSSOAP(Empresa empresa, Localarmazenagem localarmazenagem, String produtos) {
		return movimentacaoestoqueDAO.findForWSSOAP(empresa, localarmazenagem, produtos);
	}
	
	/**
	 * Baixar estoque a partir do WS SOAP
	 *
	 * @param bean
	 * @author Rodrigo Freitas
	 * @since 09/05/2016
	 */
	public boolean baixarEstoqueWSSOAP(BaixarEstoqueBean bean) {
		Date data = bean.getData();
		Integer codigo = bean.getCodigo();
		Double quantidade = bean.getQuantidade();
		Integer origem = bean.getOrigem();
		String observacao = "Baixa feita pelo Webservice SOAP." + (origem != null ? " OS " + origem : "");
		
		String cnpj_empresa_string = bean.getEmpresa();
		Cnpj cnpj_empresa = new Cnpj(cnpj_empresa_string);
		Empresa empresa = empresaService.findByCnpjWSSOAP(cnpj_empresa);
		if(empresa == null){
			throw new SinedException("Empresa n�o encontrada com o CNPJ '" + cnpj_empresa_string + "'.");
		}
		
		boolean permitirEstoqueNegativo = false;
		
		Localarmazenagem localarmazenagem = null;
		if(bean.getLocalarmazenagem() != null){
			localarmazenagem = localarmazenagemService.load(new Localarmazenagem(bean.getLocalarmazenagem()), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
		}
		
		if(localarmazenagem == null){
			Integer cdlocalarmazenagem = ParametrogeralService.getInstance().getIntegerNullable(Parametrogeral.INTEGRACAO_PEDIDOVENDA_LOCALARMAZENAGEM);
			if(cdlocalarmazenagem != null){
				localarmazenagem = localarmazenagemService.load(new Localarmazenagem(cdlocalarmazenagem), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.permitirestoquenegativo");
			}
		}
		permitirEstoqueNegativo = localarmazenagem != null && localarmazenagem.getPermitirestoquenegativo() != null && localarmazenagem.getPermitirestoquenegativo();
		
		Material material = materialService.load(new Material(codigo), "material.cdmaterial, material.nome, material.unidademedida, material.servico");
		if(material == null){
			throw new SinedException("Material n�o encontrado com o c�digo '" + codigo + "'.");
		}
		Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
		movimentacaoestoque.setMaterial(material);
		materialService.validateObrigarLote(movimentacaoestoque);
		
		if(!permitirEstoqueNegativo && (material.getServico() == null || !material.getServico())){
			Double entrada = this.getQuantidadeDeMaterialEntrada(material, localarmazenagem, empresa, null);
			if (entrada == null){
				entrada = 0d;
			}
			Double saida = this.getQuantidadeDeMaterialSaida(material, localarmazenagem, empresa, null);
			if (saida == null){
				saida = 0d;
			}

			Double qtddisponivel = SinedUtil.getQtdeEntradaSubtractSaida(entrada, saida);
			
			if(quantidade > qtddisponivel){
				return false;
			}
		}
		
		Pedidovenda pedidovenda = pedidovendaService.findByIdentificadorIntegracao(origem);
		
		
		
		Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
		movimentacaoestoqueorigem.setPedidovenda(pedidovenda);
		movimentacaoestoqueorigemService.saveOrUpdate(movimentacaoestoqueorigem);
		
		movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);		
		movimentacaoestoque.setQtde(quantidade);
		movimentacaoestoque.setDtmovimentacao(data);
		movimentacaoestoque.setLocalarmazenagem(localarmazenagem);
		movimentacaoestoque.setEmpresa(empresa);
		movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
		movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
		movimentacaoestoque.setObservacao(observacao);
		this.saveOrUpdate(movimentacaoestoque);
		
		return true;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @author Rafael Salvio
	 */
	public void ajusteEstoqueFromWms(ConsultarEstoqueBean bean) {
		movimentacaoestoqueDAO.ajusteEstoqueFromWms(bean);
	}
	
	public List<Movimentacaoanimalmotivo> motivosPorTipo(Movimentacaoestoquetipo tipo){
		if(tipo==null || parametrogeralService.getBoolean("CONTROLE_GADO")!=true){
			return null;
		}
		List<Movimentacaoanimalmotivo> motivos =  new ArrayList<Movimentacaoanimalmotivo>();
		if(Movimentacaoestoquetipo.SAIDA.equals(tipo)){
			motivos.add(Movimentacaoanimalmotivo.MORTE);
			motivos.add(Movimentacaoanimalmotivo.MUDANCA_CATEGORIA);
			motivos.add(Movimentacaoanimalmotivo.TRANSFERENCIA);
			motivos.add(Movimentacaoanimalmotivo.VENDA);
		}else if(Movimentacaoestoquetipo.ENTRADA.equals(tipo)){
			motivos.add(Movimentacaoanimalmotivo.COMPRA);
			motivos.add(Movimentacaoanimalmotivo.NASCIMENTO);
			motivos.add(Movimentacaoanimalmotivo.MUDANCA_CATEGORIA);
			motivos.add(Movimentacaoanimalmotivo.TRANSFERENCIA);
		}
		return motivos;
	}
	
	public void validaTransferenciaCategoria(WebRequestContext request, TransferenciaCategoriaBean bean){
		Map<Material, Double> mapaMaterialQtde = new HashMap<Material, Double>();
		for (TransferenciaCategoriaMaterialBean it : bean.getListaMateriais()) {
			if(it.getQuantidadetransferir() != null && it.getQuantidadetransferir() > 0.0){
				if(mapaMaterialQtde.containsKey(it.getMaterialorigem())){
					mapaMaterialQtde.put(it.getMaterialorigem(), mapaMaterialQtde.get(it.getMaterialorigem()) + it.getQuantidadetransferir());
				}else{
					mapaMaterialQtde.put(it.getMaterialorigem(), it.getQuantidadetransferir());
				}
				Material mat = null;
				mat  = materialService.load(it.getMaterialorigem(), "material.cdmaterial, material.materialgrupo");
				it.getMaterialorigem().setMaterialgrupo(mat.getMaterialgrupo());
				mat  = materialService.load(it.getMaterialdestino(), "material.cdmaterial, material.materialgrupo");
				it.getMaterialdestino().setMaterialgrupo(mat.getMaterialgrupo());
			}
		}
		
		if(mapaMaterialQtde.size() == 0){
			request.addError("Quantidade para transfer�ncia deve ser maior que zero.");
		}else{
			for(Material material: mapaMaterialQtde.keySet()){
				Double qtdeDisponivel = materialService.findEstoqueAtual(material.getCdmaterial().toString(), bean.getLocalarmazenagem(), bean.getEmpresa());
				if(qtdeDisponivel < mapaMaterialQtde.get(material)){
					request.addError("A quantidade selecionada ("+mapaMaterialQtde.get(material)+") do material "+material.getNome()+" � maior que a quantidade dispon�vel ("+qtdeDisponivel+") em estoque.");
				}
			}			
		}
	}
	
	public void transferenciaCategoria(TransferenciaCategoriaBean bean){
		String dataAtualStr = SinedDateUtils.toString(SinedDateUtils.currentDate());
		for (TransferenciaCategoriaMaterialBean it : bean.getListaMateriais()) {
			if(it.getQuantidadetransferir() <= 0.0){
				continue;
			}
			Double qtdeTransferencia = it.getQuantidadetransferir();
			
			Material material = it.getMaterialorigem();
			Materialclasse materialclasse = Materialclasse.PRODUTO;
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoque.setDtmovimentacao(bean.getDataestoque());
			movimentacaoestoque.setQtde(qtdeTransferencia);
			movimentacaoestoque.setLocalarmazenagem(bean.getLocalarmazenagem());
			movimentacaoestoque.setEmpresa(bean.getEmpresa());
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoque.setMaterialclasse(materialclasse);
			movimentacaoestoque.setObservacao("Transfer�ncia de categoria no dia " + dataAtualStr);
			if(it.getMaterialorigem().getMaterialgrupo() != null && Boolean.TRUE.equals(it.getMaterialorigem().getMaterialgrupo().getAnimal())){
				movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.MUDANCA_CATEGORIA);
			}
			//movimentacaoestoque.setLoteestoque(it.getLoteestoque());
			
			if(Boolean.TRUE.equals(it.getRegistrarpesomedio())){
				movimentacaoestoque.setPesomedio(it.getPesomedio());
				materialService.updatePesoBruto(material, it.getPesomedio());
			}
			
			this.saveOrUpdate(movimentacaoestoque);
			
			
			material = it.getMaterialdestino();
			materialclasse = Materialclasse.PRODUTO;
			
			movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
			movimentacaoestoque.setDtmovimentacao(bean.getDataestoque());
			movimentacaoestoque.setQtde(qtdeTransferencia);
			movimentacaoestoque.setLocalarmazenagem(bean.getLocalarmazenagem());
			movimentacaoestoque.setEmpresa(bean.getEmpresa());
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoque.setMaterialclasse(materialclasse);
			movimentacaoestoque.setObservacao("Transfer�ncia de categoria no dia " + dataAtualStr);
			if(it.getMaterialdestino().getMaterialgrupo() != null && Boolean.TRUE.equals(it.getMaterialdestino().getMaterialgrupo().getAnimal())){
				movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.MUDANCA_CATEGORIA);
			}
			//movimentacaoestoque.setLoteestoque(it.getLoteestoque());
			
			if(Boolean.TRUE.equals(it.getRegistrarpesomedio())){
				movimentacaoestoque.setPesomedio(it.getPesomedio());
				materialService.updatePesoBruto(material, it.getPesomedio());
			}
			
			this.saveOrUpdate(movimentacaoestoque);
		}		
	}
	
	public void validaRegistrarMorte(WebRequestContext request, List<RegistroMovimentacaoAnimalMaterialBean> listaMovimentos){
		for(RegistroMovimentacaoAnimalMaterialBean bean: listaMovimentos){
			if(bean.getQuantidade() == null || bean.getQuantidade() == 0){
				continue;
			}

			Double estoqueAtual = materialService.findEstoqueAtual(bean.getMaterial().getCdmaterial().toString(), bean.getLocalarmazenagem(), bean.getEmpresa());
			if(estoqueAtual < bean.getQuantidade()){
				request.addError("Quantidade em estoque do material "+bean.getMaterial().getNome()+" � menor que quantidade de informada para registro de mortes.");
			}
		}
	}
	
	public void registrarMorteAnimal(List<RegistroMovimentacaoAnimalMaterialBean> listaMovimentos){
		for(RegistroMovimentacaoAnimalMaterialBean bean: listaMovimentos){
			if(bean.getQuantidade() == null || bean.getQuantidade() == 0){
				continue;
			}
				
			Material material = bean.getMaterial();
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.SAIDA);
			movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoque.setQtde(bean.getQuantidade());
			movimentacaoestoque.setLocalarmazenagem(bean.getLocalarmazenagem());
			movimentacaoestoque.setEmpresa(bean.getEmpresa());
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
			movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.MORTE);
			if(bean.getValor() != null){
				movimentacaoestoque.setValor(bean.getValor().getValue().doubleValue());
			}else if(material.getValorcusto() != null){
				movimentacaoestoque.setValorcusto(material.getValorcusto());
			}
			
			if(Boolean.TRUE.equals(bean.getRegistrapesomedio())){
				movimentacaoestoque.setPesomedio(bean.getPesomedio());
			}
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				movimentacaoestoque.setContaGerencial(movimentacaoestoque.getMaterial().getMaterialRateioEstoque() != null ? movimentacaoestoque.getMaterial().getMaterialRateioEstoque().getContaGerencialPerda() : null);
				if(movimentacaoestoque.getProjetoRateio() == null){
					movimentacaoestoque.setProjetoRateio(movimentacaoestoque.getProjeto() != null ? movimentacaoestoque.getProjeto() : movimentacaoestoque.getMaterial().getMaterialRateioEstoque() != null ? movimentacaoestoque.getMaterial().getMaterialRateioEstoque().getProjeto() : null);
				}
				movimentacaoestoque.setCentrocusto(bean.getCentrocusto());
				movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque));
			}
			this.saveOrUpdate(movimentacaoestoque);
			salvarHisotirico(movimentacaoestoque, "Criado a partir do registro de morte de animal", MovimentacaoEstoqueAcao.CRIAR);
		}
	}
	
	public void registrarNascimentoAnimal(List<RegistroMovimentacaoAnimalMaterialBean> listaMovimentos){
		for(RegistroMovimentacaoAnimalMaterialBean bean: listaMovimentos){
			Material material = bean.getMaterial();
			
			Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
			movimentacaoestoqueorigem.setUsuario(SinedUtil.getUsuarioLogado());
			
			Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque();
			movimentacaoestoque.setMaterial(material);
			movimentacaoestoque.setMovimentacaoestoquetipo(Movimentacaoestoquetipo.ENTRADA);
			movimentacaoestoque.setDtmovimentacao(SinedDateUtils.currentDate());
			movimentacaoestoque.setQtde(bean.getQuantidade());
			movimentacaoestoque.setLocalarmazenagem(bean.getLocalarmazenagem());
			movimentacaoestoque.setEmpresa(bean.getEmpresa());
			movimentacaoestoque.setMovimentacaoestoqueorigem(movimentacaoestoqueorigem);
			movimentacaoestoque.setMaterialclasse(Materialclasse.PRODUTO);
			movimentacaoestoque.setMovimentacaoanimalmotivo(Movimentacaoanimalmotivo.NASCIMENTO);
			if(bean.getValor() != null){
				movimentacaoestoque.setValor(bean.getValor().getValue().doubleValue());
			}else if(material.getValorcusto() != null){
				movimentacaoestoque.setValorcusto(material.getValorcusto());
			}
			
			if(Boolean.TRUE.equals(bean.getRegistrapesomedio())){
				movimentacaoestoque.setPesomedio(bean.getPesomedio());
			}
			
			if(SinedUtil.isRateioMovimentacaoEstoque()){
				movimentacaoestoque.setContaGerencial(movimentacaoestoque.getMaterial().getMaterialRateioEstoque() != null ? movimentacaoestoque.getMaterial().getMaterialRateioEstoque().getContaGerencialEntrada() : null);
				if(movimentacaoestoque.getProjetoRateio() == null){
					movimentacaoestoque.setProjetoRateio(movimentacaoestoque.getProjeto() != null ? movimentacaoestoque.getProjeto() : movimentacaoestoque.getMaterial().getMaterialRateioEstoque() != null ? movimentacaoestoque.getMaterial().getMaterialRateioEstoque().getProjeto() : null);
				}
				movimentacaoestoque.setCentrocusto(bean.getCentrocusto());
				movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque));
			}
			this.saveOrUpdate(movimentacaoestoque);
			salvarHisotirico(movimentacaoestoque, "Criado a partir do registro de nascimento de animal", MovimentacaoEstoqueAcao.CRIAR);
		}
	}
	
	public List<DinamicaRebanhoBean> findForDinamicaRebanho(DinamicaRebanhoFiltro filtro){
		return movimentacaoestoqueDAO.findForDinamicaRebanho(filtro);
	}
	public List<Movimentacaoestoque> buscarPorCentroCusto(boolean csv, Centrocusto centroCusto, Date dtInicio, Date dtFim, Empresa empresa) {
		return movimentacaoestoqueDAO.buscarPorCentroCusto(csv, centroCusto,dtInicio,dtFim,empresa);
	}
	public Resource gerarDinamicaRebanho(WebRequestContext request, DinamicaRebanhoFiltro filtro) throws IOException{
		List<DinamicaRebanhoBean> listaDinamicarebanho = this.findForDinamicaRebanho(filtro);
		return this.geraDinamicaRebanhoExcel(request, listaDinamicarebanho, filtro);
	}
	
	private Resource geraDinamicaRebanhoExcel(WebRequestContext request, List<DinamicaRebanhoBean> listaDinamicarebanho, DinamicaRebanhoFiltro filtro) throws IOException{
		BigDecimal totalEstoqueInicial = BigDecimal.ZERO;
		BigDecimal totalNascimento = BigDecimal.ZERO;
		BigDecimal totalMorte = BigDecimal.ZERO;
		BigDecimal totalEntradaTransf = BigDecimal.ZERO;
		BigDecimal totalSaidaTransf = BigDecimal.ZERO;
		BigDecimal totalEntradaMudancaCat = BigDecimal.ZERO;
		BigDecimal totalSaidaMudancaCat = BigDecimal.ZERO;
		BigDecimal totalCompra = BigDecimal.ZERO;
		BigDecimal totalVenda = BigDecimal.ZERO;
		BigDecimal totalEstoqueFinal = BigDecimal.ZERO;

		if(listaDinamicarebanho != null && listaDinamicarebanho.size() > 0){
			for (DinamicaRebanhoBean bean : listaDinamicarebanho) {
				totalEstoqueInicial = totalEstoqueInicial.add(bean.getQtdeestoqueinicial());
				totalNascimento = totalNascimento.add(bean.getQtdenascimento());
				totalMorte = totalMorte.add(bean.getQtdemorte());
				totalEntradaTransf = totalEntradaTransf.add(bean.getQtdeentradatransferencia());
				totalSaidaTransf = totalSaidaTransf.add(bean.getQtdesaidatransferencia());
				totalEntradaMudancaCat = totalEntradaMudancaCat.add(bean.getQtdeentradamudancacategoria());
				totalSaidaMudancaCat = totalSaidaMudancaCat.add(bean.getQtdesaidamudancacategoria());
				totalVenda = totalVenda.add(bean.getQtdevenda());
				totalCompra = totalCompra.add(bean.getQtdecompra());
				totalEstoqueFinal = totalEstoqueFinal.add(bean.getQtdeestoquefinal());
			}
			
			if(filtro.getMaterialcategoria() == null && filtro.getMaterialgrupo() == null &&
					filtro.getMaterial() == null && totalEntradaMudancaCat.compareTo(totalSaidaMudancaCat) != 0){
				request.addError("Houve inconformidade no per�odo informado. O total de entradas e sa�das para Mudan�a de categoria deveria ser igual.<br>");
			}
			
			if(filtro.getLocalarmazenagem() == null && totalEntradaTransf.compareTo(totalSaidaTransf)!=0){
				request.addError("Houve inconformidade no per�odo informado. O total de entradas e sa�das para Transfer�ncia deveria ser igual.");				
			}
			for(Message msg: request.getMessages()){
				if(MessageType.ERROR.equals(msg.getType())){
					SinedUtil.redirecionamento(request, "/suprimento/relatorio/Emitirdinamicarebanho");
					return null;
				}
			}
		}
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet planilha = wb.createSheet("Planilha");
        planilha.setDefaultColumnWidth((short) 35);
		planilha.addMergedRegion(new Region(1, (short)1, 1, (short)3));
		planilha.addMergedRegion(new Region(1, (short)4, 1, (short)5));
		planilha.addMergedRegion(new Region(1, (short)6, 1, (short)7));
		planilha.addMergedRegion(new Region(1, (short)8, 1, (short)9));
		planilha.addMergedRegion(new Region(0, (short)1, 0, (short)10));
        
        HSSFRow row = null;
		HSSFCell cell = null;
        
		row = planilha.createRow((short) 0);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		cell.setCellValue("Fazenda");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		if (filtro.getEmpresa()!=null){
			Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.nome, empresa.razaosocial");
			cell.setCellValue(empresa.getRazaosocialOuNome());
		}else {
			cell.setCellValue("");			
		}
		
		short tamColunaValor = (short) (100*35);
		
		planilha.setColumnWidth((short) 0, (short) (250*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
		planilha.setColumnWidth((short) 2, (short) (120*35));
		planilha.setColumnWidth((short) 3, tamColunaValor);
		planilha.setColumnWidth((short) 4, tamColunaValor);
		planilha.setColumnWidth((short) 5, tamColunaValor);
		planilha.setColumnWidth((short) 6, tamColunaValor);
		planilha.setColumnWidth((short) 7, tamColunaValor);
		planilha.setColumnWidth((short) 8, tamColunaValor);
		planilha.setColumnWidth((short) 9, tamColunaValor);
		planilha.setColumnWidth((short) 10, (short) (150*35));
		
		row = planilha.createRow(1);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Transfer�ncia");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Mudan�a de categoria");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_LEFT);
		
		row = planilha.createRow(2);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Material");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Estoque inicial");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Nascimento");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Morte");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Entrada");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Sa�da");
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Venda");
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Compra");
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Entrada");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Sa�da");
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		cell.setCellValue("Estoque final");
		int i = 0;
		for (DinamicaRebanhoBean bean : listaDinamicarebanho) {
			row = planilha.createRow(i + 3);
			
			cell = row.createCell((short) 0);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
			cell.setCellValue(bean.getMaterial());
			
			cell = row.createCell((short) 1);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdeestoqueinicial().toString());
			
			cell = row.createCell((short) 2);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdenascimento().toString());			
			
			cell = row.createCell((short) 3);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdemorte().toString());			
			
			cell = row.createCell((short) 4);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdeentradatransferencia().toString());			
			
			cell = row.createCell((short) 5);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdesaidatransferencia().toString());
			
			cell = row.createCell((short) 6);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdevenda().toString());
			
			cell = row.createCell((short) 7);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdecompra().toString());
			
			cell = row.createCell((short) 8);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdeentradamudancacategoria().toString());
			
			cell = row.createCell((short) 9);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdesaidamudancacategoria().toString());
			
			cell = row.createCell((short) 10);
			cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
			cell.setCellValue(bean.getQtdeestoquefinal().toString());
			i++;
		}
		
		row = planilha.createRow(i+3);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue("Total");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalEstoqueInicial.toString());
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalNascimento.toString());
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalMorte.toString());
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalEntradaTransf.toString());
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalSaidaTransf.toString());
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalVenda.toString());
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalCompra.toString());
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalEntradaMudancaCat.toString());
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalSaidaMudancaCat.toString());
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_GREY);
		cell.setCellValue(totalEstoqueFinal.toString());
		
		Resource resource = wb.getWorkBookResource("dinamicarebanho_" + SinedUtil.datePatternForReport() + ".xls");
		return resource;
	}
	
	public List<Movimentacaoestoque> findForIndenizacaoContrato(String whereIn, Material material){
		return movimentacaoestoqueDAO.findForIndenizacaoContrato(whereIn, material);
	}
	
	public Movimentacaoestoque loadUltimaMovimentacaoestoque(Material material, Empresa empresa){
		return movimentacaoestoqueDAO.loadUltimaMovimentacaoestoque(material, empresa);
	}

	public void atualizaValorRateioPorCustoMedeio(Integer cdMovimentacaoEstoque) {
		if(cdMovimentacaoEstoque != null){
			Movimentacaoestoque bean = movimentacaoestoqueDAO.buscarValorCustoMedio(cdMovimentacaoEstoque);
			if(bean != null && bean.getValorcusto() != null){
				rateioService.atualizaValorRateio(rateioService.findRateio(bean.getRateio()), new Money(bean.getValorcusto()));
			}
		}
	}
	
	public void salvarHisotirico(Movimentacaoestoque movimentacaoestoque,String observacao,MovimentacaoEstoqueAcao acao){
		movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, acao, observacao, true);
	}
	
	public void salvarHisotirico(Movimentacaoestoque movimentacaoestoque,String observacao,MovimentacaoEstoqueAcao acao, Boolean useTransaction){
		movimentacaoEstoqueHistoricoService.preencherHistorico(movimentacaoestoque, acao, observacao, useTransaction);
	}
	public void updateCdRateio(Integer cdEstoque, Integer cdrateio) {
		movimentacaoestoqueDAO.updateCdRateio(cdEstoque,cdrateio);
	}
	
	public void roundQtdeByUnidadeMedida(Movimentacaoestoque movimentacaoestoque) {
		if(movimentacaoestoque != null && movimentacaoestoque.getQtde() != null && movimentacaoestoque.getQtde() > 0 && movimentacaoestoque.getMaterial() != null){
			Material aux_material = materialService.load(movimentacaoestoque.getMaterial(), "material.cdmaterial, material.unidademedida");
			if(aux_material != null){
				if(aux_material.getUnidademedida() != null && aux_material.getUnidademedida().getCasasdecimaisestoque() != null){
					movimentacaoestoque.setQtde(SinedUtil.roundByUnidademedida(movimentacaoestoque.getQtde(), aux_material.getUnidademedida()));
				}
			}
		}
	}
	
	public List<Movimentacaoestoque> findByInventario(String whereIn) {
		return movimentacaoestoqueDAO.findByInventario(whereIn);
	}
	
	public List<Movimentacaoestoque> findbyVendaList(Venda venda, Material material) {
		return movimentacaoestoqueDAO.findbyVendaList(venda, material);
	}

	public List<Movimentacaoestoque> findMovimentacoesSaidaMateriasProducao(SpedarquivoFiltro filtro) {
		return movimentacaoestoqueDAO.findMovimentacoesSaidaMateriasProducao(filtro);
	}
	public List<Movimentacaoestoque> findMateriasPrimasOrdemAvulsaForK235(SpedarquivoFiltro filtro, Integer cdProducaoOrdemOuAgenda, Integer cdMaterialProducao, List<Tipoitemsped> listaTipoitemsped) {
		return movimentacaoestoqueDAO.findMateriasPrimasOrdemAvulsaForK235(filtro, cdProducaoOrdemOuAgenda, cdMaterialProducao, listaTipoitemsped);
	}
	public List<Movimentacaoestoque> findMateriasPrimasOrdemAgenda(SpedarquivoFiltro filtro, Integer cdProducaoOrdemOuAgenda, Integer cdMaterialProducao, List<Tipoitemsped> listaTipoitemsped) {
		return movimentacaoestoqueDAO.findMateriasPrimasOrdemAgenda(filtro, cdProducaoOrdemOuAgenda, cdMaterialProducao, listaTipoitemsped);
	}
	
	public void preencheValorCusto (double valorEntrada, Movimentacaoestoque movimentacaoestoque){
		movimentacaoestoqueService.updateValorEntradaByMovimentacaoestoque(valorEntrada, movimentacaoestoque);
		
		if(parametrogeralService.getBoolean(Parametrogeral.ATUALIZA_MATERIALCUSTO_MOVIMENTACAOENTREGA)){
			materialService.updateValorCusto(movimentacaoestoque.getMaterial(), valorEntrada);
			
			List<MaterialCustoEmpresa> listaMaterialCustoEmpresa = materialCustoEmpresaService
					.findByMaterial(movimentacaoestoque.getMaterial(), movimentacaoestoque.getEmpresa());
			
			if(SinedUtil.isListEmpty(listaMaterialCustoEmpresa)) {
				materialService.preencherMaterialCustoPorEmpresa(movimentacaoestoque.getMaterial(), movimentacaoestoque, null);
				listaMaterialCustoEmpresa = materialCustoEmpresaService.findByMaterial(movimentacaoestoque.getMaterial(), null);
			}
			
			if(SinedUtil.isListNotEmpty(listaMaterialCustoEmpresa)) {
				for(MaterialCustoEmpresa mce : listaMaterialCustoEmpresa) {
					Movimentacaoestoque movimentacaoestoquePorEmpresa = movimentacaoestoqueService.loadUltimaMovimentacaoestoque(movimentacaoestoque.getMaterial(), mce.getEmpresa());
					
					if(movimentacaoestoque.getEmpresa() != null 
							&& movimentacaoestoque.getEmpresa().equals(mce.getEmpresa())
							&& movimentacaoestoquePorEmpresa != null 
							&& movimentacaoestoquePorEmpresa.getValor() != null 
							&& movimentacaoestoquePorEmpresa.getMaterial() != null) {
						
						materialCustoEmpresaService.updateValorUltimaCompra(mce, movimentacaoestoquePorEmpresa.getValor());
						materialCustoEmpresaService.updateValorCusto(mce, movimentacaoestoquePorEmpresa.getValor());
					}
				}
			}
		}else if(parametrogeralService.getBoolean(Parametrogeral.CALCULAR_CUSTO_MEDIO)){
			materialService.recalcularCustoMedio(movimentacaoestoque.getMaterial().getCdmaterial().toString(), movimentacaoestoque);
		}
	}
	
	public List<Movimentacaoestoque> findByNotaFiscalProdutoItem(String whereIn) {
		return movimentacaoestoqueDAO.findByNotaFiscalProdutoItem(whereIn);
	}
	
	public boolean existeMovimentacaoEstoqueMaterialLote(Loteestoque bean, Material material) {
		return movimentacaoestoqueDAO.existeMovimentacaoEstoqueMaterialLote(bean, material);
	}
	
	public List<Movimentacaoestoque> findForRegistroK210(SpedarquivoFiltro filtro, String whereIn) {
		return movimentacaoestoqueDAO.findForRegistroK210(filtro, whereIn);
	}
	public Movimentacaoestoque findWithLoteestoque(Movimentacaoestoque bean) {
		return movimentacaoestoqueDAO.findWithLoteestoque(bean);
	}
}
