package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Entrega;
import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.dao.EntregadocumentoDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.process.filter.GerarLancamentoContabilFiltro;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EntregadocumentoService extends GenericService<Entregadocumento> {
	
	private EntregadocumentoDAO entregadocumentoDAO;
	
	public void setEntregadocumentoDAO(EntregadocumentoDAO entregadocumentoDAO) {
		this.entregadocumentoDAO = entregadocumentoDAO;
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListEntregaDocumentoByEntrega
	 * @param entrega
	 * @return
	 * @return List<Entregadocumento>
	 * @author Thiago Augusto
	 * @date 03/08/2012
	 *
	 */
	public List<Entregadocumento> findListEntregaDocumentoByEntrega(Entrega entrega){
		return entregadocumentoDAO.findListEntregaDocumentoByEntrega(entrega); 
	}
	
	public String findNatop(Integer cdentrega){
		return entregadocumentoDAO.findNatop(cdentrega);
	}	

	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 * @name findListForValidaRomaneioEntrega
	 * @param entrega
	 * @return List<Entregadocumento>
	 * @author Lucas Costa
	 * @date 05/11/2014
	 *
	 */
	public List<Entregadocumento> findListForValidaRomaneioEntrega(Entrega entrega){
		return entregadocumentoDAO.findListForValidaRomaneioEntrega(entrega);
	}
	
	public List<Entregadocumento> findForGerarLancamentoContabil(GerarLancamentoContabilFiltro filtro, String whereInNaturezaOperacao){
		return entregadocumentoDAO.findForGerarLancamentoContabil(filtro, whereInNaturezaOperacao);
	}
	
	public List<Entregadocumento> findForGerarLancamentoContabilWithApropriacao(GerarLancamentoContabilFiltro filtro, String whereInNaturezaOperacao){
		return entregadocumentoDAO.findForGerarLancamentoContabilWithApropriacao(filtro, whereInNaturezaOperacao);
	}
	
	public List<Entregadocumento>findForAutoCompleteProcessoReferenciado(String numero){
		return entregadocumentoDAO.findForAutoCompleteProcessoReferenciado(numero);
	}
}