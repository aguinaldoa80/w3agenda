package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.dao.LoteConsultaDfeItemDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeItem;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class LoteConsultaDfeItemService extends GenericService<LoteConsultaDfeItem> {

	private LoteConsultaDfeItemDAO loteConsultaDfeItemDAO;
	
	public void setLoteConsultaDfeItemDAO(LoteConsultaDfeItemDAO loteConsultaDfeItemDAO) {
		this.loteConsultaDfeItemDAO = loteConsultaDfeItemDAO;
	}
	
	public LoteConsultaDfeItem procurarPorChaveAcessoImportacao(String chaveAcesso) {
		return loteConsultaDfeItemDAO.procurarPorChaveAcessoImportacao(chaveAcesso);
	}

	public List<LoteConsultaDfeItem> procurarItemPorLoteConsultaDfe(LoteConsultaDfe loteConsultaDfe) {
		return loteConsultaDfeItemDAO.procurarPorLoteConsultaDfe(loteConsultaDfe);
	}
}
