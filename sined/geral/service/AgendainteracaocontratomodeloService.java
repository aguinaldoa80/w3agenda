package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Agendainteracaocontratomodelo;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.dao.AgendainteracaocontratomodeloDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AgendainteracaocontratomodeloService extends GenericService<Agendainteracaocontratomodelo>{
	
	public AgendainteracaocontratomodeloDAO agendainteracaocontratomodeloDAO;
	
	public void setAgendainteracaocontratomodeloDAO(
			AgendainteracaocontratomodeloDAO agendainteracaocontratomodeloDAO) {
		this.agendainteracaocontratomodeloDAO = agendainteracaocontratomodeloDAO;
	}

	public List<Agendainteracaocontratomodelo> findForCreationAgendainteracao(Contratotipo contratotipo){
		return agendainteracaocontratomodeloDAO.findForCreationAgendainteracao(contratotipo);
	}
}
