package br.com.linkcom.sined.geral.service;

import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.BAIXADA_PARCIAL;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.COMPRA_FINALIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.COMPRA_NAO_AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.EM_ABERTO;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.EM_PROCESSO_COMPRA;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ESTORNAR;
import static br.com.linkcom.sined.geral.bean.Situacaorequisicao.ROMANEIO_COMPLETO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoqueorigem;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;
import br.com.linkcom.sined.geral.bean.Movpatrimonioorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Patrimonioitem;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Producaoagenda;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Requisicaoorigem;
import br.com.linkcom.sined.geral.bean.Romaneio;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialRateioEstoqueTipoContaGerencial;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaopedidosolicitacaocompra;
import br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.GerarRomaneioBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.ImportarDadosRequisicaomaterialBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.RequisicaomaterialFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.BaixarequisicaomaterialBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.RequisicaomaterialReportBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.thread.EmailManagerThread;

public class RequisicaomaterialService extends GenericService<Requisicaomaterial> {
	
	private RequisicaomaterialDAO requisicaomaterialDAO;
	private RequisicaoorigemService requisicaoorigemService;
	private MovimentacaoestoqueorigemService movimentacaoestoqueorigemService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private MovpatrimonioService movpatrimonioService;
	private MovpatrimonioorigemService movpatrimonioorigemService;
	private SolicitacaocompraService solicitacaocompraService;
	private PatrimonioitemService patrimonioitemService;
	private UsuarioService usuarioService;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private RomaneioService romaneioService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private LocalarmazenagemService localarmazenagemService;
	private ProducaoagendaService producaoagendaService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private ParametrogeralService parametrogeralService;
	private MotivoavisoService motivoavisoService;
	private AvisoService avisoService;
	private RequisicaomaterialService requisicaomaterialService;
	private RomaneioitemService romaneioitemService;
	private RateioService rateioService;
	private InventarioService inventarioService;
	
	public void setRomaneioService(RomaneioService romaneioService) {
		this.romaneioService = romaneioService;
	}
	public void setRequisicaoorigemService(
			RequisicaoorigemService requisicaoorigemService) {
		this.requisicaoorigemService = requisicaoorigemService;
	}
	public void setRequisicaomaterialDAO(
			RequisicaomaterialDAO requisicaomaterialDAO) {
		this.requisicaomaterialDAO = requisicaomaterialDAO;
	}
	public void setMovimentacaoestoqueorigemService(
			MovimentacaoestoqueorigemService movimentacaoestoqueorigemService) {
		this.movimentacaoestoqueorigemService = movimentacaoestoqueorigemService;
	}
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMovpatrimonioService(
			MovpatrimonioService movpatrimonioService) {
		this.movpatrimonioService = movpatrimonioService;
	}
	public void setMovpatrimonioorigemService(
			MovpatrimonioorigemService movpatrimonioorigemService) {
		this.movpatrimonioorigemService = movpatrimonioorigemService;
	}
	public void setSolicitacaocompraService(
			SolicitacaocompraService solicitacaocompraService) {
		this.solicitacaocompraService = solicitacaocompraService;
	}
	public void setPatrimonioitemService(
			PatrimonioitemService patrimonioitemService) {
		this.patrimonioitemService = patrimonioitemService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setProducaoagendahistoricoService(ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setProducaoagendaService(ProducaoagendaService producaoagendaService) {
		this.producaoagendaService = producaoagendaService;
	}
	public void setPlanejamentorecursogeralService(PlanejamentorecursogeralService planejamentorecursogeralService) {
		this.planejamentorecursogeralService = planejamentorecursogeralService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setRequisicaomaterialService(RequisicaomaterialService requisicaomaterialService) {
		this.requisicaomaterialService = requisicaomaterialService;
	}
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {
		this.romaneioitemService = romaneioitemService;
	}
	public void setRateioService(RateioService rateioService) {
		this.rateioService = rateioService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@Override
	public void saveOrUpdate(final Requisicaomaterial bean) {
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				requisicaoorigemService.saveOrUpdateNoUseTransaction(bean.getRequisicaoorigem());
				saveOrUpdateNoUseTransaction(bean);
				return status;
			}
		});
		
		// ATUALIZA A TABELA AUXILIAR DE REQUISI��O DE MATERIAL
		this.callProcedureAtualizaRequisicaomaterial(bean);
	}
	
	@Override
	public void saveOrUpdateNoUseTransaction(Requisicaomaterial bean) {
		super.saveOrUpdateNoUseTransaction(bean);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findListagem(RequisicaomaterialFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaomaterial> findListagem(RequisicaomaterialFiltro filtro) {
		return requisicaomaterialDAO.findListagem(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findWithList(String whereIn, String orderBy, boolean asc)
	*
	* @param whereIn
	* @param orderBy
	* @param asc
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Requisicaomaterial> findWithList(String whereIn, String orderBy, boolean asc) {
		return requisicaomaterialDAO.findWithList(whereIn, orderBy, asc);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoes(String whereIn) {
		return requisicaomaterialDAO.findRequisicoes(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public void doUpdateRequisicoes(String whereIn, Situacaorequisicao situacao, Boolean cancelamentoRomaneio) {
		requisicaomaterialDAO.doUpdateRequisicoes(whereIn, situacao, cancelamentoRomaneio);
		
		// ATUALIZA A TABELA AUXILIAR DE REQUISICAO DE MATERIAL
		this.callProcedureAtualizaRequisicaomaterial(whereIn);
	}
	
	public void doUpdateRequisicoes(String whereIn, Situacaorequisicao situacao) {
		doUpdateRequisicoes(whereIn, situacao, false);
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoesParaGerarCompra(String whereIn) {
		return requisicaomaterialDAO.findRequisicoesParaGerarCompra(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> findRequisicoesParaBaixa(String whereIn) {
		return requisicaomaterialDAO.findRequisicoesParaBaixa(whereIn);
	}
	
	/**
	 * M�todo que baixa as requisi��es de material criando Saidas para os materias do tipo Produto/EPI/Servi�o e 
	 * criando movimenta��es de patrim�nio para os materiais do tipo Patrimonio
	 * 
	 * @see br.com.linkcom.sined.util.neo.persistence.GenericService#saveOrUpdateNoUseTransaction
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#doUpdateRequisicoes
	 * @param movimentacoesProdutoEpiServico
	 * @param movimentacoesPatrimonio
	 * @param whereIn
	 * @author Tom�s Rabelo
	 */
	public void saveBaixa(final List<Movimentacaoestoque> movimentacoesProdutoEpiServico, final List<Movimentacaoestoque> movimentacoesProdutoEpiServicoGrade, final List<Movpatrimonio> movimentacoesPatrimonio, final String whereIn) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(whereIn != null && !whereIn.equals("")){
					if(movimentacoesProdutoEpiServico != null && movimentacoesProdutoEpiServico.size() > 0)
						for (Movimentacaoestoque movestoque : movimentacoesProdutoEpiServico){
							if(!Materialclasse.SERVICO.equals(movestoque.getMaterialclasse())){
								movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(movestoque.getMovimentacaoestoqueorigem());
								if(SinedUtil.isRateioMovimentacaoEstoque() && movestoque.getRateio() != null){
									rateioService.saveOrUpdateNoUseTransaction(movestoque.getRateio());
								}
								movimentacaoestoqueService.saveOrUpdateNoUseTransaction(movestoque);
								movimentacaoestoqueService.salvarHisotirico(movestoque, "Criado a partir da requisi��o de material " 
										+ (movestoque.getMovimentacaoestoqueorigem() != null && movestoque.getMovimentacaoestoqueorigem().getRequisicaomaterial() != null ? 
												SinedUtil.makeLinkHistorico(movestoque.getMovimentacaoestoqueorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString(), "visualizarRequisicaoMaterial") + ".": "."), MovimentacaoEstoqueAcao.CRIAR, Boolean.FALSE);
							}
						
						}
					if(movimentacoesProdutoEpiServicoGrade != null && movimentacoesProdutoEpiServicoGrade.size() > 0)
						for (Movimentacaoestoque movestoqueItemGrade : movimentacoesProdutoEpiServicoGrade){
							if(!Materialclasse.SERVICO.equals(movestoqueItemGrade.getMaterialclasse())){
								movimentacaoestoqueorigemService.saveOrUpdateNoUseTransaction(movestoqueItemGrade.getMovimentacaoestoqueorigem());
								if(SinedUtil.isRateioMovimentacaoEstoque() && movestoqueItemGrade.getRateio() != null){
									rateioService.saveOrUpdateNoUseTransaction(movestoqueItemGrade.getRateio());
								}
								movimentacaoestoqueService.saveOrUpdateNoUseTransaction(movestoqueItemGrade);
								movimentacaoestoqueService.salvarHisotirico(movestoqueItemGrade, "Criado a partir da requisi��o de material " 
										+ (movestoqueItemGrade.getMovimentacaoestoqueorigem() != null && movestoqueItemGrade.getMovimentacaoestoqueorigem().getRequisicaomaterial() != null ? 
												SinedUtil.makeLinkHistorico(movestoqueItemGrade.getMovimentacaoestoqueorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString(), "visualizarRequisicaoMaterial") + ".": "."), MovimentacaoEstoqueAcao.CRIAR, Boolean.FALSE);
							}
						
						}
					if(movimentacoesPatrimonio != null && movimentacoesPatrimonio.size() > 0)
						for (Movpatrimonio movpatrimonio : movimentacoesPatrimonio){
							movpatrimonioorigemService.saveOrUpdateNoUseTransaction(movpatrimonio.getMovpatrimonioorigem());
							movpatrimonioService.saveOrUpdateNoUseTransaction(movpatrimonio);
						}
				
					requisicaomaterialDAO.doUpdateRequisicoes(whereIn, Situacaorequisicao.BAIXADA, false);
				}
				return status;
			}
		});
		
		// ATUALIZA A TABELA AUXILIAR DE REQUISICAO DE MATERIAL
		this.callProcedureAtualizaRequisicaomaterial(whereIn);
	}
	
	/**
	 * M�todo monta a origem dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Requisicaomaterial form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getRequisicaoorigem().getUsuario() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.USUARIO, "USU�RIO", form.getRequisicaoorigem().getUsuario().getNome(), 
															  form.getRequisicaoorigem().getUsuario().getCdpessoa() != null ? form.getRequisicaoorigem().getUsuario().getCdpessoa().toString() : "", 
																	  null);
		
			listaBean.add(origemsuprimentosBean);
		} else if(form.getRequisicaoorigem().getPedidovenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PEDIDOVENDA, "PEDIDO DE VENDA", 
					form.getRequisicaoorigem().getPedidovenda().getCdpedidovenda().toString(), 
					form.getRequisicaoorigem().getPedidovenda().getCdpedidovenda().toString(), 
					  null);

			listaBean.add(origemsuprimentosBean);
		} else if (form.getRequisicaoorigem().getProducaoagenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PRODUCAOAGENDA, "AGENDA DE PRODU��O", 
					form.getRequisicaoorigem().getProducaoagenda().getCdproducaoagenda().toString(), 
					form.getRequisicaoorigem().getProducaoagenda().getCdproducaoagenda().toString(), 
					  null);

			listaBean.add(origemsuprimentosBean);
		} else if(form.getRequisicaoorigem().getPlanejamentorecursogeral() != null && form.getRequisicaoorigem().getPlanejamentorecursogeral().getPlanejamento() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PLANEJAMENTO, "PLANEJAMENTO", 
					(form.getRequisicaoorigem().getPlanejamentorecursogeral().getPlanejamento().getDescricao() != null ? form.getRequisicaoorigem().getPlanejamentorecursogeral().getPlanejamento().getDescricao() : "") , 
					(form.getRequisicaoorigem().getPlanejamentorecursogeral().getPlanejamento().getCdplanejamento() != null ? form.getRequisicaoorigem().getPlanejamentorecursogeral().getPlanejamento().getCdplanejamento().toString() : ""), 
					  null );

			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo monta destino da requisi��o. Ou seja a solicita��o de compra criada.
	 * Obs: somente solicita��o ativa que n�o esteja cancelada.
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaDestino(Requisicaomaterial form) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		List<Solicitacaocompra> listaSolicitacaocompra = solicitacaocompraService.getSolicitacoesDaRequisicao(form, false);
		if(SinedUtil.isListNotEmpty(listaSolicitacaocompra)){
			for(Solicitacaocompra solicitacaocompra : listaSolicitacaocompra){
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.SOLICITACAOCOMPRA, "SOLICITA��O DE COMPRA", solicitacaocompra.getIdentificador().toString(), solicitacaocompra.getCdsolicitacaocompra().toString(), 
					  null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		List<Romaneio> listaRomaneios = romaneioService.findByRequisicaomaterial(form);
		if(listaRomaneios != null && listaRomaneios.size() > 0){
			for (Romaneio romaneio : listaRomaneios) {
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ROMANEIO, "ROMANEIO", romaneio.getCdromaneio().toString(), romaneio.getCdromaneio().toString(), 
						  null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRequisicaomaterial(form, false);
		if(SinedUtil.isListNotEmpty(listaMovimentacaoestoque)){
			for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
				OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.MOVIMENTACAOESTOQUE, "MOVIMENTA��O DE ESTOQUE", movimentacaoestoque.getCdmovimentacaoestoque().toString(), movimentacaoestoque.getCdmovimentacaoestoque().toString(), 
						  null);
				listaBean.add(origemsuprimentosBean);
			}
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo que junta as informa��es do mestre com seus detalhes transformando em v�rias Requisi��es
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> preparaSaveEmMassa(Requisicaomaterial bean) {
		List<Requisicaomaterial> list = new ListSet<Requisicaomaterial>(Requisicaomaterial.class);
		for (Requisicaomaterial requisicaomaterial : bean.getRequisicoes()) {
			
			if(requisicaomaterial.getCdpedidovenda() != null){
				Requisicaoorigem requisicaoorigem = new Requisicaoorigem();
				requisicaoorigem.setPedidovenda(new Pedidovenda(requisicaomaterial.getCdpedidovenda()));
				bean.setRequisicaoorigem(requisicaoorigem);
			}
			else if(requisicaomaterial.getCdProducaoagenda() != null){
				Requisicaoorigem requisicaoorigem = new Requisicaoorigem();
				requisicaoorigem.setProducaoagenda(new Producaoagenda(requisicaomaterial.getCdProducaoagenda()));
				bean.setRequisicaoorigem(requisicaoorigem);
			}
			else if(requisicaomaterial.getCdplanejamentorecursogeral() != null){
				Requisicaoorigem requisicaoorigem = new Requisicaoorigem();
				requisicaoorigem.setPlanejamentorecursogeral(new Planejamentorecursogeral(requisicaomaterial.getCdplanejamentorecursogeral()));
				bean.setRequisicaoorigem(requisicaoorigem);
			}
			
			Requisicaomaterial requisicaomaterial2 = new Requisicaomaterial(
					bean.getDescricao(), bean.getDtlimite(), bean.getDtcriacao(), bean.getProjeto(), bean.getDepartamento(), bean.getLocalarmazenagem(),  bean.getCentrocusto(), bean.getEmpresa(),
					bean.getColaborador(), requisicaomaterial.getMaterial(), requisicaomaterial.getMaterialclasse(), requisicaomaterial.getQtde(), bean.getRequisicaoorigem(), bean.getIdentificador(),
					bean.getObservacao(), requisicaomaterial.getFrequencia(), requisicaomaterial.getQtdefrequencia(), requisicaomaterial.getArquivo(),
					requisicaomaterial.getAltura(), requisicaomaterial.getLargura(), requisicaomaterial.getComprimento(), requisicaomaterial.getPesototal(), requisicaomaterial.getQtdvolume());
			requisicaomaterial2.setUnidademedidatrans(requisicaomaterial.getUnidademedidatrans());
			if(requisicaomaterial2.getMaterial() != null){
				this.verificaUnidademedidaQtde(requisicaomaterial2);	
			}
			list.add(requisicaomaterial2);
		}
		
		return list;
	}
	
	/**
	 * Salva lista de requisi��es
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaoorigemService#saveOrUpdateNoUseTransaction(br.com.linkcom.sined.geral.bean.Requisicaoorigem)
	 * @see #saveOrUpdateNoUseTransaction(Requisicaomaterial)
	 * @param requisicoes
	 * @author Tom�s Rabelo
	 */
	public void salvaRequisicaoEmMassa(final List<Requisicaomaterial> requisicoes) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				for (Requisicaomaterial requisicaomaterial : requisicoes) {
					requisicaoorigemService.saveOrUpdateNoUseTransaction(requisicaomaterial.getRequisicaoorigem());
					saveOrUpdateNoUseTransaction(requisicaomaterial);
				}
				return status;
			}
		});
		
		for(Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getRequisicaoorigem() != null && requisicaomaterial.getRequisicaoorigem().getProducaoagenda() != null){
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setProducaoagenda(requisicaomaterial.getRequisicaoorigem().getProducaoagenda());
				producaoagendahistorico.setObservacao("Requisi��o de material criado <a href=/w3erp/suprimento/crud/Requisicaomaterial?ACAO=consultar&cdrequisicaomaterial=" + requisicaomaterial.getCdrequisicaomaterial() +">"+requisicaomaterial.getIdentificadorOuCdrequisicaomaterial()+"</a>");
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
			}else if(requisicaomaterial.getRequisicaoorigem() != null && requisicaomaterial.getRequisicaoorigem().getPedidovenda() != null){
				Pedidovendahistorico pvh = new Pedidovendahistorico();
				pvh.setPedidovenda(requisicaomaterial.getRequisicaoorigem().getPedidovenda());
				pvh.setObservacao("Requisi��o de material criada <a href=/w3erp/suprimento/crud/Requisicaomaterial?ACAO=consultar&cdrequisicaomaterial=" + requisicaomaterial.getCdrequisicaomaterial() +">"+requisicaomaterial.getIdentificadorOuCdrequisicaomaterial()+"</a>");
				pedidovendahistoricoService.saveOrUpdate(pvh);
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getUltimoIdentificador() {
		return requisicaomaterialDAO.getUltimoIdentificador();
	}
	
	/**
	 * M�todo que seta valores quando o usu�rio est� sendo redirecionado para a tela de cria��o
	 * 
	 * @see #getUltimoIdentificador()
	 * @param form
	 * @author Tom�s Rabelo
	 */
	public void preparaCriacaoRequisicao(Requisicaomaterial form) {
		Integer identificador = getUltimoIdentificador();
		if(form.getIdentificador() == null)
			form.setIdentificador(new Integer(identificador != null ? identificador+1 : 1));
		
		SimpleDateFormat format = new SimpleDateFormat("yy");
		if(form.getDescricao() == null || "".equals(form.getDescricao())){
			form.setDescricao(form.getIdentificador()+"/"+format.format(new Date()));
		}
		Requisicaoorigem requisicaoorigem = new Requisicaoorigem();
		requisicaoorigem.setUsuario((Usuario)Neo.getUser());
		form.setRequisicaoorigem(requisicaoorigem);
		
	}
	
	/**
	 * M�todo muda a situa��o da(s) requisi�ao(�es) para autorizada
	 * 
	 * @see #doUpdateRequisicoes(String, br.com.linkcom.sined.geral.bean.Situacaorequisicao)
	 * @param requisicoes
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 * @param errors 
	 */
	public boolean autorizar(List<Requisicaomaterial> requisicoes, String whereIn, BindException errors) {
		for (Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(AUTORIZADA)){
				errors.reject("001", "Requisi��o j� est� autorizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA) ||
					  requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_FINALIZADA) ||
					  requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(ROMANEIO_COMPLETO) ||
					  requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA_PARCIAL) ||
					  requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				errors.reject("002", "Requisi��o j� foi autorizada.");
				return false;
			}  else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(CANCELADA)){
				errors.reject("003", "Autoriza��o n�o permitida. A requisi��o est� cancelada.");
				return false;
			}
			
			Motivoaviso motivoAviso = motivoavisoService.findByMotivo(MotivoavisoEnum.REQUISICAO_MATERIAL_AUTORIZADA);
			StringBuilder notWhereInCdusuario = new StringBuilder();
			if(motivoAviso != null){
				List<Aviso> avisoList = new ArrayList<Aviso>();
				
				avisoList.add(new Aviso("Requisi��o de material autorizada", "C�digo da requisi��o de material: " + requisicaomaterial.getCdrequisicaomaterial(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
						motivoAviso.getUsuario(), AvisoOrigem.REQUISICAO_MATERIAL, requisicaomaterial.getCdrequisicaomaterial(), requisicaomaterial.getEmpresa(), null, motivoAviso));
				List<Usuario> usuarioList = avisoService.salvarAvisos(avisoList, true);
				
				for (Usuario u : usuarioList) {
					if(!"".equals(notWhereInCdusuario.toString())) {
						notWhereInCdusuario.append(",");
					}
					notWhereInCdusuario.append(u.getCdpessoa());
				}
				
				if(Boolean.TRUE.equals(motivoAviso.getEmail())){
					requisicaomaterialService.enviarEmailRequisicaomaterial(requisicaomaterial.getCdrequisicaomaterial().toString(), Situacaorequisicao.AUTORIZADA, notWhereInCdusuario.toString());
				}
			}
		}

		doUpdateRequisicoes(whereIn, AUTORIZADA);
		
		return true;
	}
	
	/**
	 * M�todo muda a situa��o da(s) requisi�ao(�es) para cancelada
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#doUpdateRequisicoes(String, br.com.linkcom.sined.geral.bean.Situacaorequisicao)
	 * @param requisicoes
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 * @param errors 
	 */
	public boolean cancelar(List<Requisicaomaterial> requisicoes, String whereIn, BindException errors) {
		for (Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(AUTORIZADA)){
				errors.reject("004", "Para cancelar, a requisi��o deve ser estornada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA)){
				errors.reject("005", "Cancelamento n�o permitido. A requisi��o est� em processo de compra.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_FINALIZADA)){
				errors.reject("006", "Cancelamento n�o permitido. A compra foi realizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(ROMANEIO_COMPLETO)){
				errors.reject("006", "Cancelamento n�o permitido. A requisi��o est� com o romaneio completo.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA_PARCIAL)){
				errors.reject("007", "Cancelamento n�o permitido. A requisi��o est� baixada parcialmente.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				errors.reject("008", "Cancelamento n�o permitido. A requisi��o est� baixada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(CANCELADA)){
				errors.reject("009", "Requisi��o j� est� cancelada.");
				return false;
			}
		}
		
		doUpdateRequisicoes(whereIn, CANCELADA);
		return true;
	}
	
	/**
	 * M�todo muda a situa��o da(s) requisi�ao(�es) para em aberto
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#doUpdateRequisicoes(String, br.com.linkcom.sined.geral.bean.Situacaorequisicao)
	 * @param requisicoes
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 * @param errors 
	 */
	public boolean estornar(List<Requisicaomaterial> requisicoes, String whereIn, BindException errors) {
		HashMap<Projeto, List<Requisicaomaterial>> mapProjetoRequisicaomaterial = new HashMap<Projeto, List<Requisicaomaterial>>();
		for (Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_ABERTO)){
				errors.reject("010", "A requisi��o j� est� estornada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA)){
				errors.reject("011", "Estorno n�o permitido. A requisi��o est� em processo de compra.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_FINALIZADA)){
				errors.reject("012", "Estorno n�o permitido. A compra foi realizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_NAO_AUTORIZADA)){
				errors.reject("013", "Estorno n�o permitido. A requisi��o n�o foi autorizada.");
				return false;
			}else {
				List<Romaneio> listaRomaneio = romaneioService.findByRequisicaomaterial(requisicaomaterial);
				if(SinedUtil.isListNotEmpty(listaRomaneio)){
					for(Romaneio romaneio : listaRomaneio){
						StringBuilder whereInMovimentacaoestoque = new StringBuilder();
						List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRomaneio(romaneio); 
						if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
							for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
								if(movimentacaoestoque.getDtcancelamento() == null){
									if(!"".equals(whereInMovimentacaoestoque.toString())) whereInMovimentacaoestoque.append(","); 
									whereInMovimentacaoestoque.append(movimentacaoestoque.getCdmovimentacaoestoque());
									
								}
							}
						}
						
						if(!"".equals(whereInMovimentacaoestoque.toString())){
							listaMovimentacaoestoque = movimentacaoestoqueService.findMovimentacoesForQtdedisponivel(whereInMovimentacaoestoque.toString());
							Double qtdeDisponivel;
							for (Movimentacaoestoque movimentacaoestoque2 : listaMovimentacaoestoque) {
								if(!localarmazenagemService.getPermitirestoquenegativo(movimentacaoestoque2.getLocalarmazenagem())){
									if(movimentacaoestoque2.getMovimentacaoestoquetipo() != null && 
											movimentacaoestoque2.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
										if(movimentacaoestoque2.getMaterialclasse() != null && !movimentacaoestoque2.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
											qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( movimentacaoestoque2.getMaterial(), movimentacaoestoque2.getMaterialclasse(), movimentacaoestoque2.getLocalarmazenagem());
											if(qtdeDisponivel != null && (qtdeDisponivel - movimentacaoestoque2.getQtde()) < 0){
												errors.reject("013", "Estorno n�o permitido. O estoque " + movimentacaoestoque2.getLocalarmazenagem().getNome() + " ficaria negativo.");
												return false;
											}
										}
									}
								}
							}
						}
					}
				}
				
				StringBuilder whereInMovimentacaoestoque = new StringBuilder();
				List<Movimentacaoestoque> listaMovimentacaoestoque = movimentacaoestoqueService.findByRequisicaomaterial(requisicaomaterial, true);
				
				if(listaMovimentacaoestoque != null && listaMovimentacaoestoque.size() > 0){
					for (Movimentacaoestoque movimentacaoestoque : listaMovimentacaoestoque) {
						if(movimentacaoestoque.getDtcancelamento() == null){
							if(!"".equals(whereInMovimentacaoestoque.toString())) whereInMovimentacaoestoque.append(","); 
							whereInMovimentacaoestoque.append(movimentacaoestoque.getCdmovimentacaoestoque());
							
						}
					}
				}
						
				if(!"".equals(whereInMovimentacaoestoque.toString())){
					listaMovimentacaoestoque = movimentacaoestoqueService.findMovimentacoesForQtdedisponivel(whereInMovimentacaoestoque.toString());
					Double qtdeDisponivel;
					for (Movimentacaoestoque movimentacaoestoque2 : listaMovimentacaoestoque) {
						if(!localarmazenagemService.getPermitirestoquenegativo(movimentacaoestoque2.getLocalarmazenagem())){
							if(movimentacaoestoque2.getMovimentacaoestoquetipo() != null && 
									movimentacaoestoque2.getMovimentacaoestoquetipo().equals(Movimentacaoestoquetipo.ENTRADA)){
								if(movimentacaoestoque2.getMaterialclasse() != null && !movimentacaoestoque2.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
									qtdeDisponivel = movimentacaoestoqueService.getQtdDisponivelProdutoEpiServico( movimentacaoestoque2.getMaterial(), movimentacaoestoque2.getMaterialclasse(), movimentacaoestoque2.getLocalarmazenagem());
									if(qtdeDisponivel != null && (qtdeDisponivel - movimentacaoestoque2.getQtde()) < 0){
										errors.reject("013", "Estorno n�o permitido. O estoque " + movimentacaoestoque2.getLocalarmazenagem().getNome() + " ficaria negativo.");
										return false;
									}
								}
							}
						}
					}
				}
			}
			
			if(requisicaomaterial.getProjeto() != null){
				if(mapProjetoRequisicaomaterial.get(requisicaomaterial.getProjeto()) == null){
					mapProjetoRequisicaomaterial.put(requisicaomaterial.getProjeto(), new ArrayList<Requisicaomaterial>());
				}
				mapProjetoRequisicaomaterial.get(requisicaomaterial.getProjeto()).add(requisicaomaterial);
			}
		}
		
		if(mapProjetoRequisicaomaterial != null && mapProjetoRequisicaomaterial.size() > 0){
			for(Projeto projeto : mapProjetoRequisicaomaterial.keySet()){
				validaQtdeCompraMaiorQuePlanejamento(mapProjetoRequisicaomaterial.get(projeto), projeto, errors);
				if(errors.hasErrors()){
					return false;
				}
			}
		}

		doUpdateRequisicoes(whereIn, ESTORNAR);
		return true;
	}
	
	/**
	 * M�todo muda a situa��o da(s) requisi��o(�es) para baixada
	 * 
	 * @param requisicoes
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 * @param errors 
	 */
	public boolean baixar(List<Requisicaomaterial> requisicoes, BindException errors) {
		for (Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_ABERTO) &&
					!SinedUtil.isUserHasAction("AUTORIZAR_REQUISICAOMATERIAL")){
				errors.reject("015", "Para baixar a requisi��o deve estar autorizada ou com a compra finalizada ou romaneio completo.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA)){
				errors.reject("016", "Para baixar a requisi��o deve estar com a compra finalizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				errors.reject("017", "Requisi��o j� est� baixada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(CANCELADA)){
				errors.reject("018", "Baixa n�o permitida. A requisi��o est� cancelada.");
				return false;
			}else if(SinedUtil.isRateioMovimentacaoEstoque() && requisicaomaterial.getMaterial() != null && 
									(requisicaomaterial.getMaterial().getMaterialRateioEstoque() == null || 
									requisicaomaterial.getMaterial().getMaterialRateioEstoque().getContaGerencialRequisicao() == null ||
									requisicaomaterial.getCentrocusto() == null)){
				errors.reject("018", "Para realizar a baixa e dar entrada do material no estoque � obrigat�rio o preenchimento da conta gerencial de consumo no cadastro de material e o preenchimento do centro de custo no cadastro de requisi��o. " + 
									(requisicaomaterial.getMaterial().getNome() != null ? "(" + requisicaomaterial.getMaterial().getNome() + ")": ""));
				return false;
			}
			
			if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), 
					requisicaomaterial.getEmpresa(), requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getProjeto())) {
				errors.reject("020", "N�o � poss�vel realizar a baixa e dar entrada do material no estoque, pois o registro est� vinculado � um registro de invent�rio. Para baixar, � necess�rio cancelar o invent�rio. ");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo muda a situa��o da(s) requisi��o(�es) para baixada
	 *
	 * @param requisicoes
	 * @param errors
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/04/2014
	 */
	public boolean gerarRomaneio(List<Requisicaomaterial> requisicoes, BindException errors) {
		Boolean permiteAutorizarRequisicaomaterial = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_REQUISICAOMATERIAL");
		
		for (Requisicaomaterial requisicaomaterial : requisicoes){
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_ABERTO) && 
					!permiteAutorizarRequisicaomaterial){
				errors.reject("019", "Para gerar romaneio a requisi��o deve estar autorizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA)){
				errors.reject("020", "Requisi��o j� est� em processo de compra.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_FINALIZADA)){
				errors.reject("021", "Gera��o de romaneio n�o permitida. Requisi��o est� com processo de compra finalizado.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA_PARCIAL)){
				errors.reject("022", "Gera��o de romaneio n�o permitida. Requisi��o est� baixada parcialmente.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				errors.reject("023","Gera��o de romaneio n�o permitida. Requisi��o est� baixada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(ROMANEIO_COMPLETO)){
				errors.reject("023","Gera��o de romaneio n�o permitida. Requisi��o est� com o romaneio completo.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(CANCELADA)){
				errors.reject("024", "Gera��o de romaneio n�o permitida. A requisi��o est� cancelada.");
				return false;
			} else if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(SinedDateUtils.currentDate(), requisicaomaterial.getEmpresa(), 
					requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getProjeto())) {
				errors.reject("025", "N�o � poss�vel realizar a movimenta��o de estoque,  pois o registro est� vinculado � um registro de invent�rio. Para prosseguir com a a��o, � necess�rio cancelar o invent�rio.");
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo que valida se as solicita��es podem ser criadas
	 * 
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#findRequisicoesParaGerarCompra(String)
	 * @param acao
	 * @return
	 * @author Tom�s Rabelo
	 * @param request 
	 */
	public boolean gerarCompra(List<Requisicaomaterial> requisicoes,	BindException errors) {
		Boolean permiteAutorizarRequisicaomaterial = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_REQUISICAOMATERIAL");
		
		for (Requisicaomaterial requisicaomaterial : requisicoes){ 
			if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_ABERTO) && !permiteAutorizarRequisicaomaterial){
				errors.reject("019", "Para gerar a compra, a requisi��o deve estar autorizada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_PROCESSO_COMPRA)){
				errors.reject("020", "Requisi��o j� est� em processo de compra.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(COMPRA_FINALIZADA)){
				errors.reject("021", "Gera��o de compra n�o permitida. Requisi��o est� com processo de compra finalizado.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA_PARCIAL)){
				errors.reject("022", "Gera��o de compra n�o permitida. Requisi��o est� baixada parcialmente.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(BAIXADA)){
				errors.reject("023","Gera��o de compra n�o permitida. Requisi��o est� baixada.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(ROMANEIO_COMPLETO)){
				errors.reject("023","Gera��o de compra n�o permitida. Requisi��o est� com o romaneio completo.");
				return false;
			} else if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(CANCELADA)){
				errors.reject("024", "Gera��o de compra n�o permitida. A requisi��o est� cancelada.");
				return false;
			}
		}
		
		if(permiteAutorizarRequisicaomaterial){
			StringBuilder whereIn = new StringBuilder();
			for (Requisicaomaterial requisicaomaterial : requisicoes){ 
				if(requisicaomaterial.getAux_requisicaomaterial().getSituacaorequisicao().equals(EM_ABERTO) && requisicaomaterial.getCdrequisicaomaterial() != null){
					whereIn
						.append(!"".equals(whereIn.toString()) ? "," : "")
						.append(requisicaomaterial.getCdrequisicaomaterial());
				}
			}
			if(!"".equals(whereIn.toString())){
				doUpdateRequisicoes(whereIn.toString(), AUTORIZADA);
			}
		}
		
		return true;
	}
	
	/**
	 * M�todo que seapara as requisi��es em 2 grupos. Patrim�nio e Produto/Epi/Servi�o
	 * 
	 * @see br.com.linkcom.sined.geral.service.MovpatrimonioService#getQuantidadeMovimentadaDaRequisicaoMaterial(Requisicaomaterial)
	 * @see br.com.linkcom.sined.geral.service.MovimentacaoestoqueService#getQuantidadeEntregueDaRequisicaoMaterial(Requisicaomaterial)
	 * @param requisicoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public BaixarequisicaomaterialBean preparaBaixaSeparandoRequisicao(List<Requisicaomaterial> requisicoes) {
		BaixarequisicaomaterialBean bean = new BaixarequisicaomaterialBean();
		int contador = 0;
		
		//Separa as requisi��es
		for (Requisicaomaterial requisicaomaterial : requisicoes) {
			requisicaomaterial.setIsMateriaprimaProducaoagenda(isMateriaprimaProducaoagenda(requisicaomaterial));
			if(requisicaomaterial.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
				Long qtdMovimentada = movpatrimonioService.getQuantidadeMovimentadaDaRequisicaoMaterial(requisicaomaterial);
				
				if(requisicaomaterial.getQtde() - qtdMovimentada > 0){
					contador++;
				}
				for (int i = 0; i < requisicaomaterial.getQtde(); i++) {
					Requisicaomaterial requisicaomaterial2 = new Requisicaomaterial(
							requisicaomaterial.getCdrequisicaomaterial(), requisicaomaterial.getIdentificador(), requisicaomaterial.getMaterial(), requisicaomaterial.getColaborador(), 
							requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getEmpresa());
					if(requisicaomaterial.getQtde() - qtdMovimentada == 0){
						requisicaomaterial2.setSomenteBaixa(Boolean.TRUE);
					}else {
						requisicaomaterial2.setSomenteBaixa(Boolean.FALSE);
					}
					bean.getListaPatrimonio().add(requisicaomaterial2);
				}
			} else{
				Long quantidade = movimentacaoestoqueService.getQuantidadeEntregueDaRequisicaoMaterial(requisicaomaterial);
				if(quantidade == null || quantidade.intValue() < 0)
					quantidade = new Long(0);
				
				if(requisicaomaterial.getQtde() - quantidade.intValue() > 0){
					contador++;
				}
				
				requisicaomaterial.setQtderestante(requisicaomaterial.getQtde() - quantidade.intValue());
				
				if(requisicaomaterial.getMaterial() != null && requisicaomaterial.getMaterial().getMaterialgrupo() != null && 
						requisicaomaterial.getMaterial().getMaterialmestregrade() == null &&
						requisicaomaterial.getMaterial().getMaterialgrupo().getGradeestoquetipo() != null){
					boolean existMaterialitemByMaterialgrademestre = materialService.existMaterialitemByMaterialgrademestre(requisicaomaterial.getMaterial());
					if(existMaterialitemByMaterialgrademestre){
						requisicaomaterial.setListaMaterialitemmestregrade(montaMaterialitemmestregrade(materialService.findMaterialitemByMaterialmestregrade(requisicaomaterial.getMaterial())));
						if(requisicaomaterial.getListaMaterialitemmestregrade() != null && 
								!requisicaomaterial.getListaMaterialitemmestregrade().isEmpty()){
							for(Requisicaomaterial rmItemGrade : requisicaomaterial.getListaMaterialitemmestregrade()){
								rmItemGrade.setMaterialclasse(requisicaomaterial.getMaterialclasse());
								rmItemGrade.setCdrequisicaomaterial(requisicaomaterial.getCdrequisicaomaterial());
								rmItemGrade.setEmpresa(requisicaomaterial.getEmpresa());
							}								
						}
						bean.getListaProdutoEpiServicoGrade().add(requisicaomaterial);
					}else {
						bean.getListaProdutoEpiServico().add(requisicaomaterial);
					}
					
				}else {
					bean.getListaProdutoEpiServico().add(requisicaomaterial);
				}
				
			}
		}
		
		if(contador == 0){
			bean.setApenasBaixa(Boolean.TRUE);
		}
		
		return bean;
	}
	
	/**
	* M�todo que verifica se o item tem origem de agenda de produ��o e � uma mat�ria-prima
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoagendaService.isMateriaprima(Material material, Producaoagenda producaoagenda)
	*
	* @param requisicaomaterial
	* @return
	* @since 02/03/2016
	* @author Luiz Fernando
	*/
	private boolean isMateriaprimaProducaoagenda(Requisicaomaterial requisicaomaterial) {
		if(requisicaomaterial.getRequisicaoorigem() != null && requisicaomaterial.getRequisicaoorigem().getProducaoagenda() != null &&
				requisicaomaterial.getMaterial() != null){
			return producaoagendaService.isMateriaprima(requisicaomaterial.getMaterial(), requisicaomaterial.getRequisicaoorigem().getProducaoagenda());
		}
		return false;
	}
	
	private List<Requisicaomaterial> montaMaterialitemmestregrade(List<Material> listaMaterialitemgrade) {
		List<Requisicaomaterial> lista = new ArrayList<Requisicaomaterial>();
		
		if(listaMaterialitemgrade != null && !listaMaterialitemgrade.isEmpty()){
			for(Material material : listaMaterialitemgrade){
				Requisicaomaterial itemBean = new Requisicaomaterial();
				itemBean.setMaterial(material);
				lista.add(itemBean);
			}
		}
		return lista;
	}
	/**
	 * Monta lista de movimenta��o de estoque com as requisi��es com a classe produto/Epi/Servi�o
	 * @param listaProdutoEpiServico
	 * @param cdrequisicoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movimentacaoestoque> preparaMovimentacaoEstoque(List<Requisicaomaterial> listaProdutoEpiServico, StringBuilder cdrequisicoes) {
		List<Movimentacaoestoque> movimentacoesProdutoEpiServico = new ArrayList<Movimentacaoestoque>();
		
		//Monta movimentacao do tipo saida para cada produto/Epi/Servi�o se tiver sido digitado qtde recebida e com valor > que 0
		for (Requisicaomaterial requisicaomaterial : listaProdutoEpiServico) {
			if(requisicaomaterial.getListaMaterialitemmestregrade() != null && !requisicaomaterial.getListaMaterialitemmestregrade().isEmpty()){
				for(Requisicaomaterial itemGrade : requisicaomaterial.getListaMaterialitemmestregrade()){
					if(itemGrade.getQtdepedida() != null && itemGrade.getQtdepedida() > 0){
						if(!cdrequisicoes.toString().contains(requisicaomaterial.getCdrequisicaomaterial().toString()))
							cdrequisicoes.append(requisicaomaterial.getCdrequisicaomaterial() + ",");
						
						Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
						movimentacaoestoqueorigem.setRequisicaomaterial(itemGrade);
						
						Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque(itemGrade.getMaterial(), Movimentacaoestoquetipo.SAIDA, itemGrade.getQtdepedida(), 
										requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getEmpresa(), movimentacaoestoqueorigem, itemGrade.getMaterialclasse(), requisicaomaterial.getProjeto());
						
						movimentacaoestoque.setValor(materialService.load(itemGrade.getMaterial(), "material.valorcusto").getValorcusto());
						if(SinedUtil.isRateioMovimentacaoEstoque()){
							movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, itemGrade.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.REQUISICAO_MATERIAL, itemGrade.getCentrocusto(), itemGrade.getProjeto()));
						}
						movimentacoesProdutoEpiServico.add(movimentacaoestoque);
					}
				}
			}else {
				if(requisicaomaterial.getQtdepedida() != null && requisicaomaterial.getQtdepedida() > 0){
					cdrequisicoes.append(requisicaomaterial.getCdrequisicaomaterial() + ",");
					
					Movimentacaoestoqueorigem movimentacaoestoqueorigem = new Movimentacaoestoqueorigem();
					movimentacaoestoqueorigem.setRequisicaomaterial(requisicaomaterial);
					
					Movimentacaoestoque movimentacaoestoque = new Movimentacaoestoque(requisicaomaterial.getMaterial(), Movimentacaoestoquetipo.SAIDA, requisicaomaterial.getQtdepedida(), 
									requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getEmpresa(), movimentacaoestoqueorigem, requisicaomaterial.getMaterialclasse(), requisicaomaterial.getProjeto());
					
					movimentacaoestoque.setValor(materialService.load(requisicaomaterial.getMaterial(), "material.valorcusto").getValorcusto());
					movimentacaoestoque.setLoteestoque(requisicaomaterial.getLoteestoque());
					if(SinedUtil.isRateioMovimentacaoEstoque()){
						movimentacaoestoque.setRateio(rateioService.criarRateio(movimentacaoestoque, requisicaomaterial.getMaterial(), MaterialRateioEstoqueTipoContaGerencial.REQUISICAO_MATERIAL, requisicaomaterial.getCentrocusto(), requisicaomaterial.getProjeto()));
					}
					movimentacoesProdutoEpiServico.add(movimentacaoestoque);
				}
			}
		}
		return movimentacoesProdutoEpiServico;
	}
	
	/**
	 * M�todo que cria uma lista com as plaquetas digitadas discartandos os itens que n�o foram digitados.
	 * 
	 * @param listaPatrimonio
	 * @param cdrequisicoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Requisicaomaterial> criaListaPlaquetasDigitadas(List<Requisicaomaterial> listaPatrimonio, StringBuilder cdrequisicoes) {
		List<Requisicaomaterial> patrimoniosComPlaquetas = new ArrayList<Requisicaomaterial>();
		for (Requisicaomaterial requisicaomaterial : listaPatrimonio) 
			if(requisicaomaterial.getPlaqueta() != null && !requisicaomaterial.getPlaqueta().equals("")){
				patrimoniosComPlaquetas.add(requisicaomaterial);
				//Adiciona requisi�oes somente 1 vez
				if(cdrequisicoes.indexOf(requisicaomaterial.getCdrequisicaomaterial().toString()) == -1){
					cdrequisicoes.append(requisicaomaterial.getCdrequisicaomaterial() + ",");
				}
			}
		return patrimoniosComPlaquetas;
	}
	
	
	/**
	 * Prepara as movimenta��es para salvar
	 * 
	 * @see br.com.linkcom.sined.geral.service.PatrimonioitemService#findPatrimonioItemByPlaqueta(String)
	 * @param patrimoniosComPlaquetas
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Movpatrimonio> montaMovimenta��esParaSalvar(List<Requisicaomaterial> patrimoniosComPlaquetas) {
		List<Movpatrimonio> movimentacoesPatrimonio = new ArrayList<Movpatrimonio>();
		
		//Carrega os patrimonios referentes as plaquetas
		String whereIn = CollectionsUtil.listAndConcatenate(patrimoniosComPlaquetas, "plaqueta", "','");
		List<Patrimonioitem> listPatrimonio = patrimonioitemService.findPatrimonioItemByPlaqueta(whereIn);
		
		//Monta movimenta��o para cada patrimonio
		for (Requisicaomaterial requisicaomaterial : patrimoniosComPlaquetas) {
			for (Patrimonioitem patrimonioitem : listPatrimonio) {
			
				if(requisicaomaterial.getPlaqueta().equals(patrimonioitem.getPlaqueta())){
					Movpatrimonioorigem movpatrimonioorigem = new Movpatrimonioorigem();
					movpatrimonioorigem.setRequisicaomaterial(requisicaomaterial);
					
					Movpatrimonio movpatrimonio = new Movpatrimonio(movpatrimonioorigem, patrimonioitem, requisicaomaterial.getDepartamento(), 
																	requisicaomaterial.getEmpresa(), requisicaomaterial.getLocalarmazenagem(), 
																	requisicaomaterial.getProjeto());

					movimentacoesPatrimonio.add(movpatrimonio);
					break;
				}
			}
		}
		
		return movimentacoesPatrimonio;
	}
	
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#callProcedureAtualizaRequisicaomaterial
	 * @param requisicaomaterial
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaRequisicaomaterial(Requisicaomaterial requisicaomaterial){
		requisicaomaterialDAO.callProcedureAtualizaRequisicaomaterial(requisicaomaterial);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	private void callProcedureAtualizaRequisicaomaterial(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Requisicaomaterial requisicaomaterial;
			for (int i = 0; i < ids.length; i++) {
				requisicaomaterial = new Requisicaomaterial(Integer.parseInt(ids[i].trim()));
				this.callProcedureAtualizaRequisicaomaterial(requisicaomaterial);
			}
		}
	}
	
	/**
	 * Gera relat�rio da listagem
	 * 
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public IReport gerarRelatorio(RequisicaomaterialFiltro filtro) {
		
		Report report = new Report("/suprimento/requisicaomaterial");
		
		List<Requisicaomaterial> listaRequisicaomaterial = requisicaomaterialDAO.findForRequisicaomaterialReport(filtro);
		List<RequisicaomaterialReportBean> lista = new ListSet<RequisicaomaterialReportBean>(RequisicaomaterialReportBean.class);
		RequisicaomaterialReportBean bean;
		
		if(listaRequisicaomaterial != null && listaRequisicaomaterial.size() > 0){
			for(Requisicaomaterial r : listaRequisicaomaterial){
				bean = new RequisicaomaterialReportBean();
				
				if(r.getIdentificador() != null)
					bean.setIdent(r.getIdentificador().toString());
				else if(r.getCdrequisicaomaterial() != null){
					bean.setIdent(r.getCdrequisicaomaterial().toString());
				}
				if(r.getDescricao() != null)
					bean.setDescricao(r.getDescricao());
				if(r.getMaterial() != null){
					bean.setMaterialservico(r.getMaterial().getAutocompleteDescription());
					bean.setUnidademedida(r.getMaterial().getUnidademedida() != null ? r.getMaterial().getUnidademedida().getSimbolo() : "");
				}
				if(r.getQtde() != null)
					bean.setQtde(SinedUtil.descriptionDecimal(r.getQtde()));
				if(r.getLocalarmazenagem() != null)
					bean.setLocalarmazenagem(r.getLocalarmazenagem().getNome());
				if(r.getDtlimite() != null)
					bean.setDatalimite(SinedDateUtils.toString(r.getDtlimite()));
				if(r.getAux_requisicaomaterial() != null && r.getAux_requisicaomaterial().getSituacaorequisicao() != null)
					bean.setSituacao(r.getAux_requisicaomaterial().getSituacaorequisicao().getDescricao());
				if(r.getColaborador() != null)
					bean.setOrigem(r.getColaborador().getNome());
				if(StringUtils.isNotEmpty(r.getObservacao()))
					bean.setObservacao(r.getObservacao());
				if(r.getDtcriacao() != null)
					bean.setDatacriacao(SinedDateUtils.toString(r.getDtcriacao()));
				
				lista.add(bean);	
			}
			
			report.setDataSource(lista);
		}
		return report;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param filtro
	 * @return
	 * @author Rafael Salvio
	 */
	public List<Requisicaomaterial> findForCsv(RequisicaomaterialFiltro filtro){
		return requisicaomaterialDAO.findForCsv(filtro);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#findForEnvioemailcompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicaomaterial> findForEnvioemailcompra(String whereIn) {
		return requisicaomaterialDAO.findForEnvioemailcompra(whereIn);
	}
	
	/**
	 * M�todo que envia email de acordo com o processo de compra
	 *
	 * @param whereIn
	 * @param situacaorequisicao
	 * @author Luiz Fernando
	 */
	public void enviarEmailRequisicaomaterial(String whereIn, Situacaorequisicao situacaorequisicao, String notWhereInCdusuario) {
		if(whereIn != null && !"".equals(whereIn)){
			List<Requisicaomaterial> listaRequisicoes = this.findForEnvioemailcompra(whereIn);
			String assunto = "";
			if(situacaorequisicao.equals(Situacaorequisicao.AUTORIZADA)){
				assunto = "Requisi��o Autorizada";
			}else if(situacaorequisicao.equals(Situacaorequisicao.EM_ABERTO)){
				assunto = "Requisi��o Criada";
			}
			if(listaRequisicoes != null && !listaRequisicoes.isEmpty()){
				for(Requisicaomaterial requisicaomaterial : listaRequisicoes){
					List<Usuario> listaUsuario = null;
					if(situacaorequisicao.equals(Situacaorequisicao.EM_ABERTO)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.AUTORIZAR_REQUISICAOMATERIAL, requisicaomaterial.getEmpresa(),
							requisicaomaterial.getProjeto() != null && requisicaomaterial.getProjeto().getCdprojeto() != null ? requisicaomaterial.getProjeto().getCdprojeto().toString() : null, null, notWhereInCdusuario != null && !"".equals(notWhereInCdusuario) ? notWhereInCdusuario : null);
					}else if(situacaorequisicao.equals(Situacaorequisicao.AUTORIZADA)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.GERAR_COMPRA_REQUISICAOMATERIAL, requisicaomaterial.getEmpresa(),
								requisicaomaterial.getProjeto() != null && requisicaomaterial.getProjeto().getCdprojeto() != null ? requisicaomaterial.getProjeto().getCdprojeto().toString() : null, null, notWhereInCdusuario != null && !"".equals(notWhereInCdusuario) ? notWhereInCdusuario : null);
					}
					
					if(listaUsuario != null && !listaUsuario.isEmpty()){
						for(Usuario usuario : listaUsuario){
							if(usuario.getEmail() != null && !"".equals(usuario.getEmail()) && (usuario.getBloqueado() == null || !usuario.getBloqueado())){
								EmailManager email;							
								try {
									email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom("w3erp@w3erp.com.br")
										.setSubject(assunto)
										.setTo(usuario.getEmail());
									
									if(situacaorequisicao.equals(Situacaorequisicao.AUTORIZADA)){
										email.addHtmlText("Aviso do sistema: A requisi��o " + requisicaomaterial.getIdentificador() + " foi autorizada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext()
												 + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}else if(situacaorequisicao.equals(Situacaorequisicao.EM_ABERTO)){
										email.addHtmlText("Aviso do sistema: A requisi��o " + requisicaomaterial.getIdentificador() + " foi criada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext()
												 + usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}
									new EmailManagerThread(email, SinedUtil.getSubdominioCliente()).start();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findRequisitanteMaterialByEntregaBaixada(String whereInEntrega)
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicaomaterial> findRequisitanteMaterialByEntregaBaixada(String whereInEntrega) {
		return requisicaomaterialDAO.findRequisitanteMaterialByEntregaBaixada(whereInEntrega);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findRequisitanteMaterialByEntrega(String whereInEntrega)
	 *
	 * @param whereInEntrega
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Requisicaomaterial> findRequisitanteMaterialByEntrega(String whereInEntrega) {
		return requisicaomaterialDAO.findRequisitanteMaterialByEntrega(whereInEntrega);
	}
	
	public List<Requisicaomaterial> findProcessoCompra(String whereIn) {
		return requisicaomaterialDAO.findProcessoCompra(whereIn);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findByCotacaoWithArquivo(Cotacao cotacao)
	 *
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public List<Requisicaomaterial> findByCotacaoWithArquivo(Cotacao cotacao) {
		return requisicaomaterialDAO.findByCotacaoWithArquivo(cotacao);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findBySolicitacaocompraWithArquivo(String whereInSolicitacoes)
	 *
	 * @param whereInSolicitacoes
	 * @return
	 * @author Rodrigo Freitas
	 * @since 27/11/2012
	 */
	public List<Requisicaomaterial> findBySolicitacaocompraWithArquivo(String whereInSolicitacoes) {
		return requisicaomaterialDAO.findBySolicitacaocompraWithArquivo(whereInSolicitacoes);
	}
	
	/**
	 * M�todo que verifica a unidademedida do material e converte a qtde caso necess�rio 
	 *
	 * @param requisicaomaterial
	 * @author Luiz Fernando
	 */
	public void verificaUnidademedidaQtde(Requisicaomaterial requisicaomaterial) {
		if(requisicaomaterial != null && requisicaomaterial.getMaterial() != null && requisicaomaterial.getUnidademedidatrans() != null && 
				requisicaomaterial.getUnidademedidatrans().getCdunidademedida() != null){
			Material material = materialService.unidadeMedidaMaterial(requisicaomaterial.getMaterial());
			if(material != null && material.getUnidademedida() != null && material.getUnidademedida().getCdunidademedida() != null && 
					!requisicaomaterial.getUnidademedidatrans().getCdunidademedida().equals(material.getUnidademedida().getCdunidademedida())){
				requisicaomaterial.setQtde(unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), 
												requisicaomaterial.getQtde(), 
												requisicaomaterial.getUnidademedidatrans(), 
												material,null,1.0));
			}
		}
	}
	
	public List<Requisicaomaterial> getListaMaterialByPedidovenda(Pedidovenda pedidovenda) {
		List<Requisicaomaterial> requisicoes = new ArrayList<Requisicaomaterial>();
		if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
				requisicaomaterial.setCdpedidovenda(pedidovenda.getCdpedidovenda());
				requisicaomaterial.setMaterial(pedidovendamaterial.getMaterial());
				requisicaomaterial.setQtde(pedidovendamaterial.getQuantidade());
				requisicaomaterial.setUnidademedidatrans(pedidovendamaterial.getUnidademedida());
				requisicaomaterial.setUnidademedida(pedidovendamaterial.getUnidademedida().getSimbolo());
				if(pedidovendamaterial.getMaterial() != null){
					if(pedidovendamaterial.getMaterial().getProduto() != null && pedidovendamaterial.getMaterial().getProduto()){
						requisicaomaterial.setMaterialclasse(Materialclasse.PRODUTO);
					}else if(pedidovendamaterial.getMaterial().getPatrimonio() != null && pedidovendamaterial.getMaterial().getPatrimonio()){
						requisicaomaterial.setMaterialclasse(Materialclasse.PATRIMONIO);
					}else if(pedidovendamaterial.getMaterial().getEpi() != null && pedidovendamaterial.getMaterial().getEpi()){
						requisicaomaterial.setMaterialclasse(Materialclasse.EPI);
					}else {
						requisicaomaterial = null;
					}
				}
				if(requisicaomaterial != null){
					requisicoes.add(requisicaomaterial);
				}
			}
		
		}
		return requisicoes;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 17/04/2014
	 */
	public List<Requisicaomaterial> findForRomaneio(String whereIn) {
		return requisicaomaterialDAO.findForRomaneio(whereIn);
	}

	/**
	 * M�todo que adiciona o material a lista de requisi��es
	 *
	 * @param listaRequisicaomaterial
	 * @param requisicaomaterial
	 * @author Lucas Costa
	 */
	public void addMateriaisAgendaproducao(List<Requisicaomaterial> listaRequisicaomaterial, Requisicaomaterial requisicaomaterial) {
		if(requisicaomaterial.getQtde() > 0){
			boolean adicionar = true;
			if(listaRequisicaomaterial.size() > 0){
				for(Requisicaomaterial rm : listaRequisicaomaterial){
					if(rm.getMaterial().equals(requisicaomaterial.getMaterial())){
						adicionar = false;
						rm.setQtde(rm.getQtde() + requisicaomaterial.getQtde());
						break;
					}
				}
			}
			if(adicionar)
				listaRequisicaomaterial.add(requisicaomaterial);
		}
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param requisicaomaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 28/01/2015
	 */
	public Requisicaomaterial loadWithSituacao(Requisicaomaterial requisicaomaterial) {
		return requisicaomaterialDAO.loadWithSituacao(requisicaomaterial);
	}
	
	/**
	* M�todo que retorna a situa��o da requisicao de material do pedido de venda
	*
	* @param pedidovenda
	* @return
	* @since 25/08/2015
	* @author Luiz Fernando
	*/
	public Situacaopedidosolicitacaocompra getSituacaoRequisicaomaterialByPedidovenda(Pedidovenda pedidovenda) {
		Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra = null;
		
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			Double qtdesolicitacao = this.getQteRequisicaomaterialByPedidovenda(pedidovenda);
			if(qtdesolicitacao > 0){
				Double qtdebaixada = this.getQteRequisicaomaterialBaixadaByPedidovenda(pedidovenda);
				if(qtdebaixada == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.SOLICITADO;
				}else if(qtdebaixada < qtdesolicitacao){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_PARCIAL;
				}else if(qtdebaixada.compareTo(qtdesolicitacao) == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_REALIZADA;
				}
			}
		}
		return situacaopedidosolicitacaocompra;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#getQteRequisicaomaterialByPedidovenda(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 25/08/2015
	* @author Luiz Fernando
	*/
	public Double getQteRequisicaomaterialByPedidovenda(Pedidovenda pedidovenda) {
		return requisicaomaterialDAO.getQteRequisicaomaterialByPedidovenda(pedidovenda);
	}

	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#getQteRequisicaomaterialBaixadaByPedidovenda(Pedidovenda pedidovenda)
	*
	* @param pedidovenda
	* @return
	* @since 25/08/2015
	* @author Luiz Fernando
	*/
	public Double getQteRequisicaomaterialBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		return requisicaomaterialDAO.getQteRequisicaomaterialBaixadaByPedidovenda(pedidovenda);
	}
	
	/**
	* M�todo que valida se a quantidade solicitada � maior que a quantidade do planejamento
	*
	* @see br.com.linkcom.sined.geral.service.RequisicaomaterialService#validaQtdeCompraMaiorQuePlanejamento(List<Requisicaomaterial> lista, BindException errors)
	*
	* @param requisicaomaterial
	* @param errors
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public void validaQtdeCompraMaiorQuePlanejamento(Requisicaomaterial requisicaomaterial, BindException errors) {
		Requisicaomaterial bean = new Requisicaomaterial();
		bean.setRequisicoes(new ArrayList<Requisicaomaterial>());
		bean.getRequisicoes().add(requisicaomaterial);
		validaQtdeCompraMaiorQuePlanejamento(bean.getRequisicoes(), requisicaomaterial.getProjeto(), errors);
	}
	
	/**
	* M�todo que valida se a quantidade solicitada � maior que a quantidade do planejamento
	*
	* @param lista
	* @param errors
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public void validaQtdeCompraMaiorQuePlanejamento(List<Requisicaomaterial> lista, Projeto projeto, BindException errors) {
	 	if(SinedUtil.isListNotEmpty(lista) && projeto != null && "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARCOMPRAPROJETO))){
	 		String whereInMaterial = CollectionsUtil.listAndConcatenate(lista, "material.cdmaterial", ",");
	 		List<Planejamentorecursogeral> listaPlanejamentorecursogeral = planejamentorecursogeralService.findForBloquearQtdeCompra(projeto, whereInMaterial);
 			
	 		if(listaPlanejamentorecursogeral == null || listaPlanejamentorecursogeral.size() == 0){
	 			StringBuilder materiais = new StringBuilder();
	 			for(Requisicaomaterial requisicaomaterial : lista){
	 				if(requisicaomaterial.getMaterial() != null && requisicaomaterial.getMaterial().getNome() != null){
	 					materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(requisicaomaterial.getMaterial().getNome());
	 				}
	 			}
	 			errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
	 		}else {
	 			List<Material> listaMaterialEncontrado = new ArrayList<Material>();
	 			StringBuilder materiais = new StringBuilder();
	 			for(Requisicaomaterial requisicaomaterial : lista){
	 				if(requisicaomaterial.getMaterial() != null){
	 					boolean materialEncontrado = false;
	 					for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
	 						if(prg.getMaterial() != null && prg.getMaterial().equals(requisicaomaterial.getMaterial())){
	 							materialEncontrado = true;
	 							if(!listaMaterialEncontrado.contains(requisicaomaterial.getMaterial())){
	 								listaMaterialEncontrado.add(requisicaomaterial.getMaterial());
	 							}
	 							break;
	 						}
	 					}
	 					if(!materialEncontrado){
	 						materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(requisicaomaterial.getMaterial().getNome());
	 					}
	 				}
	 			}
	 			if(StringUtils.isNotBlank(materiais.toString())){
	 				errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
	 			}
	 			
 				StringBuilder whereInRequisicaomaterial = new StringBuilder();
 				String whereIn = null;
 				for(Requisicaomaterial requisicaomaterial : lista){
 					if(requisicaomaterial.getCdrequisicaomaterial() != null) whereInRequisicaomaterial.append(requisicaomaterial.getCdrequisicaomaterial()).append(",");
 				}
 				if(StringUtils.isNotBlank(whereInRequisicaomaterial.toString())){
 					whereIn = whereInRequisicaomaterial.substring(0, whereInRequisicaomaterial.length()-1);
 				}
 				
 				List<Requisicaomaterial> listaAgrupada = agruparItensValidacaoPlanejamento(lista);
 				List<Requisicaomaterial> listaRM = findForBloquearQtdeCompra(projeto, whereInMaterial, whereIn);
 				
 				StringBuilder menagem =  new StringBuilder();
 				for(Requisicaomaterial requisicaomaterial : listaAgrupada){
 		 			if(requisicaomaterial.getQtde() != null && requisicaomaterial.getMaterial() != null && listaMaterialEncontrado.contains(requisicaomaterial.getMaterial())){
 		 				Double qtdecomprada = 0d;
 		 				Double qtdePrevista = 0d;
 		 				if(SinedUtil.isListNotEmpty(listaRM)){
	 		 				for(Requisicaomaterial rm : listaRM){
 								if(rm.getQtde() != null && rm.getMaterial() != null && rm.getMaterial().equals(requisicaomaterial.getMaterial())){
 									qtdecomprada = rm.getQtde();
 									break;
 								}
	 		 				}
 						}
 		 				
 		 				for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
							if(prg.getQtde() != null && prg.getMaterial() != null && prg.getMaterial().equals(requisicaomaterial.getMaterial())){
								qtdePrevista += prg.getQtde();
							}
 		 				}
 		 				
 		 				Double qtdeAcumulada = qtdecomprada + requisicaomaterial.getQtde();
 		 				if(qtdeAcumulada > qtdePrevista){
 		 					menagem.append("<br>&nbsp;&nbsp;&nbsp;" + requisicaomaterial.getMaterial().getNome() + " - Qtde Prevista: " + qtdePrevista + " - Qtde Acumulada: " + qtdeAcumulada);
 		 				}
 		 			}
 		 		}
 				if(StringUtils.isNotBlank(menagem.toString())){
 					errors.reject("001", "A quantidade de material comprado ou solicitado extrapola a quantidade prevista no Planejamento." + menagem);
 				}
 			}
 		}
	}
	
	/**
	* M�todo que agrupa os itens da requisi��o de mateiral de acordo com o material
	*
	* @param listaRequisicaomaterial
	* @return
	* @since 13/05/2016
	* @author Luiz Fernando
	*/
	private List<Requisicaomaterial> agruparItensValidacaoPlanejamento(List<Requisicaomaterial> listaRequisicaomaterial) {
		List<Requisicaomaterial> listaAgrupada = new ArrayList<Requisicaomaterial>();
		if(SinedUtil.isListNotEmpty(listaRequisicaomaterial)){
			for(Requisicaomaterial rm : listaRequisicaomaterial){
				if(rm.getQtde() != null && rm.getMaterial() != null &&
 		 					(rm.getAux_requisicaomaterial() == null ||
 		 							rm.getAux_requisicaomaterial().getSituacaorequisicao() == null ||
							 !Situacaorequisicao.BAIXADA.equals(rm.getAux_requisicaomaterial().getSituacaorequisicao()))){
					boolean adicionar = true;
					for(Requisicaomaterial itemAgrupado : listaAgrupada){
						if(rm.getMaterial().equals(itemAgrupado.getMaterial())){
							itemAgrupado.setQtde(itemAgrupado.getQtde() + rm.getQtde());
							adicionar = false;
						}
					}
					if(adicionar){
						Requisicaomaterial requisicaomaterial = new Requisicaomaterial();
						requisicaomaterial.setMaterial(rm.getMaterial());
						requisicaomaterial.setQtde(rm.getQtde());
						requisicaomaterial.setProjeto(rm.getProjeto());
						listaAgrupada.add(requisicaomaterial);
					}
				}
			}
		}
		return listaAgrupada;
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.RequisicaomaterialDAO#findForBloquearQtdeCompra(Projeto projeto, String whereInMaterial, String whereIn)
	*
	* @param projeto
	* @param whereInMaterial
	* @param whereIn
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	public List<Requisicaomaterial> findForBloquearQtdeCompra(Projeto projeto, String whereInMaterial, String whereIn) {
		return requisicaomaterialDAO.findForBloquearQtdeCompra(projeto, whereInMaterial, whereIn);
	}
	
	public List<GerarRomaneioBean> preparaRomaneioList(WebRequestContext request, List<Requisicaomaterial> requisicoes, String whereIn) {
		List<GerarRomaneioBean> lista = new ArrayList<GerarRomaneioBean>();
		
		for (Requisicaomaterial requisicaomaterial : requisicoes) {
			Double qtdeRomaneio = romaneioitemService.getQtdeRomaneioByRequisicaomaterial(requisicaomaterial); 
			Double qtdeRestante = requisicaomaterial.getQtde() - (qtdeRomaneio != null ? qtdeRomaneio : 0d);
			
			if(requisicaomaterial.getMaterialclasse().equals(Materialclasse.PATRIMONIO)){
				for (int i = 0; i < qtdeRestante; i++) {
					GerarRomaneioBean bean = new GerarRomaneioBean();
					
					bean.setMaterial(requisicaomaterial.getMaterial());
					bean.setMaterialclasse(requisicaomaterial.getMaterialclasse());
					bean.setLocalarmazenagem(requisicaomaterial.getLocalarmazenagem());
					bean.setProjeto(requisicaomaterial.getProjeto());
					bean.setRequisicaomaterial(requisicaomaterial);
					bean.setQtdeRestante(1d);
					bean.setQtde(1d);
					
					lista.add(bean);
				}
			} else{
				GerarRomaneioBean bean = new GerarRomaneioBean();
				
				bean.setMaterial(requisicaomaterial.getMaterial());
				bean.setMaterialclasse(requisicaomaterial.getMaterialclasse());
				bean.setLocalarmazenagem(requisicaomaterial.getLocalarmazenagem());
				bean.setProjeto(requisicaomaterial.getProjeto());
				bean.setRequisicaomaterial(requisicaomaterial);
				bean.setQtdeRestante(qtdeRestante);
				bean.setQtde(qtdeRestante);
				
				lista.add(bean);
			}
		}
		
		return lista;	
	}
	
	
	public Requisicaomaterial criaRequisicaomaterialByImportacao (Arquivo arquivo, Integer id, String descricao, Localarmazenagem local, java.sql.Date dtLimite, Centrocusto centroCusto, Projeto projeto, Empresa empresa, Colaborador requisicao, String observacao){
		Requisicaomaterial requisicaoMaterial = new Requisicaomaterial();
		requisicaoMaterial.setIdentificador(id);
		requisicaoMaterial.setDescricao(descricao);
		requisicaoMaterial.setLocalarmazenagem(local);
		requisicaoMaterial.setDtlimite(dtLimite);
		requisicaoMaterial.setCentrocusto(centroCusto);
		requisicaoMaterial.setProjeto(projeto);
		requisicaoMaterial.setEmpresa(empresa);
		requisicaoMaterial.setColaborador(requisicao);
		requisicaoMaterial.setObservacao(observacao);
		requisicaoMaterial.setDtcriacao(new java.sql.Date(System.currentTimeMillis()));
		List<Requisicaomaterial> listaRequisicoes = new ArrayList<Requisicaomaterial>();
		Requisicaomaterial rm;
		Material material;
		Unidademedida unidademedida;
		List<String> listaErro = new ArrayList<String>();
		if(arquivo != null){
			String strFile = new String(arquivo.getContent());
			String[] linhas = strFile.split("\\r?\\n");
			
			for (int i = 1; i < linhas.length; i++) {
				if(linhas[i] != null && !linhas[i].equals("")){
					String[] campos = linhas[i].split(";");
					
					ImportarDadosRequisicaomaterialBean bean = new ImportarDadosRequisicaomaterialBean();
					
					try{
						bean.setLinha(i+1);
						if(campos.length > 0){
						
							if(campos.length > 0) bean.setIdentificador(campos[0]);
							if(campos.length > 1) bean.setQuantidadeStr(campos[1]);
							if(campos.length > 2) bean.setUnidademedidaStr(campos[2]);
							if(campos.length > 3) bean.setQtdeVolume(campos[3]);
							
							if(bean.getIdentificador() == null || bean.getIdentificador().equals("")){
								listaErro.add("O campo Identificador do material n�o foi preenchido. Linha: " + (i+1));
							} else {
								material = materialService.getMaterialByIdentificacao(bean.getIdentificador());
								if(material != null){
									rm = new Requisicaomaterial();
									rm.setMaterial(material);
									rm.setUnidademedidatrans(material.getUnidademedida());
								//	rm.setContagerencial(material.getContagerencial());
									rm.setQtdefrequencia(1.0);
									rm.setFrequencia(new Frequencia(1));
									rm.setFrequenciatrans(new Frequencia(1));
									rm.setIdentificador(id);
									rm.setDescricao(descricao);
									rm.setLocalarmazenagem(local);
									rm.setDtlimite(dtLimite);
									rm.setCentrocusto(centroCusto);
									rm.setProjeto(projeto);
									rm.setEmpresa(empresa);
									rm.setColaborador(requisicao);
									rm.setObservacao(observacao);
									rm.setDtcriacao(new java.sql.Date(System.currentTimeMillis()));
									if(material.getProduto() !=null && material.getProduto()){
										rm.setMaterialclasse(Materialclasse.PRODUTO);
									}
									if(material.getServico()!=null && material.getServico()){
										rm.setMaterialclasse(Materialclasse.SERVICO);
									}
									if(material.getEpi() !=null && material.getEpi()){
										rm.setMaterialclasse(Materialclasse.EPI);
									}
									if(material.getPatrimonio()!=null && material.getPatrimonio()){
										rm.setMaterialclasse(Materialclasse.PATRIMONIO);
									}
									
									if(bean.getQuantidadeStr() != null && !bean.getQuantidadeStr().equals("")){
										try{
											Double qtde = Double.valueOf(bean.getQuantidadeStr().replace(",", "."));
											rm.setQtde(qtde);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getUnidademedidaStr() != null && !bean.getUnidademedidaStr().equals("")){
										try{
											unidademedida = unidademedidaService.findBySimbolo(bean.getUnidademedidaStr().trim());
											if(unidademedida != null)
												rm.setUnidademedidatrans(unidademedida);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if(bean.getQtdeVolume() != null && !bean.getQtdeVolume().equals("")){
										try{
											Double volume = Double.valueOf(bean.getQtdeVolume().replace(",", "."));
											rm.setQtdvolume(volume);
											//ordemcompramaterial.setValor(valorunitario*ordemcompramaterial.getQtdpedida());
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
			
									listaRequisicoes.add(rm);
								}else {
									listaErro.add("O material n�o foi encontrado. Identificador: " + bean.getIdentificador());
								}
							}
						}
					} catch (Exception e) {
						listaErro.add(e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}
		
		if(listaErro.size() > 0)
			requisicaoMaterial.setListaErros(listaErro);
		
		requisicaoMaterial.setRequisicoes(listaRequisicoes);
		
		return requisicaoMaterial;
		
	}
}
