package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.dao.MotivoavisoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MotivoavisoService extends GenericService<Motivoaviso>{

	private MotivoavisoDAO  motivoavisoDAO;
	
	public void setMotivoavisoDAO(MotivoavisoDAO motivoavisoDAO) {
		this.motivoavisoDAO = motivoavisoDAO;
	}

	public Motivoaviso findByMotivo(MotivoavisoEnum motivo) {
		return motivoavisoDAO.findByMotivo(motivo);
	}
	
	public List<Motivoaviso> findAllForCreateAviso() {
		return motivoavisoDAO.findAllForCreateAviso();
	}

	public void updateChecksMotivoAviso(String selectedItens, String acao) {
		motivoavisoDAO.updateChecksMotivoAviso(selectedItens, acao);
	}
}
