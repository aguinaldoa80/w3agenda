package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColetaMaterial;
import br.com.linkcom.sined.geral.bean.Coletamaterialmotivodevolucao;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.dao.ColetamaterialmotivodevolucaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColetamaterialmotivodevolucaoService extends GenericService<Coletamaterialmotivodevolucao> {
	
	private ColetamaterialmotivodevolucaoDAO coletamaterialmotivodevolucaoDAO;
	
	public void setColetamaterialmotivodevolucaoDAO(ColetamaterialmotivodevolucaoDAO coletamaterialmotivodevolucaoDAO) {
		this.coletamaterialmotivodevolucaoDAO = coletamaterialmotivodevolucaoDAO;
	}
	
	public void save(ColetaMaterial coletaMaterial, Motivodevolucao motivodevolucao, boolean delete){
		if (delete){
			this.delete(coletaMaterial);
		}
		
		if (motivodevolucao!=null && motivodevolucao.getCdmotivodevolucao()!=null){
			Coletamaterialmotivodevolucao coletamaterialmotivodevolucao = new Coletamaterialmotivodevolucao();
			coletamaterialmotivodevolucao.setColetaMaterial(coletaMaterial);
			coletamaterialmotivodevolucao.setMotivodevolucao(motivodevolucao);
			saveOrUpdate(coletamaterialmotivodevolucao);
		}
	}
	
	public void saveLista(ColetaMaterial coletaMaterial, List<Motivodevolucao> listaMotivodevolucao, boolean delete){
		if (delete){
			this.delete(coletaMaterial);
		}
		
		if (listaMotivodevolucao!=null){
			for (Motivodevolucao motivodevolucao: listaMotivodevolucao){
				Coletamaterialmotivodevolucao coletamaterialmotivodevolucao = new Coletamaterialmotivodevolucao();
				coletamaterialmotivodevolucao.setColetaMaterial(coletaMaterial);
				coletamaterialmotivodevolucao.setMotivodevolucao(motivodevolucao);
				saveOrUpdate(coletamaterialmotivodevolucao);
			}
		}
	}
	
	public void delete(ColetaMaterial coletaMaterial){
		coletamaterialmotivodevolucaoDAO.delete(coletaMaterial);
	}
}
