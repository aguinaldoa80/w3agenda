package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Processojuridicosituacao;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProcessojuridicosituacaoService extends GenericService<Processojuridicosituacao> { 
	
	/* singleton */
	private static ProcessojuridicosituacaoService instance;
	public static ProcessojuridicosituacaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProcessojuridicosituacaoService.class);
		}
		return instance;
	}

}
