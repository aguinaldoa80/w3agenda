package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetoarquivo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProjetoarquivoService extends GenericService<Projetoarquivo> {	
	
	protected ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	
	/* singleton */
	private static ProjetoarquivoService instance;
	public static ProjetoarquivoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ProjetoarquivoService.class);
		}
		return instance;
	}
	
	
	/**
	 * M�todo para salvar os arquivos de uma lista de aquivoprojeto
	 * 
	 * @param projeto
	 * @author Jo�o Paulo Zica
	 */
	public void saveArquivos(Projeto projeto) {
		if (projeto == null) {
			throw new SinedException("O par�metro projeto n�o pode ser null.");
		}
		List<Projetoarquivo> listaProjetoarquivo = projeto.getListaProjetoarquivo();
		if (listaProjetoarquivo != null && listaProjetoarquivo.size() > 0) {
			for (Projetoarquivo projetoarquivo : listaProjetoarquivo) {
				arquivoDAO.saveFile(projetoarquivo, "arquivo");
			}
		}
	}
}
