package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Areaquestionario;
import br.com.linkcom.sined.geral.dao.AreaquestionarioDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AreaquestionarioService extends GenericService<Areaquestionario> {

	private AreaquestionarioDAO areaquestionarioDAO;
	
	public void setAreaquestionarioDAO(AreaquestionarioDAO areaquestionarioDAO) {
		this.areaquestionarioDAO = areaquestionarioDAO;
	}
	
	public List<Areaquestionario> findByArea(Area area) {
		return areaquestionarioDAO.findByArea(area);
	}
	
}
