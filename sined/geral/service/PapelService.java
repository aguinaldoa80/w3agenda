package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.dao.PapelDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PapelService extends GenericService<Papel> {

	private PapelDAO papelDAO;
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	/**
	 * M�todo para salvar a lista de papel em usu�rio papel
	 * 
	 * @param listaPapel
	 * @param bean
	 * @return
	 * @author Joao Paulo Zica
	 */
	public List<Usuariopapel> createListaUsuariopapel(List<Papel> listaPapel, Usuario bean){
		List<Usuariopapel> listaUsuariopapel = new ListSet<Usuariopapel>(Usuariopapel.class);
		if (listaPapel != null) {
			for (Papel papel : listaPapel) {
				Usuariopapel usuariopapel = new Usuariopapel();
				usuariopapel.setPapel(papel);
				usuariopapel.setPessoa(bean);
				listaUsuariopapel.add(usuariopapel);
			}
		}
		return listaUsuariopapel;
	}
	
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.PapelDAO#savePermissaoTelaUsuario(Papel)
	 * @param papel
	 * @author Jo�o Paulo Zica
	 */
	public void savePermissaoTelaUsuario(Papel papel){
		papelDAO.savePermissaoTelaUsuario(papel);
	}
	
	/* singleton */
	private static PapelService instance;
	public static PapelService getInstance() {
		if(instance == null){
			instance = Neo.getObject(PapelService.class);
		}
		return instance;
	}
	
	/**
	 * Verifica se a lista de papel passada tem 
	 * algum papel como administrador
	 * @param listaPapel
	 * @return
	 * @author C�ntia Nogueira
	 */
	public boolean isAdministrador(List<Papel> listaPapel){		
		for(Papel papel: listaPapel){
			if(papel.getAdministrador()!= null && papel.getAdministrador()){
				return true;
			}
		}
		
		return false;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param key
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Papel> carregaPapeisPermissaoAcao(String key) {
		return papelDAO.carregaPapeisPermissaoAcao(key);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param pathPermissao
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Papel> carregaPapeisPermissaoUpdate(String pathPermissao) {
		return papelDAO.carregaPapeisPermissaoUpdate(pathPermissao);
	}
}
