package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Tiporelacaorecurso;
import br.com.linkcom.sined.geral.dao.TiporelacaorecursoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TiporelacaorecursoService extends GenericService<Tiporelacaorecurso>{

	private TiporelacaorecursoDAO tiporelacaorecursoDAO;
	
	public void setTiporelacaorecursoDAO(TiporelacaorecursoDAO tiporelacaorecursoDAO) {
		this.tiporelacaorecursoDAO = tiporelacaorecursoDAO;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *
	 * @see br.com.linkcom.sined.geral.dao.TiporelacaorecursoDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Alvarenga
	 */
	public List<Tiporelacaorecurso> findAllForFlex(){
		return tiporelacaorecursoDAO.findAllForFlex();
	}	
	
}
