package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.ColaboradorDespesaMotivoItem;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.dao.ColaboradorDespesaMotivoItemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ColaboradorDespesaMotivoItemService extends GenericService<ColaboradorDespesaMotivoItem>{
	
	
	private ColaboradorDespesaMotivoItemDAO colaboradorDespesaMotivoItemDAO;
	
	public void setColaboradorDespesaMotivoItemDAO(
			ColaboradorDespesaMotivoItemDAO colaboradorDespesaMotivoItemDAO) {
		this.colaboradorDespesaMotivoItemDAO = colaboradorDespesaMotivoItemDAO;
	}

	public List<ColaboradorDespesaMotivoItem> findByMotivo(Colaboradordespesamotivo motivo, Empresa empresa){
		return colaboradorDespesaMotivoItemDAO.findByMotivo(motivo, empresa);
	}
}
