package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeCte;
import br.com.linkcom.sined.geral.dao.MdfeCteDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeCteService extends GenericService<MdfeCte>{

	private MdfeCteDAO mdfeCteDAO;
	public void setMdfeCteDAO(MdfeCteDAO mdfeCteDAO) {
		this.mdfeCteDAO = mdfeCteDAO;
	}
	
	public List<MdfeCte> findByMdfe(Mdfe mdfe){
		return mdfeCteDAO.findByMdfe(mdfe); 
	}
}
