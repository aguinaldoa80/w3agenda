package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Restricaoacessopessoa;
import br.com.linkcom.sined.geral.dao.RestricaoacessopessoaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RestricaoacessopessoaService extends GenericService<Restricaoacessopessoa> {
	
	private RestricaoacessopessoaDAO restricaoacessopessoaDAO;
	
	public void setRestricaoacessopessoaDAO(RestricaoacessopessoaDAO restricaoacessopessoaDAO) {
		this.restricaoacessopessoaDAO = restricaoacessopessoaDAO;
	}
	
	/* singleton */
	private static RestricaoacessopessoaService instance;
	public static RestricaoacessopessoaService getInstance() {
		if (instance==null){
			instance = Neo.getObject(RestricaoacessopessoaService.class);
		}
		return instance;
	}
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * 
	 */
	public List<Restricaoacessopessoa> findByPessoa(Pessoa pessoa){
		return restricaoacessopessoaDAO.findByPessoa(pessoa);
	}
	
	/**
	 * 
	 * @param pessoa
	 * @author Thiago Clemente
	 * 
	 */
	public boolean isPossuiRestricaoAcessoHorario(Pessoa pessoa){
		return restricaoacessopessoaDAO.isPossuiRestricaoAcessoHorario(pessoa);
	}
	
	/**
	 * 
	 * @param pessoa
	 * @param modulo
	 * @author Filipe Santos
	 * 
	 */
	public boolean isPossuiRestricaoAcessoDia(Pessoa pessoa){
		return restricaoacessopessoaDAO.isPossuiRestricaoAcessoDia(pessoa);
	}
}