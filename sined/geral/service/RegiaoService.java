package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.sined.geral.bean.Regiao;
import br.com.linkcom.sined.geral.dao.RegiaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class RegiaoService extends GenericService<Regiao> {
	
	private static RegiaoService instance;
	
	public static RegiaoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(RegiaoService.class);
		}
		return instance;
	}
	private RegiaoDAO regiaoDAO;
	
	public void setRegiaoDAO(RegiaoDAO regiaoDAO) {
		this.regiaoDAO = regiaoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param regiao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 15/06/2016
	 */
	public Regiao loadForFiltro(Regiao regiao){
		return regiaoDAO.loadForFiltro(regiao);
	}

	public Regiao findByCep(Cep cep) {
		return regiaoDAO.findByCep(cep);
	}

}
