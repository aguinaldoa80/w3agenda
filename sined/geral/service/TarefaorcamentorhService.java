package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Tarefaorcamentorh;
import br.com.linkcom.sined.geral.dao.TarefaorcamentorhDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefaorcamentorhService extends GenericService<Tarefaorcamentorh>{
	
	private TarefaorcamentorhDAO tarefaorcamentorhDAO;
	
	public void setTarefaorcamentorhDAO(
			TarefaorcamentorhDAO tarefaorcamentorhDAO) {
		this.tarefaorcamentorhDAO = tarefaorcamentorhDAO;
	}

	public List<Tarefaorcamentorh> findByOrcamento(Orcamento orcamento) {
		return tarefaorcamentorhDAO.findByOrcamento(orcamento);
	}
}
