package br.com.linkcom.sined.geral.service;

import java.sql.Timestamp;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cacheusuariooffline;
import br.com.linkcom.sined.geral.dao.CacheusuarioofflineDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CacheusuarioofflineService extends GenericService<Cacheusuariooffline> {
	private CacheusuarioofflineDAO cacheusuarioofflineDAO;
		
	public void setCacheusuarioofflineDAO(CacheusuarioofflineDAO cacheusuarioofflineDAO) {
		this.cacheusuarioofflineDAO = cacheusuarioofflineDAO;
	}

	/* singleton */
	private static CacheusuarioofflineService instance;
	
	public static CacheusuarioofflineService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CacheusuarioofflineService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param cdusuario
	 * @return
	 * 
	 * @author Rafael Salvio
	 */
	public Timestamp getDtalteraByUsuario(Integer cdusuario){
		return cacheusuarioofflineDAO.getDtalteraByUsuario(cdusuario);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @author Rafael Salvio
	 */
	public void resetCacheusuariooffline(){
		cacheusuarioofflineDAO.resetCacheusuariooffline();
	}
	
}
