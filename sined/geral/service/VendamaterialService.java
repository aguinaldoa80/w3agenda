package br.com.linkcom.sined.geral.service;

import java.io.IOException;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Expedicaohistorico;
import br.com.linkcom.sined.geral.bean.Expedicaoitem;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialproducao;
import br.com.linkcom.sined.geral.bean.Materialrelacionado;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Reserva;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.Vendamaterial;
import br.com.linkcom.sined.geral.bean.Vendamaterialmestre;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.dao.VendamaterialDAO;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ExpedicaoTrocaLoteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.filtro.QuantitativaDeVendasFiltro;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.PneuWSBean;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.VendaMaterialWSBean;
import br.com.linkcom.sined.util.EnumUtils;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.utils.DateUtils;

public class VendamaterialService extends GenericService<Vendamaterial> {
	
	private VendamaterialDAO vendamaterialDAO;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private MaterialrelacionadoService materialrelacionadoService;
	private VendaService vendaService;
	private PneuService pneuService;
	private OrdemservicoveterinariamaterialService ordemservicoveterinariamaterialService;
	private ReservaService reservaService;
	private ExpedicaoService expedicaoService;
	private PedidovendatipoService pedidovendatipoService;
	private static final String[] ABREVIATURA_MESES = {"Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"};
	
	public void setVendamaterialDAO(VendamaterialDAO vendamaterialDAO) {
		this.vendamaterialDAO = vendamaterialDAO;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialrelacionadoService(MaterialrelacionadoService materialrelacionadoService) {
		this.materialrelacionadoService = materialrelacionadoService;
	}
	public void setVendaService(VendaService vendaService) {
		this.vendaService = vendaService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setOrdemservicoveterinariamaterialService(OrdemservicoveterinariamaterialService ordemservicoveterinariamaterialService) {
		this.ordemservicoveterinariamaterialService = ordemservicoveterinariamaterialService;
	}
	public void setReservaService(ReservaService reservaService) {
		this.reservaService = reservaService;
	}
	public void setExpedicaoService(ExpedicaoService expedicaoService) {
		this.expedicaoService = expedicaoService;
	}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}
	
	@Override
	public void saveOrUpdate(Vendamaterial bean) {
		if(bean.getPneu() != null){
			if(bean.getPneu().existeDados()){
				pneuService.saveOrUpdate(bean.getPneu());
			} else {
				bean.setPneu(null);
			}
		}
		super.saveOrUpdate(bean);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @param venda
	 * @return
	 * @author Ramon Brazil
	 */
	public List<Vendamaterial> findByVenda(Venda venda){
		return vendamaterialDAO.findByVenda(venda, null);
	}
	
	public List<Vendamaterial> findForExcelByVenda(Venda venda){
		return vendamaterialDAO.findForExcelByVenda(venda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/02/2014
	 */
	public List<Vendamaterial> findByVenda(String whereInVenda){
		return vendamaterialDAO.findByVenda(whereInVenda, null);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#findForResumo(Venda venda)
	 *
	 * @param whereInVenda
	 * @return
	 * @author Luiz Fernando
	 * @since 17/09/2013
	 */
	public List<Vendamaterial> findForResumo(String whereInVenda){
		return vendamaterialDAO.findForResumo(whereInVenda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#findByVenda(Venda venda, String orderBy)
	 *
	 * @param venda
	 * @param orderBy
	 * @return
	 * @author Luiz Fernando
	 * @since 13/09/2013
	 */
	public List<Vendamaterial> findByVenda(Venda venda, String orderBy){
		return vendamaterialDAO.findByVenda(venda, orderBy);
	}
		
	/* singleton */
	private static VendamaterialService instance;
	public static VendamaterialService getInstance() {
		if(instance == null){
			instance = Neo.getObject(VendamaterialService.class);
		}
		return instance;
	}

	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#findForOrdemProducao
	 *
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Vendamaterial> findForOrdemProducao(Venda venda) {
		return vendamaterialDAO.findForOrdemProducao(venda);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param venda
	 * @author Tom�s Rabelo
	 */
	public void deleteAllFromVenda(Venda venda) {
		vendamaterialDAO.deleteAllFromVenda(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 *
	 * @param material,cliente
	 * @return
	 * @author Rafael Salvio
	 */
	public Vendamaterial getUltimoValorMaterialByCliente(Material material,Cliente cliente, Empresa empresa, Unidademedida unidadeMedida){
		List<Vendamaterial> lista = vendamaterialDAO.getUltimoValorMaterialByCliente(material, cliente, empresa, unidadeMedida);
		if(lista!=null && !lista.isEmpty()){
			for(Vendamaterial vm:lista){
				if(vm.getMaterial()!=null && vm.getPreco()!=null)
					return vm;
			}
		}
		return null;
	}
	
	public Vendamaterial getUltimoValorDataMaterialByCliente(Material material,Cliente cliente, Empresa empresa, Unidademedida unidadeMedida){
		List<Vendamaterial> lista = vendamaterialDAO.getUltimoValorMaterialByCliente(material, cliente, empresa, unidadeMedida);
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#findForComissaoRepresentante(Venda venda)
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Vendamaterial> findForComissaoRepresentante(Venda venda) {
		return vendamaterialDAO.findForComissaoRepresentante(venda);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#getUltimoValor(Integer cdmaterial)
	 *
	 * @param cdmaterial
	 * @param cdempresa 
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getUltimoValor(Integer cdmaterial, Integer cdempresa) {
		return vendamaterialDAO.getUltimoValor(cdmaterial, cdempresa);
	}
	
	/**
	 * M�todo que cria a copia do item da venda (vendamaterial)
	 * 
	 * @param vm
	 * @return
	 * @author Luiz Fernando
	 */
	public Vendamaterial criaCopiaVendamaterial(Vendamaterial vm){
		if(vm == null) return vm;
		
		Vendamaterial vendamaterial = new Vendamaterial();
		
		vendamaterial.setCdvendamaterial(vm.getCdvendamaterial());
		vendamaterial.setVenda(vm.getVenda());
		vendamaterial.setMaterial(vm.getMaterial());
		vendamaterial.setQuantidade(vm.getQuantidade());
		vendamaterial.setComprimento(vm.getComprimento());
		vendamaterial.setComprimentooriginal(vm.getComprimentooriginal());
		vendamaterial.setLargura(vm.getLargura());
		vendamaterial.setAltura(vm.getAltura());
		vendamaterial.setFatorconversao(vm.getFatorconversao());
		vendamaterial.setQtdereferencia(vm.getQtdereferencia());
		vendamaterial.setPreco(vm.getPreco());
		vendamaterial.setDesconto(vm.getDesconto());
		vendamaterial.setUnidademedida(vm.getUnidademedida());
		vendamaterial.setPedidovendamaterial(vm.getPedidovendamaterial());
		vendamaterial.setPrazoentrega(vm.getPrazoentrega());
		vendamaterial.setObservacao(vm.getObservacao());
		vendamaterial.setLoteestoque(vm.getLoteestoque());
		vendamaterial.setValorcustomaterial(vm.getValorcustomaterial());
		vendamaterial.setSaldo(vm.getSaldo());
		vendamaterial.setMultiplicador(vm.getMultiplicador());
		vendamaterial.setProducaosemestoque(vm.getProducaosemestoque());
		vendamaterial.setMaterialmestre(vm.getMaterialmestre());
		vendamaterial.setCdproducaoordemitemadicional(vm.getCdproducaoordemitemadicional());
		vendamaterial.setCdproducaoagendaitemadicional(vm.getCdproducaoagendaitemadicional());
		
		//TRANSIENTES
		vendamaterial.setTotal(vm.getTotal());
		vendamaterial.setAchou(vm.isAchou());
		vendamaterial.setQtdedevolvida(vm.getQtdejadevolvida());
		vendamaterial.setQtdejadevolvida(vm.getQtdejadevolvida());
		vendamaterial.setValorvendamaterial(vm.getValorvendamaterial());
		vendamaterial.setPercentualdescontocustovenda(vm.getPercentualdescontocustovenda());
		vendamaterial.setMargemcustovenda(vm.getMargemcustovenda());
		vendamaterial.setProdutotrans(vm.getProdutotrans());
		
		vendamaterial.setExistematerialsimilar(vm.getExistematerialsimilar());
		vendamaterial.setExistematerialsimilarColeta(vm.getExistematerialsimilarColeta());
		
		return vendamaterial;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#findByVendaProducao(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 * @since 22/07/2013
	 */
	public List<Vendamaterial> findByVendaProducao(String whereIn) {
		return vendamaterialDAO.findByVendaProducao(whereIn);
	}
	
	private List<Vendamaterial> getListaVendamaterialKit(Venda venda) {
		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
		if(venda != null && SinedUtil.isListNotEmpty(venda.getListavendamaterial())){
			for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
				if(vendamaterial.getMaterialmestre() != null || vendamaterial.getMaterial().getVendapromocional() == null || 
						!vendamaterial.getMaterial().getVendapromocional()){ 
					listaVendamaterial.add(vendamaterial);
				}else if(vendamaterial.getMaterial().getVendapromocional() != null && vendamaterial.getMaterial().getVendapromocional()){
					Material material = materialService.loadMaterialPromocionalComMaterialrelacionado(vendamaterial.getMaterial().getCdmaterial());
					venda.setMaterial(material);
					material.setListaMaterialrelacionado(materialrelacionadoService.ordernarListaKit(material.getListaMaterialrelacionado()));
					
					try{
						material.setProduto_altura(vendamaterial.getAltura());
						material.setProduto_largura(vendamaterial.getLargura());
						material.setQuantidade(vendamaterial.getQuantidade());
						materialrelacionadoService.calculaQuantidadeKit(material);			
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(material != null && material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
						List<Vendamaterial> listaItensKit = new ArrayList<Vendamaterial>();
						Double qtdeTotal = 0d;
						Integer qdteItens = material.getListaMaterialrelacionado().size();
						Money valorTotalItens = new Money();
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getValorvenda() != null){
								valorTotalItens = valorTotalItens.add(new Money(materialrelacionado.getValorvenda().getValue().doubleValue() * (materialrelacionado.getQuantidade() != null && materialrelacionado.getQuantidade() > 0 ? materialrelacionado.getQuantidade() : 1d)));
							}
						}
						for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
							if(materialrelacionado.getMaterialpromocao() != null){
								Vendamaterial vm = new Vendamaterial();
								vm.setCdvendamaterial(vendamaterial.getCdvendamaterial());
								vm.setMaterial(materialrelacionado.getMaterialpromocao());
								
								if(qdteItens == 1){
									vm.setGrupotributacao(vendamaterial.getGrupotributacao());
									vm.setTipocalculoipi(vendamaterial.getTipocalculoipi());
									vm.setTipocobrancaipi(vendamaterial.getTipocobrancaipi());
									vm.setAliquotareaisipi(vendamaterial.getAliquotareaisipi());
									vm.setIpi(vendamaterial.getIpi());
									vm.setValoripi(vendamaterial.getValoripi());
								}
								
								Double qtde = vendamaterial.getQuantidade();
								Double preco = vendamaterial.getPreco() != null ? vendamaterial.getPreco() : 0d;
								Unidademedida unidademedida = vendamaterial.getUnidademedida();
								if(!unidademedidaService.existeMaterialComUnidademedidaVendida(vendamaterial.getUnidademedida(), materialrelacionado) && 
											vendamaterial.getFatorconversao() != null && vendamaterial.getQtdereferencia() != null && 
											vendamaterial.getFatorconversaoQtdereferencia() != 0){
									qtde = qtde / vm.getFatorconversaoQtdereferencia();
									preco = preco * vm.getFatorconversaoQtdereferencia();
									unidademedida = materialrelacionado.getMaterialpromocao().getUnidademedida();
								}
									
								vm.setQuantidade(materialrelacionado.getQuantidade() * qtde);
								vm.setUnidademedida(unidademedida);
								
								if(Boolean.TRUE.equals(materialrelacionado.getExibirKitObservacaoItemNota())){
									if(material.getNomenf() != null){
										vm.setNomeKitParaNF(material.getNomenf());
									}else if(material.getNome() != null){
										vm.setNomeKitParaNF(material.getNome());
									}
								}
									
								Double valorvendaItemKit = materialrelacionado.getValorvenda() != null ? materialrelacionado.getValorvenda().getValue().doubleValue() : 0d;
								Double valorvendaMaterial = valorTotalItens != null && valorTotalItens.getValue().doubleValue() > 0 ? valorTotalItens.getValue().doubleValue() : material.getValorvenda() != null ? material.getValorvenda() : 0d;
								if(valorvendaMaterial == 0) valorvendaMaterial = 1d;
								vm.setPreco(preco * ((valorvendaItemKit * 100) / valorvendaMaterial) / 100);
								
								qtdeTotal += vm.getQuantidade();
								vm.setVendamaterialMaterialmestre(vendamaterial);
								listaItensKit.add(vm);
							}
						}
						if(listaItensKit != null && !listaItensKit.isEmpty()){
							for(Vendamaterial vm : listaItensKit){
								if(vendamaterial.getDesconto() != null && qtdeTotal != null && vm.getQuantidade() != null){
									Double valordesconto = vendamaterial.getDesconto().getValue().doubleValue() / qtdeTotal * vm.getQuantidade();
									vm.setDesconto(new Money(valordesconto));
								}
								listaVendamaterial.add(vm);
							}
						}
					}				
				}
			}
		}		
		return listaVendamaterial;
	}
	
	private List<Vendamaterial> getListaVendamaterialForConsiderarKit(Venda venda) {
		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
		if(venda != null && venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					if(vendamaterial.getMaterialmestre() == null || !Boolean.TRUE.equals(vendamaterial.getMaterialmestre().getVendapromocional()) ||
							venda.getListaVendamaterialmestre() == null || venda.getListaVendamaterialmestre().isEmpty()){ 
						listaVendamaterial.add(vendamaterial);
					}
				}
				if(venda.getListaVendamaterialmestre() != null && !venda.getListaVendamaterialmestre().isEmpty()){
					List<Material> listaKits = new ArrayList<Material>();
					for(Vendamaterialmestre vendamaterialmestre : venda.getListaVendamaterialmestre()){
						if(vendamaterialmestre.getMaterial() != null && vendamaterialmestre.getQtde() != null &&
								Boolean.TRUE.equals(vendamaterialmestre.getMaterial().getVendapromocional()) &&
								!listaKits.contains(vendamaterialmestre.getMaterial())){
							listaKits.add(vendamaterialmestre.getMaterial());
							Money valorTotalItens = new Money();
							for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
								if(vendamaterial.getMaterialmestre() != null && 
										vendamaterial.getMaterialmestre().equals(vendamaterialmestre.getMaterial())){
									valorTotalItens = valorTotalItens.add(vendamaterial.getTotalproduto());
								}
							}
							Vendamaterial vm = new Vendamaterial();
							vm.setMaterial(vendamaterialmestre.getMaterial());
							vm.setQuantidade(vendamaterialmestre.getQtde());
							vm.setUnidademedida(vendamaterialmestre.getMaterial().getUnidademedida());
							vm.setPreco(valorTotalItens.getValue().doubleValue() / vendamaterialmestre.getQtde());
							listaVendamaterial.add(vm);
						}
					}
				}
			}
		}		
		return listaVendamaterial;
	}

	/**
	 * M�todo que retorna a lista organizada com os itens que devem ser discriminados na nota
	 *
	 * @param venda
	 * @return
	 * @author Luiz Fernando
	 * @since 06/11/2013
	 */
	private List<Vendamaterial> getListaVendamaterialDiscriminarnota(Venda venda) {
		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
		if(venda != null && venda.getListaVendamaterialmestre() != null &&  
				!venda.getListaVendamaterialmestre().isEmpty() &&
				venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
			
			if(venda.getListaVendamaterialmestre() != null && !venda.getListaVendamaterialmestre().isEmpty()){
				StringBuilder whereInMaterialmestre = new StringBuilder();
				for(Vendamaterialmestre vendamaterialmestre : venda.getListaVendamaterialmestre()){
					if(vendamaterialmestre.getMaterial() != null && vendamaterialmestre.getMaterial().getCdmaterial() != null &&
							vendamaterialmestre.getMaterial().getProducao() != null && 
							vendamaterialmestre.getMaterial().getProducao() &&
							(vendamaterialmestre.getMaterial().getVendapromocional() == null || 
									!vendamaterialmestre.getMaterial().getVendapromocional()) &&
							(vendamaterialmestre.getMaterial().getKitflexivel() == null || 
									!vendamaterialmestre.getMaterial().getKitflexivel())){
						if(!whereInMaterialmestre.toString().equals("")) whereInMaterialmestre.append(",");
						whereInMaterialmestre.append(vendamaterialmestre.getMaterial().getCdmaterial());
					}
				}
				if(!whereInMaterialmestre.toString().equals("")){
					List<Material> listaMat = materialService.findMaterialComMaterialproducao(whereInMaterialmestre.toString());
					if(listaMat != null && !listaMat.isEmpty()){
						for(Material material : listaMat){
							for(Vendamaterialmestre vendamaterialmestre : venda.getListaVendamaterialmestre()){
								if(vendamaterialmestre.getMaterial() != null && vendamaterialmestre.getMaterial().equals(material)){
									vendamaterialmestre.setMaterial(material);
								}
							}
						}
					}
				}
			}
			
			if(venda.getListavendamaterial() != null && !venda.getListavendamaterial().isEmpty()){
				for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
					if(vendamaterial.getMaterialmestre() == null || (vendamaterial.getMaterialmestre().getProducao() == null || 
							!vendamaterial.getMaterialmestre().getProducao()) || 
							!containsListaproduccao(vendamaterial.getMaterialmestre(), vendamaterial.getMaterial(), venda.getListaVendamaterialmestre())){
						listaVendamaterial.add(vendamaterial);
					}
				}
			}
			
			for(Vendamaterialmestre vendamaterialmestre : venda.getListaVendamaterialmestre()){
				if(vendamaterialmestre.getMaterial() != null && (vendamaterialmestre.getMaterial().getVendapromocional() == null || 
								!vendamaterialmestre.getMaterial().getVendapromocional()) && 
						vendamaterialmestre.getMaterial().getListaProducao() != null && 
						!vendamaterialmestre.getMaterial().getListaProducao().isEmpty()){
					
					Double valorTotalMaterialmestre = 0.0;
					Money valorTotalDescontoMaterialmestre = new Money();
					Double qtdeMaterialmestre = 1d;
					Double pesoTotal = 0d;
					Double pesobrutoTotal = 0d;
					if(vendamaterialmestre.getQtde() != null && vendamaterialmestre.getQtde() > 0){
						qtdeMaterialmestre = vendamaterialmestre.getQtde();
					}
					List<Materialproducao> listaMaterialproducaoNotDescriminarNota = new ArrayList<Materialproducao>();
					List<Materialproducao> listaMaterialproducaoDescriminarNota = new ArrayList<Materialproducao>();
					
					for(Materialproducao materialproducao : vendamaterialmestre.getMaterial().getListaProducao()){
						if(materialproducao.getMaterial() != null){
							if(materialproducao.getDiscriminarnota() != null && materialproducao.getDiscriminarnota()){
								listaMaterialproducaoDescriminarNota.add(materialproducao);
							}else {
								listaMaterialproducaoNotDescriminarNota.add(materialproducao);
							}
						}
					}
					
					if((listaMaterialproducaoDescriminarNota == null || listaMaterialproducaoDescriminarNota.isEmpty()) || 
					   (listaMaterialproducaoNotDescriminarNota != null && !listaMaterialproducaoNotDescriminarNota.isEmpty())){
						
						if(listaMaterialproducaoDescriminarNota == null || listaMaterialproducaoDescriminarNota.isEmpty()){
							for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
								if(vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterialmestre().equals(vendamaterialmestre.getMaterial())){
									if(vendamaterial.getPreco() != null && vendamaterial.getQuantidade() != null){
										valorTotalMaterialmestre += vendamaterial.getTotalproduto().getValue().doubleValue();
										if(vendamaterial.getDesconto() != null){
											valorTotalDescontoMaterialmestre = valorTotalDescontoMaterialmestre.add(vendamaterial.getDesconto()); 
										}
										if(vendamaterial.getPesoVendaOuMaterial() != null){
											pesoTotal += vendamaterial.getPesoVendaOuMaterial() * vendamaterial.getQuantidade();
										}
										if(vendamaterial.getMaterial().getPesobruto() != null){
											pesobrutoTotal += vendamaterial.getMaterial().getPesobruto() * vendamaterial.getQuantidade();
										}
									}
								}
							}
							
							Double valorUnitario = valorTotalMaterialmestre / qtdeMaterialmestre;
							
							
							Vendamaterial vendamaterial = new Vendamaterial();
							vendamaterial.setMaterial(vendamaterialmestre.getMaterial());
							vendamaterial.getMaterial().setPeso(pesoTotal);
							vendamaterial.getMaterial().setPesobruto(pesobrutoTotal);
							vendamaterial.setQuantidade(vendamaterialmestre.getQtde());
							vendamaterial.setPreco(valorUnitario);
							vendamaterial.setDesconto(valorTotalDescontoMaterialmestre);
							vendamaterial.setUnidademedida(vendamaterialmestre.getMaterial().getUnidademedida());
							
							listaVendamaterial.add(vendamaterial);							
						}else if(listaMaterialproducaoNotDescriminarNota != null && !listaMaterialproducaoNotDescriminarNota.isEmpty()){
							for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
								if(vendamaterial.getMaterialmestre() != null && 
										vendamaterial.getMaterialmestre().equals(vendamaterialmestre.getMaterial())){
									for(Materialproducao materialproducao : listaMaterialproducaoNotDescriminarNota){
										if(materialproducao.getMaterial() != null && vendamaterial.getMaterial().equals(materialproducao.getMaterial())){
											if(vendamaterial.getPreco() != null && vendamaterial.getQuantidade() != null){
												valorTotalMaterialmestre += SinedUtil.round(vendamaterial.getTotalproduto().getValue().doubleValue(),2);
												if(vendamaterial.getDesconto() != null){
													valorTotalDescontoMaterialmestre = valorTotalDescontoMaterialmestre.add(vendamaterial.getDesconto()); 
												}
												if(vendamaterial.getPesoVendaOuMaterial() != null){
													pesoTotal += vendamaterial.getPesoVendaOuMaterial() * vendamaterial.getQuantidade();
												}
												if(vendamaterial.getMaterial().getPesobruto() != null){
													pesobrutoTotal += vendamaterial.getMaterial().getPesobruto() * vendamaterial.getQuantidade();
												}
											}
											break;
										}
									}
								}
							}
							
							Double valorUnitario = valorTotalMaterialmestre / qtdeMaterialmestre;
							
							Vendamaterial vendamaterial = new Vendamaterial();
							vendamaterial.setMaterial(vendamaterialmestre.getMaterial());
							vendamaterial.getMaterial().setPeso(pesoTotal);
							vendamaterial.getMaterial().setPesobruto(pesobrutoTotal);
							vendamaterial.setQuantidade(vendamaterialmestre.getQtde());
							vendamaterial.setPreco(valorUnitario);
							vendamaterial.setDesconto(valorTotalDescontoMaterialmestre);
							vendamaterial.setUnidademedida(vendamaterialmestre.getMaterial().getUnidademedida());
							
							listaVendamaterial.add(vendamaterial);
						}
					}					
					
					if(listaMaterialproducaoDescriminarNota != null && !listaMaterialproducaoDescriminarNota.isEmpty()){ 
						for(Materialproducao materialproducao : listaMaterialproducaoDescriminarNota){
							for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
								if(vendamaterial.getMaterialmestre() != null && vendamaterial.getMaterial() != null &&
										vendamaterial.getMaterial().equals(materialproducao.getMaterial())){
									listaVendamaterial.add(vendamaterial);
								}
							}
						}
					}
				}				
			}
			
		}		
		return listaVendamaterial;
	}
	
	/**
	 * M�todo que verifica se o material est� contido na lista de produ��o de um material mestre
	 *
	 * @param materialmestre
	 * @param material
	 * @return
	 * @author Luiz Fernando
	 * @param listaVendamaterialmestre 
	 * @since 16/12/2013
	 */
	private boolean containsListaproduccao(Material materialmestre, Material material, List<Vendamaterialmestre> listaVendamaterialmestre) {
		boolean retorno = false;
		
		if(listaVendamaterialmestre != null && !listaVendamaterialmestre.isEmpty() && materialmestre != null && material != null &&
				materialmestre.getCdmaterial() != null && material.getCdmaterial() != null){
			for(Vendamaterialmestre vendamaterialmestre : listaVendamaterialmestre){
				if(vendamaterialmestre.getMaterial() != null && vendamaterialmestre.getMaterial().getCdmaterial() != null &&
						vendamaterialmestre.getMaterial().getCdmaterial().equals(materialmestre.getCdmaterial())){
						
						if(vendamaterialmestre.getMaterial().getListaProducao() != null && 
								!vendamaterialmestre.getMaterial().getListaProducao().isEmpty() && 
								material != null && material.getCdmaterial() != null){
							
							for(Materialproducao materialproducao : vendamaterialmestre.getMaterial().getListaProducao()){
								if(materialproducao.getMaterial() != null && materialproducao.getMaterial().getCdmaterial() != null && 
										material.getCdmaterial().equals(materialproducao.getMaterial().getCdmaterial())){
									retorno = true;
									break;
								}
								
							}
						}
						break;
				}
			}
		}
		return retorno;
	}
	
	/**
	 * M�todo que verifica qual material ser� discriminado na nota (caso o item fa�a parte de uma material de produ��o)
	 *
	 * @param venda
	 * @author Luiz Fernando
	 * @since 06/11/2013
	 */
	public void verificaVendaProducaoDiscriminarnota(Venda venda) {
		List<Vendamaterial> listavendamaterialAjustada = this.getListaVendamaterialDiscriminarnota(venda);
		if(listavendamaterialAjustada != null && !listavendamaterialAjustada.isEmpty()){
			venda.setListavendamaterial(listavendamaterialAjustada);
		}
	}
	
	/**
	 * M�todo que verifica se existe material de kit e adiciona os itens do kit para gerar a nota
	 *
	 * @param venda
	 * @author Luiz Fernando
	 * @since 29/04/2014
	 */
	public void verificaVendaKitDiscriminarnota(Venda venda) {
		List<Vendamaterial> listavendamaterialAjustada = this.getListaVendamaterialKit(venda);
		if(listavendamaterialAjustada != null && !listavendamaterialAjustada.isEmpty()){
			venda.setListavendamaterial(listavendamaterialAjustada);
		}
	}
	
	public void verificaVendaKitForConsiderarKit(Venda venda) {
		List<Vendamaterial> listavendamaterialAjustada = this.getListaVendamaterialForConsiderarKit(venda);
		if(listavendamaterialAjustada != null && !listavendamaterialAjustada.isEmpty()){
			venda.setListavendamaterial(listavendamaterialAjustada);
		}
	}
	
	public void updateSaldo(Vendamaterial vm, Double saldo) {
		vendamaterialDAO.updateSaldo(vm, saldo);
	}
	
	/**
	 * M�todo que retorna a lista de itens da venda com a unidade convertida para a principal, caso a o par�metro NOTAFISCAL_UNIDADEPRINCIPAL seja TRUE
	 *
	 * @param listavendamaterial
	 * @return
	 * @author Luiz Fernando
	 * @since 19/12/2013
	 */
	public List<Vendamaterial> converterForUnidadeprincipal(List<Vendamaterial> listavendamaterial) {
		if(listavendamaterial != null && !listavendamaterial.isEmpty()){
 			for(Vendamaterial vendamaterial : listavendamaterial){
				if(vendamaterial.getUnidademedida() != null && vendamaterial.getUnidademedida().getCdunidademedida() != null &&
						vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getUnidademedida() != null &&
						vendamaterial.getMaterial().getUnidademedida().getCdunidademedida() != null &&
						!vendamaterial.getMaterial().getUnidademedida().equals(vendamaterial.getUnidademedida())){
					
					boolean existelargura = vendamaterial.getLargura() != null && vendamaterial.getLargura() != 0;
					boolean existealtura = vendamaterial.getAltura() != null && vendamaterial.getAltura() != 0;
					boolean existecomprimento = vendamaterial.getComprimento() != null && vendamaterial.getComprimento() != 0;
					
					Double qtde = vendamaterial.getQuantidade();
					if( vendamaterial.getUnidademedida().getSimbolo() != null && 
						vendamaterial.getMaterial().getUnidademedida().getSimbolo() != null &&
						vendamaterial.getUnidademedida().getSimbolo().equalsIgnoreCase("MT") &&
						vendamaterial.getMaterial().getUnidademedida().getSimbolo().equalsIgnoreCase("M3") &&
						(	(existelargura && (existealtura || existecomprimento)) ||
							(existealtura && (existelargura || existecomprimento)) ||
							(existecomprimento && (existealtura || existelargura))
						)){
						qtde = (existelargura ? vendamaterial.getLargura() : 1) *
									  (existealtura? vendamaterial.getAltura() : 1) *
									  (existecomprimento ? vendamaterial.getComprimento() : 1);
						qtde = SinedUtil.roundByParametro(qtde);
						vendamaterial.setQuantidade(qtde * vendamaterial.getQuantidade());
						if(vendamaterial.getValorvendamaterial() != null && qtde != 0){
							vendamaterial.setValorvendamaterial(vendamaterial.getValorvendamaterial() / qtde);
						}
						if(vendamaterial.getPreco() != null && qtde != 0){
							vendamaterial.setPreco(vendamaterial.getPreco() / qtde);
						}
					}else {
						vendamaterial.setQuantidade(unidademedidaService.converteQtdeUnidademedida(vendamaterial.getMaterial().getUnidademedida(), 
															qtde, 
															vendamaterial.getUnidademedida(), 
															vendamaterial.getMaterial(),vendamaterial.getFatorconversao(),vendamaterial.getQtdereferencia()));
						Double fracao = null;
						if(vendamaterial.getFatorconversao() != null && vendamaterial.getQtdereferencia() != null){
							fracao = unidademedidaService.getFracaoconversaoUnidademedida(vendamaterial.getMaterial().getUnidademedida(), vendamaterial.getQuantidade(), vendamaterial.getUnidademedida(), vendamaterial.getMaterial(), vendamaterial.getFatorconversaoQtdereferencia());
						} else {
							fracao = unidademedidaService.getFracaoconversaoUnidademedida(vendamaterial.getMaterial().getUnidademedida(), vendamaterial.getQuantidade(), vendamaterial.getUnidademedida(), vendamaterial.getMaterial(), null);
						}
						if(fracao != null && vendamaterial.getPreco() != null && vendamaterial.getPreco() > 0){
							vendamaterial.setPreco(vendamaterial.getPreco()*fracao);
						}
					}
					vendamaterial.setUnidademedida(vendamaterial.getMaterial().getUnidademedida());
					vendamaterial.setPreco(SinedUtil.roundByParametro(vendamaterial.getPreco(), 10));
					vendamaterial.setQuantidade(SinedUtil.roundByParametro(vendamaterial.getQuantidade()));
				}
			}
		}
		
		return listavendamaterial;
	}
	
	/**
	 * Retorna a lista de vendamaterial a partir da venda e da lista passada por par�metro
	 *
	 * @param listaVendamaterial
	 * @param venda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 13/02/2014
	 */
	public List<Vendamaterial> getVendamaterialByVenda(List<Vendamaterial> listaVendamaterial, Venda venda) {
		List<Vendamaterial> listaVendamaterialNova = new ArrayList<Vendamaterial>();
		for (Vendamaterial vendamaterial : listaVendamaterial) {
			if(vendamaterial.getVenda() != null && 
					vendamaterial.getVenda().getCdvenda() != null &&
					venda != null && 
					venda.getCdvenda() != null &&
					vendamaterial.getVenda().getCdvenda().equals(venda.getCdvenda())){
				listaVendamaterialNova.add(vendamaterial);
			}
		}
		return listaVendamaterialNova;
	}
	public Vendamaterial findMaiorDataEntrega(Venda venda){
		return vendamaterialDAO.findMaiorDataEntrega(venda);
	}
	
	public Vendamaterial getVendamaterialForExpedicao(Vendamaterial vendamaterial, Material materialExpedicaoitem, HashMap<Material, Material> mapKit) {
		if(materialExpedicaoitem != null && vendamaterial != null && vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getVendapromocional() != null &&
				vendamaterial.getMaterial().getVendapromocional()){
			Material material = mapKit.get(vendamaterial.getMaterial());
			if(material != null && material.getListaMaterialrelacionado() != null && !material.getListaMaterialrelacionado().isEmpty()){
				for(Materialrelacionado materialrelacionado : material.getListaMaterialrelacionado()){
					if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getMaterialpromocao().equals(materialExpedicaoitem) ){
						Vendamaterial vm = new Vendamaterial();
						vm.setCdvendamaterial(vendamaterial.getCdvendamaterial());
						vm.setMaterial(materialrelacionado.getMaterialpromocao());
						vm.setQuantidade(materialrelacionado.getQuantidade() * vendamaterial.getQuantidade());
						vm.setUnidademedida(materialrelacionado.getMaterialpromocao().getUnidademedida());
						vm.setVendamaterialMaterialmestre(vendamaterial);
						return vm;
					}
				}
			}
		}
		return vendamaterial;
	}
	
	/**
	 * M�todo que identifica o fornecedor do material na venda
	 *
	 * @param listaVendamaterial
	 * @param fornecedor
	 * @return
	 * @author Jo�o Vitor
	 */
	public Boolean isFornecedorVendamaterial(List<Vendamaterial> listaVendamaterial, Fornecedor fornecedor){
		Boolean exist = Boolean.FALSE;
		if(SinedUtil.isListNotEmpty(listaVendamaterial)){
			for(Vendamaterial vendamaterial : listaVendamaterial){
				if(vendamaterial.getFornecedor() != null){
					if(vendamaterial.getFornecedor() != null && vendamaterial.getFornecedor().equals(fornecedor)){
						return Boolean.TRUE;
					}
				}
			}
		}
		return exist;
	}
	
	public void verificaVendaKitFlexivelExibir(Venda venda) {
		List<Vendamaterial> listavendamaterialAjustada = this.getListaVendamaterialKitFlexivel(venda);
		if(listavendamaterialAjustada != null && !listavendamaterialAjustada.isEmpty()){
			venda.setListavendamaterial(listavendamaterialAjustada);
		}
	}
	
	private List<Vendamaterial> getListaVendamaterialKitFlexivel(Venda venda) {
		List<Vendamaterial> listaVendamaterial = new ArrayList<Vendamaterial>();
		if(venda != null && SinedUtil.isListNotEmpty(venda.getListavendamaterial())){
			List<Vendamaterial> listaKitFlexivel = new ArrayList<Vendamaterial>();
			for(Vendamaterial vendamaterial : venda.getListavendamaterial()){
				if(vendamaterial.getMaterialmestre() == null || vendamaterial.getMaterialmestre().getKitflexivel() == null || 
						!vendamaterial.getMaterialmestre().getKitflexivel()){ 
					listaVendamaterial.add(vendamaterial);
				}else if(vendamaterial.getMaterialmestre().getKitflexivel() != null && vendamaterial.getMaterialmestre().getKitflexivel()){
					Vendamaterialmestre vmm = vendaService.getItemMestre(venda.getListaVendamaterialmestre(), vendamaterial);
					if(vmm == null || (vmm.getExibiritenskitflexivel() != null && vmm.getExibiritenskitflexivel())){
						listaVendamaterial.add(vendamaterial);
					}else {
						Vendamaterial vm = new Vendamaterial();
						vm.setMaterial(vmm.getMaterial());
						vm.setQuantidade(vmm.getQtde());
						vm.setUnidademedida(vmm.getMaterial().getUnidademedida());
						vm.setPreco(vmm.getPreco());
						vm.setDesconto(vmm.getDesconto());
						if(vmm.getPreco() != null && vmm.getQtde() != null){
							Double total = vmm.getPreco()*vmm.getQtde();
							if(vmm.getDesconto() != null){
								total = total - vmm.getDesconto().getValue().doubleValue();
							}
							vm.setTotal(new Money(total));
						}
						vm.setVendamaterialMaterialmestre(vendamaterial);
						vm.setIdentificadorinterno(vmm.getIdentificadorinterno());
						vm.setIpi(vmm.getIpi());
						vm.setAliquotareaisipi(vmm.getAliquotaReaisIpi());
						vm.setValoripi(vmm.getValoripi());
						vm.setTipocalculoipi(vmm.getTipoCalculoIpi());
						vm.setTipocobrancaipi(vmm.getTipocobrancaipi());
						vm.setGrupotributacao(vmm.getGrupotributacao());
						vm.setVendamaterialmestre(vmm);
						
						boolean existeKit = false;
						if(SinedUtil.isListNotEmpty(listaKitFlexivel)){
							for(Vendamaterial vmKit : listaKitFlexivel){
								if(vm.getIdentificadorinterno() != null && vm.getIdentificadorinterno().equals(vmKit.getIdentificadorinterno())){
									existeKit = true;
									break;
								}
							}
						}
						if(!existeKit){
							listaKitFlexivel.add(vm);
						}
					}				
				}
			}
			if(SinedUtil.isListNotEmpty(listaKitFlexivel)){
				if(SinedUtil.isListNotEmpty(listaVendamaterial)){
					listaKitFlexivel.addAll(listaVendamaterial);
					return listaKitFlexivel;
				}else {
					listaVendamaterial.addAll(listaKitFlexivel);
				}
			}
		}		
		return listaVendamaterial;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param whereInVenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 14/04/2015
	 */
	public List<Vendamaterial> findByVendaForIndustrializacao(String whereInVenda) {
		return vendamaterialDAO.findByVendaForIndustrializacao(whereInVenda);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO
	 * @param dtinicio
	 * @param dtfim
	 * @return
	 */
	public List<Vendamaterial> findByMaterialAndDataForAnaliseCusto(Material material, Date dtinicio, Date dtfim){
		return vendamaterialDAO.findByMaterialAndDataForAnaliseCusto(material, dtinicio, dtfim);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.VendamaterialService#updatePneu(Vendamaterial vm, Pneu pneu)
	*
	* @param vm
	* @param pneu
	* @since 03/02/2016
	* @author Luiz Fernando
	*/
	public void updatePneu(Vendamaterial vm, Pneu pneu) {
		vendamaterialDAO.updatePneu(vm, pneu);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.VendamaterialDAO#removePneu(Vendamaterial vendamaterial)
	*
	* @param vendamaterial
	* @since 11/02/2016
	* @author Luiz Fernando
	*/
	public void removePneu(Vendamaterial vendamaterial) {
		vendamaterialDAO.removePneu(vendamaterial);
	}
	
	public List<Vendamaterial> findForConferenciaMateriaisFromVenda(String whereIn) {
		return vendamaterialDAO.findForConferenciaMateriaisFromVenda(whereIn);
	}
	
	public List<Vendamaterial> findForConferenciaMateriaisFromExpedicao(String whereIn) {
		return vendamaterialDAO.findForConferenciaMateriaisFromExpedicao(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see  br.com.linkcom.sined.geral.service.VendamaterialService#findByItensAdicionais(String whereInProducaoordemitemadicional)
	*
	* @param whereInProducaoordemitemadicional
	* @return
	* @since 04/10/2016
	* @author Luiz Fernando
	*/
	public List<Vendamaterial> findByItensAdicionais(String whereInProducaoordemitemadicional, String whereInProducaoagendaitemadicional) {
		return vendamaterialDAO.findByItensAdicionais(whereInProducaoordemitemadicional, whereInProducaoagendaitemadicional);
	}
	
	public Double getQtdeReservada(Material material, Venda venda, Loteestoque loteestoque, String whereInOSVM) {
		String whereInNotReserva = null;
		if(StringUtils.isNotBlank(whereInOSVM)){
			whereInNotReserva = ordemservicoveterinariamaterialService.getWhereInReserva(ordemservicoveterinariamaterialService.findWithReserva(whereInOSVM));
		}
		
		Material mat = materialService.loadMaterialPromocionalComMaterialrelacionado(material.getCdmaterial());
		if(mat != null && mat.getVendapromocional() != null && mat.getVendapromocional()){
			Integer qtde = null;
			if(SinedUtil.isListNotEmpty(mat.getListaMaterialrelacionado())){
				Localarmazenagem localarmazenagem = null;
				if(venda != null && venda.getLocalarmazenagem() != null){
					localarmazenagem = venda.getLocalarmazenagem();
				}
				try{
					materialrelacionadoService.calculaQuantidadeKit(mat);			
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Double qtdeReservadaItemKit;
				for(Materialrelacionado materialrelacionado : mat.getListaMaterialrelacionado()){
					if(materialrelacionado.getMaterialpromocao() != null && materialrelacionado.getQuantidade() != null && materialrelacionado.getQuantidade() > 0 &&
							(materialrelacionado.getMaterialpromocao().getServico() == null || !materialrelacionado.getMaterialpromocao().getServico())){
						String whereInNotReservasPromo = reservaService.montaWhereInReservas(venda, materialrelacionado.getMaterialpromocao());
						if(Util.strings.isEmpty(whereInNotReserva)){
							whereInNotReserva = whereInNotReservasPromo;
						}else if(Util.strings.isNotEmpty(whereInNotReservasPromo)){
							whereInNotReserva += (","+whereInNotReservasPromo);
						}
						qtdeReservadaItemKit = reservaService.getQtdeReservada(materialrelacionado.getMaterialpromocao(), null, localarmazenagem, loteestoque, whereInNotReserva);
						
						if(qtdeReservadaItemKit == null) qtdeReservadaItemKit = 0d;
						
						qtdeReservadaItemKit = qtdeReservadaItemKit/materialrelacionado.getQuantidade();
						if(qtde == null){
							qtde = qtdeReservadaItemKit.intValue();
						}else if(qtdeReservadaItemKit.intValue() < qtde){
							qtde = qtdeReservadaItemKit.intValue();
						}
					}
				}
			
			}
			return qtde != null ? qtde.doubleValue() : 0d;
		}else {
			return reservaService.getQtdeReservada(material, null, venda != null ? venda.getLocalarmazenagem() : null, whereInNotReserva);
		}
	}
	
	public void setPercentualDesconto(Vendamaterial item) {
		if(item != null && item.getQuantidade() != null && item.getQuantidade() > 0 && item.getPreco() != null && item.getPreco() > 0){
			Double multiplicador = item.getMultiplicador() != null ? item.getMultiplicador() : 1d; 
			if(item.getDesconto() != null && item.getDesconto().getValue().doubleValue() > 0 && multiplicador > 0){
				Double valorbruto = item.getQuantidade() * item.getPreco() * multiplicador;
				item.setPercentualdesconto(item.getDesconto().getValue().doubleValue() * 100 / valorbruto);
			}
		}
		
	}
	
	public void limpaGarantiareformaitem(Vendamaterial vendamaterial){
		vendamaterialDAO.limpaGarantiareformaitem(vendamaterial);
	}
	
	public List<Vendamaterial> findForNotaDevolucaoColetaByVenda(String whereInVenda){
		return vendamaterialDAO.findForNotaDevolucaoColetaByVenda(whereInVenda);
	}
	
	// --------------------> INICIO RELATORIO QUANTITATIVA DE VENDAS <----------------------------//
	
	public Resource gerarRelatorioExcelQuantitaticaDeVendas(QuantitativaDeVendasFiltro filtro) throws IOException {
		
		int anoInicio = SinedDateUtils.getAno(filtro.getDtInicio());
		int mesInicio = SinedDateUtils.getMes(filtro.getDtInicio()) + 1;
		int periodoDeMes = diferencaMeses(filtro.getDtFim(), filtro.getDtInicio()) + 1;
		
		// foi escolhido utilizar hashmaps para encontrar mais facilmente as vendas de um material.
		Map<Material, List<Vendamaterial>> grupoVendas = new LinkedHashMap<Material, List<Vendamaterial>>();
		Map<Integer, Material> materiais = new LinkedHashMap<Integer, Material>();
		
		agruparVendaPorMaterial(vendamaterialDAO.findAllForReportQuantitativaDeVendas(filtro), grupoVendas, materiais);
		materiais = materialService.carregarEstoqueDosMateriais(materiais, filtro.getEmpresa());
		
		SinedExcel exc = new SinedExcel();
		HSSFSheet p = exc.createSheet("Planilha");
		p.setDefaultColumnWidth((short) 15);
		p.setColumnWidth((short)0, (short)(7000));
		p.setColumnWidth((short)1, (short)(12150));
		p.setColumnWidth((short)2, (short)(7000));
		
		int countRow = addHeaderRelatorio(p, filtro.getEmpresa(), filtro.getMaterialgrupo(), filtro.getMaterialtipo(), 1);
		countRow = adicionarCabecalho(p, anoInicio, mesInicio, periodoDeMes, countRow + 1);
		countRow = adicionarCorpo(p, grupoVendas, materiais, anoInicio, mesInicio, periodoDeMes, countRow, filtro.getTipoDeOrdenacao());
		
		return exc.getWorkBookResource("quantitativa_de_vendas_" + SinedUtil.datePatternForReport() + ".xls");
	}
	
	private int diferencaMeses(Date dtFim, Date dtInicio) {
		Date inicio = new Date(dtInicio.getTime());
		Date fim = new Date(dtFim.getTime());
		int diffYear = SinedDateUtils.getAno(fim) - SinedDateUtils.getAno(inicio);
		if(diffYear == 0)
			return SinedDateUtils.getMes(fim) - SinedDateUtils.getMes(inicio);
		return (12 - SinedDateUtils.getMes(inicio)) + (diffYear - 1) * 12 + SinedDateUtils.getMes(fim);
	}
	private int addHeaderRelatorio(HSSFSheet p, Empresa empresa, Materialgrupo materialgrupo, Materialtipo materialtipo, int countRow) {
		HSSFRow row = p.createRow((short) countRow++);
		int countCell = 0;
		addCell(row, "Empresa: \n" + (empresa != null && empresa.getRazaosocial() != null ? empresa.getRazaosocial() : ""), countCell++, 60, SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		addCell(row, "Grupo do Material: " + (materialgrupo != null ? materialgrupo.getNome() : ""), countCell++, 60, SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		addCell(row, "Tipo do Material: " + (materialtipo != null ? materialtipo.getNome() : ""), countCell++, 60, SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		return countRow;
	}
	
	private int adicionarCorpo(HSSFSheet p, Map<Material, List<Vendamaterial>> grupoVendas, Map<Integer, Material> materiais, int anoInicio, int mesInicio, int periodoEmMes, int countRow, String tipoDeOrdenacao) {
		HSSFRow row;
		class ItemRelatorio {
			Material material;
			double quantidades[];
			double media;
		}
		List<ItemRelatorio> itens = new ArrayList<ItemRelatorio>();
		
		for(Entry<Integer, Material> m: materiais.entrySet()){
			List<Vendamaterial> vendas = grupoVendas.get(m.getValue());
			vendas = converterForUnidadeprincipal(vendas);
			double qt[] = calcQuantidades(anoInicio, mesInicio, periodoEmMes, vendas);
			ItemRelatorio i = new ItemRelatorio();
			i.material = m.getValue();
			i.quantidades = qt;
			i.media = calcMedia(i.quantidades);
			itens.add(i);
		}
		
		
		Comparator<ItemRelatorio> comp;
		if("Decrescente".equals(tipoDeOrdenacao)){
			comp = new Comparator<ItemRelatorio>() {
				@Override
				public int compare(ItemRelatorio o1, ItemRelatorio o2) {
					return Double.compare(o2.media, o1.media);
				}
			};
		}else{
			comp = new Comparator<ItemRelatorio>() {
				@Override
				public int compare(ItemRelatorio o1, ItemRelatorio o2) {
					return Double.compare(o1.media, o2.media);
				}
			};
		}
		Collections.sort(itens, comp);
		
		for(ItemRelatorio item: itens){
			int countCell = 0;
			row = p.createRow((short) countRow++);
			addCell(row, item.material.getIdentificacaoListagem(), countCell++, SinedExcel.STYLE_DETALHE_CENTER_GREY);
			addCell(row, item.material.getNome(), countCell++, SinedExcel.STYLE_DETALHE_LEFT_GREY);
			addCell(row, item.material.getUnidademedida().getNome(), countCell++, SinedExcel.STYLE_DETALHE_CENTER_GREY);
			
			for(double q: item.quantidades)
				addNormalCell(row, q, countCell++);
			
			char c = (char)(68 + periodoEmMes - 1);
			char t = (char)((int)c + 1);
			int r = row.getRowNum() + 1;
			addCellComFormula(row, "SUM(D" + r + ":" + c + r + ")", countCell++);
			addCellComFormula(row,  "" + t + r + "/ "+ periodoEmMes, countCell++);
			if(item.material.getMateriaisDisponiveisEmEstoque() != null)
				addNormalCell(row, item.material.getMateriaisDisponiveisEmEstoque(), countCell++);
		}
		return countRow;
	}
	
	private double calcMedia(double[] qt) {
		double media = 0;
		for(double q: qt)
			media+=q;
		return media / qt.length;
	}
	private int adicionarCabecalho(HSSFSheet p, int anoInicio, int mesInicio, int periodoEmMes, int countRow){
		HSSFRow row = p.createRow((short) countRow++);
		int countCell = 0;
		countCell = addHeaderMeses(anoInicio, mesInicio, periodoEmMes, row, countCell);
		return addHeaderDataGridCamposMaterial(p, periodoEmMes, countRow);
	}
	
	private int addHeaderDataGridCamposMaterial(HSSFSheet p, int periodoEmMes, int countRow) {
		HSSFRow row;
		int countCell;
		row = p.createRow((short) countRow++);
		countCell = 0;
		addCell(row, "Identificador", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "Material", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "Unidade", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		for(int i = 0; i < periodoEmMes + 3; i++)
			addCell(row, "Quantidade", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_GREY_CENTER);
		return countRow;
	}
	
	private int addHeaderMeses(int anoInicio, int mesInicio, int periodoEmMes, HSSFRow row, int countCell) {
		addCell(row, "", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		
		int ano = anoInicio;
		int mes = mesInicio;
		for(int i = 0; i < periodoEmMes; i++){
			addCell(row, ABREVIATURA_MESES[mes - 1] + "/" + ano, countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
			if(mes == 12){
				ano++;
				mes = 1;
			}else{
				mes++;
			}
		}
		addCell(row, "Total", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "M�dia", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		addCell(row, "Estoque", countCell++, 30, SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		return countCell;
	}
	
	private double[] calcQuantidades(int anoInicio, int mesInicio, int periodoEmMes, List<Vendamaterial> vendas){
		double qt[] = new double[periodoEmMes];
		for(Vendamaterial vm: vendas) {
			int ano = SinedDateUtils.getAno( new Date(vm.getVenda().getDtvenda().getTime()));
			int mes = SinedDateUtils.getMes( new Date(vm.getVenda().getDtvenda().getTime())) + 1;
			int j = indexOfMonth(periodoEmMes, anoInicio, mesInicio, ano, mes);
			qt[j - 1] += vm.getQuantidade();
		}
		return qt;
	}
	
	private int indexOfMonth(int periodoEmMes, int anoInicio, int mesInicio, int ano, int mes){
		int a = anoInicio;
		int m = mesInicio;
		for(int i = 1; i < periodoEmMes; i++){
			if(a == ano && m == mes)
				return i;
			if(m == 12){
				a++;
				m = 1;
			}else{
				m++;
			}
		}
		return periodoEmMes;
	}
	
	private void agruparVendaPorMaterial(List<Vendamaterial> lst, Map<Material, List<Vendamaterial>> grupoVendas, Map<Integer, Material> materiais){
		if(lst == null || lst.size() == 0)
			return;
		for(Vendamaterial vm: lst){
			List<Vendamaterial> lstvm = grupoVendas.get(vm.getMaterial());
			if(lstvm == null)
				lstvm = new ArrayList<Vendamaterial>();
			lstvm.add(vm);
			grupoVendas.put(vm.getMaterial(), lstvm);
			materiais.put(vm.getMaterial().getCdmaterial(), vm.getMaterial());
		}
	}
	
	private void addCellComFormula(HSSFRow row, String formula, int j){
		HSSFCell cell = row.createCell((short) j);
		cell.setCellStyle(SinedExcel.STYLE_CENTER);
		cell.setCellFormula(formula);
	}
	
	private void addNormalCell(HSSFRow row, double value, int j) {
		HSSFCell cell = row.createCell((short) j);
		cell.setCellStyle(SinedExcel.STYLE_CENTER);
		cell.setCellValue(value);
	}
	
	private void addCell(HSSFRow row, String value, int i, HSSFCellStyle style){
		HSSFCell cell = row.createCell((short) i);
		cell.setCellStyle(style);
		cell.setCellValue(value);
	}
	
	private void addCell(HSSFRow row, String value, int i, int heightInPoints, HSSFCellStyle style){
		HSSFCell cell = row.createCell((short) i);
		row.setHeightInPoints((float)heightInPoints);
		cell.setCellStyle(style);
		cell.setCellValue(value);
	}
	
	// --------------------> FIM RELATORIO QUANTITATIVA DE VENDAS <----------------------------//
	
	public Vendamaterial loadByPedidovendamaterial(Pedidovendamaterial pedidovendamaterial, Venda venda){
		return vendamaterialDAO.loadByPedidovendamaterial(pedidovendamaterial, venda);
	}
	
	/**
	 * M�todo ajax que busca e retorna o ultimo valor de venda de determinado material para o cliente selecionado.
	 * 
	 * @param request
	 * @author Rafael Salvio
	 */
	public void buscarSugestaoUltimoValorVendaAJAX(WebRequestContext request){
		if(request.getParameter("cdmaterial")!= null && request.getParameter("cdpessoa")!=null){
			View view = View.getCurrent();
			request.getServletResponse().setContentType("text/html");
			Material material = new Material(Integer.parseInt(request.getParameter("cdmaterial")));
			Unidademedida unidadeMedida = StringUtils.isNotBlank(request.getParameter("cdunidademedida"))? new Unidademedida(Integer.parseInt(request.getParameter("cdunidademedida"))): null;
			Cliente cliente = new Cliente(Integer.parseInt(request.getParameter("cdpessoa")));
			Empresa empresa = new Empresa(Integer.parseInt(request.getParameter("cdempresa")));
			ModelAndView json = this.buscarSugestaoUltimoValorVendaAJAXJson(material, cliente, empresa, unidadeMedida);
					
			
			//Vendamaterial vendaMaterial = this.getUltimoValorMaterialByCliente(material, cliente, empresa, unidadeMedida);
			if(json.getModel().size() > 0){
				//List<Vendamaterial> lista = this.findByVenda(vendaMaterial.getVenda());
				//vendaMaterial.getVenda().setListavendamaterial(lista);
				view.println("var ultimovalor = '"+ json.getModel().get("ultimoValor") +"';");
				view.println("var dtVenda = '"+ json.getModel().get("dtVenda") +"';");
				view.println("var cdVenda = '"+ json.getModel().get("cdVenda") +"';");
				view.println("var tipoPedido = '"+ json.getModel().get("tipoPedido") +"';");
				view.println("var quantidade = '"+ json.getModel().get("quantidade") +"';");
				view.println("var preco = '"+ json.getModel().get("preco") +"';");
				view.println("var totalVenda = '"+ json.getModel().get("totalVenda") +"';");
				view.println("var desconto = '"+ json.getModel().get("desconto") +"';");
				/*view.println("var ultimovalor = '"+ vendaMaterial.getPreco() +"';");
				view.println("var dtVenda = '"+ DateUtils.dateToString(vendaMaterial.getVenda().getDtvenda(), "dd/MM/yyyy") +"';");
				view.println("var cdVenda = '"+ vendaMaterial.getVenda().getCdvenda() +"';");
				view.println("var tipoPedido = '"+ (vendaMaterial.getVenda().getPedidovendatipo() != null? vendaMaterial.getVenda().getPedidovendatipo().getDescricao(): "") +"';");
				view.println("var quantidade = '"+ vendaMaterial.getQuantidade() +"';");
				view.println("var preco = '"+ new DecimalFormat("#,##.00").format(vendaMaterial.getPreco()) +"';");
				view.println("var totalVenda = '"+ vendaMaterial.getVenda().getTotalvenda() +"';");
				view.println("var desconto = '"+ (vendaMaterial.getDesconto() != null? vendaMaterial.getDesconto(): "") +"';");*/
			}
		}	
	}
	
	public ModelAndView buscarSugestaoUltimoValorVendaAJAXJson(Material material, Cliente cliente, Empresa empresa, Unidademedida unidadeMedida){
		ModelAndView retorno = new JsonModelAndView();
		if(material != null && cliente != null){
			Vendamaterial vendaMaterial = this.getUltimoValorMaterialByCliente(material, cliente, empresa, unidadeMedida);
			if(vendaMaterial != null){
				List<Vendamaterial> lista = this.findByVenda(vendaMaterial.getVenda());
				vendaMaterial.getVenda().setListavendamaterial(lista);
				retorno.addObject("ultimoValor", vendaMaterial.getPreco());
				retorno.addObject("dtVenda", DateUtils.dateToString(vendaMaterial.getVenda().getDtvenda(), "dd/MM/yyyy"));
				retorno.addObject("cdVenda", vendaMaterial.getVenda().getCdvenda());
				retorno.addObject("tipoPedido", (vendaMaterial.getVenda().getPedidovendatipo() != null? vendaMaterial.getVenda().getPedidovendatipo().getDescricao(): ""));
				retorno.addObject("quantidade", vendaMaterial.getQuantidade());
				retorno.addObject("preco", new DecimalFormat("#,###.00").format(vendaMaterial.getPreco()));
				retorno.addObject("totalVenda", vendaMaterial.getVenda().getTotalvenda());
				retorno.addObject("desconto", (vendaMaterial.getDesconto() != null? vendaMaterial.getDesconto(): ""));
			}
		}
		return retorno;
	}
	
	public List<Vendamaterial> findForTotalizacaoVenda(Venda venda){
		return vendamaterialDAO.findForTotalizacaoVenda(venda);
	}
	
	public void updateFaixaMarkup(Vendamaterial vm, FaixaMarkupNome faixaMarkup) {
		vendamaterialDAO.updateFaixaMarkup(vm, faixaMarkup);
	}
	
	public List<Vendamaterial> findTotalVendaByVenda(Venda vendaAux) {
		return vendamaterialDAO.findTotalVendaByVenda(vendaAux);
	}
	
	public List<VendaMaterialWSBean> toWSList(List<Vendamaterial> listaVendaMaterial){
		List<VendaMaterialWSBean> lista = new ArrayList<VendaMaterialWSBean>();
		if(SinedUtil.isListNotEmpty(listaVendaMaterial)){
			
			for(Vendamaterial vm: listaVendaMaterial){
				VendaMaterialWSBean bean  = new VendaMaterialWSBean();
				
				bean.setAliquotareaisipi(vm.getAliquotareaisipi());
				bean.setAltura(vm.getAltura());
				bean.setCdproducaoagendaitemadicional(vm.getCdproducaoagendaitemadicional());
				bean.setCdproducaoordemitemadicional(vm.getCdproducaoordemitemadicional());
				bean.setCdvendamaterial(vm.getCdvendamaterial());
				bean.setComissionamento(ObjectUtils.translateEntityToGenericBean(vm.getComissionamento()));
				bean.setComprimento(vm.getComprimento());
				bean.setComprimentooriginal(vm.getComprimentooriginal());
				bean.setCustooperacional(vm.getCustooperacional());
				bean.setDesconto(vm.getDesconto());
				bean.setDescontogarantiareforma(vm.getDescontogarantiareforma());
				bean.setFaixaMarkupNome(ObjectUtils.translateEntityToGenericBean(vm.getFaixaMarkupNome()));
				bean.setFatorconversao(vm.getFatorconversao());
				bean.setFornecedor(ObjectUtils.translateEntityToGenericBean(vm.getFornecedor()));
				bean.setGarantiareformaitem(ObjectUtils.translateEntityToGenericBean(vm.getGarantiareformaitem()));
				bean.setGrupotributacao(ObjectUtils.translateEntityToGenericBean(vm.getGrupotributacao()));
				bean.setIdentificadorespecifico(vm.getIdentificadorespecifico());
				bean.setIdentificadorintegracao(vm.getIdentificadorintegracao());
				bean.setIdentificadorinterno(vm.getIdentificadorinterno());
				bean.setIpi(vm.getIpi());
				bean.setLargura(vm.getLargura());
				//bean.setListaVendamaterialseparacao(listaVendamaterialseparacao)Aliquotareaisipi(vm.getAliquotareaisipi());
				bean.setLoteestoque(ObjectUtils.translateEntityToGenericBean(vm.getLoteestoque()));
				bean.setMaterial(ObjectUtils.translateEntityToGenericBean(vm.getMaterial()));
				bean.setMaterialcoleta(ObjectUtils.translateEntityToGenericBean(vm.getMaterialcoleta()));
				bean.setMaterialFaixaMarkup(ObjectUtils.translateEntityToGenericBean(vm.getMaterialFaixaMarkup()));
				bean.setMaterialmestre(ObjectUtils.translateEntityToGenericBean(vm.getMaterialmestre()));
				bean.setMultiplicador(vm.getMultiplicador());
				bean.setObservacao(vm.getObservacao());
				bean.setOpcional(vm.getOpcional());
				bean.setOrdem(vm.getOrdem());
				bean.setPatrimonioitem(ObjectUtils.translateEntityToGenericBean(vm.getPatrimonioitem()));
				bean.setPedidovendamaterial(ObjectUtils.translateEntityToGenericBean(vm.getPedidovendamaterial()));
				bean.setPercentualcomissaoagencia(vm.getPercentualcomissaoagencia());
				bean.setPercentualdesconto(vm.getPercentualdesconto());
				bean.setPercentualrepresentacao(vm.getPercentualrepresentacao());
				bean.setPeso(vm.getPeso());
				bean.setPesomedio(vm.getPesomedio());
				//bean.setPneu(pneu)Aliquotareaisipi(vm.getAliquotareaisipi());
				if(vm.getPrazoentrega() != null)
					bean.setPrazoentrega(new java.util.Date(vm.getPrazoentrega().getTime()));
				bean.setPreco(vm.getPreco());
				bean.setProducaosemestoque(vm.getProducaosemestoque());
				bean.setQtdereferencia(vm.getQtdereferencia());
				bean.setQtdevolumeswms(vm.getQtdevolumeswms());
				bean.setQuantidade(vm.getQuantidade());
				bean.setSaldo(vm.getSaldo());
				bean.setTipocalculoipi(EnumUtils.translateEnum(vm.getTipocalculoipi()));
				bean.setTipocobrancaipi(EnumUtils.translateEnum(vm.getTipocobrancaipi()));
				bean.setUnidademedida(ObjectUtils.translateEntityToGenericBean(vm.getUnidademedida()));
				bean.setValorcustomaterial(vm.getValorcustomaterial());
				bean.setValorimposto(vm.getValorimposto());
				bean.setValoripi(vm.getValoripi());
				bean.setValorMinimo(vm.getValorMinimo());
				bean.setVenda(ObjectUtils.translateEntityToGenericBean(vm.getVenda()));
				bean.setVendaorcamentomaterial(ObjectUtils.translateEntityToGenericBean(vm.getVendaorcamentomaterial()));
				if(vm.getPneu() != null){
					bean.setPneu(new PneuWSBean(vm.getPneu()));
				}
				
				lista.add(bean);
			}
		}
		return lista;
	}
	
	public Vendamaterial loadForValidacaoServico(Vendamaterial vendaMaterial) {
		return vendamaterialDAO.loadForValidacaoServico(vendaMaterial);
	}
	
    public boolean isVendaServico(Vendamaterial vendamaterial){
    	return this.isVendaServico(vendamaterial, false);
    }
    
    public boolean isVendaServico(Vendamaterial vendamaterial, boolean loadVendaMaterial){
    	if(Util.objects.isPersistent(vendamaterial) && loadVendaMaterial){
    		vendamaterial = this.loadForValidacaoServico(vendamaterial);
    	}
    	    	
    	return  vendamaterial != null && vendamaterial.getMaterial()!= null
    			&& (Boolean.TRUE.equals(vendamaterial.getMaterial().getServico()) || Boolean.TRUE.equals(vendamaterial.getMaterial().getTributacaomunicipal()));
    }
    
	public void updateValorComissao(Vendamaterial vm) {
		vendamaterialDAO.updateValorComissao(vm);
	}
	
	public Vendamaterial loadWithLoteEstoque(Vendamaterial vm){
		return vendamaterialDAO.loadWithLoteEstoque(vm);
	}
	
	public boolean isEntregaFutura(Vendamaterial vm){
		return vendamaterialDAO.isEntregaFutura(vm);
	}
	
	/*public Vendamaterial criaCopiaVendamaterial(Vendamaterial b){
		
		return
	}*/
	
	public void saveListByExpedicao(final ExpedicaoTrocaLoteBean bean){
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			
			@Override
			public Object doInTransaction(TransactionStatus status) {
				Expedicaoitem expItem = ExpedicaoitemService.getInstance().load(bean.getExpedicaoItem());
				Material material = MaterialService.getInstance().loadMaterialWithUnidadeMedida(expItem.getMaterial());
				Loteestoque le1 = LoteestoqueService.getInstance().loadWithNumeroValidade(expItem.getLoteestoque(), expItem.getMaterial());
				String espacos = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				
				String obs = "Foi realizada a seguinte altera��o de lote no produto: "+material.getNome()+"<br><br>De:<br><br>"+espacos+"Lote: "+le1.getNumerovalidade()+" - Qtde: "+expItem.getQtdeexpedicao();
				
				obs += "<br><br>Para: <br><br>";
				
				final ExpedicaoitemService expedicaoitemService = ExpedicaoitemService.getInstance();
				final VendamaterialService vendamaterialService = VendamaterialService.getInstance();
				
				Expedicaoitem primeiroExpedicaoItem = bean.getListaExpedicaoItem().get(0);
				expItem.setQtdeexpedicao(primeiroExpedicaoItem.getQtdeexpedicao());
				expItem.setLoteEstoque(primeiroExpedicaoItem.getLoteestoque());
				expedicaoitemService.saveOrUpdateNoUseTransaction(expItem);
				
				Vendamaterial vm = vendamaterialService.loadForEntrada(bean.getExpedicaoItem().getVendamaterial());
				Venda venda = vendaService.loadWithPedidovendatipo(vm.getVenda());
				
				boolean geraReserva = venda != null && venda.getPedidovendatipo() != null && Boolean.TRUE.equals(venda.getPedidovendatipo().getReserva()) &&
											((BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum())) ||
												(pedidovendatipoService.gerarExpedicaoVenda(venda.getPedidovendatipo(), false) && BaixaestoqueEnum.APOS_EMISSAONOTA.equals(venda.getPedidovendatipo().getBaixaestoqueEnum())));
				
				boolean existsReservaVendamaterial = false;
				boolean existsReservaExpedicaoitem = false;
				Reserva reservaExistenteVenda = null;
				Reserva reservaExistenteExpedicao = null;
				/*Ao redistribuir os lotes, atualiza as reservas j� existentes para a nova quantidade*/
				if(geraReserva){
					reservaExistenteExpedicao = reservaService.findByExpedicaoitem(expItem, material);
					if(reservaExistenteExpedicao != null){
						existsReservaExpedicaoitem = true;
						Double qtdeConvertida = unidademedidaService.getQtdeConvertida(expItem.getMaterial(), expItem.getUnidademedida(), material.getUnidademedida(), expItem.getQtdeexpedicao(), false);
						reservaExistenteExpedicao = reservaService.load(reservaExistenteExpedicao);
						reservaExistenteExpedicao.setQuantidade(qtdeConvertida);
						reservaExistenteExpedicao.setLoteestoque(expItem.getLoteestoque());
						reservaService.saveOrUpdateNoUseTransaction(reservaExistenteExpedicao);
					}
					reservaExistenteVenda = reservaService.findByVendamaterial(vm, material);
					if(reservaExistenteVenda != null){
						existsReservaVendamaterial = true;
						Double qtdeConvertida = unidademedidaService.getQtdeConvertida(expItem.getMaterial(), expItem.getUnidademedida(), material.getUnidademedida(), expItem.getQtdeexpedicao(), false);
						reservaExistenteVenda = reservaService.loadForEntrada(reservaExistenteVenda);
						reservaExistenteVenda.setQuantidade(qtdeConvertida);
						reservaExistenteVenda.setLoteestoque(expItem.getLoteestoque());
						reservaService.saveOrUpdateNoUseTransaction(reservaExistenteVenda);
					}
				}
				vm = vendamaterialService.loadForEntrada(bean.getExpedicaoItem().getVendamaterial());
				
				Integer cdvendamaterial = vm.getCdvendamaterial();
				
				
				
				for(int i=bean.getListaExpedicaoItem().size()-1; i>=0; i--){
					Expedicaoitem ei = bean.getListaExpedicaoItem().get(i);
					
					Double indice = ei.getQtdeexpedicao() / vm.getQuantidade();
					Vendamaterial novoVm = null;
					if(i == 0){
						novoVm = vm;
					}else{
						novoVm = vendamaterialService.loadForEntrada(new Vendamaterial(cdvendamaterial));
						novoVm.setCdvendamaterial(null);
					}
					novoVm.setLoteestoque(ei.getLoteestoque());
					novoVm.setVenda(venda);

					novoVm.setQtdeExpedicao(ei.getQtdeexpedicao());
					novoVm.setQuantidade(ei.getQtdeexpedicao());
					
					novoVm.setDesconto(rateiaValor(vm.getDesconto(), indice));
					
					novoVm.setValordifal(rateiaValor(vm.getValordifal(), indice));
					
					novoVm.setValorfcp(rateiaValor(vm.getValorfcp(), indice));
					novoVm.setValorfcpdestinatario(rateiaValor(vm.getValorfcpdestinatario(), indice));
					novoVm.setValorfcpst(rateiaValor(vm.getValorfcpst(), indice));
					
					novoVm.setValoricms(rateiaValor(vm.getValoricms(), indice));
					novoVm.setValoricmsst(rateiaValor(vm.getValoricmsst(), indice));
					novoVm.setValoricmsdestinatario(rateiaValor(vm.getValoricmsdestinatario(), indice));
					novoVm.setValoricmsremetente(rateiaValor(vm.getValoricmsremetente(), indice));
					
					novoVm.setValorSeguro(rateiaValor(vm.getValorSeguro(), indice));
					novoVm.setOutrasdespesas(rateiaValor(vm.getOutrasdespesas(), indice));
					
					novoVm.setValoripi(rateiaValor(vm.getValoripi(), indice));
					
					novoVm.setValorbcdestinatario(rateiaValor(vm.getValorbcdestinatario(), indice));
					novoVm.setValorbcfcp(rateiaValor(vm.getValorbcfcp(), indice));
					novoVm.setValorbcfcpdestinatario(rateiaValor(vm.getValorbcfcpdestinatario(), indice));
					novoVm.setValorbcfcpst(rateiaValor(vm.getValorbcfcpst(), indice));
					novoVm.setValorbcicmsst(rateiaValor(vm.getValorbcicmsst(), indice));
					novoVm.setValorbcicms(rateiaValor(vm.getValorbcicms(), indice));
					novoVm.setValordesoneracaoicms(rateiaValor(vm.getValordesoneracaoicms(), indice));
					
					
					VendamaterialService.getInstance().saveOrUpdateNoUseTransaction(novoVm);
					
					Loteestoque le = LoteestoqueService.getInstance().loadWithNumeroValidade(ei.getLoteestoque(), ei.getMaterial());
					obs += espacos+"Lote: " + le.getNumerovalidade()+" - Qtde: "+ei.getQtdeexpedicao()+"<br>";
					if(i > 0){
						/*Ao redistribuir os lotes, cria reserva para os itens dos novos lotes*/
						if(existsReservaVendamaterial && geraReserva){
							Reserva reserva = reservaService.load(reservaExistenteVenda);
							reserva.setCdreserva(null);
							Double qtdeConvertida = unidademedidaService.getQtdeConvertida(expItem.getMaterial(), expItem.getUnidademedida(), material.getUnidademedida(), novoVm.getQuantidade(), false);
							reserva.setQuantidade(qtdeConvertida);
							reserva.setLoteestoque(ei.getLoteestoque());
							reserva.setVendamaterial(novoVm);
							
							reservaService.saveOrUpdateNoUseTransaction(reserva);
						}
						
						Expedicaoitem expedidcaoItem = ExpedicaoitemService.getInstance().load(new Expedicaoitem(expItem.getCdexpedicaoitem()));
						expedidcaoItem.setCdexpedicaoitem(null);
						expedidcaoItem.setQtdeexpedicao(ei.getQtdeexpedicao());
						expedidcaoItem.setVendamaterial(novoVm);
						expedidcaoItem.setLoteEstoque(ei.getLoteestoque());
						expedicaoitemService.saveOrUpdateNoUseTransaction(expedidcaoItem);
						
						/*Ao redistribuir os lotes, cria reserva para os itens dos novos lotes*/
						if(existsReservaExpedicaoitem && geraReserva){
							Reserva reserva = reservaService.load(reservaExistenteExpedicao);
							reserva.setCdreserva(null);
							Double qtdeConvertida = unidademedidaService.getQtdeConvertida(expItem.getMaterial(), expItem.getUnidademedida(), material.getUnidademedida(), expedidcaoItem.getQtdeexpedicao(), false);
							reserva.setQuantidade(qtdeConvertida);
							reserva.setLoteestoque(ei.getLoteestoque());
							reserva.setExpedicaoitem(expedidcaoItem);
							
							reservaService.saveOrUpdateNoUseTransaction(reserva);
						}
					}
					
				}
				Expedicaohistorico historico = new Expedicaohistorico();
				
				historico.setExpedicao(expItem.getExpedicao());
				historico.setDtaltera(SinedDateUtils.currentTimestamp());
				historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
				historico.setObservacao(obs);
				historico.setExpedicaoacao(Expedicaoacao.ALTERADA);
				ExpedicaohistoricoService.getInstance().saveOrUpdate(historico);
				
				return null;
			}
		});
	}
	
	
	private Double rateiaValor(Double valor, Double indiceRateio){
		return valor * indiceRateio;
	}
	
	private Money rateiaValor(Money valor, Double indiceRateio){
		return new Money(rateiaValor(MoneyUtils.moneyToDouble(valor), indiceRateio));
	}
	
	public void updateCampo(Vendamaterial vm, String nomeCampo, Object valor) {
		vendamaterialDAO.updateCampo(vm, nomeCampo, valor);
	}
}