package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.dao.EspecialidadeDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;


public class EspecialidadeService extends GenericService<Especialidade> {
	
	private EspecialidadeDAO especialidadeDAO;
	
	public void setEspecialidadeDAO(EspecialidadeDAO especialidadeDAO) {
		this.especialidadeDAO = especialidadeDAO;
	}
	
	public List<Especialidade> findAtivosAutocomplete(String q){
		return especialidadeDAO.findAtivosAutocomplete(q);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.EspecialidadeDAO#findByDescricao(String descricao)
	*
	* @param descricao
	* @return
	* @since 05/03/2018
	* @author Luiz Fernando
	*/
	public Especialidade findByDescricao(String descricao) {
		return especialidadeDAO.findByDescricao(descricao);
	}
}