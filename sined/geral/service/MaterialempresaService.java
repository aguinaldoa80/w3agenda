package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialempresa;
import br.com.linkcom.sined.geral.dao.MaterialempresaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.materialEmpresa.MaterialEmpresaRESTModel;

public class MaterialempresaService extends GenericService<Materialempresa> {

	private MaterialempresaDAO materialempresaDAO;
	
	public void setMaterialempresaDAO(MaterialempresaDAO materialempresaDAO) {
		this.materialempresaDAO = materialempresaDAO;
	}
	
	
	private static MaterialempresaService instance;
	public static MaterialempresaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MaterialempresaService.class);
		}
		return instance;
	}
	
	/**
	 * Faz referÍncia ao DAO.
	 *  
	 * @see br.com.linkcom.sined.geral.dao.MaterialempresaDAO#findByMaterial
	 * @param form
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Materialempresa> findByMaterial(Material form) {
		return materialempresaDAO.findByMaterial(form);
	}
	
	public List<MaterialEmpresaRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}

	public List<MaterialEmpresaRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<MaterialEmpresaRESTModel> listaRest= new ArrayList<MaterialEmpresaRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			List<Materialempresa> ListaMatEmpresa = materialempresaDAO.findForAndroid(whereIn);	
			for (Materialempresa materialEmpresa : ListaMatEmpresa) {
				MaterialEmpresaRESTModel rest = new MaterialEmpresaRESTModel();
				
				rest.setCdmaterialempresa(materialEmpresa.getCdmaterialempresa());
				rest.setCdempresa(materialEmpresa.getEmpresa().getCdpessoa());
				rest.setCdmaterial(materialEmpresa.getMaterial().getCdmaterial());
				
				listaRest.add(rest);
			}
		}

		return listaRest;
	}


}
