package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Formularh;
import br.com.linkcom.sined.geral.bean.Formularhitem;
import br.com.linkcom.sined.geral.dao.FormularhitemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class FormularhitemService extends GenericService<Formularhitem>{
	
	protected FormularhitemDAO formularhitemDAO;	
	
	public void setFormularhitemDAO(FormularhitemDAO formularhitemDAO) {this.formularhitemDAO = formularhitemDAO;}

	public List<Formularhitem> findByHolerite(Formularh formularh){
		return formularhitemDAO.findByHolerite(formularh);
	}
	
	public Formularhitem loadForGerarArquivoSEFIP(Formularh formularh, String identificador){
		return formularhitemDAO.loadForGerarArquivoSEFIP(formularh, identificador);
	}
	
	private static FormularhitemService instance;
	public static FormularhitemService getInstance() {
		if(instance == null){
			instance = Neo.getObject(FormularhitemService.class);
		}
		return instance;
	}
}