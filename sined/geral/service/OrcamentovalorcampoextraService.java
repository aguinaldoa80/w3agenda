package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Orcamentovalorcampoextra;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.dao.OrcamentovalorcampoextraDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class OrcamentovalorcampoextraService  extends GenericService<Orcamentovalorcampoextra>{

	private OrcamentovalorcampoextraDAO orcamentovalorcampoextraDAO;
	
	public void setOrcamentovalorcampoextraDAO(OrcamentovalorcampoextraDAO orcamentovalorcampoextraDAO) {
		this.orcamentovalorcampoextraDAO = orcamentovalorcampoextraDAO;
	}
	
	/**
	 * Busca todos os campos extras de um determinado {@link Vendaorcamento}
	 * @param pedidovendatipo
	 * @return
	 */
	public List<Orcamentovalorcampoextra> findByVendaorcamento(Vendaorcamento bean) {

		return orcamentovalorcampoextraDAO.findByVendaorcamento(bean);
	}
	
}
