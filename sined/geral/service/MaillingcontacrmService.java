package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.dao.MaillingcontacrmDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.MaillingcontacrmFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaillingcontacrmService extends GenericService<Contacrm> {
	
	protected MaillingcontacrmDAO maillingcontacrmDAO;
	
	public void setMaillingcontacrmDAO(MaillingcontacrmDAO maillingcontacrmDAO) {
		this.maillingcontacrmDAO = maillingcontacrmDAO;
	}
	
	@Override
	public void saveOrUpdate(Contacrm bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	@Override
	public void delete(Contacrm bean) {
		throw new SinedException("N�o � poss�vel executar esta a��o.");
	}
	
	public List<Contacrm> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return maillingcontacrmDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	public void processarLIstagem(ListagemResult<Contacrm> listagemResult, MaillingcontacrmFiltro filtro) {
		List<Contacrm> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontacrm", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(maillingcontacrmDAO.loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}
		
		for (Contacrm conta : list) {
			if(conta.getListcontacrmcontato()!= null){
				String email = "";
				for(Contacrmcontato contato : conta.getListcontacrmcontato()){
					if(contato.getListcontacrmcontatoemail() != null){
						email =  email + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(), "email", "\n") + "\n";
					}
				}
				conta.setListaEmails(email);
			}
		}
	}
	
	@Override
	protected ListagemResult<Contacrm> findForExportacao(FiltroListagem filtro) {
		ListagemResult<Contacrm> listagemResult = super.findForExportacao(filtro);
		processarLIstagem(listagemResult, (MaillingcontacrmFiltro) filtro);
		
		return listagemResult;
	}
}
