package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Rateio;
import br.com.linkcom.sined.geral.bean.Taxa;
import br.com.linkcom.sined.geral.bean.Taxaitem;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.dao.TaxaDAO;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TaxaService extends GenericService<Taxa>{

	private TaxaDAO taxaDAO;
	private DocumentoService documentoService;
	private ContapagarService contapagarService;
	private DocumentohistoricoService documentohistoricoService;
	private TaxaitemService taxaitemService;
	private TipotaxaService tipotaxaService;
	
	public void setDocumentohistoricoService(
			DocumentohistoricoService documentohistoricoService) {
		this.documentohistoricoService = documentohistoricoService;
	}
	public void setContapagarService(ContapagarService contapagarService) {
		this.contapagarService = contapagarService;
	}
	public void setDocumentoService(DocumentoService documentoService) {
		this.documentoService = documentoService;
	}
	public void setTaxaDAO(TaxaDAO taxaDAO) {
		this.taxaDAO = taxaDAO;
	}
	public void setTaxaitemService(TaxaitemService taxaitemService) {
		this.taxaitemService = taxaitemService;
	}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {
		this.tipotaxaService = tipotaxaService;
	}
	
	/**
	 * M�todo respons�vel por validar o bean de <code>Juros</code> em <code>Documento</code>.<br>
	 * Esta valida��o foi feita em um m�todo separado pois � usada em mais de um caso de uso, nos quais a propriedade <code>Juros</code> 
	 * est� contida no bean de <code>Documento</code>.
	 * 
	 * @throws SinedException - Se a propriedade <code>juros</code> ou <code>dtvencimento</code> for <code>null</code>.
	 * @param bean
	 * @param errors
	 * @author Fl�vio Tavares
	 */
	public void validateTaxas(Documento bean, BindException errors){
		if(bean.getTaxa() == null) return;
		
		Set<Taxaitem> listaItem = bean.getTaxa().getListaTaxaitem();
		if(listaItem == null || listaItem.size() == 0) return;
		
		Date dtvencimento = bean.getDtvencimento();
		
		if(dtvencimento == null){
			throw new SinedException("A propriedade Dtvencimento n�o pode ser null.");
		}
		
		for (Taxaitem taxaitem : listaItem) {
		
			if(taxaitem.getTipotaxa().equals(Tipotaxa.DESAGIO) && taxaitem.getDtlimite().after(dtvencimento)){
				if (errors == null)
					throw new SinedException("A data limite do des�gio n�o deve ser maior que a data de vencimento.");
				else 
					errors.reject("001","A data limite do des�gio n�o deve ser maior que a data de vencimento.");
			}
			
			if(taxaitem.getTipotaxa().equals(Tipotaxa.DESCONTO)){
				if(taxaitem.isPercentual()){
					if(taxaitem.getValor().getValue().doubleValue() >= 100.0){
						errors.reject("001", "Desconto acima do permitido. O valor da conta n�o pode ficar negativo.");
					}
				} else {
					if(taxaitem.getValor().getValue().doubleValue() >= bean.getValor().getValue().doubleValue()){
						errors.reject("001", "Desconto acima do permitido. O valor da conta n�o pode ficar negativo.");
					}
				}
			}
			
			if(taxaitem.getTipotaxa().equals(Tipotaxa.MULTA) && taxaitem.getDtlimite().before(dtvencimento)){
				if (errors == null)
					throw new SinedException("A data limite da multa n�o deve ser menor que a data de vencimento.");
				else 
					errors.reject("001","A data limite da multa n�o deve ser menor que a data de vencimento.");
			}
			
			if(taxaitem.getTipotaxa().equals(Tipotaxa.JUROS) && taxaitem.getDtlimite().before(dtvencimento)){
				if (errors == null)
					throw new SinedException("A data limite do juros n�o deve ser menor que a data de vencimento.");
				else 
					errors.reject("001","A data limite do juros n�o deve ser menor que a data de vencimento.");
			}
		}
		
	}
	
	/**
	 * M�todo que verifica se existe tipo de taxa duplicado para a conta corrente
	 *
	 * @param bean
	 * @param errors
	 * @author Luiz Fernando
	 */
	public void validateTaxasByConta(Conta bean, BindException errors){
		if(bean.getTaxa() == null) return;
		
		Set<Taxaitem> listaItem = bean.getTaxa().getListaTaxaitem();
		if(listaItem == null || listaItem.size() == 0) return;
		
		if(listaItem.size() > 1){
			int index = 0;
			int index2 = 0;
			for (Taxaitem taxaitem : listaItem) {
				for (Taxaitem taxaitem2 : listaItem) {
					if(index != index2){
						if(taxaitem.getTipotaxa().equals(taxaitem2.getTipotaxa())){
							errors.reject("001","Tipo de taxa duplicado.");
						}
					}
				}
			}		
			
		}
		
	}
	
	/**
	 * M�todo que obt�m a Taxa de um Documento.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TaxaService#findByDocmento(String whereIn)
	 * 
	 * @param documento
	 * @return
	 * @author Fl�vio
	 */
	public Taxa findByDocumento(Documento documento){
		if(documento == null || documento.getCddocumento() == null)
			throw new SinedException("Os par�metros documento ou cddocumento n�o podem ser null");
		
		List<Taxa> lista = findByDocmento(documento.getCddocumento().toString());
		return lista != null && lista.size() > 0 ? lista.get(0) : null;
	}
	
	/**
	* M�todo com refer�ncia ao DAO.
	* 
	* @see	br.com.linkcom.sined.geral.dao.TaxaDAO#findByDocmento(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public List<Taxa> findByDocmento(String whereIn){
		return taxaDAO.findByDocmento(whereIn);
	}
	
	/**
	 * M�todo para tirar as refer�ncias dos itens das taxas.
	 * 
	 * @param taxa
	 * @author Fl�vio Tavares
	 */
	public void limpaReferenciasTaxa(Taxa taxa){
		taxa.setCdtaxa(null);
		taxa.setCdusuarioaltera(null);
		taxa.setDtaltera(null);
		Set<Taxaitem> listaTaxaitem = taxa.getListaTaxaitem();
		
		if(listaTaxaitem != null){
			for (Taxaitem taxaitem : listaTaxaitem) {
				taxaitem.setCdtaxaitem(null);
			}
		}
	}
	
	/**
	 * M�todo que retira as refer�ncias dos itens da taxa
	 *
	 * @param listaTaxaitem
	 * @author Luiz Fernando
	 */
	public void limpaReferenciasTaxaitem(Set<Taxaitem> listaTaxaitem){
		if(listaTaxaitem != null && !listaTaxaitem.isEmpty()){
			for (Taxaitem taxaitem : listaTaxaitem) {
				taxaitem.setCdtaxaitem(null);
			}
		}
	}
	
	/**
	 * Executa um saveOrUpdate sem transa��o com o banco.
	 * 
	 * @param bean
	 * @author Hugo Ferreira
	 */
	public void saveOrUpdateNoUseTransaction(Taxa bean) {
		taxaDAO.saveOrUpdateNoUseTransaction(bean);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.TaxaDAO#findTaxa
	 * 
	 * @param taxa
	 * @return
	 * @author Rodrigo Freitas
	 */
	public Taxa findTaxa(Taxa taxa) {
		return taxaDAO.findTaxa(taxa);
	}
	
	/**
	 * M�todo para centralizar a abertura da edi��o de taxas nas telas de baixar conta
	 *
	 * @param request
	 * @param documento
	 * @return
	 * @author Rodrigo Freitas
	 * @since 26/11/2013
	 */
	public ModelAndView edicaoTaxasBaixarConta(WebRequestContext request, Documento documento) {
		String selectedItens = request.getParameter("selectedItens");
		String tipo = request.getParameter("tipo");
		
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		if(!request.getBindException().hasErrors()) {
			documento = documentoService.carregaJurosDocumento(documento);
			
			//PROVISORIAMENTE POIS ESTAVA CARREGANDO SOMENTE UM ti DA LISTA DE TAXAti
			documento.setTaxa(this.loadForEntrada(documento.getTaxa()));
		}
		
		// Seta o campo valortotaux no rateio para calcular o percentual dos campos.
		Rateio rateio = new Rateio();
//		rateio.setValortotaux(new String(documento.getListaDocumentoautori().get(0).getValor().toString()).replaceAll("\\.", ""));
		
		documento.setRateio(rateio);
				
		documento.setTipo(tipo);
		documento.setSelecteditens(selectedItens);
		return new ModelAndView("direct:/process/popup/jurosBaixarConta", "documento", documento);
	}
	
	/**
	 * M�todo que salva as taxas nas telas de baixa conta
	 *
	 * @param request
	 * @param documento
	 * @author Rodrigo Freitas
	 * @since 26/11/2013
	 */
	public void saveTaxaBaixarConta(WebRequestContext request, Documento documento) {
		if (documento == null || documento.getCddocumento() == null) {
			throw new SinedException("Documento n�o pode ser nulo.");
		}
		
		Taxa taxas = documento.getTaxa();
		
		documento = contapagarService.loadForEntradaWtihDadosPessoa(documento);
		documento.setTaxa(taxas);
		
		documento.setAcaohistorico(Documentoacao.ALTERADA);
		documento.setObservacaoHistorico("Taxa(s) alterada(s).");
		
		Documentohistorico documentohistorico = new Documentohistorico(documento);
		documentohistoricoService.saveOrUpdate(documentohistorico);
		
		boolean taxanova = false;
		if (documento.getTaxa() != null && documento.getTaxa().getCdtaxa() == null) {
			taxanova = true;
		}
		
		if (documento.getTaxa() != null){
			try{
				this.saveOrUpdate(documento.getTaxa());	
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "idx_taxaitem_tipotaxa")) {
					throw new SinedException("N�o permitida duas taxas do mesmo tipo.");
				}	
			}		
		}
		
		if (taxanova) {
			documentoService.updateTaxa(documento);
		}		
		// FEITO ISSO PARA ATUALIZAR O VALOR ATUAL DO DOCUMENTO.
		documentoService.callProcedureAtualizaDocumento(documento);
		
		
		//String href = request.getServletRequest().getContextPath() + "/financeiro/process/BaixarConta?ACAO=carregaContas&tipo="+tipo+"&selectedItens="+selectedItens;
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>window.opener.$('#recalculaValores').click();window.close();</script>");
	}
	
	/**
	 * Valida��o da tela de baixar conta
	 *
	 * @param obj
	 * @param errors
	 * @author Rodrigo Freitas
	 * @since 26/11/2013
	 */
	public void validateTaxasBaixarConta(Object obj, BindException errors) {
		Documento documento = (Documento) obj;
		this.validateTaxas(documento, errors);
		
		List<Tipotaxa> listaTipotaxa = new ArrayList<Tipotaxa>();
		if(documento.getTaxa() != null){
			Set<Taxaitem> listaItem = documento.getTaxa().getListaTaxaitem();
			if(listaItem != null && !listaItem.isEmpty()){
				for (Taxaitem taxaitem : listaItem) {
					Tipotaxa tipotaxa = taxaitem.getTipotaxa();
					if(tipotaxa != null){
						if(listaTipotaxa.contains(tipotaxa)){
							errors.reject("099", "N�o permitida duas taxas do mesmo tipo.");
							break;
						} else {
							listaTipotaxa.add(tipotaxa);
						}
					}
				}
			}
		}
	}
	
	public Double getValorTaxas(Documento documento) {
		return getValorTaxas(documento, null);
	}
	
	/**
	 * C�lcula o valor total de taxas do documento.
	 *
	 * @return
	 * @since 06/03/2013
	 * @author Giovane Freitas
	 * @param dtref 
	 */
	public Double getValorTaxas(Documento documento, java.sql.Date dtref) {
		
		Double valor = 0.0;
		Taxa taxa = documento.getTaxa();
		if (taxa == null){
			taxa = findByDocumento(documento);
		}
		
		if(taxa != null && taxa.getListaTaxaitem() != null){
			for (Taxaitem taxaitem : taxa.getListaTaxaitem()) {
				if(taxaitem.getValor() != null){
					Double valorTaxa;
					if(taxaitem.isPercentual()){
						valorTaxa = (taxaitem.getValor().getValue().doubleValue() / 100.0) * documento.getValor().getValue().doubleValue();
					} else {
						valorTaxa = taxaitem.getValor().getValue().doubleValue();
					}
					
					if(dtref != null && taxaitem.getDtlimite() != null){
						if(taxaitem.getTipotaxa().equals(Tipotaxa.MULTA) && SinedDateUtils.beforeIgnoreHour(taxaitem.getDtlimite(), dtref)){
							valor = valor + valorTaxa;
						}else if(taxaitem.getTipotaxa().equals(Tipotaxa.JUROS) && SinedDateUtils.beforeIgnoreHour(taxaitem.getDtlimite(), dtref)){
							valor = valor + valorTaxa;
						}else if(taxaitem.getTipotaxa().equals(Tipotaxa.DESAGIO) && SinedDateUtils.afterIgnoreHour(taxaitem.getDtlimite(), dtref)){
							valor = valor - valorTaxa;
						}else if(taxaitem.getTipotaxa().equals(Tipotaxa.DESCONTO) && SinedDateUtils.afterOrEqualsIgnoreHour(taxaitem.getDtlimite(), dtref)){
							valor = valor - valorTaxa;
						}else if (taxaitem.getTipotaxa().equals(Tipotaxa.TAXABOLETO) && SinedDateUtils.beforeOrEqualIgnoreHour(taxaitem.getDtlimite(), dtref)){
							valor = valor + valorTaxa;
						}else if (taxaitem.getTipotaxa().equals(Tipotaxa.TAXAMOVIMENTO) && taxaitem.getDtlimite() != null && SinedDateUtils.beforeOrEqualIgnoreHour(taxaitem.getDtlimite(), documento.getDtemissao())){
							valor = valor - valorTaxa;
						}
					}else {
						if (taxaitem.getTipotaxa().equals(Tipotaxa.DESCONTO) || taxaitem.getTipotaxa().equals(Tipotaxa.DESAGIO) ||
								taxaitem.getTipotaxa().equals(Tipotaxa.TAXAMOVIMENTO)){
							valor = valor - valorTaxa;
						} else {
							valor = valor + valorTaxa;
						}
					}
				}
			}
			
			valor = taxaitemService.getValorTaxaBaixaDesconto(new Money(valor), taxa.getListaTaxaitem(), documento.getDtemissao()).getValue().doubleValue();
			valor = taxaitemService.getValorTaxaBaixaAcrescimo(new Money(valor), taxa.getListaTaxaitem(), dtref).getValue().doubleValue();
		}

		return valor;
	}

	/**
	* M�todo que busca as taxas dos documentos
	*
	* @param listaDocumento
	* @since 26/10/2015
	* @author Luiz Fernando
	*/
	public void setTaxasDocumento(List<Documento> listaDocumento) {
		if(SinedUtil.isListNotEmpty(listaDocumento)){
			List<Taxa> listaTaxa = findByDocmento(CollectionsUtil.listAndConcatenate(listaDocumento, "cddocumento", ","));
			if(SinedUtil.isListNotEmpty(listaTaxa)){
				for(Documento documento : listaDocumento){
					if(documento.getTaxa() != null && documento.getTaxa().getCdtaxa() != null){
						for (Iterator<Taxa> iterator = listaTaxa.iterator(); iterator.hasNext();) {
							Taxa it = iterator.next();
							if(documento.getTaxa().getCdtaxa().equals(it.getCdtaxa())){
								documento.setTaxa(it);
								iterator.remove();
								break;
							}
						}
					}
				}
			}
		}
	}
	
	public Taxa findForArquivoretornoByDocumento(Documento documento){
		return taxaDAO.findForArquivoretornoByDocumento(documento);
	}
	
	public void processaBancoConfiguracaoRetorno(ArquivoConfiguravelRetornoBean bean, ArquivoConfiguravelRetornoDocumentoBean retornoBean){
		if (bean.getBancoConfiguracaoRetorno()!=null
				&& Boolean.TRUE.equals(bean.getBancoConfiguracaoRetorno().getIncluirJurosMultaDescontoTaxasDocumento())
				&& retornoBean.getDocumento()!=null){
			Taxa taxa = findByDocumento(retornoBean.getDocumento());
			
			boolean novo = false;
			boolean addDesconto = true;
			boolean addJuros = true;
			boolean addMulta = true;
			
			if (taxa==null){
				novo = true;
				taxa = new Taxa();
			}else if (taxa.getListaTaxaitem()!=null){
				for (Taxaitem taxaitem: taxa.getListaTaxaitem()){
					if (Tipotaxa.DESCONTO.equals(taxaitem.getTipotaxa())){
						addDesconto = false;
					}else if (Tipotaxa.JUROS.equals(taxaitem.getTipotaxa())){
						addJuros = false;
					}else if (Tipotaxa.MULTA.equals(taxaitem.getTipotaxa())){
						addMulta = false;
					}
				}
			}
			
			Set<Taxaitem> listaTaxaitem = taxa.getListaTaxaitem();
			
			if (listaTaxaitem==null || listaTaxaitem.isEmpty()){
				listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
			}
			
			if (addDesconto && retornoBean.getValorDescontoOutroDesconto()!=null && retornoBean.getValorDescontoOutroDesconto().toLong()>0){
				Taxaitem taxaitem = new Taxaitem();
				taxaitem.setTipotaxa(Tipotaxa.DESCONTO);
				taxaitem.setMotivo("Inserido a partir do processamento de arquivo retorno");
				taxaitem.setPercentual(false);
				taxaitem.setValor(retornoBean.getValorDescontoOutroDesconto());
				taxaitem.setDtlimite(retornoBean.getDtcredito()!=null ? new java.sql.Date(retornoBean.getDtcredito().getTime()) : SinedDateUtils.currentDate());
				listaTaxaitem.add(taxaitem);
			}
			
			if (addJuros && retornoBean.getValorjuros()!=null && retornoBean.getValorjuros().toLong()>0){
				Taxaitem taxaitem = new Taxaitem();
				taxaitem.setTipotaxa(Tipotaxa.JUROS);
				taxaitem.setMotivo("Inserido a partir do processamento de arquivo retorno");
				taxaitem.setPercentual(false);
				taxaitem.setValor(retornoBean.getValorjuros());
				taxaitem.setDtlimite(retornoBean.getDtcredito()!=null ? new java.sql.Date(retornoBean.getDtcredito().getTime()) : SinedDateUtils.currentDate());
				listaTaxaitem.add(taxaitem);
			}
			
			if (addMulta && retornoBean.getValormulta()!=null && retornoBean.getValormulta().toLong()>0){
				Taxaitem taxaitem = new Taxaitem();
				taxaitem.setTipotaxa(Tipotaxa.MULTA);
				taxaitem.setMotivo("Inserido a partir do processamento de arquivo retorno");
				taxaitem.setPercentual(false);
				taxaitem.setValor(retornoBean.getValormulta());
				taxaitem.setDtlimite(retornoBean.getDtcredito()!=null ? new java.sql.Date(retornoBean.getDtcredito().getTime()) : SinedDateUtils.currentDate());
				listaTaxaitem.add(taxaitem);
			}
			
			if (addDesconto || addJuros || addMulta){
				taxa.setListaTaxaitem(listaTaxaitem);
				saveOrUpdate(taxa);
				if (novo){
					documentoService.updateTaxa(retornoBean.getDocumento(), taxa);
				}
			}
			
			retornoBean.setAddJurosMulta(addJuros || addMulta);
		}		
	}
	
	/**
	 * TODO
	 * @param taxa
	 * @return
	 */
	public Map<String, String> criarMensagemPadraoTaxa(Taxa taxa){
		StringBuilder msg2 = new StringBuilder();
		StringBuilder msg3 =  new StringBuilder();

		if(taxa != null && SinedUtil.isListNotEmpty(taxa.getListaTaxaitem())){
			for (Taxaitem item : taxa.getListaTaxaitem()) {
				if(!Boolean.TRUE.equals(item.getGerarmensagem())){
					continue;
				}
				String deApos = tipotaxaService.isApos(item.getTipotaxa())? " ap�s ": " at� ";
				
				String msg = item.getTipotaxa().getNome() + " de " + (item.isPercentual() ? "" : "R$") +
							item.getValor().toString() + (item.isPercentual() ? "%" : "") +
							deApos + SinedDateUtils.toString(item.getDtlimite()) + ". ";
				
				if((msg2.length() + msg.length()) <= 80)
					msg2.append(msg);
				else
					msg3.append(msg);
			}
		}
		Map<String, String> mapMsg = new HashMap<String, String>();
		mapMsg.put("mensagem2", msg2.toString());
		mapMsg.put("mensagem3", msg3.toString());
		return mapMsg;
	}
}