package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Contratomaterial;
import br.com.linkcom.sined.geral.bean.Servicoservidortipo;
import br.com.linkcom.sined.geral.bean.Servicoweb;
import br.com.linkcom.sined.geral.bean.enumeration.AcaoBriefCase;
import br.com.linkcom.sined.geral.dao.ServicowebDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ServicowebService extends GenericService<Servicoweb>{
	
	private ServicowebDAO servicowebDAO;
	private ServicoftpService servicoftpService;
	private DominioService dominioService;
	private ClienteService clienteService;
	private ServicoservidorService servicoservidorService;
	
	public void setServicowebDAO(ServicowebDAO servicowebDAO) {
		this.servicowebDAO = servicowebDAO;
	}
	public void setServicoftpService(ServicoftpService servicoftpService) {
		this.servicoftpService = servicoftpService;
	}
	public void setDominioService(DominioService dominioService) {
		this.dominioService = dominioService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setServicoservidorService(
			ServicoservidorService servicoservidorService) {
		this.servicoservidorService = servicoservidorService;
	}
	
	@Override
	public void saveOrUpdate(Servicoweb bean) {
		boolean novoRegistro = false;
		if(bean.getCdservicoweb() == null){
			novoRegistro = true;
		}
		preparaServicoFTPWeb(bean);
		servicoftpService.saveOrUpdate(bean.getServicoftp());
		
		super.saveOrUpdate(bean);
		
		SinedUtil.executaBriefCase(Servicoservidortipo.WEB, novoRegistro ? AcaoBriefCase.CADASTRAR : AcaoBriefCase.ALTERAR, bean.getCdservicoweb());
	}
	
	/**
	 * Seta valores para salvar Servi�o FTP do servi�o Web
	 * 
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	private void preparaServicoFTPWeb(Servicoweb bean) {
		bean.setCliente(clienteService.load(bean.getCliente()));
		bean.setDominio(dominioService.load(bean.getDominio()));
		bean.getServicoftp().setServicoservidor(servicoservidorService.findByServidorAndServicoServidorTipoWeb(bean.getServidor()));
		bean.getServicoftp().setContratomaterial(bean.getContratomaterial());
		bean.getServicoftp().setUsuario(bean.getDominio().getNome()+"."+bean.getCliente().getNome());
		bean.getServicoftp().setCliente(bean.getCliente());
	}
	
	@Override
	public void delete(Servicoweb bean) {
		SinedUtil.executaBriefCase(Servicoservidortipo.WEB, AcaoBriefCase.REMOVER, bean.getCdservicoweb());
		super.delete(bean);
	}
	
	public int countContratomaterial(Integer cdservicoweb, Contratomaterial contratomaterial) {
		return servicowebDAO.countContratomaterial(cdservicoweb, contratomaterial);
	}
	
}
