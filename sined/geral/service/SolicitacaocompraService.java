package br.com.linkcom.sined.geral.service;

import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.AUTORIZADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.BAIXADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.CANCELADA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.EM_ABERTO;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.EM_PROCESSOCOMPRA;
import static br.com.linkcom.sined.geral.bean.Situacaosuprimentos.NAO_AUTORIZADA;

import java.awt.Image;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Cotacao;
import br.com.linkcom.sined.geral.bean.Cotacaoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialgrupousuario;
import br.com.linkcom.sined.geral.bean.Materialunidademedida;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Orcamentomaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompra;
import br.com.linkcom.sined.geral.bean.Ordemcompraacao;
import br.com.linkcom.sined.geral.bean.Ordemcompraentrega;
import br.com.linkcom.sined.geral.bean.Ordemcomprahistorico;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.bean.Ordemcompraorigem;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendahistorico;
import br.com.linkcom.sined.geral.bean.Pedidovendamaterial;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Planejamentorecursogeral;
import br.com.linkcom.sined.geral.bean.Producaoagendahistorico;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Requisicaomaterial;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;
import br.com.linkcom.sined.geral.bean.Solicitacaocompra;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraacao;
import br.com.linkcom.sined.geral.bean.Solicitacaocomprahistorico;
import br.com.linkcom.sined.geral.bean.Solicitacaocompraorigem;
import br.com.linkcom.sined.geral.bean.Tarefarecursogeral;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;
import br.com.linkcom.sined.geral.bean.Vendaorcamentomaterial;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaopedidosolicitacaocompra;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.OrigemsuprimentosBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.EmitirprazomediocompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.filter.SolicitacaocompraFiltro;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.FluxoSituacaoSolicitacaoProcess;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.PrazomediocompraReportBean;
import br.com.linkcom.sined.modulo.suprimentos.controller.report.bean.SolicitacaocompraReportBean;
import br.com.linkcom.sined.util.EmailManager;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.math.BigDecimal;

public class SolicitacaocompraService extends GenericService<Solicitacaocompra> {
	
	private SolicitacaocompraorigemService solicitacaocompraorigemService;
	private SolicitacaocompraDAO solicitacaocompraDAO;
	private AvisoService avisoService;
	private SolicitacaocomprahistoricoService solicitacaocomprahistoricoService;
	private EmpresaService empresaService;
	private UsuarioService usuarioService;
	private ParametrogeralService parametrogeralService;
	private PedidovendahistoricoService pedidovendahistoricoService;
	private ProducaoagendahistoricoService producaoagendahistoricoService;
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private CotacaoService cotacaoService;
	private RomaneioitemService romaneioitemService;
	private MaterialformulapesoService materialformulapesoService;
	private PlanejamentorecursogeralService planejamentorecursogeralService;
	private MotivoavisoService motivoavisoService;
	private AvisousuarioService avisoUsuarioService;
	private PedidovendaService pedidovendaService;
	
	public void setMaterialformulapesoService(
			MaterialformulapesoService materialformulapesoService) {
		this.materialformulapesoService = materialformulapesoService;
	}
	public void setRomaneioitemService(RomaneioitemService romaneioitemService) {
		this.romaneioitemService = romaneioitemService;
	}
	public void setProducaoagendahistoricoService(
			ProducaoagendahistoricoService producaoagendahistoricoService) {
		this.producaoagendahistoricoService = producaoagendahistoricoService;
	}
	public void setSolicitacaocompraDAO(
			SolicitacaocompraDAO solicitacaocompraDAO) {
		this.solicitacaocompraDAO = solicitacaocompraDAO;
	}
	public void setSolicitacaocompraorigemService(
			SolicitacaocompraorigemService solicitacaocompraorigemService) {
		this.solicitacaocompraorigemService = solicitacaocompraorigemService;
	}
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	public void setSolicitacaocomprahistoricoService(
			SolicitacaocomprahistoricoService solicitacaocomprahistoricoService) {
		this.solicitacaocomprahistoricoService = solicitacaocomprahistoricoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPedidovendahistoricoService(PedidovendahistoricoService pedidovendahistoricoService) {
		this.pedidovendahistoricoService = pedidovendahistoricoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setCotacaoService(CotacaoService cotacaoService) {
		this.cotacaoService = cotacaoService;
	}
	public void setPlanejamentorecursogeralService(PlanejamentorecursogeralService planejamentorecursogeralService) {
		this.planejamentorecursogeralService = planejamentorecursogeralService;
	}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}
	public void setAvisoUsuarioService(AvisousuarioService avisoUsuarioService) {
		this.avisoUsuarioService = avisoUsuarioService;
	}
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	
	@Override
	public void saveOrUpdate(final Solicitacaocompra bean) {
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(bean.getCdsolicitacaocompra() == null){
					solicitacaocompraorigemService.saveOrUpdateNoUseTransaction(bean.getSolicitacaocompraorigem());
				}
				saveOrUpdateNoUseTransaction(bean);
				
				if(bean.getSolicitacaocomprahistorico() !=  null)
					solicitacaocomprahistoricoService.saveOrUpdateNoUseTransaction(bean.getSolicitacaocomprahistorico());
				return status;
			}
		});
		
		// ATUALIZA A TABELA AUXILIAR DE SOLICITA��O DE COMPRA
		this.callProcedureAtualizaSolicitacaocompra(bean);
	}
	
	
	@Override
	public void saveOrUpdateNoUseTransaction(Solicitacaocompra bean) {
		super.saveOrUpdateNoUseTransaction(bean);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForCotacao
	 * @param whereIn
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForCotacao(String whereIn) {
		return solicitacaocompraDAO.findForCotacao(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForCsv
	 *
	 * @param filtro
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForCsv(SolicitacaocompraFiltro filtro) {
		return solicitacaocompraDAO.findForCsv(filtro);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findByCotacao
	 * @param cotacao
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findByCotacao(Cotacao cotacao){
		return solicitacaocompraDAO.findByCotacao(cotacao);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findByCotacaoForBaixaEntrega
	 * @param cotacao
	 * @param material
	 * @param localarmazenagem
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findByCotacaoForBaixaEntrega(Cotacao cotacao, Material material, Localarmazenagem localarmazenagem){
		return solicitacaocompraDAO.findByCotacaoForBaixaEntrega(cotacao, material, localarmazenagem);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForVerificacao
	 * @param ids
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Solicitacaocompra> findForVerificacao(String ids) {
		return solicitacaocompraDAO.findForVerificacao(ids);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findSolicitacoes
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> findSolicitacoes(String whereIn) {
		return solicitacaocompraDAO.findSolicitacoes(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#doUpdateSolicitacoesParaAutorizada
	 * @param bean
	 * @author Tom�s Rabelo
	 */
	public void doUpdateSituacaoSolicitacoes(String whereIn, Situacaosuprimentos situacao) {
		solicitacaocompraDAO.doUpdateSituacaoSolicitacoes(whereIn, situacao);
		
		// ATUALIZA AS SOLICITACOES DO WHERE IN
		this.callProcedureAtualizaSolicitacaocompra(whereIn);
	}
	
	/**
	 * Gera relat�rio com valores iguais o da listagem.
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForCsv
	 * 
	 * @param filtro
	 * @return
	 * @author Tom�s Rabelo
	 */
	public IReport gerarRelatorio(SolicitacaocompraFiltro filtro) {
		Report report = new Report("/suprimento/solicitacaocompra");
		
		SolicitacaocompraReportBean bean;
		List<SolicitacaocompraReportBean> lista = new ArrayList<SolicitacaocompraReportBean>();
		
		List<Solicitacaocompra> listaSol = this.findForCsv(filtro);
		Double pesoTotal = 0d;
		for (Solicitacaocompra sc : listaSol) {
			bean = new SolicitacaocompraReportBean();
			
			bean.setIdent(sc.getDescricao());
			
			if(sc.getMaterial() != null)
				bean.setMaterialservico(sc.getMaterial().getAutocompleteDescription());
			
			if(sc.getQtde() != null)
				bean.setQtde(SinedUtil.descriptionDecimal(sc.getQtde()));
			
			bean.setProjdep(sc.getProjetodepartamento());
			
			if(sc.getLocalarmazenagem() != null)
				bean.setLocal(sc.getLocalarmazenagem().getNome());
			
			if(sc.getSolicitacaocompraorigem().getUsuario() != null && sc.getSolicitacaocompraorigem().getUsuario().getNome() != null){
				bean.setSolicitante(sc.getSolicitacaocompraorigem().getUsuario().getNome());
			} else if(sc.getSolicitacaocompraorigem().getProjeto() != null && sc.getSolicitacaocompraorigem().getProjeto().getNome() != null){
				bean.setSolicitante(sc.getSolicitacaocompraorigem().getProjeto().getNome());
			} else if(sc.getSolicitacaocompraorigem().getOrcamentomaterial() != null && sc.getSolicitacaocompraorigem().getOrcamentomaterial().getDescricao() != null){
				bean.setSolicitante("[LM] " + sc.getSolicitacaocompraorigem().getOrcamentomaterial().getDescricao());
			} else if(sc.getSolicitacaocompraorigem().getRequisicaomaterial() != null && sc.getSolicitacaocompraorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null){
				bean.setSolicitante("Requisi��o material " + sc.getSolicitacaocompraorigem().getRequisicaomaterial().getCdrequisicaomaterial());
			} else if(sc.getSolicitacaocompraorigem().getMaterial() != null && sc.getSolicitacaocompraorigem().getMaterial().getNome() != null){
				bean.setSolicitante(sc.getSolicitacaocompraorigem().getMaterial().getNome());
			}
			
			if(sc.getDtsolicitacao() != null)
				bean.setDatasolicitacao(SinedDateUtils.toString(sc.getDtsolicitacao()));
			
			if(sc.getDtautorizacao() != null) 
				bean.setDataautorizacao(SinedDateUtils.toString(sc.getDtautorizacao()));
			
			if(sc.getDtlimite() != null) 
				bean.setDatanecessidade(SinedDateUtils.toString(sc.getDtlimite()));
			
			bean.setObservacao(sc.getObservacao());
			
			if(sc.getAux_solicitacaocompra() != null && sc.getAux_solicitacaocompra().getSituacaosuprimentos() != null) 
				bean.setSituacao(sc.getAux_solicitacaocompra().getSituacaosuprimentos().getDescricao());
			
			if(sc.getPesototal() != null){
				pesoTotal+= sc.getPesototal();
			}
			
			lista.add(bean);
		}
		
		Report subReportResumoUnidademedida = new Report("/suprimento/resumounidademedida");
		report.addSubReport("SUB_RESUMOUNIDADEMEDIDA", subReportResumoUnidademedida);
		report.addParameter("LISTARESUMOUNIDADEMEDIDA", this.createResumoSolicitacaocompra(listaSol));
		report.addParameter("PESOTOTAL", pesoTotal);
		report.setDataSource(lista);
		
		return report;
	}
	
	/**
	 * M�todo monta a origem dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaOrigem(Solicitacaocompra form) {
		OrigemsuprimentosBean origemsuprimentosBean = null;
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		
		if(form.getSolicitacaocompraorigem().getUsuario() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.USUARIO, "USU�RIO", form.getSolicitacaocompraorigem().getUsuario().getNome(), 
															  form.getSolicitacaocompraorigem().getUsuario().getCdpessoa() != null ? form.getSolicitacaocompraorigem().getUsuario().getCdpessoa().toString() : "", 
																	  null);
		
			listaBean.add(origemsuprimentosBean);
		} else if(form.getSolicitacaocompraorigem().getRequisicaomaterial() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.REQUISICAO, "REQUISI��O DE MATERIAL", 
					  form.getSolicitacaocompraorigem().getRequisicaomaterial().getIdentificador() != null ?  form.getSolicitacaocompraorigem().getRequisicaomaterial().getIdentificador().toString() : "", 
					  form.getSolicitacaocompraorigem().getRequisicaomaterial().getCdrequisicaomaterial() != null ? form.getSolicitacaocompraorigem().getRequisicaomaterial().getCdrequisicaomaterial().toString() : "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		} else if(form.getSolicitacaocompraorigem().getOrcamentomaterial() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.LISTAMATERIAL, "LISTA DE MATERIAL DO OR�AMENTO", 
					  form.getSolicitacaocompraorigem().getOrcamentomaterial().getDescricao() != null ?  form.getSolicitacaocompraorigem().getOrcamentomaterial().getDescricao() : "" , "", 
							  null);

			listaBean.add(origemsuprimentosBean);
		} else if(form.getSolicitacaocompraorigem().getPlanejamentorecursogeral() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PLANEJAMENTO, "PLANEJAMENTO", 
					(form.getSolicitacaocompraorigem().getPlanejamentorecursogeral().getPlanejamento().getDescricao() != null ? form.getSolicitacaocompraorigem().getPlanejamentorecursogeral().getPlanejamento().getDescricao() : "") , 
					(form.getSolicitacaocompraorigem().getPlanejamentorecursogeral().getPlanejamento().getCdplanejamento() != null ? form.getSolicitacaocompraorigem().getPlanejamentorecursogeral().getPlanejamento().getCdplanejamento().toString() : ""), 
					  null );

			listaBean.add(origemsuprimentosBean);
		} else if(form.getSolicitacaocompraorigem().getTarefarecursogeral() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.TAREFA, "TAREFA", 
					(form.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getDescricao() != null ? form.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getDescricao() : "") , 
					(form.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getCdtarefa() != null ? form.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getCdtarefa().toString() : ""), 
					  null );

			listaBean.add(origemsuprimentosBean);
		} else if(form.getSolicitacaocompraorigem().getProducaoagenda() != null){
			origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.PRODUCAOAGENDA, "AGENDA DE PRODU��O", 
					(form.getSolicitacaocompraorigem().getProducaoagenda().getCdproducaoagenda() != null ? form.getSolicitacaocompraorigem().getProducaoagenda().getCdproducaoagenda().toString() : "") , 
					(form.getSolicitacaocompraorigem().getProducaoagenda().getCdproducaoagenda() != null ? form.getSolicitacaocompraorigem().getProducaoagenda().getCdproducaoagenda().toString() : ""), 
					  null );

			listaBean.add(origemsuprimentosBean);
		}
		
		return listaBean;
	}
	
	/**
	 * M�todo que monta solicita��o de compra a partir da requisi��o e salva
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraorigemService#saveOrUpdateNoUseTransaction(Solicitacaocompraorigem)
	 * @param solicitacoes
	 * @author Tom�s Rabelo
	 */
	public String saveSolicitacoes(final List<Solicitacaocompra> solicitacoes) {
		Object codigos = getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				StringBuilder codigosGerados = new StringBuilder();
				
				for (Solicitacaocompra solicitacaocompra : solicitacoes) {
					solicitacaocompraorigemService.saveOrUpdateNoUseTransaction(solicitacaocompra.getSolicitacaocompraorigem());
					saveOrUpdateNoUseTransaction(solicitacaocompra);
					codigosGerados.append(solicitacaocompra.getCdsolicitacaocompra()+", ");
				}
				
				if(codigosGerados != null && codigosGerados.length() > 0) codigosGerados.delete(codigosGerados.length()-2, codigosGerados.length());
				
				return codigosGerados.toString();
			}
		});
		
		// ATUALIZA A TABELA AUXILIAR DE SOLICITA��O DE COMPRA
		this.callProcedureAtualizaSolicitacaocompra((String) codigos);
		
		
		return (String) codigos;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param form
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public Solicitacaocompra getSolicitacaoDaRequisicao(Requisicaomaterial form) {
		List<Solicitacaocompra> lista = getSolicitacoesDaRequisicao(form);
		return lista != null && lista.size() > 0 ? lista.get(0) : null;
	}
	
	
	/**
	* M�todo que busca as solicita��es ativas ou seja que o dtcancelamento est� nulo
	*
	* @param form
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> getSolicitacoesDaRequisicao(Requisicaomaterial form) {
		return getSolicitacoesDaRequisicao(form, true);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#getSolicitacoesDaRequisicao(Requisicaomaterial form, Boolean ativa)
	*
	* @param form
	* @param ativa
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> getSolicitacoesDaRequisicao(Requisicaomaterial form, Boolean ativa) {
		return solicitacaocompraDAO.getSolicitacoesDaRequisicao(form, ativa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * @param solicitacaocompra
	 * @return
	 * @author Tom�s T. Rabelo
	 */
	public boolean existeSolicitacaoGeradaRequisicaoDiferenteDeCancelada(Solicitacaocompra solicitacaocompra) {
		return solicitacaocompraDAO.existeSolicitacaoGeradaRequisicaoDiferenteDeCancelada(solicitacaocompra);
	}
	
	/**
	 * M�todo monta a destino dependendo do valor
	 * @param form
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<OrigemsuprimentosBean> montaDestino(Solicitacaocompra form) {
		List<OrigemsuprimentosBean> listaBean = new ArrayList<OrigemsuprimentosBean>();
		if(form.getListaCotacaoorigem() != null && form.getListaCotacaoorigem().size() > 0){
			for (Cotacaoorigem cotacaoorigem : form.getListaCotacaoorigem()) {
				if(!cotacaoorigem.getCotacao().getAux_cotacao().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.COTACAO, "COTA��O", cotacaoorigem.getCotacao().getCdcotacao().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
			}
		}
		if(form.getListaOrdemcompraorigem() != null && form.getListaOrdemcompraorigem().size() > 0){
			for (Ordemcompraorigem ordemcompraorigem : form.getListaOrdemcompraorigem()) {
				if(ordemcompraorigem.getOrdemcompra() != null && 
						ordemcompraorigem.getOrdemcompra().getAux_ordemcompra() != null && 
						ordemcompraorigem.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos() != null &&
						!ordemcompraorigem.getOrdemcompra().getAux_ordemcompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
					OrigemsuprimentosBean origemsuprimentosBean = new OrigemsuprimentosBean(OrigemsuprimentosBean.ORDEMCOMPRA, "ORDEM DE COMPRA", ordemcompraorigem.getOrdemcompra().getCdordemcompra().toString(), null);
					listaBean.add(origemsuprimentosBean);
				}
			}
		}
		return listaBean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> findSolicitacoesParaInserirCotacao(String whereIn) {
		return solicitacaocompraDAO.findSolicitacoesParaInserirCotacao(whereIn);
	}
	
	/**
	 * M�todo que junta as informa��es do mestre com seus detalhes transformando em v�rias Requisi��es
	 * 
	 * @param bean
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> preparaSaveEmMassa(Solicitacaocompra bean) {
		List<Solicitacaocompra> list = new ListSet<Solicitacaocompra>(Solicitacaocompra.class);
		
		String valor = parametrogeralService.getValorPorNome(Parametrogeral.GERARIDENTIFICADORMATERIAL);
		boolean verificadorParametroGeral = valor != null && valor.trim().toUpperCase().equals("TRUE");
		
		if (!verificadorParametroGeral){
			Integer identificador = getUltimoIdentificador();
			bean.setIdentificador(new Integer(identificador != null ? identificador+1 : 0));
			
			if(bean.getDescricao() == null || bean.getDescricao().trim().equals("")){
				SimpleDateFormat format = new SimpleDateFormat("yy");
				bean.setDescricao(bean.getIdentificador()+"/"+format.format(new Date()));
			}
		}
		
		for (Solicitacaocompra solicitacaocompra : bean.getSolicitacoes()) {
			if (verificadorParametroGeral){
				Integer identificador = getUltimoIdentificador();
				solicitacaocompra.setIdentificador(new Integer(identificador != null ? (identificador + 1) : 0));
				if(solicitacaocompra.getDescricao() == null || solicitacaocompra.getDescricao().trim().equals("")){
					SimpleDateFormat format = new SimpleDateFormat("yy");
					solicitacaocompra.setDescricao(solicitacaocompra.getIdentificador()+"/"+format.format(new Date()));
				}
			}
			
			if(solicitacaocompra.getCdtarefarecursogeral() != null){
				Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
				solicitacaocompraorigem.setTarefarecursogeral(new Tarefarecursogeral(solicitacaocompra.getCdtarefarecursogeral()));
				bean.setSolicitacaocompraorigem(solicitacaocompraorigem);
			}
			
			if(solicitacaocompra.getCdplanejamentorecursogeral() != null) {
				Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
				solicitacaocompraorigem.setPlanejamentorecursogeral(new Planejamentorecursogeral(solicitacaocompra.getCdplanejamentorecursogeral()));
				bean.setSolicitacaocompraorigem(solicitacaocompraorigem);
			}
			
			if(solicitacaocompra.getProducaoagenda() != null && solicitacaocompra.getProducaoagenda().getCdproducaoagenda() != null){
				Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
				solicitacaocompraorigem.setProducaoagenda(solicitacaocompra.getProducaoagenda());
				bean.setSolicitacaocompraorigem(solicitacaocompraorigem);
			}
			if(solicitacaocompra.getCdpedidovenda() != null){
				Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
				solicitacaocompraorigem.setPedidovenda(new Pedidovenda(solicitacaocompra.getCdpedidovenda()));
				bean.setSolicitacaocompraorigem(solicitacaocompraorigem);
			}
			if(solicitacaocompra.getCdvendaorcamento() != null){
				Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
				solicitacaocompraorigem.setVendaorcamento(new Vendaorcamento(solicitacaocompra.getCdvendaorcamento()));
				bean.setSolicitacaocompraorigem(solicitacaocompraorigem);
			}
			
			Solicitacaocompra solicitacaocompra2;
			
			if (verificadorParametroGeral){
				solicitacaocompra2 = new Solicitacaocompra(
						solicitacaocompra.getDescricao(), bean.getDtlimite(), bean.getProjeto(), bean.getDepartamento(), bean.getLocalarmazenagem(),  bean.getCentrocusto(), bean.getEmpresa(),
						bean.getColaborador(), solicitacaocompra.getMaterial(), solicitacaocompra.getMaterialclasse(), solicitacaocompra.getQtde(), bean.getSolicitacaocompraorigem(), 
						bean.getIdentificador(), bean.getFaturamentocliente(), bean.getObservacao(), solicitacaocompra.getFrequencia(), solicitacaocompra.getQtdefrequencia(), solicitacaocompra.getIdentificador(),
						solicitacaocompra.getUnidademedida());
			} else{
				solicitacaocompra2 = new Solicitacaocompra(
						bean.getDescricao(), bean.getDtlimite(), bean.getProjeto(), bean.getDepartamento(), bean.getLocalarmazenagem(),  bean.getCentrocusto(), bean.getEmpresa(),
						bean.getColaborador(), solicitacaocompra.getMaterial(), solicitacaocompra.getMaterialclasse(), solicitacaocompra.getQtde(), bean.getSolicitacaocompraorigem(), 
						bean.getIdentificador(), bean.getFaturamentocliente(), bean.getObservacao(), solicitacaocompra.getFrequencia(), solicitacaocompra.getQtdefrequencia(), bean.getIdentificador(),
						solicitacaocompra.getUnidademedida());
			}
			
			
			if(bean.getContagerencial() != null) solicitacaocompra2.setContagerencial(bean.getContagerencial());
			if(bean.getMaterialgrupo() != null) solicitacaocompra2.setMaterialgrupo(bean.getMaterialgrupo());
			solicitacaocompra2.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra2, Solicitacaocompraacao.CRIADA));
			
			if(solicitacaocompra.getCdpedidovenda() != null){
				solicitacaocompra2.setCdpedidovenda(solicitacaocompra.getCdpedidovenda());
				
				String linkPedido = "";
				if(pedidovendaService.isOtr(solicitacaocompra.getCdpedidovenda())) {
					linkPedido = "/w3erp/faturamento/crud/PedidovendaOtr";
				} else {
					linkPedido = "/w3erp/faturamento/crud/Pedidovenda";
				}
				solicitacaocompra2.getSolicitacaocomprahistorico().setObservacao("Origem pedido de venda <a href=" + linkPedido + "?ACAO=consultar&cdpedidovenda=" + solicitacaocompra2.getCdpedidovenda() +">"+solicitacaocompra2.getCdpedidovenda()+"</a>");
				
				solicitacaocompra2.setSolicitacaocompraorigem(bean.getSolicitacaocompraorigem());
			}
			if(solicitacaocompra.getCdvendaorcamento() != null){
				solicitacaocompra2.setCdvendaorcamento(solicitacaocompra.getCdvendaorcamento());
				solicitacaocompra2.getSolicitacaocomprahistorico().setObservacao("Origem or�amento <a href=/w3erp/faturamento/crud/VendaOrcamento?ACAO=consultar&cdvendaorcamento=" + solicitacaocompra2.getCdvendaorcamento() +">"+solicitacaocompra2.getCdvendaorcamento()+"</a>");
				solicitacaocompra2.setSolicitacaocompraorigem(bean.getSolicitacaocompraorigem());
			}
			if((solicitacaocompra.getCdpedidovenda() != null || solicitacaocompra.getCdvendaorcamento() != null) && Util.strings.isNotEmpty(solicitacaocompra.getObservacao())){
				if(Util.strings.isNotEmpty(solicitacaocompra2.getObservacao())){
					solicitacaocompra2.setObservacao(solicitacaocompra2.getObservacao() + " / " + solicitacaocompra.getObservacao());
				}else{
					solicitacaocompra2.setObservacao(solicitacaocompra.getObservacao());
				}
			}
			
			if(Util.strings.emptyIfNull(solicitacaocompra2.getObservacao()).length() > 100){
				solicitacaocompra2.setObservacao(solicitacaocompra2.getObservacao().substring(0, 100));
			}
			
			solicitacaocompra2.setPesototal(solicitacaocompra.getPesototal());
			solicitacaocompra2.setAltura(solicitacaocompra.getAltura());
			solicitacaocompra2.setComprimento(solicitacaocompra.getComprimento());
			solicitacaocompra2.setLargura(solicitacaocompra.getLargura());
			solicitacaocompra2.setQtdvolume(solicitacaocompra.getQtdvolume());
			
			
			if(bean.getArquivo() != null){
				solicitacaocompra2.setArquivo(new Arquivo(bean.getArquivo().getContent(), bean.getArquivo().getNome(), bean.getArquivo().getContenttype()));
				
			}
			list.add(solicitacaocompra2);
		}
		
		return list;
	}
	
	/**
	 * Salva lista de solicitac��es
	 * 
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraorigemService#saveOrUpdateNoUseTransaction(Solicitacaocompraorigem)
	 * @see #saveOrUpdateNoUseTransaction(Solicitacaocompra)
	 * @see #montaAvisosSolicitacaoCompra(List, List, String)
	 * @see br.com.linkcom.sined.geral.service.AvisoService#saveOrUpdateNoUseTransaction(Aviso)
	 * @param solicitacoes
	 * @author Tom�s Rabelo
	 * @param listaPapeis 
	 * @param boolean1 
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public void salvaSolicitacaoEmMassa(final List<Solicitacaocompra> solicitacoes, final List<Papel> listaPapeis, Boolean urgente) {
		List<Solicitacaocompra> lista = (List<Solicitacaocompra>)getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				for (Solicitacaocompra solicitacaocompra : solicitacoes) {
					solicitacaocompra.setDtsolicitacao(new java.sql.Date(System.currentTimeMillis()));
					solicitacaocompra.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					solicitacaocompraorigemService.saveOrUpdateNoUseTransaction(solicitacaocompra.getSolicitacaocompraorigem());
					saveOrUpdateNoUseTransaction(solicitacaocompra);
					solicitacaocomprahistoricoService.saveOrUpdateNoUseTransaction(solicitacaocompra.getSolicitacaocomprahistorico());
				}
				
				return solicitacoes;
			}
		});
		
		List<Usuario> usuarioList = avisoService.salvarAvisos(montaAvisosSolicitacaoCompra(MotivoavisoEnum.AUTORIZAR_SOLICITACAOCOMPRA, solicitacoes, "Aguardando autoriza��o"), true);
		
//		if(urgente != null && urgente){
//			Projeto projeto;
//			Colaborador colaborador;
//			Empresa empresa;
//			Empresa empresaPrincipal = empresaService.loadPrincipal();
//			
//			for (Solicitacaocompra sc : lista) {
//				if(sc.getProjeto() != null && sc.getProjeto().getCdprojeto() != null){
//					if(sc.getEmpresa() != null && sc.getEmpresa().getCdpessoa() != null){
//						empresa = empresaService.load(sc.getEmpresa(), "empresa.cdpessoa, empresa.email");
//					} else {
//						empresa = empresaPrincipal;
//					}
//					projeto = projetoService.loadForEmail(sc.getProjeto());
//					colaborador = projeto.getColaborador();
//
//					String assunto = "Autoriza��o da Solcita��o de compra " + sc.getDescricao();
//					String mensagem = "<b>Aviso do sistema:</b> A solicita��o de compra " + sc.getDescricao() + 
//						" necessita de autoriza��o. <BR><BR>" + "<a href=\"" + SinedUtil.getUrlWithContext() + 
//						"/suprimento/crud/Solicitacaocompra?ACAO=consultar&cdsolicitacaocompra=" + sc.getCdsolicitacaocompra() +
//						"\">Solicita��o de compra</a>";
//
//					Envioemail envioemail = envioemailService.registrarEnvio(empresa.getEmail(), assunto, mensagem, Envioemailtipo.AVISO_AUTORIZACAO_COMPRA, 
//							colaborador, colaborador.getEmail(), colaborador.getNome());
//						
//					try {
//						EmailManager email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
//						email.setFrom(empresa.getEmail());
//						email.setSubject(assunto);
//						email.setTo(colaborador.getEmail());
//						email.addHtmlText(mensagem + EmailUtil.getHtmlConfirmacaoEmail(envioemail, colaborador.getEmail()));
//				
//						email.sendMessage();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		}
		
		Pedidovendahistorico pedidovendahistorico;
		for(Solicitacaocompra solicitacaocompra : lista){
			if(solicitacaocompra.getSolicitacaocompraorigem() != null && solicitacaocompra.getSolicitacaocompraorigem().getPedidovenda() != null){
				pedidovendahistorico = new Pedidovendahistorico();
				pedidovendahistorico.setAcao("Solicita��o de compra gerada");
				pedidovendahistorico.setObservacao("Solicita��o de compra criada <a href=/w3erp/suprimento/crud/Solicitacaocompra?ACAO=consultar&cdsolicitacaocompra=" + solicitacaocompra.getCdsolicitacaocompra() +">"+solicitacaocompra.getCdsolicitacaocompra()+"</a>");
				pedidovendahistorico.setPedidovenda(solicitacaocompra.getSolicitacaocompraorigem().getPedidovenda());
				pedidovendahistoricoService.saveOrUpdate(pedidovendahistorico);
			}
			
		}
		
		
		for(Solicitacaocompra solicitacaocompra : lista){
			if(solicitacaocompra.getSolicitacaocompraorigem() != null && solicitacaocompra.getSolicitacaocompraorigem().getProducaoagenda() != null){
				Producaoagendahistorico producaoagendahistorico = new Producaoagendahistorico();
				producaoagendahistorico.setProducaoagenda(solicitacaocompra.getSolicitacaocompraorigem().getProducaoagenda());
				producaoagendahistorico.setObservacao("Solicita��o de compra criada <a href=/w3erp/suprimento/crud/Solicitacaocompra?ACAO=consultar&cdsolicitacaocompra=" + solicitacaocompra.getCdsolicitacaocompra() +">"+solicitacaocompra.getCdsolicitacaocompra()+"</a>");
				producaoagendahistoricoService.saveOrUpdate(producaoagendahistorico);
			}
		}
		
		Motivoaviso motivoaviso = motivoavisoService.findByMotivo(MotivoavisoEnum.AUTORIZAR_SOLICITACAOCOMPRA);
		if(motivoaviso != null && Boolean.TRUE.equals(motivoaviso.getEmail())){
			StringBuilder whereIn = new StringBuilder();
			for(Solicitacaocompra solicitacaocompra : lista){
				if(solicitacaocompra.getCdsolicitacaocompra() != null){
					if(!"".equals(whereIn.toString())) whereIn.append(",");
					whereIn.append(solicitacaocompra.getCdsolicitacaocompra());
				}
			}
			
			StringBuilder notWhereInCdusuario = new StringBuilder();
			for (Usuario u : usuarioList) {
				if(!"".equals(notWhereInCdusuario.toString())) {
					notWhereInCdusuario.append(",");
				}
				notWhereInCdusuario.append(u.getCdpessoa());
			}
			
			if(!"".equals(whereIn.toString())){
				try {
					this.enviarEmailSolicitacaocompra(whereIn.toString(), Situacaosuprimentos.EM_ABERTO, notWhereInCdusuario.toString());
				} catch (Exception e) {}
			}
		}
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Integer getUltimoIdentificador() {
		return solicitacaocompraDAO.getUltimoIdentificador();
	}
	
	/**
	 * M�todo que seta valores quando o usu�rio est� sendo redirecionado para a tela de cria��o
	 * 
	 * @see #getUltimoIdentificador()
	 * @param form
	 * @author Tom�s Rabelo
	 */
	public void preparaCriacaoSolicitacao(Solicitacaocompra form) {
		Solicitacaocompraorigem solicitacaocompraorigem = new Solicitacaocompraorigem();
		solicitacaocompraorigem.setUsuario((Usuario)Neo.getUser());
		form.setSolicitacaocompraorigem(solicitacaocompraorigem);
		
		if(form.getSolicitacoes() == null || form.getSolicitacoes().size() == 0){
			List<Solicitacaocompra> solicitacoes = new ListSet<Solicitacaocompra>(Solicitacaocompra.class);
			solicitacoes.add(new Solicitacaocompra());
			form.setSolicitacoes(solicitacoes);
		}
	}
	
	/**
	 * M�todo que cria solicita��es a partir das requisi��es
	 * 
	 * @see #getUltimoIdentificador()
	 * @param requisicoesGerarCompra
	 * @return
	 * @author Tom�s Rabelo
	 * @param request 
	 */
	public List<Solicitacaocompra> preparaSolicitacoes(WebRequestContext request, List<Requisicaomaterial> requisicoes) {
		List<Solicitacaocompra> solicitacoes = new ListSet<Solicitacaocompra>(Solicitacaocompra.class);
		Integer identificador = getUltimoIdentificador();
		Colaborador colaborador = SinedUtil.getUsuarioComoColaborador();
		
		List<Requisicaomaterial> listaRequisicaomaterialQtdeZerada = new ArrayList<Requisicaomaterial>();
		for (Requisicaomaterial requisicaomaterial : requisicoes) {
			Double qtdeRomaneio = romaneioitemService.getQtdeRomaneioByRequisicaomaterial(requisicaomaterial); 
			Double qtde = requisicaomaterial.getQtde() - (qtdeRomaneio != null ? qtdeRomaneio : 0d);
			if(requisicaomaterial.getDtbaixa() != null && requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque() != null && 
					requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque().getQtde() != null){
				qtde = qtde - requisicaomaterial.getVrequisicaomaterialmovimentacaoestoque().getQtde();
				
			}
			if(qtde != null && qtde > 0){
				Solicitacaocompra solicitacaocompra = new Solicitacaocompra(requisicaomaterial.getDescricao(), requisicaomaterial.getDtlimite(),  requisicaomaterial.getProjeto(), requisicaomaterial.getDepartamento(), 
						requisicaomaterial.getLocalarmazenagem(), requisicaomaterial.getCentrocusto(), requisicaomaterial.getEmpresa(), requisicaomaterial.getColaborador(), requisicaomaterial.getMaterial(), requisicaomaterial.getMaterialclasse(),
						qtde, new Solicitacaocompraorigem(requisicaomaterial), identificador != null ? identificador : 0, Boolean.FALSE, requisicaomaterial.getObservacao(), requisicaomaterial.getFrequencia(), 
						requisicaomaterial.getQtdefrequencia(), requisicaomaterial.getIdentificador(), requisicaomaterial.getAltura(), requisicaomaterial.getLargura(), requisicaomaterial.getComprimento(), requisicaomaterial.getPesototal(), requisicaomaterial.getQtdvolume());
				solicitacaocompra.setUnidademedida(requisicaomaterial.getMaterial().getUnidademedida());
				solicitacaocompra.setDtsolicitacao(new java.sql.Date(System.currentTimeMillis()));
				if(colaborador != null) solicitacaocompra.setColaborador(colaborador);
	
				Set<Materialunidademedida> listaMaterialunidademedida = requisicaomaterial.getMaterial().getListaMaterialunidademedida();
				if(listaMaterialunidademedida != null && listaMaterialunidademedida.size() > 0){
					for (Materialunidademedida materialunidademedida : listaMaterialunidademedida) {
						if(materialunidademedida.getPrioridadecompra() != null && materialunidademedida.getPrioridadecompra()){
							solicitacaocompra.setQtdvolume(solicitacaocompra.getQtde());
	
							Double fracao = materialunidademedida.getFracaoQtdereferencia();
							Double qtdeNova = solicitacaocompra.getQtde() / fracao;
							
							solicitacaocompra.setQtde(qtdeNova);
							solicitacaocompra.setUnidademedida(materialunidademedida.getUnidademedida());
							
							break;
						}
					}
				}
				
				boolean addPesoCampoQtde = false;
				Material material = materialService.load(solicitacaocompra.getMaterial(), "material.cdmaterial, material.materialtipo, material.peso, material.pesobruto");
				if(material == null){
					material = materialService.load(solicitacaocompra.getMaterial(), "material.cdmaterial, material.peso, material.pesobruto");
				}
				material.setListaMaterialformulapeso(materialformulapesoService.findByMaterial(material));
				
				Double peso = material.getPeso() != null ? material.getPeso() : material.getPesobruto();
				if(material.getListaMaterialformulapeso() != null && material.getListaMaterialformulapeso().size() > 0){
				
					Double altura = solicitacaocompra.getAltura() != null ? solicitacaocompra.getAltura() : 0d;
					Double comprimento = solicitacaocompra.getComprimento() != null ? solicitacaocompra.getComprimento() : 0d;
					Double largura = solicitacaocompra.getLargura() != null ? solicitacaocompra.getLargura() : 0d;
					Double qtdvolume = solicitacaocompra.getQtdvolume() != null ? solicitacaocompra.getQtdvolume() : 0d;
					
					material.setProduto_altura(altura);
					material.setProduto_comprimento(comprimento);
					material.setProduto_largura(largura);
					material.setQtdvolume(qtdvolume);
					
					Material m = materialService.getSimboloUnidadeMedidaETempoEntrega(solicitacaocompra.getMaterial());
					Unidademedida unidademedida = m != null ? unidademedida = m.getUnidademedida() : null;
					if(solicitacaocompra.getUnidademedida() != null && solicitacaocompra.getUnidademedida().getCdunidademedida() != null){
						solicitacaocompra.setUnidademedida(unidademedidaService.load(solicitacaocompra.getUnidademedida(), 
								"unidademedida.cdunidademedida, unidademedida.nome, unidademedida.simbolo"));
						unidademedida = solicitacaocompra.getUnidademedida();
					}
					
					try {
						peso = materialformulapesoService.calculaPesoMaterial(material);
					} catch (ScriptException e) {
						peso = 0d;
						NeoWeb.getRequestContext().addError("Erro no c�lculo do peso do material: " + e.getMessage());
						e.printStackTrace();
					}
					
					if(peso != null && 
							peso > 0 && 
							unidademedida != null && 
							unidademedida.getSimbolo() != null &&
							"KG".equalsIgnoreCase(unidademedida.getSimbolo())){
						addPesoCampoQtde = true;
					}
				}
				
				if(peso != null && !addPesoCampoQtde){
					peso = peso * qtde;
				}
				
				if(addPesoCampoQtde){
					solicitacaocompra.setQtde(peso);
				}
				solicitacaocompra.setPesototal(peso);
				
				solicitacoes.add(solicitacaocompra);
			}else {
				listaRequisicaomaterialQtdeZerada.add(requisicaomaterial);
			}
		}
		
		request.setAttribute("listaRequisicaomaterialQtdeZerada", listaRequisicaomaterialQtdeZerada);
		return solicitacoes;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#callProcedureAtualizaSolicitacaocompra
	 * @param solicitacaocompra
	 * @author Rodrigo Freitas
	 */
	public void callProcedureAtualizaSolicitacaocompra(Solicitacaocompra solicitacaocompra){
		solicitacaocompraDAO.callProcedureAtualizaSolicitacaocompra(solicitacaocompra);
	}
	
	/**
	 * Chama a procedure para um whereIn passado.
	 *
	 * @param whereIn
	 * @author Rodrigo Freitas
	 */
	private void callProcedureAtualizaSolicitacaocompra(String whereIn) {
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Solicitacaocompra solicitacaocompra;
			for (int i = 0; i < ids.length; i++) {
				solicitacaocompra = new Solicitacaocompra(Integer.parseInt(ids[i].trim()));
				this.callProcedureAtualizaSolicitacaocompra(solicitacaocompra);
			}
		}
	}
	
	/**
	 * Chama procedure do banco para atualizar situa��o das solicita��es
	 * 
	 * @see #callProcedureAtualizaSolicitacaocompra(Solicitacaocompra)
	 * @param solicitacoes
	 * @author Tom�s Rabelo
	 */
	public void atualizaSituacaoSolicitacoes(List<Solicitacaocompra> solicitacoes) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes) 
			callProcedureAtualizaSolicitacaocompra(solicitacaocompra);
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) solicita��o(�es) para autorizada
	 * 
	 * @see br.com.linkcom.sined.geral.service.PapelService#carregaPapeisPermissaoAcao(String)
	 * @see #montaAvisosSolicitacaoCompra(List, List, String)
	 * @see #geraAvisoAtualizaSolicitacoes(List, String, Situacaosuprimentos)
	 * @see br.com.linkcom.sined.geral.service.MaterialgrupoService#enviaEmailGrupoMaterial(List)
	 * @param solicitacoes
	 * @param whereIn
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean autorizar(List<Solicitacaocompra> solicitacoes, String whereIn, BindException errors, WebRequestContext request) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes){
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(AUTORIZADA) || 
			   solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_PROCESSOCOMPRA) || 
			   solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("025", "Solicita��o de compra j� foi autorizada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("026", "Autoriza��o n�o permitida. A solicita��o de compra est� cancelada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(NAO_AUTORIZADA)){
				errors.reject("026", "Autoriza��o n�o permitida. A solicita��o de compra est� n�o autorizada.");
				return false;
			} 
			
			solicitacaocompra.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra, Solicitacaocompraacao.AUTORIZADA));
		}
		
//		doUpdateSituacaoSolicitacoes(whereIn, AUTORIZADA);
		List<Aviso> listaAvisos = montaAvisosSolicitacaoCompra(MotivoavisoEnum.GERAR_COTACAO, solicitacoes, "Aguardando processo de compra");
//		try{
//			materialgrupoService.enviaEmailGrupoMaterial(solicitacoes);
//		} catch (Exception e) {
//			e.printStackTrace();
//			request.addError(e.getMessage());
//		}
		geraAvisoAtualizaSolicitacoes(listaAvisos, solicitacoes, AUTORIZADA);
		return true;
	}
	
	public boolean naoautorizar(List<Solicitacaocompra> solicitacoes, BindException errors) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes){
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(AUTORIZADA) || 
					solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_PROCESSOCOMPRA) || 
					solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("025", "Solicita��o de compra j� foi autorizada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("026", "Autoriza��o n�o permitida. A solicita��o de compra est� cancelada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(NAO_AUTORIZADA)){
				errors.reject("026", "Solicita��o de compra j� foi n�o autorizada.");
				return false;
			}
			
			solicitacaocompra.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra, Solicitacaocompraacao.AUTORIZADA));
		}
		
		return true;
	}
	
	/**
	 * Este m�todo salva a lista de avisos gerais e muda a situa��o da solicita��o.
	 * Obs: ele n�o esta em uma transaction pois o Update de situa��o n�o pode fica em transaction.
	 * 
	 * @see br.com.linkcom.sined.geral.service.AvisoService#saveOrUpdateNoUseTransaction(Aviso)
	 * @see #doUpdateSituacaoSolicitacoes(String, Situacaosuprimentos)
	 * @param listaAvisos
	 * @param solicitacoes
	 * @param situacao
	 * @author Tom�s Rabelo
	 */
	public void geraAvisoAtualizaSolicitacoes(final List<Aviso> listaAvisos, final List<Solicitacaocompra> solicitacoes, Situacaosuprimentos situacao) {
		final boolean cancelada = situacao != null && Situacaosuprimentos.CANCELADA.equals(situacao);
		final boolean autorizada = Situacaosuprimentos.AUTORIZADA.equals(situacao);
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				if(listaAvisos != null && listaAvisos.size() > 0 && autorizada){
					avisoService.salvarAvisos(listaAvisos, false);
				}
				
				for (Solicitacaocompra solicitacaocompra : solicitacoes) {
					if(cancelada && solicitacaocompra.getSolicitacaocompraorigem() != null && solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral() != null){
						solicitacaocompraorigemService.removeVinculoOrigemRecustogeral(solicitacaocompra.getSolicitacaocompraorigem());
						if(solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa() != null &&
								solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getCdtarefa() != null){
							String obsTarefa = "O vinculo com a tarefa (" + 
								solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getCdtarefa() +
								(StringUtils.isNotEmpty(solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getDescricao()) ?
										" - " + solicitacaocompra.getSolicitacaocompraorigem().getTarefarecursogeral().getTarefa().getDescricao() : "") +
								") foi removido. ";
							if(solicitacaocompra.getSolicitacaocomprahistorico() != null){
								if(StringUtils.isNotEmpty(solicitacaocompra.getSolicitacaocomprahistorico().getObservacao())){
									obsTarefa = solicitacaocompra.getSolicitacaocomprahistorico().getObservacao() + ". " + obsTarefa;
								}
							}else {
								Solicitacaocomprahistorico solicitacaocomprahistorico = new Solicitacaocomprahistorico();
								solicitacaocomprahistorico.setSolicitacaocompra(solicitacaocompra);
								solicitacaocomprahistorico.setSolicitacaocompraacao(Solicitacaocompraacao.CANCELADA);
								solicitacaocomprahistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
								try {
									solicitacaocomprahistorico.setCdusuarioaltera(((Pessoa)Neo.getUser()).getCdpessoa());
								} catch (Exception e) {}
								solicitacaocompra.setSolicitacaocomprahistorico(solicitacaocomprahistorico);
							}
							solicitacaocompra.getSolicitacaocomprahistorico().setObservacao(obsTarefa);
						}
					}
					if(solicitacaocompra.getSolicitacaocomprahistorico() != null){
						solicitacaocomprahistoricoService.saveOrUpdateNoUseTransaction(solicitacaocompra.getSolicitacaocomprahistorico());
					}
				}
					
				return status;
			}
		});
		
		doUpdateSituacaoSolicitacoes(CollectionsUtil.listAndConcatenate(solicitacoes, "cdsolicitacaocompra", ","), situacao);
	}
	
	/**
	 * Monta uma lista de avisos gerais
	 * 
	 * @param listaPapeis
	 * @param solicitacoes
	 * @param assunto
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Aviso> montaAvisosSolicitacaoCompra(MotivoavisoEnum motivo, List<Solicitacaocompra> solicitacoes, String assunto) {
		List<Aviso> avisos = new ListSet<Aviso>(Aviso.class);
		Motivoaviso motivoAviso = motivoavisoService.findByMotivo(motivo);
		
		if(motivoAviso != null){
			for (Solicitacaocompra solicitacaocompra : solicitacoes) {
				if(motivoAviso.getTipoaviso() != null){
					avisos.add(new Aviso(assunto, "Solicita��o: "+solicitacaocompra.getCdsolicitacaocompra(), motivoAviso.getTipoaviso(), motivoAviso.getPapel(), 
							motivoAviso.getUsuario(), AvisoOrigem.SOLICITACAO_DE_COMPRA, solicitacaocompra.getCdsolicitacaocompra(), solicitacaocompra.getEmpresa(), 
							solicitacaocompra.getProjeto() != null && solicitacaocompra.getProjeto().getCdprojeto() != null ? solicitacaocompra.getProjeto().getCdprojeto().toString() : null, 
									motivoAviso));
				}
			}
		}
		
		return avisos;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) solicita��o(�es) para cancelado
	 * 
	 * @see #doUpdateSituacaoSolicitacoes(String, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param solicitacoes
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean cancelar(List<Solicitacaocompra> solicitacoes, BindException errors) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes){
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(AUTORIZADA)){
				errors.reject("027", "Para cancelar, a solicita��o deve ser estornada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_PROCESSOCOMPRA)){
				errors.reject("028", "Cancelamento n�o permitido. A solicita��o de compra est� em processo de compra.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("029", "Cancelamento n�o permitido. A solicita��o de compra baixada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("030", "Solicita��o de compra j� est� cancelada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(NAO_AUTORIZADA)){
				errors.reject("027", "Para cancelar, a solicita��o deve ser estornada.");
				return false;
			}
			
		}
		
//		doUpdateSituacaoSolicitacoes(whereIn, CANCELADA);
//		geraAvisoAtualizaSolicitacoes(null, solicitacoes, CANCELADA);
		return true;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) solicita��o(�es) para em aberto
	 * 
	 * @see #existeSolicitacaoGeradaRequisicaoDiferenteDeCancelada(Solicitacaocompra)
	 * @see #doUpdateSituacaoSolicitacoes(String, br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * 
	 * @param solicitacoes
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean estornar(List<Solicitacaocompra> solicitacoes, BindException errors) {
		HashMap<Projeto, List<Solicitacaocompra>> mapProjetoSolicitacaocompra = new HashMap<Projeto, List<Solicitacaocompra>>();
		for (Solicitacaocompra solicitacaocompra : solicitacoes){
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_ABERTO)){
				errors.reject("031", "Solicita��o de compra j� est� estornada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_PROCESSOCOMPRA)){
				errors.reject("032", "Estorno n�o permitido. A solicita��o de compra est� em processo de compra.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(BAIXADA) &&
			   solicitacaocompra.getDtbaixa() == null){
				errors.reject("033", "Estorno n�o permitido. S� � permitido o estorno de uma solicita��o de compra baixada manualmente.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(CANCELADA)){
				if(solicitacaocompra.getSolicitacaocompraorigem().getRequisicaomaterial() != null)
					if(existeSolicitacaoGeradaRequisicaoDiferenteDeCancelada(solicitacaocompra)){
						errors.reject("034", "Estorno n�o permitido. H� solicita��o de compra em execu��o para  a mesma requisi��o de material.");
						return false;
					}
			}
			
			if(solicitacaocompra.getProjeto() != null){
				if(mapProjetoSolicitacaocompra.get(solicitacaocompra.getProjeto()) == null){
					mapProjetoSolicitacaocompra.put(solicitacaocompra.getProjeto(), new ArrayList<Solicitacaocompra>());
				}
				mapProjetoSolicitacaocompra.get(solicitacaocompra.getProjeto()).add(solicitacaocompra);
			}
		}
		
		if(mapProjetoSolicitacaocompra != null && mapProjetoSolicitacaocompra.size() > 0){
			for(Projeto projeto : mapProjetoSolicitacaocompra.keySet()){
				validaQtdeCompraMaiorQuePlanejamento(mapProjetoSolicitacaocompra.get(projeto), projeto, errors);
				if(errors.hasErrors()){
					return false;
				}
			}
		}
//		doUpdateSituacaoSolicitacoes(whereIn, ESTORNAR);
		return true;
	}
	
	/**
	 * M�todo que valida e muda a situa��o da(s) solicita��o(�es) para baixada
	 * 
	 * @see #doUpdateSituacaoSolicitacoes(br.com.linkcom.sined.geral.bean.Situacaosuprimentos)
	 * @param solicitacoes
	 * @param whereIn
	 * @param errors
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean baixar(List<Solicitacaocompra> solicitacoes,	BindException errors) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes){
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(EM_PROCESSOCOMPRA)){
				errors.reject("035","Solicita��o de compra deve aguardar a baixa da cota��o.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(BAIXADA)){
				errors.reject("036","Solicita��o de compra j� est� baixada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(CANCELADA)){
				errors.reject("037","Baixa n�o permitida. A solicita��o de compra est� cancelada.");
				return false;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(NAO_AUTORIZADA)){
				errors.reject("037","Baixa n�o permitida. A solicita��o de compra est� n�o autorizada.");
				return false;
			}
			
			//VWEIFICAREAPAGARsolicitacaocompra.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra, Solicitacaocompraacao.BAIXADA));
		}
		
		
//		doUpdateSituacaoSolicitacoes(whereIn, BAIXADA);
		//VERIFICAREAPAGARgeraAvisoAtualizaSolicitacoes(null, solicitacoes, BAIXADA);
		return true;
	}
	
	/**
	 * M�todo que verifica se alguma solicita��o � diferente de autorizada. Esta fun��o � chamada quando esta gerando cota��o ou 
	 * quando se quer inserir uma solicita��o de compra em uma cota��o
	 * 
	 * @param solicitacoes
	 * @param errors
	 * @param from
	 * @return
	 * @author Tom�s Rabelo
	 */
	public boolean isSolicitacoesDiferenteDeAutorizada(List<Solicitacaocompra> solicitacoes, BindException errors, int from) {
		Boolean permiteAutorizarSolicitacaocompra = usuarioService.permiteAcao(SinedUtil.getUsuarioLogado(), "AUTORIZAR_SOLICITACAOCOMPRA");
		
		for (Solicitacaocompra solicitacaocompra : solicitacoes) {
			if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_ABERTO) && !permiteAutorizarSolicitacaocompra){
				if(from == FluxoSituacaoSolicitacaoProcess.GERARCOTACAO){
					errors.reject("038", "Para gerar a cota��o, a solicita��o de compra deve estar autorizada.");
				} else{
					errors.reject("039", "Para inserir na cota��o, a solicita��o de compra deve estar autorizada.");
				}
				return true;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.EM_PROCESSOCOMPRA)){
				errors.reject("040", "Solicita��o de compra j� est� em processo de compra.");
				return true;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.BAIXADA)){
				errors.reject("041", "Solicita��o de compra j� foi realizada.");
				return true;
			} else if(solicitacaocompra.getAux_solicitacaocompra().getSituacaosuprimentos().equals(Situacaosuprimentos.CANCELADA)){
				errors.reject("042", "Gera��o de cota��o n�o permitida. A solicita��o de compra est� cancelada.");
				return true;
			} 
		}
		
		return false;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#haveSolicitacao
	 * @param orcamentomaterial
	 * @return
	 * @author Rodrigo Freitas
	 */
	public boolean haveSolicitacao(Orcamentomaterial orcamentomaterial) {
		return solicitacaocompraDAO.haveSolicitacao(orcamentomaterial);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param solicitacaocompra
	 * @param situacaosuprimentos
	 * @author Tom�s Rabelo
	 * @param coluna 
	 */
	public void doUpdateCompraSolicitacao(Solicitacaocompra solicitacaocompra, Integer coluna) {
		solicitacaocompraDAO.doUpdateCompraSolicitacao(solicitacaocompra, coluna);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param localarmazenagemorigem
	 * @param localarmazenagemdestino
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesParaRomaneio(Localarmazenagem localarmazenagemorigem,	Localarmazenagem localarmazenagemdestino) {
		return solicitacaocompraDAO.getSolicitacoesParaRomaneio(localarmazenagemorigem, localarmazenagemdestino);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereInEntrega
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Solicitacaocompra> getSolicitacoesDasEntregas(String whereInEntrega) {
		return solicitacaocompraDAO.getSolicitacoesDasEntregas(whereInEntrega);
	}
	
	/**
	 * M�todo que prepara historico das solicita��es colocando observa��o
	 * 
	 * @param solicitacoes
	 * @param solicitacaocompraacao
	 * @param solicitacaocomprahistorico
	 * @author Tom�s Rabelo
	 */
	public void preparaSolicitacaocomprahistorico(List<Solicitacaocompra> solicitacoes,	Solicitacaocompraacao solicitacaocompraacao, Solicitacaocomprahistorico solicitacaocomprahistorico) {
		for (Solicitacaocompra solicitacaocompra : solicitacoes)
			solicitacaocompra.setSolicitacaocomprahistorico(new Solicitacaocomprahistorico(solicitacaocompra, solicitacaocompraacao, solicitacaocomprahistorico.getObservacao()));
	}
	
	
	public Resource emitirSolicitacaoCompra(String whereIn, Boolean mesmaSituacao) throws Exception {
		MergeReport mergeReport = new MergeReport("solicitacaocompra_"+SinedUtil.datePatternForReport()+".pdf");
		
		Integer identificador = 0;
		String[] ids = whereIn.split(",");
		if (ids != null && ids.length > 0) {
			Solicitacaocompra solicitacaocompra;
			for (int i = 0; i < ids.length; i++) {
				solicitacaocompra = new Solicitacaocompra(Integer.parseInt(ids[i].trim()));
				solicitacaocompra = solicitacaocompraDAO.load(solicitacaocompra);
				if(!identificador.equals(solicitacaocompra.getIdentificador())){
					identificador = solicitacaocompra.getIdentificador();
					
					Report report = new Report("/suprimento/emissaosolicitacaocompra");
					Report subreport1 = new Report("suprimento/sub_emissaosolicitacaocompra_assinatura");
					
					report.addSubReport("SUB_EMISSAOSOLICITACAOCOMPRA", subreport1);
					
					List<Solicitacaocompra> solicitacao = solicitacaocompraDAO.findSolicitacaoCompra(solicitacaocompra.getIdentificador().toString(), solicitacaocompra.getCdsolicitacaocompra(), mesmaSituacao);
					
					report.setDataSource(solicitacao);
					report.addParameter("identificador", SinedUtil.isListNotEmpty(solicitacao) ? solicitacao.get(0).getIdentificador().toString() : "");
					report.addParameter("descricao", SinedUtil.isListNotEmpty(solicitacao) ? solicitacao.get(0).getDescricao() : "");
					report.addParameter("OBSERVACAO", getObservacaoImpressao(solicitacao));
					
					report.addParameter("TITULO", "SOLICITA��O DE COMPRA");
					report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
					report.addParameter("DATA", new Date(System.currentTimeMillis()));
					
					Empresa empresaPrincipal = empresaService.loadPrincipal();
					if(SinedUtil.isListNotEmpty(solicitacao) && solicitacao.get(0).getEmpresa() != null){
						
						Image image = SinedUtil.getLogo(solicitacao.get(0).getEmpresa());
						report.addParameter("LOGO", image);
						report.addParameter("EMPRESA", solicitacao.get(0).getEmpresa().getRazaosocialOuNome());
						
					} else if(empresaPrincipal != null){
						
						Image image = SinedUtil.getLogo(empresaPrincipal);
						report.addParameter("LOGO", image);
						report.addParameter("EMPRESA", empresaPrincipal.getRazaosocialOuNome());
						
					}
					
					Image imageRubricaElaboracao = null;
					Usuario usuarioElaboracao = null;
					String nomeUsuarioElaboracao = "";
					Date dtalteraElaborado = null;
					
					Solicitacaocomprahistorico oh1 = null;
					Solicitacaocomprahistorico oh2 = null;
					
					if(SinedUtil.isListNotEmpty(solicitacao)) {
						for (Solicitacaocompra soc : solicitacao) {
							oh1 = solicitacaocomprahistoricoService.getUsuarioLastAction(soc, Solicitacaocompraacao.CRIADA);
							oh2 = solicitacaocomprahistoricoService.getUsuarioLastAction(soc, Solicitacaocompraacao.AUTORIZADA);
							if(oh1 != null && oh2 != null){
								break;
							}
						}
					}
//					Solicitacaocomprahistorico oh1 = solicitacaocomprahistoricoService.getUsuarioLastAction(solicitacao.get(0), Solicitacaocompraacao.CRIADA);
					
					Image imageRubricaAutorizacao = null;
					Usuario usuarioAutorizacao = null;
					String nomeUsuarioAutorizacao = "";
					Date dtalteraAutorizado = null;
//					Solicitacaocomprahistorico oh2 = solicitacaocomprahistoricoService.getUsuarioLastAction(solicitacao.get(0), Solicitacaocompraacao.AUTORIZADA);
					
					try{
						if(oh1 != null && oh1.getCdusuarioaltera() != null){
							usuarioElaboracao = usuarioService.carregaRubricaUsuario(new Usuario(oh1.getCdusuarioaltera()));
							if(usuarioElaboracao != null && usuarioElaboracao.getRubrica() != null){
								Arquivo rubrica = usuarioElaboracao.getRubrica();
								rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
								imageRubricaElaboracao = ArquivoService.getInstance().loadAsImage(rubrica);
							}
							nomeUsuarioElaboracao = usuarioService.load(new Usuario(oh1.getCdusuarioaltera())).getNome();
							dtalteraElaborado = oh1.getDtaltera();
						}
						
						if(oh2 != null && oh2.getCdusuarioaltera() != null){
							usuarioAutorizacao = usuarioService.carregaRubricaUsuario(new Usuario(oh2.getCdusuarioaltera()));
							if(usuarioAutorizacao != null && usuarioAutorizacao.getRubrica() != null){
								Arquivo rubrica = usuarioAutorizacao.getRubrica();
								rubrica = ArquivoService.getInstance().loadWithContents(rubrica);
								imageRubricaAutorizacao = ArquivoService.getInstance().loadAsImage(rubrica);
							}
							nomeUsuarioAutorizacao = usuarioService.load(new Usuario(oh2.getCdusuarioaltera())).getNome();
							dtalteraAutorizado = oh2.getDtaltera();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					report.addParameter("USUARIOELEBORADO", nomeUsuarioElaboracao);
					report.addParameter("UAUARIOAUTORIZADO", nomeUsuarioAutorizacao);
					report.addParameter("DTALTERAELABORADO", dtalteraElaborado == null ? "" : new SimpleDateFormat("dd/MM/yyyy hh:mm").format(dtalteraElaborado));
					report.addParameter("DTALTERAAUTORIZADO", dtalteraAutorizado == null ? "" : new SimpleDateFormat("dd/MM/yyyy hh:mm").format(dtalteraAutorizado));
					report.addParameter("USUARIO_RUBRICA_ELABORACAO", imageRubricaElaboracao);
					report.addParameter("USUARIO_RUBRICA_AUTORIZACAO", imageRubricaAutorizacao);
					
					mergeReport.addReport(report);
				}
			}
		}
		
//		Solicitacaocompra solicitacaocompra = new Solicitacaocompra(Integer.parseInt(whereIn));
		
		return mergeReport.generateResource();
	}
	
	private String getObservacaoImpressao(List<Solicitacaocompra> listaSolicitacaocompra) {
		StringBuilder sb = new StringBuilder();
		if(SinedUtil.isListNotEmptyAndInitialized(listaSolicitacaocompra)){
			for(Solicitacaocompra sc : listaSolicitacaocompra){
				new br.com.linkcom.neo.util.StringUtils().append(sb, sc.getObservacao(), " | ", true);
			}
		}
		return sb.toString();
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param bean
	 * @return
	 * @ayuthor Tom�s Rabelo
	 */
	public Boolean existeSolicitacaoComSituacaoDiferente(Solicitacaocompra solicitacaocompra) {
		return solicitacaocompraDAO.existeSolicitacaoComSituacaoDiferente(solicitacaocompra);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForEnvioemailcompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForEnvioemailcompra(String whereIn) {
		return solicitacaocompraDAO.findForEnvioemailcompra(whereIn);
	}
	
	/**
	 * M�todo que envia email de acordo com o processo de compra
	 *
	 * @param whereIn
	 * @param situacao
	 * @author Luiz Fernando
	 */
	public void enviarEmailSolicitacaocompra(String whereIn, Situacaosuprimentos situacao, String notWhereInCdusuario) {
		if(whereIn != null && !"".equals(whereIn)){
			List<Solicitacaocompra> listaSolicitacoes = this.findForEnvioemailcompra(whereIn);
			String assunto = "";
			if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
				assunto = "Solicita��o Autorizada";
			}else if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
				assunto = "Solicita��o criada";
			}
			if(listaSolicitacoes != null && !listaSolicitacoes.isEmpty()){
				StringBuilder whereInCdusuario;
				Materialgrupo materialgrupo;
				for(Solicitacaocompra solicitacaocompra : listaSolicitacoes){
					whereInCdusuario = new StringBuilder();
					if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
						materialgrupo = null;
						if(solicitacaocompra.getMaterialgrupo() != null){
							materialgrupo = solicitacaocompra.getMaterialgrupo();
						}else if(solicitacaocompra.getMaterial() != null){
							materialgrupo = solicitacaocompra.getMaterial().getMaterialgrupo();
						}						
						if(materialgrupo != null && materialgrupo.getListaMaterialgrupousuario() != null &&
								!materialgrupo.getListaMaterialgrupousuario().isEmpty()){
							for(Materialgrupousuario materialgrupousuario : materialgrupo.getListaMaterialgrupousuario()){
								if(materialgrupousuario.getUsuario() != null && materialgrupousuario.getUsuario().getCdpessoa() != null){
									if(!"".equals(whereInCdusuario.toString())) whereInCdusuario.append(",");
									whereInCdusuario.append(materialgrupousuario.getUsuario().getCdpessoa());
								}
							}
						}
					}
					
					List<Usuario> listaUsuario  = null;
					if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.AUTORIZAR_SOLICITACAOCOMPRA, solicitacaocompra.getEmpresa(), 
								solicitacaocompra.getProjeto() != null && solicitacaocompra.getProjeto().getCdprojeto() != null ? solicitacaocompra.getProjeto().getCdprojeto().toString() : null, null, notWhereInCdusuario);
					}else if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
						listaUsuario = usuarioService.findForEnvioemailcompra(Acao.GERARCOTACAO_SOLICITACAOCOMPRA, solicitacaocompra.getEmpresa(), 
								solicitacaocompra.getProjeto() != null && solicitacaocompra.getProjeto().getCdprojeto() != null ? solicitacaocompra.getProjeto().getCdprojeto().toString() : null,
								!"".equals(whereInCdusuario.toString()) ? whereInCdusuario.toString() : null, notWhereInCdusuario);
					}
					
					
					if(listaUsuario != null && !listaUsuario.isEmpty()){
						for(Usuario usuario : listaUsuario){
							if(usuario.getEmail() != null && !usuario.getEmail().equals("")){
								EmailManager email;							
								try {
									email = new EmailManager(ParametrogeralService.getInstance().getValorPorNome(Parametrogeral.EMAIL_SERVER_JNDINAME));
									email
										.setFrom("w3erp@w3erp.com.br")
										.setSubject(assunto)
										.setTo(usuario.getEmail());
									
									if(situacao.equals(Situacaosuprimentos.AUTORIZADA)){
										email.addHtmlText("Aviso do sistema: A solicita��o " + solicitacaocompra.getIdentificador() +" foi autorizada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext() 
												+ usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}else if(situacao.equals(Situacaosuprimentos.EM_ABERTO)){
										email.addHtmlText("Aviso do sistema: A solicita��o " + solicitacaocompra.getIdentificador() +" foi criada. <BR><BR>URL do sistema: " + SinedUtil.getUrlWithContext() 
												+ usuarioService.addImagemassinaturaemailUsuario(SinedUtil.getUsuarioLogado(), email));
									}
									
									email.sendMessage();								
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
						
					}
				}
			}
		}	
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#existSolicitacaoDiferenteEmAberto(Solicitacaocompra solicitacaocompra)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existSolicitacaoDiferenteEmAberto(String whereIn) {
		return solicitacaocompraDAO.existSolicitacaoDiferenteEmAberto(whereIn);
	}
	
	/**
	 * M�todo que retorna a lista de solicita��o com os materiais do pedido de venda
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> getListaMaterialByPedidovenda(Pedidovenda pedidovenda) {
		List<Solicitacaocompra> solicitacoes = new ArrayList<Solicitacaocompra>();
		if(pedidovenda != null && pedidovenda.getListaPedidovendamaterial() != null && !pedidovenda.getListaPedidovendamaterial().isEmpty()){
			Solicitacaocompra solicitacaocompra;
			for(Pedidovendamaterial pedidovendamaterial : pedidovenda.getListaPedidovendamaterial()){
				solicitacaocompra = new Solicitacaocompra();
				solicitacaocompra.setCdpedidovenda(pedidovenda.getCdpedidovenda());
				solicitacaocompra.setMaterial(pedidovendamaterial.getMaterial());
				solicitacaocompra.setQtde(pedidovendamaterial.getQuantidade());
				solicitacaocompra.setUnidademedida(pedidovendamaterial.getUnidademedida());
				if(Util.strings.emptyIfNull(pedidovendamaterial.getObservacao()).length() > 100){
					solicitacaocompra.setObservacao(pedidovendamaterial.getObservacao().substring(0, 100));
				}else{
					solicitacaocompra.setObservacao(pedidovendamaterial.getObservacao());
				}
				if(pedidovendamaterial.getMaterial() != null){
					if(pedidovendamaterial.getMaterial().getProduto() != null && pedidovendamaterial.getMaterial().getProduto()){
						solicitacaocompra.setMaterialclasse(Materialclasse.PRODUTO);
					}else if(pedidovendamaterial.getMaterial().getPatrimonio() != null && pedidovendamaterial.getMaterial().getPatrimonio()){
						solicitacaocompra.setMaterialclasse(Materialclasse.PATRIMONIO);
					}else if(pedidovendamaterial.getMaterial().getEpi() != null && pedidovendamaterial.getMaterial().getEpi()){
						solicitacaocompra.setMaterialclasse(Materialclasse.EPI);
					}else {
						solicitacaocompra = null;
					}
				}
				if(solicitacaocompra != null){
					solicitacoes.add(solicitacaocompra);
				}
			}
		
		}
		return solicitacoes;
	}
	
	/**
	 * M�todo que retorna a lista de solicita��o com os materiais do or�amento
	 *
	 * @param vendaorcamento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> getListaMaterialByOrcamento(Vendaorcamento vendaorcamento) {
		List<Solicitacaocompra> solicitacoes = new ArrayList<Solicitacaocompra>();
		if(vendaorcamento != null && vendaorcamento.getListavendaorcamentomaterial() != null && !vendaorcamento.getListavendaorcamentomaterial().isEmpty()){
			Solicitacaocompra solicitacaocompra;
			for(Vendaorcamentomaterial vendaorcamentomaterial : vendaorcamento.getListavendaorcamentomaterial()){
				solicitacaocompra = new Solicitacaocompra();
				solicitacaocompra.setCdvendaorcamento(vendaorcamento.getCdvendaorcamento());
				solicitacaocompra.setMaterial(vendaorcamentomaterial.getMaterial());
				solicitacaocompra.setQtde(vendaorcamentomaterial.getQuantidade());
				solicitacaocompra.setUnidademedida(vendaorcamentomaterial.getUnidademedida());
				if(Util.strings.emptyIfNull(vendaorcamentomaterial.getObservacao()).length() > 100){
					solicitacaocompra.setObservacao(vendaorcamentomaterial.getObservacao().substring(0, 100));
				}else{
					solicitacaocompra.setObservacao(vendaorcamentomaterial.getObservacao());
				}
				if(vendaorcamentomaterial.getMaterial() != null){
					if(vendaorcamentomaterial.getMaterial().getProduto() != null && vendaorcamentomaterial.getMaterial().getProduto()){
						solicitacaocompra.setMaterialclasse(Materialclasse.PRODUTO);
					}else if(vendaorcamentomaterial.getMaterial().getPatrimonio() != null && vendaorcamentomaterial.getMaterial().getPatrimonio()){
						solicitacaocompra.setMaterialclasse(Materialclasse.PATRIMONIO);
					}else if(vendaorcamentomaterial.getMaterial().getEpi() != null && vendaorcamentomaterial.getMaterial().getEpi()){
						solicitacaocompra.setMaterialclasse(Materialclasse.EPI);
					}else {
						solicitacaocompra = null;
					}
				}
				if(solicitacaocompra != null){
					solicitacoes.add(solicitacaocompra);
				}
			}
		
		}
		return solicitacoes;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForOrdemcompra(String whereIn)
	 *
	 * @param whereIn
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForOrdemcompra(String whereIn) {
		return solicitacaocompraDAO.findForOrdemcompra(whereIn);
	}
	
	/**
	 * M�todo que retorna a situa��o da solicita��o de compra do pedido de venda
	 *
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#getQteSolicitacaoByPedidovenda(Pedidovenda pedidovenda)
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#getQteSolicitacaoByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Situacaopedidosolicitacaocompra getSituacaoSolcicitacaocompraByPedidovenda(Pedidovenda pedidovenda) {
		Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra = null;
		
		if(pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			Double qtdesolicitacao = this.getQteSolicitacaoByPedidovenda(pedidovenda);
			if(qtdesolicitacao > 0){
				Double qtdebaixada = this.getQteSolicitacaoBaixadaByPedidovenda(pedidovenda);
				if(qtdebaixada == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.SOLICITADO;
				}else if(qtdebaixada < qtdesolicitacao){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_PARCIAL;
				}else if(qtdebaixada.compareTo(qtdesolicitacao) == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_REALIZADA;
				}
			}
		}
		return situacaopedidosolicitacaocompra;
	}
	
	public Situacaopedidosolicitacaocompra getSituacaoSolcicitacaocompraByPedidovenda(Vendaorcamento vendaorcamento) {
		Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra = null;
		
		if(vendaorcamento != null && vendaorcamento.getCdvendaorcamento() != null){
			Double qtdesolicitacao = this.getQteSolicitacaoByOrcamento(vendaorcamento);
			if(qtdesolicitacao > 0){
				Double qtdebaixada = this.getQteSolicitacaoBaixadaByOrcamento(vendaorcamento);
				if(qtdebaixada == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.SOLICITADO;
				}else if(qtdebaixada < qtdesolicitacao){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_PARCIAL;
				}else if(qtdebaixada.compareTo(qtdesolicitacao) == 0){
					situacaopedidosolicitacaocompra = Situacaopedidosolicitacaocompra.COMPRA_REALIZADA;
				}
			}
		}
		return situacaopedidosolicitacaocompra;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#getQteSolicitacaoByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteSolicitacaoByPedidovenda(Pedidovenda pedidovenda) {
		return solicitacaocompraDAO.getQteSolicitacaoByPedidovenda(pedidovenda);
	}
	
	public Double getQteSolicitacaoByOrcamento(Vendaorcamento vendaorcamento) {
		return solicitacaocompraDAO.getQteSolicitacaoByOrcamento(vendaorcamento);
	}

	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#getQteSolicitacaoBaixadaByPedidovenda(Pedidovenda pedidovenda)
	 *
	 * @param pedidovenda
	 * @return
	 * @author Luiz Fernando
	 */
	public Double getQteSolicitacaoBaixadaByPedidovenda(Pedidovenda pedidovenda) {
		return solicitacaocompraDAO.getQteSolicitacaoBaixadaByPedidovenda(pedidovenda);
	}
	
	public Double getQteSolicitacaoBaixadaByOrcamento(Vendaorcamento vendaorcamento) {
		return solicitacaocompraDAO.getQteSolicitacaoBaixadaByOrcamento(vendaorcamento);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#getQtdeRestanteSolicitacaoOrdemcompra(Integer cdsolicitacaocompra)
	 *
	 * @param cdsolicitacaocompra
	 * @return
	 * @author Rodrigo Freitas
	 * @since 20/11/2012
	 */
	public Double getQtdeRestanteSolicitacaoOrdemcompra(Integer cdsolicitacaocompra) {
		Double qtde = null;
		if(cdsolicitacaocompra != null){
			qtde = solicitacaocompraDAO.getQtdeRestanteSolicitacaoOrdemcompra(cdsolicitacaocompra);
			if(qtde != null && qtde > 0){
				Solicitacaocompra solicitacaocompra = loadWithUnidademedida(cdsolicitacaocompra);
				if(solicitacaocompra != null && solicitacaocompra.getUnidademedida() != null && 
						solicitacaocompra.getMaterial() != null && 
						solicitacaocompra.getMaterial().getUnidademedida() != null && 
						!solicitacaocompra.getUnidademedida().equals(solicitacaocompra.getMaterial().getUnidademedida())){
					qtde = unidademedidaService.getQtdeConvertida(solicitacaocompra.getMaterial(), solicitacaocompra.getUnidademedida(), solicitacaocompra.getMaterial().getUnidademedida(), qtde);
				}
			}
		}
		return qtde;
	}
	
	/**
	* M�todo que carrega a solicita��o de compra com a unidade de medida
	*
	* @param cdsolicitacaocompra
	* @return
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public Solicitacaocompra loadWithUnidademedida(Integer cdsolicitacaocompra){
		List<Solicitacaocompra> lista = findWithUnidademedida(cdsolicitacaocompra.toString());
		return SinedUtil.isListNotEmpty(lista) ? lista.get(0) : null;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial)
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<Solicitacaocompra> findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		return solicitacaocompraDAO.findSolicitacaocompraByOrdemcompramaterialComCotacaoForRomaneio(ordemcompramaterial);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial)
	 *
	 * @param ordemcompramaterial
	 * @return
	 * @author Rodrigo Freitas
	 * @since 11/12/2012
	 */
	public List<Solicitacaocompra> findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(Ordemcompramaterial ordemcompramaterial) {
		return solicitacaocompraDAO.findSolicitacaocompraByOrdemcompramaterialSemCotacaoForRomaneio(ordemcompramaterial);
	}
	
	/**
	 * M�todo que veririfica se existe solicita��es de empresas distintas
	 *
	 * @param listaSolicitacaocompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Boolean existEmpresadiferente(List<Solicitacaocompra> listaSolicitacaocompra) {
		Boolean empresadiferente = false;
		Integer cdempresa = null;
		if(listaSolicitacaocompra != null && !listaSolicitacaocompra.isEmpty()){
			for(Solicitacaocompra sc : listaSolicitacaocompra){
				if(sc.getEmpresa() != null && sc.getEmpresa().getCdpessoa() != null){
					if(cdempresa == null){
						cdempresa = sc.getEmpresa().getCdpessoa();
					}else if(cdempresa != sc.getEmpresa().getCdpessoa()){
						empresadiferente = true;
						break;
					}
				}
			}
		}
		
		return empresadiferente;
	}
	
	public Double getQtdeComprada(Solicitacaocompra solicitacaocompra) {
		return solicitacaocompraDAO.getQtdeComprada(solicitacaocompra); 
	}
	
	/**
	 * M�todo que gera o relat�rio de prazo m�dio de compra
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 * @throws Exception 
	 */
	public Resource gerarRelatorioPrazomediocompra(EmitirprazomediocompraFiltro filtro) throws Exception {
		List<Solicitacaocompra> listaSolicitacaocompra = this.findForReportPrazomediocompra(filtro);
		List<PrazomediocompraReportBean> lista = new ArrayList<PrazomediocompraReportBean>();
		PrazomediocompraReportBean prazomediocompraReportBean;
		Empresa empresa = empresaService.loadPrincipal();
		
		if(listaSolicitacaocompra != null && !listaSolicitacaocompra.isEmpty()){
			Date dtbaixaordemcompra = null;
			java.sql.Date dtentrada = null;
			Ordemcomprahistorico ordemcomprahistorico;
			for(Solicitacaocompra sc : listaSolicitacaocompra){
				prazomediocompraReportBean = new PrazomediocompraReportBean();
				
				if(sc.getEmpresa() != null){
					prazomediocompraReportBean.setEmpresa(sc.getEmpresa());
				}else {
					prazomediocompraReportBean.setEmpresa(empresa);
				}
				
				prazomediocompraReportBean.setMaterial(sc.getMaterial());
				prazomediocompraReportBean.setDtsolicitacao(sc.getDtsolicitacao());
				prazomediocompraReportBean.setNomepessoasolicitante(sc.getColaborador() != null ? sc.getColaborador().getNome() : "");
				prazomediocompraReportBean.setDtautorizacao(sc.getDtautorizacao());
				prazomediocompraReportBean.setNomepessoaautorizacao(this.getNomepessoaAutorizacao(sc));
				prazomediocompraReportBean.setDtnecessidadesolicitacao(sc.getDtsolicitacao());
				prazomediocompraReportBean.setTempoentrega(sc.getMaterial().getTempoentrega());
				
				ordemcomprahistorico = this.getDtbaixaOrdemcompra(sc);
				if(ordemcomprahistorico != null){
					dtbaixaordemcompra = ordemcomprahistorico.getDtaltera();
					prazomediocompraReportBean.setNomepessoabaixouordemcompra(ordemcomprahistorico.getResponsavel());
					prazomediocompraReportBean.setDtbaixaordemcompra(dtbaixaordemcompra != null ? new java.sql.Date(dtbaixaordemcompra.getTime()) : null);
				}
				dtentrada = this.getDataEntrada(sc);
				prazomediocompraReportBean.setDtentrada(dtentrada);
				
				if(sc.getMaterial().getTempoentrega() != null && dtentrada != null)
					prazomediocompraReportBean.setPrazolimiteautorizacao(SinedDateUtils.addDiasData(new java.sql.Date(dtentrada.getTime()), -sc.getMaterial().getTempoentrega().intValue()));
				if(prazomediocompraReportBean.getPrazolimiteautorizacao() != null){
					prazomediocompraReportBean.setValorsituacaosolicitacao(SinedDateUtils.diferencaDias(prazomediocompraReportBean.getPrazolimiteautorizacao(), prazomediocompraReportBean.getDtautorizacao()));
				
					if(dtentrada != null){
						prazomediocompraReportBean.setValorcomprarecebidaposprazo(SinedDateUtils.diferencaDias(new java.sql.Date(dtentrada.getTime()), prazomediocompraReportBean.getPrazolimiteautorizacao()));
						prazomediocompraReportBean.setIndiceprazomediocompra(SinedDateUtils.diferencaDias(new java.sql.Date(dtentrada.getTime()), prazomediocompraReportBean.getDtautorizacao()));
					}
				}
				
				prazomediocompraReportBean.setMetaprazomediocompra(sc.getMaterial().getMetaprazomediocompra());
				prazomediocompraReportBean.setToleranciaprazocompra(sc.getMaterial().getToleranciaprazocompra());
				
				if(this.incluir(filtro, prazomediocompraReportBean))
					lista.add(prazomediocompraReportBean);
			}
		}
		
		MergeReport mergeReport = new MergeReport("prazomediocompras_"+SinedUtil.datePatternForReport()+".pdf");
		Report report;
		if(lista != null && !lista.isEmpty()){
			for(PrazomediocompraReportBean bean : lista){
				report = new Report("/suprimento/prazomediocompra");
				
				report.addParameter("LOGO", SinedUtil.getLogo(bean.getEmpresa()));
				report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
				report.addParameter("DATA", new Date(System.currentTimeMillis()));
				report.addParameter("TITULO", "Relat�rio - Prazo m�dio de compra");
				if(bean.getEmpresa() != null)
					report.addParameter("EMPRESA", bean.getEmpresa().getRazaosocialOuNome());
				
				report.addParameter("MATERIAL",bean.getMaterial().getNome());
				report.addParameter("DATASOLICITACAO",bean.getDtsolicitacao());
				report.addParameter("SOLICITANTE",bean.getNomepessoasolicitante());
				report.addParameter("DATAAUTORIZACAO",bean.getDtautorizacao());
				report.addParameter("AUTORIZADOPOR",bean.getNomepessoaautorizacao());
				report.addParameter("DATANECESSIDADE",bean.getDtnecessidadesolicitacao());
				report.addParameter("DATAORDEMCOMPRABAIXADA",bean.getDtbaixaordemcompra());
				report.addParameter("BAIXADAPOR",bean.getNomepessoabaixouordemcompra());
				report.addParameter("DATAENTRADA",bean.getDtentrada());
				report.addParameter("TEMPOENTREGA",bean.getTempoentrega());
				report.addParameter("PRAZOLIMITEAUTORIZACAO",bean.getPrazolimiteautorizacao());
				report.addParameter("SITUACAOSOLICITACAO",bean.getSituacaosolicitacao());
				report.addParameter("COMPRARECEBIDAPOSPRAZO",bean.getValorcomprarecebidaposprazo());
				report.addParameter("INDICEPRAZOMEDIOCOMPRA",bean.getIndiceprazomediocompra());
				report.addParameter("METAPRAZOMEDIOCOMPRA",bean.getMetaprazomediocompra());
				report.addParameter("TOLERANCIAPRAZOCOMPRA",bean.getToleranciaprazocompra());
				report.addParameter("SITUACAOCOMPRAS",bean.getSituacaocompra());
				
				mergeReport.addReport(report);
				
			}
		}else {
			throw new SinedException("Nenhum item encontrado.");
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que verifica o filtro para incluir o registro no relat�rio
	 *
	 * @param filtro
	 * @param bean
	 * @return
	 * @author Luiz Fernando
	 */
	private boolean incluir(EmitirprazomediocompraFiltro filtro, PrazomediocompraReportBean bean){
		boolean restricaometa = false;
		boolean restricaautorizacao = false;
		
		boolean metaalcacada = false;
		boolean metanaoalcacada = false;
		boolean metadentrotolerancia = false;
		
		boolean autorizadadentroprazo = false;
		boolean autorizadaforaprazo = false;
		
		if(filtro.getMetaalcancada() != null && filtro.getMetaalcancada()) { 
			if(PrazomediocompraReportBean.META_ALCANCADA.equals(bean.getSituacaocompra()))
				metaalcacada = true;
			restricaometa = true;
		}
		if(filtro.getMetatolerancia() != null && filtro.getMetatolerancia()){ 
			if(PrazomediocompraReportBean.META_DENTRO_TOLERANCIA.equals(bean.getSituacaocompra()))
				metadentrotolerancia = true;
			restricaometa = true;
		}
		if(filtro.getMetanaoalcancada() != null && filtro.getMetanaoalcancada()){ 
			if(PrazomediocompraReportBean.META_NAO_ALCANCADA.equals(bean.getSituacaocompra()))
				metanaoalcacada = true;
			restricaometa = true;
		}
		if(filtro.getSolautorizadadentroprazo() != null && filtro.getSolautorizadadentroprazo()){ 
			if(PrazomediocompraReportBean.AUTORIZADA_DENTRO_PRAZO.equals(bean.getSituacaocompra()))
				autorizadadentroprazo = true;
			restricaautorizacao = true;
		}
		if(filtro.getSolautorizadaforaprazo() != null && filtro.getSolautorizadaforaprazo()){ 
			if(PrazomediocompraReportBean.AUTORIZADA_FORA_PRAZO.equals(bean.getSituacaocompra()))
				autorizadaforaprazo = true;
			restricaautorizacao = true;
		}
		
		return (!restricaometa && !restricaautorizacao) || (metaalcacada || metadentrotolerancia || metanaoalcacada || autorizadadentroprazo || autorizadaforaprazo); 
	}
	
	/**
	 * M�todo que busca a data de entrada
	 *
	 * @param solicitacaocompra
	 * @return
	 * @author Luiz Fernando
	 */
	public java.sql.Date getDataEntrada(Solicitacaocompra solicitacaocompra) {
		java.sql.Date dtentrada = null;
		if(solicitacaocompra != null){
			if(solicitacaocompra.getListaOrdemcompraorigem() != null && !solicitacaocompra.getListaOrdemcompraorigem().isEmpty()){
				for(Ordemcompraorigem ordemcompraorigem : solicitacaocompra.getListaOrdemcompraorigem()){
					if(ordemcompraorigem.getOrdemcompra() != null && 
							ordemcompraorigem.getOrdemcompra().getListaOrdemcompraentrega() != null &&
							!ordemcompraorigem.getOrdemcompra().getListaOrdemcompraentrega().isEmpty()){
						for(Ordemcompraentrega oce : ordemcompraorigem.getOrdemcompra().getListaOrdemcompraentrega()){
							if(oce.getEntrega() != null && oce.getEntrega() != null && oce.getEntrega().getDtentrega() != null){
								dtentrada = oce.getEntrega().getDtentrega();
							}
						}
					}
					if(dtentrada != null)
						break;
				}
			}else if(solicitacaocompra.getListaCotacaoorigem() != null && !solicitacaocompra.getListaCotacaoorigem().isEmpty()){
				for(Cotacaoorigem cotacaoorigem : solicitacaocompra.getListaCotacaoorigem()){
					if(cotacaoorigem.getCotacao() != null && cotacaoorigem.getCotacao().getListaOrdemcompra() != null && 
							!cotacaoorigem.getCotacao().getListaOrdemcompra().isEmpty()){
						for(Ordemcompra ordemcompra : cotacaoorigem.getCotacao().getListaOrdemcompra()){
							if(ordemcompra.getListaOrdemcompraentrega() != null &&
									!ordemcompra.getListaOrdemcompraentrega().isEmpty()){
								for(Ordemcompraentrega oce : ordemcompra.getListaOrdemcompraentrega()){
									if(oce.getEntrega() != null && oce.getEntrega() != null && oce.getEntrega().getDtentrega() != null) {
										dtentrada = oce.getEntrega().getDtentrega();
									}
								}
							}
							if(dtentrada != null)
								break;
						}
					}
					if(dtentrada != null) break;
				}
			}
		}
		return dtentrada;
	}
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForReportPrazomediocompra(EmitirprazomediocompraFiltro filtro)
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Solicitacaocompra> findForReportPrazomediocompra(EmitirprazomediocompraFiltro filtro) {
		return solicitacaocompraDAO.findForReportPrazomediocompra(filtro);
	}
	
	/**
	 * M�todo que busca a data da baixa da ordem de compra
	 *
	 * @param solicitacaocompra
	 * @return
	 * @author Luiz Fernando
	 */
	public Ordemcomprahistorico getDtbaixaOrdemcompra(Solicitacaocompra solicitacaocompra){
		Ordemcomprahistorico ordemcomprahistorico = null;
		
		if(solicitacaocompra != null){
			if(solicitacaocompra.getListaOrdemcompraorigem() != null && !solicitacaocompra.getListaOrdemcompraorigem().isEmpty()){
				for(Ordemcompraorigem ordemcompraorigem : solicitacaocompra.getListaOrdemcompraorigem()){
					if(ordemcompraorigem.getOrdemcompra() != null && 
							ordemcompraorigem.getOrdemcompra().getListaOrdemcomprahistorico() != null &&
							!ordemcompraorigem.getOrdemcompra().getListaOrdemcomprahistorico().isEmpty()){
						for(Ordemcomprahistorico och : ordemcompraorigem.getOrdemcompra().getListaOrdemcomprahistorico()){
							if(och.getOrdemcompraacao() != null && Ordemcompraacao.ENTEGA_RECEBIDA.equals(och.getOrdemcompraacao())){
								ordemcomprahistorico = och;
								break;
							}
						}
					}
					if(ordemcomprahistorico != null)
						break;
				}
			}else if(solicitacaocompra.getListaCotacaoorigem() != null && !solicitacaocompra.getListaCotacaoorigem().isEmpty()){
				for(Cotacaoorigem cotacaoorigem : solicitacaocompra.getListaCotacaoorigem()){
					if(cotacaoorigem.getCotacao() != null && cotacaoorigem.getCotacao().getListaOrdemcompra() != null && 
							!cotacaoorigem.getCotacao().getListaOrdemcompra().isEmpty()){
						for(Ordemcompra ordemcompra : cotacaoorigem.getCotacao().getListaOrdemcompra()){
							if(ordemcompra.getListaOrdemcomprahistorico() != null && !ordemcompra.getListaOrdemcomprahistorico().isEmpty()){
								for(Ordemcomprahistorico och : ordemcompra.getListaOrdemcomprahistorico()){
									if(och.getOrdemcompraacao() != null && Ordemcompraacao.ENTEGA_RECEBIDA.equals(och.getOrdemcompraacao())){
										ordemcomprahistorico = och;
										break;
									}
								}
							}
							if(ordemcomprahistorico != null)
								break;
						}
					}
					if(ordemcomprahistorico != null) break;
				}
			}
		}
		return ordemcomprahistorico;
	}
	
	/**
	 * M�todo que busca o nome da pessoa que autorizou a solicita��o
	 *
	 * @param solicitacaocompra
	 * @return
	 * @author Luiz Fernando
	 */
	public String getNomepessoaAutorizacao(Solicitacaocompra solicitacaocompra){
		String nome = null;
		if(solicitacaocompra != null && solicitacaocompra.getListaSolicitacaocomprahistorico() != null && !solicitacaocompra.getListaSolicitacaocomprahistorico().isEmpty()){
			Collections.sort(solicitacaocompra.getListaSolicitacaocomprahistorico(), new Comparator<Solicitacaocomprahistorico>(){
				public int compare(Solicitacaocomprahistorico a, Solicitacaocomprahistorico b) {
					return a.getDtaltera().compareTo(b.getDtaltera());
				}
			});
			for(Solicitacaocomprahistorico sch : solicitacaocompra.getListaSolicitacaocomprahistorico()){
				if(sch.getSolicitacaocompraacao() != null && Solicitacaocompraacao.AUTORIZADA.getNome().equals(sch.getSolicitacaocompraacao().getNome())){
					nome = sch.getResponsavel();
					break;
				}
			}
		}
		
		return nome;
	}
	
	/**
	 * M�todo que adiciona o material a lista de solicita��es
	 *
	 * @param solicitacoes
	 * @param solicitacaocompra
	 * @author Luiz Fernando
	 */
	public void addMateriaisAgendaproducao(List<Solicitacaocompra> solicitacoes, Solicitacaocompra solicitacaocompra) {
		boolean adicionar = true;
		if(solicitacoes.size() > 0){
			for(Solicitacaocompra sc : solicitacoes){
				if(sc.getMaterial().equals(solicitacaocompra.getMaterial())){
					adicionar = false;
					sc.setQtde(sc.getQtde() + solicitacaocompra.getQtde());
					break;
				}
			}
		}
		
		if(adicionar)
			solicitacoes.add(solicitacaocompra);
	}
	
	/**
	 * M�todo que ajusta a qtde caso a solicita��o tenha o mesmo material com unidade diferente
	 *
	 * @param listaSolicitacao
	 * @author Luiz Fernando
	 */
	public void ajustaQtdeUnidademedida(List<Solicitacaocompra> listaSolicitacao) {
		if(listaSolicitacao != null && !listaSolicitacao.isEmpty()){
			HashMap<Material, Unidademedida> mapMatQtdeUnidade = new HashMap<Material, Unidademedida>();
			List<Material> listaMaterialUnidadediferente = new ArrayList<Material>();
			for(Solicitacaocompra sc : listaSolicitacao){
				if(sc.getMaterial() != null){
					if(sc.getUnidademedida() != null){
						if(mapMatQtdeUnidade.get(sc.getMaterial()) != null){
							if(!mapMatQtdeUnidade.get(sc.getMaterial()).equals(sc.getUnidademedida())){
								if(!listaMaterialUnidadediferente.contains(sc.getMaterial())){
									listaMaterialUnidadediferente.add(sc.getMaterial());
								}
							}
						}else {
							mapMatQtdeUnidade.put(sc.getMaterial(), sc.getUnidademedida());
						}
					}else if(sc.getMaterial().getUnidademedida() != null){
						if(mapMatQtdeUnidade.get(sc.getMaterial()) != null){
							if(!mapMatQtdeUnidade.get(sc.getMaterial()).equals(sc.getMaterial().getUnidademedida())){
								if(!listaMaterialUnidadediferente.contains(sc.getMaterial())){
									listaMaterialUnidadediferente.add(sc.getMaterial());
								}
							}
						}else {
							mapMatQtdeUnidade.put(sc.getMaterial(), sc.getMaterial().getUnidademedida());
						}
					}
				}
			}
			
			if(listaMaterialUnidadediferente != null && !listaMaterialUnidadediferente.isEmpty()){
				Double qtde = null;
				for(Material m : listaMaterialUnidadediferente){
					for(Solicitacaocompra sc : listaSolicitacao){
						if(sc.getMaterial() != null && sc.getUnidademedida() != null && sc.getMaterial().equals(m)){
							if(!sc.getMaterial().getUnidademedida().equals(sc.getUnidademedida())){
								Material material = materialService.unidadeMedidaMaterial(m);
								Unidademedida un = sc.getUnidademedida();
								
								sc.setUnidademedida(sc.getMaterial().getUnidademedida());
								qtde = unidademedidaService.converteQtdeUnidademedida(material.getUnidademedida(), sc.getQtde(), un, material, null, 1.0);
								if(qtde != null && qtde > 0){
									sc.setQtde(qtde);
								}
							}
						}
					}
				}
			}
		}
	}
	public String makeLinkSuprimentos(String whereIn, String tela, String idProperty) {
		if(whereIn == null) return null;
		
		StringBuilder link = new StringBuilder();
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			link.append("<a href=\"")
				.append(NeoWeb.getRequestContext().getServletRequest().getContextPath())
				.append("/suprimento/crud/")
				.append(tela)
				.append("?ACAO=consultar&")
				.append(idProperty)
				.append("=")
				.append(id)
				.append("\">")
				.append(id)
				.append("</a> ");
		}
		return link.toString();
	}
	public List<Integer> findCotacoesNaoCanceladas(String whereInCdcotacao) {
		return cotacaoService.findCotacoesNaoCanceladas(whereInCdcotacao);
	}
	public List<Solicitacaocompra> getListaMaterialByVgerenciarmaterial(List<Vgerenciarmaterial> listaVgerenciarmaterial) {
		Solicitacaocompra solicitacaocompra;
		List<Solicitacaocompra> list = new ArrayList<Solicitacaocompra>();
		String whereIn = CollectionsUtil.listAndConcatenate(listaVgerenciarmaterial, "cdmaterial", ",");
		// necessario pegar o nome do material, pelo Vgerenciarmaterial so vinha P.K. 
		List<Material> listMaterial = materialService.findForSolicitacaoCompraFromGerenciamentomaterial(whereIn);
		for (Material material : listMaterial) {
			solicitacaocompra = new Solicitacaocompra();
			solicitacaocompra.setMaterial(material);
			list.add(solicitacaocompra);
		}
		return list;
	}
	
	/**
	* M�todo que cria o resumo de unidademedida/qtde 
	*
	* @param filtro
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public void createResumoSolicitacaocompra(SolicitacaocompraFiltro filtro) {
		Double pesoTotal = 0d;
		List<GenericBean> listaResumo = new ArrayList<GenericBean>();
		List<Solicitacaocompra> lista = findForResumo(filtro);
		if(SinedUtil.isListNotEmpty(lista)){
			HashMap<Unidademedida, Double> mapUnidadeQtde = new HashMap<Unidademedida, Double>();
			for(Solicitacaocompra sc : lista){
				Unidademedida unidademedida = sc.getUnidademedida();
				if(unidademedida == null && sc.getMaterial() != null)
					unidademedida = sc.getMaterial().getUnidademedida();
				
				if(unidademedida != null && sc.getQtde() != null){
					Double qtde = mapUnidadeQtde.get(unidademedida);
					if(qtde == null)
						qtde = 0d;
					mapUnidadeQtde.put(unidademedida, qtde + sc.getQtde());
				}
				if(sc.getPesototal() != null){
					pesoTotal+= sc.getPesototal();
				}
			}
			
			for(Unidademedida unidademedida : mapUnidadeQtde.keySet()){
				listaResumo.add(new GenericBean(unidademedida.getNome(), mapUnidadeQtde.get(unidademedida)));
			}
		}
		filtro.setPesototal(pesoTotal);
		filtro.setListaResumoSolicitacaocompra(listaResumo);
	}
	
	/**
	* M�todo que cria o resumo de unidademedida/qtde 
	*
	* @param lista
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public List<GenericBean> createResumoSolicitacaocompra(List<Solicitacaocompra> lista) {
		List<GenericBean> listaResumo = new ArrayList<GenericBean>();
		if(SinedUtil.isListNotEmpty(lista)){
			HashMap<Unidademedida, Double> mapUnidadeQtde = new HashMap<Unidademedida, Double>();
			for(Solicitacaocompra sc : lista){
				Unidademedida unidademedida = sc.getUnidademedida();
				if(unidademedida == null && sc.getMaterial() != null)
					unidademedida = sc.getMaterial().getUnidademedida();
				
				if(unidademedida != null && sc.getQtde() != null){
					Double qtde = mapUnidadeQtde.get(unidademedida);
					if(qtde == null)
						qtde = 0d;
					mapUnidadeQtde.put(unidademedida, qtde + sc.getQtde());
				}
			}
			
			for(Unidademedida unidademedida : mapUnidadeQtde.keySet()){
				listaResumo.add(new GenericBean(unidademedida.getNome(), mapUnidadeQtde.get(unidademedida)));
			}
		}
		return listaResumo;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForResumo(SolicitacaocompraFiltro filtro)
	*
	* @param filtro
	* @return
	* @since 06/01/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForResumo(SolicitacaocompraFiltro filtro) {
		return solicitacaocompraDAO.findForResumo(filtro);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findWithOrigem(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 03/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithOrigem(String whereIn) {
		return solicitacaocompraDAO.findWithOrigem(whereIn);
	}
	
	/**
	* M�todo que baixa a solicita��o com quantidade restante, vincula a origem da solicita��o baixada com a nova solicita��o e 
	* registra no hist�rico
	*
	* @param bean
	* @param cdsolicitacaocompraQuantidadeRestante
	* @since 03/09/2015
	* @author Luiz Fernando
	*/
	public void baixarEregistrarHistoricoQuantidadeRestante(final Solicitacaocompra bean, final Integer cdsolicitacaocompraQuantidadeRestante) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				List<Solicitacaocompra> listaSolicitacaocompra = findWithOrigem(cdsolicitacaocompraQuantidadeRestante.toString());
				if(SinedUtil.isListNotEmpty(listaSolicitacaocompra)){
					for(Solicitacaocompra solicitacaocompra : listaSolicitacaocompra){
						Solicitacaocompraorigem sco = new Solicitacaocompraorigem();
						sco.setSolicitacaocompra(solicitacaocompra);
						if(solicitacaocompra.getSolicitacaocompraorigem() != null && 
								solicitacaocompra.getSolicitacaocompraorigem().getRequisicaomaterial() != null){
							sco.setRequisicaomaterial(solicitacaocompra.getSolicitacaocompraorigem().getRequisicaomaterial());
							
						}
						solicitacaocompraorigemService.saveOrUpdateNoUseTransaction(sco);
						
						updateVinculoSolicitacaocompraorigem(bean, sco);
					}
				}
				
				Integer identificadorOuCdsolicitacaocompra = cdsolicitacaocompraQuantidadeRestante;
				Solicitacaocompra scQuantidadeRestante = load(new Solicitacaocompra(cdsolicitacaocompraQuantidadeRestante), "solicitacaocompra.cdsolicitacaocompra, solicitacaocompra.identificador");
				if(scQuantidadeRestante != null && scQuantidadeRestante.getIdentificador() != null){
					identificadorOuCdsolicitacaocompra = scQuantidadeRestante.getIdentificador();
				}
				
				solicitacaocomprahistoricoService.saveOrUpdate(new Solicitacaocomprahistorico(
										bean, 
										Solicitacaocompraacao.CRIADA,
										"Criada a partir da solicita��o de compra <a href=\"javascript:visualizarSolicitacaocompra(" + cdsolicitacaocompraQuantidadeRestante + ");\">" + identificadorOuCdsolicitacaocompra + "</a> com a quantidade restante."));
				solicitacaocomprahistoricoService.saveOrUpdate(
							new Solicitacaocomprahistorico(
										new Solicitacaocompra(cdsolicitacaocompraQuantidadeRestante), 
										Solicitacaocompraacao.BAIXADA, 
										"Criada a solicita��o de compra <a href=\"javascript:visualizarSolicitacaocompra(" + bean.getCdsolicitacaocompra() + ");\">" + bean.getIdentificadorOuCdsolicitacaocompra() + "</a> com a quantidade restante."
										)
							);
				
				return status;
		}});
		
		List<Solicitacaocompra> listaSC = findForBaixar(cdsolicitacaocompraQuantidadeRestante + "");
		if(SinedUtil.isListNotEmpty(listaSC)){
			doUpdateSituacaoSolicitacoes(CollectionsUtil.listAndConcatenate(listaSC, "cdsolicitacaocompra", ","), Situacaosuprimentos.BAIXADA);
			doUpdatePendenciaQtde(cdsolicitacaocompraQuantidadeRestante + "", false);
		}else {
			callProcedureAtualizaSolicitacaocompra(cdsolicitacaocompraQuantidadeRestante + "");
		}
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#updateVinculoSolicitacaocompraorigem(Solicitacaocompra solicitacaocompra, Solicitacaocompraorigem solicitacaocompraorigem)
	*
	* @param solicitacaocompra
	* @param solicitacaocompraorigem
	* @since 03/09/2015
	* @author Luiz Fernando
	*/
	public void updateVinculoSolicitacaocompraorigem(Solicitacaocompra solicitacaocompra, Solicitacaocompraorigem solicitacaocompraorigem) {
		solicitacaocompraDAO.updateVinculoSolicitacaocompraorigem(solicitacaocompra, solicitacaocompraorigem);
	}
	
	/**
	* M�todo que retorna um whereIn com o identificador ou cdsolicitacaocompra
	*
	* @param whereIn
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public String getWhereInIdentificadorOuCdsolicitacaocompra(String whereIn) {
		List<Solicitacaocompra> lista = findWithIdentificador(whereIn);
		return SinedUtil.isListNotEmpty(lista) ? CollectionsUtil.listAndConcatenate(lista, "identificadorOuCdsolicitacaocompra", ",") : whereIn;
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findWithIdentificador(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithIdentificador(String whereIn){
		return solicitacaocompraDAO.findWithIdentificador(whereIn);
	}
	
	/**
	* M�todo que cria o link de hist�rico para as solicita��es de compra
	*
	* @param lista
	* @param nameFunction
	* @return
	* @since 10/09/2015
	* @author Luiz Fernando
	*/
	public String makeLinkHistoricoSolicitacaocompra(List<Solicitacaocompra> lista) {
		StringBuilder link = new StringBuilder();
		
		if(SinedUtil.isListNotEmpty(lista)){
			for(Solicitacaocompra sc : lista){
				link.append("<a href=\"javascript:visualizarSolicitacaocompra(" + sc.getCdsolicitacaocompra() +")\">" + sc.getIdentificadorOuCdsolicitacaocompra() + "</a>, ");
			}
		}
		return link.length() > 0 ? link.substring(0, link.length()-2) : "";
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#doUpdatePendenciaQtde(String whereIn, Boolean pendenciaQtde)
	*
	* @param whereIn
	* @param pendenciaQtde
	* @since 14/09/2015
	* @author Luiz Fernando
	*/
	public void doUpdatePendenciaQtde(String whereIn, Boolean pendenciaQtde) {
		solicitacaocompraDAO.doUpdatePendenciaQtde(whereIn, pendenciaQtde);
	}
	
	/**
	* M�todo que registra no hist�rico da solicita��o de compra a a��o de inserir em cota��o
	*
	* @param whereIn
	* @param cotacao
	* @since 17/09/2015
	* @author Luiz Fernando
	*/
	public void registrarHistoricoAposInserirEmCotacao(final String whereIn, final Cotacao cotacao) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				
				for(String id : whereIn.split(",")){
					solicitacaocomprahistoricoService.saveOrUpdate(new Solicitacaocomprahistorico(
											new Solicitacaocompra(Integer.parseInt(id)), 
											Solicitacaocompraacao.EM_PROCESSO_COMPRA,
											"Solicita��o de compra inserida na cota��o <a href=\"javascript:visualizarCotacao(" + cotacao.getCdcotacao() + ");\">" + cotacao.getCdcotacao() + "</a>."));
				}
				
				return status;
		}});
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findWithUnidademedida(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 21/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findWithUnidademedida(String whereIn) {
		return solicitacaocompraDAO.findWithUnidademedida(whereIn);
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForBaixar(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 25/09/2015
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForBaixar(String whereIn) {
		return solicitacaocompraDAO.findForBaixar(whereIn);
	}
	
	/**
	* M�todo que valida se a quantidade solicitada � maior que a quantidade do planejamento
	*
	* @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#validaQtdeCompraMaiorQuePlanejamento(List<Solicitacaocompra> lista, BindException errors)
	*
	* @param solicitacaocompra
	* @param errors
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public void validaQtdeCompraMaiorQuePlanejamento(Solicitacaocompra solicitacaocompra, BindException errors) {
		Solicitacaocompra bean = new Solicitacaocompra();
		bean.setSolicitacoes(new ArrayList<Solicitacaocompra>());
		bean.getSolicitacoes().add(solicitacaocompra);
		validaQtdeCompraMaiorQuePlanejamento(bean.getSolicitacoes(), solicitacaocompra.getProjeto(), errors);
	}
	
	/**
	* M�todo que valida se a quantidade solicitada � maior que a quantidade do planejamento
	*
	* @param lista
	* @param errors
	* @since 16/03/2016
	* @author Luiz Fernando
	*/
	public void validaQtdeCompraMaiorQuePlanejamento(List<Solicitacaocompra> lista, Projeto projeto, BindException errors) {
	 	if(SinedUtil.isListNotEmpty(lista) && projeto != null && "TRUE".equalsIgnoreCase(parametrogeralService.getValorPorNome(Parametrogeral.BLOQUEARCOMPRAPROJETO))){
	 		String whereInMaterial = CollectionsUtil.listAndConcatenate(lista, "material.cdmaterial", ",");
	 		List<Planejamentorecursogeral> listaPlanejamentorecursogeral = planejamentorecursogeralService.findForBloquearQtdeCompra(projeto, whereInMaterial);
	 		
	 		
	 		if(listaPlanejamentorecursogeral == null || listaPlanejamentorecursogeral.size() == 0){
	 			StringBuilder materiais = new StringBuilder();
	 			for(Solicitacaocompra solicitacaocompra : lista){
	 				if(solicitacaocompra.getMaterial() != null && solicitacaocompra.getMaterial().getNome() != null){
	 					materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(solicitacaocompra.getMaterial().getNome());
	 				}
	 			}
	 			errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
	 		}else {
	 			List<Material> listaMaterialEncontrado = new ArrayList<Material>();
	 			StringBuilder materiais = new StringBuilder();
	 			for(Solicitacaocompra solicitacaocompra : lista){
	 				if(solicitacaocompra.getMaterial() != null){
	 					boolean materialEncontrado = false;
	 					for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
	 						if(prg.getMaterial() != null && prg.getMaterial().equals(solicitacaocompra.getMaterial())){
	 							materialEncontrado = true;
	 							if(!listaMaterialEncontrado.contains(solicitacaocompra.getMaterial())){
	 								listaMaterialEncontrado.add(solicitacaocompra.getMaterial());
	 							}
	 							break;
	 						}
	 					}
	 					if(!materialEncontrado){
	 						materiais.append("<br>&nbsp;&nbsp;&nbsp;").append(solicitacaocompra.getMaterial().getNome());
	 					}
	 				}
	 			}
	 			if(StringUtils.isNotBlank(materiais.toString())){
	 				errors.reject("001", "Materiais que n�o est�o previstos no Planejamento." + materiais);
	 			}
	 			
 				StringBuilder whereInSolicitacaocompra = new StringBuilder();
 				String whereIn = null;
 				for(Solicitacaocompra solicitacaocompra : lista){
 					if(solicitacaocompra.getCdsolicitacaocompra() != null) whereInSolicitacaocompra.append(solicitacaocompra.getCdsolicitacaocompra()).append(",");
 				}
 				if(StringUtils.isNotBlank(whereInSolicitacaocompra.toString())){
 					whereIn = whereInSolicitacaocompra.substring(0, whereInSolicitacaocompra.length()-1);
 				}
 				
 				List<Solicitacaocompra> listaAgrupada = agruparItensValidacaoPlanejamento(lista);
 				List<Solicitacaocompra> listaSC = findForBloquearQtdeCompra(projeto, whereInMaterial, whereIn, null);
 				
 				StringBuilder menagem =  new StringBuilder();
 				for(Solicitacaocompra solicitacaocompra : listaAgrupada){
 		 			if(solicitacaocompra.getQtde() != null && solicitacaocompra.getMaterial() != null && listaMaterialEncontrado.contains(solicitacaocompra.getMaterial())){
 		 				Double qtdecomprada = 0d;
 		 				Double qtdePrevista = 0d;
 		 				if(SinedUtil.isListNotEmpty(listaSC)){
	 		 				for(Solicitacaocompra sc : listaSC){
 								if(sc.getQtde() != null && sc.getMaterial() != null && sc.getMaterial().equals(solicitacaocompra.getMaterial())){
 									qtdecomprada = sc.getQtde();
 									break;
 								}
	 		 				}
 						}
 		 				
 		 				for(Planejamentorecursogeral prg : listaPlanejamentorecursogeral){
							if(prg.getQtde() != null && prg.getMaterial() != null && prg.getMaterial().equals(solicitacaocompra.getMaterial())){
								qtdePrevista += prg.getQtde();
							}
 		 				}
 		 				
 		 				Double qtdeAcumulada = qtdecomprada + solicitacaocompra.getQtde();
 		 				if(qtdeAcumulada > qtdePrevista){
 		 					String nome = solicitacaocompra.getMaterial().getNome();
 		 					if(StringUtils.isBlank(nome)){
 		 						Material material = materialService.load(solicitacaocompra.getMaterial(), "material.cdmaterial, material.nome");
 		 						if(material != null) nome = material.getNome();
 		 					}
 		 					menagem.append("<br>&nbsp;&nbsp;&nbsp;" + nome + " - Qtde Prevista: " + qtdePrevista + " - Qtde Acumulada: " + new BigDecimal(qtdeAcumulada));
 		 				}
 		 			}
 		 		}
 				if(StringUtils.isNotBlank(menagem.toString())){
 					errors.reject("001", "A quantidade de material comprado ou solicitado extrapola a quantidade prevista no Planejamento." + menagem);
 				}
 			}
 		}
	}
	
	/**
	* M�todo que agrupa os itens da solicita��o de acordo com o material
	*
	* @param listaSolicitacaocompra
	* @return
	* @since 13/05/2016
	* @author Luiz Fernando
	*/
	private List<Solicitacaocompra> agruparItensValidacaoPlanejamento(List<Solicitacaocompra> listaSolicitacaocompra) {
		List<Solicitacaocompra> listaAgrupada = new ArrayList<Solicitacaocompra>();
		if(SinedUtil.isListNotEmpty(listaSolicitacaocompra)){
			for(Solicitacaocompra sc : listaSolicitacaocompra){
				if(sc.getQtde() != null && sc.getMaterial() != null && 
						(sc.getAux_solicitacaocompra() == null ||
						 sc.getAux_solicitacaocompra().getSituacaosuprimentos() == null ||
						 !Situacaosuprimentos.BAIXADA.equals(sc.getAux_solicitacaocompra().getSituacaosuprimentos()))){
					boolean adicionar = true;
					for(Solicitacaocompra itemAgrupado : listaAgrupada){
						if(sc.getMaterial().equals(itemAgrupado.getMaterial())){
							itemAgrupado.setQtde(itemAgrupado.getQtde() + sc.getQtde());
							adicionar = false;
						}
					}
					if(adicionar){
						Solicitacaocompra solicitacaocompra = new Solicitacaocompra();
						solicitacaocompra.setMaterial(sc.getMaterial());
						solicitacaocompra.setQtde(sc.getQtde());
						solicitacaocompra.setProjeto(sc.getProjeto());
						listaAgrupada.add(solicitacaocompra);
					}
				}
			}
		}
		return listaAgrupada;
	}
	
	/**
	* M�todo que busca os materiais com as solicita��es e ordens de compra do projeto
	*
	* @see br.com.linkcom.sined.geral.service.SolicitacaocompraService#findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial, String whereInSolicitacaocompra, String whereInOrdemcompramaterial)
	*
	* @param projeto
	* @param whereInMaterial
	* @param whereInSolicitacaocompra
	* @param whereInOrdemcompramaterial
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForBloquearQtdeCompra(Projeto projeto, String whereInMaterial, String whereInSolicitacaocompra, String whereInOrdemcompramaterial) {
		if(projeto == null || projeto.getCdprojeto() == null)
			throw new SinedException("Par�metro inv�lido.");
		
		return findForBloquearQtdeCompra(projeto.getCdprojeto().toString(), whereInMaterial, whereInSolicitacaocompra, whereInOrdemcompramaterial);
	}

	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.SolicitacaocompraDAO#findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial, String whereInSolicitacaocompra, String whereInOrdemcompramaterial)
	*
	* @param whereInProjeto
	* @param whereInMaterial
	* @param whereInSolicitacaocompra
	* @param whereInOrdemcompramaterial
	* @return
	* @since 18/03/2016
	* @author Luiz Fernando
	*/
	public List<Solicitacaocompra> findForBloquearQtdeCompra(String whereInProjeto, String whereInMaterial, String whereInSolicitacaocompra, String whereInOrdemcompramaterial) {
		return solicitacaocompraDAO.findForBloquearQtdeCompra(whereInProjeto, whereInMaterial, whereInSolicitacaocompra, whereInOrdemcompramaterial);
	}
	
	/**
	* M�todo que retorna a solicita��o de compra de uma determinada lista
	*
	* @param solicitacaocompra
	* @param listaSolicitacaocompra
	* @return
	* @since 25/10/2016
	* @author Luiz Fernando
	*/
	public Solicitacaocompra getSolicitacaoByLista(Solicitacaocompra solicitacaocompra,	List<Solicitacaocompra> listaSolicitacaocompra) {
		if(solicitacaocompra != null && solicitacaocompra.getCdsolicitacaocompra() != null &&
				SinedUtil.isListNotEmpty(listaSolicitacaocompra)){
			for(Solicitacaocompra sc : listaSolicitacaocompra){
				if(solicitacaocompra.equals(sc)){
					return sc;
				}
			}
		}
		return null;
	}
	
	public void criarAvisoSolicitacaoCompraAtrasoAprovacao(Motivoaviso m, java.sql.Date data, java.sql.Date dateToSearch) throws ParseException {
		List<Solicitacaocompra> solicitacaoCompraList = solicitacaocompraDAO.findByAtrasadoAprovacao(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Solicitacaocompra s : solicitacaoCompraList) {
			if (s.getDtentrega() != null && SinedDateUtils.stringToDate(s.getDtentrega()).getTime() == SinedDateUtils.addDiasData(data, 5).getTime()) {
				Aviso aviso = new Aviso("Solicita��o de compra com atraso na aprova��o", "C�digo da solicita��o de compra: " + s.getCdsolicitacaocompra(), 
						m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), s.getCdsolicitacaocompra(), s.getEmpresa() != null ? s.getEmpresa() : empresa, 
						SinedDateUtils.currentDate(), m, Boolean.FALSE);
				java.sql.Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, s.getCdsolicitacaocompra());
				
				if (lastAvisoDate != null) {
					avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, s.getCdsolicitacaocompra(), lastAvisoDate);
					
					List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
					
					if (SinedUtil.isListNotEmpty(listaUsuario)) {
						avisoService.salvarAvisos(aviso, listaUsuario, false);
					}
				} else {
					avisoList.add(aviso);
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

	public void criarAvisoSolicitacaoCompraAtrasoEntrega(Motivoaviso m, java.sql.Date data, java.sql.Date dateToSearch) throws ParseException {
		List<Solicitacaocompra> solicitacaoCompraList = solicitacaocompraDAO.findByAtrasadoEntrega(dateToSearch);
		List<Aviso> avisoList = new ArrayList<Aviso>();
		List<Avisousuario> avisoUsuarioList = null;
		
		Empresa empresa = empresaService.loadPrincipal();
		for (Solicitacaocompra s : solicitacaoCompraList) {
			if (SinedDateUtils.stringToDate(s.getDtentrega()).getTime() < data.getTime()) {
				Aviso aviso = new Aviso("Solicita��o de compra com atraso na entrega", "C�digo da solicita��o de compra: " + s.getCdsolicitacaocompra(), 
						m.getTipoaviso(), m.getPapel(), m.getAvisoorigem(), s.getCdsolicitacaocompra(), s.getEmpresa() != null ? s.getEmpresa() : empresa, 
						SinedDateUtils.currentDate(), m, Boolean.FALSE);
				java.sql.Date lastAvisoDate = avisoService.findLastAvisoDateByMotivoIdOrigem(m, s.getCdsolicitacaocompra());
				
				if (lastAvisoDate != null) {
					avisoUsuarioList = avisoUsuarioService.findAllAvisoUsuarioByLastAvisoDateMotivoIdOrigem(m, s.getCdsolicitacaocompra(), lastAvisoDate);
					
					List<Usuario> listaUsuario = avisoService.getListaCorrigidaUsuarioSalvarAviso(aviso, avisoUsuarioList);
					
					if (SinedUtil.isListNotEmpty(listaUsuario)) {
						avisoService.salvarAvisos(aviso, listaUsuario, false);
					}
				} else {
					avisoList.add(aviso);
				}
			}
		}
		
		if (avisoList != null && SinedUtil.isListNotEmpty(avisoList)) {
			avisoService.salvarAvisos(avisoList, true);
		}
	}

}
