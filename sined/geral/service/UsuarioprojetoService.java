package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Usuarioprojeto;
import br.com.linkcom.sined.geral.dao.UsuarioprojetoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class UsuarioprojetoService extends GenericService<Usuarioprojeto> {
	protected UsuarioprojetoDAO usuarioprojetoDAO;
	
	public void setUsuarioprojetoDAO(UsuarioprojetoDAO usuarioprojetoDAO) {
		this.usuarioprojetoDAO = usuarioprojetoDAO;
	}
	
	public List<Usuarioprojeto> findForAutocompleteByUsuariologado(String q) {
		return usuarioprojetoDAO.findForAutocompleteByUsuariologado(q);
	}
}
