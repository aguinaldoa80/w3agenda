package br.com.linkcom.sined.geral.service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.bean.Contacrmhistorico;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Proposta;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_proposta;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.ContacrmDAO;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.ContacrmFiltro;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.ControleInteracaoBean;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.AtualizarHistoricoVO;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.PainelInteracaoVendaInt;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmService extends GenericService<Contacrm>{
	
	private ContacrmDAO contacrmDAO;
	private OportunidadeService oportunidadeService;
	private LeadService leadService;
	private LeadhistoricoService leadhistoricoService;
	private ContacrmhistoricoService contacrmhistoricoService;
	
	public void setContacrmhistoricoService(
			ContacrmhistoricoService contacrmhistoricoService) {
		this.contacrmhistoricoService = contacrmhistoricoService;
	}
	public void setLeadhistoricoService(
			LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setOportunidadeService(OportunidadeService oportunidadeService) {
		this.oportunidadeService = oportunidadeService;
	}
	public void setContacrmDAO(ContacrmDAO contacrmDAO) {
		this.contacrmDAO = contacrmDAO;
	}
	
	
	public Contacrm loadForOportunidade(Integer cdcontacrm){
		return contacrmDAO.loadForOportunidade(cdcontacrm);
	}
	
	public List<Contacrm> findForReport(ContacrmFiltro contacrmFiltro){
		return contacrmDAO.findForReport(contacrmFiltro);
	}
	
	public IReport createRelatorioContascrm(ContacrmFiltro contacrmFiltro){
		Report report = new Report("/crm/contacrm");
		List<Contacrm> lista = this.findForReport(contacrmFiltro);
		
		for(Contacrm conta : lista){
			String strContato = "";
			
			if(conta.getListcontacrmcontato() != null){
				String strEmails = "";
				for(Contacrmcontato contato : conta.getListcontacrmcontato()){
					strContato = strContato + contato.getNome() + "\n";

					if(contato.getListcontacrmcontatoemail() != null){
						strEmails =  strEmails + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(), "email", "\n") + "\n ";
					}
					
					if(contato.getListcontacrmcontatofone() != null){
						String strFone = "";
						for(Contacrmcontatofone fone : contato.getListcontacrmcontatofone()){
							strFone = strFone + fone.getTelefone() + " (" + fone.getTelefonetipo().getNome() + ")\n ";
						}
						if(!strFone.equals("")){
							strFone = strFone.substring(0, (strFone.length()-1));
						}
						conta.setListaFones(strFone);
					}
					
				}
				
				if(!strEmails.equals("")){
					strEmails = strEmails.substring(0, (strEmails.length()-1));
				}
				conta.setEmailsreport(strEmails);
				
				if(!strContato.equals("")){
					strContato = strContato.substring(0, (strContato.length()-1));
				}
				conta.setListacontatos(strContato);
			}
		}
		
		report.setDataSource(lista);
		return report;
		
	}


	public List<Contacrm> loadWithLista(String whereIn, String orderBy, boolean asc) {
		return contacrmDAO.loadWithLista(whereIn, orderBy, asc);
	}
	
	public List<Contacrm> findContacrmAutocomplete(String q) {
		return contacrmDAO.findContacrmAutocomplete(q);
	}
	
	public Contacrm carregaContacrmcontato(String whereIn) {
		return contacrmDAO.carregaContacrmcontato(whereIn);
	}
	
	public List<Contacrm> findContacrmcontato(String whereIn) {
		return contacrmDAO.findContacrmcontato(whereIn);
	}


	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contacrm
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Contacrm loadContaWithResponsavel(Contacrm contacrm) {
		return contacrmDAO.loadContaWithResponsavel(contacrm);
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @param busca
	 * @return
	 * @author Taidson
	 * @since 04/09/2010
	 */
	public List<Contacrm> findContacrmForBuscaGeral(String busca) {
		return contacrmDAO.findContacrmForBuscaGeral(busca);
	}
	
	
	public List<PainelInteracaoVendaInt> findPesquisaAutocomplete(String s){
		
		List<PainelInteracaoVendaInt> lista = new ArrayList<PainelInteracaoVendaInt>();
		lista.addAll(this.findForAutocomplete(s, "contacrm.nome", "contacrm.nome", true));
		lista.addAll(oportunidadeService.findForAutocomplete(s, "oportunidade.nome", "oportunidade.nome", true));
		
		for (PainelInteracaoVendaInt p : lista) {
			if(p instanceof Oportunidade){
				((Oportunidade) p).setNome(((Oportunidade) p).getNome() + " (Oportunidade)");
			} else if(p instanceof Contacrm){
				((Contacrm) p).setNome(((Contacrm) p).getNome() + " (Conta)");
			}
		}
		
		Collections.sort(lista,new Comparator<PainelInteracaoVendaInt>(){
			public int compare(PainelInteracaoVendaInt o1, PainelInteracaoVendaInt o2) {
				String s1, s2;
				
				if(o1 instanceof Oportunidade){
					s1 = ((Oportunidade) o1).getNome();
				} else if(o1 instanceof Contacrm){
					s1 = ((Contacrm) o1).getNome();
				} else {
					s1 = "";
				}
				
				if(o2 instanceof Oportunidade){
					s2 = ((Oportunidade) o2).getNome();
				} else if(o2 instanceof Contacrm){
					s2 = ((Contacrm) o2).getNome();
				} else {
					s2 = "";
				}
				
				return s1.toUpperCase().compareTo(s2.toUpperCase());
			}
		});
		
		return lista;
	}
	
	public int processaArquivoMailmarketing(String string) {
		String[] linhas = string.split("\n");
		
		if(linhas == null || linhas.length < 2){
			throw new SinedException("Arquivo inv�lido. Conte�do do arquvo n�o encontrado.");
		}
		
		if(!linhas[0].equals("email;nome;data")){
			throw new SinedException("Arquivo inv�lido. Cabe�alho incorreto.");
		}
		
		List<AtualizarHistoricoVO> lista = new ArrayList<AtualizarHistoricoVO>();
		AtualizarHistoricoVO vo;
		String[] campos;
		Date aux;
		
		for (int i = 1; i < linhas.length; i++) {
			campos = linhas[i].split(";");
			
			if(campos.length != 3) throw new SinedException("Arquivo inv�lido. Linhas com mais de 3 colunas.");
			
			try{
				aux = SinedDateUtils.stringToDate(campos[2], "yyyy-MM-dd hh:mm:ss.SSS");
			} catch (Exception e) {
				throw new SinedException("Data inv�lida. " + campos[2]);
			}
			if(aux == null) throw new SinedException("Data inv�lida. " + campos[2]);
			
			vo = new AtualizarHistoricoVO();
			vo.setEmail(campos[0]);
			vo.setDtenvio(new Timestamp(aux.getTime()));
			
			lista.add(vo);
		}
		
		List<Lead> listaLead;
		List<Contacrm> listaConta;
		String obs;
		Leadhistorico lh;
		Contacrmhistorico ch;
		int contador = 0;
		
		for (AtualizarHistoricoVO atualizar : lista) {
			listaLead = leadService.findByEmail(atualizar.getEmail());
			listaConta = this.findByEmail(atualizar.getEmail());
			
			obs = this.montaObservacao(atualizar);

			for (Lead l : listaLead) {
				if(!leadhistoricoService.haveHistorico(l, atualizar.getDtenvio(), obs)){
					lh = new Leadhistorico();
					lh.setLead(l);
					lh.setObservacao(obs);
					lh.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					lh.setDtaltera(atualizar.getDtenvio());
					
					leadhistoricoService.saveOrUpdateWithoutLog(lh);
					contador++;
				}
			}
			
			for (Contacrm c : listaConta) {
				if(!contacrmhistoricoService.haveHistorico(c, atualizar.getDtenvio(), obs)){
					ch = new Contacrmhistorico();
					ch.setContacrm(c);
					ch.setObservacao(obs);
					ch.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
					ch.setDtaltera(atualizar.getDtenvio());
					
					contacrmhistoricoService.saveOrUpdateWithoutLog(ch);
					contador++;
				}
			}
		}
		
		return contador;
	}
	
	private List<Contacrm> findByEmail(String email) {
		return contacrmDAO.findByEmail(email);
	}
	
	private String montaObservacao(AtualizarHistoricoVO atualizar) {
		if(atualizar != null && atualizar.getEmail() != null && atualizar.getDtenvio() != null){
			StringBuilder sb = new StringBuilder();
			
			sb.append("E-mail marketing enviado para '");
			sb.append(atualizar.getEmail());
			sb.append("'.");
			
			return sb.toString();
		}
		return null;
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContacrmDAO#findForWebservice
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Contacrm> findForWebservice(String nome) {
		return contacrmDAO.findForWebservice(nome);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Carrega os dados da contacrm com a lista de oportunidade e situacao 
	*
	* @see	br.com.linkcom.sined.geral.dao.ContacrmDAO#carregaContacrmcontatoOportunidadeSituacao(String whereIn)
	*
	* @param whereIn
	* @return
	* @since Aug 3, 2011
	* @author Luiz Fernando F Silva
	*/
	public Contacrm carregaContacrmcontatoOportunidadeSituacao(String whereIn){
		return contacrmDAO.carregaContacrmcontatoOportunidadeSituacao(whereIn);
	}
	
	/**
	* M�todo com referencia no DAO
	* Busca UF/MUNICIPIO do colaborador cadastrado como responsavel na conta
	* 
	* @see	br.com.linkcom.sined.geral.dao.ContacrmDAO#buscaEnderecoColaborador(Colaborador colaborador, Uf uf)
	*
	* @param colaborador
	* @param uf
	* @return
	* @since Oct 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Contacrm> buscaEnderecoColaborador(Colaborador colaborador, Uf uf) {
		return contacrmDAO.buscaEnderecoColaborador(colaborador, uf);
	}
	
	public List<Contacrm> findContacrmByLeads(String whereIn) {
		return contacrmDAO.findContacrmByLeads(whereIn);
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * 
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Rafael Salvio
	 */
	public List<Contacrm> findAllForDesassociarCampanha(String whereIn) throws Exception{
		return contacrmDAO.findAllForDesassociarCampanha(whereIn);
	}
	
	public List<Contacrm> findForControleInteracao(ControleInteracaoBean bean){
		return contacrmDAO.findForControleInteracao(bean);
	}
	
	/**
	 * M�todo que retorna o hist�rico de contacrm mais recente e que possua situa��o definida.
	 * 
	 * @param lista
	 * @param datainicio
	 * @param datafim
	 * @return
	 * @author Rafael Salvio
	 */
	public Contacrmhistorico buscaMaiorDataHistoricoContacrm(List<Contacrmhistorico> lista, Timestamp datainicio, Timestamp datafim){
		Contacrmhistorico contacrmhistorico = null;

		for(Contacrmhistorico ch : lista){
			if(ch.getDtaltera().compareTo(datainicio) >= 0 && ch.getDtaltera().compareTo(datafim) < 0 && ch.getSituacaohistorico() != null 
					&& ch.getAtividadetipo() != null && !(Boolean.TRUE.equals(ch.getAtividadetipo().getNaoexibirnopainel()))){
				if(contacrmhistorico == null){
					contacrmhistorico = ch;
				} else if(ch.getDtaltera().compareTo(contacrmhistorico.getDtaltera()) > 0){
					contacrmhistorico = ch;
				}
			}
		}
		return contacrmhistorico;
	}
	
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @see  br.com.linkcom.sined.geral.dao.ContacrmDAO#loadForMakeOportunidadeRTF(Contacrm)
	 * @param conta
	 * @return 
	 * @author Matheus Santos
	 */
	public Contacrm loadForMakeOportunidadeRTF(Contacrm conta){
		return contacrmDAO.loadForMakeOportunidadeRTF(conta);
	}
	
	public Contacrm findForContato(Contacrm conta){
		return contacrmDAO.findForContato(conta);
	}
	
	public List<Contacrm> findWithResponsavel(String whereIn){
		return contacrmDAO.findWithResponsavel(whereIn);
	}
	
	public boolean usuarioPodeEditarContacrm(String whereInContacrm){
		if(SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado())){
			for(Contacrm contacrm: this.findWithResponsavel(whereInContacrm)){
				if(!SinedUtil.getUsuarioLogado().equals(contacrm.getResponsavel())){
					return false;
				}
			}
		}
		return true;
	}
	
	public Contacrm loadWithResponsavel(Integer cdcontacrm){
		return this.findWithResponsavel(cdcontacrm.toString()).get(0);
	}
	
	public void processarListagem(ListagemResult<Contacrm> listagemResult, ContacrmFiltro filtro) {
		List<Contacrm> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontacrm",",");
		if (whereIn != null && !whereIn.equals("")) {
			list.removeAll(list);
			list.addAll(loadWithLista(whereIn, filtro.getOrderBy(), filtro.isAsc()));
		}

		boolean isRestricaoClienteVendedor = SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado());
		for (Contacrm conta : list) {
			if (conta.getListcontacrmcontato() != null) {
				String email = "";
				for (Contacrmcontato contato : conta.getListcontacrmcontato()) {
					if (contato.getListcontacrmcontatoemail() != null) {
						email = email + CollectionsUtil.listAndConcatenate(contato.getListcontacrmcontatoemail(),"email", "<br>") + "<br>";
					}

					conta.setListaEmails(email);

					if (contato.getListcontacrmcontatofone() != null) {
						String strFone = "";
						for (Contacrmcontatofone fone : contato.getListcontacrmcontatofone()) {
							strFone = strFone + fone.getTelefone() + " ("
									+ fone.getTelefonetipo().getNome() + ")<br>";
						}
						if (!strFone.equals("")) {
							strFone = strFone.substring(0, (strFone.length() - 4));
						}
						conta.setListaFones(strFone);
					}
				}
			}
			conta.setPermitidoEdicao(!isRestricaoClienteVendedor ||
					(conta.getResponsavel() != null && conta.getResponsavel().getCdpessoa() != null &&  SinedUtil.getUsuarioLogado().equals(conta.getResponsavel())));
		}
	}
	
	public Resource gerarListagemCSV(ContacrmFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		
		ListagemResult<Contacrm> listagemResult = findForExportacao(filtro);
		processarListagem(listagemResult, filtro);
		List<Contacrm> lista = listagemResult.list();
		
		String cabecalho = "\"Nome\";\"Tipo de pessoa\";\"Respons�vel\";\"E-mails\";\"Telefones\";\n";
		StringBuilder csv = new StringBuilder(cabecalho);
		
		for(Contacrm bean : lista) {
			
			csv.append("\"" + bean.getNome() + "\";");
			csv.append("\"" + bean.getTipo() + "\";");
			
			if(bean.getTiporesponsavel() != null && bean.getTiporesponsavel().equals(Tiporesponsavel.AGENCIA)) {
				csv.append("\"" + bean.getAgencia().getNome() + "\";");
			} else if(bean.getTiporesponsavel() != null && bean.getTiporesponsavel().equals(Tiporesponsavel.COLABORADOR)) {
				csv.append("\"" + bean.getResponsavel().getNome() + "\";");
			} else {
				csv.append("\"" + (bean.getResponsavel() != null ? bean.getResponsavel().getNome() : "") + 
						(bean.getAgencia() != null ? bean.getAgencia().getNome() : "") + "\";");
			}
			
			csv.append("\"" + (bean.getListaEmails() != null ? bean.getListaEmails().replace("<br>", "\n") : "") + "\";");
			csv.append("\"" + (bean.getListaFones() != null ? bean.getListaFones().replace("<br>", "\n") : "") + "\";");
			csv.append("\n");
		}
		
		Resource resource = new Resource("text/csv", "conta.csv", csv.toString().getBytes());
		return resource;
	}
}
