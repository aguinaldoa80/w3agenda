package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacaohistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Contratofaturalocacaoacao;
import br.com.linkcom.sined.geral.dao.ContratofaturalocacaohistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContratofaturalocacaohistoricoService extends GenericService<Contratofaturalocacaohistorico>{

	private ContratofaturalocacaohistoricoDAO contratofaturalocacaohistoricoDAO;
	
	public void setContratofaturalocacaohistoricoDAO(
			ContratofaturalocacaohistoricoDAO contratofaturalocacaohistoricoDAO) {
		this.contratofaturalocacaohistoricoDAO = contratofaturalocacaohistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContratofaturalocacaohistoricoDAO#findByContratofaturalocacao(Contratofaturalocacao contratofaturalocacao)
	 *
	 * @param contratofaturalocacao
	 * @return
	 * @author Rodrigo Freitas
	 * @since 18/07/2013
	 */
	public List<Contratofaturalocacaohistorico> findByContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		return contratofaturalocacaohistoricoDAO.findByContratofaturalocacao(contratofaturalocacao);
	}
	
	public void createAndSaveHitorico(Contratofaturalocacao bean, String obs, boolean isCriar) {
		Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
		contratofaturalocacaohistorico.setAcao(isCriar ? Contratofaturalocacaoacao.EMITIDA : Contratofaturalocacaoacao.ALTERADA);
		contratofaturalocacaohistorico.setContratofaturalocacao(bean);
		contratofaturalocacaohistorico.setObservacao(obs.toString());
		saveOrUpdate(contratofaturalocacaohistorico);
	}
	
	public void createAndSaveHitorico(Contratofaturalocacao bean, String obs, Contratofaturalocacaoacao acao) {
		Contratofaturalocacaohistorico contratofaturalocacaohistorico = new Contratofaturalocacaohistorico();
		contratofaturalocacaohistorico.setAcao(acao);
		contratofaturalocacaohistorico.setContratofaturalocacao(bean);
		contratofaturalocacaohistorico.setObservacao(obs.toString());
		saveOrUpdate(contratofaturalocacaohistorico);
	}
}
