package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Formapagamento;
import br.com.linkcom.sined.geral.dao.ContatipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.rest.android.contatipo.ContatipoRESTModel;

public class ContatipoService extends GenericService<Contatipo> {

	private ContatipoDAO contatipoDAO;
	
	public void setContatipoDAO(ContatipoDAO contatipoDAO) {
		this.contatipoDAO = contatipoDAO;
	}

	/**
	 * Retorna uma Contatipo a partir de um agendamento. Pode retornar null.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContatipoDAO#carregarPor(Agendamento)
	 * @param agendamento
	 * @return
	 * @author Hugo Ferreira
	 */
	public Contatipo carregarPor(Agendamento agendamento) {
		return contatipoDAO.carregarPor(agendamento);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ContatipoDAO#findByEmpresa(Empresa)
	 * @param empresa
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Contatipo> findByEmpresa(Empresa empresa){
		return contatipoDAO.findByEmpresa(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ContatipoDAO#findForComboMovimentacao(String whereInContatipo, String whereInEmpresas)
	 *
	 * @param whereInContatipo
	 * @param whereInEmpresas
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contatipo> findForComboMovimentacao(String whereInContatipo, String whereInEmpresas){
		return contatipoDAO.findForComboMovimentacao(whereInContatipo,whereInEmpresas);
	}
	
	/**
	 * M�todo que busca a lista de contatipo da forma de pagamento
	 *
	 * @param formapagamento
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Contatipo> findForMovimentacao(Formapagamento formapagamento){
		List<Contatipo> listaContatipo = new ArrayList<Contatipo>();
		StringBuilder whereInContatipo = new StringBuilder();
		if(formapagamento != null){
			if (formapagamento.getCdformapagamento().equals(Formapagamento.CAIXA.getCdformapagamento())) {
				listaContatipo.add(this.load(new Contatipo(Contatipo.CAIXA)));
			} else if (formapagamento.getCdformapagamento().equals(Formapagamento.DINHEIRO.getCdformapagamento())) {
				if(!"".equals(whereInContatipo.toString()))
					whereInContatipo.append(",");
				whereInContatipo.append(Contatipo.CAIXA);
				if(!"".equals(whereInContatipo.toString()))
					whereInContatipo.append(",");
				whereInContatipo.append(Contatipo.CONTA_BANCARIA);			
			} else if (formapagamento.getCdformapagamento().equals(Formapagamento.CARTAOCREDITO.getCdformapagamento())) {
				if(!"".equals(whereInContatipo.toString()))
					whereInContatipo.append(",");
				whereInContatipo.append(Contatipo.CARTAO_DE_CREDITO);
			} else {
				if(!"".equals(whereInContatipo.toString()))
					whereInContatipo.append(",");
				whereInContatipo.append(Contatipo.CAIXA);
				if(!"".equals(whereInContatipo.toString()))
					whereInContatipo.append(",");
				whereInContatipo.append(Contatipo.CONTA_BANCARIA);		
			}
		}		
		if(whereInContatipo != null && !"".equals(whereInContatipo.toString()))
			listaContatipo = this.findForComboMovimentacao(whereInContatipo.toString(), formapagamento.getEmpresaWhereIn());
		
		return listaContatipo;
	}
	
	public List<ContatipoRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}
	
	public List<ContatipoRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ContatipoRESTModel> lista = new ArrayList<ContatipoRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Contatipo bean : contatipoDAO.findForAndroid(whereIn))
				lista.add(new ContatipoRESTModel(bean));
		}
		
		return lista;
	}
}
