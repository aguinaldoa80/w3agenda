package br.com.linkcom.sined.geral.service;

import java.util.Set;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Tarefaarquivo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TarefaarquivoService extends GenericService<Tarefaarquivo> {	
	
	protected ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	
	/* singleton */
	private static TarefaarquivoService instance;
	public static TarefaarquivoService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TarefaarquivoService.class);
		}
		return instance;
	}
	
	
	public void saveArquivos(Tarefa tarefa) {
		if (tarefa == null) {
			throw new SinedException("O par�metro tarefa n�o pode ser null.");
		}
		Set<Tarefaarquivo> listaTarefaarquivo = tarefa.getListaTarefaarquivo();
		if (listaTarefaarquivo != null && listaTarefaarquivo.size() > 0) {
			for (Tarefaarquivo tarefaarquivo : listaTarefaarquivo) {
				arquivoDAO.saveFile(tarefaarquivo, "arquivo");
			}
		}
	}
}
