package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Contacrmcontato;
import br.com.linkcom.sined.geral.bean.Contacrmcontatofone;
import br.com.linkcom.sined.geral.dao.ContacrmcontatofoneDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ContacrmcontatofoneService extends GenericService<Contacrmcontatofone>{
	
	private ContacrmcontatofoneDAO contacrmcontatofoneDAO;
	
	public void setContacrmcontatofoneDAO(
			ContacrmcontatofoneDAO contacrmcontatofoneDAO) {
		this.contacrmcontatofoneDAO = contacrmcontatofoneDAO;
	}
	
	public List<Contacrmcontatofone> findByContacrmcontato(Contacrmcontato contacrmcontato){
		return contacrmcontatofoneDAO.findByContacrmcontato(contacrmcontato);
	}
	

}
