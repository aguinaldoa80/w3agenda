package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.NotafiscalprodutoProducaoordem;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoProducaoordemDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoProducaoordemService extends GenericService<NotafiscalprodutoProducaoordem> {
	
	private NotafiscalprodutoProducaoordemDAO notafiscalprodutoProducaoordemDAO;
	
	public void setNotafiscalprodutoProducaoordemDAO(NotafiscalprodutoProducaoordemDAO notafiscalprodutoProducaoordemDAO) {
		this.notafiscalprodutoProducaoordemDAO = notafiscalprodutoProducaoordemDAO;
	}

	public boolean haveNotaDevolucaoNaoCanceladaByProducaoordem(String whereInProducaoordem, String whereInPedidovendamaterial) {
		return notafiscalprodutoProducaoordemDAO.haveNotaDevolucaoNaoCanceladaByProducaoordem(whereInProducaoordem, whereInPedidovendamaterial);
	}

}
