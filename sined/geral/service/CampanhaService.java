package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhatipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.dao.CampanhaDAO;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.CampanhaCSVBean;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CampanhaService extends GenericService<Campanha>{
	
	protected CampanhaDAO campanhaDAO;
	
	public void setCampanhaDAO(CampanhaDAO campanhaDAO) {
		this.campanhaDAO = campanhaDAO;
	}
	
	public Campanha carregaCampanha(String whereIn) {
		return campanhaDAO.carregaCampanha(whereIn);
	}
	
	public Resource prepararArquivoCampanhaCSV(String whereIn){
		List<CampanhaCSVBean> listaCampanhacsvbean = this.findForCSV(whereIn);
		
		String csv = "Nome;E-Mail\n";
		
		for (CampanhaCSVBean campanhaCSVBean : listaCampanhacsvbean) {
			csv += campanhaCSVBean.getNome();
			csv += ";";
			csv += campanhaCSVBean.getEmail();
			csv += "\n";
		}
		Resource resource = new Resource("text/csv", "campanha_" + SinedUtil.datePatternForReport() + ".csv", csv.getBytes());
		return resource;
	}
	
	
	private List<CampanhaCSVBean> findForCSV(String whereIn) {
		
		return campanhaDAO.findForCSV(whereIn);
	}

	public List<Campanha> findByOportunidadeForCombo(Oportunidade oportunidade){
		return campanhaDAO.findByContaForCombo(oportunidade);
	}

	public List<Campanha> findByContacrmForCombo(Contacrm contacrm) {
		return campanhaDAO.findByContacrmForCombo(contacrm);
	}
	
	public List<Campanha> findByCampanhatipo(Campanhatipo campanhatipo){
		return campanhaDAO.findByCampanhatipo(campanhatipo);
	}

	public Campanha findByName(String campanha) {
		return campanhaDAO.findByName(campanha);
	}

}
