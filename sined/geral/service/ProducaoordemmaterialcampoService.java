package br.com.linkcom.sined.geral.service;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.geral.bean.Producaoetapanomecampo;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterial;
import br.com.linkcom.sined.geral.bean.Producaoordemmaterialcampo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocampo;
import br.com.linkcom.sined.geral.dao.ProducaoordemmaterialcampoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ProducaoordemmaterialcampoService extends GenericService<Producaoordemmaterialcampo> {

	private ProducaoordemmaterialcampoDAO producaoordemmaterialcampoDAO;
	private ProducaoetapanomeService producaoetapanomeService;
	
	public void setProducaoordemmaterialcampoDAO(ProducaoordemmaterialcampoDAO producaoordemmaterialcampoDAO) {
		this.producaoordemmaterialcampoDAO = producaoordemmaterialcampoDAO;
	}
	public void setProducaoetapanomeService(ProducaoetapanomeService producaoetapanomeService) {
		this.producaoetapanomeService = producaoetapanomeService;
	}

	/**
	* M�todo que carrega os campos adicionais do item da ordem de produ��o
	*
	* @see br.com.linkcom.sined.geral.service.ProducaoordemmaterialcampoService.findByProducaoordemmaterial(String whereIn)
	*
	* @param producaoordemmaterial
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterialcampo> findByProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial){
		if(producaoordemmaterial == null || producaoordemmaterial.getCdproducaoordemmaterial() == null)
			throw new SinedException("Ordem de produ��o n�o pode se nula");
		
		return findByProducaoordemmaterial(producaoordemmaterial.getCdproducaoordemmaterial().toString());
	}
	
	/**
	* M�todo com refer�ncia no DAO
	*
	* @see br.com.linkcom.sined.geral.dao.ProducaoordemmaterialcampoDAO#findByProducaoordemmaterial(String whereIn)
	*
	* @param whereIn
	* @return
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public List<Producaoordemmaterialcampo> findByProducaoordemmaterial(String whereIn){
		return producaoordemmaterialcampoDAO.findByProducaoordemmaterial(whereIn);
	}
	
	/**
	* M�todo que preenche os itens da ordem de produ��o com os campos adicionais
	*
	* @param producaoordem
	* @since 23/02/2016
	* @author Luiz Fernando
	*/
	public void preencherListaCamposAdicionais(Set<Producaoordemmaterial> listaProducaoordemmaterial){
		if(SinedUtil.isListNotEmpty(listaProducaoordemmaterial)){
			StringBuilder whereIn = new StringBuilder();
			for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
				if(producaoordemmaterial.getListaCampo() == null || !Hibernate.isInitialized(producaoordemmaterial.getListaCampo())){ 
					producaoordemmaterial.setListaCampo(new ListSet<Producaoordemmaterialcampo>(Producaoordemmaterialcampo.class));
				}
				if(producaoordemmaterial.getCdproducaoordemmaterial() != null){
					whereIn.append(producaoordemmaterial.getCdproducaoordemmaterial()).append(",");
				}
			}
			if(StringUtils.isNotBlank(whereIn.toString())){
				List<Producaoordemmaterialcampo> lista = findByProducaoordemmaterial(whereIn.substring(0, whereIn.length()-1));
				if(SinedUtil.isListNotEmpty(lista)){
					for(Producaoordemmaterialcampo pomc : lista){
						if(pomc.getProducaoordemmaterial() != null && pomc.getProducaoordemmaterial().getCdproducaoordemmaterial() != null){
							for(Producaoordemmaterial pom : listaProducaoordemmaterial){
								if(pomc.getProducaoordemmaterial().getCdproducaoordemmaterial().equals(pom.getCdproducaoordemmaterial())){
									if(pom.getListaCampo() == null) pom.setListaCampo(new ListSet<Producaoordemmaterialcampo>(Producaoordemmaterialcampo.class));
									pom.getListaCampo().add(pomc);
								}
							}
						}
					}
				}
			}
			
			HashMap<Producaoetapanome, Set<Producaoetapanomecampo>> mapCampoAdicional = new HashMap<Producaoetapanome, Set<Producaoetapanomecampo>>();
			for(Producaoordemmaterial producaoordemmaterial : listaProducaoordemmaterial){
				if(producaoordemmaterial.getProducaoetapaitem() != null &&
						producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome() != null){
					Set<Producaoetapanomecampo> listaCamposAdicionais = null;
					if(mapCampoAdicional.containsKey(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome())){
						listaCamposAdicionais = mapCampoAdicional.get(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome());
					}else {
						Producaoetapanome producaoetapanome = producaoetapanomeService.loadWithCamposAdicionais(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome());
						if(producaoetapanome != null){
							listaCamposAdicionais = producaoetapanome.getListaProducaoetapanomecampo();
						}
					}
					if(SinedUtil.isListNotEmpty(listaCamposAdicionais)){
						Set<Producaoordemmaterialcampo> listaCampo = new ListSet<Producaoordemmaterialcampo>(Producaoordemmaterialcampo.class);
						boolean adicionar = true; 
						for(Producaoetapanomecampo penc : listaCamposAdicionais){
							adicionar = true;
							if(SinedUtil.isListNotEmpty(producaoordemmaterial.getListaCampo())){
								for(Producaoordemmaterialcampo producaoordemmaterialcampo : producaoordemmaterial.getListaCampo()){
									if(producaoordemmaterialcampo.getProducaoetapanomecampo() != null && producaoordemmaterialcampo.getProducaoetapanomecampo().getObrigatorio() != null && 
											producaoordemmaterialcampo.getProducaoetapanomecampo().getObrigatorio() && producaoordemmaterialcampo.getValorboleano() == null &&
											Tipocampo.BOLEANO.equals(producaoordemmaterialcampo.getProducaoetapanomecampo().getTipo())){
										producaoordemmaterialcampo.setValorboleano(false);
									}
									if(producaoordemmaterialcampo.getProducaoetapanomecampo() != null && 
											producaoordemmaterialcampo.getProducaoetapanomecampo().getCdproducaoetapanomecampo() != null &&
											producaoordemmaterialcampo.getProducaoetapanomecampo().getCdproducaoetapanomecampo().equals(penc.getCdproducaoetapanomecampo())){
										adicionar = false;
										listaCampo.add(producaoordemmaterialcampo);
										break;
									}
								}
							}
							if(adicionar){
								Producaoordemmaterialcampo pomc = new Producaoordemmaterialcampo();
								pomc.setProducaoordemmaterial(producaoordemmaterial);
								pomc.setProducaoetapanomecampo(penc);
								if(pomc.getProducaoetapanomecampo() != null && pomc.getProducaoetapanomecampo().getObrigatorio() != null && 
										pomc.getProducaoetapanomecampo().getObrigatorio() && pomc.getValorboleano() == null &&
										Tipocampo.BOLEANO.equals(pomc.getProducaoetapanomecampo().getTipo())){
									pomc.setValorboleano(false);
								}
								listaCampo.add(pomc);
							}
						}
						producaoordemmaterial.setListaCampo(listaCampo);
					}
					mapCampoAdicional.put(producaoordemmaterial.getProducaoetapaitem().getProducaoetapanome(), listaCamposAdicionais);
				}
			}
		}
	}
}
