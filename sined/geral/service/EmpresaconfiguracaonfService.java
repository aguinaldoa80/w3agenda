package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonf;
import br.com.linkcom.sined.geral.dao.EmpresaconfiguracaonfDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmpresaconfiguracaonfService extends GenericService<Empresaconfiguracaonf>{
	EmpresaconfiguracaonfDAO empresaconfiguracaonfDAO;
	
	public void setEmpresaconfiguracaonfDAO(EmpresaconfiguracaonfDAO empresaconfiguracaonfDAO) {
		this.empresaconfiguracaonfDAO = empresaconfiguracaonfDAO;
	}
	
	public Boolean verificaConfiguracao(){
		return empresaconfiguracaonfDAO.verificaConfiguracao();
	}
}
