package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.DocumentoApropriacao;
import br.com.linkcom.sined.geral.dao.DocumentoApropriacaoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoApropriacaoService extends GenericService<DocumentoApropriacao>{

	private DocumentoApropriacaoDAO documentoApropriacaoDAO;
	
	public void setDocumentoApropriacaoDAO(
			DocumentoApropriacaoDAO documentoApropriacaoDAO) {
		this.documentoApropriacaoDAO = documentoApropriacaoDAO;
	}
	
	public List<DocumentoApropriacao> findByDocumento(Documento documento){
		return documentoApropriacaoDAO.findByDocumento(documento);
	}
}
