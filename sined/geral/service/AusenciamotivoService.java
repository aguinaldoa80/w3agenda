package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.geral.dao.AusenciamotivoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class AusenciamotivoService extends GenericService<Ausenciamotivo>{
	
	private AusenciamotivoDAO ausenciamotivoDAO;

	public void setAusenciamotivoDAO(AusenciamotivoDAO ausenciamotivoDAO) {
		this.ausenciamotivoDAO = ausenciamotivoDAO;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.dao.AusenciamotivoDAO#findForComboFlex()
	 *
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Ausenciamotivo> findForComboFlex(){
		return ausenciamotivoDAO.findForComboFlex();
	}
	
}
