package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeHistorico;
import br.com.linkcom.sined.geral.dao.MdfeHistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MdfeHistoricoService extends GenericService<MdfeHistorico>{

	private MdfeHistoricoDAO mdfeHistoricoDAO;

	public void setMdfeHistoricoDAO(MdfeHistoricoDAO mdfeHistoricoDAO) {
		this.mdfeHistoricoDAO = mdfeHistoricoDAO;
	}
	
	public List<MdfeHistorico> findByMdfe(Mdfe mdfe){
		return mdfeHistoricoDAO.findByMdfe(mdfe);
	}
}
