package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendahistorico;
import br.com.linkcom.sined.geral.dao.EmporiumvendahistoricoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class EmporiumvendahistoricoService extends GenericService<Emporiumvendahistorico> {

	private EmporiumvendahistoricoDAO emporiumvendahistoricoDAO;
	
	public void setEmporiumvendahistoricoDAO(
			EmporiumvendahistoricoDAO emporiumvendahistoricoDAO) {
		this.emporiumvendahistoricoDAO = emporiumvendahistoricoDAO;
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.EmporiumvendahistoricoDAO#findByEmporiumvenda(Emporiumvenda emporiumvenda)
	 *
	 * @param emporiumvenda
	 * @return
	 * @author Rodrigo Freitas
	 * @since 16/04/2013
	 */
	public List<Emporiumvendahistorico> findByEmporiumvenda(Emporiumvenda emporiumvenda) {
		return emporiumvendahistoricoDAO.findByEmporiumvenda(emporiumvenda);
	}

}
