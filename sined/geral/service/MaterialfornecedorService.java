package br.com.linkcom.sined.geral.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialfornecedor;
import br.com.linkcom.sined.geral.bean.Ordemcompramaterial;
import br.com.linkcom.sined.geral.dao.MaterialfornecedorDAO;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class MaterialfornecedorService extends GenericService<Materialfornecedor> {

	private MaterialfornecedorDAO materialfornecedorDAO;
	
	public void setMaterialfornecedorDAO(
			MaterialfornecedorDAO materialfornecedorDAO) {
		this.materialfornecedorDAO = materialfornecedorDAO;
	}
	
	public List<Materialfornecedor> findByMaterial(Material form) {
		return materialfornecedorDAO.findByMaterial(form);
	}
	
	public void saveIfNotExists(Material material, Fornecedor fornecedor, String cprod) {
		if(!this.haveMaterialFornecedor(material, fornecedor, cprod)){
			Materialfornecedor materialfornecedor = new Materialfornecedor();
			materialfornecedor.setFornecedor(fornecedor);
			materialfornecedor.setMaterial(material);
			materialfornecedor.setCodigo(cprod);
			this.saveOrUpdate(materialfornecedor);
		}
	}

	private boolean haveMaterialFornecedor(Material material, Fornecedor fornecedor, String cprod) {
		return materialfornecedorDAO.haveMaterialFornecedor(material, fornecedor, cprod);
	}
	
	public List<Materialfornecedor> findCodigoAlternativo(Material material, Fornecedor fornecedor) {
		return materialfornecedorDAO.findCodigoAlternativo(material, fornecedor);
	}
	
	public String getCodigoAlternativo(Material material, Fornecedor fornecedor){
		
		List<Materialfornecedor> lista = findCodigoAlternativo(material, fornecedor);
		
		if(SinedUtil.isListEmpty(lista)){ return null;}
		
		if (lista.size() > 1) {
			List<String> listaExemplo = new ArrayList<String>();
	        for(Materialfornecedor mf: lista){
	            if(StringUtils.isNotBlank(mf.getCodigo()) && !listaExemplo.contains(mf.getCodigo())){
	                listaExemplo.add(mf.getCodigo());
	            }
	        }
	        if (listaExemplo.size() == 1) {
	        	return listaExemplo.get(0);
			}else {
				return null;
			}
		}else {
			return lista.get(0).getCodigo();
		}
					
	}

}
