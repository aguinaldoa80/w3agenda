package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Tipoexame;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipoexameService extends GenericService<Tipoexame> {	
	
	/* singleton */
	private static TipoexameService instance;
	public static TipoexameService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipoexameService.class);
		}
		return instance;
	}
	
}
