package br.com.linkcom.sined.geral.service;

import br.com.linkcom.sined.geral.bean.Arquivoretornocontrato;
import br.com.linkcom.sined.geral.dao.ArquivoretornocontratoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class ArquivoretornocontratoService extends GenericService<Arquivoretornocontrato> {

	private ArquivoretornocontratoDAO arquivoretornocontratoDAO;
	
	public void setArquivoretornocontratoDAO(ArquivoretornocontratoDAO arquivoretornocontratoDAO) {this.arquivoretornocontratoDAO = arquivoretornocontratoDAO;}

	public boolean existeArquivo(String numeroremessa) {
		return arquivoretornocontratoDAO.existeArquivo(numeroremessa);
	}
}
