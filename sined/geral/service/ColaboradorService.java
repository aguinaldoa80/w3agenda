package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.Region;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.report.IReport;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoImageResolver;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargomaterialseguranca;
import br.com.linkcom.sined.geral.bean.Categoriacnh;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Colaboradoracidente;
import br.com.linkcom.sined.geral.bean.Colaboradorcargo;
import br.com.linkcom.sined.geral.bean.Colaboradorcategoriacnh;
import br.com.linkcom.sined.geral.bean.Colaboradorequipamento;
import br.com.linkcom.sined.geral.bean.Colaboradorexame;
import br.com.linkcom.sined.geral.bean.Colaboradorsituacao;
import br.com.linkcom.sined.geral.bean.Colaboradorstatusfolha;
import br.com.linkcom.sined.geral.bean.Colaboradortipo;
import br.com.linkcom.sined.geral.bean.Contacrm;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Contratocolaborador;
import br.com.linkcom.sined.geral.bean.Dadobancario;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Empresacodigocnae;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoquetipo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tarefa;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipocarteiratrabalho;
import br.com.linkcom.sined.geral.bean.Tipoconta;
import br.com.linkcom.sined.geral.bean.Tipoinscricao;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Turma;
import br.com.linkcom.sined.geral.bean.Turmaparticipante;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Veiculo;
import br.com.linkcom.sined.geral.bean.auxiliar.ColaboradorVO;
import br.com.linkcom.sined.geral.bean.auxiliar.PeriodoVO;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoetiqueta;
import br.com.linkcom.sined.geral.bean.view.Vwcolaboradoretiqueta;
import br.com.linkcom.sined.geral.dao.ColaboradorDAO;
import br.com.linkcom.sined.modulo.pub.controller.process.bean.soap.retorno.ColaboradorWSBean;
import br.com.linkcom.sined.modulo.rh.controller.crud.bean.FormularioPPPBean;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.VwcolaboradoretiquetaFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.ProcessararquivopontoProcess;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.Colaboradorholerite;
import br.com.linkcom.sined.modulo.rh.controller.process.bean.ImportarcolaboradoresFPWBean;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.AlterarsituacaocolaboradorFilter;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.ExportarcolaboradorFiltro;
import br.com.linkcom.sined.modulo.rh.controller.process.filter.HoleriteFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.bean.ColaboradorEtiqueta;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.ColaboradorReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EmitirautorizacaoReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.EntregaEPIReportFiltro;
import br.com.linkcom.sined.modulo.rh.controller.report.filter.FormularioPPPReportFiltro;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.ColaboradoragendaservicoReportBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.filter.ColaboradoragendaservicoReportFiltro;
import br.com.linkcom.sined.util.ObjectUtils;
import br.com.linkcom.sined.util.RedimensionaImagem;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.offline.ColaboradorOfflineJSON;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.sined.util.rest.android.colaborador.ColaboradorRESTModel;
import br.com.linkcom.sined.util.tag.TagFunctions;
import br.com.linkcom.utils.DateUtils;


public class ColaboradorService extends GenericService<Colaborador> {

	private ColaboradorDAO colaboradorDAO;
	private ColaboradorService colaboradorService;
	private ColaboradorcargoService colaboradorcargoService;
	private ArquivoService arquivoService;
	private ExametipoService exametipoService;
	private ExameclinicaService exameclinicaService;
	private CategoriacnhService categoriacnhService;
	private TurmaparticipanteService turmaparticipanteService;
	private TurmaService turmaService;
	private PessoaService pessoaService;
	private GrupoemailService grupoemailService;
	private GrupoemailColaboradorService grupoemailColaboradorService;
	private ContratocolaboradorService contratocolaboradorService;
	private EmpresaService empresaService;
	private NeoImageResolver neoImageResolver;
	private EnderecoService enderecoService;
	private TelefoneService telefoneService;
	private ExameresponsavelService exameresponsavelService;
	private ExameconvenioService exameconvenioService;
	private VwcolaboradoretiquetaService vwcolaboradoretiquetaService;
	private UsuarioService usuarioService;
	private ColaboradorequipamentoService colaboradorequipamentoService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private DadobancarioService dadobancarioService;
	
	public static String SINED_LIST_COL = "";
	
	public void setContratocolaboradorService(ContratocolaboradorService contratocolaboradorService) {this.contratocolaboradorService = contratocolaboradorService;}
	public void setColaboradorDAO(ColaboradorDAO colaboradorDAO) {this.colaboradorDAO = colaboradorDAO;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setColaboradorcargoService(ColaboradorcargoService colaboradorcargoService) {this.colaboradorcargoService = colaboradorcargoService;}
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setExametipoService(ExametipoService exametipoService) {this.exametipoService = exametipoService;}
	public void setExameclinicaService(ExameclinicaService exameclinicaService) {this.exameclinicaService = exameclinicaService;}
	public void setCategoriacnhService(CategoriacnhService categoriacnhService) {this.categoriacnhService = categoriacnhService;}
	public void setTurmaparticipanteService(TurmaparticipanteService turmaparticipanteService) {this.turmaparticipanteService = turmaparticipanteService;}
	public void setTurmaService(TurmaService turmaService) {this.turmaService = turmaService;}
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setGrupoemailService(GrupoemailService grupoemailService) {this.grupoemailService = grupoemailService;}
	public void setGrupoemailColaboradorService(GrupoemailColaboradorService grupoemailColaboradorService) {this.grupoemailColaboradorService = grupoemailColaboradorService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNeoImageResolver(NeoImageResolver neoImageResolver) {this.neoImageResolver = neoImageResolver;}
	public void setEnderecoService(EnderecoService enderecoService) {this.enderecoService = enderecoService;}
	public void setTelefoneService(TelefoneService telefoneService) {this.telefoneService = telefoneService;}
	public void setExameresponsavelService(ExameresponsavelService exameresponsavelService) {this.exameresponsavelService = exameresponsavelService;}
	public void setExameconvenioService(ExameconvenioService exameconvenioService) {this.exameconvenioService = exameconvenioService;}
	public void setVwcolaboradoretiquetaService(VwcolaboradoretiquetaService vwcolaboradoretiquetaService) {this.vwcolaboradoretiquetaService = vwcolaboradoretiquetaService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setColaboradorequipamentoService(ColaboradorequipamentoService colaboradorequipamentoService) {this.colaboradorequipamentoService = colaboradorequipamentoService;}
	public void setMovimentacaoestoqueService(MovimentacaoestoqueService movimentacaoestoqueService) {this.movimentacaoestoqueService = movimentacaoestoqueService;}
	public void setDadobancarioService(DadobancarioService dadobancarioService) {this.dadobancarioService = dadobancarioService;}

	/* singleton */
	private static ColaboradorService instance;
	public static ColaboradorService getInstance() {
		if(instance == null){
			instance = Neo.getObject(ColaboradorService.class);
		}
		return instance;
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findColaboradorusuario(Integer)
	 * @param pessoa
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findColaboradorusuario(Integer cdpessoa){
		return colaboradorDAO.findColaboradorusuario(cdpessoa);
	}
	
	@Override
	public void saveOrUpdate(Colaborador bean) {
		//carega o arquivo
		if(bean.getFoto() != null && bean.getFoto().getContent() != null){
			RedimensionaImagem redimensionaImagem = new RedimensionaImagem();
			try {
				Arquivo arquivoFoto = redimensionaImagem.redimensionaImagem(bean.getFoto(),150,112);
				if (arquivoFoto != null) bean.setFoto(arquivoFoto);
			} catch (IOException e) {
				e.printStackTrace();
			}catch(NullPointerException nullPointer){
				throw new SinedException("Foto inv�lida.");
			}
		}
		
		Colaboradorstatusfolha colaboradorstatusfolha = new Colaboradorstatusfolha();
		colaboradorstatusfolha.setCdcolaboradorstatusfolha(Colaboradorstatusfolha.NAO_ENVIADO);
		bean.setColaboradorstatusfolha(colaboradorstatusfolha);
		
		super.saveOrUpdate(bean);
		
//		if(bean.getEndereco() != null && bean.getEndereco().getCdendereco() != null){
//			enderecoService.setPessoaEndereco(bean.getEndereco(), bean);
//		}
	}

	/**
	 * M�todo respons�vel por preparar o bean de colaborador para salvar.
	 * 
	 * @see br.com.linkcom.sined.geral.service.PessoaService#ajustaPessoaToSave(br.com.linkcom.sined.geral.bean.Pessoa)
	 * @see br.com.linkcom.sined.geral.service.CategoriacnhService#saveColaboradorcategoriacnh(Set, Colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#getCpfColaborador(Colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#carregaDadosCargo(Colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#salvaColaborador(Colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#makeColaboradorcargo(Colaborador)
	 * @param bean
	 * @author Fl�vio Tavares
	 */
	public void preparaColaboradorSalvar(Colaborador bean){
		pessoaService.ajustaPessoaToSave(bean);
		
		if(bean.getCpf()==null && bean.getVerificaCpf()!=null) bean.setCpf(bean.getVerificaCpf());
		
		//salvar categoria cnh
		Set<Categoriacnh> listaCategoriacnh = bean.getListaCategoriacnh();
		Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh = categoriacnhService.saveColaboradorcategoriacnh(listaCategoriacnh, bean);	
		bean.setListaColaboradorcategoriacnh(listaColaboradorcategoriacnh);
		
		// ao editar um registro
		if(bean.getCdpessoa()!=null){
			bean.setCpf(colaboradorService.getCpfColaborador(bean));
			colaboradorService.carregaDadosCargo(bean);
		}else{
			// ao incluir um registro
			if(bean.getCdpessoaaux() != null){
				colaboradorService.salvaColaborador(bean);
			}
			bean.setColaboradortipo(new Colaboradortipo(Colaboradortipo.NORMAL));
			bean.setColaboradorstatusfolha(new Colaboradorstatusfolha(Colaboradorstatusfolha.NAO_ENVIADO));
			
			if(bean.getCdpessoa() == null && bean.getEmpresa() != null){
				Integer proximoNumeroMatricula = empresaService.carregaProximoNumeroMatricula(bean.getEmpresa());
				if(proximoNumeroMatricula == null){
					proximoNumeroMatricula = 1;
				}
				empresaService.updateProximoNumeroMatricula(bean.getEmpresa(), proximoNumeroMatricula + 1);
				bean.setMatricula(proximoNumeroMatricula.longValue());
			}
			
			
			Set<Colaboradorcargo> listaColaboradorcargo = bean.getListaColaboradorcargo()!=null ? bean.getListaColaboradorcargo() : new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
			listaColaboradorcargo.add(colaboradorcargoService.makeColaboradorcargo(bean));
			bean.setListaColaboradorcargo(listaColaboradorcargo);
			bean.setCriarColaboradorCargo(Boolean.TRUE);
		}
	}
	

	/**
	 * M�todo para carregar os dados de colaboradorcargo para o colaborador
	 *
	 * @see #carregaColaboradorCargoAtual(Colaborador)
	 * @param List<Colaborador>
	 * @return List<Colaborador>
	 * @author Orestes
	 */
	public List<Colaborador> carregaColaboradorCargoAtual(List<Colaborador> lista) {
		for (Colaborador colaborador : lista) {
			colaborador = carregaColaboradorCargoAtual(colaborador);
		}
		return lista;
	}
	
	public List<Colaborador> carregaColaboradorAtivo() {
		return colaboradorDAO.carregaColaboradorAtivo();
	}
	
	/**
	 * M�todo para carregar os dados de colaboradorcargo para o colaborador
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findCargoAtual(Colaborador)
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#load(Colaborador, String)
	 * @param colaborador
	 * @return
	 * @author Jo�o Paulo Zica
	 * @author Fl�vio Tavares
	 */
	public Colaborador carregaColaboradorCargoAtual(Colaborador colaborador){
		if(colaborador == null){
			throw new SinedException("O par�metro colaborador n�o pode ser null.");
		}

		Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(colaborador);
		if(colaboradorcargo!=null){
//			Colaborador load;
//			if(colaborador.getColaboradorsituacao() == null ) {
//				load = colaboradorDAO.load(colaborador, "colaborador.dtadmissao");
//			}else{
//				load = colaboradorDAO.load(colaborador, "colaborador.dtadmissao,colaborador.colaboradorsituacao");
//			}
			colaborador.setCdcolaboradorcargo(colaboradorcargo.getCdcolaboradorcargo());
			colaborador.setCargotransient(colaboradorcargo.getCargo());
			colaborador.setDepartamentotransient(colaboradorcargo.getDepartamento());
			colaborador.setRegimecontratacao(colaboradorcargo.getRegimecontratacao());
//			colaborador.setDtadmissao(load.getDtadmissao());
			colaborador.setColaboradorsituacao(colaboradorcargo.getColaboradorsituacao());
			colaborador.setDtinicio(colaboradorcargo.getDtinicio());
			colaborador.setDtfim(colaboradorcargo.getDtfim());
			colaborador.setEmpresa(colaboradorcargo.getEmpresa());
			colaborador.setObservacao(colaboradorcargo.getObservacao());
			colaborador.setSalario(colaboradorcargo.getSalario());
			colaborador.setSindicato(colaboradorcargo.getSindicato());
			colaborador.setProjetotransient(colaboradorcargo.getProjeto());
//			colaborador.setDtaltera(colaboradorcargo.getDtaltera());
			if(colaboradorcargo.getCdusuarioaltera() != null){
				colaborador.setUsuarioaltera(TagFunctions.findUserByCd(colaboradorcargo.getCdusuarioaltera()));
			}
		}
		return colaborador;
	}

	/**
	 * M�todo montar a lista de Colaboradorcargo, o hist�rico com o cargo atual.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findCargoAtual(Colaborador)
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#load(Colaborador, String)
	 * 
	 * @param colaborador
	 * @author Flavio
	 */
	public void carregaDadosCargo(Colaborador colaborador){		
		Colaborador loadCol = colaboradorDAO.load(colaborador, "colaborador.colaboradorsituacao");
		if(loadCol != null){
			colaborador.setColaboradorsituacao(loadCol.getColaboradorsituacao());
		}
				
		Colaboradorcargo load = colaboradorcargoService.findCargoAtual(colaborador);
		if(load != null){
			colaboradorcargoService.updateSindicatoEmpresaSalario(load, colaborador.getSalario(), colaborador.getSindicato(), colaborador.getEmpresa());
			
			load.setSalario(colaborador.getSalario());
			load.setSindicato(colaborador.getSindicato());
			load.setEmpresa(colaborador.getEmpresa());
		
			if(colaborador.getListaColaboradorcargo()!=null){
				colaborador.getListaColaboradorcargo().add(load);
			}else{
				Set<Colaboradorcargo> listaColaboradorcargo = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
				listaColaboradorcargo.add(load);
				colaborador.setListaColaboradorcargo(listaColaboradorcargo);
			}
		}
	}
	
	public void updateColaboradorsituacaoByColaborador(Colaborador colaborador, Colaboradorsituacao colaboradorsituacao){
		colaboradorDAO.updateColaboradorsituacaoByColaborador(colaborador, colaboradorsituacao);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#salvaColaborador(Colaborador)
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 */
	public void salvaColaborador(Colaborador colaborador){
		colaboradorDAO.salvaColaborador(colaborador);
	}

	/**M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#deleteColaborador(Colaborador)
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 */
	public void deleteColaborador(Colaborador colaborador){
		colaboradorDAO.deleteColaborador(colaborador);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findColaboradorAltera(AlterarsituacaocolaboradorFilter)
	 * @param filtro
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findColaboradorAltera(AlterarsituacaocolaboradorFilter filtro){
		return colaboradorDAO.findColaboradorAltera(filtro);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findColaboradorByPk(Integer)
	 * @param cdpessoa
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findColaboradorByPk(Integer cdpessoa){
		return colaboradorDAO.findColaboradorByPk(cdpessoa);
	}


	/**
	 * M�todo de refer�ncia ao DAO
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#updateColaborador(Colaborador)
	 * @param colaborador
	 * @author Jo�o Paulo Zica
	 */
	public void updateColaborador(Colaborador colaborador){
		colaboradorDAO.updateColaborador(colaborador);
	}


	/**
	 * M�todo para cria��o do relat�rio de funcion�rios
	 *
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findForRelatorioColaboradores
	 * @param filtro
	 * @return
	 * @author Jo�o Paulo Zica
	 * @author Flavio Tavares
	 */
	public IReport createRelatorioColaboradores(ColaboradorReportFiltro filtro) {
		Report report = new Report("rh/colaborador");
		List<Colaboradorcargo> lista = colaboradorcargoService.findForRelatorioColaboradores(filtro);

		report.setDataSource(lista);
		
		
		return report;
	}

	/**
	 * M�todo para cria��o do relat�rio autoriza��o de exames
	 *
	 * @see br.com.linkcom.sined.geral.service.ExametipoService#findExameTipoByPk(Integer)
	 * @see br.com.linkcom.sined.util.SinedUtil#getDayOfWeek(Date)
	 * @see br.com.linkcom.sined.util.SinedUtil#getUsuarioLogado()
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#carregaColaboradorCargoAtual(List<Colaborador>)
	 * @see br.com.linkcom.sined.geral.service.ArquivoService#loadAsImage(br.com.linkcom.neo.types.File)
	 * 
	 * @param filtro
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public IReport createRelatorioAutorizacao(EmitirautorizacaoReportFiltro filtro) {
		if	(filtro == null){
			throw new SinedException("O par�metro filtro n�o pode ser vazio.");
		} else if (filtro.getExameclinica()== null && filtro.getExameresponsavel() == null) {
			throw new SinedException("O campo Cl�nica e/ou o campo Profissional dever�(�o) ser selecionado(s).");
		} else if (filtro.getListExametipo()== null) {
			throw new SinedException("Voc� deve selecionar pelo menos um tipo de exame.");
		}
		
		Report report = new Report("rh/autorizacaoexame");
		
		List<Colaborador> lista = setCboForReport(filtro.getListaColaboradoraux());
		lista = carregaColaboradorCargoAtual(lista);
		
		report.setDataSource(lista);
		Image image = null;
		Arquivo arquivo;
		if(filtro.getExameclinica() != null && filtro.getExameclinica().getCdexameclinica() != null){
			Exameclinica exameclinica = exameclinicaService.loadForEntrada(filtro.getExameclinica());
			report.addParameter("CLINICA", exameclinica.getNome());
			report.addParameter("ENDERECO", exameclinica.getEndereco());
			report.addParameter("TELEFONE", exameclinica.getTelefone().toString());
			report.addParameter("LOCAL", filtro.getLocal());
			report.addParameter("ENDERECOCABECALHO", enderecoService.carregaEnderecoEmpresa(filtro.getEmpresa()));
			arquivo = arquivoService.load(exameclinica.getLogotipo());
			if(arquivo != null){
				try {
					image = arquivoService.loadAsImage(arquivo);
				} catch (Exception e) {}
			}
		}
		if(filtro.getExameconvenio() != null && filtro.getExameconvenio().getCdexameconvenio() != null){
			Exameconvenio exameconvenio = exameconvenioService.loadForEntrada(filtro.getExameconvenio());
			arquivo = arquivoService.load(exameconvenio.getLogotipo());
			if(filtro.getExameclinica() == null && arquivo != null)
				image = arquivoService.loadAsImage(arquivo);
			report.addParameter("CONVENIO", exameconvenio.getNome());
		}
		if(filtro.getExameresponsavel() != null && filtro.getExameresponsavel().getCdexameresponsavel() != null){
			Exameresponsavel exameresponsavel = exameresponsavelService.load(filtro.getExameresponsavel());
			report.addParameter("PROFISSIONAL", exameresponsavel.getNome());
		}
		if(filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null){
			report.addParameter("EMPRESA", empresaService.getEmpresaRazaosocialOuNome(filtro.getEmpresa()));
		}

		List<Exametipo> listaExametipo = exametipoService.findAll(filtro.getListExametipo());
		
		for (Exametipo exametipo : listaExametipo) {
			if (exametipo.getCdexametipo().equals(Exametipo.OUTROS)) {
				if(filtro.getOutro() != null && !filtro.getOutro().equals("")){
					listaExametipo.remove(exametipo);
					exametipo = new Exametipo();
					exametipo.setNome(filtro.getOutro());
					listaExametipo.add(exametipo);
					break;
				}
			}	
		}

		report.addParameter("listaExametipo", listaExametipo);
		report.addSubReport("AUTORIZACAOEXAMETIPO",new Report("rh/autorizacaoexame_exametipo"));
		
		Colaborador colaborador = findColaboradorByPk(SinedUtil.getUsuarioLogado().getCdpessoa());
		if (colaborador != null) {
			colaborador = carregaColaboradorCargoAtual(colaborador);
			report.addParameter("USUARIO", colaborador.getNome());
			report.addParameter("CARGOUSUARIO", colaborador.getCargotransient() != null ? colaborador.getCargotransient().getNome() : null);
		}else{
			String nomeUsuario = TagFunctions.findUserByCd(SinedUtil.getUsuarioLogado().getCdpessoa());
			report.addParameter("USUARIO", nomeUsuario);
		}
		
		report.addParameter("DIASEMANA", filtro.getDataExame() != null ? SinedDateUtils.getDayOfWeek(filtro.getDataExame()) : "");
		report.addParameter("DATA_EXAME", filtro.getDataExame() != null ? filtro.getDataExame() : null);
		report.addParameter("HORA", filtro.getHoraExame() != null ? filtro.getHoraExame() : null);
		if(image != null)
			report.addParameter("IMAGEM", image);
		
		return report;
	}

	
	/**
	 * M�todo para localizar o cargo atual do colaborador e setar o cbo deste cargo.
	 * @param List<Colaborador>
	 * @return List<Colaborador>
	 * @author Jo�o Paulo Zica
	 * @see br.com.linkcom.sined.geral.service.ColaboradorcargoService#findCargoAtual(Colaborador)
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#findForAutorizacaoExameByPk(Integer)
	 */
	public List<Colaborador> setCboForReport(List<Colaborador> listaColaborador){
		List<Colaborador> lista = new ArrayList<Colaborador>();
		for (Colaborador colaborador : listaColaborador) {
			if (findForAutorizacaoExameByPk(colaborador.getCdpessoa()) != null) {
				Colaborador colaboradorAux = findForAutorizacaoExameByPk(colaborador.getCdpessoa());
				Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(colaboradorAux);
				Cbo cbo = null;
				if(colaboradorcargo != null){
					cbo = colaboradorcargo.getCargo().getCbo();
				}
				if (cbo != null) {
					colaboradorAux.setCboAux(cbo);
				}
				lista.add(colaboradorAux);
			}
		}
		return lista;
	
	}
	
	/**
	 * M�todo para cadastrar colaboradores em uma turma.
	 * 
	 * @see br.com.linkcom.sined.geral.service.TurmaService#load(Turma)
	 * @see br.com.linkcom.sined.geral.service.TurmaService#findTurmaConflitante(Colaborador, Turma)
	 * @see br.com.linkcom.sined.geral.service.TurmaparticipanteService#findByTurma(Turma)
	 * @see br.com.linkcom.sined.geral.service.TurmaparticipanteService#saveOrUpdateListaTurmaparticipante(Collection)
	 * @param listaColaborador
	 * @param turma
	 * @return true - se algum colaborador foi cadastrado
	 * 			false - se nenhum foi cadastrado
	 * @author Fl�vio Tavares
	 */
	public boolean adicionaColaboradorTurma(Collection<Colaborador> listaColaborador, Turma turma, StringBuilder conflitantes, StringBuilder existentes){
		boolean success = false;
		turma = turmaService.load(turma);
		List<Turmaparticipante> listaTp = turmaparticipanteService.findByTurma(turma);
		List<Colaborador> cConf = new ArrayList<Colaborador>(), cExis = new ArrayList<Colaborador>();
		
		for (Colaborador c : listaColaborador) {
			Turmaparticipante tp = new Turmaparticipante(turma, c);
			
			if(listaTp.contains(tp)){
				cExis.add(c);
			}else if(turmaService.findTurmaConflitante(c, turma) > 0){
				cConf.add(c);
			}else{
				listaTp.add(tp);
				success = true;
			}
		}
		conflitantes.append(this.listaNomesColaboradores(cConf));
		existentes.append(this.listaNomesColaboradores(cExis));
		
		if(success) turmaparticipanteService.saveOrUpdateListaTurmaparticipante(listaTp);
		return success;
	}
	
	/**
	 * M�todo para cadastrar colaboradores em um contrato.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContratocolaboradorService#findByContrato
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#listaNomesColaboradores
	 * @see br.com.linkcom.sined.geral.service.ContratocolaboradorService#saveLista
	 * 
	 * @param listaColaborador
	 * @param projeto
	 * @return true - se algum colaborador foi cadastrado
	 * 			false - se nenhum foi cadastrado
	 * @author Fl�vio Tavares
	 */
	public boolean adicionaColaboradorContrato(Collection<Colaborador> listaColaborador, Contratocolaborador contratocolaborador, StringBuilder existentes){
		boolean success = false;
		
		List<Contratocolaborador> listaContrato = contratocolaboradorService.findByContrato(contratocolaborador.getContrato());
		List<Colaborador> cExis = new ArrayList<Colaborador>();
		Contratocolaborador cc;
		
		for (Colaborador c : listaColaborador) {
			cc = new Contratocolaborador();
			
			cc.setContrato(contratocolaborador.getContrato());
			cc.setColaborador(c);
			cc.setEscala(contratocolaborador.getEscala());
			cc.setDtinicio(contratocolaborador.getDtinicio());
			cc.setDtfim(contratocolaborador.getDtfim());
			cc.setHorainicio(contratocolaborador.getHorainicio());
			cc.setHorafim(contratocolaborador.getHorafim());
			
			if(listaContrato.contains(cc)){
				cExis.add(c);
			} else {
				listaContrato.add(cc);
				success = true;
			}
		}
		
		existentes.append(this.listaNomesColaboradores(cExis));
		
		if(success) contratocolaboradorService.saveLista(listaContrato);
		
		return success;
	}
	
	/**
	 * M�todo para cadastrar colaboradores em uma turma.
	 * 
	 * @param listaColaborador
	 * @param grupoemail
	 * @return true - se algum colaborador foi cadastrado
	 * 			false - se nenhum foi cadastrado
	 * @author Fl�vio Tavares
	 */
	public boolean adicionaColaboradorGrupoemail(Collection<Colaborador> listaColaborador, Grupoemail grupoemail, StringBuilder existentes){
		boolean success = false;
		grupoemail = grupoemailService.load(grupoemail);
		List<GrupoemailColaborador> listaGec = grupoemailColaboradorService.findBy(grupoemail,"colaborador.cdpessoa","grupoemail.cdgrupoemail");
		List<Colaborador> cExis = new ArrayList<Colaborador>();
		
		for (Colaborador c : listaColaborador) {
			GrupoemailColaborador gec = new GrupoemailColaborador(grupoemail, c);
			
			if(listaGec.contains(gec)){
				cExis.add(c);
			}else{
				listaGec.add(gec);
				success = true;
			}
		}
		existentes.append(this.listaNomesColaboradores(cExis));
		
		if(success) grupoemailColaboradorService.saveOrUpdateListaProjetoColaborador(listaGec);
		return success;
	}
	
	/**
	 * M�todo para listar os nomes de uma lista de colaboradores.
	 * 
	 * @see #load(String, String)
	 * @param listaColaborador
	 * @return java.lang.String - com os nomes concatenados, separados por v�rgula.
	 * @author Fl�vio Tavares
	 */
	public String listaNomesColaboradores(Collection<Colaborador> listaColaborador){
		if(listaColaborador == null || listaColaborador.size() ==0){
			return "";
		}
		
		listaColaborador = this.load(CollectionsUtil.listAndConcatenate(listaColaborador, "cdpessoa", ","),"colaborador.cdpessoa,colaborador.nome");
		return CollectionsUtil.listAndConcatenate(listaColaborador, "nome", ", ");
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#load(String, String)
	 * @param whereIn
	 * @param fields
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> load(String whereIn, String fields){
		return colaboradorDAO.load(whereIn, fields);
	}
	/**
	 * M�todo de refer�ncia ao DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findForAutorizacaoExameByFk(Integer)
	 * @param filtro
	 * @return Lista de Colaboradorcargo
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findForAutorizacaoExame(EmitirautorizacaoReportFiltro filtro){
		return colaboradorDAO.findForAutorizacaoExame(filtro);
	}

	/**
	 * M�todo de refer�ncia ao DAO
	 * 
	 * @param cdpessoa
	 * @return colaborador
	 * @author Jo�o Paulo Zica
	 */
	public Colaborador findForAutorizacaoExameByPk(Integer cdpessoa){
		return colaboradorDAO.findForAutorizacaoExameByPk(cdpessoa);
	}

	/**
	 * M�todo de refer�ncia ao DAO. Utilizado na listagem de colaborador para exporta��o.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findForListagemExportarColaborador(ExportarcolaboradorFiltro)
	 * @param filtro
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findForListagemExportarColaborador(ExportarcolaboradorFiltro filtro){
		return colaboradorDAO.findForListagemExportarColaborador(filtro);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. Utilizado na listagem de colaborador para cria��o do relat�rio CSV.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findForListagemGerarCSVColaborador(ColaboradorFiltro)
	 * @param filtro
	 * @return
	 * @author Rafael Salvio Martins
	 */
	public ListagemResult<Colaborador> findForListagemGerarCSVColaborador(ColaboradorFiltro filtro){
		return colaboradorDAO.findForListagemGerarCSVColaborador(filtro);
	}
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Gera uma lista com todos os dados do colaborador para criar o arquivo de exporta��o
	 * para o NG Folha.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#gerarListaArquivoExportacao(String)
	 * @param whereIn String de c�digo de colaborador (cdpessoa) separados por v�rgula
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> gerarListaParaArquivoExportacao(String whereIn) {
		return colaboradorDAO.gerarListaArquivoExportacao(whereIn);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obter Colaborador para altera��o de cargo.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findColaboradorAlteraCargo(Colaboradorcargo)
	 * @param colaboradorcargo
	 * @return
	 * @author Flavio
	 */
	public Colaborador findColaboradorAlteraCargo(Colaboradorcargo colaboradorcargo){
		return colaboradorDAO.findColaboradorAlteraCargo(colaboradorcargo);
	}


	/**
	 * M�todo de refer�ncia ao DAO. 
	 * Atualiza o status de exporta��o para o NG Folha de um ou mais colaboradores.
	 * 
	 * @param colaboradores String de c�digos de colaboradores separados por v�rgula
	 * @param status Um status que o colaborador pode assumir (cdstatusfolha)
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#alterarStatusFolhaPorCodigo(String, Integer)
	 * @author Hugo Ferreira
	 */
	public void alterarStatusFolhaPorCodigo(String colaboradores, Integer status){
		colaboradorDAO.alterarStatusFolhaPorCodigo(colaboradores, status);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO. Altera o status NG Folha de um colaborador</p>
	 * 
	 * @param whereIn
	 * @param cdColaboradorStatusFolha
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#atualizaColaboradorStatusFolha(Colaborador)
	 * @author Hugo Ferreira
	 */
	public void atualizaColaboradorStatusFolha(String whereIn, Integer cdColaboradorStatusFolha) {
		colaboradorDAO.atualizaColaboradorStatusFolha(whereIn, cdColaboradorStatusFolha);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * Altera o status NG Folha de um colaborador.
	 * 
	 * @param whereIn
	 * @param alterado
	 * @author Hugo Ferreira
	 */
	public void atualizaAlterado(String whereIn, Boolean alterado) {
		colaboradorDAO.atualizaAlterado(whereIn, alterado);
	}
	
	/**
	 * 
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m o CPF de um colaborador
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#getCpfColaborador(Colaborador)
	 * @param colaborador
	 * @return Cpf
	 * @author Flavio
	 */
	public Cpf getCpfColaborador(Colaborador colaborador){
		return colaboradorDAO.getCpfColaborador(colaborador);
	}
	

	/**
	 * M�todo de refer�ncia ao DAO.
	 * Obt�m determinados campos de um colaborador.
	 * 
	 * @param bean
	 * @param campos
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Colaborador load(Colaborador bean, String campos) {
		return colaboradorDAO.load(bean, campos);
	}
	
	public Colaborador loadWithoutPermissao(Colaborador bean, String campos) {
		return colaboradorDAO.loadWithoutPermissao(bean, campos);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByDepartamento
	 * @param departamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findByDepartamento(Departamento departamento){
		return colaboradorDAO.findByDepartamento(departamento);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO. Recupera uma lista de colaboradores
	 * a partir de CPFs separados por v�rgula.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByCpf(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */
	public List<Colaborador> findByCpf(String whereIn) {
		return colaboradorDAO.findByCpf(whereIn);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o colaborador pelo CPF. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByCpf(Cpf cpf)
	 *
	 * @param cpf
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByCpf(Cpf cpf){
		return colaboradorDAO.findByCpf(cpf);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o colaborador pelo Nome. 
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByNome(String nome)
	 *
	 * @param nome
	 * @return
	 * @since Jul 5, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByNome(String nome){
		return colaboradorDAO.findByNome(nome);
	}

	/**
	 * M�todo de refer�ncia ao DAO. Recupera uma lista de colaboradores
	 * a partir de CNPJs separados por v�rgula.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByCnpj(String)
	 * @param whereIn
	 * @return
	 * @author Hugo Ferreira
	 */	
	public List<Colaborador> findByCnpj(String whereIn) {
		return colaboradorDAO.findByCnpj(whereIn);
	}
	
	/**
	 * M�todo para obter colaboradores de determinado cargo que est�o ligados a um projeto, ou 
	 * o pr�prio colaborador informado por par�metro.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByCargoProjeto(Cargo, Projeto, Colaborador)
	 * @param cargo
	 * @param projeto
	 * @param colaborador
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> findByCargoProjeto(Cargo cargo, Projeto projeto, Colaborador colaborador){
		return colaboradorDAO.findByCargoProjeto(cargo, projeto, colaborador);
	}
	
	/**
	 * M�todo para obter colaboradores de determinado cargo que est�o ligados a um projeto.
	 * 
	 * @see #findByCargoProjeto(Cargo, Projeto, Colaborador)
	 * @param cargo
	 * @param projeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> findByCargoProjeto(Cargo cargo, Projeto projeto){
		return colaboradorDAO.findByCargoProjeto(cargo, projeto, null);
	}
	
	/**
	 * M�todo para obter colaboradores de determinado cargo que est�o ligados a um projeto.
	 * 
	 * @param cargo
	 * @param projeto
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Colaborador> findByCargo(Cargo cargo){
		return colaboradorDAO.findByCargo(cargo);
	}
	
	/**
	 * Carrega a lista de colaborador para exibi��o no flex.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findAllForFlex
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findAllForFlex(){
		return colaboradorDAO.findAllForFlex();
	}
	
	public Colaborador loadForBoleto(Integer cdpessoa) {
		return colaboradorDAO.loadForBoleto(cdpessoa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Colaborador carregaEmailColaborador(Colaborador colaborador) {
		return colaboradorDAO.carregaEmailColaborador(colaborador);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO</p>
	 * Carrega todos os <code>Colaboradores</code> de um 
	 * <code>Contrato</code> passado por par�metro.
	 *
	 * @param contrato
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public List<Colaborador> findByContrato(Contrato contrato) {
		return colaboradorDAO.loadColaboradores(contrato);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param veiculo
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Colaborador findColaboradorResponsavelVeiculo(Veiculo veiculo){
		return colaboradorDAO.findColaboradorResponsavelVeiculo(veiculo);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param contacrm
	 * @return
	 * @author Mario Caixeta
	 */
	public List<Colaborador> findColaboradorResponsavelContacrm(Contacrm contacrm){
		return colaboradorDAO.findColaboradorResponsavelContacrm(contacrm);
	}
	
	public List<Colaborador> findDesbloqueado(){
		return colaboradorDAO.findDesbloqueado();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param busca
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaborador> findColaboradoresForBuscaGeral(String busca) {
		return colaboradorDAO.findColaboradoresForBuscaGeral(busca);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param mes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public List<Colaborador> findColaboradoresAniversariantesDoMesFlex(Integer mes){
		List<Colaborador> lista = colaboradorDAO.findColaboradoresAniversariantesDoMesFlex(mes);
		//Gambiarra data flex
		if(lista != null && !lista.isEmpty())
			for (Colaborador colaborador : lista)
				colaborador.setDtnascimento(SinedDateUtils.setDateProperty(colaborador.getDtnascimento(), 12, Calendar.HOUR_OF_DAY));
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @param dtReferencia
	 * @param dtreferenciaAte
	 * @return
	 * @author Tom�s Rabelo
	 *
	 */
	public List<Colaborador> sugestaoColaboradorMenosAtendimentoPeriodo(Material material, String dtReferencia, String dtreferenciaAte, Empresa empresa){
		return colaboradorDAO.sugestaoColaboradorMenosAtendimentoPeriodo(material, dtReferencia, dtreferenciaAte, empresa);
	}
	
	public List<Colaborador> sugestaoColaboradorMenosAtendimentoPeriodo(Material material, Date dtReferencia, Date dtreferenciaAte, Empresa empresa){
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		
		if (empresa != null && dtReferencia != null && dtreferenciaAte != null && material != null) {
			listaColaborador = colaboradorDAO.sugestaoColaboradorMenosAtendimentoPeriodo(material, DateUtils.dateToString(dtReferencia, "dd/MM/yyyy"), DateUtils.dateToString(dtreferenciaAte, "dd/MM/yyyy"), empresa);
		}
		return listaColaborador;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param material
	 * @param dtReferencia
	 * @param dtreferenciaAte
	 * @return
	 * @author Francisco Lourandes
	 *
	 */
	public List<Colaborador> sugestaoColaboradorMenosAtendimentoPeriodoAutocomplete(String q){
		
		Empresa empresa = null;
		Material material = null;
		String dt1 = "";
		String dt2 = "";
		
		try{
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		}catch(Exception e1){}
		try{
			material = new Material(Integer.parseInt(NeoWeb.getRequestContext().getParameter("material")));
		}catch(Exception e2){}
		try{
			dt1 = NeoWeb.getRequestContext().getParameter("dateFrom");
		}catch(Exception e3){}
		try{
			dt2 = NeoWeb.getRequestContext().getParameter("dateTo");
		}catch(Exception e4){}
		
		return colaboradorDAO.sugestaoColaboradorMenosAtendimentoPeriodo(material, dt1, dt2, empresa, q);

	}
	
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	public Colaborador getEscalaColaborador(Colaborador colaborador, String datede, String dateate){
		Date dtde = SinedDateUtils.stringToSqlDate(datede);
		Date dtate = SinedDateUtils.stringToSqlDate(dateate);
		
		colaborador = colaboradorDAO.getEscalaColaborador(colaborador, dtde, dtate);
		return colaborador;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param q
	 * @return
	 * @author M�rio Caixeta
	 */
	public List<Colaborador> findAutocomplete(String q) {
		return colaboradorDAO.findAutocomplete(q);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param q
	 * @return
	 * @author Jo�o Vitor
	 */
	public List<Colaborador> findAutocompleteForFunilVendas(String q) {
		return colaboradorDAO.findAutocompleteForFunilVendas(q);
	}
	
	public List<Colaborador> colaboradorArea(Area area) {
		return colaboradorDAO.colaboradorArea(area);
	}
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param q
	 * @return
	 * @author M�rio Caixeta
	 */
	public Colaborador carregaColaborador(String whereIn) {
		return colaboradorDAO.carregaColaborador(whereIn);
	}
	
	/**
	 * M�todo que gera formul�rio PPP
	 * 
	 * @param filtro
	 * @param whereIn
	 * @return
	 * @throws Exception
	 * @author Tom�s Rabelo
	 */
	public Resource geraFormularioPPP(FormularioPPPReportFiltro filtro, String whereIn) throws Exception {
		List<Colaborador> colaboradores = carregaColaboradorFormularioPPP(whereIn, filtro.getMatricula());
		if(colaboradores == null || colaboradores.isEmpty())
			throw new SinedException("Lista de colaboradores vazia.");
		
		MergeReport mergeReport = new MergeReport("formularioPPP_"+SinedUtil.datePatternForReport()+".pdf");
		Report report = null;
		Report subreport1 = null;
		Report subreport2 = null;
		Report subreport3 = null;
		Report subreport4 = null;
		Report subreport5 = null;
		Report subreport6 = null;
		Empresa empresa = empresaService.carregaEmpresa(filtro.getEmpresa());
		for (Colaborador colaborador : colaboradores) {
			FormularioPPPBean formularioPPPBean = montaFormularioColaboradorPPP(empresa, colaborador);
			report = new Report("/rh/formularioPPP");
			
			formularioPPPBean.setColaboradorusuario(findColaboradorusuario(SinedUtil.getUsuarioLogado().getCdpessoa()));
			if(filtro.getMatricula() != null){
				formularioPPPBean.setListaCargosColaborador(this.removerCargosDiferentesMatricula(filtro.getMatricula(), formularioPPPBean.getListaCargosColaborador()));
			}
			report.addParameter("previdenciasocial", neoImageResolver.getImage("/imagens/rh/previdenciasocial.gif"));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			report.addParameter("USUARIO", formularioPPPBean.getColaboradorusuario() != null ? formularioPPPBean.getColaboradorusuario().getNome() : "");
			
			subreport1 = new Report("/rh/formularioPPPAlocacaoSub");
			subreport2 = new Report("/rh/formularioPPPProfissiografiaSub");
			subreport3 = new Report("/rh/formularioPPPRiscoSub");
			subreport4 = new Report("/rh/formularioPPPResponsavelSub");
			subreport5 = new Report("/rh/formularioPPPExameSub");
			subreport6 = new Report("/rh/formularioPPPResponsavelMonitoracaoSub");
			report.addSubReport("FORMULARIOPPPALOCACAOSUB", subreport1);
			report.addSubReport("FORMULARIOPPPPROFISSIOGRAFIASUB", subreport2);
			report.addSubReport("FORMULARIOPPPRISCOSUB", subreport3);
			report.addSubReport("FORMULARIOPPPRESPONSAVELSUB", subreport4);
			report.addSubReport("FORMULARIOPPPEXAMESUB", subreport5);
			report.addSubReport("FORMULARIOPPPRESPONSAVELMONITORACAOSUB", subreport6);
			report.setDataSource(new FormularioPPPBean[]{formularioPPPBean});
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo respons�vel por eliminar informa��es dos cargos que n�o est�o
	 * relacionados ao n�mero de matricula escolhido, ou seja, no relat�rio
	 * dever� constar somente informa��es do cargo conforme n�mero de matr�cula do filtro.
	 * @param matricula
	 * @param lista
	 * @return
	 * @author Taidson
	 * @since 28/12/2010
	 */
	public List<Colaboradorcargo> removerCargosDiferentesMatricula(Integer matricula, List<Colaboradorcargo> lista){
		List<Colaboradorcargo> novaLista = new ArrayList<Colaboradorcargo>();
		for (Colaboradorcargo item : lista) {
			if(item.getMatricula() != null && item.getMatricula().equals(matricula)){
				novaLista.add(item);
			}
		}
		return novaLista;
	}
	
	/**
	 * M�todo que monta o relat�rio de cada Colaborador
	 * 
	 * @param empresa
	 * @param colaborador
	 * @return
	 * @author Tom�s Rabelo
	 */
	private FormularioPPPBean montaFormularioColaboradorPPP(Empresa empresa, Colaborador colaborador) {
		FormularioPPPBean bean = new FormularioPPPBean();
		bean.setEmpresa(empresa);
		bean.setColaborador(colaborador);
		
		Set<Empresacodigocnae> listaEmpresacodigocnae = empresa.getListaEmpresacodigocnae();
		if(listaEmpresacodigocnae != null && !listaEmpresacodigocnae.isEmpty()){
			for (Empresacodigocnae empresacodigocnae : listaEmpresacodigocnae) {
				if(empresacodigocnae.getCodigocnae() != null && empresacodigocnae.getPrincipal() != null & empresacodigocnae.getPrincipal()){
					bean.setCodigocnae(empresacodigocnae.getCodigocnae());
				}
			}
		}
		
		if(colaborador.getListaColaboradoracidente() != null && !colaborador.getListaColaboradoracidente().isEmpty()){
			Iterator<Colaboradoracidente> iterator = colaborador.getListaColaboradoracidente().iterator();
			Colaboradoracidente colaboradoracidente = iterator.next();
			bean.setDtCAT1(colaboradoracidente.getDtregistro());
			bean.setRegistroCAT1(colaboradoracidente.getRegistro());
			if(iterator.hasNext()){
				colaboradoracidente = iterator.next();
				bean.setDtCAT2(colaboradoracidente.getDtregistro());
				bean.setRegistroCAT2(colaboradoracidente.getRegistro());
			}
		}
		
		if(colaborador.getListaColaboradorexame() != null && !colaborador.getListaColaboradorexame().isEmpty()){
			bean.setListaColaboradorexame(colaborador.getListaColaboradorexame());
		}
		
		if(colaborador.getListaColaboradorcargo() != null && !colaborador.getListaColaboradorcargo().isEmpty()){
			List<Colaboradorcargo> lista = colaboradorcargoService.carregaColaboradoresCargos(CollectionsUtil.listAndConcatenate(colaborador.getListaColaboradorcargo(), "cdcolaboradorcargo", ","));
			bean.setListaCargosColaborador(lista);
			int i = 0;
			for (Colaboradorcargo colaboradorcargo : lista) {
				if(i==0){
					bean.setHigienizacao(colaboradorcargo.getHigienizacao());
					bean.setImplementacaomedidas(colaboradorcargo.getImplementacaomedidas());
					bean.setPrazovalidade(colaboradorcargo.getPrazovalidade());
					bean.setTrocaprograma(colaboradorcargo.getTrocaprograma());
					bean.setUsoepiiniterrupto(colaboradorcargo.getUsoepiiniterrupto());
				}
				i++;
				if(colaboradorcargo.getListaColaboradorcargoprofissiografia() != null && !colaboradorcargo.getListaColaboradorcargoprofissiografia().isEmpty())
					bean.getListaColaboradorcargoprofissiografia().addAll(colaboradorcargo.getListaColaboradorcargoprofissiografia());
				if(colaboradorcargo.getListaColaboradorcargorisco() != null && !colaboradorcargo.getListaColaboradorcargorisco().isEmpty())
					bean.getListaColaboradorcargorisco().addAll(colaboradorcargo.getListaColaboradorcargorisco());
			}
		}
		
		return bean;
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @param whereIn
	 * @return
	 * @author Tom�s Rabelo
	 */
	private List<Colaborador> carregaColaboradorFormularioPPP(String whereIn, Integer matricula) {
		return colaboradorDAO.carregaColaboradorFormularioPPP(whereIn, matricula);
	}


	/**
	 * Refer�ncia ao DAO.
	 *  
	 * @author Taidson
	 * @since 30/04/2010
	 */
	public List<Colaborador> findForRelatorioFichaColaborador(String colaboradoresSelecionados) {
		return colaboradorDAO.findForRelatorioFichaColaborador(colaboradoresSelecionados);
	}
	
	/**
	 * M�todo que gerar v�rias Fichas - Colaborador
	 *
	 * @param coloboradoresSelecionados
	 * @return
	 * @throws Exception
	 * @author Luiz Fernando
	 */
	public Resource gerarVariasFichasColaborador(String coloboradoresSelecionados) throws Exception {
		MergeReport mergeReport = new MergeReport("fichaColaborador_"+SinedUtil.datePatternForReport()+".pdf");
		
		List<Colaborador> listaColaborador = this.findForRelatorioFichaColaborador(coloboradoresSelecionados);
		
		if(listaColaborador != null && !listaColaborador.isEmpty()){
			for(Colaborador colaborador : listaColaborador){
				mergeReport.addReport(this.criaFichaColaboradorReport(colaborador));
			}
		}
		return mergeReport.generateResource();
	}
	
	/**
	 * M�todo que cria o relat�rio da ficha do colaborador
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	private Report criaFichaColaboradorReport(Colaborador colaborador) {
		Report report = new Report("/rh/fichaColaborador");
		
		List<Colaborador> listaColaborador = new ArrayList<Colaborador>();
		listaColaborador.add(colaborador);
		
		String brPdh = "";
		String indicePrevidencia = "";
		String fgtsopcao = "";
		
		if(colaborador.getBrpdh() == null) brPdh = "NA";
		else if(colaborador.getBrpdh()) brPdh = "BR";
		else brPdh = "PDH";
		colaborador.setBrPdhString(brPdh);
		if(colaborador.getIndiceprevidencia() == null) 
			indicePrevidencia = "Nenhum";
		else if(colaborador.getIndiceprevidencia()) 
			indicePrevidencia = "INSS";
		else indicePrevidencia = "Outro";
		
		colaborador.setIndiceprevidenciaString(indicePrevidencia);
		
		if(colaborador.getFgtsopcao() == null)
			fgtsopcao = "";
		else if(colaborador.getFgtsopcao())
			fgtsopcao = "Optante";
		else fgtsopcao = "N�o";
		
		colaborador.setFgtsopcaoString(fgtsopcao);
		
		for (Colaboradorcategoriacnh cat : colaborador.getListaColaboradorcategoriacnh()) {
			if(cat.getCategoriacnh().getCdcategoriacnh() == 1){colaborador.setCategCnhA(1);}
			if(cat.getCategoriacnh().getCdcategoriacnh() == 2){colaborador.setCategCnhB(1);}
			if(cat.getCategoriacnh().getCdcategoriacnh() == 3){colaborador.setCategCnhC(1);}
			if(cat.getCategoriacnh().getCdcategoriacnh() == 4){colaborador.setCategCnhD(1);}
			if(cat.getCategoriacnh().getCdcategoriacnh() == 5){colaborador.setCategCnhE(1);}
		}
	
		if(colaborador.getDadobancario() != null){
			if(colaborador.getDadobancario().getAgencia() != null){
				colaborador.setAgencia(colaborador.getDadobancario().getAgencia() + "-" + colaborador.getDadobancario().getDvagencia());
			}
			if(colaborador.getDadobancario().getConta() != null){
				colaborador.setConta(colaborador.getDadobancario().getConta() + "-" + colaborador.getDadobancario().getDvconta());
			}
		}
		
		Empresa empresa = null;
		for (Colaboradorcargo cargo : colaborador.getListaColaboradorcargo()) {
			if(cargo.getDtfim() == null){
				colaborador.setDepartamentotransient(cargo.getDepartamento());
				colaborador.setCargotransient(cargo.getCargo());
				colaborador.setSindicato(cargo.getSindicato());
				colaborador.setSalario(cargo.getSalario());
				colaborador.setRegimecontratacao(cargo.getRegimecontratacao());
				colaborador.setProjetotransient(cargo.getProjeto());
				if(cargo.getEmpresa() != null && cargo.getEmpresa().getRazaosocialOuNome() != null && !"".equals(cargo.getEmpresa().getRazaosocialOuNome())){
					
					empresa = cargo.getEmpresa();
				}
				if(cargo.getMatricula() != null){
					colaborador.setMatricula(cargo.getMatricula());
				}
				break;
			}

		}
		
		colaborador.setFotoImagem(getFoto(colaborador));
		
		if(empresa == null){
			empresa = empresaService.loadPrincipal();
		}
		if(empresa != null){
			report.addParameter("EMPRESA", empresa.getRazaosocialOuNome());
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			
			empresa = empresaService.loadWithEndereco(empresa);
			report.addParameter("EMPRESA_ENDERECO", empresa.getEndereco() != null ? empresa.getEndereco().getLogradouroCompletoComCep(): "");
		}
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.addParameter("TITULO", "FICHA DO COLABORADOR");
		report.addSubReport("SUBFICHACOLABORADOR", new Report("/rh/fichaColaboradorSub"));
		report.setDataSource(listaColaborador);
		
		return report;
	}
	
	/**
	 * Gera o relat�rio de ficha de colaborador.
	 * 
	 * @author Taidson
	 * @since 30/04/2010
	 */
	public IReport gerarRelatorioFichaColaborador(String coloboradoresSelecionados) {
		Report report = new Report("/rh/fichaColaborador");
		
		List<Colaborador> listaColaborador = this.findForRelatorioFichaColaborador(coloboradoresSelecionados);
		for (Colaborador item : listaColaborador) {
			String brPdh = "";
			String indicePrevidencia = "";
			String fgtsopcao = "";
			
			if(item.getBrpdh() == null) brPdh = "NA";
			else if(item.getBrpdh()) brPdh = "BR";
			else brPdh = "PDH";
			item.setBrPdhString(brPdh);
			if(item.getIndiceprevidencia() == null) 
				indicePrevidencia = "Nenhum";
			else if(item.getIndiceprevidencia()) 
				indicePrevidencia = "INSS";
			else indicePrevidencia = "Outro";
			
			item.setIndiceprevidenciaString(indicePrevidencia);
			
			if(item.getFgtsopcao() == null)
				fgtsopcao = "";
			else if(item.getFgtsopcao())
				fgtsopcao = "Optante";
			else fgtsopcao = "N�o";
			
			item.setFgtsopcaoString(fgtsopcao);
			
			for (Colaboradorcategoriacnh cat : item.getListaColaboradorcategoriacnh()) {
				if(cat.getCategoriacnh().getCdcategoriacnh() == 1){item.setCategCnhA(1);}
				if(cat.getCategoriacnh().getCdcategoriacnh() == 2){item.setCategCnhB(1);}
				if(cat.getCategoriacnh().getCdcategoriacnh() == 3){item.setCategCnhC(1);}
				if(cat.getCategoriacnh().getCdcategoriacnh() == 4){item.setCategCnhD(1);}
				if(cat.getCategoriacnh().getCdcategoriacnh() == 5){item.setCategCnhE(1);}
			}
		
			if(item.getDadobancario() != null){
				if(item.getDadobancario().getAgencia() != null){
					item.setAgencia(item.getDadobancario().getAgencia() + "-" + item.getDadobancario().getDvagencia());
				}
				if(item.getDadobancario().getConta() != null){
					item.setConta(item.getDadobancario().getConta() + "-" + item.getDadobancario().getDvconta());
				}
			}
			
			for (Colaboradorcargo cargo : item.getListaColaboradorcargo()) {
				if(cargo.getDtfim() == null){
					item.setDepartamentotransient(cargo.getDepartamento());
					item.setCargotransient(cargo.getCargo());
					item.setSindicato(cargo.getSindicato());
					item.setSalario(cargo.getSalario());
					item.setRegimecontratacao(cargo.getRegimecontratacao());
					
					if(cargo.getMatricula() != null){
						item.setMatricula(cargo.getMatricula());
					}
					break;
				}

			}
			
			item.setFotoImagem(getFoto(item));
		}
		
		report.addParameter("TITULO", "FICHA DO COLABORADOR");
		report.addSubReport("SUBFICHACOLABORADOR", new Report("/rh/fichaColaboradorSub"));
		report.setDataSource(listaColaborador);

		return report;
	}
	
	public static Image getFoto(Colaborador colaborador) {
		Image imageFoto = null;
		
		try {
			if (colaborador != null && colaborador.getFoto() != null && colaborador.getFoto().getCdarquivo() != null) {
				Arquivo foto = colaborador.getFoto();
				foto = ArquivoService.getInstance().loadWithContents(foto);
				imageFoto = ArquivoService.getInstance().loadAsImage(foto);
			}
		} catch (NeoException e) {
//			System.out.println("Foto para relat�rio n�o encontrada:");
			e.printStackTrace();
		}

		return imageFoto;
	}
	/**M�todo respons�vel por carregar os dados Agendamento de Servi�os, 
	 * Documentos para o relat�rio de Agendamentos de Servi�os.
	 * 
	 * @author Taidson
	 * @since 07/05/2010
	 */
	
	public IReport gerarRelatorioAgendaServico(ColaboradoragendaservicoReportFiltro filtro) {
		Report report = new Report("/rh/agendaServico");
		
		List<ColaboradoragendaservicoReportBean> lista = colaboradorService.buscarListagemColaboradorAgendamento(filtro, true);
		
		if(lista.size() == 0){
			throw new SinedException("N�o h� dados suficientes para a gera��o do relat�rio.");
		}
		
		report.setDataSource(lista);
		
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Empresa empresa = filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? filtro.getEmpresa() : empresaService.loadPrincipal();
		empresa = empresaService.loadForEntrada(empresa);
		
		report.addParameter("CNPJEMPRESA", empresa.getCnpj().toString());
		report.addParameter("ENDERECOEMPRESA", empresa.getEndereco()!= null? (empresa.getEndereco().getLogradouro()!=null? empresa.getEndereco().getLogradouro():"" )+", "+
				(empresa.getEndereco().getNumero()!=null?empresa.getEndereco().getNumero():"")+", "+
				(empresa.getEndereco().getComplemento()!=null?empresa.getEndereco().getComplemento():" ")+" - "+
				(empresa.getEndereco().getBairro()!=null?empresa.getEndereco().getBairro():"")+" "+
				(empresa.getEndereco().getMunicipio()!= null? empresa.getEndereco().getMunicipio().getNome():"")+" / "+
				(empresa.getEndereco().getMunicipio() != null && empresa.getEndereco().getMunicipio().getUf()!= null? empresa.getEndereco().getMunicipio().getUf().getSigla():""):"");
		report.addParameter("PERIODO", format.format(filtro.getDtinicio()) +" � "+ format.format(filtro.getDtfim()));
		
		return report;
	}
	
	/**
	 * Faz refer�ncia ao DAO. 
	 * 
	 * @author Taidson
	 * @since 07/05/2010
	 * 
	 */
	public List<Colaborador> comboColaboradorAgendamento(Material material){
		return colaboradorDAO.comboColaboradorAgendamento(material);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findForSolicitacaoservico
	 * 
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findForSolicitacaoservico() {
		return colaboradorDAO.findForSolicitacaoservico(null);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findForSolicitacaoservico
	 *
	 * @param departamento
	 * @return
	 * @author Rodrigo Freitas
	 */
	public List<Colaborador> findForSolicitacaoservico(Departamento departamento) {
		return colaboradorDAO.findForSolicitacaoservico(departamento);
	}
	
	/**
	 * M�todo preenche a combo de cargos pertencentes ao um determinado projeto
	 * @param projeto
	 * @return
	 * @author Taidson
	 * @sinice 18/06/2010
	 */
	public List<Cargo> carregaComboCargos(Projeto projeto){
		List<Cargo> listaCargosProjeto = new ListSet<Cargo>(Cargo.class);
		List<Colaboradorcargo> listaColaboradorcargo = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
		
		listaColaboradorcargo = colaboradorcargoService.cargosProjeto(projeto);

		if(listaColaboradorcargo != null && listaColaboradorcargo.size() > 0){
			for (Colaboradorcargo colaboradorcargo : listaColaboradorcargo) {
				if(!listaCargosProjeto.contains(colaboradorcargo.getCargo()))
					listaCargosProjeto.add(colaboradorcargo.getCargo());
			}
		}
		return listaCargosProjeto;
	}
	
	/**
	 * M�todo preenche a combo de colaboradores pertencentes ao um determinado cargo
	 * @param cargo
	 * @return
	 * @author Taidson
	 * @sinice 18/06/2010
	 */
	public List<Colaborador> carregaComboColaboradores(Cargo cargo){
		List<Colaborador> listaColaboradoresCargo = new ListSet<Colaborador>(Colaborador.class);
		List<Colaboradorcargo> listaColaboradorcargo = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
		
		listaColaboradorcargo = colaboradorcargoService.colaboradoresCargo(cargo);
		
		if(listaColaboradorcargo != null & listaColaboradorcargo.size() > 0){
			for (Colaboradorcargo colaboradorcargo : listaColaboradorcargo) {
				listaColaboradoresCargo.add(colaboradorcargo.getColaborador());
			}
		}
		return listaColaboradoresCargo;
	}
	
	/**
	 * M�todo que carrega o combo de colaboradores de acordo com o cargo e empresa
	 *
	 * @param cargo
	 * @param empresa
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaborador> carregaComboColaboradores(Cargo cargo, Empresa empresa){
		List<Colaborador> listaColaboradoresCargo = new ListSet<Colaborador>(Colaborador.class);
		List<Colaboradorcargo> listaColaboradorcargo = new ListSet<Colaboradorcargo>(Colaboradorcargo.class);
		
		listaColaboradorcargo = colaboradorcargoService.colaboradoresCargo(cargo, empresa);
		
		if(listaColaboradorcargo != null & listaColaboradorcargo.size() > 0){
			for (Colaboradorcargo colaboradorcargo : listaColaboradorcargo) {
				listaColaboradoresCargo.add(colaboradorcargo.getColaborador());
			}
		}
		return listaColaboradoresCargo;
	}
	
	/**
	 * M�todo respons�vel por carregar os dados para o relat�rio de Controle de Entrega de EPI
	 * @param filtro
	 * @return
	 * @author Taidson, Rodrigo Freitas
	 * @throws Exception 
	 * @since 18/06/2010
	 */
	public Resource gerarRelatorioEntregaEPI(EntregaEPIReportFiltro filtro, String whereIn, String tipo) throws Exception {
		
		MergeReport mergeReport = new MergeReport(tipo.toLowerCase()  + SinedUtil.datePatternForReport()+".pdf");
		
		List<Colaboradorcargo> listaColaboradores = colaboradorcargoService.findForRelatorioEntregaEPI(filtro, whereIn);
		
		String titulo = "CONTROLE DE ENTREGA DE EPI";
		Empresa empresa = (filtro.getEmpresa() != null && filtro.getEmpresa().getCdpessoa() != null ? 
				empresaService.carregaEmpresa(filtro.getEmpresa()) : empresaService.loadPrincipal());
		
		for (Colaboradorcargo colaboradorcargo : listaColaboradores) {
			Report report = null;
			
			List<Colaboradorequipamento> listaEquipamentoNaoVencido = new ArrayList<Colaboradorequipamento>();
			
			List<Colaboradorequipamento> listaColaboradorequipamento = colaboradorcargo.getColaborador().getListaColaboradorequipamento();
			if(listaColaboradorequipamento != null){
				for (Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento) {
					colaboradorequipamento = colaboradorequipamentoService.loadWithEPI(colaboradorequipamento);
					
					Date dtentrega = colaboradorequipamento.getDtentrega();
					colaboradorequipamento.setDtvencimentotrans(colaboradorequipamento.getDtvencimento() != null? colaboradorequipamento.getDtvencimento():
						dtentrega != null && colaboradorequipamento.getEpi().getDuracao() != null?
						SinedDateUtils.addSemanasData(dtentrega, colaboradorequipamento.getEpi().getDuracao()): null);
					
					listaEquipamentoNaoVencido.add(colaboradorequipamento);
				}
			}
			
			if(tipo.equals("termo")){
				report  = new Report("/rh/entregaEPITermoResponsabilidade");
				report.addParameter("PROJETO", colaboradorcargo.getProjeto());
				
				report.setDataSource(listaEquipamentoNaoVencido);
			} else {
				report  = new Report("/rh/entregaEPIControleEPI");
				
				Report sub1 = new Report("/rh/entregaEPIControleEPISub");
				Report sub2 = new Report("/rh/entregaEPIControleEPISub2");
				
				report.addSubReport("SUB_LISTANAOENTREGUE", sub1);
				report.addSubReport("SUB_LISTAENTREGUE", sub2);
				
				report.addParameter("LISTAENTREGUE", listaEquipamentoNaoVencido);
				
				Set<Cargomaterialseguranca> listaCargomaterialseguranca = colaboradorcargo.getCargo().getListaCargomaterialseguranca();
				if(listaCargomaterialseguranca != null){
					boolean achou = false;
					
					List<Cargomaterialseguranca> listaEquipamentoNaoEntregue = new ArrayList<Cargomaterialseguranca>();
					for (Cargomaterialseguranca cargomaterialseguranca : listaCargomaterialseguranca) {
						achou = false;
						for (Colaboradorequipamento colaboradorequipamento : listaEquipamentoNaoVencido) {
							if(colaboradorequipamento.getEpi() != null && 
									cargomaterialseguranca.getEpi() != null && 
									cargomaterialseguranca.getEpi().equals(colaboradorequipamento.getEpi())){
								achou = true;
								break;
							}
						}
						
						if(!achou){
							listaEquipamentoNaoEntregue.add(cargomaterialseguranca);
						}
					}
					
					report.addParameter("LISTANAOENTREGUE", listaEquipamentoNaoEntregue);
				}
			}
			
			report.addParameter("LOGO", SinedUtil.getLogo(empresa));
			report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
			report.addParameter("TITULO", titulo);
			report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
			report.addParameter("DATA", new Date(System.currentTimeMillis()));
			
			report.addParameter("COLABORADOR", colaboradorcargo.getColaborador());
			report.addParameter("CARGO", colaboradorcargo.getCargo());
			
			mergeReport.addReport(report);
		}
		
		return mergeReport.generateResource();
	}
	
	public Resource gerarRelatorioEntregaEPIExcel(EntregaEPIReportFiltro filtro, String whereIn) throws Exception {
		
		List<Colaboradorcargo> listaColaboradores = colaboradorcargoService.findForRelatorioEntregaEPI(filtro, whereIn);
		
		List<Colaboradorequipamento> listaEquipamentoNaoVencido = new ArrayList<Colaboradorequipamento>();
		List<Cargomaterialseguranca> listaEquipamentoNaoEntregue = new ArrayList<Cargomaterialseguranca>();
		
		for (Colaboradorcargo colaboradorcargo : listaColaboradores) {
			
			List<Colaboradorequipamento> listaColaboradorequipamento = colaboradorcargo.getColaborador().getListaColaboradorequipamento();
			if(listaColaboradorequipamento != null){
				for (Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento) {
					Date dtentrega = colaboradorequipamento.getDtentrega();
					colaboradorequipamento.setDtvencimentotrans(colaboradorequipamento.getDtvencimento() != null? colaboradorequipamento.getDtvencimento():
																dtentrega != null && colaboradorequipamento.getEpi().getDuracao() != null?
																SinedDateUtils.addSemanasData(dtentrega, colaboradorequipamento.getEpi().getDuracao()): null);
					if(dtentrega != null && colaboradorequipamento.getEpi() != null && colaboradorequipamento.getDtvencimentotrans() != null){
						if(SinedDateUtils.afterIgnoreHour(colaboradorequipamento.getDtvencimentotrans(), SinedDateUtils.currentDate())){
							listaEquipamentoNaoVencido.add(colaboradorequipamento);
						}
					} else {
						listaEquipamentoNaoVencido.add(colaboradorequipamento);
					}
				}
			}
			
			Set<Cargomaterialseguranca> listaCargomaterialseguranca = colaboradorcargo.getCargo().getListaCargomaterialseguranca();
			if (listaCargomaterialseguranca != null){
				boolean achou = false;
				for (Cargomaterialseguranca cargomaterialseguranca : listaCargomaterialseguranca) {
					achou = false;
					for (Colaboradorequipamento colaboradorequipamento : listaEquipamentoNaoVencido) {
						if(colaboradorequipamento.getEpi() != null && 
								cargomaterialseguranca.getEpi() != null && 
								cargomaterialseguranca.getEpi().equals(colaboradorequipamento.getEpi())){
							achou = true;
							break;
						}
					}
					if(!achou){
						listaEquipamentoNaoEntregue.add(cargomaterialseguranca);
					}
				}
			}
			
		}
		
		int linha = 0;
		
		SinedExcel wb = new SinedExcel();
		
		HSSFSheet p = wb.createSheet("Planilha");
		p.setDefaultColumnWidth((short) 20);
		p.setDisplayGridlines(false);
		
		HSSFRow row;
		HSSFCell cell;

        row = p.createRow((short) linha++);
        row.setHeight((short) (12*35));
        
        cell = row.createCell((short) 0);
        cell.setCellStyle(SinedExcel.STYLE_HEADER_RELATORIO);
        cell.setCellValue("EQUIPAMENTOS - MATERIAIS");
        
        p.addMergedRegion(new Region(0, (short) 0, 0, (short) 13 ));
        
        String[] colunas = new String[7];
        colunas[0] = "EPI FORNECIDO";
        colunas[1] = "QTDE";
        colunas[2] = "DATA ENTREGA";
        colunas[3] = "CA";
        colunas[4] = "VALIDADE CA";
        colunas[5] = "DATA DEVOLU��O";
        colunas[6] = "RUBRICA";
        
        row = p.createRow((short) linha++);
        
        for (int i=0; i<7; i++){
        	
        	int inc = 7;
        	
        	cell = row.createCell((short) i);
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
            cell.setCellValue(colunas[i]);
            
            cell = row.createCell((short) (i+inc));
            cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_GREY);
            cell.setCellValue(colunas[i]);
        }
        
        Collections.sort(listaEquipamentoNaoVencido, new Comparator<Colaboradorequipamento>() {
			@Override
			public int compare(Colaboradorequipamento o1, Colaboradorequipamento o2) {
				return o1.getEpi().getNomeMaterialNomeReduzido().compareToIgnoreCase(o2.getEpi().getNomeMaterialNomeReduzido());
			}
		});
        
        Collections.sort(listaEquipamentoNaoEntregue, new Comparator<Cargomaterialseguranca>() {
        	@Override
        	public int compare(Cargomaterialseguranca o1, Cargomaterialseguranca o2) {
        		return o1.getEpi().getNomeMaterialNomeReduzido().compareToIgnoreCase(o2.getEpi().getNomeMaterialNomeReduzido());
        	}
        });
        
        int mod = 0;
        
        for (Cargomaterialseguranca cargomaterialseguranca: listaEquipamentoNaoEntregue){
        	
        	if (mod%2==0){
        		row = p.createRow((short) linha++);
        	}
        	
        	for (int i=0; i<7; i++){
            	
        		int inc = mod%2==0 ? 0 : 7;
        		String value = "";
        		
        		if (i==0){
        			value = cargomaterialseguranca.getEpi().getNomeMaterialNomeReduzido();
        		}else if (i==1){
        			value = cargomaterialseguranca.getQuantidade()!=null ? cargomaterialseguranca.getQuantidade().toString() : "";
        		}else if (i==3){
        			value = cargomaterialseguranca.getEpi().getCaepi()!=null ? cargomaterialseguranca.getEpi().getCaepi().toString() : "";
        		}
        		
                cell = row.createCell((short) (i+inc));
                cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
                cell.setCellValue(value);
            }
        	mod++;
        }
        
        for (Colaboradorequipamento colaboradorequipamento: listaEquipamentoNaoVencido){
        	
        	if (mod%2==0){
        		row = p.createRow((short) linha++);
        	}
        	
        	for (int i=0; i<7; i++){
        		
        		int inc = mod%2==0 ? 0 : 7;
        		String value = "";
        		
        		if (i==0){
        			value = colaboradorequipamento.getEpi().getNomeMaterialNomeReduzido();
        		}else if (i==1){
        			value = colaboradorequipamento.getQtde()!=null ? colaboradorequipamento.getQtde().toString() : "";
        		}else if (i==2){
        			value = new SimpleDateFormat("dd/MM/yyyy").format(colaboradorequipamento.getDtentrega());
        		}else if (i==3){
        			value = !Util.strings.isEmpty(colaboradorequipamento.getCertificadoaprovacao())? colaboradorequipamento.getCertificadoaprovacao():
        					colaboradorequipamento.getEpi().getCaepi()!=null ? colaboradorequipamento.getEpi().getCaepi().toString() : "";
        		}else if (i==4){
        			value = colaboradorequipamento.getDtvencimentotrans()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(colaboradorequipamento.getDtvencimentotrans()) : "";
        		}else if (i==5){
        			value = colaboradorequipamento.getDtdevolucao()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(colaboradorequipamento.getDtdevolucao()) : "";
        		}
        		
        		cell = row.createCell((short) (i+inc));
        		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
        		cell.setCellValue(value);
        	}
        	mod++;
        }
        
        if (mod%2==1){
        	for (int i=0; i<7; i++){
        		int inc = 7;
        		cell = row.createCell((short) (i+inc));
        		cell.setCellStyle(SinedExcel.STYLE_DETALHE_LEFT_WHITE);
        		cell.setCellValue("");
        	}
        }
        
        return wb.getWorkBookResource("controle_" + SinedUtil.datePatternForReport() + ".xls");
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * @param cdpessoa
	 * @return
	 * @author Taidson
	 * @since 19/07/2010
	 */
	public Colaborador carregaColaboradorresponsavel(Integer cdpessoa) {
		return colaboradorDAO.carregaColaboradorresponsavel(cdpessoa);
	}
	
	/**
	 * Cria relat�rio de etiquetas de colaboradores de com os parametros do filtro.
	 * @param filtro
	 * @param whereIn
	 * @return
	 * @author Taidson
	 * @since 04/08/2010
	 */
	public IReport createRelatorioEtiqueta(VwcolaboradoretiquetaFiltro filtro, String whereIn) {
		Report report = new Report("rh/relEtiqueta" + filtro.getTipoetiqueta().getSufix());
		
		//List<Endereco> listaEndereco = enderecoService.findForEtiquetaColaborador(whereIn);
		List<String> listAux = new ArrayList<String>();
		for(String str: whereIn.split(",")){
			listAux.add("'"+str+"'");
		}
		List<Vwcolaboradoretiqueta> listaVwcolaboradoretiquetas = vwcolaboradoretiquetaService.findForColaboradorEtiqueta(CollectionsUtil.concatenate(listAux, ",")); 
		List<ColaboradorEtiqueta> listaColaboradorEtiqueta = new ArrayList<ColaboradorEtiqueta>();
		ColaboradorEtiqueta colaboradorEtiqueta = null;
		
		for (Vwcolaboradoretiqueta vwcolaboradoretiqueta : listaVwcolaboradoretiquetas) {
			colaboradorEtiqueta = new ColaboradorEtiqueta();
			/*
			if(vwcolaboradoretiqueta.getBairro() == null)
				colaboradorEtiqueta.setBairro(vwcolaboradoretiqueta.getBairro());
			else*/
			colaboradorEtiqueta.setBairro(vwcolaboradoretiqueta.getBairro());
			colaboradorEtiqueta.setCep(vwcolaboradoretiqueta.getCep() != null ? vwcolaboradoretiqueta.getCep().toString() : null);
			colaboradorEtiqueta.setCidade(vwcolaboradoretiqueta.getMunicipio_nome() != null ? vwcolaboradoretiqueta.getMunicipio_nome() : null);
			colaboradorEtiqueta.setUf(vwcolaboradoretiqueta.getUf_sigla() != null ? vwcolaboradoretiqueta.getUf_sigla() : null);
			colaboradorEtiqueta.setNomea(vwcolaboradoretiqueta.getPessoa_nome() != null ? vwcolaboradoretiqueta.getPessoa_nome() : null);
			colaboradorEtiqueta.setEndereco(vwcolaboradoretiqueta.getLogradouro() != null ? vwcolaboradoretiqueta.getLogradouro() : null);
			
			listaColaboradorEtiqueta.add(colaboradorEtiqueta);
		}
		
		if(filtro.getApartircoluna() != null && filtro.getApartirlinha() != null){
			int posicao = filtro.getApartirlinha() * 2;
			if (filtro.getApartircoluna() % 2 != 0){
				for (int i = 0; i < posicao -2; i++) {
					listaColaboradorEtiqueta.add(0,new ColaboradorEtiqueta());
				}
			} else if (filtro.getApartircoluna().equals(2)){
				for (int i = 0; i < posicao -1; i++) {
					listaColaboradorEtiqueta.add(0,new ColaboradorEtiqueta());
				}
			}
		}
		
		
		if(filtro.getTipoetiqueta().equals(Tipoetiqueta.COMUNICADO)){
			Empresa empresa = empresaService.loadForEntrada(empresaService.loadPrincipal());
			String nomeEmp ="";
			String razaoEmp ="";
			String enderecoEmp ="";
			String bairroEmp ="";
			String municipioEmp ="";
			String ufEmp ="";
			String cepEmp ="";
			//setando dados de endere�o da empresa principal
			if (empresa != null && empresa.getListaEndereco() != null && empresa.getListaEndereco().size() > 0){
				if (empresa.getRazaosocialOuNome()!= null)
					razaoEmp += empresa.getRazaosocialOuNome();
				if (empresa.getListaEnderecoLIST().get(0).getLogradouro() != null)
					enderecoEmp += empresa.getListaEnderecoLIST().get(0).getLogradouro() +" ";
				if (empresa.getListaEnderecoLIST().get(0).getNumero() != null)
					enderecoEmp += empresa.getListaEnderecoLIST().get(0).getNumero()+" ";
				if (empresa.getListaEnderecoLIST().get(0).getComplemento() != null)
					enderecoEmp += empresa.getListaEnderecoLIST().get(0).getComplemento()+" ";
	
				if (empresa.getListaEnderecoLIST().get(0).getBairro() != null)
					bairroEmp += empresa.getListaEnderecoLIST().get(0).getBairro();
				if (empresa.getListaEnderecoLIST().get(0).getMunicipio() != null){
					municipioEmp += empresa.getListaEnderecoLIST().get(0).getMunicipio().getNome();
					ufEmp += empresa.getListaEnderecoLIST().get(0).getMunicipio().getUf().getSigla();
				}
				if (empresa.getListaEnderecoLIST().get(0).getCep() != null)
					cepEmp += empresa.getListaEnderecoLIST().get(0).getCep();	
				if (empresa.getListaEnderecoLIST().get(0).getCaixapostal() != null)
					bairroEmp += " "+empresa.getListaEnderecoLIST().get(0).getCaixapostal();
			}
			
			//adicionando paramentros do tipo comunicado
			report.addParameter("NOMEEMP", nomeEmp);
			report.addParameter("RAZAOEMP", razaoEmp);
			report.addParameter("ENDERECOEMP", enderecoEmp);
			report.addParameter("BAIRROEMP", bairroEmp);
			report.addParameter("MUNICIPIOEMP", municipioEmp);
			report.addParameter("UFEMP", ufEmp);
			report.addParameter("CEPEMP", cepEmp);
		}
		
		report.addParameter("FORMATADOR", new MessageFormat("{0}\n{1}\n{2}\n{3}\n{4} {5} {6}\n"));
		report.setDataSource(listaColaboradorEtiqueta);
		return report;
	}
	
	/**
	 * M�todo respons�vel por manipular dodos da listagem e montar o arquivo CSV
	 * da listagem de Colaborador
	 * @param filtro
	 * @return
	 * @author Taidson
	 * @since 30/08/2010
	 */
	@SuppressWarnings("unchecked")
	public Resource gerarRelatorioCSVListagem(ColaboradorFiltro filtro) {
		filtro.setPageSize(Integer.MAX_VALUE);
		ListagemResult<Colaborador> listagemResult = this.findForListagemGerarCSVColaborador(filtro);
		Calendar dtnasc = Calendar.getInstance();
		Colaboradorcargo colaboradorcargo = null;
		
		StringBuilder csv = new StringBuilder();
		csv.append("\"Nome\";\"CPF\";\"RG\";\"Endere�o\";\"Telefones\";\"E-mail\";\"Dia de Anivers�rio\";\"M�s de Anivers�rio\";\"Data de Admiss�o\";\"N� Carteira\";\"PIS\";\"Departamento\";\"Cargo\";\"Sal�rio\";\"Data de in�cio\";\"Data de fim\";\n");
		
		for (Colaborador colaborador : listagemResult.list()) {
			colaborador.setListaTelefone(SinedUtil.listToSet(telefoneService.findByPessoa(colaborador), Telefone.class));
			
			if(colaborador.getListaTelefone()!= null){
				colaborador.setTelefones(telefoneService.createListaTelefone(colaborador.getListaTelefone(), " "));
			}
			if(colaborador.getDtnascimento() != null){
				dtnasc.setTime(colaborador.getDtnascimento());
			}
		
			csv
			.append("\"" + colaborador.getNome() + "\";")
			.append("\"" + (colaborador.getCpfOuCnpj() != null ? colaborador.getCpfOuCnpj() : "") + "\";")
			.append("\"" + (colaborador.getRg() != null ? colaborador.getRg() : "") + "\";")
			.append("\"" + (colaborador.getEndereco() != null ? colaborador.getEndereco().getLogradouroCompletoComBairro() : "") + "\";")
			.append("\"" + (colaborador.getTelefones().replace("\n", " ") + "\";"))
			.append("\"" + (colaborador.getEmail() != null ? colaborador.getEmail() : "") + "\";")
			.append("\"" + (colaborador.getDtnascimento() != null ? dtnasc.get(Calendar.DAY_OF_MONTH) : "") + "\";")
			.append("\"" + (colaborador.getDtnascimento() != null ? dtnasc.get(Calendar.MONTH)+1 : "") + "\";")
			.append("\"" + (colaborador.getDtadmissao() != null ? colaborador.getDtadmissao().toString() : "" ) + "\";")
			.append("\"" + (colaborador.getCarteiratrabalho() != null ? colaborador.getCarteiratrabalho() : "") + "\";");
			
			if(colaborador.getTipoinscricao() != null && colaborador.getTipoinscricao().getCdtipoinscricao() != null 
					&& Tipoinscricao.PIS.equals(colaborador.getTipoinscricao().getCdtipoinscricao())){
				csv.append("\"" + (colaborador.getNumeroinscricao() != null ? colaborador.getNumeroinscricao() : "") + "\";");
			} else {
				csv.append("\"\";");
			}
			
			if(colaborador.getListaColaboradorcargo() != null && !colaborador.getListaColaboradorcargo().isEmpty() ){
				colaboradorcargo = null;
				int count = 0;
				
				for(Colaboradorcargo colaboradorcargo1 : colaborador.getListaColaboradorcargo()){
					
					boolean departamentoDiferente = colaboradorcargo != null && (colaboradorcargo1.getDepartamento()!=null && colaboradorcargo1.getDepartamento().getNome()!=null
							&& colaboradorcargo.getDepartamento()!=null && colaboradorcargo.getDepartamento().getNome()!=null
							&& !colaboradorcargo1.getDepartamento().getNome().equals(colaboradorcargo.getDepartamento().getNome()));
					
					if(colaboradorcargo == null || departamentoDiferente){
						
						if(departamentoDiferente) csv.append("\n\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";");
						
						colaboradorcargo = colaboradorcargo1;
						
						if(colaboradorcargo.getDepartamento()!=null)
							csv.append("\"" + (colaboradorcargo.getDepartamento().getNome()!=null ? colaboradorcargo.getDepartamento().getNome():"") + "\";");
						else
							csv.append("\"\";");
						
						if(colaboradorcargo.getCargo()!=null)
							csv.append("\"" + (colaboradorcargo.getCargo().getNome()!=null ? colaboradorcargo.getCargo().getNome():"") + "\";");
						else
							csv.append("\"\";");
						
						csv.append("\"" + (colaboradorcargo.getSalario()!=null ? colaboradorcargo.getSalario():"") + "\";");
						csv.append("\"" + (colaboradorcargo.getDtinicio()!=null ? SinedDateUtils.toString(colaboradorcargo.getDtinicio()):"") + "\";");
						csv.append("\"" + (colaboradorcargo.getDtfim()!=null ? SinedDateUtils.toString(colaboradorcargo.getDtfim()):"") + "\";");
					
					} else {
						colaboradorcargo = colaboradorcargo1;
						
						csv.append("\n\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";");
						
						if(colaboradorcargo.getCargo() != null)
							csv.append("\"" + (colaboradorcargo.getCargo().getNome()!=null ? colaboradorcargo.getCargo().getNome():"") + "\";");
						else
							csv.append("\"\";");
						
						csv.append("\"" + (colaboradorcargo.getSalario()!=null ? colaboradorcargo.getSalario():"") + "\";");
					}
					
					count++;
					
					if(count >= colaborador.getListaColaboradorcargo().size()) csv.append("\n");
				}
			} else {
				csv.append("\"\";\"\";\"\";\"\";\"\";\n");
			}
		}
		
		Resource resource = new Resource("text/csv", "colaborador_" + SinedUtil.datePatternForReport() + ".csv", csv.toString().getBytes());
		return resource;
	}
	
	public List<ColaboradoragendaservicoReportBean> buscarListagemColaboradorAgendamento(ColaboradoragendaservicoReportFiltro filtro, boolean fromReport) {
		return colaboradorDAO.buscarListagemColaboradorAgendamento(filtro, fromReport);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param contrato
	 * @return
	 * @author Taidson
	 * @since 01/12/2010
	 */
	public List<Colaborador> loadColaboradores(Contrato contrato) {
		if(contrato != null){
			return colaboradorDAO.loadColaboradores(contrato);
		}else{
			return colaboradorDAO.loadColaboradoresAtivos();
		}
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * @param contrato
	 * @return
	 * @author Vinicius Moret
	 * @since 29/07/2013
	 */
	public List<Colaborador> loadColaboradoresAtivos(boolean vendedoresAtuais) {
		if(vendedoresAtuais)
			return colaboradorDAO.loadColaboradoresAtivos();
		else return colaboradorDAO.findAll();
	}

	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o Colaborador pela Matr�cula.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByMatricula(Integer matricula)
	 * 
	 * @param matricula
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByMatricula(Long matricula) {
		return colaboradorDAO.findByMatricula(matricula, null);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Busca o Colaborador pela Matr�cula e empresa.
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findByMatricula(Integer matricula)
	 * 
	 * @param matricula
	 * @param empresa
	 * @return
	 * @since Jun 10, 2011
	 * @author Rodrigo Freitas
	 */
	public Colaborador findByMatricula(Long matricula, Empresa empresa) {
		return colaboradorDAO.findByMatricula(matricula, empresa);
	}
	
	public List<Colaborador> findAllByMatricula(Long matricula) {
		return colaboradorDAO.findAllByMatricula(matricula);
	}
	
	/**
	 * Faz refer�ncia ao DAO.
	 * Executa o update no registro de colaborador.
	 * 
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO.updateCampoColaborador(boolean pessoa, String campo, String valor, Colaborador colaborador)
	 * 
	 * @param pessoa
	 * @param campo
	 * @param valor
	 * @param colaborador
	 * @since Jul 6, 2011
	 * @author Rodrigo Freitas
	 * @throws UnsupportedEncodingException 
	 */
	public void updateCampoColaborador(boolean pessoa, String campo, Object valor, Colaborador colaborador) throws UnsupportedEncodingException {
		colaboradorDAO.updateCampoColaborador(pessoa, campo, valor, colaborador);
	}
	
	/**
	* M�todo para listar colaborador da tarefa ou do cargo
	* Se a tarefa tiver colaborador, exibe colaborador da tarefa, sen�o exibe colaborador do cargo
	*
	* @param cargo
	* @param tarefa
	* @return
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaborador> listaColaboradorTarefaOuCargo(Cargo cargo, Tarefa tarefa){
		
		
		if(tarefa != null && tarefa.getCdtarefa() != null){
			List<Colaborador> listaColaboradortarefa = this.findByColaboradorTarefa(tarefa);
			if(listaColaboradortarefa != null && !listaColaboradortarefa.isEmpty()){
				return listaColaboradortarefa;
			}
		}
		
		return this.findByCargo(cargo);
	}
	
	/**
	* M�todo com refer�ncia ao DAO
	* Busca lista de colaborador alocado na tarefa
	*
	* @see	br.com.linkcom.sined.geral.dao.ColaboradorDAO.findByColaboradorTarefa(Tarefa tarefa)
	*
	* @param tarefa
	* @return
	* @since Aug 12, 2011
	* @author Luiz Fernando F Silva
	*/
	public List<Colaborador> findByColaboradorTarefa(Tarefa tarefa){
		return colaboradorDAO.findByColaboradorTarefa(tarefa);
	}
	
	/**
	 * Busca todos os colaboradores com seus salario. 
	 * @author Filipe Santos
	 * @since 18/01/2012
	 * 
	 */
	public List<Colaboradorholerite> findByHolerite(HoleriteFiltro filtro){
		return colaboradorDAO.findByHolerite(filtro);
	}
	
	/**
	 * 
	 * M�todo com refer�ncia no DAO.
	 *
	 *@author Thiago Augusto
	 *@date 02/02/2012
	 * @return
	 */
	public List<Colaborador> findListColaboradorOportunidade(){
		return colaboradorDAO.findListColaboradorOportunidade();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#isColaboradorTarefa(Tarefa tarefa, Colaborador colaborador)
	 * 
	 * @param tarefa
	 * @param colaborador
	 * @return
	 */
	public Boolean isColaboradorTarefa(Tarefa tarefa, Colaborador colaborador) {
		return colaboradorDAO.isColaboradorTarefa(tarefa, colaborador);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 * 
	 * @see br.com.linkcom.sined.geral.service.ColaboradorService#isColaboradorCargo(Cargo cargo, Colaborador colaborador)
	 * 
	 * @param cargo
	 * @param colaborador
	 * @return
	 */
	public Boolean isColaboradorCargo(Cargo cargo, Colaborador colaborador){
		return colaboradorDAO.isColaboradorCargo(cargo, colaborador);
	}
	
	
	
	/**
     * Busca os dados para o cache da tela de pedido de venda offline
     * @author Igor Silv�rio Costa
	 * @date 20/04/2012
	 * 
	 */
	public List<ColaboradorOfflineJSON> findForPVOffline() {
		List<ColaboradorOfflineJSON> lista = new ArrayList<ColaboradorOfflineJSON>();
		for(Colaborador c : colaboradorDAO.findForPVOffline())
			lista.add(new ColaboradorOfflineJSON(c));
		
		return lista;
	}
	
	/**
	 * M�todo com refer�ncia no DAO.
	 * @return
	 * @author Rafael Salvio
	 * @param colaborador 
	 */
	public List<Colaborador> findColaboradoresComVenda(Empresa empresa, Colaborador colaborador, boolean ativo){
		return colaboradorDAO.findColaboradoresComVenda(empresa, colaborador, ativo);
	}
	
	public List<Colaborador> findWithVenda(Empresa empresa){
		return colaboradorDAO.findWithVenda(empresa);
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#findColaboradoresAutocompleteVenda(String s, Boolean isRestricaoClienteVendedor)
	 *
	 * @param s
	 * @return
	 * @author Luiz Fernando
	 */
	public List<Colaborador> findColaboradoresAutocompleteVenda(String s){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Boolean isRestricaoVendaVendedor = Boolean.FALSE;
		Boolean isRestricaoClienteVendedor = Boolean.FALSE;
		if(usuarioLogado != null && usuarioLogado.getCdpessoa() != null){
			isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(usuarioLogado);
			isRestricaoClienteVendedor = usuarioService.isRestricaoClienteVendedor(usuarioLogado);
		}
		return colaboradorDAO.findColaboradoresAutocompleteVenda(s, isRestricaoVendaVendedor, isRestricaoClienteVendedor);
	}
	
	public List<Colaborador> findColaboradoresVenda(String s){
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		Boolean isRestricaoVendaVendedor = Boolean.FALSE;
		Boolean isRestricaoClienteVendedor = Boolean.FALSE;
		if(usuarioLogado != null && usuarioLogado.getCdpessoa() != null){
			isRestricaoVendaVendedor = usuarioService.isRestricaoVendaVendedor(usuarioLogado);
			isRestricaoClienteVendedor = usuarioService.isRestricaoClienteVendedor(usuarioLogado);
		}
		return colaboradorDAO.findColaboradoresVenda(s, isRestricaoVendaVendedor, isRestricaoClienteVendedor);
	}
	
	public Colaborador loadForMovimentacaocontabil(Pessoa pessoa) {
		return colaboradorDAO.loadForMovimentacaocontabil(pessoa);
	}
	
	/**
	 * M�todo que gera o CSV da autoriza��o de exame
	 *
	 * @param filtro
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView createCSVAutorizacaoExame(EmitirautorizacaoReportFiltro filtro) {
		if	(filtro == null){
			throw new SinedException("O par�metro filtro n�o pode ser vazio.");
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy");
		List<Colaborador> lista = colaboradorService.findForAutorizacaoExame(filtro);
		lista = carregaColaboradorCargoAtual(lista);
		
		StringBuilder csv = new StringBuilder();
		StringBuilder indiceresultado = new StringBuilder();
		csv
			.append("\"Colaborador\";\"CPF\";\"Empresa\";\"Projeto\";\"Departamento\";\"Cargo\";\"Data de Admiss�o\";\"Data de Exame\";\"Profissional Respons�vel\";")
			.append("\"Tipo\";\"Natureza\";\"Exame(R/S)\";\"Pr�ximo Exame\";\"Indica��o de Resultado\"\n");
		
		if(lista != null && !lista.isEmpty()){
			for(Colaborador colaborador : lista){
				csv
					.append("\"" + colaborador.getNome() + "\"").append(";")
					.append("\"" + colaborador.getCpf() + "\"").append(";");
				
				//empresa
				if(colaborador.getEmpresa() != null)
					csv.append("\"" + colaborador.getEmpresa().getRazaosocialOuNome() + "\"");
				else
					csv.append("\"\";");
				csv.append(";");
				
				//projeto
				if(colaborador.getProjetotransient() != null)
					csv.append("\"" + colaborador.getProjetotransient().getNome() + "\"");
				csv.append(";");
				
				//departamento
				if(colaborador.getDepartamentotransient() != null)
					csv.append("\"" + colaborador.getDepartamentotransient().getNome() + "\"");
				csv.append(";");
				
				//cargo
				if(colaborador.getCargotransient() != null)
					csv.append("\"" + colaborador.getCargotransient().getNome() + "\"");
				csv.append(";");
				
				//admissao
				if(colaborador.getDtadmissao() != null)
					csv.append("\"" + colaborador.getDtadmissao() + "\"");
				csv.append(";");
				
				if(colaborador.getListaColaboradorexame() != null && !colaborador.getListaColaboradorexame().isEmpty()){
					for(Colaboradorexame colaboradorexame : colaborador.getListaColaboradorexame()){
						//data do exame
						if(colaboradorexame.getDtexame() != null)
							csv.append("\"" + format.format(colaboradorexame.getDtexame()) + "\"");
						csv.append(";");
						
						//funcion�rio respons�vel
						if(colaboradorexame.getExameresponsavel() != null && colaboradorexame.getExameresponsavel().getNome() != null)
							csv.append("\"" + colaboradorexame.getExameresponsavel().getNome() + "\"");
						csv.append(";");
						
						//tipo
						if(colaboradorexame.getExametipo() != null)
							csv.append("\"" + colaboradorexame.getExametipo().getNome() + "\"");
						csv.append(";");
						
						//natureza
						if(colaboradorexame.getExamenatureza() != null)
							csv.append("\"" + colaboradorexame.getExamenatureza().getNome() + "\"");
						csv.append(";");				
						
						//exame r/s
						if(colaboradorexame.getExamers() != null)
							csv.append("\"" + (colaboradorexame.getExamers() ? "Referencial" : "Sequencial")  + "\"");
						csv.append(";");
						
						//data pr�ximo exame
						if(colaboradorexame.getDtproximoexame() != null)
							csv.append("\"" + format.format(colaboradorexame.getDtproximoexame()) + "\"");
						csv.append(";");
						
						indiceresultado = new StringBuilder();
						
						csv.append("\"");
						if(colaboradorexame.getResultadonormal() != null && colaboradorexame.getResultadonormal())
							indiceresultado.append("Normal");
						if(colaboradorexame.getResultadoalterado() != null && colaboradorexame.getResultadoalterado())
							indiceresultado
								.append(indiceresultado.toString().equals("") ? "": "/")
								.append("Alterado");
						if(colaboradorexame.getResultadoestavel() != null && colaboradorexame.getResultadoestavel())
							indiceresultado
								.append(indiceresultado.toString().equals("") ? "": "/")
								.append("Est�vel");
						if(colaboradorexame.getResultadoagravamento() != null && colaboradorexame.getResultadoagravamento())
							indiceresultado
								.append(indiceresultado.toString().equals("") ? "": "/")
								.append("Agravamento");
						if(colaboradorexame.getResultadoocupacional() != null && colaboradorexame.getResultadoocupacional())
							indiceresultado
								.append(indiceresultado.toString().equals("") ? "": "/")
								.append("Ocupacional");
						if(colaboradorexame.getResultadonaoocupacional() != null && colaboradorexame.getResultadonaoocupacional())
							indiceresultado
								.append(indiceresultado.toString().equals("") ? "": "/")
								.append("N�o ocupacional");
						csv.append(indiceresultado.toString())
							.append("\";\n\"\";\"\";\"\";\"\";\"\";\"\";\"\";");
					}
				}
				
				csv.append("\n");				
			}
		}
		
		Resource resource = new Resource("text/csv","autorizacao_exame_medico" + SinedUtil.datePatternForReport() + ".csv", csv.toString().replaceAll("null;", ";").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
	/**
	 * M�todo que retorna o respons�vel pelo Departamento do colaborador
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 */
	public Colaborador getResponsavelDepartamento(Colaborador colaborador) {
		Colaborador responsavel = null;
		
		if(colaborador != null && colaborador.getCdpessoa() != null){
			Colaboradorcargo colaboradorcargo = colaboradorcargoService.loadCargoatualWithResponsavelDepartamento(colaborador);
			if(colaboradorcargo != null && colaboradorcargo.getDepartamento() != null && 
					colaboradorcargo.getDepartamento().getResponsavel() != null && 
					colaboradorcargo.getDepartamento().getResponsavel().getEmail() != null && 
					!"".equals(colaboradorcargo.getDepartamento().getResponsavel().getEmail())){
				responsavel = colaboradorcargo.getDepartamento().getResponsavel();
			}
		}
		return responsavel;
	}
	
	public List<Colaborador> findForIntegracaoEmporium() {
		return colaboradorDAO.findForIntegracaoEmporium();
	}
	
	/**
	 * M�todo com refer�ncia no DAO
	 *
	 * @see br.com.linkcom.sined.geral.dao.ColaboradorDAO#loadWithListaCargo(Colaborador colaborador)
	 *
	 * @param colaborador
	 * @return
	 * @author Luiz Fernando
	 * @since 23/01/2014
	 */
	public Colaborador loadWithListaCargo(Colaborador colaborador) {
		return colaboradorDAO.loadWithListaCargo(colaborador);
	}
	
	public List<Colaborador> findAllDestinatarios() {
		return colaboradorDAO.findAllDestinatarios();
	}
	
	public Colaborador findByNumeroinscricao(String numeroInscricao) {
		return colaboradorDAO.findByNumeroinscricao(numeroInscricao);
	}
	
	/**
	 * M�todo respons�vel pela integridade dos dados lan�ados no arquivo.
	 * Garante que todas as marca��es registradas possue hora de entrada e sa�da.
	 * Lan�amentos n�o conforme ser�o removidos da lista.
	 * 
	 * @see ProcessararquivopontoProcess#processar(br.com.linkcom.neo.core.web.WebRequestContext)
	 * @author Filipe Santos
	 * @since 03/01/2014
	 * @param List<ColaboradorVO> listaColaboradorVO
	 * @return List<ColaboradorVO> listaColaboradorVO
	 */
	public List<ColaboradorVO> removeLancamentoIncompleto (List<ColaboradorVO> listaColaboradorVO){		
		if(listaColaboradorVO==null || listaColaboradorVO.isEmpty())
			throw new SinedException("O arquivo processado n�o segue o padr�o AFD.");
				
		for (ColaboradorVO colaboradorVO : listaColaboradorVO) {
			if(!colaboradorVO.getListaPeriodo().isEmpty()){
				Iterator<PeriodoVO> iterator = colaboradorVO.getListaPeriodo().iterator();
				while (iterator.hasNext()){
					PeriodoVO periodoVO = iterator.next();
					if(periodoVO.getEntrada()!=null && periodoVO.getSaida()==null){
						iterator.remove();									
					}
				}
			}
		}				
		return listaColaboradorVO;
	}
	
	/**
	 * 
	 * Trata e valida os dados de equipamento do colaborador para ent�o gerar a sa�da ou a entrada desses equipamentos no estoque de acordo com a
	 * intera��o do usu�rio
	 * 
	 * @param colaborador
	 * @since 25/07/2014
	 * @author Rafael Patr�cio
	 */
	public void geraSaidaEntradaEquipamentoColaborador(Colaborador colaborador){
		List<Colaboradorequipamento> listaColaboradorequipamento = colaborador.getListaColaboradorequipamento();
		
		if(listaColaboradorequipamento!=null && !listaColaboradorequipamento.isEmpty()){
			
			List<Colaboradorequipamento> listaEntradaNoEstoque = new ArrayList<Colaboradorequipamento>();
			List<Colaboradorequipamento> listaSaidaDoEstoque = new ArrayList<Colaboradorequipamento>();
			
			if(colaborador.getCdpessoa()!=null){
				List<Colaboradorequipamento> listaAntes = colaboradorequipamentoService.loadByColaborador(colaborador);
				
				for(Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento){
					colaboradorequipamento.setColaborador(colaborador);
					
					//Se o equipamento n�o possui cdcolaboradorequipamento ent�o � uma nova entrega de equipamento ao colaborador
					if(colaboradorequipamento.getCdcolaboradorequipamento()==null){
						if(colaboradorequipamento.getQtde()<1){
							throw new SinedException("A quantidade do equipamento n�o pode ser 0");
						}
						//Preciso derar sa�da no estoque para o equipamento dessa itera��o
						listaSaidaDoEstoque.add(colaboradorequipamento);
						
						//Caso contr�rio eu verifico se o equipamento est� sendo entregue nesse momento
					}else{
						//Se a data de devolu��o n�o for nula eu n�o continuo o processo pois n�o houve devolu��o
						if(colaboradorequipamento.getDtdevolucao()!=null){
							
							//� imposs�vel haver um equipamento com a data de devolu��o != null e a listaAntes estar null ou vazia
							if(listaAntes!=null && !listaAntes.isEmpty()){
								
								//Comparo com a lista de equipamentos como estava antes da altera��o do cliente...
								for(Colaboradorequipamento equipamentoAntes : listaAntes){
									//...se for o mesmo equipamento...
									if(colaboradorequipamento.getCdcolaboradorequipamento().equals(equipamentoAntes.getCdcolaboradorequipamento())){
										//... e se ele j� n�o estava entregue...
										if(equipamentoAntes.getDtdevolucao()==null){
											//Gero entrada no estoque para o equipamento dessa itera��o 
											listaEntradaNoEstoque.add(colaboradorequipamento);
										}
									}
								}
							}else throw new SinedException("Erro na devolu��o de equipamento, n�o existem equipamentos em posse do colaborador");
						}
					}
				}
			}else{
				for(Colaboradorequipamento colaboradorequipamento : listaColaboradorequipamento){
					if(colaboradorequipamento.getQtde()<1){
						throw new SinedException("A quantidade do equipamento n�o pode ser 0");
					}
					listaSaidaDoEstoque.add(colaboradorequipamento);
				}
			}
			
			if(!listaSaidaDoEstoque.isEmpty()){
				movimentacaoestoqueService.geraSaidaEntradaEquipamentoColaborador(listaSaidaDoEstoque, Movimentacaoestoquetipo.SAIDA);
			}
			if(!listaEntradaNoEstoque.isEmpty()){
				movimentacaoestoqueService.geraSaidaEntradaEquipamentoColaborador(listaEntradaNoEstoque, Movimentacaoestoquetipo.ENTRADA);
			}
		}
	}
	
	public List<ColaboradorRESTModel> findForAndroid() {
		return findForAndroid(null, true);
	}

	public List<ColaboradorRESTModel> findForAndroid(String whereIn, boolean sincronizacaoInicial) {
		List<ColaboradorRESTModel> lista = new ArrayList<ColaboradorRESTModel>();
		if(sincronizacaoInicial || StringUtils.isNotEmpty(whereIn)){
			for(Colaborador bean : colaboradorDAO.findForAndroid(whereIn))
				lista.add(new ColaboradorRESTModel(bean));
		}
		
		return lista;
	}
	public boolean isColaborador(Integer cdpessoa) {
		return colaboradorDAO.isColaborador(cdpessoa);
	}
	
	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 * @param cargo 
	 * @param dataReferencia 
	 * @since 26/04/2016
	 */
	public List<ColaboradorWSBean> findForWSSOAP(Timestamp dataReferencia, Integer cargo) {
		List<ColaboradorWSBean> lista = new ArrayList<ColaboradorWSBean>();
		for(Colaborador c: colaboradorDAO.findForWSSOAP(dataReferencia, cargo))
			lista.add(new ColaboradorWSBean(c));
		return lista;
	}
	
	public Colaborador findByTipoAtividade(Atividadetipo tipo){
		return this.colaboradorDAO.findByTipoAtividade(tipo);
	}
	
	public List<Colaborador> findForImportacaoFolhaPagamento(List<Cpf> listaCpf){
		if (listaCpf==null || listaCpf.isEmpty()){
			return new ArrayList<Colaborador>();
		}
		return colaboradorDAO.findForImportacaoFolhaPagamento(listaCpf);
	}
	
	public List<Colaborador> findColaboradorAutocompleteByEmpresaCargo(String query){
		Empresa empresa = null;
		Cargo cargo = null;
		try{
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		}catch(Exception e1){}
		try{
			cargo = new Cargo(Integer.parseInt(NeoWeb.getRequestContext().getParameter("cargo")));
		}catch(Exception e2){}
		return colaboradorDAO.findColaboradorAutocompleteByEmpresaCargo(query, empresa, cargo);
	}
	
	public List<Colaborador> findColaboradorAutoCompleteByMaterialEmpresa(String query){
		Empresa empresa = null;
		Material material = null;
		try{
			empresa = new Empresa(Integer.parseInt(NeoWeb.getRequestContext().getParameter("empresa")));
		}catch(Exception e1){}
		try{
			material = new Material(Integer.parseInt(NeoWeb.getRequestContext().getParameter("material")));
		}catch(Exception e2){}
		return colaboradorDAO.findColaboradorAutoCompleteByMaterialEmpresa(query, empresa, material);
	}
	public Colaborador getUniqueColaborador(Colaborador colaborador, Material material, Empresa empresa) {
		return colaboradorDAO.getUniqueColaborador(colaborador, material, empresa);
	}
	
	public List<GenericBean> findAutocompleteWSVenda(String q) {
		return ObjectUtils.translateEntityListToGenericBeanList(colaboradorDAO.findAutocomplete(q));
	}
	
	public void atualizaColaboradorFromImportacao(final ImportarcolaboradoresFPWBean bean, final Colaborador colaborador, final List<String> listaErros) {
		
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				 try {
					 if(bean.getNome() != null && !bean.getNome().equals("") && (colaborador.getNome() == null || !colaborador.getNome().equals(bean.getNome()))){
							colaboradorService.updateCampoColaborador(true, "NOME", bean.getNome(), colaborador);
						}
						
						if(bean.getSexoObj() != null && (colaborador.getSexo() == null || !colaborador.getSexo().equals(bean.getSexoObj()))){
							colaboradorService.updateCampoColaborador(false, "CDSEXO", bean.getSexoObj().getCdsexo(), colaborador);
						}
						
						if(bean.getDtnascimento() != null && (colaborador.getDtnascimento() == null || !colaborador.getDtnascimento().equals(bean.getDtnascimento()))){
							colaboradorService.updateCampoColaborador(true, "DTNASCIMENTO", bean.getDtnascimento(), colaborador);
						}
						
						if(bean.getEtniaObj() != null && (colaborador.getEtnia() == null || !colaborador.getEtnia().equals(bean.getEtniaObj()))){
							colaboradorService.updateCampoColaborador(false, "CDETNIA", bean.getEtniaObj().getCdetnia(), colaborador);
						}
						
						if(bean.getEstadocivil() != null && (colaborador.getEstadocivil() == null || !colaborador.getEstadocivil().equals(bean.getEstadocivil()))){
							colaboradorService.updateCampoColaborador(false, "CDESTADOCIVIL", bean.getEstadocivil().getCdestadocivil(), colaborador);
						}
						
						if(bean.getGrauinstrucao() != null && (colaborador.getGrauinstrucao() == null || !colaborador.getGrauinstrucao().equals(bean.getGrauinstrucao()))){
							colaboradorService.updateCampoColaborador(false, "CDGRAUINSTRUCAO", bean.getGrauinstrucao().getCdgrauinstrucao(), colaborador);
						}
						
						if(bean.getColaboradortipoObj() != null && (colaborador.getColaboradortipo() == null || !colaborador.getColaboradortipo().equals(bean.getColaboradortipoObj()))){
							colaboradorService.updateCampoColaborador(false, "CDCOLABORADORTIPO", bean.getColaboradortipoObj().getCdcolaboradortipo(), colaborador);
						}
						
						if(bean.getNacionalidadeObj() != null && (colaborador.getNacionalidade() == null || !colaborador.getNacionalidade().equals(bean.getNacionalidadeObj()))){
							colaboradorService.updateCampoColaborador(true, "NACIONALIDADE", bean.getNacionalidadeObj().ordinal(), colaborador);	
						}
						
						if(bean.getNome_mae() != null && !bean.getNome_mae().equals("") && (colaborador.getMae() == null || !colaborador.getMae().equals(bean.getNome_mae()))){
							colaboradorService.updateCampoColaborador(false, "MAE", bean.getNome_mae(), colaborador);
						}
						
						if(bean.getNome_pai() != null && !bean.getNome_pai().equals("") && (colaborador.getPai() == null || !colaborador.getPai().equals(bean.getNome_pai()))){
							colaboradorService.updateCampoColaborador(false, "PAI", bean.getNome_pai(), colaborador);
						}
						
						if(bean.getRg_numero() != null && !bean.getRg_numero().equals("") && (colaborador.getRg() == null || !colaborador.getRg().equals(bean.getRg_numero()))){
							colaboradorService.updateCampoColaborador(false, "RG", bean.getRg_numero(), colaborador);
						}
						
						if(bean.getRgdtemissao() != null && (colaborador.getDtemissaorg() == null || !colaborador.getDtemissaorg().equals(bean.getRgdtemissao()))){
							colaboradorService.updateCampoColaborador(false, "DTEMISSAORG", bean.getRgdtemissao(), colaborador);	
						}
						
						if((bean.getRg_orgao_emissor() != null && !bean.getRg_numero().equals("")) || (bean.getRguf() != null)){
							String orgao = bean.getRg_orgao_emissor() != null ? bean.getRg_orgao_emissor() : "";
							orgao += " ";
							orgao += bean.getRguf() != null ?  bean.getRguf().getSigla() : ""; 
							if(colaborador.getOrgaoemissorrg() == null || !colaborador.getOrgaoemissorrg().equals(orgao)){
								colaboradorService.updateCampoColaborador(false, "ORGAOEMISSORRG", orgao, colaborador);
							}
						}

						if(bean.getTitulo_eleitor_numero() != null && !bean.getTitulo_eleitor_numero().equals("") && (colaborador.getTituloeleitoral() == null || !colaborador.getTituloeleitoral().equals(bean.getTitulo_eleitor_numero()))){
							colaboradorService.updateCampoColaborador(false, "TITULOELEITORAL", bean.getTitulo_eleitor_numero(), colaborador);
						}
						
						if(bean.getTitulo_eleitor_secao() != null && !bean.getTitulo_eleitor_secao().equals("") && (colaborador.getSecaoeleitoral() == null || !colaborador.getSecaoeleitoral().equals(bean.getTitulo_eleitor_secao()))){
							colaboradorService.updateCampoColaborador(false, "SECAOELEITORAL", bean.getTitulo_eleitor_secao(), colaborador);
						}
						
						if(bean.getTitulo_eleitor_zona() != null && !bean.getTitulo_eleitor_zona().equals("") && (colaborador.getZonaeleitoral() == null || !colaborador.getZonaeleitoral().equals(bean.getTitulo_eleitor_zona()))){
							colaboradorService.updateCampoColaborador(false, "ZONAELEITORAL", bean.getTitulo_eleitor_zona(), colaborador);
						}
						
						if(bean.getPis_pasep() != null && !bean.getPis_pasep().equals("") && (colaborador.getNumeroinscricao() == null || !colaborador.getNumeroinscricao().equals(bean.getPis_pasep()))){
							colaboradorService.updateCampoColaborador(false, "CDTIPOINSCRICAO", Tipoinscricao.PIS, colaborador);
							colaboradorService.updateCampoColaborador(false, "NUMEROINSCRICAO", bean.getPis_pasep(), colaborador);
						}
						
						if(bean.getCpfObj() != null && (colaborador.getCpf() == null || !colaborador.getCpf().equals(bean.getCpfObj()))){
							colaboradorService.updateCampoColaborador(true, "TIPOPESSOA", Tipopessoa.PESSOA_FISICA.ordinal(), colaborador);
							colaboradorService.updateCampoColaborador(true, "CPF", bean.getCpfObj().getValue(), colaborador);
						}
						
						if(bean.getDtadmissao() != null && (colaborador.getDtadmissao() == null || !colaborador.getDtadmissao().equals(bean.getDtadmissao()))){
							colaboradorService.updateCampoColaborador(false, "DTADMISSAO", bean.getDtadmissao(), colaborador);	
						}
						
						if(bean.getCtpsdtemissao() != null && (colaborador.getDtEmissaoCtps() == null || !colaborador.getDtEmissaoCtps().equals(bean.getCtpsdtemissao()))){
							colaboradorService.updateCampoColaborador(false, "DTEMISSAOCTPS", bean.getCtpsdtemissao(), colaborador);	
						}
						
						if(bean.getCtps_numero() != null && (colaborador.getCtps() == null || !colaborador.getCtps().equals(bean.getCtps_numero()))){
							colaboradorService.updateCampoColaborador(false, "CTPS", bean.getCtps_numero(), colaborador);	
						}
						
						if(bean.getCtps_serie() != null && (colaborador.getSeriectps() == null || !colaborador.getSeriectps().equals(Integer.parseInt(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getCtps_serie()))))){
							colaboradorService.updateCampoColaborador(false, "SERIECTPS", Integer.parseInt(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getCtps_serie())), colaborador);
						}
						
						if(bean.getCtpsuf() != null && (colaborador.getUfctps() == null || !colaborador.getUfctps().equals(bean.getCtpsuf()))){
							colaboradorService.updateCampoColaborador(false, "CDUFCTPS", bean.getCtpsuf().getCduf(), colaborador);
						}
						
						if(bean.getBanco() != null){
							if(colaborador.getListaDadobancario() != null && colaborador.getListaDadobancario().size() > 0){
								dadobancarioService.deletaColaborador(colaborador);
							}
							
							Dadobancario dadobancario = new Dadobancario();
							dadobancario.setTipoconta(new Tipoconta(Tipoconta.CORRENTE));
							dadobancario.setAgencia(bean.getConta_agencia());
							dadobancario.setBanco(bean.getBanco());
							if(bean.getConta_numero() != null && bean.getConta_numero().length() > 2){
								dadobancario.setConta(bean.getConta_numero().substring(0, bean.getConta_numero().length() - 1));
								dadobancario.setDvconta(bean.getConta_numero().substring(bean.getConta_numero().length() - 1));
							}
							dadobancario.setPessoa(colaborador);
							dadobancarioService.saveOrUpdate(dadobancario);
						}
						
						if(bean.getLogradouro() != null && !bean.getLogradouro().equals("") &&
							bean.getBairro() != null && !bean.getBairro().equals("") &&
							bean.getCepObj() != null && bean.getMunicipio() != null &&
							bean.getNumero_endereco() != null && !bean.getNumero_endereco().equals("") ) {
							
							Integer cdendereco = null;
							if(colaborador.getEndereco() != null){
								Endereco end = colaborador.getEndereco();
								cdendereco = end.getCdendereco();
//								boolean logradouro = bean.getLogradouro() != null && !bean.getLogradouro().equals("")  ? bean.getLogradouro().equals(end.getLogradouro()) : end.getLogradouro() == null || end.getLogradouro().equals(""); 
//								boolean bairro = bean.getBairro() != null && !bean.getBairro().equals("") ? bean.getBairro().equals(end.getBairro()) : end.getBairro() == null || end.getBairro().equals(""); 
//								boolean cep = bean.getCepObj() != null ? bean.getCepObj().equals(end.getCep()) : end.getCep() == null; 
//								boolean municipio = bean.getMunicipio() != null ? bean.getMunicipio().equals(end.getMunicipio()) : end.getMunicipio() == null; 
//									
//								if(logradouro && bairro && cep && municipio) {
//									cdendereco = end.getCdendereco();
//								}
							}
							
							Endereco endereco = new Endereco();
							endereco.setCdendereco(cdendereco);			
							if(bean.getLogradouro() != null) endereco.setLogradouro(new String(bean.getLogradouro().getBytes(), "LATIN1"));
							if(bean.getNumero_endereco() != null) endereco.setNumero(new String(bean.getNumero_endereco().getBytes(), "LATIN1"));
							if(bean.getBairro() != null) endereco.setBairro(new String(bean.getBairro().getBytes(), "LATIN1"));
							endereco.setCep(bean.getCepObj());
							endereco.setMunicipio(bean.getMunicipio());
//							if(cdendereco == null) endereco.setEnderecotipo(Enderecotipo.UNICO);
//							else endereco.setEnderecotipo(Enderecotipo.CORRESPONDENCIA);
							endereco.setEnderecotipo(Enderecotipo.UNICO);
							endereco.setPessoa(colaborador);
							
							if(endereco.getCdendereco() == null){
								enderecoService.saveOrUpdate(endereco);
								colaborador.setEndereco(endereco);
								colaboradorService.saveOrUpdate(colaborador);
								
							}else{
								enderecoService.saveOrUpdate(endereco);
							}
						}
						
						if((bean.getTelefone() != null && !bean.getTelefone().equals("")) ||
								bean.getCelular() != null && !bean.getCelular().equals("")){
							
							boolean achouCel = false, achouTel = false;
							Set<Telefone> listaTelefone = colaborador.getListaTelefone();
							for (Telefone tel : listaTelefone) {
								if(tel.getTelefonetipo() != null && tel.getTelefone() != null && !tel.getTelefone().equals("")){
									String telStr = br.com.linkcom.neo.util.StringUtils.soNumero(tel.getTelefone().trim());
									String celStr = br.com.linkcom.neo.util.StringUtils.soNumero(tel.getTelefone().trim());
									if(bean.getTelefone() != null && bean.getTelefone().equals(telStr)){
										achouTel = true;						
									}
									if(bean.getCelular() != null && bean.getCelular().equals(celStr)){
										achouCel = true;
									}
								}
							}
							
							if(!achouTel && bean.getTelefone() != null && !bean.getTelefone().equals("")){
								Telefone telefone = new Telefone();
								telefone.setPessoa(colaborador);
								telefone.setTelefone(bean.getTelefone());
								telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
								
								telefoneService.saveOrUpdate(telefone);
							}
							
							if(!achouCel && bean.getCelular() != null && !bean.getCelular().equals("")){
								Telefone celular = new Telefone();
								celular.setPessoa(colaborador);
								celular.setTelefone(bean.getCelular());
								celular.setTelefonetipo(new Telefonetipo(Telefonetipo.CELULAR));
								
								telefoneService.saveOrUpdate(celular);
							}
						}

						boolean boolMatricula = bean.getMatricula() != null && !bean.getMatricula().equals("");
						boolean boolDepartamento = bean.getDepartamento() != null;
						boolean boolCargo = bean.getCargo() != null;
						
						if(boolMatricula && boolDepartamento && boolCargo){
							Date dtinicio = bean.getDtinicio_cargo();
							boolean criarNovo = false;
							Colaboradorcargo colaboradorcargo = colaboradorcargoService.findCargoAtual(colaborador);
							
							if(colaboradorcargo != null){
								Long matricula = Long.parseLong(bean.getMatricula());
								Departamento departamento = bean.getDepartamento();
								Cargo cargo = bean.getCargo();
								Projeto projeto = bean.getProjeto();
								
								if(!matricula.equals(colaboradorcargo.getMatricula())) criarNovo = true;
								if(!departamento.equals(colaboradorcargo.getDepartamento())) criarNovo = true;
								if(!cargo.equals(colaboradorcargo.getCargo())) criarNovo = true;
								if((bean.getProjeto() != null && colaboradorcargo.getProjeto() == null) ||(bean.getProjeto() == null && colaboradorcargo.getProjeto() != null) || (!projeto.equals(colaboradorcargo.getProjeto()))) criarNovo = true;
								
								if(criarNovo && SinedDateUtils.equalsIgnoreHour(colaboradorcargo.getDtinicio(), bean.getDtinicio_cargo())){
									listaErros.add("J� existe cargo para o colaborador " +bean.getNome() + " com a mesma data de in�cio.");
									throw new SinedException();
								}else if(criarNovo){
									Date dtfim = SinedDateUtils.addDiasData(dtinicio, -1);
									colaboradorcargoService.updateDtfim(colaboradorcargo, dtfim);
								} else {
									if(bean.getCaged() != null){
										colaboradorcargo.setCaged(bean.getCaged());
									}
									if(bean.getSindicato() != null){
										colaboradorcargo.setSindicato(bean.getSindicato());
									}
									colaboradorcargo.setMatricula(Long.parseLong(bean.getMatricula()));
									colaboradorcargo.setCargo(bean.getCargo());
									colaboradorcargo.setDepartamento(bean.getDepartamento());
									colaboradorcargo.setProjeto(bean.getProjeto());										
									colaboradorcargo.setEmpresa(bean.getEmpresa());
									colaboradorcargo.setColaboradorsituacao(bean.getColaboradorsituacao());
									colaboradorcargo.setRegimecontratacao(bean.getRegimecontratacaoObj());
									
									colaboradorcargoService.saveOrUpdate(colaboradorcargo);
								}
							} else criarNovo = true;
							
							if(criarNovo){
								Colaboradorcargo novo = new Colaboradorcargo();
								novo.setColaborador(colaborador);
								novo.setMatricula(Long.parseLong(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getMatricula())));
								novo.setCaged(bean.getCaged());
								novo.setDepartamento(bean.getDepartamento());
								novo.setCargo(bean.getCargo());
								novo.setSindicato(bean.getSindicato());
								novo.setProjeto(bean.getProjeto());
								novo.setDtinicio(dtinicio);
								novo.setEmpresa(bean.getEmpresa());
								novo.setColaboradorsituacao(bean.getColaboradorsituacao());
								novo.setRegimecontratacao(bean.getRegimecontratacaoObj());
								
								colaboradorcargoService.saveOrUpdate(novo);
							}
						}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					throw new SinedException(e);
				}
				
				return null;
			}
		});
	}

	public void criarColaboradorFromImportacao(final ImportarcolaboradoresFPWBean bean) {
		getGenericDAO().getTransactionTemplate().execute(new TransactionCallback() {
			public Object doInTransaction(TransactionStatus status) {
				
				Colaborador colaborador = new Colaborador();
				
				Pessoa pessoa = pessoaService.findPessoaByCpf(bean.getCpfObj());
				if(pessoa != null) colaborador.setCdpessoa(pessoa.getCdpessoa());
				
				colaborador.setNome(bean.getNome());
				colaborador.setSexo(bean.getSexoObj());
				colaborador.setDtnascimento(bean.getDtnascimento());
				colaborador.setEstadocivil(bean.getEstadocivil());
				colaborador.setGrauinstrucao(bean.getGrauinstrucao());
				colaborador.setNacionalidade(bean.getNacionalidadeObj());
				colaborador.setMunicipionaturalidade(bean.getMunicipionaturalidade());
				colaborador.setMae(bean.getNome_mae());
				colaborador.setPai(bean.getNome_pai());
				colaborador.setRg(bean.getRg_numero());
				colaborador.setDtadmissao(bean.getDtadmissao());
				colaborador.setDtemissaorg(bean.getRgdtemissao());
				colaborador.setOrgaoemissorrg(bean.getRg_orgao_emissor() + " " + bean.getRguf().getSigla());
				colaborador.setTituloeleitoral(bean.getTitulo_eleitor_numero());
				colaborador.setSecaoeleitoral(bean.getTitulo_eleitor_secao());
				colaborador.setZonaeleitoral(bean.getTitulo_eleitor_zona());
				colaborador.setTipoinscricao(new Tipoinscricao(Tipoinscricao.PIS));
				colaborador.setNumeroinscricao(bean.getPis_pasep());
				colaborador.setCpf(bean.getCpfObj());
				colaborador.setCtps(bean.getCtps_numero());
				if(bean.getCtps_serie() != null) colaborador.setSeriectps(Integer.parseInt(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getCtps_serie())));
				colaborador.setDtEmissaoCtps(bean.getCtpsdtemissao());
				colaborador.setUfctps(bean.getCtpsuf());
				
				// PADR�ES
				colaborador.setColaboradorformapagamento(bean.getColaboradorformapagamento());
				colaborador.setTipocarteiratrabalho(new Tipocarteiratrabalho(Tipocarteiratrabalho.DEFINITIVO));
				colaborador.setEtnia(bean.getEtniaObj());
				colaborador.setColaboradortipo(bean.getColaboradortipoObj());
				colaborador.setRecontratar(Boolean.TRUE);
				colaborador.setColaboradorsituacao(bean.getColaboradorsituacao());
				
				Endereco endereco = new Endereco();
				endereco.setLogradouro(bean.getLogradouro());
				endereco.setNumero(bean.getNumero_endereco());
				endereco.setBairro(bean.getBairro());
				endereco.setUf(bean.getUfObj());
				endereco.setCep(bean.getCepObj());
				endereco.setMunicipio(bean.getMunicipio());
				endereco.setEnderecotipo(Enderecotipo.UNICO);
				colaborador.setEndereco(endereco);
				
				colaboradorService.saveOrUpdate(colaborador);
				
				Dadobancario dadobancario = new Dadobancario();
				dadobancario.setBanco(bean.getBanco());
				dadobancario.setTipoconta(new Tipoconta(Tipoconta.CORRENTE));
				dadobancario.setAgencia(bean.getConta_agencia());
				
				if(bean.getConta_numero() != null && bean.getConta_numero().length() > 2){
					dadobancario.setConta(bean.getConta_numero().substring(0, bean.getConta_numero().length() - 1));
					dadobancario.setDvconta(bean.getConta_numero().substring(bean.getConta_numero().length() - 1));
				}
				if(bean.getConta_operacao() != null && bean.getConta_operacao().length() <= 5){
					dadobancario.setOperacao(bean.getConta_operacao());
				}
				dadobancario.setPessoa(colaborador);
				dadobancarioService.saveOrUpdate(dadobancario);
				
				if(bean.getTelefone() != null && !bean.getTelefone().equals("")){
					try{
						Telefone telefone = new Telefone();
						telefone.setTelefonetipo(new Telefonetipo(Telefonetipo.PRINCIPAL));
						telefone.setTelefone(bean.getTelefone());
						telefone.setPessoa(colaborador);
						
						telefoneService.saveOrUpdate(telefone);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if(bean.getCelular() != null && !bean.getCelular().equals("")){
					try{
						Telefone celular = new Telefone();
						celular.setTelefonetipo(new Telefonetipo(Telefonetipo.CELULAR));
						celular.setTelefone(bean.getCelular());
						celular.setPessoa(colaborador);
						
						telefoneService.saveOrUpdate(celular);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				if(bean.getCargo() != null && bean.getDepartamento() != null){
					try{
						Colaboradorcargo colaboradorcargo = new Colaboradorcargo();
						colaboradorcargo.setEmpresa(bean.getEmpresa());
						colaboradorcargo.setColaborador(colaborador);
						colaboradorcargo.setCaged(bean.getCaged());
						colaboradorcargo.setCargo(bean.getCargo());
						colaboradorcargo.setDepartamento(bean.getDepartamento());
						colaboradorcargo.setDtinicio(bean.getDtinicio_cargo());
						colaboradorcargo.setProjeto(bean.getProjeto());
						colaboradorcargo.setColaboradorsituacao(bean.getColaboradorsituacao());
						colaboradorcargo.setSindicato(bean.getSindicato());
						colaboradorcargo.setRegimecontratacao(bean.getRegimecontratacaoObj());
						try{colaboradorcargo.setMatricula(Long.parseLong(br.com.linkcom.neo.util.StringUtils.soNumero(bean.getMatricula())));} catch (Exception e) {e.printStackTrace();}
						
						colaboradorcargoService.saveOrUpdate(colaboradorcargo);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				return null;
			}
		});
	}

}
