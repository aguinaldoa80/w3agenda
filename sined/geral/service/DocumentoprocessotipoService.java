package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Documentoprocessotipo;
import br.com.linkcom.sined.geral.dao.DocumentoprocessotipoDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class DocumentoprocessotipoService extends GenericService<Documentoprocessotipo> {
	
	private DocumentoprocessotipoDAO documentoprocessotipoDAO;

	public void setDocumentoprocessotipoDAO(
			DocumentoprocessotipoDAO documentoprocessotipoDAO) {
		this.documentoprocessotipoDAO = documentoprocessotipoDAO;
	}
	
	/**
	 * Retorna os documentos ativos na ordena��o passada
	 * @param orderBy
	 * @return
	 * @author C�ntia Nogueira
	 * @see br.com.linkcom.sined.geral.dao.DocumentoprocessotipoDAO#findAtivos(String)
	 */
	public List<Documentoprocessotipo> findAtivos(String orderBy){
	  return documentoprocessotipoDAO.findAtivos(orderBy);
	}

}
