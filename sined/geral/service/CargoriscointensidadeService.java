package br.com.linkcom.sined.geral.service;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Cargoriscointensidade;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class CargoriscointensidadeService extends GenericService<Cargoriscointensidade> {	
	
	/* singleton */
	private static CargoriscointensidadeService instance;
	public static CargoriscointensidadeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(CargoriscointensidadeService.class);
		}
		return instance;
	}
	
}
