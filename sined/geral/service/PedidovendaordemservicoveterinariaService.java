package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Ordemservicoveterinaria;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Pedidovendaordemservicoveterinaria;
import br.com.linkcom.sined.geral.dao.PedidovendaordemservicoveterinariaDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class PedidovendaordemservicoveterinariaService extends GenericService<Pedidovendaordemservicoveterinaria> {
	
	private PedidovendaordemservicoveterinariaDAO pedidovendaordemservicoveterinariaDAO;
	
	public void setPedidovendaordemservicoveterinariaDAO(PedidovendaordemservicoveterinariaDAO pedidovendaordemservicoveterinariaDAO) {
		this.pedidovendaordemservicoveterinariaDAO = pedidovendaordemservicoveterinariaDAO;
	}

	public void savePedidovendaordemservicoveterinaria(String whereInOSV, Pedidovenda venda){
		for(String id : whereInOSV.split(",")){
			Ordemservicoveterinaria ordemservicoveterinaria = new Ordemservicoveterinaria(Integer.parseInt(id));
			Pedidovendaordemservicoveterinaria vendaordemservicoveterinaria = new Pedidovendaordemservicoveterinaria();
			vendaordemservicoveterinaria.setOrdemservicoveterinaria(ordemservicoveterinaria);
			vendaordemservicoveterinaria.setPedidovenda(venda);
			saveOrUpdate(vendaordemservicoveterinaria);
		}
	}

	public List<Pedidovendaordemservicoveterinaria> findForReservarAposCancelamentoPedido(String whereInPedidovenda) {
		return pedidovendaordemservicoveterinariaDAO.findForReservarAposCancelamentoPedido(whereInPedidovenda);
	}
}
