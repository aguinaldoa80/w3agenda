package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.Colaboradorformapagamento;
import br.com.linkcom.sined.geral.bean.Tipoconta;
import br.com.linkcom.sined.geral.dao.TipocontaDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class TipocontaService extends GenericService<Tipoconta> {	
	
	private TipocontaDAO tipocontaDAO;
	public void setTipocontaDAO(TipocontaDAO tipocontaDAO) {
		this.tipocontaDAO = tipocontaDAO;
	}
	
	/**
	 * M�todo para obter lista de tipos de conta de acordo com a forma de pagamento.
	 * 
	 * @see #findAll()
	 * @see br.com.linkcom.sined.geral.dao.TipocontaDAO#findTipoCorrente()
	 * @see br.com.linkcom.sined.geral.dao.TipocontaDAO#findTipoPoupanca()
	 * @param forma
	 * @return
	 * @author Fl�vio Tavares
	 */
	public List<Tipoconta> findTipoconta(Colaboradorformapagamento forma){
		Integer cdforma = forma.getCdcolaboradorformapagamento();
		if(forma == null || cdforma == null){
			throw new SinedException("Os par�metros forma ou cdcolaboradorformapagamento n�o podem ser null.");
		}
		
		if(cdforma.equals(Colaboradorformapagamento.DEPOSITO_CONTA_CORRENTE)){
			return tipocontaDAO.findTipoCorrente();
		}else if(cdforma.equals(Colaboradorformapagamento.DEPOSITO_CONTA_POUPANCA)){
			return tipocontaDAO.findTipoPoupanca();
		}else{
			return this.findAll();
		}
	}
	
	
	

	/* singleton */
	private static TipocontaService instance;
	public static TipocontaService getInstance() {
		if(instance == null){
			instance = Neo.getObject(TipocontaService.class);
		}
		return instance;
	}
	
}
