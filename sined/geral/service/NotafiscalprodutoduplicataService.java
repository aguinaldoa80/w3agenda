package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoduplicata;
import br.com.linkcom.sined.geral.dao.NotafiscalprodutoduplicataDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class NotafiscalprodutoduplicataService extends GenericService<Notafiscalprodutoduplicata> {

	private NotafiscalprodutoduplicataDAO notafiscalprodutoduplicataDAO;
	
	public void setNotafiscalprodutoduplicataDAO(
			NotafiscalprodutoduplicataDAO notafiscalprodutoduplicataDAO) {
		this.notafiscalprodutoduplicataDAO = notafiscalprodutoduplicataDAO;
	}
	
	public List<Notafiscalprodutoduplicata> findByNotafiscalproduto(Notafiscalproduto form) {
		return notafiscalprodutoduplicataDAO.findByNotafiscalproduto(form);
	}

	public void updateNumeroDuplicata(Notafiscalprodutoduplicata dup, String numero){
		notafiscalprodutoduplicataDAO.updateNumeroDuplicata(dup, numero);
	}
	
	public Boolean existeDuplicata(Notafiscalproduto notafiscalproduto) {
		return notafiscalprodutoduplicataDAO.existeDuplicata(notafiscalproduto);
	}

	public List<Notafiscalprodutoduplicata> findByNotaForIntegracaoTEF(String whereInNota) {
		return notafiscalprodutoduplicataDAO.findByNotaForIntegracaoTEF(whereInNota);
	}

	public void updateInfoCartao(Notafiscalprodutoduplicata notafiscalprodutoduplicata, String autorizacao, String bandeira, String adquirente) {
		notafiscalprodutoduplicataDAO.updateInfoCartao(notafiscalprodutoduplicata, autorizacao, bandeira, adquirente);
	}
}
