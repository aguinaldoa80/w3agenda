package br.com.linkcom.sined.geral.service;

import java.awt.Image;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.linkcom.lkbanco.boleto.BarCode2of5;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.ArquivoMdfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Mdfe;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioTerminalCarregamento;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioTerminalDescarregamento;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioUnidadeCargaVazia;
import br.com.linkcom.sined.geral.bean.MdfeAquaviarioUnidadeTransporteVazia;
import br.com.linkcom.sined.geral.bean.MdfeCte;
import br.com.linkcom.sined.geral.bean.MdfeFerroviarioVagao;
import br.com.linkcom.sined.geral.bean.MdfeHistorico;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaCte;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeCargaNfe;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteCte;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteMdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeInformacaoUnidadeTransporteNfe;
import br.com.linkcom.sined.geral.bean.MdfeNfe;
import br.com.linkcom.sined.geral.bean.MdfeReferenciado;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioCondutor;
import br.com.linkcom.sined.geral.bean.MdfeRodoviarioValePedagio;
import br.com.linkcom.sined.geral.bean.MdfeUfPercurso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.geral.dao.MdfeDAO;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeCondutor;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeDadosTerminal;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeDocFiscTranspCarga;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeUnidadeTranspCarga;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeVagao;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.DamdfeValePedagio;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.persistence.GenericService;
import br.com.linkcom.sined.util.report.MergeReport;
import br.com.linkcom.utils.LkMdfeUtil;

public class MdfeService extends GenericService<Mdfe>{

	private MdfeDAO mdfeDAO;
	private MdfeHistoricoService mdfeHistoricoService;
	private TransactionTemplate transactionTemplate;
	private MdfeLocalCarregamentoService mdfeLocalCarregamentoService;
	private MdfeUfPercursoService mdfeUfPercursoService;
	private MdfeAutorizacaoDownloadService mdfeAutorizacaoDownloadService;
	private MdfeLacreService mdfeLacreService;
	private MdfeLacreUnidadeCargaNfeService mdfeLacreUnidadeCargaNfeService;
	private MdfeLacreUnidadeCargaCteService mdfeLacreUnidadeCargaCteService;
	private MdfeLacreUnidadeCargaMdfeReferenciadoService mdfeLacreUnidadeCargaMdfeReferenciadoService;
	private MdfeLacreUnidadeTransporteNfeService mdfeLacreUnidadeTransporteNfeService;
	private MdfeLacreUnidadeTransporteCteService mdfeLacreUnidadeTransporteCteService;
	private MdfeLacreUnidadeTransporteMdfeReferenciadoService mdfeLacreUnidadeTransporteMdfeReferenciadoService;
	private MdfeInformacaoUnidadeTransporteNfeService mdfeInformacaoUnidadeTransporteNfeService;
	private MdfeInformacaoUnidadeTransporteCteService mdfeInformacaoUnidadeTransporteCteService;
	private MdfeInformacaoUnidadeTransporteMdfeReferenciadoService mdfeInformacaoUnidadeTransporteMdfeReferenciadoService;
	private MdfeInformacaoUnidadeCargaNfeService mdfeInformacaoUnidadeCargaNfeService;
	private MdfeInformacaoUnidadeCargaCteService mdfeInformacaoUnidadeCargaCteService;
	private MdfeInformacaoUnidadeCargaMdfeReferenciadoService mdfeInformacaoUnidadeCargaMdfeReferenciadoService;
	private MdfeReferenciadoService mdfeReferenciadoService;
	private MdfeCteService mdfeCteService;
	private MdfeRodoviarioCiotService mdfeRodoviarioCiotService;
	private MdfeRodoviarioCondutorService mdfeRodoviarioCondutorService;
	private MdfeRodoviarioContratanteService mdfeRodoviarioContratanteService;
	private MdfeRodoviarioReboqueService mdfeRodoviarioReboqueService;
	private MdfeRodoviarioValePedagioService mdfeRodoviarioValePedagioService;
	private MdfeAquaviarioEmbarcacaoComboioService mdfeAquaviarioEmbarcacaoComboioService;
	private MdfeAquaviarioTerminalCarregamentoService mdfeAquaviarioTerminalCarregamentoService;
	private MdfeAquaviarioTerminalDescarregamentoService mdfeAquaviarioTerminalDescarregamentoService;
	private MdfeAquaviarioUnidadeCargaVaziaService mdfeAquaviarioUnidadeCargaVaziaService;
	private MdfeAquaviarioUnidadeTransporteVaziaService mdfeAquaviarioUnidadeTransporteVaziaService;
	private MdfeFerroviarioVagaoService mdfeFerroviarioVagaoService;
	private MdfeNfeService mdfeNfeService;
	private MdfeProdutoPerigosoCteService mdfeProdutoPerigosoCteService;
	private MdfeProdutoPerigosoNfeService mdfeProdutoPerigosoNfeService;
	private MdfeProdutoPerigosoMdfeReferenciadoService mdfeProdutoPerigosoMdfeReferenciadoService;
	private MdfeSeguroCargaService mdfeSeguroCargaService;
	private MdfeSeguroCargaNumeroAverbacaoService mdfeSeguroCargaNumeroAverbacaoService;
	private ArquivoMdfeService arquivoMdfeService;
	private EmpresaService empresaService;
	
	private List<MdfeSituacao> listaSituacoesNaoTransients = Arrays.asList(new MdfeSituacao[]{MdfeSituacao.EMITIDO, MdfeSituacao.SOLICITADO,
							MdfeSituacao.ENVIADO, MdfeSituacao.AUTORIZADO, MdfeSituacao.REJEITADO,
							MdfeSituacao.ENCERRADO, MdfeSituacao.EM_CANCELAMENTO, MdfeSituacao.CANCELADO,
							MdfeSituacao.EM_ENCERRAMENTO});
	
	public static MdfeService instance;
	public static MdfeService getInstance() {
		if(instance == null){
			instance = Neo.getObject(MdfeService.class);
		}
		return instance;
	}
	
	public void setMdfeDAO(MdfeDAO mdfeDAO) {
		this.mdfeDAO = mdfeDAO;
	}
	public void setMdfeHistoricoService(MdfeHistoricoService mdfeHistoricoService) {
		this.mdfeHistoricoService = mdfeHistoricoService;
	}
	public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	public void setMdfeLocalCarregamentoService(
			MdfeLocalCarregamentoService mdfeLocalCarregamentoService) {
		this.mdfeLocalCarregamentoService = mdfeLocalCarregamentoService;
	}

	public void setMdfeUfPercursoService(MdfeUfPercursoService mdfeUfPercursoService) {
		this.mdfeUfPercursoService = mdfeUfPercursoService;
	}

	public void setMdfeAutorizacaoDownloadService(
			MdfeAutorizacaoDownloadService mdfeAutorizacaoDownloadService) {
		this.mdfeAutorizacaoDownloadService = mdfeAutorizacaoDownloadService;
	}

	public void setMdfeLacreService(MdfeLacreService mdfeLacreService) {
		this.mdfeLacreService = mdfeLacreService;
	}

	public void setMdfeLacreUnidadeCargaNfeService(
			MdfeLacreUnidadeCargaNfeService mdfeLacreUnidadeCargaNfeService) {
		this.mdfeLacreUnidadeCargaNfeService = mdfeLacreUnidadeCargaNfeService;
	}

	public void setMdfeLacreUnidadeCargaCteService(
			MdfeLacreUnidadeCargaCteService mdfeLacreUnidadeCargaCteService) {
		this.mdfeLacreUnidadeCargaCteService = mdfeLacreUnidadeCargaCteService;
	}

	public void setMdfeLacreUnidadeCargaMdfeReferenciadoService(
			MdfeLacreUnidadeCargaMdfeReferenciadoService mdfeLacreUnidadeCargaMdfeReferenciadoService) {
		this.mdfeLacreUnidadeCargaMdfeReferenciadoService = mdfeLacreUnidadeCargaMdfeReferenciadoService;
	}

	public void setMdfeLacreUnidadeTransporteNfeService(
			MdfeLacreUnidadeTransporteNfeService mdfeLacreUnidadeTransporteNfeService) {
		this.mdfeLacreUnidadeTransporteNfeService = mdfeLacreUnidadeTransporteNfeService;
	}

	public void setMdfeLacreUnidadeTransporteCteService(
			MdfeLacreUnidadeTransporteCteService mdfeLacreUnidadeTransporteCteService) {
		this.mdfeLacreUnidadeTransporteCteService = mdfeLacreUnidadeTransporteCteService;
	}

	public void setMdfeLacreUnidadeTransporteMdfeReferenciadoService(
			MdfeLacreUnidadeTransporteMdfeReferenciadoService mdfeLacreUnidadeTransporteMdfeReferenciadoService) {
		this.mdfeLacreUnidadeTransporteMdfeReferenciadoService = mdfeLacreUnidadeTransporteMdfeReferenciadoService;
	}

	public void setMdfeInformacaoUnidadeTransporteNfeService(
			MdfeInformacaoUnidadeTransporteNfeService mdfeInformacaoUnidadeTransporteNfeService) {
		this.mdfeInformacaoUnidadeTransporteNfeService = mdfeInformacaoUnidadeTransporteNfeService;
	}

	public void setMdfeInformacaoUnidadeTransporteCteService(
			MdfeInformacaoUnidadeTransporteCteService mdfeInformacaoUnidadeTransporteCteService) {
		this.mdfeInformacaoUnidadeTransporteCteService = mdfeInformacaoUnidadeTransporteCteService;
	}

	public void setMdfeInformacaoUnidadeTransporteMdfeReferenciadoService(
			MdfeInformacaoUnidadeTransporteMdfeReferenciadoService mdfeInformacaoUnidadeTransporteMdfeReferenciadoService) {
		this.mdfeInformacaoUnidadeTransporteMdfeReferenciadoService = mdfeInformacaoUnidadeTransporteMdfeReferenciadoService;
	}

	public void setMdfeInformacaoUnidadeCargaNfeService(
			MdfeInformacaoUnidadeCargaNfeService mdfeInformacaoUnidadeCargaNfeService) {
		this.mdfeInformacaoUnidadeCargaNfeService = mdfeInformacaoUnidadeCargaNfeService;
	}

	public void setMdfeInformacaoUnidadeCargaCteService(
			MdfeInformacaoUnidadeCargaCteService mdfeInformacaoUnidadeCargaCteService) {
		this.mdfeInformacaoUnidadeCargaCteService = mdfeInformacaoUnidadeCargaCteService;
	}

	public void setMdfeInformacaoUnidadeCargaMdfeReferenciadoService(
			MdfeInformacaoUnidadeCargaMdfeReferenciadoService mdfeInformacaoUnidadeCargaMdfeReferenciadoService) {
		this.mdfeInformacaoUnidadeCargaMdfeReferenciadoService = mdfeInformacaoUnidadeCargaMdfeReferenciadoService;
	}

	public void setMdfeReferenciadoService(
			MdfeReferenciadoService mdfeReferenciadoService) {
		this.mdfeReferenciadoService = mdfeReferenciadoService;
	}

	public void setMdfeCteService(MdfeCteService mdfeCteService) {
		this.mdfeCteService = mdfeCteService;
	}

	public void setMdfeRodoviarioCiotService(
			MdfeRodoviarioCiotService mdfeRodoviarioCiotService) {
		this.mdfeRodoviarioCiotService = mdfeRodoviarioCiotService;
	}

	public void setMdfeRodoviarioCondutorService(
			MdfeRodoviarioCondutorService mdfeRodoviarioCondutorService) {
		this.mdfeRodoviarioCondutorService = mdfeRodoviarioCondutorService;
	}

	public void setMdfeRodoviarioContratanteService(
			MdfeRodoviarioContratanteService mdfeRodoviarioContratanteService) {
		this.mdfeRodoviarioContratanteService = mdfeRodoviarioContratanteService;
	}

	public void setMdfeRodoviarioReboqueService(
			MdfeRodoviarioReboqueService mdfeRodoviarioReboqueService) {
		this.mdfeRodoviarioReboqueService = mdfeRodoviarioReboqueService;
	}

	public void setMdfeRodoviarioValePedagioService(
			MdfeRodoviarioValePedagioService mdfeRodoviarioValePedagioService) {
		this.mdfeRodoviarioValePedagioService = mdfeRodoviarioValePedagioService;
	}

	public void setMdfeAquaviarioEmbarcacaoComboioService(
			MdfeAquaviarioEmbarcacaoComboioService mdfeAquaviarioEmbarcacaoComboioService) {
		this.mdfeAquaviarioEmbarcacaoComboioService = mdfeAquaviarioEmbarcacaoComboioService;
	}

	public void setMdfeAquaviarioTerminalCarregamentoService(
			MdfeAquaviarioTerminalCarregamentoService mdfeAquaviarioTerminalCarregamentoService) {
		this.mdfeAquaviarioTerminalCarregamentoService = mdfeAquaviarioTerminalCarregamentoService;
	}

	public void setMdfeAquaviarioTerminalDescarregamentoService(
			MdfeAquaviarioTerminalDescarregamentoService mdfeAquaviarioTerminalDescarregamentoService) {
		this.mdfeAquaviarioTerminalDescarregamentoService = mdfeAquaviarioTerminalDescarregamentoService;
	}

	public void setMdfeAquaviarioUnidadeCargaVaziaService(
			MdfeAquaviarioUnidadeCargaVaziaService mdfeAquaviarioUnidadeCargaVaziaService) {
		this.mdfeAquaviarioUnidadeCargaVaziaService = mdfeAquaviarioUnidadeCargaVaziaService;
	}

	public void setMdfeAquaviarioUnidadeTransporteVaziaService(
			MdfeAquaviarioUnidadeTransporteVaziaService mdfeAquaviarioUnidadeTransporteVaziaService) {
		this.mdfeAquaviarioUnidadeTransporteVaziaService = mdfeAquaviarioUnidadeTransporteVaziaService;
	}

	public void setMdfeFerroviarioVagaoService(
			MdfeFerroviarioVagaoService mdfeFerroviarioVagaoService) {
		this.mdfeFerroviarioVagaoService = mdfeFerroviarioVagaoService;
	}

	public void setMdfeNfeService(MdfeNfeService mdfeNfeService) {
		this.mdfeNfeService = mdfeNfeService;
	}

	public void setMdfeProdutoPerigosoCteService(
			MdfeProdutoPerigosoCteService mdfeProdutoPerigosoCteService) {
		this.mdfeProdutoPerigosoCteService = mdfeProdutoPerigosoCteService;
	}

	public void setMdfeProdutoPerigosoNfeService(
			MdfeProdutoPerigosoNfeService mdfeProdutoPerigosoNfeService) {
		this.mdfeProdutoPerigosoNfeService = mdfeProdutoPerigosoNfeService;
	}

	public void setMdfeProdutoPerigosoMdfeReferenciadoService(
			MdfeProdutoPerigosoMdfeReferenciadoService mdfeProdutoPerigosoMdfeReferenciadoService) {
		this.mdfeProdutoPerigosoMdfeReferenciadoService = mdfeProdutoPerigosoMdfeReferenciadoService;
	}

	public void setMdfeSeguroCargaService(
			MdfeSeguroCargaService mdfeSeguroCargaService) {
		this.mdfeSeguroCargaService = mdfeSeguroCargaService;
	}

	public void setMdfeSeguroCargaNumeroAverbacaoService(
			MdfeSeguroCargaNumeroAverbacaoService mdfeSeguroCargaNumeroAverbacaoService) {
		this.mdfeSeguroCargaNumeroAverbacaoService = mdfeSeguroCargaNumeroAverbacaoService;
	}

	public boolean alterarStatusAcao(Mdfe mdfe, String observacao) {
		return mdfeDAO.alterarStatusAcao(mdfe, observacao);
	}
	
	public void setArquivoMdfeService(ArquivoMdfeService arquivoMdfeService) {
		this.arquivoMdfeService = arquivoMdfeService;
	}

	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void carregarListas(Mdfe mdfe) {
		mdfe.setListaLocalCarregamento(mdfeLocalCarregamentoService.findByMdfe(mdfe));
		mdfe.setListaUfPercurso(mdfeUfPercursoService.findBy(mdfe, "cdMdfeUfPercurso", "ordem", "uf.cduf", "uf.sigla"));
		ordenarListaUfPercurso(mdfe.getListaUfPercurso());
		mdfe.setListaAquaviarioEmbarcacaoComboio(mdfeAquaviarioEmbarcacaoComboioService.findBy(mdfe, false));
		mdfe.setListaAquaviarioTerminalCarregamento(mdfeAquaviarioTerminalCarregamentoService.findBy(mdfe, false));
		mdfe.setListaAquaviarioTerminalDescarregamento(mdfeAquaviarioTerminalDescarregamentoService.findBy(mdfe, false));			
		mdfe.setListaAquaviarioUnidadeCargaVazia(mdfeAquaviarioUnidadeCargaVaziaService.findBy(mdfe, false));
		mdfe.setListaAquaviarioUnidadeTransporteVazia(mdfeAquaviarioUnidadeTransporteVaziaService.findBy(mdfe, false));
		mdfe.setListaAutorizacaoDownload(mdfeAutorizacaoDownloadService.findBy(mdfe, false));
		mdfe.setListaCiot(mdfeRodoviarioCiotService.findBy(mdfe, false));
		mdfe.setListaCondutor(mdfeRodoviarioCondutorService.findBy(mdfe, false));
		mdfe.setListaContratante(mdfeRodoviarioContratanteService.findBy(mdfe, false));
		mdfe.setListaValePedagio(mdfeRodoviarioValePedagioService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeCargaCte(mdfeInformacaoUnidadeCargaCteService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeCargaNfe(mdfeInformacaoUnidadeCargaNfeService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeCargaMdfeReferenciado(mdfeInformacaoUnidadeCargaMdfeReferenciadoService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeTransporteCte(mdfeInformacaoUnidadeTransporteCteService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeTransporteNfe(mdfeInformacaoUnidadeTransporteNfeService.findBy(mdfe, false));
		mdfe.setListaInformacaoUnidadeTransporteMdfeReferenciado(mdfeInformacaoUnidadeTransporteMdfeReferenciadoService.findBy(mdfe, false));
		mdfe.setListaLacre(mdfeLacreService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeCargaCte(mdfeLacreUnidadeCargaCteService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeCargaNfe(mdfeLacreUnidadeCargaNfeService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeCargaMdfeReferenciado(mdfeLacreUnidadeCargaMdfeReferenciadoService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeTransporteCte(mdfeLacreUnidadeTransporteCteService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeTransporteNfe(mdfeLacreUnidadeTransporteNfeService.findBy(mdfe, false));
		mdfe.setListaLacreUnidadeTransporteMdfeReferenciado(mdfeLacreUnidadeTransporteMdfeReferenciadoService.findBy(mdfe, false));
		mdfe.setListaMdfeCte(mdfeCteService.findByMdfe(mdfe));
		mdfe.setListaMdfeReferenciado(mdfeReferenciadoService.findByMdfe(mdfe));
		mdfe.setListaNfe(mdfeNfeService.findByMdfe(mdfe));
		mdfe.setListaProdutoPerigosoCte(mdfeProdutoPerigosoCteService.findBy(mdfe, false));
		mdfe.setListaProdutoPerigosoNfe(mdfeProdutoPerigosoNfeService.findBy(mdfe, false));
		mdfe.setListaProdutoPerigosoMdfeReferenciado(mdfeProdutoPerigosoMdfeReferenciadoService.findBy(mdfe, false));
		mdfe.setListaReboque(mdfeRodoviarioReboqueService.findBy(mdfe, false));
		mdfe.setListaSeguroCarga(mdfeSeguroCargaService.findBy(mdfe, false));
		mdfe.setListaVagao(mdfeFerroviarioVagaoService.findBy(mdfe, false));
		mdfe.setListaSeguroCargaNumeroAverbacao(mdfeSeguroCargaNumeroAverbacaoService.findBy(mdfe, false));
		mdfe.setListaHistoricoTrans(mdfeHistoricoService.findByMdfe(mdfe));
	}
	
	private void ordenarListaUfPercurso(List<MdfeUfPercurso> lista) {
		Collections.sort(lista, new Comparator<MdfeUfPercurso>() {
			public int compare(MdfeUfPercurso bean1, MdfeUfPercurso bean2) {
				return bean2.getOrdem() != null && bean1.getOrdem() != null ? bean1.getOrdem().compareTo(bean2.getOrdem()) : -1;
			}
		});
	}

	public boolean alterarStatusAcaoSemTransacao(Mdfe mdfe, String observacao) {
		if (mdfe == null || mdfe.getCdmdfe() == null) {
			throw new SinedException("A MDF-e n�o pode ser nula.");
		}
		if (mdfe.getMdfeSituacao() == null) {
			throw new SinedException("A situa��o da MDF-e n�o pode ser nula.");
		}
		if(mdfe.getMdfeSituacaoHistoricoTrans()==null){
			mdfe.setMdfeSituacaoHistoricoTrans(mdfe.getMdfeSituacao());
		}
		
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		MdfeHistorico mdfeHistorico = new MdfeHistorico();
		mdfeHistorico.setMdfe(mdfe);
		mdfeHistorico.setMdfeSituacao(mdfe.getMdfeSituacaoHistoricoTrans());
		mdfeHistorico.setCdusuarioaltera(usuarioLogado != null ? usuarioLogado.getCdpessoa() : null);
		mdfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		mdfeHistorico.setObservacao(observacao);
		
		atualizaMdfeSituacao(mdfe, mdfe.getMdfeSituacao());
		
		mdfeHistoricoService.saveOrUpdateNoUseTransaction(mdfeHistorico);
		return true;
	}
	
	public void atualizaMdfeSituacao(Mdfe mdfe, MdfeSituacao mdfeSituacao) {
		mdfeDAO.atualizaMdfeSituacao(mdfe, mdfeSituacao);
	}
	
	public void cancelarInternamente(final String whereIn, final String observacao){
		transactionTemplate.execute(new TransactionCallback() {
			
			@Override
			public Object doInTransaction(TransactionStatus status) {
				for(String cdMdfe: whereIn.split(",")){
					Mdfe mdfe = new Mdfe(Integer.parseInt(cdMdfe));
					mdfe.setMdfeSituacao(MdfeSituacao.CANCELADO);
					MdfeService.getInstance().alterarStatusAcaoSemTransacao(mdfe, observacao);
				}
				return null;
			}
		});
	}
	
	public void estornar(final String whereIn, final String observacao){
		transactionTemplate.execute(new TransactionCallback() {
			
			@Override
			public Object doInTransaction(TransactionStatus status) {
				for(String cdMdfe: whereIn.split(",")){
					Mdfe mdfe = new Mdfe(Integer.parseInt(cdMdfe));
					mdfe.setMdfeSituacao(MdfeSituacao.EMITIDO);
					mdfe.setMdfeSituacaoHistoricoTrans(MdfeSituacao.ESTORNADA);
					MdfeService.getInstance().alterarStatusAcaoSemTransacao(mdfe, observacao);
					ArquivoMdfeService.getInstance().naoEnviarArquivo_MdfeEstornada(mdfe);
				}
				return null;
			}
		});
	}
	
	public List<Mdfe> findByNotInSituacoes(String whereIn, MdfeSituacao... situacoes){
		return mdfeDAO.findByNotInSituacoes(whereIn, situacoes);
	}

	public boolean haveMDFeDiferenteStatus(String whereIn, MdfeSituacao... status) {
		return mdfeDAO.haveMDFeDiferenteStatus(whereIn, status);
	}
	
	public List<MdfeSituacao> getListaSituacoesNaoTransients() {
		return listaSituacoesNaoTransients;
	}

	public Resource gerarDamdfe(String itensSelecionados) throws Exception {
		MergeReport mergeReport = new MergeReport("damdfe_" + SinedUtil.datePatternForReport() + ".pdf");
		List<Mdfe> listaMdfe = this.findForDamdfe(itensSelecionados);
		
		for (Mdfe mdfe : listaMdfe) {
			this.carregarListas(mdfe);
			mergeReport.addReport(this.danfe(mdfe));
		}
		
		return mergeReport.generateResource();
	}

	private Report danfe(Mdfe mdfe) {
		List<ArquivoMdfe> arquivoMdfeList = new ArrayList<ArquivoMdfe>();
		
		if (mdfe.getCdmdfe() != null) {
			arquivoMdfeList = arquivoMdfeService.findByMdfeAutorizado(mdfe.getCdmdfe().toString());
		}
		
		DamdfeBean damdfeBean = this.converterMdfeEmDamdfeBean(mdfe, (arquivoMdfeList.size() > 0 ? arquivoMdfeList.get(0) : null));
		
		return damdfeBean.makeReport(mdfe.getModalidadeFrete(), mdfe.getFormaEmissao());
	}

	private DamdfeBean converterMdfeEmDamdfeBean(Mdfe mdfe, ArquivoMdfe arquivoMdfe) {
		DamdfeBean damdfeBean = new DamdfeBean();
		
		if (mdfe.getEmpresa() != null) {
			damdfeBean.setLogo(SinedUtil.getLogo(mdfe.getEmpresa()));
			damdfeBean.setNomeEmpresa(mdfe.getEmpresa().getNome() != null ? mdfe.getEmpresa().getNome() : "");
			damdfeBean.setCnpjEmpresa(mdfe.getEmpresa().getCnpj() != null ? mdfe.getEmpresa().getCnpj().getValue() : "");
			damdfeBean.setIeEmpresa(mdfe.getEmpresa().getInscricaoestadual() != null ? mdfe.getEmpresa().getInscricaoestadual() : "");
			
			if (mdfe.getEmpresa().getListaEndereco() != null && mdfe.getEmpresa().getListaEndereco().size() > 0) {
				Endereco enderecoMDFE = null;
				if(SinedUtil.isListNotEmpty(mdfe.getEmpresa().getListaEndereco())){
					for(Endereco endereco : mdfe.getEmpresa().getListaEndereco()){
						if(!Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
							enderecoMDFE = endereco;
							break;
						}else if(enderecoMDFE == null){
							enderecoMDFE = endereco;
						}
					}
				}
				if (enderecoMDFE != null) {
					Endereco endereco = enderecoMDFE;
					
					damdfeBean.setLogradouroEmpresa(endereco.getLogradouro() != null ? endereco.getLogradouro() : "");
					damdfeBean.setNumeroEmpresa(endereco.getNumero() != null ? endereco.getNumero() : "");
					damdfeBean.setComplementoEmpresa(endereco.getComplemento() != null ? endereco.getComplemento() : "");
					damdfeBean.setBairroEmpresa(endereco.getBairro() != null ? endereco.getBairro() : "");
					damdfeBean.setUfEmpresa(endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && endereco.getMunicipio().getUf().getSigla() != null ? 
							endereco.getMunicipio().getUf().getSigla() : "");
					damdfeBean.setMunicipioEmpresa(endereco.getMunicipio() != null && endereco.getMunicipio().getNome() != null ? endereco.getMunicipio().getNome() : "");
					damdfeBean.setCepEmpresa(endereco.getCep() != null && endereco.getCep().getValue() != null ? endereco.getCep().getValue() : "");
				}
			}
		}
		
		if (arquivoMdfe != null && arquivoMdfe.getChaveacesso() != null) {
			BarCode2of5 bc = new BarCode2of5();
			bc.setSize(351, 32);
			Image img = bc.createImage(arquivoMdfe.getChaveacesso());
			damdfeBean.setCodigoBarra(img);
		}
		
		damdfeBean.setChaveAcesso(arquivoMdfe != null && arquivoMdfe.getChaveacesso() != null ? arquivoMdfe.getChaveacesso() : "");
		damdfeBean.setProtocoloAutorizacao((arquivoMdfe != null && arquivoMdfe.getProtocolomdfe() != null ? arquivoMdfe.getProtocolomdfe() : "") + "   " + 
				(arquivoMdfe != null && arquivoMdfe.getDtprocessamento() != null ? SinedDateUtils.toString(arquivoMdfe.getDtprocessamento()) : ""));
		
		damdfeBean.setModelo("58");
		damdfeBean.setSerie(mdfe.getSerie() != null ? mdfe.getSerie() : "");
		damdfeBean.setNumero(mdfe.getNumero() != null ? mdfe.getNumero() : "");
		damdfeBean.setFl("");
		damdfeBean.setDtEmissao(mdfe.getDataHoraEmissao() != null ? SinedDateUtils.toString(mdfe.getDataHoraEmissao(), "dd/MM/yyyy HH:mm") : "");
		damdfeBean.setUfCarreg(mdfe.getUfLocalCarregamento() != null && mdfe.getUfLocalCarregamento().getSigla() != null ? mdfe.getUfLocalCarregamento().getSigla() : "");
		damdfeBean.setUfDescar(mdfe.getUfLocalDescarregamento() != null && mdfe.getUfLocalDescarregamento().getSigla() != null ? mdfe.getUfLocalDescarregamento().getSigla() : "");
		
		if (StringUtils.isNotEmpty(mdfe.getQrCode())) {
			try {
				damdfeBean.setQrCode(LkMdfeUtil.gerarByteQrCode(mdfe.getQrCode(), 100, 100));
			} catch (Exception e) {
			}
		}
		
		
		damdfeBean.setQtdCte(mdfe.getListaMdfeCte() != null ? mdfe.getListaMdfeCte().size() + "" : "0");
		damdfeBean.setQtdNfe(mdfe.getListaNfe() != null ? mdfe.getListaNfe().size() + "" : "0");
		damdfeBean.setPesoTotal(mdfe.getPesoBrutoTotalCarga() != null ? new Money(mdfe.getPesoBrutoTotalCarga()).toString() : "0,00");
		damdfeBean.setTipoPeso(mdfe.getUnidadeMedidaPesoBrutoCarga() != null ? mdfe.getUnidadeMedidaPesoBrutoCarga().getNome().split("-")[1].trim() : "");
		
		damdfeBean.setPlaca(mdfe.getPlaca() != null ? mdfe.getPlaca().toUpperCase() : "");
		damdfeBean.setRntrc(mdfe.getRntrc() != null ? mdfe.getRntrc() : "");
		
		List<DamdfeValePedagio> damdfeValePedagioList = new ArrayList<DamdfeValePedagio>();
		if (mdfe.getListaValePedagio() != null) {
			DamdfeValePedagio damdfeValePedagio;
			
			for (MdfeRodoviarioValePedagio valePedagio : mdfe.getListaValePedagio()) {
				damdfeValePedagio = new DamdfeValePedagio();
				
				damdfeValePedagio.setFornecedoraCnpj(valePedagio.getCnpjEmpresaFornecedora() != null && valePedagio.getCnpjEmpresaFornecedora().getValue() != null ? 
						valePedagio.getCnpjEmpresaFornecedora().getValue() : "");
				damdfeValePedagio.setnComprovante(valePedagio.getNumeroComprovante() != null ? valePedagio.getNumeroComprovante() : "");
				damdfeValePedagio.setResponsavelCnpj(valePedagio.getCnpjResponsavelPagamento() != null && valePedagio.getCnpjResponsavelPagamento().getValue() != null ? 
						valePedagio.getCnpjResponsavelPagamento().getValue() : 
						(valePedagio.getCpfResponsavelPagamento() != null && valePedagio.getCpfResponsavelPagamento().getValue() != null) ? 
						valePedagio.getCpfResponsavelPagamento().getValue() : "");
				
				damdfeValePedagioList.add(damdfeValePedagio);
			}
		}
		damdfeBean.setDamdfeValePedagioList(damdfeValePedagioList);
		
		List<DamdfeCondutor> damdfeCondutorList = new ArrayList<DamdfeCondutor>();
		if (mdfe.getListaCondutor() != null) {
			DamdfeCondutor damdfeCondutor;
			
			for (MdfeRodoviarioCondutor condutor : mdfe.getListaCondutor()) {
				damdfeCondutor = new DamdfeCondutor();
				
				damdfeCondutor.setCpf(condutor.getCpf() != null && condutor.getCpf().getValue() != null ? 
						condutor.getCpf().getValue() : "");
				damdfeCondutor.setNome(condutor.getNome() != null ? condutor.getNome() : "");
				
				damdfeCondutorList.add(damdfeCondutor);
			}
		}
		damdfeBean.setDamdfeCondutorList(damdfeCondutorList);
		
		damdfeBean.setObservacao(mdfe.getInfoAdicionaisContribuinte() != null ? mdfe.getInfoAdicionaisContribuinte() : "");
		
		damdfeBean.setMarcaNacionalidadeAeronave(mdfe.getMarcaNacionalidadeAeronave() != null ? mdfe.getMarcaNacionalidadeAeronave() : "");
		damdfeBean.setMarcaMatriculaAeronave(mdfe.getMarcaMatriculaAeronave() != null ? mdfe.getMarcaMatriculaAeronave() : "");
		damdfeBean.setNumeroVoo(mdfe.getNumeroVoo() != null ? mdfe.getNumeroVoo() : "");
		damdfeBean.setDataVoo(mdfe.getDataVoo() != null ? SinedDateUtils.toString(mdfe.getDataVoo()) : "");
		damdfeBean.setAerodromoEmbarque(mdfe.getAerodromoEmbarque() != null ? mdfe.getAerodromoEmbarque() : "");
		damdfeBean.setAerodromoDestino(mdfe.getAerodromoDestino() != null ? mdfe.getAerodromoDestino() : "");
		
		damdfeBean.setPrefixoTrem(mdfe.getPrefixoTrem() != null ? mdfe.getPrefixoTrem() : "");
		damdfeBean.setDtTrem(mdfe.getDataHoraLiberacaoTremOrigem() != null ? SinedDateUtils.toString(mdfe.getDataHoraLiberacaoTremOrigem(), "dd/MM/yyyy HH:mm") : "");
		damdfeBean.setOrigemTrem(mdfe.getOrigemTrem() != null ? mdfe.getOrigemTrem() : "");
		damdfeBean.setDestinoTrem(mdfe.getDestinoTrem() != null ? mdfe.getDestinoTrem() : "");
		damdfeBean.setQtdeVagoesCarregados(mdfe.getQtdeVagoesCarregados() != null ? mdfe.getQtdeVagoesCarregados().toString() : "0");
		
		List<DamdfeVagao> damdfeVagaoList = new ArrayList<DamdfeVagao>();
		if (mdfe.getListaVagao() != null) {
			DamdfeVagao damdfeVagao;
			
			for (MdfeFerroviarioVagao vagao : mdfe.getListaVagao()) {
				damdfeVagao = new DamdfeVagao();
				
				damdfeVagao.setNumeroIdent(vagao.getNumeroIdentificacao() != null ? vagao.getNumeroIdentificacao() : "");
				damdfeVagao.setSeq(vagao.getSequenciaNaComposicao() != null ? vagao.getSequenciaNaComposicao() : "");
				damdfeVagao.setSerieIdent(vagao.getSerieIdentificacao() != null ? vagao.getSerieIdentificacao() : "");
				damdfeVagao.setTonUtil(vagao.getToneladaUtil() != null ? new Money(vagao.getToneladaUtil()).toString() : "0,00");
				
				damdfeVagaoList.add(damdfeVagao);
			}
		}
		damdfeBean.setDamdfeVagoesList(damdfeVagaoList);
		
		damdfeBean.setCodigoEmbarcacao(mdfe.getCodigoEmbarcacao() != null ? mdfe.getCodigoEmbarcacao() : "");
		damdfeBean.setNomeEmbarcacao(mdfe.getNomeEmbarcacao() != null ? mdfe.getNomeEmbarcacao() : "");
		damdfeBean.setQtdMdfe(mdfe.getListaMdfeReferenciado() != null ? mdfe.getListaMdfeReferenciado().size() + "" : "0");
		
		List<DamdfeDadosTerminal> damdfeDadosTerminalCarregList = new ArrayList<DamdfeDadosTerminal>();
		if (mdfe.getListaAquaviarioTerminalCarregamento() != null) {
			DamdfeDadosTerminal damdfeDadosTerminal;
			
			for (MdfeAquaviarioTerminalCarregamento bean : mdfe.getListaAquaviarioTerminalCarregamento()) {
				damdfeDadosTerminal = new DamdfeDadosTerminal();
				
				damdfeDadosTerminal.setCodigo(bean.getCodigoTerminal());
				damdfeDadosTerminal.setNome(bean.getNomeTerminal());
				
				damdfeDadosTerminalCarregList.add(damdfeDadosTerminal);
			}
		}
		damdfeBean.setDamdfeDadosTerminalCarregList(damdfeDadosTerminalCarregList);
		
		List<DamdfeDadosTerminal> damdfeDadosTerminalDescarregList = new ArrayList<DamdfeDadosTerminal>();
		if (mdfe.getListaAquaviarioTerminalDescarregamento() != null) {
			DamdfeDadosTerminal damdfeDadosTerminal;
			
			for (MdfeAquaviarioTerminalDescarregamento bean : mdfe.getListaAquaviarioTerminalDescarregamento()) {
				damdfeDadosTerminal = new DamdfeDadosTerminal();
				
				damdfeDadosTerminal.setCodigo(bean.getCodigoTerminal());
				damdfeDadosTerminal.setNome(bean.getNomeTerminal());
				
				damdfeDadosTerminalDescarregList.add(damdfeDadosTerminal);
			}
		}
		damdfeBean.setDamdfeDadosTerminalDescarregList(damdfeDadosTerminalDescarregList);
		
		List<DamdfeUnidadeTranspCarga> damdfeUnidadeTranspCargaList = new ArrayList<DamdfeUnidadeTranspCarga>();
		DamdfeUnidadeTranspCarga damdfeUnidadeTranspCarga;
		
		if (mdfe.getListaAquaviarioUnidadeTransporteVazia() != null && mdfe.getListaAquaviarioUnidadeCargaVazia() == null) {
			for (MdfeAquaviarioUnidadeTransporteVazia bean : mdfe.getListaAquaviarioUnidadeTransporteVazia()) {
				damdfeUnidadeTranspCarga = new DamdfeUnidadeTranspCarga();
				
				damdfeUnidadeTranspCarga.setUnidadeTransp(bean.getIdentificacaoUnidadeTransporte());
				
				damdfeUnidadeTranspCargaList.add(damdfeUnidadeTranspCarga);
			}
		} else if (mdfe.getListaAquaviarioUnidadeTransporteVazia() == null && mdfe.getListaAquaviarioUnidadeCargaVazia() != null) {
			for (MdfeAquaviarioUnidadeCargaVazia bean : mdfe.getListaAquaviarioUnidadeCargaVazia()) {
				damdfeUnidadeTranspCarga = new DamdfeUnidadeTranspCarga();
				
				damdfeUnidadeTranspCarga.setUnidadeCarga(bean.getIdentificacaoUnidadeCarga());
				
				damdfeUnidadeTranspCargaList.add(damdfeUnidadeTranspCarga);
			}
		} else if (mdfe.getListaAquaviarioUnidadeTransporteVazia() != null && mdfe.getListaAquaviarioUnidadeCargaVazia() != null) {
			if (mdfe.getListaAquaviarioUnidadeTransporteVazia().size() >= mdfe.getListaAquaviarioUnidadeCargaVazia().size()) {
				for (int i = 0; i < mdfe.getListaAquaviarioUnidadeTransporteVazia().size(); i++) {
					damdfeUnidadeTranspCarga = new DamdfeUnidadeTranspCarga();
					
					damdfeUnidadeTranspCarga.setUnidadeTransp(mdfe.getListaAquaviarioUnidadeTransporteVazia().get(i).getIdentificacaoUnidadeTransporte());
					
					if (mdfe.getListaAquaviarioUnidadeCargaVazia().size() > i) {
						damdfeUnidadeTranspCarga.setUnidadeCarga(mdfe.getListaAquaviarioUnidadeCargaVazia().get(i).getIdentificacaoUnidadeCarga());
					} else {
						damdfeUnidadeTranspCarga.setUnidadeCarga("");
					}
					
					damdfeUnidadeTranspCargaList.add(damdfeUnidadeTranspCarga);
				}
			} else if (mdfe.getListaAquaviarioUnidadeTransporteVazia().size() <= mdfe.getListaAquaviarioUnidadeCargaVazia().size()) {
				for (int i = 0; i <= mdfe.getListaAquaviarioUnidadeTransporteVazia().size(); i++) {
					damdfeUnidadeTranspCarga = new DamdfeUnidadeTranspCarga();
					
					damdfeUnidadeTranspCarga.setUnidadeCarga(mdfe.getListaAquaviarioUnidadeCargaVazia().get(i).getIdentificacaoUnidadeCarga());
					
					
					if (mdfe.getListaAquaviarioUnidadeTransporteVazia().size() > i) {
						damdfeUnidadeTranspCarga.setUnidadeTransp(mdfe.getListaAquaviarioUnidadeTransporteVazia().get(i).getIdentificacaoUnidadeTransporte());
					} else {
						damdfeUnidadeTranspCarga.setUnidadeTransp("");
					}
					
					damdfeUnidadeTranspCargaList.add(damdfeUnidadeTranspCarga);
				}
			}
		}
		
		damdfeBean.setDamdfeUnidadeTranspCargaList(damdfeUnidadeTranspCargaList);
		
		List<DamdfeDocFiscTranspCarga> damdfeDocFiscTranspCargaList = new ArrayList<DamdfeDocFiscTranspCarga>();
		DamdfeDocFiscTranspCarga damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga();
		if (mdfe.getListaMdfeCte() != null && mdfe.getListaMdfeCte().size() > 0) {
			for (MdfeCte mdfeCte : mdfe.getListaMdfeCte()) {
				List<MdfeInformacaoUnidadeTransporteCte> transpCteList = mdfeCte.getListaInformacaoUnidadeTransporteCte(mdfeCte, mdfe);
				
				if (transpCteList != null && transpCteList.size() > 0) {
					for (MdfeInformacaoUnidadeTransporteCte transpCte : transpCteList) {
						List<MdfeInformacaoUnidadeCargaCte> cargaCteList = transpCte.getListaInformacaoUnidadeCargaCte(transpCte, mdfe);
						
						if (cargaCteList != null && cargaCteList.size() > 0) {
							for (MdfeInformacaoUnidadeCargaCte cargaCte : cargaCteList) {
								damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("CT-e: " + mdfeCte.getChaveAcesso(), transpCte.getIdentificacaoTransporte(), 
										cargaCte.getIdentificacaoUnidadeCarga());
								damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
							}
						} else {
							damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("CT-e: " + mdfeCte.getChaveAcesso(), transpCte.getIdentificacaoTransporte(), "");
							damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
						}
					}
				} else {
					damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("CT-e: " + mdfeCte.getChaveAcesso(), "", "");
					damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
				}
			}
		}
		
		if (mdfe.getListaNfe() != null && mdfe.getListaNfe().size() > 0) {
			for (MdfeNfe mdfeNfe : mdfe.getListaNfe()) {
				List<MdfeInformacaoUnidadeTransporteNfe> transpNfeList = mdfeNfe.getListaInformacaoUnidadeTransporteNfe(mdfeNfe, mdfe);
				
				if (transpNfeList != null && transpNfeList.size() > 0) {
					for (MdfeInformacaoUnidadeTransporteNfe transpNfe : transpNfeList) {
						List<MdfeInformacaoUnidadeCargaNfe> cargaNfeList = transpNfe.getListaInformacaoUnidadeCargaNfe(transpNfe, mdfe);
						
						if (cargaNfeList != null && cargaNfeList.size() > 0) {
							for (MdfeInformacaoUnidadeCargaNfe cargaNfe : cargaNfeList) {
								damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("NF-e: " + mdfeNfe.getChaveAcesso(), transpNfe.getIdentificacaoTransporte(), 
										cargaNfe.getIdentificacaoUnidadeCarga());
								damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
							}
						} else {
							damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("NF-e: " + mdfeNfe.getChaveAcesso(), transpNfe.getIdentificacaoTransporte(), "");
							damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
						}
					}
				} else {
					damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("NF-e: " + mdfeNfe.getChaveAcesso(), "", "");
					damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
				}
			}
		}
		
		if (mdfe.getListaMdfeReferenciado() != null && mdfe.getListaMdfeReferenciado().size() > 0) {
			for (MdfeReferenciado mdfeReferenciado : mdfe.getListaMdfeReferenciado()) {
				List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> transpMdfeReferenciadoList = mdfeReferenciado.getListaInformacaoUnidadeTransporteMdfeReferenciado(mdfeReferenciado, mdfe);
				
				if (transpMdfeReferenciadoList != null && transpMdfeReferenciadoList.size() > 0) {
					for (MdfeInformacaoUnidadeTransporteMdfeReferenciado transpMdfeReferenciado : transpMdfeReferenciadoList) {
						List<MdfeInformacaoUnidadeCargaMdfeReferenciado> cargaMdfeReferenciadoList = transpMdfeReferenciado.getListaInformacaoUnidadeCargaMdfeReferenciado(transpMdfeReferenciado, mdfe);
						
						if (cargaMdfeReferenciadoList != null && cargaMdfeReferenciadoList.size() > 0) {
							for (MdfeInformacaoUnidadeCargaMdfeReferenciado cargaMdfeReferenciado : cargaMdfeReferenciadoList) {
								damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("MDF-e: " + mdfeReferenciado.getChaveAcesso(), transpMdfeReferenciado.getIdentificacaoTransporte(), 
										cargaMdfeReferenciado.getIdentificacaoUnidadeCarga());
								damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
							}
						} else {
							damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("MDF-e: " + mdfeReferenciado.getChaveAcesso(), transpMdfeReferenciado.getIdentificacaoTransporte(), "");
							damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
						}
					}
				} else {
					damdfeDocFiscTranspCarga = new DamdfeDocFiscTranspCarga("MDF-e: " + mdfeReferenciado.getChaveAcesso(), "", "");
					damdfeDocFiscTranspCargaList.add(damdfeDocFiscTranspCarga);
				}
			}
		}
		
		damdfeBean.setDamdfeDocFiscTranspCargaList(damdfeDocFiscTranspCargaList);
		
		damdfeBean.setSituacao(mdfe.getMdfeSituacao() != null && mdfe.getMdfeSituacao().getValue() != null ? mdfe.getMdfeSituacao().getValue().toString() : "");
		
		return damdfeBean;
	}

	private List<Mdfe> findForDamdfe(String itensSelecionados) {
		return mdfeDAO.findForDamdfe(itensSelecionados);
	}
	
	public Integer getProximoNumeroMdfe(Empresa empresa){
		Integer proximoNumero = empresaService.carregaProxNumMdfe(empresa);
		if(proximoNumero == null){
			proximoNumero = 1;
		}
		
		return proximoNumero;
	}
	
	public void updateNumeroMdfe(Mdfe mdfe, Integer proxNum) {
		mdfeDAO.updateNumeroMdfe(mdfe, proxNum);
	}
	
	public void updateNumero(Mdfe mdfe) {
		if(mdfe.getCdmdfe() != null){
			Integer proximoNumero = getProximoNumeroMdfe(mdfe.getEmpresa());
			empresaService.updateProximoNumMdfe(mdfe.getEmpresa(), proximoNumero + 1);
			mdfe.setNumero(proximoNumero.toString());
			this.updateNumeroMdfe(mdfe, proximoNumero);
		}
	}

	public void alterarQrCode(Mdfe mdfe) {
		mdfeDAO.alterarQrCode(mdfe);
	}
}
