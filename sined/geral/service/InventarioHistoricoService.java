package br.com.linkcom.sined.geral.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.geral.bean.Inventario;
import br.com.linkcom.sined.geral.bean.InventarioHistorico;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioAcao;
import br.com.linkcom.sined.geral.dao.InventarioHistoricoDAO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class InventarioHistoricoService extends GenericService<InventarioHistorico> {
	
	private InventarioHistoricoDAO inventarioHistoricoDAO;
	
	public void setInventarioHistoricoDAO(
			InventarioHistoricoDAO inventarioHistoricoDAO) {
		this.inventarioHistoricoDAO = inventarioHistoricoDAO;
	}

	/**
	 * M�todo que faz refer�ncia ao DAO.
	 *
	 * @param inventario
	 * @return
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public List<InventarioHistorico> findByInventario(Inventario inventario) {
		return inventarioHistoricoDAO.findByInventario(inventario);
	}
	
	/**
	 * Cria hist�rico de invent�rio
	 *
	 * @param inventario
	 * @param inventarioAcao
	 * @param observacao
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public void createHistoricoByInventario(Inventario inventario, InventarioAcao inventarioAcao, String observacao){
		if(inventario == null || inventario.getCdinventario() == null){
			throw new SinedException("Invent�rio n�o pode ser nulo.");
		}
		
		InventarioHistorico inventarioHistorico = new InventarioHistorico();
		inventarioHistorico.setInventario(inventario);
		inventarioHistorico.setInventarioAcao(inventarioAcao);
		inventarioHistorico.setObservacao(observacao);
		this.saveOrUpdate(inventarioHistorico);
	}
	
	/**
	 * Cria hist�rico de invent�rio
	 *
	 * @param inventario
	 * @param inventarioAcao
	 * @since 03/10/2019
	 * @author Rodrigo Freitas
	 */
	public void createHistoricoByInventario(Inventario inventario, InventarioAcao inventarioAcao){
		this.createHistoricoByInventario(inventario, inventarioAcao, null);
	}
	
	/**
	 * Cria hist�rico de invent�rio
	 *
	 * @param whereIn
	 * @param inventarioAcao
	 * @param observacao
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public void createHistoricoByInventario(String whereIn, InventarioAcao inventarioAcao, String observacao){
		if(StringUtils.isBlank(whereIn)) return;
		
		String[] ids = whereIn.split(",");
		for (String id : ids) {
			this.createHistoricoByInventario(new Inventario(Integer.parseInt(id)), inventarioAcao, observacao);
		}
	}
	
	/**
	 * Cria hist�rico de invent�rio
	 *
	 * @param whereIn
	 * @param inventarioAcao
	 * @since 04/10/2019
	 * @author Rodrigo Freitas
	 */
	public void createHistoricoByInventario(String whereIn, InventarioAcao inventarioAcao){
		this.createHistoricoByInventario(whereIn, inventarioAcao, null);
	}
	
}
