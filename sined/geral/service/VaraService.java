package br.com.linkcom.sined.geral.service;

import java.util.List;

import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Vara;
import br.com.linkcom.sined.geral.dao.VaraDAO;
import br.com.linkcom.sined.util.neo.persistence.GenericService;

public class VaraService extends GenericService<Vara>{
	
	private VaraDAO varaDAO;
	
	public void setVaraDAO(VaraDAO varaDAO) {this.varaDAO = varaDAO;}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * @author Taidson Santos
	 */
	public List<Vara> findByVaraUf(Uf uf, Municipio municipio){
		return varaDAO.findByVaraUf(uf , municipio);
	}
	
	/**
	 * M�todo de refer�ncia ao DAO.
	 * @author Taidson Santos
	 */
	public List<Vara> findByMunicipio(Municipio municipio){
		return varaDAO.findByMunicipio(municipio);
	}
	/**
	 * M�todo de refer�ncia ao DAO.
	 * @author Taidson Santos
	 */
	public Vara findByVaraComarcaUf(Vara vara){
		return varaDAO.findByVaraComarcaUf(vara);
	}
	
}
