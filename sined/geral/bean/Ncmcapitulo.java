package br.com.linkcom.sined.geral.bean;

import java.text.DecimalFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Ncmcapitulo {

	protected Integer cdncmcapitulo;
	protected String descricao;
	
	public Ncmcapitulo() {
	}
	
	public Ncmcapitulo(Integer cdncmcapitulo) {
		this.cdncmcapitulo = cdncmcapitulo;
	}
	
	@Id
	public Integer getCdncmcapitulo() {
		return cdncmcapitulo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"cdncmcapitulo","descricao"})
	public String getCodNome(){
		return getCodeFormat() + " - " + (descricao.length() > 100 ? descricao.substring(0, 100) + "..." : descricao);
	}
	
	@Transient
	public String getCodeFormat(){
		return new DecimalFormat("00").format(cdncmcapitulo);		
	}
	
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdncmcapitulo(Integer cdncmcapitulo) {
		this.cdncmcapitulo = cdncmcapitulo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdncmcapitulo == null) ? 0 : cdncmcapitulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Ncmcapitulo other = (Ncmcapitulo) obj;
		if (cdncmcapitulo == null) {
			if (other.cdncmcapitulo != null)
				return false;
		} else if (!cdncmcapitulo.equals(other.cdncmcapitulo))
			return false;
		return true;
	}
	
}
