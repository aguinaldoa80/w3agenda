package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialtabelaprecocliente", sequenceName = "sq_materialtabelaprecocliente")
public class Materialtabelaprecocliente {

	protected Integer cdmaterialtabelaprecocliente;
	protected Materialtabelapreco materialtabelapreco;
	protected Cliente cliente;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtabelaprecocliente")
	public Integer getCdmaterialtabelaprecocliente() {
		return cdmaterialtabelaprecocliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	public void setCdmaterialtabelaprecocliente(Integer cdmaterialtabelaprecocliente) {
		this.cdmaterialtabelaprecocliente = cdmaterialtabelaprecocliente;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
