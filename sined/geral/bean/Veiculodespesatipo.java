package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Veiculodespesatipo{
	
	public static final Veiculodespesatipo MANUTENCAO = new Veiculodespesatipo(1, "Manuten��o");
	public static final Veiculodespesatipo IMPOSTO = new Veiculodespesatipo(2, "Imposto");
	public static final Veiculodespesatipo INFRACAO = new Veiculodespesatipo(3, "Infra��o");
	public static final Veiculodespesatipo ABASTECIMENTO = new Veiculodespesatipo(4, "Abastecimento");
	public static final Veiculodespesatipo PNEU = new Veiculodespesatipo(5, "Pneu");

	protected Integer cdveiculodespesatipo;
	protected String descricao;
	
	public Veiculodespesatipo(){
	}

	public Veiculodespesatipo(Integer cdveiculodespesatipo, String descricao){
		this.cdveiculodespesatipo = cdveiculodespesatipo;
		this.descricao = descricao;
	}
	
	@Id
	public Integer getCdveiculodespesatipo() {
		return cdveiculodespesatipo;
	}
	@DescriptionProperty
	@Required
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdveiculodespesatipo(Integer cdveiculodespesatipo) {
		this.cdveiculodespesatipo = cdveiculodespesatipo;
	}
	
}