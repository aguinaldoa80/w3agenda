package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_expedicaoproducaohistorico", sequenceName = "sq_expedicaoproducaohistorico")
public class Expedicaoproducaohistorico implements Log {

	protected Integer cdexpedicaoproducaohistorico;
	protected Expedicaoproducao expedicaoproducao;
	protected Expedicaoacao expedicaoacao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicaoproducaohistorico")
	public Integer getCdexpedicaoproducaohistorico() {
		return cdexpedicaoproducaohistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicaoproducao")
	public Expedicaoproducao getExpedicaoproducao() {
		return expedicaoproducao;
	}

	@MaxLength(500)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("A��o")
	public Expedicaoacao getExpedicaoacao() {
		return expedicaoacao;
	}
	
	public void setExpedicaoacao(Expedicaoacao expedicaoacao) {
		this.expedicaoacao = expedicaoacao;
	}

	public void setCdexpedicaoproducaohistorico(Integer cdexpedicaoproducaohistorico) {
		this.cdexpedicaoproducaohistorico = cdexpedicaoproducaohistorico;
	}

	public void setExpedicaoproducao(Expedicaoproducao expedicaoproducao) {
		this.expedicaoproducao = expedicaoproducao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
