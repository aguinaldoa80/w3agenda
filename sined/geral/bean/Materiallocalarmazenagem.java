package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materiallocalarmazenagem", sequenceName = "sq_materiallocalarmazenagem")
public class Materiallocalarmazenagem {
	
	protected Integer cdmateriallocalarmazenagem;
	protected Localarmazenagem localarmazenagem;
	protected Material material;
	protected Double quantidademinima;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materiallocalarmazenagem")
	public Integer getCdmateriallocalarmazenagem() {
		return cdmateriallocalarmazenagem;
	}
	@Required
	@DisplayName("Local de armazenagem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Quantidade M�nima")
	public Double getQuantidademinima() {
		return quantidademinima;
	}
	
	public void setCdmateriallocalarmazenagem(Integer cdmateriallocalarmazenagem) {
		this.cdmateriallocalarmazenagem = cdmateriallocalarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidademinima(Double quantidademinima) {
		this.quantidademinima = quantidademinima;
	}
	
}