package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorstatusfolha", sequenceName = "sq_colaboradorstatusfolha")
public class Colaboradorstatusfolha {

	protected Integer cdcolaboradorstatusfolha;
	protected String nome;
	
	public static final Integer NAO_ENVIADO = 1;
	public static final Integer AGUARDANDO_RESPOSTA = 2;
	public static final Integer ERRO = 3;
	public static final Integer SINCRONIZADO = 4;

	public Colaboradorstatusfolha(){}
	
	public Colaboradorstatusfolha(Integer cdcolaboradorstatusfolha){
		this.cdcolaboradorstatusfolha = cdcolaboradorstatusfolha;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorstatusfolha")
	public Integer getCdcolaboradorstatusfolha() {
		return cdcolaboradorstatusfolha;
	}
	public void setCdcolaboradorstatusfolha(Integer id) {
		this.cdcolaboradorstatusfolha = id;
	}

	
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
