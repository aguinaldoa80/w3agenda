package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Saldo do ICMS")
@SequenceGenerator(name = "sq_saldoicms", sequenceName = "sq_saldoicms")
public class SaldoIcms implements Log {

	private Integer cdSaldoIcms;
	private Date mesAno;
	private Empresa empresa;
	private Money valor;
	private String tipoUtilizacaoCredito;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	protected String mesAnoAux;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_saldoicms")
	public Integer getCdSaldoIcms() {
		return cdSaldoIcms;
	}

	public void setCdSaldoIcms(Integer cdSaldoIcms) {
		this.cdSaldoIcms = cdSaldoIcms;
	}

	public Date getMesAno() {
		return mesAno;
	}

	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Required
	@MaxLength(8)
	@DisplayName("Tipo de Utiliza��o de Cr�dito")
	public String getTipoUtilizacaoCredito() {
		return tipoUtilizacaoCredito;
	}
	
	public void setTipoUtilizacaoCredito(String tipoUtilizacaoCredito) {
		this.tipoUtilizacaoCredito = tipoUtilizacaoCredito;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Required
	@Transient
	@DisplayName("M�s/ano")
	public String getMesAnoAux() {
		if (this.mesAno != null) {
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		}
		
		return null;
	}

	public void setMesAnoAux(String mesAnoAux) {
		try {
			this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAnoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
