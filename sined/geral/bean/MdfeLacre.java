package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_mdfelacre", sequenceName = "sq_mdfelacre")
public class MdfeLacre {

	protected Integer cdMdfeLacre;
	protected Mdfe mdfe;
	protected String numeroLacre;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelacre")
	public Integer getCdMdfeLacre() {
		return cdMdfeLacre;
	}
	public void setCdMdfeLacre(Integer cdMdfeLacre) {
		this.cdMdfeLacre = cdMdfeLacre;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=60)
	@DisplayName("N�mero do lacre")
	public String getNumeroLacre() {
		return numeroLacre;
	}
	public void setNumeroLacre(String numerolacre) {
		this.numeroLacre = numerolacre;
	}
}
