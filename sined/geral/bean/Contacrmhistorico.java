package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_contacrmhistorico", sequenceName = "sq_contacrmhistorico")
public class Contacrmhistorico implements Log{
	
	
	protected Integer cdcontacrmhistorico;
	protected String observacao;
	protected Contacrm contacrm;
	protected Contacrmcontato contacrmcontato;
	protected Meiocontato meiocontato;
	protected Atividadetipo atividadetipo;
	protected Situacaohistorico situacaohistorico;
	protected Empresa empresahistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transient
	protected String dataHistorico;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrmhistorico")
	public Integer getCdcontacrmhistorico() {
		return cdcontacrmhistorico;
	}

	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Conta")
	@JoinColumn(name="cdcontacrm")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrm getContacrm() {
		return contacrm;
	}
	
	@JoinColumn(name="cdcontacrmcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}

	@JoinColumn(name="cdmeiocontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}

	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}

	@DisplayName("Situa��o")
	@JoinColumn(name="cdsituacaohistorico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Situacaohistorico getSituacaohistorico() {
		return situacaohistorico;
	}

	@Required
	@DisplayName("Empresa")
	@JoinColumn(name="cdempresahistorico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	
	public void setSituacaohistorico(Situacaohistorico situacaohistorico) {
		this.situacaohistorico = situacaohistorico;
	}

	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}

	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}

	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}

	public void setCdcontacrmhistorico(Integer cdcontacrmhistorico) {
		this.cdcontacrmhistorico = cdcontacrmhistorico;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	
	@Transient
	public String getUsuarioaltera(){
		if(getCdusuarioaltera() != null){
			return TagFunctions.findUserByCd(getCdusuarioaltera());
		} else return null;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}


	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	public String getDataHistorico() {
		return new SimpleDateFormat("dd/MM/yyyy").format(SinedDateUtils.timestampToDate(this.dtaltera));
	}
	public void setDataHistorico(String dataHistorico) {
		this.dataHistorico = dataHistorico;
	}
	
	
	
}
