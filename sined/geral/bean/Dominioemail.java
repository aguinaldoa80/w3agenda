package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dominioemail", sequenceName = "sq_dominioemail")
@DisplayName("Dom�nio de e-mail")
public class Dominioemail implements Log{
		
	protected Integer cddominioemail;
	protected Servicoservidor servicoservidor;
	protected Dominio dominio;
	protected Dominioemailtransporte dominioemailtransporte;
	protected Dominiozona dominiozonadestino;
	protected Boolean publico;
	protected Boolean ativo = true;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transient
	protected Servidor servidor;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominioemail")
	public Integer getCddominioemail() {
		return cddominioemail;
	}
		
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidor")
	@DisplayName("Servi�o de servidor")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominioemailtransporte")
	@DisplayName("Transporte")
	public Dominioemailtransporte getDominioemailtransporte() {
		return dominioemailtransporte;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominiozonadestino")
	@DisplayName("Servidor de destino")
	public Dominiozona getDominiozonadestino() {
		return dominiozonadestino;
	}
	
	@DisplayName("P�blico")
	public Boolean getPublico() {
		return publico;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCddominioemail(Integer cddominioemail) {
		this.cddominioemail = cddominioemail;
	}

	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setDominioemailtransporte(
			Dominioemailtransporte dominioemailtransporte) {
		this.dominioemailtransporte = dominioemailtransporte;
	}

	public void setDominiozonadestino(Dominiozona dominiozonadestino) {
		this.dominiozonadestino = dominiozonadestino;
	}

	public void setPublico(Boolean publico) {
		this.publico = publico;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	@Transient
	@Required
	public Servidor getServidor() {
		return servidor;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
}
