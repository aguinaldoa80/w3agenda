package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Producaochaofabricasituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_producaochaofabrica", sequenceName = "sq_producaochaofabrica")
public class Producaochaofabrica implements Log {

	protected Integer cdproducaochaofabrica;
	protected Producaoagendamaterial producaoagendamaterial;
	protected Producaochaofabricasituacao producaochaofabricasituacao;
	protected Boolean emitiretiquetaautomatico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Producaochaofabrica() {
	}
	
	public Producaochaofabrica(Integer cdproducaochaofabrica){
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaochaofabrica")
	public Integer getCdproducaochaofabrica() {
		return cdproducaochaofabrica;
	}
	
	@Required
	@JoinColumn(name="cdproducaoagendamaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagendamaterial getProducaoagendamaterial() {
		return producaoagendamaterial;
	}

	@Required
	@DisplayName("Situa��o")
	public Producaochaofabricasituacao getProducaochaofabricasituacao() {
		return producaochaofabricasituacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Boolean getEmitiretiquetaautomatico() {
		return emitiretiquetaautomatico;
	}
	
	public void setEmitiretiquetaautomatico(Boolean emitiretiquetaautomatico) {
		this.emitiretiquetaautomatico = emitiretiquetaautomatico;
	}

	public void setCdproducaochaofabrica(Integer cdproducaochaofabrica) {
		this.cdproducaochaofabrica = cdproducaochaofabrica;
	}

	public void setProducaoagendamaterial(
			Producaoagendamaterial producaoagendamaterial) {
		this.producaoagendamaterial = producaoagendamaterial;
	}

	public void setProducaochaofabricasituacao(
			Producaochaofabricasituacao producaochaofabricasituacao) {
		this.producaochaofabricasituacao = producaochaofabricasituacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
