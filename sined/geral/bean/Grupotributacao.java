package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.util.Log;
@Entity
@SequenceGenerator(name="sq_grupotributacao",sequenceName="sq_grupotributacao")
public class Grupotributacao implements Log{
	
	protected Integer cdgrupotributacao;
	protected String nome;
	protected Operacao operacao;
	protected Cfop cfopgrupo;
	protected String condicao;
	protected Boolean tributadoicms;
	protected Codigocnae codigocnae;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservico;
	protected Boolean ativo = Boolean.TRUE;
	protected Boolean tributacaomunicipiocliente;
	protected Boolean tributacaomunicipioprojeto;
	protected Boolean calcularDifal;
	protected Localdestinonfe localDestinoNfe;
	protected Contribuinteicmstipo contribuinteIcmsTipo;
	protected Operacaonfe operacaonfe;
	
	//ABA ICMS
	protected Boolean exibirmensagemstpaga;
	protected Boolean exibirmensagemstpagatotal;
	protected Boolean stfixa;
	protected Boolean incluiricmsvalor;
	protected Origemproduto origemprodutoicms;
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms;
	protected Modalidadebcicms modalidadebcicms;
	protected Boolean calcularbcicmsretidoanteriormente;
	protected Double reducaobcicms;
	protected Money bcoperacaopropriaicms;
	protected Motivodesoneracaoicms motivodesoneracaoicms;
	protected Double percentualdesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	protected ValorFiscal valorFiscalIcms;
	protected Double aliquotacreditoicms;
	protected Naturezabasecalculocredito naturezabcc;
	protected String formulabcicms;
	protected String formulabcicmsdestinatario;
	
	//ABA ICMS ST
	protected String cest;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Double aliquotaicmsst;
	protected Boolean buscarfaixaimpostoaliquotaicmsst;
	protected Double margemvaloradicionalicmsst;
	protected Boolean buscarfaixaimpostomargemvaloradicionalicmsst;
	protected Boolean naoconsideraricmsst;
	protected Double reducaobcicmsst;
	protected Uf uficmsst;
	protected String formulabcst;
	protected String formulavalorst;
	
	//ABA ISS
	protected Boolean incluirissvalor;
	protected Boolean incentivofiscal;
	protected Tipotributacaoiss tipotributacaoiss;
	protected Municipio municipioincidenteiss;
	
	//ABA IPI
	protected Boolean incluiripivalor;
	protected Tipocobrancaipi tipocobrancaipi;
	protected Tipocalculo tipocalculoipi;
	protected Money aliquotareaisipi;
	protected String codigoenquadramentoipi;
	protected ValorFiscal valorFiscalIpi;
	protected String formulabcipi;
	
	//ABA PIS
	protected Boolean incluirpisvalor;
	protected Tipocobrancapis tipocobrancapis;
	protected Tipocalculo tipocalculopis;
	protected Money aliquotareaispis;
	
	//ABA COFINS
	protected Boolean incluircofinsvalor;
	protected Tipocobrancacofins tipocobrancacofins;
	protected Tipocalculo tipocalculocofins;
	protected Money aliquotareaiscofins;
	
	//ABA INF. ADICIONAIS
	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	
	protected Set<Grupotributacaoempresa> listaGrupotributacaoempresa = new ListSet<Grupotributacaoempresa>(Grupotributacaoempresa.class);
	protected Set<GrupotributacaoNcm> listaGrupotributacaoncm = new ListSet<GrupotributacaoNcm>(GrupotributacaoNcm.class);
	protected Set<Grupotributacaonaturezaoperacao>  listaGrupotributacaonaturezaoperacao = new ListSet<Grupotributacaonaturezaoperacao>(Grupotributacaonaturezaoperacao.class);
	protected Set<Grupotributacaomaterialgrupo>  listaGrupotributacaomaterialgrupo = new ListSet<Grupotributacaomaterialgrupo>(Grupotributacaomaterialgrupo.class);
	protected List<Grupotributacaoimposto> listaGrupotributacaoimposto = new ListSet<Grupotributacaoimposto>(Grupotributacaoimposto.class);
	protected List<Grupotributacaohistorico> listaGrupotributacaohistorico = new ListSet<Grupotributacaohistorico>(Grupotributacaohistorico.class);
	protected List<GrupotributacaoMaterialTipo> listaGrupotributacaoMaterialTipo = new ListSet<GrupotributacaoMaterialTipo>(GrupotributacaoMaterialTipo.class);
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	//TRANSIENT
	protected Material materialTrans;
	protected Empresa empresaTributacaoOrigem;
	protected Endereco enderecoTributacaoOrigem;
	protected Endereco enderecoTributacaoDestino;
	protected Endereco endereconotaDestino;
	protected Boolean enderecoavulso;
	protected Naturezaoperacao naturezaoperacaoTrans;
	protected Cliente clienteTrans;
	protected Fornecedor fornecedorTrans;
	protected Boolean modelodocumentofiscalICMS;
	
	public Grupotributacao() {}
	
	public Grupotributacao(Integer cdgrupotributacao) {
		this.cdgrupotributacao = cdgrupotributacao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacao")
	public Integer getCdgrupotributacao() {
		return cdgrupotributacao;
	}
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@DisplayName("Impostos")
	@OneToMany(mappedBy="grupotributacao")
	public List<Grupotributacaoimposto> getListaGrupotributacaoimposto() {
		return listaGrupotributacaoimposto;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="grupotributacao")
	public List<Grupotributacaohistorico> getListaGrupotributacaohistorico() {
		return listaGrupotributacaohistorico;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	@DisplayName("Incentivo fiscal")
	public Boolean getIncentivofiscal() {
		return incentivofiscal;
	}
	public void setIncentivofiscal(Boolean incentivofiscal) {
		this.incentivofiscal = incentivofiscal;
	}
	public void setCdgrupotributacao(Integer cdgrupotributacao) {
		this.cdgrupotributacao = cdgrupotributacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaGrupotributacaoimposto(
			List<Grupotributacaoimposto> listaGrupotributacaoimposto) {
		this.listaGrupotributacaoimposto = listaGrupotributacaoimposto;
	}
	public void setListaGrupotributacaohistorico(
			List<Grupotributacaohistorico> listaGrupotributacaohistorico) {
		this.listaGrupotributacaohistorico = listaGrupotributacaohistorico;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Tipo de cobran�a ICMS")
	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}
	@DisplayName("Tipo de cobran�a do IPI")
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	@DisplayName("Tipo de cobran�a do PIS")
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}
	@DisplayName("Tipo de cobran�a do COFINS")
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}
	@DisplayName("Tipo de tributa��o do ISS")
	public Tipotributacaoiss getTipotributacaoiss() {
		return tipotributacaoiss;
	}
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculopis() {
		return tipocalculopis;
	}
	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaispis() {
		return aliquotareaispis;
	}
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}
	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculocofins() {
		return tipocalculocofins;
	}
	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaiscofins() {
		return aliquotareaiscofins;
	}
	@DisplayName("Modal. de determ. do BC do ICMS")
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}
	@DisplayName("Calcular BC e ICMS retido anteriormente")
	public Boolean getCalcularbcicmsretidoanteriormente() {
		return calcularbcicmsretidoanteriormente;
	}
	@DisplayName("Modal. de determ. do BC do ICMS ST")
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	@DisplayName("% redu��o da BC do ICMS ST")
	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}
	@DisplayName("% margem de valor adic. ICMS ST")
	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}
	@DisplayName("% redu��o da BC do ICMS")
	public Double getReducaobcicms() {
		return reducaobcicms;
	}
	@DisplayName("% BC da opera��o pr�pria")
	public Money getBcoperacaopropriaicms() {
		return bcoperacaopropriaicms;
	}
	@DisplayName("UF do ICMS ST devido na opera��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduficmsst")
	public Uf getUficmsst() {
		return uficmsst;
	}
	@DisplayName("Motivo da desonera��o do ICMS")
	public Motivodesoneracaoicms getMotivodesoneracaoicms() {
		return motivodesoneracaoicms;
	}
	@DisplayName("Origem do Produto do ICMS")
	public Origemproduto getOrigemprodutoicms() {
		return origemprodutoicms;
	}
	@DisplayName("Regime do ICMS")
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}
	@DisplayName("Incluir valor do ISS no valor bruto")
	public Boolean getIncluirissvalor() {
		return incluirissvalor;
	}
	@DisplayName("Incluir valor do ICMS no valor bruto")
	public Boolean getIncluiricmsvalor() {
		return incluiricmsvalor;
	}
	@DisplayName("Incluir valor do IPI no valor bruto")
	public Boolean getIncluiripivalor() {
		return incluiripivalor;
	}
	@DisplayName("Incluir valor do PIS no valor bruto")
	public Boolean getIncluirpisvalor() {
		return incluirpisvalor;
	}
	@DisplayName("Incluir valor do COFINS no valor bruto")
	public Boolean getIncluircofinsvalor() {
		return incluircofinsvalor;
	}
	@DisplayName("Exibir mensagem ST j� paga")
	public Boolean getExibirmensagemstpaga() {
		return exibirmensagemstpaga;
	}
	@DisplayName("Exibir mensagem somente do total ST j� paga")
	public Boolean getExibirmensagemstpagatotal() {
		return exibirmensagemstpagatotal;
	}
	@DisplayName("ST fixa")
	public Boolean getStfixa() {
		return stfixa;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Fisco")
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Contribuinte")
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIcms() {
		return valorFiscalIcms;
	}
	@DisplayName("Al�quota aplic�vel de c�lculo do cr�dito")
	public Double getAliquotacreditoicms() {
		return aliquotacreditoicms;
	}
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIpi() {
		return valorFiscalIpi;
	}
	@DisplayName("F�rmula para c�lculo da base de c�lculo do IPI")
	public String getFormulabcipi() {
		return formulabcipi;
	}
	@DisplayName("CFOP")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfopgrupo")
	public Cfop getCfopgrupo() {
		return cfopgrupo;
	}
	@DisplayName("Utiliza��o")
	public Operacao getOperacao() {
		return operacao;
	}
	@DisplayName("Condi��o")
	public String getCondicao() {
		return condicao;
	}
	@DisplayName("Produto tributado pelo:")
	public Boolean getTributadoicms() {
		return tributadoicms;
	}
	@DisplayName("Tributacao no munic�pio do cliente")
	public Boolean getTributacaomunicipiocliente() {
		return tributacaomunicipiocliente;
	}
	@DisplayName("Calcular DIFAL na venda")
	public Boolean getCalcularDifal() {
		return calcularDifal;
	}
	@DisplayName("Tributa��o no munic�pio do projeto")
	public Boolean getTributacaomunicipioprojeto() {
		return tributacaomunicipioprojeto;
	}
	@DisplayName("Al�quota ICMS ST")
	public Double getAliquotaicmsst() {
		return aliquotaicmsst;
	}
	@DisplayName("Buscar da faixa de imposto")
	public Boolean getBuscarfaixaimpostoaliquotaicmsst() {
		return buscarfaixaimpostoaliquotaicmsst;
	}
	@DisplayName("Buscar da faixa de imposto")
	public Boolean getBuscarfaixaimpostomargemvaloradicionalicmsst() {
		return buscarfaixaimpostomargemvaloradicionalicmsst;
	}
	@DisplayName("F�rmula para calculo da base de calculo do ST")
	public String getFormulabcst() {
		return formulabcst;
	}
	@DisplayName("F�rmula para calculo do valor do ST")
	public String getFormulavalorst() {
		return formulavalorst;
	}
	@DisplayName("Munic�pio de incid�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipioincidenteiss")
	public Municipio getMunicipioincidenteiss() {
		return municipioincidenteiss;
	}
	
	@DisplayName("Percentual de desoneracao")
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	@DisplayName("Abater desonera��o")
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}
	
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}

	public void setTributacaomunicipiocliente(Boolean tributacaomunicipiocliente) {
		this.tributacaomunicipiocliente = tributacaomunicipiocliente;
	}
	public void setExibirmensagemstpaga(Boolean exibirmensagemstpaga) {
		this.exibirmensagemstpaga = exibirmensagemstpaga;
	}
	public void setExibirmensagemstpagatotal(Boolean exibirmensagemstpagatotal) {
		this.exibirmensagemstpagatotal = exibirmensagemstpagatotal;
	}
	public void setStfixa(Boolean stfixa) {
		this.stfixa = stfixa;
	}
	public void setIncluiricmsvalor(Boolean incluiricmsvalor) {
		this.incluiricmsvalor = incluiricmsvalor;
	}
	public void setOrigemprodutoicms(Origemproduto origemprodutoicms) {
		this.origemprodutoicms = origemprodutoicms;
	}
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	public void setCalcularbcicmsretidoanteriormente(Boolean calcularbcicmsretidoanteriormente) {
		this.calcularbcicmsretidoanteriormente = calcularbcicmsretidoanteriormente;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	public void setTributacaomunicipioprojeto(Boolean tributacaomunicipioprojeto) {
		this.tributacaomunicipioprojeto = tributacaomunicipioprojeto;
	}
	public void setCalcularDifal(Boolean calcularDifal) {
		this.calcularDifal = calcularDifal;
	}
	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}
	public void setReducaobcicms(Double reducaobcicms) {
		this.reducaobcicms = reducaobcicms;
	}
	public void setBcoperacaopropriaicms(Money bcoperacaopropriaicms) {
		this.bcoperacaopropriaicms = bcoperacaopropriaicms;
	}
	public void setUficmsst(Uf uficmsst) {
		this.uficmsst = uficmsst;
	}
	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}
	public void setMotivodesoneracaoicms(Motivodesoneracaoicms motivodesoneracaoicms) {
		this.motivodesoneracaoicms = motivodesoneracaoicms;
	}
	public void setIncluirissvalor(Boolean incluirissvalor) {
		this.incluirissvalor = incluirissvalor;
	}
	public void setTipotributacaoiss(Tipotributacaoiss tipotributacaoiss) {
		this.tipotributacaoiss = tipotributacaoiss;
	}
	public void setIncluiripivalor(Boolean incluiripivalor) {
		this.incluiripivalor = incluiripivalor;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public void setIncluirpisvalor(Boolean incluirpisvalor) {
		this.incluirpisvalor = incluirpisvalor;
	}
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}
	public void setTipocalculopis(Tipocalculo tipocalculopis) {
		this.tipocalculopis = tipocalculopis;
	}
	public void setAliquotareaispis(Money aliquotareaispis) {
		this.aliquotareaispis = aliquotareaispis;
	}
	public void setIncluircofinsvalor(Boolean incluircofinsvalor) {
		this.incluircofinsvalor = incluircofinsvalor;
	}
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}
	public void setTipocalculocofins(Tipocalculo tipocalculocofins) {
		this.tipocalculocofins = tipocalculocofins;
	}
	public void setAliquotareaiscofins(Money aliquotareaiscofins) {
		this.aliquotareaiscofins = aliquotareaiscofins;
	}
	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
	public void setValorFiscalIcms(ValorFiscal valorFiscalIcms) {
		this.valorFiscalIcms = valorFiscalIcms;
	}
	public void setAliquotacreditoicms(Double aliquotacreditoicms) {
		this.aliquotacreditoicms = aliquotacreditoicms;
	}
	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi) {
		this.valorFiscalIpi = valorFiscalIpi;
	}
	public void setFormulabcipi(String formulabcipi) {
		this.formulabcipi = formulabcipi;
	}
	public void setCfopgrupo(Cfop cfopgrupo) {
		this.cfopgrupo = cfopgrupo;
	}
	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}
	public void setCondicao(String condicao) {
		this.condicao = condicao;
	}
	public void setTributadoicms(Boolean tributadoicms) {
		this.tributadoicms = tributadoicms;
	}
	public void setAliquotaicmsst(Double aliquotaicmsst) {
		this.aliquotaicmsst = aliquotaicmsst;
	}
	public void setBuscarfaixaimpostoaliquotaicmsst(Boolean buscarfaixaimpostoaliquotaicmsst) {
		this.buscarfaixaimpostoaliquotaicmsst = buscarfaixaimpostoaliquotaicmsst;
	}
	public void setBuscarfaixaimpostomargemvaloradicionalicmsst(Boolean buscarfaixaimpostomargemvaloradicionalicmsst) {
		this.buscarfaixaimpostomargemvaloradicionalicmsst = buscarfaixaimpostomargemvaloradicionalicmsst;
	}
	public void setFormulabcst(String formulabcst) {
		this.formulabcst = formulabcst;
	}
	public void setFormulavalorst(String formulavalorst) {
		this.formulavalorst = formulavalorst;
	}
	public void setMunicipioincidenteiss(Municipio municipioincidenteiss) {
		this.municipioincidenteiss = municipioincidenteiss;
	}
	
	@DisplayName("Empresa")
	@OneToMany(mappedBy="grupotributacao")
	public Set<Grupotributacaoempresa> getListaGrupotributacaoempresa() {
		return listaGrupotributacaoempresa;
	}
	
	@DisplayName("NCM")
	@OneToMany(mappedBy="grupotributacao")
	public Set<GrupotributacaoNcm> getListaGrupotributacaoncm() {
		return listaGrupotributacaoncm;
	}
	
	@DisplayName("Natureza Opera��o")
	@OneToMany(mappedBy="grupotributacao")
	public Set<Grupotributacaonaturezaoperacao> getListaGrupotributacaonaturezaoperacao() {
		return listaGrupotributacaonaturezaoperacao;
	}
	@DisplayName("Grupo Material")
	@OneToMany(mappedBy="grupotributacao")
	public Set<Grupotributacaomaterialgrupo> getListaGrupotributacaomaterialgrupo() {
		return listaGrupotributacaomaterialgrupo;
	}
	public void setListaGrupotributacaoempresa(Set<Grupotributacaoempresa> listaGrupotributacaoempresa) {
		this.listaGrupotributacaoempresa = listaGrupotributacaoempresa;
	}
	public void setListaGrupotributacaonaturezaoperacao(Set<Grupotributacaonaturezaoperacao> listaGrupotributacaonaturezaoperacao) {
		this.listaGrupotributacaonaturezaoperacao = listaGrupotributacaonaturezaoperacao;
	}
	public void setListaGrupotributacaomaterialgrupo(Set<Grupotributacaomaterialgrupo> listaGrupotributacaomaterialgrupo) {
		this.listaGrupotributacaomaterialgrupo = listaGrupotributacaomaterialgrupo;
	}
	
	
	@Transient
	public Material getMaterialTrans() {
		return materialTrans;
	}
	@Transient
	public Empresa getEmpresaTributacaoOrigem() {
		return empresaTributacaoOrigem;
	}
	@Transient
	public Endereco getEnderecoTributacaoOrigem() {
		return enderecoTributacaoOrigem;
	}
	@Transient
	public Endereco getEnderecoTributacaoDestino() {
		return enderecoTributacaoDestino;
	}
	@Transient
	public Endereco getEndereconotaDestino() {
		return endereconotaDestino;
	}
	@Transient
	public Boolean getEnderecoavulso() {
		return enderecoavulso;
	}
	@Transient
	public Naturezaoperacao getNaturezaoperacaoTrans() {
		return naturezaoperacaoTrans;
	}
	@Transient
	public Cliente getClienteTrans() {
		return clienteTrans;
	}
	@Transient
	public Fornecedor getFornecedorTrans() {
		return fornecedorTrans;
	}
	
	public void setFornecedorTrans(Fornecedor fornecedorTrans) {
		this.fornecedorTrans = fornecedorTrans;
	}
	public void setClienteTrans(Cliente clienteTrans) {
		this.clienteTrans = clienteTrans;
	}
	public void setMaterialTrans(Material materialTrans) {
		this.materialTrans = materialTrans;
	}
	public void setEmpresaTributacaoOrigem(Empresa empresaTributacaoOrigem) {
		this.empresaTributacaoOrigem = empresaTributacaoOrigem;
	}
	public void setEnderecoTributacaoOrigem(Endereco enderecoTributacaoOrigem) {
		this.enderecoTributacaoOrigem = enderecoTributacaoOrigem;
	}
	public void setEnderecoTributacaoDestino(Endereco enderecoTributacaoDestino) {
		this.enderecoTributacaoDestino = enderecoTributacaoDestino;
	}
	public void setEndereconotaDestino(Endereco endereconotaDestino) {
		this.endereconotaDestino = endereconotaDestino;
	}
	public void setEnderecoavulso(Boolean enderecoavulso) {
		this.enderecoavulso = enderecoavulso;
	}
	public void setNaturezaoperacaoTrans(Naturezaoperacao naturezaoperacaoTrans) {
		this.naturezaoperacaoTrans = naturezaoperacaoTrans;
	}
	
	@DisplayName("Natureza base de c�lculo cr�dito")
	public Naturezabasecalculocredito getNaturezabcc() {
		return naturezabcc;
	}
	@DisplayName("F�rmula para calculo da base de calculo do ICMS")
	public String getFormulabcicms() {
		return formulabcicms;
	}
	@DisplayName("F�rmula para calculo da base de calculo do destinat�rio")
	public String getFormulabcicmsdestinatario() {
		return formulabcicmsdestinatario;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setFormulabcicmsdestinatario(String formulabcicmsdestinatario) {
		this.formulabcicmsdestinatario = formulabcicmsdestinatario;
	}
	public void setNaturezabcc(Naturezabasecalculocredito naturezabcc) {
		this.naturezabcc = naturezabcc;
	}
	public void setFormulabcicms(String formulabcicms) {
		this.formulabcicms = formulabcicms;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@DisplayName("C�digo CNAE")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigocnae")
	public Codigocnae getCodigocnae() {
		return codigocnae;
	}

	@DisplayName("C�digo de tributa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigotributacao")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	@DisplayName("Item da lista de servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cditemlistaservico")
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}

	public void setCodigocnae(Codigocnae codigocnae) {
		this.codigocnae = codigocnae;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}

	@Transient
	public Boolean getModelodocumentofiscalICMS() {
		return modelodocumentofiscalICMS;
	}
	
	public void setModelodocumentofiscalICMS(Boolean modelodocumentofiscalICMS) {
		this.modelodocumentofiscalICMS = modelodocumentofiscalICMS;
	}

	@DisplayName("C�digo de enquadramento")
	@MaxLength(3)
	public String getCodigoenquadramentoipi() {
		return codigoenquadramentoipi;
	}

	public void setCodigoenquadramentoipi(String codigoenquadramentoipi) {
		this.codigoenquadramentoipi = codigoenquadramentoipi;
	}

	@MaxLength(7)
	public String getCest() {
		return cest;
	}

	public void setCest(String cest) {
		this.cest = cest;
	}
	
	public void setListaGrupotributacaoncm(Set<GrupotributacaoNcm> listaGrupotributacaoncm) {
		this.listaGrupotributacaoncm = listaGrupotributacaoncm;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Grupotributacao) {
			Grupotributacao p = (Grupotributacao) obj;
			return p.cdgrupotributacao.equals(this.cdgrupotributacao);
		}
		return super.equals(obj);
	}
	
	@DisplayName("N�o Considerar o Valor do ICMS ST no Total da Nota")
	public Boolean getNaoconsideraricmsst() {
		return naoconsideraricmsst;
	}

	public void setNaoconsideraricmsst(Boolean naoconsideraricmsst) {
		this.naoconsideraricmsst = naoconsideraricmsst;
	}
	
	@DisplayName("Tipo de material")
	@OneToMany(mappedBy = "grupotributacao")
	public List<GrupotributacaoMaterialTipo> getListaGrupotributacaoMaterialTipo() {
		return listaGrupotributacaoMaterialTipo;
	}
	
	public void setListaGrupotributacaoMaterialTipo(
			List<GrupotributacaoMaterialTipo> listaGrupotributacaoMaterialTipo) {
		this.listaGrupotributacaoMaterialTipo = listaGrupotributacaoMaterialTipo;
	}
	
	@DisplayName("Local de destino")
	public Localdestinonfe getLocalDestinoNfe() {
		return localDestinoNfe;
	}
	public void setLocalDestinoNfe(Localdestinonfe localDestinoNfe) {
		this.localDestinoNfe = localDestinoNfe;
	}
	
	@DisplayName("Contribuinte ICMS")
	public Contribuinteicmstipo getContribuinteIcmsTipo() {
		return contribuinteIcmsTipo;
	}
	public void setContribuinteIcmsTipo(Contribuinteicmstipo contribuinteIcmsTipo) {
		this.contribuinteIcmsTipo = contribuinteIcmsTipo;
	}
	
	@DisplayName("Consumidor final")
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
}
