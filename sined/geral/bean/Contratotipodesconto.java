package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator (name = "sq_contratotipodesconto", sequenceName = "sq_contratotipodesconto")
@DisplayName("Tipo de Desconto")
public class Contratotipodesconto {
	
	private Integer cdcontratotipodesconto;
	private Contratotipo contratotipo;
	private Integer diasantesvencimento;
	private boolean forma;
	private Money valor;
	private Date limite;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratotipodesconto")
	public Integer getCdcontratotipodesconto() {
		return cdcontratotipodesconto;
	}
	public void setCdcontratotipodesconto(Integer cdcontratotipodesconto) {
		this.cdcontratotipodesconto = cdcontratotipodesconto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratotipo")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	
	@Required
	@DisplayName("Limite de dias antes do vencimento")
	public Integer getDiasantesvencimento() {
		return diasantesvencimento;
	}
	public void setDiasantesvencimento(Integer diasantesvencimento) {
		this.diasantesvencimento = diasantesvencimento;
	}
	
	@Required
	@DisplayName("Tipo de c�lculo")
	public boolean isForma() {
		return forma;
	}
	public void setForma(boolean forma) {
		this.forma = forma;
	}
	
	@Required
	@DisplayName("Valor / Percentual")
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@DisplayName("Valido at�")
	public Date getLimite() {
		return limite;
	}
	public void setLimite(Date limite) {
		this.limite = limite;
	}
}
