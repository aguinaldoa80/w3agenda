package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_atividadetiporesponsavel",sequenceName="sq_atividadetiporesponsavel")
public class Atividadetiporesponsavel {
	
	private Integer cdatividadetiporesponsavel;
	private Atividadetipo atividadetipo;
	private Colaborador colaborador;
	private Projeto projeto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_atividadetiporesponsavel", strategy=GenerationType.AUTO)
	public Integer getCdatividadetiporesponsavel() {
		return cdatividadetiporesponsavel;
	}
	
	public void setCdatividadetiporesponsavel(Integer cdatividadetiporesponsavel) {
		this.cdatividadetiporesponsavel = cdatividadetiporesponsavel;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatividadetipo")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	@Required
	@DisplayName("Nome")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}
