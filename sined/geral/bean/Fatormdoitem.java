package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Item de fator de m�o-de-obra")
@SequenceGenerator(name = "sq_fatormdoitem", sequenceName = "sq_fatormdoitem")
public class Fatormdoitem {
	
	protected Integer cdfatormdoitem;
	protected Fatormdo fatormdo;
	protected String sigla;
	protected String nome;
	protected Double valor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fatormdoitem")
	public Integer getCdfatormdoitem() {
		return cdfatormdoitem;
	}
	
	@DisplayName("Fato de m�o-de-obra")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdfatormdo")
	@Required	
	public Fatormdo getFatormdo() {
		return fatormdo;
	}

	@DisplayName("Nome")
	@MaxLength(30)
	@DescriptionProperty
	@Required	
	public String getNome() {
		return nome;
	}

	@DisplayName("Sigla")
	@MaxLength(2)
	@Required	
	public String getSigla() {
		return sigla;
	}
	
	@Required
	@MinValue(0)
	@MaxValue(999999999)
	public Double getValor() {
		return valor;
	}

	public void setCdfatormdoitem(Integer cdfatormdoitem) {
		this.cdfatormdoitem = cdfatormdoitem;
	}

	public void setFatormdo(Fatormdo fatormdo) {
		this.fatormdo = fatormdo;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Fatormdoitem) {
			Fatormdoitem that = (Fatormdoitem) obj;
			return this.getCdfatormdoitem().equals(that.getCdfatormdoitem());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdfatormdoitem != null) {
			return cdfatormdoitem.hashCode();
		}
		return super.hashCode();
	}
}
