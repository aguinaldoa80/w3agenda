package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_restricao",sequenceName="sq_restricao")
public class Restricao implements Log{

	protected Integer cdrestricao;
	protected Date dtrestricao;
	protected String motivorestricao;
	protected Date dtliberacao;
	protected String motivoliberacao;
	protected Pessoa pessoa;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer cdsituacaorestricao;
	protected Contrato contrato;
	
	@Id
	@GeneratedValue(generator="sq_restricao",strategy=GenerationType.AUTO)
	public Integer getCdrestricao() {
		return cdrestricao;
	}
	@Required
	@DisplayName("Data da restri��o")
	public Date getDtrestricao() {
		return dtrestricao;
	}
	@MaxLength(100)
	@DisplayName("Motivo da restri��o")
	public String getMotivorestricao() {
		return motivorestricao;
	}
	@DisplayName("Data da libera��o")
	public Date getDtliberacao() {
		return dtliberacao;
	}
	@MaxLength(100)
	@DisplayName("Motivo da libera��o")
	public String getMotivoliberacao() {
		return motivoliberacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	public Integer getCdsituacaorestricao() {
		return cdsituacaorestricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	
	public void setCdrestricao(Integer cdrestricao) {
		this.cdrestricao = cdrestricao;
	}
	public void setDtrestricao(Date dtrestricao) {
		this.dtrestricao = dtrestricao;
	}
	public void setMotivorestricao(String motivorestricao) {
		this.motivorestricao = motivorestricao;
	}
	public void setDtliberacao(Date dtliberacao) {
		this.dtliberacao = dtliberacao;
	}
	public void setMotivoliberacao(String motivoliberacao) {
		this.motivoliberacao = motivoliberacao;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setCdsituacaorestricao(Integer cdsituacaorestricao) {
		this.cdsituacaorestricao = cdsituacaorestricao;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
