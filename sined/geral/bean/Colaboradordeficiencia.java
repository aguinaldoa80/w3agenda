package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Colaboradordeficiencia{

	public static final Colaboradordeficiencia NAO_POSSUI = new Colaboradordeficiencia(0);
	public static final Colaboradordeficiencia FISICA = new Colaboradordeficiencia(1);
	public static final Colaboradordeficiencia AUDITIVA = new Colaboradordeficiencia(2);
	public static final Colaboradordeficiencia VISUAL = new Colaboradordeficiencia(3);
	public static final Colaboradordeficiencia MENTAL = new Colaboradordeficiencia(4);
	public static final Colaboradordeficiencia MULTIPLA = new Colaboradordeficiencia(5);
	public static final Colaboradordeficiencia REABILITADO = new Colaboradordeficiencia(6);
	
	protected Integer cdcolaboradordeficiencia;
	protected String nome;
	
	public Colaboradordeficiencia() {
	}
	public Colaboradordeficiencia(Integer cdcolaboradordeficiencia) {
		this.cdcolaboradordeficiencia = cdcolaboradordeficiencia;
	}
	
	@Id
	@DisplayName("Id")
	public Integer getCdcolaboradordeficiencia() {
		return cdcolaboradordeficiencia;
	}
	
	public void setCdcolaboradordeficiencia(Integer cdcolaboradordeficiencia) {
		this.cdcolaboradordeficiencia = cdcolaboradordeficiencia;
	}

	@Required
	@MaxLength(15)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Colaboradordeficiencia) {
			Colaboradordeficiencia cd = (Colaboradordeficiencia) obj;
			return this.getCdcolaboradordeficiencia().equals(cd.getCdcolaboradordeficiencia());
		}
		return super.equals(obj);
	}
}
