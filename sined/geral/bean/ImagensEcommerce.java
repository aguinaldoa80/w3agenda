package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_imagensecommerce", sequenceName = "sq_imagensecommerce")
public class ImagensEcommerce {
	private Integer cdimagensecommerce;
	private String hash;
	private String descricao;
	private Arquivo arquivo;
	private Material material;
	
	@Id
	@GeneratedValue(generator = "sq_imagensecommerce", strategy = GenerationType.AUTO)
	public Integer getCdimagensecommerce() {
		return cdimagensecommerce;
	}
	public String getHash() {
		return hash;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setCdimagensecommerce(Integer cdimagensecommerce) {
		this.cdimagensecommerce = cdimagensecommerce;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}