package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_oportunidadehistorico", sequenceName = "sq_oportunidadehistorico")
public class Oportunidadehistorico implements Log{
 
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdoportunidadehistorico;
	protected String observacao;
	protected Oportunidade oportunidade;
	protected Oportunidadesituacao oportunidadesituacao;
	protected Integer probabilidade; 
	protected Contacrmcontato contacrmcontato;
	protected Meiocontato meiocontato;
	protected Atividadetipo atividadetipo;
	protected Arquivo arquivo;
	protected Empresa empresahistorico;
	
	//TRANSIENTE
	protected Long cdarquivo;
	protected String nomeusuarioaltera;
	protected Boolean podeEditar = Boolean.TRUE;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidadehistorico")
	public Integer getCdoportunidadehistorico() {
		return cdoportunidadehistorico;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Oportunidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdoportunidadesituacao")
	public Oportunidadesituacao getOportunidadesituacao() {
		return oportunidadesituacao;
	}
	
	public Integer getProbabilidade() {
		return probabilidade;
	}
	
	@JoinColumn(name="cdcontacrmcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}

	@JoinColumn(name="cdmeiocontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}

	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	@JoinColumn(name="cdarquivo")
	@ManyToOne(fetch=FetchType.EAGER)
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@Required
	@DisplayName("Empresa")
	@JoinColumn(name="cdempresahistorico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}

	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}

	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}

	public void setCdoportunidadehistorico(Integer cdoportunidadehistorico) {
		this.cdoportunidadehistorico = cdoportunidadehistorico;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	public void setOportunidadesituacao(Oportunidadesituacao oportunidadesituacao) {
		this.oportunidadesituacao = oportunidadesituacao;
	}
	public void setProbabilidade(Integer probabilidade) {
		this.probabilidade = probabilidade;
	}
	
	
	
	@Transient
	public String getUsuarioaltera(){
		if(getCdusuarioaltera() != null){
			return TagFunctions.findUserByCd(getCdusuarioaltera());
		} else return null;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	
	@Transient
	public Long getCdarquivo() {
		return getArquivo() != null ? getArquivo().getCdarquivo() : null;
	}
	
	public void setCdarquivo(Long cdarquivo) {
		if(cdarquivo != null){
			this.arquivo = new Arquivo(cdarquivo);
		}
		this.cdarquivo = cdarquivo;
	}

	@Transient
	public String getNomeusuarioaltera() {
		return this.getUsuarioaltera();
	}

	public void setNomeusuarioaltera(String nomeusuarioaltera) {
		this.nomeusuarioaltera = nomeusuarioaltera;
	}
	
	@Transient
	public Boolean getPodeEditar() {
		if(this.getObservacao() != null && (this.getObservacao().contains("javascript:visualizarVenda") || this.getObservacao().contains("javascript:visualizarOrcamento"))){
			return false;
		}
		return podeEditar;
	}
	public void setPodeEditar(Boolean podeEditar) {
		this.podeEditar = podeEditar;
	}
	
	@Transient
	public String getLinkVendaOrcamento(){
		if(this.getObservacao() != null){
			if(this.getObservacao().contains("javascript:visualizarVenda") || this.getObservacao().contains("javascript:visualizarOrcamento")){
				try {
					Matcher matcher = Pattern.compile("javascript:(\\w+)\\((\\w+)\\)").matcher(this.getObservacao());
					if (matcher.find()){
						return matcher.group(0);
					}
				} catch (Exception e) {}
			}
		}
		return null;
	}
	
	@Transient
	public String getLinkCopiarOrcamento(){
		if(this.getObservacao() != null && this.getObservacao().contains("javascript:visualizarOrcamento")){
			try {
				Matcher matcher = Pattern.compile("javascript:(\\w+)\\((\\w+)\\)").matcher(this.getObservacao());
				if (matcher.find()){
					String s = matcher.group(0);
					return s.replace("visualizarOrcamento", "copiarOrcamento");
				}
			} catch (Exception e) {}
		}
		return null;
	}
}
