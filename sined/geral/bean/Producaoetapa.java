package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BaixarEstoqueMateriasPrimasEnum;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_producaoetapa", sequenceName = "sq_producaoetapa")
@DisplayName("Ciclo de Produ��o")
public class Producaoetapa implements Log{

	protected Integer cdproducaoetapa;
	protected String nome;
	protected Boolean corteproducao;
	protected Boolean naoagruparmaterial;
	protected Boolean naogerarentradaconclusao;
	protected List<Producaoetapaitem> listaProducaoetapaitem;
	protected BaixarEstoqueMateriasPrimasEnum baixarEstoqueMateriaprima;
	protected Boolean baixarmateriaprimacascata;
	protected Integer ordemprioridade;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	public Producaoetapa() {
	}
	
	public Producaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoetapa")
	public Integer getCdproducaoetapa() {
		return cdproducaoetapa;
	}
	@DescriptionProperty
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Apenas Corte na produ��o")
	public Boolean getCorteproducao() {
		return corteproducao;
	}
	@DisplayName("Itens da Etapa")
	@OneToMany(mappedBy="producaoetapa")
	public List<Producaoetapaitem> getListaProducaoetapaitem() {
		return listaProducaoetapaitem;
	}
	@DisplayName("Gerar uma ordem de produ��o para cada material")
	public Boolean getNaoagruparmaterial() {
		return naoagruparmaterial;
	}
	@DisplayName("N�o gerar entrada no estoque ao concluir ordem de produ��o")
	public Boolean getNaogerarentradaconclusao() {
		return naogerarentradaconclusao;
	}
	@Required
	@DisplayName("Baixar estoque das mat�rias-primas")
	public BaixarEstoqueMateriasPrimasEnum getBaixarEstoqueMateriaprima() {
		return baixarEstoqueMateriaprima;
	}
	@DisplayName("Baixar mat�rias-primas em cascata")
	public Boolean getBaixarmateriaprimacascata() {
		return baixarmateriaprimacascata;
	}
	@MaxLength(10)
	public Integer getOrdemprioridade() {
		return ordemprioridade;
	}

	public void setOrdemprioridade(Integer ordemprioridade) {
		this.ordemprioridade = ordemprioridade;
	}

	public void setNaoagruparmaterial(Boolean naoagruparmaterial) {
		this.naoagruparmaterial = naoagruparmaterial;
	}
	public void setNaogerarentradaconclusao(Boolean naogerarentradaconclusao) {
		this.naogerarentradaconclusao = naogerarentradaconclusao;
	}
	public void setCdproducaoetapa(Integer cdproducaoetapa) {
		this.cdproducaoetapa = cdproducaoetapa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCorteproducao(Boolean corteproducao) {
		this.corteproducao = corteproducao;
	}
	public void setListaProducaoetapaitem(List<Producaoetapaitem> listaProducaoetapaitem) {
		this.listaProducaoetapaitem = listaProducaoetapaitem;
	}
	public void setBaixarEstoqueMateriaprima(BaixarEstoqueMateriasPrimasEnum baixarEstoqueMateriaprima) {
		this.baixarEstoqueMateriaprima = baixarEstoqueMateriaprima;
	}
	public void setBaixarmateriaprimacascata(Boolean baixarmateriaprimacascata) {
		this.baixarmateriaprimacascata = baixarmateriaprimacascata;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Producaoetapa) {
			Producaoetapa p = (Producaoetapa) obj;
			return p.cdproducaoetapa.equals(this.cdproducaoetapa);
		}
		return super.equals(obj);
	}
}