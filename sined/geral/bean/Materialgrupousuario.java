package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialgrupousuario", sequenceName = "sq_materialgrupousuario")
@DisplayName("Compradores")
public class Materialgrupousuario {

	protected Integer cdmaterialgrupousuario;
	protected Materialgrupo materialgrupo;
	protected Usuario usuario;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialgrupousuario")	
	public Integer getCdmaterialgrupousuario() {
		return cdmaterialgrupousuario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	
	@Required
	@DisplayName("Comprador")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setCdmaterialgrupousuario(Integer cdmaterialgrupousuario) {
		this.cdmaterialgrupousuario = cdmaterialgrupousuario;
	}
	
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}	
}
