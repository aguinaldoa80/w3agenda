package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_movimentacaohistorico",sequenceName="sq_movimentacaohistorico")
public class Movimentacaohistorico implements Log{
	
	protected Integer cdmovimentacaohistorico;
	protected Movimentacao movimentacao;
	protected Movimentacaoacao movimentacaoacao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Movimentacao> listaMovimentacaoTransferencia;
	
	public Movimentacaohistorico() {}
	
	
	
	public Movimentacaohistorico(Integer cdmovimentacaohistorico,
			Movimentacao movimentacao, Movimentacaoacao movimentacaoacao,
			String observacao, Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdmovimentacaohistorico = cdmovimentacaohistorico;
		this.movimentacao = movimentacao;
		this.movimentacaoacao = movimentacaoacao;
		this.observacao = observacao;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}



	@Id
	@GeneratedValue(generator="sq_movimentacaohistorico",strategy=GenerationType.AUTO)
	public Integer getCdmovimentacaohistorico() {
		return cdmovimentacaohistorico;
	}
	@DisplayName("Movimenta��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacao")
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	@DisplayName("A��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoacao")
	public Movimentacaoacao getMovimentacaoacao() {
		return movimentacaoacao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel(){
		if(this.cdusuarioaltera != null){
			return TagFunctions.findUserByCd(cdusuarioaltera); 
		}else return null;
	}
	@Transient
	public List<Movimentacao> getListaMovimentacaoTransferencia() {
		return listaMovimentacaoTransferencia;
	}
	
	
	public void setCdmovimentacaohistorico(Integer cdmovimentacaohistorico) {
		this.cdmovimentacaohistorico = cdmovimentacaohistorico;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setMovimentacaoacao(Movimentacaoacao movimentacaoacao) {
		this.movimentacaoacao = movimentacaoacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaMovimentacaoTransferencia(List<Movimentacao> listaMovimentacaoTransferencia) {
		this.listaMovimentacaoTransferencia = listaMovimentacaoTransferencia;
	}
}
