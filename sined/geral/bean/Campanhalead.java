package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_campanhalead", sequenceName = "sq_campanhalead")
public class Campanhalead implements Log{
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdcampanhalead;
	protected Campanha campanha;
	protected Lead lead;
	
	//Transients
	
	protected String leadEmpresa;
	protected String leadResponsavel;
	protected String leadListaEmails;
	protected String leadSituacao;
	protected String leadUltimoHistorico;
	protected Integer leadId;
	
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campanhalead")
	public Integer getCdcampanhalead() {
		return cdcampanhalead;
	}
	
	@DisplayName("Campanha")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampanha")
	public Campanha getCampanha() {
		return campanha;
	}

	@DisplayName("Lead")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlead")
	public Lead getLead() {
		return lead;
	}
	
	

	public void setCdcampanhalead(Integer cdcampanhalead) {
		this.cdcampanhalead = cdcampanhalead;
	}

	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
	
	

	@Transient
	@DisplayName("Empresa")
	public String getLeadEmpresa() {
		return leadEmpresa;
	}

	@Transient
	@DisplayName("Respons�vel")
	public String getLeadResponsavel() {
		return leadResponsavel;
	}

	@Transient
	@DisplayName("E-mails")
	public String getLeadListaEmails() {
		return leadListaEmails;
	}
	
	@Transient
	@DisplayName("Situa��o")
	public String getLeadSituacao() {
		return leadSituacao;
	}	

	@Transient
	@DisplayName("�ltimo Hist�rico")
	public String getLeadUltimoHistorico() {		
		return leadUltimoHistorico;
	}

	@Transient
	public Integer getLeadId() {
		return leadId;
	}

	public void setLeadEmpresa(String leadEmpresa) {
		this.leadEmpresa = leadEmpresa;
	}

	public void setLeadResponsavel(String leadResponsavel) {
		this.leadResponsavel = leadResponsavel;
	}

	public void setLeadListaEmails(String leadListaEmails) {
		this.leadListaEmails = leadListaEmails;
	}
	
	public void setLeadSituacao(String leadSituacao) {
		this.leadSituacao = leadSituacao;
	}

	public void setLeadUltimoHistorico(String leadUltimoHistorico) {
		this.leadUltimoHistorico = leadUltimoHistorico;
	}
	
	public void setLeadId(Integer leadId) {
		this.leadId = leadId;
	}
	
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
