package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_colaboradorcomissao", sequenceName = "sq_colaboradorcomissao")
public class Colaboradorcomissao implements Log{
	
	protected Integer cdcolaboradorcomissao;
	protected Colaborador colaborador;
	protected Venda venda;
	protected Documentoorigem documentoorigem; // conta a receber
	protected Documento documento;// conta a pagar
	protected NotaDocumento notaDocumento;
	protected Integer cdusuarioaltera;	
	protected Timestamp dtaltera;
	protected Boolean desempenho;
//	protected List<Documentocomissao> listaDocumentocomissao = new ListSet<Documentocomissao>(Documentocomissao.class);
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcomissao")
	public Integer getCdcolaboradorcomissao() {
		return cdcolaboradorcomissao;
	}

	@Required
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@JoinColumn(name="cdvenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Venda getVenda() {
		return venda;
	}

	@JoinColumn(name="cddocumentoorigem")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentoorigem getDocumentoorigem() {
		return documentoorigem;
	}

	@Required
	@JoinColumn(name="cddocumento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documento getDocumento() {
		return documento;
	}
	
	@JoinColumn(name="cdnotadocumento")
	@ManyToOne(fetch=FetchType.LAZY)
	public NotaDocumento getNotaDocumento() {
		return notaDocumento;
	}
/*	
	@OneToMany(mappedBy="cddocumentocomissao")
	public List<Documentocomissao> getListaDocumentocomissao() {
		return listaDocumentocomissao;
	}*/
	
	@DisplayName("Desempenho")
	public Boolean getDesempenho() {
		return desempenho;
	}

	public void setCdcolaboradorcomissao(Integer cdcolaboradorcomissao) {
		this.cdcolaboradorcomissao = cdcolaboradorcomissao;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setDocumentoorigem(Documentoorigem documentoorigem) {
		this.documentoorigem = documentoorigem;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setNotaDocumento(NotaDocumento notaDocumento) {
		this.notaDocumento = notaDocumento;
	}
	
	/*public void setListaDocumentocomissao(List<Documentocomissao> listaDocumentocomissao) {
		this.listaDocumentocomissao = listaDocumentocomissao;
	}*/
	
	public void setDesempenho(Boolean desempenho) {
		this.desempenho = desempenho;
	}

	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
