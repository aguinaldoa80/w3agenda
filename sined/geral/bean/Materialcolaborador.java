package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialcolaborador", sequenceName = "sq_materialcolaborador")
@DisplayName("Material colaborador")
public class Materialcolaborador implements Log {

	protected Integer cdmaterialcolaborador;
	protected Colaborador colaborador;
	protected Material material;
	protected Double percentual;
	protected Money valor;
	protected Boolean ativo;
	protected Documentotipo documentotipo;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialcolaborador")
	public Integer getCdmaterialcolaborador() {
		return cdmaterialcolaborador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@MaxLength(9)
	public Double getPercentual() {
		return percentual;
	}
	@MaxLength(9)
	public Money getValor() {
		return valor;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	
	public void setCdmaterialcolaborador(Integer cdmaterialcolaborador) {
		this.cdmaterialcolaborador = cdmaterialcolaborador;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@DisplayName("Forma de Pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
}
