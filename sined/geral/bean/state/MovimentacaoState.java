package br.com.linkcom.sined.geral.bean.state;

import br.com.linkcom.sined.geral.bean.Movimentacao;

/**
 * <p>Esta classe abstrata � a implementa��o do Design-Pattern State.
 * <p>Seu papel � executar as transi��es de estado do caso de uso Manter Movimenta��o Financeira.
 * <p>As opera��es de transi��o devem ser implementadas nas subclasses concretas de MovimentacaoState.
 * 
 * @author Fl�vio Tavares
 */
abstract public class MovimentacaoState {

	protected Movimentacao movimentacao;
	
	public MovimentacaoState(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	
	/**
	 * Executa o procedimento para cancelar uma movimenta��o.
	 *
	 * @return true - se a movimenta��o foi cancelada
	 * 			false - se a movimenta��o n�o pode ser cancelada
	 * @author Fl�vio Tavares
	 */
	public abstract boolean cancelar();
	
	/**
	 * Executa o procedimento para conciliar uma movimenta��o.
	 * 
	 * @return true - se a movimenta��o foi conciliada
	 * 			false - se a movimenta��o n�o pode ser conciliada
	 * @author Fl�vio Tavares
	 */
	public abstract boolean conciliar();
	
	/**
	 * Executa o procedimento para conciliar uma movimenta��o.
	 * 
	 * @return true - se a movimenta��o foi estornada
	 * 			false - se a movimenta��o n�o pode ser estornada
	 * @author Fl�vio Tavares
	 */
	public abstract boolean estornar();
}
