package br.com.linkcom.sined.geral.bean.state.movimentacao;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.state.MovimentacaoState;

public class Conciliada extends MovimentacaoState {

	public Conciliada(Movimentacao movimentacao) {
		super(movimentacao);
	}

	@Override
	public boolean cancelar() {
		// N�o se pode cancelar uma movimenta��o conciliada.
		return false;
	}

	@Override
	public boolean conciliar() {
		// N�o se pode conciliar uma movimenta��o conciliada.
		return false;
	}

	/*
	 * M�todo altera o estado da movimenta��o conciliada para Normal.
	 * 
	 * @author Fl�vio Tavares
	 */
	@Override
	public boolean estornar() {
		movimentacao.alterarEstado(movimentacao.getNormal());
		movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		return true;
	}

}
