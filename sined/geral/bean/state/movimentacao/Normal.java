package br.com.linkcom.sined.geral.bean.state.movimentacao;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.state.MovimentacaoState;

public class Normal extends MovimentacaoState {

	public Normal(Movimentacao movimentacao) {
		super(movimentacao);
	}

	/*
	 * M�todo altera o estado da movimenta��o Normal para Cancelada.
	 * 
	 * @author Fl�vio Tavares
	 */
	@Override
	public boolean cancelar() {
		movimentacao.alterarEstado(movimentacao.getCancelada());
		movimentacao.setMovimentacaoacao(Movimentacaoacao.CANCELADA);
		return true;
	}

	/*
	 * M�todo altera o estado da movimenta��o Normal para Conciliada.
	 * 
	 * @author Fl�vio Tavares
	 */
	@Override
	public boolean conciliar() {
		movimentacao.alterarEstado(movimentacao.getConciliada());
		movimentacao.setMovimentacaoacao(Movimentacaoacao.CONCILIADA);
		return true;
	}

	@Override
	public boolean estornar() {
		// N�o se pode estornar uma movimenta��o normal.
		return false;
	}

}
