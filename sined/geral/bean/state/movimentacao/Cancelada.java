package br.com.linkcom.sined.geral.bean.state.movimentacao;

import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.state.MovimentacaoState;

public class Cancelada extends MovimentacaoState {

	public Cancelada(Movimentacao movimentacao) {
		super(movimentacao);
	}

	@Override
	public boolean cancelar() {
		// N�o � poss�vel conciliaar uma movimenta��o cancelada
		return false;
	}

	@Override
	public boolean conciliar() {
		// N�o � poss�vel conciliaar uma movimenta��o cancelada
		return false;
	}

	/**
	 * M�todo para estornar uma movimenta��o cancelada. Para isto � verificado se todos
	 * os documentos da origem da movimenta�� est� com situa��o Autorizada, se for conta a pagar, 
	 * ou Definitiva, se for conta a receber.
	 * 
	 * @see br.com.linkcom.sined.geral.bean.Movimentacao#isOrigensAutorizadasDefinitivas()
	 * @author Fl�vio Tavares
	 */
	@Override
	public boolean estornar() {
		if(!movimentacao.isOrigensAutorizadasDefinitivas()){
			return false;
		}
		movimentacao.alterarEstado(movimentacao.getNormal());
		movimentacao.setMovimentacaoacao(Movimentacaoacao.NORMAL);
		return true;
	}

}
