package br.com.linkcom.sined.geral.bean.state;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class AgendamentoAcao {

	public static final Integer CRIADO = 1;
	public static final Integer CONSOLIDADO = 2;
	public static final Integer ALTERADO = 3;
	public static final Integer FINALIZADO = 4;
	public static final Integer CANCEL_MOVIMENTACAO = 104;
	public static final Integer ESTORNADO = 5;
	
	protected Integer cdAgendamentoAcao;
	protected String nome;
	
	public AgendamentoAcao(){}
	
	public AgendamentoAcao(Integer cdAgendamentoAcao){
		this.cdAgendamentoAcao = cdAgendamentoAcao;
	}
	
	@Id
	public Integer getCdAgendamentoAcao() {
		return cdAgendamentoAcao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdAgendamentoAcao(Integer cdAgendamentoAcao) {
		this.cdAgendamentoAcao = cdAgendamentoAcao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
