package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Ausenciacolaboradorhistoricoacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_ausenciacolaboradorhistorico", sequenceName = "sq_ausenciacolaboradorhistorico")
public class Ausenciacolaboradorhistorico implements Log{
	
	protected Integer cdausenciacolaboradorhistorico;
	protected Ausenciacolaborador ausenciacolaborador;
	protected Ausenciacolaboradorhistoricoacao acao;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ausenciacolaboradorhistorico")
	public Integer getCdausenciacolaboradorhistorico() {
		return cdausenciacolaboradorhistorico;
	}
	@DisplayName("Ausencia Colaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdausenciacolaborador")
	public Ausenciacolaborador getCdausenciacolaborador() {
		return ausenciacolaborador;
	}
	@DisplayName("A��o")
	public Ausenciacolaboradorhistoricoacao getAcao() {
		return acao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdausenciacolaboradorhistorico(Integer cdausenciacolaboradorhistorico) {
		this.cdausenciacolaboradorhistorico = cdausenciacolaboradorhistorico;
	}
	public void setCdausenciacolaborador(Ausenciacolaborador ausenciacolaborador) {
		this.ausenciacolaborador = ausenciacolaborador;
	}
	public void setAcao(Ausenciacolaboradorhistoricoacao acao) {
		this.acao = acao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
