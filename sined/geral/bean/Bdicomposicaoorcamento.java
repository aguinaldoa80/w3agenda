package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_bdicomposicaoorcamento",sequenceName="sq_bdicomposicaoorcamento")
public class Bdicomposicaoorcamento implements Log {

	protected Integer cdbdicomposicaoorcamento;
	protected Bdi bdi;
	protected Composicaoorcamento composicaoorcamento;	
	protected Boolean inclui;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_bdicomposicaoorcamento", strategy=GenerationType.AUTO)	
	public Integer getCdbdicomposicaoorcamento() {
		return cdbdicomposicaoorcamento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbdi")
	@DisplayName("Bdi")
	public Bdi getBdi() {
		return bdi;
	}
	
	@DisplayName("Composi��o do or�amento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomposicaoorcamento")
	@Required
	public Composicaoorcamento getComposicaoorcamento() {
		return composicaoorcamento;
	}

	@Required
	public Boolean getInclui() {
		return inclui;
	}	

	public void setCdbdicomposicaoorcamento(Integer cdbdicomposicaoorcamento) {
		this.cdbdicomposicaoorcamento = cdbdicomposicaoorcamento;
	}

	public void setBdi(Bdi bdi) {
		this.bdi = bdi;
	}

	public void setComposicaoorcamento(Composicaoorcamento composicaoorcamento) {
		this.composicaoorcamento = composicaoorcamento;
	}

	public void setInclui(Boolean inclui) {
		this.inclui = inclui;
	}

	/*************/
	/**** Log ****/
	/*************/
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bdicomposicaoorcamento) {
			Bdicomposicaoorcamento that = (Bdicomposicaoorcamento) obj;
			return this.getCdbdicomposicaoorcamento().equals(that.getCdbdicomposicaoorcamento());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdbdicomposicaoorcamento != null) {
			return cdbdicomposicaoorcamento.hashCode();
		}
		return super.hashCode();		
	}

	
}
