package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_lotecontabil", sequenceName="sq_lotecontabil")
public class LoteContabil {
	private Integer cdLoteContabil;
	
	@Id
	@DisplayName("ID do Lote Cont�bil")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_lotecontabil")
	public Integer getCdLoteContabil() {
		return cdLoteContabil;
	}
	
	public void setCdLoteContabil(Integer cdLoteContabil) {
		this.cdLoteContabil = cdLoteContabil;
	}
}
