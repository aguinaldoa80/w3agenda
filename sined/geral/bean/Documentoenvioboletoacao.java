package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Documentoenvioboletoacao {
	CRIADO(0, "Cria��o"),
	ALTERADO(1, "Altera��o"),
	CANCELADO(2, "Cancelamento de agendamento"),
	CONCLUIDO(3, "Conclu�do");
	
	private Integer value;
	private String nome;
	
	
	private Documentoenvioboletoacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	

	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
