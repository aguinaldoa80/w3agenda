package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;

@Entity
@SequenceGenerator(name = "sq_expedicaoitem", sequenceName = "sq_expedicaoitem")
public class Expedicaoitem implements ValidateLoteestoqueObrigatorioInterface{
	
	protected Integer cdexpedicaoitem;
	protected Expedicao expedicao;
	protected Vendamaterial vendamaterial;
	protected Cliente cliente;
	protected Material material;
	protected Localarmazenagem localarmazenagem;
	protected Date dtexpedicaoitem;
	protected Endereco enderecocliente;
	protected Contato contatocliente;
	protected Double qtdeexpedicao;
	protected Boolean faturada;
	protected Integer ordementrega;
	protected Unidademedida unidademedida;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected String observacao;
	
	protected List<Expedicaoiteminspecao> listaInspecao;
	
	protected ColetaMaterial coletaMaterial;
	protected Notafiscalprodutoitem notaFiscalProdutoItem;
	protected Pneu pneu;
	
	private Loteestoque loteEstoque;
	
	// TRANSIENTE
	protected Integer codigovenda;
	protected Double qtdevendida;
	protected Double qtderestante;
	protected Endereco enderecoclienteSelecionado;
	protected Contato contatoclienteSelecionado;
	protected String unidadeMedidaStr;
	protected Double quantidadePrincipal;
	protected List<Expedicaoitem> listaMaterialitemmestregrade;
	protected Material materialmestregrade;
	protected Unidademedida unidademedidaAntiga;
	protected boolean podeBaixarEstoque = false;
	protected Boolean entregaFutura;
	protected Empresa empresa;
	protected String codigoBarrasEmbalagem;
	protected Boolean unidadesMedidaDiferentesConferenciaExpedicao;
	protected Boolean unidadeMedidaPrincipalMaterial;	
	protected String validadeMaterialLote;
	protected Boolean ultimoitem;
	
	public Expedicaoitem() {}
	
	public Expedicaoitem(Integer cdexpedicaoitem) {
		this.cdexpedicaoitem = cdexpedicaoitem;
	}

	public Expedicaoitem(Expedicaoitem item) {
		if(item != null){
			this.expedicao = item.getExpedicao();
			this.vendamaterial = item.getVendamaterial();
			this.cliente = item.getCliente();
			this.material = item.getMaterial();
			this.localarmazenagem = item.getLocalarmazenagem();
			this.dtexpedicaoitem = item.getDtexpedicaoitem();
			this.enderecocliente = item.getEnderecocliente();
			this.contatocliente = item.getContatocliente();
			this.qtdeexpedicao = item.getQtdeexpedicao();
			this.faturada = item.getFaturada();
			this.ordementrega = item.getOrdementrega();
			this.unidademedida = item.getUnidademedida();
			this.listaInspecao = item.getListaInspecao();
			this.loteEstoque = item.getLoteEstoque();
		}
	}
	/////////////////////////////////////////|
	//  INICIO DOS GET 				/////////|
	/////////////////////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicaoitem")
	public Integer getCdexpedicaoitem() {
		return cdexpedicaoitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@Required
	@DisplayName("Produto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Data de expedi��o")
	public Date getDtexpedicaoitem() {
		return dtexpedicaoitem;
	}

	@DisplayName("Local de expedi��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecocliente")
	public Endereco getEnderecocliente() {
		return enderecocliente;
	}

	@DisplayName("Respons�vel pela retirada")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontatocliente")
	public Contato getContatocliente() {
		return contatocliente;
	}

	@Required
	@DisplayName("Qtde. para expedi��o")
	public Double getQtdeexpedicao() {
		return qtdeexpedicao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@DisplayName("Ordem de Entrega")
	public Integer getOrdementrega() {
		return ordementrega;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@DisplayName("Unidade de Medida")
	@Required
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public Double getFatorconversao() {
		return fatorconversao;
	}

	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoletamaterial")
	public ColetaMaterial getColetaMaterial() {
		return coletaMaterial;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneu")
	public Pneu getPneu() {
		return pneu;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotaFiscalProdutoItem() {
		return notaFiscalProdutoItem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteEstoque() {
		return loteEstoque;
	}
	
	@Transient
	public Loteestoque getLoteestoque() {//Criado esse transient apenas para atender � interface ValidateLoteestoqueObrigatorioInterface
		return getLoteEstoque();
	}
	
	/////////////////////////////////////////|
	//  INICIO DOS SET 				/////////|
	/////////////////////////////////////////|
	
	public void setLoteEstoque(Loteestoque loteEstoque) {
		this.loteEstoque = loteEstoque;
	}
	
	public void setColetaMaterial(ColetaMaterial coletaMaterial) {
		this.coletaMaterial = coletaMaterial;
	}
	public void setNotaFiscalProdutoItem(Notafiscalprodutoitem notaFiscalProdutoItem) {
		this.notaFiscalProdutoItem = notaFiscalProdutoItem;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setOrdementrega(Integer ordementrega) {
		this.ordementrega = ordementrega;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public Boolean getFaturada() {
		return faturada;
	}
	
	public void setFaturada(Boolean faturada) {
		this.faturada = faturada;
	}

	public void setCdexpedicaoitem(Integer cdexpedicaoitem) {
		this.cdexpedicaoitem = cdexpedicaoitem;
	}

	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}

	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setDtexpedicaoitem(Date dtexpedicaoitem) {
		this.dtexpedicaoitem = dtexpedicaoitem;
	}

	public void setEnderecocliente(Endereco enderecocliente) {
		this.enderecocliente = enderecocliente;
	}

	public void setContatocliente(Contato contatocliente) {
		this.contatocliente = contatocliente;
	}

	public void setQtdeexpedicao(Double qtdeexpedicao) {
		this.qtdeexpedicao = qtdeexpedicao;
	}	
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	//////////////////////////////////////////////|
	//  INICIO DOS TRANSIENTES	E OUTROS /////////|
	//////////////////////////////////////////////|	
	
	// TRANSIENTES

	@Transient
	@DisplayName("C�digo da venda")
	public Integer getCodigovenda() {
		if(this.getVendamaterial() != null && this.getVendamaterial().getVenda() != null && this.getVendamaterial().getVenda().getCdvenda() != null){
			return this.getVendamaterial().getVenda().getCdvenda();
		}
		return codigovenda;
	}

	@Transient
	@DisplayName("Qtde. vendida")
	public Double getQtdevendida() {
		if(this.getVendamaterial() != null && this.getVendamaterial().getQuantidade() != null){
			return this.getVendamaterial().getQuantidade();
		}
		return qtdevendida;
	}

	@Transient
	@DisplayName("Qtde. restante")
	public Double getQtderestante() {
		return qtderestante;
	}

	public void setCodigovenda(Integer codigovenda) {
		this.codigovenda = codigovenda;
	}
	public void setQtdevendida(Double qtdevendida) {
		this.qtdevendida = qtdevendida;
	}

	public void setQtderestante(Double qtderestante) {
		this.qtderestante = qtderestante;
	}
	
	@Transient
	public Endereco getEnderecoclienteSelecionado() {
		if(this.enderecocliente != null){
			return this.enderecocliente;
		}
		return enderecoclienteSelecionado;
	}
	
	@Transient
	public Contato getContatoclienteSelecionado() {
		if(this.contatocliente != null){
			return this.contatocliente;
		}
		return contatoclienteSelecionado;
	}
	
	public void setContatoclienteSelecionado(Contato contatoclienteSelecionado) {
		this.contatoclienteSelecionado = contatoclienteSelecionado;
	}
	
	public void setEnderecoclienteSelecionado(Endereco enderecoclienteSelecionado) {
		this.enderecoclienteSelecionado = enderecoclienteSelecionado;
	}
	
	@Transient
	@DisplayName("Unidade de Medida")
	public String getUnidadeMedidaStr() {
		return unidadeMedidaStr;
	}
	
	public void setUnidadeMedidaStr(String unidadeMedidaStr) {
		this.unidadeMedidaStr = unidadeMedidaStr;
	}
	
	@DisplayName("Item inspe��o")
	@OneToMany(mappedBy="expedicaoitem")
	public List<Expedicaoiteminspecao> getListaInspecao() {
		return listaInspecao;
	}
	public void setListaInspecao(List<Expedicaoiteminspecao> listaInspecao) {
		this.listaInspecao = listaInspecao;
	}

	@Transient
	public Double getQuantidadePrincipal() {
		return quantidadePrincipal;
	}
	@Transient
	public List<Expedicaoitem> getListaMaterialitemmestregrade() {
		return listaMaterialitemmestregrade;
	}
	@Transient
	public Material getMaterialmestregrade() {
		return materialmestregrade;
	}

	public void setMaterialmestregrade(Material materialmestregrade) {
		this.materialmestregrade = materialmestregrade;
	}

	public void setListaMaterialitemmestregrade(List<Expedicaoitem> listaMaterialitemmestregrade) {
		this.listaMaterialitemmestregrade = listaMaterialitemmestregrade;
	}
	public void setQuantidadePrincipal(Double quantidadePrincipal) {
		this.quantidadePrincipal = quantidadePrincipal;
	}
	
	@Transient
	public String getMaterialDescricao(){
		return getMaterial() != null ? getMaterial().getNome() : "";
	}
	
	@Transient
	public String getDescricaoQtde(){
		String descricaoqtde = "Quantidade";
		if(getUnidademedida() != null && getUnidademedida().getSimbolo() != null &&
				!"".equals(getUnidademedida().getSimbolo())){
			descricaoqtde += " (" + getUnidademedida().getSimbolo() + ")";
		}
		descricaoqtde += ": " + (getQtdeexpedicao() != null ? getQtdeexpedicao() : "");
		return descricaoqtde;
	}
	
	@Transient
	public Unidademedida getUnidademedidaAntiga() {
		return unidademedidaAntiga;
	}
	
	public void setUnidademedidaAntiga(Unidademedida unidademedidaAntiga) {
		this.unidademedidaAntiga = unidademedidaAntiga;
	}
	
	@Transient
	public boolean isPodeBaixarEstoque() {
		return podeBaixarEstoque;
	}
	
	public void setPodeBaixarEstoque(boolean podeBaixarEstoque) {
		this.podeBaixarEstoque = podeBaixarEstoque;
	}
	
	@Transient
	public Boolean getEntregaFutura() {
		return entregaFutura;
	}
	
	public void setEntregaFutura(Boolean entregaFutura) {
		this.entregaFutura = entregaFutura;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public String getCodigoBarrasEmbalagem() {
		return codigoBarrasEmbalagem;
	}
	
	public void setCodigoBarrasEmbalagem(String codigoBarrasEmbalagem) {
		this.codigoBarrasEmbalagem = codigoBarrasEmbalagem;
	}
	
	@Transient
	public Boolean getUnidadesMedidaDiferentesConferenciaExpedicao() {
		return unidadesMedidaDiferentesConferenciaExpedicao;
	}
	
	public void setUnidadesMedidaDiferentesConferenciaExpedicao(
			Boolean unidadesMedidaDiferentesConferenciaExpedicao) {
		this.unidadesMedidaDiferentesConferenciaExpedicao = unidadesMedidaDiferentesConferenciaExpedicao;
	}
	
	@Transient
	public Boolean getUnidadeMedidaPrincipalMaterial() {
		return unidadeMedidaPrincipalMaterial;
	}
	
	public void setUnidadeMedidaPrincipalMaterial(
			Boolean unidadeMedidaPrincipalMaterial) {
		this.unidadeMedidaPrincipalMaterial = unidadeMedidaPrincipalMaterial;
	}

	@Transient
	public String getValidadeMaterialLote() {
		return validadeMaterialLote;
	}

	public void setValidadeMaterialLote(String validadeMaterialLote) {
		this.validadeMaterialLote = validadeMaterialLote;
	}

	@Transient
	public Boolean getUltimoitem() {
		return ultimoitem;
	}

	public void setUltimoitem(Boolean ultimoitem) {
		this.ultimoitem = ultimoitem;
	}
}