package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_usuariopapel", sequenceName = "sq_usuariopapel")
public class Usuariopapel implements Log{

	protected Integer cdusuariopapel;
	protected Usuario pessoa;
	protected Papel papel;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_usuariopapel")
	public Integer getCdusuariopapel() {
		return cdusuariopapel;
	}
	public void setCdusuariopapel(Integer id) {
		this.cdusuariopapel = id;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Usuario getPessoa() {
		return pessoa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	@Required
	public Papel getPapel() {
		return papel;
	}

	
	public void setPessoa(Usuario pessoa) {
		this.pessoa = pessoa;
	}
	
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public Integer getCdusuarioaltera() {
		return  cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera =  cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
