package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessa",sequenceName="sq_bancoconfiguracaoremessa")
public class BancoConfiguracaoRemessa implements Log {

	private Integer cdbancoconfiguracaoremessa;
	private String nome;
	private Banco banco;
	private BancoConfiguracaoRemessaTipoEnum tipo;
	private Integer tamanho;
	private Boolean utilizaTabelaAuxiliar1;
	private Boolean utilizaTabelaAuxiliar2;
	private String nomeTabelaAuxiliar1;
	private String nomeTabelaAuxiliar2;
	private Boolean considerarcontasituacao;
	private Boolean empresaobrigatoria = Boolean.TRUE;
	protected List<BancoConfiguracaoRemessaDocumentoacao> listaBancoConfiguracaoRemessaDocumentoacao;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	private BancoTipoPagamento bancoTipoPagamento;
	private BancoFormapagamento bancoFormapagamento;
	
	private List<BancoConfiguracaoRemessaSegmento> listaBancoConfiguracaoRemessaSegmento = new ListSet<BancoConfiguracaoRemessaSegmento>(BancoConfiguracaoRemessaSegmento.class);
	private List<BancoConfiguracaoRemessaInstrucao> listaBancoConfiguracaoRemessaInstrucao = new ListSet<BancoConfiguracaoRemessaInstrucao>(BancoConfiguracaoRemessaInstrucao.class);
	private List<BancoConfiguracaoRemessaTipoPagamento> listaBancoConfiguracaoRemessaTipoPagamento = new ListSet<BancoConfiguracaoRemessaTipoPagamento>(BancoConfiguracaoRemessaTipoPagamento.class);
	private List<BancoConfiguracaoRemessaFormaPagamento> listaBancoConfiguracaoRemessaFormaPagamento = new ListSet<BancoConfiguracaoRemessaFormaPagamento>(BancoConfiguracaoRemessaFormaPagamento.class);

	private List<BancoConfiguracaoRemessaAuxiliar> listaBancoConfiguracaoRemessaAuxiliar1 = new ListSet<BancoConfiguracaoRemessaAuxiliar>(BancoConfiguracaoRemessaAuxiliar.class);
	private List<BancoConfiguracaoRemessaAuxiliar> listaBancoConfiguracaoRemessaAuxiliar2 = new ListSet<BancoConfiguracaoRemessaAuxiliar>(BancoConfiguracaoRemessaAuxiliar.class);
	
	private Set<Contacarteira> listaContacarteira = new ListSet<Contacarteira>(Contacarteira.class);
	
	//TRANSIENT
	protected List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessa")
	public Integer getCdbancoconfiguracaoremessa() {
		return cdbancoconfiguracaoremessa;
	}

	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	@Required
	public Banco getBanco() {
		return banco;
	}
	
	@Required
	public BancoConfiguracaoRemessaTipoEnum getTipo() {
		return tipo;
	}
	
	@MaxLength(9)
	@Required
	public Integer getTamanho() {
		return tamanho;
	}
	
	@DisplayName("Utiliza tabela auxiliar 1")
	public Boolean getUtilizaTabelaAuxiliar1() {
		return utilizaTabelaAuxiliar1;
	}
	
	@DisplayName("Utiliza tabela auxiliar 2")
	public Boolean getUtilizaTabelaAuxiliar2() {
		return utilizaTabelaAuxiliar2;
	}
	
	@DisplayName("Nome da tabela auxiliar 1")
	public String getNomeTabelaAuxiliar1() {
		return nomeTabelaAuxiliar1;
	}
	
	@DisplayName("Nome da tabela auxiliar 2")
	public String getNomeTabelaAuxiliar2() {
		return nomeTabelaAuxiliar2;
	}

	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public List<BancoConfiguracaoRemessaSegmento> getListaBancoConfiguracaoRemessaSegmento() {
		return listaBancoConfiguracaoRemessaSegmento;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public List<BancoConfiguracaoRemessaInstrucao> getListaBancoConfiguracaoRemessaInstrucao() {
		return listaBancoConfiguracaoRemessaInstrucao;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public List<BancoConfiguracaoRemessaTipoPagamento> getListaBancoConfiguracaoRemessaTipoPagamento() {
		return listaBancoConfiguracaoRemessaTipoPagamento;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public List<BancoConfiguracaoRemessaFormaPagamento> getListaBancoConfiguracaoRemessaFormaPagamento() {
		return listaBancoConfiguracaoRemessaFormaPagamento;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa1")
	public List<BancoConfiguracaoRemessaAuxiliar> getListaBancoConfiguracaoRemessaAuxiliar1() {
		return listaBancoConfiguracaoRemessaAuxiliar1;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa2")
	public List<BancoConfiguracaoRemessaAuxiliar> getListaBancoConfiguracaoRemessaAuxiliar2() {
		return listaBancoConfiguracaoRemessaAuxiliar2;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public Set<Contacarteira> getListaContacarteira() {
		return listaContacarteira;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoformapagamento")
	@DisplayName("Forma de pagamento")
	public BancoFormapagamento getBancoFormapagamento() {
		return bancoFormapagamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancotipopagamento")
	@DisplayName("Tipo de pagamento")
	public BancoTipoPagamento getBancoTipoPagamento() {
		return bancoTipoPagamento;
	}
	
	public void setCdbancoconfiguracaoremessa(Integer cdbancoconfiguracaoremessa) {
		this.cdbancoconfiguracaoremessa = cdbancoconfiguracaoremessa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	public void setTipo(BancoConfiguracaoRemessaTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public void setUtilizaTabelaAuxiliar1(Boolean utilizaTabelaAuxiliar1) {
		this.utilizaTabelaAuxiliar1 = utilizaTabelaAuxiliar1;
	}
	
	public void setUtilizaTabelaAuxiliar2(Boolean utilizaTabelaAuxiliar2) {
		this.utilizaTabelaAuxiliar2 = utilizaTabelaAuxiliar2;
	}
	
	public void setNomeTabelaAuxiliar1(String nomeTabelaAuxiliar1) {
		this.nomeTabelaAuxiliar1 = nomeTabelaAuxiliar1;
	}
	
	public void setNomeTabelaAuxiliar2(String nomeTabelaAuxiliar2) {
		this.nomeTabelaAuxiliar2 = nomeTabelaAuxiliar2;
	}
	
	public void setListaBancoConfiguracaoRemessaSegmento(
			List<BancoConfiguracaoRemessaSegmento> listaBancoConfiguracaoRemessaSegmento) {
		this.listaBancoConfiguracaoRemessaSegmento = listaBancoConfiguracaoRemessaSegmento;
	}

	public void setListaBancoConfiguracaoRemessaInstrucao(
			List<BancoConfiguracaoRemessaInstrucao> listaBancoConfiguracaoRemessaInstrucao) {
		this.listaBancoConfiguracaoRemessaInstrucao = listaBancoConfiguracaoRemessaInstrucao;
	}
	
	public void setListaBancoConfiguracaoRemessaTipoPagamento(
			List<BancoConfiguracaoRemessaTipoPagamento> listaBancoConfiguracaoRemessaTipoPagamento) {
		this.listaBancoConfiguracaoRemessaTipoPagamento = listaBancoConfiguracaoRemessaTipoPagamento;
	}
	
	public void setListaBancoConfiguracaoRemessaFormaPagamento(
			List<BancoConfiguracaoRemessaFormaPagamento> listaBancoConfiguracaoRemessaFormaPagamento) {
		this.listaBancoConfiguracaoRemessaFormaPagamento = listaBancoConfiguracaoRemessaFormaPagamento;
	}
	
	public void setListaBancoConfiguracaoRemessaAuxiliar1(
			List<BancoConfiguracaoRemessaAuxiliar> listaBancoConfiguracaoRemessaAuxiliar1) {
		this.listaBancoConfiguracaoRemessaAuxiliar1 = listaBancoConfiguracaoRemessaAuxiliar1;
	}
	
	public void setListaBancoConfiguracaoRemessaAuxiliar2(
			List<BancoConfiguracaoRemessaAuxiliar> listaBancoConfiguracaoRemessaAuxiliar2) {
		this.listaBancoConfiguracaoRemessaAuxiliar2 = listaBancoConfiguracaoRemessaAuxiliar2;
	}
	
	public void setListaContacarteira(Set<Contacarteira> listaContacarteira) {
		this.listaContacarteira = listaContacarteira;
	}
	
	public void setBancoFormapagamento(BancoFormapagamento bancoFormapagamento) {
		this.bancoFormapagamento = bancoFormapagamento;
	}
	
	public void setBancoTipoPagamento(BancoTipoPagamento bancoTipoPagamento) {
		this.bancoTipoPagamento = bancoTipoPagamento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Considerar contas com a situa��o")
	public Boolean getConsiderarcontasituacao() {
		return considerarcontasituacao;
	}
	
	@DisplayName("Obrigar empresa na gera��o de remessa")
	public Boolean getEmpresaobrigatoria() {
		return empresaobrigatoria;
	}
	public void setEmpresaobrigatoria(Boolean empresaobrigatoria) {
		this.empresaobrigatoria = empresaobrigatoria;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRemessa")
	public List<BancoConfiguracaoRemessaDocumentoacao> getListaBancoConfiguracaoRemessaDocumentoacao() {
		return listaBancoConfiguracaoRemessaDocumentoacao;
	}

	public void setConsiderarcontasituacao(Boolean considerarcontasituacao) {
		this.considerarcontasituacao = considerarcontasituacao;
	}
	public void setListaBancoConfiguracaoRemessaDocumentoacao(
			List<BancoConfiguracaoRemessaDocumentoacao> listaBancoConfiguracaoRemessaDocumentoacao) {
		this.listaBancoConfiguracaoRemessaDocumentoacao = listaBancoConfiguracaoRemessaDocumentoacao;
	}

	@Transient
	@DisplayName("Situa��o")
	public List<Documentoacao> getListaDocumentoacao() {
		return listaDocumentoacao;
	}
	public void setListaDocumentoacao(List<Documentoacao> listaDocumentoacao) {
		this.listaDocumentoacao = listaDocumentoacao;
	}
}
