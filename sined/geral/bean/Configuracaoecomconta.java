package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_configuracaoecomconta", sequenceName = "sq_configuracaoecomconta")
public class Configuracaoecomconta {

	protected Integer cdconfiguracaoecomconta;
	protected Configuracaoecom configuracaoecom;
	protected Conta conta;
	protected Documentotipo documentotipo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaoecomconta")
	public Integer getCdconfiguracaoecomconta() {
		return cdconfiguracaoecomconta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaoecom")	
	public Configuracaoecom getConfiguracaoecom() {
		return configuracaoecom;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")	
	public Conta getConta() {
		return conta;
	}

	@Required
	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")	
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setCdconfiguracaoecomconta(Integer cdconfiguracaoecomconta) {
		this.cdconfiguracaoecomconta = cdconfiguracaoecomconta;
	}

	public void setConfiguracaoecom(Configuracaoecom configuracaoecom) {
		this.configuracaoecom = configuracaoecom;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
}