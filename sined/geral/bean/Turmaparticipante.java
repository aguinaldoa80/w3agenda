package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_turmaparticipante", sequenceName = "sq_turmaparticipante")
public class Turmaparticipante {

	protected Integer cdturmaparticipante;
	protected Colaborador colaborador;
	protected Turma turma;
	
	public Turmaparticipante(){}
	public Turmaparticipante(Integer cdturmaparticipante){
		this.cdturmaparticipante = cdturmaparticipante;
	}
	public Turmaparticipante(Turma turma, Colaborador colaborador){
		this.turma = turma;
		this.colaborador = colaborador;
	}
	
//	TRANSIENTES
	protected Departamento departamento;
	protected Colaborador colaboradortrans;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_turmaparticipante")
	public Integer getCdturmaparticipante() {
		return cdturmaparticipante;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdturma")
	@Required
	public Turma getTurma() {
		return turma;
	}
	
	@DisplayName("Participante")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setCdturmaparticipante(Integer cdturmaparticipante) {
		this.cdturmaparticipante = cdturmaparticipante;
	}


	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	
	@Transient
	@Required
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	@Transient
	@Required
	@DisplayName("Participante")
	public Colaborador getColaboradortrans() {
		return colaboradortrans;
	}
	
	public void setColaboradortrans(Colaborador colaboradortrans) {
		this.colaboradortrans = colaboradortrans;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Turmaparticipante) {
			Turmaparticipante tp = (Turmaparticipante) obj;
			return this.getTurma().equals(tp.getTurma()) && this.getColaborador().equals(tp.getColaborador());
		}
		return super.equals(obj);
	}

}
