package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_producaoagendahistorico", sequenceName = "sq_producaoagendahistorico")
public class Producaoagendahistorico implements Log {
	
	protected Integer cdproducaoagendahistorico; 
	protected Producaoagenda producaoagenda;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	// TRANSIENTES
	protected String whereIn;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendahistorico")
	public Integer getCdproducaoagendahistorico() {
		return cdproducaoagendahistorico;
	}

	@JoinColumn(name="cdproducaoagenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdproducaoagendahistorico(Integer cdproducaoagendahistorico) {
		this.cdproducaoagendahistorico = cdproducaoagendahistorico;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	@Transient
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
}
