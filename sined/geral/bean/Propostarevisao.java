package br.com.linkcom.sined.geral.bean;


import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_propostarevisao", sequenceName = "sq_propostarevisao")
public class Propostarevisao implements Log{
	
	protected Integer cdpropostarevisao;
	protected Proposta proposta;
	protected Date dtvalidade;
	protected String prazo;
	protected Arquivo arquivoPTM;
	protected Date dtentregaPTM;
	protected String revisaoPTM;
	protected Arquivo arquivoPCM;
	protected Date dtentregaPCM;
	protected String revisaoPCM;
	protected String formapagamento;
	protected Money mobilizacao;
	protected Money mdo;
	protected Money materiais;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String tipoarquivo1 = "PTM";
	protected String tipoarquivo2 = "PCM";
	
//	TRANSIENTES
	protected String total;
	protected Integer cdorcamento;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_propostarevisao")
	public Integer getCdpropostarevisao() {
		return cdpropostarevisao;
	}
	
	@DisplayName("Proposta")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdproposta")
	public Proposta getProposta() {
		return proposta;
	}
	@DisplayName("Validade")
	public Date getDtvalidade() {
		return dtvalidade;
	}
	@DisplayName("Prazo de execu��o")
	@MaxLength(30)
	public String getPrazo() {
		return prazo;
	}
	@DisplayName("Arquivo 1")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdarquivoptm")
	public Arquivo getArquivoPTM() {
		return arquivoPTM;
	}
	@DisplayName("Data de entrega do Aquivo PTM")
	public Date getDtentregaPTM() {
		return dtentregaPTM;
	}
	@DisplayName("Revis�o PTM")
	@MaxLength(5)
	public String getRevisaoPTM() {
		return revisaoPTM;
	}
	@DisplayName("Arquivo 2")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdarquivopcm")
	public Arquivo getArquivoPCM() {
		return arquivoPCM;
	}
	@DisplayName("Data de entrega do Aquivo PCM")
	public Date getDtentregaPCM() {
		return dtentregaPCM;
	}
	@DisplayName("Revis�o PCM")
	@MaxLength(5)
	public String getRevisaoPCM() {
		return revisaoPCM;
	}
	@DisplayName("Forma de pagamento")
	@MaxLength(100)
	public String getFormapagamento() {
		return formapagamento;
	}
	@DisplayName("Mobiliza��o")
	public Money getMobilizacao() {
		return mobilizacao;
	}
	@DisplayName("MDO")
	public Money getMdo() {
		return mdo;
	}
	public Money getMateriais() {
		return materiais;
	}
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Obsrva��o")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdpropostarevisao(Integer cdpropostarevisao) {
		this.cdpropostarevisao = cdpropostarevisao;
	}
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}
	public void setDtentregaPTM(Date dtentregaPTM) {
		this.dtentregaPTM = dtentregaPTM;
	}
	public void setRevisaoPTM(String revisaoPTM) {
		this.revisaoPTM = revisaoPTM;
	}
	public void setDtentregaPCM(Date dtentregaPCM) {
		this.dtentregaPCM = dtentregaPCM;
	}
	public void setRevisaoPCM(String revisaoPCM) {
		this.revisaoPCM = revisaoPCM;
	}
	public void setFormapagamento(String formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setMobilizacao(Money mobilizacao) {
		this.mobilizacao = mobilizacao;
	}
	public void setMdo(Money mdo) {
		this.mdo = mdo;
	}
	public void setMateriais(Money materiais) {
		this.materiais = materiais;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setArquivoPCM(Arquivo arquivoPCM) {
		this.arquivoPCM = arquivoPCM;
	}
	public void setArquivoPTM(Arquivo arquivoPTM) {
		this.arquivoPTM = arquivoPTM;
	}
			
//	TRANSIENTES
	@Transient
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	@Transient
	public Integer getCdorcamento() {
		return cdorcamento;
	}
	public void setCdorcamento(Integer cdorcamento) {
		this.cdorcamento = cdorcamento;
	}

	@MaxLength(30)
	public String getTipoarquivo1() {
		return tipoarquivo1;
	}

	public void setTipoarquivo1(String tipoarquivo1) {
		this.tipoarquivo1 = tipoarquivo1;
	}
	
	@MaxLength(30)
	public String getTipoarquivo2() {
		return tipoarquivo2;
	}

	public void setTipoarquivo2(String tipoarquivo2) {
		this.tipoarquivo2 = tipoarquivo2;
	}
}
