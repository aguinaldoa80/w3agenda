package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name = "sq_comissionamentometa", sequenceName = "sq_comissionamentometa")
public class Comissionamentometa {

	protected Integer cdcomissionamentometa;
	protected Comissionamento comissionamento;
	protected Money metavenda;
	protected Double comissao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comissionamentometa")
	public Integer getCdcomissionamentometa() {
		return cdcomissionamentometa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")	
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@DisplayName("Meta venda")
	public Money getMetavenda() {
		return metavenda;
	}
	@DisplayName("Comiss�o")
	public Double getComissao() {
		return comissao;
	}

	public void setCdcomissionamentometa(Integer cdcomissionamentometa) {
		this.cdcomissionamentometa = cdcomissionamentometa;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setMetavenda(Money metavenda) {
		this.metavenda = metavenda;
	}
	public void setComissao(Double comissao) {
		this.comissao = comissao;
	}
}
