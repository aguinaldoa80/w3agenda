package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_producaoagendamaterial", sequenceName = "sq_producaoagendamaterial")
public class Producaoagendamaterial {
	
	protected Integer cdproducaoagendamaterial; 
	protected Producaoagenda producaoagenda;
	protected Material material;
	protected Double largura;
	protected Double altura;
	protected Double comprimento; 
	protected Double qtde;
	protected Producaoetapa producaoetapa;
	protected Pedidovendamaterial pedidovendamaterial;
	protected String observacao;
	protected Unidademedida unidademedida;
	protected Pneu pneu;
	protected Boolean bandaenviada;
	protected Integer cdordemproducaoPrimeiraEtapa;
	protected Integer cdordemproducaoUltimaEtapa;

	protected List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima;
	
	// TRANSIENTES
	protected Unidademedida unidademedidaTrans;
	protected Material materialChaofabrica;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendamaterial")
	public Integer getCdproducaoagendamaterial() {
		return cdproducaoagendamaterial;
	}

	@JoinColumn(name="cdproducaoagenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@JoinColumn(name="cdpedidovendamaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	@OneToMany(mappedBy="producaoagendamaterial")
	public List<Producaoagendamaterialmateriaprima> getListaProducaoagendamaterialmateriaprima() {
		return listaProducaoagendamaterialmateriaprima;
	}
	
	@JoinColumn(name="cdpneu")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pneu getPneu() {
		return pneu;
	}
	
	public Boolean getBandaenviada() {
		return bandaenviada;
	}

	public void setBandaenviada(Boolean bandaenviada) {
		this.bandaenviada = bandaenviada;
	}

	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	public void setListaProducaoagendamaterialmateriaprima(List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima) {
		this.listaProducaoagendamaterialmateriaprima = listaProducaoagendamaterialmateriaprima;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setCdproducaoagendamaterial(Integer cdproducaoagendamaterial) {
		this.cdproducaoagendamaterial = cdproducaoagendamaterial;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getAltura() {
		return altura;
	}

	public Double getComprimento() {
		return comprimento;
	}
	@DisplayName("Ciclo de produ��o")
	@JoinColumn(name="cdproducaoetapa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	
	@DisplayName("U.M.")
	@JoinColumn(name="cdunidademedida")
	@ManyToOne(fetch=FetchType.LAZY)
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	@Transient
	public Unidademedida getUnidademedidaTrans() {
		return unidademedidaTrans;
	}
	
	public void setUnidademedidaTrans(Unidademedida unidademedidaTrans) {
		this.unidademedidaTrans = unidademedidaTrans;
	}
	
	@Transient
	public Material getMaterialChaofabrica() {
		return materialChaofabrica;
	}
	
	public void setMaterialChaofabrica(Material materialChaofabrica) {
		this.materialChaofabrica = materialChaofabrica;
	}
	
	public Integer getCdordemproducaoPrimeiraEtapa() {
		return cdordemproducaoPrimeiraEtapa;
	}
	
	public Integer getCdordemproducaoUltimaEtapa() {
		return cdordemproducaoUltimaEtapa;
	}
	
	public void setCdordemproducaoPrimeiraEtapa(Integer cdordemproducaoPrimeiraEtapa) {
		this.cdordemproducaoPrimeiraEtapa = cdordemproducaoPrimeiraEtapa;
	}
	
	public void setCdordemproducaoUltimaEtapa(Integer cdordemproducaoUltimaEtapa) {
		this.cdordemproducaoUltimaEtapa = cdordemproducaoUltimaEtapa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdproducaoagendamaterial == null) ? 0
						: cdproducaoagendamaterial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producaoagendamaterial other = (Producaoagendamaterial) obj;
		if (cdproducaoagendamaterial == null) {
			if (other.cdproducaoagendamaterial != null)
				return false;
		} else if (!cdproducaoagendamaterial
				.equals(other.cdproducaoagendamaterial))
			return false;
		return true;
	}	
	
}
