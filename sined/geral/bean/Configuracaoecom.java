package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_configuracaoecom", sequenceName = "sq_configuracaoecom")
public class Configuracaoecom implements Log {
	
	protected Integer cdconfiguracaoecom;
	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Materialtabelapreco materialtabelapreco;
	protected Conta conta;
	protected String url;
	protected Boolean principal;
	protected Boolean enviaridentificador;
	protected List<Configuracaoecomconta> listaConfiguracaoecomconta = new ListSet<Configuracaoecomconta>(Configuracaoecomconta.class);

	// LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaoecom")
	public Integer getCdconfiguracaoecom() {
		return cdconfiguracaoecom;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@DisplayName("Tabela de pre�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}

	@Required
	@DisplayName("URL")
	@MaxLength(200)
	public String getUrl() {
		return url;
	}
	
	@DisplayName("Principal")
	public Boolean getPrincipal() {
		return principal;
	}

	@DisplayName("Configura��o de Tipo de documento / Contas")
	@OneToMany(mappedBy="configuracaoecom")
	public List<Configuracaoecomconta> getListaConfiguracaoecomconta() {
		return listaConfiguracaoecomconta;
	}
	
	@DisplayName("Enviar Identificador Material")
	public Boolean getEnviaridentificador() {
		return enviaridentificador;
	}
	
	public void setEnviaridentificador(Boolean enviaridentificador) {
		this.enviaridentificador = enviaridentificador;
	}
	
	public void setListaConfiguracaoecomconta(
			List<Configuracaoecomconta> listaConfiguracaoecomconta) {
		this.listaConfiguracaoecomconta = listaConfiguracaoecomconta;
	}
	
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public void setCdconfiguracaoecom(Integer cdconfiguracaoecom) {
		this.cdconfiguracaoecom = cdconfiguracaoecom;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	// LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}
