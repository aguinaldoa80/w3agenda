package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_coletamaterialdevolucao", sequenceName = "sq_coletamaterialdevolucao")
public class ColetaMaterialDevolucao{
	protected Integer cdcoletamaterialdevolucao;
	protected ColetaMaterial coletamaterial;
	protected Notafiscalprodutoitem notafiscalprodutoitem;

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_coletamaterialdevolucao")
	public Integer getCdcoletamaterialdevolucao() {
		return cdcoletamaterialdevolucao;
	}
	public void setCdcoletamaterialdevolucao(Integer cdcoletamaterialdevolucao) {
		this.cdcoletamaterialdevolucao = cdcoletamaterialdevolucao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoletamaterial")
	public ColetaMaterial getColetamaterial() {
		return coletamaterial;
	}
	public void setColetamaterial(ColetaMaterial coletamaterial) {
		this.coletamaterial = coletamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotafiscalprodutoitem() {
		return notafiscalprodutoitem;
	}
	public void setNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem) {
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}	
}
