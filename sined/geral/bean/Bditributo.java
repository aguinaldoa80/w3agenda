package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_bditributo",sequenceName="sq_bditributo")
public class Bditributo implements Log {

	protected Integer cdbditributo;
	protected Bdi bdi;
	protected Bditributo bditributopai;	
	protected String nome;
	protected Double valor;
	protected Contagerencial contagerencial;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_bditributo", strategy=GenerationType.AUTO)	
	public Integer getCdbditributo() {
		return cdbditributo;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbdi")
	@DisplayName("Bdi")
	public Bdi getBdi() {
		return bdi;
	}
	
	@DisplayName("Item pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbditributopai")	
	public Bditributo getBditributopai() {
		return bditributopai;
	}

	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@Required
	public Double getValor() {
		return valor;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public void setCdbditributo(Integer cdbditributo) {
		this.cdbditributo = cdbditributo;
	}

	public void setBdi(Bdi bdi) {
		this.bdi = bdi;
	}

	public void setBditributopai(Bditributo bditributopai) {
		this.bditributopai = bditributopai;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	/*************/
	/**** Log ****/
	/*************/
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bditributo) {
			Bditributo that = (Bditributo) obj;
			return this.getCdbditributo().equals(that.getCdbditributo());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdbditributo != null) {
			return cdbditributo.hashCode();
		}
		return super.hashCode();		
	}

	
}
