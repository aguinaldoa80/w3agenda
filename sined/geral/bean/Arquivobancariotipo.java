package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Arquivobancariotipo {

	public static Arquivobancariotipo REMESSA = new Arquivobancariotipo(1);
	public static Arquivobancariotipo RETORNO = new Arquivobancariotipo(2);
	
	protected Integer cdarquivobancariotipo;
	protected String nome;
	
	public Arquivobancariotipo() {
	}
	
	public Arquivobancariotipo(Integer cdarquivobancariotipo) {
		this.cdarquivobancariotipo = cdarquivobancariotipo;
	}
	
	@Id
	public Integer getCdarquivobancariotipo() {
		return cdarquivobancariotipo;
	}
	@DisplayName("Nome")
	@DescriptionProperty
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	public void setCdarquivobancariotipo(Integer cdarquivobancariotipo) {
		this.cdarquivobancariotipo = cdarquivobancariotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arquivobancariotipo) {
			Arquivobancariotipo arquivobancariotipo = (Arquivobancariotipo) obj;
			return arquivobancariotipo.getCdarquivobancariotipo().equals(this.getCdarquivobancariotipo());
		}
		return super.equals(obj);
	}
}
