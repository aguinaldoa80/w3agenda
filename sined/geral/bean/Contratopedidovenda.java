package br.com.linkcom.sined.geral.bean;



import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_contratopedidovenda", sequenceName = "sq_contratopedidovenda")
public class Contratopedidovenda {
	
	protected Integer cdcontratopedidovenda; 
	protected Contrato contrato;
	protected Pedidovenda pedidovenda;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratopedidovenda")
	public Integer getCdcontratopedidovenda() {
		return cdcontratopedidovenda;
	}
	
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@JoinColumn(name="cdpedidovenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setCdcontratopedidovenda(Integer cdcontratopedidovenda) {
		this.cdcontratopedidovenda = cdcontratopedidovenda;
	}
	
}
