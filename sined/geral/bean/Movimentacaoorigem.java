package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_movimentacaoorigem",sequenceName="sq_movimentacaoorigem")
public class Movimentacaoorigem implements Log{

	protected Integer cdmovimentacaoorigem;
	protected Movimentacao movimentacao;
	protected Documento documento;
	protected Conta conta;
	protected Agendamento agendamento;
	protected Despesaviagem despesaviagemadiantamento;
	protected Despesaviagem despesaviagemacerto;
	protected Movimentacao movimentacaodevolucao;
	protected Movimentacao movimentacaorelacionada;
	protected Movimentacao movimentacaorelacionadacartaocredito;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTE
	protected Money valorQ100;
	
	public Movimentacaoorigem() {}
	
	
	
	public Movimentacaoorigem(Integer cdmovimentacaoorigem,
			Movimentacao movimentacao, Documento documento, Conta conta,
			Agendamento agendamento, Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdmovimentacaoorigem = cdmovimentacaoorigem;
		this.movimentacao = movimentacao;
		this.documento = documento;
		this.conta = conta;
		this.agendamento = agendamento;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}



	@Id
	@GeneratedValue(generator="sq_movimentacaoorigem",strategy=GenerationType.AUTO)
	public Integer getCdmovimentacaoorigem() {
		return cdmovimentacaoorigem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacao")
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamento")
	public Agendamento getAgendamento() {
		return agendamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemacerto")
	public Despesaviagem getDespesaviagemacerto() {
		return despesaviagemacerto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagemadiantamento")
	public Despesaviagem getDespesaviagemadiantamento() {
		return despesaviagemadiantamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaodevolucao")
	public Movimentacao getMovimentacaodevolucao() {
		return movimentacaodevolucao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaorelacionada")
	public Movimentacao getMovimentacaorelacionada() {
		return movimentacaorelacionada;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaorelacionadacartaocredito")
	public Movimentacao getMovimentacaorelacionadacartaocredito() {
		return movimentacaorelacionadacartaocredito;
	}

	public void setDespesaviagemacerto(Despesaviagem despesaviagemacerto) {
		this.despesaviagemacerto = despesaviagemacerto;
	}
	
	public void setDespesaviagemadiantamento(
			Despesaviagem despesaviagemadiantamento) {
		this.despesaviagemadiantamento = despesaviagemadiantamento;
	}
	
	public void setMovimentacaodevolucao(Movimentacao movimentacaodevolucao) {
		this.movimentacaodevolucao = movimentacaodevolucao;
	}
	
	public void setMovimentacaorelacionada(Movimentacao movimentacaorelacionada) {
		this.movimentacaorelacionada = movimentacaorelacionada;
	}
	public void setMovimentacaorelacionadacartaocredito(Movimentacao movimentacaorelacionadacartaocredito) {
		this.movimentacaorelacionadacartaocredito = movimentacaorelacionadacartaocredito;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdmovimentacaoorigem(Integer cdmovimentacaoorigem) {
		this.cdmovimentacaoorigem = cdmovimentacaoorigem;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Money getValorQ100() {
		return valorQ100;
	}
	
	public void setValorQ100(Money valorQ100) {
		this.valorQ100 = valorQ100;
	}
	
}
