package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutoduplicata",sequenceName="sq_notafiscalprodutoduplicata")
public class Notafiscalprodutoduplicata {
	
	protected Integer cdnotafiscalprodutoduplicata;
	protected Notafiscalproduto notafiscalproduto;
	protected Documentotipo documentotipo;
	protected String numero;
	protected Date dtvencimento;
	protected Money valor;
	
	protected String autorizacao;
	protected String bandeira;
	protected String adquirente; 
	
	@Id
	@GeneratedValue(generator="sq_notafiscalprodutoduplicata", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutoduplicata() {
		return cdnotafiscalprodutoduplicata;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	@DisplayName("Forma de pagamento")
	@JoinColumn(name="cdformapagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}

	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Money getValor() {
		return valor;
	}
	
	public String getAutorizacao() {
		return autorizacao;
	}

	public String getBandeira() {
		return bandeira;
	}

	public String getAdquirente() {
		return adquirente;
	}

	public void setAutorizacao(String autorizacao) {
		this.autorizacao = autorizacao;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public void setAdquirente(String adquirente) {
		this.adquirente = adquirente;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setCdnotafiscalprodutoduplicata(Integer cdnotafiscalprodutoduplicata) {
		this.cdnotafiscalprodutoduplicata = cdnotafiscalprodutoduplicata;
	}

	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
		
}