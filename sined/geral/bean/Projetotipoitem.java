package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_projetotipoitem",sequenceName="sq_projetotipoitem")
public class Projetotipoitem {

	protected Integer cdprojetotipoitem;
	protected Projetotipo projetotipo;
	protected String nome;
	protected Boolean obrigatorio;
	
	@Id
	@GeneratedValue(generator="sq_projetotipoitem",strategy=GenerationType.AUTO)
	public Integer getCdprojetotipoitem() {
		return cdprojetotipoitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetotipo")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Campo Obrigatório")
	public Boolean getObrigatorio() {
		return obrigatorio;
	}
	
	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}

	public void setCdprojetotipoitem(Integer cdprojetotipoitem) {
		this.cdprojetotipoitem = cdprojetotipoitem;
	}

	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
