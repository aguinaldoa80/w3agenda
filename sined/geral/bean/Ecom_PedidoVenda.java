package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_ecom_pedidovenda", sequenceName="sq_ecom_pedidovenda")
public class Ecom_PedidoVenda {

	private Integer cdEcom_pedidovenda;
	private Integer id;
	private String data;
	private String hora;
	private Date data_pagamento;
	private Integer id_formapagto;
	private String nome_formapagto;
	private Integer id_situacao;
	private String nome_situacao;
	private Integer numero_parcelas;
	private Double valor_parcela;
	private Double valor_cupomdesconto;
	private Double valor_juros;
	private Double valor_desconto;
	private Double valor_impostos;
	private Double valor_frete;
	private Double valor_total;
	private Integer id_forma_entrega;
	private String forma_entrega;
	private List<Ecom_PedidoVendaItem> pedidoItens;
	private List<Ecom_PedidoVendaSituacao> pedidoSituacoes;
	private List<Ecom_PedidoVendaPagamento> pagamentos;
	private Ecom_Cliente pedidoCliente;
	private Boolean processado;
	private Pedidovenda pedidoVenda;
	private Integer tipoEcommerce;;
	private Integer codigoParceiro;
	private String observacao;
	
	private String rastreamento;
	private String urlRastreamento;
	private String numeroNotaFiscal;
	private String chaveAcessoNFE;
	private String serieNFE;
	
	private Money valorCustoFrete;
	private Date dataPrevisaoEntrega;
	private String transportadora;
	private Integer quantidadevolumes;
	
	private Date dataFinalizacaoEntregaEcommerce;
	private Timestamp ultimaVerificacaoSituacao;
	private Integer cdVendaTrans;
	private Integer cdExpedicaoTrans;
	private Fornecedor transportadoraTrans;
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_pedidovenda", strategy=GenerationType.AUTO)
	public Integer getCdEcom_pedidovenda() {
		return cdEcom_pedidovenda;
	}
	public void setCdEcom_pedidovenda(Integer cdEcom_pedidovenda) {
		this.cdEcom_pedidovenda = cdEcom_pedidovenda;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	/*public void setData(String data){
		try {
			this.data = new Date(new SimpleDateFormat("yyyy-MM-dd").parse(data).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public Date getData_pagamento() {
		return data_pagamento;
	}
	public void setData_pagamento(Date data_pagamento) {
		this.data_pagamento = data_pagamento;
	}
	public Integer getId_formapagto() {
		return id_formapagto;
	}
	public void setId_formapagto(Integer id_formapagto) {
		this.id_formapagto = id_formapagto;
	}
	public String getNome_formapagto() {
		return nome_formapagto;
	}
	public void setNome_formapagto(String nome_formapagto) {
		this.nome_formapagto = nome_formapagto;
	}
	public Integer getId_situacao() {
		return id_situacao;
	}
	public void setId_situacao(Integer id_situacao) {
		this.id_situacao = id_situacao;
	}
	public String getNome_situacao() {
		return nome_situacao;
	}
	public void setNome_situacao(String nome_situacao) {
		this.nome_situacao = nome_situacao;
	}
	public Integer getNumero_parcelas() {
		return numero_parcelas;
	}
	public void setNumero_parcelas(Integer numero_parcelas) {
		this.numero_parcelas = numero_parcelas;
	}
	public Double getValor_parcela() {
		return valor_parcela;
	}
	public void setValor_parcela(Double valor_parcela) {
		this.valor_parcela = valor_parcela;
	}
	public Double getValor_cupomdesconto() {
		return valor_cupomdesconto;
	}
	public void setValor_cupomdesconto(Double valor_cupomdesconto) {
		this.valor_cupomdesconto = valor_cupomdesconto;
	}
	public Double getValor_juros() {
		return valor_juros;
	}
	public void setValor_juros(Double valor_juros) {
		this.valor_juros = valor_juros;
	}
	public Double getValor_desconto() {
		return valor_desconto;
	}
	public void setValor_desconto(Double valor_desconto) {
		this.valor_desconto = valor_desconto;
	}
	public Double getValor_impostos() {
		return valor_impostos;
	}
	public void setValor_impostos(Double valor_impostos) {
		this.valor_impostos = valor_impostos;
	}
	public Double getValor_frete() {
		return valor_frete;
	}
	public void setValor_frete(Double valor_frete) {
		this.valor_frete = valor_frete;
	}
	public Double getValor_total() {
		return valor_total;
	}
	public void setValor_total(Double valor_total) {
		this.valor_total = valor_total;
	}
	public Integer getId_forma_entrega() {
		return id_forma_entrega;
	}
	public void setId_forma_entrega(Integer id_forma_entrega) {
		this.id_forma_entrega = id_forma_entrega;
	}
	public String getForma_entrega() {
		return forma_entrega;
	}
	public void setForma_entrega(String forma_entrega) {
		this.forma_entrega = forma_entrega;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ecomPedidoVenda")
	public List<Ecom_PedidoVendaItem> getPedidoItens() {
		return pedidoItens;
	}
	public void setPedidoItens(List<Ecom_PedidoVendaItem> pedidoItens) {
		this.pedidoItens = pedidoItens;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ecomPedidoVenda")
	public List<Ecom_PedidoVendaSituacao> getPedidoSituacoes() {
		return pedidoSituacoes;
	}
	public void setPedidoSituacoes(List<Ecom_PedidoVendaSituacao> pedidoSituacoes) {
		this.pedidoSituacoes = pedidoSituacoes;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ecomPedidoVenda")
	public List<Ecom_PedidoVendaPagamento> getPagamentos() {
		return pagamentos;
	}
	public void setPagamentos(List<Ecom_PedidoVendaPagamento> pagamentos) {
		this.pagamentos = pagamentos;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_cliente")
	public Ecom_Cliente getPedidoCliente() {
		return pedidoCliente;
	}
	public void setPedidoCliente(Ecom_Cliente pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}
	public Boolean getProcessado() {
		return processado;
	}
	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidoVenda() {
		return pedidoVenda;
	}
	public void setPedidoVenda(Pedidovenda pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}
	public Integer getTipoEcommerce() {
		return tipoEcommerce;
	}
	public void setTipoEcommerce(Integer tipoEcommerce) {
		this.tipoEcommerce = tipoEcommerce;
	}
	public Integer getCodigoParceiro() {
		return codigoParceiro;
	}
	public void setCodigoParceiro(Integer codigoParceiro) {
		this.codigoParceiro = codigoParceiro;
	}
    public String getObservacao() {
        return observacao;
    }
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
	public String getRastreamento() {
		return rastreamento;
	}
	public String getUrlRastreamento() {
		return urlRastreamento;
	}
	public String getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}
	public String getChaveAcessoNFE() {
		return chaveAcessoNFE;
	}
	public String getSerieNFE() {
		return serieNFE;
	}
	public Date getDataPrevisaoEntrega() {
		return dataPrevisaoEntrega;
	}
	public Money getValorCustoFrete() {
		return valorCustoFrete;
	}
	public String getTransportadora() {
		return transportadora;
	}
	public Integer getQuantidadevolumes() {
		return quantidadevolumes;
	}
	public void setRastreamento(String rastreamento) {
		this.rastreamento = rastreamento;
	}
	public void setUrlRastreamento(String urlRastreamento) {
		this.urlRastreamento = urlRastreamento;
	}
	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}
	public void setChaveAcessoNFE(String chaveAcessoNFE) {
		this.chaveAcessoNFE = chaveAcessoNFE;
	}
	public void setSerieNFE(String serieNFE) {
		this.serieNFE = serieNFE;
	}
	public void setDataPrevisaoEntrega(Date dataPrevisaoEntrega) {
		this.dataPrevisaoEntrega = dataPrevisaoEntrega;
	}
	public void setValorCustoFrete(Money valorCustoFrete) {
		this.valorCustoFrete = valorCustoFrete;
	}
	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}
	public void setQuantidadevolumes(Integer quantidadevolumes) {
		this.quantidadevolumes = quantidadevolumes;
	}
	public Date getDataFinalizacaoEntregaEcommerce() {
		return dataFinalizacaoEntregaEcommerce;
	}
	public void setDataFinalizacaoEntregaEcommerce(Date dataFinalizacaoEntregaEcommerce) {
		this.dataFinalizacaoEntregaEcommerce = dataFinalizacaoEntregaEcommerce;
	}
	public Timestamp getUltimaVerificacaoSituacao() {
		return ultimaVerificacaoSituacao;
	}
	public void setUltimaVerificacaoSituacao(Timestamp ultimaVerificacaoSituacao) {
		this.ultimaVerificacaoSituacao = ultimaVerificacaoSituacao;
	}
	@Transient
	public Integer getCdVendaTrans() {
		return cdVendaTrans;
	}
	public void setCdVendaTrans(Integer cdVendaTrans) {
		this.cdVendaTrans = cdVendaTrans;
	}
	@Transient
	public Integer getCdExpedicaoTrans() {
		return cdExpedicaoTrans;
	}
	public void setCdExpedicaoTrans(Integer cdExpedicaoTrans) {
		this.cdExpedicaoTrans = cdExpedicaoTrans;
	}
	@Transient
	public Fornecedor getTransportadoraTrans() {
		return transportadoraTrans;
	}
	public void setTransportadoraTrans(Fornecedor transportadoraTrans) {
		this.transportadoraTrans = transportadoraTrans;
	}
}
