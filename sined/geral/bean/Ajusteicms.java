package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Ajuste de apura��o de ICMS")
@SequenceGenerator(name = "sq_ajusteicms", sequenceName = "sq_ajusteicms")
public class Ajusteicms implements Log {

	protected Integer cdajusteicms;
	protected Empresa empresa;
	protected Date dtfatogerador;
	protected Ajusteicmstipo ajusteicmstipo;
	protected Uf uf;
	protected Double valor;
	protected String descricaocomplementar;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Ajusteicmsprocesso> listaAjusteicmsprocesso = new ListSet<Ajusteicmsprocesso>(Ajusteicmsprocesso.class);
	protected Set<Ajusteicmsdocumento> listaAjusteicmsdocumento = new ListSet<Ajusteicmsdocumento>(Ajusteicmsdocumento.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteicms")
	public Integer getCdajusteicms() {
		return cdajusteicms;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data do fato gerador")
	public Date getDtfatogerador() {
		return dtfatogerador;
	}

	@Required
	@DisplayName("Ajuste")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdajusteicmstipo")
	public Ajusteicmstipo getAjusteicmstipo() {
		return ajusteicmstipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}

	@Required
	public Double getValor() {
		return valor;
	}

	@MaxLength(500)
	@DisplayName("Descri��o complementar")
	public String getDescricaocomplementar() {
		return descricaocomplementar;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Processos judiciais")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ajusteicms")
	public Set<Ajusteicmsprocesso> getListaAjusteicmsprocesso() {
		return listaAjusteicmsprocesso;
	}
	
	@DisplayName("Documentos fiscais")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ajusteicms")
	public Set<Ajusteicmsdocumento> getListaAjusteicmsdocumento() {
		return listaAjusteicmsdocumento;
	} 
	
	public void setListaAjusteicmsdocumento(
			Set<Ajusteicmsdocumento> listaAjusteicmsdocumento) {
		this.listaAjusteicmsdocumento = listaAjusteicmsdocumento;
	}
	
	public void setListaAjusteicmsprocesso(
			Set<Ajusteicmsprocesso> listaAjusteicmsprocesso) {
		this.listaAjusteicmsprocesso = listaAjusteicmsprocesso;
	}

	public void setCdajusteicms(Integer cdajusteicms) {
		this.cdajusteicms = cdajusteicms;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtfatogerador(Date dtfatogerador) {
		this.dtfatogerador = dtfatogerador;
	}

	public void setAjusteicmstipo(Ajusteicmstipo ajusteicmstipo) {
		this.ajusteicmstipo = ajusteicmstipo;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setDescricaocomplementar(String descricaocomplementar) {
		this.descricaocomplementar = descricaocomplementar;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
