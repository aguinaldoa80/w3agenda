package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_veiculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.geral.bean.view.Vwitemvencido;
import br.com.linkcom.sined.geral.bean.view.Vwveiculo;
import br.com.linkcom.sined.util.Log;

@DisplayName("Ve�culo")
@Entity
@SequenceGenerator(name = "sq_veiculo", sequenceName = "sq_veiculo")
public class Veiculo implements Log {

	protected Integer cdveiculo;
	protected Patrimonioitem patrimonioitem;
	protected String placa;
	protected Tipocontroleveiculo tipocontroleveiculo;
	protected Integer kminicial;
	protected Double horimetroinicial;
	protected Date dtentrada;
	protected Fornecedor fornecedor;
	protected Colaborador colaborador;
	protected Veiculomodelo veiculomodelo;
	protected Veiculocor veiculocor;
	protected Veiculocombustivel veiculocombustivel;
	protected Empresa empresa;
	protected Integer anofabricacao;
	protected Integer anomodelo;
	protected String chassi;
	protected String renavam;
	protected Uf uf;
	protected Municipio municipio;
	protected String anotacoes;
	protected String prefixo;
	protected Integer prazopreventiva;
	protected Escala escala;
	protected Fornecedor seguradora;
	protected Date dtvencimentoseguro;
	
	protected Aux_veiculo aux_veiculo;
	protected Vwveiculo vwveiculo;
	
	protected List<Veiculoacessorio> listaveiculoacessorio = new ListSet<Veiculoacessorio>(Veiculoacessorio.class);
	protected List<Vwitemvencido> vwitemvencido;
	protected List<Veiculouso> listaVeiculouso;
	protected List<Veiculokm> listaVeiculoKm;
	protected List<Veiculohorimetro> listaVeiculohorimetro;

	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	//TRANSIENTE
	protected Patrimonioitem patrimonioitemtrans;
	protected String descriptionProperty;
	protected Projeto ultimoProjeto;
	
	public Veiculo(){
	}

	public Veiculo(Integer cdveiculo){
		this.cdveiculo = cdveiculo;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculo")
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	@Required
	@DisplayName("Patrim�nio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	@MaxLength(10)
	@Required
	public String getPlaca() {
		return placa;
	}
	@Required
	@DisplayName("Tipo de controle")
	public Tipocontroleveiculo getTipocontroleveiculo() {
		return tipocontroleveiculo;
	}
	@MaxLength(9)
	@DisplayName("Km inicial")
	public Integer getKminicial() {
		return kminicial;
	}
	@DisplayName("Hor�metro inicial")
	public Double getHorimetroinicial() {
		return horimetroinicial;
	}
	@Required
	@DisplayName("Data entrada")
	public Date getDtentrada() {
		return dtentrada;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Required
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Modelo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculomodelo")
	public Veiculomodelo getVeiculomodelo() {
		return veiculomodelo;
	}
	@DisplayName("Cor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculocor")
	public Veiculocor getVeiculocor() {
		return veiculocor;
	}
	@DisplayName("Combust�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculocombustivel")
	public Veiculocombustivel getVeiculocombustivel() {
		return veiculocombustivel;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@MaxLength(4)
	@DisplayName("Ano fabrica��o")
	public Integer getAnofabricacao() {
		return anofabricacao;
	}
	@MaxLength(4)
	@DisplayName("Ano modelo")
	public Integer getAnomodelo() {
		return anomodelo;
	}
	@MaxLength(20)
	public String getChassi() {
		return chassi;
	}
	@MaxLength(20)
	public String getRenavam() {
		return renavam;
	}
	@MaxLength(255)
	@DisplayName("Anota��es")
	public String getAnotacoes() {
		return anotacoes;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@OneToMany(mappedBy="veiculo")
	public List<Veiculoacessorio> getListaveiculoacessorio() {
		return listaveiculoacessorio;
	}
	@OneToMany(mappedBy="veiculo")
	public List<Veiculouso> getListaVeiculouso() {
		return listaVeiculouso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo", insertable=false, updatable=false)
	public Aux_veiculo getAux_veiculo() {
		return aux_veiculo;
	}
	@MaxLength(20)
	public String getPrefixo() {
		return prefixo;
	}
	@DisplayName("Prazo de inspe��o preventiva (Dias)")
	@MaxLength(8)
	public Integer getPrazopreventiva() {
		return prazopreventiva;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo", insertable=false, updatable=false)
	public Vwveiculo getVwveiculo() {
		return vwveiculo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescala")
	public Escala getEscala() {
		return escala;
	}
	
	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo", insertable=false, updatable=false)
	public List<Vwitemvencido> getVwitemvencido() {
		return vwitemvencido;
	}
	@OneToMany(mappedBy="veiculo")
	public List<Veiculokm> getListaVeiculoKm() {
		return listaVeiculoKm;
	}
	public void setListaVeiculoKm(List<Veiculokm> listaVeiculoKm) {
		this.listaVeiculoKm = listaVeiculoKm;
	}
	@OneToMany(mappedBy="veiculo")
	public List<Veiculohorimetro> getListaVeiculohorimetro() {
		return listaVeiculohorimetro;
	}
	public void setListaVeiculohorimetro(List<Veiculohorimetro> listaVeiculohorimetro) {
		this.listaVeiculohorimetro = listaVeiculohorimetro;
	}
	public void setVwitemvencido(List<Vwitemvencido> vwitemvencido) {
		this.vwitemvencido = vwitemvencido;
	}
	public void setVwveiculo(Vwveiculo vwveiculo) {
		this.vwveiculo = vwveiculo;
	}
	public void setPrazopreventiva(Integer prazopreventiva) {
		this.prazopreventiva = prazopreventiva;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setAux_veiculo(Aux_veiculo aux_veiculo) {
		this.aux_veiculo = aux_veiculo;
	}
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setTipocontroleveiculo(Tipocontroleveiculo tipocontroleveiculo) {
		this.tipocontroleveiculo = tipocontroleveiculo;
	}
	public void setKminicial(Integer kminicial) {
		this.kminicial = kminicial;
	}
	public void setHorimetroinicial(Double horimetroinicial) {
		this.horimetroinicial = horimetroinicial;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setVeiculomodelo(Veiculomodelo veiculomodelo) {
		this.veiculomodelo = veiculomodelo;
	}
	public void setVeiculocor(Veiculocor veiculocor) {
		this.veiculocor = veiculocor;
	}
	public void setVeiculocombustivel(Veiculocombustivel veiculocombustivel) {
		this.veiculocombustivel = veiculocombustivel;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setAnofabricacao(Integer anofabricacao) {
		this.anofabricacao = anofabricacao;
	}
	public void setAnomodelo(Integer anomodelo) {
		this.anomodelo = anomodelo;
	}
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaveiculoacessorio(
			List<Veiculoacessorio> listaveiculoacessorio) {
		this.listaveiculoacessorio = listaveiculoacessorio;
	}
	public void setListaVeiculouso(List<Veiculouso> listaVeiculouso) {
		this.listaVeiculouso = listaVeiculouso;
	}
	public void setEscala(Escala escala) {
		this.escala = escala;
	}
	
	@Transient
	@DisplayName("Ve�culo")
	@DescriptionProperty
	public String getDescriptionProperty(){
		if(descriptionProperty==null){
			descriptionProperty = "";
			if(patrimonioitem != null && patrimonioitem.getBempatrimonio()!=null && patrimonioitem.getBempatrimonio().getNome()!=null)
				descriptionProperty = patrimonioitem.getBempatrimonio().getNome(); 
			
			if(StringUtils.isNotBlank(placa))
				descriptionProperty += " ("+getPlaca()+")";
		}
		return descriptionProperty;
	}
	
	public void setDescriptionProperty(String descriptionProperty) {
		this.descriptionProperty = descriptionProperty;
	}
	
	@Transient
	public Projeto getUltimoProjeto() {
		return ultimoProjeto;
	}
	
	public void setUltimoProjeto(Projeto ultimoProjeto) {
		this.ultimoProjeto = ultimoProjeto;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Veiculo){
			return this.cdveiculo.equals(((Veiculo)obj).getCdveiculo());
		}
		return super.equals(obj);
	}
	
	@Transient
	@DisplayName("Patrim�nio")
	public Patrimonioitem getPatrimonioitemtrans() {
		return getPatrimonioitem();
	}
	
	public void setPatrimonioitemtrans(Patrimonioitem patrimonioitemtrans) {
		this.patrimonioitemtrans = patrimonioitemtrans;
	}
	
	@DisplayName("Seguradora")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdseguradora")
	public Fornecedor getSeguradora() {
		return seguradora;
	}
	@DisplayName("Vencimento do seguro")
	public Date getDtvencimentoseguro() {
		return dtvencimentoseguro;
	}

	public void setSeguradora(Fornecedor seguradora) {
		this.seguradora = seguradora;
	}
	public void setDtvencimentoseguro(Date dtvencimentoseguro) {
		this.dtvencimentoseguro = dtvencimentoseguro;
	}

	@DisplayName("UF")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}