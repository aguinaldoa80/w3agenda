package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_telefone",sequenceName="sq_telefone")
public class Telefone implements Log{

	protected Integer cdtelefone;
	protected String telefone;
	protected Telefonetipo telefonetipo;
	protected Pessoa pessoa;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_telefone",strategy=GenerationType.AUTO)
	public Integer getCdtelefone() {
		return cdtelefone;
	}
	
	@DescriptionProperty
	@MaxLength(20)
	@Required
	public String getTelefone() {
		return telefone;
	}
	
	@DisplayName("Tipo de telefone")
	@JoinColumn(name="cdtelefonetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Telefonetipo getTelefonetipo() {
		return telefonetipo;
	}
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdtelefone(Integer cdtelefone) {
		this.cdtelefone = cdtelefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setTelefonetipo(Telefonetipo telefonetipo) {
		this.telefonetipo = telefonetipo;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Telefone))
			return false;
		final Telefone other = (Telefone) obj;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if(StringUtils.isEmpty(this.telefone)){
			return "";
		} else {
			try {
				StringBuilder builder = new StringBuilder(this.telefone);
				int size = this.telefone.length();
//				builder.insert(size==10?6:4, '-');
				if(size==10){
					builder.insert(6, '-');
				}else if(size == 11 && !builder.toString().contains("-")){
					builder.insert(7, '-');
				}else if(size == 8 && !builder.toString().contains("-")){
					builder.insert(4, '-');
				}
				if (size == 10) {
					builder.insert(2, ' ');
					builder.insert(2, ')');
					builder.insert(0, '(');
				}
				return builder.toString();
			} catch (IndexOutOfBoundsException e) {
				//System.out.println("\n************************\nTelefone inv�lido: "+value);
				return this.telefone;
			}
		}
	}
	
	@Transient
	public String getTelefonesComTipo(){
		if(StringUtils.isNotBlank(getTelefone())){
			return getTelefone() + (getTelefonetipo() != null ? "(" + getTelefonetipo().getNome() + ") " : "");
		}
		return null;
	}
}
