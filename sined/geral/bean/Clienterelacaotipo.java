package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_clienterelacaotipo", sequenceName = "sq_clienterelacaotipo")
@DisplayName("Tipo de Rela��o")
public class Clienterelacaotipo implements Log{
	
	protected Integer cdclienterelacaotipo;
	protected String descricao;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienterelacaotipo")
	public Integer getCdclienterelacaotipo() {
		return cdclienterelacaotipo;
	}
	
	public void setCdclienterelacaotipo(Integer cdclienterelacaotipo) {
		this.cdclienterelacaotipo = cdclienterelacaotipo;
	}
	
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {
		return dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
}
