package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_notadocumento", sequenceName = "sq_notadocumento")
public class NotaDocumento implements Log {

	protected Integer cdNotaDocumento;
	protected Nota nota;
	protected Documento documento;
	protected Integer ordem;
	protected Boolean fromwebservice = Boolean.FALSE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public NotaDocumento() {
		super();
	}
	
	public NotaDocumento(Nota nota, Documento documento) {
		this.nota = nota;
		this.documento = documento;
	}
	
	public NotaDocumento(Documento documento, Nota nota, Integer ordem, Boolean fromwebservice) {
		this.nota = nota;
		this.documento = documento;
		this.ordem = ordem;
		this.fromwebservice = fromwebservice;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notadocumento")
	public Integer getCdNotaDocumento() {
		return cdNotaDocumento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Nota getNota() {
		return nota;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	@DisplayName("Parcela")
	public Integer getOrdem() {
		return ordem;
	}
	
	public Boolean getFromwebservice() {
		return fromwebservice;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setFromwebservice(Boolean fromwebservice) {
		this.fromwebservice = fromwebservice;
	}
	
	public void setCdNotaDocumento(Integer cdNotaDocumento) {
		this.cdNotaDocumento = cdNotaDocumento;
	}
	
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
