package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Vcategoria {

	protected Integer cdcategoria;
	protected String nome;
	protected String identificador;
	protected Integer nivel;
	protected String arvorepai;
	
	public Vcategoria() {
		// TODO Auto-generated constructor stub
	}
	
	public Vcategoria(String _identificador) {
		this.identificador = _identificador;
	}
	
	@Id
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	public String getNome() {
		return nome;
	}
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("N�vel")
	public Integer getNivel() {
		return nivel;
	}
	public String getArvorepai() {
		return arvorepai;
	}
	public void setArvorepai(String arvorepai) {
		this.arvorepai = arvorepai;
	}
	public void setCdcategoria(Integer cdcontagerencial) {
		this.cdcategoria = cdcontagerencial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	    
}
