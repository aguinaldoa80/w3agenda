package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_producaoagendamaterialmateriaprima", sequenceName = "sq_producaoagendamaterialmateriaprima")
@DisplayName("Mat�ria-prima")
public class Producaoagendamaterialmateriaprima {

	protected Integer cdproducaoagendamaterialmateriaprima;
	protected Producaoagendamaterial producaoagendamaterial;
	protected Material material;
	protected Double qtdeprevista;
	protected Materialproducao materialproducao;
	protected Integer agitacao;
	protected Integer quantidadepercentual;
	protected Loteestoque loteestoque;
	protected Unidademedida unidademedida;
	protected Double fracaounidademedida;
	protected Double qtdereferenciaunidademedida;
	protected Producaoetapanome producaoetapanome; 
	
	//CONTROLE
	protected Boolean naocalcularqtdeprevista;
	
	//TRANSIENT
	protected Boolean existematerialsimilar;
	protected Material aux_material;
	protected Loteestoque aux_loteestoque;
	protected Unidademedida unidademedidaTrans;
	protected Unidademedida unidademedidaAntiga;
	
	public Producaoagendamaterialmateriaprima(){}
	
	public Producaoagendamaterialmateriaprima(Materialproducao materialproducao) {
		this.material = materialproducao.getMaterial();
		this.qtdeprevista = materialproducao.getConsumo();
		this.naocalcularqtdeprevista = materialproducao.getNaocalcularqtdeprevista();
		if(materialproducao.getCdmaterialproducao() != null){
			this.materialproducao = materialproducao;
		}
		this.loteestoque = materialproducao.getLoteestoque();
		this.agitacao = materialproducao.getAgitacao();
		this.quantidadepercentual = materialproducao.getQuantidadepercentual();
		this.unidademedida = materialproducao.getUnidademedida();
		this.unidademedidaTrans = materialproducao.getUnidademedida();
		this.unidademedidaAntiga = materialproducao.getUnidademedida();
		this.producaoetapanome = materialproducao.getProducaoetapanome();
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendamaterialmateriaprima")
	public Integer getCdproducaoagendamaterialmateriaprima() {
		return cdproducaoagendamaterialmateriaprima;
	}
	@JoinColumn(name="cdproducaoagendamaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagendamaterial getProducaoagendamaterial() {
		return producaoagendamaterial;
	}
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Qtde Prevista")
	public Double getQtdeprevista() {
		return qtdeprevista;
	}
	public Boolean getNaocalcularqtdeprevista() {
		return naocalcularqtdeprevista;
	}
	@JoinColumn(name="cdmaterialproducao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Materialproducao getMaterialproducao() {
		return materialproducao;
	}
	@DisplayName("Agita��o")
	public Integer getAgitacao() {
		return agitacao;
	}
	@DisplayName("Qtde Percentual")
	public Integer getQuantidadepercentual() {
		return quantidadepercentual;
	}
	@DisplayName("Lote")
	@JoinColumn(name="cdloteestoque")
	@ManyToOne(fetch=FetchType.LAZY)
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@JoinColumn(name="cdunidademedida")
	@ManyToOne(fetch=FetchType.LAZY)
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	@DisplayName("Etapa de produ��o")
	@JoinColumn(name="cdproducaoetapanome")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoetapanome getProducaoetapanome() {
		return producaoetapanome;
	}

	public Double getFracaounidademedida() {
		return fracaounidademedida;
	}

	public Double getQtdereferenciaunidademedida() {
		return qtdereferenciaunidademedida;
	}
	
	public void setCdproducaoagendamaterialmateriaprima(Integer cdproducaoagendamaterialmateriaprima) {
		this.cdproducaoagendamaterialmateriaprima = cdproducaoagendamaterialmateriaprima;
	}
	public void setProducaoagendamaterial(Producaoagendamaterial producaoagendamaterial) {
		this.producaoagendamaterial = producaoagendamaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	public void setNaocalcularqtdeprevista(Boolean naocalcularqtdeprevista) {
		this.naocalcularqtdeprevista = naocalcularqtdeprevista;
	}
	public void setMaterialproducao(Materialproducao materialproducao) {
		this.materialproducao = materialproducao;
	}

	public void setFracaounidademedida(Double fracaounidademedida) {
		this.fracaounidademedida = fracaounidademedida;
	}
	
	public void setQtdereferenciaunidademedida(Double qtdereferenciaunidademedida) {
		this.qtdereferenciaunidademedida = qtdereferenciaunidademedida;
	}
	
	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	@Transient
	public Material getAux_material() {
		return aux_material;
	}
	@Transient
	public Loteestoque getAux_loteestoque() {
		return aux_loteestoque;
	}
	
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	public void setAux_material(Material auxMaterial) {
		aux_material = auxMaterial;
	}
	public void setAux_loteestoque(Loteestoque auxLoteestoque) {
		aux_loteestoque = auxLoteestoque;
	}
	public void setAgitacao(Integer agitacao) {
		this.agitacao = agitacao;
	}
	public void setQuantidadepercentual(Integer quantidadepercentual) {
		this.quantidadepercentual = quantidadepercentual;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setProducaoetapanome(Producaoetapanome producaoetapanome) {
		this.producaoetapanome = producaoetapanome;
	}
	
	@Transient
	public Unidademedida getUnidademedidaTrans() {
		return unidademedidaTrans;
	}
	
	public void setUnidademedidaTrans(Unidademedida unidademedidaTrans) {
		this.unidademedidaTrans = unidademedidaTrans;
	}

	@Transient
	public Unidademedida getUnidademedidaAntiga() {
		return unidademedidaAntiga;
	}

	public void setUnidademedidaAntiga(Unidademedida unidademedidaAntiga) {
		this.unidademedidaAntiga = unidademedidaAntiga;
	}
}
