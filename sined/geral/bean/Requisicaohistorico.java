package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_requisicaohistorico", sequenceName = "sq_requisicaohistorico")
@DisplayName("Hist�rico da requisi��o")
public class Requisicaohistorico implements Log {

	protected Integer cdrequisicaohistorico;
	protected String observacao;
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	protected Requisicao requisicao;
	protected Arquivo arquivo;
	protected Usuario usuarioAltera; //Para n�o precisar de carregar fora da query
	protected Boolean observacaointerna;
	
	// Transient
	protected Pessoa pessoa;
	
	public Requisicaohistorico(){
	}

	public Requisicaohistorico(Arquivo arquivo, String observacao, Requisicao requisicao){
		this.arquivo = arquivo;
		if(requisicao.getAgendainteracao() != null){
			this.observacao = observacao != null ? observacao : " " + " \nC�digo Agenda Intera��o: " +requisicao.getAgendainteracao().getCdagendainteracao() ;
		}else{
			this.observacao = observacao != null ? observacao : " " ;
		}
		this.requisicao = requisicao;
		this.observacaointerna = requisicao.getObservacaoInterna();
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_requisicaohistorico")
	public Integer getCdrequisicaohistorico() {
		return cdrequisicaohistorico;
	}
	
	@DisplayName("Observa��o")
	@DescriptionProperty
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	
	@DisplayName("Arquivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	public void setCdrequisicaohistorico(Integer cdrequisicaohistorico) {
		this.cdrequisicaohistorico = cdrequisicaohistorico;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}

	@Transient
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	@Transient
	public String getObservacaovendaTrans(){
		StringBuilder obs = new StringBuilder();
		if(this.getObservacao() != null && !"".equals(this.getObservacao())){
			try {
				obs.append(this.getObservacao().substring(0, this.getObservacao().indexOf("<")-1));
				obs.append(" ").append(this.getObservacao().substring(this.getObservacao().indexOf(">")+1, this.getObservacao().indexOf("</")));
			} catch (Exception e) {}
		}
		
		return obs.toString();
	}
	
	@Transient
	public String getObservacaoOsTrans(){
		StringBuilder obs = new StringBuilder();
		if(this.getObservacao() != null && !"".equals(this.getObservacao()) ){
			if(this.getObservacao().indexOf("<")>-1 && this.getObservacao().indexOf("</")>-1){
				try {
					obs.append(this.getObservacao().substring(0, this.getObservacao().indexOf("<")-1));
					obs.append(" ").append(this.getObservacao().substring(this.getObservacao().indexOf(">")+1, this.getObservacao().indexOf("</")));
				} catch (Exception e) {}
			}else{
				obs.append(this.getObservacao());
			}
		}
		
		return obs.toString();
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdUsuarioAltera", insertable=false, updatable=false)
	public Usuario getUsuarioAltera() {
		return usuarioAltera;
	}

	public void setUsuarioAltera(Usuario usuarioAltera) {
		this.usuarioAltera = usuarioAltera;
	}

	public Boolean getObservacaointerna() {
		return observacaointerna;
	}

	public void setObservacaointerna(Boolean observacaointerna) {
		this.observacaointerna = observacaointerna;
	}
}