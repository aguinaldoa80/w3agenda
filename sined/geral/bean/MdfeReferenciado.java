package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfereferenciado", sequenceName = "sq_mdfereferenciado")
@DisplayName("MDF-e referenciado")
public class MdfeReferenciado {

	protected Integer cdMdfeReferenciado;
	protected Mdfe mdfe;
	protected String chaveAcesso;
	protected Boolean indicadorReentrega;
	protected String idMdfeReferenciado;
	protected Municipio municipio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfereferenciado")
	public Integer getCdMdfeReferenciado() {
		return cdMdfeReferenciado;
	}
	public void setCdMdfeReferenciado(Integer cdMdfeReferenciado) {
		this.cdMdfeReferenciado = cdMdfeReferenciado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=44)
	@DisplayName("Chave de acesso")
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	@DisplayName("Indicador de reentrega")
	public Boolean getIndicadorReentrega() {
		return indicadorReentrega;
	}
	public void setIndicadorReentrega(Boolean indicadorReentrega) {
		this.indicadorReentrega = indicadorReentrega;
	}
	
	@DisplayName("ID do MDF-e referenciado")
	public String getIdMdfeReferenciado() {
		return idMdfeReferenciado;
	}
	public void setIdMdfeReferenciado(String idMdfeReferenciado) {
		this.idMdfeReferenciado = idMdfeReferenciado;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> getListaInformacaoUnidadeTransporteMdfeReferenciado(MdfeReferenciado mdfeReferenciado, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeTransporteMdfeReferenciado> list = new ArrayList<MdfeInformacaoUnidadeTransporteMdfeReferenciado>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteMdfeReferenciado())){
			for(MdfeInformacaoUnidadeTransporteMdfeReferenciado item : mdfe.getListaInformacaoUnidadeTransporteMdfeReferenciado()){
				if(item.getIdMdfeReferenciado().equals(mdfeReferenciado.getIdMdfeReferenciado())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeProdutoPerigosoMdfeReferenciado> getListaProdutoPerigosoMdfeReferenciado(MdfeReferenciado mdfeReferenciado, Mdfe mdfe) {
		List<MdfeProdutoPerigosoMdfeReferenciado> list = new ArrayList<MdfeProdutoPerigosoMdfeReferenciado>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteMdfeReferenciado())){
			for(MdfeProdutoPerigosoMdfeReferenciado item : mdfe.getListaProdutoPerigosoMdfeReferenciado()){
				if(item.getIdMdfeReferenciado().equals(mdfeReferenciado.getIdMdfeReferenciado())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@DisplayName("Município de descarregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}