package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipometacomercial;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_metacomercial", sequenceName = "sq_metacomercial")
public class Metacomercial implements Log{
	
	protected Integer cdmetacomercial;
	protected String nome;
	protected Date dtinicio;
	protected Date dtfim;
	protected Tipometacomercial tipometacomercial;
	protected Money meta;
	protected List<Metacomercialitem> listaMetacomercialitem;
	
	//TRANSIENT
	protected String listametacomercial;
	protected String dtinicioAux;
	protected String dtfimAux;
	
	//LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_metacomercial")
	public Integer getCdmetacomercial() {
		return cdmetacomercial;
	}
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("V�lido de")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("V�lido at�")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@DisplayName("Tipo de meta")
	public Tipometacomercial getTipometacomercial() {
		return tipometacomercial;
	}
	@Required
	public Money getMeta() {
		return meta;
	}
	@DisplayName("Colaboradores")
	@OneToMany(mappedBy="metacomercial")
	public List<Metacomercialitem> getListaMetacomercialitem() {
		return listaMetacomercialitem;
	}
	@Transient
	public String getListametacomercial() {
		return listametacomercial;
	}
	public void setListametacomercial(String listametacomercial) {
		this.listametacomercial = listametacomercial;
	}
	public void setCdmetacomercial(Integer cdmetacomercial) {
		this.cdmetacomercial = cdmetacomercial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setTipometacomercial(Tipometacomercial tipometacomercial) {
		this.tipometacomercial = tipometacomercial;
	}
	public void setMeta(Money meta) {
		this.meta = meta;
	}
	public void setListaMetacomercialitem(List<Metacomercialitem> listaMetacomercialitem) {
		this.listaMetacomercialitem = listaMetacomercialitem;
	}

	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("M�s inicial")
	@Transient
	public String getDtinicioAux() {
		if(this.dtinicio != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtinicio);
		return null;
	}
	
	public void setDtinicioAux(String mesanoAux) {
		try {
			if(mesanoAux != null){
				this.dtinicio = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			} else {
				this.dtinicio = null;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
	@DisplayName("M�s final")
	@Transient
	public String getDtfimAux() {
		if(this.dtfim != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtfim);
		return null;
	}
	
	public void setDtfimAux(String mesanoAux) {
		try {
			if(mesanoAux != null){
				this.dtfim = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			} else {
				this.dtfim = null;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
}