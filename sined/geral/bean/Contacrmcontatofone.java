package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_contacrmcontatofone", sequenceName = "sq_contacrmcontatofone")
public class Contacrmcontatofone {
	
	protected Integer cdcontacrmcontatofone;
	protected Telefonetipo telefonetipo;
	protected Contacrmcontato contacrmcontato;
	protected String telefone;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrmcontatofone")
	public Integer getCdcontacrmcontatofone() {
		return cdcontacrmcontatofone;
	}
	
	@DisplayName("Tipo de telefone")
	@JoinColumn(name="cdtelefonetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Telefonetipo getTelefonetipo() {
		return telefonetipo;
	}
	
	@DisplayName("Contato")
	@JoinColumn(name="cdcontacrmcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Telefone")
	public String getTelefone() {
		return telefone;
	}
	
	
	
	
	public void setCdcontacrmcontatofone(Integer cdcontacrmcontatofone) {
		this.cdcontacrmcontatofone = cdcontacrmcontatofone;
	}
	public void setTelefonetipo(Telefonetipo telefonetipo) {
		this.telefonetipo = telefonetipo;
	}
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	
	
	
}
