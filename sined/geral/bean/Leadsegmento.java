package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_leadsegmento", sequenceName = "sq_leadsegmento")
public class Leadsegmento {
	
	protected Integer cdleadsegmento;
	protected Segmento segmento;
	protected Lead lead;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_leadsegmento")
	public Integer getCdleadsegmento() {
		return cdleadsegmento;
	}
	
	@DisplayName("Segmento")
	@JoinColumn(name="cdsegmento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Segmento getSegmento() {
		return segmento;
	}
	
	@DisplayName("Lead")
	@JoinColumn(name="cdlead")
	@ManyToOne(fetch=FetchType.LAZY)
	public Lead getLead() {
		return lead;
	}
	
	public void setCdleadsegmento(Integer cdleadsegmento) {
		this.cdleadsegmento = cdleadsegmento;
	}
	
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	
	public void setLead(Lead lead) {
		this.lead = lead;
	}

}
