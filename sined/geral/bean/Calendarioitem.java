package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_calendarioitem", sequenceName = "sq_calendarioitem")
public class Calendarioitem implements Log{

	protected Integer cdcalendarioitem;
	protected Calendario calendario;
	protected String descricao;
	protected Date dtferiado;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_calendarioitem")
	public Integer getCdcalendarioitem() {
		return cdcalendarioitem;
	}
	
	public void setCdcalendarioitem(Integer cdcalendarioitem) {
		this.cdcalendarioitem = cdcalendarioitem;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);;
	}
	
	@Required
	@DisplayName("Data")
	public Date getDtferiado() {
		return dtferiado;
	}
	
	public void setDtferiado(Date dtferiado) {
		this.dtferiado = dtferiado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcalendario")
	public Calendario getCalendario() {
		return calendario;
	}
	
	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
}
