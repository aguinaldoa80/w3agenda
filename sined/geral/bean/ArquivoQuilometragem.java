package br.com.linkcom.sined.geral.bean;


public class ArquivoQuilometragem {
	
	private Arquivo arquivo;
	
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
}