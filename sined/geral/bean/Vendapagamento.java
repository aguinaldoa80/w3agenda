package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@SequenceGenerator(name = "sq_vendapagamento", sequenceName = "sq_vendapagamento")
public class Vendapagamento {

	protected Integer cdvendapagamento;
	protected Venda venda;
	protected Documento documento;
	protected Documentotipo documentotipo;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected String numero;
	protected Money valororiginal;
	protected Date dataparcela;
	protected Money valorjuros;
	protected Boolean prazomedio;
	protected Documento documentoantecipacao;
	protected Cheque cheque;
	protected String emitente;
	protected String cpfcnpj;
	
	protected String autorizacao;
	protected String bandeira;
	protected String adquirente; 
	
	
	//TRANSIENT
	protected Double taxa;
	protected Date dataparcelaEmissaonota;
	protected String numeroEmissaonota;
	protected Conta contadestino;
	protected Boolean podeGerarCheque;
	
	public Vendapagamento(){
	}

	public Vendapagamento(Venda venda, Documento documento, String banco, Integer agencia, String conta, String numero, Money valorOriginal){
		this.venda = venda;
		this.documento = documento;
		this.banco = banco != null && !banco.equals("") ? Integer.valueOf(banco) : null;
		this.agencia = agencia;
		this.conta = conta != null && !conta.equals("") ? Integer.valueOf(conta) : null;
		this.numero = numero;
		this.valororiginal = valorOriginal;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendapagamento")
	public Integer getCdvendapagamento() {
		return cdvendapagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public Integer getBanco() {
		return banco;
	}
	@DisplayName("Ag�ncia")
	public Integer getAgencia() {
		return agencia;
	}
	@MaxLength(9)
	public Integer getConta() {
		return conta;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public Money getValororiginal() {
		return valororiginal;
	}
	@DisplayName("Vencimento")
	public Date getDataparcela() {
		return dataparcela;
	}	
	@DisplayName("Considerar prazo m�dio")
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoantecipacao")
	@DisplayName("Pagamento adiantado")
	public Documento getDocumentoantecipacao() {
		return documentoantecipacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	@DisplayName("Cheque")
	public Cheque getCheque() {
		return cheque;
	}
	public String getAutorizacao() {
		return autorizacao;
	}
	public String getBandeira() {
		return bandeira;
	}
	public String getAdquirente() {
		return adquirente;
	}
	public void setAutorizacao(String autorizacao) {
		this.autorizacao = autorizacao;
	}
	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}
	public void setAdquirente(String adquirente) {
		this.adquirente = adquirente;
	}
	public void setPodeGerarCheque(Boolean podeGerarCheque) {
		this.podeGerarCheque = podeGerarCheque;
	}

	public void setCdvendapagamento(Integer cdvendapagamento) {
		this.cdvendapagamento = cdvendapagamento;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setValororiginal(Money valororiginal) {
		this.valororiginal = valororiginal;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public void setDocumentoantecipacao(Documento documentoantecipacao) {
		this.documentoantecipacao = documentoantecipacao;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

	@Transient
	public Double getTaxa() {
		return taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}

	@Transient
	public Date getDataparcelaEmissaonota() {
		return dataparcelaEmissaonota;
	}
	@Transient
	public String getNumeroEmissaonota() {
		return numeroEmissaonota;
	}
	public void setDataparcelaEmissaonota(Date dataparcelaEmissaonota) {
		this.dataparcelaEmissaonota = dataparcelaEmissaonota;
	}
	public void setNumeroEmissaonota(String numeroEmissaonota) {
		this.numeroEmissaonota = numeroEmissaonota;
	}
	
	@Transient
	public Conta getContadestino() {
		return contadestino;
	}
	
	public void setContadestino(Conta contadestino) {
		this.contadestino = contadestino;
	}
	
	@DisplayName("Juros Parcela")
	public Money getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}

	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	
	@DisplayName("CNPJ/CPF do emitente")
	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	@Transient
	public Boolean getPodeGerarCheque() {
		if(documentotipo == null || documentotipo.getCddocumentotipo() == null || (cheque != null && cheque.getCdcheque() != null)){
			podeGerarCheque = false;
		}else{
			podeGerarCheque = StringUtils.isNotEmpty(this.getNumero()) && (!Boolean.TRUE.equals(documentotipo.getCartao()));
		}
		return podeGerarCheque;
	}
}
