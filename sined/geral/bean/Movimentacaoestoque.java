package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Movimentacaoanimalmotivo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Movimenta��o de estoque")
@SequenceGenerator(name = "sq_movimentacaoestoque", sequenceName = "sq_movimentacaoestoque")
public class Movimentacaoestoque implements Log, PermissaoProjeto, ValidateLoteestoqueObrigatorioInterface{

	protected Integer cdmovimentacaoestoque;
	protected Material material;
	protected Material materialOrigem;
	protected Movimentacaoestoquetipo movimentacaoestoquetipo;
	protected Double comprimento;
	protected Double qtde;
	protected Date dtmovimentacao;
	protected Localarmazenagem localarmazenagem;
	protected Loteestoque loteestoque;
	protected Empresa empresa;
	protected Movimentacaoestoqueorigem movimentacaoestoqueorigem;
	protected Materialclasse materialclasse;
	protected Date dtcancelamento;
	protected Double valor;
	protected String observacao;
	protected Projeto projeto;
	protected Double valorcusto;
	protected Double valorCustoPorEmpresa;
	protected Movimentacaoanimalmotivo movimentacaoanimalmotivo;
	protected Double pesomedio;
	protected List<MovimentacaoEstoqueHistorico> listaMovimentacaoEstoqueHistorico;
	
	protected Rateio rateio;

//	TRANSIENT
	protected Double qtdeTotal;
	protected Double qtdeConsumo;
	protected Unidademedida unidademedidatrans;
	protected Integer cdmaterial;
	protected Integer cdlocalarmazenagem;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected String origemreport;
	protected Notafiscalproduto notafiscalproduto;
	protected Notafiscalprodutoitem notaFiscalProdutoItem;
	protected List<Movimentacaoestoque> listaMovimentacaoEstoqueGradeItem;
	protected String tipobuscamaterial;
	protected Boolean itemutilizado;
	protected Contagerencial contaGerencial;
	protected Centrocusto centrocusto;
	protected Projeto projetoRateio;
	protected Empresa empresaAnterior;
	protected String indentificacaoMaterial;
	
	protected Money valorTotal;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	protected Boolean movimentacaoAvulsa;
	
	public Movimentacaoestoque(){
	}
	
	public Movimentacaoestoque(Integer cdmovimentacaoestoque) {
		this.cdmovimentacaoestoque = cdmovimentacaoestoque;
	}

	public Movimentacaoestoque(Material material, Movimentacaoestoquetipo movimentacaoestoquetipo, Double qtde, Localarmazenagem localarmazenagem,
							   Empresa empresa, Movimentacaoestoqueorigem movimentacaoestoqueorigem, Materialclasse materialclasse){
		this.material = material;
		this.movimentacaoestoquetipo = movimentacaoestoquetipo;
		this.qtde = qtde;
		this.localarmazenagem = localarmazenagem;
		this.empresa = empresa;
		this.movimentacaoestoqueorigem = movimentacaoestoqueorigem;
		this.materialclasse = materialclasse;
		this.dtmovimentacao = new Date(System.currentTimeMillis());
	}
	
	public Movimentacaoestoque(Material material, Movimentacaoestoquetipo movimentacaoestoquetipo, Double qtde, Localarmazenagem localarmazenagem,
			   Empresa empresa, Movimentacaoestoqueorigem movimentacaoestoqueorigem, Materialclasse materialclasse, Projeto projeto){
		this.material = material;
		this.movimentacaoestoquetipo = movimentacaoestoquetipo;
		this.qtde = qtde;
		this.localarmazenagem = localarmazenagem;
		this.empresa = empresa;
		this.movimentacaoestoqueorigem = movimentacaoestoqueorigem;
		this.materialclasse = materialclasse;
		this.dtmovimentacao = new Date(System.currentTimeMillis());
		this.projeto = projeto;
	}
	
	public Movimentacaoestoque(Movimentacaoestoque me){
		this.material = me.getMaterial();
		this.movimentacaoestoquetipo = me.getMovimentacaoestoquetipo();
		this.qtde = me.getQtde();
		this.localarmazenagem = me.getLocalarmazenagem();
		this.empresa = me.getEmpresa();
		this.movimentacaoestoqueorigem = me.getMovimentacaoestoqueorigem();
		this.materialclasse = me.getMaterialclasse();
		this.dtmovimentacao = new Date(System.currentTimeMillis());
		this.projeto = me.getProjeto();
	}
	
	public Movimentacaoestoque(Integer cdmaterial, String identificacaoMaterial, Date dtmovimentacao, Double qtde) {
		super();
		this.cdmaterial = cdmaterial;
		this.indentificacaoMaterial = identificacaoMaterial;
		this.dtmovimentacao = dtmovimentacao;
		this.qtde = qtde;
	}

	@Id
	@DisplayName("Movimenta��o")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_movimentacaoestoque")
	public Integer getCdmovimentacaoestoque() {
		return cdmovimentacaoestoque;
	}
	@Required
	@DisplayName("Data")
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoestoquetipo")
	@DisplayName("Tipo")
	public Movimentacaoestoquetipo getMovimentacaoestoquetipo() {
		return movimentacaoestoquetipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoestoqueorigem")
	@DisplayName("V�nculo")
	public Movimentacaoestoqueorigem getMovimentacaoestoqueorigem() {
		return movimentacaoestoqueorigem;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DisplayName("Produto/EPI/Servi�o")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Comprimento")
	public Double getComprimento() {
		return comprimento;
	}
	@Required
	@DisplayName("Quantidade")
	public Double getQtde() {
		return qtde;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	@DisplayName("Local")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	@DisplayName("Lote")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	@DisplayName("Necessidade")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	@DisplayName("Cancel.")
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	public Double getValor() {
		return valor;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="movimentacaoEstoque")
	public List<MovimentacaoEstoqueHistorico> getListaMovimentacaoEstoqueHistorico() {
		return listaMovimentacaoEstoqueHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}
	
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	
	public void setListaMovimentacaoEstoqueHistorico(List<MovimentacaoEstoqueHistorico> listaMovimentacaoEstoqueHistorico) {
		this.listaMovimentacaoEstoqueHistorico = listaMovimentacaoEstoqueHistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}
	public void setMovimentacaoestoqueorigem(
			Movimentacaoestoqueorigem movimentacaoestoqueorigem) {
		this.movimentacaoestoqueorigem = movimentacaoestoqueorigem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMovimentacaoestoquetipo(
			Movimentacaoestoquetipo movimentacaoestoquetipo) {
		this.movimentacaoestoquetipo = movimentacaoestoquetipo;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdmovimentacaoestoque(Integer cdmovimentacaoestoque) {
		this.cdmovimentacaoestoque = cdmovimentacaoestoque;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public String subQueryProjeto() {
		return "select movimentacaoestoquesubQueryProjeto.cdmovimentacaoestoque " +
				"from Movimentacaoestoque movimentacaoestoquesubQueryProjeto " +
				"left outer join movimentacaoestoquesubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public Double getQtdeTotal() {
		return qtdeTotal;
	}

	public void setQtdeTotal(Double qtdeTotal) {
		this.qtdeTotal = qtdeTotal;
	}

	@Transient
	public Double getQtdeConsumo() {
		return qtdeConsumo;
	}

	public void setQtdeConsumo(Double qtdeConsumo) {
		this.qtdeConsumo = qtdeConsumo;
	}
	
	@Transient
	public String getMaterialDescricao(){
		return getMaterial() != null ? getMaterial().getNome() : "";
	}
	
	@Transient
	@DisplayName("Local")
	public String getLocalArmazenagemDescricao(){
		return getLocalarmazenagem() != null ? getLocalarmazenagem().getNome() : "";
	}
	@Required
	@Transient
	@DisplayName("Unidade de Medida")
	public Unidademedida getUnidademedidatrans() {
		return unidademedidatrans;
	}

	public void setUnidademedidatrans(Unidademedida unidademedidatrans) {
		this.unidademedidatrans = unidademedidatrans;
	}	
	
	@Transient
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	@Transient
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}

	@Transient
	public String getOrigemreport() {
		StringBuilder s = new StringBuilder();
		StringBuilder fornecedor = new StringBuilder();
		if(this.getMovimentacaoestoqueorigem() != null){
			if(this.getMovimentacaoestoqueorigem().getVenda() != null){
				s.append("Venda ").append(this.getMovimentacaoestoqueorigem().getVenda().getCdvenda()).append(": ");
				if(this.getMovimentacaoestoqueorigem().getVenda().getCliente() != null){
					s.append(this.getMovimentacaoestoqueorigem().getVenda().getCliente().getNome());
				}

			}else if(this.getMovimentacaoestoqueorigem().getEntrega() != null){
				List<Entregadocumento> listaEd = this.getMovimentacaoestoqueorigem().getEntrega().getListaEntregadocumento();
				s.append("Compra ").append(this.getMovimentacaoestoqueorigem().getEntrega().getCdentrega()).append(": ");
				if(listaEd != null && !listaEd.isEmpty()){
					for(Entregadocumento ed : listaEd){
						if(ed.getFornecedor() != null){
							if(ed.getListaEntregamaterial() != null && !ed.getListaEntregamaterial().isEmpty()){
								for(Entregamaterial em : ed.getListaEntregamaterial()){
									if(em.getMaterial() != null && this.getMaterial() != null && 
									   em.getMaterial().getCdmaterial() != null && this.getMaterial().getCdmaterial() != null && 
									   em.getMaterial().getCdmaterial().equals(this.getMaterial().getCdmaterial())){
										if(!"".equals(fornecedor.toString())) fornecedor.append(" / ");
										fornecedor.append(ed.getFornecedor().getNome());
									}
								}
							}
						}
					}
					s.append(fornecedor.toString());
				}

			}else if(this.getMovimentacaoestoqueorigem().getRomaneio() != null){
				s.append("Romaneio: ").append(this.getMovimentacaoestoqueorigem().getRomaneio().getCdromaneio());
			}else if(this.getMovimentacaoestoqueorigem().getRomaneiomanual() != null && this.getMovimentacaoestoqueorigem().getRomaneiomanual()){
				s.append("Romaneio manual");
			}else if (Boolean.TRUE.equals(this.getMovimentacaoestoqueorigem().getConsumo())){
				s.append("Consumo");
			}
		}
		return s.toString();
	}
	
	@Transient
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}

	@Transient
	public List<Movimentacaoestoque> getListaMovimentacaoEstoqueGradeItem() {
		return listaMovimentacaoEstoqueGradeItem;
	}

	public void setListaMovimentacaoEstoqueGradeItem(
			List<Movimentacaoestoque> listaMovimentacaoEstoqueGradeItem) {
		this.listaMovimentacaoEstoqueGradeItem = listaMovimentacaoEstoqueGradeItem;
	}

	@DisplayName("Valor do Custo")
	public Double getValorcusto() {
		return valorcusto;
	}
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}
	
	@DisplayName("Valor do Custo (Por Empresa)")
	public Double getValorCustoPorEmpresa() {
		return valorCustoPorEmpresa;
	}
	public void setValorCustoPorEmpresa(Double valorCustoPorEmpresa) {
		this.valorCustoPorEmpresa = valorCustoPorEmpresa;
	}
	
	@DisplayName("Motivo")
	public Movimentacaoanimalmotivo getMovimentacaoanimalmotivo() {
		return movimentacaoanimalmotivo;
	}
	public void setMovimentacaoanimalmotivo(
			Movimentacaoanimalmotivo movimentacaoanimalmotivo) {
		this.movimentacaoanimalmotivo = movimentacaoanimalmotivo;
	}
	
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}

	@Transient
	@DisplayName("Buscar Produto/EPI/Servi�o por")
	public String getTipobuscamaterial() {
		return tipobuscamaterial;
	}
	@Transient
	@DisplayName("Valor Total")
	public Money getValorTotal() {
		Money valorTotal = new Money();
		if((getValor() != null || getValorcusto() != null) && getQtde() != null){
			valorTotal = new Money((getValor() != null ? getValor() : getValorcusto()) * getQtde());
		}
		return valorTotal;
	}
	public void setTipobuscamaterial(String tipobuscamaterial) {
		this.tipobuscamaterial = tipobuscamaterial;
	}
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	@Transient
	public Boolean getItemutilizado() {
		return itemutilizado;
	}
	@Transient
	public Contagerencial getContaGerencial() {
		return contaGerencial;
	}
	@Transient
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@Transient
	public Projeto getProjetoRateio() {
		return projetoRateio;
	}
	@Transient
	public Empresa getEmpresaAnterior() {
		return empresaAnterior;
	}

	public void setItemutilizado(Boolean itemutilizado) {
		this.itemutilizado = itemutilizado;
	}
	public void setContaGerencial(Contagerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjetoRateio(Projeto projetoRateio) {
		this.projetoRateio = projetoRateio;
	}
	public void setEmpresaAnterior(Empresa empresaAnterior) {
		this.empresaAnterior = empresaAnterior;
	}
	
	@Transient
	public String getIndentificacaoMaterial() {
		return indentificacaoMaterial;
	}
	
	public void setIndentificacaoMaterial(String indentificacaoMaterial) {
		this.indentificacaoMaterial = indentificacaoMaterial;
	}
	
	@Transient
	public String getIdentificacaoOuCdmaterial(){
		String s = "";
		if(this.indentificacaoMaterial != null && !"".equals(this.indentificacaoMaterial)){
			s = this.indentificacaoMaterial;
		}else if(this.cdmaterial != null){
			s = this.cdmaterial.toString();
		}		
		return s;
	}

	@Transient
	public Notafiscalprodutoitem getNotaFiscalProdutoItem() {
		return notaFiscalProdutoItem;
	}

	public void setNotaFiscalProdutoItem(Notafiscalprodutoitem notaFiscalProdutoItem) {
		this.notaFiscalProdutoItem = notaFiscalProdutoItem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialorigem")
	public Material getMaterialOrigem() {
		return materialOrigem;
	}
	
	public void setMaterialOrigem(Material materialOrigem) {
		this.materialOrigem = materialOrigem;
	}
	
	@Transient
	public Boolean getMovimentacaoAvulsa(){
		return movimentacaoAvulsa;
	}
	public void setMovimentacaoAvulsa(Boolean movimentacaoAvulsa) {
		this.movimentacaoAvulsa = movimentacaoAvulsa;
	}
}