package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.enumeration.Coletaacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_coletahistorico", sequenceName = "sq_coletahistorico")
public class Coletahistorico implements Log {
	
	protected Integer cdcoletahistorico;
	protected Coleta coleta;
	protected Coletaacao acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_coletahistorico")
	public Integer getCdcoletahistorico() {
		return cdcoletahistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("A��o")
	public Coletaacao getAcao() {
		return acao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdcoletahistorico(Integer cdcoletahistorico) {
		this.cdcoletahistorico = cdcoletahistorico;
	}

	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}

	public void setAcao(Coletaacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@Transient
	public String getObservacaoMotivodevolucao(){
		Boolean existeMotivo = false;
		String motivo = "";
		String obs = "";
		if(getColeta() != null){
			for(ColetaMaterial coletamaterial : getColeta().getListaColetaMaterial()){
				if(getAcao() != null && getAcao().equals(Coletaacao.DEVOLVIDO) &&
						coletamaterial.getColeta() != null && coletamaterial.getColeta().getCdcoleta().equals(getColeta().getCdcoleta())){
					obs = getObservacao() != null ? getObservacao()+". " : "";
//					if(coletamaterial.getMotivodevolucao() != null){
//						motivo = coletamaterial.getMotivodevolucao().getDescricao();
//						existeMotivo = true;
//					}
					if(Hibernate.isInitialized(coletamaterial.getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(coletamaterial.getListaColetamaterialmotivodevolucao())){
						motivo = CollectionsUtil.listAndConcatenate(coletamaterial.getListaColetamaterialmotivodevolucao(), "motivodevolucao.descricao", ", ");
						existeMotivo = true;
					}
				}
			}
			if(existeMotivo){
				return obs + "Motivo da devolu��o: " + motivo;
			} else {
				return getObservacao() != null ? getObservacao() : " ";
			}
		}
		return getObservacao() != null ? getObservacao() : " ";
	}
	
	
}
