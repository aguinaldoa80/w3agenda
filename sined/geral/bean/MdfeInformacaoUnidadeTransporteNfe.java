package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeTipoTransporte;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeinformacaounidadetransportenfe", sequenceName = "sq_mdfeinformacaounidadetransportenfe")
public class MdfeInformacaoUnidadeTransporteNfe {

	protected Integer cdMdfeInformacaoUnidadeTransporteNfe;
	protected Mdfe mdfe;
	protected String idNfe;
	protected String idUnidadeTransporte;
	protected MdfeTipoTransporte tipoTransporte;
	protected String identificacaoTransporte;
	protected Money quantidadeRateada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeinformacaounidadetransportenfe")
	public Integer getCdMdfeInformacaoUnidadeTransporteNfe() {
		return cdMdfeInformacaoUnidadeTransporteNfe;
	}
	public void setCdMdfeInformacaoUnidadeTransporteNfe(
			Integer cdMdfeInformacaoUnidadeTransporte) {
		this.cdMdfeInformacaoUnidadeTransporteNfe = cdMdfeInformacaoUnidadeTransporte;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}

	@DisplayName("ID da NFe")
	public String getIdNfe() {
		return idNfe;
	}
	public void setIdNfe(String idNfe) {
		this.idNfe = idNfe;
	}
	
	@DisplayName("ID da unidade de transporte")
	public String getIdUnidadeTransporte() {
		return idUnidadeTransporte;
	}
	public void setIdUnidadeTransporte(String idUnidadeTransporte) {
		this.idUnidadeTransporte = idUnidadeTransporte;
	}
	
	@Required
	@DisplayName("Tipo de transporte")
	public MdfeTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}
	public void setTipoTransporte(MdfeTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação do transporte")
	public String getIdentificacaoTransporte() {
		return identificacaoTransporte;
	}
	public void setIdentificacaoTransporte(String identificacaoTransporte) {
		this.identificacaoTransporte = identificacaoTransporte;
	}

	@DisplayName("Quantidade rateada")
	public Money getQuantidadeRateada() {
		return quantidadeRateada;
	}
	public void setQuantidadeRateada(Money quantidadeRateada) {
		this.quantidadeRateada = quantidadeRateada;
	}
	
	@Transient
	public List<MdfeLacreUnidadeTransporteNfe> getListaLacreUnidadeTransporteNfe(MdfeInformacaoUnidadeTransporteNfe infUnTransNfe, Mdfe mdfe) {
		List<MdfeLacreUnidadeTransporteNfe> list = new ArrayList<MdfeLacreUnidadeTransporteNfe>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaLacreUnidadeTransporteNfe())){
			for(MdfeLacreUnidadeTransporteNfe item : mdfe.getListaLacreUnidadeTransporteNfe()){
				if(item.getIdUnidadeTransporte().equals(infUnTransNfe.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeCargaNfe> getListaInformacaoUnidadeCargaNfe(MdfeInformacaoUnidadeTransporteNfe infUnTransNfe, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeCargaNfe> list = new ArrayList<MdfeInformacaoUnidadeCargaNfe>(); 
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaNfe())){
			for(MdfeInformacaoUnidadeCargaNfe item : mdfe.getListaInformacaoUnidadeCargaNfe()){
				if(item.getIdUnidadeTransporte().equals(infUnTransNfe.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
