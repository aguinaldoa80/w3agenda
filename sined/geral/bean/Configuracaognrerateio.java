package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_configuracaognrerateio", sequenceName="sq_configuracaognrerateio")
public class Configuracaognrerateio {

	protected Integer cdconfiguracaognrerateio;
	protected Configuracaognre configuracaognre;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Double percentual;
	
	@Id
	@GeneratedValue(generator="sq_configuracaognrerateio", strategy=GenerationType.AUTO)
	public Integer getCdconfiguracaognrerateio() {
		return cdconfiguracaognrerateio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaognre")
	public Configuracaognre getConfiguracaognre() {
		return configuracaognre;
	}

	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")	
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")	
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@Required
	public Double getPercentual() {
		return percentual;
	}

	public void setCdconfiguracaognrerateio(Integer cdconfiguracaognrerateio) {
		this.cdconfiguracaognrerateio = cdconfiguracaognrerateio;
	}

	public void setConfiguracaognre(Configuracaognre configuracaognre) {
		this.configuracaognre = configuracaognre;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
}
