package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_colaboradorcategoriacnh", sequenceName = "sq_colaboradorcategoriacnh")
public class Colaboradorcategoriacnh implements Log{

	protected Integer cdcolaboradorcategoriacnh;
	protected Colaborador colaborador;
	protected Categoriacnh categoriacnh;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcategoriacnh")
	public Integer getCdcolaboradorcategoriacnh() {
		return cdcolaboradorcategoriacnh;
	}
	public void setCdcolaboradorcategoriacnh(Integer id) {
		this.cdcolaboradorcategoriacnh = id;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoriacnh")
	@Required
	public Categoriacnh getCategoriacnh() {
		return categoriacnh;
	}

	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setCategoriacnh(Categoriacnh categoriacnh) {
		this.categoriacnh = categoriacnh;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
