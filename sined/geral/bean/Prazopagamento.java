package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoiniciodata;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_prazopagamento",sequenceName="sq_prazopagamento")
@DisplayName("Prazo de Pagamento")
public class Prazopagamento implements Log{
	
	public static final Prazopagamento UNICA = new Prazopagamento(1, "�nica");
	
	protected Integer cdprazopagamento;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String nome;
	protected List<Prazopagamentoitem> listaPagamentoItem;
	protected Double juros;
	protected Boolean parcelasiguaisjuros;
	protected Boolean prazomedio;
	protected Money valorminimo;
	protected Integer prazomediodias;
	protected Integer diasparainiciar;
	protected Boolean dataparceladiautil;
	protected Boolean dataparcelaultimodiautilmes;
	protected Double valordescontomaximo;
	protected Boolean considerarjurosnota;
	protected Tipoiniciodata tipoiniciodata;
	protected Boolean avista;
	protected Boolean processoDeCompra;
	protected Boolean processoDeVenda;
	private List<ClientePrazoPagamento> listaClientePrazoPagamento;
	
	public Prazopagamento(){
		if(this.cdprazopagamento == null){
			this.ativo = true;
		}
	}
	
	public Prazopagamento(Integer cdprazopagamento){
		this.cdprazopagamento = cdprazopagamento;
	}
	
	public Prazopagamento(String nome, Boolean ativo, List<Prazopagamentoitem> listaPagamentoItem){
		this.nome = nome;
		this.ativo = ativo;
		this.listaPagamentoItem = listaPagamentoItem;
	}
	
	public Prazopagamento(int cdPrazoPagemento, String nome) {
		this.cdprazopagamento = cdPrazoPagemento;
		this.nome = nome;
	}

	@Id
	@GeneratedValue(generator="sq_prazopagamento",strategy=GenerationType.AUTO)
	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@DescriptionProperty
	@MaxLength(50)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy="prazopagamento")
	public List<Prazopagamentoitem> getListaPagamentoItem() {
		return listaPagamentoItem;
	}
	
	@DisplayName("Considerar Juros na Nota")
	public Boolean getConsiderarjurosnota() {
		return considerarjurosnota;
	}
	
	public void setConsiderarjurosnota(Boolean considerarjurosnota) {
		this.considerarjurosnota = considerarjurosnota;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
	
	/* API */
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaPagamentoItem(
			List<Prazopagamentoitem> listaPagamentoItem) {
		this.listaPagamentoItem = listaPagamentoItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdprazopagamento == null) ? 0 : cdprazopagamento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Prazopagamento other = (Prazopagamento) obj;
		if (cdprazopagamento == null) {
			if (other.cdprazopagamento != null)
				return false;
		} else if (!cdprazopagamento.equals(other.cdprazopagamento))
			return false;
		return true;
	}
	@DisplayName("Juros(M�s)")
	@MaxLength(8)
	public Double getJuros() {
		return juros;
	}
	@DisplayName("Parcelas Iguais com Juros")
	public Boolean getParcelasiguaisjuros() {
		return parcelasiguaisjuros;
	}

	public void setJuros(Double juros) {
		this.juros = juros;
	}

	public void setParcelasiguaisjuros(Boolean parcelasiguaisjuros) {
		this.parcelasiguaisjuros = parcelasiguaisjuros;
	}
	@DisplayName("Prazo m�dio")
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	@DisplayName("Valor m�nimo")
	public Money getValorminimo() {
		return valorminimo;
	}

	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public void setValorminimo(Money valorminimo) {
		this.valorminimo = valorminimo;
	}
	@DisplayName("Dias")
	@Transient
	public Integer getPrazomediodias() {
		return prazomediodias;
	}
	public void setPrazomediodias(Integer prazomediodias) {
		this.prazomediodias = prazomediodias;
	}

	@DisplayName("Dias para iniciar")
	public Integer getDiasparainiciar() {
		return diasparainiciar;
	}
	public void setDiasparainiciar(Integer diasparainiciar) {
		this.diasparainiciar = diasparainiciar;
	}
	@DisplayName("Desconto m�ximo na venda")
	public Double getValordescontomaximo() {
		return valordescontomaximo;
	}
	public void setValordescontomaximo(Double valordescontomaximo) {
		this.valordescontomaximo = valordescontomaximo;
	}
	@DisplayName("Iniciar a partir da")
	public Tipoiniciodata getTipoiniciodata() {
		return tipoiniciodata;
	}
	public void setTipoiniciodata(Tipoiniciodata tipoiniciodata) {
		this.tipoiniciodata = tipoiniciodata;
	}

	@Required
	@DisplayName("A Vista")
	public Boolean getAvista() {
		return avista;
	}
	public void setAvista(Boolean avista) {
		this.avista = avista;
	}	
	
	@DisplayName("Processo de Compra")
	public Boolean getProcessoDeCompra() {
		return processoDeCompra;
	}
	public void setProcessoDeCompra(Boolean processoDeCompra) {
		this.processoDeCompra = processoDeCompra;
	}
	
	@DisplayName("Processo de Venda")
	public Boolean getProcessoDeVenda() {
		return processoDeVenda;
	}
	public void setProcessoDeVenda(Boolean processoDeVenda) {
		this.processoDeVenda = processoDeVenda;
	}

	@DisplayName("Data da parcela deve ser um dia �til")
	public Boolean getDataparceladiautil() {
		return dataparceladiautil;
	}
	@DisplayName("Vencimento em dia �til dentro do m�s da parcela")
	public Boolean getDataparcelaultimodiautilmes() {
		return dataparcelaultimodiautilmes;
	}

	public void setDataparceladiautil(Boolean dataparceladiautil) {
		this.dataparceladiautil = dataparceladiautil;
	}
	public void setDataparcelaultimodiautilmes(Boolean dataparcelaultimodiautilmes) {
		this.dataparcelaultimodiautilmes = dataparcelaultimodiautilmes;
	}
	
	@OneToMany(mappedBy="prazopagamento")
	public List<ClientePrazoPagamento> getListaClientePrazoPagamento() {
		return listaClientePrazoPagamento;
	}
	
	public void setListaClientePrazoPagamento(List<ClientePrazoPagamento> listaClientePrazoPagamento) {
		this.listaClientePrazoPagamento = listaClientePrazoPagamento;
	}
}