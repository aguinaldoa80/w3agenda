package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_historicooperacao", sequenceName="sq_historicooperacao")
public class Historicooperacao implements Log{

	private Integer cdhistoricooperacao;
	private Tipohistoricooperacao tipohistoricooperacao;
	private String historico;
	private String historicocomplementar;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Boolean permitircomplementomanual;
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_historicooperacao")
	public Integer getCdhistoricooperacao() {
		return cdhistoricooperacao;
	}
	public void setCdhistoricooperacao(Integer cdhistoricooperacao) {
		this.cdhistoricooperacao = cdhistoricooperacao;
	}
	
	@Required
	@DisplayName("Tipo de lan�amento")
	public Tipohistoricooperacao getTipohistoricooperacao() {
		return tipohistoricooperacao;
	}
	public void setTipohistoricooperacao(Tipohistoricooperacao tipohistoricooperacao) {
		this.tipohistoricooperacao = tipohistoricooperacao;
	}

	@MaxLength(150)
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	@MaxLength(150)
	@DisplayName("Hist�rico complementar")
	public String getHistoricocomplementar() {
		return historicocomplementar;
	}
	public void setHistoricocomplementar(String historicocomplementar) {
		this.historicocomplementar = historicocomplementar;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Permitir complemento manual")
	public Boolean getPermitircomplementomanual() {
		return permitircomplementomanual;
	}
	public void setPermitircomplementomanual(Boolean permitircomplementomanual) {
		this.permitircomplementomanual = permitircomplementomanual;
	}
}
