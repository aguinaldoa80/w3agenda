package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;


@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornoidentificador",sequenceName="sq_bancoconfiguracaoretornoidentificador")
public class BancoConfiguracaoRetornoIdentificador {
	private Integer cdbancoConfiguracaoRetornoIdentificador;
	private BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	private BancoConfiguracaoCampoEnum tipo;
	private Integer posIni;
	private Integer tamanho;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornoidentificador")
	public Integer getCdbancoConfiguracaoRetornoIdentificador() {
		return cdbancoConfiguracaoRetornoIdentificador;
	}
	public void setCdbancoConfiguracaoRetornoIdentificador(
			Integer cdbancoConfiguracaoRetornoIdentificador) {
		this.cdbancoConfiguracaoRetornoIdentificador = cdbancoConfiguracaoRetornoIdentificador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}
	public BancoConfiguracaoCampoEnum getTipo() {
		return tipo;
	}
	public void setTipo(BancoConfiguracaoCampoEnum tipo) {
		this.tipo = tipo;
	}
	public Integer getPosIni() {
		return posIni;
	}
	public void setPosIni(Integer posIni) {
		this.posIni = posIni;
	}
	public Integer getTamanho() {
		return tamanho;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	
	@Transient
	public String getValorString(String linha){
		String valor = null;		
		if (getPosIni() != null && getTamanho() != null){
			Integer posFim = getPosIni() + getTamanho() - 1;
			if (linha.length() >= posFim){
				valor = linha.substring(getPosIni() - 1, posFim);
			}
		}		
		return valor != null ? valor.trim() : null;		
	}
	
}
