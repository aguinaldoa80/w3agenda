package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_materialtabelaprecoitemhistorico", sequenceName = "sq_materialtabelaprecoitemhistorico")
public class MaterialTabelaPrecoItemHistorico {

	private Integer cdMaterialTabelaPrecoItemHistorico;
	private Vendamaterial vendaMaterial;
	private Pedidovendamaterial pedidoVendaMaterial;
	private Vendaorcamentomaterial vendaOrcamentoMaterial;
	private Materialtabelaprecoitem materialTabelaPrecoItem;
	private Double valor;
	private Double percentualDesconto;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtabelaprecoitemhistorico")
	public Integer getCdMaterialTabelaPrecoItemHistorico() {
		return cdMaterialTabelaPrecoItemHistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidoVendaMaterial() {
		return pedidoVendaMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendaMaterial() {
		return vendaMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamentomaterial")
	public Vendaorcamentomaterial getVendaOrcamentoMaterial() {
		return vendaOrcamentoMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelaprecoitem")
	public Materialtabelaprecoitem getMaterialTabelaPrecoItem() {
		return materialTabelaPrecoItem;
	}
	public Double getValor() {
		return valor;
	}
	public Double getPercentualDesconto() {
		return percentualDesconto;
	}
	
	public void setCdMaterialTabelaPrecoItemHistorico(
			Integer cdMaterialTabelaPrecoItemHistorico) {
		this.cdMaterialTabelaPrecoItemHistorico = cdMaterialTabelaPrecoItemHistorico;
	}
	public void setVendaMaterial(Vendamaterial vendaMaterial) {
		this.vendaMaterial = vendaMaterial;
	}
	public void setPedidoVendaMaterial(Pedidovendamaterial pedidoVendaMaterial) {
		this.pedidoVendaMaterial = pedidoVendaMaterial;
	}
	public void setVendaOrcamentoMaterial(
			Vendaorcamentomaterial vendaOrcamentoMaterial) {
		this.vendaOrcamentoMaterial = vendaOrcamentoMaterial;
	}
	public void setMaterialTabelaPrecoItem(
			Materialtabelaprecoitem materialTabelaPrecoItem) {
		this.materialTabelaPrecoItem = materialTabelaPrecoItem;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}
}
