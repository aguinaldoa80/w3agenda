package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@DisplayName("Benef�cios")
@Entity
@SequenceGenerator(name="sq_colaboradorbeneficio",sequenceName="sq_colaboradorbeneficio")
public class Colaboradorbeneficio {
	
	private Integer cdcolaboradorbeneficio;
	private Colaborador colaborador;
	private Beneficio beneficio;
	private Integer quantidade;
	
	//Transient
	private String beneficiotipoStr;
	private Money valor;
	private Money total;
	
	@Id
	@GeneratedValue(generator="sq_colaboradorbeneficio", strategy=GenerationType.AUTO)
	public Integer getCdcolaboradorbeneficio() {
		return cdcolaboradorbeneficio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")	
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Benef�cio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbeneficio")	
	public Beneficio getBeneficio() {
		return beneficio;
	}
	@Required
	@DisplayName("Quantidade")
	public Integer getQuantidade() {
		return quantidade;
	}
	@Transient
	@DisplayName("Tipo")
	public String getBeneficiotipoStr() {
		return beneficiotipoStr;
	}
	@Transient
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	@Transient
	@DisplayName("Total")
	public Money getTotal() {
		return total;
	}
	
	public void setCdcolaboradorbeneficio(Integer cdcolaboradorbeneficio) {
		this.cdcolaboradorbeneficio = cdcolaboradorbeneficio;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setBeneficio(Beneficio beneficio) {
		this.beneficio = beneficio;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setBeneficiotipoStr(String beneficiotipoStr) {
		this.beneficiotipoStr = beneficiotipoStr;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
}