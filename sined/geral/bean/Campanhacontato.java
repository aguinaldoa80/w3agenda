package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_campanhacontato", sequenceName = "sq_campanhacontato")
public class Campanhacontato implements Log{
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdcampanhacontato;
	protected Campanha campanha;
	protected Contacrmcontato contacrmcontato;
	
	//Transients
	
	protected String contatoResponsavel;
	protected String contatoListaEmails;
	protected String oportunidadesituacao;
	protected String ultimaInteracaoVenda;
	protected List<Contacrmcontato> listaContatos = new ArrayList<Contacrmcontato>();
	
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campanhacontato")
	public Integer getCdcampanhacontato() {
		return cdcampanhacontato;
	}

	@DisplayName("Campanha")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampanha")
	public Campanha getCampanha() {
		return campanha;
	}

	@DisplayName("Contato")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrmcontato")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	

	public void setCdcampanhacontato(Integer cdcampanhacontato) {
		this.cdcampanhacontato = cdcampanhacontato;
	}

	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}

	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	
	
	@Transient
	@DisplayName("Responsável")
	public String getContatoResponsavel() {
		return contatoResponsavel;
	}
	@Transient
	@DisplayName("E-mails")
	public String getContatoListaEmails() {
		return contatoListaEmails;
	}
	@Transient
	public List<Contacrmcontato> getListaContatos() {
		return listaContatos;
	}
	@Transient	
	public String getOportunidadesituacao() {
		return oportunidadesituacao;
	}
	@Transient
	public String getUltimaInteracaoVenda() {
		return ultimaInteracaoVenda;
	}


	public void setContatoResponsavel(String contatoResponsavel) {
		this.contatoResponsavel = contatoResponsavel;
	}
	public void setContatoListaEmails(String contatoListaEmails) {
		this.contatoListaEmails = contatoListaEmails;
	}
	public void setListaContatos(List<Contacrmcontato> listaContatos) {
		this.listaContatos = listaContatos;
	}
	public void setOportunidadesituacao(String oportunidadesituacao) {
		this.oportunidadesituacao = oportunidadesituacao;
	}	
	public void setUltimaInteracaoVenda(String ultimaInteracaoVenda) {
		this.ultimaInteracaoVenda = ultimaInteracaoVenda;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
