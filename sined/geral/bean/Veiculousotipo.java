package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculousotipo", sequenceName = "sq_veiculousotipo")
public class Veiculousotipo implements Log{

	protected Integer cdveiculousotipo;
	protected String descricao;
	protected Boolean escolta;
	protected Boolean normal;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	public Veiculousotipo(){
		if(this.cdveiculousotipo == null){
			this.escolta = Boolean.FALSE;
			this.normal = Boolean.FALSE;
		}
	}

	public Veiculousotipo(Integer cdveiculousotipo, String descricao){
		this.cdveiculousotipo = cdveiculousotipo;
		this.descricao = descricao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculousotipo")
	public Integer getCdveiculousotipo() {
		return cdveiculousotipo;
	}
	@DisplayName("Descri��o")
	@MaxLength(30)
	@DescriptionProperty
	@Required
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public Boolean getEscolta() {
		return escolta;
	}
	public Boolean getNormal() {
		return normal;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao.trim();
	}
	public void setCdveiculousotipo(Integer cdveiculousotipo) {
		this.cdveiculousotipo = cdveiculousotipo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setEscolta(Boolean escolta) {
		this.escolta = escolta;
	}
	public void setNormal(Boolean normal) {
		this.normal = normal;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Veiculousotipo)
			return ((Veiculousotipo)obj).getCdveiculousotipo().equals(this.getCdveiculousotipo());
		return super.equals(obj);
	}
	
}