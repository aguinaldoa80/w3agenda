package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_exameconvenioclinica", sequenceName = "sq_exameconvenioclinica")
public class Exameconvenioclinica {
	protected Integer cdexameconvenioclinica;
	protected Exameclinica exameclinica;
	protected Exameconvenio exameconvenio;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exameconvenioclinica")
	public Integer getCdexameconvenioclinica() {
		return cdexameconvenioclinica;
	}
	
	@DisplayName("Cl�nica")
	@JoinColumn(name="cdexameclinica")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Exameclinica getExameclinica() {
		return exameclinica;
	}
	
	@DisplayName("Conv�nio")
	@JoinColumn(name="cdexameconvenio")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Exameconvenio getExameconvenio() {
		return exameconvenio;
	}
	
	
	public void setCdexameconvenioclinica(Integer cdexameconvenioclinica) {
		this.cdexameconvenioclinica = cdexameconvenioclinica;
	}
	public void setExameclinica(Exameclinica exameclinica) {
		this.exameclinica = exameclinica;
	}
	public void setExameconvenio(Exameconvenio exameconvenio) {
		this.exameconvenio = exameconvenio;
	}
	
	
	
	

}
