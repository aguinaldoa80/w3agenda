package br.com.linkcom.sined.geral.bean;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_materialclasse", sequenceName = "sq_materialclasse")
public class Materialclasse{

	public static final Materialclasse PRODUTO = new Materialclasse(1,"Produto"); 
	public static final Materialclasse PATRIMONIO = new Materialclasse(2,"Patrim�nio"); 
	public static final Materialclasse EPI = new Materialclasse(3,"EPI"); 
	public static final Materialclasse SERVICO = new Materialclasse(4,"Servi�o"); 
	
	protected Integer cdmaterialclasse;
	protected String nome;
	
	public Materialclasse() {
	}
	
	public Materialclasse(Integer cdmaterialclasse) {
		this.cdmaterialclasse = cdmaterialclasse;
	}
	
	public Materialclasse(Integer cdmaterialclasse, String nome) {
		this.cdmaterialclasse = cdmaterialclasse;
		this.nome = nome;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialclasse")
	public Integer getCdmaterialclasse() {
		return cdmaterialclasse;
	}
	@DescriptionProperty
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	public void setCdmaterialclasse(Integer cdmaterialclasse) {
		this.cdmaterialclasse = cdmaterialclasse;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Materialclasse) {
			Materialclasse bean = (Materialclasse) obj;
			return bean.getCdmaterialclasse().equals(this.getCdmaterialclasse());
		}
		return super.equals(obj);
	}

}
