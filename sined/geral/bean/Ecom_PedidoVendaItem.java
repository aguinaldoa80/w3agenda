package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_ecom_pedidovendaitem", sequenceName="sq_ecom_pedidovendaitem")
public class Ecom_PedidoVendaItem {
	
	private Integer cdEcom_PedidoVendaItem;
	private Integer id;
	private String codigo_interno;
	private Double quantidade;
	private Double preco_venda;
	private Double preco_embalagem_total;
	private Double desconto;
	private String observacao;
	private Ecom_PedidoVenda ecomPedidoVenda;
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_pedidovendaitem", strategy=GenerationType.AUTO)
	public Integer getCdEcom_PedidoVendaItem() {
		return cdEcom_PedidoVendaItem;
	}
	public void setCdEcom_PedidoVendaItem(Integer cdEcom_PedidoVendaItem) {
		this.cdEcom_PedidoVendaItem = cdEcom_PedidoVendaItem;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCodigo_interno() {
		return codigo_interno;
	}
	public void setCodigo_interno(String codigo_interno) {
		this.codigo_interno = codigo_interno;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco_venda() {
		return preco_venda;
	}
	public void setPreco_venda(Double preco_venda) {
		this.preco_venda = preco_venda;
	}
	public Double getPreco_embalagem_total() {
		return preco_embalagem_total;
	}
	public void setPreco_embalagem_total(Double preco_embalagem_total) {
		this.preco_embalagem_total = preco_embalagem_total;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_pedidovenda")
	public Ecom_PedidoVenda getEcomPedidoVenda() {
		return ecomPedidoVenda;
	}
	public void setEcomPedidoVenda(Ecom_PedidoVenda ecomPedidoVenda) {
		this.ecomPedidoVenda = ecomPedidoVenda;
	}
}
