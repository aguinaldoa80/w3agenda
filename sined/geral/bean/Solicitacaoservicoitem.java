package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_solicitacaoservicoitem",sequenceName="sq_solicitacaoservicoitem")
public class Solicitacaoservicoitem {
	
	protected Integer cdsolicitacaoservicoitem;
	protected Solicitacaoservico solicitacaoservico;
	protected Solicitacaoservicotipoitem solicitacaoservicotipoitem;
	protected String valor;
	
	@Id
	@GeneratedValue(generator="sq_solicitacaoservicoitem",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservicoitem() {
		return cdsolicitacaoservicoitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservico")
	public Solicitacaoservico getSolicitacaoservico() {
		return solicitacaoservico;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservicotipoitem")
	public Solicitacaoservicotipoitem getSolicitacaoservicotipoitem() {
		return solicitacaoservicotipoitem;
	}

	@MaxLength(500)
	public String getValor() {
		return valor;
	}

	public void setSolicitacaoservicotipoitem(
			Solicitacaoservicotipoitem solicitacaoservicotipoitem) {
		this.solicitacaoservicotipoitem = solicitacaoservicotipoitem;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public void setCdsolicitacaoservicoitem(Integer cdsolicitacaoservicoitem) {
		this.cdsolicitacaoservicoitem = cdsolicitacaoservicoitem;
	}

	public void setSolicitacaoservico(Solicitacaoservico solicitacaoservico) {
		this.solicitacaoservico = solicitacaoservico;
	}

	
}
