package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_notafiscalprodutoproducaoordem", sequenceName = "sq_notafiscalprodutoproducaoordem")
public class NotafiscalprodutoProducaoordem{

	protected Integer cdnotafiscalprodutoproducaoordem;
	protected Producaoordem producaoordem;
	protected Notafiscalproduto notafiscalproduto;
	protected Boolean devolucao;
	
	public NotafiscalprodutoProducaoordem() {}
	
	public NotafiscalprodutoProducaoordem(Integer cdproducaoordem, Integer cdnotafiscalproduto) {
		this.producaoordem = new Producaoordem(cdproducaoordem);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
		this.devolucao = Boolean.FALSE;
	}
	public NotafiscalprodutoProducaoordem(Integer cdproducaoordem, Integer cdnotafiscalproduto, Boolean devolucao) {
		this.producaoordem = new Producaoordem(cdproducaoordem);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
		this.devolucao = devolucao != null ? devolucao : Boolean.FALSE;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutoproducaoordem")
	public Integer getCdnotafiscalprodutoproducaoordem() {
		return cdnotafiscalprodutoproducaoordem;
	}
	public void setCdnotafiscalprodutoproducaoordem(Integer cdnotafiscalprodutoproducaoordem) {
		this.cdnotafiscalprodutoproducaoordem = cdnotafiscalprodutoproducaoordem;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordem")
	public Producaoordem getProducaoordem() {
		return producaoordem;
	}
	public void setProducaoordem(Producaoordem producaoordem) {
		this.producaoordem = producaoordem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	
	public Boolean getDevolucao() {
		return devolucao;
	}
	public void setDevolucao(Boolean devolucao) {
		this.devolucao = devolucao;
	}
}