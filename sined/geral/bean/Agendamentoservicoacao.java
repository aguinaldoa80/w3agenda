package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Situa��o Agendamento Servi�o")
public class Agendamentoservicoacao{

	public static final Agendamentoservicoacao AGENDADO = new Agendamentoservicoacao(1, "Agendado");
	public static final Agendamentoservicoacao EM_ANDAMENTO = new Agendamentoservicoacao(2, "Em andamento");
	public static final Agendamentoservicoacao ATENCAO = new Agendamentoservicoacao(3, "Aten��o");
	public static final Agendamentoservicoacao CANCELADO = new Agendamentoservicoacao(4, "Cancelado");
	public static final Agendamentoservicoacao REALIZADO = new Agendamentoservicoacao(5, "Realizado");
	public static final Agendamentoservicoacao ANOTADO = new Agendamentoservicoacao(6, "Anota��o");
	
	protected Integer cdagendamentoservicoacao;
	protected String descricao;
	
	public Agendamentoservicoacao(){
	}

	public Agendamentoservicoacao(Integer cdagendamentoservicoacao){
		this.cdagendamentoservicoacao = cdagendamentoservicoacao;
	}
	
	public Agendamentoservicoacao(Integer cdagendamentoservicoacao, String descricao){
		this.cdagendamentoservicoacao = cdagendamentoservicoacao;
		this.descricao = descricao;
	}
	
	@Id
	@DisplayName("Id")
	public Integer getCdagendamentoservicoacao() {
		return cdagendamentoservicoacao;
	}
	@DescriptionProperty
	@Required
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdagendamentoservicoacao(Integer cdagendamentoservicoacao) {
		this.cdagendamentoservicoacao = cdagendamentoservicoacao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Agendamentoservicoacao)
			return ((Agendamentoservicoacao)obj).cdagendamentoservicoacao.equals(this.cdagendamentoservicoacao);
		return super.equals(obj);
	}
		
}
