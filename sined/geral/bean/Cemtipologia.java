package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;


@Entity
@SequenceGenerator(name = "sq_cemtipologia", sequenceName = "sq_cemtipologia")
public class Cemtipologia implements ImportacaoproducaoMaterial {

	protected Integer cdcemtipologia;
	protected Cemobra cemobra;
	protected String projetista;
	protected String codesqd;
	protected String tipo;
	protected Double qtde;
	protected Double largura;
	protected Double altura;
	protected String trat_perf;
	protected String descr;
	protected Double peso_unit;
	protected Double preco_unit;
	protected String cor_comp;
	protected String descr_vidros;
	protected Double custo_unit;
	
	protected Set<Cemcomponente> listaCemcomponente = new ListSet<Cemcomponente>(Cemcomponente.class);
	protected Set<Cemperfil> listaCemperfil = new ListSet<Cemperfil>(Cemperfil.class);
	protected Set<Cemvidro> listaCemvidro = new ListSet<Cemvidro>(Cemvidro.class);
	protected Set<Cempainel> listaCempainel = new ListSet<Cempainel>(Cempainel.class);
	
	protected List<Cemcomponente> listaCemcomponenteLIST = new ListSet<Cemcomponente>(Cemcomponente.class);
	protected List<Cemperfil> listaCemperfilLIST = new ListSet<Cemperfil>(Cemperfil.class);
	protected List<Cemvidro> listaCemvidroLIST = new ListSet<Cemvidro>(Cemvidro.class);
	protected List<Cempainel> listaCempainelLIST = new ListSet<Cempainel>(Cempainel.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cemtipologia")
	public Integer getCdcemtipologia() {
		return cdcemtipologia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcemobra")
	public Cemobra getCemobra() {
		return cemobra;
	}

	@MaxLength(20)
	public String getProjetista() {
		return projetista;
	}
	
	public String getCodesqd() {
		return codesqd;
	}

	@MaxLength(20)
	public String getTipo() {
		return tipo;
	}

	public Double getQtde() {
		return qtde;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getAltura() {
		return altura;
	}

	@MaxLength(60)
	public String getTrat_perf() {
		return trat_perf;
	}

	@MaxLength(240)
	public String getDescr() {
		return descr;
	}

	@DisplayName("Peso unit�rio")
	public Double getPeso_unit() {
		return peso_unit;
	}

	@DisplayName("Pre�o unit�rio")
	public Double getPreco_unit() {
		return preco_unit;
	}
	
	@OneToMany(mappedBy="cemtipologia")
	public Set<Cemcomponente> getListaCemcomponente() {
		return listaCemcomponente;
	}

	@OneToMany(mappedBy="cemtipologia")
	public Set<Cemperfil> getListaCemperfil() {
		return listaCemperfil;
	}

	@OneToMany(mappedBy="cemtipologia")
	public Set<Cemvidro> getListaCemvidro() {
		return listaCemvidro;
	}

	@OneToMany(mappedBy="cemtipologia")
	public Set<Cempainel> getListaCempainel() {
		return listaCempainel;
	}
	
	@MaxLength(20)
	public String getCor_comp() {
		return cor_comp;
	}

	@MaxLength(160)
	public String getDescr_vidros() {
		return descr_vidros;
	}

	public Double getCusto_unit() {
		return custo_unit;
	}

	public void setCor_comp(String corComp) {
		cor_comp = corComp;
	}

	public void setDescr_vidros(String descrVidros) {
		descr_vidros = descrVidros;
	}

	public void setCusto_unit(Double custoUnit) {
		custo_unit = custoUnit;
	}

	public void setListaCemcomponente(Set<Cemcomponente> listaCemcomponente) {
		this.listaCemcomponente = listaCemcomponente;
	}

	public void setListaCemperfil(Set<Cemperfil> listaCemperfil) {
		this.listaCemperfil = listaCemperfil;
	}

	public void setListaCemvidro(Set<Cemvidro> listaCemvidro) {
		this.listaCemvidro = listaCemvidro;
	}

	public void setListaCempainel(Set<Cempainel> listaCempainel) {
		this.listaCempainel = listaCempainel;
	}

	public void setCdcemtipologia(Integer cdcemtipologia) {
		this.cdcemtipologia = cdcemtipologia;
	}

	public void setCemobra(Cemobra cemobra) {
		this.cemobra = cemobra;
	}

	public void setProjetista(String projetista) {
		this.projetista = projetista;
	}
	
	public void setCodesqd(String codesqd) {
		this.codesqd = codesqd;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setTrat_perf(String tratPerf) {
		trat_perf = tratPerf;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public void setPeso_unit(Double pesoUnit) {
		peso_unit = pesoUnit;
	}

	public void setPreco_unit(Double precoUnit) {
		preco_unit = precoUnit;
	}

	@Transient
	public List<Cemcomponente> getListaCemcomponenteLIST() {
		if(this.listaCemcomponente != null && this.listaCemcomponente.size() > 0){
			listaCemcomponenteLIST = new ArrayList<Cemcomponente>(this.listaCemcomponente);
			
			Collections.sort(listaCemcomponenteLIST,new Comparator<Cemcomponente>(){
				public int compare(Cemcomponente o1, Cemcomponente o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCemcomponenteLIST;
	}

	@Transient
	public List<Cemperfil> getListaCemperfilLIST() {
		if(this.listaCemperfil != null && this.listaCemperfil.size() > 0){
			listaCemperfilLIST = new ArrayList<Cemperfil>(this.listaCemperfil);
			
			Collections.sort(listaCemperfilLIST,new Comparator<Cemperfil>(){
				public int compare(Cemperfil o1, Cemperfil o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCemperfilLIST;
	}

	@Transient
	public List<Cemvidro> getListaCemvidroLIST() {
		if(this.listaCemvidro != null && this.listaCemvidro.size() > 0){
			listaCemvidroLIST = new ArrayList<Cemvidro>(this.listaCemvidro);
			
			Collections.sort(listaCemvidroLIST,new Comparator<Cemvidro>(){
				public int compare(Cemvidro o1, Cemvidro o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCemvidroLIST;
	}

	@Transient
	public List<Cempainel> getListaCempainelLIST() {
		if(this.listaCempainel != null && this.listaCempainel.size() > 0){
			listaCempainelLIST = new ArrayList<Cempainel>(this.listaCempainel);
			
			Collections.sort(listaCempainelLIST,new Comparator<Cempainel>(){
				public int compare(Cempainel o1, Cempainel o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCempainelLIST;
	}

	public void setListaCemcomponenteLIST(List<Cemcomponente> listaCemcomponenteLIST) {
		this.listaCemcomponenteLIST = listaCemcomponenteLIST;
	}

	public void setListaCemperfilLIST(List<Cemperfil> listaCemperfilLIST) {
		this.listaCemperfilLIST = listaCemperfilLIST;
	}

	public void setListaCemvidroLIST(List<Cemvidro> listaCemvidroLIST) {
		this.listaCemvidroLIST = listaCemvidroLIST;
	}

	public void setListaCempainelLIST(List<Cempainel> listaCempainelLIST) {
		this.listaCempainelLIST = listaCempainelLIST;
	}

	@Transient
	@Override
	public String getCodigo() {
		return this.codesqd;
	}

	@Transient
	@Override
	public Double getCustoMaterial() {
		return this.custo_unit;
	}

	@Transient
	@Override
	public Double getPesoMaterial() {
		return this.peso_unit;
	}
	
	@Transient
	@Override
	public Double getComprimentoMaterial() {
		return null;
	}

	@Transient
	@Override
	public Double getAlturaMaterial() {
		return this.altura;
	}

	@Transient
	@Override
	public Double getLarguraMaterial() {
		return this.largura;
	}
	
	@Transient
	public List<ImportacaoproducaoMaterial> getListaImportacaoproducaoMaterial(){
		List<ImportacaoproducaoMaterial> listaImportacaoproducaoMaterial = new ArrayList<ImportacaoproducaoMaterial>();
		
		if(this.listaCemcomponente != null) listaImportacaoproducaoMaterial.addAll(this.listaCemcomponente);
		if(this.listaCempainel != null) listaImportacaoproducaoMaterial.addAll(this.listaCempainel);
		if(this.listaCemperfil != null) listaImportacaoproducaoMaterial.addAll(this.listaCemperfil);
		if(this.listaCemvidro != null) listaImportacaoproducaoMaterial.addAll(this.listaCemvidro);
		
		return listaImportacaoproducaoMaterial;
	}
	
	@Transient
	@Override
	public String getNomeMaterial() {
		return this.descr + 
				(this.codesqd != null ? " " + this.codesqd : "") + 
				(this.tipo != null ? " " + this.tipo : "");
	}
	
	@Transient
	@Override
	public Double getQuantidade() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public TipoImportacaoProducao getTipoImportacao() {
		return TipoImportacaoProducao.TIPOLOGIA;
	}
	
	@Transient
	@Override
	public Double getVolume() {
		return null;
	}

	@Transient
	@Override
	public String getCorMaterial() {
		return this.trat_perf;
	}

	@Transient
	@Override
	public String getGrupoMaterial() {
		return this.tipo;
	}

	@Transient
	@Override
	public String getTipoMaterial() {
		return "PRM REVISADO";
	}

	@Transient
	@Override
	public String getUnidademedidaMaterial() {
		return "UN";
	}
	
}
