package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_expedicaoproducaoitem", sequenceName = "sq_expedicaoproducaoitem")
public class Expedicaoproducaoitem {

	protected Integer cdexpedicaoproducaoitem;
	protected Expedicaoproducao expedicaoproducao;
	protected Cliente cliente;
	protected Contrato contrato;
	protected Material material;
	protected Endereco enderecocliente;
	protected Date dtexpedicaoproducaoitem;
	protected Localarmazenagem localarmazenagem;
	protected Double qtdeproduzida;
	protected Double qtdeexpedicaoproducao; 
	protected Contato contatocliente;
	protected String etiqueta;
	
	//transient
	protected Producaoagenda producaoagenda;
	protected Endereco enderecoclienteSelecionado;
	protected Contato contatoclienteSelecionado;
	protected Contrato contratoSelecionado;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicaoproducaoitem")
	public Integer getCdexpedicaoproducaoitem() {
		return cdexpedicaoproducaoitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicaoproducao")
	public Expedicaoproducao getExpedicaoproducao() {
		return expedicaoproducao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}

	@Required
	@DisplayName("Produto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Data de expedi��o")
	public Date getDtexpedicaoproducaoitem() {
		return dtexpedicaoproducaoitem;
	}

	@DisplayName("Local de expedi��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecocliente")
	public Endereco getEnderecocliente() {
		return enderecocliente;
	}

	@DisplayName("Respons�vel pela retirada")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontatocliente")
	public Contato getContatocliente() {
		return contatocliente;
	}
	
	@DisplayName("Qtde. produzida")
	public Double getQtdeproduzida(){
		return qtdeproduzida;
	}
	
	@Required
	@DisplayName("Qtde. para expedi��o")
	public Double getQtdeexpedicaoproducao() {
		return qtdeexpedicaoproducao;
	}

	public void setCdexpedicaoproducaoitem(Integer cdexpedicaoproducaoitem) {
		this.cdexpedicaoproducaoitem = cdexpedicaoproducaoitem;
	}

	public void setExpedicaoproducao(Expedicaoproducao expedicaoproducao) {
		this.expedicaoproducao = expedicaoproducao;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setContrato(Contrato contrato){
		this.contrato = contrato;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setDtexpedicaoproducaoitem(Date dtexpedicaoproducaoitem) {
		this.dtexpedicaoproducaoitem = dtexpedicaoproducaoitem;
	}

	public void setEnderecocliente(Endereco enderecocliente) {
		this.enderecocliente = enderecocliente;
	}

	public void setContatocliente(Contato contatocliente) {
		this.contatocliente = contatocliente;
	}
	
	public void setQtdeproduzida(Double qtdeproduzida){
		this.qtdeproduzida = qtdeproduzida;
	}
	
	public void setQtdeexpedicaoproducao(Double qtdeexpedicaoproducao) {
		this.qtdeexpedicaoproducao = qtdeexpedicaoproducao;
	}
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	@Transient
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}

	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	
	@Transient
	public Endereco getEnderecoclienteSelecionado() {
		if(this.enderecocliente != null){
			return this.enderecocliente;
		}
		return enderecoclienteSelecionado;
	}
	
	@Transient
	public Contato getContatoclienteSelecionado() {
		if(this.contatocliente != null){
			return this.contatocliente;
		}
		return contatoclienteSelecionado;
	}
	
	@Transient
	public Contrato getContratoSelecionado() {
		if(this.contrato != null){
			return this.contrato;
		}
		return contratoSelecionado;
	}
	
	public void setContratoSelecionado(Contrato contratoSelecionado) {
		this.contratoSelecionado = contratoSelecionado;
	}
	
	public void setContatoclienteSelecionado(Contato contatoclienteSelecionado) {
		this.contatoclienteSelecionado = contatoclienteSelecionado;
	}
	
	public void setEnderecoclienteSelecionado(Endereco enderecoclienteSelecionado) {
		this.enderecoclienteSelecionado = enderecoclienteSelecionado;
	}
}
