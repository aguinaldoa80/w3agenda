package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
public class Servidorfirewallsentido{

	public static Servidorfirewallsentido ENTRADA = new Servidorfirewallsentido(1, "Entrada");
	public static Servidorfirewallsentido SAIDA = new Servidorfirewallsentido(2, "Sa�da");
	
	protected Integer cdservidorfirewallsentido;
	protected String nome;

	public Servidorfirewallsentido(){
	}

	public Servidorfirewallsentido(Integer cdservidorfirewallsentido, String nome){
		this.cdservidorfirewallsentido = cdservidorfirewallsentido;
		this.nome = nome;
	}
	
	@Id
	@DisplayName("Id")
	public Integer getCdservidorfirewallsentido() {
		return cdservidorfirewallsentido;
	}

	@Required
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setCdservidorfirewallsentido(Integer cdservidorfirewallsentido) {
		this.cdservidorfirewallsentido = cdservidorfirewallsentido;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Servidorfirewallsentido)
			return ((Servidorfirewallsentido)obj).cdservidorfirewallsentido.equals(this.cdservidorfirewallsentido);
		return super.equals(obj);
	}
	
}
