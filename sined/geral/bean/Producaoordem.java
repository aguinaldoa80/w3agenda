package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoordemsituacao;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.TrocarProdutoFinalBean;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_producaoordem", sequenceName = "sq_producaoordem")
@DisplayName("Ordem de produ��o")
@JoinEmpresa("producaoordem.empresa")
public class Producaoordem implements Log{
	
	protected Integer cdproducaoordem; 
	protected Empresa empresa;
	protected Departamento departamento;
	protected Date dtproducaoordem;
	protected Date dtprevisaoentrega;
	protected Date dtconclusaoordem;
	protected Producaoordemsituacao producaoordemsituacao = Producaoordemsituacao.EM_ESPERA;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Producaoordem producaoordemanterior;
	
	protected Set<Producaoordemmaterial> listaProducaoordemmaterial = new ListSet<Producaoordemmaterial>(Producaoordemmaterial.class);
	protected Set<Producaoordemitemadicional> listaProducaoordemitemadicional = new ListSet<Producaoordemitemadicional>(Producaoordemitemadicional.class);
	protected List<Producaoordemhistorico> listaProducaoordemhistorico = new ListSet<Producaoordemhistorico>(Producaoordemhistorico.class);
	protected Set<Producaoordemequipamento> listaProducaoordemequipamento = new ListSet<Producaoordemequipamento>(Producaoordemequipamento.class);
	protected Set<Producaoordemconstatacao> listaProducaoordemconstatacao = new ListSet<Producaoordemconstatacao>(Producaoordemconstatacao.class);
	
	// TRANSIENTE
	protected String historico;
	protected String observacaoHistorico;
	protected Boolean hasPerda;
	protected Integer etapa_id;
	protected Integer etapaanterior_id;
	protected Producaoordemmaterial producaoordemmaterial;
	protected boolean salvarRegistrarProducaoLote = false;
	protected List<Producaoagendahistorico> listaProducaoagendahistorico = new ArrayList<Producaoagendahistorico>();
	protected List<TrocarProdutoFinalBean> listaTrocarProdutoFinal = new ArrayList<TrocarProdutoFinalBean>();
	protected String whereInColetamaterial;
	protected Localarmazenagem localarmazenagem;
	
	public Producaoordem(){}
	
	public Producaoordem(Integer cdproducaoordem){
		this.cdproducaoordem = cdproducaoordem;
	}	
	
	public Producaoordem(Integer cdproducaoordem, Date dtproducaoordem, Date dtconclusaoordem) {
		this.cdproducaoordem = cdproducaoordem;
		this.dtproducaoordem = dtproducaoordem;
		this.dtconclusaoordem = dtconclusaoordem;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoordem")
	public Integer getCdproducaoordem() {
		return cdproducaoordem;
	}

	@Required
	@DisplayName("Previs�o de entrega")
	public Date getDtprevisaoentrega() {
		return dtprevisaoentrega;
	}

	@Required
	@DisplayName("Situa��o")
	public Producaoordemsituacao getProducaoordemsituacao() {
		return producaoordemsituacao;
	}
	
	@JoinColumn(name="cddepartamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@DisplayName("Data")
	public Date getDtproducaoordem() {
		return dtproducaoordem;
	}
	
	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@JoinColumn(name="cdproducaoordemanterior")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordem getProducaoordemanterior() {
		return producaoordemanterior;
	}
	
	@DisplayName("Data de conclus�o")
	public Date getDtconclusaoordem() {
		return dtconclusaoordem;
	}
	
	public void setProducaoordemanterior(Producaoordem producaoordemanterior) {
		this.producaoordemanterior = producaoordemanterior;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCdproducaoordem(Integer cdproducaoordem) {
		this.cdproducaoordem = cdproducaoordem;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setDtproducaoordem(Date dtproducaoordem) {
		this.dtproducaoordem = dtproducaoordem;
	}
	
	public void setDtprevisaoentrega(Date dtprevisaoentrega) {
		this.dtprevisaoentrega = dtprevisaoentrega;
	}
	
	public void setProducaoordemsituacao(Producaoordemsituacao producaoordemsituacao) {
		this.producaoordemsituacao = producaoordemsituacao;
	}
	
	public void setDtconclusaoordem(Date dtconclusaoordem) {
		this.dtconclusaoordem = dtconclusaoordem;
	}

	// LISTAS

	@OneToMany(mappedBy="producaoordem")
	public List<Producaoordemhistorico> getListaProducaoordemhistorico() {
		return listaProducaoordemhistorico;
	}
	
	@OneToMany(mappedBy="producaoordem")
	public Set<Producaoordemmaterial> getListaProducaoordemmaterial() {
		return listaProducaoordemmaterial;
	}
	
	@OneToMany(mappedBy="producaoordem")
	public Set<Producaoordemequipamento> getListaProducaoordemequipamento() {
		return listaProducaoordemequipamento;
	}
	
	@OneToMany(mappedBy="producaoordem")
	public Set<Producaoordemconstatacao> getListaProducaoordemconstatacao() {
		return listaProducaoordemconstatacao;
	}
	
	@OneToMany(mappedBy="producaoordem")
	public Set<Producaoordemitemadicional> getListaProducaoordemitemadicional() {
		return listaProducaoordemitemadicional;
	}
	
	public void setListaProducaoordemitemadicional(
			Set<Producaoordemitemadicional> listaProducaoordemitemadicional) {
		this.listaProducaoordemitemadicional = listaProducaoordemitemadicional;
	}
	
	public void setListaProducaoordemhistorico(
			List<Producaoordemhistorico> listaProducaoordemhistorico) {
		this.listaProducaoordemhistorico = listaProducaoordemhistorico;
	}
	
	public void setListaProducaoordemmaterial(
			Set<Producaoordemmaterial> listaProducaoordemmaterial) {
		this.listaProducaoordemmaterial = listaProducaoordemmaterial;
	}

	public void setListaProducaoordemequipamento(Set<Producaoordemequipamento> listaProducaoordemequipamento) {
		this.listaProducaoordemequipamento = listaProducaoordemequipamento;
	}
	
	public void setListaProducaoordemconstatacao(Set<Producaoordemconstatacao> listaProducaoordemconstatacao) {
		this.listaProducaoordemconstatacao = listaProducaoordemconstatacao;
	}

	//	LOG	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTE
	@Transient
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	@Transient
	public String getEtapasListagem(){
		StringBuilder sb = new StringBuilder();
		if(listaProducaoordemmaterial != null && listaProducaoordemmaterial.size() > 0){
			return SinedUtil.listAndConcatenateSort(listaProducaoordemmaterial, "producaoetapaitem.producaoetapanome.nome", "<BR>");
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdproducaoordem == null) ? 0 : cdproducaoordem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producaoordem other = (Producaoordem) obj;
		if (cdproducaoordem == null) {
			if (other.cdproducaoordem != null)
				return false;
		} else if (!cdproducaoordem.equals(other.cdproducaoordem))
			return false;
		return true;
	}
	
	@Transient
	public String getOrigemListagem(){
		StringBuilder sb = new StringBuilder();
		if(this.listaProducaoordemmaterial != null && this.listaProducaoordemmaterial.size() > 0){
			List<Integer> idContrato = new ArrayList<Integer>();
			List<Integer> idPedido = new ArrayList<Integer>();
			
			List<String> idPedidoIdentificado = new ArrayList<String>();
			
			for (Producaoordemmaterial it : listaProducaoordemmaterial) {
				if(it.getListaProducaoordemmaterialorigem() != null && it.getListaProducaoordemmaterialorigem().size() > 0){
					for (Producaoordemmaterialorigem itOrigem : it.getListaProducaoordemmaterialorigem()) {
						if(itOrigem.getProducaoagenda() != null){
							if(itOrigem.getProducaoagenda().getContrato() != null && itOrigem.getProducaoagenda().getContrato().getCdcontrato() != null){
								if(!idContrato.contains(itOrigem.getProducaoagenda().getContrato().getCdcontrato())){
									idContrato.add(itOrigem.getProducaoagenda().getContrato().getCdcontrato());
								}
							}
							if(itOrigem.getProducaoagenda().getPedidovenda() != null && itOrigem.getProducaoagenda().getPedidovenda().getCdpedidovenda() != null){
								if(!idPedido.contains(itOrigem.getProducaoagenda().getPedidovenda().getCdpedidovenda())){
									idPedido.add(itOrigem.getProducaoagenda().getPedidovenda().getCdpedidovenda());
									if(itOrigem.getProducaoagenda().getPedidovenda().getIdentificacaoexterna() != null && StringUtils.isNotBlank(itOrigem.getProducaoagenda().getPedidovenda().getIdentificacaoexterna())){
										idPedidoIdentificado.add(itOrigem.getProducaoagenda().getPedidovenda().getCdpedidovenda()+" / " + itOrigem.getProducaoagenda().getPedidovenda().getIdentificacaoexterna());										
									}else{
										idPedidoIdentificado.add(itOrigem.getProducaoagenda().getPedidovenda().getCdpedidovenda()+"");
									}
								}
							}
						}
					}
				}
			}
			
			if(idContrato.size() > 0){
				sb.append("Contrato");
				if(idContrato.size() > 1) sb.append("s");
				sb.append(": ");
				sb.append(CollectionsUtil.concatenate(idContrato, ", "));
				sb.append("<BR/>");
			}
			
//			if(idPedido.size() > 0){
//				sb.append("Pedido");
//				if(idPedido.size() > 1) sb.append("s");
//				sb.append(": ");
//				sb.append(CollectionsUtil.concatenate(idPedido, ", "));
//				sb.append("<BR/>");
//			}
			
			if(idPedidoIdentificado.size() > 0){
				sb.append("Pedido");
				if(idPedidoIdentificado.size() > 1) sb.append("s");
				sb.append(": ");
				sb.append(CollectionsUtil.concatenate(idPedidoIdentificado, ", "));
				sb.append("<BR/>");
			}
		}
		return sb.toString();
	}
	
	@Transient
	public String getMateriaisListagem(){
		StringBuilder sb = new StringBuilder();
		if(this.listaProducaoordemmaterial != null && this.listaProducaoordemmaterial.size() > 0){
			for (Producaoordemmaterial it : listaProducaoordemmaterial) {
				if(it.getMaterial() != null){
					sb.append(it.getMaterial().getNome());
					if(it.getObservacao() != null && !it.getObservacao().trim().equals("")){
						sb.append(" (").append(it.getObservacao()).append(")");
					}
					if(SinedUtil.isRecapagem() && 
							((it.getPedidovendamaterial() != null && it.getPedidovendamaterial().getCdpedidovendamaterial() != null) ||
							  it.getPneu() != null && it.getPneu().getCdpneu() != null)){
						sb.append(" (");
						if(it.getPedidovendamaterial() != null && it.getPedidovendamaterial().getCdpedidovendamaterial() != null){
							sb.append(" Item: ").append(it.getPedidovendamaterial().getCdpedidovendamaterial());
							if(it.getPneu() != null && it.getPneu().getCdpneu() != null){
								sb.append(" - ");
							}
						}
						if(it.getPneu() != null && it.getPneu().getCdpneu() != null){
							sb.append(" Pneu: ").append(it.getPneu().getCdpneu());
						}
						sb.append(" )");
					}
					sb.append("<BR/>");
				}
			}
		}
		return sb.toString();
	}
	
	@Transient
	public String getObservacaoHistorico() {
		return observacaoHistorico;
	}
	
	public void setObservacaoHistorico(String observacaoHistorico) {
		this.observacaoHistorico = observacaoHistorico;
	}
	
	@Transient
	public Boolean getHasPerda() {
		return hasPerda;
	}
	public void setHasPerda(Boolean hasPerda) {
		this.hasPerda = hasPerda;
	}

	@Transient
	public Integer getEtapa_id() {
		return etapa_id;
	}

	@Transient
	public Integer getEtapaanterior_id() {
		return etapaanterior_id;
	}

	public void setEtapa_id(Integer etapa_id) {
		this.etapa_id = etapa_id;
	}

	public void setEtapaanterior_id(Integer etapaanterior_id) {
		this.etapaanterior_id = etapaanterior_id;
	}
	
	@Transient
	public List<Producaoagendahistorico> getListaProducaoagendahistorico() {
		return listaProducaoagendahistorico;
	}
	
	public void setListaProducaoagendahistorico(
			List<Producaoagendahistorico> listaProducaoagendahistorico) {
		this.listaProducaoagendahistorico = listaProducaoagendahistorico;
	}
	
	@Transient
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	
	@Transient
	@DisplayName("Trocar produto final")
	public List<TrocarProdutoFinalBean> getListaTrocarProdutoFinal() {
		return listaTrocarProdutoFinal;
	}
	
	public void setListaTrocarProdutoFinal(List<TrocarProdutoFinalBean> listaTrocarProdutoFinal) {
		this.listaTrocarProdutoFinal = listaTrocarProdutoFinal;
	}
	
	@Transient
	public boolean isSalvarRegistrarProducaoLote() {
		return salvarRegistrarProducaoLote;
	}
	
	public void setSalvarRegistrarProducaoLote(boolean salvarRegistrarProducaoLote) {
		this.salvarRegistrarProducaoLote = salvarRegistrarProducaoLote;
	}

	@Transient
	public String getWhereInColetamaterial() {
		return whereInColetamaterial;
	}

	public void setWhereInColetamaterial(String whereInColetamaterial) {
		this.whereInColetamaterial = whereInColetamaterial;
	}
	
	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
}