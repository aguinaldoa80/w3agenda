package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_fatorajustevalor", sequenceName = "sq_fatorajustevalor")
public class Fatorajustevalor {

	protected Integer cdfatorajustevalor;
	protected Fatorajuste fatorajuste;
	protected Date mesano;
	protected Money valor;
	
	protected String mesanoAux;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fatorajustevalor")
	public Integer getCdfatorajustevalor() {
		return cdfatorajustevalor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfatorajuste")
	public Fatorajuste getFatorajuste() {
		return fatorajuste;
	}
	@DisplayName("Mes/ano")
	public Date getMesano() {
		return mesano;
	}
	@DisplayName("Valor")
	@Required
	public Money getValor() {
		return valor;
	}

	@Required
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}

	public void setCdfatorajustevalor(Integer cdfatorajustevalor) {
		this.cdfatorajustevalor = cdfatorajustevalor;
	}

	public void setFatorajuste(Fatorajuste fatorajuste) {
		this.fatorajuste = fatorajuste;
	}

	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setMesanoAux(String mesanoAux) {
		try {
			this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
	
}
