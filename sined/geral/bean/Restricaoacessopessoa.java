package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.geral.bean.enumeration.EnumTipoRestricaoAcessoPessoa;

@Entity
@SequenceGenerator(name="sq_restricaoacessopessoa", sequenceName="sq_restricaoacessopessoa")
@DisplayName("Restri��o Acesso")
public class Restricaoacessopessoa {
	
	protected Integer cdrestricaoacessopessoa;
	protected EnumTipoRestricaoAcessoPessoa tipo;
	protected Pessoa pessoa;
	protected Hora hrinicio;
	protected Hora hrfim;
	protected DiaSemana diasemana;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_restricaoacessopessoa")
	public Integer getCdrestricaoacessopessoa() {
		return cdrestricaoacessopessoa;
	}
	@Required
	@DisplayName("Tipo")
	public EnumTipoRestricaoAcessoPessoa getTipo() {
		return tipo;
	}
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	@DisplayName("Hora In�cio")
	public Hora getHrinicio() {
		return hrinicio;
	}
	@DisplayName("Hora Fim")
	public Hora getHrfim() {
		return hrfim;
	}
	@DisplayName("Dia da Semana")
	public DiaSemana getDiasemana() {
		return diasemana;
	}
	
	public void setCdrestricaoacessopessoa(Integer cdrestricaoacessopessoa) {
		this.cdrestricaoacessopessoa = cdrestricaoacessopessoa;
	}
	public void setDiasemana(DiaSemana diasemana) {
		this.diasemana = diasemana;
	}
	public void setTipo(EnumTipoRestricaoAcessoPessoa tipo) {
		this.tipo = tipo;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}
}