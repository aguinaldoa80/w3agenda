package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_contacontabilsaldoinicial", sequenceName="sq_contacontabilsaldoinicial")
public class ContaContabilSaldoInicial implements Log{

	private Integer cdContaContabilSaldoInicial;
	private ContaContabil contaContabil;
	private String codigoContaContabil;
	private String codigoAlternativoContaContabil;
	private String identificador;
	private String descricao;
	private Money saldo;
	private Date dtSaldo;
	private Arquivo arquivo;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	//TRANSIENTS
	private String erro;

	
	@Id
	@GeneratedValue(generator="sq_contacontabilsaldoinicial", strategy=GenerationType.AUTO)
	public Integer getCdContaContabilSaldoInicial() {
		return cdContaContabilSaldoInicial;
	}
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	@DisplayName("C�digo da conta cont�bil")
	public String getCodigoContaContabil() {
		return codigoContaContabil;
	}
	@DisplayName("C�digo alternativo da conta cont�bil")
	public String getCodigoAlternativoContaContabil() {
		return codigoAlternativoContaContabil;
	}
	@DisplayName("Identificador da conta cont�bil")
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Saldo inicial (R$)")
	public Money getSaldo() {
		return saldo;
	}
	@DisplayName("Data do saldo inicial")
	public Date getDtSaldo() {
		return dtSaldo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Transient
	public String getErro() {
		return erro;
	}
	
	public void setCdContaContabilSaldoInicial(
			Integer cdContaContabilSaldoInicial) {
		this.cdContaContabilSaldoInicial = cdContaContabilSaldoInicial;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setCodigoContaContabil(String codigoContaContabil) {
		this.codigoContaContabil = codigoContaContabil;
	}
	public void setCodigoAlternativoContaContabil(
			String codigoAlternativoContaContabil) {
		this.codigoAlternativoContaContabil = codigoAlternativoContaContabil;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	public void setDtSaldo(Date dtSaldo) {
		this.dtSaldo = dtSaldo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Override
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	@DisplayName("Data de importa��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
}
