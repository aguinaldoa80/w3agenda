package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedException;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_sexo", sequenceName = "sq_sexo")
public class Sexo {

	protected Integer cdsexo;
	protected String nome;
	
	public static final Integer MASCULINO = 1;
	public static final Integer FEMININO = 2;
	public static final String MASCULINO_CHAR = "M";
	public static final String FEMININO_CHAR = "F";
	
	public Sexo() {
	}
	
	public Sexo(Integer cdsexo) {
		this.cdsexo = cdsexo;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_sexo")
	public Integer getCdsexo() {
		return cdsexo;
	}
	public void setCdsexo(Integer id) {
		this.cdsexo = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Transient
	public String toChar() {
		if (this.cdsexo.equals(1))
			return MASCULINO_CHAR;
		else if (this.cdsexo.equals(2))
			return FEMININO_CHAR;
		else
			throw new SinedException("Sexo n�o identificado");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdsexo == null) ? 0 : cdsexo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Sexo other = (Sexo) obj;
		if (cdsexo == null) {
			if (other.cdsexo != null)
				return false;
		} else if (!cdsexo.equals(other.cdsexo))
			return false;
		return true;
	}
	
}
