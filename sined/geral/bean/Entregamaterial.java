package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Naturezabasecalculocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_entregamaterial", sequenceName = "sq_entregamaterial")
public class Entregamaterial implements Log, ValidateLoteestoqueObrigatorioInterface{

	protected Integer cdentregamaterial;
	protected Entregadocumento entregadocumento;
	protected Pedidovendamaterial pedidovendamaterial;
	
	protected Material material;
	protected Grupotributacao grupotributacao;
	protected Double qtde;
	protected Double comprimento;
	protected Double valorunitario;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Materialclasse materialclasse;
	protected Double pesomedio;
	
	protected String cprod;
	protected String xprod;
	protected Cfop cfop;
	protected Unidademedida unidademedidacomercial;
	protected Double valorproduto;
	protected Money valordesconto;
	protected Money valorfrete;
	protected Money valorseguro;
	protected Money valoroutrasdespesas;
	
	protected Tipocobrancaicms csticms;
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Double valormva;
	protected Boolean naoconsideraricmsst;
	protected Origemproduto origemProduto;
	
	private Money valorIcmsDesonerado;
	private Motivodesoneracaoicms motivoDesoneracaoIcms;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Tipocobrancaipi cstipi;
	protected Money valorbcipi;
	protected Double ipi;
	protected Money valoripi;
	
	protected Tipocobrancapis cstpis;
	protected Money valorbcpis;
	protected Double pis;
	protected Double pisretido;
	protected Double qtdebcpis;
	protected Double valorunidbcpis;
	protected Money valorpis;
	protected Money valorpisretido;
	
	protected Tipocobrancacofins cstcofins;
	protected Money valorbccofins;
	protected Double cofins;
	protected Double cofinsretido;
	protected Double qtdebccofins;
	protected Double valorunidbccofins;
	protected Money valorcofins;
	protected Money valorcofinsretido;
	
	protected Money valorcsll;
	protected Money valorir;
	protected Money valorinss;
	protected Money valorimpostoimportacao;
	
	protected Localarmazenagem localarmazenagem;
	protected Boolean tipolote;
	protected Loteestoque loteestoque;
	protected Set<Entregamateriallote> listaEntregamateriallote = new ListSet<Entregamateriallote>(Entregamateriallote.class);
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Boolean faturamentocliente;
	
//	CAMPOS DE IMPOSTO SOBRE SERVI�O (ISS)
	protected Money valorbciss;
	protected Double iss;
	protected Money valoriss;
	protected Municipio municipioiss;
	protected Tipotributacaoiss tipotributacaoiss;
	protected String clistservico;
	
	protected Naturezabasecalculocredito naturezabcc;
	
	protected ValorFiscal valorFiscalIcms;
	protected ValorFiscal valorFiscalIpi;
	
	protected List<Entregamaterialinspecao> listaInspecao = new ListSet<Entregamaterialinspecao>(Entregamaterialinspecao.class);
	protected Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial = new ListSet<Ordemcompraentregamaterial>(Ordemcompraentregamaterial.class);
	protected List<Notafiscalprodutoitementregamaterial> listaNotafiscalprodutoitementregamaterial;
	
//	CAMPO PARA CONTROLE DOS MATERIAIS CRIADOS AO MONTAR GRADE
	protected String whereInMaterialItenGrade;
	protected Boolean considerarItemGradeForWms;
	
//	CAMPOS DE Imposto de Importa��o (II)
	protected Money valorbcii;
	protected Money valoriiItem;
	protected Money valorDespesasAduaneiras;
	protected Money iof;
	
	protected String index;
	protected String identificadorinterno;
	
	protected Integer sequenciaitem;
	
//	TRANSIENTES
	protected Double valortotal;
	protected Double resto;
	protected Materialclasse materialclassetrans;
	protected Money valorunitariotrans;
	protected Integer sequencial;
	protected Boolean showcomprimento;
	protected Ordemcompramaterial ocm;
	protected String whereInOrdemcompramaterial;
	protected String nomematerialOrdemcompramaterial;
	protected Boolean removerItem;
	protected Boolean novoItemGrade;
	protected Boolean exibirButtonGrade = false;
	protected Boolean exibirButtonItemGrade = false;
	protected Boolean exibirMontargrade = false;
	protected Boolean isEcommerce = false;
	protected Grupotributacao grupotributacaotrans;
	protected Boolean origemMaterialmestregrade;
	protected List<Ordemcompramaterial> listaOrdemcompramaterialByMaterialmestregrade;
	protected Double valorEntrada;
	protected Double qtde_convertida;
	protected Boolean marcadoPopupNotaIndustrializacao = true;
	protected Double qtdedisponivelNotaIndustrializacao = 0d;
	protected Double qtdeNotaIndustrializacao;
	protected Double qtdeVenda = 0d;
	protected Unidademedida unidademedidacomercialAnterior;
	protected Boolean xmlchecado;
	protected Double xmlqtderestante;
	protected Double xmlqtdeassociada;
	protected Double xmlqtdeinformada;
	protected Ordemcompra ordemcompra;
	protected String whereInOrdemcompra;
	protected Double qtdeanteriorassociada;
	protected Boolean checkbox;
	protected Double saldo;
	protected Loteestoque loteestoqueAux;
	protected Double qtdeAux;
	protected Double percentualBcicms;
	protected Double percentualBcfcp;
	protected Double percentualBcfcpst;
	protected Double percentualBcipi;
	protected Double percentualBcpis;
	protected Double percentualBccofins;
	protected Double percentualBciss;
	protected Double percentualBccsll;
	protected Double percentualBcir;
	protected Double percentualBcinss;
	protected Boolean obrigarlote;
	protected Boolean habilitaPesoMedio;
	protected Empresa empresa;
	protected Boolean gerarE200;
	protected String identificadortela;
	protected Integer indexLista;

	protected Double valorProporcionalApropriacao;
//	CAMPO PARA CONTROLE DE SALDO DE IMPOSTO RETIDO
	protected Double qtdeutilizada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregamaterial")
	public Integer getCdentregamaterial() {
		return cdentregamaterial;
	}
	
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("Grupo de Tributa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	@Required
	@DisplayName("Qtde. entregue")
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("Comprimento")
	public Double getComprimento() {
		return comprimento;
	}

	@Required
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}

	@DisplayName("Item inspe��o")
	@OneToMany(mappedBy="entregamaterial")
	public List<Entregamaterialinspecao> getListaInspecao() {
		return listaInspecao;
	}
	
	@MaxLength(60)
	@DisplayName("C�digo do produto")
	public String getCprod() {
		return cprod;
	}

	@DisplayName("Descri��o produto")
	@MaxLength(120)
	public String getXprod() {
		return xprod;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfop")
	public Cfop getCfop() {
		return cfop;
	}

	@DisplayName("Unid. medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedidacomercial")
	@Required
	public Unidademedida getUnidademedidacomercial() {
		return unidademedidacomercial;
	}

	public Double getValorproduto() {
		return valorproduto;
	}

	@DisplayName("Valor frete")
	@MaxLength(15)
	public Money getValorfrete() {
		return valorfrete;
	}
	
	@DisplayName("Valor seguro")
	@MaxLength(15)
	public Money getValorseguro() {
		return valorseguro;
	}

	@DisplayName("Valor outras desp.")
	@MaxLength(15)
	public Money getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}

	@DisplayName("C�d. Sit. Trib. ICMS")
	public Tipocobrancaicms getCsticms() {
		return csticms;
	}

	@MaxLength(15)
	@DisplayName("Valor base de c�lculo ICMS")
	public Money getValorbcicms() {
		return valorbcicms;
	}

	@DisplayName("Al�q. ICMS (%)")
	@MaxLength(5)
	public Double getIcms() {
		return icms;
	}

	@DisplayName("Valor ICMS")
	@MaxLength(15)
	public Money getValoricms() {
		return valoricms;
	}

	@DisplayName("VALOR BC ICMS ST")
	@MaxLength(15)
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	@DisplayName("Al�q. ICMS ST (%)")
	@MaxLength(15)
	public Double getIcmsst() {
		return icmsst;
	}

	@DisplayName("VALOR ICMS ST")
	@MaxLength(15)
	public Money getValoricmsst() {
		return valoricmsst;
	}

	@DisplayName("C�d. Sit. Trib. IPI")
	public Tipocobrancaipi getCstipi() {
		return cstipi;
	}

	@DisplayName("Valor base de c�lculo IPI")
	@MaxLength(15)
	public Money getValorbcipi() {
		return valorbcipi;
	}

	@DisplayName("Al�q. IPI (%)")
	@MaxLength(5)
	public Double getIpi() {
		return ipi;
	}

	@MaxLength(15)
	@DisplayName("Valor IPI")
	public Money getValoripi() {
		return valoripi;
	}

	@DisplayName("C�d. Sit. Trib. PIS")
	public Tipocobrancapis getCstpis() {
		return cstpis;
	}

	@MaxLength(15)
	@DisplayName("Valor base de c�lculo PIS")
	public Money getValorbcpis() {
		return valorbcpis;
	}

	@MaxLength(5)
	@DisplayName("Al�q. PIS (%)")
	public Double getPis() {
		return pis;
	}

	@MaxLength(15)
	@DisplayName("Valor PIS")
	public Money getValorpis() {
		return valorpis;
	}

	@DisplayName("C�d. Sit. Trib. COFINS")
	public Tipocobrancacofins getCstcofins() {
		return cstcofins;
	}

	@DisplayName("Valor base de c�lculo COFINS")
	@MaxLength(15)
	public Money getValorbccofins() {
		return valorbccofins;
	}

	@DisplayName("Al�q. COFINS (%)")
	@MaxLength(5)
	public Double getCofins() {
		return cofins;
	}

	@DisplayName("Valor COFINS")
	@MaxLength(15)
	public Money getValorcofins() {
		return valorcofins;
	}
	
	@MaxLength(15)
	@DisplayName("Qtde. vendida PIS")
	public Double getQtdebcpis() {
		return qtdebcpis;
	}

	@MaxLength(15)
	@DisplayName("Al�q. PIS (R$)")
	public Double getValorunidbcpis() {
		return valorunidbcpis;
	}

	@MaxLength(15)
	@DisplayName("Qtde. vendida COFINS")
	public Double getQtdebccofins() {
		return qtdebccofins;
	}

	@MaxLength(15)
	@DisplayName("Al�q. COFINS (R$)")
	public Double getValorunidbccofins() {
		return valorunidbccofins;
	}

	@OneToMany(mappedBy="entregamaterial")
	public Set<Ordemcompraentregamaterial> getListaOrdemcompraentregamaterial() {
		return listaOrdemcompraentregamaterial;
	}
	
	public void setListaOrdemcompraentregamaterial(
			Set<Ordemcompraentregamaterial> listaOrdemcompraentregamaterial) {
		this.listaOrdemcompraentregamaterial = listaOrdemcompraentregamaterial;
	}
	
	@DisplayName("Valor desconto")
	public Money getValordesconto() {
		return valordesconto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	
	public void setQtdebcpis(Double qtdebcpis) {
		this.qtdebcpis = qtdebcpis;
	}

	public void setValorunidbcpis(Double valorunidbcpis) {
		this.valorunidbcpis = valorunidbcpis;
	}

	public void setQtdebccofins(Double qtdebccofins) {
		this.qtdebccofins = qtdebccofins;
	}

	public void setValorunidbccofins(Double valorunidbccofins) {
		this.valorunidbccofins = valorunidbccofins;
	}

	public void setCprod(String cprod) {
		this.cprod = cprod;
	}

	public void setXprod(String xprod) {
		this.xprod = xprod;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	public void setUnidademedidacomercial(Unidademedida unidademedidacomercial) {
		this.unidademedidacomercial = unidademedidacomercial;
	}

	public void setValorproduto(Double valorproduto) {
		this.valorproduto = valorproduto;
	}

	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}

	public void setValoroutrasdespesas(Money valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}

	public void setCsticms(Tipocobrancaicms csticms) {
		this.csticms = csticms;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setCstipi(Tipocobrancaipi cstipi) {
		this.cstipi = cstipi;
	}

	public void setValorbcipi(Money valorbcipi) {
		this.valorbcipi = valorbcipi;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}

	public void setCstpis(Tipocobrancapis cstpis) {
		this.cstpis = cstpis;
	}

	public void setValorbcpis(Money valorbcpis) {
		this.valorbcpis = valorbcpis;
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}

	public void setCstcofins(Tipocobrancacofins cstcofins) {
		this.cstcofins = cstcofins;
	}

	public void setValorbccofins(Money valorbccofins) {
		this.valorbccofins = valorbccofins;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setCdentregamaterial(Integer cdentregamaterial) {
		this.cdentregamaterial = cdentregamaterial;
	}

	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
		
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaInspecao(List<Entregamaterialinspecao> listaInspecao) {
		this.listaInspecao = listaInspecao;
	}
	
//	TRANSIENTES
	
	@Transient
	public Double getValortotalmaterial() {
		if(this.getValorunitario() != null && this.getQtde() != null){
			return this.getValorunitario() * this.qtde;
		}
		return 0d;
	}
	
	@Transient
	public Double getValortotalmaterial_com_icmsst() {
		Double valorqtde = this.getValorunitario() * this.qtde;
		if(getValoricmsst() != null && (getNaoconsideraricmsst() == null || !getNaoconsideraricmsst())){
			valorqtde += getValoricmsst().getValue().doubleValue();
		}
		if(getValoripi() != null){
			valorqtde += getValoripi().getValue().doubleValue();
		}
		return valorqtde;
	}
	
	@DisplayName("Valor Total")
	@Transient
	public Double getValortotaloperacao() {
		return getValortotaloperacao(true);
	}
	
	@Transient
	public Double getValortotaloperacao(boolean consideraripi) {
		Double valorqtde = (this.getValorunitario() != null ? this.getValorunitario() : 0) * (this.qtde != null ? this.qtde : 1d);
		if(getValoricmsst() != null && (getNaoconsideraricmsst() == null || !getNaoconsideraricmsst())){
			valorqtde += getValoricmsst().getValue().doubleValue();
		}
		if(this.getValorfrete() != null){
			valorqtde += this.getValorfrete().getValue().doubleValue();
		}
		if(this.getValordesconto() != null){
			valorqtde -= this.getValordesconto().getValue().doubleValue();
		}
		if(this.getValorseguro() != null){
			valorqtde += this.getValorseguro().getValue().doubleValue();
		}
		if(this.getValoroutrasdespesas() != null){
			valorqtde += this.getValoroutrasdespesas().getValue().doubleValue();
		}
		if(consideraripi && getValoripi() != null){
			valorqtde += getValoripi().getValue().doubleValue();
		}
		if(consideraripi && getValorIcmsDesonerado() != null){
			valorqtde += getValorIcmsDesonerado().getValue().doubleValue();
		}
		return SinedUtil.round(valorqtde, 2);
	}
	
	@Transient
	public Double getValortotal() {
		return valortotal;
	}
	
	@Required
	@Transient
	public Materialclasse getMaterialclassetrans() {
		return materialclassetrans;
	}
	
	@Transient
	@DisplayName("Valor unit�rio")
	public Double getValorunitariotrans() {
		return valorunitario;
	}
	
	public void setMaterialclassetrans(Materialclasse materialclassetrans) {
		this.materialclassetrans = materialclassetrans;
	}
	
	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}
	
	public void setValorunitariotrans(Money valorunitariotrans) {
		this.valorunitariotrans = valorunitariotrans;
	}
	@Transient
	public Double getResto() {
		return resto;
	}
	public void setResto(Double resto) {
		this.resto = resto;
	}
	@DisplayName("Natureza base de c�lculo cr�dito")
	public Naturezabasecalculocredito getNaturezabcc() {
		return naturezabcc;
	}
	
	public void setNaturezabcc(Naturezabasecalculocredito naturezabcc) {
		this.naturezabcc = naturezabcc;
	}

	@DisplayName("Valor da base de c�lculo")
	public Money getValorbciss() {
		return valorbciss;
	}

	public void setValorbciss(Money valorbciss) {
		this.valorbciss = valorbciss;
	}

	@DisplayName("Al�quota (%)")
	public Double getIss() {
		return iss;
	}

	public void setIss(Double iss) {
		this.iss = iss;
	}

	@DisplayName("Valor")
	public Money getValoriss() {
		return valoriss;
	}

	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipioiss")
	@DisplayName("Munic�pio do ISS")
	public Municipio getMunicipioiss() {
		return municipioiss;
	}

	public void setMunicipioiss(Municipio municipioiss) {
		this.municipioiss = municipioiss;
	}

	@DisplayName("Tipo de tributa��o do ISS")
	public Tipotributacaoiss getTipotributacaoiss() {
		return tipotributacaoiss;
	}

	public void setTipotributacaoiss(Tipotributacaoiss tipotributacaoiss) {
		this.tipotributacaoiss = tipotributacaoiss;
	}

	@DisplayName("C�digo da lista de servi�o")
	@MaxLength(7)
	public String getClistservico() {
		return clistservico;
	}

	public void setClistservico(String clistservico) {
		this.clistservico = clistservico;
	}

	@Transient
	public Integer getSequencial() {
		return sequencial;
	}
	
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	
	@DisplayName("Qtde utilizada")
	public Double getQtdeutilizada() {
		return qtdeutilizada;
	}	
	public void setQtdeutilizada(Double qtdeutilizada) {
		this.qtdeutilizada = qtdeutilizada;
	}
	
	@Transient
	public Boolean getShowcomprimento() {
		return showcomprimento;
	}
	public void setShowcomprimento(Boolean showcomprimento) {
		this.showcomprimento = showcomprimento;
	}

	@DisplayName("MVA")
	public Double getValormva() {
		return valormva;
	}
	@DisplayName("N�o considerar no total da nota")
	public Boolean getNaoconsideraricmsst() {
		return naoconsideraricmsst;
	}
	@DisplayName("Origem do produto")
	public Origemproduto getOrigemProduto() {
		return origemProduto;
	}
	@DisplayName("Valor CSLL")
	public Money getValorcsll() {
		return valorcsll;
	}
	@DisplayName("Valor IR")
	public Money getValorir() {
		return valorir;
	}
	@DisplayName("Valor INSS")
	public Money getValorinss() {
		return valorinss;
	}
	@DisplayName("Imposto de Importa��o")
	public Money getValorimpostoimportacao() {
		return valorimpostoimportacao;
	}
	
	@DisplayName("Motivo da desonera��o do ICMS")
	public Motivodesoneracaoicms getMotivoDesoneracaoIcms() {
		return motivoDesoneracaoIcms;
	}
	
	public void setMotivoDesoneracaoIcms(Motivodesoneracaoicms motivoDesoneracaoIcms) {
		this.motivoDesoneracaoIcms = motivoDesoneracaoIcms;
	}
	
	@DisplayName("Valor da desonera��o do ICMS")
	public Money getValorIcmsDesonerado() {
		return valorIcmsDesonerado;
	}
	
	public void setValorIcmsDesonerado(Money valorIcmsDesonerado) {
		this.valorIcmsDesonerado = valorIcmsDesonerado;
	}

	public void setValormva(Double valormva) {
		this.valormva = valormva;
	}
	public void setNaoconsideraricmsst(Boolean naoconsideraricmsst) {
		this.naoconsideraricmsst = naoconsideraricmsst;
	}
	public void setOrigemProduto(Origemproduto origemProduto) {
		this.origemProduto = origemProduto;
	}
	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}
	public void setValorir(Money valorir) {
		this.valorir = valorir;
	}
	public void setValorinss(Money valorinss) {
		this.valorinss = valorinss;
	}
	public void setValorimpostoimportacao(Money valorimpostoimportacao) {
		this.valorimpostoimportacao = valorimpostoimportacao;
	}

	@DisplayName("Al�quota Pis Retido")
	public Double getPisretido() {
		return pisretido;
	}
	@DisplayName("Valor Pis Retido")
	public Money getValorpisretido() {
		return valorpisretido;
	}
	@DisplayName("Al�quota Cofins Retido")
	public Double getCofinsretido() {
		return cofinsretido;
	}
	@DisplayName("Valor Cofins Retido")
	public Money getValorcofinsretido() {
		return valorcofinsretido;
	}

	public void setPisretido(Double pisretido) {
		this.pisretido = pisretido;
	}
	public void setValorpisretido(Money valorpisretido) {
		this.valorpisretido = valorpisretido;
	}
	public void setCofinsretido(Double cofinsretido) {
		this.cofinsretido = cofinsretido;
	}
	public void setValorcofinsretido(Money valorcofinsretido) {
		this.valorcofinsretido = valorcofinsretido;
	}
	
	@DisplayName("Tipo de Lote")
	public Boolean getTipolote() {
		return tipolote;
	}
	@DisplayName("Lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@DisplayName("Lote")
	@OneToMany(mappedBy="entregamaterial")
	public Set<Entregamateriallote> getListaEntregamateriallote() {
		return listaEntregamateriallote;
	}

	public void setTipolote(Boolean tipolote) {
		this.tipolote = tipolote;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setListaEntregamateriallote(Set<Entregamateriallote> listaEntregamateriallote) {
		this.listaEntregamateriallote = listaEntregamateriallote;
	}

	@DisplayName("Local de Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@Transient
	public Ordemcompramaterial getOcm() {
		return ocm;
	}
	
	public void setOcm(Ordemcompramaterial ocm) {
		this.ocm = ocm;
	}
	
	@Transient
	public String getWhereInOrdemcompramaterial() {
		return whereInOrdemcompramaterial;
	}
	
	public void setWhereInOrdemcompramaterial(String whereInOrdemcompramaterial) {
		this.whereInOrdemcompramaterial = whereInOrdemcompramaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	@Transient
	public String getNomematerialOrdemcompramaterial() {
		return nomematerialOrdemcompramaterial;
	}
	public void setNomematerialOrdemcompramaterial(String nomematerialOrdemcompramaterial) {
		this.nomematerialOrdemcompramaterial = nomematerialOrdemcompramaterial;
	}
	
	@Transient
	public String getIdOrdemcompra(){
		String idordemcompra = null;
		
		if(this.getEntregadocumento() != null && this.getEntregadocumento().getEntrega() != null && 
				this.getEntregadocumento().getEntrega().getListaOrdemcompraentrega() != null && 
				!this.getEntregadocumento().getEntrega().getListaOrdemcompraentrega().isEmpty()){
			for(Ordemcompraentrega ordemcompraentrega : this.getEntregadocumento().getEntrega().getListaOrdemcompraentrega()){
				if(ordemcompraentrega.getOrdemcompra() != null && ordemcompraentrega.getOrdemcompra().getCdordemcompra() != null){
					idordemcompra = ordemcompraentrega.getOrdemcompra().getCdordemcompra().toString();
					break;
				}
			}
		}
		return idordemcompra;
	}
	
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIcms() {
		return valorFiscalIcms;
	}
	
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIpi() {
		return valorFiscalIpi;
	}

	public void setValorFiscalIcms(ValorFiscal valorFiscalIcms) {
		this.valorFiscalIcms = valorFiscalIcms;
	}

	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi) {
		this.valorFiscalIpi = valorFiscalIpi;
	}	
	
	@DisplayName("VALOR BASE DE C�LCULO FCP")
	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	@DisplayName("AL�Q. FCP (%)")
	public Double getFcp() {
		return fcp;
	}

	@DisplayName("VALOR FCP")
	public Money getValorfcp() {
		return valorfcp;
	}

	@DisplayName("VALOR BASE DE C�LCULO FCP ST")
	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	@DisplayName("AL�Q. FCP ST (%)")
	public Double getFcpst() {
		return fcpst;
	}

	@DisplayName("Valor FCP ST")
	public Money getValorfcpst() {
		return valorfcpst;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}

	@Transient
	public Money getValorcofinsSped(){
		Double cofins = 0.0;
		if(this.getCofins() != null){
			cofins = this.getCofins();
		}
		if(this.getQtdebccofins() != null && this.getQtdebccofins() > 0){
			return new Money(cofins * this.getQtdebccofins());
		}else {
			return this.getValorcofins();
		}
	}
	@Transient
	public Money getValorpisSped(){
		Double pis = 0.0;
		if(this.getPis() != null){
			pis = this.getPis();
		}
		if(this.getQtdebcpis() != null && this.getQtdebcpis() > 0){
			return new Money(pis * this.getQtdebcpis());
		}else {
			return this.getValorpis();
		}
	}

	public Boolean getFaturamentocliente() {
		return faturamentocliente;
	}

	public void setFaturamentocliente(Boolean faturamentocliente) {
		this.faturamentocliente = faturamentocliente;
	}
	
	@Transient
	public Boolean getRemoverItem() {
		return removerItem;
	}
	@Transient
	public Boolean getNovoItemGrade() {
		return novoItemGrade;
	}

	public void setRemoverItem(Boolean removerItem) {
		this.removerItem = removerItem;
	}
	public void setNovoItemGrade(Boolean novoItemGrade) {
		this.novoItemGrade = novoItemGrade;
	}

	@Transient
	public Boolean getExibirButtonGrade() {
		return exibirButtonGrade;
	}
	@Transient
	public Boolean getExibirButtonItemGrade() {
		return exibirButtonItemGrade;
	}
	@Transient
	public Boolean getExibirMontargrade() {
		return exibirMontargrade;
	}
	
	@Transient
	public Boolean getIsEcommerce() {
		return isEcommerce;
	}

	public void setExibirButtonGrade(Boolean exibirButtonGrade) {
		this.exibirButtonGrade = exibirButtonGrade;
	}

	public void setIsEcommerce(Boolean isEcommerce) {
		this.isEcommerce = isEcommerce;
	}

	public void setExibirButtonItemGrade(Boolean exibirButtonItemGrade) {
		this.exibirButtonItemGrade = exibirButtonItemGrade;
	}

	public void setExibirMontargrade(Boolean exibirMontargrade) {
		this.exibirMontargrade = exibirMontargrade;
	}

	public String getWhereInMaterialItenGrade() {
		return whereInMaterialItenGrade;
	}

	public void setWhereInMaterialItenGrade(String whereInMaterialItenGrade) {
		this.whereInMaterialItenGrade = whereInMaterialItenGrade;
	}

	@Transient
	public Grupotributacao getGrupotributacaotrans() {
		return grupotributacaotrans;
	}

	public void setGrupotributacaotrans(Grupotributacao grupotributacaotrans) {
		this.grupotributacaotrans = grupotributacaotrans;
	}
	@Transient
	public Boolean getConsiderarItemGradeForWms() {
		return considerarItemGradeForWms;
	}

	public void setConsiderarItemGradeForWms(Boolean considerarItemGradeForWms) {
		this.considerarItemGradeForWms = considerarItemGradeForWms;
	}
	
	@Transient
	public Money getValorunitarioMoney(){
		if (getValorunitario()!=null){
			return new Money(getValorunitario());
		}else {
			return null;
		}
	}

	@Transient
	public Boolean getOrigemMaterialmestregrade() {
		return origemMaterialmestregrade;
	}
	@Transient
	public List<Ordemcompramaterial> getListaOrdemcompramaterialByMaterialmestregrade() {
		return listaOrdemcompramaterialByMaterialmestregrade;
	}

	public void setOrigemMaterialmestregrade(Boolean origemMaterialmestregrade) {
		this.origemMaterialmestregrade = origemMaterialmestregrade;
	}
	public void setListaOrdemcompramaterialByMaterialmestregrade(List<Ordemcompramaterial> listaOrdemcompramaterialByMaterialmestregrade) {
		this.listaOrdemcompramaterialByMaterialmestregrade = listaOrdemcompramaterialByMaterialmestregrade;
	}
	
	@Transient
	public String getCstIcmsSPED(){
		String cstIcmsSPED = null;
		if(getCsticms() != null){
			if(getMaterial() != null && getMaterial().getOrigemproduto() != null){
				cstIcmsSPED = getMaterial().getOrigemproduto().getValue().toString();
			}
			cstIcmsSPED = (cstIcmsSPED != null ? cstIcmsSPED : "") + getCsticms().getCdnfe();
		}
		return cstIcmsSPED;
	}

	@DisplayName("Base de c�lculo")
	public Money getValorbcii() {
		return valorbcii;
	}

	public void setValorbcii(Money valorbcii) {
		this.valorbcii = valorbcii;
	}

	@DisplayName("Imposto de Importa��o")
	public Money getValoriiItem() {
		return valoriiItem;
	}

	public void setValoriiItem(Money valoriiItem) {
		this.valoriiItem = valoriiItem;
	}

	@DisplayName("Despesas Aduaneiras")
	public Money getValorDespesasAduaneiras() {
		return valorDespesasAduaneiras;
	}

	public void setValorDespesasAduaneiras(Money valorDespesasAduaneiras) {
		this.valorDespesasAduaneiras = valorDespesasAduaneiras;
	}

	@DisplayName("IOF")
	public Money getIof() {
		return iof;
	}

	public void setIof(Money iof) {
		this.iof = iof;
	}
	
	@DisplayName("Seq_Item")
	public Integer getSequenciaitem() {
		return sequenciaitem;
	}
	
	public void setSequenciaitem(Integer sequenciaitem) {
		this.sequenciaitem = sequenciaitem;
	}

	@Transient
	public Double getValorEntrada() {
		return valorEntrada;
	}
	public void setValorEntrada(Double valorEntrada) {
		this.valorEntrada = valorEntrada;
	}
	@Transient
	public Double getQtde_convertida() {
		return qtde_convertida;
	}
	public void setQtde_convertida(Double qtdeConvertida) {
		qtde_convertida = qtdeConvertida;
	}

	@Transient
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	@Transient
	public Boolean getMarcadoPopupNotaIndustrializacao() {
		return marcadoPopupNotaIndustrializacao;
	}
	
	public void setMarcadoPopupNotaIndustrializacao(
			Boolean marcadoPopupNotaIndustrializacao) {
		this.marcadoPopupNotaIndustrializacao = marcadoPopupNotaIndustrializacao;
	}

	@Transient
	public Double getQtdedisponivelNotaIndustrializacao() {
		return qtdedisponivelNotaIndustrializacao;
	}

	@Transient
	public Double getQtdeNotaIndustrializacao() {
		return qtdeNotaIndustrializacao;
	}

	public void setQtdedisponivelNotaIndustrializacao(
			Double qtdedisponivelNotaIndustrializacao) {
		this.qtdedisponivelNotaIndustrializacao = qtdedisponivelNotaIndustrializacao;
	}

	public void setQtdeNotaIndustrializacao(Double qtdeNotaIndustrializacao) {
		this.qtdeNotaIndustrializacao = qtdeNotaIndustrializacao;
	}
	
	@Transient
	public Double getQtdeVenda() {
		return qtdeVenda;
	}
	
	public void setQtdeVenda(Double qtdeVenda) {
		this.qtdeVenda = qtdeVenda;
	}

	@Transient
	public Unidademedida getUnidademedidacomercialAnterior() {
		return unidademedidacomercialAnterior;
	}

	public void setUnidademedidacomercialAnterior(Unidademedida unidademedidacomercialAnterior) {
		this.unidademedidacomercialAnterior = unidademedidacomercialAnterior;
	}

	@Transient
	public Boolean getXmlchecado() {
		return xmlchecado;
	}
	@Transient
	public Double getXmlqtderestante() {
		return xmlqtderestante;
	}
	@Transient
	public Double getXmlqtdeassociada() {
		return xmlqtdeassociada;
	}
	@Transient
	public Double getXmlqtdeinformada() {
		return xmlqtdeinformada;
	}
	@Transient
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@Transient
	public String getWhereInOrdemcompra() {
		return whereInOrdemcompra;
	}
	@Transient
	public Double getQtdeanteriorassociada() {
		return qtdeanteriorassociada;
	}
	
	public void setXmlchecado(Boolean xmlchecado) {
		this.xmlchecado = xmlchecado;
	}
	public void setXmlqtderestante(Double xmlqtderestante) {
		this.xmlqtderestante = xmlqtderestante;
	}
	public void setXmlqtdeassociada(Double xmlqtdeassociada) {
		this.xmlqtdeassociada = xmlqtdeassociada;
	}
	public void setXmlqtdeinformada(Double xmlqtdeinformada) {
		this.xmlqtdeinformada = xmlqtdeinformada;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setWhereInOrdemcompra(String whereInOrdemcompra) {
		this.whereInOrdemcompra = whereInOrdemcompra;
	}
	public void setQtdeanteriorassociada(Double qtdeanteriorassociada) {
		this.qtdeanteriorassociada = qtdeanteriorassociada;
	}

	@Transient
	public Boolean getCheckbox() {
		return checkbox;
	}

	@Transient
	public Double getSaldo() {
		return saldo;
	}

	public void setCheckbox(Boolean checkbox) {
		this.checkbox = checkbox;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	@OneToMany(mappedBy="entregamaterial")
	public List<Notafiscalprodutoitementregamaterial> getListaNotafiscalprodutoitementregamaterial() {
		return listaNotafiscalprodutoitementregamaterial;
	}
	
	public void setListaNotafiscalprodutoitementregamaterial(List<Notafiscalprodutoitementregamaterial> listaNotafiscalprodutoitementregamaterial) {
		this.listaNotafiscalprodutoitementregamaterial = listaNotafiscalprodutoitementregamaterial;
	}

	@Transient
	public Loteestoque getLoteestoqueAux() {
		return loteestoqueAux;
	}
	@Transient
	public Double getQtdeAux() {
		return qtdeAux;
	}

	public void setLoteestoqueAux(Loteestoque loteestoqueAux) {
		this.loteestoqueAux = loteestoqueAux;
	}
	public void setQtdeAux(Double qtdeAux) {
		this.qtdeAux = qtdeAux;
	}

	@Transient
	public String getNumeroLotes() {
		if(getLoteestoque() != null){
			return getLoteestoque().getNumero();
		}else if(SinedUtil.isListNotEmpty(getListaEntregamateriallote())){
			String numerolotes = CollectionsUtil.listAndConcatenate(getListaEntregamateriallote(), "loteestoque.numero", ",");
			if(StringUtils.isNotBlank(numerolotes)){
				return numerolotes.length() > 30 ? numerolotes.substring(0, 30) : numerolotes;
			}
		}
		return null;
	}

	@Transient
	public Double getPercentualBcicms() {
		return percentualBcicms;
	}
	@Transient
	public Double getPercentualBcfcp() {
		return percentualBcfcp;
	}
	@Transient
	public Double getPercentualBcfcpst() {
		return percentualBcfcpst;
	}
	@Transient
	public Double getPercentualBcipi() {
		return percentualBcipi;
	}
	@Transient
	public Double getPercentualBcpis() {
		return percentualBcpis;
	}
	@Transient
	public Double getPercentualBccofins() {
		return percentualBccofins;
	}
	@Transient
	public Double getPercentualBciss() {
		return percentualBciss;
	}
	@Transient
	public Double getPercentualBccsll() {
		return percentualBccsll;
	}
	@Transient
	public Double getPercentualBcir() {
		return percentualBcir;
	}
	@Transient
	public Double getPercentualBcinss() {
		return percentualBcinss;
	}

	public void setPercentualBcicms(Double percentualBcicms) {
		this.percentualBcicms = percentualBcicms;
	}
	
	public void setPercentualBcfcp(Double percentualBcfcp) {
		this.percentualBcfcp = percentualBcfcp;
	}
	
	public void setPercentualBcfcpst(Double percentualBcfcpst) {
		this.percentualBcfcpst = percentualBcfcpst;
	}

	public void setPercentualBcipi(Double percentualBcipi) {
		this.percentualBcipi = percentualBcipi;
	}

	public void setPercentualBcpis(Double percentualBcpis) {
		this.percentualBcpis = percentualBcpis;
	}

	public void setPercentualBccofins(Double percentualBccofins) {
		this.percentualBccofins = percentualBccofins;
	}

	public void setPercentualBciss(Double percentualBciss) {
		this.percentualBciss = percentualBciss;
	}

	public void setPercentualBccsll(Double percentualBccsll) {
		this.percentualBccsll = percentualBccsll;
	}

	public void setPercentualBcir(Double percentualBcir) {
		this.percentualBcir = percentualBcir;
	}

	public void setPercentualBcinss(Double percentualBcinss) {
		this.percentualBcinss = percentualBcinss;
	}

	@Transient
	public Boolean getObrigarlote() {
		return obrigarlote;
	}

	public void setObrigarlote(Boolean obrigarlote) {
		this.obrigarlote = obrigarlote;
	}
	
	@Transient
	public Boolean getHabilitaPesoMedio() {
		return habilitaPesoMedio;
	}
	public void setHabilitaPesoMedio(Boolean habilitaPesoMedio) {
		this.habilitaPesoMedio = habilitaPesoMedio;
	}

	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public Double getValorTotalItem(){
		return (this.getValorunitario() != null ? this.getValorunitario() : 0d) * (this.getQtde() != null ? this.getQtde() : 0d);
	}
	
	@Transient
	public Money getValorImportacaoCalculado() {
		Money valor = new Money();
		
		valor = valor.add(SinedUtil.zeroIfNull(getValorDespesasAduaneiras()));
		valor = valor.add(SinedUtil.zeroIfNull(getValoriiItem()));
		valor = valor.add(SinedUtil.zeroIfNull(getIof()));
		
		return valor;
	}
	
	@Transient
	public Boolean getGerarE200() {
		return gerarE200;
	}

	public void setGerarE200(Boolean gerarE200) {
		this.gerarE200 = gerarE200;
	}
	
	@Transient
	public Double getValorProporcionalApropriacao() {
		return valorProporcionalApropriacao;
	}
	public void setValorProporcionalApropriacao(
			Double valorProporcionalApropriacao) {
		this.valorProporcionalApropriacao = valorProporcionalApropriacao;
	}
	
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	@Transient
	public Integer getIndexLista() {
		return indexLista;
	}
	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}
}