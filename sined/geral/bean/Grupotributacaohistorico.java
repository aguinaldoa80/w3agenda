package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_grupotributacaohistorico", sequenceName = "sq_grupotributacaohistorico")
public class Grupotributacaohistorico implements Log {

	protected Integer cdgrupotributacaohistorico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Grupotributacao grupotributacao;
	protected String observacao;
	
	//Transientes
	private String responsavel;

	@Id
	@GeneratedValue(generator="sq_grupotributacaohistorico",strategy=GenerationType.AUTO)
	public Integer getCdgrupotributacaohistorico() {
		return cdgrupotributacaohistorico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}

	@DisplayName("Observação")
	@MaxLength(10000)
	public String getObservacao() {
		return observacao;
	}
	
	@Transient
	@DisplayName("Responsável")
	public String getResponsavel() {
		
		if(cdusuarioaltera != null && responsavel == null)
			responsavel = TagFunctions.findUserByCd(cdusuarioaltera);
		
		return responsavel;
	}

	public void setCdgrupotributacaohistorico(Integer cdgrupotributacaohistorico) {
		this.cdgrupotributacaohistorico = cdgrupotributacaohistorico;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
}
