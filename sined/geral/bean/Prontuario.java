package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_prontuario", sequenceName="sq_prontuario")
@DisplayName("Prontu�rio")
public class Prontuario implements Log {
	
	private Integer cdprontuario;
	private Colaborador terapeuta;
	private Cliente paciente;
	private Integer numero;
	private Date dtinicio;
	private Hora hrinicio;
	private Date dtfim;
	private Hora hrfim;
	private String observacao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private List<Prontuariohistorico> listaProntuariohistorico;
	
	//Transient
	private String senha;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_prontuario")
	public Integer getCdprontuario() {
		return cdprontuario;
	}
	@Required
	@DisplayName("Terapeuta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdterapeuta")
	public Colaborador getTerapeuta() {
		return terapeuta;
	}
	@Required
	@DisplayName("Paciente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpaciente")
	public Cliente getPaciente() {
		return paciente;
	}
	@DisplayName("N�mero")
	@MaxLength(9)
	public Integer getNumero() {
		return numero;
	}
	@Required
	@DisplayName("Data (in�cio)")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Hora (in�cio)")
	@Required
	public Hora getHrinicio() {
		return hrinicio;
	}
	@DisplayName("Data (fim)")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Hora (fim)")
	public Hora getHrfim() {
		return hrfim;
	}
	@DisplayName("Observa��o")
	@MaxLength(5000)
	public String getObservacao() {
		return observacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="prontuario")
	public List<Prontuariohistorico> getListaProntuariohistorico() {
		return listaProntuariohistorico;
	}
	@Transient
	@Required
	@DisplayName("Senha")
	public String getSenha() {
		return senha;
	}
	
	public void setCdprontuario(Integer cdprontuario) {
		this.cdprontuario = cdprontuario;
	}
	public void setTerapeuta(Colaborador terapeuta) {
		this.terapeuta = terapeuta;
	}
	public void setPaciente(Cliente paciente) {
		this.paciente = paciente;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaProntuariohistorico(List<Prontuariohistorico> listaProntuariohistorico) {
		this.listaProntuariohistorico = listaProntuariohistorico;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}