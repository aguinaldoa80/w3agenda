package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_unidademedidaconversao", sequenceName = "sq_unidademedidaconversao")
@DisplayName("Unidade de medida convers�o")
public class Unidademedidaconversao{

	protected Integer cdunidademedidaconversao;
	protected Unidademedida unidademedida;
	protected Unidademedida unidademedidarelacionada;
	protected Double fracao;
	protected Double qtdereferencia;	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_unidademedidaconversao")
	public Integer getCdunidademedidaconversao() {
		return cdunidademedidaconversao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@Required
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedidarelacionada")
	@DisplayName("Unidade relacionada")
	@Required
	public Unidademedida getUnidademedidarelacionada() {
		return unidademedidarelacionada;
	}
	@MaxLength(15)
	@DisplayName("Fra��o")
	@Required
	public Double getFracao() {
		return fracao;
	}
	@DisplayName("Qtde Refer�ncia")
	@Required
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	
	public void setCdunidademedidaconversao(Integer cdunidademedidaconversao) {
		this.cdunidademedidaconversao = cdunidademedidaconversao;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setUnidademedidarelacionada(Unidademedida unidademedidarelacionada) {
		this.unidademedidarelacionada = unidademedidarelacionada;
	}

	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Unidademedidaconversao)
			return this.cdunidademedidaconversao.equals(((Unidademedidaconversao)obj).cdunidademedidaconversao);
		return super.equals(obj);
	}
	
	
	@Transient
	public Double getFracaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fracao != null)
			valor = (this.fracao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		return valor;
	}
	
}
