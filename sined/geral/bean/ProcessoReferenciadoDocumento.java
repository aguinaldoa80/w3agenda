package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoOperacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.Entradafiscal;

@Entity
@SequenceGenerator(name = "sq_processoreferenciadodocumento", sequenceName = "sq_processoreferenciadodocumento")
public class ProcessoReferenciadoDocumento {

	protected Integer cdProcessoReferenciadoDocumento;
	protected TipoOperacaoEnum tipoOperacao;
	protected Pessoa participante;
	protected Notafiscalproduto notaFiscalProduto;
	protected Entregadocumento entradaFiscal;
	protected Date dataOperacao;
	protected Money valorOperacao;
	protected Material material;
	protected Tipocobrancapis cstPis;
	protected Double aliqPis;
	protected Money vlPis;
	protected Money vlBcPis;
	protected Tipocobrancacofins cstCofins;
	protected Double aliqCofins;
	protected Money vlCofins;
	protected Money vlBcCofins;
	protected Tipocobrancapis cstPisSuspensa;
	protected Double bcPisSuspenso;
	protected Double aliqPisSuspenso;
	protected Double vlrPisSuspenso;
	protected Tipocobrancacofins cstCofinsSuspenso;
	protected Double bcCofinsSuspenso;
	protected Double aliqCofinsSuspenso;
	protected Double vlrCofinsSuspenso;
	protected ContaContabil contaContabil;
	protected String descricaoDocumento;
	protected ProcessoReferenciado processoReferenciado;
	protected Tipocalculo tipoCalculoPis;
	protected Tipocalculo tipoCalculoCofins;
	protected Double qtdevendidapis;
	protected Double qtdevendidaCofins;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processoreferenciadodocumento")
	public Integer getCdProcessoReferenciadoDocumento() {
		return cdProcessoReferenciadoDocumento;
	}
	public void setCdProcessoReferenciadoDocumento(
			Integer cdProcessoReferenciadoDocumento) {
		this.cdProcessoReferenciadoDocumento = cdProcessoReferenciadoDocumento;
	}

	@Required
	public TipoOperacaoEnum getTipoOperacao() {
		return tipoOperacao;
	}
	public void setTipoOperacao(TipoOperacaoEnum tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getParticipante() {
		return participante;
	}
	public void setParticipante(Pessoa participante) {
		this.participante = participante;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Notafiscalproduto getNotaFiscalProduto() {
		return notaFiscalProduto;
	}
	public void setNotaFiscalProduto(Notafiscalproduto notaFiscalProduto) {
		this.notaFiscalProduto = notaFiscalProduto;
	}
	
	@Required
	public Date getDataOperacao() {
		return dataOperacao;
	}
	public void setDataOperacao(Date dataOperacao) {
		this.dataOperacao = dataOperacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntradaFiscal() {
		return entradaFiscal;
	}
	public void setEntradaFiscal(Entregadocumento entradaFiscal) {
		this.entradaFiscal = entradaFiscal;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}

	@Required
	public Tipocobrancapis getCstPis() {
		return cstPis;
	}
	public void setCstPis(Tipocobrancapis cstPis) {
		this.cstPis = cstPis;
	}
	@Required
	public Tipocobrancacofins getCstCofins() {
		return cstCofins;
	}
	public void setCstCofins(Tipocobrancacofins cstCofins) {
		this.cstCofins = cstCofins;
	}
	public Tipocobrancapis getCstPisSuspensa() {
		return cstPisSuspensa;
	}
	public void setCstPisSuspensa(Tipocobrancapis cstPisSuspensa) {
		this.cstPisSuspensa = cstPisSuspensa;
	}
	public Tipocobrancacofins getCstCofinsSuspenso() {
		return cstCofinsSuspenso;
	}
	public void setCstCofinsSuspenso(Tipocobrancacofins cstCofinsSuspenso) {
		this.cstCofinsSuspenso = cstCofinsSuspenso;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public String getDescricaoDocumento() {
		return descricaoDocumento;
	}
	public void setDescricaoDocumento(String descricaoDocumento) {
		this.descricaoDocumento = descricaoDocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdProcessoReferenciado")
	public ProcessoReferenciado getProcessoReferenciado() {
		return processoReferenciado;
	}
	public void setProcessoReferenciado(ProcessoReferenciado processoReferenciado) {
		this.processoReferenciado = processoReferenciado;
	}
	@Required
	public Money getValorOperacao() {
		return valorOperacao;
	}
	public void setValorOperacao(Money valorOperacao) {
		this.valorOperacao = valorOperacao;
	}
	@Required
	@DisplayName("Aliquota Pis")
	public Double getAliqPis() {
		return aliqPis;
	}
	public void setAliqPis(Double aliqPis) {
		this.aliqPis = aliqPis;
	}
	@Required
	@DisplayName("Aliquota Cofins")
	public Double getAliqCofins() {
		return aliqCofins;
	}
	public void setAliqCofins(Double aliqCofins) {
		this.aliqCofins = aliqCofins;
	}
	@DisplayName("Base de C�lculo Pis Suspenso")
	public Double getBcPisSuspenso() {
		return bcPisSuspenso;
	}
	public void setBcPisSuspenso(Double bcPisSuspenso) {
		this.bcPisSuspenso = bcPisSuspenso;
	}
	@DisplayName("Aliquota Pis Suspenso")
	public Double getAliqPisSuspenso() {
		return aliqPisSuspenso;
	}
	public void setAliqPisSuspenso(Double aliqPisSuspenso) {
		this.aliqPisSuspenso = aliqPisSuspenso;
	}
	@DisplayName("Valor Pis Suspenso")
	public Double getVlrPisSuspenso() {
		return vlrPisSuspenso;
	}
	public void setVlrPisSuspenso(Double vlrPisSuspenso) {
		this.vlrPisSuspenso = vlrPisSuspenso;
	}
	@DisplayName("Base de C�lculo Cofins Suspenso")
	public Double getBcCofinsSuspenso() {
		return bcCofinsSuspenso;
	}
	public void setBcCofinsSuspenso(Double bcCofinsSuspenso) {
		this.bcCofinsSuspenso = bcCofinsSuspenso;
	}
	@DisplayName("Aliquota Cofins Suspenso")
	public Double getAliqCofinsSuspenso() {
		return aliqCofinsSuspenso;
	}
	public void setAliqCofinsSuspenso(Double aliqCofinsSuspenso) {
		this.aliqCofinsSuspenso = aliqCofinsSuspenso;
	}
	@DisplayName("Valor Cofins Suspenso")
	public Double getVlrCofinsSuspenso() {
		return vlrCofinsSuspenso;
	}
	public void setVlrCofinsSuspenso(Double vlrCofinsSuspenso) {
		this.vlrCofinsSuspenso = vlrCofinsSuspenso;
	}
	@DisplayName("Valor Pis")
	public Money getVlPis() {
		return vlPis;
	}
	public void setVlPis(Money vlPis) {
		this.vlPis = vlPis;
	}
	@DisplayName("Valor Base de C�lculo Pis")
	public Money getVlBcPis() {
		return vlBcPis;
	}
	public void setVlBcPis(Money vlBcPis) {
		this.vlBcPis = vlBcPis;
	}
	@DisplayName("Valor Cofins")
	public Money getVlCofins() {
		return vlCofins;
	}
	public void setVlCofins(Money vlCofins) {
		this.vlCofins = vlCofins;
	}
	@DisplayName("Valor Base de C�lculo Cofins")
	public Money getVlBcCofins() {
		return vlBcCofins;
	}
	public void setVlBcCofins(Money vlBcCofins) {
		this.vlBcCofins = vlBcCofins;
	}
	public Tipocalculo getTipoCalculoPis() {
		return tipoCalculoPis;
	}
	public void setTipoCalculoPis(Tipocalculo tipoCalculoPis) {
		this.tipoCalculoPis = tipoCalculoPis;
	}
	public Tipocalculo getTipoCalculoCofins() {
		return tipoCalculoCofins;
	}
	public void setTipoCalculoCofins(Tipocalculo tipoCalculoCofins) {
		this.tipoCalculoCofins = tipoCalculoCofins;
	}
	@DisplayName("Quantidade Vendida Pis")
	public Double getQtdevendidapis() {
		return qtdevendidapis;
	}
	public void setQtdevendidapis(Double qtdevendidapis) {
		this.qtdevendidapis = qtdevendidapis;
	}
	@DisplayName("Quantidade Vendida Cofins")
	public Double getQtdevendidaCofins() {
		return qtdevendidaCofins;
	}
	public void setQtdevendidaCofins(Double qtdevendidaCofins) {
		this.qtdevendidaCofins = qtdevendidaCofins;
	}
	
	
}
