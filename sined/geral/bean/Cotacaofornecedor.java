package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_cotacaofornecedor",sequenceName="sq_cotacaofornecedor")
@DisplayName("Cota��o")
public class Cotacaofornecedor implements Log{
	
	protected Integer cdcotacaofornecedor;
	protected Cotacao cotacao;
	protected Fornecedor fornecedor;
	protected Contato contato;
	protected Boolean conviteenviado;
	protected Money desconto;
	protected Prazopagamento prazopagamento;
	protected String observacao;
	protected Date dtvalidade;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	protected List<Cotacaofornecedoritem> listaCotacaofornecedoritem = new ListSet<Cotacaofornecedoritem>(Cotacaofornecedoritem.class);
	
//	TRANSIENTES
	protected Contato contatotrans;
	protected Fornecedor fornecedortrans;
	protected List<Cotacaofornecedoritem> listaCotacaofornecedoritemAux;
	protected Money total;
	protected Double valorTotalFornecedorItem;
	
	protected Boolean enviaranexonoemail = Boolean.TRUE;
	protected Boolean checado;
	
	public Cotacaofornecedor(){
	}

	public Cotacaofornecedor(Fornecedor fornecedor){
		this.fornecedor = fornecedor;
	}
	
	@Id
	@GeneratedValue(generator="sq_cotacaofornecedor",strategy=GenerationType.AUTO)
	public Integer getCdcotacaofornecedor() {
		return cdcotacaofornecedor;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcotacao")
	public Cotacao getCotacao() {
		return cotacao;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	public Contato getContato() {
		return contato;
	}

	public Boolean getConviteenviado() {
		return conviteenviado;
	}

	public Money getDesconto() {
		return desconto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@MaxLength(200)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Validade")
	public Date getDtvalidade() {
		return dtvalidade;
	}

	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}

	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setConviteenviado(Boolean conviteenviado) {
		this.conviteenviado = conviteenviado;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdcotacaofornecedor(Integer cdcotacaofornecedor) {
		this.cdcotacaofornecedor = cdcotacaofornecedor;
	}
	
	@OneToMany(mappedBy="cotacaofornecedor")
	public List<Cotacaofornecedoritem> getListaCotacaofornecedoritem() {
		return listaCotacaofornecedoritem;
	}
	
	public void setListaCotacaofornecedoritem(List<Cotacaofornecedoritem> listaCotacaofornecedoritem) {
		this.listaCotacaofornecedoritem = listaCotacaofornecedoritem;
	}
	
	/* API */
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	TRANSIENTES
	@Transient
	@DisplayName("Contato")
	public Contato getContatotrans() {
		return contatotrans;
	}
	
	public void setContatotrans(Contato contatotrans) {
		this.contatotrans = contatotrans;
	}
	@Transient
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedortrans() {
		return fornecedortrans;
	}
	
	public void setFornecedortrans(Fornecedor fornecedortrans) {
		this.fornecedortrans = fornecedortrans;
	}
	
	@Transient
	public List<Cotacaofornecedoritem> getListaCotacaofornecedoritemAux() {
		return listaCotacaofornecedoritemAux;
	}
	
	public void setListaCotacaofornecedoritemAux(
			List<Cotacaofornecedoritem> listaCotacaofornecedoritemAux) {
		this.listaCotacaofornecedoritemAux = listaCotacaofornecedoritemAux;
	}
	
	@Transient
	public String getMateriais(){
		String string = "";
		if (getListaCotacaofornecedoritem() != null && getListaCotacaofornecedoritem().size() > 0) {
			string = SinedUtil.listAndConcatenateSort(getListaCotacaofornecedoritem(), "material.nome", "<BR>");
		}
		return string;
	}
	
	@Transient
	public String getLocais(){
		String string = "";
		if (getListaCotacaofornecedoritem() != null && getListaCotacaofornecedoritem().size() > 0) {
			string = SinedUtil.listAndConcatenateSort(getListaCotacaofornecedoritem(), "localarmazenagem.nome", "<BR>");
		}
		return string;
	}
	
	@Transient
	@DisplayName("Anexo")
	public Boolean getEnviaranexonoemail() {
		return enviaranexonoemail;
	}
	
	public void setEnviaranexonoemail(Boolean enviaranexonoemail) {
		this.enviaranexonoemail = enviaranexonoemail;
	}
	
	@Transient
	public Boolean getChecado() {
		return checado;
	}
	
	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	
	@Transient
	@DisplayName("Total")
	public Money getTotal() {
		return total;
	}
	
	public void setTotal(Money total) {
		this.total = total;
	}

	@Transient
	public Double getValorTotalFornecedorItem() {
		return valorTotalFornecedorItem;
	}

	public void setValorTotalFornecedorItem(Double valorTotalFornecedorItem) {
		this.valorTotalFornecedorItem = valorTotalFornecedorItem;
	}

	@Transient
	public Money getTotalDescontoItens() {
		Money total = new Money();
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getDesconto() != null) total = total.add(item.getDesconto());
			}
		}
		return total;
	}

	@Transient
	public Money getTotalImpostoItens() {
		Double total = 0.0;
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getIcmsiss() != null) total += (item.getIcmsiss() / 100) * (item.getValorSemDesconto() != null ? item.getValorSemDesconto() : 0.0);
				if(item.getIpi() != null) total += (item.getIpi() / 100) * (item.getValorSemDesconto() != null ? item.getValorSemDesconto() : 0.0);
			}
		}
		return new Money(total);
	}
	@Transient
	public Money getTotalFreteItens() {
		Double total = 0.0;
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getFrete() != null) total += item.getFrete();
			}
		}
		return new Money(total);
	}
	@Transient
	public Money getTotalItens() {
		Double total = 0.0;
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getValor() != null) total += item.getValor();
			}
		}
		return new Money(total);
	}
	
	@Transient
	public Money getTotalItensCotacao() {
		Double total = 0.0;
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getValor() != null) total += item.getValor();
				if(item.getIcmsiss() != null && item.getIcmsiss() > 0 && 
						(item.getIcmsissincluso() == null || !item.getIcmsissincluso())){
					total += (item.getIcmsiss() / 100) * (item.getValor() != null ? item.getValor() : 0.0);
				}
				if(item.getIpi() != null && item.getIpi() > 0 && 
						(item.getIpiincluso() == null || !item.getIpiincluso())){
					total += (item.getIpi() / 100) * (item.getValorSemDesconto() != null ? item.getValorSemDesconto() : 0.0);
				}
				if(item.getFrete() != null && item.getFrete() > 0){
					total += item.getFrete();
				}
			}
		}
		return new Money(total);
	}
	
	@Transient
	public Boolean isTodosItensGerados(){
		Boolean todositensgerados = true;
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getOrdemcompragerada() == null || !item.getOrdemcompragerada()){
					todositensgerados = false;
				}
			}
		}else 
			todositensgerados = false;
		
		return todositensgerados;
	}

	@Transient
	public Date getdtEntregatrans() {
		if(this.listaCotacaofornecedoritem != null && !this.listaCotacaofornecedoritem.isEmpty()){
			for(Cotacaofornecedoritem item : this.listaCotacaofornecedoritem){
				if(item.getDtentrega() != null) return item.getDtentrega();
			}
		}
		return null;
	}
}
