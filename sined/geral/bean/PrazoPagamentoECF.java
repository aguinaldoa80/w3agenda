package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_prazopagamentoecf", sequenceName = "sq_prazopagamentoecf")
public class PrazoPagamentoECF {

	private Integer cdPrazoPagamentoECF;
	private Integer codECF;
	private Prazopagamento prazoPagamento;
	private Documentotipo documentoTipo;
	
	////////////////////////|
	/// INICIO DOS get   ///|
	////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_prazopagamentoecf")
	public Integer getCdPrazoPagamentoECF() {
		return cdPrazoPagamentoECF;
	}
	@Required
	@DisplayName("C�digo Finalizadora (ECF)")
	public Integer getCodECF() {
		return codECF;
	}
	@JoinColumn(name="cdprazopagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Prazopagamento getPrazoPagamento() {
		return prazoPagamento;
	}
	@JoinColumn(name="cddocumentotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Documentotipo getDocumentoTipo() {
		return documentoTipo;
	}
	////////////////////////|
	/// INICIO DOS set   ///|
	////////////////////////|
	public void setCdPrazoPagamentoECF(Integer cdPrazoPagamentoECF) {
		this.cdPrazoPagamentoECF = cdPrazoPagamentoECF;
	}
	public void setCodECF(Integer codECF) {
		this.codECF = codECF;
	}
	public void setPrazoPagamento(Prazopagamento prazoPagamento) {
		this.prazoPagamento = prazoPagamento;
	}
	
	public void setDocumentoTipo(Documentotipo documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	
}
