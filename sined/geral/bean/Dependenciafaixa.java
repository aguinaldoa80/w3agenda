package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Rela��o de faixas de depend�ncia")
@SequenceGenerator(name = "sq_dependenciafaixa", sequenceName = "sq_dependenciafaixa")
public class Dependenciafaixa {
	
	protected Integer cddependenciafaixa;
	protected Relacaocargo relacaocargo;
	protected Double faixade;
	protected Double faixaate;
	protected Double quantidade;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dependenciafaixa")	
	public Integer getCddependenciafaixa() {
		return cddependenciafaixa;
	}
	
	@DisplayName("Rela��o entre cargos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdrelacaocargo")
	@Required	
	public Relacaocargo getRelacaocargo() {
		return relacaocargo;
	}
	
	@DisplayName("De")
	@MinValue(1)
	@MaxValue(999999999)
	@Required
	public Double getFaixade() {
		return faixade;
	}
	
	@DisplayName("At�")
	@MinValue(1)
	@MaxValue(999999999)
	public Double getFaixaate() {
		return faixaate;
	}
	
	@DisplayName("Quantidade")
	@MinValue(0)
	@MaxValue(999999999)
	@Required	
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setCddependenciafaixa(Integer cddependenciafaixa) {
		this.cddependenciafaixa = cddependenciafaixa;
	}
	
	public void setRelacaocargo(Relacaocargo relacaocargo) {
		this.relacaocargo = relacaocargo;
	}
	
	public void setFaixade(Double faixade) {
		this.faixade = faixade;
	}
	
	public void setFaixaate(Double faixaate) {
		this.faixaate = faixaate;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
}
