package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.BoletoCobrancaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoarquivoremessa;

@Entity
@SequenceGenerator(name = "sq_contacarteira", sequenceName = "sq_contacarteira")
public class Contacarteira {

	protected Integer cdcontacarteira;
	protected Conta conta;
	protected String convenio;
	protected String conveniolider;	
	protected String carteira;
	protected String variacaocarteira;
	protected String carteiracarne;
	protected String codigocarteira;
	protected Tipoarquivoremessa tipoarquivoremessa;	
	protected Boolean fatorvencimentozerado;
	protected String msgboleto1;
	protected String msgboleto2;	
	protected Boolean bancogeranossonumero;
	protected String idemissaoboleto;
	protected String idemissaoboletoimpresso;
	protected String iddistribuicao;	
	protected String tipocobranca;
	protected String instrucao1;
	protected String instrucao2;
	protected Integer qtdedias;
	protected String especiedocumento;
	protected String codigotransmissao;
	protected String especiedocumentoremessa;
	protected String aceito;
	protected Boolean protestoautomaticoremessa;
	protected Boolean enviardescontoremessa;
	protected Boolean enviarmultaremessa;
	protected Boolean enviarmoraremessa;
	protected BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	protected BancoConfiguracaoRetorno bancoConfiguracaoRetorno;
	protected BancoConfiguracaoRemessa bancoConfiguracaoRemessaPagamento;
	protected BancoConfiguracaoRetorno bancoConfiguracaoRetornoPagamento;	
	protected boolean padrao = false;
	protected Integer qtdediasdevolucao;
	protected Integer qtdediasdistribuicao;
	protected BoletoCobrancaEnum boletocobranca;
	protected String operacao;
	protected Boolean aprovadoproducao;
	protected Boolean escritural;
	protected String limiteposicoesintervalonossonumero;
	protected Boolean naoassociarcontareceber; 
	protected Boolean naogerarboleto;
	protected Boolean nossonumeroindividual;
	protected Integer nossonumero;
	
	public Contacarteira() {}
	
	public Contacarteira(Integer cdcontacarteira) {
		this.cdcontacarteira = cdcontacarteira;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacarteira")
	@DisplayName("Id")
	public Integer getCdcontacarteira() {
		return cdcontacarteira;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}
	@MaxLength(16)
	@DisplayName("Conv�nio")
	public String getConvenio() {
		return convenio;
	}
	@DisplayName("Carteira")
	@MaxLength(3)
	public String getCarteira() {
		return carteira;
	}
	@DisplayName("Varia��o da Carteira")
	@MaxLength(3)
	public String getVariacaocarteira() {
		return variacaocarteira;
	}
	@DisplayName("Carteira (carn�)")
	@MaxLength(3)
	public String getCarteiracarne() {
		return carteiracarne;
	}
	@DisplayName("Sem fator vencimento")
	public Boolean getFatorvencimentozerado() {
		return fatorvencimentozerado;
	}
	@MaxLength(80)
	@DisplayName("Mensagem padr�o do boleto 1")
	public String getMsgboleto1() {
		return msgboleto1;
	}
	@MaxLength(80)
	@DisplayName("Mensagem padr�o do boleto 2")
	public String getMsgboleto2() {
		return msgboleto2;
	}
	@DisplayName("Padr�o arquivo remessa")
	public Tipoarquivoremessa getTipoarquivoremessa() {
		return tipoarquivoremessa;
	}
	@DisplayName("Nosso n�mero gerado pelo banco")
	public Boolean getBancogeranossonumero() {
		return bancogeranossonumero;
	}

	@DisplayName("C�digo da carteira")
	@MaxLength(1)
	public String getCodigocarteira() {
		return codigocarteira;
	}
	@DisplayName("Identifica��o da emiss�o do bloqueto")
	@MaxLength(1)
	public String getIdemissaoboleto() {
		return idemissaoboleto;
	}
	@DisplayName("Identifica��o da emiss�o do bloqueto (Impresso)")
	@MaxLength(1)
	public String getIdemissaoboletoimpresso() {
		return idemissaoboletoimpresso;
	}
	@DisplayName("Identifica��o da distribui��o do bloqueto")
	@MaxLength(1)
	public String getIddistribuicao() {
		return iddistribuicao;
	}
	@DisplayName("Conv�nio l�der")
	@MaxLength(16)
	public String getConveniolider() {
		return conveniolider;
	}
	@DisplayName("Tipo de cobran�a / Modalidade")
	@MaxLength(5)
	public String getTipocobranca() {
		return tipocobranca;
	}
	@DisplayName("Instru��o de cobran�a 1")
	@MaxLength(2)
	public String getInstrucao1() {
		return instrucao1;
	}
	@DisplayName("Instru��o de cobran�a 2")
	@MaxLength(2)
	public String getInstrucao2() {
		return instrucao2;
	}
	@DisplayName("Quantidade de dias para protesto")
	public Integer getQtdedias() {
		return qtdedias;
	}
	@DisplayName("Esp�cie do documento")
	@MaxLength(3)
	public String getEspeciedocumento() {
		return especiedocumento;
	}
	@DisplayName("C�digo de transmiss�o")
	@MaxLength(20)
	public String getCodigotransmissao() {
		return codigotransmissao;
	}
	@DisplayName("Esp�cie de documento (Remessa)")
	@MaxLength(2)
	public String getEspeciedocumentoremessa() {
		return especiedocumentoremessa;
	}
	@MaxLength(1)
	public String getAceito() {
		return aceito;
	}
	@DisplayName("Protesto autom�tico (Remessa)")
	public Boolean getProtestoautomaticoremessa() {
		return protestoautomaticoremessa;
	}
	@DisplayName("Enviar desconto na remessa")
	public Boolean getEnviardescontoremessa() {
		return enviardescontoremessa;
	}
	@DisplayName("Enviar multa na remessa")
	public Boolean getEnviarmultaremessa() {
		return enviarmultaremessa;
	}
	@DisplayName("Enviar mora na remessa")
	public Boolean getEnviarmoraremessa() {
		return enviarmoraremessa;
	}
	@DisplayName("Arquivo de remessa configur�vel (Cobran�a)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	@DisplayName("Arquivo de retorno configur�vel (Cobran�a)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretorno")
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetorno() {
		return bancoConfiguracaoRetorno;
	}
	@DisplayName("Arquivo de remessa configur�vel (Pagamento)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessapagamento")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessaPagamento() {
		return bancoConfiguracaoRemessaPagamento;
	}
	@DisplayName("Arquivo de retorno configur�vel (Pagamento)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoretornopagamento")
	public BancoConfiguracaoRetorno getBancoConfiguracaoRetornoPagamento() {
		return bancoConfiguracaoRetornoPagamento;
	}
	@DisplayName("Padr�o")
	public boolean isPadrao() {
		return padrao;
	}
	@DisplayName("Quantidade de dias para devolu��o")
	public Integer getQtdediasdevolucao() {
		return qtdediasdevolucao;
	}
	@DisplayName("Quantidade de dias para distribui��o")
	public Integer getQtdediasdistribuicao() {
		return qtdediasdistribuicao;
	}
	@DisplayName("Tipo de cobran�a")
	@Enumerated(EnumType.STRING)
	public BoletoCobrancaEnum getBoletocobranca() {
		return boletocobranca;
	}
	@DisplayName("OP. p/ boleto")
	@MaxLength(3)
	public String getOperacao() {
		return operacao;
	}
	@DisplayName("Aprovado produ��o")
	public Boolean getAprovadoproducao() {
		return aprovadoproducao;
	}
	
	@DisplayName("Limite do nosso n�mero")
	@MaxLength(50)
	public String getLimiteposicoesintervalonossonumero() {
		return limiteposicoesintervalonossonumero;
	}
	
	@DisplayName("N�o permitir associar a conta a receber")
	public Boolean getNaoassociarcontareceber() {
		return naoassociarcontareceber;
	}
	
	@DisplayName("N�o gerar boleto")
	public Boolean getNaogerarboleto() {
		return naogerarboleto;
	}

	public void setAprovadoproducao(Boolean aprovadoproducao) {
		this.aprovadoproducao = aprovadoproducao;
	}
	
	public void setCdcontacarteira(Integer cdcontacarteira) {
		this.cdcontacarteira = cdcontacarteira;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}
	public void setVariacaocarteira(String variacaocarteira) {
		this.variacaocarteira = variacaocarteira;
	}
	public void setCarteiracarne(String carteiracarne) {
		this.carteiracarne = carteiracarne;
	}
	public void setFatorvencimentozerado(Boolean fatorvencimentozerado) {
		this.fatorvencimentozerado = fatorvencimentozerado;
	}
	public void setMsgboleto1(String msgboleto1) {
		this.msgboleto1 = msgboleto1;
	}
	public void setMsgboleto2(String msgboleto2) {
		this.msgboleto2 = msgboleto2;
	}
	public void setTipoarquivoremessa(Tipoarquivoremessa tipoarquivoremessa) {
		this.tipoarquivoremessa = tipoarquivoremessa;
	}
	public void setBancogeranossonumero(Boolean bancogeranossonumero) {
		this.bancogeranossonumero = bancogeranossonumero;
	}
	public void setCodigocarteira(String codigocarteira) {
		this.codigocarteira = codigocarteira;
	}
	public void setIdemissaoboleto(String idemissaoboleto) {
		this.idemissaoboleto = idemissaoboleto;
	}
	public void setIdemissaoboletoimpresso(String idemissaoboletoimpresso) {
		this.idemissaoboletoimpresso = idemissaoboletoimpresso;
	}
	public void setIddistribuicao(String iddistribuicao) {
		this.iddistribuicao = iddistribuicao;
	}
	public void setConveniolider(String conveniolider) {
		this.conveniolider = conveniolider;
	}
	public void setTipocobranca(String tipocobranca) {
		this.tipocobranca = tipocobranca;
	}
	public void setInstrucao1(String instrucao1) {
		this.instrucao1 = instrucao1;
	}
	public void setInstrucao2(String instrucao2) {
		this.instrucao2 = instrucao2;
	}
	public void setQtdedias(Integer qtdedias) {
		this.qtdedias = qtdedias;
	}
	public void setEspeciedocumento(String especiedocumento) {
		this.especiedocumento = especiedocumento;
	}
	public void setCodigotransmissao(String codigotransmissao) {
		this.codigotransmissao = codigotransmissao;
	}
	public void setEspeciedocumentoremessa(String especiedocumentoremessa) {
		this.especiedocumentoremessa = especiedocumentoremessa;
	}
	public void setAceito(String aceito) {
		this.aceito = aceito;
	}
	public void setProtestoautomaticoremessa(Boolean protestoautomaticoremessa) {
		this.protestoautomaticoremessa = protestoautomaticoremessa;
	}
	public void setEnviardescontoremessa(Boolean enviardescontoremessa) {
		this.enviardescontoremessa = enviardescontoremessa;
	}
	public void setEnviarmultaremessa(Boolean enviarmultaremessa) {
		this.enviarmultaremessa = enviarmultaremessa;
	}
	public void setEnviarmoraremessa(Boolean enviarmoraremessa) {
		this.enviarmoraremessa = enviarmoraremessa;
	}
	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	public void setBancoConfiguracaoRetorno(
			BancoConfiguracaoRetorno bancoConfiguracaoRetorno) {
		this.bancoConfiguracaoRetorno = bancoConfiguracaoRetorno;
	}
	public void setBancoConfiguracaoRemessaPagamento(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessaPagamento) {
		this.bancoConfiguracaoRemessaPagamento = bancoConfiguracaoRemessaPagamento;
	}
	public void setBancoConfiguracaoRetornoPagamento(
			BancoConfiguracaoRetorno bancoConfiguracaoRetornoPagamento) {
		this.bancoConfiguracaoRetornoPagamento = bancoConfiguracaoRetornoPagamento;
	}
	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}	
	public void setQtdediasdevolucao(Integer qtdediasdevolucao) {
		this.qtdediasdevolucao = qtdediasdevolucao;
	}
	public void setQtdediasdistribuicao(Integer qtdediasdistribuicao) {
		this.qtdediasdistribuicao = qtdediasdistribuicao;
	}
	public void setBoletocobranca(BoletoCobrancaEnum boletocobranca) {
		this.boletocobranca = boletocobranca;
	}
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	public Boolean getEscritural() {
		return escritural;
	}
	public void setEscritural(Boolean escritural) {
		this.escritural = escritural;
	}
	public void setLimiteposicoesintervalonossonumero(
			String limiteposicoesintervalonossonumero) {
		this.limiteposicoesintervalonossonumero = limiteposicoesintervalonossonumero;
	}
	public void setNaoassociarcontareceber(Boolean naoassociarcontareceber) {
		this.naoassociarcontareceber = naoassociarcontareceber;
	}
	public void setNaogerarboleto(Boolean naogerarboleto) {
		this.naogerarboleto = naogerarboleto;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"carteira", "variacaocarteira"})
	public String getDescriptionProperty(){
		StringBuilder sb = new StringBuilder();
		sb.append(Util.strings.emptyIfNull(this.carteira));
		if(StringUtils.isNotBlank(this.variacaocarteira)){
			sb.append(StringUtils.isNotBlank(this.carteira) ? " - " : "").append(variacaocarteira);
		}
		return sb.toString();
	}
	
	@DisplayName("Nosso n�mero individual")
	public Boolean getNossonumeroindividual() {
		return nossonumeroindividual;
	}
	public void setNossonumeroindividual(Boolean nossonumeroindividual) {
		this.nossonumeroindividual = nossonumeroindividual;
	}
	
	@Transient
	public Integer getNossonumero() {
		return nossonumero;
	}
	public void setNossonumero(Integer nossonumero) {
		this.nossonumero = nossonumero;
	}
}
