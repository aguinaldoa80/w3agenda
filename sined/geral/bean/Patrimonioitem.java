package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.view.Vpatrimonioitem;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_patrimonioitem",sequenceName="sq_patrimonioitem")
public class Patrimonioitem implements Log{
	
	protected Integer cdpatrimonioitem;
	protected Bempatrimonio bempatrimonio;
	protected String plaqueta;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Date dtaquisicao;
	protected Empresa empresa;
	protected Fornecimentocontrato fornecimentocontrato;
	protected Arquivo foto;
	protected Vpatrimonioitem vpatrimonioitem;
	protected Date dtregistroperda;
	protected Materialnumeroserie materialnumeroserie;
	
	protected List<Movpatrimonio> listaMovpatrimonio;
	protected List<Patrimonioitemhistorico> listaHistorico;
	
//	TRANSIENTES
	protected Boolean naoexisteplaqueta;
	protected String observacao;
	protected Boolean fromVeiculo;
	protected String empresafornecedor;
	protected String aquisicaolocacao;
	protected String localizacao;
	protected String localnome;
	protected Money valorPatrimonioitemReport;
	protected Money valoratualPatrimonioitemReport;
	protected Double horimetro;
	protected String clientenomelistagem;
	protected String localnomelistagem;
	
	public Patrimonioitem(){
	}
	
	public Patrimonioitem(Bempatrimonio bempatrimonio){
		this.bempatrimonio = bempatrimonio;
	}
	
	public Patrimonioitem(Integer cdpatrimonioitem) {
		this.cdpatrimonioitem = cdpatrimonioitem;
	}
	
	public Patrimonioitem(Integer cdpatrimonioitem, String localnome) {
		this.cdpatrimonioitem = cdpatrimonioitem;
		this.localnome = localnome;
	}

	//	TRANSIENTES
	protected String descricao;
	
	@Id
	@GeneratedValue(generator="sq_patrimonioitem",strategy=GenerationType.AUTO)
	public Integer getCdpatrimonioitem() {
		return cdpatrimonioitem;
	}
	
	@Required
	@DisplayName("Material (Patrim�nio)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbempatrimonio")
	public Bempatrimonio getBempatrimonio() {
		return bempatrimonio;
	}

	@Required
	@MaxLength(50)
	public String getPlaqueta() {
		return plaqueta;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfoto")
	public Arquivo getFoto() {
		return foto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem", insertable=false, updatable=false)
	public Vpatrimonioitem getVpatrimonioitem() {
		return vpatrimonioitem;
	}
	
	@DisplayName("Data de registro de perda")
	public Date getDtregistroperda() {
		return dtregistroperda;
	}
	
	public void setDtregistroperda(Date dtregistroperda) {
		this.dtregistroperda = dtregistroperda;
	}
	
	public void setVpatrimonioitem(Vpatrimonioitem vpatrimonioitem) {
		this.vpatrimonioitem = vpatrimonioitem;
	}
	
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setCdpatrimonioitem(Integer cdpatrimonioitem) {
		this.cdpatrimonioitem = cdpatrimonioitem;
	}

	public void setBempatrimonio(Bempatrimonio bempatrimonio) {
		this.bempatrimonio = bempatrimonio;
	}

	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"plaqueta","bempatrimonio"})
	public String getDescricao() {	
		return (getBempatrimonio() != null ? getBempatrimonio().getNome() : "") + (getPlaqueta() != null ? "(" + getPlaqueta() + ")" : "");
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@OneToMany(mappedBy="patrimonioitem")
	public List<Movpatrimonio> getListaMovpatrimonio() {
		return listaMovpatrimonio;
	}
	
	public void setListaMovpatrimonio(List<Movpatrimonio> listaMovpatrimonio) {
		this.listaMovpatrimonio = listaMovpatrimonio;
	}
	
	@DisplayName("Data aquisi��o")
	public Date getDtaquisicao() {
		return dtaquisicao;
	}
	
	public void setDtaquisicao(Date dtaquisicao) {
		this.dtaquisicao = dtaquisicao;
	}
	
	@OneToMany(mappedBy="patrimonioitem")
	public List<Patrimonioitemhistorico> getListaHistorico() {
		return listaHistorico;
	}
	
	public void setListaHistorico(List<Patrimonioitemhistorico> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}
	
	@Transient
	public Boolean getNaoexisteplaqueta() {
		return naoexisteplaqueta;
	}
	
	public void setNaoexisteplaqueta(Boolean naoexisteplaqueta) {
		this.naoexisteplaqueta = naoexisteplaqueta;
	}
	
	@Transient
	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Transient
	public Boolean getFromVeiculo() {
		return fromVeiculo;
	}

	public void setFromVeiculo(Boolean fromVeiculo) {
		this.fromVeiculo = fromVeiculo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Contrato de Fornecimento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentocontrato")
	public Fornecimentocontrato getFornecimentocontrato() {
		return fornecimentocontrato;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		this.fornecimentocontrato = fornecimentocontrato;
	}

	@Transient
	public String getEmpresafornecedor() {
		String nome = "";
		if(this.getEmpresa() != null && this.getEmpresa().getRazaosocialOuNome() != null){
			nome = this.getEmpresa().getRazaosocialOuNome();
		}
		if(this.getFornecimentocontrato() != null && this.getFornecimentocontrato().getFornecedor() != null && 
				this.getFornecimentocontrato().getFornecedor().getNome() != null){
			if(!"".equals(nome))
				nome += " / ";
			nome +=  this.getFornecimentocontrato().getFornecedor().getNome();
		}
		return nome;
	}
	@Transient
	public String getAquisicaolocacao() {
		String nome = "";
		if(this.getFornecimentocontrato() != null && this.getFornecimentocontrato() != null){
			if(this.getFornecimentocontrato().getDtinicio() != null){
				nome = new SimpleDateFormat("dd/MM/yyyy").format(this.getFornecimentocontrato().getDtinicio());
			}
			
			if(this.bempatrimonio != null && this.getFornecimentocontrato().getListaFornecimentocontratoitem() != null && 
					!this.getFornecimentocontrato().getListaFornecimentocontratoitem().isEmpty()){
				for(Fornecimentocontratoitem item : this.getFornecimentocontrato().getListaFornecimentocontratoitem()){
					if(item.getMaterial() != null && item.getMaterial().getCdmaterial() != null && 
							item.getMaterial().getCdmaterial().equals(this.bempatrimonio.getCdmaterial())){
						if(item.getValor() != null){
							if(!"".equals(nome)) nome += " / ";
							nome +=  "R$ " + item.getValor();
						}
						break;
					}
				}
			}
		}
		return nome;
	}
	@Transient
	public String getLocalizacao() {
		String nome = "";
		if(this.getFornecimentocontrato() != null && this.getFornecimentocontrato() != null){
			if(this.localnome != null){
				nome = this.localnome;
			}
			
			if(this.getFornecimentocontrato().getColaborador() != null && this.getFornecimentocontrato().getColaborador().getNome() != null){
				if(!"".equals(nome)) nome += " / ";
				nome += this.getFornecimentocontrato().getColaborador().getNome();
			}
		}
		return nome;
	}

	@Transient
	public String getLocalnome() {
		return localnome;
	}
	public void setLocalnome(String localnome) {
		this.localnome = localnome;
	}
	
	@Transient
	public Money getValorPatrimonioitemReport(){
		Money valor = new Money();
		
		if( this.getBempatrimonio() != null){
			if(this.getBempatrimonio().getValor() != null){
				valor = this.getBempatrimonio().getValor();
			}
		
			if(this.getFornecimentocontrato() != null && this.getFornecimentocontrato().getListaFornecimentocontratoitem() != null && 
					!this.getFornecimentocontrato().getListaFornecimentocontratoitem().isEmpty()){
				for(Fornecimentocontratoitem item : this.getFornecimentocontrato().getListaFornecimentocontratoitem()){
					if(item.getMaterial() != null && item.getMaterial().equals(this.getBempatrimonio())){
						if(item.getValorequipamento() != null){
							valor = new Money(item.getValorequipamento());
						}
						break;
					}
				}
			}
		}
		
		return valor;
	}
	
	@Transient
	public Money getValoratualPatrimonioitemReport(){
		Money valor = new Money();
		
		if( this.getBempatrimonio() != null){
			if(this.getDtaquisicao() == null || this.getBempatrimonio().getDepreciacao() == null || 
					this.getBempatrimonio().getDepreciacao().getValue().doubleValue() == 0){
//				valor = new Money(this.getBempatrimonio().getDepreciacao());
				valor = new Money(0);
			}else {
				valor = new Money(
						this.getBempatrimonio().getDepreciacao().getValue().doubleValue() -
						(this.getValorPatrimonioitemReport().getValue().doubleValue()*
								(((((System.currentTimeMillis() - this.getDtaquisicao().getTime()) / 1000 / 60 / 60) / 24 / 30)*
										this.getBempatrimonio().getDepreciacao().getValue().doubleValue())/100)
						));
			}
			
		
			if(this.getFornecimentocontrato() != null && this.getFornecimentocontrato().getListaFornecimentocontratoitem() != null && 
					!this.getFornecimentocontrato().getListaFornecimentocontratoitem().isEmpty()){
				for(Fornecimentocontratoitem item : this.getFornecimentocontrato().getListaFornecimentocontratoitem()){
					if(item.getMaterial() != null && item.getMaterial().equals(this.getBempatrimonio())){
						if(item.getValorequipamento() != null){
							valor = new Money(item.getValorequipamento());
						}
						break;
					}
				}
			}
		}
		
		return valor;
	}
	
	@DisplayName("Hor�metro")
	@Transient
	public Double getHorimetro() {
		return horimetro;
	}
	
	public void setHorimetro(Double horimetro) {
		this.horimetro = horimetro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((getCdpatrimonioitem() == null) ? 0 : getCdpatrimonioitem().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patrimonioitem other = (Patrimonioitem) obj;
		if (getCdpatrimonioitem() == null) {
			if (other.getCdpatrimonioitem() != null)
				return false;
		} else if (!getCdpatrimonioitem().equals(other.getCdpatrimonioitem()))
			return false;
		return true;
	}
	
	@Transient
	public String getPlaquetaSerie() {
		String retorno = "";

		if (plaqueta!=null && !plaqueta.trim().equals("")){
			retorno = plaqueta;
		}
		
		if (materialnumeroserie!=null && materialnumeroserie.getNumero()!=null){
			retorno += !retorno.equals("") ? "/" : "";
			retorno += materialnumeroserie.getNumero();			
		}
		
		if (retorno.trim().equals("-/-")){
			return null;
		}else {
			return retorno;
		}
	}
	
	@Transient
	public String getBempatrimonioPlaquetaSerie() {	
		String plaquetaSerie = getPlaquetaSerie();
		return (getBempatrimonio()!=null ? getBempatrimonio().getNome() : "") + (plaquetaSerie!=null ? " (" + plaquetaSerie + ")" : "");
	}
	
	@DisplayName("S�rie")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialnumeroserie")
	public Materialnumeroserie getMaterialnumeroserie() {
		return materialnumeroserie;
	}
	
	public void setMaterialnumeroserie(Materialnumeroserie materialnumeroserie) {
		this.materialnumeroserie = materialnumeroserie;
	}
	
	@Transient
	@DisplayName("Cliente")
	public String getClientenomelistagem() {
		if(listaMovpatrimonio != null && listaMovpatrimonio.size() == 1){
			if(listaMovpatrimonio.get(0).getCliente() != null){
				clientenomelistagem = listaMovpatrimonio.get(0).getCliente().getNome();
			}
		}
		return clientenomelistagem;
	}
	
	public void setClientenomelistagem(String clientenomelistagem) {
		this.clientenomelistagem = clientenomelistagem;
	}
	
	@Transient
	@DisplayName("Local")
	public String getLocalnomelistagem() {
		if(listaMovpatrimonio != null && listaMovpatrimonio.size() == 1){
			if(listaMovpatrimonio.get(0).getLocalarmazenagem() != null){
				localnomelistagem = listaMovpatrimonio.get(0).getLocalarmazenagem().getNome();
			}
		}
		return localnomelistagem;
	}
	
	public void setLocalnomelistagem(String localnomelistagem) {
		this.localnomelistagem = localnomelistagem;
	}
}