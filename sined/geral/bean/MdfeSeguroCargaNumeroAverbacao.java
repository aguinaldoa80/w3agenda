package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfesegurocarganumeroaverbacao", sequenceName = "sq_mdfesegurocarganumeroaverbacao")
public class MdfeSeguroCargaNumeroAverbacao {

	protected Integer cdMdfeSeguroCargaNumeroAverbacao;
	protected Mdfe mdfe;
	protected String idSeguroCarga;
	protected String idSeguroCargaNumeroAverbacao;
	protected String numeroAverbacao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfesegurocarganumeroaverbacao")
	public Integer getCdMdfeSeguroCargaNumeroAverbacao() {
		return cdMdfeSeguroCargaNumeroAverbacao;
	}
	public void setCdMdfeSeguroCargaNumeroAverbacao(
			Integer cdMdfeSeguroCargaNumeroAverbacao) {
		this.cdMdfeSeguroCargaNumeroAverbacao = cdMdfeSeguroCargaNumeroAverbacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID do seguro de carga")
	public String getIdSeguroCarga() {
		return idSeguroCarga;
	}
	public void setIdSeguroCarga(String idSeguroCarga) {
		this.idSeguroCarga = idSeguroCarga;
	}
	
	@DisplayName("ID do n�mero de averba��o")
	public String getIdSeguroCargaNumeroAverbacao() {
		return idSeguroCargaNumeroAverbacao;
	}
	public void setIdSeguroCargaNumeroAverbacao(String idSeguroCargaNumeroAverbacao) {
		this.idSeguroCargaNumeroAverbacao = idSeguroCargaNumeroAverbacao;
	}
	
	@Required
	@DisplayName("N�mero da averba��o")
	@MaxLength(value=40)
	public String getNumeroAverbacao() {
		return numeroAverbacao;
	}
	public void setNumeroAverbacao(String numeroAverbacao) {
		this.numeroAverbacao = numeroAverbacao;
	}
}
