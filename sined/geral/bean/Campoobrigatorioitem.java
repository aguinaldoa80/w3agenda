package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.ValidacaoCampoObrigatorio;

@Entity
@SequenceGenerator(name = "sq_campoobrigatorioitem", sequenceName = "sq_campoobrigatorioitem")
public class Campoobrigatorioitem implements ValidacaoCampoObrigatorio {
	
	protected Integer cdcampoobrigatorioitem;
	protected Campoobrigatorio campoobrigatorio;
	protected String prefixo;
	protected String campo;
	protected String condicao;
	protected Boolean pelomenosumregistro;
	
	//TRANSIENT
	protected String displayNameCampo;
	protected String displayNameLista;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campoobrigatorioitem")	
	public Integer getCdcampoobrigatorioitem() {
		return cdcampoobrigatorioitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampoobrigatorio")
	public Campoobrigatorio getCampoobrigatorio() {
		return campoobrigatorio;
	}

	@MaxLength(100)
	public String getPrefixo() {
		return prefixo;
	}

	@Required
	@MaxLength(100)
	public String getCampo() {
		return campo;
	}

	@DisplayName("Condi��o")
	public String getCondicao() {
		return condicao;
	}

	@DisplayName("Pelo menos um registro")
	public Boolean getPelomenosumregistro() {
		return pelomenosumregistro;
	}

	public void setCdcampoobrigatorioitem(Integer cdcampoobrigatorioitem) {
		this.cdcampoobrigatorioitem = cdcampoobrigatorioitem;
	}

	public void setCampoobrigatorio(Campoobrigatorio campoobrigatorio) {
		this.campoobrigatorio = campoobrigatorio;
	}

	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setCondicao(String condicao) {
		this.condicao = condicao;
	}

	public void setPelomenosumregistro(Boolean pelomenosumregistro) {
		this.pelomenosumregistro = pelomenosumregistro;
	}

	@Transient
	public String getDisplayNameCampo() {
		return displayNameCampo;
	}

	@Transient
	public String getDisplayNameLista() {
		return displayNameLista;
	}

	public void setDisplayNameCampo(String displayNameCampo) {
		this.displayNameCampo = displayNameCampo;
	}

	public void setDisplayNameLista(String displayNameLista) {
		this.displayNameLista = displayNameLista;
	}
	
	
	
}
