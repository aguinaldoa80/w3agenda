package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name = "sq_faturamentohistoricomaterial", sequenceName = "sq_faturamentohistoricomaterial")
public class Faturamentohistoricomaterial {

	protected Integer cdfaturamentohistoricomaterial;
	protected Faturamentohistorico faturamentohistorico;
	protected Material material;
	protected Double qtde;
	protected Money valorunitario;
	protected Money valordesconto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_faturamentohistoricomaterial")
	public Integer getCdfaturamentohistoricomaterial() {
		return cdfaturamentohistoricomaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaturamentohistorico")
	public Faturamentohistorico getFaturamentohistorico() {
		return faturamentohistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public Double getQtde() {
		return qtde;
	}

	public Money getValorunitario() {
		return valorunitario;
	}

	public Money getValordesconto() {
		return valordesconto;
	}

	public void setCdfaturamentohistoricomaterial(
			Integer cdfaturamentohistoricomaterial) {
		this.cdfaturamentohistoricomaterial = cdfaturamentohistoricomaterial;
	}

	public void setFaturamentohistorico(Faturamentohistorico faturamentohistorico) {
		this.faturamentohistorico = faturamentohistorico;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setValorunitario(Money valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	
}
