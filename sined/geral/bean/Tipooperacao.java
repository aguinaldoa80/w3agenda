package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name="sq_tipooperacao", sequenceName="sq_tipooperacao")
public class Tipooperacao {

	public static final Tipooperacao TIPO_DEBITO = new Tipooperacao(1, "D�bito");
	public static final Tipooperacao TIPO_CREDITO = new Tipooperacao(2, "Cr�dito");
	public static final Tipooperacao TIPO_CONTABIL = new Tipooperacao(3, "Cont�bil");
	
	protected Integer cdtipooperacao;
	protected String nome;

	public Tipooperacao(){}
	
	public Tipooperacao(Integer cdtipooperacao) {
		this.cdtipooperacao = cdtipooperacao;
	}

	public Tipooperacao(Integer cdtipooperacao, String nome) {
		this.cdtipooperacao = cdtipooperacao;
		this.nome = nome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipooperacao")
	public Integer getCdtipooperacao() {
		return cdtipooperacao;
	}
	
	@DescriptionProperty
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("O")
	@Transient
	public String getSigla(){
		return this.nome.substring(0,1).toUpperCase();
	}
	
	public void setCdtipooperacao(Integer cdtipooperacao) {
		this.cdtipooperacao = cdtipooperacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdtipooperacao == null) ? 0 : cdtipooperacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Tipooperacao other = (Tipooperacao) obj;
		if (cdtipooperacao == null) {
			if (other.cdtipooperacao != null)
				return false;
		} else if (!cdtipooperacao.equals(other.cdtipooperacao))
			return false;
		return true;
	}
	
	public static List<Tipooperacao> getListaTipooperacao(boolean contabil){
		List<Tipooperacao> lista = new ArrayList<Tipooperacao>();
		if(contabil){
			lista.add(Tipooperacao.TIPO_CONTABIL);
		}else {
			lista.add(Tipooperacao.TIPO_CREDITO);
			lista.add(Tipooperacao.TIPO_DEBITO);
		}
		return lista;
	}
	
}
