package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@JoinEmpresa("cliente.listaClienteEmpresa.empresa")
public class Cliente extends Pessoa implements Log{

	protected Arquivo logocliente;
	protected String razaosocial;
	protected Uf ufinscricaoestadual;
	protected String inscricaoestadual;
	protected String inscricaomunicipal;
	protected String inscricaosuframa;
	
	protected String fax;
	protected String site;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Tipoidentidade tipoidentidade;
	
	protected Set<Restricao> listaRestricao;
	protected Set<Proposta> listaProposta;
	protected List<Projeto> listaProjeto;
	protected List<Orcamento> listaOrcamento;
	protected List<Clienteprocesso> listaClienteprocesso;
	protected List<Contrato> listaContrato;
	protected Set<Clientearquivo> listaClientearquivo;
	protected Set<Lead> listaLead;
	
	protected String mae; 
	protected String pai; 
	protected Municipio municipionaturalidade;
	protected Sexo sexo;
	protected String rg;
	protected Timestamp dtemissaorg;
	protected String orgaoemissorrg;
	protected Estadocivil estadocivil;	
	protected Clienteprofissao clienteprofissao;
	protected Clienteprofissao profissaomae;
	protected Clienteprofissao profissaopai;
	protected String conjuge;
	protected Clienteprofissao profissaoconjuge;
	protected Grauinstrucao grauinstrucao;
	protected String passaporte;
	protected Date validadepassaporte;
	protected Boolean naoconsiderartaxaboleto;
	protected Boolean discriminartaxaboleto;
	protected Boolean discriminardescontonota;
	protected Codigoregimetributario crt;	
	protected Money taxapedidovenda;
	protected Double creditolimitecompra;
	protected Date dtvalidadelimitecompra;
	protected Contribuinteicmstipo contribuinteicmstipo;
	protected ReportTemplateBean layoutdiscriminacaoservico;
	protected Money valorMinimoCompra;
	
	protected Cliente responsavelFinanceiro; //cdclientejudiciario
	protected List<Clientehistorico> listClientehistorico = new ListSet<Clientehistorico>(Clientehistorico.class);
	protected List<Requisicao> listaClienterequisicao = new ListSet<Requisicao>(Requisicao.class);
	protected Clienteindicacao clienteindicacao;
	
	protected String login;
	protected String senha;
	
	protected String identificador;
	protected Integer identificadorordenacao;
	protected Timestamp dtinsercao;
	protected Set<Clienterelacao> listaClienterelacao;
	protected Set<Clientevendedor> listaClientevendedor;
	protected List<Valecompra> listaValecompra;
	protected Boolean isClienteVendaEdicao;
	protected Boolean contabilidadecentralizada;
	protected Boolean incidiriss;
	protected ContaContabil contaContabil;
	protected Codigotributacao codigotributacao;
	protected String ramoatividade;
	protected String objetosocial;
	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	protected String observacaoVenda;
	protected String informacaovenda;
	protected String informacaoOrdemServico;
	protected Grupotributacao grupotributacao;
	protected Boolean contribuinteICMS;
	protected Operacaonfe operacaonfe;
	protected Fornecedor associacao;
	
	protected List<ClienteDocumentoTipo> listDocumentoTipo;
	protected List<ClientePrazoPagamento> listPrazoPagamento;
	protected Set<ClienteSegmento> listaClienteSegmento; 
	protected List<ClienteEmpresa> listaClienteEmpresa;
	protected List<ClienteEmpresa> listaClienteEmpresaNaoAssociada;
	protected List<Venda> listaVenda;
	protected List<Clienteespecialidade> listaClienteespecialidade;
	protected Set<ClienteLicencaSipeagro> listaClienteLicencaSipeagro;
	
	//Transient
	protected String listaTelefones;
	protected String cpfcnpj;
	protected String ativosrelatorio;
	protected String telefones;
	protected String tipopessoarel;
	protected String contatos;
	protected String historico;
	protected Boolean possuiContasAtrasadas;
	protected String fullNome;
	protected String nomeIdAutocomplete;
	protected Oportunidade oportunidade;
	protected List<Documentocomissao> listaDocumentocomissao = new ListSet<Documentocomissao>(Documentocomissao.class);
	protected Boolean isIdentificadorGerado;
	protected Telefone telefonePrincipal;
	protected Telefone celular; 
	protected List<Vendahistorico> listaVendahistorico;
	protected String historicoLead;
	protected List<Contato> emailsSelected;
	protected Boolean emailPrincipal;
	protected String telefonesTransientForAutocomplete;
	protected Money saldoValecompra = new Money();
	protected Money saldoAntecipacao = new Money();
	protected String contatoAndTelefoneResponsavel;
	protected Set<Pessoaquestionario> listaQuestionarioWithAtividadetipo;
	private Pedidovenda pedidovenda;
	
	public Cliente() {}
	
	public Cliente(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	public Cliente(Integer cdpessoa, Integer cdendereco) {
		this.cdpessoa = cdpessoa;
		if(cdendereco != null){ 
			this.enderecoAux = new Endereco(cdendereco);
		}
	}
	
	public Cliente(String nome) {
		this.nome = nome;
	}
	
	public Cliente(String nome, Tipopessoa tipopessoa) {
		this.nome = nome;
		this.tipopessoa = tipopessoa;
	}

	public Cliente(Integer cdpessoa, String nome) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;

	}
	
	public Cliente(Integer cdpessoa, String nome, String identificador) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.identificador = identificador;
	}
	
	public Cliente(Integer cdpessoa, String nome, Tipopessoa tipopessoa, Boolean ativo, String email, String identificador, Integer coalesce){
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.identificador = identificador;
		this.tipopessoa = tipopessoa;
		this.ativo = ativo;
		this.email = email;
	}
	
	@Transient
	public List<Contato> getEmailsSelected() {
		return emailsSelected;
	}

	@Transient
	public Boolean getEmailPrincipal() {
		return emailPrincipal;
	}

	public void setEmailPrincipal(Boolean emailPrincipal) {
		this.emailPrincipal = emailPrincipal;
	}

	public void setEmailsSelected(List<Contato> emailsSelected) {
		this.emailsSelected = emailsSelected;
	}

	/**
	 * Sobrescrito para retirar a obrigatoriedade em Cliente
	 * @author Fl�vio Tavares
	 */
	@Override
	@Transient
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return super.getCnpj();
	}
	/**
	 * Sobrescrito para retirar a obrigatoriedade em Cliente
	 * @author Fl�vio Tavares
	 */
	@Override
	@Transient
	@DisplayName("CPF")
	public Cpf getCpf() {
		return super.getCpf();
	}
	
	@DisplayName("Raz�o social")
	@MaxLength(200)
	public String getRazaosocial() {
		return razaosocial;
	}

	@DisplayName("Inscri��o estadual")
	@MaxLength(20)
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	
	@DisplayName("Inscri��o SUFRAMA")
	public String getInscricaosuframa() {
		return inscricaosuframa;
	}

	@DisplayName("Inscri��o municipal")
	@MaxLength(20)
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	@DisplayName("Fax")
	@MaxLength(20)
	public String getFax(){
		return fax;
	}

	@DisplayName("Site")
	@MaxLength(50)
	public String getSite() {
		return site;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("M�e")
	@MaxLength(50)
	public String getMae() {
		return mae;
	}
	
	@DisplayName("C�njuge")
	@MaxLength(50)
	public String getConjuge() {
		return conjuge;
	}
	
	@DisplayName("Pai")
	@MaxLength(50)
	public String getPai() {
		return pai;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdmunicipionaturalidade")
    @DisplayName("Naturalidade")
    @ValidationOverride(field="municipionaturalidade.uf")
	public Municipio getMunicipionaturalidade() {
		return municipionaturalidade;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")	
	public Sexo getSexo() {
		return sexo;
	}
	
	@DisplayName("Identidade")
	@MaxLength(20)
	public String getRg() {
		return rg;
	}
	
	@DisplayName("Data expedi��o")	
	public Timestamp getDtemissaorg() {
		return dtemissaorg;
	}
	
	@DisplayName("�rg�o expedidor")
	@MaxLength(10)
	public String getOrgaoemissorrg() {
		return orgaoemissorrg;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdestadocivil")
	@DisplayName("Estado civil")
	public Estadocivil getEstadocivil() {
		return estadocivil;
	}
	
	@OneToMany(mappedBy="pessoa")
	@DisplayName("Restri��es")
	public Set<Restricao> getListaRestricao() {
		return listaRestricao;
	}
	@OneToMany(mappedBy="cliente")
	public Set<Proposta> getListaProposta() {
		return listaProposta;
	}
	@OneToMany(mappedBy="cliente")
	@DisplayName("Projetos")
	public List<Projeto> getListaProjeto() {
		return listaProjeto;
	}
	@OneToMany(mappedBy="cliente")
	@DisplayName("Or�amentos")	
	public List<Orcamento> getListaOrcamento() {
		return listaOrcamento;
	}
	@OneToMany(mappedBy="cliente")
	@DisplayName("Processos")
	public List<Clienteprocesso> getListaClienteprocesso() {
		return listaClienteprocesso;
	}
	@OneToMany(mappedBy="cliente")
	@DisplayName("Arquivos")
	public Set<Clientearquivo> getListaClientearquivo() {
		return listaClientearquivo;
	}
	@OneToMany(mappedBy="cliente")
	public Set<Lead> getListaLead() {
		return listaLead;
	}
	@OneToMany(mappedBy="cliente")
	@DisplayName("Contrato")
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlogocliente")
	@DisplayName("Logo")
	public Arquivo getLogocliente() {
		return logocliente;
	}
	
	@DisplayName("Contribuinte ICMS")
	public Contribuinteicmstipo getContribuinteicmstipo() {
		return contribuinteicmstipo;
	}
	
	@DisplayName("Consumidor final?")
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
	
	@MaxLength(11)
	public String getIdentificador() {
		return identificador;
	}
	
	@MaxLength(20)
	public Money getValorMinimoCompra() {
		return valorMinimoCompra;
	}
	
	public void setValorMinimoCompra(Money valorMinimoCompra) {
		this.valorMinimoCompra = valorMinimoCompra;
	}
	
	@JoinColumn(name="cdreporttemplate")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Layout Discrimina��o dos servi�os")
	public ReportTemplateBean getLayoutdiscriminacaoservico() {
		return layoutdiscriminacaoservico;
	}
	
	@Transient
	public String getNomeIdAutocomplete() {
		return this.nome + ( this.identificador != null ? " - " + this.identificador : "");
	}
	@Transient
	public String getAutocomplete() {
		String nome = this.nome != null ? this.nome.toString() : "";
		return nome;
	}
	
	@Transient
	public String getNomeIdRazaosocialCpfcnpjAutocomplete(){
		StringBuilder s = new StringBuilder();
		
		s.append(nome)
		 .append("<span><BR>")
		 .append("Identificador: " + (identificador != null ? identificador : "") + "<BR>")
		 .append("Razao Social: " + (razaosocial != null ? razaosocial : "") + "<BR>");
		 
		if(cpf != null)
			s.append("CPF: " + cpf + "<BR>");
		else if(cnpj != null)
			s.append("CNPJ: " + cnpj + "<BR>");
		
		s.append("</span>");
		
		return s.toString();
	}
	
	public void setNomeIdAutocomplete(String nomeIdAutocomplete) {
		this.nomeIdAutocomplete = nomeIdAutocomplete;
	}
	
	@DisplayName("Telefones")
	@Transient
	public String getListaTelefones() {
		return listaTelefones;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Transient
	public String getTelefones() {
		Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone = getListaTelefone();
		StringBuilder telefones = new StringBuilder("");
		if (listaTelefone != null && !listaTelefone.isEmpty()) {
			for (br.com.linkcom.sined.geral.bean.Telefone telefone : listaTelefone) {
				if(telefone.getTelefonetipo() != null)
					telefones.append(telefone.getTelefone().toString() + "(" + telefone.getTelefonetipo().getNome()).append(")\n");
			}
		}
		return telefones.toString();
	}
	
	@Transient
	public String getTelefonesHtml() {
		Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone = getListaTelefone();
		StringBuilder telefones = new StringBuilder("");
		if (listaTelefone != null && !listaTelefone.isEmpty()) {
			for (br.com.linkcom.sined.geral.bean.Telefone telefone : listaTelefone) {
				if(telefone.getTelefonetipo() != null)
					telefones.append(telefone.getTelefone().toString() + "(" + telefone.getTelefonetipo().getNome()).append(")<BR>");
			}
		}
		return telefones.toString();
	}
	
	@Transient 
	public String getTelefonesSemQuebraLinha(){
		if(this.telefones != null && !this.telefones.equals("")){
			return this.telefones;
		}else{
			StringBuilder stringBuilder = new StringBuilder("");
			if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
				for (Telefone telefone : this.listaTelefone) {
					stringBuilder.append(telefone.getTelefone()).append(" / ");
				}
			}
			if(!stringBuilder.toString().equals(""))
				this.telefones = stringBuilder.substring(0, stringBuilder.length()-2).toString();
			return telefones;
		}
	}
	
	@Transient
	public String getTelefonesSemQuebraLinhaPipe() {
		if(this.telefones != null && !this.telefones.equals("")){
			return this.telefones;
		}else{
			telefones = "";
			StringBuilder stringBuilder = new StringBuilder("");
			if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
				for (Telefone telefone : this.listaTelefone) {
					stringBuilder.append(telefone.toString()).append(" | ");
				}
			}
			if(!stringBuilder.toString().equals(""))
				this.telefones = stringBuilder.substring(0, stringBuilder.length()-2).toString();
			return telefones;
		}
	}
	
	@Transient
	public String getTelefonesTransientForAutocomplete(){
		return telefonesTransientForAutocomplete;
	}
	
	@DisplayName("Observa��o de venda")
	public String getObservacaoVenda() {
		return observacaoVenda;
	}

	public void setObservacaoVenda(String observacaoVenda) {
		this.observacaoVenda = observacaoVenda;
	}

	@DisplayName("Informa��es do cliente (Alertar na venda)")
	@MaxLength(200)
	public String getInformacaovenda() {
		return informacaovenda;
	}
	
	public void setInformacaovenda(String informacaovenda) {
		this.informacaovenda = informacaovenda;
	}
	
	@DisplayName("Informa��es do cliente (Alertar na OS)")
	@MaxLength(200)
	public String getInformacaoOrdemServico() {
		return informacaoOrdemServico;
	}
	
	public void setInformacaoOrdemServico(String informacaoOrdemServico) {
		this.informacaoOrdemServico = informacaoOrdemServico;
	}	

	public void setTelefonesTransientForAutocomplete(String telefonesTransientForAutocomplete){
		
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	
	@Transient
	public String getAtivosrelatorio() {
		return ativosrelatorio;
	}
	
	public void setAtivosrelatorio(String ativosrelatorio) {
		this.ativosrelatorio = ativosrelatorio;
	}
	
	@Transient
	public String getTipopessoarel() {
		return tipopessoarel;
	}
	
	public void setTipopessoarel(String tipopessoarel) {
		this.tipopessoarel = tipopessoarel;
	}
	
	@Transient
	@DisplayName("CNPJ/CPF")
	public String getCpfcnpj() {
		if(this.cpf != null){
			return this.cpf.toString();
		} else if(this.cnpj != null){
			return this.cnpj.toString();
		}
		return cpfcnpj;
	}

	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	
	@DisplayName("Respons�vel Financeiro")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclientejuridico")
	public Cliente getResponsavelFinanceiro() {
		return responsavelFinanceiro;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="cliente")
	public List<Clientehistorico> getListClientehistorico() {
		return listClientehistorico;
	}
	
	@OneToMany(mappedBy="cliente")
	public List<Requisicao> getListaClienterequisicao() {
		return listaClienterequisicao;
	}
	
	@DisplayName("Cliente Indica��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteindicacao")
	public Clienteindicacao getClienteindicacao() {
		return clienteindicacao;
	}
	
	@Transient
	@MaxLength(1024)
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}
	
	@DisplayName("Profiss�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteprofissao")
	public Clienteprofissao getClienteprofissao() {
		return clienteprofissao;
	}
	
	@DisplayName("Profiss�o da m�e")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprofissaomae")
	public Clienteprofissao getProfissaomae() {
		return profissaomae;
	}
	
	@DisplayName("Profiss�o do pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprofissaopai")
	public Clienteprofissao getProfissaopai() {
		return profissaopai;
	}
	
	@DisplayName("Profiss�o do c�njuge")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprofissaoconjuge")
	public Clienteprofissao getProfissaoconjuge() {
		return profissaoconjuge;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrauinstrucao")
	@DisplayName("Escolaridade")
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	
	public Timestamp getDtinsercao() {
		return dtinsercao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufinscricaoestadual")
	@DisplayName("UF da Inscri��o Estadual")
	public Uf getUfinscricaoestadual() {
		return ufinscricaoestadual;
	}
	@DisplayName("Incidir ISS")
	public Boolean getIncidiriss() {
		return incidiriss;
	}
	public void setIncidiriss(Boolean incidiriss) {
		this.incidiriss = incidiriss;
	}
	public void setUfinscricaoestadual(Uf ufinscricaoestadual) {
		this.ufinscricaoestadual = ufinscricaoestadual;
	}
	
	public void setClienteprofissao(Clienteprofissao clienteprofissao) {
		this.clienteprofissao = clienteprofissao;
	}
	public void setProfissaomae(Clienteprofissao profissaomae) {
		this.profissaomae = profissaomae;
	}
	public void setProfissaopai(Clienteprofissao profissaopai) {
		this.profissaopai = profissaopai;
	}
	public void setProfissaoconjuge(Clienteprofissao profissaoconjuge) {
		this.profissaoconjuge = profissaoconjuge;
	}
	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	public void setResponsavelFinanceiro(Cliente responsavelFinanceiro) {
		this.responsavelFinanceiro = responsavelFinanceiro;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setInscricaosuframa(String inscricaosuframa) {
		this.inscricaosuframa = inscricaosuframa;
	}
	public void setListClientehistorico(List<Clientehistorico> listClientehistorico) {
		this.listClientehistorico = listClientehistorico;
	}
	public void setListaClienterequisicao(List<Requisicao> listaClienterequisicao) {
		this.listaClienterequisicao = listaClienterequisicao;
	}
	public void setClienteindicacao(Clienteindicacao clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public void setListaClientearquivo(Set<Clientearquivo> listaClientearquivo) {
		this.listaClientearquivo = listaClientearquivo;
	}
	public void setListaLead(Set<Lead> listaLead) {
		this.listaLead = listaLead;
	}

	@MaxLength(50)
	public String getLogin() {
		return login;
	}
	
	@MaxLength(15)
	public String getSenha() {
		return senha;
	}
	
	@OneToMany(mappedBy="cliente")
	@DisplayName("Segmentos")
	public Set<ClienteSegmento> getListaClienteSegmento() {
		return listaClienteSegmento;
	}
	
	public void setListaClienteSegmento(Set<ClienteSegmento> listaClienteSegmento) {
		this.listaClienteSegmento = listaClienteSegmento;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void setRazaosocial(String razaosocial) {
		this.razaosocial = StringUtils.trimToNull(razaosocial);
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
	public void setMae(String mae) {
		this.mae = mae;
	}
	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}
	public void setPai(String pai) {
		this.pai = pai;
	}	
	public void setMunicipionaturalidade(Municipio municipionaturalidade) {
		this.municipionaturalidade = municipionaturalidade;
	}	
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}	
	public void setContribuinteicmstipo(
			Contribuinteicmstipo contribuinteicmstipo) {
		this.contribuinteicmstipo = contribuinteicmstipo;
	}
	public void setOperacaonfe(Operacaonfe operacaoNfe) {
		this.operacaonfe = operacaoNfe;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}	
	public void setDtemissaorg(Timestamp dtemissaorg) {
		this.dtemissaorg = dtemissaorg;
	}	
	public void setOrgaoemissorrg(String orgaoemissorrg) {
		this.orgaoemissorrg = orgaoemissorrg;
	}	
	public void setEstadocivil(Estadocivil estadocivil) {
		this.estadocivil = estadocivil;
	}	
	public void setListaTelefones(String listaTelefones) {
		this.listaTelefones = listaTelefones;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaRestricao(Set<Restricao> listaRestricao) {
		this.listaRestricao = listaRestricao;
	}
	public void setLogocliente(Arquivo logocliente) {
		this.logocliente = logocliente;
	}
	public void setListaProposta(Set<Proposta> listaProposta) {
		this.listaProposta = listaProposta;
	}
	public void setListaProjeto(List<Projeto> listaProjeto) {
		this.listaProjeto = listaProjeto;
	}
	public void setListaOrcamento(List<Orcamento> listaOrcamento) {
		this.listaOrcamento = listaOrcamento;
	}
	public void setListaClienteprocesso(List<Clienteprocesso> listaClienteprocesso) {
		this.listaClienteprocesso = listaClienteprocesso;
	}
	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	public void setIdentificador(String identificador) {
		this.identificador = StringUtils.trimToNull(identificador);
	}	
	public void setDtinsercao(Timestamp dtinsercao) {
		this.dtinsercao = dtinsercao;
	}
	
	@Transient
	public String getContatos() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			StringBuilder sb = new StringBuilder("");
			for (PessoaContato pessoaContato : this.listaContato) 
				if (pessoaContato.getContato() != null) {
					sb.append(pessoaContato.getContato().getNome()).append(", ");
				}
			return sb.substring(0, sb.length()-2).toString();
		}
		return contatos;
	}
	
	public void setContatos(String contatos) {
		this.contatos = contatos;
	}
	
	@Transient
	public String getContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getNome() != null){
					return pessoaContato.getPessoa().getNome();
				}
			}
		}
		return "";
	}
	
	@Transient
	public String getEmailContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getEmail() != null){
					return pessoaContato.getPessoa().getEmail();
				}
			}
		}
		return "";
	}
	
	@Transient
	public String getTelefoneContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getTelefones() != null){
					return pessoaContato.getPessoa().getTelefones();
				}
			}
		}
		return "";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cliente) {
			Cliente cliente = (Cliente) obj;
			return this.getCdpessoa().equals(cliente.getCdpessoa());
		}else{
			return false;	
		}
	}
	
	@Transient
	public String getFullNome() {
		return (this.pessoatratamento != null && this.pessoatratamento.getCdpessoatratamento() != null ? this.pessoatratamento.getNome() : "") + this.nome;
	}
	
	public void setFullNome(String fullNome) {
		this.fullNome = fullNome;
	}
	
	@Override
	public int hashCode() {
		return cdpessoa.hashCode();
	}
	
	@Transient
	public Boolean getPossuiContasAtrasadas() {
		return possuiContasAtrasadas;
	}
	
	public void setPossuiContasAtrasadas(Boolean possuiContasAtrasadas) {
		this.possuiContasAtrasadas = possuiContasAtrasadas;
	}
	
	@Transient
	public String getIdentificadorNome(){
		return (this.identificador != null ? this.identificador+" - " : "")+this.nome;
	}
	
	@Transient
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	
	@Transient
	public List<Documentocomissao> getListaDocumentocomissao() {
		return listaDocumentocomissao;
	}
	
	public void setListaDocumentocomissao(List<Documentocomissao> listaDocumentocomissao) {
		this.listaDocumentocomissao = listaDocumentocomissao;
	}
	@DisplayName("Tipo Identidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoidentidade")
	public Tipoidentidade getTipoidentidade() {
		return tipoidentidade;
	}
	public void setTipoidentidade(Tipoidentidade tipoidentidade) {
		this.tipoidentidade = tipoidentidade;
	}
	
	@Transient
	public Boolean getIsIdentificadorGerado() {
		return isIdentificadorGerado;
	}

	public void setIsIdentificadorGerado(Boolean isIdentificadorGerado) {
		this.isIdentificadorGerado = isIdentificadorGerado;
	}
	
	@Transient
	public String getNomeCpfOuCnpj() {
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null && this.getCnpj().getValue() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null && this.getCpf().getValue() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}
	
	@Transient
	public String getNomeCpfOuCnpjRazaosocial() {
		StringBuilder s = new StringBuilder();
		if(this.getIdentificador() != null && !"".equals(this.getIdentificador())){
			s.append(this.getIdentificador() + " - ");
		}
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		if(this.razaosocial != null){
			s.append("<span><BR>Raz�o Social: ").append(this.razaosocial).append("</span>");
		}
		return s.toString();
	}

	@Transient
	public String getNomeCpfOuCnpjRazaosocialTelefone() {
		StringBuilder s = new StringBuilder();
		if(this.getIdentificador() != null && !"".equals(this.getIdentificador())){
			s.append(this.getIdentificador() + " - ");
		}
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		if(this.razaosocial != null){
			s.append("<span><BR>Raz�o Social: ").append(this.razaosocial).append("</span>");
		}
		String telefonePrincipal = this.getTelefoneprincipal();
		if(telefonePrincipal != null && !telefonePrincipal.equals("")){
			s.append("<span><BR>Telefone: ").append(telefonePrincipal).append("</span>");
		}
		return s.toString();
	}
	
	@Transient
	public String getIdentificadorNomeCpfOuCnpj() {
		StringBuilder s = new StringBuilder();
		if (this.getIdentificador() != null){
			s.append(this.getIdentificador()).append(" - ");
		}
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}
	@DisplayName("C�digo")
	@Transient
	public String getIdentificadorOuCdpessoa() {
		StringBuilder s = new StringBuilder();
		if(this.getIdentificador() != null){
			s.append(this.getIdentificador());
		}
		
		else if (this.getCdpessoa() != null){
			s.append(this.getCdpessoa());
		}
		
		return s.toString();
	}
	
	@OneToMany(mappedBy="cliente")
	@DisplayName("Refer�ncia")
	public Set<Clienterelacao> getListaClienterelacao() {
		return listaClienterelacao;
	}
	public void setListaClienterelacao(Set<Clienterelacao> listaClienterelacao) {
		this.listaClienterelacao = listaClienterelacao;
	}
	@DisplayName("Vendedor")
	@OneToMany(mappedBy="cliente")
	public Set<Clientevendedor> getListaClientevendedor() {
		return listaClientevendedor;
	}

	public void setListaClientevendedor(Set<Clientevendedor> listaClientevendedor) {
		this.listaClientevendedor = listaClientevendedor;
	}
	
	@DisplayName("Contabilidade centralizada")
	public Boolean getContabilidadecentralizada() {
		return contabilidadecentralizada;
	}
	
	public void setContabilidadecentralizada(Boolean contabilidadecentralizada) {
		this.contabilidadecentralizada = contabilidadecentralizada;
	}

	@Transient
	public Telefone getTelefonePrincipal() {
		if(telefonePrincipal!=null)
			return telefonePrincipal;
		
		if(listaTelefone!=null && !listaTelefone.isEmpty()){
			for(Telefone telefone : listaTelefone){
				if(Telefonetipo.PRINCIPAL.equals(telefone.getTelefonetipo()))
					return telefone;
			}
		}
		return null;
	}

	public void setTelefonePrincipal(Telefone telefonePrincipal) {
		this.telefonePrincipal = telefonePrincipal;
	}
	
	@Transient
	public Boolean getIsClienteVendaEdicao() {
		return isClienteVendaEdicao;
	}
	public void setIsClienteVendaEdicao(Boolean isClienteVendaEdicao) {
		this.isClienteVendaEdicao = isClienteVendaEdicao;
	}

	@Transient
	public Telefone getCelular() {
		return celular;
	}

	public void setCelular(Telefone celular) {
		this.celular = celular;
	}

	@DisplayName("Passaporte")
	public String getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}
	
	@DisplayName("Validade Passaporte")
	public Date getValidadepassaporte() {
		return validadepassaporte;
	}

	public void setValidadepassaporte(Date validadepassaporte) {
		this.validadepassaporte = validadepassaporte;
	}

	@DisplayName("N�o considerar taxa de boleto")
	public Boolean getNaoconsiderartaxaboleto() {
		return naoconsiderartaxaboleto;
	}

	public void setNaoconsiderartaxaboleto(Boolean naoconsiderartaxaboleto) {
		this.naoconsiderartaxaboleto = naoconsiderartaxaboleto;
	}

	@DisplayName("Discriminar Taxa de Boleto")
	public Boolean getDiscriminartaxaboleto() {
		return discriminartaxaboleto;
	}

	public void setDiscriminartaxaboleto(Boolean discriminartaxaboleto) {
		this.discriminartaxaboleto = discriminartaxaboleto;
	}

	@DisplayName("Discriminar Desconto nas Notas")
	public Boolean getDiscriminardescontonota() {
		return discriminardescontonota;
	}
	
	public void setDiscriminardescontonota(Boolean discriminardescontonota) {
		this.discriminardescontonota = discriminardescontonota;
	}
	
	@DisplayName("C�digo de regime tribut�rio")
	public Codigoregimetributario getCrt() {
		return crt;
	}

	public void setCrt(Codigoregimetributario crt) {
		this.crt = crt;
	}
	
	@Transient
	public List<Vendahistorico> getListaVendahistorico() {
		return listaVendahistorico;
	}
	public void setListaVendahistorico(
			List<Vendahistorico> listaVendahistorico) {
		this.listaVendahistorico = listaVendahistorico;
	}
	@Transient
	public String getHistoricoLead() {
		return historicoLead;
	}

	public void setHistoricoLead(String historicoLead) {
		this.historicoLead = historicoLead;
	}
	@DisplayName("Taxa de Pedido de Venda")
	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}
	@DisplayName("Cr�dito Limite para Compras")
	public Double getCreditolimitecompra() {
		return creditolimitecompra;
	}
	@DisplayName("Validade do limite para compras")
	public Date getDtvalidadelimitecompra() {
		return dtvalidadelimitecompra;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}

	public void setCreditolimitecompra(Double creditolimitecompra) {
		this.creditolimitecompra = creditolimitecompra;
	}

	public void setDtvalidadelimitecompra(Date dtvalidadelimitecompra) {
		this.dtvalidadelimitecompra = dtvalidadelimitecompra;
	}

	@DisplayName("Vale Compra")
	@OneToMany(mappedBy="cliente")
	public List<Valecompra> getListaValecompra() {
		return listaValecompra;
	}
	public void setListaValecompra(List<Valecompra> listaValecompra) {
		this.listaValecompra = listaValecompra;
	}
	
	@DisplayName("Forma Pagamento")
	@OneToMany(mappedBy="cliente")
	public List<ClienteDocumentoTipo> getListDocumentoTipo() {
		return listDocumentoTipo;
	}

	@DisplayName("Condi��o de Pagamento")
	@OneToMany(mappedBy="cliente")
	public List<ClientePrazoPagamento> getListPrazoPagamento() {
		return listPrazoPagamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	@DisplayName("Conta cont�bil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setContaContabil(ContaContabil contacontabil) {
		this.contaContabil = contacontabil;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigotributacao")
	@DisplayName("C�digo Tributa��o")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}
	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}
	@DisplayName("Ramo Atividade")
	@MaxLength(500)
	public String getRamoatividade() {
		return ramoatividade;
	}
	public void setRamoatividade(String ramoatividade) {
		this.ramoatividade = ramoatividade;
	}
	@DisplayName("Objeto Social")
	@MaxLength(1000)
	public String getObjetosocial() {
		return objetosocial;
	}
	public void setObjetosocial(String objetosocial) {
		this.objetosocial = objetosocial;
	}

	@DisplayName("Informa��es Adicionais de Interesse do Fisco")
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Contribuinte")
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}

	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
	
	@Transient
	public String getNomeWithIdentificadorOuCdpessoa() {
		StringBuilder s = new StringBuilder();
		
		if(this.getIdentificador() != null && !"".equals(this.getIdentificador())){
			s.append(this.getIdentificador());
		}else if (this.getCdpessoa() != null){
			s.append(this.getCdpessoa());
		}
		
		s.append(!"".equals(s.toString()) ? " - " : "").append(this.getNome());
		
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}else if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		
		
		return s.toString();
	}
	
	@DisplayName("GRUPO DE TRIBUTA��O")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public void setListDocumentoTipo(List<ClienteDocumentoTipo> listTipoDocumento) {
		this.listDocumentoTipo = listTipoDocumento;
	}

	public void setListPrazoPagamento(List<ClientePrazoPagamento> listPrazoPagamento) {
		this.listPrazoPagamento = listPrazoPagamento;
	}
	
	@Transient
	public Money getSaldoValecompra() {
		return saldoValecompra;
	}
	
	public void setSaldoValecompra(Money saldoValecompra) {
		this.saldoValecompra = saldoValecompra;
	}

	@DisplayName("Contribuinte ICMS")
	public Boolean getContribuinteICMS() {
		return contribuinteICMS;
	}

	public void setContribuinteICMS(Boolean contribuinteICMS) {
		this.contribuinteICMS = contribuinteICMS;
	}
	
	public void setLayoutdiscriminacaoservico(ReportTemplateBean layoutdiscriminacaoservico) {
		this.layoutdiscriminacaoservico = layoutdiscriminacaoservico;
	}
	
	public Integer getIdentificadorordenacao() {
		return identificadorordenacao;
	}
	
	public void setIdentificadorordenacao(Integer identificadorordenacao) {
		this.identificadorordenacao = identificadorordenacao;
	}
	
	@Transient
	public Contato getContatoResponsavel() {
		if(this.listaContato != null && this.listaContato.size() > 0){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null){
					Contato contato = pessoaContato.getContato();
					if (contato.getResponsavelos()) {
						return contato;
					}
				}
			}
			return this.listaContato.iterator().next().getContato();
		}
		return null;
	}

	@Transient
	public Money getSaldoAntecipacao() {
		return saldoAntecipacao;
	}

	public void setSaldoAntecipacao(Money saldoAntecipacao) {
		this.saldoAntecipacao = saldoAntecipacao;
	}
	
	@Transient
	public String getRazaoOrNomeByTipopessoa(){
		if(this.tipopessoa != null){
			if(this.tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)) 
				return this.razaosocial != null ? this.razaosocial : this.nome;
			else 
				return this.nome;
		} else return this.razaosocial;
	}
	
	@DisplayName("Empresa")
	@OneToMany(mappedBy="cliente")
	public List<ClienteEmpresa> getListaClienteEmpresa() {
		return listaClienteEmpresa;
	}
	
	public void setListaClienteEmpresa(List<ClienteEmpresa> listaClienteEmpresa) {
		this.listaClienteEmpresa = listaClienteEmpresa;
	}
	
	@Transient
	public List<ClienteEmpresa> getListaClienteEmpresaNaoAssociada() {
		return listaClienteEmpresaNaoAssociada;
	}
	
	public void setListaClienteEmpresaNaoAssociada(List<ClienteEmpresa> listaClienteEmpresaNaoAssociada) {
		this.listaClienteEmpresaNaoAssociada = listaClienteEmpresaNaoAssociada;
	}
	
	@Transient
	public String getContatoAndTelefoneResponsavel() {
		contatoAndTelefoneResponsavel = "";
		if(this.getContatoResponsavel()!=null){
			contatoAndTelefoneResponsavel = this.getContatoResponsavel().getNome();
			if(this.getContatoResponsavel().getTelefones() != null){
				contatoAndTelefoneResponsavel = contatoAndTelefoneResponsavel + " - " + this.getContatoResponsavel().getTelefonesSeparadosPorBarra();
			}
		}
		
		return contatoAndTelefoneResponsavel;
	}
	public void setContatoAndTelefoneResponsavel(String contatoAndTelefoneResponsavel) {
		this.contatoAndTelefoneResponsavel = contatoAndTelefoneResponsavel;
	}
	
	@OneToMany(mappedBy="cliente")
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	
	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	@Transient
	@DisplayName("Question�rio/Servi�os")
	public Set<Pessoaquestionario> getListaQuestionarioWithAtividadetipo() {
		return listaQuestionarioWithAtividadetipo;
	}
	
	public void setListaQuestionarioWithAtividadetipo(
			Set<Pessoaquestionario> listaQuestionarioWithAtividadetipo) {
		this.listaQuestionarioWithAtividadetipo = listaQuestionarioWithAtividadetipo;
	}
	
	@DisplayName("Associa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdassociacao")
	public Fornecedor getAssociacao() {
		return associacao;
	}
	
	public void setAssociacao(Fornecedor associacao) {
		this.associacao = associacao;
	}
	
	@DisplayName("Especialidades")
	@OneToMany(mappedBy="cliente")
	public List<Clienteespecialidade> getListaClienteespecialidade() {
		return listaClienteespecialidade;
	}
	
	public void setListaClienteespecialidade(List<Clienteespecialidade> listaClienteespecialidade) {
		this.listaClienteespecialidade = listaClienteespecialidade;
	}
	
	@Transient
	public String getSegmentos(){
		StringBuilder sb = new StringBuilder();
		if(listaClienteSegmento != null && listaClienteSegmento.size() > 0){
			for (ClienteSegmento cs : listaClienteSegmento) {
				if(cs.getSegmento() != null){
					sb.append(cs.getSegmento().getNome());
					sb.append(", ");
				}
			}
			if(!sb.toString().equals("")){
				sb.delete(sb.length() - 2, sb.length());
			}
		}
		return sb.toString();
	}

	@Transient
	public String getRgOrIeByTipopessoa() {
		if(this.tipopessoa != null) {
			if(this.tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)) 
				return this.inscricaoestadual;
			else 
				return this.rg;
		} else return null;
	}
	
	@Transient
	public String getCpfOrCnpjByTipopessoa() {
		if(this.tipopessoa != null) {
			if(this.tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)) 
				return this.cnpj != null ? this.cnpj.getValue() : null;
			else 
				return this.cpf != null ? this.cpf.getValue() : null;
		} else return null;
	}
	
	@Transient
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	@OneToMany(mappedBy="cliente")
	@DisplayName("Licen�a")
	public Set<ClienteLicencaSipeagro> getListaClienteLicencaSipeagro() {
		return listaClienteLicencaSipeagro;
	}

	public void setListaClienteLicencaSipeagro(
			Set<ClienteLicencaSipeagro> listaClienteLicencaSipeagro) {
		this.listaClienteLicencaSipeagro = listaClienteLicencaSipeagro;
	}


	
	
}