package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_planejamentodespesa",sequenceName="sq_planejamentodespesa")
public class Planejamentodespesa implements Log{
	
	protected Integer cdplanejamentodespesa;
	protected Planejamento planejamento;
	protected Fornecimentotipo fornecimentotipo;
	protected Integer qtde;
	protected Money unitario;
	protected Money total;
	protected Contagerencial contagerencial;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_planejamentodespesa",strategy=GenerationType.AUTO)
	public Integer getCdplanejamentodespesa() {
		return cdplanejamentodespesa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentotipo")
	public Fornecimentotipo getFornecimentotipo() {
		return fornecimentotipo;
	}
	@Required
	@MaxLength(9)
	public Integer getQtde() {
		return qtde;
	}
	@Required
	@Transient
	@MaxLength(20)
	public Money getTotal() {
		return total;
	}
	@Transient
	public Money calculaTotal(){
		return unitario.multiply(new Money(qtde));
	}
	@Required
	@Column(name="valorunitario")
	public Money getUnitario() {
		return unitario;
	}
	@MaxLength(200)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setUnitario(Money unitario) {
		this.unitario = unitario;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setFornecimentotipo(Fornecimentotipo fornecimentotipo) {
		this.fornecimentotipo = fornecimentotipo;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCdplanejamentodespesa(Integer cdplanejamentodespesa) {
		this.cdplanejamentodespesa = cdplanejamentodespesa;
	}
	
	
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
