package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;


@Entity
@SequenceGenerator(name = "sq_turma", sequenceName = "sq_turma")
public class Turma implements Log{

	protected Integer cdturma;
	protected Treinamento treinamento;
	protected Date dtinicio;
	protected Date dtfim;
	protected Integer cargahoraria;
	protected Hora horainicio;
	protected Hora horafim;
	protected String local;
	protected Instrutor instrutor;
	protected String empresa;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Set<Turmaparticipante> listaTurmaparticipante;
	
//	TRANSIENTES
	protected String periodo;
	protected String data;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_turma")
	public Integer getCdturma() {
		return cdturma;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtreinamento")
	public Treinamento getTreinamento() {
		return treinamento;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public Date getDtfim() {
		return dtfim;
	}
	
	public Hora getHorainicio() {
		return horainicio;
	}

	public Hora getHorafim() {
		return horafim;
	}
	
	@MaxLength(100)
	public String getLocal() {
		return local;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinstrutor")
	@DisplayName("Ministrador")
	public Instrutor getInstrutor() {
		return instrutor;
	}
	@MaxLength(50)
	@DisplayName("Empresa respons�vel")
	public String getEmpresa() {
		return empresa;
	}
	@MaxLength(5)
	@DisplayName("Carga hor�ria")
	public Integer getCargahoraria() {
		return cargahoraria;
	}
	@OneToMany(mappedBy="turma")
	@DisplayName("Participantes")
	public Set<Turmaparticipante> getListaTurmaparticipante() {
		return listaTurmaparticipante;
	}
	public void setListaTurmaparticipante(
			Set<Turmaparticipante> listaTurmaparticipante) {
		this.listaTurmaparticipante = listaTurmaparticipante;
	}
	public void setCargahoraria(Integer cargahoraria) {
		this.cargahoraria = cargahoraria;
	}
	public void setTreinamento(Treinamento treinamento) {
		this.treinamento = treinamento;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}
	   public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public void setInstrutor(Instrutor instrutor) {
		this.instrutor = instrutor;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public void setCdturma(Integer cdturma) {
		this.cdturma = cdturma;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}	
	
	@Transient
	@DisplayName("Per�odo")
	public String getPeriodo() {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(getDtinicio()) + " � " + format.format(getDtfim());
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"treinamento","dtinicio"})
	public String getDescription(){
		return this.treinamento.getNome() + " - " + SinedDateUtils.toString(this.dtinicio);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Turma) {
			Turma c = (Turma) obj;
			return c.getCdturma().equals(this.getCdturma());
		}
		return super.equals(obj);
	}
	
}
