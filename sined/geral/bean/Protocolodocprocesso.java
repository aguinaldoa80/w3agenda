package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_protocolodocprocesso",sequenceName="sq_protocolodocprocesso")
@DisplayName("Documento")
public class Protocolodocprocesso implements Log{
	//Tabela protocolodocprocesso	
	protected Integer cdprotocolodocprocesso;
	protected Documentoprocesso documentoprocesso;
	protected Protocolo protocolo;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("Id")
	@Id
	@GeneratedValue(generator="sq_protocolodocprocesso",strategy=GenerationType.AUTO)
	public Integer getCdprotocolodocprocesso() {
		return cdprotocolodocprocesso;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocesso")	
	@DisplayName("Documento/processo")
	public Documentoprocesso getDocumentoprocesso() {
		return documentoprocesso;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprotocolo")	
	public Protocolo getProtocolo() {
		return protocolo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdprotocolodocprocesso(Integer cdprotocolodocprocesso) {
		this.cdprotocolodocprocesso = cdprotocolodocprocesso;
	}
	public void setDocumentoprocesso(Documentoprocesso documentoprocesso) {
		this.documentoprocesso = documentoprocesso;
	}
	public void setProtocolo(Protocolo protocolo) {
		this.protocolo = protocolo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
