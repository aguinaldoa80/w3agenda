package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Referencia {
	
	MES_ANO_ANTERIOR("M�s/Ano anterior"), // 0
	MES_ANO_CORRENTE("M�s/Ano corrente"), // 1
	MES_ANO_FUTURO("M�s/Ano futuro");	  // 2	
	
	private Referencia (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
}
