package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradoracidente", sequenceName = "sq_colaboradoracidente")
public class Colaboradoracidente{

	protected Integer cdcolaboradoracidente;
	protected Colaborador colaborador;
	protected String registro;
	protected Date dtregistro;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradoracidente")
	public Integer getCdcolaboradoracidente() {
		return cdcolaboradoracidente;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(20)
	@DisplayName("N�mero da CAT")
	public String getRegistro() {
		return registro;
	}
	
	@Required
	@DisplayName("Data do registro")
	public Date getDtregistro() {
		return dtregistro;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setDtregistro(Date dtregistro) {
		this.dtregistro = dtregistro;
	}
	
	public void setCdcolaboradoracidente(Integer cdcolaboradoracidente) {
		this.cdcolaboradoracidente = cdcolaboradoracidente;
	}
	
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	
}
