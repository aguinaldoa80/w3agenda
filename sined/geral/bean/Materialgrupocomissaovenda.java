package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Materialgrupocomissaopara;


@Entity
@SequenceGenerator(name = "sq_materialgrupocomissaovenda", sequenceName = "sq_materialgrupocomissaovenda")
public class Materialgrupocomissaovenda {

	protected Integer cdmaterialgrupocomissaovenda;
	protected Materialgrupo materialgrupo;
	protected Cargo cargo;
	protected Comissionamento comissionamento;
	protected Documentotipo documentotipo;
	protected Pedidovendatipo pedidovendatipo;
	protected Fornecedor fornecedor;
	protected Materialgrupocomissaopara comissaopara;
	protected Categoria categoria;
//	protected Double comissao;  
//	protected Boolean dividirprincipal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialgrupocomissaovenda")
	public Integer getCdmaterialgrupocomissaovenda() {
		return cdmaterialgrupocomissaovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Cargo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("Tipo de Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	
//	@Required
//	@DisplayName("Comiss�o")
//	public Double getComissao() {
//		return comissao;
//	}
//	@DisplayName("Dividir com o Vendedor Principal")
//	public Boolean getDividirprincipal() {
//		return dividirprincipal;
//	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@Required
	@DisplayName("Comiss�o Para")
	public Materialgrupocomissaopara getComissaopara() {
		return comissaopara;
	}
	
	@DisplayName("Categoria")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoria")
	public Categoria getCategoria() {
		return categoria;
	}
	
	public void setComissaopara(Materialgrupocomissaopara comissaopara) {
		this.comissaopara = comissaopara;
	}
	
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public void setCdmaterialgrupocomissaovenda(Integer cdmaterialgrupocomissaovenda) {
		this.cdmaterialgrupocomissaovenda = cdmaterialgrupocomissaovenda;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
//	public void setComissao(Double comissao) {
//		this.comissao = comissao;
//	}
//	public void setDividirprincipal(Boolean dividirprincipal) {
//		this.dividirprincipal = dividirprincipal;
//	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@DisplayName("Forma de Pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}
