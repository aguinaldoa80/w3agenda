package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.EntityBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("Tipo de e-mail")
public class Envioemailtipo extends EntityBean {

	public static final Envioemailtipo RECUPERACAO_SENHA = new Envioemailtipo(1, "Recupera��o de senha");
	public static final Envioemailtipo AVISO_ATRASO_LANCAMENTO = new Envioemailtipo(2, "Aviso de atraso de lan�amento");
	public static final Envioemailtipo ENVIO_SENHA_CADASTRO_CURRICULO = new Envioemailtipo(3, "Envio de senha do cadastro de curr�culo");
	public static final Envioemailtipo COMUNICADO = new Envioemailtipo(4, "Envio de comunicado");
	public static final Envioemailtipo BOLETO = new Envioemailtipo(5, "Envio de boleto");
	public static final Envioemailtipo AVISO_CONTRATO_SUSPENSO = new Envioemailtipo(6, "Aviso de contrato suspenso");
	public static final Envioemailtipo CONVITE_COTACAO = new Envioemailtipo(7, "Convite para cota��o");
	public static final Envioemailtipo MAILING_CLIENTE = new Envioemailtipo(8, "Mailing de cliente");
	public static final Envioemailtipo AVISO_APROVACAO_ORDEM_COMPRA = new Envioemailtipo(10, "Aviso de aprova��o de ordem de compra");
	public static final Envioemailtipo PEDIDO_ORDEM_COMPRA = new Envioemailtipo(11, "Envio de pedido para ordem de compra");
	public static final Envioemailtipo AVISO_ORDEM_SERVICO_CONCLUIDA = new Envioemailtipo(12, "Aviso de ordem de servi�o conclu�da");
	public static final Envioemailtipo AVISO_ALTERACAO_ORDEM_SERVICO = new Envioemailtipo(13, "Aviso de altera��o de ordem de servi�o");
	public static final Envioemailtipo AVISO_AUTORIZACAO_COMPRA = new Envioemailtipo(14, "Aviso de autoriza��o da solicita��o de compra ");
	public static final Envioemailtipo AVISO_SOLICITACAO_SERVICO = new Envioemailtipo(15, "Aviso de solicita��o de servi�o");
	public static final Envioemailtipo AVISO_AGENDAMENTO_INSPECAO = new Envioemailtipo(16, "Aviso de agendamento dos itens de inspe��o");
	public static final Envioemailtipo MAILING_CONTA_CRM = new Envioemailtipo(17, "Mailing de conta CRM");
	public static final Envioemailtipo MAILING_LEAD = new Envioemailtipo(18, "Mailing de Lead");
	public static final Envioemailtipo ENVIO_DANFE_CLIENTE = new Envioemailtipo(19, "Envio de DANFE para cliente");
	public static final Envioemailtipo ENVIO_NFE_CLIENTE = new Envioemailtipo(20, "Envio de NF-e para cliente");
	public static final Envioemailtipo AVISO_CONTA_VENCIDA = new Envioemailtipo(21, "Aviso de conta vencida");
	public static final Envioemailtipo ENVIO_PROTOCOLO = new Envioemailtipo(22, "Envio de Protocolo de Provid�ncia");
	public static final Envioemailtipo COBRANCA_PENDENCIA_FINANCEIRA = new Envioemailtipo(23, "Cobran�a de pend�ncia financeira");
	public static final Envioemailtipo ENVIO_DANFE_XML_CLIENTE = new Envioemailtipo(25, "Envio de Danfe e Xml para cliente");
	public static final Envioemailtipo ENVIO_NFSE_XML_CLIENTE = new Envioemailtipo(26, "Envio de NFS-e e Xml para cliente");
	public static final Envioemailtipo COMPROVANTE_VENDA = new Envioemailtipo(27, "Comprovante de Venda");
	public static final Envioemailtipo COMPROVANTE_ORCAMENTO = new Envioemailtipo(28, "Comprovante de Or�amento");
	public static final Envioemailtipo COMPROVANTE_PEDIDO_VENDA = new Envioemailtipo(29, "Comprovante de Pedido de Venda");
	public static final Envioemailtipo ENVIO_NFSE_CLIENTE = new Envioemailtipo(30, "Envio de NFS-e para cliente");
	public static final Envioemailtipo ENVIO_XML_CLIENTE = new Envioemailtipo(31, "Envio de Xml para cliente");
	public static final Envioemailtipo AVISO_DE_PROXIMA_VACINACAO = new Envioemailtipo(32, "Aviso de pr�xima vacina��o");
	public static final Envioemailtipo ENVIO_FATURA_LOCACAO = new Envioemailtipo(34, "Envio de fatura de loca��o");
	public static final Envioemailtipo ENVIO_AVISO_DIARIO = new Envioemailtipo(35, "Envio do aviso di�rio");
	public static final Envioemailtipo ENVIO_AVISO = new Envioemailtipo(36, "Envio do aviso");
	public static final Envioemailtipo ENVIO_ACEITE_DIGITAL = new Envioemailtipo(37, "Envio do aceite digital");
	public static final Envioemailtipo ENVIO_RASTREAMENTO_ECOMMERCE = new Envioemailtipo(38, "Envio do c�digo de rastreio");
	public static final Envioemailtipo ENVIO_ENTREGA_COM_IMPEDIMENTOS_ECOMMERCE = new Envioemailtipo(39, "Envio do e-mail de entrega com impedimentos");

	protected Integer cdenvioemailtipo;
	protected String nome;

	public Envioemailtipo() {
	}

	public Envioemailtipo(Integer cdenvioemailtipo, String nome) {
		this.cdenvioemailtipo = cdenvioemailtipo;
		this.nome = nome;
	}

	@Id
	public Integer getCdenvioemailtipo() {
		return cdenvioemailtipo;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdenvioemailtipo(Integer cdenvioemailtipo) {
		this.cdenvioemailtipo = cdenvioemailtipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}