package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_servidorinterface", sequenceName = "sq_servidorinterface")
public class Servidorinterface{

	protected Integer cdservidorinterface;
	protected String nome;
	protected Boolean ativo = true;
	protected Servidor servidor;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidorinterface")
	public Integer getCdservidorinterface() {
		return cdservidorinterface;
	}

	@Required
	@MaxLength(250)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")
	public Servidor getServidor() {
		return servidor;
	}

	public void setCdservidorinterface(Integer cdservidorinterface) {
		this.cdservidorinterface = cdservidorinterface;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
}
