package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_custeio_tipo", sequenceName = "sq_custeio_tipo")
@DisplayName("TIPO")
@Table(name="custeiotipodocumento")
public class CusteioTipoDocumento {
	
						
	private Integer cdCusteioTipoDocumento;
	private TipoDocumentoCusteioEnum tipoDocumentoCusteioEnum;
	private Integer codObjetoOrigem;
	private Rateio codRateioOrigem;
	private Rateio codRateioProcessado;
	private CusteioProcessado custeioProcessado;
	
	
	//inicio dos GET
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_custeio_tipo")
	public Integer getCdCusteioTipoDocumento() {
		return cdCusteioTipoDocumento;
	}
	public TipoDocumentoCusteioEnum getTipoDocumentoCusteioEnum() {
		return tipoDocumentoCusteioEnum;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="codrateioorigem")
	public Rateio getCodRateioOrigem() {
		return codRateioOrigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="codrateioprocessado")
	public Rateio getCodRateioProcessado() {
		return codRateioProcessado;
	}
	@DisplayName("Custeio")
	@JoinColumn(name = "cdcusteioprocessado")
	@ManyToOne(fetch = FetchType.LAZY)
	public CusteioProcessado getCusteioProcessado() {
		return custeioProcessado;
	}
	public Integer getCodObjetoOrigem() {
		return codObjetoOrigem;
	}
	//inicio dos SET
	public void setCdCusteioTipoDocumento(Integer cdCusteioTipoDocumento) {
		this.cdCusteioTipoDocumento = cdCusteioTipoDocumento;
	}
	public void setTipoDocumentoCusteioEnum(
			TipoDocumentoCusteioEnum tipoDocumentoCusteioEnum) {
		this.tipoDocumentoCusteioEnum = tipoDocumentoCusteioEnum;
	}
	
	public void setCusteioProcessado(CusteioProcessado custeioProcessado) {
		this.custeioProcessado = custeioProcessado;
	}
	public void setCodObjetoOrigem(Integer codObjetoOrigem) {
		this.codObjetoOrigem = codObjetoOrigem;
	}
	public void setCodRateioOrigem(Rateio codRateioOrigem) {
		this.codRateioOrigem = codRateioOrigem;
	}
	public void setCodRateioProcessado(Rateio codRateioProcessado) {
		this.codRateioProcessado = codRateioProcessado;
	}
	
	
}
