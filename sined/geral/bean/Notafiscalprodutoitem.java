package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoDIFALInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.ExigibilidadeIssNfe;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.InformacaoAdicionalProdutoBean;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutoitem",sequenceName="sq_notafiscalprodutoitem")
public class Notafiscalprodutoitem implements Log, Cloneable, CalculoDIFALInterface, CalculoImpostoInterface, ValidateLoteestoqueObrigatorioInterface {
	
	protected Integer cdnotafiscalprodutoitem;
	protected Notafiscalproduto notafiscalproduto;
	protected Vendamaterial vendamaterial;
	protected Vendamaterialmestre vendamaterialmestre;
	protected Pedidovendamaterial pedidovendamaterial;
	
	protected Material material;
	protected Pneu pneu;
	protected Grupotributacao grupotributacao;
	protected Materialclasse materialclasse;
	protected Localarmazenagem localarmazenagem;
	protected Unidademedida unidademedida;
	protected Patrimonioitem patrimonioitem;
	protected String gtin;
	protected String identificadorespecifico;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
	protected String extipi;
	protected String nve;
	protected String recopi;
	protected String cbenef;
	
	protected String numeropedidocompra;
	protected Integer itempedidocompra;
	
	protected Double qtde;
	protected Double valorunitario;
	protected Money valorbruto;
	protected Boolean incluirvalorprodutos; 
	
	protected Double percentualimpostoecf;
	protected Money valortotaltributos;
	
	private Double percentualImpostoFederal;
	private Double percentualImpostoEstadual;
	private Double percentualImpostoMunicipal;
	private Money valorImpostoFederal;
	private Money valorImpostoEstadual;
	private Money valorImpostoMunicipal;
	
	protected Money valordesconto;
	protected Money valorfrete;
	protected Money valorseguro;
	protected Money outrasdespesas;
	
	protected Double pesoliquido;
	protected Double pesobruto;
	
	protected Double percentualdevolvido;
	protected Money valoripidevolvido;
	
	protected Boolean tributadoicms;
	
	protected Boolean incluirissvalor;
	protected Boolean incluiricmsvalor;
	protected Boolean incluiripivalor;
	protected Boolean incluirpisvalor;
	protected Boolean incluircofinsvalor;
	
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms; 
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	
	protected Origemproduto origemproduto;
	protected Modalidadebcicms modalidadebcicms;
	protected String cest;
	protected Modalidadebcicmsst modalidadebcicmsst;
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	protected Double reducaobcicms;
	protected Money bcoperacaopropriaicms;
	protected Uf uficmsst;
	protected Motivodesoneracaoicms motivodesoneracaoicms; 
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Money valorbcicmsstremetente;
	protected Money valoricmsstremetente;
	protected Money valorbcicmsstdestinatario;
	protected Money valoricmsstdestinatario;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	private Money valorIcmsPropSub;
	private Double redBaseCalcEfetiva;
	private Money valorBaseCalcEfetiva;
	private Double icmsEfetiva;
	private Money valorIcmsEfetiva;
	
	private String numeroFci;
	
	protected Double aliquotacreditoicms;
	protected Money valorcreditoicms;
	
	protected String codigoseloipi;
	protected Integer qtdeseloipi;
	protected Cnpj cnpjprodutoripi;
	
	protected Tipocobrancaipi tipocobrancaipi;
	protected Money valorbcipi;
	protected Double ipi;
	protected Money valoripi;
	protected Tipocalculo tipocalculoipi;
	protected Money aliquotareaisipi;
	protected Double qtdevendidaipi;
	protected String codigoenquadramentoipi;
	
	protected Tipocobrancapis tipocobrancapis;
	protected Money valorbcpis;
	protected Double pis;
	protected Money valorpis;
	protected Tipocalculo tipocalculopis;
	protected Money aliquotareaispis;
	protected Double qtdevendidapis;
	
	protected Tipocobrancacofins tipocobrancacofins;
	protected Money valorbccofins;
	protected Double cofins;
	protected Money valorcofins;
	protected Tipocalculo tipocalculocofins;
	protected Money aliquotareaiscofins;
	protected Double qtdevendidacofins;
	
	protected Money valorbciss;
	protected Double iss;
	protected Money valoriss;
	protected Municipio municipioiss;
	protected Tipotributacaoiss tipotributacaoiss;
	protected Itemlistaservico itemlistaservico;
	
	protected Money valorbccsll;
	protected Double csll;
	protected Money valorcsll;
	protected Money valorbcir;
	protected Double ir;
	protected Money valorir;
	protected Money valorbcinss;
	protected Double inss;
	protected Money valorinss;
	
	protected Money valordeducaoservico;
	protected Money valoroutrasretencoesservico;
	protected Money valordescontocondicionadoservico;
	protected Money valordescontoincondicionadoservico;
	protected Money valorissretido;
	protected ExigibilidadeIssNfe exigibilidadeiss;
	protected String codigoservico;
	protected Municipio municipioservico;
	protected Pais paisservico;
	protected String processoservico;
	protected Boolean incentivofiscal;
	
	protected Boolean dadosimportacao;
	protected Money valorbcii;
	protected Money despesasaduaneiras;
	protected Money valoriof;
	protected Money valorii;
	
	protected String infoadicional;
	protected Loteestoque loteestoque;
	
	protected Boolean dadoscombustivel;
	protected Codigoanp codigoanp;
	protected String codif;
	protected Double qtdetemperatura;
	protected Double percentualglp;
	protected Double percentualgnn;
	protected Double percentualgni;
	protected Double valorpartida;
	protected Uf ufconsumo;
	protected Double qtdebccide;
	protected Double valoraliqcide;
	protected Double valorcide;
	
	protected Boolean dadosexportacao;
	protected String drawbackexportacao;
	protected String numregistroexportacao;
	protected String chaveacessoexportacao;
	protected Double quantidadeexportacao;
	
	protected ValorFiscal valorFiscalIcms;
	protected ValorFiscal valorFiscalIpi;
	
	protected Set<Materialnumeroserie> listaMaterialnumeroserie;
	protected Set<Notafiscalprodutoitemdi> listaNotafiscalprodutoitemdi;
	protected Set<Notafiscalprodutoitemnotaentrada> listaNotafiscalprodutoitemnotaentrada;
	protected List<Notafiscalprodutoitementregamaterial> listaNotaprodutoitementregamaterial;
	protected List<Entregamaterial> listaEntregamaterial;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	
	private Money valorComissao;
	protected Double valorcustomaterial;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transcient
	protected String identificadortela;
	protected Integer indexlista;
	protected String cdNota;
	protected Integer cdentregadocumento;
	protected Boolean itemescrito;
	protected Double ValorEntrada;
//	protected Materialgrupotributacao materialgrupotributacaotrans;
	protected Grupotributacao grupotributacaotrans;
	protected Unidademedida unidademedidaAntiga;
	protected Naturezaoperacao naturezaoperacao;
	protected Empresa empresa;
	protected Endereco endereco;
	protected Double percentualBcicms;
	protected Double percentualBcipi;
	protected Double percentualBcpis;
	protected Double percentualBccofins;
	protected Double percentualBciss;
	protected Double percentualBccsll;
	protected Double percentualBcir;
	protected Double percentualBcinss;
	protected Double percentualBcfcp;
	protected Double percentualBcfcpst;
	protected Integer cdcoletamaterial;
	protected Double percentualBcicmsEfetiva;
	protected Integer cdproducaoordemmaterial;
	protected Motivodevolucao motivodevolucao;
	protected Finalidadenfe finalidadenfe;
	protected Boolean haveNumeroSerie;
	protected Notafiscalprodutoitem notafiscalprodutoitemIndustrializacao;
	protected Boolean isItemRetorno;
	protected String identificadorItemRetorno;
	protected String numeroEntregadocumento;
	protected Boolean naocalcularipi;
	protected String numeroNFEntrada;
	protected Date dtemissao;
	protected Fornecedor fornecedor;
	protected Operacaonfe operacaonfe;
	protected Money valorSemDesconto;
	protected InformacaoAdicionalProdutoBean informacaoAdicionalProdutoBean;
	protected String infoAdicionalItemAnterior; 
	protected List<Motivodevolucao> listaMotivodevolucao;	
	protected Boolean gerarE200;
	protected Boolean considerarIdPneu;
	protected Boolean nfe = Boolean.TRUE;
	protected Cliente cliente;
	protected Endereco enderecoCliente;
	protected ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	protected Localdestinonfe localdestinonfe;
	protected Money valordifal;
	protected Double difal;
	protected Double qtde_convertida;
	

	//M�TODO UTILZIADO PARA GERA��O DE NOTA FISCAL E SPED, CASO SEJA NECESS�RIO ALTERAR ESTE PROCESSO, � PRECISO VALIDAR O IMPACTO
	@Transient
	public String getIdentificadorForNotaSped(){
		String identificador = getIdentificadorespecifico();
		if(identificador != null && !identificador.equals("")){
			return identificador;
		} else {
			return getMaterial().getIdentificacaoOuCdmaterial();
		}
	}
	
	
	@Id
	@GeneratedValue(generator="sq_notafiscalprodutoitem", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutoitem() {
		return cdnotafiscalprodutoitem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdvendamaterialmestre")
	public Vendamaterialmestre getVendamaterialmestre() {
		return vendamaterialmestre;
	}

	public void setVendamaterialmestre(Vendamaterialmestre vendamaterialmestre) {
		this.vendamaterialmestre = vendamaterialmestre;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpneu")
	public Pneu getPneu() {
		return pneu;
	}
	
	@DisplayName("Patrim�nio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	@DisplayName("Local de Armazenagem")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}
	
	@Required
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	
	@DisplayName("GTIN (C�digo EAN)")
	@MaxLength(14)
	public String getGtin() {
		return gtin;
	}

	@DisplayName("Valor do frete")
	public Money getValorfrete() {
		return valorfrete;
	}
	
	@DisplayName("Valor do desconto")
	public Money getValordesconto() {
		return valordesconto;
	}

	@DisplayName("Valor de seguro")
	public Money getValorseguro() {
		return valorseguro;
	}

	@DisplayName("Outras despesas")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}

	@DisplayName("Tipo de cobran�a ICMS")
	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}

	@DisplayName("Valor da base de c�lculo do ICMS")
	public Money getValorbcicms() {
		return valorbcicms;
	}

	@DisplayName("Al�quota do ICMS (%)")
	public Double getIcms() {
		return icms;
	}

	@DisplayName("Valor do ICMS")
	public Money getValoricms() {
		return valoricms;
	}

	@DisplayName("Valor da base de c�lculo do ICMS ST")
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	@DisplayName("Al�quota do ICMS ST (%)")
	public Double getIcmsst() {
		return icmsst;
	}
	
	@DisplayName("Valor do ICMS ST")
	public Money getValoricmsst() {
		return valoricmsst;
	}
	
	@DisplayName("C�digo do selo")
	@MaxLength(60)
	public String getCodigoseloipi() {
		return codigoseloipi;
	}
	
	@DisplayName("CNPJ do Produtor")
	public Cnpj getCnpjprodutoripi() {
		return cnpjprodutoripi;
	}

	@DisplayName("Qtde. de selos")
	public Integer getQtdeseloipi() {
		return qtdeseloipi;
	}

	@DisplayName("Tipo de cobran�a do IPI")
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}

	@DisplayName("Valor da base de c�lculo")
	public Money getValorbcipi() {
		return valorbcipi;
	}

	@DisplayName("Aliquota (%)")
	public Double getIpi() {
		return ipi;
	}

	@DisplayName("Valor")
	public Money getValoripi() {
		return valoripi;
	}

	@DisplayName("Tipo de cobran�a do PIS")
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}

	@DisplayName("Valor da base de c�lculo")
	public Money getValorbcpis() {
		return valorbcpis;
	}

	@DisplayName("Al�quota (%)")
	public Double getPis() {
		return pis;
	}

	@DisplayName("Valor")
	public Money getValorpis() {
		return valorpis;
	}

	@DisplayName("Tipo de cobran�a do COFINS")
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}

	@DisplayName("Valor da base de c�lculo")
	public Money getValorbccofins() {
		return valorbccofins;
	}

	@DisplayName("Al�quota (%)")
	public Double getCofins() {
		return cofins;
	}

	@DisplayName("Valor")
	public Money getValorcofins() {
		return valorcofins;
	}

	@DisplayName("Valor da base de c�lculo")
	public Money getValorbciss() {
		return valorbciss;
	}

	@DisplayName("Aliquota (%)")
	public Double getIss() {
		return iss;
	}
	
	@DisplayName("Valor")
	public Money getValoriss() {
		return valoriss;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipioiss")
	@DisplayName("Munic�pio de incid�ncia")
	public Municipio getMunicipioiss() {
		return municipioiss;
	}

	@DisplayName("Tipo de tributa��o do ISS")
	public Tipotributacaoiss getTipotributacaoiss() {
		return tipotributacaoiss;
	}
	
	@DisplayName("Valor da BC do CSLL")
	public Money getValorbccsll() {
		return valorbccsll;
	}
	@DisplayName("Al�quota CSLL")
	public Double getCsll() {
		return csll;
	}
	@DisplayName("Valor do CSLL")
	public Money getValorcsll() {
		return valorcsll;
	}
	@DisplayName("Valor da BC do IR")
	public Money getValorbcir() {
		return valorbcir;
	}
	@DisplayName("Al�quota IR(%)")
	public Double getIr() {
		return ir;
	}
	@DisplayName("Valor do IR")
	public Money getValorir() {
		return valorir;
	}
	@DisplayName("Valor da BC do INSS")
	public Money getValorbcinss() {
		return valorbcinss;
	}
	@DisplayName("Al�quota INSS(%)")
	public Double getInss() {
		return inss;
	}
	@DisplayName("Valor do INSS")
	public Money getValorinss() {
		return valorinss;
	}

	@DisplayName("Informa��o adicional")
	public String getInfoadicional() {
		return infoadicional;
	}
	
	@DisplayName("NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	@MaxLength(9)
	@DisplayName("NCM completo")
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	
	@MaxLength(6)
	@DisplayName("NVE")
	public String getNve() {
		return nve;
	}
	
	@MaxLength(20)
	@DisplayName("N�mero do RECOPI")
	public String getRecopi() {
		return recopi;
	}
	
	@DisplayName("C�digo Benef�cio Fiscal")
	public String getCbenef() {
		return cbenef;
	}

	@DisplayName("EX da TIPI")
	@MaxLength(3)
	public String getExtipi() {
		return extipi;
	}
	
	@DisplayName("Valor bruto")
	public Money getValorbruto() {
		return valorbruto;
	}
	
	@DisplayName("Incluir no valor dos produtos")
	public Boolean getIncluirvalorprodutos() {
		return incluirvalorprodutos;
	}
	
	@DisplayName("Produto tributado pelo:")
	public Boolean getTributadoicms() {
		return tributadoicms;
	}
	
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculopis() {
		return tipocalculopis;
	}

	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaispis() {
		return aliquotareaispis;
	}

	@DisplayName("Qtde. vendida")
	public Double getQtdevendidapis() {
		return qtdevendidapis;
	}
	
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}
	
	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	
	@DisplayName("Qtde. vendida")
	public Double getQtdevendidaipi() {
		return qtdevendidaipi;
	}

	@DisplayName("C�digo de enquadramento")
	@MaxLength(3)
	public String getCodigoenquadramentoipi() {
		return codigoenquadramentoipi;
	}
	
	@DisplayName("Tipo de c�lculo")
	public Tipocalculo getTipocalculocofins() {
		return tipocalculocofins;
	}

	@DisplayName("Al�quota (em reais)")
	public Money getAliquotareaiscofins() {
		return aliquotareaiscofins;
	}

	@DisplayName("Qtde. vendida")
	public Double getQtdevendidacofins() {
		return qtdevendidacofins;
	}
	
	@DisplayName("Modal. de determ. do BC do ICMS")
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}

	@DisplayName("Modal. de determ. do BC do ICMS ST")
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}

	@DisplayName("% redu��o da BC do ICMS ST")
	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}

	@DisplayName("% margem de valor adic. ICMS ST")
	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}

	@DisplayName("% redu��o da BC do ICMS")
	public Double getReducaobcicms() {
		return reducaobcicms;
	}

	@DisplayName("% BC da opera��o pr�pria")
	public Money getBcoperacaopropriaicms() {
		return bcoperacaopropriaicms;
	}

	@DisplayName("UF do ICMS ST devido na opera��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduficmsst")
	public Uf getUficmsst() {
		return uficmsst;
	}

	@DisplayName("Motivo da desonera��o do ICMS")
	public Motivodesoneracaoicms getMotivodesoneracaoicms() {
		return motivodesoneracaoicms;
	}
	
	@DisplayName("Regime do ICMS")
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}
	
	@DisplayName("Al�quota aplic�vel de c�lculo do cr�dito")
	public Double getAliquotacreditoicms() {
		return aliquotacreditoicms;
	}
	
	@DisplayName("Cr�dito do ICMS que pode ser aproveitado")
	public Money getValorcreditoicms() {
		return valorcreditoicms;
	}
	
	@DisplayName("Incluir valor do ISS no valor bruto")
	public Boolean getIncluirissvalor() {
		return incluirissvalor;
	}

	@DisplayName("Incluir valor do ICMS no valor bruto")
	public Boolean getIncluiricmsvalor() {
		return incluiricmsvalor;
	}

	@DisplayName("Incluir valor do IPI no valor bruto")
	public Boolean getIncluiripivalor() {
		return incluiripivalor;
	}
	
	@DisplayName("Incluir valor do PIS no valor bruto")
	public Boolean getIncluirpisvalor() {
		return incluirpisvalor;
	}

	@DisplayName("Incluir valor do COFINS no valor bruto")
	public Boolean getIncluircofinsvalor() {
		return incluircofinsvalor;
	}
	
	@DisplayName("Item da lista de servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cditemlistaservico")
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}
	
	@DisplayName("DECLARA��O DE IMPORTA��O")
	@OneToMany(mappedBy="notafiscalprodutoitem")
	public Set<Notafiscalprodutoitemdi> getListaNotafiscalprodutoitemdi() {
		return listaNotafiscalprodutoitemdi;
	}
	
	@OneToMany(mappedBy="notafiscalprodutoitem")
	public Set<Notafiscalprodutoitemnotaentrada> getListaNotafiscalprodutoitemnotaentrada() {
		return listaNotafiscalprodutoitemnotaentrada;
	}
	
	@DisplayName("Valor da base de c�lculo")
	public Money getValorbcii() {
		return valorbcii;
	}

	@DisplayName("Despesas aduaneiras")
	public Money getDespesasaduaneiras() {
		return despesasaduaneiras;
	}

	@DisplayName("Valor do IOF")
	public Money getValoriof() {
		return valoriof;
	}

	@DisplayName("Valor do II")
	public Money getValorii() {
		return valorii;
	}
	
	@DisplayName("Dados de importa��o")
	public Boolean getDadosimportacao() {
		return dadosimportacao;
	}
	
	@DisplayName("Origem do produto")
	public Origemproduto getOrigemproduto() {
		return origemproduto;
	}
	
	@DisplayName("Dados de exporta��o")
	public Boolean getDadosexportacao() {
		return dadosexportacao;
	}
	
	@MaxLength(11)
	@DisplayName("N�mero de Drawback")
	public String getDrawbackexportacao() {
		return drawbackexportacao;
	}
	
	@MaxLength(12)
	@DisplayName("N�mero de registro")
	public String getNumregistroexportacao() {
		return numregistroexportacao;
	}
	
	@MaxLength(44)
	@DisplayName("Chave de acesso de NF-e")
	public String getChaveacessoexportacao() {
		return chaveacessoexportacao;
	}
	
	@DisplayName("Quantidade exportada")
	public Double getQuantidadeexportacao() {
		return quantidadeexportacao;
	}
	
	@DisplayName("Dados de combust�vel")
	public Boolean getDadoscombustivel() {
		return dadoscombustivel;
	}
	

	@DisplayName("C�digo ANP")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigoanp")
	public Codigoanp getCodigoanp() {
		return codigoanp;
	}

	@DisplayName("CODIF")
	@MaxLength(21)
	public String getCodif() {
		return codif;
	}

	@DisplayName("Qtde. temp. ambiente")
	public Double getQtdetemperatura() {
		return qtdetemperatura;
	}
	
	@DisplayName("Percentual GLP")
	public Double getPercentualglp() {
		return percentualglp;
	}

	@DisplayName("Percentual GNN")
	public Double getPercentualgnn() {
		return percentualgnn;
	}

	@DisplayName("Percentual GNI")
	public Double getPercentualgni() {
		return percentualgni;
	}

	@DisplayName("Valor de Partida")
	public Double getValorpartida() {
		return valorpartida;
	}

	@DisplayName("UF de consumo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufconsumo")
	public Uf getUfconsumo() {
		return ufconsumo;
	}
	
	@DisplayName("Base de c�lculo em qtde.")
	public Double getQtdebccide() {
		return qtdebccide;
	}

	@DisplayName("Valor al�quota")
	public Double getValoraliqcide() {
		return valoraliqcide;
	}

	@DisplayName("Valor")
	public Double getValorcide() {
		return valorcide;
	}

	@DisplayName("Peso bruto")
	public Double getPesobruto() {
		return pesobruto;
	}
	
	@DisplayName("Peso l�quido")
	public Double getPesoliquido() {
		return pesoliquido;
	}
	
	@DisplayName("Valor Total dos Tributos")
	public Money getValortotaltributos() {
		return valortotaltributos;
	}
	
	public Money getValorImpostoFederal() {
		return valorImpostoFederal;
	}
	
	public Money getValorImpostoEstadual() {
		return valorImpostoEstadual;
	}
	
	public Money getValorImpostoMunicipal() {
		return valorImpostoMunicipal;
	}
	
	public Double getPercentualImpostoFederal() {
		return percentualImpostoFederal;
	}

	public Double getPercentualImpostoEstadual() {
		return percentualImpostoEstadual;
	}

	public Double getPercentualImpostoMunicipal() {
		return percentualImpostoMunicipal;
	}

	@MaxLength(15)
	@DisplayName("N�mero do Pedido de Compra")
	public String getNumeropedidocompra() {
		return numeropedidocompra;
	}

	@MaxLength(6)
	@DisplayName("Item do Pedido de Compra")
	public Integer getItempedidocompra() {
		return itempedidocompra;
	}
	
	@DisplayName("Identificador")
	@MaxLength(14)
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	
	@DisplayName("% da quantidade devolvida")
	public Double getPercentualdevolvido() {
		return percentualdevolvido;
	}
	
	@DisplayName("Valor do ipi devolvido")
	public Money getValoripidevolvido() {
		return valoripidevolvido;
	}
	
	@DisplayName("Valor das dedu��es")
	public Money getValordeducaoservico() {
		return valordeducaoservico;
	}

	@DisplayName("Valor outras reten��es")
	public Money getValoroutrasretencoesservico() {
		return valoroutrasretencoesservico;
	}

	@DisplayName("Valor desconto condicionado")
	public Money getValordescontocondicionadoservico() {
		return valordescontocondicionadoservico;
	}
	
	@DisplayName("Valor desconto incondicionado")
	public Money getValordescontoincondicionadoservico() {
		return valordescontoincondicionadoservico;
	}

	@DisplayName("Valor reten��o iss")
	public Money getValorissretido() {
		return valorissretido;
	}

	@DisplayName("Exigibilidade do ISS")
	public ExigibilidadeIssNfe getExigibilidadeiss() {
		return exigibilidadeiss;
	}

	@MaxLength(20)
	@DisplayName("C�digo do servi�o")
	public String getCodigoservico() {
		return codigoservico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipioservico")
	@DisplayName("Munic�pio de ocorr�ncia")
	public Municipio getMunicipioservico() {
		return municipioservico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpaisservico")
	@DisplayName("Pa�s")
	public Pais getPaisservico() {
		return paisservico;
	}

	@MaxLength(30)
	@DisplayName("Processo judicial/administrativo")
	public String getProcessoservico() {
		return processoservico;
	}

	@DisplayName("Incentivo fiscal")
	public Boolean getIncentivofiscal() {
		return incentivofiscal;
	}
	
	@OneToMany(mappedBy="notafiscalprodutoitem")
	public List<Notafiscalprodutoitementregamaterial> getListaNotaprodutoitementregamaterial() {
		return listaNotaprodutoitementregamaterial;
	}
	
	@DisplayName("Dados de ICMS Interestadual")
	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}

	@DisplayName("Valor BC Destinat�rio")
	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}
	
	@DisplayName("Valor BC FCP Destinat�rio")
	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}

	@DisplayName("% FCP Destinat�rio")
	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}

	@DisplayName("% ICMS Destinat�rio")
	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}

	@DisplayName("% ICMS Interestadual")
	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}

	@DisplayName("% ICMS Partilha")
	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}

	@DisplayName("Valor FCP Destinat�rio")
	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}

	@DisplayName("Valor ICMS Destinat�rio")
	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}

	@DisplayName("Valor ICMS Remetente")
	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}
	
	@DisplayName("BC ICMS ST retido na UF remetente")
	public Money getValorbcicmsstremetente() {
		return valorbcicmsstremetente;
	}

	@DisplayName("ICMS ST retido na UF remetente")
	public Money getValoricmsstremetente() {
		return valoricmsstremetente;
	}

	@DisplayName("BC ICMS ST da UF destino")
	public Money getValorbcicmsstdestinatario() {
		return valorbcicmsstdestinatario;
	}

	@DisplayName("ICMS ST da UF destino")
	public Money getValoricmsstdestinatario() {
		return valoricmsstdestinatario;
	}
	
	@DisplayName("Valor da base de c�lculo do FCP")
	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	@DisplayName("% do FCP")
	public Double getFcp() {
		return fcp;
	}

	@DisplayName("Valor do FCP")
	public Money getValorfcp() {
		return valorfcp;
	}

	@DisplayName("Valor da base de c�lculo do FCP ST")
	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	@DisplayName("% do FCP ST")
	public Double getFcpst() {
		return fcpst;
	}

	@DisplayName("Valor do FCP ST")
	public Money getValorfcpst() {
		return valorfcpst;
	}
	
	@DisplayName("Al�quota ICMS Efetiva")
	public Double getIcmsEfetiva() {
		return icmsEfetiva;
	}
	
	@DisplayName("Redu��o ICMS Efetiva")
	public Double getRedBaseCalcEfetiva() {
		return redBaseCalcEfetiva;
	}
	
	@DisplayName("Base de C�lculo ICMS Efetiva")
	public Money getValorBaseCalcEfetiva() {
		return valorBaseCalcEfetiva;
	}
	
	@DisplayName("Valor ICMS Efetiva")
	public Money getValorIcmsEfetiva() {
		return valorIcmsEfetiva;
	}
	
	@DisplayName("Valor ICMS Pr�prio do Substituto")
	public Money getValorIcmsPropSub() {
		return valorIcmsPropSub;
	}
	
	@DisplayName("N�mero da Ficha de Conte�do de Importa��o")
	@MaxLength(36)
	public String getNumeroFci() {
		return numeroFci;
	}
	
	@DisplayName("Valor de desonera��o")
	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	@DisplayName("% desonera��o do IMCS")
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	@DisplayName("Abater desonera��o")
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}

	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}

	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}
	
	public void setNumeroFci(String numeroFci) {
		this.numeroFci = numeroFci;
	}
	
	public void setIcmsEfetiva(Double icmsEfetiva) {
		this.icmsEfetiva = icmsEfetiva;
	}
	
	public void setRedBaseCalcEfetiva(Double redBaseCalcEfetiva) {
		this.redBaseCalcEfetiva = redBaseCalcEfetiva;
	}
	
	public void setValorBaseCalcEfetiva(Money valorBaseCalcEfetiva) {
		this.valorBaseCalcEfetiva = valorBaseCalcEfetiva;
	}
	
	public void setValorIcmsEfetiva(Money valorIcmsEfetiva) {
		this.valorIcmsEfetiva = valorIcmsEfetiva;
	}
	
	public void setValorIcmsPropSub(Money valorIcmsPropSub) {
		this.valorIcmsPropSub = valorIcmsPropSub;
	}
	
	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}

	public void setValorbcicmsstremetente(Money valorbcicmsstremetente) {
		this.valorbcicmsstremetente = valorbcicmsstremetente;
	}

	public void setValoricmsstremetente(Money valoricmsstremetente) {
		this.valoricmsstremetente = valoricmsstremetente;
	}

	public void setValorbcicmsstdestinatario(Money valorbcicmsstdestinatario) {
		this.valorbcicmsstdestinatario = valorbcicmsstdestinatario;
	}

	public void setValoricmsstdestinatario(Money valoricmsstdestinatario) {
		this.valoricmsstdestinatario = valoricmsstdestinatario;
	}

	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}

	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}

	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}

	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}

	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}

	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}

	public void setListaNotaprodutoitementregamaterial(
			List<Notafiscalprodutoitementregamaterial> listaNotaprodutoitementregamaterial) {
		this.listaNotaprodutoitementregamaterial = listaNotaprodutoitementregamaterial;
	}

	public void setValordeducaoservico(Money valordeducaoservico) {
		this.valordeducaoservico = valordeducaoservico;
	}

	public void setValoroutrasretencoesservico(Money valoroutrasretencoesservico) {
		this.valoroutrasretencoesservico = valoroutrasretencoesservico;
	}

	public void setValordescontocondicionadoservico(
			Money valordescontocondicionadoservico) {
		this.valordescontocondicionadoservico = valordescontocondicionadoservico;
	}

	public void setValordescontoincondicionadoservico(
			Money valordescontoincondicionadoservico) {
		this.valordescontoincondicionadoservico = valordescontoincondicionadoservico;
	}

	public void setValorissretido(Money valorissretido) {
		this.valorissretido = valorissretido;
	}

	public void setExigibilidadeiss(ExigibilidadeIssNfe exigibilidadeiss) {
		this.exigibilidadeiss = exigibilidadeiss;
	}

	public void setCodigoservico(String codigoservico) {
		this.codigoservico = codigoservico;
	}

	public void setMunicipioservico(Municipio municipioservico) {
		this.municipioservico = municipioservico;
	}

	public void setPaisservico(Pais paisservico) {
		this.paisservico = paisservico;
	}

	public void setProcessoservico(String processoservico) {
		this.processoservico = processoservico;
	}

	public void setIncentivofiscal(Boolean incentivofiscal) {
		this.incentivofiscal = incentivofiscal;
	}

	public void setPercentualdevolvido(Double percentualdevolvido) {
		this.percentualdevolvido = percentualdevolvido;
	}
	
	public void setValoripidevolvido(Money valoripidevolvido) {
		this.valoripidevolvido = valoripidevolvido;
	}
	
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}

	public void setNumeropedidocompra(String numeropedidocompra) {
		this.numeropedidocompra = numeropedidocompra;
	}

	public void setItempedidocompra(Integer itempedidocompra) {
		this.itempedidocompra = itempedidocompra;
	}

	public void setValortotaltributos(Money valortotaltributos) {
		this.valortotaltributos = valortotaltributos;
	}
	
	public void setValorImpostoFederal(Money valorImpostoFederal) {
		this.valorImpostoFederal = valorImpostoFederal;
	}
	
	public void setValorImpostoEstadual(Money valorImpostoEstadual) {
		this.valorImpostoEstadual = valorImpostoEstadual;
	}
	
	public void setValorImpostoMunicipal(Money valorImpostoMunicipal) {
		this.valorImpostoMunicipal = valorImpostoMunicipal;
	}
	
	public void setPercentualImpostoFederal(Double percentualImpostoFederal) {
		this.percentualImpostoFederal = percentualImpostoFederal;
	}

	public void setPercentualImpostoEstadual(Double percentualImpostoEstadual) {
		this.percentualImpostoEstadual = percentualImpostoEstadual;
	}

	public void setPercentualImpostoMunicipal(Double percentualImpostoMunicipal) {
		this.percentualImpostoMunicipal = percentualImpostoMunicipal;
	}

	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	
	public void setPesoliquido(Double pesoliquido) {
		this.pesoliquido = pesoliquido;
	}
	
	public void setDadosexportacao(Boolean dadosexportacao) {
		this.dadosexportacao = dadosexportacao;
	}

	public void setDrawbackexportacao(String drawbackexportacao) {
		this.drawbackexportacao = drawbackexportacao;
	}

	public void setNumregistroexportacao(String numregistroexportacao) {
		this.numregistroexportacao = numregistroexportacao;
	}

	public void setChaveacessoexportacao(String chaveacessoexportacao) {
		this.chaveacessoexportacao = chaveacessoexportacao;
	}

	public void setQuantidadeexportacao(Double quantidadeexportacao) {
		this.quantidadeexportacao = quantidadeexportacao;
	}

	public void setDadoscombustivel(Boolean dadoscombustivel) {
		this.dadoscombustivel = dadoscombustivel;
	}

	public void setCodigoanp(Codigoanp codigoanp) {
		this.codigoanp = codigoanp;
	}

	public void setCodif(String codif) {
		this.codif = codif;
	}

	public void setQtdetemperatura(Double qtdetemperatura) {
		this.qtdetemperatura = qtdetemperatura;
	}
	
	public void setPercentualglp(Double percentualglp) {
		this.percentualglp = percentualglp;
	}

	public void setPercentualgnn(Double percentualgnn) {
		this.percentualgnn = percentualgnn;
	}

	public void setPercentualgni(Double percentualgni) {
		this.percentualgni = percentualgni;
	}
	
	public void setValorpartida(Double valorpartida) {
		this.valorpartida = valorpartida;
	}

	public void setUfconsumo(Uf ufconsumo) {
		this.ufconsumo = ufconsumo;
	}
	
	public void setRecopi(String recopi) {
		this.recopi = recopi;
	}
	
	public void setCbenef(String cbenef) {
		this.cbenef = cbenef;
	}

	public void setQtdebccide(Double qtdebccide) {
		this.qtdebccide = qtdebccide;
	}

	public void setValoraliqcide(Double valoraliqcide) {
		this.valoraliqcide = valoraliqcide;
	}

	public void setValorcide(Double valorcide) {
		this.valorcide = valorcide;
	}
	
	public void setOrigemproduto(Origemproduto origemproduto) {
		this.origemproduto = origemproduto;
	}
	
	public void setDadosimportacao(Boolean dadosimportacao) {
		this.dadosimportacao = dadosimportacao;
	}
	
	public void setValorbcii(Money valorbcii) {
		this.valorbcii = valorbcii;
	}

	public void setDespesasaduaneiras(Money despesasaduaneiras) {
		this.despesasaduaneiras = despesasaduaneiras;
	}

	public void setValoriof(Money valoriof) {
		this.valoriof = valoriof;
	}

	public void setValorii(Money valorii) {
		this.valorii = valorii;
	}

	public void setListaNotafiscalprodutoitemdi(
			Set<Notafiscalprodutoitemdi> listaNotafiscalprodutoitemdi) {
		this.listaNotafiscalprodutoitemdi = listaNotafiscalprodutoitemdi;
	}
	
	public void setListaNotafiscalprodutoitemnotaentrada(
			Set<Notafiscalprodutoitemnotaentrada> listaNotafiscalprodutoitemnotaentrada) {
		this.listaNotafiscalprodutoitemnotaentrada = listaNotafiscalprodutoitemnotaentrada;
	}
	
	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}

	public void setIncluirissvalor(Boolean incluirissvalor) {
		this.incluirissvalor = incluirissvalor;
	}

	public void setIncluiricmsvalor(Boolean incluiricmsvalor) {
		this.incluiricmsvalor = incluiricmsvalor;
	}

	public void setIncluiripivalor(Boolean incluiripivalor) {
		this.incluiripivalor = incluiripivalor;
	}

	public void setIncluirpisvalor(Boolean incluirpisvalor) {
		this.incluirpisvalor = incluirpisvalor;
	}

	public void setIncluircofinsvalor(Boolean incluircofinsvalor) {
		this.incluircofinsvalor = incluircofinsvalor;
	}

	public void setAliquotacreditoicms(Double aliquotacreditoicms) {
		this.aliquotacreditoicms = aliquotacreditoicms;
	}

	public void setValorcreditoicms(Money valorcreditoicms) {
		this.valorcreditoicms = valorcreditoicms;
	}

	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}

	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}

	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}

	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}
	
	public void setCnpjprodutoripi(Cnpj cnpjprodutoripi) {
		this.cnpjprodutoripi = cnpjprodutoripi;
	}

	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}

	public void setReducaobcicms(Double reducaobcicms) {
		this.reducaobcicms = reducaobcicms;
	}

	public void setBcoperacaopropriaicms(Money bcoperacaopropriaicms) {
		this.bcoperacaopropriaicms = bcoperacaopropriaicms;
	}

	public void setUficmsst(Uf uficmsst) {
		this.uficmsst = uficmsst;
	}

	public void setMotivodesoneracaoicms(Motivodesoneracaoicms motivodesoneracaoicms) {
		this.motivodesoneracaoicms = motivodesoneracaoicms;
	}

	public void setTipocalculocofins(Tipocalculo tipocalculocofins) {
		this.tipocalculocofins = tipocalculocofins;
	}

	public void setAliquotareaiscofins(Money aliquotareaiscofins) {
		this.aliquotareaiscofins = aliquotareaiscofins;
	}

	public void setQtdevendidacofins(Double qtdevendidacofins) {
		this.qtdevendidacofins = qtdevendidacofins;
	}

	public void setTipocalculopis(Tipocalculo tipocalculopis) {
		this.tipocalculopis = tipocalculopis;
	}

	public void setAliquotareaispis(Money aliquotareaispis) {
		this.aliquotareaispis = aliquotareaispis;
	}

	public void setQtdevendidapis(Double qtdevendidapis) {
		this.qtdevendidapis = qtdevendidapis;
	}
	
	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	
	public void setQtdevendidaipi(Double qtdevendidaipi) {
		this.qtdevendidaipi = qtdevendidaipi;
	}

	public void setCodigoenquadramentoipi(String codigoenquadramentoipi) {
		this.codigoenquadramentoipi = codigoenquadramentoipi;
	}

	public void setTributadoicms(Boolean tributadoicms) {
		this.tributadoicms = tributadoicms;
	}
	
	public void setIncluirvalorprodutos(Boolean incluirvalorprodutos) {
		this.incluirvalorprodutos = incluirvalorprodutos;
	}

	public void setValorbruto(Money valorbruto) {
		this.valorbruto = valorbruto;
	}
	
	public void setExtipi(String extipi) {
		this.extipi = extipi;
	}

	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	
	public void setNve(String nve) {
		this.nve = nve;
	}

	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}
	public void setInfoadicional(String infoadicional) {
		this.infoadicional = infoadicional;
	}
	public void setValorbccsll(Money valorbccsll) {
		this.valorbccsll = valorbccsll;
	}
	public void setCsll(Double csll) {
		this.csll = csll;
	}
	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}
	public void setValorbcir(Money valorbcir) {
		this.valorbcir = valorbcir;
	}
	public void setIr(Double ir) {
		this.ir = ir;
	}
	public void setValorir(Money valorir) {
		this.valorir = valorir;
	}
	public void setValorbcinss(Money valorbcinss) {
		this.valorbcinss = valorbcinss;
	}
	public void setInss(Double inss) {
		this.inss = inss;
	}
	public void setValorinss(Money valorinss) {
		this.valorinss = valorinss;
	}
	public void setGtin(String gtin) {
		this.gtin = gtin;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}
	public void setIcms(Double icms) {
		this.icms = icms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public void setCodigoseloipi(String codigoseloipi) {
		this.codigoseloipi = codigoseloipi;
	}
	public void setQtdeseloipi(Integer qtdeseloipi) {
		this.qtdeseloipi = qtdeseloipi;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public void setValorbcipi(Money valorbcipi) {
		this.valorbcipi = valorbcipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}
	public void setValorbcpis(Money valorbcpis) {
		this.valorbcpis = valorbcpis;
	}
	public void setPis(Double pis) {
		this.pis = pis;
	}
	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}
	public void setValorbccofins(Money valorbccofins) {
		this.valorbccofins = valorbccofins;
	}
	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}
	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}
	public void setValorbciss(Money valorbciss) {
		this.valorbciss = valorbciss;
	}
	public void setIss(Double iss) {
		this.iss = iss;
	}
	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}
	public void setMunicipioiss(Municipio municipioiss) {
		this.municipioiss = municipioiss;
	}
	public void setTipotributacaoiss(Tipotributacaoiss tipotributacaoiss) {
		this.tipotributacaoiss = tipotributacaoiss;
	}
	public void setCdnotafiscalprodutoitem(Integer cdnotafiscalprodutoitem) {
		this.cdnotafiscalprodutoitem = cdnotafiscalprodutoitem;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	//Log
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}

	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}

	@Transient
	public String getCdNota() {
		return cdNota;
	}

	public void setCdNota(String cdNota) {
		this.cdNota = cdNota;
	}

	@Transient
	public Set<Materialnumeroserie> getListaMaterialnumeroserie() {
		return listaMaterialnumeroserie;
	}

	public void setListaMaterialnumeroserie(
			Set<Materialnumeroserie> listaMaterialnumeroserie) {
		this.listaMaterialnumeroserie = listaMaterialnumeroserie;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	@Transient
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}

	@Transient
	public Boolean getItemescrito() {
		return itemescrito;
	}
	public void setItemescrito(Boolean itemescrito) {
		this.itemescrito = itemescrito;
	}
	
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIcms() {
		return valorFiscalIcms;
	}
	
	@DisplayName("Valores Fiscais")
	public ValorFiscal getValorFiscalIpi() {
		return valorFiscalIpi;
	}
	
	public Money getValorComissao() {
		return valorComissao;
	}
	
	public void setValorComissao(Money valorComissao) {
		this.valorComissao = valorComissao;
	}

	public void setValorFiscalIcms(ValorFiscal valorFiscalIcms) {
		this.valorFiscalIcms = valorFiscalIcms;
	}

	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi) {
		this.valorFiscalIpi = valorFiscalIpi;
	}

//	@Transient
//	public Materialgrupotributacao getMaterialgrupotributacaotrans() {
//		return materialgrupotributacaotrans;
//	}
//	public void setMaterialgrupotributacaotrans(Materialgrupotributacao materialgrupotributacaotrans) {
//		this.materialgrupotributacaotrans = materialgrupotributacaotrans;
//	}
	
	@Transient
	public Grupotributacao getGrupotributacaotrans() {
		return grupotributacaotrans;
	}

	public void setGrupotributacaotrans(Grupotributacao grupotributacaotrans) {
		this.grupotributacaotrans = grupotributacaotrans;
	}

	@Transient
	public Unidademedida getUnidademedidaAntiga() {
		return unidademedidaAntiga;
	}
	
	public void setUnidademedidaAntiga(Unidademedida unidademedidaAntiga) {
		this.unidademedidaAntiga = unidademedidaAntiga;
	}
	
	@Transient
	public Money getValorBasecalculoC190(){
		return getValorBasecalculo(false);
	}
	
	@Transient
	public Money getValorBasecalculo(){
		return getValorBasecalculo(true);
	}
	
	@Transient
	public Money getValorBasecalculo(Boolean considerarValorII){
		Money basecalculo = this.getValorbruto() != null ? this.getValorbruto() : new Money();
		if(this.getValorfrete() != null){
			basecalculo = basecalculo.add(this.getValorfrete());
		}
		if(this.getValordesconto() != null){
			basecalculo = basecalculo.subtract(this.getValordesconto());
		}
		if(this.getValorseguro() != null){
			basecalculo = basecalculo.add(this.getValorseguro());
		}
		if(this.getOutrasdespesas() != null){
			basecalculo = basecalculo.add(this.getOutrasdespesas());
		}
		if(this.getTipocobrancaicms() != null && 
				!this.getTipocobrancaicms().equals(Tipocobrancaicms.COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO) &&
				!this.getTipocobrancaicms().equals(Tipocobrancaicms.ICMS_COBRADO_ANTERIORMENTE)){
			if (this.getValoricmsst() != null)
				basecalculo = basecalculo.add(this.getValoricmsst());
		}
		if (this.getValoripi() != null)
			basecalculo = basecalculo.add(this.getValoripi());
		
		if(this.getValorii() != null && considerarValorII != null && considerarValorII)
			basecalculo = basecalculo.add(this.getValorii());
		
		return basecalculo;
	}
	
	@Transient
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Transient
	public Endereco getEndereco() {
		return endereco;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@DisplayName("Grupo de Tributa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}	
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	@Transient
	public Notafiscalprodutoitem getClone()  {
		try {
			return (Notafiscalprodutoitem) super.clone();
		}catch(Exception e){
			return null;
		}
	}	
	
	@Transient
	public String getCstIcmsSPED(){
		String cstIcmsSPED = null;
		if(getTipocobrancaicms() != null){
			if(getMaterial() != null && getMaterial().getOrigemproduto() != null){
				cstIcmsSPED = getMaterial().getOrigemproduto().getValue().toString();
			}
			cstIcmsSPED = (cstIcmsSPED != null ? cstIcmsSPED : "") + getTipocobrancaicms().getCdnfe();
		}
		return cstIcmsSPED;
	}
	
	@Transient
	public String getCstCofinsSPED(){
		String cstCofinsSPED = null;
		if(getTipocobrancacofins() != null){
			if(getMaterial() != null && getMaterial().getOrigemproduto() != null){
				cstCofinsSPED = getMaterial().getOrigemproduto().getValue().toString();
			}
			cstCofinsSPED = (cstCofinsSPED != null ? cstCofinsSPED : "") + getTipocobrancacofins().getCdnfe();
		}
		return cstCofinsSPED;
	}
	
	@Transient
	public String getCstPisSPED(){
		String cstPisSPED = null;
		if(getTipocobrancapis() != null){
			if(getMaterial() != null && getMaterial().getOrigemproduto() != null){
				cstPisSPED = getMaterial().getOrigemproduto().getValue().toString();
			}
			cstPisSPED = (cstPisSPED != null ? cstPisSPED : "") + getTipocobrancapis().getCdnfe();
		}
		return cstPisSPED;
	}

	public Double getPercentualimpostoecf() {
		return percentualimpostoecf;
	}

	public void setPercentualimpostoecf(Double percentualimpostoecf) {
		this.percentualimpostoecf = percentualimpostoecf;
	}
	
	@Transient
	public Double getPercentualBcicms() {
		return percentualBcicms;
	}
	@Transient
	public Double getPercentualBcicmsEfetiva() {
		return percentualBcicmsEfetiva;
	}
	@Transient
	public Double getPercentualBcipi() {
		return percentualBcipi;
	}
	@Transient
	public Double getPercentualBcpis() {
		return percentualBcpis;
	}
	@Transient
	public Double getPercentualBccofins() {
		return percentualBccofins;
	}
	@Transient
	public Double getPercentualBciss() {
		return percentualBciss;
	}
	@Transient
	public Double getPercentualBccsll() {
		return percentualBccsll;
	}
	@Transient
	public Double getPercentualBcir() {
		return percentualBcir;
	}
	@Transient
	public Double getPercentualBcinss() {
		return percentualBcinss;
	}
	@Transient
	public Double getPercentualBcfcp() {
		return percentualBcfcp;
	}
	@Transient
	public Double getPercentualBcfcpst() {
		return percentualBcfcpst;
	}

	public void setPercentualBcicms(Double percentualBcicms) {
		this.percentualBcicms = percentualBcicms;
	}
	public void setPercentualBcicmsEfetiva(Double percentualBcicmsEfetiva) {
		this.percentualBcicmsEfetiva = percentualBcicmsEfetiva;
	}
	public void setPercentualBcipi(Double percentualBcipi) {
		this.percentualBcipi = percentualBcipi;
	}
	public void setPercentualBcpis(Double percentualBcpis) {
		this.percentualBcpis = percentualBcpis;
	}
	public void setPercentualBccofins(Double percentualBccofins) {
		this.percentualBccofins = percentualBccofins;
	}
	public void setPercentualBciss(Double percentualBciss) {
		this.percentualBciss = percentualBciss;
	}
	public void setPercentualBccsll(Double percentualBccsll) {
		this.percentualBccsll = percentualBccsll;
	}
	public void setPercentualBcir(Double percentualBcir) {
		this.percentualBcir = percentualBcir;
	}
	public void setPercentualBcinss(Double percentualBcinss) {
		this.percentualBcinss = percentualBcinss;
	}
	public void setPercentualBcfcp(Double percentualBcfcp) {
		this.percentualBcfcp = percentualBcfcp;
	}
	public void setPercentualBcfcpst(Double percentualBcfcpst) {
		this.percentualBcfcpst = percentualBcfcpst;
	}

	@Transient
	public Integer getCdcoletamaterial() {
		return cdcoletamaterial;
	}
	@Transient
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	@Transient
	public Integer getCdproducaoordemmaterial() {
		return cdproducaoordemmaterial;
	}

	public void setCdcoletamaterial(Integer cdcoletamaterial) {
		this.cdcoletamaterial = cdcoletamaterial;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	public void setCdproducaoordemmaterial(Integer cdproducaoordemmaterial) {
		this.cdproducaoordemmaterial = cdproducaoordemmaterial;
	}
	
	@Transient
	public Finalidadenfe getFinalidadenfe() {
		return finalidadenfe;
	}
	public void setFinalidadenfe(Finalidadenfe finalidadenfe) {
		this.finalidadenfe = finalidadenfe;
	}
	
	@Transient
	public Boolean getHaveNumeroSerie() {
		return haveNumeroSerie;
	}
	public void setHaveNumeroSerie(Boolean haveNumeroSerie) {
		this.haveNumeroSerie = haveNumeroSerie;
	}

	@Transient
	public Notafiscalprodutoitem getNotafiscalprodutoitemIndustrializacao() {
		return notafiscalprodutoitemIndustrializacao;
	}

	public void setNotafiscalprodutoitemIndustrializacao(Notafiscalprodutoitem notafiscalprodutoitemIndustrializacao) {
		this.notafiscalprodutoitemIndustrializacao = notafiscalprodutoitemIndustrializacao;
	}

	@Transient
	public Boolean getIsItemRetorno() {
		return isItemRetorno;
	}

	public void setIsItemRetorno(Boolean isItemRetorno) {
		this.isItemRetorno = isItemRetorno;
	}
	@Transient
	public String getIdentificadorItemRetorno() {
		return identificadorItemRetorno;
	}

	public void setIdentificadorItemRetorno(String identificadorItemRetorno) {
		this.identificadorItemRetorno = identificadorItemRetorno;
	}
	
	@Transient
	public String getNomenfdescricaonota() {
		String nomedescricaonota = "";
		if(vendamaterialmestre != null && StringUtils.isNotEmpty(vendamaterialmestre.getNomealternativo())){
			nomedescricaonota = vendamaterialmestre.getNomealternativo();
		} else if(material != null){
			if (pneu != null && pneu.getCdpneu() != null && BooleanUtils.isTrue(considerarIdPneu)) {
				nomedescricaonota = pneu.getCdpneu().toString();
				
				nomedescricaonota += " - ";
				
				if (material.getNomenf() != null && !"".equals(material.getNomenf())) {
					nomedescricaonota += material.getNomenf();
				} else {
					nomedescricaonota += material.getNome();
				}
				
				if(material.getAlturaLarguraComprimentoConcatenados() != null && !"".equals(material.getAlturaLarguraComprimentoConcatenados())){
					nomedescricaonota += " " +  material.getAlturaLarguraComprimentoConcatenados();
				}
			} else {
				nomedescricaonota = material.getNomenfdescricaonota();
			}
		}
		return nomedescricaonota;
	}

	@Transient
	public String getNumeroEntregadocumento() {
		return numeroEntregadocumento;
	}

	public void setNumeroEntregadocumento(String numeroEntregadocumento) {
		this.numeroEntregadocumento = numeroEntregadocumento;
	}

	@Transient
	public Boolean getNaocalcularipi() {
		return naocalcularipi;
	}

	public void setNaocalcularipi(Boolean naocalcularipi) {
		this.naocalcularipi = naocalcularipi;
	}

	@MaxLength(7)
	public String getCest() {
		return cest;
	}

	public void setCest(String cest) {
		this.cest = cest;
	}
	
	@MaxLength(50)
	@DisplayName("N�mero NF Entrada")
	@Transient
	public String getNumeroNFEntrada() {
		return numeroNFEntrada;
	}
	@DisplayName("Emiss�o")
	@Transient
	public Date getDtemissao() {
		return dtemissao;
	}
	
	@DisplayName("Fornecedor")
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setNumeroNFEntrada(String numeroNFEntrada) {
		this.numeroNFEntrada = numeroNFEntrada;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public List<Entregamaterial> getListaEntregamaterial() {
		return listaEntregamaterial;
	}
	
	public void setListaEntregamaterial(List<Entregamaterial> listaEntregamaterial) {
		this.listaEntregamaterial = listaEntregamaterial;
	}

	@Transient
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}

	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
	
	@Transient
	public Money getValorSemDesconto() {
		if(this.valorbruto != null){
			valorSemDesconto = this.valordesconto == null? this.valorbruto: this.valorbruto.subtract(this.valordesconto);
		}
		return valorSemDesconto;
	}
	public void setValorSemDesconto(Money valorSemDesconto) {
		this.valorSemDesconto = valorSemDesconto;
	}

	@Transient
	public InformacaoAdicionalProdutoBean getInformacaoAdicionalProdutoBean() {
		return informacaoAdicionalProdutoBean;
	}

	public void setInformacaoAdicionalProdutoBean(InformacaoAdicionalProdutoBean informacaoAdicionalProdutoBean) {
		this.informacaoAdicionalProdutoBean = informacaoAdicionalProdutoBean;
	}

	@Transient
	public String getInfoAdicionalItemAnterior() {
		return infoAdicionalItemAnterior;
	}

	public void setInfoAdicionalItemAnterior(String infoAdicionalItemAnterior) {
		this.infoAdicionalItemAnterior = infoAdicionalItemAnterior;
	}
	
	@Transient
	public List<Motivodevolucao> getListaMotivodevolucao() {
		return listaMotivodevolucao;
	}
	
	public void setListaMotivodevolucao(List<Motivodevolucao> listaMotivodevolucao) {
		this.listaMotivodevolucao = listaMotivodevolucao;
	}

	@Transient
	public Boolean getGerarE200() {
		return gerarE200;
	}

	public void setGerarE200(Boolean gerarE200) {
		this.gerarE200 = gerarE200;
	}
	
	@Transient
	public Boolean getConsiderarIdPneu() {
		return considerarIdPneu;
	}
	
	public void setConsiderarIdPneu(Boolean considerarIdPneu) {
		this.considerarIdPneu = considerarIdPneu;
	}
	
	@Transient
	public Boolean getNfe() {
		return nfe;
	}
	
	public void setNfe(Boolean nfe) {
		this.nfe = nfe;
	}
	
	@Transient
	public Cliente getCliente(){
		return cliente;
	}
	
	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@Override
	public Endereco getEnderecoCliente() {
		return enderecoCliente;
	}
	
	@Override
	public void setEnderecoCliente(Endereco enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}

	@Transient
	@Override
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}

	@Override
	public void setModeloDocumentoFiscalEnum(
			ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}

	@Transient
	@Override
	public Localdestinonfe getLocaldestinonfe() {
		return localdestinonfe;
	}

	@Override
	public void setLocaldestinonfe(Localdestinonfe localdestinonfe) {
		this.localdestinonfe = localdestinonfe;
	}
	
	@Transient
	@Override
	public Money getValordifal() {
		return valordifal;
	}
	@Override
	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}
	
	@Transient
	@Override
	public Double getDifal() {
		return difal;
	}
	@Override
	public void setDifal(Double difal) {
		this.difal = difal;
	}
	
	@Transient
	@Override
	public Boolean getCalcularFcp() {
		return true;
	}
	@Transient
	@Override
	public Boolean getCalcularIcms() {
		return true;
	}
	
	@Transient
	@Override
	public Boolean getCalcularIpi() {
		return true;
	}

	@Transient
	public Double getValorEntrada() {
		return ValorEntrada;
	}

	public void setValorEntrada(Double valorEntrada) {
		ValorEntrada = valorEntrada;
	}

	@Transient
	public Double getQtde_convertida() {
		return qtde_convertida;
	}

	public void setQtde_convertida(Double qtde_convertida) {
		this.qtde_convertida = qtde_convertida;
	}
	
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
}