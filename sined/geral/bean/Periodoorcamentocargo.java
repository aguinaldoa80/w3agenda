package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("Histograma")
@SequenceGenerator(name = "sq_periodoorcamentocargo", sequenceName = "sq_periodoorcamentocargo")
public class Periodoorcamentocargo {
	
	protected Integer cdperiodoorcamentocargo;
	protected Cargo cargo;
	protected Orcamento orcamento;
	protected Double cargohorasemanal;
	protected Double totalhoracalculada;
	
	protected List<Periodoorcamento> listaPeriodoorcamento = new ArrayList<Periodoorcamento>();
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_periodoorcamentocargo")
	public Integer getCdperiodoorcamentocargo() {
		return cdperiodoorcamentocargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}
	public Double getCargohorasemanal() {
		return cargohorasemanal;
	}
	public Double getTotalhoracalculada() {
		return totalhoracalculada;
	}
	
	public void setCargohorasemanal(Double cargohorasemanal) {
		this.cargohorasemanal = cargohorasemanal;
	}
	public void setCdperiodoorcamentocargo(Integer cdperiodoorcamentocargo) {
		this.cdperiodoorcamentocargo = cdperiodoorcamentocargo;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setTotalhoracalculada(Double totalhoracalculada) {
		this.totalhoracalculada = totalhoracalculada;
	}
	
	@OneToMany(mappedBy="periodoorcamentocargo")
	public List<Periodoorcamento> getListaPeriodoorcamento() {
		return listaPeriodoorcamento;
	}
	public void setListaPeriodoorcamento(
			List<Periodoorcamento> listaPeriodoorcamento) {
		this.listaPeriodoorcamento = listaPeriodoorcamento;
	}
	
	/*************
	 * Transientes
	 *************/
	
	@Transient
	public Double getTotalHoraEfetiva() {
		Double totalHomemEfetivo = 0.0;
		
		if (listaPeriodoorcamento != null) {
			for (Periodoorcamento periodoorcamento : listaPeriodoorcamento) {
				totalHomemEfetivo += periodoorcamento.getQtde();
			}
		}		
		return totalHomemEfetivo * (cargohorasemanal != null ? cargohorasemanal : 0);
	}
}
