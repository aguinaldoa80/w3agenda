package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class ColaboradorOcorrenciaTipo {

	protected Integer cdcolaboradorocorrenciatipo;
	protected String nome;
	
	@Id
	public Integer getCdcolaboradorocorrenciatipo() {
		return cdcolaboradorocorrenciatipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdcolaboradorocorrenciatipo(Integer cdcolaboradorocorrenciatipo) {
		this.cdcolaboradorocorrenciatipo = cdcolaboradorocorrenciatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
