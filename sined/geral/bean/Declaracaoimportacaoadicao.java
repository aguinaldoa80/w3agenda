package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_declaracaoimportacaoadicao",sequenceName="sq_declaracaoimportacaoadicao")
public class Declaracaoimportacaoadicao {
	
	protected Integer cddeclaracaoimportacaoadicao;
	protected Declaracaoimportacao declaracaoimportacao;
	protected Integer item;
	protected Integer numeroadicao;
	protected String codigofabricante;
	protected Money valordesconto;
	protected String numerodrawback;
	protected String numeropedidocompra;
	protected Integer itempedidocompra;
	
	@Id
	@GeneratedValue(generator="sq_declaracaoimportacaoadicao", strategy=GenerationType.AUTO)
	public Integer getCddeclaracaoimportacaoadicao() {
		return cddeclaracaoimportacaoadicao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cddeclaracaoimportacao")
	public Declaracaoimportacao getdeclaracaoimportacao() {
		return declaracaoimportacao;
	}

	@Required
	@DisplayName("Item (Sequencial)")
	@MaxLength(7)
	public Integer getItem() {
		return item;
	}

	@Required
	@DisplayName("N�mero")
	@MaxLength(7)
	public Integer getNumeroadicao() {
		return numeroadicao;
	}

	@Required
	@DisplayName("Cod. fabricante")
	public String getCodigofabricante() {
		return codigofabricante;
	}

	@DisplayName("Desconto")
	public Money getValordesconto() {
		return valordesconto;
	}

	@DisplayName("N� do pedido de compra")
	public String getNumeropedidocompra() {
		return numeropedidocompra;
	}

	@DisplayName("Item do pedido de compra")
	@MaxLength(7)
	public Integer getItempedidocompra() {
		return itempedidocompra;
	}
	
	@MaxLength(11)
	@DisplayName("N�mero de drawback")
	public String getNumerodrawback() {
		return numerodrawback;
	}
	
	public void setNumerodrawback(String numerodrawback) {
		this.numerodrawback = numerodrawback;
	}

	public void setCddeclaracaoimportacaoadicao(
			Integer cddeclaracaoimportacaoadicao) {
		this.cddeclaracaoimportacaoadicao = cddeclaracaoimportacaoadicao;
	}

	public void setDeclaracaoimportacao(
			Declaracaoimportacao declaracaoimportacao) {
		this.declaracaoimportacao = declaracaoimportacao;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public void setNumeroadicao(Integer numeroadicao) {
		this.numeroadicao = numeroadicao;
	}

	public void setCodigofabricante(String codigofabricante) {
		this.codigofabricante = codigofabricante;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setNumeropedidocompra(String numeropedidocompra) {
		this.numeropedidocompra = numeropedidocompra;
	}

	public void setItempedidocompra(Integer itempedidocompra) {
		this.itempedidocompra = itempedidocompra;
	}
	
		
}