package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_campoextrasegmento", sequenceName = "sq_campoextrasegmento")
public class CampoExtraSegmento {

	private Integer cdcampoextrasegmento;
	private Segmento segmento;
	private String nome;
	private String conteudoPadrao;
	private Boolean obrigatorio = Boolean.FALSE;
	private Integer ordem;

	@Id
	@GeneratedValue(generator="sq_campoextrasegmento",strategy=GenerationType.AUTO)
	public Integer getCdcampoextrasegmento() {
		return cdcampoextrasegmento;
	}

	public void setCdcampoextrasegmento(Integer cdcampoextrasegmento) {
		this.cdcampoextrasegmento = cdcampoextrasegmento;
	}

	@DisplayName("Segmento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsegmento")
	public Segmento getSegmento() {
		return segmento;
	}

	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@DisplayName("Conte�do Padr�o")
	@MaxLength(8000)
	public String getConteudoPadrao() {
		return conteudoPadrao;
	}

	public void setConteudoPadrao(String conteudoPadrao) {
		this.conteudoPadrao = conteudoPadrao;
	}

	public Boolean getObrigatorio() {
		return obrigatorio;
	}

	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
}
