package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_apontamentohoras", sequenceName="sq_apontamentohoras")
public class ApontamentoHoras implements Log{

	protected Integer cdapontamentohoras;
	protected Apontamento apontamento;
	protected Hora hrinicio;
	protected Hora hrfim;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_apontamentohoras", strategy=GenerationType.AUTO)
	public Integer getCdapontamentohoras() {
		return cdapontamentohoras;
	}
	@Required
	@JoinColumn(name="cdapontamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Apontamento getApontamento() {
		return apontamento;
	}
	@Required
	@DisplayName("In�cio")
	public Hora getHrinicio() {
		return hrinicio;
	}
	@Required
	@DisplayName("Fim")
	public Hora getHrfim() {
		return hrfim;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdapontamentohoras(Integer cdapontamentohoras) {
		this.cdapontamentohoras = cdapontamentohoras;
	}
	public void setApontamento(Apontamento apontamento) {
		this.apontamento = apontamento;
	}
	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}
	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
