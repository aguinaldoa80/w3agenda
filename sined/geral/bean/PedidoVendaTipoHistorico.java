package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.geral.bean.enumeration.PedidoVendaTipoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_pedidovendatipohistorico", sequenceName = "sq_pedidovendatipohistorico")
public class PedidoVendaTipoHistorico implements Log{

	private Integer cdPedidoVendaTipoHistorico;
	private Pedidovendatipo pedidovendatipo;
	private PedidoVendaTipoEnum pedidoVendaTipoAcao;
	private String observacao;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
/////////////////////////////////////////////////////////////////////|
//  INICIO DOS GET 											/////////|
/////////////////////////////////////////////////////////////////////|	
	public PedidoVendaTipoHistorico(){
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendatipohistorico")
	public Integer getCdPedidoVendaTipoHistorico() {
		return cdPedidoVendaTipoHistorico;
	}
	@DisplayName("Tipo de Pedido de Venda")
	@JoinColumn(name = "cdpedidovendatipo")
	@ManyToOne(fetch = FetchType.LAZY)
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	@DisplayName("A��o")
	public PedidoVendaTipoEnum getPedidoVendaTipoAcao() {
		return pedidoVendaTipoAcao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
/////////////////////////////////////////////////////////////////////|
//  INICIO DOS SET 											/////////|
/////////////////////////////////////////////////////////////////////|
	public void setCdPedidoVendaTipoHistorico(
			Integer cdPedidoVendaTipoHistorico) {
		this.cdPedidoVendaTipoHistorico = cdPedidoVendaTipoHistorico;
	}

	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	public void setPedidoVendaTipoAcao(PedidoVendaTipoEnum pedidoVendaTipoAcao) {
	 this.pedidoVendaTipoAcao = pedidoVendaTipoAcao;
 }
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	
}