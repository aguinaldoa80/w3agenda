package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_movimentacaocontabilorigem", sequenceName="sq_movimentacaocontabilorigem")
public class Movimentacaocontabilorigem {

	protected Integer cdmovimentacaocontabilorigem;
	protected Movimentacaocontabil movimentacaocontabil;
	protected Nota nota;
	protected Entregadocumento entregadocumento;
	protected Documento documento;
	protected Movimentacao movimentacao;
	protected Venda venda;
	protected Rateioitem rateioitem;
	protected Movimentacaoestoque movimentacaoestoque;
	protected Provisao provisao;
	protected Colaboradordespesa colaboradorDespesa;
	protected Colaboradordespesaitem colaboradorDespesaItem;
	protected EventoPagamento eventoPagamento;
	
	public Movimentacaocontabilorigem(){
	}
	
	public Movimentacaocontabilorigem(Nota nota){
		this.nota = nota;
	}
	
	public Movimentacaocontabilorigem(Entregadocumento entregadocumento){
		this.entregadocumento = entregadocumento;
	}
	
	public Movimentacaocontabilorigem(Documento documento){
		this.documento = documento;
	}
	
	public Movimentacaocontabilorigem(Movimentacao movimentacao){
		this.movimentacao = movimentacao;
	}
	
	public Movimentacaocontabilorigem(Venda venda){
		this.venda = venda;
	}
	
	public Movimentacaocontabilorigem(Rateioitem rateioitem) {
		this.rateioitem = rateioitem;
	}
	
	public Movimentacaocontabilorigem(Rateioitem rateioitem, Movimentacaoestoque movimentacaoestoque) {
		this.rateioitem = rateioitem;
		this.movimentacaoestoque = movimentacaoestoque;
	}
	
	public Movimentacaocontabilorigem(Movimentacaoestoque movimentacaoestoque) {
		this.movimentacaoestoque = movimentacaoestoque;
	}
	
	public Movimentacaocontabilorigem(Provisao provisao){
		this.provisao = provisao;
	}
	
	public Movimentacaocontabilorigem(Colaboradordespesa colaboradorDespesa){
		this.colaboradorDespesa = colaboradorDespesa;
	}
	
	public Movimentacaocontabilorigem(Colaboradordespesaitem colaboradorDespesaItem, EventoPagamento eventoPagamento){
		this.colaboradorDespesaItem = colaboradorDespesaItem;
		this.eventoPagamento = eventoPagamento;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_movimentacaocontabilorigem")
	public Integer getCdmovimentacaocontabilorigem() {
		return cdmovimentacaocontabilorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaocontabil")
	public Movimentacaocontabil getMovimentacaocontabil() {
		return movimentacaocontabil;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacao")
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateioitem")
	public Rateioitem getRateioitem() {
		return rateioitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoestoque")
	public Movimentacaoestoque getMovimentacaoestoque() {
		return movimentacaoestoque;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprovisao")
	public Provisao getProvisao() {
		return provisao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesa")
	public Colaboradordespesa getColaboradorDespesa() {
		return colaboradorDespesa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesaitem")
	public Colaboradordespesaitem getColaboradorDespesaItem() {
		return colaboradorDespesaItem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEventoPagamento() {
		return eventoPagamento;
	}

	public void setCdmovimentacaocontabilorigem(Integer cdmovimentacaocontabilorigem) {
		this.cdmovimentacaocontabilorigem = cdmovimentacaocontabilorigem;
	}
	public void setMovimentacaocontabil(Movimentacaocontabil movimentacaocontabil) {
		this.movimentacaocontabil = movimentacaocontabil;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setRateioitem(Rateioitem rateioitem) {
		this.rateioitem = rateioitem;
	}

	public void setMovimentacaoestoque(Movimentacaoestoque movimentacaoestoque) {
		this.movimentacaoestoque = movimentacaoestoque;
	}
	
	public void setProvisao(Provisao provisao) {
		this.provisao = provisao;
	}
	
	public void setColaboradorDespesa(Colaboradordespesa colaboradorDespesa) {
		this.colaboradorDespesa = colaboradorDespesa;
	}
	public void setColaboradorDespesaItem(
			Colaboradordespesaitem colaboradorDespesaItem) {
		this.colaboradorDespesaItem = colaboradorDespesaItem;
	}
	public void setEventoPagamento(EventoPagamento eventoPagamento) {
		this.eventoPagamento = eventoPagamento;
	}
}