package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_producaoagendatipo", sequenceName = "sq_producaoagendatipo")
@DisplayName("Tipo de Produ��o")
public class Producaoagendatipo {

	protected Integer cdproducaoagendatipo;
	protected String nome;
	protected Producaoetapa producaoetapa;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendatipo")
	public Integer getCdproducaoagendatipo() {
		return cdproducaoagendatipo;
	}
	@DescriptionProperty
	@MaxLength(30)
	public String getNome() {
		return nome;
	}
	@DisplayName("Ciclo de Produ��o Padr�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapa")
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}

	public void setCdproducaoagendatipo(Integer cdproducaoagendatipo) {
		this.cdproducaoagendatipo = cdproducaoagendatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	
//	LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Producaoagendatipo){
			return this.getCdproducaoagendatipo() != null && this.getCdproducaoagendatipo().equals(((Producaoagendatipo)obj).getCdproducaoagendatipo());
		} else
			return false;
	}
}
