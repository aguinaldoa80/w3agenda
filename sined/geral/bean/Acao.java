package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Modulo;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_acao", sequenceName = "sq_acao")
@DisplayName("A��o")
public class Acao implements Log{
	
	public static final String AUTORIZAR_REQUISICAOMATERIAL = "AUTORIZAR_REQUISICAOMATERIAL";
	public static final String AUTORIZAR_SOLICITACAOCOMPRA = "AUTORIZAR_SOLICITACAOCOMPRA";
	public static final String AUTORIZAR_ORDEMCOMPRA = "AUTORIZAR_ORDEMCOMPRA";	
	public static final String GERAR_COMPRA_REQUISICAOMATERIAL = "GERAR_COMPRA_REQUISICAOMATERIAL";
	public static final String GERARCOTACAO_SOLICITACAOCOMPRA = "GERARCOTACAO_SOLICITACAOCOMPRA";
	public static final String APROVAR_ORDEMCOMPRA = "APROVAR_ORDEMCOMPRA";
	public static final String ENVIAR_PEDIDO_ORDEMCOMPRA = "ENVIAR_PEDIDO_ORDEMCOMPRA";
	public static final String DEFINIR_LOTE_PEDIDO = "DEFINIR_LOTE_PEDIDO";
	public static final String APROVAR_VENDA = "APROVAR_VENDA";
	public static final String VENDA_GERAR_PRODUCAO = "VENDA_GERAR_PRODUCAO";
	public static final String GERAR_ORDEMCOMPRA_ACIMA_PREXOMAXIMO = "GERAR_ORDEMCOMPRA_ACIMA_PREXOMAXIMO";
	public static final String VISUALIZAR_CUSTO_MATERIAL = "VISUALIZAR_CUSTO_MATERIAL";
	public static final String VISUALIZAR_VALOR_CUSTO_ESTOQUE = "VISUALIZAR_VALOR_CUSTO_ESTOQUE";

	
	protected Integer cdacao;
	protected String descricao;
	protected String key;	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Modulo modulo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_acao")
	public Integer getCdacao() {
		return cdacao;
	}
		
	@MaxLength(100)
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}
	
	
	@MaxLength(50)
	@DisplayName("Chave")
	@Required
	public String getKey() {
		return key;
	}

	public void setCdacao(Integer cdacao) {
		this.cdacao = cdacao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setKey(String key) {
		this.key = key;
	}

	//log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdacao == null) ? 0 : cdacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acao other = (Acao) obj;
		if (cdacao == null) {
			if (other.cdacao != null)
				return false;
		} else if (!cdacao.equals(other.cdacao))
			return false;
		return true;
	}
	
	@Enumerated(EnumType.STRING)
	public Modulo getModulo() {
		return modulo;
	}
	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}
	
}
