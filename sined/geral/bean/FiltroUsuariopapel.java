package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_filtrousuariopapel", sequenceName="sq_filtrousuariopapel")
public class FiltroUsuariopapel {

	private Integer cdfiltrousuariopapel;
	private Filtro filtro;
	private Usuariopapel usuariopapel;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_filtrousuariopapel")
	public Integer getCdfiltrousuariopapel() {
		return cdfiltrousuariopapel;
	}
	
	@Required
	@ManyToOne
	@JoinColumn(name="cdfiltro")
	public Filtro getFiltro() {
		return filtro;
	}
	
	@Required
	@ManyToOne
	@JoinColumn(name="cdusuariopapel")
	@DescriptionProperty
	public Usuariopapel getUsuariopapel() {
		return usuariopapel;
	}
	
	public void setCdfiltrousuariopapel(Integer cdfiltrousuariopapel) {
		this.cdfiltrousuariopapel = cdfiltrousuariopapel;
	}
	public void setFiltro(Filtro filtro) {
		this.filtro = filtro;
	}
	public void setUsuariopapel(Usuariopapel usuariopapel) {
		this.usuariopapel = usuariopapel;
	}
	
	
}
