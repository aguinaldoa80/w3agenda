package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsTipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_ajusteicmstipo", sequenceName = "sq_ajusteicmstipo")
public class Ajusteicmstipo implements Log {

	protected Integer cdajusteicmstipo;
	protected String descricao;
	protected AjusteIcmsTipo apuracao;
	protected TipoAjuste tipoajuste;
	protected String codigo;
	protected Uf uf;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteicmstipo")
	public Integer getCdajusteicmstipo() {
		return cdajusteicmstipo;
	}

	@Required
	@MaxLength(200)
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Apura��o")
	public AjusteIcmsTipo getApuracao() {
		return apuracao;
	}

	@Required
	@DisplayName("Tipo de ajuste")
	@Type(type="br.com.linkcom.sined.util.neo.persistence.TipoAjusteType")
	public TipoAjuste getTipoajuste() {
		return tipoajuste;
	}

	@Required
	@MaxLength(4)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@Required
	@DisplayName("UF correspondente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setApuracao(AjusteIcmsTipo apuracao) {
		this.apuracao = apuracao;
	}

	public void setTipoajuste(TipoAjuste tipoajuste) {
		this.tipoajuste = tipoajuste;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setCdajusteicmstipo(Integer cdajusteicmstipo) {
		this.cdajusteicmstipo = cdajusteicmstipo;
	}
	
	@Transient
	public String getCodigoSPED(){
		return getCodigoSPED(null);
	}
	
	@Transient
	public String getCodigoSPED(String siglaUf){
		StringBuilder sb = new StringBuilder();
		if(siglaUf != null){
			sb.append(siglaUf);
		}else if(this.uf != null && this.uf.getSigla() != null){
			sb.append(this.uf.getSigla());
		}
		if(this.apuracao != null){
			sb.append(this.apuracao.getId());
		}
		if(this.tipoajuste != null){
			sb.append(this.tipoajuste.getId());
		}
		if(this.codigo != null){
			sb.append(this.codigo);
		}
		return sb.toString();
	}
	
}
