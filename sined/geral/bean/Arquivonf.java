package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivonfsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontigencia;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivonf", sequenceName = "sq_arquivonf")
public class Arquivonf implements Log {

	protected Integer cdarquivonf;
	protected Date dtenvio;
	protected Empresa empresa;
	protected Configuracaonfe configuracaonfe;
	protected String numerorecibo;
	protected String numeroprotocolo;
	protected Arquivonfsituacao arquivonfsituacao;
	protected Timestamp dtrecebimento;
	protected String numeroLote;
	
	// CAMPOS DE CONTIGENCIA
	protected Tipocontigencia tipocontigencia;
	protected Timestamp dtentradacontigencia;
	protected String justificativacontigencia;

	// FLAGS
	protected Boolean assinando;
	protected Boolean enviando;
	protected Boolean consultando;
	
	protected Boolean emitindo;
	protected Boolean consultandolote;
	
	// ARQUIVOS DE ENVIO
	protected Arquivo arquivoxml;
	protected Arquivo arquivoxmlassinado;
	protected Arquivo arquivoxmlconsultasituacao;
	protected Arquivo arquivoxmlconsultalote;
	
	// ARQUIVOS DE RETORNO
	protected Arquivo arquivoretornoenvio;
	protected Arquivo arquivoretornoconsulta;
	protected Arquivo arquivoretornoconsultasituacao;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<Arquivonfnota> listaArquivonfnota = new ListSet<Arquivonfnota>(Arquivonfnota.class);
	
	// TRANSIENTES
	protected List<Arquivonfnotaerro> listaArquivonfnotaerro;
	protected String flag;
	protected Arquivo arquivoatualizacao;
	protected String protocoloatualizacao;
	protected Arquivo arquivoatualizacaolote;
	protected String cdarquivonfnota;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivonf")
	public Integer getCdarquivonf() {
		return cdarquivonf;
	}
	
	@DisplayName("XML")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}

	@DisplayName("XML assinado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlassinado")
	public Arquivo getArquivoxmlassinado() {
		return arquivoxmlassinado;
	}

	@DisplayName("Data de envio")
	public Date getDtenvio() {
		return dtenvio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfe")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	@DisplayName("Retorno do envio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoenvio")
	public Arquivo getArquivoretornoenvio() {
		return arquivoretornoenvio;
	}

	@DisplayName("Retorno da consulta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoconsulta")
	public Arquivo getArquivoretornoconsulta() {
		return arquivoretornoconsulta;
	}
	
	@DisplayName("Retorno da consulta de situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoconsultasituacao")
	public Arquivo getArquivoretornoconsultasituacao() {
		return arquivoretornoconsultasituacao;
	}

	@DisplayName("N�mero do recibo")
	public String getNumerorecibo() {
		return numerorecibo;
	}

	@DisplayName("N�mero do protocolo")
	public String getNumeroprotocolo() {
		return numeroprotocolo;
	}
	
	@DisplayName("Situa��o")
	public Arquivonfsituacao getArquivonfsituacao() {
		return arquivonfsituacao;
	}
	
	public Boolean getAssinando() {
		return assinando;
	}
	
	public Boolean getEnviando() {
		return enviando;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("XML de consulta de situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlconsultasituacao")
	public Arquivo getArquivoxmlconsultasituacao() {
		return arquivoxmlconsultasituacao;
	}

	@DisplayName("XML de consulta do lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlconsultalote")
	public Arquivo getArquivoxmlconsultalote() {
		return arquivoxmlconsultalote;
	}
	
	public Boolean getConsultando() {
		return consultando;
	}
	
	@DisplayName("Data de recebimento")
	public Timestamp getDtrecebimento() {
		return dtrecebimento;
	}
	
	public Boolean getEmitindo() {
		return emitindo;
	}
	
	@DisplayName("Tipo de contig�ncia")
	public Tipocontigencia getTipocontigencia() {
		return tipocontigencia;
	}

	@DisplayName("Data de entrada em contig�ncia")
	public Timestamp getDtentradacontigencia() {
		return dtentradacontigencia;
	}

	@DisplayName("Justificativa de contig�ncia")
	public String getJustificativacontigencia() {
		return justificativacontigencia;
	}

	@DisplayName("Consultando")
	public Boolean getConsultandolote() {
		return consultandolote;
	}
	
	public void setConsultandolote(Boolean consultandolote) {
		this.consultandolote = consultandolote;
	}
	
	public String getNumeroLote() {
		return numeroLote;
	}
	
	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}

	public void setTipocontigencia(Tipocontigencia tipocontigencia) {
		this.tipocontigencia = tipocontigencia;
	}

	public void setDtentradacontigencia(Timestamp dtentradacontigencia) {
		this.dtentradacontigencia = dtentradacontigencia;
	}

	public void setJustificativacontigencia(String justificativacontigencia) {
		this.justificativacontigencia = justificativacontigencia;
	}

	public void setEmitindo(Boolean emitindo) {
		this.emitindo = emitindo;
	}
	
	public void setDtrecebimento(Timestamp dtrecebimento) {
		this.dtrecebimento = dtrecebimento;
	}
	
	public void setArquivoretornoconsultasituacao(
			Arquivo arquivoretornoconsultasituacao) {
		this.arquivoretornoconsultasituacao = arquivoretornoconsultasituacao;
	}
	
	public void setConsultando(Boolean consultando) {
		this.consultando = consultando;
	}

	public void setArquivoxmlconsultasituacao(Arquivo arquivoxmlconsultasituacao) {
		this.arquivoxmlconsultasituacao = arquivoxmlconsultasituacao;
	}

	public void setArquivoxmlconsultalote(Arquivo arquivoxmlconsultalote) {
		this.arquivoxmlconsultalote = arquivoxmlconsultalote;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setEnviando(Boolean enviando) {
		this.enviando = enviando;
	}
	
	public void setAssinando(Boolean assinando) {
		this.assinando = assinando;
	}
	
	public void setArquivonfsituacao(Arquivonfsituacao arquivonfsituacao) {
		this.arquivonfsituacao = arquivonfsituacao;
	}

	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}

	public void setArquivoxmlassinado(Arquivo arquivoxmlassinado) {
		this.arquivoxmlassinado = arquivoxmlassinado;
	}

	public void setDtenvio(Date dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public void setArquivoretornoenvio(Arquivo arquivoretornoenvio) {
		this.arquivoretornoenvio = arquivoretornoenvio;
	}

	public void setArquivoretornoconsulta(Arquivo arquivoretornoconsulta) {
		this.arquivoretornoconsulta = arquivoretornoconsulta;
	}

	public void setNumerorecibo(String numerorecibo) {
		this.numerorecibo = numerorecibo;
	}

	public void setNumeroprotocolo(String numeroprotocolo) {
		this.numeroprotocolo = numeroprotocolo;
	}

	public void setCdarquivonf(Integer cdarquivonf) {
		this.cdarquivonf = cdarquivonf;
	}
	
//	LISTAS
	
	@DisplayName("Nota(s) Fiscal(is)")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="arquivonf")
	public List<Arquivonfnota> getListaArquivonfnota() {
		return listaArquivonfnota;
	}
	
	public void setListaArquivonfnota(List<Arquivonfnota> listaArquivonfnota) {
		this.listaArquivonfnota = listaArquivonfnota;
	}
	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
//	TRANSIENTES
	
	@Transient
	public String getFlag() {
		return flag;
	}
	
	@Transient
	public Arquivo getArquivoatualizacao() {
		return arquivoatualizacao;
	}
	
	@Transient
	public Arquivo getArquivoatualizacaolote() {
		return arquivoatualizacaolote;
	}

	@Transient
	public List<Arquivonfnotaerro> getListaArquivonfnotaerro() {
		return listaArquivonfnotaerro;
	}
	
	@Transient
	public String getProtocoloatualizacao() {
		return protocoloatualizacao;
	}
	
	@Transient
	public String getCdarquivonfnota() {
		return cdarquivonfnota;
	}
	
	public void setCdarquivonfnota(String cdarquivonfnota) {
		this.cdarquivonfnota = cdarquivonfnota;
	}
	
	public void setProtocoloatualizacao(String protocoloatualizacao) {
		this.protocoloatualizacao = protocoloatualizacao;
	}
	
	public void setListaArquivonfnotaerro(
			List<Arquivonfnotaerro> listaArquivonfnotaerro) {
		this.listaArquivonfnotaerro = listaArquivonfnotaerro;
	}
	
	public void setArquivoatualizacao(Arquivo arquivoatualizacao) {
		this.arquivoatualizacao = arquivoatualizacao;
	}
	
	public void setArquivoatualizacaolote(Arquivo arquivoatualizacaolote) {
		this.arquivoatualizacaolote = arquivoatualizacaolote;
	}
	
	public void setFlag(String flag) {
		this.flag = flag;
	}
}
