package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessasegmento",sequenceName="sq_bancoconfiguracaoremessasegmento")
public class BancoConfiguracaoRemessaSegmento {

	private Integer cdbancoconfiguracaoremessasegmento;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private BancoConfiguracaoSegmento bancoConfiguracaoSegmento;
	private Integer ordem;
	private String condicao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessasegmento")
	public Integer getCdbancoconfiguracaoremessasegmento() {
		return cdbancoconfiguracaoremessasegmento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaosegmento")
	@Required
	public BancoConfiguracaoSegmento getBancoConfiguracaoSegmento() {
		return bancoConfiguracaoSegmento;
	}
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@DisplayName("Condi��o")
	public String getCondicao() {
		return condicao;
	}
	public void setCdbancoconfiguracaoremessasegmento(
			Integer cdbancoconfiguracaoremessasegmento) {
		this.cdbancoconfiguracaoremessasegmento = cdbancoconfiguracaoremessasegmento;
	}
	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	public void setBancoConfiguracaoSegmento(
			BancoConfiguracaoSegmento bancoConfiguracaoSegmento) {
		this.bancoConfiguracaoSegmento = bancoConfiguracaoSegmento;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setCondicao(String condicao) {
		this.condicao = condicao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdbancoconfiguracaoremessasegmento == null) ? 0
						: cdbancoconfiguracaoremessasegmento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BancoConfiguracaoRemessaSegmento other = (BancoConfiguracaoRemessaSegmento) obj;
		if (cdbancoconfiguracaoremessasegmento == null) {
			if (other.cdbancoconfiguracaoremessasegmento != null)
				return false;
		} else if (!cdbancoconfiguracaoremessasegmento
				.equals(other.cdbancoconfiguracaoremessasegmento))
			return false;
		return true;
	}
	
}
