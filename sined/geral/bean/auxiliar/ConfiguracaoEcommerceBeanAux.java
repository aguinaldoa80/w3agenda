package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.List;

import br.com.linkcom.sined.modulo.pub.controller.process.bean.GenericEnvioInformacaoBean;

public class ConfiguracaoEcommerceBeanAux extends GenericEnvioInformacaoBean {

	private Integer cdEmpresa;
	private Integer cdPrazoPagamento;
	private Integer cdLocalArmazenagem;
	private Integer cdMaterialTabelaPreco;
	private Integer cdPedidoVendaTipo;
	private Integer cdColaborador;
	private Integer cdEnderecoTipo;
	private List<ConfiguracaoEcommerceFormaPagamento> listaPagamentos;
	private List<ConfiguracaoEcommerceConta> listaContas;
	
	
	
	public Integer getCdEmpresa() {
		return cdEmpresa;
	}
	public void setCdEmpresa(Integer cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}
	public Integer getCdPrazoPagamento() {
		return cdPrazoPagamento;
	}
	public void setCdPrazoPagamento(Integer cdPrazoPagamento) {
		this.cdPrazoPagamento = cdPrazoPagamento;
	}
	public Integer getCdLocalArmazenagem() {
		return cdLocalArmazenagem;
	}
	public void setCdLocalArmazenagem(Integer cdLocalArmazenagem) {
		this.cdLocalArmazenagem = cdLocalArmazenagem;
	}
	public Integer getCdMaterialTabelaPreco() {
		return cdMaterialTabelaPreco;
	}
	public void setCdMaterialTabelaPreco(Integer cdMaterialTabelaPreco) {
		this.cdMaterialTabelaPreco = cdMaterialTabelaPreco;
	}
	public Integer getCdPedidoVendaTipo() {
		return cdPedidoVendaTipo;
	}
	public void setCdPedidoVendaTipo(Integer cdPedidoVendaTipo) {
		this.cdPedidoVendaTipo = cdPedidoVendaTipo;
	}
	public List<ConfiguracaoEcommerceFormaPagamento> getListaPagamentos() {
		return listaPagamentos;
	}
	public void setListaPagamentos(List<ConfiguracaoEcommerceFormaPagamento> listaPagamentos) {
		this.listaPagamentos = listaPagamentos;
	}
	public Integer getCdColaborador() {
		return cdColaborador;
	}
	public void setCdColaborador(Integer cdColaborador) {
		this.cdColaborador = cdColaborador;
	}
	public Integer getCdEnderecoTipo() {
		return cdEnderecoTipo;
	}
	public void setCdEnderecoTipo(Integer cdEnderecoTipo) {
		this.cdEnderecoTipo = cdEnderecoTipo;
	}
	public List<ConfiguracaoEcommerceConta> getListaContas() {
		return listaContas;
	}
	public void setListaContas(List<ConfiguracaoEcommerceConta> listaContas) {
		this.listaContas = listaContas;
	}
}
