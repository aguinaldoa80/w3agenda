package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculo;

@Entity
public class Aux_veiculo {

	protected Integer cdveiculo;
	protected SituacaoVeiculo situacao;
	
	@Id
	public Integer getCdveiculo() {
		return cdveiculo;
	}

	public SituacaoVeiculo getSituacao() {
		return situacao;
	}
	
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}
	
	public void setSituacao(SituacaoVeiculo situacao) {
		this.situacao = situacao;
	}
}
