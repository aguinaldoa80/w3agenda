package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;

@Entity
public class Aux_ordemcompra {

	protected Integer cdordemcompra;
	protected Situacaosuprimentos situacaosuprimentos;
	protected Integer entregapercentual;
	protected Double valor;
	
	@Id
	public Integer getCdordemcompra() {
		return cdordemcompra;
	}
	@Column(name="situacao")
	@DisplayName("Situa��o")
	public Situacaosuprimentos getSituacaosuprimentos() {
		return situacaosuprimentos;
	}
	@Column(name="entregapercentual")
	public Integer getEntregapercentual() {
		return entregapercentual;
	}
	@DisplayName("Valor")
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setCdordemcompra(Integer cdordemcompra) {
		this.cdordemcompra = cdordemcompra;
	}
	public void setSituacaosuprimentos(Situacaosuprimentos situacaosuprimentos) {
		this.situacaosuprimentos = situacaosuprimentos;
	}
	public void setEntregapercentual(Integer entregapercentual) {
		this.entregapercentual = entregapercentual;
	}
	
}
