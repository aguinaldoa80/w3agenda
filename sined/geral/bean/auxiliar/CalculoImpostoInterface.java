package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Motivodesoneracaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.enumeration.ValorFiscal;

public interface CalculoImpostoInterface {
	public Material getMaterial();
	public Grupotributacao getGrupotributacao();
	public Grupotributacao getGrupotributacaotrans();
	public Cfop getCfop();
	public void setMaterial(Material material);
	public void setGrupotributacao(Grupotributacao grupotributacao);
	public void setGrupotributacaotrans(Grupotributacao grupotributacao);
	public void setCfop(Cfop cfop);
	
	public Double getIcms();
	public Double getIpi();
	public Double getIcmsst();
	public Double getPis();
	public Double getCofins();
	public Double getIss();
	public Double getIr();
	public Double getInss();
	public Double getFcp();
	public Double getFcpst();
	public Double getCsll();
	public void setIcms(Double icms);
	public void setIpi(Double ipi);
	public void setIcmsst(Double icmsst);
	public void setPis(Double pis);
	public void setCofins(Double cofins);
	public void setIss(Double iss);
	public void setIr(Double ir);
	public void setInss(Double inss);
	public void setFcp(Double fcp);
	public void setCsll(Double csll);
	public void setFcpst(Double fcpst);
	
	public Boolean getCalcularIcms();
	public Boolean getCalcularFcp();
	public Boolean getCalcularIpi();
	
	public Double getPercentualBcicms();
	public Double getPercentualBcipi();
	public Double getPercentualBcpis();
	public Double getPercentualBccofins();
	public Double getPercentualBciss();
	public Double getPercentualBcir();
	public Double getPercentualBcinss();
	public Double getPercentualBcfcp();
	public Double getPercentualBccsll();
	public void setPercentualBcicms(Double percentualBcicms);
	public void setPercentualBcipi(Double percentualBcipi);
	public void setPercentualBcpis(Double percentualBcpis);
	public void setPercentualBccofins(Double percentualBccofins);
	public void setPercentualBciss(Double percentualBciss);
	public void setPercentualBcir(Double percentualBcir);
	public void setPercentualBcinss(Double percentualBcinss);
	public void setPercentualBcfcp(Double percentualBcfcp);
	public void setPercentualBccsll(Double percentualBccsll);
	
	public Double getMargemvaloradicionalicmsst();
	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst);
	
	public Boolean getTributadoicms();
	public Origemproduto getOrigemproduto();
	public Tipotributacaoicms getTipotributacaoicms();
	public Tipocobrancaicms getTipocobrancaicms();
	public Modalidadebcicms getModalidadebcicms();
	public Modalidadebcicmsst getModalidadebcicmsst();
	public Double getReducaobcicms();
	public Double getReducaobcicmsst();
	public ValorFiscal getValorFiscalIcms();
	public Double getAliquotacreditoicms();
	public Boolean getIncluiricmsvalor();
	public String getCest();
	public Money getBcoperacaopropriaicms();
	public Uf getUficmsst();
	public Motivodesoneracaoicms getMotivodesoneracaoicms();
	public Double getPercentualdesoneracaoicms();
	public Money getValordesoneracaoicms();
	public Boolean getAbaterdesoneracaoicms();
	public Itemlistaservico getItemlistaservico();
	public void setTributadoicms(Boolean tributadoicms);
	public void setOrigemproduto(Origemproduto origemprodutoicms);
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms);
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms);
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms);
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst);
	public void setReducaobcicms(Double reducaobcicms);
	public void setReducaobcicmsst(Double reducaobcicmsst);
	public void setValorFiscalIcms(ValorFiscal valorFiscal);
	public void setAliquotacreditoicms(Double aliquotacreditoicms);
	public void setIncluiricmsvalor(Boolean incluiricmsvalor);
	public void setCest(String cest);
	public void setBcoperacaopropriaicms(Money bcoperacaopropriaicms);
	public void setUficmsst(Uf uficmsst);
	public void setMotivodesoneracaoicms(Motivodesoneracaoicms motivodesoneracaoicms);
	public void setPercentualdesoneracaoicms(Double percentualrdesoneracaoicms);
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms);
	public void setValordesoneracaoicms(Money valordesoneracaoicms);
	public void setItemlistaservico(Itemlistaservico itemlistaservico);
	
	public Boolean getIncluiripivalor();
	public Tipocobrancaipi getTipocobrancaipi();
	public Tipocalculo getTipocalculoipi();
	public Money getAliquotareaisipi();			
	public String getCodigoenquadramentoipi();
	public ValorFiscal getValorFiscalIpi();
	public void setIncluiripivalor(Boolean incluiripivalor);
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi);
	public void setTipocalculoipi(Tipocalculo tipocalculoipi);
	public void setAliquotareaisipi(Money aliquotareaisipi);			
	public void setCodigoenquadramentoipi(String codigoenquadramentoipi);
	public void setValorFiscalIpi(ValorFiscal valorFiscalIpi);
	
	public Boolean getIncluirpisvalor();
	public Tipocobrancapis getTipocobrancapis();
	public Tipocalculo getTipocalculopis();
	public Money getAliquotareaispis();
	public void setIncluirpisvalor(Boolean incluirpisvalor);
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis);
	public void setTipocalculopis(Tipocalculo tipocalculopis);
	public void setAliquotareaispis(Money aliquotareaispi);
	
	public Boolean getIncluircofinsvalor();
	public Tipocobrancacofins getTipocobrancacofins();
	public Tipocalculo getTipocalculocofins();
	public Money getAliquotareaiscofins();
	public void setIncluircofinsvalor(Boolean incluircofinsvalor);
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins);
	public void setTipocalculocofins(Tipocalculo tipocalculocofins);
	public void setAliquotareaiscofins(Money aliquotareaiscofins);
	
	public Boolean getIncluirissvalor();					
	public Tipotributacaoiss getTipotributacaoiss();
	public Boolean getIncentivofiscal();
	public void setIncluirissvalor(Boolean incluirissvalor);					
	public void setTipotributacaoiss(Tipotributacaoiss tipotributacaoiss);
	public void setIncentivofiscal(Boolean incentivofiscal);
	
	public Double getValorunitario();
	public Double getQtde();
	public Money getValordesconto();
	public Money getValorfrete();
	public Money getValorseguro();
	public Money getOutrasdespesas();
	public void setValordesconto(Money valordesconto);
	public void setValorfrete(Money valorfrete);
	public void setValorseguro(Money valorseguro);
	public void setOutrasdespesas(Money outrasdespesas);
	
	

	public void setValorbcicms(Money valorbcicms);
	public void setValoricms(Money valoricms);
	public void setValorbciss(Money valorbciss);
	public void setValoriss(Money valorbciss);
	public void setNaocalcularipi(Boolean naocalcularipi);
	public void setQtdevendidaipi(Double qtdevendidaipi);
	public void setValorbcpis(Money valorbcpis);
	public void setValoripi(Money valoripi);
	public void setValorpis(Money valorpis);
	public void setValorbcipi(Money valorbcipi);
	public void setValorbccofins(Money valorbccofins);
	public void setValorcofins(Money valorcofins);
	public void setValorfcp(Money valorfcp);
	public void setValorfcpst(Money valorfcpst);
	public void setValorbcicmsst(Money valorbcicmsst);
	public void setValoricmsst(Money valoricmsst);
	public void setValorcreditoicms(Money valorcreditoicms);
	public void setQtdevendidapis(Double qtdevendidapis);
	public void setQtdevendidacofins(Double qtdevendidacofins);
	public void setValorbruto(Money valorbruto);
	public void setValorbcfcp(Money valorbcfcp);
	public void setValorbcfcpst(Money valorbcfcpst);
	
	
	public Money getValorbccofins();
	public Money getValoriss();
	public Money getValoricms();
	public Money getValorpis();
	public Money getValorcofins();
	public Money getValorbruto();
	public Money getValorbciss();
	public Boolean getNaocalcularipi();
	public Money getValorbcipi();
	public Money getValorbcpis();
	public Money getValoripi();
	public Money getValorbcfcp();
	public Money getValorbcicmsst();
	public Money getValoricmsst();
	public Money getValorbcicms();
	public Money getValorfcp();
	public Money getValorfcpst();
	public Money getValorbcfcpst();
	
	public Double getQtdevendidapis();
	public Double getQtdevendidacofins();
	public Double getQtdevendidaipi();
	
	public Operacaonfe getOperacaonfe();
	public Money getValorcreditoicms();
	
}
