package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;

@Entity
public class Aux_entrega {

	protected Integer cdentrega;
	protected Situacaosuprimentos situacaosuprimentos;
	
	@Id
	public Integer getCdentrega() {
		return cdentrega;
	}
	
	@Column(name="situacao")
	public Situacaosuprimentos getSituacaosuprimentos() {
		return situacaosuprimentos;
	}
	
	public void setSituacaosuprimentos(Situacaosuprimentos situacaosuprimentos) {
		this.situacaosuprimentos = situacaosuprimentos;
	}
	
	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}
	
	
}
