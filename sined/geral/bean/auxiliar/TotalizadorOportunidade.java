package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;

public class TotalizadorOportunidade {
	
	private String nome;
	private Money valor;	
	private Integer qtde;
	
	public TotalizadorOportunidade(){}

	public TotalizadorOportunidade(String nome, Double valor, Integer qtde) {
		this.nome = nome;
		this.valor = valor != null && valor > 0 ? new Money(valor/100) : new Money();
		this.qtde = qtde;
	}

	public String getNome() {
		return nome;
	}
	public Money getValor() {
		return valor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

	public Integer getQtde() {
		return qtde;
	}

	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}	
	
	

}
