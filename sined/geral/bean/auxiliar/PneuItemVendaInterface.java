package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public interface PneuItemVendaInterface {

	public Integer getId();
	public Material getMaterial();
	public Unidademedida getUnidademedida();
	public Double getQuantidade();
	public Double getPreco();
	public Money getDesconto();
	public Double getPercentualdesconto();
	public Money getTotalprodutoOtr();
	public Integer getIndice();
	public Date getDtprazoentrega();
	public String getDtprazoentregaStr();
	public Pneu getPneu();
	public Boolean getAgendaProducaoGerada();
	public Integer getOrdem();
	public String getLinkGarantiareformaVenda();
	public Garantiareformaitem getGarantiareformaitem();
	
	public void setMaterial(Material material);
	public void setUnidademedida(Unidademedida unidademedida);
	public void setQuantidade(Double quantidade);
	public void setPreco(Double preco);
	public void setDesconto(Money desconto);
	public void setPercentualdesconto(Double percentualdesconto);
	public void setTotalprodutoOtr(Money totalproduto);
	public void setId(Integer id);
	public void setIndice(Integer index);
	public void setDtprazoentrega(Date dtprazoentrega);
	public void setDtprazoentregaStr(String dtprazoentregaStr);
	public void setPneu(Pneu pneu);
	public void setAgendaProducaoGerada(Boolean agendaProducaoGerada);
	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem);
}
