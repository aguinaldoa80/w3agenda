package br.com.linkcom.sined.geral.bean.auxiliar;


public class ConfiguracaoEcommerceConta {

	private Integer idFormaPagamentoW3erp;
	private Integer idContaBancariaW3erp;
	private Integer codigoParceiro;
	
	
	public Integer getIdFormaPagamentoW3erp() {
		return idFormaPagamentoW3erp;
	}
	public void setIdFormaPagamentoW3erp(Integer idFormaPagamentoW3erp) {
		this.idFormaPagamentoW3erp = idFormaPagamentoW3erp;
	}
	public Integer getIdContaBancariaW3erp() {
		return idContaBancariaW3erp;
	}
	public void setIdContaBancariaW3erp(Integer idContaBancariaW3erp) {
		this.idContaBancariaW3erp = idContaBancariaW3erp;
	}
	public Integer getCodigoParceiro() {
		return codigoParceiro;
	}
	public void setCodigoParceiro(Integer codigoParceiro) {
		this.codigoParceiro = codigoParceiro;
	}
}
