package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.sined.geral.bean.Loteestoque;
import br.com.linkcom.sined.geral.bean.Material;

public interface InclusaoLoteVendaInterface {

	public Material getMaterial();
	public Loteestoque getLoteestoque();
	public Double getQuantidade();
}
