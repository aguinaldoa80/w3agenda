package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;

@Entity
public class Aux_cotacao {

	protected Integer cdcotacao;
	protected String fornecedores;
	protected Date dtlimite;
	protected Situacaosuprimentos situacaosuprimentos;
	
	@Id
	public Integer getCdcotacao() {
		return cdcotacao;
	}
	
	@DisplayName("Fornecedor (% cotado)")
	public String getFornecedores() {
		return fornecedores;
	}
	
	public Date getDtlimite() {
		return dtlimite;
	}
	
	@Column(name="situacao")
	public Situacaosuprimentos getSituacaosuprimentos() {
		return situacaosuprimentos;
	}
	
	public void setSituacaosuprimentos(Situacaosuprimentos situacaosuprimentos) {
		this.situacaosuprimentos = situacaosuprimentos;
	}
	
	public void setDtlimite(Date dtlimite) {
		this.dtlimite = dtlimite;
	}
	
	public void setFornecedores(String fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	public void setCdcotacao(Integer cdcotacao) {
		this.cdcotacao = cdcotacao;
	}
	
	/**
	 * Este m�todo � utilizado somente no report ele
	 * remove a tag <img> que � gravada no banco deixando somente a porcentagem
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	@Transient
	public String getFornecedoresForReport(){
		StringBuilder fornecedoresBuilder = new StringBuilder(this.fornecedores);
		String retorno = "";
		if(this.fornecedores != null && !this.fornecedores.equals("")){
			while(fornecedoresBuilder.indexOf("<img") != -1){
				String porcentegem = fornecedoresBuilder.substring(fornecedoresBuilder.indexOf("onmouseover=Tip(")+17, fornecedoresBuilder.indexOf("')/>"));
				fornecedoresBuilder.replace(fornecedoresBuilder.indexOf("<img"), fornecedoresBuilder.indexOf("/>")+2, "("+porcentegem+")");
			}
			String strRegEx = "<[^>]*>";
			retorno = fornecedoresBuilder.toString();
			retorno = retorno.replaceAll(strRegEx, "");
		}
		return retorno != null ? retorno : null;
	}
	
	@Transient
	public String getFornecedoresForCsv(){
		StringBuilder fornecedoresBuilder = new StringBuilder(this.fornecedores);
		String retorno = "";
		if(this.fornecedores != null && !this.fornecedores.equals("")){
			while(fornecedoresBuilder.indexOf("<img") != -1){
				String porcentegem = fornecedoresBuilder.substring(fornecedoresBuilder.indexOf("onmouseover=Tip(")+17, fornecedoresBuilder.indexOf("')/>"));
				fornecedoresBuilder.replace(fornecedoresBuilder.indexOf("<img"), fornecedoresBuilder.indexOf("/>")+2, "("+porcentegem+")");
			}
			
			retorno = fornecedoresBuilder.toString().replaceAll("<BR>", " / ");
			if(retorno.endsWith(" / ")){
				retorno = retorno.substring(0 ,retorno.lastIndexOf(" / ")); 
			}
		}
		
		//remove todas as tags html
		String strRegEx = "<[^>]*>";
		retorno = retorno.replaceAll(strRegEx, "");
		
			
		return retorno;
	}
	
}
