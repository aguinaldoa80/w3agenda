package br.com.linkcom.sined.geral.bean.auxiliar;

import java.text.DecimalFormat;

import br.com.linkcom.utils.SIntegraUtil;

public class DominiointegracaoRegistro04 {

	public final static String IDENTIFICADOR = "04";
	
	private String identificador;
	private Integer sequencial;
	private Integer contadebito;
	private Integer contacredito;
	private Double valorlancamento;
	private String branco;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getIdentificador(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printInteger(getContadebito(), 7) +
			   SIntegraUtil.printInteger(getContacredito(), 7) +
			   SIntegraUtil.printDouble(getValorlancamento(), new DecimalFormat("#0.00"), 15) +
			   SIntegraUtil.printString(getBranco(), 100) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificador() {
		return identificador;
	}
	public Integer getSequencial() {
		return sequencial;
	}
	public Integer getContadebito() {
		return contadebito;
	}
	public Integer getContacredito() {
		return contacredito;
	}
	public Double getValorlancamento() {
		return valorlancamento;
	}
	public String getBranco() {
		return branco;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public void setContadebito(Integer contadebito) {
		this.contadebito = contadebito;
	}
	public void setContacredito(Integer contacredito) {
		this.contacredito = contacredito;
	}
	public void setValorlancamento(Double valorlancamento) {
		this.valorlancamento = valorlancamento;
	}
	public void setBranco(String branco) {
		this.branco = branco;
	}
}
