package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.List;

public class FormasPrazosPagamentoVO {
	
	private boolean mostrarAlerta;
	private List<DocumentotipoVO> listaDocumentotipo;
	private List<PrazopagamentoVO> listaPrazopagamento;
	private String formasPagamento;
	private String prazosPagamento;
	
	public FormasPrazosPagamentoVO(boolean mostrarAlerta, List<DocumentotipoVO> listaDocumentotipo, List<PrazopagamentoVO> listaPrazopagamento, String formasPagamento, String prazosPagamento){
		this.mostrarAlerta = mostrarAlerta;
		this.listaDocumentotipo = listaDocumentotipo;
		this.listaPrazopagamento = listaPrazopagamento;
		this.formasPagamento = formasPagamento;
		this.prazosPagamento = prazosPagamento;
	}
	
	public boolean isMostrarAlerta() {
		return mostrarAlerta;
	}
	public List<DocumentotipoVO> getListaDocumentotipo() {
		return listaDocumentotipo;
	}
	public List<PrazopagamentoVO> getListaPrazopagamento() {
		return listaPrazopagamento;
	}
	public String getFormasPagamento() {
		return formasPagamento;
	}
	public String getPrazosPagamento() {
		return prazosPagamento;
	}
	
	public void setMostrarAlerta(boolean mostrarAlerta) {
		this.mostrarAlerta = mostrarAlerta;
	}
	public void setListaDocumentotipo(List<DocumentotipoVO> listaDocumentotipo) {
		this.listaDocumentotipo = listaDocumentotipo;
	}
	public void setListaPrazopagamento(List<PrazopagamentoVO> listaPrazopagamento) {
		this.listaPrazopagamento = listaPrazopagamento;
	}
	public void setFormasPagamento(String formasPagamento) {
		this.formasPagamento = formasPagamento;
	}
	public void setPrazosPagamento(String prazosPagamento) {
		this.prazosPagamento = prazosPagamento;
	}
}