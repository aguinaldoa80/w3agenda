package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.sined.util.SinedDateUtils;

public class ColaboradorVO {

	private Long matricula;
	private List<PeriodoVO> listaPeriodo = new ArrayList<PeriodoVO>();
	
	public Long getMatricula() {
		return matricula;
	}
	
	public List<PeriodoVO> getListaPeriodo() {
		return listaPeriodo;
	}
	
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}	
	
	public void setListaPeriodo(List<PeriodoVO> listaPeriodo) {
		this.listaPeriodo = listaPeriodo;
	}
	
	public Hora getTotalHoras(){
		Double somatorio = 0D;
		if(!listaPeriodo.isEmpty()){
			for (PeriodoVO periodoVO : listaPeriodo) {
				if(periodoVO.getSaida()!=null && periodoVO.getEntrada()!=null)
					somatorio += SinedDateUtils.hoursToDouble(periodoVO.getSaida().toString()) - SinedDateUtils.hoursToDouble(periodoVO.getEntrada().toString());	
			}
		}
		return new Hora(SinedDateUtils.doubleToHours(somatorio));
	}	
	
	public Hora getTotalDia(Date data){
		if(!listaPeriodo.isEmpty() && data!=null){
			for (PeriodoVO periodoVO : listaPeriodo) {
				if(periodoVO.getData().equals(data) && periodoVO.getSaida()!=null && periodoVO.getSaida()!=null && periodoVO.getEntrada()!=null){					
					Double hora = SinedDateUtils.hoursToDouble(periodoVO.getSaida().toString()) - SinedDateUtils.hoursToDouble(periodoVO.getEntrada().toString());				
					return new Hora(SinedDateUtils.doubleToHours(hora));
				}
			}
		}
		return new Hora();
	}	
	
	public Hora getHoraEntradaByDia(Date data){
		if(!listaPeriodo.isEmpty() && data!=null){
			for (PeriodoVO periodoVO : listaPeriodo) {
				if(periodoVO.getData().equals(data)){				
					return periodoVO.getEntrada();
				}
			}
		}
		return null;		
	}
	
	public Hora getHoraSaidaByDia(Date data){
		if(!listaPeriodo.isEmpty() && data!=null){
			for (PeriodoVO periodoVO : listaPeriodo) {
				if(periodoVO.getData().equals(data)){				
					return periodoVO.getSaida();
				}
			}
		}
		return null;		
	}
	
	public Hora getTotalByEntradaSaida(PeriodoVO periodoVO){
		if(periodoVO.getSaida()!=null && periodoVO.getEntrada()!=null){					
			Double hora = SinedDateUtils.hoursToDouble(periodoVO.getSaida().toString()) - SinedDateUtils.hoursToDouble(periodoVO.getEntrada().toString());				
			return new Hora(SinedDateUtils.doubleToHours(hora));
		}
		
		return new Hora();
	}	
}
