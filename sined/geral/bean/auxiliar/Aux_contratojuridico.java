package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContratojuridico;

@Entity
public class Aux_contratojuridico {

	protected Integer cdcontratojuridico;
	protected SituacaoContratojuridico situacao;
	protected Date dtproximovencimento;
	
	@Id
	public Integer getCdcontratojuridico() {
		return cdcontratojuridico;
	}

	@DisplayName("Situa��o")
	public SituacaoContratojuridico getSituacao() {
		return situacao;
	}
	
	@DisplayName("Pr�ximo vencimento")
	public Date getDtproximovencimento() {
		return dtproximovencimento;
	}

	public void setDtproximovencimento(Date dtproximovencimento) {
		this.dtproximovencimento = dtproximovencimento;
	}

	public void setCdcontratojuridico(Integer cdcontratojuridico) {
		this.cdcontratojuridico = cdcontratojuridico;
	}
	
	public void setSituacao(SituacaoContratojuridico situacao) {
		this.situacao = situacao;
	}
}
