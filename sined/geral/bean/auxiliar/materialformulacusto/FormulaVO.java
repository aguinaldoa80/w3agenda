package br.com.linkcom.sined.geral.bean.auxiliar.materialformulacusto;

import java.util.ArrayList;
import java.util.List;

public class FormulaVO {

	private int cdmaterial;
	private double custooperacionaladministrativo; 
	private double custocomercialadministrativo;
	private MaterialVO aux_materialVO;
	private List<MaterialVO> listaMaterial = new ArrayList<MaterialVO>();
	
	public double getCustooperacionaladministrativo() {
		return custooperacionaladministrativo;
	}
	public double getCustocomercialadministrativo() {
		return custocomercialadministrativo;
	}
	public MaterialVO getAux_materialVO() {
		return aux_materialVO;
	}
	public List<MaterialVO> getListaMaterial() {
		return listaMaterial;
	}
	public int getCdmaterial() {
		return cdmaterial;
	}
	
	public void setAux_materialVO(MaterialVO aux_materialVO) {
		this.aux_materialVO = aux_materialVO;
	}
	public void setCdmaterial(int cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setCustooperacionaladministrativo(double custooperacionaladministrativo) {
		this.custooperacionaladministrativo = custooperacionaladministrativo;
	}
	public void setCustocomercialadministrativo(double custocomercialadministrativo) {
		this.custocomercialadministrativo = custocomercialadministrativo;
	}
	public void setListaMaterial(List<MaterialVO> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	
	public void addMaterial(MaterialVO materialVO){
		listaMaterial.add(materialVO);
	}
	
	public double rateiooperacional(){
		return aux_materialVO.getRateiooperacional();
	}
	
	public double rateiooperacional(Integer cdmaterial){
		if(cdmaterial == null){
			return aux_materialVO.getRateiooperacional();
		}
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial().equals(cdmaterial)){
					return materialVO.getRateiooperacional();
				}
			}
		}
		return 0.0;
	}
	
	public double valorcustomateriaprima(){
		return aux_materialVO.getValorcustomaterialprima();
	}
	
	public double valorcustomateriaprima(Integer cdmaterial){
		if(cdmaterial == null){
			return aux_materialVO.getValorcustomaterialprima();
		}
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial().equals(cdmaterial)){
					return materialVO.getValorcustomaterialprima();
				}
			}
		}
		return 0.0;
	}
}
