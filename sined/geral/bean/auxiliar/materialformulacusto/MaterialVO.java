package br.com.linkcom.sined.geral.bean.auxiliar.materialformulacusto;

import br.com.linkcom.sined.geral.bean.Material;

public class MaterialVO {

	private Integer cdmaterial;
	private double valorcustomaterialprima;
	private double valorcustooperacional;
	private double rateiooperacional;
	
	public MaterialVO(){}
	
	public MaterialVO(Material material, Double valorcustooperacional){
		this.valorcustooperacional = valorcustooperacional != null ? valorcustooperacional : 0d;
		if(material != null){
			this.valorcustomaterialprima = material.getValorcustomateriaprima() != null ? 
					material.getValorcustomateriaprima().getValue().doubleValue() : 0d;
			this.rateiooperacional = material.getMaterialgrupo() != null && material.getMaterialgrupo().getRateiocustoproducao() != null ? 
					material.getMaterialgrupo().getRateiocustoproducao() : 0d;
		}else {
			this.valorcustomaterialprima = 0d;
			this.rateiooperacional = 0d;
		}
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public double getValorcustomaterialprima() {
		return valorcustomaterialprima;
	}

	public double getValorcustooperacional() {
		return valorcustooperacional;
	}

	public double getRateiooperacional() {
		return rateiooperacional;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setValorcustomaterialprima(double valorcustomaterialprima) {
		this.valorcustomaterialprima = valorcustomaterialprima;
	}

	public void setValorcustooperacional(double valorcustooperacional) {
		this.valorcustooperacional = valorcustooperacional;
	}

	public void setRateiooperacional(double rateiooperacional) {
		this.rateiooperacional = rateiooperacional;
	}
}
