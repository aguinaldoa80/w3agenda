package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Transient;

public class MaterialunidademedidaVO {

	private Integer cdunidademedida;
	private String descricao;
	private Double total;
	private Double qtde;
	private Double qtdereferencia;
	private Double fatorconversao;
	private Double fracao;
	
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	public String getDescricao() {
		return descricao;
	}
	public Double getTotal() {
		return total;
	}
	public Double getQtde() {
		return qtde;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getFracao() {
		return fracao;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	
	@Transient
	public Double getFracaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fracao != null)
			valor = this.fracao / (this.qtdereferencia != null && this.qtdereferencia != 0 ? this.qtdereferencia : 1.0);
		return valor;
	}
}
