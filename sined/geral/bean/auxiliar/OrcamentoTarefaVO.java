package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Composicaomaoobra;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Indice;
import br.com.linkcom.sined.geral.bean.Orcamentorecursohumano;
import br.com.linkcom.sined.geral.bean.Recursocomposicao;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public class OrcamentoTarefaVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<Contagerencial> listaContagerencial;
	private List<Indice> listaIndice;
	private List<Area> listaArea;
	private List<MaterialVO> listaMaterialVO;
	private List<Cargo> listaCargo;
	private List<Unidademedida> listaUnidademedida;
	private Calendario calendario;
	private List<Recursocomposicao> listaRecursocomposicao;
	private List<Orcamentorecursohumano> listaOrcamentoRecursohumano;
	private List<Composicaomaoobra> listaComposicaomaoobra;
	
	public List<Contagerencial> getListaContagerencial() {
		return listaContagerencial;
	}
	public List<Indice> getListaIndice() {
		return listaIndice;
	}
	public List<Area> getListaArea() {
		return listaArea;
	}
	public List<MaterialVO> getListaMaterialVO() {
		return listaMaterialVO;
	}
	public List<Cargo> getListaCargo() {
		return listaCargo;
	}
	public List<Unidademedida> getListaUnidademedida() {
		return listaUnidademedida;
	}
	public Calendario getCalendario() {
		return calendario;
	}
	public List<Recursocomposicao> getListaRecursocomposicao() {
		return listaRecursocomposicao;
	}
	public List<Orcamentorecursohumano> getListaOrcamentoRecursohumano() {
		return listaOrcamentoRecursohumano;
	}
	public List<Composicaomaoobra> getListaComposicaomaoobra() {
		return listaComposicaomaoobra;
	}
	public void setListaComposicaomaoobra(
			List<Composicaomaoobra> listaComposicaomaoobra) {
		this.listaComposicaomaoobra = listaComposicaomaoobra;
	}
	public void setListaContagerencial(List<Contagerencial> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
	public void setListaIndice(List<Indice> listaIndice) {
		this.listaIndice = listaIndice;
	}
	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}
	public void setListaMaterialVO(List<MaterialVO> listaMaterialVO) {
		this.listaMaterialVO = listaMaterialVO;
	}
	public void setListaCargo(List<Cargo> listaCargo) {
		this.listaCargo = listaCargo;
	}
	public void setListaUnidademedida(List<Unidademedida> listaUnidademedida) {
		this.listaUnidademedida = listaUnidademedida;
	}
	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}
	public void setListaRecursocomposicao(
			List<Recursocomposicao> listaRecursocomposicao) {
		this.listaRecursocomposicao = listaRecursocomposicao;
	}
	public void setListaOrcamentoRecursohumano(
			List<Orcamentorecursohumano> listaOrcamentoRecursohumano) {
		this.listaOrcamentoRecursohumano = listaOrcamentoRecursohumano;
	}
}
