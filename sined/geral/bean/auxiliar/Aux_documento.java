package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;

@Entity
public class Aux_documento {
	
	private Integer cddocumento;
	private Money valoratual;
	private Money valorjuros;
	private Money valorjurosmes;
	private Money valormulta;
	private Money valordesconto;
	private Money valordesagio;
	private Money valortaxaboleto;
	private Money valortermo;
	private Money valortaxamovimento;
	private Money valortaxabaixa;
	private Money valortaxabaixaacrescimo;
	
	public Aux_documento() {
	}
	
	public Aux_documento(Money valoratual) {
		this.valoratual = valoratual;
	}
	@Id
	public Integer getCddocumento() {
		return cddocumento;
	}
	@DisplayName("Valor atual")
	public Money getValoratual() {
		return valoratual;
	}
	
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
	public void setValoratual(Money valoratual) {
		this.valoratual = valoratual;
	}

	public Money getValorjuros() {
		return valorjuros;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}
	
	public Money getValorjurosmes() {
		return valorjurosmes;
	}
	public void setValorjurosmes(Money valorjurosmes) {
		this.valorjurosmes = valorjurosmes;
	}

	public Money getValormulta() {
		return valormulta;
	}
	public void setValormulta(Money valormulta) {
		this.valormulta = valormulta;
	}

	public Money getValordesconto() {
		return valordesconto;
	}

	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}

	public Money getValordesagio() {
		return valordesagio;
	}

	public void setValordesagio(Money valordesagio) {
		this.valordesagio = valordesagio;
	}

	public Money getValortaxaboleto() {
		return valortaxaboleto;
	}

	public void setValortaxaboleto(Money valortaxaboleto) {
		this.valortaxaboleto = valortaxaboleto;
	}

	public Money getValortermo() {
		return valortermo;
	}

	public void setValortermo(Money valortermo) {
		this.valortermo = valortermo;
	}

	public Money getValortaxamovimento() {
		return valortaxamovimento;
	}

	public void setValortaxamovimento(Money valortaxamovimento) {
		this.valortaxamovimento = valortaxamovimento;
	}

	public Money getValortaxabaixa() {
		return valortaxabaixa;
	}

	public void setValortaxabaixa(Money valortaxabaixa) {
		this.valortaxabaixa = valortaxabaixa;
	}

	public Money getValortaxabaixaacrescimo() {
		return valortaxabaixaacrescimo;
	}

	public void setValortaxabaixaacrescimo(Money valortaxabaixaacrescimo) {
		this.valortaxabaixaacrescimo = valortaxabaixaacrescimo;
	}
	
	@Transient
	public Money getValorjurosOuJurosMes() {
		if(getValorjuros() != null && getValorjuros().getValue().doubleValue() >  0){
			return getValorjuros();
		}else{
			return getValorjurosmes();			
		}
	}
}