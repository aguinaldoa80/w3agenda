package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVeiculouso;

@Entity
public class Aux_veiculouso {

	protected Integer cdveiculouso;
	protected SituacaoVeiculouso situacao;
	
	@Id
	public Integer getCdveiculouso() {
		return cdveiculouso;
	}

	public SituacaoVeiculouso getSituacao() {
		return situacao;
	}
	
	public void setCdveiculouso(Integer cdveiculouso) {
		this.cdveiculouso = cdveiculouso;
	}
	
	public void setSituacao(SituacaoVeiculouso situacao) {
		this.situacao = situacao;
	}
}
