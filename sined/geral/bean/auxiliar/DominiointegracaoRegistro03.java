package br.com.linkcom.sined.geral.bean.auxiliar;

import java.text.DecimalFormat;

import br.com.linkcom.utils.SIntegraUtil;

public class DominiointegracaoRegistro03 {

	public final static String IDENTIFICADOR = "03";
	
	private String identificador;
	private Integer codigosequencial;
	private Integer contadebito;
	private Integer contacredito;
	private Double valorlancamento;
	private Integer codigohistorico;
	private String historico;
	private Integer codigofilialmatriz;
	private String branco;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getIdentificador(), 2) + 
			   SIntegraUtil.printInteger(getCodigosequencial(), 7) +
			   SIntegraUtil.printInteger(getContadebito(), 7) +
			   SIntegraUtil.printInteger(getContacredito(), 7) +
			   SIntegraUtil.printDouble(getValorlancamento(), new DecimalFormat("#0.00"), 15) +
			   SIntegraUtil.printInteger(getCodigohistorico(), 7) +
			   SIntegraUtil.printString(getHistorico(), 512) +
			   SIntegraUtil.printInteger(getCodigofilialmatriz(), 7) +
			   SIntegraUtil.printString(getBranco(), 100) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificador() {
		return identificador;
	}
	public Integer getCodigosequencial() {
		return codigosequencial;
	}
	public Integer getContadebito() {
		return contadebito;
	}
	public Integer getContacredito() {
		return contacredito;
	}
	public Double getValorlancamento() {
		return valorlancamento;
	}
	public Integer getCodigohistorico() {
		return codigohistorico;
	}
	public String getHistorico() {
		return historico;
	}
	public Integer getCodigofilialmatriz() {
		return codigofilialmatriz;
	}
	public String getBranco() {
		return branco;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setCodigosequencial(Integer codigosequencial) {
		this.codigosequencial = codigosequencial;
	}
	public void setContadebito(Integer contadebito) {
		this.contadebito = contadebito;
	}
	public void setContacredito(Integer contacredito) {
		this.contacredito = contacredito;
	}
	public void setValorlancamento(Double valorlancamento) {
		this.valorlancamento = valorlancamento;
	}
	public void setCodigohistorico(Integer codigohistorico) {
		this.codigohistorico = codigohistorico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setCodigofilialmatriz(Integer codigofilialmatriz) {
		this.codigofilialmatriz = codigofilialmatriz;
	}
	public void setBranco(String branco) {
		this.branco = branco;
	}
}
