package br.com.linkcom.sined.geral.bean.auxiliar;

import java.text.DecimalFormat;

import br.com.linkcom.utils.SIntegraUtil;

public class DominiointegracaoRegistro05 {

	public final static String IDENTIFICADOR = "05";
	
	private String identificador;
	private Integer sequencial;
	private Integer centrocustodebito;
	private Integer centrocustocredito;
	private Double valorlancamento;
	private String branco;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getIdentificador(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printInteger(getCentrocustodebito(), 7) +
			   SIntegraUtil.printInteger(getCentrocustocredito(), 7) +
			   SIntegraUtil.printDouble(getValorlancamento(), new DecimalFormat("#0.00"), 15) +
			   SIntegraUtil.printString(getBranco(), 100) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificador() {
		return identificador;
	}
	public Integer getSequencial() {
		return sequencial;
	}
	public Integer getCentrocustodebito() {
		return centrocustodebito;
	}
	public Integer getCentrocustocredito() {
		return centrocustocredito;
	}
	public Double getValorlancamento() {
		return valorlancamento;
	}
	public String getBranco() {
		return branco;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public void setCentrocustodebito(Integer centrocustodebito) {
		this.centrocustodebito = centrocustodebito;
	}
	public void setCentrocustocredito(Integer centrocustocredito) {
		this.centrocustocredito = centrocustocredito;
	}
	public void setValorlancamento(Double valorlancamento) {
		this.valorlancamento = valorlancamento;
	}
	public void setBranco(String branco) {
		this.branco = branco;
	}
}
