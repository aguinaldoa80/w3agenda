package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.PedidoVendaFornecedorTicketMedio;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Venda;
import br.com.linkcom.sined.geral.bean.VendaFornecedorTicketMedio;
import br.com.linkcom.sined.geral.bean.VendaOrcamentoFornecedorTicketMedio;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;

public class VendaFornecedorTicketMedioBean {

	private Fornecedor fornecedor;
	private Money valorTicketEsperado;
	private Money valorTicketAtingido;
	
	public VendaFornecedorTicketMedioBean(){
		
	}
	
	public VendaFornecedorTicketMedioBean(Fornecedor fornecedor, Money valorTicketEsperado, Money valorTicketAtingido){
		this.fornecedor = fornecedor;
		this.valorTicketEsperado = valorTicketEsperado;
		this.valorTicketAtingido = valorTicketAtingido;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getValorTicketEsperado() {
		return valorTicketEsperado;
	}
	public void setValorTicketEsperado(Money valorTicketEsperado) {
		this.valorTicketEsperado = valorTicketEsperado;
	}
	public Money getValorTicketAtingido() {
		return valorTicketAtingido;
	}
	public void setValorTicketAtingido(Money valorTicketAtingido) {
		this.valorTicketAtingido = valorTicketAtingido;
	}
	
	public VendaFornecedorTicketMedio translateForVenda(Venda venda){
		return new VendaFornecedorTicketMedio(venda, fornecedor, valorTicketEsperado, valorTicketAtingido);
	}
	
	public PedidoVendaFornecedorTicketMedio translateForPedidoVenda(Pedidovenda pedidoVenda){
		return new PedidoVendaFornecedorTicketMedio(pedidoVenda, fornecedor, valorTicketEsperado, valorTicketAtingido);
	}
	
	public VendaOrcamentoFornecedorTicketMedio translateForVendaOrcamento(Vendaorcamento vendaOrcamento){
		return new VendaOrcamentoFornecedorTicketMedio(vendaOrcamento, fornecedor, valorTicketEsperado, valorTicketAtingido);
	}
	
	public boolean isAtingiuTicketMedioEsperado(){
		return getValorTicketAtingido().doubleValue() >= getValorTicketEsperado().doubleValue();
	}
}
