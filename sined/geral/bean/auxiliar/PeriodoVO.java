package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.Date;

import br.com.linkcom.neo.types.Hora;

public class PeriodoVO {

	private Date data;
	private Hora entrada;
	private Hora saida;	
	private Boolean isProcessado;
	
	public Date getData() {
		return data;
	}
	public Hora getEntrada() {
		return entrada;
	}
	public Hora getSaida() {
		return saida;
	}	
	public Boolean getIsProcessado() {
		return isProcessado;
	}
	public void setIsProcessado(Boolean isProcessado) {
		this.isProcessado = isProcessado;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setEntrada(Hora entrada) {
		this.entrada = entrada;
	}
	public void setSaida(Hora saida) {
		this.saida = saida;
	}	
}
