package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;

public interface CalculoTicketMedioPorFornecedor {

	public Material getMaterial();
	public Money getTotal();
	public Double getQuantidade();
	public Double getPreco();
	public Money getDesconto();
	public Double getMultiplicador();
	public Money getOutrasdespesas();
	public Money getValorSeguro();
	public Money getTotalprodutoItemSemArredondamento();
	public void setTotal(Money total);
}
