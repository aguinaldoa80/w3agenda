package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;

public class GarantiareformaClienteBean {

	private Cliente cliente;
	private List<Garantiareformaitem> listaGarantias;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public List<Garantiareformaitem> getListaGarantias() {
		if(this.listaGarantias == null){
			this.setListaGarantias(new ArrayList<Garantiareformaitem>());
		}
		return listaGarantias;
	}
	public void setListaGarantias(List<Garantiareformaitem> listaGarantias) {
		this.listaGarantias = listaGarantias;
	}
	
}