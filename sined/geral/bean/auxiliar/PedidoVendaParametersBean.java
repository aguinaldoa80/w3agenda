package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Vendaorcamento;

public class PedidoVendaParametersBean {

	private Boolean coletaAutomatica;
	private Boolean emitirComprovante;
	private Boolean closeOnSave;
	private Boolean producaoAutomatica;
	private Cliente cliente;
	private String whereInOrdemservicoveterinaria;
	private Vendaorcamento vendaOrcamento;
	private Boolean sincronizarPedidovenda;
	private Boolean showCancelLink;
	private Boolean desfazerReserva;
	private String action;
	private Boolean copiar;
	private Boolean aprovarPedidovenda;
	private Boolean aprovarVenda;
	private Boolean addForma;
	private Boolean addPrazo;
	private Boolean fromPedidoVenda;
	private String largura;
	private String altura;
	private String comprimento;
	private String margemArredondamento;
	private String valorFrete;
	private Boolean origemVenda;
	private Boolean fromWebService;
	private Integer percentualPedidoVenda;
	private String whereInOportunidade;
	private Boolean gerarNotaAutomatico;
	private Integer cdVendaOrcamento;
	private Integer cdPedidoVenda;
	private Boolean somenteProduto;
	private Boolean somenteServico;
	private Integer cdCliente;
	private String selectedItens;
	private Boolean existeReserva;
	private Boolean existeGrade;
	private Boolean ignoreHackAndroidDownload;
	private String cdPedidoVendaImprimir;
	private Boolean bloquearIdentificador;
	
	
	public Boolean getColetaAutomatica() {
		if(coletaAutomatica == null){
			coletaAutomatica = false;
		}
		return coletaAutomatica;
	}
	public void setColetaAutomatica(Boolean coletaAutomatica) {
		this.coletaAutomatica = coletaAutomatica;
	}
	public Boolean getEmitirComprovante() {
		if(emitirComprovante == null){
			emitirComprovante = false;
		}
		return emitirComprovante;
	}
	public void setEmitirComprovante(Boolean emitirComprovante) {
		this.emitirComprovante = emitirComprovante;
	}
	public Boolean getCloseOnSave() {
		if(closeOnSave == null){
			closeOnSave = false;
		}
		return closeOnSave;
	}
	public void setCloseOnSave(Boolean closeOnSave) {
		this.closeOnSave = closeOnSave;
	}
	public Boolean getProducaoAutomatica() {
		if(producaoAutomatica == null){
			producaoAutomatica = false;
		}
		return producaoAutomatica;
	}
	public void setProducaoAutomatica(Boolean producaoAutomatica) {
		this.producaoAutomatica = producaoAutomatica;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getWhereInOrdemservicoveterinaria() {
		return whereInOrdemservicoveterinaria;
	}
	public void setWhereInOrdemservicoveterinaria(
			String whereInOrdemservicoveterinaria) {
		this.whereInOrdemservicoveterinaria = whereInOrdemservicoveterinaria;
	}
	public Vendaorcamento getVendaOrcamento() {
		return vendaOrcamento;
	}
	public void setVendaOrcamento(Vendaorcamento vendaOrcamento) {
		this.vendaOrcamento = vendaOrcamento;
	}
	public Boolean getSincronizarPedidovenda() {
		if(sincronizarPedidovenda == null){
			sincronizarPedidovenda = false;
		}
		return sincronizarPedidovenda;
	}
	public void setSincronizarPedidovenda(Boolean sincronizarPedidovenda) {
		this.sincronizarPedidovenda = sincronizarPedidovenda;
	}
	public Boolean getShowCancelLink() {
		if(showCancelLink == null){
			showCancelLink = false;
		}
		return showCancelLink;
	}
	public void setShowCancelLink(Boolean showCancelLink) {
		this.showCancelLink = showCancelLink;
	}
	public Boolean getDesfazerReserva() {
		if(desfazerReserva == null){
			desfazerReserva = false;
		}
		return desfazerReserva;
	}
	public void setDesfazerReserva(Boolean desfazerReserva) {
		this.desfazerReserva = desfazerReserva;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Boolean getCopiar() {
		if(copiar == null){
			copiar = false;
		}
		return copiar;
	}
	public void setCopiar(Boolean copiar) {
		this.copiar = copiar;
	}
	public Boolean getAprovarPedidovenda() {
		if(aprovarPedidovenda == null){
			aprovarPedidovenda = false;
		}
		return aprovarPedidovenda;
	}
	public void setAprovarPedidovenda(Boolean aprovarPedidovenda) {
		this.aprovarPedidovenda = aprovarPedidovenda;
	}
	public Boolean getAprovarVenda() {
		if(aprovarVenda == null){
			aprovarVenda = false;
		}
		return aprovarVenda;
	}
	public void setAprovarVenda(Boolean aprovarVenda) {
		this.aprovarVenda = aprovarVenda;
	}
	public Boolean getAddForma() {
		if(addForma == null){
			addForma = false;
		}
		return addForma;
	}
	public void setAddForma(Boolean addForma) {
		this.addForma = addForma;
	}
	public Boolean getAddPrazo() {
		if(addPrazo == null){
			addPrazo = false;
		}
		return addPrazo;
	}
	public void setAddPrazo(Boolean addPrazo) {
		this.addPrazo = addPrazo;
	}
	public Boolean getFromPedidoVenda() {
		if(fromPedidoVenda == null){
			fromPedidoVenda = false;
		}
		return fromPedidoVenda;
	}
	public void setFromPedidoVenda(Boolean fromPedidoVenda) {
		this.fromPedidoVenda = fromPedidoVenda;
	}
	public String getLargura() {
		return largura;
	}
	public void setLargura(String largura) {
		this.largura = largura;
	}
	public String getAltura() {
		return altura;
	}
	public void setAltura(String altura) {
		this.altura = altura;
	}
	public String getComprimento() {
		return comprimento;
	}
	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}
	public String getMargemArredondamento() {
		return margemArredondamento;
	}
	public void setMargemArredondamento(String margemarredondamento) {
		this.margemArredondamento = margemarredondamento;
	}
	public String getValorFrete() {
		return valorFrete;
	}
	public void setValorFrete(String valorfrete) {
		this.valorFrete = valorfrete;
	}
	public Boolean getOrigemVenda() {
		if(origemVenda == null){
			origemVenda = false;
		}
		return origemVenda;
	}
	public void setOrigemVenda(Boolean origemVenda) {
		this.origemVenda = origemVenda;
	}
	public Boolean getFromWebService() {
		if(fromWebService == null){
			fromWebService = false;
		}
		return fromWebService;
	}
	public void setFromWebService(Boolean fromWebService) {
		this.fromWebService = fromWebService;
	}
	public Integer getPercentualPedidoVenda() {
		return percentualPedidoVenda;
	}
	public void setPercentualPedidoVenda(Integer percentualPedidoVenda) {
		this.percentualPedidoVenda = percentualPedidoVenda;
	}
	public String getWhereInOportunidade() {
		return whereInOportunidade;
	}
	public void setWhereInOportunidade(String whereInOportunidade) {
		this.whereInOportunidade = whereInOportunidade;
	}
	public Boolean getGerarNotaAutomatico() {
		if(gerarNotaAutomatico == null){
			gerarNotaAutomatico = false;
		}
		return gerarNotaAutomatico;
	}
	public void setGerarNotaAutomatico(Boolean gerarNotaAutomatico) {
		this.gerarNotaAutomatico = gerarNotaAutomatico;
	}
	public Integer getCdVendaOrcamento() {
		return cdVendaOrcamento;
	}
	public void setCdVendaOrcamento(Integer cdVendaOrcamento) {
		this.cdVendaOrcamento = cdVendaOrcamento;
	}
	public Integer getCdPedidoVenda() {
		return cdPedidoVenda;
	}
	public void setCdPedidoVenda(Integer cdPedidoVenda) {
		this.cdPedidoVenda = cdPedidoVenda;
	}
	public Boolean getSomenteProduto() {
		if(somenteProduto == null){
			somenteProduto = false;
		}
		return somenteProduto;
	}
	public void setSomenteProduto(Boolean somenteProduto) {
		this.somenteProduto = somenteProduto;
	}
	public Boolean getSomenteServico() {
		if(somenteServico == null){
			somenteServico = false;
		}
		return somenteServico;
	}
	public void setSomenteServico(Boolean somenteServico) {
		this.somenteServico = somenteServico;
	}
	public Integer getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(Integer cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getSelectedItens() {
		return selectedItens;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
	public Boolean getExisteReserva() {
		if(existeReserva == null){
			existeReserva = false;
		}
		return existeReserva;
	}
	public void setExisteReserva(Boolean existeReserva) {
		this.existeReserva = existeReserva;
	}
	public Boolean getExisteGrade() {
		if(existeGrade == null){
			existeGrade = false;
		}
		return existeGrade;
	}
	public void setExisteGrade(Boolean existeGrade) {
		this.existeGrade = existeGrade;
	}
	public Boolean getIgnoreHackAndroidDownload() {
		if(ignoreHackAndroidDownload == null){
			ignoreHackAndroidDownload = false;
		}
		return ignoreHackAndroidDownload;
	}
	public void setIgnoreHackAndroidDownload(Boolean ignoreHackAndroidDownload) {
		this.ignoreHackAndroidDownload = ignoreHackAndroidDownload;
	}
	public String getCdPedidoVendaImprimir() {
		return cdPedidoVendaImprimir;
	}
	public void setCdPedidoVendaImprimir(String cdPedidoVendaImprimir) {
		this.cdPedidoVendaImprimir = cdPedidoVendaImprimir;
	}
	public Boolean getBloquearIdentificador() {
		return bloquearIdentificador;
	}
	public void setBloquearIdentificador(Boolean bloquearIdentificador) {
		this.bloquearIdentificador = bloquearIdentificador;
	}
}
