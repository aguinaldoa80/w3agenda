package br.com.linkcom.sined.geral.bean.auxiliar;

public class LoteestoqueVO {

	private Integer cdloteestoque;
	private String numerovalidadeqtde;
	private String numerovalidade;

	public LoteestoqueVO(){}
	
	public LoteestoqueVO(Integer cdloteestoque, String numerovalidade, String numerovalidadeqtde){
		this.cdloteestoque = cdloteestoque;
		this.numerovalidade = numerovalidade;
		this.numerovalidadeqtde = numerovalidadeqtde;
	}

	public Integer getCdloteestoque() {
		return cdloteestoque;
	}
	public String getNumerovalidadeqtde() {
		return numerovalidadeqtde;
	}
	public String getNumerovalidade() {
		return numerovalidade;
	}

	public void setCdloteestoque(Integer cdloteestoque) {
		this.cdloteestoque = cdloteestoque;
	}
	public void setNumerovalidadeqtde(String numerovalidadeqtde) {
		this.numerovalidadeqtde = numerovalidadeqtde;
	}
	public void setNumerovalidade(String numerovalidade) {
		this.numerovalidade = numerovalidade;
	}
}
