package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaocabecalho {

	public final static String CABECALHO = "01";
	public final static String VALORFIXON = "N";
	public final static String CONSTANTE = "00000";
	public final static String VALORFIXO18 = "18";
	public final static String VALORFIXO17 = "17";
	
	private String cabecalho;
	private Integer codigoempresa;
	private String cgcempresa;
	private Date datainicial;
	private Date datafinal;
	private String  valorfixoN;
	private Integer tiponota;
	private String constante;
	private Integer sistema;
	private String valorfixo;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getCabecalho(), 2) + 
			   SIntegraUtil.printInteger(getCodigoempresa(), 7) +
			   SIntegraUtil.printStringCpfCnpj(getCgcempresa(), 14) +
			   SIntegraUtil.printDateDominio(getDatainicial(), 10) +
			   SIntegraUtil.printDateDominio(getDatafinal(), 10) +
			   SIntegraUtil.printString(getValorfixoN(), 01) +
			   SIntegraUtil.printInteger(getTiponota(), 02) +
			   SIntegraUtil.printStringOrigemInteger(getConstante(), 05) +
			   SIntegraUtil.printInteger(getSistema(), 01) +
			   SIntegraUtil.printString(getValorfixo(), 02) + SIntegraUtil.SEPARADOR_LINHA;
	}
	
	public String getCabecalho() {
		return cabecalho;
	}
	public Integer getCodigoempresa() {
		return codigoempresa;
	}	
	public String getCgcempresa() {
		return cgcempresa;
	}
	public Date getDatainicial() {
		return datainicial;
	}
	public Date getDatafinal() {
		return datafinal;
	}
	public String getValorfixoN() {
		return valorfixoN;
	}
	public Integer getTiponota() {
		return tiponota;
	}
	public String getConstante() {
		return constante;
	}
	public Integer getSistema() {
		return sistema;
	}
	public String getValorfixo() {
		return valorfixo;
	}
	
	
	public void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}
	public void setCodigoempresa(Integer codigoempresa) {
		this.codigoempresa = codigoempresa;
	}
	public void setCgcempresa(String cgcempresa) {
		this.cgcempresa = cgcempresa;
	}
	public void setDatainicial(Date datainicial) {
		this.datainicial = datainicial;
	}
	public void setDatafinal(Date datafinal) {
		this.datafinal = datafinal;
	}
	public void setValorfixoN(String valorfixoN) {
		this.valorfixoN = valorfixoN;
	}
	public void setTiponota(Integer tiponota) {
		this.tiponota = tiponota;
	}
	public void setConstante(String constante) {
		this.constante = constante;
	}
	public void setSistema(Integer sistema) {
		this.sistema = sistema;
	}
	public void setValorfixo(String valorfixo) {
		this.valorfixo = valorfixo;
	}
}
