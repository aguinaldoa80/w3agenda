package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Aux_agendamento {
	
	private Integer cdagendamento;
	private Date dtUltimaConsolidacao;
	private Integer situacao;
	
	public Aux_agendamento() {
	}
	
	public Aux_agendamento(Date dtUltimaConsolidacao, Integer situacao) {
		this.dtUltimaConsolidacao = dtUltimaConsolidacao;
		this.situacao = situacao;
	}
	
	@Id
	public Integer getCdagendamento() {
		return cdagendamento;
	}
	@DisplayName("�ltima consolida��o")
	public Date getDtUltimaConsolidacao() {
		return dtUltimaConsolidacao;
	}
	@DisplayName("Situa��o")
	public Integer getSituacao() {
		return situacao;
	}
	
	public void setCdagendamento(Integer cdagendamento) {
		this.cdagendamento = cdagendamento;
	}
	public void setDtUltimaConsolidacao(Date dtUltimaConsolidacao) {
		this.dtUltimaConsolidacao = dtUltimaConsolidacao;
	}
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
	
	
}
