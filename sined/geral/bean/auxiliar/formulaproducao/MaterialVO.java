package br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao;

public class MaterialVO {

	private int cdmaterial;
	private double altura;
	private double largura;
	private double peso;
	
	public int getCdmaterial() {
		return cdmaterial;
	}
	public double getAltura() {
		return altura;
	}
	public double getLargura() {
		return largura;
	}
	public double getPeso() {
		return peso;
	}
	public void setCdmaterial(int cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
}
