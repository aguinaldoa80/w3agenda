package br.com.linkcom.sined.geral.bean.auxiliar.formulaproducao;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Producaoagenda;

public class FormulaVO {

	private double alturaMaterialVenda;
	private double larguraMaterialVenda;
	private double quantidadeMaterialVenda;
	private Producaoagenda producaoagenda;
	
	private List<MaterialVO> listaMaterial = new ArrayList<MaterialVO>();
	private List<MaterialVO> listaMaterialProducao = new ArrayList<MaterialVO>();
	
	public double getAlturaMaterialVenda() {
		return alturaMaterialVenda;
	}
	public double getLarguraMaterialVenda() {
		return larguraMaterialVenda;
	}
	public double getQuantidadeMaterialVenda() {
		return quantidadeMaterialVenda;
	}
	public List<MaterialVO> getListaMaterial() {
		return listaMaterial;
	}
	public List<MaterialVO> getListaMaterialProducao() {
		return listaMaterialProducao;
	}
	public void setAlturaMaterialVenda(double alturaMaterialVenda) {
		this.alturaMaterialVenda = alturaMaterialVenda;
	}
	public void setLarguraMaterialVenda(double larguraMaterialVenda) {
		this.larguraMaterialVenda = larguraMaterialVenda;
	}
	public void setQuantidadeMaterialVenda(double quantidadeMaterialVenda) {
		this.quantidadeMaterialVenda = quantidadeMaterialVenda;
	}
	public void setListaMaterial(List<MaterialVO> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	public void setListaMaterialProducao(List<MaterialVO> listaMaterialProducao) {
		this.listaMaterialProducao = listaMaterialProducao;
	}
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	
	public void addMaterial(MaterialVO materialVO){
		listaMaterial.add(materialVO);
	}
	
	public void addMaterialProducao(MaterialVO materialVO){
		listaMaterialProducao.add(materialVO);
	}
	
	public double larguraMaterialProducao(int cdmaterial){
		if(listaMaterialProducao != null && listaMaterialProducao.size() > 0){
			for (MaterialVO materialVO : listaMaterialProducao) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getLargura();
				}
			}
		}
		return 0.0;
	}
	
	public double alturaMaterialProducao(Integer cdmaterial){
		if(listaMaterialProducao != null && listaMaterialProducao.size() > 0){
			for (MaterialVO materialVO : listaMaterialProducao) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getAltura();
				}
			}
		}
		return 0.0;
	}
	
	public double larguraMaterial(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getLargura();
				}
			}
		}
		return 0.0;
	}
	
	public double alturaMaterial(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getAltura();
				}
			}
		}
		return 0.0;
	}
	
	public double pesoMaterial(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getPeso();
				}
			}
		}
		return 0.0;
	}
	
	public boolean isMaterialVenda(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return true;
				}
			}
		}
		return false;
	}
	
	public String getTipoProducao(){
		if(getProducaoagenda() != null && getProducaoagenda().getProducaoagendatipo() != null && 
				getProducaoagenda().getProducaoagendatipo().getNome() != null){
			return getProducaoagenda().getProducaoagendatipo().getNome();
		}			
		return "";
	}
}
