package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaoentrada {

	public final static String ENTRADA = "02";
	
	private String entrada;
	private Integer sequencial;
	private Integer codigoempresa;
	private String inscricaofornecedorCpfCnpjCei;
	private Integer codigoespecie;
	private Integer  codigoexclusaodief;
	private String codigoacumulador;
	private String cfop;
	private Integer seguimento;
	private String numerodocumento;
	private String serie;
	private Integer documentofinal;
	private Date dataentrada;
	private Date dataemissao;
	private Double valorcontabil;
	private Double valorexcusaodief;
	private String reservado;
	private String  uffornecedor;
	private String modalidadefrete;
	private String emitentenota;
	private String fatogeradorcrf;
	private String fatogeradorirrf;
	private Integer codigomunicipio;
	private Integer  cfopestendidodetalhamento;
	private Integer codigotransferencianotacredito;
	private String brancos180;
	private String brancos186;
	private Integer codigoobservacao;
	private Date datavisto;
	private Double valorfrete;
	private Double valorseguro;
	private Double valordespesasacessorias;
	private Integer codigoantecipacaotributaria;
	private Double valorpis;
	private Double valorcofins;
	private Double valorprodutos;
	private Integer valorbcicmsst;
	private Double entradassaidaisenta;
	private Double outrasentradasisentas;
	private Double valortransporteincluidobase;
	private Integer ressarcimento;
	private String codigomodelodocumentofiscal;
	private Integer codigosituacaotributaria;
	private Integer subserie;
	private String inscricaoestadualfornecedor;
	private String inscricaomunicipalfornecedor;
	private String observacao;
	private String chavenfe;
	private String codigorecolhimentofethab;
	private String responsavelrecolhimentofethab;
	private Integer tipocte;
	private String ctereferencia;
	private String brancos;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getEntrada(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printInteger(getCodigoempresa(), 7) +
			   SIntegraUtil.printStringCpfCnpj(getInscricaofornecedorCpfCnpjCei(), 14) +
			   SIntegraUtil.printInteger(getCodigoespecie(), 7) +
			   SIntegraUtil.printInteger(getCodigoexclusaodief(), 2) +
			   SIntegraUtil.printStringOrigemInteger(getCodigoacumulador(), 7) +
			   SIntegraUtil.printStringOrigemInteger(getCfop(), 7) +
			   SIntegraUtil.printInteger(getSeguimento(), 2) +
			   SIntegraUtil.printStringOrigemInteger(getNumerodocumento(), 7) +			   
			   SIntegraUtil.printString(getSerie(), 7) + 
			   SIntegraUtil.printInteger(getDocumentofinal(), 7) +
			   SIntegraUtil.printDateDominio(getDataentrada(), 10) +
			   SIntegraUtil.printDateDominio(getDataemissao(), 10) +
			   SIntegraUtil.printDouble(getValorcontabil(), 13) +
			   SIntegraUtil.printDouble(getValorexcusaodief(), 13) +
			   SIntegraUtil.printString(getReservado(), 30) +
			   SIntegraUtil.printString(getUffornecedor(), 2) +
			   SIntegraUtil.printString(getModalidadefrete(), 1) +
			   SIntegraUtil.printString(getEmitentenota(), 1) +			   
			   SIntegraUtil.printString(getFatogeradorcrf(), 1) +
			   SIntegraUtil.printString(getFatogeradorirrf(), 1) +
			   SIntegraUtil.printInteger(getCodigomunicipio(), 7) +
			   SIntegraUtil.printInteger(getCfopestendidodetalhamento(), 7) +
			   SIntegraUtil.printInteger(getCodigotransferencianotacredito(), 7) +
			   SIntegraUtil.printString(getBrancos180(), 6) +
			   SIntegraUtil.printString(getBrancos186(), 6) +
			   SIntegraUtil.printInteger(getCodigoobservacao(), 7) +
			   SIntegraUtil.printDateDominio(getDatavisto(), 10) +
			   SIntegraUtil.printDouble(getValorfrete(), 13) +
			   SIntegraUtil.printDouble(getValorseguro(), 13) +
			   SIntegraUtil.printDouble(getValordespesasacessorias(), 13) +
			   SIntegraUtil.printInteger(getCodigoantecipacaotributaria(), 7) +
			   SIntegraUtil.printDouble(getValorpis(), 13) +
			   SIntegraUtil.printDouble(getValorcofins(), 13) +
			   SIntegraUtil.printDouble(getValorprodutos(), 13) +
			   SIntegraUtil.printInteger(getValorbcicmsst(), 1) +
			   SIntegraUtil.printDouble(getEntradassaidaisenta(), 13) +
			   SIntegraUtil.printDouble(getOutrasentradasisentas(), 13) +
			   SIntegraUtil.printDouble(getValortransporteincluidobase(), 13) +
			   SIntegraUtil.printInteger(getRessarcimento(), 1) +
			   SIntegraUtil.printStringOrigemInteger(getCodigomodelodocumentofiscal(), 7) +
			   SIntegraUtil.printInteger(getCodigosituacaotributaria(), 7) +
			   SIntegraUtil.printInteger(getSubserie(), 7) +
			   SIntegraUtil.printString(getInscricaoestadualfornecedor(), 20) +
			   SIntegraUtil.printString(getInscricaomunicipalfornecedor(), 20) +
			   SIntegraUtil.printString(getObservacao(), 300) +
			   SIntegraUtil.printString(getChavenfe(), 44) +
			   SIntegraUtil.printString(getCodigorecolhimentofethab(), 6) +
			   SIntegraUtil.printString(getResponsavelrecolhimentofethab(), 1) +
			   SIntegraUtil.printInteger(getTipocte(), 1) +
			   SIntegraUtil.printString(getCtereferencia(), 44) +
			   SIntegraUtil.printString(getBrancos(), 48) +		   
			   
			   SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getEntrada() {
		return entrada;
	}
	public Integer getSequencial() {
		return sequencial;
	}
	public Integer getCodigoempresa() {
		return codigoempresa;
	}
	public String getInscricaofornecedorCpfCnpjCei() {
		return inscricaofornecedorCpfCnpjCei;
	}
	public Integer getCodigoespecie() {
		return codigoespecie;
	}
	public Integer getCodigoexclusaodief() {
		return codigoexclusaodief;
	}
	public String getCodigoacumulador() {
		return codigoacumulador;
	}
	public String getCfop() {
		return cfop;
	}
	public Integer getSeguimento() {
		return seguimento;
	}
	public String getNumerodocumento() {
		return numerodocumento;
	}
	public String getSerie() {
		return serie;
	}
	public Integer getDocumentofinal() {
		return documentofinal;
	}
	public Date getDataentrada() {
		return dataentrada;
	}
	public Date getDataemissao() {
		return dataemissao;
	}
	public Double getValorcontabil() {
		return valorcontabil;
	}
	public Double getValorexcusaodief() {
		return valorexcusaodief;
	}
	public String getReservado() {
		return reservado;
	}
	public String getUffornecedor() {
		return uffornecedor;
	}
	public String getModalidadefrete() {
		return modalidadefrete;
	}
	public String getEmitentenota() {
		return emitentenota;
	}
	public String getFatogeradorcrf() {
		return fatogeradorcrf;
	}
	public String getFatogeradorirrf() {
		return fatogeradorirrf;
	}
	public Integer getCodigomunicipio() {
		return codigomunicipio;
	}
	public Integer getCfopestendidodetalhamento() {
		return cfopestendidodetalhamento;
	}
	public Integer getCodigotransferencianotacredito() {
		return codigotransferencianotacredito;
	}
	public String getBrancos180() {
		return brancos180;
	}
	public String getBrancos186() {
		return brancos186;
	}
	public Integer getCodigoobservacao() {
		return codigoobservacao;
	}
	public Date getDatavisto() {
		return datavisto;
	}
	public Double getValorfrete() {
		return valorfrete;
	}
	public Double getValorseguro() {
		return valorseguro;
	}
	public Double getValordespesasacessorias() {
		return valordespesasacessorias;
	}
	public Integer getCodigoantecipacaotributaria() {
		return codigoantecipacaotributaria;
	}
	public Double getValorpis() {
		return valorpis;
	}
	public Double getValorcofins() {
		return valorcofins;
	}
	public Double getValorprodutos() {
		return valorprodutos;
	}
	public Integer getValorbcicmsst() {
		return valorbcicmsst;
	}
	public Double getEntradassaidaisenta() {
		return entradassaidaisenta;
	}
	public Double getOutrasentradasisentas() {
		return outrasentradasisentas;
	}
	public Double getValortransporteincluidobase() {
		return valortransporteincluidobase;
	}
	public Integer getRessarcimento() {
		return ressarcimento;
	}
	public String getCodigomodelodocumentofiscal() {
		if(codigomodelodocumentofiscal != null && "xx".equalsIgnoreCase(codigomodelodocumentofiscal))
			return "";
		return codigomodelodocumentofiscal;
	}
	public Integer getCodigosituacaotributaria() {
		return codigosituacaotributaria;
	}
	public Integer getSubserie() {
		return subserie;
	}
	public String getInscricaoestadualfornecedor() {
		return inscricaoestadualfornecedor;
	}
	public String getInscricaomunicipalfornecedor() {
		return inscricaomunicipalfornecedor;
	}
	public String getObservacao() {
		return observacao;
	}
	public String getChavenfe() {
		return chavenfe;
	}
	public String getCodigorecolhimentofethab() {
		return codigorecolhimentofethab;
	}
	public String getResponsavelrecolhimentofethab() {
		return responsavelrecolhimentofethab;
	}
	public Integer getTipocte() {
		return tipocte;
	}
	public String getCtereferencia() {
		return ctereferencia;
	}
	public String getBrancos() {
		return brancos;
	}
	
	
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	public void setCodigoempresa(Integer codigoempresa) {
		this.codigoempresa = codigoempresa;
	}
	public void setInscricaofornecedorCpfCnpjCei(
			String inscricaofornecedorCpfCnpjCei) {
		this.inscricaofornecedorCpfCnpjCei = inscricaofornecedorCpfCnpjCei;
	}
	public void setCodigoespecie(Integer codigoespecie) {
		this.codigoespecie = codigoespecie;
	}
	public void setCodigoexclusaodief(Integer codigoexclusaodief) {
		this.codigoexclusaodief = codigoexclusaodief;
	}
	public void setCodigoacumulador(String codigoacumulador) {
		this.codigoacumulador = codigoacumulador;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}
	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setDocumentofinal(Integer documentofinal) {
		this.documentofinal = documentofinal;
	}
	public void setDataentrada(Date dataentrada) {
		this.dataentrada = dataentrada;
	}
	public void setDataemissao(Date dataemissao) {
		this.dataemissao = dataemissao;
	}
	public void setValorcontabil(Double valorcontabil) {
		this.valorcontabil = valorcontabil;
	}
	public void setValorexcusaodief(Double valorexcusaodief) {
		this.valorexcusaodief = valorexcusaodief;
	}
	public void setReservado(String reservado) {
		this.reservado = reservado;
	}
	public void setUffornecedor(String uffornecedor) {
		this.uffornecedor = uffornecedor;
	}
	public void setModalidadefrete(String modalidadefrete) {
		this.modalidadefrete = modalidadefrete;
	}
	public void setEmitentenota(String emitentenota) {
		this.emitentenota = emitentenota;
	}
	public void setFatogeradorcrf(String fatogeradorcrf) {
		this.fatogeradorcrf = fatogeradorcrf;
	}
	public void setFatogeradorirrf(String fatogeradorirrf) {
		this.fatogeradorirrf = fatogeradorirrf;
	}
	public void setCodigomunicipio(Integer codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}
	public void setCfopestendidodetalhamento(Integer cfopestendidodetalhamento) {
		this.cfopestendidodetalhamento = cfopestendidodetalhamento;
	}
	public void setCodigotransferencianotacredito(
			Integer codigotransferencianotacredito) {
		this.codigotransferencianotacredito = codigotransferencianotacredito;
	}
	public void setBrancos180(String brancos180) {
		this.brancos180 = brancos180;
	}
	public void setBrancos186(String brancos186) {
		this.brancos186 = brancos186;
	}
	public void setCodigoobservacao(Integer codigoobservacao) {
		this.codigoobservacao = codigoobservacao;
	}
	public void setDatavisto(Date datavisto) {
		this.datavisto = datavisto;
	}
	public void setValorfrete(Double valorfrete) {
		this.valorfrete = valorfrete;
	}
	public void setValorseguro(Double valorseguro) {
		this.valorseguro = valorseguro;
	}
	public void setValordespesasacessorias(Double valordespesasacessorias) {
		this.valordespesasacessorias = valordespesasacessorias;
	}
	public void setCodigoantecipacaotributaria(Integer codigoantecipacaotributaria) {
		this.codigoantecipacaotributaria = codigoantecipacaotributaria;
	}
	public void setValorpis(Double valorpis) {
		this.valorpis = valorpis;
	}
	public void setValorcofins(Double valorcofins) {
		this.valorcofins = valorcofins;
	}
	public void setValorprodutos(Double valorprodutos) {
		this.valorprodutos = valorprodutos;
	}
	public void setValorbcicmsst(Integer valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public void setEntradassaidaisenta(Double entradassaidaisenta) {
		this.entradassaidaisenta = entradassaidaisenta;
	}
	public void setOutrasentradasisentas(Double outrasentradasisentas) {
		this.outrasentradasisentas = outrasentradasisentas;
	}
	public void setValortransporteincluidobase(Double valortransporteincluidobase) {
		this.valortransporteincluidobase = valortransporteincluidobase;
	}
	public void setRessarcimento(Integer ressarcimento) {
		this.ressarcimento = ressarcimento;
	}
	public void setCodigomodelodocumentofiscal(String codigomodelodocumentofiscal) {
		this.codigomodelodocumentofiscal = codigomodelodocumentofiscal;
	}
	public void setCodigosituacaotributaria(Integer codigosituacaotributaria) {
		this.codigosituacaotributaria = codigosituacaotributaria;
	}
	public void setSubserie(Integer subserie) {
		this.subserie = subserie;
	}
	public void setInscricaoestadualfornecedor(String inscricaoestadualfornecedor) {
		this.inscricaoestadualfornecedor = inscricaoestadualfornecedor;
	}
	public void setInscricaomunicipalfornecedor(String inscricaomunicipalfornecedor) {
		this.inscricaomunicipalfornecedor = inscricaomunicipalfornecedor;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setChavenfe(String chavenfe) {
		this.chavenfe = chavenfe;
	}
	public void setCodigorecolhimentofethab(String codigorecolhimentofethab) {
		this.codigorecolhimentofethab = codigorecolhimentofethab;
	}
	public void setResponsavelrecolhimentofethab(
			String responsavelrecolhimentofethab) {
		this.responsavelrecolhimentofethab = responsavelrecolhimentofethab;
	}
	public void setTipocte(Integer tipocte) {
		this.tipocte = tipocte;
	}
	public void setCtereferencia(String ctereferencia) {
		this.ctereferencia = ctereferencia;
	}
	public void setBrancos(String brancos) {
		this.brancos = brancos;
	}
}
