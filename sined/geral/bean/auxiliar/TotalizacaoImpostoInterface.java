package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;

public interface TotalizacaoImpostoInterface {

	public Money getTotalFcp();
	public void setTotalFcp(Money totalFcp);
	
	public Money getTotalFcpSt();
	public void setTotalFcpSt(Money totalFcpSt);
	
	public Money getTotalIcms();
	public void setTotalIcms(Money totalIcms);

	public Money getTotalIcmsSt();
	public void setTotalIcmsSt(Money totalIcmsSt);
	
	public Money getTotalipi();
	
	public Money getTotalDifal();
	public void setTotalDifal(Money totalDifal);
	
	public Money getTotalDesoneracaoIcms();
	public void setTotalDesoneracaoIcms(Money totalDesoneracaoIcms);
}
