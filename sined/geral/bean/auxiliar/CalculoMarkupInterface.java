package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;

public interface CalculoMarkupInterface {

	public Money getValorusadovalecompra();
	public Money getValorfrete();
	public Money getDesconto();
	public Money getTotalvendaSemArredondamento();
	public Empresa getEmpresa();
	public Pedidovendatipo getPedidovendatipo();
}
