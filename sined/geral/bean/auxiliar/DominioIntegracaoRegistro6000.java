package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.utils.DominioIntegracaoUtil;

public class DominioIntegracaoRegistro6000 {
	public final static String IDENTIFICADOR = "6000";
	
	private String tipoLancamento;
	private Integer codigoLancamentoPadrao;
	private String localizador;
	private String rttFcont;
	
	@Override
	public String toString() {
		return DominioIntegracaoUtil.printString(IDENTIFICADOR) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(tipoLancamento) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(codigoLancamentoPadrao) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(localizador) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(rttFcont) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.SEPARADOR_LINHA;
	}
	
	public String getTipoLancamento() {
		return tipoLancamento;
	}
	public Integer getCodigoLancamentoPadrao() {
		return codigoLancamentoPadrao;
	}
	public String getLocalizador() {
		return localizador;
	}
	public String getRttFcont() {
		return rttFcont;
	}
	public void setTipoLancamento(String tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	public void setCodigoLancamentoPadrao(Integer codigoLancamentoPadrao) {
		this.codigoLancamentoPadrao = codigoLancamentoPadrao;
	}
	public void setLocalizador(String localizador) {
		this.localizador = localizador;
	}
	public void setRttFcont(String rttFcont) {
		this.rttFcont = rttFcont;
	}
}
