package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Situacaosuprimentos;

@Entity
public class Aux_solicitacaocompra {

	protected Integer cdsolicitacaocompra;
	protected Situacaosuprimentos situacaosuprimentos;
	protected Boolean pendenciaqtde;
	protected String cotacao;
	protected String ordemcompra;
	protected String entrega;
	
	@Id
	public Integer getCdsolicitacaocompra() {
		return cdsolicitacaocompra;
	}
	@Column(name="situacao")
	@DisplayName("Situa��o")
	public Situacaosuprimentos getSituacaosuprimentos() {
		return situacaosuprimentos;
	}
	
	public Boolean getPendenciaqtde() {
		return pendenciaqtde;
	}
	
	public String getCotacao() {
		return cotacao;
	}
	
	public String getOrdemcompra() {
		return ordemcompra;
	}
	
	public String getEntrega() {
		return entrega;
	}
	
	public void setCotacao(String cotacao) {
		this.cotacao = cotacao;
	}
	public void setOrdemcompra(String ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	public void setPendenciaqtde(Boolean pendenciaqtde) {
		this.pendenciaqtde = pendenciaqtde;
	}
	public void setCdsolicitacaocompra(Integer cdsolicitacaocompra) {
		this.cdsolicitacaocompra = cdsolicitacaocompra;
	}
	public void setSituacaosuprimentos(Situacaosuprimentos situacaosuprimentos) {
		this.situacaosuprimentos = situacaosuprimentos;
	}
	
}
