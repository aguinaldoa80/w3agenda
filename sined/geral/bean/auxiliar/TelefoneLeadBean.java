package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.sined.geral.bean.Telefone;

public class TelefoneLeadBean {
	private String telefone;
	private Integer cdtelefonetipo;
	
	public TelefoneLeadBean() {}
	
	public TelefoneLeadBean(Telefone telefone) {
		this.telefone = telefone != null ? telefone.getTelefone() : "";
		this.cdtelefonetipo = telefone != null && telefone.getTelefonetipo() != null ? telefone.getTelefonetipo().getCdtelefonetipo(): null; 
	}

	public String getTelefone() {
		return telefone;
	}

	public Integer getCdtelefonetipo() {
		return cdtelefonetipo;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setCdtelefonetipo(Integer cdtelefonetipo) {
		this.cdtelefonetipo = cdtelefonetipo;
	}
}
