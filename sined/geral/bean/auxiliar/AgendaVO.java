package br.com.linkcom.sined.geral.bean.auxiliar;

public class AgendaVO {

	private String horario;
	private String descricao;
	
	public String getHorario() {
		return horario;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
