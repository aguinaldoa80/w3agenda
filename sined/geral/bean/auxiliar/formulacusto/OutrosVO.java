package br.com.linkcom.sined.geral.bean.auxiliar.formulacusto;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Entregadocumentofreterateio;

public class OutrosVO {

	private double vr_total;
	private double vr_rateio;
	
	public OutrosVO(){}
	
	public OutrosVO(Entregadocumento entregadocumento, Entregadocumentofreterateio entregadocumentofreterateio){
		this.vr_total = entregadocumento.getValortotalOutrasdespesasDocCarga().getValue().doubleValue();
		this.vr_rateio = entregadocumentofreterateio != null && entregadocumentofreterateio.getValor() != null ? entregadocumentofreterateio.getValor().getValue().doubleValue() : 0d;
	}
	
	public double getVr_total() {
		return vr_total;
	}
	public double getVr_rateio() {
		return vr_rateio;
	}
	
	public void setVr_total(double vrTotal) {
		vr_total = vrTotal;
	}
	public void setVr_rateio(double vrRateio) {
		vr_rateio = vrRateio;
	}
}
