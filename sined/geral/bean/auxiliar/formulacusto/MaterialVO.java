package br.com.linkcom.sined.geral.bean.auxiliar.formulacusto;

import br.com.linkcom.sined.geral.bean.Entregamaterial;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;
import br.com.linkcom.sined.geral.bean.Notafiscalprodutoitem;

public class MaterialVO {

	private double qtde_compra;
	private double qtde_compra_convertida;
	private double valorunitario_compra;
	private double vr_frete;
	private double vr_seguro;
	private double vr_outras;
	private double vr_desconto;
	private String cfop;
	private double vr_ir;
	private double vr_inss;
	private double vr_csll;
	private double vr_ii;
	private String cst_icms;
	private double bc_icms;
	private double vr_icms;
	private double bc_icmsst;
	private double vr_icmsst;
	private String cst_ipi;
	private double bc_ipi;
	private double vr_ipi;
	private String cst_pis;
	private double bc_pis;
	private double vr_pis;
	private String cst_cofins;
	private double bc_cofins;
	private double vr_cofins;
	private double bc_importacao;
	private double vr_importacao;
	private double vr_iof;
	private String local_estoque;
	private double valorcusto_atual;
	private String projeto;
	private double valorcustofcp;
	private double valorcustofcpst;
	private boolean naoconsideraricmsst;
	private double vr_icmsdeson;
	
	public MaterialVO(){}
	
	public MaterialVO(Entregamaterial entregamaterial){
		this.qtde_compra = entregamaterial.getQtde() != null ? entregamaterial.getQtde() : 0d;
		this.qtde_compra_convertida = entregamaterial.getQtde_convertida() != null ? entregamaterial.getQtde_convertida() : 0d;
		this.valorunitario_compra = entregamaterial.getValorunitario() != null ? entregamaterial.getValorunitario() : 0d;
		this.vr_frete = entregamaterial.getValorfrete() != null ? entregamaterial.getValorfrete().getValue().doubleValue() : 0d;
		this.vr_seguro = entregamaterial.getValorseguro() != null ? entregamaterial.getValorseguro().getValue().doubleValue() : 0d;
		this.vr_outras = entregamaterial.getValoroutrasdespesas() != null ? entregamaterial.getValoroutrasdespesas().getValue().doubleValue() : 0d;
		this.vr_desconto = entregamaterial.getValordesconto() != null ? entregamaterial.getValordesconto().getValue().doubleValue() : 0d;
		this.cfop = entregamaterial.getCfop() != null && entregamaterial.getCfop().getCodigo() != null ? entregamaterial.getCfop().getCodigo() : "";
		this.vr_ir = entregamaterial.getValorir() != null ? entregamaterial.getValorir().getValue().doubleValue() : 0d;
		this.vr_inss = entregamaterial.getValorinss() != null ? entregamaterial.getValorinss().getValue().doubleValue() : 0d;
		this.vr_csll = entregamaterial.getValorcsll() != null ? entregamaterial.getValorcsll().getValue().doubleValue() : 0d;
		this.vr_ii = entregamaterial.getValorimpostoimportacao() != null ? entregamaterial.getValorimpostoimportacao().getValue().doubleValue() : 0d;
		this.cst_icms = entregamaterial.getCsticms() != null ? entregamaterial.getCsticms().getCdnfe() : "";
		this.bc_icms = entregamaterial.getValorbcicms() != null ? entregamaterial.getValorbcicms().getValue().doubleValue() : 0d;
		this.vr_icms = entregamaterial.getValoricms() != null ? entregamaterial.getValoricms().getValue().doubleValue() : 0d;
		this.bc_icmsst = entregamaterial.getValorbcicmsst() != null ? entregamaterial.getValorbcicmsst().getValue().doubleValue() : 0d;
		this.vr_icmsst = entregamaterial.getValoricmsst() != null ? entregamaterial.getValoricmsst().getValue().doubleValue() : 0d;
		this.cst_ipi = entregamaterial.getCstipi() != null ? entregamaterial.getCstipi().getCdnfe() : "";
		this.bc_ipi = entregamaterial.getValorbcipi() != null ? entregamaterial.getValorbcipi().getValue().doubleValue() : 0d;
		this.vr_ipi = entregamaterial.getValoripi() != null ? entregamaterial.getValoripi().getValue().doubleValue() : 0d;
		this.cst_pis = entregamaterial.getCstpis() != null ? entregamaterial.getCstpis().getCdnfe() : "";
		this.bc_pis = entregamaterial.getValorbcpis() != null ? entregamaterial.getValorbcpis().getValue().doubleValue() : 0d;
		this.vr_pis = entregamaterial.getValorpis() != null ? entregamaterial.getValorpis().getValue().doubleValue() : 0d;
		this.cst_cofins = entregamaterial.getCstcofins() != null ? entregamaterial.getCstcofins().getCdnfe() : "";
		this.bc_cofins = entregamaterial.getValorbccofins() != null ? entregamaterial.getValorbccofins().getValue().doubleValue() : 0d;
		this.vr_cofins = entregamaterial.getValorcofins() != null ? entregamaterial.getValorcofins().getValue().doubleValue() : 0d;
		this.bc_importacao = entregamaterial.getValorbcii() != null ? entregamaterial.getValorbcii().getValue().doubleValue() : 0d;
		this.vr_importacao = entregamaterial.getValoriiItem() != null ? entregamaterial.getValoriiItem().getValue().doubleValue() : 0d;
		this.vr_iof = entregamaterial.getIof() != null ? entregamaterial.getIof().getValue().doubleValue() : 0d;
		this.local_estoque = entregamaterial.getLocalarmazenagem() != null ? entregamaterial.getLocalarmazenagem().getNome() : null;
		this.valorcusto_atual = entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getValorcusto() != null ? entregamaterial.getMaterial().getValorcusto() : 0d;
		this.projeto = entregamaterial.getProjeto() != null ? entregamaterial.getProjeto().getNome() : null;
		this.valorcustofcp = entregamaterial.getValorfcp() != null ? entregamaterial.getValorfcp().getValue().doubleValue() : 0d;
		this.valorcustofcpst = entregamaterial.getValorfcpst() != null ? entregamaterial.getValorfcpst().getValue().doubleValue() : 0d;
		this.vr_icmsdeson = entregamaterial.getValorIcmsDesonerado() != null ? entregamaterial.getValorIcmsDesonerado().getValue().doubleValue() : 0d;
		if(Boolean.TRUE.equals(entregamaterial.getNaoconsideraricmsst())){
			this.naoconsideraricmsst = true;
		}else{
			this.naoconsideraricmsst = false;
		}
	}
	
	
	public MaterialVO(Notafiscalprodutoitem notaFiscalProdutoItem , Notafiscalproduto notaFiscalProduto){
		this.qtde_compra = notaFiscalProdutoItem.getQtde() != null ? notaFiscalProdutoItem.getQtde() : 0d;
		this.qtde_compra_convertida = notaFiscalProdutoItem.getQtde_convertida() != null ? notaFiscalProdutoItem.getQtde_convertida() : 0d;
		this.valorunitario_compra = notaFiscalProdutoItem.getValorunitario() != null ? notaFiscalProdutoItem.getValorunitario() : 0d;
		this.vr_frete = notaFiscalProdutoItem.getValorfrete() != null ? notaFiscalProdutoItem.getValorfrete().getValue().doubleValue() : 0d;
		this.vr_seguro = notaFiscalProdutoItem.getValorseguro() != null ? notaFiscalProdutoItem.getValorseguro().getValue().doubleValue() : 0d;
		this.vr_outras = notaFiscalProdutoItem.getOutrasdespesas() != null ? notaFiscalProdutoItem.getOutrasdespesas().getValue().doubleValue() : 0d;
		this.vr_desconto = notaFiscalProdutoItem.getValordesconto() != null ? notaFiscalProdutoItem.getValordesconto().getValue().doubleValue() : 0d;
		this.cfop = notaFiscalProdutoItem.getCfop() != null && notaFiscalProdutoItem.getCfop().getCodigo() != null ? notaFiscalProdutoItem.getCfop().getCodigo() : "";
		this.vr_ir = notaFiscalProdutoItem.getValorir() != null ? notaFiscalProdutoItem.getValorir().getValue().doubleValue() : 0d;
		this.vr_inss = notaFiscalProdutoItem.getValorinss() != null ? notaFiscalProdutoItem.getValorinss().getValue().doubleValue() : 0d;
		this.vr_csll = notaFiscalProdutoItem.getValorcsll() != null ? notaFiscalProdutoItem.getValorcsll().getValue().doubleValue() : 0d;
		this.vr_ii = notaFiscalProdutoItem.getValorii() != null ? notaFiscalProdutoItem.getValorii().getValue().doubleValue() : 0d;
		this.cst_icms = notaFiscalProdutoItem.getCstIcmsSPED() != null ? notaFiscalProdutoItem.getCstIcmsSPED() : "";
		this.bc_icms = notaFiscalProdutoItem.getValorbcicms() != null ? notaFiscalProdutoItem.getValorbcicms().getValue().doubleValue() : 0d;
		this.vr_icms = notaFiscalProdutoItem.getValoricms() != null ? notaFiscalProdutoItem.getValoricms().getValue().doubleValue() : 0d;
		this.bc_icmsst = notaFiscalProdutoItem.getValorbcicmsst() != null ? notaFiscalProdutoItem.getValorbcicmsst().getValue().doubleValue() : 0d;
		this.vr_icmsst = notaFiscalProdutoItem.getValoricmsst() != null ? notaFiscalProdutoItem.getValoricmsst().getValue().doubleValue() : 0d;
		this.cst_ipi = notaFiscalProdutoItem.getTipocobrancaipi() != null ? notaFiscalProdutoItem.getTipocobrancaipi().getCdnfe() : "";
		this.bc_ipi = notaFiscalProdutoItem.getValorbcipi() != null ? notaFiscalProdutoItem.getValorbcipi().getValue().doubleValue() : 0d;
		this.vr_ipi = notaFiscalProdutoItem.getValoripi() != null ? notaFiscalProdutoItem.getValoripi().getValue().doubleValue() : 0d;
		this.cst_pis = notaFiscalProdutoItem.getTipocobrancapis() != null ? notaFiscalProdutoItem.getTipocobrancapis().getCdnfe() : "";
		this.bc_pis = notaFiscalProdutoItem.getValorbcpis() != null ? notaFiscalProdutoItem.getValorbcpis().getValue().doubleValue() : 0d;
		this.vr_pis = notaFiscalProdutoItem.getValorpis() != null ? notaFiscalProdutoItem.getValorpis().getValue().doubleValue() : 0d;
		this.cst_cofins = notaFiscalProdutoItem.getTipocobrancacofins() != null ? notaFiscalProdutoItem.getTipocobrancacofins().getCdnfe() : "";
		this.bc_cofins = notaFiscalProdutoItem.getValorbccofins() != null ? notaFiscalProdutoItem.getValorbccofins().getValue().doubleValue() : 0d;
		this.vr_cofins = notaFiscalProdutoItem.getValorcofins() != null ? notaFiscalProdutoItem.getValorcofins().getValue().doubleValue() : 0d;
		this.bc_importacao = notaFiscalProdutoItem.getValorbcii() != null ? notaFiscalProdutoItem.getValorbcii().getValue().doubleValue() : 0d;
		this.vr_importacao = notaFiscalProdutoItem.getValorii() != null ? notaFiscalProdutoItem.getValorii().getValue().doubleValue() : 0d;
		this.vr_iof = notaFiscalProdutoItem.getValoriof() != null ? notaFiscalProdutoItem.getValoriof().getValue().doubleValue() : 0d;
		this.local_estoque = notaFiscalProdutoItem.getLocalarmazenagem() != null ? notaFiscalProdutoItem.getLocalarmazenagem().getNome() : null;
		this.valorcusto_atual = notaFiscalProdutoItem.getMaterial() != null && notaFiscalProdutoItem.getMaterial().getValorcusto() != null ? notaFiscalProdutoItem.getMaterial().getValorcusto() : 0d;
		this.projeto = notaFiscalProduto.getProjeto() != null ? notaFiscalProduto.getProjeto().getNome() : null;
		this.valorcustofcp = notaFiscalProdutoItem.getValorfcp() != null ? notaFiscalProdutoItem.getValorfcp().getValue().doubleValue() : 0d;
		this.valorcustofcpst = notaFiscalProdutoItem.getValorfcpst() != null ? notaFiscalProdutoItem.getValorfcpst().getValue().doubleValue() : 0d;
		this.vr_icmsdeson = notaFiscalProdutoItem.getValordesoneracaoicms() != null ? notaFiscalProdutoItem.getValordesoneracaoicms().getValue().doubleValue() :  0d;
		this.naoconsideraricmsst = notaFiscalProdutoItem.getGrupotributacao() != null && Boolean.TRUE.equals(notaFiscalProdutoItem.getGrupotributacao().getNaoconsideraricmsst());
		
	}
	
	
	public double getQtde_compra() {
		return qtde_compra;
	}
	public double getQtde_compra_convertida() {
		return qtde_compra_convertida;
	}
	public double getValorunitario_compra() {
		return valorunitario_compra;
	}
	public double getVr_frete() {
		return vr_frete;
	}
	public double getVr_seguro() {
		return vr_seguro;
	}
	public double getVr_outras() {
		return vr_outras;
	}
	public double getVr_desconto() {
		return vr_desconto;
	}
	public String getCfop() {
		return cfop;
	}
	public double getVr_ir() {
		return vr_ir;
	}
	public double getVr_inss() {
		return vr_inss;
	}
	public double getVr_csll() {
		return vr_csll;
	}
	public double getVr_ii() {
		return vr_ii;
	}
	public String getCst_icms() {
		return cst_icms;
	}
	public double getBc_icms() {
		return bc_icms;
	}
	public double getVr_icms() {
		return vr_icms;
	}
	public double getBc_icmsst() {
		return bc_icmsst;
	}
	public double getVr_icmsst() {
		return vr_icmsst;
	}
	public String getCst_ipi() {
		return cst_ipi;
	}
	public double getBc_ipi() {
		return bc_ipi;
	}
	public double getVr_ipi() {
		return vr_ipi;
	}
	public String getCst_pis() {
		return cst_pis;
	}
	public double getBc_pis() {
		return bc_pis;
	}
	public double getVr_pis() {
		return vr_pis;
	}
	public String getCst_cofins() {
		return cst_cofins;
	}
	public double getBc_cofins() {
		return bc_cofins;
	}
	public double getVr_cofins() {
		return vr_cofins;
	}
	public double getBc_importacao() {
		return bc_importacao;
	}
	public double getVr_importacao() {
		return vr_importacao;
	}
	public double getVr_iof() {
		return vr_iof;
	}
	public String getLocal_estoque() {
		return local_estoque;
	}
	public double getValorcusto_atual() {
		return valorcusto_atual;
	}
	public String getProjeto() {
		return projeto;
	}
	
	public double getValorcustofcp() {
		return valorcustofcp;
	}

	public double getValorcustofcpst() {
		return valorcustofcpst;
	}

	public boolean isNaoconsideraricmsst() {
		return naoconsideraricmsst;
	}
	
	public double getVr_icmsdeson() {
		return vr_icmsdeson;
	}
	
	public void setVr_icmsdeson(double vr_icmsdeson) {
		this.vr_icmsdeson = vr_icmsdeson;
	}

	public void setValorcustofcp(double valorcustofcp) {
		this.valorcustofcp = valorcustofcp;
	}

	public void setValorcustofcpst(double valorcustofcpst) {
		this.valorcustofcpst = valorcustofcpst;
	}
	
	public void setQtde_compra(double qtdeCompra) {
		qtde_compra = qtdeCompra;
	}
	public void setNaoconsideraricmsst(boolean naoconsideraricmsst) {
		this.naoconsideraricmsst = naoconsideraricmsst;
	}

	public void setQtde_compra_convertida(double qtdeCompraConvertida) {
		qtde_compra_convertida = qtdeCompraConvertida;
	}
	public void setValorunitario_compra(double valorunitarioCompra) {
		valorunitario_compra = valorunitarioCompra;
	}
	public void setVr_frete(double vrFrete) {
		vr_frete = vrFrete;
	}
	public void setVr_seguro(double vrSeguro) {
		vr_seguro = vrSeguro;
	}
	public void setVr_outras(double vrOutras) {
		vr_outras = vrOutras;
	}
	public void setVr_desconto(double vrDesconto) {
		vr_desconto = vrDesconto;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}
	public void setVr_ir(double vrIr) {
		vr_ir = vrIr;
	}
	public void setVr_inss(double vrInss) {
		vr_inss = vrInss;
	}
	public void setVr_csll(double vrCsll) {
		vr_csll = vrCsll;
	}
	public void setVr_ii(double vrIi) {
		vr_ii = vrIi;
	}
	public void setCst_icms(String cstIcms) {
		cst_icms = cstIcms;
	}
	public void setBc_icms(double bcIcms) {
		bc_icms = bcIcms;
	}
	public void setVr_icms(double vrIcms) {
		vr_icms = vrIcms;
	}
	public void setBc_icmsst(double bcIcmsst) {
		bc_icmsst = bcIcmsst;
	}
	public void setVr_icmsst(double vrIcmsst) {
		vr_icmsst = vrIcmsst;
	}
	public void setCst_ipi(String cstIpi) {
		cst_ipi = cstIpi;
	}
	public void setBc_ipi(double bcIpi) {
		bc_ipi = bcIpi;
	}
	public void setVr_ipi(double vrIpi) {
		vr_ipi = vrIpi;
	}
	public void setCst_pis(String cstPis) {
		cst_pis = cstPis;
	}
	public void setBc_pis(double bcPis) {
		bc_pis = bcPis;
	}
	public void setVr_pis(double vrPis) {
		vr_pis = vrPis;
	}
	public void setCst_cofins(String cstCofins) {
		cst_cofins = cstCofins;
	}
	public void setBc_cofins(double bcCofins) {
		bc_cofins = bcCofins;
	}
	public void setVr_cofins(double vrCofins) {
		vr_cofins = vrCofins;
	}
	public void setBc_importacao(double bcImportacao) {
		bc_importacao = bcImportacao;
	}
	public void setVr_importacao(double vrImportacao) {
		vr_importacao = vrImportacao;
	}
	public void setVr_iof(double vrIof) {
		vr_iof = vrIof;
	}
	public void setLocal_estoque(String localEstoque) {
		local_estoque = localEstoque;
	}
	public void setValorcusto_atual(double valorcustoAtual) {
		valorcusto_atual = valorcustoAtual;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
}
