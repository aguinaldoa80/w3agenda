package br.com.linkcom.sined.geral.bean.auxiliar.formulacusto;

import br.com.linkcom.neo.types.Money;

public class FreteVO {

	private double vr_total;
	private double vr_rateio;
	private double percentual_rateio;
	
	public FreteVO(){}
	
	public FreteVO(Money valorTotal, Money valorRateio){
		this.vr_total = valorTotal != null ? valorTotal.getValue().doubleValue() : 0d;
		this.vr_rateio = valorRateio != null ? valorRateio.getValue().doubleValue() : 0d;
		this.percentual_rateio = 0d;
	}
	
	public double getVr_total() {
		return vr_total;
	}
	public double getVr_rateio() {
		return vr_rateio;
	}
	public double getPercentual_rateio() {
		return percentual_rateio;
	}
	
	public void setVr_total(double vrTotal) {
		vr_total = vrTotal;
	}
	public void setVr_rateio(double vrRateio) {
		vr_rateio = vrRateio;
	}
	public void setPercentual_rateio(double percentualRateio) {
		percentual_rateio = percentualRateio;
	}
}
