package br.com.linkcom.sined.geral.bean.auxiliar.formulacusto;

import br.com.linkcom.sined.geral.bean.Entregadocumento;
import br.com.linkcom.sined.geral.bean.Notafiscalproduto;

public class NotaVO {

	private double vr_itens;
	private double vr_documento;
	private double vr_outras;
	private double vr_desconto;
	private double vr_frete;
	private double vr_seguro;
	private double vr_docref;
	private double bc_icms;
	private double vr_icms;
	private double bc_icmsst;
	private double vr_icmsst;
	private double bc_ipi;
	private double vr_ipi;
	private double bc_pis;
	private double vr_pis;
	private double bc_cofins;
	private double vr_cofins;
	private double bc_ii;
	private double vr_ii;
	private double vr_iof;
	private boolean simplesremessa = Boolean.FALSE;
	private boolean faturamentocliente = Boolean.FALSE;
	private double vr_fcpst;
	
	public NotaVO(){}
	
	public NotaVO(Entregadocumento entregadocumento){
		this.vr_itens = entregadocumento.getValormercadoria() != null ? entregadocumento.getValormercadoria().getValue().doubleValue() : 0d;
		this.vr_documento = entregadocumento.getValordocumento() != null ? entregadocumento.getValordocumento().getValue().doubleValue() : 0d;
		this.vr_outras = entregadocumento.getValoroutrasdespesas() != null ? entregadocumento.getValoroutrasdespesas().getValue().doubleValue() : 0d;
		this.vr_desconto = entregadocumento.getValordesconto() != null ? entregadocumento.getValordesconto().getValue().doubleValue() : 0d;
		this.vr_frete = entregadocumento.getValorfrete() != null ? entregadocumento.getValorfrete().getValue().doubleValue() : 0d;
		this.vr_seguro = entregadocumento.getValorseguro() != null ? entregadocumento.getValorseguro().getValue().doubleValue() : 0d;
		this.vr_docref = entregadocumento.getValortotalDocReferenciado().getValue().doubleValue();
		this.bc_icms = entregadocumento.getValortotalbcicms() != null ? entregadocumento.getValortotalbcicms().getValue().doubleValue() : 0d;
		this.vr_icms = entregadocumento.getValortotalicms() != null ? entregadocumento.getValortotalicms().getValue().doubleValue() : 0d;
		this.bc_icmsst = entregadocumento.getValortotalbcicmsst() != null ? entregadocumento.getValortotalbcicmsst().getValue().doubleValue() : 0d;
		this.vr_icmsst = entregadocumento.getValortotalicmsst() != null ? entregadocumento.getValortotalicmsst().getValue().doubleValue() : 0d;
		this.bc_ipi = entregadocumento.getValortotalbcipi() != null ? entregadocumento.getValortotalbcipi().getValue().doubleValue() : 0d;
		this.vr_ipi = entregadocumento.getValortotalipi() != null ? entregadocumento.getValortotalipi().getValue().doubleValue() : 0d;
		this.bc_pis = entregadocumento.getValortotalbcpis() != null ? entregadocumento.getValortotalbcpis().getValue().doubleValue() : 0d;
		this.vr_pis = entregadocumento.getValortotalpis() != null ? entregadocumento.getValortotalpis().getValue().doubleValue() : 0d;
		this.bc_cofins = entregadocumento.getValortotalbccofins() != null ? entregadocumento.getValortotalbccofins().getValue().doubleValue() : 0d;
		this.vr_cofins = entregadocumento.getValortotalcofins() != null ? entregadocumento.getValortotalcofins().getValue().doubleValue() : 0d;
		this.bc_ii = entregadocumento.getValortotalbcii() != null ? entregadocumento.getValortotalbcii().getValue().doubleValue() : 0d;
		this.vr_ii = entregadocumento.getValortotalimpostoimportacao() != null ? entregadocumento.getValortotalimpostoimportacao().getValue().doubleValue() : 0d;
		this.vr_iof = entregadocumento.getValortotaliof() != null ? entregadocumento.getValortotaliof().getValue().doubleValue() : 0d;
		this.simplesremessa = entregadocumento.getSimplesremessa() != null ? entregadocumento.getSimplesremessa() : false;
		this.faturamentocliente = entregadocumento.getEntrega() != null ? entregadocumento.getEntrega().getFaturamentocliente() : false;
		this.vr_fcpst = entregadocumento.getValortotalfcpst() != null ? entregadocumento.getValortotalfcpst().getValue().doubleValue() : 0.0;
	}
	public NotaVO(Notafiscalproduto notafiscalProduto){
		this.vr_itens = notafiscalProduto.getValorprodutos() != null ? notafiscalProduto.getValorprodutos().getValue().doubleValue() : 0d;
		this.vr_documento = notafiscalProduto.getValorNota() != null ? notafiscalProduto.getValorNota().getValue().doubleValue() : 0d;
		this.vr_outras = notafiscalProduto.getOutrasdespesas() != null ? notafiscalProduto.getOutrasdespesas().getValue().doubleValue() : 0d;
		this.vr_desconto = notafiscalProduto.getValordesconto() != null ? notafiscalProduto.getValordesconto().getValue().doubleValue() : 0d;
		this.vr_frete = notafiscalProduto.getValorfrete() != null ? notafiscalProduto.getValorfrete().getValue().doubleValue() : 0d;
		this.vr_seguro = notafiscalProduto.getValorseguro() != null ? notafiscalProduto.getValorseguro().getValue().doubleValue() : 0d;
		this.vr_docref = 0;
		this.bc_icms = notafiscalProduto.getValorbcicms() != null ? notafiscalProduto.getValorbcicms().getValue().doubleValue() : 0d;
		this.vr_icms = notafiscalProduto.getValoricms() != null ? notafiscalProduto.getValoricms().getValue().doubleValue() : 0d;
		this.bc_icmsst = notafiscalProduto.getValorbcicmsst() != null ? notafiscalProduto.getValorbcicmsst().getValue().doubleValue() : 0d;
		this.vr_icmsst = notafiscalProduto.getValoricmsst() != null ? notafiscalProduto.getValoricmsst().getValue().doubleValue() : 0d;
		this.bc_ipi = notafiscalProduto.getValorTotalBcipi() != null ? notafiscalProduto.getValorTotalBcipi().getValue().doubleValue() : 0d;
		this.vr_ipi = notafiscalProduto.getValoripi() != null ? notafiscalProduto.getValoripi().getValue().doubleValue() : 0d;
		this.bc_pis = notafiscalProduto.getValorTotalBcpis() != null ? notafiscalProduto.getValorTotalBcpis().getValue().doubleValue() : 0d;
		this.vr_pis = notafiscalProduto.getValorpis() != null ? notafiscalProduto.getValorpis().getValue().doubleValue() : 0d;
		this.bc_cofins = notafiscalProduto.getValorTotalBccofins() != null ? notafiscalProduto.getValorTotalBccofins().getValue().doubleValue() : 0d;
		this.vr_cofins = notafiscalProduto.getValorcofins() != null ? notafiscalProduto.getValorcofins().getValue().doubleValue() : 0d;
		this.bc_ii = notafiscalProduto.getValorTotalBcii() != null ? notafiscalProduto.getValorTotalBcii().getValue().doubleValue() : 0d;
		this.vr_ii = notafiscalProduto.getValorii() != null ? notafiscalProduto.getValorii().getValue().doubleValue() : 0d;
		this.vr_iof = 0;
	//	this.simplesremessa = entregadocumento.getSimplesremessa() != null ? entregadocumento.getSimplesremessa() : false;
		//this.faturamentocliente = entregadocumento.getEntrega() != null ? entregadocumento.getEntrega().getFaturamentocliente() : false;
		this.vr_fcpst = notafiscalProduto.getValorfcpretidost() != null ? notafiscalProduto.getValorfcpretidost().getValue().doubleValue() : 0.0;
	
	}
	
	public double getVr_itens() {
		return vr_itens;
	}
	public double getVr_documento() {
		return vr_documento;
	}
	public double getVr_outras() {
		return vr_outras;
	}
	public double getVr_desconto() {
		return vr_desconto;
	}
	public double getVr_frete() {
		return vr_frete;
	}
	public double getVr_seguro() {
		return vr_seguro;
	}
	public double getVr_docref() {
		return vr_docref;
	}
	public double getBc_icms() {
		return bc_icms;
	}
	public double getVr_icms() {
		return vr_icms;
	}
	public double getBc_icmsst() {
		return bc_icmsst;
	}
	public double getVr_icmsst() {
		return vr_icmsst;
	}
	public double getBc_ipi() {
		return bc_ipi;
	}
	public double getVr_ipi() {
		return vr_ipi;
	}
	public double getBc_pis() {
		return bc_pis;
	}
	public double getVr_pis() {
		return vr_pis;
	}
	public double getBc_cofins() {
		return bc_cofins;
	}
	public double getVr_cofins() {
		return vr_cofins;
	}
	public double getBc_ii() {
		return bc_ii;
	}
	public double getVr_ii() {
		return vr_ii;
	}
	public double getVr_iof() {
		return vr_iof;
	}
	public boolean isSimplesremessa() {
		return simplesremessa;
	}
	public boolean isFaturamentocliente() {
		return faturamentocliente;
	}
	public double getVr_fcpst() {
		return vr_fcpst;
	}
	public void setVr_fcpst(double vr_fcpst) {
		this.vr_fcpst = vr_fcpst;
	}
	public void setVr_itens(double vrItens) {
		vr_itens = vrItens;
	}
	public void setVr_documento(double vrDocumento) {
		vr_documento = vrDocumento;
	}
	public void setVr_outras(double vrOutras) {
		vr_outras = vrOutras;
	}
	public void setVr_desconto(double vrDesconto) {
		vr_desconto = vrDesconto;
	}
	public void setVr_frete(double vrFrete) {
		vr_frete = vrFrete;
	}
	public void setVr_seguro(double vrSeguro) {
		vr_seguro = vrSeguro;
	}
	public void setVr_docref(double vrDocref) {
		vr_docref = vrDocref;
	}
	public void setBc_icms(double bcIcms) {
		bc_icms = bcIcms;
	}
	public void setVr_icms(double vrIcms) {
		vr_icms = vrIcms;
	}
	public void setBc_icmsst(double bcIcmsst) {
		bc_icmsst = bcIcmsst;
	}
	public void setVr_icmsst(double vrIcmsst) {
		vr_icmsst = vrIcmsst;
	}
	public void setBc_ipi(double bcIpi) {
		bc_ipi = bcIpi;
	}
	public void setVr_ipi(double vrIpi) {
		vr_ipi = vrIpi;
	}
	public void setBc_pis(double bcPis) {
		bc_pis = bcPis;
	}
	public void setVr_pis(double vrPis) {
		vr_pis = vrPis;
	}
	public void setBc_cofins(double bcCofins) {
		bc_cofins = bcCofins;
	}
	public void setVr_cofins(double vrCofins) {
		vr_cofins = vrCofins;
	}
	public void setBc_ii(double bcIi) {
		bc_ii = bcIi;
	}
	public void setVr_ii(double vrIi) {
		vr_ii = vrIi;
	}
	public void setVr_iof(double vrIof) {
		vr_iof = vrIof;
	}
	public void setSimplesremessa(boolean simplesremessa) {
		this.simplesremessa = simplesremessa;
	}
	public void setFaturamentocliente(boolean faturamentocliente) {
		this.faturamentocliente = faturamentocliente;
	}
}
