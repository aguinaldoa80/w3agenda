package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.Modulos;
import br.com.linkcom.utils.StringUtils;

import com.ibm.icu.text.SimpleDateFormat;

public class GeralVO {

	private Integer sequencial;
	private Integer sequencialdetalhe;
	private Integer sequencialpagamento;
	private Integer sequencialcheque;
	private Money valortotal;
	private Integer totalregistro;
	private Integer totallinha;
	private Integer totallinhalote;
	private Money valoroutrasentidades;
	private Money valoratual;
	private Money atualizacaomonetaria;
	
	private Integer instrucaocobrancacodigo;
	private String instrucaocobrancanome;
	
	private Date dataatual;
	private Time horaatual;
	
	private Integer quantidadedocumento;
	private Integer sequencialarquivo;
	
	private Date datapagamento;
	private Integer tipopagamento;
	private Integer formapagamento;
	private String tipodocumento;
	private String tipotributo;
	private String finalidadeted;
	private String finalidadedoc;
	private String codigoauxiliar1;
	private String nomeauxiliar1;
	private String codigoauxiliar2;
	private String nomeauxiliar2;
	private Boolean sacadoravalista;
	
	public GeralVO(){
		this.dataatual = new Date(System.currentTimeMillis());
		this.horaatual = new Time(System.currentTimeMillis());
		this.sequencialarquivo = 0;
	}
	
	public Integer getSequencial() {
		return sequencial;
	}
	
	public Integer getSequencialdetalhe() {
		return sequencialdetalhe;
	}
	
	public Integer getSequencialpagamento() {
		return sequencialpagamento;
	}
	
	public Integer getSequencialcheque() {
		return sequencialcheque;
	}
	
	public Money getValortotal() {
		return valortotal;
	}

	public Integer getTotalregistro() {
		return totalregistro;
	}

	public Integer getTotallinha() {
		return totallinha;
	}

	public Integer getTotallinhalote() {
		return totallinhalote;
	}

	public Integer getInstrucaocobrancacodigo() {
		return instrucaocobrancacodigo;
	}
	
	public String getInstrucaocobrancanome() {
		return instrucaocobrancanome;
	}

	public Date getDataatual() {
		return dataatual;
	}
	
	public Time getHoraatual() {
		return horaatual;
	}
	
	public Integer getQuantidadedocumento() {
		return quantidadedocumento;
	}
	
	public Integer getSequencialarquivo() {
		return sequencialarquivo;
	}
	
	public Integer getTipopagamento() {
		return tipopagamento;
	}

	public Integer getFormapagamento() {
		return formapagamento;
	}

	public String getFinalidadeted() {
		return finalidadeted;
	}

	public String getFinalidadedoc() {
		return finalidadedoc;
	}
	
	public String getCodigoauxiliar1() {
		return codigoauxiliar1;
	}
	
	public String getCodigoauxiliar2() {
		return codigoauxiliar2;
	}
	
	public String getNomeauxiliar1() {
		return nomeauxiliar1;
	}
	
	public String getNomeauxiliar2() {
		return nomeauxiliar2;
	}

	public Boolean getSacadoravalista() {
		return sacadoravalista;
	}

	public Date getDatapagamento() {
		return datapagamento;
	}
	
	public Money getValoroutrasentidades() {
		return valoroutrasentidades;
	}

	public void setValoroutrasentidades(Money valoroutrasentidades) {
		this.valoroutrasentidades = valoroutrasentidades;
	}

	public Money getValoratual() {
		return valoratual;
	}

	public void setValoratual(Money valoratual) {
		this.valoratual = valoratual;
	}

	public Money getAtualizacaomonetaria() {
		return atualizacaomonetaria;
	}

	public void setAtualizacaomonetaria(Money atualizacaomonetaria) {
		this.atualizacaomonetaria = atualizacaomonetaria;
	}

	public void setSequencialcheque(Integer sequencialcheque) {
		this.sequencialcheque = sequencialcheque;
	}
	
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	
	public void setSequencialdetalhe(Integer sequencialdetalhe) {
		this.sequencialdetalhe = sequencialdetalhe;
	}
	
	public void setSequencialpagamento(Integer sequencialpagamento) {
		this.sequencialpagamento = sequencialpagamento;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}

	public void setTotalregistro(Integer totalregistro) {
		this.totalregistro = totalregistro;
	}

	public void setTotallinha(Integer totallinha) {
		this.totallinha = totallinha;
	}

	public void setTotallinhalote(Integer totallinhalote) {
		this.totallinhalote = totallinhalote;
	}

	public void setInstrucaocobrancacodigo(Integer instrucaocobrancacodigo) {
		this.instrucaocobrancacodigo = instrucaocobrancacodigo;
	}
	
	public void setInstrucaocobrancanome(String instrucaocobrancanome) {
		this.instrucaocobrancanome = instrucaocobrancanome;
	}
	public void setDataatual(Date dataatual) {
		this.dataatual = dataatual;
	}
	
	public void setHoraatual(Time horaatual) {
		this.horaatual = horaatual;
	}
	
	public void setQuantidadedocumento(Integer quantidadedocumento) {
		this.quantidadedocumento = quantidadedocumento;
	}
	
	public void setSequencialarquivo(Integer sequencialarquivo) {
		this.sequencialarquivo = sequencialarquivo;
	}
	
	public void setDatapagamento(Date datapagamento) {
		this.datapagamento = datapagamento;
	}
	
	public void setTipopagamento(Integer tipopagamento) {
		this.tipopagamento = tipopagamento;
	}

	public void setFormapagamento(Integer formapagamento) {
		this.formapagamento = formapagamento;
	}

	public void setFinalidadeted(String finalidadeted) {
		this.finalidadeted = finalidadeted;
	}

	public void setFinalidadedoc(String finalidadedoc) {
		this.finalidadedoc = finalidadedoc;
	}
	
	public void setCodigoauxiliar1(String codigoauxiliar1) {
		this.codigoauxiliar1 = codigoauxiliar1;
	}
	
	public void setCodigoauxiliar2(String codigoauxiliar2) {
		this.codigoauxiliar2 = codigoauxiliar2;
	}
	
	public void setNomeauxiliar1(String nomeauxiliar1) {
		this.nomeauxiliar1 = nomeauxiliar1;
	}
	
	public void setNomeauxiliar2(String nomeauxiliar2) {
		this.nomeauxiliar2 = nomeauxiliar2;
	}

	public void setSacadoravalista(Boolean sacadoravalista) {
		this.sacadoravalista = sacadoravalista;
	}
	
	public String getTipodocumento() {
		return tipodocumento;
	}
	
	public String getTipotributo() {
		return tipotributo;
	}
	
	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	
	public void setTipotributo(String tipotributo) {
		this.tipotributo = tipotributo;
	}
	
	//--------------------------------------------------------------------------
	//Fun��es
	

	/**
	 * Realiza o c�lculo do m�dulo 10
	 * 
	 * @param numero
	 * 
	 */
	@DisplayName("modulo10(numero)")
	public Integer modulo10(String numero) {	
		if (numero == null || "".equals(numero))
			return null;
		
		String s = "";
		int indexador;		
		int soma     = 0;
		int mult     = 0;
		int result   = 0;		
		int tamanho  = numero.length();
		for (indexador  = 1; indexador <= tamanho; indexador++) {
			mult = (indexador  % 2) + 1; //Sequ�ncia alternada de 2 e 1 
			s   += String.valueOf((Integer.parseInt(numero.substring(tamanho - indexador, tamanho - indexador + 1)) * mult));
		}
		soma    = 0;
		tamanho = s.length();
		for (indexador = 1; indexador <= tamanho; indexador++) {
			soma += Integer.parseInt(s.substring(indexador - 1, indexador));
		}
		result = 10 - (soma % 10);
		
		if (result == 10) 
			result = 0;
		
		return result;
	}
	
	/**
	 * Realiza o c�lculo do m�dulo 11
	 * 
	 * @param numero
	 * 	 
	 */
	@DisplayName("modulo11(numero)")
	public Integer modulo11(String numero) {
		if (numero == null || "".equals(numero))
			return null;
		
		int indexador;
		int soma    = 0;
		int mult    = 0;
		int result  = 0;		
		int tamanho = numero.length();
		
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 8) + 2; //Sequ�ncia de 2 a 9
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		result = 11 - (soma % 11);
		
		if (result>9){
			result = 0; 
		}	
		
		return result;
	}
	
	/**
	 * Realiza o c�lculo do m�dulo 11 do bradesco
	 * 
	 * @param numero
	 * 	 
	 */
	@DisplayName("modulo11Bradesco(numero)")
	public String modulo11Bradesco(String numero){
		
		int indexador;
		int soma = 0;
		int mult = 0;
		int tamanho = numero.length();
	
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 6) + 2; //Sequ�ncia de 2 a 7
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		
		String result = String.valueOf(11 - (soma % 11));
		
		if (result.equals("10")){
			result = "P";
		}else if (result.equals("11")){
			result = "0";			
		}
		
		return result;
	}
	
	@DisplayName("modulo11BANCOOB(numero, agencia, codigocedente)")
	public String modulo11BANCOOB(String numero, String agencia, String codigocedente){
		String valor = StringUtils.stringCheia(agencia, "0", 4, false);
		valor += StringUtils.stringCheia(codigocedente, "0", 10, false);
		valor += StringUtils.stringCheia(numero, "0", 7, false);
		
		String formula = "319731973197319731973";
		
		int soma = 0;
		for (int i = 0; i < 21; i++) {
			soma += Integer.parseInt(valor.substring(i, i+1)) * Integer.parseInt(formula.substring(i, i+1));
		}
		
		int resto = soma % 11;
		
		if(resto < 2) return "0";
		else return (11 - resto) + "";
	}
	
	/**
	 * Realiza o c�lculo do m�dulo 11 do uniprime
	 * 
	 * @param numero
	 * 	 
	 */
	@DisplayName("modulo11Uniprime(numero, isCodBarra)")
	public String modulo11Uniprime(String numero, Boolean isCodBarra){
		return Boolean.TRUE.equals(isCodBarra)? String.valueOf(Modulos.modulo11(numero, 5)): String.valueOf(Modulos.Modulo11UniprimeBase7(numero));
	}
	
	@DisplayName("digitoDuploBanrisul(numero)")
	public String digitoDuploBanrisul(String numero){
		int modulo10 = modulo10(numero);
		String modulo11;
		
		while ((modulo11 = modulo11Banrisul(numero + modulo10)).equals("")){
			if (modulo10==9){
				modulo10 = 0;
			}
			modulo10++;
		}
		
		return modulo10 + modulo11;
	}
	
	@DisplayName("modulo11Banrisul(numero)")
	public static String modulo11Banrisul(String numero){
		int indexador;
		int soma = 0;
		int mult = 0;
		int tamanho = numero.length();
	
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 6) + 2; //Sequ�ncia de 2 a 7
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		
		//1 - Caso o somat�rio obtido seja menor que 11, considerar como resto da divis�o o pr�prio somat�rio.
		
		//2 - Caso o resto obtido no c�lculo do m�dulo 11 seja igual a 1, considera-se o DV inv�lido. 
		//Ent�o soma-se, ent�o, 1 ao DV obtido do m�dulo 10 e refaz-se o c�lculo do m�dulo 11. 
		//Se o d�gito obtido pelo m�dulo 10 era igual a 9, considera-se ent�o (9+1=10) DV inv�lido. 
		//Neste caso, o DV do m�dulo 10 automaticamente ser� igual a 0 e procede-se assim novo c�lculo pelo m�dulo 11.
		
		//3 - Caso o resto obtido no c�lculo do m�dulo 11 seja 0, o segundo NC ser� igual ao pr�prio resto.
		
		int resto = soma<11 ? soma : soma % 11;
		
		if (resto==1){
			return "";
		}else {
			if (resto==0){
				return "0";
			}else {
				return String.valueOf(11 - resto); 
			}
		}
	}
	
	@DisplayName("tiraacento(texto)")
	public String tiraacento(String s){
		String string = Util.strings.tiraAcento(s);
		return org.apache.commons.lang.StringUtils.replaceChars(string, ".,;:'\"#$%&*()+{}[]/\\", "                    ");
	}
	
	@DisplayName("repeticao(texto, tamanho)")
	public String repeticao(String s, int tamanho){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tamanho; i++) {
			sb.append(s);
		}
		return sb.toString();
	}
	
	@DisplayName("substring(texto, inicio, fim)")
	public String substring(String s, int beginIndex, int endIndex){
		if(s != null){
			if (endIndex > s.length()) endIndex = s.length();
			if (beginIndex < 0) beginIndex = 0;			
			return s.substring(beginIndex, endIndex);
		} else return null;
	}
	
	@DisplayName("lancarexcecao(mensagem)")
	public SinedException lancarexcecao(String mensagem) throws Exception{
		return new SinedException(mensagem);
	}
	
	@DisplayName("completar(texto, completarCom, tamanho, direita)")
	public String completar(String texto, String completarCom, Integer tamanho, boolean direita){
		return StringUtils.stringCheia(texto, completarCom, tamanho, direita);
	}
	
	@DisplayName("addDias(data, dias)")
	public Date addDias(Date data, int dias){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		return new Date(calendar.getTimeInMillis());
	}
	
	@DisplayName("multiplicar(valor1, valor2)")
	public Money multiplicar(String valor1, String valor2){
		Money money1 = new Money(valor1 != null && !"".equals(valor1) ? new Double(valor1.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		Money money2 = new Money(valor2 != null && !"".equals(valor2) ? new Double(valor2.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		
		return money1.multiply(money2);
	}
	
	@DisplayName("dividir(valor1, valor2)")
	public Money dividir(String valor1, String valor2){
		Money money1 = new Money(valor1 != null && !"".equals(valor1) ? new Double(valor1.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		Money money2 = new Money(valor2 != null && !"".equals(valor2) ? new Double(valor2.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		
		return money1.divide(money2);
	}
	
	@DisplayName("truncar(valor, qtdeCasasDecimais)")
	public Money truncar(String valor, Integer qtdeCasasDecimais){
		Money money = new Money(valor != null && !"".equals(valor) ? new Double(valor.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		return new Money(SinedUtil.truncateFormat(money.getValue().doubleValue(), qtdeCasasDecimais));
	}
	
	@DisplayName("round(valor, qtdeCasasDecimais)")
	public Money round(String valor, Integer qtdeCasasDecimais){
		Money money = new Money(valor != null && !"".equals(valor) ? new Double(valor.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		return new Money(SinedUtil.round(money.getValue().doubleValue(), qtdeCasasDecimais));
	}
	
	@DisplayName("somar(valor1, valor2)")
	public Money somar(String valor1, String valor2){
		Money money1 = new Money(valor1 != null && !"".equals(valor1) ? new Double(valor1.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		Money money2 = new Money(valor2 != null && !"".equals(valor2) ? new Double(valor2.toString().replaceAll("\\.", "").replace(",", ".")) : 0d);
		
		return money1.add(money2);
	}
	
	@DisplayName("length(texto)")
	public int length(String str){
		if(str == null) return 0;
		return str.length();
	}
	
	@DisplayName("modulo11brb(String numero)")
	public String modulo11brb(String numero){
		
		int indexador;
		int soma = 0;
		int mult = 0;
		int tamanho = numero.length();
	
		for (indexador = 0; indexador < (numero.length()); indexador++) {
			mult = (indexador % 6) + 2; //Sequ�ncia de 2 a 7
			try {
				soma = soma + (Integer.parseInt(numero.substring(tamanho - indexador - 1, tamanho - indexador)) * mult);
			}
			catch (NumberFormatException e){
				e.printStackTrace(); 
				throw e;
			}
		}
		
		String result = String.valueOf(soma % 11);
		
		if (result.equals("0")){
			result = "0";
		}else if (result.equals("1")){
			String d1 = numero.substring(numero.length()-1, numero.length());
			Integer novoD1 = Integer.parseInt(d1)+1;
			if(novoD1 == 10){
				novoD1 = 0;
				numero = numero.substring(0, numero.length()-1)+"0";
			}
			numero = numero.substring(0, numero.length()-1) + String.valueOf(novoD1);
			result = modulo11brb(numero);
		}else{
			result = String.valueOf(11-Integer.parseInt(result));
		}
		
		return result;
	}

	@DisplayName("formatadata(data, formato)")
	public String formatadata(Date data, String formato){
		return data != null? new SimpleDateFormat(formato).format(data): "";
	}
}
