package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;

public class BancoVO {

	private String nome;
	private String numero;
	
	public String getNome() {
		return nome;
	}
	public String getNumero() {
		return numero;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}	
	
}
