package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;

import java.util.ArrayList;
import java.util.List;

public class RemessaVO {

	private GeralVO geralVO;
	private BancoVO bancoVO;
	private ContaBancariaVO contaBancariaVO;
	private EmpresaVO empresaVO;
	private EmpresaVO empresatitularVO;
	private List<DocumentoVO> listaDocumentoVO = new ArrayList<DocumentoVO>();
	private List<MovimentacaoVO> listaMovimentacaoVO = new ArrayList<MovimentacaoVO>();
	
	//Campos para controle durante a gera��o do arquivo de remessa
	
	private boolean utilizouSequencial = false;
	private boolean utilizouSequencialpagamento = false;
	private boolean utilizouSequencialcheque = false;
	private List<String> listaErro = new ArrayList<String>();
	
	//Getters and Setters
	
	public GeralVO getGeralVO() {
		return geralVO;
	}	
	public BancoVO getBancoVO() {
		return bancoVO;
	}
	public ContaBancariaVO getContaBancariaVO() {
		return contaBancariaVO;
	}
	public EmpresaVO getEmpresaVO() {
		return empresaVO;
	}
	public List<DocumentoVO> getListaDocumentoVO() {
		return listaDocumentoVO;
	}
	public List<MovimentacaoVO> getListaMovimentacaoVO() {
		return listaMovimentacaoVO;
	}
	public void setGeralVO(GeralVO geralVO) {
		this.geralVO = geralVO;
	}
	public void setBancoVO(BancoVO bancoVO) {
		this.bancoVO = bancoVO;
	}
	public void setContaBancariaVO(ContaBancariaVO contaBancariaVO) {
		this.contaBancariaVO = contaBancariaVO;
	}
	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}
	public void setListaDocumentoVO(List<DocumentoVO> listaDocumentoVO) {
		this.listaDocumentoVO = listaDocumentoVO;
	}
	public void setListaMovimentacaoVO(List<MovimentacaoVO> listaMovimentacaoVO) {
		this.listaMovimentacaoVO = listaMovimentacaoVO;
	}
	
	//Campos para controle durante a gera��o do arquivo de remessa
	
	public boolean isUtilizouSequencial() {
		return utilizouSequencial;
	}
	
	public void setUtilizouSequencial(boolean utilizouSequencial) {
		this.utilizouSequencial = utilizouSequencial;
	}
	
	public boolean isUtilizouSequencialpagamento() {
		return utilizouSequencialpagamento;
	}
	
	public boolean isUtilizouSequencialcheque() {
		return utilizouSequencialcheque;
	}
	
	public void setUtilizouSequencialpagamento(boolean utilizouSequencialpagamento) {
		this.utilizouSequencialpagamento = utilizouSequencialpagamento;
	}
	
	public void setUtilizouSequencialcheque(boolean utilizouSequencialcheque) {
		this.utilizouSequencialcheque = utilizouSequencialcheque;
	}
	
	public List<String> getListaErro() {
		return listaErro;
	}
	
	public void setListaErro(List<String> listaErro) {
		this.listaErro = listaErro;
	}
	public EmpresaVO getEmpresatitularVO() {
		return empresatitularVO;
	}
	public void setEmpresatitularVO(EmpresaVO empresatitularVO) {
		this.empresatitularVO = empresatitularVO;
	}
}
