package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class MovimentacaoVO {
	
	private String bordero;
	private String chequenumero;
	private String chequelinhanumerica;
	private String chequeemitente;
	private String chequecpfcnpj;
	private Date chequebompara;
	private Integer chequebanco;
	private Integer chequeagencia;
	private Integer chequeconta;
	private Money chequevalor;
	private Date dtmovimentacao;
	private Money valor;
	
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	public Money getValor() {
		return valor;
	}
	public String getBordero() {
		return bordero;
	}
	public String getChequelinhanumerica() {
		return chequelinhanumerica;
	}
	public Money getChequevalor() {
		return chequevalor;
	}
	public String getChequenumero() {
		return chequenumero;
	}
	public String getChequeemitente() {
		return chequeemitente;
	}
	public Date getChequebompara() {
		return chequebompara;
	}
	public Integer getChequebanco() {
		return chequebanco;
	}
	public Integer getChequeagencia() {
		return chequeagencia;
	}
	public Integer getChequeconta() {
		return chequeconta;
	}
	public String getChequecpfcnpj() {
		return chequecpfcnpj;
	}
	public void setChequecpfcnpj(String chequecpfcnpj) {
		this.chequecpfcnpj = chequecpfcnpj;
	}
	public void setChequenumero(String chequenumero) {
		this.chequenumero = chequenumero;
	}
	public void setChequeemitente(String chequeemitente) {
		this.chequeemitente = chequeemitente;
	}
	public void setChequebompara(Date chequebompara) {
		this.chequebompara = chequebompara;
	}
	public void setChequebanco(Integer chequebanco) {
		this.chequebanco = chequebanco;
	}
	public void setChequeagencia(Integer chequeagencia) {
		this.chequeagencia = chequeagencia;
	}
	public void setChequeconta(Integer chequeconta) {
		this.chequeconta = chequeconta;
	}
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}	
	public void setBordero(String bordero) {
		this.bordero = bordero;
	}
	public void setChequelinhanumerica(String chequelinhanumerica) {
		this.chequelinhanumerica = chequelinhanumerica;
	}
	public void setChequevalor(Money chequevalor) {
		this.chequevalor = chequevalor;
	}	
}