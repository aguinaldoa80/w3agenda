package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;


public class ContaBancariaVO {

	private String agencia;
	private String dvagencia;
	private String conta;
	private String dvconta;
	private String convenio;
	private String conveniolider;
	private String carteira;
	private String codigocarteira;
	private String titular;
	private String sequencial;
	private String sequencialcheque; 
	private String msgboleto1;
	private String msgboleto2;
	private String instrucao1;
	private String instrucao2;
	private String tipocobranca;
	private String localpagamento;
	private String especiedocumento;
	private Integer qtdediasprotesto;
	private Integer qtdediasdevolucao;
	private String codigotransmissao;
	private String aceito;
	private String idemissaoboleto;
	private String iddistribuicao;
	private Integer qtdediasdistribuicao;
	private String especiedocumentoremessa;
	private Integer cdempresatitular;
	private String variacaocarteira;
	private String codigoempresasispag;
	
	public String getAgencia() {
		return agencia;
	}
	public String getDvagencia() {
		return dvagencia;
	}
	public String getConta() {
		return conta;
	}
	public String getDvconta() {
		return dvconta;
	}
	public String getConvenio() {
		return convenio;
	}
	public String getConveniolider() {
		return conveniolider;
	}
	public String getCarteira() {
		return carteira;
	}
	public String getCodigocarteira() {
		return codigocarteira;
	}
	public String getTitular() {
		return titular;
	}
	public String getSequencial() {
		return sequencial;
	}
	public String getMsgboleto1() {
		return msgboleto1;
	}
	public String getMsgboleto2() {
		return msgboleto2;
	}
	public String getInstrucao1() {
		return instrucao1;
	}
	public String getInstrucao2() {
		return instrucao2;
	}
	public String getTipocobranca() {
		return tipocobranca;
	}
	public String getLocalpagamento() {
		return localpagamento;
	}
	public String getEspeciedocumento() {
		return especiedocumento;
	}
	public Integer getQtdediasprotesto() {
		return qtdediasprotesto;
	}
	public Integer getQtdediasdevolucao() {
		return qtdediasdevolucao;
	}
	public String getCodigotransmissao() {
		return codigotransmissao;
	}
	public String getAceito() {
		return aceito;
	}
	public String getIdemissaoboleto() {
		return idemissaoboleto;
	}
	public String getIddistribuicao() {
		return iddistribuicao;
	}
	public Integer getQtdediasdistribuicao() {
		return qtdediasdistribuicao;
	}
	public String getEspeciedocumentoremessa() {
		return especiedocumentoremessa;
	}
	public Integer getCdempresatitular() {
		return cdempresatitular;
	}
	public String getVariacaocarteira() {
		return variacaocarteira;
	}
	public String getCodigoempresasispag() {
		return codigoempresasispag;
	}
	public String getSequencialcheque() {
		return sequencialcheque;
	}
	public void setSequencialcheque(String sequencialcheque) {
		this.sequencialcheque = sequencialcheque;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public void setDvconta(String dvconta) {
		this.dvconta = dvconta;
	}
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	public void setConveniolider(String conveniolider) {
		this.conveniolider = conveniolider;
	}
	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}
	public void setCodigocarteira(String codigocarteira) {
		this.codigocarteira = codigocarteira;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}
	public void setMsgboleto1(String msgboleto1) {
		this.msgboleto1 = msgboleto1;
	}
	public void setMsgboleto2(String msgboleto2) {
		this.msgboleto2 = msgboleto2;
	}
	public void setInstrucao1(String instrucao1) {
		this.instrucao1 = instrucao1;
	}
	public void setInstrucao2(String instrucao2) {
		this.instrucao2 = instrucao2;
	}
	public void setTipocobranca(String tipocobranca) {
		this.tipocobranca = tipocobranca;
	}
	public void setLocalpagamento(String localpagamento) {
		this.localpagamento = localpagamento;
	}
	public void setEspeciedocumento(String especiedocumento) {
		this.especiedocumento = especiedocumento;
	}
	public void setQtdediasprotesto(Integer qtdediasprotesto) {
		this.qtdediasprotesto = qtdediasprotesto;
	}
	public void setQtdediasdevolucao(Integer qtdediasdevolucao) {
		this.qtdediasdevolucao = qtdediasdevolucao;
	}
	public void setCodigotransmissao(String codigotransmissao) {
		this.codigotransmissao = codigotransmissao;
	}
	public void setAceito(String aceito) {
		this.aceito = aceito;
	}
	public void setIdemissaoboleto(String idemissaoboleto) {
		this.idemissaoboleto = idemissaoboleto;
	}
	public void setIddistribuicao(String iddistribuicao) {
		this.iddistribuicao = iddistribuicao;
	}
	public void setQtdediasdistribuicao(Integer qtdediasdistribuicao) {
		this.qtdediasdistribuicao = qtdediasdistribuicao;
	}
	public void setEspeciedocumentoremessa(String especiedocumentoremessa) {
		this.especiedocumentoremessa = especiedocumentoremessa;
	}
	public void setCdempresatitular(Integer cdempresatitular) {
		this.cdempresatitular = cdempresatitular;
	}
	public void setVariacaocarteira(String variacaocarteira) {
		this.variacaocarteira = variacaocarteira;
	}
	public void setCodigoempresasispag(String codigoempresasispag) {
		this.codigoempresasispag = codigoempresasispag;
	}
}
