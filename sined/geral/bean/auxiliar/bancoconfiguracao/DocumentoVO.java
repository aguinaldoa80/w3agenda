package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;

public class DocumentoVO {

	private Integer cddocumento;
	private Date dtvencimento;
	private Date dtemissao;
	private Money valor;
	private Money valoratual;
	private String documento;
	private String nossonumero;
	private String descricao;
	private String mensagem1;
	private String mensagem2;
	private String mensagem3;
	private String mensagem4;
	private String mensagem5;
	private String mensagem6;
	
	private Money desagiopercentual;
	private Money descontopercentual;
	private Money jurospercentual;
	private Money multapercentual;
	private Money taxaboletopercentual;
	private Money termopercentual;
	private Money taxamovimentopercentual;
	
	private Money desagiovalor;
	private Money descontovalor;
	private Money jurosvalor;
	private Money multavalor;
	private Money taxaboletovalor;
	private Money termovalor;
	private Money taxamovimentovalor;
	
	private Money desagiovalorcalculado;
	private Money descontovalorcalculado;
	private Money jurosvalorcalculado;
	private Money multavalorcalculado;
	private Money taxaboletovalorcalculado;
	private Money termovalorcalculado;
	private Money taxamovimentovalorcalculado;
	
	private Date desagiodtlimite;
	private Date descontodtlimite;
	private Date jurosdtlimite;
	private Date multadtlimite;
	private Date taxaboletodtlimite;
	private Date termodtlimite;
	private Date taxamovimentodtlimite;
	
	private String desagiotipo;
	private String descontotipo;
	private String jurostipo;
	private String multatipo;
	private String taxaboletotipo;
	private String termotipo;	
	private String taxamovimentotipo;
	
	private Integer cddocumentotipo;
	private String documentotipo;
	private String tipotributo;
	
	protected String codigoreceita;
	protected String competencia;
	protected Money valoroutrasentidades;
	protected Money atualizacaomonetaria;
	protected Date periodoapuracao;
	protected String numeroreferencia;
	protected Money valorreceita;
	protected Money percentualreceita;
	protected String identificacaotributo;
	protected String inscestcodmunicipnumdec;
	protected String dividaativanumetiqueta;
	protected String numparcelanotificacao;
	protected Integer anobase;
	protected Date mesano;
	protected String renavam;
	protected String siglauf;
	protected String codigomunicipio;
	protected String placa;
	protected String opcaopagamento;
	

	protected String identificadordofgts;
	protected String lacreconectividadesocial;
	protected String digitolacreconectividadesocial;
	protected Integer codigojurosmora;
	
	//Campos SisPag
	private String codigobarra;
	
	private PessoaVO pessoaVO;
	private EmpresaVO empresaDOC;
	private EmpresaVO empresaTITULAR;
	private Boolean sacadoravalista;
	
	
	private List<Integer> idsAgrupamento = new ArrayList<Integer>();

	public Integer getCddocumento() {
		return cddocumento;
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Date getDtemissao() {
		return dtemissao;
	}

	public Money getValor() {
		return valor;
	}
	
	public Money getValoratual() {
		return valoratual;
	}

	public String getDocumento() {
		return documento;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getMensagem1() {
		return mensagem1;
	}

	public String getMensagem2() {
		return mensagem2;
	}

	public String getMensagem3() {
		return mensagem3;
	}

	public String getMensagem4() {
		return mensagem4;
	}

	public String getMensagem5() {
		return mensagem5;
	}

	public String getMensagem6() {
		return mensagem6;
	}

	public Money getDesagiopercentual() {
		return desagiopercentual;
	}

	public Money getDescontopercentual() {
		return descontopercentual;
	}

	public Money getJurospercentual() {
		return jurospercentual;
	}

	public Money getMultapercentual() {
		return multapercentual;
	}

	public Money getTaxaboletopercentual() {
		return taxaboletopercentual;
	}

	public Money getTermopercentual() {
		return termopercentual;
	}

	public Money getDesagiovalor() {
		return desagiovalor;
	}

	public Money getDescontovalor() {
		return descontovalor;
	}

	public Money getJurosvalor() {
		return jurosvalor;
	}

	public Money getMultavalor() {
		return multavalor;
	}

	public Money getTaxaboletovalor() {
		return taxaboletovalor;
	}

	public Money getTermovalor() {
		return termovalor;
	}

	public Date getDesagiodtlimite() {
		return desagiodtlimite;
	}

	public Date getDescontodtlimite() {
		return descontodtlimite;
	}

	public Date getJurosdtlimite() {
		return jurosdtlimite;
	}

	public Date getMultadtlimite() {
		return multadtlimite;
	}

	public Date getTaxaboletodtlimite() {
		return taxaboletodtlimite;
	}

	public Date getTermodtlimite() {
		return termodtlimite;
	}
	
	public String getCodigobarra() {
		return codigobarra;
	}
	
	public PessoaVO getPessoaVO() {
		return pessoaVO;
	}
	
	public EmpresaVO getEmpresaDOC() {
		return empresaDOC;
	}
	
	public String getDesagiotipo() {
		return desagiotipo;
	}

	public String getDescontotipo() {
		return descontotipo;
	}

	public String getJurostipo() {
		return jurostipo;
	}

	public String getMultatipo() {
		return multatipo;
	}

	public String getTaxaboletotipo() {
		return taxaboletotipo;
	}

	public String getTermotipo() {
		return termotipo;
	}
	
	public Money getTaxamovimentovalor() {
		return taxamovimentovalor;
	}
	
	public Money getTaxamovimentopercentual() {
		return taxamovimentopercentual;
	}

	public Date getTaxamovimentodtlimite() {
		return taxamovimentodtlimite;
	}

	public String getTaxamovimentotipo() {
		return taxamovimentotipo;
	}

	public Money getDesagiovalorcalculado() {
		return desagiovalorcalculado;
	}

	public Money getDescontovalorcalculado() {
		return descontovalorcalculado;
	}

	public Money getJurosvalorcalculado() {
		return jurosvalorcalculado;
	}

	public Money getMultavalorcalculado() {
		return multavalorcalculado;
	}

	public Money getTaxaboletovalorcalculado() {
		return taxaboletovalorcalculado;
	}

	public Money getTermovalorcalculado() {
		return termovalorcalculado;
	}

	public Money getTaxamovimentovalorcalculado() {
		return taxamovimentovalorcalculado;
	}
	
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	
	public String getDocumentotipo() {
		return documentotipo;
	}
	
	public String getTipotributo() {
		return tipotributo;
	}

	public List<Integer> getIdsAgrupamento() {
		return idsAgrupamento;
	}
	
	public String getNossonumero() {
		return nossonumero;
	}

	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	
	public void setIdsAgrupamento(List<Integer> idsAgrupamento) {
		this.idsAgrupamento = idsAgrupamento;
	}

	public void setDesagiovalorcalculado(Money desagiovalorcalculado) {
		this.desagiovalorcalculado = desagiovalorcalculado;
	}

	public void setDescontovalorcalculado(Money descontovalorcalculado) {
		this.descontovalorcalculado = descontovalorcalculado;
	}

	public void setJurosvalorcalculado(Money jurosvalorcalculado) {
		this.jurosvalorcalculado = jurosvalorcalculado;
	}

	public void setMultavalorcalculado(Money multavalorcalculado) {
		this.multavalorcalculado = multavalorcalculado;
	}

	public void setTaxaboletovalorcalculado(Money taxaboletovalorcalculado) {
		this.taxaboletovalorcalculado = taxaboletovalorcalculado;
	}

	public void setTermovalorcalculado(Money termovalorcalculado) {
		this.termovalorcalculado = termovalorcalculado;
	}

	public void setTaxamovimentovalorcalculado(Money taxamovimentovalorcalculado) {
		this.taxamovimentovalorcalculado = taxamovimentovalorcalculado;
	}

	public void setTaxamovimentopercentual(Money taxamovimentopercentual) {
		this.taxamovimentopercentual = taxamovimentopercentual;
	}

	public void setTaxamovimentodtlimite(Date taxamovimentodtlimite) {
		this.taxamovimentodtlimite = taxamovimentodtlimite;
	}

	public void setTaxamovimentotipo(String taxamovimentotipo) {
		this.taxamovimentotipo = taxamovimentotipo;
	}

	public void setTaxamovimentovalor(Money taxamovimentovalor) {
		this.taxamovimentovalor = taxamovimentovalor;
	}
	
	public void setDesagiotipo(String desagiotipo) {
		this.desagiotipo = desagiotipo;
	}

	public void setDescontotipo(String descontotipo) {
		this.descontotipo = descontotipo;
	}

	public void setJurostipo(String jurostipo) {
		this.jurostipo = jurostipo;
	}

	public void setMultatipo(String multatipo) {
		this.multatipo = multatipo;
	}

	public void setTaxaboletotipo(String taxaboletotipo) {
		this.taxaboletotipo = taxaboletotipo;
	}

	public void setTermotipo(String termotipo) {
		this.termotipo = termotipo;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValoratual(Money valoratual) {
		this.valoratual = valoratual;
	}
	
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}

	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}

	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}

	public void setMensagem4(String mensagem4) {
		this.mensagem4 = mensagem4;
	}

	public void setMensagem5(String mensagem5) {
		this.mensagem5 = mensagem5;
	}

	public void setMensagem6(String mensagem6) {
		this.mensagem6 = mensagem6;
	}

	public void setDesagiopercentual(Money desagiopercentual) {
		this.desagiopercentual = desagiopercentual;
	}

	public void setDescontopercentual(Money descontopercentual) {
		this.descontopercentual = descontopercentual;
	}

	public void setJurospercentual(Money jurospercentual) {
		this.jurospercentual = jurospercentual;
	}

	public void setMultapercentual(Money multapercentual) {
		this.multapercentual = multapercentual;
	}

	public void setTaxaboletopercentual(Money taxaboletopercentual) {
		this.taxaboletopercentual = taxaboletopercentual;
	}

	public void setTermopercentual(Money termopercentual) {
		this.termopercentual = termopercentual;
	}

	public void setDesagiovalor(Money desagiovalor) {
		this.desagiovalor = desagiovalor;
	}

	public void setDescontovalor(Money descontovalor) {
		this.descontovalor = descontovalor;
	}

	public void setJurosvalor(Money jurosvalor) {
		this.jurosvalor = jurosvalor;
	}

	public void setMultavalor(Money multavalor) {
		this.multavalor = multavalor;
	}

	public void setTaxaboletovalor(Money taxaboletovalor) {
		this.taxaboletovalor = taxaboletovalor;
	}

	public void setTermovalor(Money termovalor) {
		this.termovalor = termovalor;
	}

	public void setDesagiodtlimite(Date desagiodtlimite) {
		this.desagiodtlimite = desagiodtlimite;
	}

	public void setDescontodtlimite(Date descontodtlimite) {
		this.descontodtlimite = descontodtlimite;
	}

	public void setJurosdtlimite(Date jurosdtlimite) {
		this.jurosdtlimite = jurosdtlimite;
	}

	public void setMultadtlimite(Date multadtlimite) {
		this.multadtlimite = multadtlimite;
	}

	public void setTaxaboletodtlimite(Date taxaboletodtlimite) {
		this.taxaboletodtlimite = taxaboletodtlimite;
	}

	public void setTermodtlimite(Date termodtlimite) {
		this.termodtlimite = termodtlimite;
	}
	
	public void setCodigobarra(String codigobarra) {
		this.codigobarra = codigobarra;
	}

	public void setPessoaVO(PessoaVO pessoaVO) {
		this.pessoaVO = pessoaVO;
	}
	
	public void setEmpresaDOC(EmpresaVO empresaDOC) {
		this.empresaDOC = empresaDOC;
	}
	
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	
	public void setDocumentotipo(String documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setTipotributo(String tipotributo) {
		this.tipotributo = tipotributo;
	}

	public Boolean getSacadoravalista() {
		return sacadoravalista;
	}

	public void setSacadoravalista(Boolean sacadoravalista) {
		this.sacadoravalista = sacadoravalista;
	}

	public EmpresaVO getEmpresaTITULAR() {
		return empresaTITULAR;
	}

	public void setEmpresaTITULAR(EmpresaVO empresaTITULAR) {
		this.empresaTITULAR = empresaTITULAR;
	}

	public String getCodigoreceita() {
		return codigoreceita;
	}

	public String getCompetencia() {
		return competencia;
	}

	public Money getValoroutrasentidades() {
		return valoroutrasentidades;
	}

	public Money getAtualizacaomonetaria() {
		return atualizacaomonetaria;
	}

	public Date getPeriodoapuracao() {
		return periodoapuracao;
	}

	public String getNumeroreferencia() {
		return numeroreferencia;
	}

	public Money getValorreceita() {
		return valorreceita;
	}

	public Money getPercentualreceita() {
		return percentualreceita;
	}

	public String getIdentificacaotributo() {
		return identificacaotributo;
	}

	public String getInscestcodmunicipnumdec() {
		return inscestcodmunicipnumdec;
	}

	public String getDividaativanumetiqueta() {
		return dividaativanumetiqueta;
	}

	public String getNumparcelanotificacao() {
		return numparcelanotificacao;
	}

	public Integer getAnobase() {
		return anobase;
	}

	public Date getMesano() {
		return mesano;
	}

	public String getRenavam() {
		return renavam;
	}

	public String getSiglauf() {
		return siglauf;
	}

	public String getCodigomunicipio() {
		return codigomunicipio;
	}

	public String getPlaca() {
		return placa;
	}

	public String getOpcaopagamento() {
		return opcaopagamento;
	}

	public void setCodigoreceita(String codigoreceita) {
		this.codigoreceita = codigoreceita;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public void setValoroutrasentidades(Money valoroutrasentidades) {
		this.valoroutrasentidades = valoroutrasentidades;
	}

	public void setAtualizacaomonetaria(Money atualizacaomonetaria) {
		this.atualizacaomonetaria = atualizacaomonetaria;
	}

	public void setPeriodoapuracao(Date periodoapuracao) {
		this.periodoapuracao = periodoapuracao;
	}

	public void setNumeroreferencia(String numeroreferencia) {
		this.numeroreferencia = numeroreferencia;
	}

	public void setValorreceita(Money valorreceita) {
		this.valorreceita = valorreceita;
	}

	public void setPercentualreceita(Money percentualreceita) {
		this.percentualreceita = percentualreceita;
	}

	public void setIdentificacaotributo(String identificacaotributo) {
		this.identificacaotributo = identificacaotributo;
	}

	public void setInscestcodmunicipnumdec(String inscestcodmunicipnumdec) {
		this.inscestcodmunicipnumdec = inscestcodmunicipnumdec;
	}

	public void setDividaativanumetiqueta(String dividaativanumetiqueta) {
		this.dividaativanumetiqueta = dividaativanumetiqueta;
	}

	public void setNumparcelanotificacao(String numparcelanotificacao) {
		this.numparcelanotificacao = numparcelanotificacao;
	}

	public void setAnobase(Integer anobase) {
		this.anobase = anobase;
	}
	
	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}

	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}

	public void setSiglauf(String siglauf) {
		this.siglauf = siglauf;
	}

	public void setCodigomunicipio(String codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setOpcaopagamento(String opcaopagamento) {
		this.opcaopagamento = opcaopagamento;
	}

	
	public String getIdentificadordofgts() {
		return identificadordofgts;
	}

	public void setIdentificadordofgts(String identificadordofgts) {
		this.identificadordofgts = identificadordofgts;
	}

	public String getLacreconectividadesocial() {
		return lacreconectividadesocial;
	}

	public void setLacreconectividadesocial(String lacreconectividadesocial) {
		this.lacreconectividadesocial = lacreconectividadesocial;
	}

	public String getDigitolacreconectividadesocial() {
		return digitolacreconectividadesocial;
	}

	public void setDigitolacreconectividadesocial(String digitolacreconectividadesocial) {
		this.digitolacreconectividadesocial = digitolacreconectividadesocial;
	}

	public Integer getCodigojurosmora() {
		return codigojurosmora;
	}
	
	public void setCodigojurosmora(Integer codigojurosmora) {
		this.codigojurosmora = codigojurosmora;
	}
	
}