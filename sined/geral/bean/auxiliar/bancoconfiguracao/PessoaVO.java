package br.com.linkcom.sined.geral.bean.auxiliar.bancoconfiguracao;


public class PessoaVO {

	private Integer cdpessoa;
	private String nome;
	private String cnpj;
	private String cpf;
	
	private String cnpjcpf;
	private Integer tipopessoa;
	
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	private String municipio;
	private String uf;
	
	private String banconumero;
	private String agencia;
	private String dvagencia;
	private String conta;
	private String dvconta;
	private Integer cdtipoconta;
	
	private String email;
	
	public String getNome() {
		return nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public String getCpf() {
		return cpf;
	}
	public String getCnpjcpf() {
		return cnpjcpf;
	}
	public Integer getTipopessoa() {
		return tipopessoa;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getUf() {
		return uf;
	}
	public String getAgencia() {
		return agencia;
	}
	public String getDvagencia() {
		return dvagencia;
	}
	public String getConta() {
		return conta;
	}
	public String getDvconta() {
		return dvconta;
	}
	public Integer getCdtipoconta() {
		return cdtipoconta;
	}
	public String getEmail() {
		return email;
	}
	public String getBanconumero() {
		return banconumero;
	}
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public void setCdpessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setBanconumero(String banconumero) {
		this.banconumero = banconumero;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}
	public void setTipopessoa(Integer tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public void setDvconta(String dvconta) {
		this.dvconta = dvconta;
	}
	public void setCdtipoconta(Integer cdtipoconta) {
		this.cdtipoconta = cdtipoconta;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
