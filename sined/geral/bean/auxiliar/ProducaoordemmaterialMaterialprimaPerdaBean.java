package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Material;

public class ProducaoordemmaterialMaterialprimaPerdaBean {

	private Boolean selected;
	private Integer cdproducaoordem;
	private Integer cdproducaoagenda;
	private Localarmazenagem localarmazenagem;
	private Material materiaprima;
	private Double quantidade;
	private Double consumo;
	
	
	public Integer getCdproducaoordem() {
		return cdproducaoordem;
	}
	public void setCdproducaoordem(Integer cdproducaoordem) {
		this.cdproducaoordem = cdproducaoordem;
	}
	
	public Integer getCdproducaoagenda() {
		return cdproducaoagenda;
	}
	public void setCdproducaoagenda(Integer cdproducaoagenda) {
		this.cdproducaoagenda = cdproducaoagenda;
	}
	
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public Material getMateriaprima() {
		return materiaprima;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}
	public Double getConsumo() {
		return consumo;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMateriaprima(Material materiaprima) {
		this.materiaprima = materiaprima;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}
	
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
}
