package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaoservico {

	public final static String SERVICO = "02";
	
	private String servico;
	private Integer sequencial;
	private String codigoempresa;
	private String inscricaofornecedorCpfCnpjCei;
	private Integer codigoespecie;
	private Integer codigoacumulador;
	private String siglaestadocliente;
	private Integer seguimento;
	private String numerodocumento;
	private Integer documentofinal;
	private String serie;	
	private Date dataservico;
	private Date dataemissao;
	private Double valorcontabil;
	private String reservado;
	private String fatogeradorcrf;
	private String fatogeradorirrf;
	private String fatogeradorcrfop;
	private String fatogeradorirrfp;
	private String branco142;
	private Integer codigomunicipio;
	private Integer codigoobservacao;
	private Integer codigomodelodocumentofiscal;
	private Integer codigofiscalprestacaoservico;
	private Integer subserie;
	private String inscricaoestadualcliente;
	private String inscricaomunicipalcliente;
	private String observacao;
	private String brancos;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getServico(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printString(getCodigoempresa(), 7) +
			   SIntegraUtil.printStringCpfCnpj(getInscricaofornecedorCpfCnpjCei(), 14) +
			   SIntegraUtil.printInteger(getCodigoespecie(), 7) +
			   SIntegraUtil.printInteger(getCodigoacumulador(), 7) +
			   SIntegraUtil.printString(getSiglaestadocliente(), 2) +
			   SIntegraUtil.printInteger(getSeguimento(), 7) +
			   SIntegraUtil.printStringOrigemInteger(getNumerodocumento(), 7) +			   
			   SIntegraUtil.printInteger(getDocumentofinal(), 7) +
			   SIntegraUtil.printString(getSerie(), 7) + 
			   SIntegraUtil.printDateDominio(getDataservico(), 10) +
			   SIntegraUtil.printDateDominio(getDataemissao(), 10) +
			   SIntegraUtil.printDouble(getValorcontabil(), 13) +
			   SIntegraUtil.printString(getReservado(), 30) + 
			   SIntegraUtil.printString(getFatogeradorcrf(), 1) +
			   SIntegraUtil.printString(getFatogeradorirrf(), 1) +
			   SIntegraUtil.printString(getFatogeradorcrfop(), 1) +
			   SIntegraUtil.printString(getFatogeradorirrfp(), 1) +
			   SIntegraUtil.printString(getBranco142(), 1) +
			   SIntegraUtil.printInteger(getCodigomunicipio(), 7) +			   
			   SIntegraUtil.printInteger(getCodigoobservacao(), 7) +
			   SIntegraUtil.printInteger(getCodigomodelodocumentofiscal(), 7) +
			   SIntegraUtil.printInteger(getCodigofiscalprestacaoservico(), 7) +
			   SIntegraUtil.printInteger(getSubserie(), 7) +
			   SIntegraUtil.printString(getInscricaoestadualcliente(), 20) +
			   SIntegraUtil.printString(getInscricaomunicipalcliente(), 20) +
			   SIntegraUtil.printString(getObservacao(), 300) +
			   SIntegraUtil.printString(getBrancos(), 100) +		   
			   
			   SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getServico() {
		return servico;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public String getCodigoempresa() {
		return codigoempresa;
	}

	public String getInscricaofornecedorCpfCnpjCei() {
		return inscricaofornecedorCpfCnpjCei;
	}

	public Integer getCodigoespecie() {
		return codigoespecie;
	}

	public Integer getCodigoacumulador() {
		return codigoacumulador;
	}

	public String getSiglaestadocliente() {
		return siglaestadocliente;
	}

	public Integer getSeguimento() {
		return seguimento;
	}

	public String getNumerodocumento() {
		return numerodocumento;
	}

	public Integer getDocumentofinal() {
		return documentofinal;
	}

	public String getSerie() {
		return serie;
	}

	public Date getDataservico() {
		return dataservico;
	}

	public Date getDataemissao() {
		return dataemissao;
	}

	public Double getValorcontabil() {
		return valorcontabil;
	}

	public String getReservado() {
		return reservado;
	}

	public String getFatogeradorcrf() {
		return fatogeradorcrf;
	}

	public String getFatogeradorirrf() {
		return fatogeradorirrf;
	}

	public String getFatogeradorcrfop() {
		return fatogeradorcrfop;
	}

	public String getFatogeradorirrfp() {
		return fatogeradorirrfp;
	}

	public String getBranco142() {
		return branco142;
	}

	public Integer getCodigomunicipio() {
		return codigomunicipio;
	}

	public Integer getCodigoobservacao() {
		return codigoobservacao;
	}

	public Integer getCodigomodelodocumentofiscal() {
		return codigomodelodocumentofiscal;
	}

	public Integer getCodigofiscalprestacaoservico() {
		return codigofiscalprestacaoservico;
	}

	public Integer getSubserie() {
		return subserie;
	}

	public String getInscricaoestadualcliente() {
		return inscricaoestadualcliente;
	}

	public String getInscricaomunicipalcliente() {
		return inscricaomunicipalcliente;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getBrancos() {
		return brancos;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setCodigoempresa(String codigoempresa) {
		this.codigoempresa = codigoempresa;
	}

	public void setInscricaofornecedorCpfCnpjCei(
			String inscricaofornecedorCpfCnpjCei) {
		this.inscricaofornecedorCpfCnpjCei = inscricaofornecedorCpfCnpjCei;
	}

	public void setCodigoespecie(Integer codigoespecie) {
		this.codigoespecie = codigoespecie;
	}

	public void setCodigoacumulador(Integer codigoacumulador) {
		this.codigoacumulador = codigoacumulador;
	}

	public void setSiglaestadocliente(String siglaestadocliente) {
		this.siglaestadocliente = siglaestadocliente;
	}

	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	public void setDocumentofinal(Integer documentofinal) {
		this.documentofinal = documentofinal;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setDatasservico(Date dataservico) {
		this.dataservico = dataservico;
	}

	public void setDataemissao(Date dataemissao) {
		this.dataemissao = dataemissao;
	}

	public void setValorcontabil(Double valorcontabil) {
		this.valorcontabil = valorcontabil;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	public void setFatogeradorcrf(String fatogeradorcrf) {
		this.fatogeradorcrf = fatogeradorcrf;
	}

	public void setFatogeradorirrf(String fatogeradorirrf) {
		this.fatogeradorirrf = fatogeradorirrf;
	}

	public void setFatogeradorcrfop(String fatogeradorcrfop) {
		this.fatogeradorcrfop = fatogeradorcrfop;
	}

	public void setFatogeradorirrfp(String fatogeradorirrfp) {
		this.fatogeradorirrfp = fatogeradorirrfp;
	}

	public void setBranco142(String branco142) {
		this.branco142 = branco142;
	}

	public void setCodigomunicipio(Integer codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}

	public void setCodigoobservacao(Integer codigoobservacao) {
		this.codigoobservacao = codigoobservacao;
	}

	public void setCodigomodelodocumentofiscal(Integer codigomodelodocumentofiscal) {
		this.codigomodelodocumentofiscal = codigomodelodocumentofiscal;
	}

	public void setCodigofiscalprestacaoservico(Integer codigofiscalprestacaoservico) {
		this.codigofiscalprestacaoservico = codigofiscalprestacaoservico;
	}

	public void setSubserie(Integer subserie) {
		this.subserie = subserie;
	}

	public void setInscricaoestadualcliente(String inscricaoestadualcliente) {
		this.inscricaoestadualcliente = inscricaoestadualcliente;
	}

	public void setInscricaomunicipalcliente(String inscricaomunicipalcliente) {
		this.inscricaomunicipalcliente = inscricaomunicipalcliente;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setBrancos(String brancos) {
		this.brancos = brancos;
	}	
}
