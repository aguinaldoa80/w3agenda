package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.utils.DominioIntegracaoUtil;

public class DominioIntegracaoRegistro6100 {
	public final static String IDENTIFICADOR = "6100";
	
	private Date dtLancamento;
	private Integer contaContabilDebito;
	private Integer contaContabilCredito;
	private Double valor;
	private Integer codigoHistoricoContabil;
	private String descricaoHistoricoContabil;
	private String usuario;
	private Integer codigoFilial;
	private Integer lancamentosSCP;
	
	@Override
	public String toString() {
		return DominioIntegracaoUtil.printString(IDENTIFICADOR) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printDateDominio(dtLancamento) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(contaContabilDebito) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(contaContabilCredito) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   new Money(valor).toString().replace(".", "") + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(codigoHistoricoContabil) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(descricaoHistoricoContabil) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(usuario) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(codigoFilial) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printInteger(lancamentosSCP) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.SEPARADOR_LINHA;
	}
	
	public Date getDtLancamento() {
		return dtLancamento;
	}
	public Integer getContaContabilDebito() {
		return contaContabilDebito;
	}
	public Integer getContaContabilCredito() {
		return contaContabilCredito;
	}
	public Double getValor() {
		return valor;
	}
	public Integer getCodigoHistoricoContabil() {
		return codigoHistoricoContabil;
	}
	public String getDescricaoHistoricoContabil() {
		return descricaoHistoricoContabil;
	}
	public String getUsuario() {
		return usuario;
	}
	public Integer getCodigoFilial() {
		return codigoFilial;
	}
	public Integer getLancamentosSCP() {
		return lancamentosSCP;
	}
	public void setDtLancamento(Date dtLancamento) {
		this.dtLancamento = dtLancamento;
	}
	public void setContaContabilDebito(Integer contaContabilDebito) {
		this.contaContabilDebito = contaContabilDebito;
	}
	public void setContaContabilCredito(Integer contaContabilCredito) {
		this.contaContabilCredito = contaContabilCredito;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setCodigoHistoricoContabil(Integer codigoHistoricoContabil) {
		this.codigoHistoricoContabil = codigoHistoricoContabil;
	}
	public void setDescricaoHistoricoContabil(String descricaoHistoricoContabil) {
		this.descricaoHistoricoContabil = descricaoHistoricoContabil;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setCodigoFilial(Integer codigoFilial) {
		this.codigoFilial = codigoFilial;
	}
	public void setLancamentosSCP(Integer lancamentosSCP) {
		this.lancamentosSCP = lancamentosSCP;
	}
}
