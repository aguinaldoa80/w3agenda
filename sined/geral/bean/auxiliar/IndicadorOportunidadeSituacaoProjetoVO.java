package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;

public class IndicadorOportunidadeSituacaoProjetoVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer cdSituacao; 
	private String nomeSituacao;
	private Integer cdProjeto;
	private String nomeProjeto;
	private int valorMes1;
	private int valorMes2;
	private int valorMes3;
	
	public Integer getCdSituacao() {
		return cdSituacao;
	}
	public String getNomeSituacao() {
		return nomeSituacao;
	}
	public Integer getCdProjeto() {
		return cdProjeto;
	}
	public String getNomeProjeto() {
		return nomeProjeto;
	}
	public int getValorMes1() {
		return valorMes1;
	}
	public int getValorMes2() {
		return valorMes2;
	}
	public int getValorMes3() {
		return valorMes3;
	}
	public void setCdSituacao(Integer cdSituacao) {
		this.cdSituacao = cdSituacao;
	}
	public void setNomeSituacao(String nomeSituacao) {
		this.nomeSituacao = nomeSituacao;
	}
	public void setCdProjeto(Integer cdProjeto) {
		this.cdProjeto = cdProjeto;
	}
	public void setNomeProjeto(String nomeProjeto) {
		this.nomeProjeto = nomeProjeto;
	}
	public void setValorMes1(int valorMes1) {
		this.valorMes1 = valorMes1;
	}
	public void setValorMes2(int valorMes2) {
		this.valorMes2 = valorMes2;
	}
	public void setValorMes3(int valorMes3) {
		this.valorMes3 = valorMes3;
	}
}