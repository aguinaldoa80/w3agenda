package br.com.linkcom.sined.geral.bean.auxiliar;

public class DocumentotipoVO {
	
	private Integer cddocumentotipo;
	private String nome;
	
	public DocumentotipoVO(Integer cddocumentotipo, String nome){
		this.cddocumentotipo = cddocumentotipo;
		this.nome = nome;
	}
	
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	public String getNome() {
		return nome;
	}
	
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}