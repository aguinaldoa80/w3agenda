package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaoprodutodnf {

	public final static String PRODUTO = "06";
	
	private String produto;
	private Integer sequencial;
	private String codigoproduto;
	private Double quantidade;
	private Double valortotal;
	private Double valoripi;
	private Double basecalculo;
	private Integer notaextra;
	private Date data;
	private Integer numeroregistrodeclaracaoimportacao;
	private String codigosituacaotributariaicms;
	private Double valorbrutoproduto;
	private Double valordesconto;
	private Double basecalculoicms;
	private Double basecalculoicmsst;
	private Double aliquotaicms;
	private String produtoincentivado;
	private Integer codigoapuracao;
	private Double valorfrete;
	private Double valorseguro;
	private Double valordespesasacessorias;
	private Double quantidadegasolina;
	private Double valoricms;
	private Double valorsubtri;
	private Double valorisentasipi;
	private Double valoroutrasipi;
	private Double icmsnfp;
	private Double valorunitario;
	private Double aliquotasubstituicaotributaria;
	private Integer codigotributacaoipi;
	private Double aliquotaipi;	
	private Double basecalculoissqn;
	private Double aliquotaissqn;
	private Double valorissqn;
	private String cfop;
	private String serieecf;
	private Double aliquotapis;
	private Double valorpis;
	private Double aliquotacofins;
	private Double valorcofins;
	private Double custototal;
	private Integer cstpis;
	private Double basecalculopis;
	private Integer cstcofins;
	private Double basecalculocofins;
	private String chassiveiculo;
	private Integer tipooperacaocomveiculo;
	private String lotemedicamento;
	private Integer quantidadeitemporlotemedicamento;
	private Date datavalidade;
	private Date datafabricacaomedicamento;
	private Integer referenciabasecalculo;
	private Double valortabeladomaximo;
	private String numeroseriearma;
	private String numeroseriecano;
	private String enquadramentoipi;	
	private String brancos;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getProduto(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printString(getCodigoproduto(), 14) +
			   SIntegraUtil.printDouble(getQuantidade(), 13) +
			   SIntegraUtil.printDouble(getValortotal(), 13) +
			   SIntegraUtil.printDouble(getValoripi(), 13) +
			   SIntegraUtil.printDouble(getBasecalculo(), 13) +
			   SIntegraUtil.printInteger(getNotaextra(), 1) +
			   SIntegraUtil.printDateDominio(getData(), 10) +
			   SIntegraUtil.printInteger(getNumeroregistrodeclaracaoimportacao(), 10) +
			   SIntegraUtil.printStringOrigemInteger(getCodigosituacaotributariaicms(), 7) +			   
			   SIntegraUtil.printDouble(getValorbrutoproduto(), 13) + 
			   SIntegraUtil.printDouble(getValordesconto(), 13) +
			   SIntegraUtil.printDouble(getBasecalculoicms(), 13) +
			   SIntegraUtil.printDouble(getBasecalculoicmsst(), 13) +
			   SIntegraUtil.printDouble(getAliquotaicms(), 13) +
			   SIntegraUtil.printString(getProdutoincentivado(), 1) +
			   SIntegraUtil.printInteger(getCodigoapuracao(), 7) +
			   SIntegraUtil.printDouble(getValorfrete(), 13) +
			   SIntegraUtil.printDouble(getValorseguro(), 13) +
			   SIntegraUtil.printDouble(getValordespesasacessorias(), 13) +			   
			   SIntegraUtil.printDouble(getQuantidadegasolina(), 13) +
			   SIntegraUtil.printDouble(getValoricms(), 13) +
			   SIntegraUtil.printDouble(getValorsubtri(), 13) +
			   SIntegraUtil.printDouble(getValorisentasipi(), 13) +
			   SIntegraUtil.printDouble(getValoroutrasipi(), 13) +
			   SIntegraUtil.printDouble(getIcmsnfp(), 13) +
			   SIntegraUtil.printDouble(getValorunitario(), 15) +
			   SIntegraUtil.printDouble(getAliquotasubstituicaotributaria(), 5) +
			   SIntegraUtil.printInteger(getCodigotributacaoipi(), 7) +
			   SIntegraUtil.printDouble(getAliquotaipi(), 5) +
			   SIntegraUtil.printDouble(getBasecalculoissqn(), 13) +
			   SIntegraUtil.printDouble(getAliquotaissqn(), 5) +
			   SIntegraUtil.printDouble(getValorissqn(), 13) +
			   SIntegraUtil.printStringOrigemInteger(getCfop(), 7) +
			   SIntegraUtil.printString(getSerieecf(), 20) +
			   SIntegraUtil.printDouble(getAliquotapis(), 5) +
			   SIntegraUtil.printDouble(getValorpis(), 13) +
			   SIntegraUtil.printDouble(getAliquotacofins(), 5) +
			   SIntegraUtil.printDouble(getValorcofins(), 13) +
			   SIntegraUtil.printDouble(getCustototal(), 13) +
			   SIntegraUtil.printInteger(getCstpis(), 7) +
			   SIntegraUtil.printDouble(getBasecalculopis(), 13) +
			   SIntegraUtil.printInteger(getCstcofins(), 7) +
			   SIntegraUtil.printDouble(getBasecalculocofins(), 13) +
			   SIntegraUtil.printString(getChassiveiculo(), 17) +
			   SIntegraUtil.printInteger(getTipooperacaocomveiculo(), 1) +
			   SIntegraUtil.printString(getLotemedicamento(), 255) +
			   SIntegraUtil.printInteger(getQuantidadeitemporlotemedicamento(), 7) +
			   SIntegraUtil.printDateDominio(getDatavalidade(), 10) +
			   SIntegraUtil.printDateDominio(getDatafabricacaomedicamento(), 10) +
			   SIntegraUtil.printInteger(getReferenciabasecalculo(), 1) +
			   SIntegraUtil.printDouble(getValortabeladomaximo(), 13) +
			   SIntegraUtil.printString(getNumeroseriearma(), 255) +
			   SIntegraUtil.printString(getNumeroseriecano(), 255) +
			   SIntegraUtil.printString(getEnquadramentoipi(), 3) +
			   SIntegraUtil.printString(getBrancos(), 100) +		   
			   
			   SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getProduto() {
		return produto;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public String getCodigoproduto() {
		return codigoproduto;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public Double getValortotal() {
		return valortotal;
	}

	public Double getValoripi() {
		return valoripi;
	}

	public Double getBasecalculo() {
		return basecalculo;
	}

	public Integer getNotaextra() {
		return notaextra;
	}

	public Date getData() {
		return data;
	}	

	public Integer getNumeroregistrodeclaracaoimportacao() {
		return numeroregistrodeclaracaoimportacao;
	}
	
	public String getCodigosituacaotributariaicms() {
		return codigosituacaotributariaicms;
	}

	public Double getValorbrutoproduto() {
		return valorbrutoproduto;
	}

	public Double getValordesconto() {
		return valordesconto;
	}

	public Double getBasecalculoicms() {
		return basecalculoicms;
	}

	public Double getBasecalculoicmsst() {
		return basecalculoicmsst;
	}

	public Double getAliquotaicms() {
		return aliquotaicms;
	}

	public String getProdutoincentivado() {
		return produtoincentivado;
	}

	public Integer getCodigoapuracao() {
		return codigoapuracao;
	}

	public Double getValorfrete() {
		return valorfrete;
	}

	public Double getValorseguro() {
		return valorseguro;
	}

	public Double getValordespesasacessorias() {
		return valordespesasacessorias;
	}

	public Double getQuantidadegasolina() {
		return quantidadegasolina;
	}

	public Double getValoricms() {
		return valoricms;
	}

	public Double getValorsubtri() {
		return valorsubtri;
	}

	public Double getValorisentasipi() {
		return valorisentasipi;
	}

	public Double getValoroutrasipi() {
		return valoroutrasipi;
	}
	
	public Double getIcmsnfp() {
		return icmsnfp;
	}

	public Double getValorunitario() {
		return valorunitario;
	}
	
	public Double getAliquotasubstituicaotributaria() {
		return aliquotasubstituicaotributaria;
	}

	public Integer getCodigotributacaoipi() {
		return codigotributacaoipi;
	}

	public Double getAliquotaipi() {
		return aliquotaipi;
	}

	public Double getBasecalculoissqn() {
		return basecalculoissqn;
	}	

	public Double getAliquotaissqn() {
		return aliquotaissqn;
	}

	public Double getValorissqn() {
		return valorissqn;
	}

	public String getCfop() {
		return cfop;
	}

	public String getSerieecf() {
		return serieecf;
	}

	public Double getAliquotapis() {
		return aliquotapis;
	}

	public Double getValorpis() {
		return valorpis;
	}

	public Double getAliquotacofins() {
		return aliquotacofins;
	}

	public Double getValorcofins() {
		return valorcofins;
	}

	public Double getCustototal() {
		return custototal;
	}

	public Integer getCstpis() {
		return cstpis;
	}

	public Double getBasecalculopis() {
		return basecalculopis;
	}

	public Integer getCstcofins() {
		return cstcofins;
	}

	public Double getBasecalculocofins() {
		return basecalculocofins;
	}

	public String getChassiveiculo() {
		return chassiveiculo;
	}

	public Integer getTipooperacaocomveiculo() {
		return tipooperacaocomveiculo;
	}

	public String getLotemedicamento() {
		return lotemedicamento;
	}

	public Integer getQuantidadeitemporlotemedicamento() {
		return quantidadeitemporlotemedicamento;
	}

	public Date getDatavalidade() {
		return datavalidade;
	}

	public Date getDatafabricacaomedicamento() {
		return datafabricacaomedicamento;
	}

	public Integer getReferenciabasecalculo() {
		return referenciabasecalculo;
	}

	public Double getValortabeladomaximo() {
		return valortabeladomaximo;
	}

	public String getNumeroseriearma() {
		return numeroseriearma;
	}

	public String getNumeroseriecano() {
		return numeroseriecano;
	}

	public String getEnquadramentoipi() {
		return enquadramentoipi;
	}

	public String getBrancos() {
		return brancos;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setCodigoproduto(String codigoproduto) {
		this.codigoproduto = codigoproduto;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}

	public void setValoripi(Double valoripi) {
		this.valoripi = valoripi;
	}

	public void setBasecalculo(Double basecalculo) {
		this.basecalculo = basecalculo;
	}

	public void setNotaextra(Integer notaextra) {
		this.notaextra = notaextra;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setNumeroregistrodeclaracaoimportacao(
			Integer numeroregistrodeclaracaoimportacao) {
		this.numeroregistrodeclaracaoimportacao = numeroregistrodeclaracaoimportacao;
	}

	public void setCodigosituacaotributariaicms(String codigosituacaotributariaicms) {
		this.codigosituacaotributariaicms = codigosituacaotributariaicms;
	}

	public void setValorbrutoproduto(Double valorbrutoproduto) {
		this.valorbrutoproduto = valorbrutoproduto;
	}

	public void setValordesconto(Double valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setBasecalculoicms(Double basecalculoicms) {
		this.basecalculoicms = basecalculoicms;
	}

	public void setBasecalculoicmsst(Double basecalculoicmsst) {
		this.basecalculoicmsst = basecalculoicmsst;
	}

	public void setAliquotaicms(Double aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public void setProdutoincentivado(String produtoincentivado) {
		this.produtoincentivado = produtoincentivado;
	}

	public void setCodigoapuracao(Integer codigoapuracao) {
		this.codigoapuracao = codigoapuracao;
	}

	public void setValorfrete(Double valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValorseguro(Double valorseguro) {
		this.valorseguro = valorseguro;
	}

	public void setValordespesasacessorias(Double valordespesasacessorias) {
		this.valordespesasacessorias = valordespesasacessorias;
	}

	public void setQuantidadegasolina(Double quantidadegasolina) {
		this.quantidadegasolina = quantidadegasolina;
	}

	public void setValoricms(Double valoricms) {
		this.valoricms = valoricms;
	}

	public void setValorsubtri(Double valorsubtri) {
		this.valorsubtri = valorsubtri;
	}

	public void setValorisentasipi(Double valorisentasipi) {
		this.valorisentasipi = valorisentasipi;
	}

	public void setValoroutrasipi(Double valoroutrasipi) {
		this.valoroutrasipi = valoroutrasipi;
	}

	public void setIcmsnfp(Double icmsnfp) {
		this.icmsnfp = icmsnfp;
	}
	
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setAliquotasubstituicaotributaria(
			Double aliquotasubstituicaotributaria) {
		this.aliquotasubstituicaotributaria = aliquotasubstituicaotributaria;
	}

	public void setCodigotributacaoipi(Integer codigotributacaoipi) {
		this.codigotributacaoipi = codigotributacaoipi;
	}

	public void setAliquotaipi(Double aliquotaipi) {
		this.aliquotaipi = aliquotaipi;
	}

	public void setBasecalculoissqn(Double basecalculoissqn) {
		this.basecalculoissqn = basecalculoissqn;
	}

	public void setAliquotaissqn(Double aliquotaissqn) {
		this.aliquotaissqn = aliquotaissqn;
	}

	public void setValorissqn(Double valorissqn) {
		this.valorissqn = valorissqn;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public void setSerieecf(String serieecf) {
		this.serieecf = serieecf;
	}

	public void setAliquotapis(Double aliquotapis) {
		this.aliquotapis = aliquotapis;
	}

	public void setValorpis(Double valorpis) {
		this.valorpis = valorpis;
	}

	public void setAliquotacofins(Double aliquotacofins) {
		this.aliquotacofins = aliquotacofins;
	}

	public void setValorcofins(Double valorcofins) {
		this.valorcofins = valorcofins;
	}

	public void setCustototal(Double custototal) {
		this.custototal = custototal;
	}

	public void setCstpis(Integer cstpis) {
		this.cstpis = cstpis;
	}

	public void setBasecalculopis(Double basecalculopis) {
		this.basecalculopis = basecalculopis;
	}

	public void setCstcofins(Integer cstcofins) {
		this.cstcofins = cstcofins;
	}

	public void setBasecalculocofins(Double basecalculocofins) {
		this.basecalculocofins = basecalculocofins;
	}

	public void setChassiveiculo(String chassiveiculo) {
		this.chassiveiculo = chassiveiculo;
	}

	public void setTipooperacaocomveiculo(Integer tipooperacaocomveiculo) {
		this.tipooperacaocomveiculo = tipooperacaocomveiculo;
	}

	public void setLotemedicamento(String lotemedicamento) {
		this.lotemedicamento = lotemedicamento;
	}

	public void setQuantidadeitemporlotemedicamento(
			Integer quantidadeitemporlotemedicamento) {
		this.quantidadeitemporlotemedicamento = quantidadeitemporlotemedicamento;
	}

	public void setDatavalidade(Date datavalidade) {
		this.datavalidade = datavalidade;
	}

	public void setDatafabricacaomedicamento(Date datafabricacaomedicamento) {
		this.datafabricacaomedicamento = datafabricacaomedicamento;
	}

	public void setReferenciabasecalculo(Integer referenciabasecalculo) {
		this.referenciabasecalculo = referenciabasecalculo;
	}

	public void setValortabeladomaximo(Double valortabeladomaximo) {
		this.valortabeladomaximo = valortabeladomaximo;
	}

	public void setNumeroseriearma(String numeroseriearma) {
		this.numeroseriearma = numeroseriearma;
	}

	public void setNumeroseriecano(String numeroseriecano) {
		this.numeroseriecano = numeroseriecano;
	}

	public void setEnquadramentoipi(String enquadramentoipi) {
		this.enquadramentoipi = enquadramentoipi;
	}

	public void setBrancos(String brancos) {
		this.brancos = brancos;
	}	
}
