package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class DominiointegracaoRegistro11 {

	public final static String IDENTIFICADOR = "11";
	
	private String identificador;
	private Integer codigoempresa;
	private String siglaestado;
	private Integer codigoconta;
	private String codigomunicipio;
	private String nomereduzido;
	private String nomecliente;
	private String endereco;
	private String numeroendereco;
	private String branco123;
	private String cep;
	private String inscricao;
	private String inscricaoestadual;
	private String fone;
	private String fax;
	private String agropecuario; //INFORMAR S,N
	private String icms; //INFORMAR S,N
	private Integer tipoinscricao; //1=CGC, 2=CPF, 3=CEI, 4=Outros
	private String inscricaomunicipal;
	private String bairro;
	private Integer dddfone;
	private Integer codigopais;
	private String numeroinscricaosuframa;
	private Date datacadastro;
	private String complementoendereco;
	private String branco;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getIdentificador(), 2) + 
			   SIntegraUtil.printInteger(getCodigoempresa(), 7) +
			   SIntegraUtil.printString(getSiglaestado(), 2) +
			   SIntegraUtil.printInteger(getCodigoconta(), 7) +
			   SIntegraUtil.printStringOrigemInteger(getCodigomunicipio(), 7) +
			   SIntegraUtil.printString(getNomereduzido(), 10) +
			   SIntegraUtil.printString(getNomecliente(), 40) +
			   SIntegraUtil.printString(getEndereco(), 40) +
			   SIntegraUtil.printStringOrigemInteger(getNumeroendereco(), 7) +
			   SIntegraUtil.printString(getBranco123(), 30) +
			   SIntegraUtil.printString(getCep(), 8) +
			   SIntegraUtil.printString(getInscricao(), 14) +
			   SIntegraUtil.printString(getInscricaoestadual(), 20) +
			   SIntegraUtil.printString(getFone(), 14) +
			   SIntegraUtil.printString(getFax(), 14) +
			   SIntegraUtil.printString(getAgropecuario(), 1) +
			   SIntegraUtil.printString(getIcms(), 1) +
			   SIntegraUtil.printInteger(getTipoinscricao(), 1) +
			   SIntegraUtil.printString(getInscricaomunicipal(), 20) +
			   SIntegraUtil.printString(getBairro(), 20) +
			   SIntegraUtil.printInteger(getDddfone(), 4) +
			   SIntegraUtil.printInteger(getCodigopais(), 7) +
			   SIntegraUtil.printString(getNumeroinscricaosuframa(), 11) +
			   SIntegraUtil.printDate(getDatacadastro(), 10) +
			   SIntegraUtil.printString(getComplementoendereco(), 40) +
			   SIntegraUtil.printString(getBranco(), 48) + SIntegraUtil.SEPARADOR_LINHA;
	}

	
	public String getIdentificador() {
		return identificador;
	}

	public Integer getCodigoempresa() {
		return codigoempresa;
	}

	public String getSiglaestado() {
		return siglaestado;
	}

	public Integer getCodigoconta() {
		return codigoconta;
	}

	public String getCodigomunicipio() {
		return codigomunicipio;
	}

	public String getNomereduzido() {
		return nomereduzido;
	}

	public String getNomecliente() {
		return nomecliente;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getNumeroendereco() {
		return numeroendereco;
	}

	public String getBranco123() {
		return branco123;
	}

	public String getCep() {
		return cep;
	}

	public String getInscricao() {
		return inscricao;
	}

	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	public String getFone() {
		return fone;
	}

	public String getFax() {
		return fax;
	}

	public String getAgropecuario() {
		return agropecuario;
	}

	public String getIcms() {
		return icms;
	}

	public Integer getTipoinscricao() {
		return tipoinscricao;
	}

	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	public String getBairro() {
		return bairro;
	}

	public Integer getDddfone() {
		return dddfone;
	}

	public Integer getCodigopais() {
		return codigopais;
	}

	public String getNumeroinscricaosuframa() {
		return numeroinscricaosuframa;
	}

	public Date getDatacadastro() {
		return datacadastro;
	}

	public String getComplementoendereco() {
		return complementoendereco;
	}

	public String getBranco() {
		return branco;
	}


	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setCodigoempresa(Integer codigoempresa) {
		this.codigoempresa = codigoempresa;
	}

	public void setSiglaestado(String siglaestado) {
		this.siglaestado = siglaestado;
	}

	public void setCodigoconta(Integer codigoconta) {
		this.codigoconta = codigoconta;
	}

	public void setCodigomunicipio(String codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}

	public void setNomereduzido(String nomereduzido) {
		this.nomereduzido = nomereduzido;
	}

	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setNumeroendereco(String numeroendereco) {
		this.numeroendereco = numeroendereco;
	}

	public void setBranco123(String branco123) {
		this.branco123 = branco123;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setInscricao(String inscricao) {
		this.inscricao = inscricao;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setAgropecuario(String agropecuario) {
		this.agropecuario = agropecuario;
	}

	public void setIcms(String icms) {
		this.icms = icms;
	}

	public void setTipoinscricao(Integer tipoinscricao) {
		this.tipoinscricao = tipoinscricao;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setDddfone(Integer dddfone) {
		this.dddfone = dddfone;
	}

	public void setCodigopais(Integer codigopais) {
		this.codigopais = codigopais;
	}

	public void setNumeroinscricaosuframa(String numeroinscricaosuframa) {
		this.numeroinscricaosuframa = numeroinscricaosuframa;
	}

	public void setDatacadastro(Date datacadastro) {
		this.datacadastro = datacadastro;
	}

	public void setComplementoendereco(String complementoendereco) {
		this.complementoendereco = complementoendereco;
	}

	public void setBranco(String branco) {
		this.branco = branco;
	}
	
	
}
