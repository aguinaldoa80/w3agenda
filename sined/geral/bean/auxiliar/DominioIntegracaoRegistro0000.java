package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.utils.DominioIntegracaoUtil;

public class DominioIntegracaoRegistro0000 {
	public final static String IDENTIFICADOR = "0000";
	
	private String inscricaoEmpresa;
	
	@Override
	public String toString() {
		return DominioIntegracaoUtil.printString(IDENTIFICADOR) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.printString(inscricaoEmpresa) + DominioIntegracaoUtil.SEPARADOR_CAMPO +
			   DominioIntegracaoUtil.SEPARADOR_LINHA;
	}
	
	public String getInscricaoEmpresa() {
		return inscricaoEmpresa;
	}

	public void setInscricaoEmpresa(String inscricaoEmpresa) {
		this.inscricaoEmpresa = DominioIntegracaoUtil.soNumero(inscricaoEmpresa);
	}
}
