package br.com.linkcom.sined.geral.bean.auxiliar;

public class TotalCamposRateadosVendaBean {

	public Double desconto;
	public Double frete;
	public Double valecompra;
	
	
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Double getFrete() {
		return frete;
	}
	public void setFrete(Double frete) {
		this.frete = frete;
	}
	public Double getValecompra() {
		return valecompra;
	}
	public void setValecompra(Double valecompra) {
		this.valecompra = valecompra;
	}
}
