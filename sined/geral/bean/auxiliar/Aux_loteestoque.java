package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Aux_loteestoque {
	
	private Integer cdloteestoque;
	private Date validade;
	
	@Id
	public Integer getCdloteestoque() {
		return cdloteestoque;
	}
	public Date getValidade() {
		return validade;
	}
	public void setCdloteestoque(Integer cdloteestoque) {
		this.cdloteestoque = cdloteestoque;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	
}