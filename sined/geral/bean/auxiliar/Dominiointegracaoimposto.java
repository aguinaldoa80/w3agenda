package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaoimposto {

	public final static String IMPOSTO = "03";
	
	private String imposto;
	private Integer sequencial;
	private Integer codigoimposto;
	private Double percentualreducaobasecalculo;
	private Double basecalculo;
	private Double aliquota;
	private Double valorimposto;
	private Double valorisentas;
	private Double valoroutras;
	private Double valoripi;
	private Double valorsubstituicaotributaria;
	private Double valorcontabil;
	private String codigorecolhimento;
	private Double valornaotributada;
	private Double valorparcelareduzida;
	private String brancos;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getImposto(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printInteger(getCodigoimposto(), 7) +
			   SIntegraUtil.printDouble(getPercentualreducaobasecalculo(), 5) +
			   SIntegraUtil.printDouble(getBasecalculo(), 13) +
			   SIntegraUtil.printDouble(getAliquota(), 5) +
			   SIntegraUtil.printDouble(getValorimposto(), 13) +
			   SIntegraUtil.printDouble(getValorisentas(), 13) +
			   SIntegraUtil.printDouble(getValoroutras(), 13) +
			   SIntegraUtil.printDouble(getValoripi(), 13) +
			   SIntegraUtil.printDouble(getValorsubstituicaotributaria(), 13) +
			   SIntegraUtil.printDouble(getValorcontabil(), 13) +
			   SIntegraUtil.printString(getCodigorecolhimento(), 6) +
			   SIntegraUtil.printDouble(getValornaotributada(), 13) +
			   SIntegraUtil.printDouble(getValorparcelareduzida(), 13) +
			   SIntegraUtil.printString(getBrancos(), 74) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getImposto() {
		return imposto;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public Integer getCodigoimposto() {
		return codigoimposto;
	}

	public Double getPercentualreducaobasecalculo() {
		return percentualreducaobasecalculo;
	}

	public Double getBasecalculo() {
		return basecalculo;
	}

	public Double getAliquota() {
		return aliquota;
	}

	public Double getValorimposto() {
		return valorimposto;
	}

	public Double getValorisentas() {
		return valorisentas;
	}

	public Double getValoroutras() {
		return valoroutras;
	}

	public Double getValoripi() {
		return valoripi;
	}

	public Double getValorsubstituicaotributaria() {
		return valorsubstituicaotributaria;
	}

	public Double getValorcontabil() {
		return valorcontabil;
	}

	public String getCodigorecolhimento() {
		return codigorecolhimento;
	}

	public Double getValornaotributada() {
		return valornaotributada;
	}

	public Double getValorparcelareduzida() {
		return valorparcelareduzida;
	}

	public String getBrancos() {
		return brancos;
	}

	public void setImposto(String imposto) {
		this.imposto = imposto;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setCodigoimposto(Integer codigoimposto) {
		this.codigoimposto = codigoimposto;
	}

	public void setPercentualreducaobasecalculo(Double percentualreducaobasecalculo) {
		this.percentualreducaobasecalculo = percentualreducaobasecalculo;
	}

	public void setBasecalculo(Double basecalculo) {
		this.basecalculo = basecalculo;
	}

	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}

	public void setValorimposto(Double valorimposto) {
		this.valorimposto = valorimposto;
	}

	public void setValorisentas(Double valorisentas) {
		this.valorisentas = valorisentas;
	}

	public void setValoroutras(Double valoroutras) {
		this.valoroutras = valoroutras;
	}

	public void setValoripi(Double valoripi) {
		this.valoripi = valoripi;
	}

	public void setValorsubstituicaotributaria(Double valorsubstituicaotributaria) {
		this.valorsubstituicaotributaria = valorsubstituicaotributaria;
	}

	public void setValorcontabil(Double valorcontabil) {
		this.valorcontabil = valorcontabil;
	}

	public void setCodigorecolhimento(String codigorecolhimento) {
		this.codigorecolhimento = codigorecolhimento;
	}

	public void setValornaotributada(Double valornaotributada) {
		this.valornaotributada = valornaotributada;
	}

	public void setValorparcelareduzida(Double valorparcelareduzida) {
		this.valorparcelareduzida = valorparcelareduzida;
	}

	public void setBrancos(String brancos) {
		this.brancos = brancos;
	}
	
	
}
