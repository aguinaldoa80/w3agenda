package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Unidademedida;

public interface CalculoMarkupItemInterface {

	public Money getTotalprodutoItemSemArredondamento();
	public Money getDesconto();
	public Double getQuantidade();
	public Unidademedida getUnidademedida();
	public Material getMaterial();
	public void setMaterialFaixaMarkup(MaterialFaixaMarkup materialFaixaMarkup);
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome);
}
