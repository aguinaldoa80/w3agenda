package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;

public class TotalOrcamentoMes {
	
	protected Integer cdcontagerencial;
	protected Integer cdcentrocusto;
	protected Money valor;
	
	public TotalOrcamentoMes() {}
	
	public TotalOrcamentoMes(Integer cdcontagerencial, Integer cdcentrocusto, Double valor){
		this.cdcontagerencial = cdcontagerencial;
		this.cdcentrocusto = cdcentrocusto;
		this.valor = valor != null && valor > 0 ? new Money(valor/100) : new Money();
	}

	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public Money getValor() {
		return valor;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
