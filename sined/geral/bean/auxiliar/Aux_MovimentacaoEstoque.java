package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Movimentacaoestoque;
import br.com.linkcom.sined.geral.bean.Movpatrimonio;

public class Aux_MovimentacaoEstoque {

	private List<Movimentacaoestoque> listaMovimentacaoEstoque = new ListSet<Movimentacaoestoque>(Movimentacaoestoque.class);
	private List<Movimentacaoestoque> listaMovimentacaoGradeEstoque = new ListSet<Movimentacaoestoque>(Movimentacaoestoque.class);
	private List<Movpatrimonio> listaMovpatrimonio = new ListSet<Movpatrimonio>(Movpatrimonio.class);

	public List<Movimentacaoestoque> getListaMovimentacaoEstoque() {
		return listaMovimentacaoEstoque;
	}
	
	public List<Movpatrimonio> getListaMovpatrimonio() {
		return listaMovpatrimonio;
	}
	
	public void setListaMovpatrimonio(List<Movpatrimonio> listaMovpatrimonio) {
		this.listaMovpatrimonio = listaMovpatrimonio;
	}

	public void setListaMovimentacaoEstoque(
			List<Movimentacaoestoque> listaMovimentacaoEstoque) {
		this.listaMovimentacaoEstoque = listaMovimentacaoEstoque;
	}

	public List<Movimentacaoestoque> getListaMovimentacaoGradeEstoque() {
		return listaMovimentacaoGradeEstoque;
	}

	public void setListaMovimentacaoGradeEstoque(
			List<Movimentacaoestoque> listaMovimentacaoGradeEstoque) {
		this.listaMovimentacaoGradeEstoque = listaMovimentacaoGradeEstoque;
	}
}
