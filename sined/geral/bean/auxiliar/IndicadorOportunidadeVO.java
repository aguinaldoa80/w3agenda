package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;
import java.util.List;

public class IndicadorOportunidadeVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String descMes1;
	private String descMes2;
	private String descMes3;
	
	private List<IndicadorOportunidadeSituacaoVO> listaIndicadorOportunidadeSituacaoVO;
	private List<IndicadorOportunidadeSituacaoProjetoVO> listaIndicadorOportunidadeSituacaoProjetoVO;
	
	public String getDescMes1() {
		return descMes1;
	}
	public String getDescMes2() {
		return descMes2;
	}
	public String getDescMes3() {
		return descMes3;
	}
	public List<IndicadorOportunidadeSituacaoVO> getListaIndicadorOportunidadeSituacaoVO() {
		return listaIndicadorOportunidadeSituacaoVO;
	}
	public List<IndicadorOportunidadeSituacaoProjetoVO> getListaIndicadorOportunidadeSituacaoProjetoVO() {
		return listaIndicadorOportunidadeSituacaoProjetoVO;
	}
	public void setDescMes1(String descMes1) {
		this.descMes1 = descMes1;
	}
	public void setDescMes2(String descMes2) {
		this.descMes2 = descMes2;
	}
	public void setDescMes3(String descMes3) {
		this.descMes3 = descMes3;
	}
	public void setListaIndicadorOportunidadeSituacaoVO(List<IndicadorOportunidadeSituacaoVO> listaIndicadorOportunidadeSituacaoVO) {
		this.listaIndicadorOportunidadeSituacaoVO = listaIndicadorOportunidadeSituacaoVO;
	}
	public void setListaIndicadorOportunidadeSituacaoProjetoVO(List<IndicadorOportunidadeSituacaoProjetoVO> listaIndicadorOportunidadeSituacaoProjetoVO) {
		this.listaIndicadorOportunidadeSituacaoProjetoVO = listaIndicadorOportunidadeSituacaoProjetoVO;
	}
}