package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamentoServico;

@Entity
public class Aux_agendamentoservico {

	protected Integer cdagendamentoservico;
	protected SituacaoAgendamentoServico situacao;
	
	@javax.persistence.Id
	public Integer getCdagendamentoservico() {
		return cdagendamentoservico;
	}
	@DescriptionProperty
	@DisplayName("Situa��o")
	public SituacaoAgendamentoServico getSituacao() {
		return situacao;
	}
	public void setCdagendamentoservico(Integer cdagendamentoservico) {
		this.cdagendamentoservico = cdagendamentoservico;
	}
	public void setSituacao(SituacaoAgendamentoServico situacao) {
		this.situacao = situacao;
	}
	
}
