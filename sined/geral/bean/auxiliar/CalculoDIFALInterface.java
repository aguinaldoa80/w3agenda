package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;

public interface CalculoDIFALInterface {
	public Money getValorbruto();
	public Money getValorfrete();
	public Money getOutrasdespesas();
	public Money getValordesconto();
	public Money getValoripi();
	public Money getValorseguro();
	public Material getMaterial();
	public Grupotributacao getGrupotributacao();
	public String getNcmcompleto();
	public Naturezaoperacao getNaturezaoperacao();
	public Empresa getEmpresa();
	public Cliente getCliente();
	public Endereco getEnderecoCliente();
	public Operacaonfe getOperacaonfe();
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum();
	public Date getDtemissao();
	public Localdestinonfe getLocaldestinonfe();
	
	public Double getDifal();
	public Money getValordifal();
	public void setDifal(Double difal);
	public void setValordifal(Money valordifal);
	
	public Money getValorbcdestinatario();
	public Money getValorbcfcpdestinatario();
	public Double getFcpdestinatario();
	public Double getIcmsdestinatario();
	public Double getIcmsinterestadual();
	public Double getIcmsinterestadualpartilha();
	public Money getValorfcpdestinatario();
	public Money getValoricmsdestinatario();
	public Money getValoricmsremetente();
	public Double getIcms();
	public Double getFcp();
	
	public void setFcp(Double fcp);
	public void setIcms(Double icms);
	public void setValorseguro(Money valorseguro);
	public void setValorbcdestinatario(Money valorbcdestinatario);
	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario);
	public void setFcpdestinatario(Double aliquotaFCP);
	public void setIcmsdestinatario(Double aliquotaICMSIntra);
	public void setIcmsinterestadual(Double aliquotaICMSInter);
	public void setIcmsinterestadualpartilha(Double aliquotaICMSPart);
	public void setValorfcpdestinatario(Money valorfcpdestinatario);
	public void setValoricmsdestinatario(Money valoricmsdestinatario);
	public void setValoricmsremetente(Money valoricmsremetente);
	public void setCliente(Cliente cliente);
	public void setEmpresa(Empresa empresa);
	public void setEnderecoCliente(Endereco enderecoCliente);
	public void setOperacaonfe(Operacaonfe operacaonfe);
	public void setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum);
	public void setDtemissao(Date dtEmissao);
	public void setLocaldestinonfe(Localdestinonfe localdestinonfe);
}
