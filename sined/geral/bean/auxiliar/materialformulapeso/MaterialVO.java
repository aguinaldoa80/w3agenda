package br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso;

public class MaterialVO {

	private double largura = 0d;
	private double comprimento = 0d;
	private double altura = 0d;
	private double qtdvolume = 0d;
	private double pesobruto = 0d;
	
	public double getLargura() {
		return largura;
	}
	public double getComprimento() {
		return comprimento;
	}
	public double getAltura() {
		return altura;
	}
	public double getQtdvolume() {
		return qtdvolume;
	}
	public double getPesobruto() {
		return pesobruto;
	}
	public void setPesobruto(double pesobruto) {
		this.pesobruto = pesobruto;
	}
	public void setQtdvolume(double qtdvolume) {
		this.qtdvolume = qtdvolume;
	}
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
}
