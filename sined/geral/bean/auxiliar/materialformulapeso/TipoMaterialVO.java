package br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso;

public class TipoMaterialVO {

	private double pesoEspecifico = 0d;

	public double getPesoEspecifico() {
		return pesoEspecifico;
	}

	public void setPesoEspecifico(double pesoEspecifico) {
		this.pesoEspecifico = pesoEspecifico;
	}
	
}
