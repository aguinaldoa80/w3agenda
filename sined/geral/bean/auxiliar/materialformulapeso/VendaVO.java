package br.com.linkcom.sined.geral.bean.auxiliar.materialformulapeso;

public class VendaVO {

	private double largura = 0d;
	private double comprimento = 0d;
	private double altura = 0d;
	private String unidademedida = new String();
	
	public double getLargura() {
		return largura;
	}
	public double getComprimento() {
		return comprimento;
	}
	public double getAltura() {
		return altura;
	}
	public String getUnidademedida() {
		return unidademedida;
	}
	
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}
}
