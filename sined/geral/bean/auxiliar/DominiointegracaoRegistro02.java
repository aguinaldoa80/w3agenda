package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class DominiointegracaoRegistro02 {

	public final static String IDENTIFICADOR = "02";
	
	private String identificador;
	private Integer codigosequencial;
	private String tipo;
	private Date dtlancamento;
	private String usuario;
	private String branco;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getIdentificador(), 2) + 
			   SIntegraUtil.printInteger(getCodigosequencial(), 7) +
			   SIntegraUtil.printString(getTipo(), 1) +
			   SIntegraUtil.printDateDominio(getDtlancamento(), 10) + 
			   SIntegraUtil.printString(getUsuario(), 30) +
			   SIntegraUtil.printString(getBranco(), 100) + SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getIdentificador() {
		return identificador;
	}
	public Integer getCodigosequencial() {
		return codigosequencial;
	}
	public String getTipo() {
		return tipo;
	}
	public Date getDtlancamento() {
		return dtlancamento;
	}
	public String getUsuario() {
		return usuario;
	}
	public String getBranco() {
		return branco;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setCodigosequencial(Integer codigosequencial) {
		this.codigosequencial = codigosequencial;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setDtlancamento(Date dtlancamento) {
		this.dtlancamento = dtlancamento;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setBranco(String branco) {
		this.branco = branco;
	}
}
