package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.sined.geral.bean.Orcamentomaterialsituacao;

@Entity
public class Aux_orcamentomaterial {

	protected Integer cdorcamentomaterial;
	protected Orcamentomaterialsituacao orcamentomaterialsituacao;
	
	@Id
	public Integer getCdorcamentomaterial() {
		return cdorcamentomaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamentomaterialsituacao")
	public Orcamentomaterialsituacao getOrcamentomaterialsituacao() {
		return orcamentomaterialsituacao;
	}
	
	public void setCdorcamentomaterial(Integer cdorcamentomaterial) {
		this.cdorcamentomaterial = cdorcamentomaterial;
	}
	public void setOrcamentomaterialsituacao(
			Orcamentomaterialsituacao orcamentomaterialsituacao) {
		this.orcamentomaterialsituacao = orcamentomaterialsituacao;
	}
	
}
