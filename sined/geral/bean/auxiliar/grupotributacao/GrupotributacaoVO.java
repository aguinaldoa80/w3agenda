package br.com.linkcom.sined.geral.bean.auxiliar.grupotributacao;

import java.sql.Date;
import java.util.List;

import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;

public class GrupotributacaoVO {

	private Operacao operacao; 
	private Empresa empresa; 
	private Naturezaoperacao naturezaoperacao; 
	private Boolean tributadoicms;
	private Boolean considerarTributadoicms = Boolean.TRUE;
	private Materialgrupo materialgrupo; 
	private String ncmcompleto; 
	private String materialtipo;
	private String materialnome;
	private Integer materialorigem;
	private String clientecpf; 
	private String clientecnpj;
	private String clientenomefantasia; 
	private String clientemunicipio;
	private String clienteuf;
	private String clienteregimetributacao; 
	private Boolean clienteincideiriss;
	private String clientecodigotributacao;
	private String clientecategoria;
	private String fornecedorcategoria;
	private String fornecedoruf;
	private Boolean clienteContribuinteICMS;
	private String clienteContribuinteicmstipo;
	private Boolean consumidorfinal;
	private String modelo;
	
	private Materialtipo materialTipoBean;
	private Localdestinonfe localDestinoNfe;
	private Contribuinteicmstipo contribuinteIcmsTipoEnum;
	private Date notaDtEmissao;
	private Operacaonfe clienteConsumidorFinal;
	private Operacaonfe operacaonfe;
	
	public GrupotributacaoVO(){}
	
	public GrupotributacaoVO(
			Operacao operacao, 
			Empresa empresa, 
			Naturezaoperacao naturezaoperacao, 
			Materialgrupo materialgrupo,
			Cliente cliente,
			Boolean tributadoicms, 
			Material material, 
			String ncmcompleto, 
			Endereco clienteEndereco, 
			Endereco fornecedorEndereco,
			List<Categoria> listaCategoriaCliente,
			List<Categoria> listaCategoriaFornecedor,
			Operacaonfe operacaonfe,
			Boolean considerarTributadoicms,
			ModeloDocumentoFiscalEnum modelodocumentoEnum,
			Localdestinonfe localDestinoNfe,
			Date notaDtEmissao){
		
		this.operacao = operacao;
		this.empresa = empresa;
		this.naturezaoperacao = naturezaoperacao;
		this.materialgrupo = materialgrupo;
		this.tributadoicms = tributadoicms;
		
		if(considerarTributadoicms != null){
			this.considerarTributadoicms = considerarTributadoicms;
		}
		
		if(cliente != null){
			this.clientecpf = (cliente.getCpf() != null ? cliente.getCpf().getValue() : null);
			this.clientecnpj = (cliente.getCnpj() != null ? cliente.getCnpj().getValue() : null);
			this.clientenomefantasia = cliente.getNome();
			this.clienteregimetributacao = (cliente.getCrt() != null ? cliente.getCrt().getDescricao() : "");
			this.clienteincideiriss = cliente.getIncidiriss();
			this.clienteContribuinteICMS = Contribuinteicmstipo.CONTRIBUINTE.equals(cliente.getContribuinteicmstipo());
			this.clienteContribuinteicmstipo = cliente.getContribuinteicmstipo()!=null ? cliente.getContribuinteicmstipo().name() : "";
			this.clientecodigotributacao = cliente.getCodigotributacao() != null ? cliente.getCodigotributacao().getCodigo() : "";
			this.contribuinteIcmsTipoEnum = cliente.getContribuinteicmstipo() != null ? cliente.getContribuinteicmstipo() : null;
			this.clienteConsumidorFinal = cliente.getOperacaonfe() != null ? cliente.getOperacaonfe() : null;
		}
		
		if(ncmcompleto != null && !"".equals(ncmcompleto)){
			this.ncmcompleto = ncmcompleto;
		} else if(material != null && material.getNcmcompleto() != null){
			this.ncmcompleto = material.getNcmcompleto();
		}
		
		if(material != null){
			this.materialnome = material.getNome();
			this.materialorigem = material.getOrigemproduto() != null ? material.getOrigemproduto().getValue() : null;
			this.materialtipo = material.getMaterialtipo() != null ? material.getMaterialtipo().getNome() : null;
			
			if(material.getTributacaoestadual() != null && 
					material.getTributacaoestadual() && 
					tributadoicms != null && 
					tributadoicms){
				this.considerarTributadoicms = false;
				this.contribuinteIcmsTipoEnum = null;
			}
			if(material.getModelodocumentofiscalICMS() != null && material.getModelodocumentofiscalICMS()){
				this.considerarTributadoicms = true;
			}
			
			this.materialTipoBean = material.getMaterialtipo() != null ? material.getMaterialtipo() : null;
		}
		
		if(clienteEndereco != null && clienteEndereco.getMunicipio() != null){
			if(clienteEndereco.getMunicipio().getNome() != null){
				this.clientemunicipio = clienteEndereco.getMunicipio().getNome();
			}
			if(clienteEndereco.getMunicipio().getUf() != null &&
					clienteEndereco.getMunicipio().getUf().getSigla() != null){
				this.clienteuf = clienteEndereco.getMunicipio().getUf().getSigla();
			}
		}
		
		if(fornecedorEndereco != null){ 
			if(fornecedorEndereco.getMunicipio() != null &&
					fornecedorEndereco.getMunicipio().getUf() != null &&
					fornecedorEndereco.getMunicipio().getUf().getSigla() != null){
				this.fornecedoruf = fornecedorEndereco.getMunicipio().getUf().getSigla();
			}
		}
		
		if(listaCategoriaCliente != null && listaCategoriaCliente.size() > 0){
			this.clientecategoria = CollectionsUtil.listAndConcatenate(listaCategoriaCliente, "nome", ", ");
		}
		if(listaCategoriaFornecedor != null && listaCategoriaFornecedor.size() > 0){
			this.fornecedorcategoria = CollectionsUtil.listAndConcatenate(listaCategoriaFornecedor, "nome", ", ");
		}
		if(operacaonfe != null){
			this.consumidorfinal = Operacaonfe.CONSUMIDOR_FINAL.equals(operacaonfe);
			this.operacaonfe = operacaonfe;
		}
		if(modelodocumentoEnum != null){
			this.modelo = modelodocumentoEnum.name();
		}
		if(localDestinoNfe != null) {
			this.localDestinoNfe = localDestinoNfe;
		}
		if(notaDtEmissao != null){
			this.notaDtEmissao = notaDtEmissao;
		}
	}

	public Operacao getOperacao() {
		return operacao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public String getClienteregimetributacao() {
		return clienteregimetributacao;
	}
	public Boolean getTributadoicms() {
		return tributadoicms;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public String getClienteuf() {
		return clienteuf;
	}
	public Boolean getClienteincideiriss() {
		return clienteincideiriss;
	}
	public String getClientecodigotributacao() {
		return clientecodigotributacao;
	}
	public String getFornecedoruf() {
		return fornecedoruf;
	}
	public String getMaterialtipo() {
		return materialtipo;
	}
	public String getMaterialnome() {
		return materialnome;
	}
	public Integer getMaterialorigem() {
		return materialorigem;
	}
	public String getClientecategoria() {
		return clientecategoria;
	}
	public String getFornecedorcategoria() {
		return fornecedorcategoria;
	}
	public String getClientecpf() {
		return clientecpf;
	}
	public String getClientenomefantasia() {
		return clientenomefantasia;
	}
	public String getClientemunicipio() {
		return clientemunicipio;
	}
	public String getClientecnpj() {
		return clientecnpj;
	}
	public Operacaonfe getClienteConsumidorFinal() {
		return clienteConsumidorFinal;
	}
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
	public void setClientecnpj(String clientecnpj) {
		this.clientecnpj = clientecnpj;
	}
	public void setMaterialtipo(String materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setMaterialnome(String materialnome) {
		this.materialnome = materialnome;
	}
	public void setMaterialorigem(Integer materialorigem) {
		this.materialorigem = materialorigem;
	}
	public void setClientecategoria(String clientecategoria) {
		this.clientecategoria = clientecategoria;
	}
	public void setFornecedorcategoria(String fornecedorcategoria) {
		this.fornecedorcategoria = fornecedorcategoria;
	}
	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setClienteregimetributacao(String clienteregimetributacao) {
		this.clienteregimetributacao = clienteregimetributacao;
	}
	public void setTributadoicms(Boolean tributadoicms) {
		this.tributadoicms = tributadoicms;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	public void setClienteuf(String clienteuf) {
		this.clienteuf = clienteuf;
	}
	public void setClienteincideiriss(Boolean clienteincideiriss) {
		this.clienteincideiriss = clienteincideiriss;
	}
	public void setClientecodigotributacao(String clientecodigotributacao) {
		this.clientecodigotributacao = clientecodigotributacao;
	}
	public void setFornecedoruf(String fornecedoruf) {
		this.fornecedoruf = fornecedoruf;
	}
	public Boolean getConsiderarTributadoicms() {
		return considerarTributadoicms;
	}
	public void setConsiderarTributadoicms(Boolean considerarTributadoicms) {
		this.considerarTributadoicms = considerarTributadoicms;
	}
	public void setClientecpf(String clientecpf) {
		this.clientecpf = clientecpf;
	}
	public void setClientenomefantasia(String clientenomefantasia) {
		this.clientenomefantasia = clientenomefantasia;
	}
	public void setClientemunicipio(String clientemunicipio) {
		this.clientemunicipio = clientemunicipio;
	}
	public Boolean getClienteContribuinteICMS() {
		return clienteContribuinteICMS;
	}
	public void setClienteContribuinteICMS(Boolean clienteContribuinteICMS) {
		this.clienteContribuinteICMS = clienteContribuinteICMS;
	}
	public String getClienteContribuinteicmstipo() {
		return clienteContribuinteicmstipo;
	}
	public Boolean getConsumidorfinal() {
		return consumidorfinal;
	}
	public void setConsumidorfinal(Boolean consumidorfinal) {
		this.consumidorfinal = consumidorfinal;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Materialtipo getMaterialTipoBean() {
		return materialTipoBean;
	}
	public void setMaterialTipoBean(Materialtipo materialTipoBean) {
		this.materialTipoBean = materialTipoBean;
	}
	public Localdestinonfe getLocalDestinoNfe() {
		return localDestinoNfe;
	}
	public void setLocalDestinoNfe(Localdestinonfe localDestinoNfe) {
		this.localDestinoNfe = localDestinoNfe;
	}
	public Contribuinteicmstipo getContribuinteIcmsTipoEnum() {
		return contribuinteIcmsTipoEnum;
	}
	public void setContribuinteIcmsTipoEnum(Contribuinteicmstipo contribuinteIcmsTipoEnum) {
		this.contribuinteIcmsTipoEnum = contribuinteIcmsTipoEnum;
	}
	public Date getNotaDtEmissao() {
		return notaDtEmissao;
	}
	public void setNotaDtEmissao(Date notaDtEmissao) {
		this.notaDtEmissao = notaDtEmissao;
	}
	public void setClienteConsumidorFinal(Operacaonfe clienteConsumidorFinal) {
		this.clienteConsumidorFinal = clienteConsumidorFinal;
	}
	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
}