package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;

@Entity
public class Aux_contrato {

	protected Integer cdcontrato;
	protected SituacaoContrato situacao;
	protected Long identificadornumerico;
	
	@Id
	public Integer getCdcontrato() {
		return cdcontrato;
	}

	public SituacaoContrato getSituacao() {
		return situacao;
	}
	
	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}
	
	public void setSituacao(SituacaoContrato situacao) {
		this.situacao = situacao;
	}
	
	public Long getIdentificadornumerico() {
		return identificadornumerico;
	}
	
	public void setIdentificadornumerico(Long identificadornumerico) {
		this.identificadornumerico = identificadornumerico;
	}
}
