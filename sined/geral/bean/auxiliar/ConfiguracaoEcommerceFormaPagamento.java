package br.com.linkcom.sined.geral.bean.auxiliar;

public class ConfiguracaoEcommerceFormaPagamento {

	private Integer idEcommerce;
	private Integer cdFormaPagamentoW3erp;
	
	public Integer getIdEcommerce() {
		return idEcommerce;
	}
	public void setIdEcommerce(Integer idEcommerce) {
		this.idEcommerce = idEcommerce;
	}
	public Integer getCdFormaPagamentoW3erp() {
		return cdFormaPagamentoW3erp;
	}
	public void setCdFormaPagamentoW3erp(Integer cdFormaPagamentoW3erp) {
		this.cdFormaPagamentoW3erp = cdFormaPagamentoW3erp;
	}
}
