package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Situacaorequisicao;

@Entity
public class Aux_requisicaomaterial {

	protected Integer cdrequisicaomaterial;
	protected Situacaorequisicao situacaorequisicao;
	
	@Id
	public Integer getCdrequisicaomaterial() {
		return cdrequisicaomaterial;
	}
	@Column(name="situacao")
	@DisplayName("Situa��o")
	public Situacaorequisicao getSituacaorequisicao() {
		return situacaorequisicao;
	}
	
	public void setCdrequisicaomaterial(Integer cdrequisicaomaterial) {
		this.cdrequisicaomaterial = cdrequisicaomaterial;
	}
	public void setSituacaorequisicao(Situacaorequisicao situacaorequisicao) {
		this.situacaorequisicao = situacaorequisicao;
	}
	
}
