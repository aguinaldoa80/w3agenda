package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavendamaximo;

import java.io.Serializable;

import br.com.linkcom.sined.geral.bean.Tabelavalor;

public class TabelavalorVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String identificador;
	private Double valor;
	
	public TabelavalorVO() {}
	
	public TabelavalorVO(Tabelavalor tabelavalor) {
		if(tabelavalor != null){
			this.identificador = tabelavalor.getIdentificador();
			this.valor = tabelavalor.getValor();
		}
	}
	
	public String getIdentificador() {
		return identificador;
	}
	public Double getValor() {
		return valor;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
}
