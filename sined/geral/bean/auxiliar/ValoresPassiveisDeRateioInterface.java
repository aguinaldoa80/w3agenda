package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;

public interface ValoresPassiveisDeRateioInterface {

	public Money getTotalvendaSemArredondamento();
	public Money getValorusadovalecompra();
	public Money getDesconto();
	public Money getValorfrete();
}
