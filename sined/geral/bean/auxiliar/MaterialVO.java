package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;

public class MaterialVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer cdmaterial;
	private String nome;
	private Double valorcusto;
	private Double valorvenda;
	
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public String getNome() {
		return nome;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getValorcusto() {
		return valorcusto;
	}
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public Double getValorvenda() {
		return valorvenda;
	}
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
}
