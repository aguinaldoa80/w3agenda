package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;

import br.com.linkcom.neo.types.Money;

public class ImpostoVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Money basecalculo;
	private Money aliquota;
	private Money valor;
	private Boolean incide;
	private Boolean cumulativoCobrado;
	
	private Money aliquotaretencao;
	
	public Money getBasecalculo() {
		return basecalculo;
	}
	public Money getAliquota() {
		return aliquota;
	}
	public Money getValor() {
		return valor;
	}
	public Boolean getIncide() {
		return incide;
	}
	public Boolean getCumulativoCobrado() {
		return cumulativoCobrado;
	}
	public void setBasecalculo(Money basecalculo) {
		this.basecalculo = basecalculo;
	}
	public void setAliquota(Money aliquota) {
		this.aliquota = aliquota;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setIncide(Boolean incide) {
		this.incide = incide;
	}
	public void setCumulativoCobrado(Boolean cumulativoCobrado) {
		this.cumulativoCobrado = cumulativoCobrado;
	}
	public Money getAliquotaretencao() {
		return aliquotaretencao;
	}
	public void setAliquotaretencao(Money aliquotaretencao) {
		this.aliquotaretencao = aliquotaretencao;
	}
}