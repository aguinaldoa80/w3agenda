package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.types.Money;

@Entity
public class Aux_proposta {
	
	private Integer cdproposta;
	private Date dtvalidade;
	private String revisaoptm;
	private String revisaopcm;
	private Date dtentregapcm;
	private Date dtentregaptm;
	private Money valor;
	private Money mobilizacao;
	private Money mdo;
	private Money materiais;
	
	@Id
	public Integer getCdproposta() {
		return cdproposta;
	}


	public Date getDtvalidade() {
		return dtvalidade;
	}


	public Money getValor() {
		return valor;
	}

	
	
	public Money getMobilizacao() {
		return mobilizacao;
	}


	public Money getMdo() {
		return mdo;
	}


	public Money getMateriais() {
		return materiais;
	}
	
	

	public Date getDtentregapcm() {
		return dtentregapcm;
	}


	public Date getDtentregaptm() {
		return dtentregaptm;
	}
	
	public String getRevisaoptm() {
		return revisaoptm;
	}
	
	public String getRevisaopcm() {
		return revisaopcm;
	}
	public void setDtentregapcm(Date dtentregapcm) {
		this.dtentregapcm = dtentregapcm;
	}


	public void setDtentregaptm(Date dtentregaptm) {
		this.dtentregaptm = dtentregaptm;
	}


	public void setMobilizacao(Money mobilizacao) {
		this.mobilizacao = mobilizacao;
	}


	public void setMdo(Money mdo) {
		this.mdo = mdo;
	}


	public void setMateriais(Money materiais) {
		this.materiais = materiais;
	}


	public void setCdproposta(Integer cdproposta) {
		this.cdproposta = cdproposta;
	}


	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}


	public void setRevisaoptm(String revisaoptm) {
		this.revisaoptm = revisaoptm;
	}
	
	public void setRevisaopcm(String revisaopcm) {
		this.revisaopcm = revisaopcm;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

}
