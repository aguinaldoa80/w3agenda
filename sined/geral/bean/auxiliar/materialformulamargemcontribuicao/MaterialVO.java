package br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao;

public class MaterialVO {

	private int cdmaterial;
	private double valorcusto = 0d;
	private double valorvenda = 0d;
	
	public int getCdmaterial() {
		return cdmaterial;
	}
	public double getValorcusto() {
		return valorcusto;
	}
	public double getValorvenda() {
		return valorvenda;
	}
	
	public void setCdmaterial(int cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setValorcusto(double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public void setValorvenda(double valorvenda) {
		this.valorvenda = valorvenda;
	}
}
