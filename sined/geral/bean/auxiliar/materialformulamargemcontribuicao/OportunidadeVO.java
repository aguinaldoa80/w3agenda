package br.com.linkcom.sined.geral.bean.auxiliar.materialformulamargemcontribuicao;

import java.io.Serializable;

public class OportunidadeVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private double valorunitario;
	private double quantidade;
	private double comissao;
	private Boolean revendedor;
	
	public double getValorunitario() {
		return valorunitario;
	}
	public double getQuantidade() {
		return quantidade;
	}
	public double getComissao() {
		return comissao;
	}
	public Boolean getRevendedor() {
		return revendedor;
	}
	
	public void setValorunitario(double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	public void setRevendedor(Boolean revendedor) {
		this.revendedor = revendedor;
	}
}
