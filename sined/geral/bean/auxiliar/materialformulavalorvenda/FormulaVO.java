package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda;

import java.util.ArrayList;
import java.util.List;

public class FormulaVO {

	private int cdgrupomaterial;
	private int origem;
	private MaterialVO aux_materialVO;
	private List<MaterialVO> listaMaterial = new ArrayList<MaterialVO>();	
	
	public MaterialVO getAux_materialVO() {
		return aux_materialVO;
	}
	public List<MaterialVO> getListaMaterial() {
		return listaMaterial;
	}
	
	public void setAux_materialVO(MaterialVO auxMaterialVO) {
		aux_materialVO = auxMaterialVO;
	}
	public void setListaMaterial(List<MaterialVO> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	
	public void addMaterial(MaterialVO materialVO){
		listaMaterial.add(materialVO);
	}
	
	public double largura(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getLargura();
				}
			}
		}
		return 0.0;
	}
	
	public double altura(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getAltura();
				}
			}
		}
		return 0.0;
	}
	
	public double comprimento(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getComprimento();
				}
			}
		}
		return 0.0;
	}
	
	public double frete(Integer cdmaterial){
		if(cdmaterial == null){
			return aux_materialVO.getFrete();
		}
		
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getFrete();
				}
			}
		}
		return 0.0;
	}
	
	public double pesobruto(Integer cdmaterial){
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getPesobruto();
				}
			}
		}
		return 0.0;
	}
	
	public double valorcusto(){
		return aux_materialVO.getValorcusto();
	}
	
	public double valorcusto(Integer cdmaterial){
		if(cdmaterial == null){
			return aux_materialVO.getValorcusto();
		}
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getValorcusto();
				}
			}
		}
		return 0.0;
	}
	
	public double valorvenda(){
		return aux_materialVO.getValorvenda();
	}
	
	public double valorvenda(Integer cdmaterial){
		if(cdmaterial == null){
			return aux_materialVO.getValorvenda();
		}
		if(listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == cdmaterial){
					return materialVO.getValorvenda();
				}
			}
		}
		return 0.0;
	}
	
	public int getCdgrupomaterial() {
		return cdgrupomaterial;
	}
	public int getOrigem() {
		return origem;
	}
	
	public void setCdgrupomaterial(int cdgrupomaterial) {
		this.cdgrupomaterial = cdgrupomaterial;
	}
	public void setOrigem(int origem) {
		this.origem = origem;
	}
	
	public double preco_banda(){
		if(aux_materialVO.getCdmaterialbanda() != null && listaMaterial != null && listaMaterial.size() > 0){
			for (MaterialVO materialVO : listaMaterial) {
				if(materialVO.getCdmaterial() == aux_materialVO.getCdmaterialbanda()){
					return materialVO.getValorvenda();
				}
			}
		}
		return 0.0;
	}
}
