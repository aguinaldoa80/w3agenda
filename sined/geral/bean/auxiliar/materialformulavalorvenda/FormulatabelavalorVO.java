package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.sined.util.SinedUtil;

public class FormulatabelavalorVO {

	private List<TabelavalorVO> listaTabelavalor = new ArrayList<TabelavalorVO>();

	public List<TabelavalorVO> getListaTabelavalor() {
		return listaTabelavalor;
	}
	
	public void setListaTabelavalor(List<TabelavalorVO> listaTabelavalor) {
		this.listaTabelavalor = listaTabelavalor;
	}
	
	public void addTabelavalor(TabelavalorVO tabelavalorVO){
		listaTabelavalor.add(tabelavalorVO);
	}
	
	public double retorna(String identificador){
		if(StringUtils.isNotEmpty(identificador) && SinedUtil.isListNotEmpty(listaTabelavalor)){
			for (TabelavalorVO tabelavalorVO : listaTabelavalor) {
				if(identificador.equals(tabelavalorVO.getIdentificador())){
					return tabelavalorVO.getValor();
				}
			}
		}
		return 0.0;
	}
}
