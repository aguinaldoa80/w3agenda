package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda;

public class VendaVO {

	private double largura = 0d;
	private double comprimento = 0d;
	private double altura = 0d;
	private double frete = 0d;
	
	public double getLargura() {
		return largura;
	}
	public double getComprimento() {
		return comprimento;
	}
	public double getAltura() {
		return altura;
	}
	public double getFrete() {
		return frete;
	}
	
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public void setFrete(double frete) {
		this.frete = frete;
	}
}
