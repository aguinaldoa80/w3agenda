package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavalorvenda;

import java.io.Serializable;

public class OportunidadeVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Boolean revendedor;
	private double quantidade;

	public Boolean getRevendedor() {
		return revendedor;
	}
	public double getQuantidade() {
		return quantidade;
	}
	
	public void setRevendedor(Boolean revendedor) {
		this.revendedor = revendedor;
	}
	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}
}
