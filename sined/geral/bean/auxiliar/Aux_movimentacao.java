package br.com.linkcom.sined.geral.bean.auxiliar;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Aux_movimentacao {

	protected Integer cdmovimentacao;
	protected String movimentacaodescricao;
	
	@Id
	public Integer getCdmovimentacao() {
		return cdmovimentacao;
	}
	@DisplayName("Descri��o")
	public String getMovimentacaodescricao() {
		return movimentacaodescricao;
	}
	
	public void setCdmovimentacao(Integer cdmovimentacao) {
		this.cdmovimentacao = cdmovimentacao;
	}
	public void setMovimentacaodescricao(String movimentacaodescricao) {
		this.movimentacaodescricao = movimentacaodescricao;
	}
	
}
