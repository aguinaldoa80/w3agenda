package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.sined.geral.bean.NovidadeVersao;

public class Aux_novidadeVersao {
	private NovidadeVersao novidadeVersao;
	
	public void setNovidadeVersao(NovidadeVersao novidadeVersao) {
		this.novidadeVersao = novidadeVersao;
	}
	
	public NovidadeVersao getNovidadeVersao() {
		return novidadeVersao;
	}
}
