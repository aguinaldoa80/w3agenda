package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;

import br.com.linkcom.neo.types.Money;

public class DistribuicaoReceitaDespesaItemVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String descricao;
	private Money valor;
	
	public DistribuicaoReceitaDespesaItemVO() {
	}
	
	public DistribuicaoReceitaDespesaItemVO(String descricao, Money valor) {
		this.descricao = descricao;
		this.valor = valor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
