package br.com.linkcom.sined.geral.bean.auxiliar;

public class PrazopagamentoVO {
	
	private Integer cdprazopagamento;
	private String nome;
	
	public PrazopagamentoVO(Integer cdprazopagamento, String nome){
		this.cdprazopagamento = cdprazopagamento;
		this.nome = nome;
	}
	
	public Integer getCdprazopagamento() {
		return cdprazopagamento;
	}
	public String getNome() {
		return nome;
	}
	
	public void setCdprazopagamento(Integer cdprazopagamento) {
		this.cdprazopagamento = cdprazopagamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
}