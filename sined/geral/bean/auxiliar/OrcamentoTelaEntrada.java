package br.com.linkcom.sined.geral.bean.auxiliar;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Bdicalculotipo;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Oportunidade;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Orcamentosituacao;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.Uf;

public class OrcamentoTelaEntrada {

	private Orcamento orcamento;
	
	private List<Cliente> listaCliente = new ArrayList<Cliente>();
	private List<Projetotipo> listaProjetotipo = new ArrayList<Projetotipo>();
	private List<Oportunidade> listaOportunidade = new ArrayList<Oportunidade>();
	private List<Bdicalculotipo> listaBdicalculotipo = new ArrayList<Bdicalculotipo>();
	private List<Orcamentosituacao> listaOrcamentosituacao = new ArrayList<Orcamentosituacao>();
	private List<Uf> listaUf = new ArrayList<Uf>();
	private List<Contagerencial> listaContagerencial = new ArrayList<Contagerencial>();
	
	
	public Orcamento getOrcamento() {
		return orcamento;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public List<Cliente> getListaCliente() {
		return listaCliente;
	}
	public List<Projetotipo> getListaProjetotipo() {
		return listaProjetotipo;
	}
	public List<Oportunidade> getListaOportunidade() {
		return listaOportunidade;
	}
	public List<Bdicalculotipo> getListaBdicalculotipo() {
		return listaBdicalculotipo;
	}
	public List<Orcamentosituacao> getListaOrcamentosituacao() {
		return listaOrcamentosituacao;
	}
	public List<Uf> getListaUf() {
		return listaUf;
	}
	public List<Contagerencial> getListaContagerencial() {
		return listaContagerencial;
	}
	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}
	public void setListaProjetotipo(List<Projetotipo> listaProjetotipo) {
		this.listaProjetotipo = listaProjetotipo;
	}
	public void setListaOportunidade(List<Oportunidade> listaOportunidade) {
		this.listaOportunidade = listaOportunidade;
	}
	public void setListaBdicalculotipo(List<Bdicalculotipo> listaBdicalculotipo) {
		this.listaBdicalculotipo = listaBdicalculotipo;
	}
	public void setListaOrcamentosituacao(
			List<Orcamentosituacao> listaOrcamentosituacao) {
		this.listaOrcamentosituacao = listaOrcamentosituacao;
	}
	public void setListaUf(List<Uf> listaUf) {
		this.listaUf = listaUf;
	}
	public void setListaContagerencial(List<Contagerencial> listaContagerencial) {
		this.listaContagerencial = listaContagerencial;
	}
}
