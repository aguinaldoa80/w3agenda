package br.com.linkcom.sined.geral.bean.auxiliar;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Material;

public class Aux_VendamaterialTabelapreco {

	private Money desconto;
	private Money percentualdesconto;
	private Double valorvendamaterial;
	private Double valorvendamaterialsemdesconto;
	private Material material;
	
	public Aux_VendamaterialTabelapreco(Material material){
		this.setMaterial(material);
	}
	
	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	
	public Money getPercentualdesconto() {
		return percentualdesconto;
	}
	public void setPercentualdesconto(Money percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}

	public Double getValorvendamaterialsemdesconto() {
		return valorvendamaterialsemdesconto;
	}
	public void setValorvendamaterialsemdesconto(Double valorvendamaterialsemdesconto) {
		this.valorvendamaterialsemdesconto = valorvendamaterialsemdesconto;
	}
}
