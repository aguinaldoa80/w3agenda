package br.com.linkcom.sined.geral.bean.auxiliar.materialformulavendaminimo;

public class MaterialVO {

	private int cdmaterial;
	private double largura = 0d;
	private double comprimento = 0d;
	private double altura = 0d;
	private double frete = 0d;
	private double valorcusto = 0d;
	private double valorvenda = 0d;
	private double pesobruto = 0d;
	private double valorvendabanda = 0d;
	private Integer cdmaterialbanda;
	
	public int getCdmaterial() {
		return cdmaterial;
	}
	public double getLargura() {
		return largura;
	}
	public double getComprimento() {
		return comprimento;
	}
	public double getAltura() {
		return altura;
	}
	public double getFrete() {
		return frete;
	}
	public double getValorcusto() {
		return valorcusto;
	}
	public double getValorvenda() {
		return valorvenda;
	}
	public double getPesobruto() {
		return pesobruto;
	}
	public double getValorvendabanda() {
		return valorvendabanda;
	}
	
	public void setCdmaterial(int cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setLargura(double largura) {
		this.largura = largura;
	}
	public void setComprimento(double comprimento) {
		this.comprimento = comprimento;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public void setFrete(double frete) {
		this.frete = frete;
	}
	public void setValorcusto(double valorcusto) {
		this.valorcusto = valorcusto;
	}
	public void setValorvenda(double valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setPesobruto(double pesobruto) {
		this.pesobruto = pesobruto;
	}
	public void setValorvendabanda(double valorvendabanda) {
		this.valorvendabanda = valorvendabanda;
	}

	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}
	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}
}
