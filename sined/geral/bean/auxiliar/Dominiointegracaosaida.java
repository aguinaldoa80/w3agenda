package br.com.linkcom.sined.geral.bean.auxiliar;

import java.sql.Date;

import br.com.linkcom.utils.SIntegraUtil;

public class Dominiointegracaosaida {

	public final static String SAIDA = "02";
	
	private String saida;
	private Integer sequencial;
	private Integer codigoempresa;
	private String inscricaoclienteCpfCnpjCei;
	private Integer codigoespecie;
	private Integer codigoexclusaodief;
	private Integer codigoacumulador;
	private Integer cfop;
	private String siglaestadocliente;
	private Integer seguimento;
	private String numerodocumento;
	private String serie;
	private Integer documentofinal;
	private Date datasaida;
	private Date dataemissao;
	private Double valorcontabil;
	private Double valorexcusaodief;
	private String reservado;	
	private String modalidadefrete;
	private Integer codigomunicipio;
	private String fatogeradorcrf;
	private String fatogeradorcrfop;
	private String fatogeradorirrfp;
	private Integer tiporeceita;
	private String branco167;
	private Integer cfopestendidodetalhamento;
	private Integer codigotransferencianotacredito;
	private Integer codigoobservacao;
	private Date datavisto;
	private Integer codigotipoantecipacaotributaria;
	private Double valorfrete;
	private Double valorseguro;
	private Double valordespesasacessorias;
	private Double valorprodutos;	
	private Integer valorbcicmsst;
	private Double outrassaidas;
	private Double saidasisentas;
	private Double saidasisentascupomfiscal;
	private Double saidasisentasmodelo02;
	private Integer codigomodelodocumentofiscal;
	private Integer codigofiscalprestacaoservico;
	private Integer codigosituacaotributaria;
	private Integer subserie;
	private String tipotitulo;
	private String indentificacaotitulo;
	private String inscricaoestadualcliente;
	private String inscricaomunicipalcliente;
	private String observacao;
	private String chavenfe;
	private String codigorecolhimentofethab;
	private String responsavelrecolhimentofethab;
	private Integer tipocte;
	private String ctereferencia;
	private String brancos;
	
	@Override
	public String toString() {
		return SIntegraUtil.printString(getSaida(), 2) + 
			   SIntegraUtil.printInteger(getSequencial(), 7) +
			   SIntegraUtil.printInteger(getCodigoempresa(), 7) +
			   SIntegraUtil.printStringCpfCnpj(getInscricaoclienteCpfCnpjCei(), 14) +
			   SIntegraUtil.printInteger(getCodigoespecie(), 7) +
			   SIntegraUtil.printInteger(getCodigoexclusaodief(), 2) +
			   SIntegraUtil.printInteger(getCodigoacumulador(), 7) +
			   SIntegraUtil.printInteger(getCfop(), 7) +
			   SIntegraUtil.printInteger(getSeguimento(), 2) +
			   SIntegraUtil.printStringOrigemInteger(getNumerodocumento(), 7) +			   
			   SIntegraUtil.printString(getSerie(), 7) + 
			   SIntegraUtil.printInteger(getDocumentofinal(), 7) +
			   SIntegraUtil.printDateDominio(getDatasaida(), 10) +
			   SIntegraUtil.printDateDominio(getDataemissao(), 10) +
			   SIntegraUtil.printDouble(getValorcontabil(), 13) +
			   SIntegraUtil.printDouble(getValorexcusaodief(), 13) +
			   SIntegraUtil.printString(getReservado(), 30) +
			   SIntegraUtil.printString(getModalidadefrete(), 1) + 	   
			   SIntegraUtil.printInteger(getCodigomunicipio(), 7) +
			   SIntegraUtil.printString(getFatogeradorcrf(), 1) +
			   SIntegraUtil.printString(getFatogeradorcrfop(), 1) +
			   SIntegraUtil.printString(getFatogeradorirrfp(), 1) +
			   SIntegraUtil.printInteger(getTiporeceita(), 1) +
			   SIntegraUtil.printString(getBranco167(), 1) +
			   SIntegraUtil.printInteger(getCfopestendidodetalhamento(), 7) +
			   SIntegraUtil.printInteger(getCodigotransferencianotacredito(), 7) +
			   SIntegraUtil.printInteger(getCodigoobservacao(), 7) +
			   SIntegraUtil.printDateDominio(getDatavisto(), 10) +
			   SIntegraUtil.printInteger(getCodigotipoantecipacaotributaria(), 7) +
			   SIntegraUtil.printDouble(getValorfrete(), 13) +
			   SIntegraUtil.printDouble(getValorseguro(), 13) +
			   SIntegraUtil.printDouble(getValordespesasacessorias(), 13) +
			   SIntegraUtil.printDouble(getValorprodutos(), 13) +			   
			   SIntegraUtil.printInteger(getValorbcicmsst(), 13) +
			   SIntegraUtil.printDouble(getOutrassaidas(), 13) +
			   SIntegraUtil.printDouble(getSaidasisentas(), 13) +
			   SIntegraUtil.printDouble(getSaidasisentascupomfiscal(), 13) +
			   SIntegraUtil.printDouble(getSaidasisentasmodelo02(), 13) +
			   SIntegraUtil.printInteger(getCodigomodelodocumentofiscal(), 7) +
			   SIntegraUtil.printInteger(getCodigosituacaotributaria(), 7) +
			   SIntegraUtil.printInteger(getSubserie(), 7) +
			   SIntegraUtil.printString(getTipotitulo(), 2) +
			   SIntegraUtil.printString(getIndentificacaotitulo(), 50) +
			   SIntegraUtil.printString(getInscricaoestadualcliente(), 20) +
			   SIntegraUtil.printString(getInscricaomunicipalcliente(), 20) +
			   SIntegraUtil.printString(getObservacao(), 300) +
			   SIntegraUtil.printString(getChavenfe(), 44) +
			   SIntegraUtil.printString(getCodigorecolhimentofethab(), 6) +
			   SIntegraUtil.printString(getResponsavelrecolhimentofethab(), 1) +
			   SIntegraUtil.printInteger(getTipocte(), 1) +
			   SIntegraUtil.printString(getCtereferencia(), 44) +
			   SIntegraUtil.printString(getBrancos(), 48) +		   
			   
			   SIntegraUtil.SEPARADOR_LINHA;
	}

	public String getSaida() {
		return saida;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public Integer getCodigoempresa() {
		return codigoempresa;
	}

	public String getInscricaoclienteCpfCnpjCei() {
		return inscricaoclienteCpfCnpjCei;
	}

	public Integer getCodigoespecie() {
		return codigoespecie;
	}

	public Integer getCodigoexclusaodief() {
		return codigoexclusaodief;
	}

	public Integer getCodigoacumulador() {
		return codigoacumulador;
	}

	public Integer getCfop() {
		return cfop;
	}

	public String getSiglaestadocliente() {
		return siglaestadocliente;
	}

	public Integer getSeguimento() {
		return seguimento;
	}

	public String getNumerodocumento() {
		return numerodocumento;
	}

	public String getSerie() {
		return serie;
	}

	public Integer getDocumentofinal() {
		return documentofinal;
	}

	public Date getDatasaida() {
		return datasaida;
	}

	public Date getDataemissao() {
		return dataemissao;
	}

	public Double getValorcontabil() {
		return valorcontabil;
	}

	public Double getValorexcusaodief() {
		return valorexcusaodief;
	}

	public String getReservado() {
		return reservado;
	}

	public String getModalidadefrete() {
		return modalidadefrete;
	}

	public Integer getCodigomunicipio() {
		return codigomunicipio;
	}

	public String getFatogeradorcrf() {
		return fatogeradorcrf;
	}

	public String getFatogeradorcrfop() {
		return fatogeradorcrfop;
	}

	public String getFatogeradorirrfp() {
		return fatogeradorirrfp;
	}

	public Integer getTiporeceita() {
		return tiporeceita;
	}

	public String getBranco167() {
		return branco167;
	}

	public Integer getCfopestendidodetalhamento() {
		return cfopestendidodetalhamento;
	}

	public Integer getCodigotransferencianotacredito() {
		return codigotransferencianotacredito;
	}

	public Integer getCodigoobservacao() {
		return codigoobservacao;
	}

	public Date getDatavisto() {
		return datavisto;
	}

	public Integer getCodigotipoantecipacaotributaria() {
		return codigotipoantecipacaotributaria;
	}	

	public Double getValorfrete() {
		return valorfrete;
	}

	public Double getValorseguro() {
		return valorseguro;
	}

	public Double getValordespesasacessorias() {
		return valordespesasacessorias;
	}

	public Double getValorprodutos() {
		return valorprodutos;
	}

	public Integer getValorbcicmsst() {
		return valorbcicmsst;
	}

	public Double getOutrassaidas() {
		return outrassaidas;
	}

	public Double getSaidasisentas() {
		return saidasisentas;
	}

	public Double getSaidasisentascupomfiscal() {
		return saidasisentascupomfiscal;
	}

	public Double getSaidasisentasmodelo02() {
		return saidasisentasmodelo02;
	}

	public Integer getCodigomodelodocumentofiscal() {
		return codigomodelodocumentofiscal;
	}

	public Integer getCodigofiscalprestacaoservico() {
		return codigofiscalprestacaoservico;
	}

	public Integer getCodigosituacaotributaria() {
		return codigosituacaotributaria;
	}

	public Integer getSubserie() {
		return subserie;
	}

	public String getTipotitulo() {
		return tipotitulo;
	}

	public String getIndentificacaotitulo() {
		return indentificacaotitulo;
	}

	public String getInscricaoestadualcliente() {
		return inscricaoestadualcliente;
	}

	public String getInscricaomunicipalcliente() {
		return inscricaomunicipalcliente;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getChavenfe() {
		return chavenfe;
	}

	public String getCodigorecolhimentofethab() {
		return codigorecolhimentofethab;
	}

	public String getResponsavelrecolhimentofethab() {
		return responsavelrecolhimentofethab;
	}

	public Integer getTipocte() {
		return tipocte;
	}

	public String getCtereferencia() {
		return ctereferencia;
	}

	public String getBrancos() {
		return brancos;
	}

	public void setSaida(String saida) {
		this.saida = saida;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setCodigoempresa(Integer codigoempresa) {
		this.codigoempresa = codigoempresa;
	}

	public void setInscricaoclienteCpfCnpjCei(
			String inscricaoclienteCpfCnpjCei) {
		this.inscricaoclienteCpfCnpjCei = inscricaoclienteCpfCnpjCei;
	}

	public void setCodigoespecie(Integer codigoespecie) {
		this.codigoespecie = codigoespecie;
	}

	public void setCodigoexclusaodief(Integer codigoexclusaodief) {
		this.codigoexclusaodief = codigoexclusaodief;
	}

	public void setCodigoacumulador(Integer codigoacumulador) {
		this.codigoacumulador = codigoacumulador;
	}

	public void setCfop(Integer cfop) {
		this.cfop = cfop;
	}

	public void setSiglaestadocliente(String siglaestadocliente) {
		this.siglaestadocliente = siglaestadocliente;
	}

	public void setSeguimento(Integer seguimento) {
		this.seguimento = seguimento;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setDocumentofinal(Integer documentofinal) {
		this.documentofinal = documentofinal;
	}

	public void setDatasaida(Date datasaida) {
		this.datasaida = datasaida;
	}

	public void setDataemissao(Date dataemissao) {
		this.dataemissao = dataemissao;
	}

	public void setValorcontabil(Double valorcontabil) {
		this.valorcontabil = valorcontabil;
	}

	public void setValorexcusaodief(Double valorexcusaodief) {
		this.valorexcusaodief = valorexcusaodief;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	public void setModalidadefrete(String modalidadefrete) {
		this.modalidadefrete = modalidadefrete;
	}

	public void setCodigomunicipio(Integer codigomunicipio) {
		this.codigomunicipio = codigomunicipio;
	}

	public void setFatogeradorcrf(String fatogeradorcrf) {
		this.fatogeradorcrf = fatogeradorcrf;
	}

	public void setFatogeradorcrfop(String fatogeradorcrfop) {
		this.fatogeradorcrfop = fatogeradorcrfop;
	}

	public void setFatogeradorirrfp(String fatogeradorirrfp) {
		this.fatogeradorirrfp = fatogeradorirrfp;
	}

	public void setTiporeceita(Integer tiporeceita) {
		this.tiporeceita = tiporeceita;
	}

	public void setBranco167(String branco167) {
		this.branco167 = branco167;
	}

	public void setCfopestendidodetalhamento(Integer cfopestendidodetalhamento) {
		this.cfopestendidodetalhamento = cfopestendidodetalhamento;
	}

	public void setCodigotransferencianotacredito(
			Integer codigotransferencianotacredito) {
		this.codigotransferencianotacredito = codigotransferencianotacredito;
	}

	public void setCodigoobservacao(Integer codigoobservacao) {
		this.codigoobservacao = codigoobservacao;
	}

	public void setDatavisto(Date datavisto) {
		this.datavisto = datavisto;
	}
	
	public void setCodigotipoantecipacaotributaria(
			Integer codigotipoantecipacaotributaria) {
		this.codigotipoantecipacaotributaria = codigotipoantecipacaotributaria;
	}

	public void setValorfrete(Double valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValorseguro(Double valorseguro) {
		this.valorseguro = valorseguro;
	}

	public void setValordespesasacessorias(Double valordespesasacessorias) {
		this.valordespesasacessorias = valordespesasacessorias;
	}

	public void setValorprodutos(Double valorprodutos) {
		this.valorprodutos = valorprodutos;
	}

	public void setValorbcicmsst(Integer valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public void setOutrassaidas(Double outrassaidas) {
		this.outrassaidas = outrassaidas;
	}

	public void setSaidasisentas(Double saidasisentas) {
		this.saidasisentas = saidasisentas;
	}

	public void setSaidasisentascupomfiscal(Double saidasisentascupomfiscal) {
		this.saidasisentascupomfiscal = saidasisentascupomfiscal;
	}

	public void setSaidasisentasmodelo02(Double saidasisentasmodelo02) {
		this.saidasisentasmodelo02 = saidasisentasmodelo02;
	}

	public void setCodigomodelodocumentofiscal(Integer codigomodelodocumentofiscal) {
		this.codigomodelodocumentofiscal = codigomodelodocumentofiscal;
	}

	public void setCodigofiscalprestacaoservico(Integer codigofiscalprestacaoservico) {
		this.codigofiscalprestacaoservico = codigofiscalprestacaoservico;
	}

	public void setCodigosituacaotributaria(Integer codigosituacaotributaria) {
		this.codigosituacaotributaria = codigosituacaotributaria;
	}

	public void setSubserie(Integer subserie) {
		this.subserie = subserie;
	}

	public void setTipotitulo(String tipotitulo) {
		this.tipotitulo = tipotitulo;
	}

	public void setIndentificacaotitulo(String indentificacaotitulo) {
		this.indentificacaotitulo = indentificacaotitulo;
	}

	public void setInscricaoestadualcliente(String inscricaoestadualcliente) {
		this.inscricaoestadualcliente = inscricaoestadualcliente;
	}

	public void setInscricaomunicipalcliente(String inscricaomunicipalcliente) {
		this.inscricaomunicipalcliente = inscricaomunicipalcliente;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setChavenfe(String chavenfe) {
		this.chavenfe = chavenfe;
	}

	public void setCodigorecolhimentofethab(String codigorecolhimentofethab) {
		this.codigorecolhimentofethab = codigorecolhimentofethab;
	}

	public void setResponsavelrecolhimentofethab(
			String responsavelrecolhimentofethab) {
		this.responsavelrecolhimentofethab = responsavelrecolhimentofethab;
	}

	public void setTipocte(Integer tipocte) {
		this.tipocte = tipocte;
	}

	public void setCtereferencia(String ctereferencia) {
		this.ctereferencia = ctereferencia;
	}

	public void setBrancos(String brancos) {
		this.brancos = brancos;
	}
}
