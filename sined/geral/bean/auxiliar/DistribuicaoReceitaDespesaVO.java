package br.com.linkcom.sined.geral.bean.auxiliar;

import java.io.Serializable;
import java.util.List;

public class DistribuicaoReceitaDespesaVO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<DistribuicaoReceitaDespesaItemVO> listaReceita;
	private List<DistribuicaoReceitaDespesaItemVO> listaDespesa;
	
	public List<DistribuicaoReceitaDespesaItemVO> getListaReceita() {
		return listaReceita;
	}
	public List<DistribuicaoReceitaDespesaItemVO> getListaDespesa() {
		return listaDespesa;
	}
	public void setListaReceita(List<DistribuicaoReceitaDespesaItemVO> listaReceita) {
		this.listaReceita = listaReceita;
	}
	public void setListaDespesa(List<DistribuicaoReceitaDespesaItemVO> listaDespesa) {
		this.listaDespesa = listaDespesa;
	}
}
