package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporecursogeral;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_tarefarecursogeral",sequenceName="sq_tarefarecursogeral")
public class Tarefarecursogeral implements Log, PermissaoProjeto{

	protected Integer cdtarefarecursogeral;
	protected Tarefa tarefa;
	protected Material material;
	protected Double qtde;
	protected Boolean acumulativo;
	protected Tiporecursogeral tiporecursogeral;
	protected String outro;
	protected Unidademedida unidademedida;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTE PARA O FLEX
	protected Integer tiporecursogeralflex;
	protected Boolean existeRecursogeralComOrigem;
	
	protected List<Solicitacaocompraorigem> listaSolicitacaocompraorigem = new ListSet<Solicitacaocompraorigem>(Solicitacaocompraorigem.class);
	
	public Tarefarecursogeral() {
	}
	
	public Tarefarecursogeral(Integer cdtarefarecursogeral) {
		this.cdtarefarecursogeral = cdtarefarecursogeral;
	}
	
	@Id
	@GeneratedValue(generator="sq_tarefarecursogeral",strategy=GenerationType.AUTO)
	public Integer getCdtarefarecursogeral() {
		return cdtarefarecursogeral;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefa")
	public Tarefa getTarefa() {
		return tarefa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	public Double getQtde() {
		return qtde;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Boolean getAcumulativo() {
		return acumulativo;
	}
	public String getOutro() {
		return outro;
	}
	public Tiporecursogeral getTiporecursogeral() {
		return tiporecursogeral;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setTiporecursogeral(Tiporecursogeral tiporecursogeral) {
		this.tiporecursogeral = tiporecursogeral;
	}
	public void setOutro(String outro) {
		this.outro = outro;
	}
	public void setCdtarefarecursogeral(Integer cdtarefarecursogeral) {
		this.cdtarefarecursogeral = cdtarefarecursogeral;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setAcumulativo(Boolean acumulativo) {
		this.acumulativo = acumulativo;
	}
	
	@OneToMany(mappedBy="tarefarecursogeral")
	public List<Solicitacaocompraorigem> getListaSolicitacaocompraorigem() {
		return listaSolicitacaocompraorigem;
	}
	
	public void setListaSolicitacaocompraorigem(
			List<Solicitacaocompraorigem> listaSolicitacaocompraorigem) {
		this.listaSolicitacaocompraorigem = listaSolicitacaocompraorigem;
	}

	public String subQueryProjeto() {
		return "select tarefarecursogeralsubQueryProjeto.cdtarefarecursogeral " +
				"from Tarefarecursogeral tarefarecursogeralsubQueryProjeto " +
				"join tarefarecursogeralsubQueryProjeto.tarefa tarefasubQueryProjeto " +
				"join tarefasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public Integer getTiporecursogeralflex() {
		return tiporecursogeral != null ? tiporecursogeral.ordinal() : 0;
	}
	
	public void setTiporecursogeralflex(Integer tiporecursogeralflex) {
		if(tiporecursogeralflex == null) tiporecursogeralflex = 0;
		this.tiporecursogeral = Tiporecursogeral.values()[tiporecursogeralflex];
	}

	@Transient
	public Boolean getExisteRecursogeralComOrigem() {
		return existeRecursogeralComOrigem;
	}

	public void setExisteRecursogeralComOrigem(Boolean existeRecursogeralComOrigem) {
		this.existeRecursogeralComOrigem = existeRecursogeralComOrigem;
	}
}
