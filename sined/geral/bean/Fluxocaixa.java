package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_fluxocaixa", sequenceName="sq_fluxocaixa")
@DisplayName("Fluxo de caixa")
public class Fluxocaixa implements Log{

	protected Integer cdfluxocaixa;
	protected String descricao;
	protected Money valor;
	protected FluxocaixaTipo fluxocaixaTipo;
	protected Prazopagamento prazopagamento;
	protected Date dtinicio;
	protected Date dtfim;
	protected Rateio rateio;
	protected Tipooperacao tipooperacao;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Fluxocaixaorigem> listaFluxocaixaorigem = new ListSet<Fluxocaixaorigem>(Fluxocaixaorigem.class);
	
	//TRANSIENTS
	protected Ordemcompra ordemcompra;
	protected String projeto;
	
	{
		this.ativo = Boolean.TRUE;
	}
	
	public Fluxocaixa() {
	}
	public Fluxocaixa(Fluxocaixa fc) {
		this.cdfluxocaixa = fc.cdfluxocaixa;
		this.descricao = fc.descricao;
		this.valor = fc.valor;
		this.fluxocaixaTipo = fc.fluxocaixaTipo;
		this.prazopagamento = fc.prazopagamento;
		this.dtinicio = fc.dtinicio;
		this.dtfim = fc.dtfim;
		this.rateio = fc.rateio;
		this.tipooperacao = fc.tipooperacao;
		this.cdusuarioaltera = fc.cdusuarioaltera;
		this.dtaltera = fc.dtaltera;
		this.ordemcompra = fc.ordemcompra;
		this.projeto = fc.projeto;
	}
	
	public Fluxocaixa(FluxocaixaTipo fluxocaixaTipo, Prazopagamento prazopagamento, Rateio rateio, 
			          Tipooperacao tipooperacao, Date iciodtinicio, Date dtfim, String descricao, Money valor){
		this.fluxocaixaTipo = fluxocaixaTipo;
		this.prazopagamento = prazopagamento;
		this.rateio = rateio;
		this.tipooperacao = tipooperacao;
		this.dtinicio = iciodtinicio;
		this.descricao = descricao;
		this.valor = valor;
		this.dtfim = dtfim;
	}
	
	public Fluxocaixa(Integer cdfluxocaixa, String descricao, Long valor, Date dtinicio, Date dtfim, Integer cdtipooperacao, String nomeTipooperacao, 
			Integer cdprazopagamento, String nomePrazopagamento, Integer cdfluxocaixatipo, String nomeFluxocaixatipo, String sigla, Integer cdpessoa, 
			String nomeFornecedor, Integer cdordemcompra, String descricaoOrdemcompra, String projeto){
		this.cdfluxocaixa = cdfluxocaixa;
		this.descricao = descricao;
		this.valor = new Money(valor, true);
		this.dtinicio = dtinicio;
		this.dtfim = dtfim;
		this.tipooperacao = new Tipooperacao(cdtipooperacao, nomeTipooperacao);
		this.prazopagamento = new Prazopagamento(cdprazopagamento, nomePrazopagamento);
		this.fluxocaixaTipo = new FluxocaixaTipo(cdfluxocaixatipo, nomeFluxocaixatipo, sigla);
		if(this.fluxocaixaTipo != null && this.fluxocaixaTipo.getSigla().toUpperCase().equals("OC"))
			this.ordemcompra = new Ordemcompra(cdordemcompra, descricaoOrdemcompra, new Fornecedor(cdpessoa, nomeFornecedor));
		this.projeto = projeto;
	}
	
	@Id
	@GeneratedValue(generator="sq_fluxocaixa",strategy=GenerationType.AUTO)
	public Integer getCdfluxocaixa() {
		return cdfluxocaixa;
	}
	@DisplayName("Descri��o")
	@Required
	@MaxLength(150)
	public String getDescricao() {
		return descricao;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@DisplayName("Tipo de fluxo de caixa")
	@JoinColumn(name="cdfluxocaixatipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public FluxocaixaTipo getFluxocaixaTipo() {
		return fluxocaixaTipo;
	}
	@Required
	@DisplayName("Frequ�ncia")
	@JoinColumn(name="cdprazopagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@Required
	@DisplayName("Data inicial")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data final")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@JoinColumn(name="cdrateio")
	@ManyToOne(fetch=FetchType.LAZY)
	public Rateio getRateio() {
		return rateio;
	}
	@Required
	@DisplayName("Tipo de opera��o")
	@JoinColumn(name="cdtipooperacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@OneToMany(mappedBy="fluxocaixa")
	public List<Fluxocaixaorigem> getListaFluxocaixaorigem() {
		return listaFluxocaixaorigem;
	}
	
	public void setListaFluxocaixaorigem(
			List<Fluxocaixaorigem> listaFluxocaixaorigem) {
		this.listaFluxocaixaorigem = listaFluxocaixaorigem;
	}
	public void setCdfluxocaixa(Integer cdfluxocaixa) {
		this.cdfluxocaixa = cdfluxocaixa;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setFluxocaixaTipo(FluxocaixaTipo fluxocaixaTipo) {
		this.fluxocaixaTipo = fluxocaixaTipo;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Transient
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	@Transient
	public String getProjeto() {
		return projeto;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	
}
