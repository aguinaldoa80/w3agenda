package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_taxaparcela", sequenceName="sq_taxaparcela")
public class Taxaparcela implements Log {
	private Integer cdtaxaparcela;
	private Integer numeroparcelas;
	private Double taxa;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_taxaparcela")
	public Integer getCdtaxaparcela() {
		return cdtaxaparcela;
	}
	@DisplayName("N�mero de parcelas")
	public Integer getNumeroparcelas() {
		return numeroparcelas;
	}
	@DisplayName("Taxa")
	public Double getTaxa() {
		return taxa;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdtaxaparcela(Integer cdtaxaparcela) {
		this.cdtaxaparcela = cdtaxaparcela;
	}
	public void setNumeroparcelas(Integer numeroparcelas) {
		this.numeroparcelas = numeroparcelas;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
