package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_clientelicencasipeagro", sequenceName = "sq_clientelicencasipeagro")
@DisplayName("Licen�a")
public class ClienteLicencaSipeagro {

	protected Integer cdclientelicencasipeagro;
	protected String  numero;
	protected Date dtinicio;
	protected Date dtfim;
	protected Cliente cliente;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientelicencasipeagro")
	public Integer getCdclientelicencasipeagro() {
		return cdclientelicencasipeagro;
	}
	public void setCdclientelicencasipeagro(Integer cdclientelicencasipeagro) {
		this.cdclientelicencasipeagro = cdclientelicencasipeagro;
	}
	@Required
	@DisplayName("N�MERO")
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	@Required
	@DisplayName("DATA IN�CIO")
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	@Required
	@DisplayName("DATA FIM")
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
	
}
