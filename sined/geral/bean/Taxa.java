package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_taxa",sequenceName="sq_taxa")
@DisplayName("Taxas")
public class Taxa implements Log, Cloneable{
	
	protected Integer cdtaxa;
	protected Set<Taxaitem> listaTaxaitem;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Documento> listaDocumento;
	
	protected Integer cdconta;
	
	public Taxa() {}
	
	public Taxa(Taxa outraTaxa) {
		this.cdtaxa = outraTaxa.cdtaxa;
		this.listaTaxaitem = new ListSet<Taxaitem>(Taxaitem.class);
		for (Taxaitem taxaItemOutra : outraTaxa.listaTaxaitem) {
			this.listaTaxaitem.add(new Taxaitem(taxaItemOutra));
		}
	}
	
	@Override
	public Taxa clone() throws CloneNotSupportedException {
		Taxa taxa = (Taxa) super.clone();
		taxa.setCdtaxa(null);
		if(taxa.getListaTaxaitem() == null)
			return taxa;
		for(Taxaitem item: taxa.getListaTaxaitem())
			item.setCdtaxaitem(null);
		return taxa;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_taxa")
	public Integer getCdtaxa() {
		return cdtaxa;
	}
	@DisplayName("Taxas")
	@OneToMany(mappedBy="taxa")
	public Set<Taxaitem> getListaTaxaitem() {
		return listaTaxaitem;
	}
	
	@OneToMany(mappedBy="taxa")
	public Set<Documento> getListaDocumento() {
		return listaDocumento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdtaxa(Integer cdtaxa) {
		this.cdtaxa = cdtaxa;
	}
	public void setListaTaxaitem(Set<Taxaitem> listaTaxaitem) {
		this.listaTaxaitem = listaTaxaitem;
	}
	public void setListaDocumento(Set<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	public Integer getCdconta() {
		return cdconta;
	}

	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
}
