package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_vendafornecedorticketmedio", sequenceName="sq_vendafornecedorticketmedio")
public class VendaFornecedorTicketMedio {

	private Integer cdVendaFornecedorTicketMedio;
	private Venda venda;
	private Fornecedor fornecedor;
	private Money valorTicketEsperado;
	private Money valorTicketAtingido;
	
	public VendaFornecedorTicketMedio(){
		
	}
	
	public VendaFornecedorTicketMedio(Venda venda, Fornecedor fornecedor, Money valorTicketEsperado, Money valorTicketAtingido){
		this.venda = venda;
		this.fornecedor = fornecedor;
		this.valorTicketEsperado = valorTicketEsperado;
		this.valorTicketAtingido = valorTicketAtingido;
	}
	
	@Id
	@GeneratedValue(generator="sq_vendafornecedorticketmedio",strategy=GenerationType.AUTO)
	public Integer getCdVendaFornecedorTicketMedio() {
		return cdVendaFornecedorTicketMedio;
	}
	public void setCdVendaFornecedorTicketMedio(Integer cdVendaFornecedorTicketMedio) {
		this.cdVendaFornecedorTicketMedio = cdVendaFornecedorTicketMedio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public Money getValorTicketEsperado() {
		return valorTicketEsperado;
	}
	public void setValorTicketEsperado(Money valorTicketEsperado) {
		this.valorTicketEsperado = valorTicketEsperado;
	}
	public Money getValorTicketAtingido() {
		return valorTicketAtingido;
	}
	public void setValorTicketAtingido(Money valorTicketAtingido) {
		this.valorTicketAtingido = valorTicketAtingido;
	}
}
