package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("F�rmula de c�lculo da quantidade dos recursos da composi��o")
public class Formulacomposicao {

	public static final Formulacomposicao MEDIA = new Formulacomposicao(1, "M�dia");	
	public static final Formulacomposicao TOTAL = new Formulacomposicao(2, "Total");
	public static final Formulacomposicao VALOR_MAXIMO = new Formulacomposicao(3, "Valor M�ximo");
	
	protected Integer cdformulacomposicao;
	protected String nome;
	
	public Formulacomposicao() {		
	}
	
	public Formulacomposicao(Integer cdformulacomposicao, String nome) {
		this.cdformulacomposicao = cdformulacomposicao;
		this.nome = nome;
	}
	
	@Id	
	public Integer getCdformulacomposicao() {
		return cdformulacomposicao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdformulacomposicao(Integer cdformulacomposicao) {
		this.cdformulacomposicao = cdformulacomposicao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Formulacomposicao){
			Formulacomposicao that = (Formulacomposicao) obj;
			return this.getCdformulacomposicao().equals(that.getCdformulacomposicao());
		}
		return super.equals(obj);
	}
}
