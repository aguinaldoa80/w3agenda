package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_pedidovendaordemservicoveterinaria", sequenceName = "sq_pedidovendaordemservicoveterinaria")
public class Pedidovendaordemservicoveterinaria {

	protected Integer cdpedidovendaordemservicoveterinaria;
	protected Pedidovenda pedidovenda;
	protected Ordemservicoveterinaria ordemservicoveterinaria;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendaordemservicoveterinaria")
	public Integer getCdpedidovendaordemservicoveterinaria() {
		return cdpedidovendaordemservicoveterinaria;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	
	public void setCdpedidovendaordemservicoveterinaria(Integer cdpedidovendaordemservicoveterinaria) {
		this.cdpedidovendaordemservicoveterinaria = cdpedidovendaordemservicoveterinaria;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	
}
