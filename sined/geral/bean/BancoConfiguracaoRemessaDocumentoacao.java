package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessadocumentoacao",sequenceName="sq_bancoconfiguracaoremessadocumentoacao")
public class BancoConfiguracaoRemessaDocumentoacao {

	protected Integer cdBancoConfiguracaoRemessaDocumentoacao;
	protected BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	protected Documentoacao documentoacao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessadocumentoacao")
	public Integer getCdBancoConfiguracaoRemessaDocumentoacao() {
		return cdBancoConfiguracaoRemessaDocumentoacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	
	public void setCdBancoConfiguracaoRemessaDocumentoacao(Integer cdBancoConfiguracaoRemessaDocumentoacao) {
		this.cdBancoConfiguracaoRemessaDocumentoacao = cdBancoConfiguracaoRemessaDocumentoacao;
	}
	public void setBancoConfiguracaoRemessa(BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
}
