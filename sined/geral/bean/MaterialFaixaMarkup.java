package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_materialfaixamarkup", sequenceName="sq_materialfaixamarkup")
public class MaterialFaixaMarkup {

	private Integer cdMaterialFaixaMarkup;
	private Material material;
	private FaixaMarkupNome faixaMarkupNome;
	private Empresa empresa;
	private Double markup;
	private Double valorCusto;
	private Double valorVenda;
	private Boolean isValorMinimo;
	private Boolean isValorIdeal;
    private Unidademedida unidadeMedida;
	
	//TRANSIENTS
	private String descricaoMaterialFaixa;
	
	
	@Id
	@GeneratedValue(generator="sq_materialfaixamarkup", strategy=GenerationType.AUTO)
	public Integer getCdMaterialFaixaMarkup() {
		return cdMaterialFaixaMarkup;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Faixa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("% Markup")
	public Double getMarkup() {
		return markup;
	}
	
	@Transient
	@DisplayName("Valor de custo")
	public Double getValorCusto() {
		return valorCusto;
	}
	
	@DisplayName("Valor de venda")
	public Double getValorVenda() {
		return valorVenda;
	}
	
	@DisplayName("Valor m�nimo")
	public Boolean getIsValorMinimo() {
		return isValorMinimo;
	}
	
	@DisplayName("Valor ideal")
	public Boolean getIsValorIdeal() {
		return isValorIdeal;
	}
	
	@Required
    @DisplayName("Unidade de medida")
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdunidademedida")
    public Unidademedida getUnidadeMedida() {
        return unidadeMedida;
    }
	
	@Transient
	public String getDescricaoMaterialFaixa() {
		if(descricaoMaterialFaixa == null){
			descricaoMaterialFaixa = cdMaterialFaixaMarkup.toString();
			if(faixaMarkupNome.getNome() != null){
				descricaoMaterialFaixa += (" - "+faixaMarkupNome.getNome());
			}
		}
		return descricaoMaterialFaixa;
	}
	
	
	public void setCdMaterialFaixaMarkup(Integer cdMaterialFaixaMarkup) {
		this.cdMaterialFaixaMarkup = cdMaterialFaixaMarkup;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	public void setValorCusto(Double valorCusto) {
		this.valorCusto = valorCusto;
	}
	public void setValorVenda(Double valorVenda) {
		this.valorVenda = valorVenda;
	}
	public void setIsValorMinimo(Boolean isValorMinimo) {
		this.isValorMinimo = isValorMinimo;
	}
	public void setIsValorIdeal(Boolean isValorIdeal) {
		this.isValorIdeal = isValorIdeal;
	}
    public void setUnidadeMedida(Unidademedida unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }
	public void setDescricaoMaterialFaixa(String descricaoMaterialFaixa) {
		this.descricaoMaterialFaixa = descricaoMaterialFaixa;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdMaterialFaixaMarkup == null) ? 0 : cdMaterialFaixaMarkup
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MaterialFaixaMarkup other = (MaterialFaixaMarkup) obj;
		if (cdMaterialFaixaMarkup == null) {
			if (other.cdMaterialFaixaMarkup != null)
				return false;
		} else if (!cdMaterialFaixaMarkup.equals(other.cdMaterialFaixaMarkup))
			return false;
		return true;
	}
	
}