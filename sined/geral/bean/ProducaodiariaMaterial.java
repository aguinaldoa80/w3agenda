package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_producaodiariamaterial",sequenceName="sq_producaodiariamaterial")
public class ProducaodiariaMaterial implements Log{

	protected Integer cdproducaodiariamaterial;
	protected Material material;
	protected Producaodiaria producaodiaria;
	protected Double qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_producaodiariamaterial", strategy=GenerationType.AUTO)
	public Integer getCdproducaodiariamaterial() {
		return cdproducaodiariamaterial;
	}
	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Produ��o di�ria")
	@JoinColumn(name="cdproducaodiaria")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaodiaria getProducaodiaria() {
		return producaodiaria;
	}
	@Required
	@DisplayName("QTDE.")
	public Double getQtde() {
		return qtde;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdproducaodiariamaterial(Integer cdapontamentomaterial) {
		this.cdproducaodiariamaterial = cdapontamentomaterial;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setProducaodiaria(Producaodiaria producaodiaria) {
		this.producaodiaria = producaodiaria;
	}
	public void setQtde(Double quantidade) {
		this.qtde = quantidade;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
