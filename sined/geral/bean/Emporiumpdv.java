package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_emporiumpdv", sequenceName = "sq_emporiumpdv")
public class Emporiumpdv implements Log{

	protected Integer cdemporiumpdv;
	protected Empresa empresa;
	protected Conta conta;
	protected String codigoemporium;
	protected String descricao;
	protected String modeloequipamento;
	protected String numeroserie;
	protected String modelodocfiscal;
	protected Localarmazenagem localarmazenagem;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean equipamentoNaoFiscal;
	protected Boolean gerarNfce;
	protected Timestamp dataInicioGerarNfce;
	
	public Emporiumpdv() {
	}
	
	public Emporiumpdv(Integer cdemporiumpdv) {
		this.cdemporiumpdv = cdemporiumpdv;
	}
	
	public Emporiumpdv(Emporiumpdv emporiumpdv){
		this.cdemporiumpdv = emporiumpdv.cdemporiumpdv;
		this.empresa = emporiumpdv.empresa;
		this.conta = emporiumpdv.conta;
		this.codigoemporium = emporiumpdv.codigoemporium;
		this.descricao = emporiumpdv.descricao;
		this.modeloequipamento = emporiumpdv.modeloequipamento;
		this.numeroserie = emporiumpdv.numeroserie;
		this.modelodocfiscal = emporiumpdv.modelodocfiscal;
		this.localarmazenagem = emporiumpdv.localarmazenagem;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumpdv")
	public Integer getCdemporiumpdv() {
		return cdemporiumpdv;
	}
	
	@Required
	@DisplayName("C�digo no ECF")
	@MaxLength(10)
	public String getCodigoemporium() {
		return codigoemporium;
	}

	@Required
	@DescriptionProperty
	@DisplayName("Descri��o")
	@MaxLength(200)
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Conta para baixa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}
	
	@DisplayName("Local para estoque")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@MaxLength(20)
	@DisplayName("Modelo do equipamento")
	public String getModeloequipamento() {
		return modeloequipamento;
	}
	
	@MaxLength(21)
	@DisplayName("N�mero de s�rie")
	public String getNumeroserie() {
		return numeroserie;
	}

	@MaxLength(2)
	@DisplayName("Modelo do documento fiscal")
	public String getModelodocfiscal() {
		return modelodocfiscal;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setModeloequipamento(String modeloequipamento) {
		this.modeloequipamento = modeloequipamento;
	}

	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}

	public void setModelodocfiscal(String modelodocfiscal) {
		this.modelodocfiscal = modelodocfiscal;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCdemporiumpdv(Integer cdemporiumpdv) {
		this.cdemporiumpdv = cdemporiumpdv;
	}

	public void setCodigoemporium(String codigoemporium) {
		this.codigoemporium = codigoemporium;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
//	LOG

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdemporiumpdv == null) ? 0 : cdemporiumpdv.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Emporiumpdv other = (Emporiumpdv) obj;
		if (cdemporiumpdv == null) {
			if (other.cdemporiumpdv != null)
				return false;
		} else if (!cdemporiumpdv.equals(other.cdemporiumpdv))
			return false;
		return true;
	}
	
	@Transient
	public String getCodigoemporiumSemZeroEsquerda(){
		if(this.codigoemporium == null) return null;
		return SinedUtil.retiraZeroEsquerda(this.codigoemporium);
	}
	
	@DisplayName("Equipamento n�o fiscal")
	public Boolean getEquipamentoNaoFiscal() {
		return equipamentoNaoFiscal;
	}
	public void setEquipamentoNaoFiscal(Boolean equipamentoNaoFiscal) {
		this.equipamentoNaoFiscal = equipamentoNaoFiscal;
	}
	@DisplayName("Considerar NFC-e geradas a partir de")
	public Timestamp getDataInicioGerarNfce() {
		return dataInicioGerarNfce;
	}
	public void setDataInicioGerarNfce(Timestamp dataInicioGerarNfce) {
		this.dataInicioGerarNfce = dataInicioGerarNfce;
	}
	@DisplayName("Emite NFC-e")
	public Boolean getGerarNfce() {
		return gerarNfce;
	}
	public void setGerarNfce(Boolean gerarNfce) {
		this.gerarNfce = gerarNfce;
	}
}
