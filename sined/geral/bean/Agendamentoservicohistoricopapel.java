package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_agendamentoservicohistoricopapel", sequenceName = "sq_agendamentoservicohistoricopapel")
@DisplayName("Agendamento Servi�o Hist�rico Papel")
public class Agendamentoservicohistoricopapel implements Log {

	protected Integer cdagendamentoservicohistoricopapel;
	protected Agendamentoservicohistorico agendamentoservicohistorico;
	protected Papel papel;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	public Agendamentoservicohistoricopapel(){
	}

	public Agendamentoservicohistoricopapel(Papel papel){
		this.papel = papel;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamentoservicohistoricopapel")
	public Integer getCdagendamentoservicohistoricopapel() {
		return cdagendamentoservicohistoricopapel;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservicohistorico")
	public Agendamentoservicohistorico getAgendamentoservicohistorico() {
		return agendamentoservicohistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	public Papel getPapel() {
		return papel;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setCdagendamentoservicohistoricopapel(
			Integer cdagendamentoservicohistoricopapel) {
		this.cdagendamentoservicohistoricopapel = cdagendamentoservicohistoricopapel;
	}
	public void setAgendamentoservicohistorico(
			Agendamentoservicohistorico agendamentoservicohistorico) {
		this.agendamentoservicohistorico = agendamentoservicohistorico;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
}
