package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_despesaavulsarhmotivo", sequenceName = "sq_despesaavulsarhmotivo")
public class Despesaavulsarhmotivo implements Log {

	private Integer cddespesaavulsarhmotivo;
	private String descricao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	@Id
	@GeneratedValue(generator = "sq_despesaavulsarhmotivo", strategy = GenerationType.AUTO)
	public Integer getCddespesaavulsarhmotivo() {
		return cddespesaavulsarhmotivo;
	}

	public void setCddespesaavulsarhmotivo(Integer cddespesaavulsarhmotivo) {
		this.cddespesaavulsarhmotivo = cddespesaavulsarhmotivo;
	}

	@Required
	@MaxLength(200)
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("�ltima altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
