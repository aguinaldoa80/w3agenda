package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("C�digo de Tributa��o")
@SequenceGenerator(name = "sq_codigotributacao", sequenceName = "sq_codigotributacao")
public class Codigotributacao implements Log {

	protected Integer cdcodigotributacao;
	protected String codigo;
	protected String descricao;
	protected Municipio municipio;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Codigotributacaoitemlistaservico> listaCodigotributacaoitemlistaservico = new ListSet<Codigotributacaoitemlistaservico>(Codigotributacaoitemlistaservico.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_codigotributacao")
	public Integer getCdcodigotributacao() {
		return cdcodigotributacao;
	}
	
	@MaxLength(20)
	@Required
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@MaxLength(1000)
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Munic�pio")
	@JoinColumn(name="cdmunicipio")
	@ManyToOne(fetch=FetchType.LAZY)
	public Municipio getMunicipio() {
		return municipio;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setCdcodigotributacao(Integer cdcodigotributacao) {
		this.cdcodigotributacao = cdcodigotributacao;
	}
	
	// LISTAS
	
	@DisplayName("Itens da lista de servi�o")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="codigotributacao")
	public List<Codigotributacaoitemlistaservico> getListaCodigotributacaoitemlistaservico() {
		return listaCodigotributacaoitemlistaservico;
	}
	
	public void setListaCodigotributacaoitemlistaservico(
			List<Codigotributacaoitemlistaservico> listaCodigotributacaoitemlistaservico) {
		this.listaCodigotributacaoitemlistaservico = listaCodigotributacaoitemlistaservico;
	}
	
	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTES
	
	@Transient
	public String getMunicipioUf(){
		StringBuilder sb = new StringBuilder();
		if(municipio != null){
			sb.append(municipio.getNome());
			if(municipio.getUf() != null){
				sb.append("/").append(municipio.getUf().getSigla());
			}
		}
		return sb.toString();
	}
	
	@Transient
	@DescriptionProperty(usingFields={"codigo","descricao"})
	public String getDescricaoCombo(){
		StringBuilder sb = new StringBuilder();
		if(this.codigo != null && this.descricao != null){
			sb.append(this.codigo).append(" - ").append(this.descricao);
		}
		return sb.toString();
	}
	
	@Transient
	public String getDescriptionAutocompleteWithMunicipio(){
		StringBuilder sb = new StringBuilder();
		if(this.codigo != null && this.descricao != null){
			sb.append(this.codigo).append(" - ").append(this.descricao);
		}
		if(this.municipio != null && this.municipio.getNome() != null){
			sb.append(" (").append(this.municipio.getNomecompleto()).append(")");
		}
		return sb.toString();
	}
}
