package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.BounceTypeEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SendgridEvent;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.EmailCobranca;

@Entity
@SequenceGenerator(name="sq_historicoemailcobranca", sequenceName="sq_historicoemailcobranca")
public class HistoricoEmailCobranca {
	private Integer cdhistorico;
	private SendgridEvent event;
	private String response;
	private Integer attempt;
	private String urlclicked;
	private String status;
	private String reason;
	private BounceTypeEnum type;
	private String ip;
	private String useragent;
	private EmailCobranca emailcobranca;
	private Timestamp ultimaverificacao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_historicoemailcobranca")
	@DisplayName("ID")
	public Integer getCdhistorico() {
		return cdhistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemailcobranca")
	public EmailCobranca getEmailcobranca() {
		return emailcobranca;
	}
	@DisplayName("Resposta")
	public String getResponse() {
		return response;
	}
	@DisplayName("URL Acessada")
	public String getUrlclicked() {
		return urlclicked;
	}	
	public String getStatus() {
		return status;
	}
	public String getIp() {
		return ip;
	}
	@DisplayName("Dispositivo")
	public String getUseragent() {
		return useragent;
	}
	@DisplayName("Evento SendGrid")
	public SendgridEvent getEvent() {
		return event;
	}
	@DisplayName("Tentativas")
	public Integer getAttempt() {
		return attempt;
	}
	@DisplayName("Motivo")
	public String getReason() {
		return reason;
	}
	@DisplayName("Tipo")
	public BounceTypeEnum getType() {
		return type;
	}

	
	public void setResponse(String response) {
		this.response = response;
	}
	public void setCdhistorico(Integer cdhistorico) {
		this.cdhistorico = cdhistorico;
	}

	public void setUrlclicked(String urlclicked) {
		this.urlclicked = urlclicked;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}
	public void setEvent(SendgridEvent event) {
		this.event = event;
	}
	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public void setType(BounceTypeEnum type) {
		this.type = type;
	}
	public void setEmailcobranca(EmailCobranca emailcobranca) {
		this.emailcobranca = emailcobranca;
	}
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}	
}
