package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_oportunidadeestagio", sequenceName = "sq_oportunidadeestagio")
public class Oportunidadeestagio implements Log{
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdoportunidadeestagio;
	protected String nome;
	

	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidadeestagio")
	public Integer getCdoportunidadeestagio() {
		return cdoportunidadeestagio;
	}
	
	@Required	
	@MaxLength(20)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	
	
	public void setCdoportunidadeestagio(Integer cdoportunidadeestagio) {
		this.cdoportunidadeestagio = cdoportunidadeestagio;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
