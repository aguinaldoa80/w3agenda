package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Horimetrotipoajuste;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculohorimetro", sequenceName = "sq_veiculohorimetro")
public class Veiculohorimetro implements Log{

	protected Integer cdveiculohorimetro;
	protected Veiculo veiculo;
	protected Horimetrotipoajuste horimetrotipoajuste;
	protected Double horimetronovo;
	protected Date dtentrada;
	protected String motivo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Veiculohorimetro veiculohorimetroatual;
	protected Date dtentradatrans;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculohorimetro")
	public Integer getCdveiculohorimetro() {
		return cdveiculohorimetro;
	}
	@Required
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	@DisplayName("Tipo de Ajuste")
	public Horimetrotipoajuste getHorimetrotipoajuste() {
		return horimetrotipoajuste;
	}
	@Required
	@DisplayName("Hor�metro novo")
	public Double getHorimetronovo() {
		return horimetronovo;
	}
	@DisplayName("Data de Entrada")
	public Date getDtentrada() {
		return dtentrada;
	}
	@Required
	public String getMotivo() {
		return motivo;
	}
	public void setCdveiculohorimetro(Integer cdveiculohorimetro) {
		this.cdveiculohorimetro = cdveiculohorimetro;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setHorimetrotipoajuste(Horimetrotipoajuste horimetrotipoajuste) {
		this.horimetrotipoajuste = horimetrotipoajuste;
	}
	public void setHorimetronovo(Double horimetronovo) {
		this.horimetronovo = horimetronovo;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	=================== Transients =============
	
	@DisplayName("Hor�metro atual")
	@DescriptionProperty
	@Transient
	public String getDescriptionProperty(){
		return ""+ (this.veiculohorimetroatual.getHorimetronovo() != null ? this.veiculohorimetroatual.getHorimetronovo() : "")+" - " 
				 +(this.veiculohorimetroatual.getDtentrada() != null ? DateFormat.getDateInstance(DateFormat.SHORT, new Locale("PT", "BR")).format(this.veiculohorimetroatual.getDtentrada()) : "");
	}
	
	@Transient
	public Veiculohorimetro getVeiculohorimetroatual() {
		return veiculohorimetroatual;
	}
	
	public void setVeiculohorimetroatual(Veiculohorimetro veiculohorimetroatual) {
		this.veiculohorimetroatual = veiculohorimetroatual;
	}

	@Transient
	public Date getDtentradatrans() {
		return getDtentrada();
	}

	public void setDtentradatrans(Date dtentradatrans) {
		this.dtentradatrans = dtentradatrans;
	}
}
