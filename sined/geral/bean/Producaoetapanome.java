package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_producaoetapanome", sequenceName = "sq_producaoetapanome")
@DisplayName("Etapa de Produ��o")
public class Producaoetapanome implements Log{

	protected Integer cdproducaoetapanome;
	protected String nome;
	protected Boolean permitirtrocarprodutofinal;
	protected Boolean permitirgerargarantia;
	protected Boolean enviaritemadicionalaposconclusao;
	protected Boolean enviarservicoaposconclusao;
	protected Boolean enviarbandaaposconclusao;
	protected Set<Producaoetapanomecampo> listaProducaoetapanomecampo;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoetapanome")
	public Integer getCdproducaoetapanome() {
		return cdproducaoetapanome;
	}
	@DescriptionProperty
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Permitir trocar produto final")
	public Boolean getPermitirtrocarprodutofinal() {
		return permitirtrocarprodutofinal;
	}
	@DisplayName("Permitir gerar garantia")
	public Boolean getPermitirgerargarantia() {
		return permitirgerargarantia;
	}
	@OneToMany(mappedBy="producaoetapanome")
	public Set<Producaoetapanomecampo> getListaProducaoetapanomecampo() {
		return listaProducaoetapanomecampo;
	}
	
	public void setCdproducaoetapanome(Integer cdproducaoetapanome) {
		this.cdproducaoetapanome = cdproducaoetapanome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setPermitirtrocarprodutofinal(Boolean permitirtrocarprodutofinal) {
		this.permitirtrocarprodutofinal = permitirtrocarprodutofinal;
	}
	public void setPermitirgerargarantia(Boolean permitirgerargarantia) {
		this.permitirgerargarantia = permitirgerargarantia;
	}
	public void setListaProducaoetapanomecampo(Set<Producaoetapanomecampo> listaProducaoetapanomecampo) {
		this.listaProducaoetapanomecampo = listaProducaoetapanomecampo;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdproducaoetapanome == null) ? 0 : cdproducaoetapanome
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producaoetapanome other = (Producaoetapanome) obj;
		if (cdproducaoetapanome == null) {
			if (other.cdproducaoetapanome != null)
				return false;
		} else if (!cdproducaoetapanome.equals(other.cdproducaoetapanome))
			return false;
		return true;
	}
	
	@DisplayName("Enviar itens adicionais ap�s conclus�o desta etapa")
	public Boolean getEnviaritemadicionalaposconclusao() {
		return enviaritemadicionalaposconclusao;
	}

	public void setEnviaritemadicionalaposconclusao(Boolean enviaritemadicionalaposconclusao) {
		this.enviaritemadicionalaposconclusao = enviaritemadicionalaposconclusao;
	}
	@DisplayName("Enviar servi�o ap�s conclus�o desta etapa")
	public Boolean getEnviarservicoaposconclusao() {
		return enviarservicoaposconclusao;
	}
	@DisplayName("Enviar banda ap�s conclus�o desta etapa.")
	public Boolean getEnviarbandaaposconclusao() {
		return enviarbandaaposconclusao;
	}
	public void setEnviarservicoaposconclusao(Boolean enviarservicoaposconclusao) {
		this.enviarservicoaposconclusao = enviarservicoaposconclusao;
	}
	public void setEnviarbandaaposconclusao(Boolean enviarbandaaposconclusao) {
		this.enviarbandaaposconclusao = enviarbandaaposconclusao;
	}	
}