package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Tipoinscricao {
	
	public static final Integer PIS=1;
	public static final Integer PASEP=2;
	public static final Integer CI=3;
	public static final Integer SUS=4;
	
	protected Integer cdtipoinscricao;
	protected String nome;
	
	public Tipoinscricao(){}
	
	public Tipoinscricao(Integer cdtipoinscricao){
		this.cdtipoinscricao = cdtipoinscricao;
	}
	
	@Id
	public Integer getCdtipoinscricao() {
		return cdtipoinscricao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdtipoinscricao(Integer cdtipoinscricao) {
		this.cdtipoinscricao = cdtipoinscricao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
