package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Materialacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_materialhistorico",sequenceName="sq_materialhistorico")
public class Materialhistorico implements Log{

	protected Integer cdmaterialhistorico;
	protected Material material;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Materialacao acao;
	protected String observacao;
	protected String observacaoFaixaMarkup;
	
	// CAMPOS DO MATERIAL
	protected String nome;
	protected String ncmcompleto;
	protected Boolean vendapromocional;
	protected Double valorvenda;
	protected Double valorVendaMinimo;
	protected Unidademedida unidademedida;
	protected Materialgrupo materialgrupo;
	protected String codigobarras;
	protected Boolean ativo;
	protected Boolean vendaecf;
	
	
	@Id
	@GeneratedValue(generator="sq_materialhistorico",strategy=GenerationType.AUTO)
	public Integer getCdmaterialhistorico() {
		return cdmaterialhistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("A��o")
	public Materialacao getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public String getObservacaoFaixaMarkup() {
		return observacaoFaixaMarkup;
	}

	public String getNome() {
		return nome;
	}

	public String getNcmcompleto() {
		return ncmcompleto;
	}

	public Boolean getVendapromocional() {
		return vendapromocional;
	}

	public Double getValorvenda() {
		return valorvenda;
	}

	@DisplayName("Valor M�nimo")
	public Double getValorVendaMinimo() {
		return valorVendaMinimo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	public String getCodigobarras() {
		return codigobarras;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public Boolean getVendaecf() {
		return vendaecf;
	}

	public void setCdmaterialhistorico(Integer cdmaterialhistorico) {
		this.cdmaterialhistorico = cdmaterialhistorico;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setAcao(Materialacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setObservacaoFaixaMarkup(String observacaoFaixaMarkup) {
		this.observacaoFaixaMarkup = observacaoFaixaMarkup;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}

	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}

	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	public void setValorVendaMinimo(Double valorVendaMinimo) {
		this.valorVendaMinimo = valorVendaMinimo;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setVendaecf(Boolean vendaecf) {
		this.vendaecf = vendaecf;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
