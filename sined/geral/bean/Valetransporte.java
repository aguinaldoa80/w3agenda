package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_valetransporte", sequenceName = "sq_valetransporte")
public class Valetransporte implements Log {

	protected Integer cdvaletransporte;
	protected String descricao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	public Valetransporte(){}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_valetransporte")
	public Integer getCdvaletransporte() {
		return cdvaletransporte;
	}
		
	@DisplayName("Descri��o")
	@Required
	@MaxLength(50)
	@DescriptionProperty	
	public String getDescricao() {
		return descricao;
	}

	public void setCdvaletransporte(Integer cdvaletransporte) {
		this.cdvaletransporte = cdvaletransporte;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	
}
