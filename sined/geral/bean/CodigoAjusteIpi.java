package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;

@Entity
@SequenceGenerator(name = "sq_codigoajusteipi", sequenceName = "sq_codigoajusteipi")
@DisplayName("C�digo de Ajuste de IPI")
public class CodigoAjusteIpi {

	private Integer cdCodigoAjusteIpi;
	private String codigo;
	private String descricao;
	private CreditoDebitoEnum natureza;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_codigoajusteipi")
	public Integer getCdCodigoAjusteIpi() {
		return cdCodigoAjusteIpi;
	}
	
	public void setCdCodigoAjusteIpi(Integer cdCodigoAjusteIpi) {
		this.cdCodigoAjusteIpi = cdCodigoAjusteIpi;
	}
	
	@Required
	@MaxLength(3)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@Required
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Required
	@DisplayName("Natureza")
	public CreditoDebitoEnum getNatureza() {
		return natureza;
	}
	
	public void setNatureza(CreditoDebitoEnum natureza) {
		this.natureza = natureza;
	}

	@Transient
	@DescriptionProperty(usingFields={"codigo", "descricao"})
	public String getDescricaoCompleta() {
		return codigo + " - " + descricao;
	}
}
