package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_leadtelefone", sequenceName = "sq_leadtelefone")
public class Leadtelefone {
	
	protected Integer cdleadtelefone;
	protected Telefonetipo telefonetipo;
	protected Lead lead;
	protected String telefone;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_leadtelefone")
	public Integer getCdleadtelefone() {
		return cdleadtelefone;
	}
	
	@DisplayName("Tipo de telefone")
	@JoinColumn(name="cdtelefonetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Telefonetipo getTelefonetipo() {
		return telefonetipo;
	}
	
	@DisplayName("Lead")
	@JoinColumn(name="cdlead")
	@ManyToOne(fetch=FetchType.LAZY)
	public Lead getLead() {
		return lead;
	}
	
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Telefone")
	public String getTelefone() {
		return telefone;
	}
	
	
	
	
	public void setCdleadtelefone(Integer cdleadtelefone) {
		this.cdleadtelefone = cdleadtelefone;
	}

	public void setTelefonetipo(Telefonetipo telefonetipo) {
		this.telefonetipo = telefonetipo;
	}
	
	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	
	

}
