package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsOperacao;


@Entity
@SequenceGenerator(name = "sq_ajusteicmsdocumento", sequenceName = "sq_ajusteicmsdocumento")
public class Ajusteicmsdocumento {

	protected Integer cdajusteicmsdocumento;
	protected Ajusteicms ajusteicms;
	protected AjusteIcmsOperacao tipo;
	protected Pessoa pessoa;
	protected Modelodocumentofiscal modelonf;
	protected String serie;
	protected String subserie;
	protected String numero;
	protected String tipoUtilizacaoCredito;
	protected Date dtemissao;
	protected Material material;
	protected Double valorajuste;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteicmsdocumento")
	public Integer getCdajusteicmsdocumento() {
		return cdajusteicmsdocumento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdajusteicms")
	public Ajusteicms getAjusteicms() {
		return ajusteicms;
	}

	@Required
	public AjusteIcmsOperacao getTipo() {
		return tipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}

	@Required
	@DisplayName("Modelo NF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmodelodocumentofiscal")
	public Modelodocumentofiscal getModelonf() {
		return modelonf;
	}

	@MaxLength(4)
	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}
	
	@MaxLength(3)
	@DisplayName("Subs�rie")
	public String getSubserie() {
		return subserie;
	}

	@Required
	@MaxLength(8)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}

	@Required
	@DisplayName("Data de emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Valor do ajuste")
	public Double getValorajuste() {
		return valorajuste;
	}
	
	@Required
	@MaxLength(4)
	@DisplayName("Tipo de utiliza��o do cr�dito")
	public String getTipoUtilizacaoCredito() {
		return tipoUtilizacaoCredito;
	}
	
	public void setTipoUtilizacaoCredito(String tipoUtilizacaoCredito) {
		this.tipoUtilizacaoCredito = tipoUtilizacaoCredito;
	}

	public void setCdajusteicmsdocumento(Integer cdajusteicmsdocumento) {
		this.cdajusteicmsdocumento = cdajusteicmsdocumento;
	}

	public void setAjusteicms(Ajusteicms ajusteicms) {
		this.ajusteicms = ajusteicms;
	}

	public void setTipo(AjusteIcmsOperacao tipo) {
		this.tipo = tipo;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setModelonf(Modelodocumentofiscal modelonf) {
		this.modelonf = modelonf;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setSubserie(String subserie) {
		this.subserie = subserie;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setValorajuste(Double valorajuste) {
		this.valorajuste = valorajuste;
	}
	
	@Transient
	public Fornecedor getFornecedor(){
		if(this.pessoa != null && this.tipo != null && this.tipo.equals(AjusteIcmsOperacao.ENTRADA)){
			if(this.pessoa instanceof Fornecedor){
				return (Fornecedor)this.pessoa;
			}
			Fornecedor fornecedor = new Fornecedor(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			return fornecedor;
		}
		return null;
	}
	
	public void setFornecedor(Fornecedor fornecedor){
		if(fornecedor != null && this.tipo != null && this.tipo.equals(AjusteIcmsOperacao.ENTRADA)){
			this.pessoa = fornecedor;
		}
	}
	
	@Transient
	public Cliente getCliente(){
		if(this.pessoa != null && this.tipo != null && this.tipo.equals(AjusteIcmsOperacao.SAIDA)){
			if(this.pessoa instanceof Cliente){
				return (Cliente)this.pessoa;
			}
			Cliente cliente = new Cliente(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			return cliente;
		}
		return null;
	}
	
	public void setCliente(Cliente cliente){
		if(cliente != null && this.tipo != null && this.tipo.equals(AjusteIcmsOperacao.SAIDA)){
			this.pessoa = cliente;
		}
	}

}
