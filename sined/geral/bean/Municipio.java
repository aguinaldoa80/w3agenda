package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_municipio", sequenceName = "sq_municipio")
public class Municipio implements Log{

	protected Integer cdmunicipio;
	protected String nome;
	protected Uf uf;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String cdibge;
	protected String cdsiafi;
	protected String codigotom;
	protected String codigogoiania;
	
	public static Municipio BELO_HORIZONTE = new Municipio(472,Uf.MINAS_GERAIS);
	public static Municipio EXTERIOR_MDFE = new Municipio("9999999", "EXTERIOR", Uf.EXTERIOR_MDFE);
	
	public Municipio(){}
	
	public Municipio(Integer cdmunicipio){
		this.cdmunicipio = cdmunicipio;
	}
	
	public Municipio(Integer cdmunicipio, Uf uf){
		this.cdmunicipio = cdmunicipio;
		this.uf = uf;
	}
	
	public Municipio(String cdibge, String nome, Uf uf){
		this.nome = nome;
		this.cdibge = cdibge;
		this.uf = uf;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_municipio")
	public Integer getCdmunicipio() {
		return cdmunicipio;
	}
	public void setCdmunicipio(Integer id) {
		this.cdmunicipio = id;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("C�digo IBGE")
	public String getCdibge() {
		return cdibge;
	}
	@DisplayName("C�digo TOM")
	public String getCodigotom() {
		return codigotom;
	}
	@DisplayName("C�digo SIAFI")
	public String getCdsiafi() {
		return cdsiafi;
	}
	@DisplayName("C�digo Pref. Goi�nia/GO")
	public String getCodigogoiania() {
		return codigogoiania;
	}
	
	public void setCodigogoiania(String codigogoiania) {
		this.codigogoiania = codigogoiania;
	}
	public void setCdsiafi(String cdsiafi) {
		this.cdsiafi = cdsiafi;
	}
	
	public void setCodigotom(String codigotom) {
		this.codigotom = codigotom;
	}
	
	public void setCdibge(String cdibge) {
		this.cdibge = cdibge;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getNomecompleto(){
		return this.nome + (this.uf != null && this.uf.getSigla() != null ? " - " + this.uf.getSigla() : "");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmunicipio == null) ? 0 : cdmunicipio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (cdmunicipio == null) {
			if (other.cdmunicipio != null)
				return false;
		} else if (!cdmunicipio.equals(other.cdmunicipio))
			return false;
		return true;
	}
	
}
