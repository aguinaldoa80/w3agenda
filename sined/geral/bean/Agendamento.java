package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_agendamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoAgendamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_agendamento", sequenceName = "sq_agendamento")
@JoinEmpresa("agendamento.empresa")
public class Agendamento implements Log, PermissaoProjeto, PermissaoClienteEmpresa, PermissaoFornecedorEmpresa{

	protected Integer cdagendamento;
	protected String descricao;
	protected Tipooperacao tipooperacao = Tipooperacao.TIPO_DEBITO;
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected Cliente cliente;
	protected Pessoa pessoa;
	protected Money valor;
	protected Date dtproximo;
	protected Documentotipo documentotipo;
	protected Frequencia frequencia = new Frequencia(Frequencia.MENSAL);
	protected Date dtinicio;
	protected Date dtfim;
	protected Integer iteracoes;
	protected String documento;
	protected String observacao;
	protected Tipovinculo tipovinculo;
	protected Conta conta;
	protected Rateio rateio;
	protected Taxa taxa;
	protected Boolean ativo = Boolean.TRUE;
	protected Set<AgendamentoHistorico> listaAgendamentoHistorico = new ListSet<AgendamentoHistorico>(AgendamentoHistorico.class);
	protected Aux_agendamento aux_agendamento;
	protected Empresa empresa;
	protected Tipopagamento tipopagamento;
	protected String outrospagamento;
	protected Documentoacao documentoacao = Documentoacao.PREVISTA;
	protected Operacaocontabil operacaocontabil;
	protected Formapagamento formapagamento;
	protected Conta vinculoProvisionado;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	//Trasient
	protected Boolean agendamentoTemPrazoParaTerminio;
	protected SituacaoAgendamento situacao;
	protected Conta contaBancaria;
	protected Conta caixa;
	protected Conta cartaoCredito;
	protected String whereIn;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected Date dtatual = SinedDateUtils.currentDateToBeginOfDay();
	protected Rateiomodelo rateiomodelo;
	
	public Agendamento() {}

	public Agendamento (Integer cdagendamento) {
		this.cdagendamento = cdagendamento;
	}
	
	public Agendamento(Agendamento ag){
		this.cdagendamento = ag.cdagendamento;
		this.ativo = ag.ativo;
		this.dtproximo = ag.dtproximo;
		this.dtfim = ag.dtfim;
		this.valor = ag.valor;
		this.tipooperacao = ag.tipooperacao;
		this.fornecedor = ag.fornecedor;
		this.cliente = ag.cliente;
		this.pessoa = ag.pessoa;
		this.tipopagamento = ag.tipopagamento;
		this.outrospagamento = ag.outrospagamento;
		this.descricao = ag.descricao;
		this.rateio = ag.rateio;
		this.documento = ag.documento;
		this.vinculoProvisionado = ag.vinculoProvisionado;
	}
	
	
	public Agendamento(Integer agendamento_cdagendamento, 
						String agendamento_descricao, 
						java.util.Date agendamento_dtproximo, 
						Boolean agendamento_ativo, 
						Money agendamento_valor, 
						Integer pessoa_cdpessoa,
						String pessoa_nome,
						String agendamento_outrospagamento,
						Tipopagamento agendamento_tipopagamento,
						java.util.Date aux_agendamento_dtUltimaConsolidacao, 
						Integer aux_agendamento_situacao,
						Integer tipooperacao_cdtipooperacao,
						String tipooperacao_nome,
						Integer tipovinculo_cdtipovinculo,
						String tipovinculo_nome,
						String conta_nome,
						String coalesce_pagamento){
		this.cdagendamento = agendamento_cdagendamento;
		this.descricao = agendamento_descricao;
		this.dtproximo = agendamento_dtproximo != null ? new Date(agendamento_dtproximo.getTime()) : null;
		this.ativo = agendamento_ativo;
		this.valor = agendamento_valor;
		this.pessoa = new Pessoa(pessoa_cdpessoa, pessoa_nome);
		this.outrospagamento = agendamento_outrospagamento;
		this.tipopagamento = agendamento_tipopagamento;
		this.aux_agendamento = new Aux_agendamento(aux_agendamento_dtUltimaConsolidacao != null ? new Date(aux_agendamento_dtUltimaConsolidacao.getTime()) : null , aux_agendamento_situacao);
		this.tipooperacao = new Tipooperacao(tipooperacao_cdtipooperacao, tipooperacao_nome);
		this.tipovinculo = new Tipovinculo(tipovinculo_cdtipovinculo, tipovinculo_nome);
		this.conta = new Conta(conta_nome);
	}
	
	@Transient
	@DisplayName("Este agendamento tem prazo para t�rmino")
	public Boolean getAgendamentoTemPrazoParaTerminio() {
		return agendamentoTemPrazoParaTerminio;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@Transient
	@DisplayName("Caixa")
	@Required
	public Conta getCaixa() {
		return caixa;
	}

	@Transient
	@DisplayName("Cart�o de cr�dito")
	@Required
	public Conta getCartaoCredito() {
		return cartaoCredito;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamento")
	public Integer getCdagendamento() {
		return cdagendamento;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Transient
	@DisplayName("Cliente")
	@Required
	public Cliente getCliente() {
		return cliente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	@DisplayName("Conta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}

	@Transient
	@DisplayName("Conta banc�ria")
	@Required
	public Conta getContaBancaria() {
		return contaBancaria;
	}

	@DisplayName("Descri��o")
	@DescriptionProperty
	@MaxLength(100)
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Documento")
	@MaxLength(50)
	public String getDocumento() {
		return documento;
	}

	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("�ltima data")
	public Date getDtfim() {
		return dtfim;
	}

	@DisplayName("Primeira data")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Pr�ximo vencimento")
	@Required
	public Date getDtproximo() {
		return dtproximo;
	}

	@Transient
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
		
	@Transient
	public Date getDtatual() {
		return dtatual;
	}

	@Transient
	@DisplayName("Fornecedor")
	@Required
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@DisplayName("Frequ�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	@Required
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	@Required
	@DisplayName("Forma de Pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")	
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}

	@DisplayName("V�nculo Provisionado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvinculoprovisionado")
	public Conta getVinculoProvisionado() {
		return vinculoProvisionado;
	}
	
	@DisplayName("N�mero de itera��es")
	@MinValue(1)
	@MaxValue(200)
	@Required
	public Integer getIteracoes() {
		return iteracoes;
	}

	@DisplayName("Taxa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxa")
	public Taxa getTaxa() {
		return taxa;
	}

	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="agendamento")
	public Set<AgendamentoHistorico> getListaAgendamentoHistorico() {
		return listaAgendamentoHistorico;
	}

	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}

	@Transient
	@DisplayName("Situa��o")
	public SituacaoAgendamento getSituacao() {
		if (this.situacao == null && this.aux_agendamento != null && this.aux_agendamento.getSituacao() != null) {
			SituacaoAgendamento[] values = SituacaoAgendamento.values();
			for (SituacaoAgendamento situacaoAgendamento : values) {
				if(situacaoAgendamento.getValue().equals(this.aux_agendamento.getSituacao())){
					this.situacao = situacaoAgendamento;
					break;
				}
			}
		}
		return situacao;
	}

	@DisplayName("Opera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	@Required
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	@DisplayName("V�nculo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipovinculo")
	@Required
	public Tipovinculo getTipovinculo() {
		return tipovinculo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamento", insertable=false, updatable=false)
	public Aux_agendamento getAux_agendamento() {
		return aux_agendamento;
	}

	@DisplayName("Valor")
	@Required
	public Money getValor() {
		return valor;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Tipo de pagamento")
	@Required
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	
	@Required
	@DisplayName("Outros")
	@MaxLength(50)
	public String getOutrospagamento() {
		return outrospagamento;
	}
	
	@DisplayName("Consolidar como")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
	
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setAgendamentoTemPrazoParaTerminio(Boolean agendamentoTemPrazoParaTerminio) {
		this.agendamentoTemPrazoParaTerminio = agendamentoTemPrazoParaTerminio;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setCaixa(Conta caixa) {
		this.caixa = caixa;
	}

	public void setCartaoCredito(Conta cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}

	public void setCdagendamento(Integer cdagendamento) {
		this.cdagendamento = cdagendamento;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setContaBancaria(Conta contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtproximo(Date dtproximo) {
		this.dtproximo = dtproximo;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setIteracoes(Integer iteracoes) {
		this.iteracoes = iteracoes;
	}

	public void setTaxa(Taxa taxas) {
		this.taxa = taxas;
	}

	public void setListaAgendamentoHistorico(Set<AgendamentoHistorico> listaAgendamentoHistorico) {
		this.listaAgendamentoHistorico = listaAgendamentoHistorico;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}

	public void setSituacao(SituacaoAgendamento situacao) {
		this.situacao = situacao;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setTipovinculo(Tipovinculo tipovinculo) {
		this.tipovinculo = tipovinculo;
	}

	public void setAux_agendamento(Aux_agendamento aux_agendamento) {
		this.aux_agendamento = aux_agendamento;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@SuppressWarnings("unused")
	private String pessoaOutros;
	
	@Transient
	@DisplayName("Pessoa")
	public String getPessoaOutros() {
		if (getPessoa() != null && getPessoa().getNome() != null) {
			return getPessoa().getNome();
		} else if (getOutrospagamento() != null) {
			return getOutrospagamento();
		} else return null;
	}
	
	public void setPessoaOutros(String pessoaOutros) {
		this.pessoaOutros = pessoaOutros;
	}
	
	@Override
	public String toString() {
		String string = " Data: " + SinedDateUtils.toString(this.dtproximo) + 
		" Descricao: "+(this.cliente != null ? this.cliente.nome : this.fornecedor!=null ? this.fornecedor.nome : this.descricao) + 
		" ID: " + this.cdagendamento;
		return string;
	}

	/**
	 * Este m�todo � usado para comparar os campos obrigatorios de dois agendamentos.
	 * � utilizado para verificar se o mesmo agendamento est� sendo salvo duas vezes.
	 * Estar� inv�lido quando o problema de o sistema aceitar v�rias requisi��es de salvar ao mesmo tempo.
	 * @param a
	 * @return
	 */
	public boolean equalsToSave(Agendamento a) {
		boolean test =
			this.descricao.equals(a.descricao)
			&& this.tipooperacao.equals(a.tipooperacao)
			&& this.valor.compareTo(a.valor) == 0
			&& this.dtproximo.equals(a.dtproximo)
			&& this.frequencia.equals(a.frequencia);
		
		if(this.fornecedor != null && a.fornecedor != null){
			test = test && this.fornecedor.equals(a.fornecedor);
		}
		if(this.cliente != null && a.cliente != null){
			test = test && this.cliente.equals(a.cliente);
		}
		if(this.colaborador != null && a.colaborador != null){
			test = test && this.colaborador.equals(a.colaborador);
		}
		if(this.outrospagamento != null && !this.outrospagamento.equals("") && a.outrospagamento != null && !a.outrospagamento.equals("")){
			test = test && this.outrospagamento.equals(a.outrospagamento);
		}
		if(test){
			if((this.getRateio() != null && a.getRateio() == null) || (this.getRateio() == null && a.getRateio() != null)){
				test = false;
			} else if(this.getRateio() != null && a.getRateio() != null){
				if((this.getRateio().getListaRateioitem() != null && a.getRateio().getListaRateioitem() == null) || (this.getRateio().getListaRateioitem() == null && a.getRateio().getListaRateioitem() != null)){
					test = false;
				} else if(this.getRateio().getListaRateioitem() != null && a.getRateio().getListaRateioitem() != null){
					if(this.getRateio().getListaRateioitem().size() ==  a.getRateio().getListaRateioitem().size()){
						for(Rateioitem itemSession : a.getRateio().getListaRateioitem()){
							for(Rateioitem itemThis : this.getRateio().getListaRateioitem()){
								if(itemThis.toStringAgendamento().equals(itemSession.toStringAgendamento())){
									test = true;
								} else {
									test = false;
									break;
								}
							}
						}
					}
				} else {
					test = false;
				}
			} 		
		}
		return test;
	}
	
	/**
	 * M�todo para carregar a pessoa correspondente do agendamento de acordo com tipo de pagamento
	 * 
	 * @return o pr�prio objeto
	 * @author Fl�vio Tavares
	 */
	public Agendamento ajustaBeanToLoad(){
		if(this.pessoa != null){
			if(Tipopagamento.CLIENTE.equals(this.tipopagamento)){
				this.cliente = new Cliente(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			}else
			if(Tipopagamento.FORNECEDOR.equals(this.tipopagamento)){
				this.fornecedor = new Fornecedor(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			}else
			if(Tipopagamento.COLABORADOR.equals(this.tipopagamento)){
				this.colaborador = new Colaborador(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			}
		}
		return this;
	}
	
	/**
	 * M�todo para setar a pessoa com a pessoa correspondente escolhida (Cliente, Fornecedor ou Colaborador)
	 * dependendo do tipo de pagamento
	 * 
	 * @return o pr�prio objeto
	 * @author Fl�vio Tavares
	 */
	public Agendamento ajustaBeanToSave(){
		Pessoa pessoa = null;
		if(Tipopagamento.CLIENTE.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.cliente.getCdpessoa(),null);
		}else
		if(Tipopagamento.FORNECEDOR.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.fornecedor.getCdpessoa(),null);
		}else
		if(Tipopagamento.COLABORADOR.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.colaborador.getCdpessoa(),null);
		}
		
		this.pessoa = pessoa;
		
		return this;
	}

	public String subQueryProjeto() {
		return "select agendamentosubQueryProjeto.cdagendamento " +
				"from Agendamento agendamentosubQueryProjeto " +
				"join agendamentosubQueryProjeto.rateio rateiosubQueryProjeto " +
				"join rateiosubQueryProjeto.listaRateioitem listaRateioitemsubQueryProjeto " +
				"join listaRateioitemsubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select agendamentoSubQueryClienteEmpresa.cdagendamento " +
				"from Agendamento agendamentoSubQueryClienteEmpresa " +
				"left outer join agendamentoSubQueryClienteEmpresa.pessoa clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	public String subQueryFornecedorEmpresa(){
		return  "select agendamentoSubQueryFornecedorEmpresa.cdagendamento " +
				"from Agendamento agendamentoSubQueryFornecedorEmpresa " +
				"left outer join agendamentoSubQueryFornecedorEmpresa.pessoa fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@Transient
	@DisplayName("V�nculo")
	public String getVinculo(){
		if (getConta() != null && getConta().getNome() != null)
			return getConta().getNome();
		else if (getTipovinculo() != null && getTipovinculo().getNome() != null)
			return getTipovinculo().getNome();
		else 
			return "";
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
	@Required
	@DisplayName("Opera��o Cont�bil")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}

	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}

	public void setDtatual(Date dtatual) {
		this.dtatual = dtatual;
	}

	public void setVinculoProvisionado(Conta vinculoProvisionado) {
		this.vinculoProvisionado = vinculoProvisionado;
	}
	
	@Transient
	public Rateiomodelo getRateiomodelo() {
		return rateiomodelo;
	}
	public void setRateiomodelo(Rateiomodelo rateiomodelo) {
		this.rateiomodelo = rateiomodelo;
	}
}