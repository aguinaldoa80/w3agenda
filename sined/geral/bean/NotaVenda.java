package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_notavenda", sequenceName = "sq_notavenda")
public class NotaVenda {

	protected Integer cdnotavenda;
	protected Nota nota;
	protected Venda venda;
	protected Expedicao expedicao;
	
	public NotaVenda (){
		super();
	}
	
	public NotaVenda (Nota nota, Venda venda){
		this.nota = nota;
		this.venda = venda;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notavenda")
	public Integer getCdnotavenda() {
		return cdnotavenda;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Nota getNota() {
		return nota;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}

	public void setCdnotavenda(Integer cdnotavenda) {
		this.cdnotavenda = cdnotavenda;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
}
