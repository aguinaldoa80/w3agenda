package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_clienteprofissao", sequenceName = "sq_clienteprofissao")
public class Clienteprofissao implements Log {
	
	protected Integer cdclienteprofissao;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Clienteprofissao() {
		// TODO Auto-generated constructor stub
	}
	
	public Clienteprofissao(Integer cdclienteprofissao) {
		super();
		this.cdclienteprofissao = cdclienteprofissao;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienteprofissao")
	public Integer getCdclienteprofissao() {
		return cdclienteprofissao;
	}

	@Required
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdclienteprofissao(Integer cdclienteprofissao) {
		this.cdclienteprofissao = cdclienteprofissao;
	}

	public void setNome(String nome) {
		this.nome = nome.trim();
	}
	
	// LOG

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
