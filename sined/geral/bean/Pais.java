package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_pais", sequenceName = "sq_pais")
public class Pais implements Log{
	
	public static final Pais BRASIL = new Pais(36);
	
	protected Integer cdpais;
	protected String nome;
	protected String cdbacen;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Pais() {
	}
	
	public Pais(Integer cdpais) {
		this.cdpais = cdpais;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pais")
	public Integer getCdpais() {
		return cdpais;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCdbacen() {
		return cdbacen;
	}
	
	public void setCdbacen(String cdbacen) {
		this.cdbacen = cdbacen;
	}
	
	public void setCdpais(Integer cdpais) {
		this.cdpais = cdpais;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdpais == null) ? 0 : cdpais.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (cdpais == null) {
			if (other.cdpais != null)
				return false;
		} else if (!cdpais.equals(other.cdpais))
			return false;
		return true;
	}
	
}
