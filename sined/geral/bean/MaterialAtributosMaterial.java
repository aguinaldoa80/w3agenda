package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_materialatributosmaterial", sequenceName = "sq_materialatributosmaterial")
public class MaterialAtributosMaterial implements Log{
	protected Integer cdmaterialatributosmaterial;
	protected AtributosMaterial atributosMaterial;
	protected Material material;
	protected String valor;
	protected Boolean exibir = Boolean.TRUE;	;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialatributosmaterial")
	public Integer getCdmaterialatributosmaterial() {
		return cdmaterialatributosmaterial;
	}
	public void setCdmaterialatributosmaterial(Integer cdmaterialatributosmaterial) {
		this.cdmaterialatributosmaterial = cdmaterialatributosmaterial;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatributosmaterial")
	public AtributosMaterial getAtributosMaterial() {
		return atributosMaterial;
	}
	public void setAtributosMaterial(AtributosMaterial atributosMaterial) {
		this.atributosMaterial = atributosMaterial;
	}
	@Required
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public Boolean getExibir() {
		return exibir;
	}
	public void setExibir(Boolean exibir) {
		this.exibir = exibir;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	
	
	
	
}
