package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaavulsarhacao;

@Entity
@SequenceGenerator(name="sq_despesaavulsarhhistorico",sequenceName="sq_despesaavulsarhhistorico")
public class Despesaavulsarhhistorico {

	private Integer cddespesaavulsarhhistorico;
	private Despesaavulsarh despesaavulsarh;
	private Despesaavulsarhacao acao;
	private String observacao;
	private Timestamp dtaltera;
	private Usuario usuario;
	
	@Id
	@GeneratedValue(generator = "sq_despesaavulsarhhistorico", strategy = GenerationType.AUTO)
	public Integer getCddespesaavulsarhhistorico() {
		return cddespesaavulsarhhistorico;
	}

	public void setCddespesaavulsarhhistorico(Integer cddespesaavulsarhhistorico) {
		this.cddespesaavulsarhhistorico = cddespesaavulsarhhistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaavulsarh")
	public Despesaavulsarh getDespesaavulsarh() {
		return despesaavulsarh;
	}

	public void setDespesaavulsarh(Despesaavulsarh despesaavulsarh) {
		this.despesaavulsarh = despesaavulsarh;
	}

	@DisplayName("A��o")
	public Despesaavulsarhacao getAcao() {
		return acao;
	}

	public void setAcao(Despesaavulsarhacao acao) {
		this.acao = acao;
	}

	@MaxLength(100)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@JoinColumn(name="cdusuarioaltera")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Respons�vel")
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
