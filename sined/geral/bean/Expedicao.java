package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacaoentrega;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.ConferienciaPneuBeam;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.CompararCampo;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@JoinEmpresa("expedicao.empresa")
@SequenceGenerator(name = "sq_expedicao", sequenceName = "sq_expedicao")
public class Expedicao implements Log, PermissaoProjeto {

	protected Integer cdexpedicao;
	protected Long identificadorcarregamento;
	protected Empresa empresa;
	protected Fornecedor transportadora;
	protected Date dtexpedicao;
	protected String observacao;
	protected Integer quantidadevolumes;
	protected Expedicaosituacao expedicaosituacao = Expedicaosituacao.EM_ABERTO; 
	protected Expedicaosituacaoentrega expedicaosituacaoentrega; 
	protected Boolean inspecaocompleta;
	protected Boolean conferenciamateriais;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Expedicaoitem> listaExpedicaoitem = new ListSet<Expedicaoitem>(Expedicaoitem.class);
	protected List<Expedicaohistorico> listaExpedicaohistorico = new ListSet<Expedicaohistorico>(Expedicaohistorico.class);
	protected List<ExpedicaoAcompanhamentoEntrega> listaAcompanhamentoEntrega;
	protected Boolean isPneuConferido;
	protected List<ConferenciaMaterialExpedicao> listaConferencia;
	protected EventosFinaisCorreios eventosFinaisCorreios;
	
	private String rastreamento;
	private String urlRastreamento;
	
	private Money valorCustoFrete;
	private Date dataPrevisaoEntrega;
	private EventosCorreios eventoCorreios;

	
	// TRANSENTES
	protected Boolean fromVenda = Boolean.FALSE;
	protected String vendas;
	protected Boolean isCriar;
	private String whereIn;
	private List<Expedicao> listaExpedicao;
	private List<Venda> listaVenda;
	private String controller;
	private Cliente cliente;
	private Venda venda;
	private List<String> listDtVenda;
	private Boolean existeNotaVendaEmitida;
	private List<String> vendasTrans;
	private String whereInColetaOuNota;
	private List<ConferienciaPneuBeam> listaConfderencia;
	private Notafiscalproduto notaFiscalProduto;
	private Boolean identificadorAutomatico;
	private Boolean conferirExpedicaoFromVenda = Boolean.TRUE;
	
	// Clients Data
	private Map<String, String> listaRazaoSocial;
	private Map<String, String> listaNomeFantasia;
	private List<String> listaCpfCnpj;
	private List<String> listaEnderecos;
	private List<String> listaCidades;
	private String idTelaSession;
	private String idTela;
	private Boolean gerandoPelaVenda;
	protected EventosFinaisCorreios eventosFinaisCorreiosTrans;
	
	public Expedicao() {
	}
	
	public Expedicao(Integer cdexpedicao) {
		this.cdexpedicao = cdexpedicao;
	}
	/////////////////////////////////////////|
	//  INICIO DOS GET 				/////////|
	/////////////////////////////////////////|
	@Id
	@DisplayName("Id da expedi��o")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicao")
	public Integer getCdexpedicao() {
		return cdexpedicao;
	}
	
	@Required
	@MaxLength(10)
	@DisplayName("Id do carregamento")
	public Long getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtransportadora")
	public Fornecedor getTransportadora() {
		return transportadora;
	}

	@Required
	@DisplayName("Data")
	public Date getDtexpedicao() {
		return dtexpedicao;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("Quantidade de Volumes")
	public Integer getQuantidadevolumes() {
		return quantidadevolumes;
	}
	
	@DisplayName("Situa��o")
	public Expedicaosituacao getExpedicaosituacao() {
		return expedicaosituacao;
	}
	
	@DisplayName("Situa��o Entrega")
	public Expedicaosituacaoentrega getExpedicaosituacaoentrega() {
		return expedicaosituacaoentrega;
	}
	
	@OneToMany(mappedBy="expedicao")
	public List<Expedicaoitem> getListaExpedicaoitem() {
		return listaExpedicaoitem;
	}
	
	@OneToMany(mappedBy="expedicao")
	public List<Expedicaohistorico> getListaExpedicaohistorico() {
		return listaExpedicaohistorico;
	}
	
	@OneToMany(mappedBy="expedicao")
	public List<ExpedicaoAcompanhamentoEntrega> getListaAcompanhamentoEntrega() {
		return listaAcompanhamentoEntrega;
	}
	
	public Boolean getInspecaocompleta() {
		return inspecaocompleta;
	}

	public Boolean getConferenciamateriais() {
		return conferenciamateriais;
	}
	
	public Boolean getIsPneuConferido() {
		return isPneuConferido;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="expedicao")
	public List<ConferenciaMaterialExpedicao> getListaConferencia() {
		return listaConferencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventosfinaiscorreios")
	public EventosFinaisCorreios getEventosFinaisCorreios() {
		return eventosFinaisCorreios;
	}
	/////////////////////////////////////////|
	//  INICIO DOS SET 				/////////|
	/////////////////////////////////////////|
	public void setIsPneuConferido(Boolean isPneuConferido) {
		this.isPneuConferido = isPneuConferido;
	}
	public void setListaConferencia(List<ConferenciaMaterialExpedicao> listaConferencia) {
		this.listaConferencia = listaConferencia;
	}
	public void setEventosFinaisCorreios(EventosFinaisCorreios eventosFinaisCorreios) {
		this.eventosFinaisCorreios = eventosFinaisCorreios;
	}
	public void setListaExpedicaohistorico(
			List<Expedicaohistorico> listaExpedicaohistorico) {
		this.listaExpedicaohistorico = listaExpedicaohistorico;
	}
	public void setListaAcompanhamentoEntrega(List<ExpedicaoAcompanhamentoEntrega> listaAcompanhamentoEntrega) {
		this.listaAcompanhamentoEntrega = listaAcompanhamentoEntrega;
	}
	public void setListaExpedicaoitem(List<Expedicaoitem> listaExpedicaoitem) {
		this.listaExpedicaoitem = listaExpedicaoitem;
	}

	public void setCdexpedicao(Integer cdexpedicao) {
		this.cdexpedicao = cdexpedicao;
	}

	public void setIdentificadorcarregamento(Long identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setTransportadora(Fornecedor transportadora) {
		this.transportadora = transportadora;
	}

	public void setDtexpedicao(Date dtexpedicao) {
		this.dtexpedicao = dtexpedicao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setQuantidadevolumes(Integer quantidadevolumes) {
		this.quantidadevolumes = quantidadevolumes;
	}
	
	public void setExpedicaosituacao(Expedicaosituacao expedicaosituacao) {
		this.expedicaosituacao = expedicaosituacao;
	}
	
	public void setExpedicaosituacaoentrega(
			Expedicaosituacaoentrega expedicaosituacaoentrega) {
		this.expedicaosituacaoentrega = expedicaosituacaoentrega;
	}

	public void setInspecaocompleta(Boolean inspecaocompleta) {
		this.inspecaocompleta = inspecaocompleta;
	}
	
	public void setConferenciamateriais(Boolean conferenciamateriais) {
		this.conferenciamateriais = conferenciamateriais;
	}

	//////////////////////////////////////////////|
	//  CAMPOS DO LOG					 /////////|
	//////////////////////////////////////////////|
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	//////////////////////////////////////////////|
	//  INICIO DOS TRANSIENTES	E OUTROS /////////|
	//////////////////////////////////////////////|
	
	@Transient
	public Boolean getFromVenda() {
		return fromVenda;
	}
	
	@Transient
	public String getVendas() {
		return vendas;
	}
	
	@Transient
	public Boolean getIsCriar() {
		return isCriar;
	}
	@Transient
	public String getWhereInColetaOuNota() {
		return whereInColetaOuNota;
	}
	@Transient
	public List<ConferienciaPneuBeam> getListaConfderencia() {
		return listaConfderencia;
	}
	@Transient
	public Notafiscalproduto getNotaFiscalProduto() {
		return notaFiscalProduto;
	}

	public void setNotaFiscalProduto(Notafiscalproduto notaFiscalProduto) {
		this.notaFiscalProduto = notaFiscalProduto;
	}
	public void setListaConfderencia(List<ConferienciaPneuBeam> listaConfderencia) {
		this.listaConfderencia = listaConfderencia;
	}

	public void setWhereInColetaOuNota(String whereInColetaOuNota) {
		this.whereInColetaOuNota = whereInColetaOuNota;
	}
	
	public void setIsCriar(Boolean isCriar) {
		this.isCriar = isCriar;
	}
	
	public void setVendas(String vendas) {
		this.vendas = vendas;
	}

	public void setFromVenda(Boolean fromVenda) {
		this.fromVenda = fromVenda;
	}	
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	@Transient
	public List<Expedicao> getListaExpedicao() {
		return listaExpedicao;
	}
	@Transient
	public String getController() {
		return controller;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setListaExpedicao(List<Expedicao> listaExpedicao) {
		this.listaExpedicao = listaExpedicao;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	
	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	@Transient
	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	public List<Venda> getListaVenda() {
		return listaVenda;
	}

	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	@Transient
	public Boolean getExisteNotaVendaEmitida() {
		return existeNotaVendaEmitida;
	}
	
	@Transient
	@DisplayName("Vendas")
	public List<String> getVendasTrans() {
		List<String> codVendas = new ArrayList<String>();
		if(this.getListaExpedicaoitem()!=null){
			for(Expedicaoitem item: this.getListaExpedicaoitem()){
				if(item.getVendamaterial()!=null && item.getVendamaterial().getVenda()!=null){
					Venda venda = item.getVendamaterial().getVenda();
					if(venda.getIdentificadorOrCdvenda()!=null && !codVendas.contains(venda.getIdentificadorOrCdvenda())){
						codVendas.add(item.getVendamaterial().getVenda().getIdentificadorOrCdvenda());
					}

				}
			}		
		}
		return codVendas;
	}
	
	public void setVendasTrans(List<String> vendasTrans) {
		this.vendasTrans = vendasTrans;
	}
	
	@Transient
	public List<String> getListDtVenda() {
		List<String> listDtVenda = new ArrayList<String>();
		if(this.getListaExpedicaoitem()!=null){			
			List<String> codVendas = new ArrayList<String>();
			for(Expedicaoitem item: this.getListaExpedicaoitem()){
				if(item.getVendamaterial()!=null && item.getVendamaterial().getVenda()!=null){
					Venda venda = item.getVendamaterial().getVenda();
					if(venda.getIdentificadorOrCdvenda()!=null && !codVendas.contains(venda.getIdentificadorOrCdvenda())){
						codVendas.add(item.getVendamaterial().getVenda().getIdentificadorOrCdvenda());
						listDtVenda.add(SinedDateUtils.toString(venda.getDtvenda()));
					}

				}
			}			
		}
		return listDtVenda;
	}

	public void setListDtVenda(List<String> listDtVenda) {
		this.listDtVenda = listDtVenda;
	}

	public void setExisteNotaVendaEmitida(Boolean existeNotaVendaEmitida) {
		this.existeNotaVendaEmitida = existeNotaVendaEmitida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdexpedicao == null) ? 0 : cdexpedicao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Expedicao other = (Expedicao) obj;
		if (cdexpedicao == null) {
			if (other.cdexpedicao != null)
				return false;
		} else if (!cdexpedicao.equals(other.cdexpedicao))
			return false;
		return true;
	}
	
	@Transient
	public String getCodigosDeVenda(){
		if(listaExpedicaoitem == null || listaExpedicaoitem.isEmpty())
			return null;
		
		List<Integer> codigosVenda = new ArrayList<Integer>();
		for(Expedicaoitem expedicaoitem : listaExpedicaoitem){
			if(expedicaoitem.getCodigovenda() != null && !codigosVenda.contains(expedicaoitem.getCodigovenda())){
				codigosVenda.add(expedicaoitem.getCodigovenda());
			}
		}
		return CollectionsUtil.concatenate(codigosVenda, ", ");
	}
	
	public String subQueryProjeto() {
		return "select expedicaosubQueryProjeto.cdexpedicao " +
				"from Expedicao expedicaosubQueryProjeto " +
				"left outer join expedicaosubQueryProjeto.listaExpedicaoitem listaExpedicaoitemsubQueryProjeto " +
				"left outer join listaExpedicaoitemsubQueryProjeto.vendamaterial vendamaterialsubQueryProjeto " +
				"left outer join vendamaterialsubQueryProjeto.venda vendasubQueryProjeto " +
				"left outer join vendasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto is null or projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	public void setListaRazaoSocial(Map<String, String> listaRazaoSocial ) {
		this.listaRazaoSocial = listaRazaoSocial ;
	}

	@Transient
	public Map<String, String> getListaRazaoSocial() {
		List<String> cdVendas = getVendasTrans();
		List<String> newCdVendas = new ArrayList<String>();
		Map<String, String> listaRazaoSocial = new HashMap<String,String>();
		if(getListaExpedicaoitem() != null && getListaExpedicaoitem().size() > 0){
			if(cdVendas.size() > 0){
				for(Expedicaoitem ei: getListaExpedicaoitem()){
					if(ei.getVendamaterial() != null){
						if(!newCdVendas.contains(ei.getVendamaterial().getVenda().getIdentificadorOrCdvenda())){
							newCdVendas.add(ei.getVendamaterial().getVenda().getIdentificadorOrCdvenda());
							if(ei.getCliente().getRazaosocial() != null && !ei.getCliente().getRazaosocial().isEmpty()){
								listaRazaoSocial.put("<b>Nome:</b> "+ei.getCliente().getNome()+" / <br/><b>Raz�o Social: </b>"+ei.getCliente().getRazaosocial(), ei.getCliente().getRazaosocial());										
								
							}else{
								listaRazaoSocial.put("<b>Nome:</b> "+ei.getCliente().getNome(), ei.getCliente().getNome());				
							}						
						}											
					}
				}
			} else {
				for(Expedicaoitem ei: getListaExpedicaoitem()){
					if(ei.getCliente().getRazaosocial() != null && !ei.getCliente().getRazaosocial().isEmpty()){
						if(!listaRazaoSocial.containsValue(ei.getCliente().getRazaosocial())){
							listaRazaoSocial.put("<b>Nome:</b> "+ei.getCliente().getNome()+" / <br/><b>Raz�o Social: </b>"+ei.getCliente().getRazaosocial(),
									ei.getCliente().getRazaosocial());										
						}					
					}else{
						if(!listaRazaoSocial.containsValue(ei.getCliente().getNome())){
							listaRazaoSocial.put("<b>Nome:</b> "+ei.getCliente().getNome(), ei.getCliente().getNome());				
						}	
					}
				}
				if(listaRazaoSocial.size() > 5){
					listaRazaoSocial.clear();
					listaRazaoSocial.put("\n", "DIVERSOS");
				}
			}
		}else {
			listaRazaoSocial.put("\n", "N�O DEFINIDO");
		}
		return listaRazaoSocial;
	}

	@Transient
	public Map<String, String> getListaNomeFantasia() {
		Map<String, String> listaNomeFantasia = new HashMap<String,String>();
		List<String> cdVendas = getVendasTrans();
		List<String> newCdVendas = new ArrayList<String>();
		if(getListaExpedicaoitem() != null && getListaExpedicaoitem().size() > 0){
			if(cdVendas.size() > 0){
				for(Expedicaoitem ei: getListaExpedicaoitem()){
					if(ei.getVendamaterial() != null){
						if(!newCdVendas.contains(ei.getVendamaterial().getVenda().getIdentificadorOrCdvenda())){
							newCdVendas.add(ei.getVendamaterial().getVenda().getIdentificadorOrCdvenda());
							String rsocial = "<b>Nome:</b> " + ei.getCliente().getNome();
							rsocial += ei.getCliente().getRazaosocial() == null ? "" :  "<br/><b>Raz�o Social:</b> " +ei.getCliente().getRazaosocial();
							listaNomeFantasia.put(rsocial, ei.getCliente().getNome());						
						}	
					}
				}
			}else{				
				for(Expedicaoitem ei: getListaExpedicaoitem()){
					if(!listaNomeFantasia.containsValue(ei.getCliente().getNome())){
						String rsocial = "<b>Nome:</b> " + ei.getCliente().getNome();
						rsocial += ei.getCliente().getRazaosocial() == null ? "" :  "<br/><b>Raz�o Social:</b> " +ei.getCliente().getRazaosocial();
						listaNomeFantasia.put(rsocial, ei.getCliente().getNome());				
					}
				}
				if(listaNomeFantasia.size() > 5){
					listaNomeFantasia.clear();
					listaNomeFantasia.put("\n", "DIVERSOS");
				}		
			}
		} else {
			listaNomeFantasia.put("\n", "N�O DEFINIDO");
		}	
		return listaNomeFantasia;
	}

	public void setListaNomeFantasia(Map<String, String> listaNomeFantasia) {
		this.listaNomeFantasia = listaNomeFantasia;
	}

	@Transient
	public List<String> getListaCpfCnpj() {
		boolean isNomeFantasia = SinedUtil.isListarRazaoSocianClienteExpedicao();
		Map<String, String> listNomes = isNomeFantasia ? this.getListaRazaoSocial() : this.getListaNomeFantasia();
		List<String> listaCpfCnpj = new ArrayList<String>();	
		
		if(listNomes.size() > 0){
			if(getListaExpedicaoitem() != null && getListaExpedicaoitem().size() > 0){
				for(Map.Entry<String, String> nomes: listNomes.entrySet()){
					List<String> currentCpfCnpj = new ArrayList<String>();
					for(Expedicaoitem ei: getListaExpedicaoitem()){
						String cpfCnpj = ei.getCliente().getCpfCnpj();
						if(!currentCpfCnpj.contains(cpfCnpj)){
							currentCpfCnpj.add(cpfCnpj);
							if(isNomeFantasia && ei.getCliente().getRazaosocial() != null){
								if(nomes.getValue().equals(ei.getCliente().getRazaosocial())){
									listaCpfCnpj.add(cpfCnpj != null && !cpfCnpj.isEmpty() ? cpfCnpj : "&nbsp;");
								}	
							} else {
								if(nomes.getValue().equals(ei.getCliente().getNome())){
									listaCpfCnpj.add(cpfCnpj != null && !cpfCnpj.isEmpty() ? cpfCnpj : "&nbsp;");
								}						
							}							
						}
						
					}
				}
			}
		}
		return listaCpfCnpj;
	}

	public void setListaCpfCnpj(List<String> listaCpfCnpj) {
		this.listaCpfCnpj = listaCpfCnpj;
	}

	@Transient
	public List<String> getListaEnderecos() {
		boolean isNomeFantasia = SinedUtil.isListarRazaoSocianClienteExpedicao();
		Map<String, String> listNomes = isNomeFantasia ? this.getListaRazaoSocial() : this.getListaNomeFantasia();
		List<String> listaEnderecos = new ArrayList<String>();	
		if(listNomes.size() > 0){
			if(getListaExpedicaoitem() != null && getListaExpedicaoitem().size() > 0){
				for(Map.Entry<String, String> nomes: listNomes.entrySet()){
					List<String> currentEndereco = new ArrayList<String>();					
					for(Expedicaoitem ei: getListaExpedicaoitem()){
						String endereco = "";
						
						if(ei.getCliente().getEnderecoBairro() != null && !ei.getCliente().getEnderecoBairro().isEmpty()){
							endereco = ei.getCliente().getEnderecoBairro();
						}				
						if(ei.getCliente().getEnderecoCidade() != null && !ei.getCliente().getEnderecoCidade().isEmpty()){
							if(StringUtils.isNotBlank(endereco) && ei.getCliente().getEnderecoCidade() != null) 
								endereco += " - ";
							endereco += ei.getCliente().getEnderecoCidade();
						}				
						if(ei.getCliente().getEnderecoSiglaEstado() != null && !ei.getCliente().getEnderecoSiglaEstado().isEmpty()){
							if(StringUtils.isNotBlank(endereco)) endereco += "/";
							endereco += ei.getCliente().getEnderecoSiglaEstado();
						}	
						
						if(!currentEndereco.contains(endereco)){
							currentEndereco.add(endereco);
							if(isNomeFantasia && ei.getCliente().getRazaosocial() != null){
								if(nomes.getValue().equals(ei.getCliente().getRazaosocial())){
									listaEnderecos.add(endereco != null && !endereco.isEmpty() ? endereco : "&nbsp;");
								}	
							} else {
								if(nomes.getValue().equals(ei.getCliente().getNome())){
									listaEnderecos.add(endereco != null && !endereco.isEmpty() ? endereco : "&nbsp;");
								}						
							}	
						}						
					}
				}
			}
		}
		return listaEnderecos;
	}
	

	public void setListaEnderecos(List<String> listaEnderecos) {
		this.listaEnderecos = listaEnderecos;
	}

	@Transient
	public List<String> getListaCidades() {
		List<String> listaCidades = new ArrayList<String>();
		if(getListaExpedicaoitem() != null && getListaExpedicaoitem().size() > 0){
			for(Expedicaoitem ei: getListaExpedicaoitem()){
				if(!listaCidades.contains(ei.getCliente().getEnderecoCidade()+ "/" + ei.getCliente().getEnderecoSiglaEstado())){
					if(!ei.getCliente().getEnderecoCidade().isEmpty()){
						listaCidades.add(ei.getCliente().getEnderecoCidade() + "/" + ei.getCliente().getEnderecoSiglaEstado());											
					}
				}
			}
			if(listaCidades.size() > 5){
				listaCidades.clear();
				listaCidades.add("DIVERSOS");
			}
		} else {
			listaCidades.add("N�O DEFINIDO");
		}
		return listaCidades;
	}

	public void setListaCidades(List<String> listaCidades) {
		this.listaCidades = listaCidades;
	}	

	@Transient
	public Boolean getIdentificadorAutomatico() {
		return identificadorAutomatico;
	}
	
	public void setIdentificadorAutomatico(Boolean identificadorAutomatico) {
		this.identificadorAutomatico = identificadorAutomatico;
	}	
	
	@Transient
	public String getIdTelaSession() {
		return idTelaSession;
	}
	public void setIdTelaSession(String idTela) {
		this.idTelaSession = idTela;
	}
	
	@Transient
	public String getIdTela() {
		return idTela;
	}
	public void setIdTela(String idTela) {
		this.idTela = idTela;
	}

	@Transient
	public Boolean getConferirExpedicaoFromVenda() {
		return conferirExpedicaoFromVenda;
	}

	public void setConferirExpedicaoFromVenda(Boolean conferirExpedicaoFromVenda) {
		this.conferirExpedicaoFromVenda = conferirExpedicaoFromVenda;
	}
	
	@Transient
	public Boolean getGerandoPelaVenda() {
		return gerandoPelaVenda;
	}
	
	public void setGerandoPelaVenda(Boolean gerandoPelaVenda) {
		this.gerandoPelaVenda = gerandoPelaVenda;
	}

	@DisplayName("C�digo de Rastreamento")
	@CompararCampo
	public String getRastreamento() {
		return rastreamento;
	}

	public void setRastreamento(String rastreamento) {
		this.rastreamento = rastreamento;
	}

	@DisplayName("URL de Rastreamento")
	@CompararCampo
	public String getUrlRastreamento() {
		return urlRastreamento;
	}

	public void setUrlRastreamento(String urlRastreamento) {
		this.urlRastreamento = urlRastreamento;
	}

	@DisplayName("Custo do Frete")
	@CompararCampo
	public Money getValorCustoFrete() {
		return valorCustoFrete;
	}

	public void setValorCustoFrete(Money valorCustoFrete) {
		this.valorCustoFrete = valorCustoFrete;
	}

	@DisplayName("Previs�o de Entrega")
	@CompararCampo
	public Date getDataPrevisaoEntrega() {
		return dataPrevisaoEntrega;
	}

	public void setDataPrevisaoEntrega(Date dataPrevisaoEntrega) {
		this.dataPrevisaoEntrega = dataPrevisaoEntrega;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventoscorreios")
	public EventosCorreios getEventoCorreios() {
		return eventoCorreios;
	}
	public void setEventoCorreios(EventosCorreios eventoCorreios) {
		this.eventoCorreios = eventoCorreios;
	}
	
	@Transient
	public EventosFinaisCorreios getEventosFinaisCorreiosTrans() {
		return eventosFinaisCorreiosTrans;
	}
	public void setEventosFinaisCorreiosTrans(EventosFinaisCorreios eventosFinaisCorreiosTrans) {
		this.eventosFinaisCorreiosTrans = eventosFinaisCorreiosTrans;
	}
}