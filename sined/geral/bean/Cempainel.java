package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;


@Entity
@SequenceGenerator(name = "sq_cempainel", sequenceName = "sq_cempainel")
public class Cempainel implements ImportacaoproducaoMaterial {

	protected Integer cdcempainel;
	protected Cemtipologia cemtipologia;
	protected String codigo;
	protected String ref;
	protected String descricao;
	protected Double qtde;
	protected Double altura; 
	protected Double largura; 
	protected Double custo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cempainel")
	public Integer getCdcempainel() {
		return cdcempainel;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcemtipologia")
	public Cemtipologia getCemtipologia() {
		return cemtipologia;
	}

	@MaxLength(20)
	public String getRef() {
		return ref;
	}

	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public Double getQtde() {
		return qtde;
	}

	public Double getCusto() {
		return custo;
	}
	
	public Double getAltura() {
		return altura;
	}

	public Double getLargura() {
		return largura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setCdcempainel(Integer cdcempainel) {
		this.cdcempainel = cdcempainel;
	}

	public void setCemtipologia(Cemtipologia cemtipologia) {
		this.cemtipologia = cemtipologia;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}
	
	@Transient
	@Override
	public String getUnidademedidaMaterial() {
		return "UN";
	}
	
	@Transient
	@Override
	public Double getPesoMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public Double getComprimentoMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public String getNomeMaterial() {
		return this.descricao + " " + this.codigo;
	}
	
	@Transient
	@Override
	public Double getQuantidade() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public TipoImportacaoProducao getTipoImportacao() {
		return TipoImportacaoProducao.PAINEL;
	}
	
	@Transient
	@Override
	public Double getCustoMaterial() {
		return this.custo;
	}
	
	@Transient
	@Override
	public Double getVolume() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public Double getAlturaMaterial() {
		return this.altura;
	}
	
	@Transient
	@Override
	public Double getLarguraMaterial() {
		return this.largura;
	}

	@Transient	
	@Override
	public String getCorMaterial() {
		return null;
	}

	@Transient	
	@Override
	public String getGrupoMaterial() {
		return this.ref;
	}

	@Transient	
	@Override
	public String getTipoMaterial() {
		return "PAINEL";
	}
	
}
