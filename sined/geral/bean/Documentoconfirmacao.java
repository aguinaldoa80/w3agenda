package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentoconfirmacao",sequenceName="sq_documentoconfirmacao")
public class Documentoconfirmacao implements Log{

	protected Integer cddocumentoconfirmacao;
	protected Documento documento;
	protected Date data;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_documentoconfirmacao",strategy=GenerationType.AUTO)
	public Integer getCddocumentoconfirmacao() {
		return cddocumentoconfirmacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setCddocumentoconfirmacao(Integer cddocumentoconfirmacao) {
		this.cddocumentoconfirmacao = cddocumentoconfirmacao;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
	//Log
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
