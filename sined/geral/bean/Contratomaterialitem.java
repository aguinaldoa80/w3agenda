package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_contratomaterialitem", sequenceName = "sq_contratomaterialitem")
public class Contratomaterialitem {
	
	protected Integer cdcontratomaterialitem;
	protected Contratomaterial contratomaterial;
	protected Material material;
	protected Localarmazenagem localarmazenagem;
	protected Double qtde;
			
	// TRANSIENTE
	protected Double qtdedisponivel;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratomaterialitem")
	public Integer getCdcontratomaterialitem() {
		return cdcontratomaterialitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}

	public void setCdcontratomaterialitem(Integer cdcontratomaterialitem) {
		this.cdcontratomaterialitem = cdcontratomaterialitem;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	@Transient
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
	
}
