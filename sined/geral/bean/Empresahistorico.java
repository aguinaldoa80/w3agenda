package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Codigoregimetributario;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadesped;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceatividadespedpiscofins;
import br.com.linkcom.sined.geral.bean.enumeration.Indiceperfilsped;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_empresahistorico", sequenceName = "sq_empresahistorico")
public class Empresahistorico implements Log {

	protected Integer cdempresahistorico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Empresa empresa;
	protected String observacao;

	protected String nomefantasia;
	protected Integer proximonumnf;
	protected Integer proximonumnfproduto;

	protected Naturezaoperacao naturezaoperacao;
	protected Regimetributacao regimetributacao;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservico;
	protected Codigoregimetributario crt;
	protected Boolean tributacaomunicipiocliente;
	protected Boolean naopreencherdtsaida;
	protected String especienf;
	protected String marcanf;
	protected Boolean discriminarservicoqtdevalor;

	protected Indiceperfilsped indiceperfilsped;
	protected Indiceatividadesped indiceatividadesped;
	protected Indiceatividadespedpiscofins indiceatividadespedpiscofins;
	protected Uf ufsped;
	protected Municipio municipiosped;
	protected Boolean geracaospedpiscofins;
	protected Boolean geracaospedicmsipi;
	protected Fornecedor escritoriocontabilista;
	protected Fornecedor colaboradorcontabilista;
	protected String crccontabilista;
	protected Boolean calcularQuantidadeTransporte;


	protected Tipocobrancapis cstpisnfs;
	protected Tipocobrancacofins cstcofinsnfs;
	
	//Transientes
	private String responsavel;

	@Id
	@GeneratedValue(generator="sq_empresahistorico",strategy=GenerationType.AUTO)
	public Integer getCdempresahistorico() {
		return cdempresahistorico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Observa��o")
	@MaxLength(10000)
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Pr�ximo n�mero de nota fiscal de servi�o")
	public Integer getProximonumnf() {
		return proximonumnf;
	}

	@DisplayName("Pr�ximo n�mero de nota fiscal de produto")
	public Integer getProximonumnfproduto() {
		return proximonumnfproduto;
	}

	@Required
	@DescriptionProperty
	@DisplayName("Nome fantasia")
	public String getNomefantasia() {
		return nomefantasia;
	}

	@DisplayName("C�digo de regime tribut�rio")
	public Codigoregimetributario getCrt() {
		return crt;
	}

	@DisplayName("Perfil de apresenta��o do arquivo fiscal")
	public Indiceperfilsped getIndiceperfilsped() {
		return indiceperfilsped;
	}

	@DisplayName("Tipo de atividade Fiscal")
	public Indiceatividadesped getIndiceatividadesped() {
		return indiceatividadesped;
	}
	@DisplayName("Tipo de atividade EFD-Contribui��es")
	public Indiceatividadespedpiscofins getIndiceatividadespedpiscofins() {
		return indiceatividadespedpiscofins;
	}

	@DisplayName("Uf do domic�lio fiscal")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdufsped")
	public Uf getUfsped() {
		return ufsped;
	}

	@DisplayName("Munic�pio do domic�lio fiscal")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipiosped")
	public Municipio getMunicipiosped() {
		return municipiosped;
	}

	@DisplayName("Tributa��o no munic�pio do cliente")
	public Boolean getTributacaomunicipiocliente() {
		return tributacaomunicipiocliente;
	}

	@DisplayName("Marca")
	public String getMarcanf() {
		return marcanf;
	}
	
	@DisplayName("Esp�cie")
	public String getEspecienf() {
		return especienf;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdescritoriocontabil")
	@DisplayName("Escrit�rio Contabilista")
	public Fornecedor getEscritoriocontabilista() {
		return escritoriocontabilista;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabilista")
	@DisplayName("Contabilista")
	public Fornecedor getColaboradorcontabilista() {
		return colaboradorcontabilista;
	}

	@DisplayName("CRC")
	@MaxLength(15)
	public String getCrccontabilista() {
		return crccontabilista;
	}

	@DisplayName("Calcular quantidade para transporte")
	public Boolean getCalcularQuantidadeTransporte() {
		return calcularQuantidadeTransporte;
	}
	
	public void setMarcanf(String marcanf) {
		this.marcanf = marcanf;
	}
	
	public void setEspecienf(String especienf) {
		this.especienf = especienf;
	}

	public void setTributacaomunicipiocliente(Boolean tributacaomunicipiocliente) {
		this.tributacaomunicipiocliente = tributacaomunicipiocliente;
	}

	public void setIndiceperfilsped(Indiceperfilsped indiceperfilsped) {
		this.indiceperfilsped = indiceperfilsped;
	}

	public void setIndiceatividadesped(Indiceatividadesped indiceatividadesped) {
		this.indiceatividadesped = indiceatividadesped;
	}
	
	public void setIndiceatividadespedpiscofins(Indiceatividadespedpiscofins indiceatividadespedpiscofins) {
		this.indiceatividadespedpiscofins = indiceatividadespedpiscofins;
	}

	public void setUfsped(Uf ufsped) {
		this.ufsped = ufsped;
	}

	public void setMunicipiosped(Municipio municipiosped) {
		this.municipiosped = municipiosped;
	}

	public void setCrt(Codigoregimetributario crt) {
		this.crt = crt;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public void setProximonumnfproduto(Integer proximonumnfproduto) {
		this.proximonumnfproduto = proximonumnfproduto;
	}

	public void setProximonumnf(Integer proximonumnf) {
		this.proximonumnf = proximonumnf;
	}

	@DisplayName("Gera��o SPED ICMS/IPI")
	public Boolean getGeracaospedicmsipi() {
		return geracaospedicmsipi;
	}
	
	@DisplayName("Gera��o SPED PIS/COFINS")
	public Boolean getGeracaospedpiscofins() {
		return geracaospedpiscofins;
	}
	
	public void setGeracaospedicmsipi(Boolean geracaospedicmsipi) {
		this.geracaospedicmsipi = geracaospedicmsipi;
	}
	
	public void setGeracaospedpiscofins(Boolean geracaospedpiscofins) {
		this.geracaospedpiscofins = geracaospedpiscofins;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnaturezaoperacao")
	@DisplayName("Natureza de opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdregimetributacao")
	@DisplayName("Regime especial de tributa��o")
	public Regimetributacao getRegimetributacao() {
		return regimetributacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcodigotributacao")
	@DisplayName("C�digo de tributa��o")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cditemlistaservico")
	@DisplayName("Item da lista de servi�os")
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setRegimetributacao(Regimetributacao regimetributacao) {
		this.regimetributacao = regimetributacao;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}

	@DisplayName("C�d. Sit. Trib. PIS")
	public Tipocobrancapis getCstpisnfs() {
		return cstpisnfs;
	}

	@DisplayName("C�d. Sit. Trib. COFINS")
	public Tipocobrancacofins getCstcofinsnfs() {
		return cstcofinsnfs;
	}

	public void setCstpisnfs(Tipocobrancapis cstpisnfs) {
		this.cstpisnfs = cstpisnfs;
	}

	public void setCstcofinsnfs(Tipocobrancacofins cstcofinsnfs) {
		this.cstcofinsnfs = cstcofinsnfs;
	}

	@DisplayName("N�o preencher data de sa�da")
	public Boolean getNaopreencherdtsaida() {
		return naopreencherdtsaida;
	}

	public void setNaopreencherdtsaida(Boolean naopreencherdtsaida) {
		this.naopreencherdtsaida = naopreencherdtsaida;
	}
	
	@DisplayName("Discriminar servi�o, quantidade e valor")
	public Boolean getDiscriminarservicoqtdevalor() {
		return discriminarservicoqtdevalor;
	}

	public void setDiscriminarservicoqtdevalor(Boolean discriminarservicoqtdevalor) {
		this.discriminarservicoqtdevalor = discriminarservicoqtdevalor;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		
		if(cdusuarioaltera != null && responsavel == null)
			responsavel = TagFunctions.findUserByCd(cdusuarioaltera);
		return responsavel;
	}

	public void setCdempresahistorico(Integer cdempresahistorico) {
		this.cdempresahistorico = cdempresahistorico;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setEscritoriocontabilista(Fornecedor escritoriocontabilista) {
		this.escritoriocontabilista = escritoriocontabilista;
	}

	public void setColaboradorcontabilista(Fornecedor colaboradorcontabilista) {
		this.colaboradorcontabilista = colaboradorcontabilista;
	}

	public void setCrccontabilista(String crccontabilista) {
		this.crccontabilista = crccontabilista;
	}

	public void setCalcularQuantidadeTransporte(Boolean calcularQuantidadeTransporte) {
		this.calcularQuantidadeTransporte = calcularQuantidadeTransporte;
	}

}
