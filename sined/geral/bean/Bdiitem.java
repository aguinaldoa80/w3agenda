package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_bdiitem",sequenceName="sq_bdiitem")
public class Bdiitem implements Log {

	protected Integer cdbdiitem;
	protected Bdi bdi;
	protected Bdiitem bdiitempai;	
	protected String nome;
	protected Double valor;
	protected Contagerencial contagerencial;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_bdiitem", strategy=GenerationType.AUTO)	
	public Integer getCdbdiitem() {
		return cdbdiitem;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbdi")
	@DisplayName("Bdi")
	public Bdi getBdi() {
		return bdi;
	}
	
	@DisplayName("Item pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbdiitempai")	
	public Bdiitem getBdiitempai() {
		return bdiitempai;
	}

	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@Required
	public Double getValor() {
		return valor;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public void setCdbdiitem(Integer cdbdiitem) {
		this.cdbdiitem = cdbdiitem;
	}

	public void setBdi(Bdi bdi) {
		this.bdi = bdi;
	}

	public void setBdiitempai(Bdiitem bdiitempai) {
		this.bdiitempai = bdiitempai;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	/*************/
	/**** Log ****/
	/*************/
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bdiitem) {
			Bdiitem that = (Bdiitem) obj;
			return this.getCdbdiitem().equals(that.getCdbdiitem());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdbdiitem != null) {
			return cdbdiitem.hashCode();
		}
		return super.hashCode();		
	}

	
}
