package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_pedidovendahistorico", sequenceName = "sq_pedidovendahistorico")
public class Pedidovendahistorico implements Log{

	public static String APROVADO = "Aprovado";
	public static String VISUALIZADO = "Pedido de Venda do e-commerce marcado como Visualizado";
	
	protected Integer cdpedidovendahistorico;
	protected Pedidovenda pedidovenda;
	protected String acao;
	protected String observacao;
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
//	TRANSIENT
	protected String ids;	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendahistorico")
	@DisplayName("C�digo")
	public Integer getCdpedidovendahistorico() {
		return cdpedidovendahistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@DisplayName("A��o")
	@MaxLength(100)
	public String getAcao() {
		return acao;
	}
	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}
	public void setCdpedidovendahistorico(Integer cdpedidovendahistorico) {
		this.cdpedidovendahistorico = cdpedidovendahistorico;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	@Transient
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
}
