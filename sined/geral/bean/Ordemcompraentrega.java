package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_ordemcompraentrega", sequenceName = "sq_ordemcompraentrega")
public class Ordemcompraentrega {
	
	private Integer cdordemcompraentrega;
	private Ordemcompra ordemcompra;
	private Entrega entrega;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcompraentrega")
	public Integer getCdordemcompraentrega() {
		return cdordemcompraentrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setCdordemcompraentrega(Integer cdordemcompraentrega) {
		this.cdordemcompraentrega = cdordemcompraentrega;
	}
	
}