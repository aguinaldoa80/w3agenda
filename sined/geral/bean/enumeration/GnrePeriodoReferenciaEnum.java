package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GnrePeriodoReferenciaEnum {
	
	MENSAL		(0, "0 - Mensal"),
	QUINZENA_1	(1, "1 - 1� Quinzena"),
	QUINZENA_2	(2, "2 - 2� Quinzena"),
	DECENDIO_1	(3, "3 - 1� Dec�ndio;"),
	DECENDIO_2	(4, "4 - 2� Dec�ndio;"),
	DECENDIO_3	(5, "5 - 3� Dec�ndio;");

	private Integer value;
	private String nome;
	
	private GnrePeriodoReferenciaEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
