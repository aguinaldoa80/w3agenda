package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipotributacaoicms {
	
	NORMAL				(0,  "Tributação Normal"),
	SIMPLES_NACIONAL	(1,  "Simples Nacional");
	
	private Integer value;
	private String descricao;
	
	private Tipotributacaoicms(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
