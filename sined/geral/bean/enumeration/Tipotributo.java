package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipotributo {

	GPS							(0, "GPS"), 
	DARF_NORMAL					(1, "DARF NORMAL"),
	DARF_SIMPLES 				(2, "DARF SIMPLES"),
	GARE_SP 					(3, "GARE SP"),
	IPVA 						(4, "IPVA"),
	DPVAT 						(5, "DPVAT"),
	LICENCIAMENTO 				(6, "LICENCIAMENTO"),
	TRIBUTOS_COM_CODIGO_BARRAS 	(7, "Tributos com c�digo de barras"),
	FGTS 						(8, "FGTS"),
	; 
	
	private Integer value;
	private String descricao;
		
	private Tipotributo(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return getDescricao();
	}	
}
