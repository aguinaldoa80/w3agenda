package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import org.jaxen.function.ext.UpperFunction;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoVinculoExpedicao {
	
	PENDENTE(0,"!","Expedi��o pendente"),
	EXPEDI��O_PARCIAL(1,"EP","Expedi��o parcial"),
	EXPEDI��O_COMPLETA(2,"EC","Expedi��o completa");
	
	private Integer value;
	private String nome;
	private String descricao;
	
	private SituacaoVinculoExpedicao(Integer value, String nome,String descricao){
		this.value = value;
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return descricao;
	}

	
}
