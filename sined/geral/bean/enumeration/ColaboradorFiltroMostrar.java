package br.com.linkcom.sined.geral.bean.enumeration;

public enum ColaboradorFiltroMostrar {
	
	EXAMES_A_REALIZAR ("Exames a Realizar"),
	EXAMES_VENCIDOS ("Exames Vencidos"),
	EPI_VENCIDOS ("EPIs Vencidos");
	
	private String descricao;
	
	private ColaboradorFiltroMostrar(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
