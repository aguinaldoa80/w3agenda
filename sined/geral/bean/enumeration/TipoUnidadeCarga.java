package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoUnidadeCarga {

	CONTAINER	(0, "1", "1 - Container"),
	ULD			(1, "2", "2 - ULD"),
	PALLET		(2, "3", "3 - Pallet"),
	OUTROS		(3, "4", "4 - Outros");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private TipoUnidadeCarga(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
