package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum DominioTipointegracao {

	ENTRADA						(2,"Entrada"),
	SAIDA						(3,"Sa�da"),
	SERVICO						(4,"Servi�o"),
	LANCAMENTO_CONTABIL			(5,"Lan�amentos Cont�beis"),
	CADASTRO_FORNECEDOR_CLIENTE (6, "Pessoa"),
	COMPLETO 					(7, "Completo");
	
	private Integer value;
	private String nome;
	
	private DominioTipointegracao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
