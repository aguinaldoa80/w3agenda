package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Finalidadenfe {
	
	NORMAL			(0, 1, "NF-e normal"),
	COMPLEMENTAR	(1, 2, "NF-e complementar"),
	AJUSTE 			(2, 3, "NF-e de ajuste"),
	DEVOLUCAO		(3, 4, "Devolu��o de mercadoria");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private Finalidadenfe(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
