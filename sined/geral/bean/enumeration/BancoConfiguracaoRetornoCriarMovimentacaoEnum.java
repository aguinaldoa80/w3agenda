package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRetornoCriarMovimentacaoEnum {

	POR_REGISTRO("Por registro"),
	AGRUPADA("Agrupada"); 
	
	private String nome;
		
	private BancoConfiguracaoRetornoCriarMovimentacaoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
	
}
