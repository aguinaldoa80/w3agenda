package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GnreCodigoReceitaEnum {
	
	COD_100102	(0, "100102 - ICMS Consumidor Final N�o Contribuinte Outra UF por Opera��o", "100102"),
	COD_100099	(1, "100099 - ICMS Subst. Tribut�ria por Opera��o", "100099"),
	COD_100129 	(2, "100129 - ICMS Fundo Estadual de Combate � Pobreza por Opera��o", "100129"),
	COD_100080	(3, "100080 - ICMS Recolhimentos Especiais", "100080");

	private Integer value;
	private String nome;
	private String codigo;
	
	private GnreCodigoReceitaEnum(Integer _value, String _nome, String _codigo) {
		this.value = _value;
		this.nome = _nome;
		this.codigo = _codigo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
