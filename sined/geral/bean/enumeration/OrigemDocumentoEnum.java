package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrigemDocumentoEnum {

	PROCESSO_JUDICIAL 		(0, "Processo Judicial"),
	PROCESSO_ADMINISTRATIVO (1, "Processo Administrativo"),
	PER_DCOMP 				(2, "PER/DCOMP"),
	DOCUMENTO_FISCAL 		(3, "Documento Fiscal"),
	OUTROS 					(9, "Outros");
	
	private Integer value;
	private String descricao;
	
	private OrigemDocumentoEnum(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
