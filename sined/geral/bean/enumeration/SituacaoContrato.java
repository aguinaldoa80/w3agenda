package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoContrato {
	
	EM_ESPERA		(0,"Em Espera"),
	NORMAL			(1,"Normal"),
	A_CONSOLIDAR	(2,"A Consolidar"),
	ATENCAO			(3,"Aten��o"),
	ATRASADO		(4,"Atrasado"), 
	FINALIZADO		(5,"Finalizado"),
	CANCELADO		(6,"Cancelado"),
	SUSPENSO		(7,"Suspenso"),
	ISENTO			(8,"Isento");
	
	private Integer value;
	private String nome;
	
	private SituacaoContrato(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<SituacaoContrato> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoContrato s : SituacaoContrato.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoContrato){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
	public static String listAndConcatenateValue(List<SituacaoContrato> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoContrato s : SituacaoContrato.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoContrato){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.value)){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
