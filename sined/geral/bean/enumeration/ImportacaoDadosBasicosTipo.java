package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ImportacaoDadosBasicosTipo {
	
	CLIENTE 	("Cliente"),
	FORNECEDOR 	("Fornecedor"),
	MATERIAL 	("Material"),
	LEAD		("Lead"),
	;
	
	private String descricao;
	
	private ImportacaoDadosBasicosTipo(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
