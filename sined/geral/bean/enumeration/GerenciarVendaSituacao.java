package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.geral.bean.Vendasituacao;

public enum GerenciarVendaSituacao {

	ORCAMENTO	(1, "Or�amento"),
	FATURADA	(2, "Faturada"),
	EM_PRODUCAO (3, "Em Produ��o"),
	CANCELADA	(4, "Cancelada"),
	REALIZADA	(5, "Realizada"),
	AGUARDANDO_APROVACAO (6, "Aguardando aprova��o");
	
	private Integer value;
	private String nome;
	
	private GerenciarVendaSituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
		
	public static String listAndConcatenateDescription(List<Vendasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (GerenciarVendaSituacao s : GerenciarVendaSituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Vendasituacao){
					if((situacoes.get(i).getCdvendasituacao()) == s.getValue()){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
