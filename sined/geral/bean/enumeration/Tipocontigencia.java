package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocontigencia {

	SVRS (0, "Sefaz Virtual de Contingência - Rio Grande do Sul"), 
	SVAN (1, "Sefaz Virtual de Contingência - Ambiente Nacional"); 
	
	private Integer id;
	private String nome;
		
	private Tipocontigencia(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
