package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ContasFinanceiroSituacao {
	TODOS					(2,"Todos"),
	CONTA_EM_ABERTO			(0,"Conta em aberto"),
	CONTA_BAIXADA			(1,"Conta baixada");
	
	private Integer value;
	private String nome;
	
	
	private ContasFinanceiroSituacao(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}


	public Integer getValue() {
		return value;
	}


	public void setValue(Integer value) {
		this.value = value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}