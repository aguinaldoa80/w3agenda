package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tiponotareferencia {
	
	NF_E		(0, "Nota Fiscal Eletrônica (NF-e)"),
	NF			(1, "Nota Fiscal Modelo 1/1A"), 
	NF_PRODUTOR	(2, "Nota Fiscal de Produtor Rural"), 
	CT_E		(3, "Conhecimento de Transporte Eletrônico (CT-e)"),
	CUPOM_FISCAL(4, "Cupom Fiscal");
	
	private Integer value;
	private String descricao;
	
	private Tiponotareferencia(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
