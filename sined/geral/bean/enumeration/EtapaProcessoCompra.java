package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum EtapaProcessoCompra {
	
	COTACAO			("Cota��o"),
	ORDEM_COMRPA	("Ordem de compra"),
	RECEBIMENTO		("Recebimento");
	
	private String descricao;
	
	private EtapaProcessoCompra(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
