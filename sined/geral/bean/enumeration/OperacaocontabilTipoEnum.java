package br.com.linkcom.sined.geral.bean.enumeration;

public enum OperacaocontabilTipoEnum {
	
	UM_DEBITO_UM_CREDITO(0, "1 D�bito e 1 Cr�dito", MovimentacaocontabilTipoEnum.UM_DEBITO_UM_CREDITO),
	N_DEBITOS_UM_CREDITO(1, "N D�bitos e 1 Cr�dito", MovimentacaocontabilTipoEnum.N_DEBITOS_UM_CREDITO),
	UM_DEBITO_N_CREDITOS(2, "1 D�bito e N Cr�ditos", MovimentacaocontabilTipoEnum.UM_DEBITO_N_CREDITOS),
	N_DEBITOS_N_CREDITOS(3, "N D�bitos e N Cr�ditos", MovimentacaocontabilTipoEnum.N_DEBITOS_N_CREDITOS);
	
	private Integer id;
	private String nome;
	private MovimentacaocontabilTipoEnum movimentacaocontabilTipo;
	
	private OperacaocontabilTipoEnum(Integer id, String nome, MovimentacaocontabilTipoEnum movimentacaocontabilTipo){
		this.id = id;
		this.nome = nome;
		this.movimentacaocontabilTipo = movimentacaocontabilTipo;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public MovimentacaocontabilTipoEnum getMovimentacaocontabilTipo() {
		return movimentacaocontabilTipo;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}