package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Materialgrupocomissaopara {
	
	AGENCIA 	(0,"Ag�ncia"),
	COLABORADOR (1,"Colaborador");
	
	private Integer valor;
	private String descricao;
	
	private Materialgrupocomissaopara(Integer valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
}
