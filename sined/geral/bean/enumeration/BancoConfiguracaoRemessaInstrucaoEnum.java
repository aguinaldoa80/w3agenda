package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum BancoConfiguracaoRemessaInstrucaoEnum {

	ALTERACAO_DADOS_COBRANCA("Altera��o de dados da CR", 'C'), 
	PROTESTO("Protesto", 'C'),
	CANCELAR_PROTESTO("Cancelar protesto", 'C'),
	BAIXAR("Baixar", 'C'),
	PEDIDO_RESGISTRO("Pedido de registro (Entrada)", 'C'),
	
	ALTERACAO_DADOS_PAGAMENTO("Altera��o de dados", 'P'),
	PEDIDO_PAGAMENTO("Pedido de pagamento", 'P'),
	CANCELAR_PAGAMENTO("Cancelar pagamento", 'P'),
	DEMONSTRATIVO_PAGAMENTO("Demonstrativo de pagamento", 'P'),
	HOLERITE("Holerite", 'P'),
	
	CANCELAR_COBRANCA("Cancelar cobran�a", 'C');	
	
	private String nome;
	private Character tipo;
		
	private BancoConfiguracaoRemessaInstrucaoEnum(String nome, Character tipo){
		this.nome = nome;
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}
	
	public Character getTipo() {
		return tipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}
	
	public String toString() {
		return getNome();
	}	
	
	public static List<BancoConfiguracaoRemessaInstrucaoEnum> getListaInstrucaoByTipo(Character tipo){
		List<BancoConfiguracaoRemessaInstrucaoEnum> list = new ArrayList<BancoConfiguracaoRemessaInstrucaoEnum>();
		BancoConfiguracaoRemessaInstrucaoEnum[] values = BancoConfiguracaoRemessaInstrucaoEnum.values();		
		for (int i = 0; i < values.length; i++) {
			if(values[i].getTipo().equals(tipo))
				list.add(values[i]);
		}
		
		Collections.sort(list, new Comparator<BancoConfiguracaoRemessaInstrucaoEnum>() {
			@Override
			public int compare(BancoConfiguracaoRemessaInstrucaoEnum o1, BancoConfiguracaoRemessaInstrucaoEnum o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
		});
		
		return list;
	}
	
	public static List<BancoConfiguracaoRemessaInstrucaoEnum> getListaInstrucaoCobranca(){
		return getListaInstrucaoByTipo('C');
	}
	
	public static List<BancoConfiguracaoRemessaInstrucaoEnum> getListaInstrucaoPagamento(){
		return getListaInstrucaoByTipo('P');
	}
	
}
