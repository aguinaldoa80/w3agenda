package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoOperacaoEnum {

	ENTRADA	(0, "Entrada"), 
	SAIDA	(1, "Saida")
	; 
	
	private Integer value;
	private String descricao;
		
	private TipoOperacaoEnum(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}

	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
