package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipovalorreceber {

	CONTA_RECEBER	(0, "Conta a receber"),
	TAXA			(1, "Taxa");
	
	private Integer value;
	private String descricao;
	
	private Tipovalorreceber (Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
