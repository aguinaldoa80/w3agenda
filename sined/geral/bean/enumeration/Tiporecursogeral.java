package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tiporecursogeral {
	
	MATERIAL (0,"Material"),
	OUTRO	 (1,"Outro");
	
	private Integer id;
	private String desricao;
	
	private Tiporecursogeral(Integer id, String descricao){
		this.id = id;
		this.desricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setCdacao(Integer id) {
		this.id = id;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
