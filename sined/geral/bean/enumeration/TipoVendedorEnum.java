package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoVendedorEnum {

	RESPONSAVEL(0, "Responsável"),
	PRINCIPAL(1, "Principal");
	
	private Integer cdTipoVendedor;
	private String descricao;
	
	private TipoVendedorEnum() {}
	
	private TipoVendedorEnum(Integer cdTipoVendedor, String descricao){
		this.cdTipoVendedor = cdTipoVendedor;
		this.descricao = descricao;
	}
	
	public Integer getCdTipoVendedor() {
		return cdTipoVendedor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdTipoVendedor(Integer cdTipoVendedor) {
		this.cdTipoVendedor = cdTipoVendedor;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
