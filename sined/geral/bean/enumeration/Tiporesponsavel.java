package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tiporesponsavel {
	
	COLABORADOR		(0,"Colaborador"),
	AGENCIA			(1,"Ag�ncia");
	
	private Integer cdvendedortipo;
	private String descricao;	
	
	private Tiporesponsavel(Integer cdvendedortipo, String descricao) {
		this.cdvendedortipo = cdvendedortipo;
		this.descricao = descricao;
	}
	
	
	public Integer getCdvendedortipo() {
		return cdvendedortipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdvendedortipo(Integer cdvendedortipo) {
		this.cdvendedortipo = cdvendedortipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
	
	

}