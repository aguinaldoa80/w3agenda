package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GnreAcaoEnum {
	
	CRIADA				(0, "Criada"),
	ALTERADA			(1, "Alterada"),
	CONTA_PAGAR_GERADA	(2, "Conta a pagar gerada"),
	ESTORNADA			(3, "Estornada"),
	CANCELADA			(4, "Cancelada"),
	XML_GERADO			(5, "XML gerado"),
	CONTA_PAGAR_CANCELADA (6, "Conta a pagar cancelada"),
	;

	private Integer value;
	private String nome;
	
	private GnreAcaoEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
