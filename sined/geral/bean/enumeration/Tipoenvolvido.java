package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoenvolvido {
	
	CLIENTE 		(0, "Cliente"),
	PARTE_CONTRARIA (1, "Parte contrária");
	
	private Integer value;
	private String descricao;
	
	private Tipoenvolvido(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
