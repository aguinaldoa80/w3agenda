package br.com.linkcom.sined.geral.bean.enumeration;

public enum Garantiasituacao {

	DISPONIVEL("Disponível"),
	INCONFORME("Inconforme"),
	UTILIZADA("Utilizada"),
	CANCELADA("Cancelada");
	
	private String nome;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	private Garantiasituacao(String nome){
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.getNome();
	}
}
