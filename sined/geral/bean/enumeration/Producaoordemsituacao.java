package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Producaoordemsituacao {

	EM_ESPERA	(0, "Em espera"),
	EM_ANDAMENTO(1, "Em andamento"),
	CONCLUIDA	(2, "Conclu�da"),
	CANCELADA 	(3, "Cancelada"),
	MATERIA_PRIMA_PENDENTE(4, "Baixa de mat�ria-prima pendente");

	private Integer value;
	private String nome;
	
	private Producaoordemsituacao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Producaoordemsituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Producaoordemsituacao situacao : Producaoordemsituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Producaoordemsituacao){
					if((situacoes.get(i)).equals(situacao)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(situacao.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(situacao.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
	
}
