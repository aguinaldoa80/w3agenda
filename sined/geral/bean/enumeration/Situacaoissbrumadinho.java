package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaoissbrumadinho {
	
	TRIBUTADA		(0, "T", "Tributada"),
	RETIDA			(1, "R", "Retida"),
	CANCELADA		(2, "C", "Cancelada"),
	ISENTA			(3, "I", "Isenta"),
	NAO_TRIBUTADA	(4, "N", "N�o Tributada"),
	IMUNE			(5, "U", "Imune");
	
	private Integer value;
	private String simbolo;
	private String descricao;
	
	private Situacaoissbrumadinho(Integer value, String simbolo, String descricao){
		this.value = value;
		this.descricao = descricao;
		this.simbolo = simbolo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getSimbolo() {
		return simbolo;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
