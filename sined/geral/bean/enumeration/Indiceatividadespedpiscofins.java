package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indiceatividadespedpiscofins {
	
	INDUSTRIAL				(0, "Industrial ou equiparado a industrial"),
	PRESTADOR_SERVICO		(1, "Prestador de servi�os"),
	ATIVIDADE_ECONOMICA		(2, "Atividade de com�rcio"),
	PESSOA_JURIDICA			(3, "Pessoas jur�dicas referidas nos �� 6�, 8� e 9� do a rt. 3� da Lei n� 9.718, de 1998"),
	ATIVIDADE_IMOBILIARIA	(4, "Atividade imobili�ria"),
	OUTROS					(9, "Outros");
	
	private Integer value;
	private String descricao;
	
	private Indiceatividadespedpiscofins(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
