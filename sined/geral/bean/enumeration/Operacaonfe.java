package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Operacaonfe {
	
	NORMAL			(0, "Normal", "N�o"),
	CONSUMIDOR_FINAL(1, "Consumidor final", "Sim");
	
	private Integer value;
	private String descricao;
	private String descricaoConsumidorfinal;
	
	private Operacaonfe(Integer value, String descricao, String descricaoConsumidorfinal){
		this.value = value;
		this.descricao = descricao;
		this.descricaoConsumidorfinal = descricaoConsumidorfinal;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@DescriptionProperty
	public String getDescricaoConsumidorfinal() {
		return descricaoConsumidorfinal;
	}
	
	@Override
	public String toString() {
		return getDescricaoConsumidorfinal();
	}
	

}
