package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoDataReferenciaContrato {

	DATAINICIO("Data de in�cio"),
	DATAASSINATURA("Data de assinatura"),
	DATARENOVACAO("Data de renova��o"),
	DATAFINALIZACAO("Data de finaliza��o"),
	PROXIMOVENCIMENTO("Pr�ximo vencimento");
	
	private String nome;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	private TipoDataReferenciaContrato(String nome){
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
