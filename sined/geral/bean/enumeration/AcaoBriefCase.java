package br.com.linkcom.sined.geral.bean.enumeration;

public enum AcaoBriefCase {
	
	CADASTRAR			(1,"Cadastrar"),
	ALTERAR				(2,"Alterar"),
	REMOVER				(3,"Remover"),
	ATUALIZAR			(4,"Atualizar"),
	LISTAR				(5,"Listar"),
	LIMPAR				(6,"Limpar"),
	ATIVAR				(7,"Ativar"),
	DESATIVAR			(8,"Desativar"),
	REINICIAR			(9,"Reiniciar");
	
	private Integer cdacao;
	private String desricao;
	
	private AcaoBriefCase(Integer cdacao, String descricao){
		this.cdacao = cdacao;
		this.desricao = descricao;
	}
	
	public Integer getCdacao() {
		return cdacao;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setCdacao(Integer cdacao) {
		this.cdacao = cdacao;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
