package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MdfeTipoTransporte {

	RODOVIARIO_TRACAO	(0, "1", "1 - Rodovi�rio Tra��o"),
	RODOVIARIO_REBOQUE	(1, "2", "2 - Rodovi�rio Reboque"),
	NAVIO				(2, "3", "3 - Navio"),
	BALSA				(3, "4", "4 - Balsa"),
	AERONAVE			(4, "5", "5 - Aeronave"),
	VAGAO				(5, "6", "6 - Vag�o"),
	OUTRO				(6, "7", "7 - Outro");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private MdfeTipoTransporte(Integer _value, String _cdnfe, String _nome) {
		this.cdnfe = _cdnfe;
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
