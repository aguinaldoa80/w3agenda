package br.com.linkcom.sined.geral.bean.enumeration;


public enum TipoCriacaoPedidovenda {
	
	PEDIDOVENDA,
	VENDA,
	PEDIDOVENDA_BONIFICACAO,
	ORCAMENTO
}
