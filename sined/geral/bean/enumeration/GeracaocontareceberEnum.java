package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GeracaocontareceberEnum {
	
	APOS_EMISSAONOTA			(0, "Ap�s emiss�o de nota"),
	APOS_VENDAREALIZADA			(1, "Ap�s venda realizada"),
	APOS_PEDIDOVENDAREALIZADA	(2, "Ap�s pedido realizado"),
	NAO_GERAR_CONTA				(3, "N�o gerar conta");;

	private Integer value;
	private String nome;
	
	private GeracaocontareceberEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}

