package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum IndicadorSituacaoEspecial {
	NORMAL(0, "Normal"),
	FALECIMENTO(1, "Falecimento"),
	ESPOLIO(2, "Esp�lio"),
	SAIDA_DEFINITIVA_DO_PAIS(3, "Sa�da definitiva do Pa�s");
	
	private Integer id;
	private String descricao;
	
	private IndicadorSituacaoEspecial(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
