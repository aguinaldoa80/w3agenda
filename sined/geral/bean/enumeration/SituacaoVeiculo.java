package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoVeiculo {
	
	PARADO(0,"Parado"),
	EM_TRANSITO(1,"Em tr�nsito"),
	INATIVO(2,"Inativo");
	
	private Integer value;
	private String nome;
	
	private SituacaoVeiculo(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
