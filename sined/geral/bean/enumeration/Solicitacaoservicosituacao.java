package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Solicitacaoservicosituacao {
	
	PENDENTE	(0, "Pendente"),
	EM_ANDAMENTO(1, "Em andamento"),
	CONCLUIDA	(2, "Conclu�da"),
	CANCELADA	(3, "Cancelada");
	
	private Integer value;
	private String nome;
	
	private Solicitacaoservicosituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Solicitacaoservicosituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		if(situacoes != null){
		for (int i = 0; i < situacoes.size(); i++)
			for (Solicitacaoservicosituacao s : Solicitacaoservicosituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Solicitacaoservicosituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		}
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
	public static String listAndConcatenateDescription(List<Solicitacaoservicosituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Solicitacaoservicosituacao s : Solicitacaoservicosituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Solicitacaoservicosituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
	public static List<Solicitacaoservicosituacao> listaSituacaoFiltro(){
		List<Solicitacaoservicosituacao> lista = new ArrayList<Solicitacaoservicosituacao>();
		lista.add(PENDENTE);
		lista.add(EM_ANDAMENTO);
		lista.add(CONCLUIDA);
		lista.add(CANCELADA);
		return lista;
	}
}
