package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoArquivoIntegracaoBridgestone {
	PRODUTO			(0,"Produto"),
	SERVICO				(1,"Servi�o");
	
	private Integer value;
	private String desricao;
	
	private TipoArquivoIntegracaoBridgestone(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
