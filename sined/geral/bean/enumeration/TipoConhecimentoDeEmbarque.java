package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoConhecimentoDeEmbarque {

	AWB 			(1,"AWB","01"),
	MAWB 			(2,"MAWB","02"),
	HAWB			(3,"HAWB","03"),
	COMAT 			(4,"COMAT","04"),
	R_EXPRESSAS 	(6,"R. EXPRESSAS", "06"),
	ETIQ_REXPRESSAS (7,"ETIQ. REXPRESSAS","07"),
	HR_EXPRESSAS 	(8,"HR. EXPRESSAS","08"),
	AV7 			(9,"AV7","09"),
	BL 				(10,"BL","10"),
	HBL 			(12,"HBL","12"),
	CRT 			(13,"CRT","13"),
	DSIC 			(14,"DSIC","14"),
	COMAT_BL		(16,"COMAT BL","16"),
	RWB 			(17,"RWB","17"),
	HRWB 			(18,"HRWB","18"),
	TIF_DTA 		(19,"TIF/DTA","19"),
	CP2 			(20,"CP2","20"),
	NAO_IATA 		(91,"N�O IATA","91"),
	MNAO_IATA		(92 ,"MNAO IATA","92"),
	HNAO_IATA		(93 ,"HNAO IATA","93"),
	OUTROS    		(99 ,"OUTROS","99");

	private Integer id;
	private String nome;
	private String descricao;

	private TipoConhecimentoDeEmbarque(Integer id, String nome,String descricao) {
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String toString() {
		return getNome();
	}
	public String getDescricao() {
		return descricao;
	}
	
	
}
