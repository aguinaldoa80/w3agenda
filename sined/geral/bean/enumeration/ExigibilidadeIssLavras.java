package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ExigibilidadeIssLavras {
	
	EXIGIVEL				(0, "Exigivel"),
	NAO_INCIDENCIA			(1, "N�o Incidencia"),
	ISENCAO					(2, "Isen��o"),
	EXPORTACAO				(3, "Exporta��o"),
	IMUNIDADE				(4, "Imunidade"),
	SUSPENSA_JUDICIAL		(5, "Exigibilidade Suspensa por Decis�o Judicial"),
	SUSPENSA_ADMINISTRATIVO	(6, "Exigibilidade Suspensa por Processo Administrativo");
	
	private Integer value;
	private String nome;
	
	private ExigibilidadeIssLavras(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
