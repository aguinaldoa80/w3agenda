package br.com.linkcom.sined.geral.bean.enumeration;

public enum EmitirautorizacaoReportFiltroMostrar {
	
	EXAMES_A_REALIZAR ("Exames a Realizar"),
	EXAMES_VENCIDOS ("Exames Vencidos");
	
	private String descricao;
	
	private EmitirautorizacaoReportFiltroMostrar(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
