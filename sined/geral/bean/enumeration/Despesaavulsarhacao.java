package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Despesaavulsarhacao {
	
	CRIADA			("Criada"),		// 0 
	AUTORIZADA		("Autorizada"), // 1
	ALTERADA		("Alterada"), 	// 2
	ESTORNADA		("Estornada"), 	// 3
	PROCESSADA		("Processada"), // 4
	CANCELADA		("Cancelada");	// 5
	
	private String nome;
	
	private Despesaavulsarhacao(String nome){
		this.nome = nome;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<Despesaavulsarhacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Despesaavulsarhacao s : Despesaavulsarhacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Despesaavulsarhacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
