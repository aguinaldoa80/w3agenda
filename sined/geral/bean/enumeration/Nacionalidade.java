package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Nacionalidade {

	BRASILEIRA					(10, "Brasileira"), 				
	NATURALIZADO_BRASILEIRA		(20, "Naturalizado Brasileiro"), 	
	ARGENTINA					(21, "Argentina"), 					
	BOLIVIANA					(22, "Boliviana"), 					
	CHILENA						(23, "Chilena"), 					
	PARAGUAIA					(24, "Paraguaia"), 					
	URUGUAIA					(25, "Uruguaia"),					
	ALEMA						(30, "Alem�"), 						
	BELGA						(31, "Belga"), 						
	BRITANICA					(32, "Brit�nica"), 						
	CANADENSE					(34, "Canadense"), 						
	ESPANHOL					(35, "Espanhol"), 						
	NORTE_AMERICANA_USA			(36, "Norte Americana (EUA)"), 						
	FRANCESA					(37, "Francesa"),				
	SUICA						(38, "Su��a"),				
	ITALIANA					(39, "Italiana"),				
	JAPONESA					(41, "Japonesa"),				
	CHINESA						(42, "Chinesa"),				
	COREANA						(43, "Coreana"),				
	PORTUGUESA					(45, "Portuguesa"),				
	LATINO_AMERICANO			(48, "Outros Latino-Americanos"),				
	ASIATICO					(49, "Outros Asi�ticos"),				
	OUTROS						(50, "Outras");
	
	private Nacionalidade(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	private Integer value;
	private String descricao;
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
