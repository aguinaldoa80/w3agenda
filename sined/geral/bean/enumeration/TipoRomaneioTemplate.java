package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Tipo de Template de Impress�o do Romaneio")
public enum TipoRomaneioTemplate {

	ROMANEIO_PADRAO(1, "Romaneio Padr�o"),
	ROMANEIO_SUBSTITUICAO(2, "Romaneio Substitui��o");
	
	private Integer codigo;
	private String descricao;
	
	private TipoRomaneioTemplate(Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<TipoRomaneioTemplate> getListaTipoRomaneioTemplate(){
		List<TipoRomaneioTemplate> lista = new ArrayList<TipoRomaneioTemplate>();
		for (int i = 0; i < TipoRomaneioTemplate.values().length; i++) {
			lista.add(TipoRomaneioTemplate.values()[i]);
		}
		return lista;
	}
}