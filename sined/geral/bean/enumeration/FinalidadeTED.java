package br.com.linkcom.sined.geral.bean.enumeration;

public enum FinalidadeTED {
	
	PAGAMENTO_IMPOSTOS_TRIBUTOS("00001", "Pagamento de Impostos, Tributos e Taxas"),
	PAGAMENTO_CONCESSIONARIA_SERVICO_PUBLICO("00002", "Pagamento a Concession�rias de Servi�o P�blico"),
	PAGAMENTO_DIVIDENDOS("00003", "Pagamento de Dividendos"),
	PAGAMENTO_SALARIOS("00004", "Pagamento de Sal�rios"),
	PAGAMENTO_FORNECEDORES("00005", "Pagamento de Fornecedores"),
	PAGAMENTO_HONORARIOS("00006", "Pagamento de Honor�rios"),
	PAGAMENTO_ALUGUEIS("00007", "Pagamento de Alugu�is e Taxas e Condom�nio"),
	PAGAMENTO_DUPLICATAS("00008", "Pagamento de Duplicatas e T�tulos"),
	PAGAMENTO_DE_HONORARIOS("00009", "Pagamento de Honor�rios"),
	CREDITO_CONTA("00010", "Cr�dito em Conta"),
	PAGAMENTO_PAGAMENTO_CORRETORA("00011", "Pagamento a Corretoras"),
	CREDITO_CONTA_INVESTIMENTO("00016", "Cr�dito em Conta Investimento"),
	DEPOSITO_JUDICIAL("00100", "Dep�sito Judicial"),
	PENSAO_ALIMENTICIA("00101", "Pens�o Aliment�cia"),
	TRANSFERENCIA_INTERNACIONAL("00200", "Transfer�ncia Internacional de Reais"),
	AJUSTE_MERCADO_FUTURO("00201", "Ajuste Posi��o Mercado Futuro"),
	REPASSE_VALORES_BNDES("00202", "Repasse de valores do BNDES"),
	LIQUIDACAO_BNDES("00203", "Liquida��o de compromisso com BNDES"),
	COMRA_VENDA_ACOES("00204", "Compra/Venda de A��es - Bolsas de Valores e Mercado de Balc�o"),
	CONTRATO_REFERENCIADO("00205", "Contrato referenciado em A��es/�ndices de A��es - BV/BMF"),
	RESTITUICAO_IR("00300", "Restitui��o de Imposto de Renda"),
	RESTITUICAO_SEGUROS("00500", "Restitui��o de Pr�mio de Seguros"),
	PAGAMENTO_INDENIZACAO_SINISTRO("00501", "Pagamento de indeniza��o de Sinistro de Seguro"),
	PAGAMENTO_PREMIO("00502", "Pagamento de Pr�mio de Co-seguro"),
	RESTITUICAO_CO_SEGURO("00503", "Restitui��o de pr�mio de Co-seguro"),
	PAGAMENTO_INDENIZACAO_CO_SEGURO("00504", "Pagamento de indeniza��o de Co-seguro"),
	PAGAMENTO_PREMIO_RESSEGURO("00505", "Pagamento de pr�mio de Resseguro"),
	RESTITUICAO_PREMIO_RESSEGURO("00506", "Restitui��o de pr�mio de Resseguro"),
	PAGAMENTO_SINISTRO_RESSEGURO("00507", "Pagamento de Indeniza��o de Sinistro de Resseguro"),
	RESTITUICAO_SINISTRO_RESSEGURO("00508", "Restitui��o de Indeniza��o de Sinistro de Resseguro"),
	PAGAMENTO_DESPESAS_SINISTRO("00509", "Pagamento de Despesas com Sinistros"),
	PAGAMENTO_VISTORIA_PREVIA("00510", "Pagamento de Inspe��es/Vistorias Pr�vias"),
	PAGAMENTO_RESGATE_TITULO_CAPITALIZACAO("00511", "Pagamento de Resgate de T�tulo de Capitaliza��o"),
	PAGAMENTO_SORTEIO_TITULO_CAPITALIZACAO("00512", "Pagamento de Sorteio de T�tulo de Capitaliza��o"),
	PAGAMENTO_DEVOLUCAO_TITULO_CAPITALIZACAO("00513", "Pagamento de Devolu��o de Mensalidade de T�tulo de Capitaliza��o"),
	RESTITUICAO_CONTRIBUICAO_PREVIDENCIARIO("00514", "Restitui��o de Contribui��o de Plano Previdenci�rio"),
	PAGAMENTO_PREVIDENCIARIO_PECULIO("00515", "Pagamento de Benef�cio Previdenci�rio de Pec�lio"),
	PAGAMENTO_PREVIDENCIARIO_PENSAO("00516", "Pagamento de Benef�cio Previdenci�rio de Pens�o"),
	PAGAMENTO_PREVIDENCIARIO_APOSENTADORIA("00517", "Pagamento de Benef�cio Previdenci�rio de Aposentadoria"),
	PAGAMENTO_RESGATE_PREVIDENCIA("00518", "Pagamento de Resgate Previdenci�rio"),
	PAGAMENTO_COMISSAO_CORRETAGEM("00519", "Pagamento de Comiss�o de Corretagem"),
	PAGAMENTO_TRANSFERENCIA_PORTABILIDADE("00520", "Pagamento de Transfer�ncias/Portabilidade de Reserva de Seguro/Previd�ncia"),
	OUTROS("99999", "Outros");
	
	
	private String codigo;
	private String descricao;
	
	private FinalidadeTED(String codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	@Override
	public String toString() {
		return descricao;
	}

}
