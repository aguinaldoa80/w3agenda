package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoTributacaoIssETransparencia {
	
	TRIBUTADO		(1, "Tributado. O Servi�o � tributado de acordo com a al�quota ISS da lista de servi�os da prefeitura"),
	ISENCAO			(2, "Isen��o / Imunidade. Neste caso a al�quota de ISS � igual a zero"),
	SUSPENSAO		(3, "Suspens�o. Se a tributa��o est� em decis�o judicial, ser� acatada a al�quota de ISS informada no RPS, podendo esta ser igual a zero"),
	SIMPLES			(4, "Simples Nacional. O servi�o � tributado de acordo com a al�quota do contribuinte no Simples Nacional"),
	ISS_FIXO		(5, "ISS Fixo; Neste caso a al�quota de ISS a enviar deve ser igual a zero"),
	ISENCAO_PARCIAL	(6, "Isen��o parcial. O servi�o � tributado de acordo com a Al�quota no Cadastro do contribuinte. Esta n�o � a al�quota do simples nacional");
	
	private Integer value;
	private String nome;
	
	private TipoTributacaoIssETransparencia(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
