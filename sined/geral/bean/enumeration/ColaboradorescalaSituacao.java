package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ColaboradorescalaSituacao {
	
	TRABALHO(0,"Trabalho"),
	FOLGA(1,"Folga"),
	FALTA(2,"Falta"),
	SUBSTITUTO(3,"Substituto");
	
	private Integer value;
	private String nome;
	
	private ColaboradorescalaSituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
