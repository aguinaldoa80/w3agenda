package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum PagamentoTEFSituacao {
	
	EM_ANDAMENTO(0, "Em andamento"),
	RECUSADO	(1, "Recusado"),
	REALIZADO	(2, "Realizado");
	
	private Integer value;
	private String descricao;
	
	private PagamentoTEFSituacao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static String listAndConcatenate(List<PagamentoTEFSituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (PagamentoTEFSituacao s : PagamentoTEFSituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof PagamentoTEFSituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}

}
