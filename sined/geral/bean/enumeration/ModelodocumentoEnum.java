package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ModelodocumentoEnum {
	
	DAE("DAE - Documento de arrecadação estadual"),							
	GNRE("GNRE - Guia Nacional de Recolhimento de Tributos Estaduais"),
	DAM("DAM - Documento de arrecadação municipal"),
	DARF("DARF - Documento de arrecadação da receita federal");
	
	
	private String modelo;
	
	private ModelodocumentoEnum(String modelo){
		this.modelo = modelo;
	}

	@DescriptionProperty
	public String getModelo() {
		return modelo;
	}
	
	@Override
	public String toString() {
		return getModelo();
	}
}
