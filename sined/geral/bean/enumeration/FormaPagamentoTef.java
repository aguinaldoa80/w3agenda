package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaPagamentoTef {
	
	TEF_OUTROS	(24, "TEF - Perguntar no pinpad"),
	TEF_CREDITO	(21, "TEF - Cr�dito"),
	TEF_DEBITO	(22, "TEF - D�bito"),
//	TEF_VOUCHER	(23, "TEF - Voucher"),
	;

	private Integer value;
	private String nome;
	private String imagem;
	
	private FormaPagamentoTef(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getImagem() {
		return imagem;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
