package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoconfiguracaonfe {
	
	NOTA_FISCAL_SERVICO    (0, "Nota Fiscal de Serviço"),
	NOTA_FISCAL_PRODUTO    (1, "Nota Fiscal de Produto"),
	MDFE			       (2, "Manifesto Eletrônico de Documentos Fiscais - MDF-e"),
	DDFE				   (3, "Distribuição de Documento Fiscal Eletrônico"),
	MD				       (4, "Manifestação do Destinatário"),
	NOTA_FISCAL_CONSUMIDOR (5, "Nota Fiscal de Consumidor");
	
	private Integer value;
	private String descricao;
	
	private Tipoconfiguracaonfe(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}