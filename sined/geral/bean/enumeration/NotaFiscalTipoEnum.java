package br.com.linkcom.sined.geral.bean.enumeration;

public enum NotaFiscalTipoEnum {
	
	ABATIMENTO(0,"Abater no valor bruto da nota"),
	DESCONTO_INCONDICIONADO(1,"Desconto incondicionado"),
	DESCONTO_CONDICIONADO(2,"Desconto condicionado"),
	DEDUCAO(3,"Dedu��o"),
	OUTRAS_RETENCOES(4,"Outras reten��es");
	
	private Integer idNota;
	private String descricao;
	
	private NotaFiscalTipoEnum(Integer _idNota, String _descricao) {
		this.idNota = _idNota;
		this.descricao = _descricao;
	}

	public Integer getIdNota() {
		return idNota;
	}

	public void setIdNota(Integer idNota) {
		this.idNota = idNota;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return getDescricao().toUpperCase();
	}
}
