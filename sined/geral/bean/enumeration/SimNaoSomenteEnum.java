package br.com.linkcom.sined.geral.bean.enumeration;

public enum SimNaoSomenteEnum {
	
	SIM 	("Sim"),
	SOMENTE ("Somente"),
	NAO 	("N�o");
	
	private String descricao;
	
	private SimNaoSomenteEnum(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
