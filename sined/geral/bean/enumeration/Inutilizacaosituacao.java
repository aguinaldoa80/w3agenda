package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Inutilizacaosituacao {
	
	GERADO					(0, "Gerado"),
	ENVIANDO				(1, "Enviando"),
	PROCESSADO_COM_ERRO		(2, "Processado com erro"),
	PROCESSADO_COM_SUCESSO	(3, "Processado com sucesso");
	
	private Integer value;
	private String nome;
	
	private Inutilizacaosituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Inutilizacaosituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Inutilizacaosituacao s : Inutilizacaosituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Inutilizacaosituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
