package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivofolhasituacao {
	
	EM_ABERTO	(0, "Em aberto"),
	AUTORIZADO	(1, "Autorizado"),
	PROCESSADO	(2, "Processado"),
	CANCELADO	(3, "Cancelado");
	
	private Integer value;
	private String descricao;
	
	private Arquivofolhasituacao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static String listAndConcatenate(List<Arquivofolhasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Arquivofolhasituacao s : Arquivofolhasituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Arquivofolhasituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}

}
