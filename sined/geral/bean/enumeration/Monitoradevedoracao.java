package br.com.linkcom.sined.geral.bean.enumeration;

	public enum Monitoradevedoracao {
	
	DESBLOQUEAR			(0,"Desbloquear"),
	BLOQUEAR_PARCIAL	(1,"Bloquear Parcial"),
	BLOQUEAR_TOTAL		(2,"Bloquear total");
	
	private Integer cdmonitoraclienteacao;
	private String descricao;
	
	private Monitoradevedoracao(Integer cdmonitoraclienteacao, String descricao){
		this.cdmonitoraclienteacao = cdmonitoraclienteacao;
		this.descricao = descricao;
	}
	
	public Integer getCdmonitoraclienteacao() {
		return cdmonitoraclienteacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdmonitoraclienteacao(Integer cdmonitoraclienteacao) {
		this.cdmonitoraclienteacao = cdmonitoraclienteacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao().toUpperCase();
	}
}