package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ModalidadefreteMdfe {

	RODOVIARIO(1, "1 - Rodovi�rio"),
	AEREO(2, "2 - A�reo"),
	AQUAVIARIO(3, "3 - Aquavi�rio"),
	FERROVIARIO(4, "4 - Ferrovi�rio");
	
	private Integer value;
	private String nome;
	
	private ModalidadefreteMdfe(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
