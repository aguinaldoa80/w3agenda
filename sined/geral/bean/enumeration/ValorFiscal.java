package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ValorFiscal {
	
	OPERACOES_COM_CREDITO_IMPOSTO(1, "OPERA��ES COM CR�DITO DO IMPOSTO"),
	OPERACOES_SEM_CREDITO_IMPOSTO_ISENTAS_NAO_TRIBUTADAS(2, "OPERA��ES SEM CR�DITO DO IMPOSTO - ISENTAS/N�O TRIBUTADAS"),
	OPERACOES_SEM_CREDITO_IMPOSTO_OUTRAS(3, "OPERA��ES SEM CR�DITO DO IMPOSTO - OUTRAS");
	
	private Integer codigo;
	private String descricao;
	
	private ValorFiscal(Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}