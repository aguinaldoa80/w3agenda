package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrigemMovimentacaoEstoqueEnum {

	CONSUMO_MATERIAL			(0, "Consumo de Material"),
	RECEBIMENTO_MATERIAL		(1, "Recebimento de Material"),
	REQUISICAO_MATERIAL			(2, "Requisi��o de Material"),
	ROMANEIO_MATERIAL			(3, "Romaneio de material"),
	VENDA_MATERIAL				(4, "Venda de Material"),
	ORDEM_PRODUCAO_ENTRADA		(5, "Ordem de produ��o (Entrada)"),
	COLETA						(6, "Coleta"),
	ORDEM_SERVICO_VETERINARIA	(7, "Ordem de servi�o de veterin�ria"),
	ORDEM_PRODUCAO_SAIDA		(8, "Ordem de produ��o (Saida)"),
	NOTA_FISCAL					(9, "Nota fiscal"),
	REQUISICAO					(10, "Requisi��o"),
	AVULSO						(11, "Avulso"),
	EXPEDICAO_AVULSA			(12, "Expedi��o Avulsa");

	private Integer value;
	private String nome;
	private OrigemMovimentacaoEstoqueEnum(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getNome() {
		return nome;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
