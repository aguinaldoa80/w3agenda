package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipooperacaonota {
	
	ENTRADA	(0, "Entrada"),
	SAIDA	(1, "Sa�da");
	
	private Integer value;
	private String descricao;
	
	private Tipooperacaonota(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
