package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ProducaoagendaFiltroMostrar {
	
	PENDENCIA_FINANCEIRA	(0, "Pend�ncia financeira"),
	COMPRA_SOLICITADA		(1, "Compra solicitada"),
	EXPEDICAO_PENDENTE		(2, "Expedi��o pendente"),
	CONTRATO_CANCELADO		(3, "Contrato cancelado"),
	;
	
	private Integer value;
	private String nome;
	
	private ProducaoagendaFiltroMostrar(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
