package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Presencacompradornfe {
	
	NAO_SE_APLICA	(0, 0, "N�o se aplica (por exemplo, para a Nota Fiscal complementar ou de ajuste)", false),
	PRESENCIAL		(1, 1, "Presencial", true),
	INTERNET 		(2, 2, "N�o presencial, pela Internet", true),
	TELEATENDIMENTO	(3, 3, "N�o presencial, Teleatendimento", true),
	OUTROS	 		(4, 9, "N�o presencial, outros", true),
	PRESENCIAL_FORA	(5, 5, "Presencial, fora do estabelecimento;", true),
	NFCE_ENTREGA	(6, 4, "NFC-e em opera��o com entrega a domic�lio.", false);
	
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	private Boolean venda;
	
	private Presencacompradornfe(Integer value, Integer cdnfe, String descricao, Boolean venda){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
		this.venda = venda;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Boolean getVenda() {
		return venda;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Presencacompradornfe> getListaPresencacompradornfeForVenda(){
		List<Presencacompradornfe> lista = new ArrayList<Presencacompradornfe>();
		Presencacompradornfe[] values = Presencacompradornfe.values();
		for (Presencacompradornfe presencacompradornfe : values) {
			if(presencacompradornfe.getVenda()){
				lista.add(presencacompradornfe);
			}
		}
		return lista;
	}
	
	public static Presencacompradornfe getByCdnfe(Integer cdnfe){
		if(cdnfe == null) return null;
		Presencacompradornfe[] values = Presencacompradornfe.values();
		for (Presencacompradornfe presencacompradornfe : values) {
			if(cdnfe.equals(presencacompradornfe.getCdnfe())) return presencacompradornfe;
		}
		return null;
	}

}
