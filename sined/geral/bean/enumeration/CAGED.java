package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum CAGED {

	ADMISSAO_PRIMEIRO			("Admiss�o de empregado no 1� emprego", 10), 				
	ADMISSAO_ANTERIOR			("Admiss�o de empregado com emprego anterior", 20), 	
	PRAZO_DETERMINADO			("Contrato por prazo determinado", 25), 					
	DISPENSA_SEM_CAUSA			("Dispensa sem justa causa", 31), 					
	DISPENSA_POR_CAUSA			("Dispensa por justa causa", 32), 					
	REINTEGRACAO				("Reintegra��o", 35), 					
	DESLIGAMENTO				("Desligamento por iniciativa pr�pria (espont�neo)", 40),					
	TERMINO_DETERMINADO			("T�rmino de Contrato por prazo determinado", 43), 						
	TERMINO						("T�rmino de Contrato", 45), 						
	DESLIGAMENTO_APOSENTADORIA	("Desligamento por aposentadoria", 50), 						
	DESLIGAMENTO_MORTE			("Desligamento por morte", 60), 						
	TRANSFERENCIA_ENTRADA		("Transfer�ncia de entrada", 70), 						
	TRANSFERENCIA_SAIDA			("Transfer�ncia de sa�da", 80); 						
	
	private CAGED(String descricao, Integer value){
		this.value = value;
		this.descricao = descricao;
	}
	
	private Integer value;
	private String descricao;
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
