package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Notaerrocorrecaotipo {
	
	NOTA_FISCAL_PRODUTO			(0, "Nota fiscal de produto"),
	NOTA_FISCAL_SERVICO			(1, "Nota fiscal de servi�o"),
	NOTA_FISCAL_CONSUMIDOR		(2, "Nota fiscal do consumidor");
	
	private Integer value;
	private String nome;
	
	private Notaerrocorrecaotipo(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
