package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoTributariaIss {
	
	TRIBUTADA_INTEGRALMENTE_1 	(0, 0, "Tributada Integralmente: o valor do imposto ser� lan�ado para o emissor da nota."),
	TRIBUTADA_INTEGRALMENTE_2 	(1, 1, "Tributada Integralmente com ISSRF: o valor do imposto n�o ser� lan�ado, pois ser� recolhido pelo tomador, caso seja �rg�o p�blico municipal."),
	TRIBUTADA_INTEGRALMENTE_3 	(2, 2, "Tributada Integralmente e sujeita � Substitui��o Tribut�ria: o valor do imposto n�o ser� lan�ado, pois ser� recolhido pelo tomador (substituto tribut�rio), caso n�o seja um �rg�o p�blico municipal."),
	TRIBUTADA_REDUCAO_1 		(3, 3, "Tributada com redu��o da base de c�lculo: o valor do imposto ser� lan�ado para o emissor da nota, por�m, na apura��o da base de c�lculo, ser� descontado o valor da tag <valor_deducao> (esta situa��o tribut�ria somente se aplica, caso o servi�o consignado seja o de c�digo 1705)."),
	TRIBUTADA_REDUCAO_2 		(4, 4, "Tributada com redu��o da base de c�lculo com ISSRF: o valor do imposto n�o ser� lan�ado, pois ser� recolhido pelo tomador, caso seja �rg�o p�blico municipal, por�m na apura��o da base de c�lculo ser� descontado o valor da tag <valor_deducao> (esta situa��o tribut�ria somente se aplica, caso o servi�o consignado seja o de c�digo 1705)."),
	TRIBUTADA_REDUCAO_3 		(5, 5, "Tributada com redu��o da base de c�lculo e sujeita � Substitui��o Tribut�ria: o valor do imposto n�o ser� lan�ado, pois ser� recolhido pelo tomador, caso n�o seja um �rg�o p�blico municipal, por�m na apura��o da base de c�lculo ser� descontado o valor da tag <valor_deducao> (esta situa��o tribut�ria somente se aplica, caso o servi�o consignado seja o de c�digo 1705)."),
	ISENTA 						(6, 6, "Isenta: n�o ir� gerar valor de imposto, pois o prestador � isento."),
	IMUNE 						(7, 7, "Imune: n�o ir� gerar valor do imposto, pois o prestador � imune."),
	NAO_TRIBUTADA_1 			(8, 8, "N�o Tributada - ISS regime Fixo: n�o ir� influenciar no c�lculo do imposto, pois o valor � previamente calculado."),
	NAO_TRIBUTADA_2 			(9, 9, "N�o Tributada - ISS regime Estimativa: n�o ir� influenciar no c�lculo do imposto, pois o valor � previamente estimado."),
	NAO_TRIBUTADA_3 			(10, 10, "N�o Tributada - ISS Constru��o Civil recolhido antecipadamente: n�o ir� gerar valor de imposto, pois foi recolhido antecipadamente (esta situa��o tribut�ria somente se aplica, caso os servi�os consignados sejam os de c�digo 1701, 1702, 1703, 1705, 1719)."),
	NAO_TRIBUTADA_4 			(11, 15, "N�o Tributada - Ato Cooperado: n�o ir� gerar valor do imposto, pois a presta��o de servi�o para cooperados n�o est� sujeita ao ISS; por�m, mesmo que cooperativa e caso o servi�o seja prestado para um n�o cooperado, devese utilizar das outras situa��es tribut�rias, de acordo com o caso.");
	
	private Integer value;
	private Integer cdnfe;
	private String descricao;
	
	private SituacaoTributariaIss(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.descricao = descricao;
		this.cdnfe = cdnfe;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
}
