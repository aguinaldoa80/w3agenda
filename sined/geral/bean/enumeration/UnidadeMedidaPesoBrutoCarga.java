package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum UnidadeMedidaPesoBrutoCarga {

	KG(0, "01", "01 - KG"),
	TON(1, "02", "02 - TON");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private UnidadeMedidaPesoBrutoCarga(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
