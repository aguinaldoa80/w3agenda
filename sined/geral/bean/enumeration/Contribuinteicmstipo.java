package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contribuinteicmstipo {
	
	CONTRIBUINTE		(0, 1, "Contribuinte ICMS (informar a IE do destinat�rio)"),
	ISENTO 				(1, 2, "Contribuinte isento de Inscri��o no cadastro de Contribuintes do ICMS"),
	NAO_CONTRIBUINTE	(2, 9, "N�o Contribuinte, que pode ou n�o possuir Inscri��o Estadual no Cadastro de Contribuintes do ICMS");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private Contribuinteicmstipo(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
