package br.com.linkcom.sined.geral.bean.enumeration;

public enum Gradeestoquetipo {

	MESTRE		(0,"Mestre"),
	ITEMGRADE	(1,"Item da Grade");
	
	private Integer value;
	private String descricao;	
	
	private Gradeestoquetipo(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	
	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
