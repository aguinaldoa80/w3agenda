package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Comissionamentodesempenhoconsiderar {
		
	SALDO_VENDA			(0,"Saldo de Venda"),
	TOTAL_VENDA			(1,"Total de Venda"),	
	MARKUP_VENDA		(2,"Markup de Venda"),
	TOTAL_CONTARECEBER	(3,"Total de Conta a Receber");
	
	
	private Integer value;
	private String nome;
	
	private Comissionamentodesempenhoconsiderar(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return getNome();
	}
	
	public static String listAndConcatenate(List<Comissionamentodesempenhoconsiderar> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Comissionamentodesempenhoconsiderar s : Comissionamentodesempenhoconsiderar.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Comissionamentodesempenhoconsiderar){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}

