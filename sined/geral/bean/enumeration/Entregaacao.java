package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Entregaacao {

	RECEBIMENTO_CRIADO		(0, "Recebimento criado"),
	RECEBIMENTO_BAIXADO		(1, "Recebimento baixado e entrada do material realizada"),
	RECEBIMENTO_CANCELADO	(2, "Recebimento cancelado"),
	RECEBIMENTO_ESTORNADO	(3, "Recebimento estornado"),
	ORDEM_COMPRA_ASSOCIADA	(4, "Ordem de compra associada"),
	ROMANEIO_CRIADO			(5, "Romaneio criado");
	
	private Integer value;
	private String nome;
	
	private Entregaacao(Integer codigo, String descricao){
		this.value = codigo;
		this.nome = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
