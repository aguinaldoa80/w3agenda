package br.com.linkcom.sined.geral.bean.enumeration;

public enum Documentocomissaoreferenciatipoperiodo {
	
	CONTA_ORIGINAL	("Conta Original"),
	CONTA_NEGOCIADA	("Conta Negociada");
	
	private String desricao;
	
	private Documentocomissaoreferenciatipoperiodo(String descricao){
		this.desricao = descricao;
	}
	
	public String getDesricao() {
		return desricao;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
