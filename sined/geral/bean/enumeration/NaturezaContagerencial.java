package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum NaturezaContagerencial {
	
	CONTAS_ATIVO		(0, "01", "Contas de ativo"),
	CONTAS_PASSIVO		(1, "02", "Contas de passivo"),
	PATRIMONIO_LIQUIDO	(2, "03", "Patrim�nio l�quido"),
	CONTAS_RESULTADO 	(3, "04", "Contas de resultado"),
	CONTAS_COMPENSACAO 	(4, "05", "Contas de compensa��o"),
	OUTRAS 				(5, "09", "Outras"),
	CONTA_GERENCIAL     (6, "06", "Conta gerencial");

	private Integer value;
	private String cdsped;
	private String nome;
	
	private NaturezaContagerencial(Integer _value, String _cdsped, String _nome) {
		this.cdsped = _cdsped;
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCdsped() {
		return cdsped;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<NaturezaContagerencial> situacoes){
		StringBuilder codigos = new StringBuilder();
		if(situacoes != null){
			for (int i = 0; i < situacoes.size(); i++)
				for (NaturezaContagerencial s : NaturezaContagerencial.values()){
					boolean existe = false;
					if(situacoes.get(i) instanceof NaturezaContagerencial){
						if((situacoes.get(i)).equals(s)){
							existe = true;
						}
					} else{
						if(((Object)situacoes.get(i)).equals(s.name())){
							existe = true;
						}
					}
					if(existe){
						codigos.append(s.getValue()+", ");
						break;
					}
				}
		}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
