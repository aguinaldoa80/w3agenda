package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum OperacaocontabildebitocreditoControleEnum {
	
	VALOR_BRUTO_NOTA(0, "Valor bruto da nota"),
	VALOR_LIQUIDO_NOTA(1, "Valor l�quido da nota"),
	VALOR_OUTRAS_DESPESAS(2, "Valor outras despesas"),
	VALOR_FRETE(3, "Valor do frete"),
	VALOR_SEGURO(4, "Valor do seguro"),
	VALOR_IR(5, "Valor IR retido"),
	VALOR_INSS_RETIDO(6, "Valor INSS retido"),
	VALOR_CSLL(7, "Valor CSLL retido"),
	VALOR_II(8, "Valor II"),
	VALOR_ISS(9, "Valor ISS"),
	VALOR_ICMS(10, "Valor ICMS"),
	VALOR_IPI(11, "Valor IPI"),
	VALOR_PIS(12, "Valor PIS"),
	VALOR_COFINS(13, "Valor COFINS"),
	VALOR_JUROS(14, "Valor juros"),
	VALOR_MULTA(15, "Valor multa"),
	VALOR_DESCONTO(16, "Valor do desconto"),
	VALOR_DESAGIO(17, "Valor des�gio"),
	VALOR_TAXA_BOLETO(18, "Valor taxa de boleto"),
	VALOR_TERMO(19, "Valor termo"),
	VALOR_MOVIMENTO(20, "Valor taxa de movimento"),
	VALOR_MOVIMENTACAO(21, "Valor da movimenta��o"),
	VALOR_ATUAL_CONTA(22, "Valor atual da conta"),
	VALOR_MATERIAL(23, "Valor material"),
	VALOR_PIS_RETIDO(24, "Valor PIS retido"),
	VALOR_COFINS_RETIDO(25, "Valor COFINS retido"),
	VALOR_ISS_RETIDO(26, "Valor ISS retido"),
	VALOR_CALCULADO(27, "Valor calculado"),
	VALOR_VENDA(28, "Valor venda"),
	VALOR_ORIGINAL_CONTA(29, "Valor original da conta"),
	VALOR_ICMSST(30, "Valor ICMS ST"),
	VALOR_IMPORTACAO(31, "Valor importa��o"),
	VALOR_ITEM_RATEIO(32, "Valor do item de rateio"),
	VALOR_CUSTO(33, "Valor custo"),
	VALOR_CUSTO_MEDIO(34, "Valor custo m�dio"),
	VALOR_MOVIMENTACAO_ESTOQUE(35, "Valor da movimenta��o de estoque"),
	VALOR_UTILIZADO(36, "Valor utilizado"),
	VALOR_PROVISAO(37, "Valor de provis�o"),
	VALOR_DESPESA_COLABORADOR(38, "Valor de despesa do colaborador"),
	VALOR_EVENTO_DESPESA_COLABORADOR(39, "Valor do evento da despesa do colaborador");
	
	
	private Integer id;
	private String nome;
	
	private OperacaocontabildebitocreditoControleEnum(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static List<OperacaocontabildebitocreditoControleEnum> getListaOrdenada(){
		List<OperacaocontabildebitocreditoControleEnum> listaOrdenada = Arrays.asList(OperacaocontabildebitocreditoControleEnum.values());
		
		Collections.sort(listaOrdenada, new Comparator<OperacaocontabildebitocreditoControleEnum>() {
			@Override
			public int compare(OperacaocontabildebitocreditoControleEnum o1, OperacaocontabildebitocreditoControleEnum o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
		});
		
		return listaOrdenada;		
	}
}