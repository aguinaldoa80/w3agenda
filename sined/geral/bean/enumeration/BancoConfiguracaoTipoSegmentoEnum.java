package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoTipoSegmentoEnum {

	HEADER("Header"), 
	DETALHE("Detalhe"), 
	TRAILER("Trailer");
	
	private String nome;
		
	private BancoConfiguracaoTipoSegmentoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
	
}
