package br.com.linkcom.sined.geral.bean.enumeration;

public enum ControleOrcamentoHistoricoEnum {
	
	CRIADO		(0,"Criado"),
	ALTERADO	(1,"Alterado"),
	APROVADO	(2,"Aprovado"),
	ESTORNADO	(3,"Estornado"),
	REPLANEJADO (4,"Replanejado");
	
	private Integer value;
	private String nome;
	
	private ControleOrcamentoHistoricoEnum (Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getNome() {
		return nome;
	}
	@Override
	public String toString() {
		return this.getNome();
	}
}
