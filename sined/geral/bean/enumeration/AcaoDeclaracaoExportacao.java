package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AcaoDeclaracaoExportacao {
	CRIAR 	(1,"Criando declaração"),
	ALTETAR (2,"Alteração na declaração");
	
	private Integer value;
	private String descricao;
	
	private AcaoDeclaracaoExportacao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
	
}
