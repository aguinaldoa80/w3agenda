package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoarquivoretorno {

	ITAU("ITAU (Conta a pagar)"), 					
	ITAUCNAB400("ITAU (Conta a receber - CNAB 400)"),
	CAIXASINCO240("CAIXA (Conta a receber - CNAB 240 - SINCO)"),
	BRADESCO("BRADESCO (Conta a receber - CNAB 400)"),	
	REAL("BANCO REAL (Conta a receber - CNAB 400)"),
	BRASILCNAB240("BANCO DO BRASIL (Conta a receber - CNAB 240)"),
	BRASILCNAB400("BANCO DO BRASIL (Conta a receber - CNAB 400)"),
	SANTANDER ("SANTANDER (Conta a receber - CNAB 400)"),
	RURAL ("BANCO RURAL (Conta a receber - CNAB 400)");
	
	private Tipoarquivoretorno (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
