package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contratofaturalocacaoacao {

	EMITIDA("Emitida"), 			// 0 
	CANCELADA("Cancelada"), 		// 1
	FATURADA("Faturada"),			// 2
	ALTERADA("Alterada"),			// 3
	ENVIO_FATURA("Envio de fatura") // 4
	; 
	
	private Contratofaturalocacaoacao (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
