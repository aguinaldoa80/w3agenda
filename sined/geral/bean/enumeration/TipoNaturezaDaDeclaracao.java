package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoNaturezaDaDeclaracao {
	DIRETA (0,"Exportação Direta"),
	INDIRETA (1,"Exportação Indireta");
	
	private Integer id;
	private String nome;
	private TipoNaturezaDaDeclaracao(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String toString() {
		return getNome();
	}
	
}
