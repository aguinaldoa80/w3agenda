package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoarquivoremessa {

	CNAB240 ("CNAB 240", 0),
	CNAB400 ("CNAB 400", 1);
	
	private Tipoarquivoremessa (String nome, Integer value){
		this.nome = nome;
		this.value = value;
	}
	
	private String nome;
	private Integer value;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Integer getValue() {
		return value;
	}	
	
	@Override
	public String toString() {
		return this.nome;
	}

}
