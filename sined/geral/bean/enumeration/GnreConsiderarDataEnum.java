package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GnreConsiderarDataEnum {
	
	DATA_ATUAL	(0, "Data atual"),
	DATA_SAIDA	(1, "Data de sa�da"),
	DATA_EMISSAO(2, "Data de emiss�o"),
	;

	private Integer value;
	private String nome;
	
	private GnreConsiderarDataEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
