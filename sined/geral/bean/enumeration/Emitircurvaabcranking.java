package br.com.linkcom.sined.geral.bean.enumeration;

public enum Emitircurvaabcranking {
	
	PRODUTO			(1,"Produto"),
	VENDEDOR		(2,"Vendedor"),
	CLIENTE			(3,"Cliente"),
	FORNECEDOR		(4,"Fabricante"),
	GRUPOMATERIAL	(5,"Grupo de Material"),
	TIPOMATERIAL	(6,"Tipo de Material"),
	CIDADE 			(7,"Cidade");
	
	private Integer value;
	private String desricao;
	
	private Emitircurvaabcranking(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}

	@Override
	public String toString() {
		return getDesricao();
	}
}
