package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRetornoAcaoEnum {

	BAIXAR("Baixar"),
	BAIXA_HISTORICO("Baixa com histórico especial"),
	LANCAMENTO_HISTORICO("Lançamento de histórico"),	
	CONFIRMACAO("Confirmação"),
	CONFIRMACAO_REGISTRO("Confirmação de registro"),
	CONFIRMACAO_AGENDAMENTO("Confirmação de agendamento"),
	CANCELAR("Cancelar"),
	REJEITADO("Registro rejeitado"),
	ESTORNAR_PREVISTA("Estornar para prevista"),
	PROTESTAR("Protestar"),
	CANCELAR_PROTESTO("Cancelar protesto"),
	ATUALIZAR_VENCIMENTO("Atualizar vencimento"),
	ATUALIZAR_VENCIMENTO_DATALIMITE_JUROS_MULTA("Atualizar Vencimento e data limite de Juros/Multa"),
	BLOQUEAR_EMISSAO_BOLETO("Bloquear emissão de boleto");
		
	private String nome;
		
	private BancoConfiguracaoRetornoAcaoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
		
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}
	
	public Integer getOrdinal() {
		return this.ordinal();
	}
}
