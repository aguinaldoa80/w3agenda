package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public enum OperacaocontabildebitocreditoTipoEnum {
	
	CONTA_CONTABIL					(0, "Conta cont�bil"),
	CLIENTE							(1, "Cliente"),
	FORNECEDOR						(2, "Fornecedor"),
	VINCULO_CONTA_BANCARIA_CAIXA	(3, "V�nculo (conta banc�ria ou caixa)"),
	VINCULO_ORIGEM					(4, "V�nculo origem"),
	MATERIAL						(5, "Material"),
	PESSOA						    (6, "Pessoa"),
	COLABORADOR						(7, "Colaborador"),
	EVENTO						    (8, "Evento (Despesa de colaboradores)");
	
	
	private Integer id;
	private String nome;
	
	private OperacaocontabildebitocreditoTipoEnum(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static List<OperacaocontabildebitocreditoTipoEnum> getListaOrdenada(){
		List<OperacaocontabildebitocreditoTipoEnum> listaOrdenada = Arrays.asList(OperacaocontabildebitocreditoTipoEnum.values());
		
		Collections.sort(listaOrdenada, new Comparator<OperacaocontabildebitocreditoTipoEnum>() {
			@Override
			public int compare(OperacaocontabildebitocreditoTipoEnum o1, OperacaocontabildebitocreditoTipoEnum o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
		});
		
		return listaOrdenada;		
	}
}