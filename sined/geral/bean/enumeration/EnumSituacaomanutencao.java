package br.com.linkcom.sined.geral.bean.enumeration;

public enum EnumSituacaomanutencao {
	
	PENDENTE(1,"Pendente"),
	EM_ANDAMENTO(2,"Em andamento"),
	REALIZADA(3,"Realizada"),
	CANCELADA(4,"Cancelada");
	
	private Integer idSitucao;
	private String desricao;
	
	private EnumSituacaomanutencao(Integer idSitucao, String descricao){
		this.idSitucao = idSitucao;
		this.desricao = descricao;
	}
	
	public Integer getIdSitucao() {
		return idSitucao;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setIdSitucao(Integer idSitucao) {
		this.idSitucao = idSitucao;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
