package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Configuracaodreseparacao {

	ANTES_RESULTADO_BRUTO	(0, "Antes do resultado bruto"), 
	ANTES_RESULTADO_LIQUIDO	(1, "Antes do resultado l�quido"),
	ANTES_IMPOSTOS			(2, "Antes dos impostos da DRE")
	;
	
	
	private Integer id;
	private String nome;
		
	private Configuracaodreseparacao(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
