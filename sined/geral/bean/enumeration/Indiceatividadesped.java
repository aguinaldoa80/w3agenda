package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indiceatividadesped {
	
	INDUSTRIAL	("Industrial ou equiparado a industrial"),
	OUTROS		("Outros");
	
	private String descricao;
	
	private Indiceatividadesped(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
