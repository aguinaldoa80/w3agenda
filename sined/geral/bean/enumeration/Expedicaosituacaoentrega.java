package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;

public enum Expedicaosituacaoentrega {
	
	PENDENTE	(0, "Entrega pendente"),
	IMPEDIDA	(1, "Entrega com impedimentos"),
	REALIZADA	(2, "Entrega Realizada");
	
	private Integer value;
	private String nome;
	
	private Expedicaosituacaoentrega(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Expedicaosituacaoentrega> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Expedicaosituacaoentrega s : Expedicaosituacaoentrega.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Expedicaosituacaoentrega){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}

	public static List<Expedicaosituacaoentrega> getListagem() {
		List<Expedicaosituacaoentrega> listaExpedicaosituacao = new ArrayList<Expedicaosituacaoentrega>();
		
		for (Expedicaosituacaoentrega expedicaosituacao : Expedicaosituacaoentrega.values()) {
			listaExpedicaosituacao.add(expedicaosituacao);
		}
		
		return listaExpedicaosituacao;
	}
	
}
