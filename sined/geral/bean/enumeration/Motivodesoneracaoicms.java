package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Motivodesoneracaoicms {
	
	TAXI						(0, 1, "T�xi"),
	DEFICIENTE_FISICO			(1, 2, "Deficiente F�sico"),
	PRODUTOR_AGROPECUARIO		(2, 3, "Produtor Agropecu�rio"),
	FROTISTA_LOCADORA			(3, 4, "Frotista/Locadora"),
	DIPLOMATICO_CONSULAR		(4, 5, "Diplom�tico/Consular"), 
	UTILITARIOS_MOTOCICLETAS	(5, 6, "Utilit�rios e Motocicletas da Amaz�nia Ocidental e �reas de Livre Com�rcio (Resolu��o 714/88 e 790/94 � CONTRAN e suas altera��es)"), 
	SUFRAMA						(6, 7, "SUFRAMA"),
	OUTROS						(7, 9, "Outros"),
	ORGAO_PUBLICO				(8, 8, "Venda a �rg�o P�blico"),
	DEFICIENTE_CONDUTOR			(9, 10, "Deficiente Condutor (Conv�nio ICMS 38/12)"),
	DEFICIENTE_NAO_CONDUTOR		(10, 11, "Deficiente N�o Condutor (Conv�nio ICMS 38/12)"),
	ORGAO_DESENV_AGROPECUARIO	(11, 12, "�rg�o de fomento e desenvolvimento agropecu�rio");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private Motivodesoneracaoicms(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static Motivodesoneracaoicms getMotivodesoneracaoicms(Integer cdnfe){
		Motivodesoneracaoicms[] motivos = Motivodesoneracaoicms.values();
		for (Motivodesoneracaoicms motivodesoneracaoicms : motivos) {
			if(motivodesoneracaoicms.getCdnfe().equals(cdnfe)){
				return motivodesoneracaoicms;
			}
		}
		return null;
	}

}
