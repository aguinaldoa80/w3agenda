package br.com.linkcom.sined.geral.bean.enumeration;

public enum SituacaoEnum {

	APROVADO(0,"Aprovado"),
	REPROVADO(1,"Reprovado");
	
	private String nome;
	private Integer valor; 
	
	private SituacaoEnum(Integer valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return getNome().toUpperCase();
	}
}
