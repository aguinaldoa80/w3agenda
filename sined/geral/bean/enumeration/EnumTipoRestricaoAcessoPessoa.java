package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum EnumTipoRestricaoAcessoPessoa {
	
	DIA("Dia"),
	HORARIO("Hor�rio");
	
	private String descricao;
	
	private EnumTipoRestricaoAcessoPessoa(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}