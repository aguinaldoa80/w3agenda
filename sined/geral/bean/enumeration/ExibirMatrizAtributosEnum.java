package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ExibirMatrizAtributosEnum {
	
	SIM		(0, "Sim"),
	NAO		(1, "N�o"),
	NEUTRO	(2, "Neutro");

	private Integer value;
	private String nome;
	
	private ExibirMatrizAtributosEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}