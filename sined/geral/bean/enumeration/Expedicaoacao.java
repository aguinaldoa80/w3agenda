package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Expedicaoacao {
	
	CRIADA		(0, "Criada"),
	ALTERADA	(1, "Alterada"),
	CONFIRMADA	(2, "Confirmada"),
	CANCELADA	(3, "Cancelada"),
	ESTORNADA	(4, "Estornada"),
	FATURADA	(5, "Faturada"),
	VENDA_ADICIONADA (6, "Venda Adicionada"), 
	CONFERENCIA_REALIZADA (7, "Confer�ncia realizada"),
	VENDA_REMOVIDA (8, "Venda(s) removida(s) da expedi��o"),
	SEPARACAO_INICIADA (9, "Separa��o iniciada"),
	SEPARACAO_FINALIZADA (10, "Separa��o finalizada"),
	RELATORIO_IMPRESSO (11, "Relat�rio de separa��o impresso"),
	CONFERENCIA_PREVIA_REALIZADA (12, "Confer�ncia pr�via realizada"),
	CONFERENCIA_INICIADA (13, "Confer�ncia iniciada"),
	CONFERENCIA_FINALIZADA (14, "Confer�ncia finalizada"),
	FINALIZAR_SEM_CONFERENCIA (15, "Finalizado sem confer�ncia"),
	RASTREIO (16, "Rastreio"),
	FINALIZAR_ENTREGA_EXPEDICAO_ECOMMERCE (17, "Entrega finalizada"),
	CONSULTA_RASTREIO_CORREIOS (18, "Consulta eventos dos Correios"),
	;
	
	private Integer value;
	private String nome;
	
	private Expedicaoacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
