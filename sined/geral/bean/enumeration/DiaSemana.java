package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum DiaSemana {

	DOMINGO("Domingo", "D"),				// 0
	SEGUNDA("Segunda-feira", "S"), 		// 1 
	TERCA("Ter�a-feira", "T"), 			// 2
	QUARTA("Quarta-feira", "Q"),			// 3
	QUINTA("Quinta-feira", "Q"),			// 4
	SEXTA("Sexta-feira", "S"),			// 5
	SABADO("S�bado", "S");				// 6
	
	private DiaSemana (String nome, String sigla){
		this.nome = nome;
		this.sigla = sigla;
	}
	
	private String nome;
	private String sigla;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public String getSigla() {
		return sigla;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
	public static DiaSemana getDiasemanaByString(String value){
		for (DiaSemana diaSemana : DiaSemana.values()) 
			if(diaSemana.name().equals(value) || diaSemana.getNome().equals(value))
				return diaSemana;
		return null;
	}
}
