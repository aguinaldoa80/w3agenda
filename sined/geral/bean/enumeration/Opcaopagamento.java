package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Opcaopagamento {

	PARCELA_UNICA_COM_DESCONTO	(0, "Parcela �nica com desconto"), 
	PARCELA_UNICA_SEM_DESCONTO	(1, "Parcela �nica sem desconto"),
	PARCELA_1 					(2, "Parcela N� 1"),
	PARCELA_2 					(3, "Parcela N� 2"),
	PARCELA_3 					(4, "Parcela N� 3"),
	PARCELA_4 					(5, "Parcela N� 4"),
	PARCELA_5 					(6, "Parcela N� 5"),
	PARCELA_6 					(7, "Parcela N� 6"),
	; 
	
	private Integer value;
	private String descricao;
		
	private Opcaopagamento(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return getDescricao();
	}	
}
