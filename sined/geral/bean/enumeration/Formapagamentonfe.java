package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Formapagamentonfe {
	
	A_VISTA	(0, "� Vista"),
	A_PRAZO	(1, "A Prazo"), 
	OUTROS	(2, "Outros");
	
	private Integer value;
	private String descricao;
	
	private Formapagamentonfe(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
