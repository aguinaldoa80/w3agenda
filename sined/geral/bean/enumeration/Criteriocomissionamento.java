package br.com.linkcom.sined.geral.bean.enumeration;

public enum Criteriocomissionamento {

	SEM_CRITERIO					(0,"Sem Crit�rio"),
	DIVIDIR_VENDEDOR_PRINCIPAL		(1,"Dividir com o Vendedor Principal"),
	ABAIXO_VALOR_VENDA				(2,"Abaixo do Valor de Venda"),
	ACIMA_VALOR_VENDA				(3,"Acima do Valor de Venda"),
	PRIMEIRA_VENDA_CLIENTE			(4,"Primeira Venda por Cliente"),
	PRIMEIRA_VENDA_GRUPO_CLIENTE	(5,"Primeira Venda de um Grupo por Cliente"),
	INDICADOR_VENDA					(6,"Indicador da Venda"),
	IGUAL_VALOR_VENDA				(7,"Igual ao valor de venda"),
	FAIXA_DESCONTO					(8,"Faixa de Desconto na Venda"),
	FAIXA_MARKUP					(9,"Por Faixa");
	
	private Integer value;
	private String descricao;	
	
	private Criteriocomissionamento(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
