/*package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoEventoEnum {
	
	BDE			(0, "BDE"),
	BDI			(1, "BDI"),
	BDR			(2, "BDR"),
	BLQ			(3, "BLQ"),
	PAR			(4, "PAR"),
	EST			(5, "EST"),
	RO			(6, "RO"),
	DO			(7, "DO"),
	FC			(8, "FC"),
	LDE			(9, "LDE"),
	LDI			(10, "LDI"),
	OEC			(11, "OEC"),
	PO			(12, "PO");

	private Integer value;
	private String nome;
	
	
	private TipoEventoEnum(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}
	
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
*/