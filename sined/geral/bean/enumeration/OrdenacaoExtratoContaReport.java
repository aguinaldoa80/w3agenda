package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoExtratoContaReport {
	DATA("dtmovimentacao", "Data"), 
	NUMERO("cdmovimentacao", "N�mero"), 
	DOCUMENTO("checknum","Documento");

	private OrdenacaoExtratoContaReport(String campo, String nome) {
		this.campo = campo;
		this.nome = nome;
	}

	private String campo;
	private String nome;
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}

}
