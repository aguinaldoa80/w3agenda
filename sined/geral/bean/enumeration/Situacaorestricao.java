package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaorestricao {
	NAO_BLOQUEADO 		("N�o bloqueado"), 		// 0
	BLOQUEIO_PARCIAL 	("Bloqueio parcial"),	// 1  
	BLOQUEIO_TOTAL 		("Bloqueio total");		// 2
	
	private String nome;
	
	private Situacaorestricao (String nome){
		this.nome = nome;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
