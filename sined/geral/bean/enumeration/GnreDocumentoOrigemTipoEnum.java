package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GnreDocumentoOrigemTipoEnum {
	
	NUMERO			(0, "N�mero da nota"),
	CHAVE_ACESSO	(1, "Chave de acesso");

	private Integer value;
	private String nome;
	
	private GnreDocumentoOrigemTipoEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
