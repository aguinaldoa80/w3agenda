package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FaturamentoVeterinaria {
	
	QUANTIDADE_CONSUMIDA	(0, "Qtde. Consumida"),
	QUANTIDADE_APLICACAO	(1, "Qtde. de aplica��o"),
	UNIDADE_INTEGRAL		(2, "Unidade integral (Frasco/Cartela)");
	
	private Integer value;
	private String nome;
	
	private FaturamentoVeterinaria(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
