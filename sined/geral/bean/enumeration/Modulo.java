package br.com.linkcom.sined.geral.bean.enumeration;


public enum Modulo{

	ADM("Administra��o"),
	BRIEFCASE("BriefCase"),
	CRM("CRM"),
	FATURAMENTO("Faturamento"),
	FINANCEIRO("Financeiro"),
	FISCAL("Fiscal"),
	FORNECEDOR("Cota��o de fornecedor"),
	JURIDICO("Jur�dico"),
	OFFLINE("Offline"),
	PRODUCAO("Produ��o"),
	PROJETO("Projeto"),
	PUB("P�blico"),
	RH("Recursos humanos"),
	SERVICOINTERNO("Servi�os"),
	SISTEMA("Sistema"),
	SUPRIMENTO("Suprimento"),
	VEICULO("Ve�culo"),
	VETERINARIA("Veterin�ria"),
	CONTABIL("Cont�bil"),
	AGRO("Agro");
	
	private String descricao;
	
	private Modulo(String descricao){
		this.descricao = descricao;
	}
	
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
}