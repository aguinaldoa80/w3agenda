package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipotitulocredito {
	
	DUPLICATA	("00", "Duplicata"),
	CHEQUE		("01", "Cheque"), 
	PROMISSORIA	("02", "Promissória"),
	RECIBO		("03", "Recibo"),
	OUTROS		("99", "Outros");
	
	private String value;
	private String descricao;
	
	private Tipotitulocredito(String value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static Tipotitulocredito getModeloByValor(String valor){
		Tipotitulocredito[] values = Tipotitulocredito.values();
		for (int i = 0; i < values.length; i++) {
			if(values[i].getValue().equals(valor)) return values[i];
		}
		return null;
	}

}
