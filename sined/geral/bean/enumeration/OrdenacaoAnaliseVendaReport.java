package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoAnaliseVendaReport {

	CLIENTE			("cliente",					"Cliente"), 		
	ULTIMA_COMPRA		("ultimaCompra",					"�ltima Compra"),
	PERIODICIDADE		("periodicidade", 				"Periodicidade");			
	
	private OrdenacaoAnaliseVendaReport (String campo, String nome){
		this.nome = nome;
		this.campo = campo;
	}
	
	private String campo;
	private String nome;
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
