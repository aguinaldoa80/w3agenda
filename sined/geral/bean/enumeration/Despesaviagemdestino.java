package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Despesaviagemdestino {
	AVULSO		(1,"Avulso"),
	CLIENTE		(2,"Cliente"),
	CONTA_CRM	(3,"Conta CRM"),
	FORNECEDOR	(4,"Fornecedor");
	
	protected Integer value;
	protected String nome;
	
	private Despesaviagemdestino(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setValue(Integer value) {
		this.value = value;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	@Override
	public String toString() {
		return getNome();
	}
}
