package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocobrancaicms {
	
	TRIBUTADA_INTEGRALMENTE						(0, false,  "00", "00 - Tributada integralmente"),
	TRIBUTADA_COM_SUBSTITUICAO					(1, false,  "10", "10 - Tributada e com cobran�a do ICMS por substitui��o tribut�ria"),
	TRIBUTADA_COM_SUBSTITUICAO_PARTILHA			(2, false,  "10", "10 - Tributada e com cobran�a do ICMS por substitui��o tribut�ria (com partilha do ICMS entre a UF de origem e a UF de destino ou a UF definida na legisla��o)"),
	TRIBUTADA_COM_REDUCAO_BC					(3, false,  "20", "20 - Com redu��o de base de c�lculo"),
	NAO_TRIBUTADA_COM_SUBSTITUICAO				(4, false,  "30", "30 - Isenta ou n�o tributada e com cobran�a do ICMS por substitui��o tribut�ria"),
	ISENTA										(5, false,  "40", "40 - Isenta"), 
	NAO_TRIBUTADA								(6, false,  "41", "41 - N�o tributada"),
	NAO_TRIBUTADA_ICMSST_DEVENDO				(7, false,  "41", "41 - N�o tributada (ICMSST devido para a UF de destino, nas opera��es interestaduais de produtos que tiveram reten��o antecipada de ICMS por ST na UF do remetente)"),
	SUSPENSAO									(8, false,  "50", "50 - Suspens�o"),
	DIFERIMENTO									(9, false,  "51", "51 - Diferimento"),
	COBRADO_ANTERIORMENTE_COM_SUBSTITUICAO		(10, false, "60", "60 - ICMS cobrado anteriormente por substitui��o tribut�ria"),
	TRIBUTADA_COM_REDUCAO_BC_COM_SUBSTITUICAO	(11, false, "70", "70 - Com redu��o de base de c�lculo e cobran�a do ICMS por substitui��o tribut�ria ICMS por substitui��o tribut�ria"),
	OUTROS										(12, false, "90", "90 - Outros"),
	OUTROS_PARTILHA								(13, false, "90", "90 - Outros (com partilha do ICMS entre a UF de origem e a UF de destino ou a UF definida na legisla��o)"), 
	
	SIMPLES_NACIONAL_COM_PERMISSAO_CREDITO		(14, true,  "101", "101 - Tributada pelo Simples Nacional com permiss�o de cr�dito"), 
	SIMPLES_NACIONAL_SEM_PERMISSAO_CREDITO		(15, true,  "102", "102 - Tributada pelo Simples Nacional sem permiss�o de cr�dito"), 
	ISENCAO_ICMS_SIMPLES_NACIONAL				(16, true,  "103", "103 - Isen��o do ICMS no Simples Nacional para faixa de receita bruta"), 
	COM_PERMISSAO_CREDITO_ICMS_ST				(17, true,  "201", "201 - Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria"), 
	SEM_PERMISSAO_CREDITO_ICMS_ST				(18, true,  "202", "202 - Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria"), 
	ISENCAO_ICMS_SIMPLES_NACIONAL_ICMS_ST		(19, true,  "203", "203 - Isen��o do ICMS nos Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por Substitui��o Tribut�ria"), 
	IMUNE										(20, true,  "300", "300 - Imune"), 
	NAO_TRIBUTADA_SIMPLES_NACIONAL				(21, true,  "400", "400 - N�o tributada pelo Simples Nacional"), 
	ICMS_COBRADO_ANTERIORMENTE					(22, true,  "500", "500 - ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o"), 
	OUTROS_SIMPLES_NACIONAL						(23, true,  "900", "900 - Outros"),
	
	COBRADO_ANTERIORMENTE_ICMSST_DEVENDO		(24, false,  "60", "60 - ICMS cobrado anteriormente por substitui��o tribut�ria (ICMSST devido para a UF de destino, nas opera��es interestaduais de produtos que tiveram reten��o antecipada de ICMS por ST na UF do remetente);");
	
	private Integer value;
	private String descricao;
	private String cdnfe;
	private boolean simplesnacional;
	
	private Tipocobrancaicms(Integer value, Boolean simplesnacional, String cdnfe, String descricao){
		this.value = value;
		this.descricao = descricao;
		this.cdnfe = cdnfe;
		this.simplesnacional = simplesnacional;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	public boolean isSimplesnacional() {
		return simplesnacional;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Tipocobrancaicms> getListaCobranca(boolean simplesnacional){
		Tipocobrancaicms[] values = Tipocobrancaicms.values();
		List<Tipocobrancaicms> lista = new ArrayList<Tipocobrancaicms>();
		for (int i = 0; i < values.length; i++) {
			if(simplesnacional){
				if(values[i].simplesnacional){
					lista.add(values[i]);
				}
			} else {
				if(!values[i].simplesnacional){
					lista.add(values[i]);
				}
			}
		}
		
		Collections.sort(lista,new Comparator<Tipocobrancaicms>(){
			public int compare(Tipocobrancaicms o1, Tipocobrancaicms o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		
		return lista;
	}
	
	public static Tipocobrancaicms getTipocobrancaicms(String cdnfe){
		for(Tipocobrancaicms tipo: values())
			if(tipo.getCdnfe().equals(cdnfe)) return tipo;
		return null;
	}
	
}
