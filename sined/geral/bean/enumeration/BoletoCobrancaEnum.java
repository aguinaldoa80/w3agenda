package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum11pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum11posComRegistro;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSICOB_nossoNum16pos;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSIGCB;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSIGCB_ComRegistro;
import br.com.linkcom.lkbanco.boleto.BoletoBancarioCaixaSINCO;
import br.com.linkcom.neo.types.ListSet;

public enum BoletoCobrancaEnum {
	
	SICOB("104", "Sicob", BoletoBancarioCaixaSICOB.class),
	SICOB_11("104", "Sicob nossoNum11", BoletoBancarioCaixaSICOB_nossoNum11pos.class),
	SICOB_11CR("104", "Sicob nossoNum11 com registro", BoletoBancarioCaixaSICOB_nossoNum11posComRegistro.class),
	SICOB_16("104", "Sicob nossoNum16", BoletoBancarioCaixaSICOB_nossoNum16pos.class),
	SIGCB("104", "Sigcb", BoletoBancarioCaixaSIGCB.class),
	SIGCB_CR("104", "Sigcb com registro", BoletoBancarioCaixaSIGCB_ComRegistro.class),
	SINCO("104", "Sinco", BoletoBancarioCaixaSINCO.class);			
		
	
	private BoletoCobrancaEnum(String numerobanco, String nome,  Class<?> leiauteboleto) {
		this.numerobanco = numerobanco;
		this.nome = nome;
		this.leiauteboleto = leiauteboleto;
	}
	
	private String numerobanco;
	private String nome;
	private Class<?> leiauteboleto;
	
	public String getNumerobanco() {
		return numerobanco;
	}
	public void setNumerobanco(String numerobanco) {
		this.numerobanco = numerobanco;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Class<?> getLeiauteboleto() {
		return leiauteboleto;
	}
	public void setLeiauteboleto(Class<?> leiauteboleto) {
		this.leiauteboleto = leiauteboleto;
	}
	
	@Override
	public String toString() {
		return getNome();
	}	
	
	public static List<BoletoCobrancaEnum> getAllByBanco(String numerobanco){
		List<BoletoCobrancaEnum> lista = new ListSet<BoletoCobrancaEnum>(BoletoCobrancaEnum.class);
		for(BoletoCobrancaEnum cobrancaenum : BoletoCobrancaEnum.values()){
			if(cobrancaenum.numerobanco.equals(numerobanco))
				lista.add(cobrancaenum);
		}
		
		return lista;
	}
	
	public static String getAllByBancoForSelect(String numerobanco){
		StringBuilder sb = new StringBuilder();
		for(BoletoCobrancaEnum boletocobranca : BoletoCobrancaEnum.values()){
			if(boletocobranca.numerobanco.equals(numerobanco)){
				sb.append("<option value=\"")
					.append(boletocobranca.name())
					.append("\"> ")
					.append(boletocobranca.toString())
					.append("</option>");
			}
		}
		if(sb.length() > 0){
			sb.insert(0, "<option value=\"<null>\"></option>");
		}
		return sb.toString();
	}
}
