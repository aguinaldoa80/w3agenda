package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoNavegacaoAquaviario {

	INTERIOR(0, "0 - Interior"),
	CABOTAGEM(1, "1 - Cabotagem");
	
	private Integer value;
	private String nome;
	
	private TipoNavegacaoAquaviario(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
