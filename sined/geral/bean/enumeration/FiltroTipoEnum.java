package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FiltroTipoEnum {
	
	INDIVIDUAL	("Individual"),
	COMPARTILHADO			("Compartilhado");
	
	private String descricao;
	
	private FiltroTipoEnum(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
