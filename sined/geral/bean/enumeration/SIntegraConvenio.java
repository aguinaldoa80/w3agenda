package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SIntegraConvenio {

	ESTRUTURA_CONVENIO_57_95_VERSAO_CONVENIO_31_99_ALTERACOES_CONVENIO_30_02	(0,"1","Estrutura conforme Conv�nio ICMS 57/95, na vers�o estabelecida pelo Conv�nio ICMS 31/99 e com as altera��es promovidas at� o Conv�nio ICMS 30/02"),
	ESTRUTURA_CONVENIO_57_95_VERSAO_CONVENIO_69_02_ALTERACOES_CONVENIO_142_02	(1,"2","Estrutura conforme Conv�nio ICMS 57/95, na vers�o estabelecida pelo Conv�nio ICMS 69/02 e com as altera��es promovidas pelo Conv�nio ICMS 142/02"),
	ESTRUTURA_CONVENIO_57_95_ALTERACOES_CONVENIO_76_03							(2,"3","Estrutura conforme Conv�nio ICMS 57/95, com as altera��es promovidas pelo Conv�nio ICMS 76/03");
	
	private Integer value;
	private String cdsintegra;
	private String nome;
	
	private SIntegraConvenio(Integer value, String cdsintegra, String nome){
		this.value = value;
		this.cdsintegra = cdsintegra;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	public String getCdsintegra() {
		return cdsintegra;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override	
	public String toString(){
		return nome;
	}
}
