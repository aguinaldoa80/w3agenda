package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum BaixarEstoqueMateriasPrimasEnum {

	AO_PRODUZIR_AGENDA			(0, "Ao produzir agenda"),
	AO_CONCLUIR_ULTIMA_ETAPA	(1, "Ao concluir �ltima etapa"),
	AO_CONCLUIR_CADA_ETAPA		(2, "Ao concluir cada etapa");
	
	private Integer value;
	private String nome;
	
	private BaixarEstoqueMateriasPrimasEnum(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
