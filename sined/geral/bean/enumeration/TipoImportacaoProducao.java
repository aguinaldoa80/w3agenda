package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoImportacaoProducao {
	PERFIL, COMPONENTE, VIDRO, PAINEL, TIPOLOGIA
}