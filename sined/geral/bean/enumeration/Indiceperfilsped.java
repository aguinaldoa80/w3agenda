package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indiceperfilsped {
	
	PERFIL_A	("A", "Perfil A"),
	PERFIL_B	("B", "Perfil B"),
	PERFIL_C	("C", "Perfil C");
	
	private String value;
	private String descricao;
	
	private Indiceperfilsped(String value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
