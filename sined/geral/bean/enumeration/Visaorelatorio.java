package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Visaorelatorio {

	COLABORADOR		(0, "Colaborador"),
	CONTRATO		(1, "Contrato");
	
	private Integer value;
	private String nome;
	
	private Visaorelatorio(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	} 
}
