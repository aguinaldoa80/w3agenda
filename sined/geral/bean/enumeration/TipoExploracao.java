package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoExploracao {
	EXPLORACAO(1, "Explora��o Individual"),
	CONDOMINIO(2, "Condom�nio"),
	IMOVEL_ARRENDADO(3, "Im�vel Arrendado"),
	PARCEIRA(4, "Parceria"),
	COMODATO(5, "Comodato"),
	OUTROS(6, "Outros");
	
	private Integer id;
	private String descricao;
	
	private TipoExploracao(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
