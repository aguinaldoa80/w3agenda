package br.com.linkcom.sined.geral.bean.enumeration;


public enum Tipohistoricooperacao {

	EMISSAO_NF(0, "Emiss�o de nota fiscal"),
	CONTA_RECEBER_BAIXADA_DINHEIRO(1, "Conta a receber baixada (Dinheiro)"),
	CONTA_RECEBER_BAIXADA_CRED_EM_CONTA(2, "Conta a receber baixada (Cr�dito em conta corrente)"),
	CONTA_RECEBER_BAIXADA_CHEQUE(3, "Conta a receber baixada (Cheque)"),
	CONTA_RECEBER_BAIXADA_CARTAO(4, "Conta a receber baixada (Cart�o)"),
	CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO(5, "Conta a receber baixada (Retorno banc�rio)"),
	CONCILIACAO_CARTAO(6, "Concilia��o de cart�o"),
	CREDITO_RETORNO_BANCARIO(7, "Cr�dito gerado pelo retorno banc�rio"),
	JUROS_RECEBIDO(8, "Juros recebidos"),
	DESCONTO_CONCEDIDO(9, "Descontos concedidos"),
	REGISTRO_ENTRADA_NF(10, "Registro de entrada fiscal"),
	CONTA_PAGAR_BAIXADA_FORNECEDOR(11, "Conta a pagar baixada (Fornecedor)"),
	JUROS_PAGOS(12, "Juros pagos"),
	DESCONTO_OBTIDO(13, "Descontos obtidos"),
	TRANSFERENCIA_ENTRE_CONTAS(14, "Transfer�ncia entre contas"),
	TAXA_RECEBIMENTO(15, "Taxa de recebimento");
	
	private Tipohistoricooperacao(Integer codigo, String nome){
		this.nome = nome;
		this.cdtipohistoricooperacao = codigo;
	}
	
	private Integer cdtipohistoricooperacao;
	private String nome;
	
	public Integer getCdtipohistoricooperacao() {
		return cdtipohistoricooperacao;
	}
	public void setCdtipohistoricooperacao(Integer cdtipohistoricooperacao) {
		this.cdtipohistoricooperacao = cdtipohistoricooperacao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.getNome();
	}
}