package br.com.linkcom.sined.geral.bean.enumeration;

public enum MailingDevedorMostrar {
	
	PENDENTES_COBRANCA ("Pendentes de cobran�a "),
	SOMENTE_COBRADOS ("Somente j� cobrados");
	
	private String descricao;
	
	private MailingDevedorMostrar(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
