package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoCampoEnum {

	NUMERO_SEQUENCIAL("N�mero sequencial", false, false, false, false, 0),
	
	AGENCIA_CEDENTE("Ag�ncia cedente", false, false, false, false, 1),
	CONTA_CEDENTE("Conta cedente", false, false, false, false, 2),
	CODIGO_CEDENTE("C�digo cedente", false, false, false, false, 3),	
	NOME_SACADO("Nome do sacado", false, false, false, false, 4),
	BANCO_IDENTIFICADOR("Banco identificador", false, false, false, false, 5),
	
	DATA_GERACAO("Data de gera��o", false, true, false, false, 6),
	DATA_CREDITO("Data cr�dito", true, true, false, false, 7),
	DATA_OCORRENCIA("Data de ocorr�ncia", false, true, false, false, 8),
	DATA_VENCIMENTO("Data de vencimento", false, true, false, false, 9),
	DATA_PAGAMENTO("Data de pagamento", false, true, false, false, 10),
	
	VALOR_DESCONTO("Valor desconto", false, false, true, false, 11),
	VALOR_PAGO("Valor pago", true, false, true, true, 120),
	VALOR_JUROS("Valor juros", false, false, true, false, 13),
	VALOR_MULTA("Valor multa", false, false, true, false, 14),
	VALOR_ACRESCIMO("Valor acr�scimo", false, false, true, false, 15),
	VALOR_OUTRODESCONTO("Valor outros descontos", false, false, true, false, 16),
	VALOR_TITULO("Valor do t�tulo", true, false, true, false, 17),
	
	NOSSO_NUMERO("Nosso n�mero", true, false, false, false, 18),
	USO_EMPRESA("Uso da empresa", false, false, false, false, 19),
	
	CODIGO_OCORRENCIA("C�digo de ocorr�ncia", true, false, false, false, 20),
	MOTIVO_REJEICAO("Motivo rejei��o", false, false, false, false, 21),
	MENSAGEM("Mensagem", false, false, false, false, 22),
	
	VALOR_TARIFA("Valor tarifa", false, false, true, false, 23),
	MOTIVO_TARIFA("Motivo tarifa", false, false, false, false, 24),
	
	IDENTIFICADOR_HEADER("Identificador (Header)", true, false, false, false, 25),
	IDENTIFICADOR_DETALHE("Identificador (Detalhe)", true, false, false, false, 26),
	IDENTIFICADOR_TRAILER("Identificador (Trailer)", true, false, false, false, 27);
			
	private String nome;
	private boolean obrigatorio;
	private boolean data;
	private boolean valor;
	private boolean calculado;
	private Integer ordem;
		
	private BancoConfiguracaoCampoEnum(String nome, boolean obrigatorio, boolean data, boolean valor, boolean calculado, Integer ordem){
		this.nome = nome;
		this.obrigatorio = obrigatorio;
		this.data = data;
		this.valor = valor;
		this.calculado = calculado;
		this.ordem = ordem;
	}
	
	public String getNome() {
		return nome;
	}
	
	public boolean isData() {
		return data;
	}
	
	public boolean isObrigatorio() {
		return obrigatorio;
	}
	
	public boolean isCalculado() {
		return calculado;
	}
	
	public boolean isValor() {
		return valor;
	}
	
	public Integer getOrdem() {
		return ordem;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setData(boolean data) {
		this.data = data;
	}
	
	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	
	public void setCalculado(boolean calculado) {
		this.calculado = calculado;
	}
	
	public void setValor(boolean valor) {
		this.valor = valor;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String toString() {
		return getNome();
	}
	
	public static BancoConfiguracaoCampoEnum getBancoConfiguracaoCampoByNome(String nome){
		nome = nome.toUpperCase();
		BancoConfiguracaoCampoEnum campo = null;
		BancoConfiguracaoCampoEnum[] campos = BancoConfiguracaoCampoEnum.values();
		for (BancoConfiguracaoCampoEnum bcc : campos){
			if (bcc.getNome().equals(nome)){
				campo = bcc;
				break;
			}
		}
		return campo;
	}
	
}
