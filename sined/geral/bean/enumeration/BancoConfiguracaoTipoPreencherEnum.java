package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoTipoPreencherEnum {

	ESPACO("Espa�os"),
	ZERO("Zeros"); 
	
	private String nome;
		
	private BancoConfiguracaoTipoPreencherEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
	
}
