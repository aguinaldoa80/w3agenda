package br.com.linkcom.sined.geral.bean.enumeration;

public enum PedidoVendaTipoEnum {
	
	CRIADO("Criado"), 
	ALTERADO("Alterado");
	
	private String nome;
		
	private PedidoVendaTipoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
}
