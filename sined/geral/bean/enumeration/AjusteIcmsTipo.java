package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AjusteIcmsTipo {

	ICMS		(0, "ICMS"), 
	ICMS_ST		(1, "ICMS ST"),
	ICMS_DIFAL	(2, "ICMS - DIFAL"), 
	ICMS_FCP	(3, "ICMS - FCP"),
	;
	
	
	private Integer id;
	private String nome;
		
	private AjusteIcmsTipo(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
