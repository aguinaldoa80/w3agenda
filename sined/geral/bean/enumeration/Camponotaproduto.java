package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Camponotaproduto {
	
	CAMPO_LIVRE 			(0, "Campo livre", false),
	NUMERO_NOTA 			(1, "N�mero da nota", false), 
	DTEMISSAO_NOTA 			(2, "Data de emiss�o da nota", false), 
	END_CLIENTE 			(3, "Endere�o do cliente", false), 
	ENDNUM_CLIENTE 			(4, "Endere�o com n�mero do cliente", false), 
	ENDNUMCOMP_CLIENTE 		(5, "Endere�o com n�mero e complemento do cliente", false), 
	ENDNUMCOMPBAI_CLIENTE 	(6, "Endere�o com n�mero, complemento e bairro do cliente", false), 
	NUMERO_CLIENTE 			(7, "N�mero do endere�o do cliente", false), 
	COMPLEMENTO_CLIENTE 	(8, "Complemento do endere�o do cliente", false), 
	BAIRRO_CLIENTE 			(9, "Bairro do endere�o do cliente", false), 
	CEP_CLIENTE 			(10, "Cep do cliente", false), 
	MUNICIPIO_CLIENTE 		(11, "Munic�pio do cliente", false), 
	MUNICIPIOUF_CLIENTE 	(12, "Munic�pio e sigla da UF do cliente", false), 
	UFDESCRICAO_CLIENTE 	(13, "Descri��o da UF do cliente", false), 
	UFSIGLA_CLIENTE 		(14, "Sigla da UF do cliente", false), 
	CPFCNPJ_CLIENTE 		(15, "CPF/CNPJ do cliente", false), 
	INSCESTADUAL_CLIENTE 	(16, "Inscri��o estadual do cliente", false),
	INSCMUNICIPAL_CLIENTE 	(17, "Inscri��o municipal do cliente", false),
	RAZAOSOCIALNOME_CLIENTE	(18, "Raz�o social ou nome do cliente", false),
	RAZAOSOCIAL_CLIENTE		(19, "Raz�o social do cliente", false),
	NOME_CLIENTE			(20, "Nome do cliente", false),
	DIAEMISSAO_NOTA			(21, "Dia da emiss�o da nota", false),
	MESNUMEMISSAO_NOTA		(22, "N�mero do m�s da emiss�o da nota", false),
	MESDESCRICAOEMISSAO_NOTA(23, "Descri��o do m�s da emiss�o da nota", false),
	ANOEMISSAO_NOTA			(24, "Ano da emiss�o da nota", false),
	VALORPRODUTOS_NOTA		(25, "Valor dos produtos da nota sem impostos", false),
	VALORISS_NOTA			(26, "Valor do ISS da nota", false),
	PERCENTUALISS_NOTA		(27, "Percentual do ISS da nota", false),
	VALORINSS_NOTA			(28, "Valor do INSS da nota", false),
	PERCENTUALINSS_NOTA		(29, "Percentual do INSS da nota", false),
	VALORCOFINS_NOTA		(30, "Valor do COFINS da nota", false),
	PERCENTUALCOFINS_NOTA	(31, "Percentual do COFINS da nota", false),
	VALORCSLL_NOTA			(32, "Valor do CSLL da nota", false),
	PERCENTUALCSLL_NOTA		(33, "Percentual do CSLL da nota", false),
	VALORICMS_NOTA			(34, "Valor do ICMS da nota", false),
	PERCENTUALICMS_NOTA		(35, "Percentual do ICMS da nota", false),
	VALORTOTALEXTENSO_NOTA	(36, "Valor total da nota por extenso", false),
	VOLORTOTAL_NOTA			(37, "Valor total da nota", false),
	QTDE_NOTAITEM			(38, "Quantidade do item", true),
	DISCRIMINACAOPRODUTO_NOTAITEM	(39, "Discrimina��o do produto", true),
	VALORUNITARIO_NOTAITEM	(40, "Valor unit�rio do item", true),
	VALORTOTALITEM_NOTAITEM	(41, "Valor total do item", true),
	CDCONTRATO				(42, "C�digo do Contrato", false),
	VALORDESCONTO_NOTA		(43, "Valor total do desconto", false),
	CDDOCUMENTO				(44, "C�digo do boleto", false),
	TIPO_NOTA				(45, "Tipo da Nota Fiscal", false),
	NATUREZAOPERACAO_NOTA	(46, "Natureza da Opera��o", false),
	CFOP_NOTAITEM			(47, "CFOP", true),
	INSCESTADUAL_SUBTRIBUTARIO	(48, "Inscri��o Estadual do Substituo Tribut�rio", false),
	CSTICMS_NOTAITEM		(49, "C.S.T.", true),
	ALIQUOTAICMS_NOTAITEM	(50, "Al�quota ICMS", true),
	UNIDADEMEDIDA_NOTAITEM	(51, "Unidade de Medida", true),
	VALORFRETE_NOTA			(52, "Valor do Frete", false),
	VALORSEGURO_NOTA		(53, "Valor do Seguro", false),
	VALOROUTRASDESPESAS_NOTA(54, "Valor Outras Despesas", false),
	PLACAVEICULO_NOTA		(55, "Placa do Ve�culo", false),
	VENDEDOR_NOTA			(56, "Vendedor", false),
	CONDICAOPAGAMENTO_NOTA	(57, "Condi��o de Pagamento", false),
	RESPONSAVELFRETE_NOTA	(58, "Frete por conta de", false),	
	
	PAGINA_NOTA				(59, "P�gina", false);
	
	
	private Integer value;
	private String descricao;
	private boolean corpo;
	
	private Camponotaproduto(Integer value, String descricao, boolean corpo){
		this.value = value;
		this.descricao = descricao;
		this.corpo = corpo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public boolean isCorpo() {
		return corpo;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Camponotaproduto> getCampos(boolean corpo){
		List<Camponotaproduto> lista = new ArrayList<Camponotaproduto>();
		for (int i = 0; i < Camponotaproduto.values().length; i++) {
			if(corpo){
				if(Camponotaproduto.values()[i].isCorpo()) lista.add(Camponotaproduto.values()[i]);
			} else {
				if(!Camponotaproduto.values()[i].isCorpo()) lista.add(Camponotaproduto.values()[i]);
			}
		}
		
		Collections.sort(lista,new Comparator<Camponotaproduto>(){
			public int compare(Camponotaproduto o1, Camponotaproduto o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		
		return lista;
	}
	
}
