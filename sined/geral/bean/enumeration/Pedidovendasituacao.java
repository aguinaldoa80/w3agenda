package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Pedidovendasituacao {
	
	PREVISTA					(0, "Prevista"),
	CANCELADO					(1, "Cancelada"),
	CONFIRMADO					(2, "Confirmado"),
	CONFIRMADO_PARCIALMENTE 	(3, "Confirmado Parcialmente"),
	AGUARDANDO_APROVACAO		(4, "Aguardando Aprova��o");
//	CONFIRMADO_COM_PENDENCIA	(5, "Confirmado com Pend�ncia");
	
	private Integer value;
	private String nome;
	
	private Pedidovendasituacao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Pedidovendasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Pedidovendasituacao pedidoVenda : Pedidovendasituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Pedidovendasituacao){
					if((situacoes.get(i)).equals(pedidoVenda)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(pedidoVenda.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(pedidoVenda.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
	
	public static String listAndConcatenateDescription(List<Pedidovendasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Pedidovendasituacao s : Pedidovendasituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Pedidovendasituacao){
					if((situacoes.get(i).getValue()) == s.getValue()){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+"\n ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
