package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum StatusUsoEnum {
	
	NOVO					(0, "00", "Novo"),
	USADO					(1, "01", "Usado"),
	RECONDICIONADO			(2, "02", "Recondicionado"),
	REMANUFATURADO			(3, "03", "Remanufaturado"),
	MOSTRUARIO				(4, "04", "Mostruário"),
	DANIFICADO				(5, "05", "Danificado");
	
	private int id;
	private String value;
	private String descricao;
	
	private StatusUsoEnum(int id, String value, String descricao){
		this.id = id;
		this.value = value;
		this.descricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
