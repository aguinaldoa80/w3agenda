package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Documentodigitalsituacao {
	
	EM_ESPERA				(0, "Em espera"),
	ENVIADO					(1, "Enviado"),
	ACEITE_PARCIALMENTE		(2, "Aceite parcialmente"),
	ACEITE					(3, "Aceite"),
	;
	
	private Integer value;
	private String nome;
	
	private Documentodigitalsituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Documentodigitalsituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Documentodigitalsituacao s : Documentodigitalsituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Documentodigitalsituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
