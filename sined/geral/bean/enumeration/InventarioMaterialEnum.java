package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum InventarioMaterialEnum{

	CRIADO					(0, "Criado"),
	ALTERADO				(1, "Alterado"),
	GERARBLOCOK 			(2,"Gerar Bloco K"),
	NAOGERARBLOCOK 			(3,"N�o Gerar Bloco K"),
	GERARBLOCOH 			(4,"Gerar Bloco H"),
	NAOGERARBLOCOH  		(5,"N�o Gerar Bloco H"),
	;
	
	private Integer value;
	private String nome;
	
	private InventarioMaterialEnum(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
