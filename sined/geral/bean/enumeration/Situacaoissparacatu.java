package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaoissparacatu {
	
	TI		(0, 0, "TI", "Tributada Integralmente"),
	TIRF	(1, 1, "TIRF", "Tributada Integralmente com ISSRF"),
	TIST	(2, 2, "TIST", "Tributada Integralmente e sujeita � Substitui��o Tribut�ria"),
	TRBC	(3, 3, "TRBC", "Tributada com redu��o da base de c�lculo"),
	TRBCRF	(4, 4, "TRBCRF", "Tributada com redu��o da base de c�lculo com ISSRF"),
	TRBCST	(5, 5, "TRBCST", "Tributada com redu��o da base de c�lculo e sujeita � Substitui��o Tribut�ria"),
	ISE		(6, 6, "ISE", "Isenta"),
	IMU		(7, 7, "IMU", "Imune"),
	NTIFx	(8, 8, "NTIFx", "N�o Tributada - ISS regime Fixo"),
	NTIEs	(9, 9, "NTIEs", "N�o Tributada - ISS regime Estimativa"),
	NTICc	(10, 10, "NTICc", "N�o Tributada - ISS Constru��o Civil recolhido antecipadamente"),
	NTINa	(11, 11, "NTINa", "N�o Tributada - ISS recolhido por Nota Avulsa"),
	NTPEM	(12, 12, "NTPEM", "N�o Tributada - Prestador estabelecido no Munic�pio"),
	NTREP	(13, 13, "NTREP", "N�o Tributada - Recolhimento efetuado pelo prestador de fora do Munic�pio"),
	NTRIB	(14, 14, "NTRIB", "N�o Tributada"),
	NTAC	(15, 15, "NTAC", "N�o Tributada - Ato Cooperado"),
	PDFC	(16, 99, "PDFC", "Produtos Documento Fiscal Conjugado");
	
	private Integer value;
	private Integer cdnfe;
	private String simbolo;
	private String descricao;
	
	private Situacaoissparacatu(Integer value, Integer cdnfe, String simbolo, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
		this.simbolo = simbolo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getSimbolo() {
		return simbolo;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
