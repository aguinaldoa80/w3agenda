package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoTerceiros {
	NAO_SE_APLICA(1,"N�o se aplica"),
	CONDOMINO(2, "Cond�mino"),
	ARRENDADOR(3, "Arrendador"),
	PARCEIRO(4, "Parceiro"),
	COMODANTE(5, "Comodante"),
	OUTROS(6, "Outros");
	
	private Integer id;
	private String descricao;
	
	private TipoTerceiros(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
