package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tabelaimpostoitemtipo {
	
	PRODUTO			(0, "Produto"),
	SERVICO_NBS		(1, "Servi�o NBS"),
	SERVICO_ITEM	(2, "Item de servi�o");

	private Integer value;
	private String nome;
	
	private Tabelaimpostoitemtipo(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
