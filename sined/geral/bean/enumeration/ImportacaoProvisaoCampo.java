package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ImportacaoProvisaoCampo {
	MATRICULA(1, "Matrícula"),
	CNPJ(2, "Cnpj");
	
	private Integer id;
	private String descricao;
	
	private ImportacaoProvisaoCampo(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
