package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocalculo {
	
	PERCENTUAL	(0, "Percentual"),
	EM_VALOR	(1, "Em valor");
	
	private Integer value;
	private String descricao;
	
	private Tipocalculo(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}