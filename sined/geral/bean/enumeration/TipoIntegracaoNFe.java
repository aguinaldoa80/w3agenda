package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoIntegracaoNFe {
	
	EXIGIVEL				(1, "Pagamento integrado com o sistema de automa��o da empresa"),
	NAO_INCIDENCIA			(2, "Pagamento n�o integrado com o sistema de automa��o da empresa");
	
	private Integer value;
	private String nome;
	
	private TipoIntegracaoNFe(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
