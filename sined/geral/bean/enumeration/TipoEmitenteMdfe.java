package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoEmitenteMdfe {

	PRESTADOR_SERVICO_TRANSPORTE(1, "1 - Prestador de Servi�o de Transporte"),
	NAO_PRESTADOR_SERVICO_TRANSPORTE(2, "2 - N�o Prestador de Servi�o de Transporte"),
	PRESTADOR_SERVICO_TRANSPORTE_COM_CTE_GLOBALIZADO(3, "3 - Prestador de Servi�o de Transporte com CTe Globalizado");
	
	private Integer value;
	private String nome;
	
	private TipoEmitenteMdfe(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
