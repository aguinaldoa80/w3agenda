package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum CategoriaClienteLogin {
	
	ATACADISTAS_REPRESENTANTES			(0, "Atacadistas e Representantes", "fundo_distribuidora.jpg"),
	CONTRUTORAS_ENGENHARIA_ARQUITETURA	(1, "Construtoras, engenharia e arquitetura", "fundo_construtora.jpg"),
	FABRICAS_INDUSTRIAS					(2, "F�bricas e ind�strias", "fundo_fabrica.jpg"),
	SERVICOS							(3, "Servi�os", "fundo_servicos.jpg"),
	REFORMADORA_PNEU					(4, "Reformadora de Pneu", "fundo_pneu.jpg"),
	AGRICULTURA							(5, "Agricultura", "fundo_agricultura.jpg")
	;

	private Integer value;
	private String nome;
	private String imagem;
	
	private CategoriaClienteLogin(Integer _value, String _nome, String _imagem) {
		this.value = _value;
		this.nome = _nome;
		this.imagem = _imagem;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getImagem() {
		return imagem;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
