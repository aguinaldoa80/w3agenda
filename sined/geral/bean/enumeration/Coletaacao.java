package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Coletaacao {
	
	CRIADO				(0, "Criado"),
	DEVOLVIDO			(1, "Devolvido"),
	ALTERADO			(2, "Alterado"),
	CONFIRMADO			(3, "Confirmado"),
	COMPRA_CLIENTE 		(4, "Compra realizada"),
	ENTRADA_ESTOQUE		(5, "Entrada no estoque"),
	ESTORNO_DEVOLUCAO	(6, "Estorno de devolução"),
	PAGAMENTO			(7, "Pagamento realizado"),
	NOTA_EMITIDA		(8, "Nota emitida"),
	GARANTIA			(9, "Garantia"),
	GARANTIA_NEGADA		(10, "Garantia negada"),
	CONSTATACAO			(11, "Constatação"),
	ENTREGA            	(12,"Entrega"),
	CANCELAMENTO_ENTREGA(13,"Cancelamento entrega");
	
	private Integer value;
	private String nome;
	
	private Coletaacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
