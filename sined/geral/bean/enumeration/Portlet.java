package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Portlet {

	TWITTER_LINKCOM	("Redes Sociais", "facebook_linkcom.jsp"),						
	FEED_RSS		("Artigos", "feedRSS.jsp"),
	AVISOS			("Avisos", "avisosequipe.jsp")
	;
	
	private Portlet (String nome, String pagina){
		this.nome = nome;
		this.pagina = pagina;
	}
	
	private String nome;
	private String pagina;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getPagina() {
		return pagina;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}	
	
}
