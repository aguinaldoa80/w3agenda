package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tarefasituacao {

	EM_ESPERA		(0, "Em Espera"),
	EM_ANDAMENTO	(1, "Em Andamento"),
	ATRASADA		(2, "Atrasada"),
	CONCLUIDA		(3, "Conclu�da");
	
	private Integer value;
	private String nome;	
	
	private Tarefasituacao(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Tarefasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Tarefasituacao s : Tarefasituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Tarefasituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
		
}
