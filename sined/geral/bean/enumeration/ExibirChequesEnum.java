package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ExibirChequesEnum {
	
	RECEBIDOS		(0, "Recebidos"),
	EMITIDOS		(1, "Emitidos"),
	TODOS			(2, "Todos");

	private Integer value;
	private String nome;
	
	private ExibirChequesEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}