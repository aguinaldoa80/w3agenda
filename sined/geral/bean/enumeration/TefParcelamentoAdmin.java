package br.com.linkcom.sined.geral.bean.enumeration;

public enum TefParcelamentoAdmin {
	
	PERGUNTAR		("Perguntar no Pinpad"),
	LOJISTA			("Lojista"),
	ADMINISTRADORA  ("Administradora");
	
	private String descricao;	
	
	private TefParcelamentoAdmin(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}

}