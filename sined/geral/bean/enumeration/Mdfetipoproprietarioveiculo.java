package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Mdfetipoproprietarioveiculo {

	TAC_AGREGADO(0, "0 - TAC Agregado"),
	TAC_INDEPENDENTE(1, "1 - TAC Independente"),
	OUTROS(2, "2 - Outros");
	
	private Integer value;
	private String nome;
	
	private Mdfetipoproprietarioveiculo(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
