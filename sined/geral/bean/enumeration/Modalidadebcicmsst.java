package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Modalidadebcicmsst {
	
	PRECO_TABELADO_MAXIMO_SUGERIDO	(0, "Pre�o tabelado ou m�ximo sugerido"),
	LISTA_NEGATIVA					(1, "Lista Negativa (valor)"),
	LISTA_POSITIVA					(2, "Lista Positiva (valor)"),
	LISTA_NEUTRA					(3, "Lista Neutra (valor)"),
	MARGEM_VALOR_AGREGADO			(4, "Margem Valor Agregado (%)"),
	PAUTA							(5, "Pauta (valor)"),
	VALOR_OPERACAO					(6, "Valor da Opera��o");
	;
	
	private Integer value;
	private String descricao;
	
	private Modalidadebcicmsst(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
