package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;


public enum Agendainteracaosituacao {
	
	NORMAL 		(0, "Normal"),
	ATENCAO 	(1, "Aten��o"),
	ATRASADO	(2, "Atrasado"),
	CANCELADO 	(3, "Cancelado"),
	CONCLUIDO	(4, "Conclu�do");
	
	
	private Integer value;
	private String nome;
	
	
	private Agendainteracaosituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	

	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
