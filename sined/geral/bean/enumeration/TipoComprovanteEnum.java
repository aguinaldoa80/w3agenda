package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoComprovanteEnum {

	ORCAMENTO(0, "Or�amento"),
	PEDIDO_DE_VENDA(1, "PedidoVenda");
	
	private TipoComprovanteEnum (Integer value, String tipo){
		this.tipo = tipo;
		this.value = value;
	}
	
	private Integer value;
	private String tipo;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}
}