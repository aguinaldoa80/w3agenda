package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MovimentacaoFinanceiraVinculoEnum {
	TODAS(1, "Todas"),
	COM_ORIGEM_DOCUMENTO(2, "Movimentações com vínculo em Documento"),
	SEM_ORIGEM_DOCUMENTO(3, "Movimentações sem vínculo em Documento");
	
	private Integer id;
	private String descricao;
	
	private MovimentacaoFinanceiraVinculoEnum(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
