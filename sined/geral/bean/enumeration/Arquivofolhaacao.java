package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivofolhaacao {

	CRIADO		("Criado"), 			// 0 
	ALTERADO	("Alterado"),			// 1
	AUTORIZADO	("Autorizado"),       	// 2
	ESTORNADO	("Estornado"),			// 3
	PROCESSADO	("Processado"),			// 4
	CANCELADO	("Cancelado");			// 5
	
	private Arquivofolhaacao (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
