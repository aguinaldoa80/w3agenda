package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocalculodias {
	
	POS_VENCIMENTO	 (0,"Dias ap�s o vencimento"),
	ANTES_VENCIMENTO (1,"Dias antes do vencimento");
	
	private Tipocalculodias (Integer codigo){
		this.codigo = codigo;
	}
	
	private Tipocalculodias (Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	private Integer codigo;
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
