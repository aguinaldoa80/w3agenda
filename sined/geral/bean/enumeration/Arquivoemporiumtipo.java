package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivoemporiumtipo {

	IMPORTACAO_PRODUTOS					("Importação de produtos"), 
	IMPORTACAO_PEDIDOVENDA				("Importação de pedido de venda"), 
	EXPORTACAO_DADOSFISCAISGERAIS 		("Exportação de dados ficais gerais"),
	EXPORTACAO_DADOSFISCAISTRIBUTACAO 	("Exportação de dados ficais por tributação"),
	EXPORTACAO_MOVIMENTODETALHADO		("Exportação dos movimentos detalhados"),
	IMPORTACAO_CLIENTES					("Importação de clientes"), 
	EXPORTACAO_CLIENTES					("Exportação de clientes"),
	;
	
	private Arquivoemporiumtipo (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Integer getOrdinal() {
		return this.ordinal();
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
