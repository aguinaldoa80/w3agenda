package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Mes {

	JANEIRO("Janeiro"), 		// 0 
	FEVEREIRO("Fevereiro"), 	// 1
	MARCO("Mar�o"),				// 2
	ABRIL("Abril"),				// 3
	MAIO("Maio"),				// 4
	JUNHO("Junho"),				// 5
	JULHO("Julho"),				// 6
	AGOSTO("Agosto"),			// 7
	SETEMBRO("Setembro"),		// 8
	OUTUBRO("Outubro"),			// 9
	NOVEMBRO("Novembro"),		// 10
	DEZEMBRO("Dezembro");		// 11
	
	private Mes (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
