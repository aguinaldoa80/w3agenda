package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocobrancacofins {
	
	OPERACAO_01 (0, "01", "01 - Opera��o Tribut�vel com Al�quota B�sica"),
	OPERACAO_02 (1, "02", "02 - Opera��o Tribut�vel com Al�quota Diferenciada"),
	OPERACAO_03 (2, "03", "03 - Opera��o Tribut�vel com Al�quota por Unidade de Medida de Produto"),
	OPERACAO_04 (3, "04", "04 - Opera��o Tribut�vel Monof�sica - Revenda a Al�quota Zero"),
	OPERACAO_05 (4, "05", "05 - Opera��o Tribut�vel por Substitui��o Tribut�ria"),
	OPERACAO_06 (5, "06", "06 - Opera��o Tribut�vel a Al�quota Zero"),
	OPERACAO_07 (6, "07", "07 - Opera��o Isenta da Contribui��o"),
	OPERACAO_08 (7, "08", "08 - Opera��o sem Incid�ncia da Contribui��o"),
	OPERACAO_09 (8, "09", "09 - Opera��o com Suspens�o da Contribui��o"),
	OPERACAO_49 (9, "49", "49 - Outras Opera��es de Sa�da"),
	OPERACAO_50 (10, "50", "50 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	OPERACAO_51 (11, "51", "51 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno"),
	OPERACAO_52 (12, "52", "52 - Opera��o com Direito a Cr�dito - Vinculada Exclusivamente a Receita de Exporta��o"),
	OPERACAO_53 (13, "53", "53 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno"),
	OPERACAO_54 (14, "54", "54 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_55 (15, "55", "55 - Opera��o com Direito a Cr�dito - Vinculada a Receitas N�o Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_56 (16, "56", "56 - Opera��o com Direito a Cr�dito - Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_60 (17, "60", "60 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	OPERACAO_61 (18, "61", "61 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita N�o-Tributada no Mercado Interno"),
	OPERACAO_62 (19, "62", "62 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada Exclusivamente a Receita de Exporta��o"),
	OPERACAO_63 (20, "63", "63 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno"),
	OPERACAO_64 (21, "64", "64 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_65 (22, "65", "65 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas N�o-Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_66 (23, "66", "66 - Cr�dito Presumido - Opera��o de Aquisi��o Vinculada a Receitas Tributadas e N�o-Tributadas no Mercado Interno e de Exporta��o"),
	OPERACAO_67 (24, "67", "67 - Cr�dito Presumido - Outras Opera��es"),
	OPERACAO_70 (25, "70", "70 - Opera��o de Aquisi��o sem Direito a Cr�dito"),
	OPERACAO_71 (26, "71", "71 - Opera��o de Aquisi��o com Isen��o"),
	OPERACAO_72 (27, "72", "72 - Opera��o de Aquisi��o com Suspens�o"),
	OPERACAO_73 (28, "73", "73 - Opera��o de Aquisi��o a Al�quota Zero"),
	OPERACAO_74 (29, "74", "74 - Opera��o de Aquisi��o sem Incid�ncia da Contribui��o"),
	OPERACAO_75 (30, "75", "75 - Opera��o de Aquisi��o por Substitui��o Tribut�ria"),
	OPERACAO_98 (31, "98", "98 - Outras Opera��es de Entrada"),
	OPERACAO_99 (32, "99", "99 - Outras Opera��es");

	private Integer value;
	private String descricao;
	private String cdnfe;
	
	private Tipocobrancacofins(Integer value, String cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static Tipocobrancacofins getTipocobrancacofins(String string){
		Tipocobrancacofins[] values = Tipocobrancacofins.values();
		for (int i = 0; i < values.length; i++) {
			if (string.equals(values[i].getCdnfe())){
				return values[i];
			}
		}
		return null;
	}
	
	public static boolean isCreditoCofins(Tipocobrancacofins cstcofins) {
		boolean cstcofinsCredito = cstcofins != null && (
				cstcofins.getCdnfe().equals("50") ||
				cstcofins.getCdnfe().equals("51") ||
				cstcofins.getCdnfe().equals("53") ||
				cstcofins.getCdnfe().equals("54") ||
				cstcofins.getCdnfe().equals("55") ||
				cstcofins.getCdnfe().equals("56") ||
				cstcofins.getCdnfe().equals("60") ||
				cstcofins.getCdnfe().equals("61") ||
				cstcofins.getCdnfe().equals("62") ||
				cstcofins.getCdnfe().equals("63") ||
				cstcofins.getCdnfe().equals("64") ||
				cstcofins.getCdnfe().equals("65") ||
				cstcofins.getCdnfe().equals("66") 
		);
		return cstcofinsCredito;
	}
	
	public static boolean isDebitoCofins(Tipocobrancacofins cstcofins) {
		boolean cstcofinsDebito = cstcofins != null && (
				cstcofins.getCdnfe().equals("01") ||
				cstcofins.getCdnfe().equals("02") ||
				cstcofins.getCdnfe().equals("03") ||
				cstcofins.getCdnfe().equals("05") 
				);
		return cstcofinsDebito;
	}

}
