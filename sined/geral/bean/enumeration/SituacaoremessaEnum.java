package br.com.linkcom.sined.geral.bean.enumeration;


public enum SituacaoremessaEnum {

	TODOS("Todos"),
	NAO_ENVIADO("N�o enviado"),
	ENVIADO("Enviado"),
	CANCELADO("Cancelado");
	
	private String nome;
	
	private SituacaoremessaEnum(String nome){
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.getNome();
	}
}
