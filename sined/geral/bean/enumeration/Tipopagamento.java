package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipopagamento {

	CLIENTE("Cliente"), 		// 0 
	COLABORADOR("Colaborador"), // 1
	FORNECEDOR("Fornecedor"),	// 2
	OUTROS("Outros");			// 3
	
	private Tipopagamento (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
