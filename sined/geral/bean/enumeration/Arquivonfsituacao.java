package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivonfsituacao {
	
	GERADO					(0, "Gerado"),
	ENVIADO					(1, "Enviado"),
	NAO_ENVIADO				(2, "N�o enviado"),
	PROCESSADO_COM_ERRO		(3, "Processado com erro"),
	PROCESSADO_COM_SUCESSO	(4, "Processado com sucesso"),
	GERADO_PARA_SCAN		(5, "Gerado para SCAN"),
	;
	
	private Integer value;
	private String nome;
	
	private Arquivonfsituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Arquivonfsituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Arquivonfsituacao s : Arquivonfsituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Arquivonfsituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
