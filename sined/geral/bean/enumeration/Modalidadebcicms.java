package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Modalidadebcicms {
	
	MARGEM_VALOR_AGREGADO	(0, "Margem Valor Agregado"),
	PAUTA					(1, "Pauta (valor)"),
	PRECO_TABELADO_MAX		(2, "Pre�o Tabelado M�x. (valor)"),
	VALOR_OPERACAO			(3, "Valor da Opera��o");
	
	private Integer value;
	private String descricao;
	
	private Modalidadebcicms(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
