package br.com.linkcom.sined.geral.bean.enumeration;


public enum Custooperacionaltipo {

	CUSTO_OPERACIONAL	(0, "Custo Operacional"),
	CUSTO_COMERCIAL		(1, "Custo Comercial")
	; 
	
	private Integer value;
	private String descricao;
		
	private Custooperacionaltipo(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}

	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
