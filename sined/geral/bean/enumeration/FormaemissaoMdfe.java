package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaemissaoMdfe {

	NORMAL(1, "1 - Normal"),
	CONTINGENCIA(2, "2 - Contingência");
	
	private Integer value;
	private String nome;
	
	private FormaemissaoMdfe(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
