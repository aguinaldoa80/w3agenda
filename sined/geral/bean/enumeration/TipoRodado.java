package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoRodado {

	TRUCK				(0, "01", "01 - Truck"),
	TOCO				(1, "02", "02 - Toco"),
	CAVALO_MECANICO		(2, "03", "03 - Cavalo Mec�nico"),
	VAN					(3, "04", "04 - VAN"),
	UTILITARIO			(4, "05", "05 - Utilit�rio"),
	OUTROS				(5, "06", "06 - Outros");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private TipoRodado(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
