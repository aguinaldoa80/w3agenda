package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Horimetrotipoajuste {
	
	MARCHA_RE		(0,"Marcha r�"),
	TROCA_HORIMETRO	(1,"Troca de hor�metro"),
	OUTROS			(2,"Outros"),
	INICIAL			(3,"Inicial");
	
	protected Integer value;
	protected String nome;
	
	private Horimetrotipoajuste(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setValue(Integer value) {
		this.value = value;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	@Override
	public String toString() {
		return getNome();
	}
}
