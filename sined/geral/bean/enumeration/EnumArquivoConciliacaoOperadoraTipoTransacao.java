package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Tipo de transa��o")
public enum EnumArquivoConciliacaoOperadoraTipoTransacao {
	VENDA(1,"Venda"),
	AJUSTE_A_CREDITO(2,"Ajuste a cr�dito"),
	AJUSTE_A_DEBITO(3,"Ajuste a d�bito"),
	PACOTE_CIELO(4,"Pacote cielo"),
	REAGENDAMENTO(5,"Reagendamento");
	
	private Integer codigo;
	private String descricao;
	
	private EnumArquivoConciliacaoOperadoraTipoTransacao(Integer inicio, String descricao) {
		this.codigo = inicio;
		this.descricao = descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public static EnumArquivoConciliacaoOperadoraTipoTransacao getEnumTipoTransacao(Integer codigo){
		for (EnumArquivoConciliacaoOperadoraTipoTransacao tipoTransacao : EnumArquivoConciliacaoOperadoraTipoTransacao.values()){
			if (tipoTransacao.getCodigo().equals(codigo)) {
				return tipoTransacao;
			} 		
		}
		return null;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
