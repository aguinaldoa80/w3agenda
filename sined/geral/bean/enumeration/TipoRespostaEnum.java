package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoRespostaEnum {
	
	DESCRITIVO(0, "Descritivo"),
	SIM_NAO(1, "Sim/N�o"),
	OUTRAS_RESPOSTAS(2, "Outras Respostas");
	
	private Integer idSitucao;
	private String desricao;
	
	private TipoRespostaEnum(Integer idSitucao, String descricao){
		this.idSitucao = idSitucao;
		this.desricao = descricao;
	}

	public Integer getIdSitucao() {
		return idSitucao;
	}

	public void setIdSitucao(Integer idSitucao) {
		this.idSitucao = idSitucao;
	}

	public String getDesricao() {
		return desricao;
	}

	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}

}
