package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoContaPagar {

	PREVISTA					(0, "Prevista"),
	DEFINITIVA					(1, "Definitiva"),
	AUTORIZADA					(2, "Autorizada"),
	AUTORIZADA_PARCIAL 			(3, "Autorizada Parcialmente"),
	NEGOCIADA					(4, "Negociada");

	
	private Integer value;
	private String nome;
	
	private SituacaoContaPagar(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
