package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AjusteIcmsOperacao {

	ENTRADA	(0, "Entrada"), 
	SAIDA	(1, "Sa�da"); 
	
	private Integer id;
	private String nome;
		
	private AjusteIcmsOperacao(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
