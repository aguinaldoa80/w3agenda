package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Materialacao {
	
	CRIADO					(0, "Criado"),
	ALTERADO				(1, "Alterado"),
	ALTERADO_ENTRADAFISCAL	(2, "Alterado via Recebimento"),
	RECALCULADO_CUSTO_MEDIO (3, "Recalcular Custo M�dio"),
	DEFINIDO_CUSTO_MEDIO 	(4, "Definir Custo Inicial")
	;
	
	private Integer value;
	private String nome;
	
	private Materialacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
