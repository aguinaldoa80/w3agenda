package br.com.linkcom.sined.geral.bean.enumeration;

public enum Documentocomissaotipoperiodo {
	
	VENCIMENTO			(1,"Vencimento"),
	PAGAMENTO			(2,"Pagamento"),
	EMISSAO				(3,"Emiss�o");
	
	private Integer value;
	private String desricao;
	
	private Documentocomissaotipoperiodo(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
