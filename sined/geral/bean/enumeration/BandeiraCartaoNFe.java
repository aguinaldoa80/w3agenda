package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum BandeiraCartaoNFe {
	
	VISA			(1, "Visa", new String[]{"VISA"}),
	MASTERCARD		(2, "Mastercard", new String[]{"MASTERCARD"}),
	AMERICAN_EXPRESS(3, "American Express", new String[]{"AMEX"}),
	SOROCRED		(4, "Sorocred", new String[]{"SOROCRED"}),
	DINERS_CLUB		(5, "Diners Club", new String[]{"DINERS"}),
	ELO				(6, "Elo", new String[]{"ELO"}),
	HIPERCARD		(7, "Hipercard", new String[]{"HIPERCARD"}),
	AURA			(8, "Aura", new String[]{"AURA"}),
	CABAL			(9, "Cabal", new String[]{"CABAL"}),
	OUTROS			(99, "Outros", new String[]{}),
	;
	
	
	private Integer value;
	private String nome;
	private String[] arrayIntegracao;
	
	private BandeiraCartaoNFe(Integer value, String nome, String[] arrayIntegracao){
		this.value = value;
		this.nome = nome;
		this.arrayIntegracao = arrayIntegracao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static BandeiraCartaoNFe getByNameIntegracao(String name){
		BandeiraCartaoNFe[] values = BandeiraCartaoNFe.values();
		for (BandeiraCartaoNFe it : values) {
			for (String nameIt : it.arrayIntegracao) {
				if(nameIt.equalsIgnoreCase(name)){
					return it;
				}
			} 
		}
		return null;
	}
	
}
