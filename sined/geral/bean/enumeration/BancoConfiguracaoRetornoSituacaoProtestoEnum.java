package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRetornoSituacaoProtestoEnum {

	ENVIADO_CARTORIO("Enviado ao cart�rio"),
	ENVIADO_JURIDICO("Enviado ao jur�dico"),
	PROTESTADA("Protestada");	
		
	private String nome;
		
	private BancoConfiguracaoRetornoSituacaoProtestoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
		
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}
		
}
