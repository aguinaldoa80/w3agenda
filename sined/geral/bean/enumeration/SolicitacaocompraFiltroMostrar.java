package br.com.linkcom.sined.geral.bean.enumeration;

public enum SolicitacaocompraFiltroMostrar {
	
	SOLICITACOES_ATRASADAS ("Solicitações atrasadas"),
	SOLICITACOES_PENDENTES ("Solicitações pendentes");
	
	private String descricao;
	
	private SolicitacaocompraFiltroMostrar(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
