package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tipobuscaconta {

	VENCIMENTO			(1,"Vencimento"),
	EMISSAO				(2,"Emiss�o");
	
	private Integer cdtipobuscaconta;
	private String descricao;
	
	private Tipobuscaconta(Integer cdtipobuscaconta, String descricao){
		this.cdtipobuscaconta = cdtipobuscaconta;
		this.descricao = descricao;
	}
	
	public Integer getCdtipobuscaconta() {
		return cdtipobuscaconta;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdtipobuscaconta(Integer cdtipobuscaconta) {
		this.cdtipobuscaconta = cdtipobuscaconta;
	}
	public void setNome(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	
}
