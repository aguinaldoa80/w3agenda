package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRemessaTipoEnum {

	COBRANCA("Cobran�a"),
	PAGAMENTO("Pagamento"),
	CHEQUE("Cheque"); 
	
	private String nome;
		
	private BancoConfiguracaoRemessaTipoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
	
}
