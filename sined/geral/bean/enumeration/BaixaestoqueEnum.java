package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum BaixaestoqueEnum {
	
	APOS_EMISSAONOTA			(0, "Ap�s emiss�o de nota"),
	APOS_VENDAREALIZADA			(1, "Ap�s venda realizada"),
	NAO_BAIXAR					(2, "N�o baixar"),
	APOS_EXPEDICAOCONFIRMADA	(3, "Ap�s expedi��o confirmada"),
	APOS_EXPEDICAOWMS			(4, "Ap�s expedi��o no WMS");

	private Integer value;
	private String nome;
	
	private BaixaestoqueEnum(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
