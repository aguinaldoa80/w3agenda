package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum IndicadorInicioDoPeriodo {
	REGULAR(0, "Regular (In�cio no primeiro dia do ano)"),
	ABERTURA(1, "Abertura (In�cio de atividades no ano-calend�rio)"),
	INICIO_OBRIGATORIEDADE(2, "In�cio de obrigatoriedade da escritura��o no curso do ano calend�rio");
	
	private Integer id;
	private String descricao;
	
	private IndicadorInicioDoPeriodo(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
