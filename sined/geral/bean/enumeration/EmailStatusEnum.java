package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;


public enum EmailStatusEnum {
	
	ENVIADO 		(1, "Enviado"),
	ENTREGUE 		(2, "Entregue"),
	LIDO 			(3, "Lido"),
	CAIXA_DE_SPAM 	(4, "Caixa de Spam"),
	EMAIL_INVALIDO 	(5, "E-mail Inv�lido");
//	NAO_ENVIADO 	(6, "N�o Enviado");
	
	private Integer valor;
	private String name;
	
	private EmailStatusEnum(Integer valor, String name){
		this.valor = valor;
		this.name = name;
	}
	
	public Integer getValor(){
		return valor;
	}
	@DescriptionProperty
	public String getName() {
		return name;
	}
	@Override
	public String toString() {	
		return name;
	}
	
	public static EmailStatusEnum fromSendgridEvent(SendgridEvent event) {
		switch(event){
			case BOUNCE:
				return EMAIL_INVALIDO;

			case DROPPED:
				return EMAIL_INVALIDO;
				
			case DELIVERED:
				return ENTREGUE;

			case OPEN:
				return LIDO;
				
			case CLICK:
				return LIDO;
				
			case DEFERRED:
				return ENVIADO;

			case SPAMREPORT:
				return CAIXA_DE_SPAM;
				
			case REQUEST:
				return ENVIADO;
				
			case HARD_BOUNCE:
				return EMAIL_INVALIDO;
				
			case SOFT_BOUNCE:
				return EMAIL_INVALIDO;
				
			case BLOCKED:
				return EMAIL_INVALIDO;
				
			case SPAM:
				return CAIXA_DE_SPAM;
				
			case INVALID_EMAIL:
				return EMAIL_INVALIDO;
				
			case OPENED:
				return LIDO;
				
			case UNSUBSCRIBE:
				return CAIXA_DE_SPAM;
				
			case UNIQUE_OPENED:
				return LIDO;

			default:
				throw new RuntimeException("Status n�o previsto: " + event);
		}
	}
	
	public static String listAndConcatenate(List<EmailStatusEnum> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (EmailStatusEnum s : EmailStatusEnum.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof EmailStatusEnum){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.ordinal()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
}
