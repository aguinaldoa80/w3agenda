package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum InventarioAcao {
	
	CRIADO		(0, "Criado"),
	AUTORIZADO	(1, "Autorizado"),
	AJUSTADO	(2, "Ajustado"),
	CANCELADO	(3, "Cancelado"),
	ESTORNADO	(4, "Estornado"),
	ALTERADO	(5, "Alterado"),
	;
	
	private int value;
	private String nome;
	
	InventarioAcao(){}
	
	private InventarioAcao(int value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	
	public int getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
