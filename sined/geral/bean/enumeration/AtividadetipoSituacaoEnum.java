package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AtividadetipoSituacaoEnum {

	POUCO_CRITICA(0, "Pouco cr�tica"), 
	CRITICA(1, "Cr�tica"), 
	MUITO_CRITICA(2, "Muito cr�tica"), 
	NAO_PROCEDENTE(3, "N�o procedente"); 
	
	private Integer id;
	private String nome;
		
	private AtividadetipoSituacaoEnum(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
