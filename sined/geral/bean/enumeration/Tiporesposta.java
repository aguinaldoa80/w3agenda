package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tiporesposta {
	
	TEXTO 	(0, "Texto"),
	SIM_NAO (1, "Sim/N�o");
	
	private Integer value;
	private String descricao;
	
	private Tiporesposta(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
}
