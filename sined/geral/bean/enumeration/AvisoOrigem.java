package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AvisoOrigem {

	CADASTRO(0,"Cadastro"), 		
	SOLICITACAO_DE_COMPRA(1,"Solicita��o de compra"), 
	COTACAO(2,"Cota��o"),	
	ORDEM_DE_COMPRA(3,"Ordem de compra"),
	ENTREGA(4,"Entrega"),
	ROMANEIO(5,"Romaneio"),
	AGENDAMENTO(6,"Agendamento"),
	CONTA_A_PAGAR(7,"Conta a pagar"),			
	PLANEJAMENTO(8,"Planejamento"),
	PEDIDOVENDA(9,"Pedido de venda"),
	RECEBIMENTO(10, "Recebimento"),
	LOTE_NOTA_FISCAL(11, "Lote NF-e"),
	ESTOQUE(12, "Estoque"),
	CONTA_A_RECEBER(13, "Conta a receber"),
	VENDA(14, "Venda"),
	NOTA_FISCAL(15, "Nota Fiscal"),
	COLETA(16, "Coleta"),
	FATURA_LOCACAO(17, "Fatura de loca��o"),
	FORNECIMENTO(18, "Fornecimento"),
	ENTRADA_FISCAL(19, "Entrada fiscal"),
	AGENDA_INTERACAO(20, "Agenda de intera��o"),
	ORDEM_SERVICO(21, "Ordem de servi�o"),
	SPED_FISCAL(22, "SPED Fiscal"),
	EFD_CONTRIBUICOES(23, "EFD Contribui��es"),
	CONCILIACAO_BANCARIA(24, "Concilia��o Banc�ria"),
	FECHAMENTO_FINANCEIRO(25, "Fechamento financeiro"),
	REQUISICAO_MATERIAL(26, "Requisi��o de material"),
	SOLICITACAO_SERVICO(27, "Solicita��o de servi�o");
	
	private AvisoOrigem (Integer codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}
	
	private Integer codigo;
	private String nome;
	
	public Integer getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
	public static List<AvisoOrigem> getListaOrdenada(){
		List<AvisoOrigem> list = Arrays.asList(AvisoOrigem.values());
		Collections.sort(list, new Comparator<AvisoOrigem>() {
	            @Override
	            public int compare(AvisoOrigem o1, AvisoOrigem o2) {
	                return o1.getNome().compareTo(o2.getNome());
	            }
	        });
		return list;
	}
}
