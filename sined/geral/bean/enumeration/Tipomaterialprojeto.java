package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tipomaterialprojeto {

	PROJETO							(0,"Projeto"),
	PRODUTO_SERVICO		(1,"Produto/Servi�o");
	
	private Integer cdvendedortipo;
	private String descricao;	
	
	private Tipomaterialprojeto(Integer cdvendedortipo, String descricao) {
		this.cdvendedortipo = cdvendedortipo;
		this.descricao = descricao;
	}
	
	
	public Integer getCdvendedortipo() {
		return cdvendedortipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdvendedortipo(Integer cdvendedortipo) {
		this.cdvendedortipo = cdvendedortipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
	
}
