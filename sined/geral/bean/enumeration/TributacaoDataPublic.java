package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TributacaoDataPublic {

	TRIBUTAVEL 				(1, "Tribut�vel"),
	SUSPENSA_JUDICIAL 		(2, "Exigibilidade Suspensa Dec. Jud."),
	SUSPENSA_ADMINISTRATIVO (3, "Exigibilidade Suspensa Proc. Adm."),
	TRIBUTAVEL_SN 			(5, "Tribut�vel Simples Nacional"),
	ISENTO 					(6, "Isento ISS"),
	NAO_INCIDENCIA 			(7, "N�o Incid�ncia no Munic�pio"),
	TRIBUTAVEL_MEI 			(8, "Tribut�vel MEI"),
	IMUNE 					(9, "Imune");
	
	private Integer value;
	private String nome;
	
	private TributacaoDataPublic(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
