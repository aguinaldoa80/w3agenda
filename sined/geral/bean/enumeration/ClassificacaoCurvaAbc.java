package br.com.linkcom.sined.geral.bean.enumeration;

public enum ClassificacaoCurvaAbc {

	VALOR("Valor"),
	QUANTIDADE("Quantidade"),
	MARKUP_CUSTO("Markup de Custo"),
	MARKUP_VENDA("Markup de Venda"),
	LUCRO("Lucro");
	
	private String nome;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	private ClassificacaoCurvaAbc(String nome){
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.getNome();
	}
}
