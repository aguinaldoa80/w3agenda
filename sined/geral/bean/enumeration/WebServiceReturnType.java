package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum WebServiceReturnType {
	
	XML	(0, "xml"),
	JSON(1, "json");
	
	private Integer value;
	private String descricao;
	
	private WebServiceReturnType(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public boolean equals(String descricao){
		if(descricao != null && this.descricao.equalsIgnoreCase(descricao) )
			return true;
		else return false;
	}

	public static String getDefault(){
		return WebServiceReturnType.XML.toString();
	}
}
