package br.com.linkcom.sined.geral.bean.enumeration;

public enum NotaerrocorrecaoHistoricoEnum {
	
	CRIADO		(0,"Criado"),
	ALTERADO	(1,"Alterado");
	
	private Integer value;
	private String nome;
	
	private NotaerrocorrecaoHistoricoEnum (Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getNome() {
		return nome;
	}
	@Override
	public String toString() {
		return this.getNome();
	}
}

