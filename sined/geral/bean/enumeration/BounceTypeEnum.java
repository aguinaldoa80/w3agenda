package br.com.linkcom.sined.geral.bean.enumeration;

public enum BounceTypeEnum {
	BOUNCE(1, "bounce"),
	BLOCKED(2, "blocked"),
	EXPIRED(3, "expired");
	
	private Integer valor;
	private String nome;
	
	private BounceTypeEnum(Integer valor, String nome){
		this.valor = valor;
		this.nome = nome;
	}

	public Integer getValor() {
		return valor;
	}

	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {		
		return nome;
	}

}
