package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

public enum Comissionamentotipo {

	CONTRATO		(0,"Contrato"),
	DESEMPENHO		(1,"Desempenho"),
	VENDA			(2,"Venda");
	
	private Integer value;
	private String descricao;	
	
	private Comissionamentotipo(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	
	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
	
	public static String listAndConcatenate(List<Comissionamentotipo> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Comissionamentotipo c : Comissionamentotipo.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Comissionamentotipo){
					if((situacoes.get(i)).equals(c)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(c.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(c.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
}
