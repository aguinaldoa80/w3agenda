package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Chequesituacao {
	
	PREVISTO		(0, "Previsto"),
	DEVOLVIDO		(1, "Devolvido"),
	BAIXADO			(2, "Baixado"),
	CANCELADO		(3, "Cancelado"),
	SUBSTITUIDO		(4, "Substituído"),
	COMPENSADO		(5, "Compensado");

	private Integer value;
	private String nome;
	
	private Chequesituacao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Chequesituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Chequesituacao chequeSituacao : Chequesituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Chequesituacao){
					if((situacoes.get(i)).equals(chequeSituacao)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(chequeSituacao.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(chequeSituacao.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
}
