package br.com.linkcom.sined.geral.bean.enumeration;

public enum ImpostoEcfEnum {

	FEDERAL,
	ESTADUAL,
	MUNICIPAL;
}
