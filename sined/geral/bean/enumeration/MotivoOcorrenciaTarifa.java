package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MotivoOcorrenciaTarifa {
		
	COB_LOTERI	(2, "COB LOTERI"),	
	COB_AGENC 	(3, "COB AGENC"),	
	COB_COMPE 	(4, "COB COMPE"),	
	COB_INTERN 	(6, "COB INTERN");	
	
	private MotivoOcorrenciaTarifa (Integer codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}
	
	private Integer codigo;
	private String nome;
	
	public Integer getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
	/**
	 * Retorna a nome do motivo de tarifa a partir do c�digo obtido no arquivo CEF.
	 * @param motivoTarifa
	 * @return
	 * @author Taidson
	 * @since 19/08/2010
	 */
	public static String getDescricaoMotivoTarifa(String motivoTarifa){
		String observacao = null;
		switch(Integer.parseInt(motivoTarifa)){
		case 6:
			observacao = COB_INTERN.getNome();
			break;
		case 2:
			observacao = COB_LOTERI.getNome();
			break;
		case 3:
			observacao = COB_AGENC.getNome();
			break;
		case 4:
			observacao = COB_COMPE.getNome();
			break;
		default:
			observacao = "Outras Tarifas";
			break;
		}
		return observacao;
	}

}
