package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Status do pagamento")
public enum EnumArquivoConciliacaoOperadoraStatusPagamento {
	AGENDADO(0,"Agendado"),
	PAGO(1,"Pago");

	private Integer codigo;
	private String descricao;
	
	private EnumArquivoConciliacaoOperadoraStatusPagamento(Integer inicio, String descricao) {
		this.codigo = inicio;
		this.descricao = descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public static EnumArquivoConciliacaoOperadoraStatusPagamento getEnumArquivoConciliacaoOperadoraStatusPagamento(Integer codigo) {
		for (EnumArquivoConciliacaoOperadoraStatusPagamento statusPagamento : EnumArquivoConciliacaoOperadoraStatusPagamento.values()) {
			if (statusPagamento.getCodigo().equals(codigo)) {
				return statusPagamento;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
