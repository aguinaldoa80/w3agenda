package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AjusteIcmsOrigemProcesso {

	SEFAZ			(0, "Sefaz"), 
	JUSTICA_FEDERAL	(1, "Justi�a Federal"), 
	JUSTICA_ESTADUAL(2, "Justi�a Estadual"), 
	OUTROS			(3, "Outros"); 
	
	private Integer id;
	private String nome;
		
	private AjusteIcmsOrigemProcesso(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
