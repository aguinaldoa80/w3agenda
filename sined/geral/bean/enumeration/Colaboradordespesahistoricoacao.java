package br.com.linkcom.sined.geral.bean.enumeration;

public enum Colaboradordespesahistoricoacao {

	CRIADO			(0,"Criado"),
	AUTORIZADO		(1,"Autorizado"),
	PROCESSADO		(2,"Processado"),
	CANCELADO		(3,"Cancelado"),
	ALTERADO		(4, "Alterado"),
	ESTORNADO		(5, "Estornado");
	
	private Integer value;
	private String nome;
	
	private Colaboradordespesahistoricoacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return nome;
	}
	
	
}
