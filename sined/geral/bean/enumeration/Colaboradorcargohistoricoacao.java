package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Colaboradorcargohistoricoacao {
	
	CRIADO	(0, "Criado"),
	ALTERADO (1, "Alterado");
	
	private Integer value;
	private String descricao;
	
	private Colaboradorcargohistoricoacao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
