package br.com.linkcom.sined.geral.bean.enumeration;

public enum Movimentacaoanimalmotivo {

	TRANSFERENCIA(0, "Transferência"),
	MUDANCA_CATEGORIA(1, "Mudança de categoria"),
	COMPRA(2, "Compra"),
	NASCIMENTO(3, "Nascimento"),
	VENDA(4, "Venda"),
	MORTE(5, "Morte");
	
	private Integer idMotivo;
	private String descricao;
	
	private Movimentacaoanimalmotivo(Integer idMotivo, String descricao){
		this.idMotivo = idMotivo;
		this.descricao = descricao;
	}

	public Integer getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Integer idMotivo) {
		this.idMotivo = idMotivo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao().toUpperCase();
	}
}
