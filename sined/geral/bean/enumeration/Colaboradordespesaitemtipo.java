package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Colaboradordespesaitemtipo {

	CREDITO			(0,"Cr�dito"),
	DEBITO			(1,"D�bito");
	
	private Integer value;
	private String nome;
	
	private Colaboradordespesaitemtipo(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
