package br.com.linkcom.sined.geral.bean.enumeration;


public enum Operacao {

	ENTRADA	(0, "Entrada Fiscal"), 
	SAIDA	(1, "Nota fiscal Produto/Servi�o")
	; 
	
	private Integer value;
	private String descricao;
		
	private Operacao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}

	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
