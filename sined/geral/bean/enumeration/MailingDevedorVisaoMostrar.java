package br.com.linkcom.sined.geral.bean.enumeration;

public enum MailingDevedorVisaoMostrar {
	
	DIARIA ("Di�ria"),
	SEMANAL ("Semanal"),
	MENSAL ("Mensal");
	
	private String descricao;
	
	private MailingDevedorVisaoMostrar(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
