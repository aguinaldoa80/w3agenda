package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoiniciodata {

	DATA_VENDA 		(0, "Data de Venda"), 					
	DATA_ENTREGA	(1, "Data de Entrega");
	
	private Tipoiniciodata (Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	private Integer value;
	private String nome;
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
