package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AnaliseReceitaDespesaOrigem {
	
	MOVIMENTACOES		("Movimentações"),
	CONTA_PAGAR_RECEBER	("Contas a Pagar/Contas a Receber");
	
	private String descricao;
	
	private AnaliseReceitaDespesaOrigem(String descricao){
		this.descricao = descricao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
