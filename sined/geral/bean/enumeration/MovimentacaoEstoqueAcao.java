package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MovimentacaoEstoqueAcao {

	CRIAR 					(0, "Criada"),
	ALTERAR 				(1, "Alterada"),
	CANCELAR 				(2, "Cancelada"),
	ATUALIZACAO_CUSTO_MEDIO (3, "Atualiza��o do valor de custo m�dio"),
	CUSTEIO 				(4, "Atualizado pelo processamento do custeio"); 
	
	private Integer value;
	private String descricao;
	
	private MovimentacaoEstoqueAcao(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
}
