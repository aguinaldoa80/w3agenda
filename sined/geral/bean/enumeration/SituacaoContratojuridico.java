package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoContratojuridico {
	
	NORMAL					(0,"Normal"),
	A_CONSOLIDAR			(1,"A Consolidar"),
	ATENCAO					(2,"Aten��o"),
	ATRASADO				(3,"Atrasado"), 
	AGUARDANDO_SENTENCA		(4,"Aguardando senten�a"), 
	FINALIZADO				(5,"Finalizado"),
	CANCELADO				(6,"Cancelado");
	
	private Integer value;
	private String nome;
	
	private SituacaoContratojuridico(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<SituacaoContratojuridico> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoContratojuridico s : SituacaoContratojuridico.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoContratojuridico){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
