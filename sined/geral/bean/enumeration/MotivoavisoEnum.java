package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MotivoavisoEnum {

	AUTORIZAR_SOLICITACAOCOMPRA (0, "Autorizar solicita��o de compra"),
	GERAR_COTACAO				(1, "Gerar cota��o"),
	COTACA_GERADA				(2, "Cota��o gerada"),
	AUTORIZAR_ORDEMCOMPRA		(3, "Autorizar ordem de compra"),
	ENVIAR_PEDIDO_COMPRA		(4, "Enviar pedido de compra"),
	PEDIDO_COMPRA_ENVIADO		(5, "Pedido de compra enviado"),
	FATURAR_ENTREGA				(6, "Faturar entrega"),
	BAIXAR_ROMANEIO				(7, "Baixar romaneio"),
	CONSOLIDAR_AGENDAMENTO		(8, "Consolidar agendamento"),
	AUTORIZAR_CONTAPAGAR		(9, "Autorizar conta a pagar"),
	PLANEJAMENTO_AUTORIZADO		(10, "Planejamento autorizado"),
	APROVAR_PEDIDOVENDA			(11, "Aprovar pedido de venda"),
	RECEBIMENTO_BAIXADO			(12, "Recebimento Baixado"),
	LOTE_NF_NAO_ENVIADO			(13, "Lote de nota fiscal n�o enviado"),
	LOTE_NF_PROCESSADO_ERRO		(14, "Lote de nota fiscal processado com erro"),
	MATERIAL_ESTOQUE_MINIMO		(15, "Material com estoque m�nimo"),
	MATERIAL_SEM_ESTOQUE		(16, "Material sem estoque"),
	MATERIAL_ESTOQUE_NEGATIVO	(17, "Material com estoque negativo"),
	NF_DENEGADA					(18, "Nota fiscal denegada"),
	AGENDAMENTO_PROX_VENCIMENTO	(19, "Agendamento pr�ximo do vencimento"),
	AGENDAMENTO_ATRASADO		(20, "Agendamento atrasado"),
	CONTA_PAGAR_PROX_VENCIMENTO (21, "Conta a pagar pr�ximo do vencimento"),
	CONTA_PAGAR_VENCIDA			(22, "Conta a pagar vencida"),
	CONTA_RECEBER_VENCIDA		(23, "Conta a receber vencida"),
	VENDA_SEM_VINCULO_CONTA_RECEBER	(24, "Venda sem v�nculo com conta a receber"),
	NF_SEM_VINCULO_CONTA_RECEBER    (25, "Nota fiscal sem v�nculo a uma conta a receber"),
	SOLICITACAO_COMPRA_ATRASO_APROVACAO	(26, "Solicita��o de compra com atraso na aprova��o"),
	SOLICITACAO_COMPRA_ATRASO_ENTREGA	(27, "Solicita��o de compra com atraso na entrega"),
	COTACAO_ATRASO				(28, "Cota��o com atraso"),
	ORDEM_COMPRA_ATRASO			(29, "Ordem de compra com atraso na entrega"),
	RECEBIMENTO_SEM_REGISTRO_FINANCEIRO	(30, "Recebimento sem registro financeiro: Recebimento sem v�nculo a um registro financeiro"),
	COLETA_DEVOLUCAO_NAO_CONFIRMADA	(31, "Coleta com devolu��o n�o confirmada"),
	DATA_LOCACAO_MATERIAL_VENCIDA	(32, "Data da loca��o do material vencida"),
	CONTRATO_FORNECIMENTO_EXPIRADO	(33, "Contrato de fornecimento expirado"),
	ENTRADA_FISCAL_SEM_VINCULO_REGISTRO_FINANCEIRO	(34, "Entrada fiscal sem v�nculo a um registro financeiro"),
	ATRASO_AGENDA_INTERACAO		(35, "Atraso na Agenda de Intera��o"),
	ORDEM_SERVICO_ATRASADA		(36, "Ordem de servi�o atrasada"),
	SPED_FISCAL_NAO_EMITIDO		(37, "SPED Fiscal n�o emitido"),
	SPED_CONTRIBUICOES_NAO_EMITIDO	(38, "SPED Contribui��es n�o emitido"),
	CONCILIACAO_FINANCEIRA_NAO_REALIZADA	(39, "Concilia��o financeira da conta banc�ria n�o realizada"),
	FECHAMENTO_FINANCEIRO_NAO_REALIZADO		(40, "Fechamento financeiro n�o realizado"),
	REQUISICAO_MATERIAL_CRIADA (41, "Requisi��o de material criada"),
	SOLICITACAO_SERVICO (42, "Solicita��o de servi�o"),
	APROVAR_ORDEMCOMPRA (43, "Aprovar ordem de compra"),
	ORDEMCOMPRA_CRIADA (44, "Ordem de compra criada"),
	REQUISICAO_MATERIAL_AUTORIZADA (45, "Requisi��o de material autorizada"),
	CADASTRO_AVISO(46, "Cadastro avulso"),
	LOTE_NOTA_GERADO(47, "Lote de Notas Geradas");
	
	private Integer id;
	private String nome;
		
	private MotivoavisoEnum(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
