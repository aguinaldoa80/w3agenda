package br.com.linkcom.sined.geral.bean.enumeration;

public enum NaturezaContaCompensacaoEnum {
	
	CREDORA			(0, "01", "Credora"),
	DEVEDORA		(1, "02", "Devedora");
	
	private Integer value;
	private String cdsped;
	private String nome;
	
	
	private NaturezaContaCompensacaoEnum(Integer value, String cdsped, String nome) {
		this.value = value;
		this.cdsped = cdsped;
		this.nome = nome;
	}


	public Integer getValue() {
		return value;
	}


	public String getCdsped() {
		return cdsped;
	}


	public String getNome() {
		return nome;
	}


	public void setValue(Integer value) {
		this.value = value;
	}


	public void setCdsped(String cdsped) {
		this.cdsped = cdsped;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
