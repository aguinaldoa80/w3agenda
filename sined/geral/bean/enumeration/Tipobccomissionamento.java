package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tipobccomissionamento {

	VALOR_BRUTO		(0,"Valor Bruto"),
	VALOR_LIQUIDO	(1,"Valor L�quido"),
	VALOR_PAGO		(2,"Valor Pago");
	
	private Integer value;
	private String descricao;	
	
	private Tipobccomissionamento(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	
	public Integer getValue() {
		return value;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
