package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipopessoainventario {

	CLIENTE		(0, "Cliente"), 
	FORNECEDOR	(1, "Fornecedor");	
	
	private Tipopessoainventario (Integer _value, String nome){
		this.value = _value;
		this.nome = nome;
	}
	
	private Integer value;
	private String nome;
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
