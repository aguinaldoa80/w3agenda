package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum CreditoDebitoEnum {

	DEBITO  (0, "D�bito"),
	CREDITO (1, "Cr�dito");
	
	private Integer value;
	private String descricao;
	
	private CreditoDebitoEnum(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
