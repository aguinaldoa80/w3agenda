package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocobrancaipi {
	
	ENTRADA_RECUPERACAO_CREDITO		(0, "00", "IPI 00 - Entrada com recupera��o de cr�dito"),
	ENTRADA_TRIBUTADA_ALIQUOTA_ZERO	(1, "01", "IPI 01 - Entrada tributada com al�quota zero"),
	ENTRADA_ISENTA					(2, "02", "IPI 02 - Entrada isenta"),
	ENTRADA_NAO_TRIBUTADA			(3, "03", "IPI 03 - Entrada n�o tributada"),
	ENTRADA_IMUNE					(4, "04", "IPI 04 - Entrada imune"),
	ENTRADA_SUSPENSAO				(5, "05", "IPI 05 - Entrada com suspens�o"),
	ENTRADA_OUTRAS					(6, "49", "IPI 49 - Outras entradas"),
	SAIDA_TRIBUTABA					(7, "50", "IPI 50 - Sa�da tributada"),
	SAIDA_TRIBUTADA_ALIQUOTA_ZERO	(8, "51", "IPI 51 - Sa�da tributada com al�quota zero"), 
	SAIDA_ISENTA					(9, "52", "IPI 52 - Sa�da isenta"), 
	SAIDA_NAO_TRIBUTADA				(10,"53", "IPI 53 - Sa�da n�o tributada"),
	SAIDA_IMUNE						(11,"54", "IPI 54 - Sa�da imune"),
	SAIDA_SUSPENSAO					(12,"55", "IPI 55 - Sa�da com suspens�o"),
	SAIDA_OUTRAS					(13,"99", "IPI 99 - Outras sa�das");
	
	private Integer value;
	private String descricao;
	private String cdnfe;
	
	private Tipocobrancaipi(Integer value, String cdnfe, String descricao){
		this.value = value;
		this.descricao = descricao;
		this.cdnfe = cdnfe;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static Tipocobrancaipi getTipocobrancaipi(String string){
		Tipocobrancaipi [] values = Tipocobrancaipi.values();
		for (int i = 0; i < values.length; i++) {
			if (string.equals(values[i].getCdnfe())){
				return values[i];
			}
		}
		return null;
	}
}
