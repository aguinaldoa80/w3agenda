package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipotributacaoiss {
	
	NORMAL		(0, "NORMAL", 		"N"),
	RETIDA		(1, "RETIDA", 		"R"), 
	SUBSTITUTA	(2, "SUBSTITUTA", 	"S"), 
	ISENTA		(3, "ISENTA", 		"I");
	
	private Integer value;
	private String simbolo;
	private String descricao;
	
	private Tipotributacaoiss(Integer value, String descricao, String simbolo){
		this.value = value;
		this.descricao = descricao;
		this.simbolo = simbolo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getSimbolo() {
		return simbolo;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static Tipotributacaoiss getTipotributacaoiss(String string){
		Tipotributacaoiss[] values = Tipotributacaoiss.values();
		for (int i = 0; i < values.length; i++) {
			if (string.equals(values[i].getSimbolo())){
				return values[i];
			}
		}
		return null;
	}

}
