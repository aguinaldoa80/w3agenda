package br.com.linkcom.sined.geral.bean.enumeration;

public enum AgendainteracaocontratomodeloPessoatipo {

	RESPONSAVEL("Responsável"),
	VENDEDOR("Vendedor");
	
	private String nome;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	private AgendainteracaocontratomodeloPessoatipo(String nome){
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}
