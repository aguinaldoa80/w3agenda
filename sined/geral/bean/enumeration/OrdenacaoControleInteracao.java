package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoControleInteracao {
	CATEGORIA ("categoria.nome",	"Categoria"), 		
	CIDADE("municipio.nome", "Cidade"),			
	MUNICIPIOUF ("uf.sigla, municipio.nome", "Município/UF"),
	UF ("uf.sigla", "UF");
	
	private OrdenacaoControleInteracao(String campo, String nome){
		this.nome = nome;
		this.campo = campo;
	}
	
	private String campo;
	private String nome;
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
