package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoPesquisa {

	NOME_COMPLETO(0, "Nome Completo"),
	NOME_PARCIAL(1, "Nome Parcial");
	
	private Integer value;
	private String descricao;
	
	private TipoPesquisa(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
