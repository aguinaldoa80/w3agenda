package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRetornoDataMovimentacaoEnum {

	DATA_ATUAL("Data atual"),
	DATA_CREDITO("Data de cr�dito"),
	DATA_PAGAMENTO("Data de pagamento"); 
	
	private String nome;
		
	private BancoConfiguracaoRetornoDataMovimentacaoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
}