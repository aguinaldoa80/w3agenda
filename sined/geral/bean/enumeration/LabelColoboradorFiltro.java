package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum LabelColoboradorFiltro {
	
	COLETOR					(0, "Coletor"),
	VENDENDOR_PRINCIPAL		(1, "Vendedor Principal");
	
	private Integer value;
	private String label;
	
	private LabelColoboradorFiltro(Integer value, String label) {
		this.value = value;
		this.label = label;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getNome() {
		return this.getLabel();
	}
	@DescriptionProperty
	public String getLabel() {
		return label;
	}
	
	@Override
	public String toString() {
		return label;
	}
	
}
