package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;

public enum Expedicaosituacao {
	
	EM_ABERTO	(0, "Em aberto"),
	CONFIRMADA	(1, "Confirmada"),
	CANCELADA	(2, "Cancelada"),
	FATURADA	(3, "Faturada"),
	EM_SEPARACAO	(4, "Em separação"),
	SEPARACAO_REALIZADA	(5, "Separação Realizada"),
	EM_CONFERENCIA    (6, "Em conferência"),
	CONFERENCIA_REALIZADA	(7, "Conferência Realizada");
	
	private Integer value;
	private String nome;
	
	private Expedicaosituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Expedicaosituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Expedicaosituacao s : Expedicaosituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Expedicaosituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}

	public static List<Expedicaosituacao> getListagem() {
		List<Expedicaosituacao> listaExpedicaosituacao = new ArrayList<Expedicaosituacao>();
		
		List<Expedicaosituacao> listaNotIn = new ArrayList<Expedicaosituacao>();
		if(!(ParametrogeralService.getInstance().getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO))){
			listaNotIn.add(Expedicaosituacao.EM_SEPARACAO);
			listaNotIn.add(Expedicaosituacao.SEPARACAO_REALIZADA);
			listaNotIn.add(Expedicaosituacao.EM_CONFERENCIA);
			listaNotIn.add(Expedicaosituacao.CONFERENCIA_REALIZADA);
		}
		
		for (Expedicaosituacao expedicaosituacao : Expedicaosituacao.values()) {
			if(!listaNotIn.contains(expedicaosituacao)){
				listaExpedicaosituacao.add(expedicaosituacao);
			}
		}
		
		return listaExpedicaosituacao;
	}
	
}
