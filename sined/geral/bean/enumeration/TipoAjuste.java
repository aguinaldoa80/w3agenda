package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoAjuste {

	OUTROS_DEBITOS	(0, "Outros d�bitos"), 
	ESTORNO_CREDITOS(1, "Estorno de cr�ditos"),
	OUTROS_CREDITOS	(2, "Outros cr�ditos"),
	ESTORNO_DEBITOS	(3, "Estorno de d�bitos"),
	DEDUCOES_IMPOSTO(4, "Dedu��es do imposto apurado"),
	DEBITO_ESPECIAL	(5, "D�bito especial"),
	CONTROLE_ICMS_EXTRA	(9, "Controle do ICMS extra-apura��o"),
	; 
	
	private Integer id;
	private String nome;
		
	private TipoAjuste(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}
	
}
