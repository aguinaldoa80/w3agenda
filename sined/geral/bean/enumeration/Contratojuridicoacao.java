package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contratojuridicoacao {

	CRIADO		("Criado"), 			// 0 
	COSOLIDADO	("Consolidado"), 		// 1
	ALTERADO	("Alterado"),			// 2
	FINALIZADO	("Finalizado"),       	// 3
	SENTENCA	("Senten�a"),			// 4
	CANCELADO	("Cancelado"),			// 5
	ESTORNADO	("Estornado");			// 6
	
	private Contratojuridicoacao (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
