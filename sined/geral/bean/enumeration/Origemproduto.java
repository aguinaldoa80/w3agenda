package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Origemproduto {
	
	NACIONAL_0 		(0, "0 - Nacional, exceto as indicadas nos c�digos 3 a 5"),
	ESTRAGEIRA_1 	(1, "1 - Estrangeira - Importa��o direta, exceto a indicada no c�digo 6"),
	ESTRAGEIRA_2 	(2, "2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no c�digo 7"),
	NACIONAL_3 		(3, "3 - Nacional, mercadoria ou bem com Conte�do de Importa��o superior a  40% (quarenta por cento)"),
	NACIONAL_4 		(4, "4 - Nacional, cuja produ��o tenha sido feita em conformidade com os processos produtivos b�sicos de que tratam o Decreto-Lei n� 288/67, e as Leis n�s 8.248/91, 8.387/91, 10.176/01 e 11.484/07"),
	NACIONAL_5 		(5, "5 - Nacional, mercadoria ou bem com Conte�do de Importa��o inferior ou igual a 40% (quarenta por cento)"),
	ESTRAGEIRA_6 	(6, "6 - Estrangeira - Importa��o direta, sem similar nacional, constante em lista de Resolu��o CAMEX"),
	ESTRAGEIRA_7 	(7, "7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista de Resolu��o CAMEX"),
	NACIONAL_8 		(8, "8 - Nacional, mercadoria ou bem com Conte�do de Importa��o superior a 70%");
	
	private Integer value;
	private String nome;
	
	private Origemproduto(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}

	public static Origemproduto getEnum(int value) {
		for(Origemproduto origem: values())
			if(origem.getValue().equals(value)) return origem;
		return null;
	}
	
}
