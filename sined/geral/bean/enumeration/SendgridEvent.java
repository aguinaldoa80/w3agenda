package br.com.linkcom.sined.geral.bean.enumeration;

public enum SendgridEvent {

	DELIVERED("delivered"), 
	OPEN("open"), 
	SPAMREPORT("spamreport"), 
	BOUNCE("bounce"), 
	DROPPED("dropped"), 
	DEFERRED("deferred"),
	CLICK("click"),
	REQUEST("request"),
	HARD_BOUNCE("hard_bounce"),
	SOFT_BOUNCE("soft_bounce"),
	BLOCKED("blocked"),
	SPAM("spam"),
	INVALID_EMAIL("invalid_email"),
	OPENED("opened"),
	UNSUBSCRIBE("unsubscribe"),
	UNIQUE_OPENED("unique_opened");
	
	private String name;
	
	private SendgridEvent(String name){
		this.name = name;
	}	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {		
		return name;
	}
}
