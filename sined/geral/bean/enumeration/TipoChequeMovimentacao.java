package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Tipo de cheque")
public enum TipoChequeMovimentacao {
	
	CHEQUE_MOVIMENTACAO_CONTA_A_PAGAR	(0,"chequemovimentacaocontapagar","Cheque movimentacao financeira conta a pagar.",0,0,true);

	private TipoChequeMovimentacao(Integer codigo, String sufix, String descricao, Integer coluna, Integer linha, boolean expedicao) {
		this.codigo = codigo;
		this.sufix = sufix;
		this.descricao = descricao;
		this.coluna = coluna;
		this.linha = linha;
		this.expedicao = expedicao;
	}
	
	private Integer codigo;
	private String sufix;
	private String descricao;
	private Integer coluna;
	private Integer linha;
	private boolean expedicao;

	public Integer getCodigo() {
		return codigo;
	}
	public String getSufix() {
		return sufix;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public Integer getColuna() {
		return coluna;
	}
	public Integer getLinha() {
		return linha;
	}
	public boolean isExpedicao() {
		return expedicao;
	}
	public void setExpedicao(boolean expedicao) {
		this.expedicao = expedicao;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setSufix(String sufix) {
		this.sufix = sufix;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Tipoetiqueta> getListaTipoetiquetaExpedicao(){
		List<Tipoetiqueta> lista = new ArrayList<Tipoetiqueta>();
		for (int i = 0; i < Tipoetiqueta.values().length; i++) {
			if(Tipoetiqueta.values()[i].isExpedicao()){
				lista.add(Tipoetiqueta.values()[i]);
			}
		}
		return lista;
	}
}
