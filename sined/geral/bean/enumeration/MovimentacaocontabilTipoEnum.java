package br.com.linkcom.sined.geral.bean.enumeration;

public enum MovimentacaocontabilTipoEnum {
	
	UM_DEBITO_UM_CREDITO(0, "1 D�bito e 1 Cr�dito", "X"),
	N_DEBITOS_UM_CREDITO(1, "N D�bitos e 1 Cr�dito", "C"),
	UM_DEBITO_N_CREDITOS(2, "1 D�bito e N Cr�ditos", "D"),
	N_DEBITOS_N_CREDITOS(3, "N D�bitos e N Cr�ditos", "V");
	
	private Integer id;
	private String nome;
	private String sigla;
	
	private MovimentacaocontabilTipoEnum(Integer id, String nome, String sigla){
		this.id = id;
		this.nome = nome;
		this.sigla = sigla;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public String getSigla() {
		return sigla;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}