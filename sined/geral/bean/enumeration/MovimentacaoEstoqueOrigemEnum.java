package br.com.linkcom.sined.geral.bean.enumeration;

public enum MovimentacaoEstoqueOrigemEnum {

	COMPRA					("Compra"),
	CONSUMO					("Consumo"), 
	ROMANEIO				("Romaneio"), 
	ROMANEIO_MANUAL			("Romaneio Manual"), 
	VENDA					("Venda"),
	NOTA_SAIDA				("Nota Fiscal de Sa�da"),
	PRODUCAOAGENDA			("Agenda de produ��o"),
	AJUSTE_AUTOMATICO_WMS	("Ajuste autom�tico do WMS"),
	AJUSTE_MANUAL_WMS		("Ajuste manual do WMS"),
	PEDIDOVENDA				("Pedido de Venda"),
	INVENTARIO				("Invent�rio");
	
	private String nome;
		
	private MovimentacaoEstoqueOrigemEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}	
	
}
