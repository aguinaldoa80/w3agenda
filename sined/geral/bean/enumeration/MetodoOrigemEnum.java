package br.com.linkcom.sined.geral.bean.enumeration;

public enum MetodoOrigemEnum {

	GERAR		(0, "metodo gerar"),
	PROCESSAR	(1, "metodo processar");

	private Integer value;
	private String metodo;
	private MetodoOrigemEnum(Integer value, String metodo) {
		this.value = value;
		this.metodo = metodo;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	
}
