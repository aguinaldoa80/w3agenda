package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Emporiumvendaacao {

	CRIADA("Criada"), 				// 0 
	ALTERADA("Alterada"), 			// 1
	NFCE_EMITIDA("NFC-e Emitida"), 	// 2
	; 
	
	private Emporiumvendaacao (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
