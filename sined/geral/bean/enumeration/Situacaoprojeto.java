package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaoprojeto {
	
	EM_ESPERA		(0,"Em espera"),
	EM_ANDAMENTO	(1,"Em Andamento"),
	CONCLUIDO		(2,"Conclu�do"),
	CANCELADO		(3,"Cancelado");
	
	private Integer value;
	private String nome;
	
	private Situacaoprojeto(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Situacaoprojeto> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Situacaoprojeto s : Situacaoprojeto.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Situacaoprojeto){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
