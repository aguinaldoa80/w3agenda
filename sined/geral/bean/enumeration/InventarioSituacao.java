package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum InventarioSituacao {
	
	EM_ABERTO	(0, "Em aberto"),
	AUTORIZADO	(1, "Autorizado"),
	CANCELADO	(2, "Cancelado");
	
	private int value;
	private String nome;
	
	InventarioSituacao(){}
	
	private InventarioSituacao(int value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	
	public int getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static String listAndConcatenate(List<InventarioSituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (InventarioSituacao s : InventarioSituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof InventarioSituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
}
