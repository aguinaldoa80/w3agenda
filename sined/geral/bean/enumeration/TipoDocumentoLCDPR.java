package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoDocumentoLCDPR {
	NOTA_FISCAL(1, "Nota Fiscal"),
	FATURA(2, "Fatura"),
	RECIBO(3, "Recibo"),
	CONTRATO(4, "Contrato"),
	FOLHA_PAGAMENTO(5, "Folha de Pagamento"),
	OUTROS(6, "Outros");
	
	private Integer id;
	private String descricao;
	
	private TipoDocumentoLCDPR(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
