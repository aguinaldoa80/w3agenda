package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaApuracao {
	LIVRO_CAIXA(1, "Livro caixa"),
	VINTE_POR_CENTO_RENDA_BRUTA(2, "Apura��o do lucro pelo disposta no art. 5� da Lei n� 8.023, de 1990");
	
	private FormaApuracao(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	private Integer id;
	private String descricao;
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
