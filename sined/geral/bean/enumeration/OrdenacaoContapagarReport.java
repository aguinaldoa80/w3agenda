package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoContapagarReport {

	CONTA		("cdconta",				"Conta"), 		
	VENCIMENTO	("dtVencimento", 		"Vencimento"), 
	PAGAMENTO_A	("descricaoPagamento", 	"Pagamento a"),
	DOCUMENTO	("numero",				"Documento"),
	DESCRICAO	("descricao", 			"Descri��o"),
	NUM_OC		("numero", 				"N� O.C."),
	CHEQUE		("cheque", 				"Cheque"),
	VALOR		("valorAtual", 			"Valor"),
	CENTRO_CUSTO("centrocustoNome",		"Centro de custo");			
	
	private OrdenacaoContapagarReport (String campo, String nome){
		this.nome = nome;
		this.campo = campo;
	}
	
	private String campo;
	private String nome;
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
