package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

public enum SolicitacaoServicoPessoa {
	
	CLIENTE		    (0,"Cliente"),
	FORNECEDOR		(1,"Fornecedor"),
	OUTRO          (2,"Outro");
	
	private Integer value;
	private String descricao;	
	
	private SolicitacaoServicoPessoa(Integer value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}

	public static List<SolicitacaoServicoPessoa> listaTipoPessoa(){
		List<SolicitacaoServicoPessoa> lista = new ArrayList<SolicitacaoServicoPessoa>();
		lista.add(SolicitacaoServicoPessoa.CLIENTE);
		lista.add(SolicitacaoServicoPessoa.FORNECEDOR);
		lista.add(SolicitacaoServicoPessoa.OUTRO);
		return lista;
	}
}
