package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Despesaviagemacao {
	
	CRIADA				(0, "Criada"),		
	ALTERADA			(1, "Alterada"), 	
	AUTORIZADA			(2, "Autorizada"), 
	ADIANTAMENTO		(3, "Adiantamento"), 	
	CANCELADA			(4, "Cancelada"), 
	ESTORNADA			(5, "Estornada"),	
	ACERTO    			(6, "Acerto"), 
	REALIZADA			(7, "Realizada"), 
	RATEIO_ATUALIZADO 	(8, "Rateio Atualizado"), 
	REEMBOLSO 			(9, "Reenbolso"); 
	
	private Integer value;
	private String nome;
	
	private Despesaviagemacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}

}
