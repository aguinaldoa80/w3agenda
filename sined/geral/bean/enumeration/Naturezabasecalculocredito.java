package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Naturezabasecalculocredito {
	
	AQUISICAO_BENS_REVENDA								(0, "01", "Aquisi��o de bens para revenda"),
	AQUISICAO_BENS_INSUMO								(1, "02", "Aquisi��o de bens utilizados como insumo"), 
	AQUISICAO_SERVICOS_INSUMO							(2, "03", "Aquisi��o de servi�os utilizados"),
	ENERGIA_ELETRICA_ESTABELECIMENTO_PESSOA_JURIDICA	(3, "04", "Energia el�trica utilizada nos estabelecimentos da pessoa jur�dica"),
	ALUGUEIS_PREDIOS									(4, "05", "Alugu�is de pr�dios"),
	ALUGUEIS_MAQUINAS_EQUIPAMENTOS						(5, "06", "Alugu�is de m�quinas e equipamentos"),
	ARMAZENAGEM_MERCADORIA_FRETE_OP_VENDA				(6, "07", "Armazenagem de mercadoria e frete na opera��o de venda"),
	CONTRAPRESTACAO										(7, "08", "Contrapresta��es de arrendamento mercantil"),
	MAQUINAS_BENS_ATIVOIMOBILIZADO_ENCARGOS_DEPRECIACAO	(8, "09", "M�quinas, equipamentos e outros bens incorporados ao ativo imobilizado (cr�dito sobre encargos de deprecia��o)"),
	MAQUINAS_BENS_ATIVOIMOBILIZADO_VALOR_AQUISICAO		(9, "10", "M�quinas, equipamentos e outros bens incorporados ao ativo imobilizado (cr�dito com base no valor de aquisi��o)"),
	AMORTIZACAO_DEPRECIACAO								(10,"11", "Amortiza��o e Deprecia��o de edifica��es e benfeitorias em im�veis"),
	DEVOLUCAO_VENDA_INCIDNECIA_NAOCUMULATIVA			(11,"12", "Devolu��o de Vendas Sujeitas � Incid�ncia N�o-Cumulativa"),
	OUTRAS_OPERACOES_CREDITO							(12,"13", "Outras Opera��es com Direito a Cr�dito"),
	ATIVIDADE_TRANSPORTE_CARGAS							(13,"14", "Atividade de Transporte de Cargas � Subcontrata��o"),
	ATIVIDADE_IMOBILIARIA_CUSTO_INCORRIDO				(14,"15", "Atividade Imobili�ria � Custo Incorrido de Unidade Imobili�ria"),
	ATIVIDADE_IMOBILIARIA_CUSTO_ORCADO					(15,"16", "Atividade Imobili�ria � Custo Or�ado de unidade n�o conclu�da"),
	ATIVIDADE_PRESTACAO_SERVICO							(16,"17", "Atividade de Presta��o de Servi�os de Limpeza, Conserva��o e Manuten��o � vale-transporte, vale-refei��o ou vale-alimenta��o, fardamento ou uniforme."),
	ESTOQUE_ABERTURA_BENS								(17,"18", "Estoque de abertura de bens");
	
	private Integer value;
	private String descricao;
	private String cdsped;
	
	private Naturezabasecalculocredito(Integer value, String cdsped, String descricao){
		this.value = value;
		this.descricao = descricao;
		this.cdsped = cdsped;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public String getCdsped() {
		return cdsped;
	}

	@Override
	public String toString() {
		return getDescricao();
	}
	
}
