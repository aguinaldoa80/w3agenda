package br.com.linkcom.sined.geral.bean.enumeration;

public enum LogoImagem {
	
	IMG_W3ERP_BRIEFCASE		(1,"/imagens/sys/logo_briefcase_site.png"),
	IMG_W3ERP				(2,"/imagens/sys/logo_w3erp.png"),
	IMG_SINED				(3,"/imagens/sys/logo_sined.png");
	
	private Integer cdacao;
	private String desricao;
	
	private LogoImagem(Integer cdacao, String descricao){
		this.cdacao = cdacao;
		this.desricao = descricao;
	}
	
	public Integer getCdacao() {
		return cdacao;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setCdacao(Integer cdacao) {
		this.cdacao = cdacao;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
