package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Tipo de etiqueta")
public enum Tipoetiqueta {

//	PARA TODA ETIQUETA MATRICIAL ADICIONAR AO FINAL DELA "_MATRICIAL"
	
	AVERY_A4			(0,	"averya4",		"averya4 / 8923-A / A4 / 2 X 12 / etiquetas",		2,12,true),
	AVERY_CARTA			(1,	"avery",		"avery / 8923 / Carta / 2 X 5 / etiquetas",			2,5,true),	
	COMUNICADO			(2,	"comunicado",	"comunicado",										0,0,false),
	MAXPRINT			(3,	"maxprint",		"maxprint / 2 X 8 / etiquetas",						2,8,true),
	PIMACO_6081			(4,	"pimaco6081",	"pimaco / 6081/6281 / Carta / 2 X 10 / etiquetas",	2,10,true),	
	PIMACO_6181			(5,	"pimaco6181",	"pimaco / 6181 / Carta / 2 X 10 / etiquetas",		2,10,true),	
	PIMACO3X10			(6,	"pimaco3x10",	"pimaco3x10 / 6080 / Carta / 3 X 10 / etiquetas",	3,10,true),
	TEXTO				(7,	"texto",		"texto",											0,0,true),
	PIMACO_MATRICIAL	(8,	"texto",		"pimaco / pimatab 89x23 1 carreira",				0,0,false),
	//TERMICA_10X5		(9,	"Termica10X5",	"Impressora T�rmica (10cm x 5cm)",					0,0,true),
	PIMACO_6282			(10,"pimaco6282",	"pimaco / 6282 / Carta / 2 X 7 / etiquetas",		2,7,true),
	ETIQUETA_9X115		(11,"etiqueta9X115","etiquetas / 9 X 11,5 ",		    				0,0,true);

	private Tipoetiqueta(Integer codigo, String sufix, String descricao, Integer coluna, Integer linha, boolean expedicao) {
		this.codigo = codigo;
		this.sufix = sufix;
		this.descricao = descricao;
		this.coluna = coluna;
		this.linha = linha;
		this.expedicao = expedicao;
	}
	
	private Integer codigo;
	private String sufix;
	private String descricao;
	private Integer coluna;
	private Integer linha;
	private boolean expedicao;

	public Integer getCodigo() {
		return codigo;
	}
	public String getSufix() {
		return sufix;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public Integer getColuna() {
		return coluna;
	}
	public Integer getLinha() {
		return linha;
	}
	public boolean isExpedicao() {
		return expedicao;
	}
	public void setExpedicao(boolean expedicao) {
		this.expedicao = expedicao;
	}
	public void setLinha(Integer linha) {
		this.linha = linha;
	}
	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setSufix(String sufix) {
		this.sufix = sufix;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Tipoetiqueta> getListaTipoetiquetaExpedicao(){
		List<Tipoetiqueta> lista = new ArrayList<Tipoetiqueta>();
		for (int i = 0; i < Tipoetiqueta.values().length; i++) {
			if(Tipoetiqueta.values()[i].isExpedicao()){
				lista.add(Tipoetiqueta.values()[i]);
			}
		}
		return lista;
	}
}
