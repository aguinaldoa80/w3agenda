package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoRegistroDetalheAcompanhamentoEconomico {
	
	FORNECIMENTO	(0, "Fornecimento"),
	ORDEM_COMPRA	(1, "Ordem de compra"),
	CONTA_PAGAR 	(2, "Conta a pagar");
	
	private Integer value;
	private String descricao;
	
	private TipoRegistroDetalheAcompanhamentoEconomico(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}
