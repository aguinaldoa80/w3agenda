package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Processojuridicoclienteenvolvido {
	
	CLIENTE			(0, "Cliente"),
	PARTE_CONTRARIA	(1, "Parte contrária");
	
	private Integer value;
	private String nome;
	
	private Processojuridicoclienteenvolvido(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
