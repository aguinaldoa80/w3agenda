package br.com.linkcom.sined.geral.bean.enumeration;

public enum Comprovantestatus {

	EMITIDO ("Emiss�o de comprovante"),
	ENVIADO ("Envio de comprovante");
	
	private String descricao;
	
	private Comprovantestatus(String descricao){
		this.setDescricao(descricao);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}

}
