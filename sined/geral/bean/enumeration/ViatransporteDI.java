package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ViatransporteDI {
	
	MARITIMA	(0, 1, "Mar�tima"),
	FLUVIAL		(1, 2, "Fluvial"),
	LACUSTRE 	(2, 3, "Lacustre"),
	AEREA		(3, 4, "A�rea"),
	POSTAL 		(4, 5, "Postal"),
	FERROVIARIA (5, 6, "Ferrovi�ria"),
	RODOVIARIA 	(6, 7, "Rodovi�ria"),
	CONDUTO_REDE_TRANSMISSAO (7, 8, "Conduto / Rede Transmiss�o"),
	MEIOS_PROPRIOS 		(8, 9, "Meios Pr�prios"),
	ENTRADA_SAIDA_FICTA	(9, 10, "Entrada / Sa�da ficta");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private ViatransporteDI(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static ViatransporteDI getByCdnfe(Integer codigo){
		for (ViatransporteDI via : ViatransporteDI.values()) {
			if(via.getCdnfe().equals(codigo)) return via;
		}
		return null;
	}

}
