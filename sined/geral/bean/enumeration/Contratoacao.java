package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contratoacao {

	CRIADO("Criado"), 				// 0 
	COSOLIDADO("Consolidado"), 		// 1
	ALTERADO("Alterado"),			// 2
	FINALIZADO("Finalizado"),       // 3
	CONFIRMADO("Confirmado"),		// 4
	CANCELADO("Cancelado"),			// 5
	ESTORNADO("Estornado"),			// 6
	SUSPENSO("Suspenso"),			// 7
	RENOVADO("Renovado"),			// 8
	CONTRATO_ISENTO("Contrato isento"), // 9
	ROMANEIO("Romaneio"), 				// 10
	FECHAMENTO_ROMANEIO("Fechamento"), 	// 11
	GERAR_PRODUCAO("Gerar produ��o"), 	// 12
	RECALCULO_COMISSAO("Rec�lculo de comiss�o"), 	// 13
	SUBSTITUICAO_LOCACAO("Substitui��o de Loca��o"),// 14
	ROMANEIO_CANCELADO("Cancelamento Romaneio"), 	// 15
	DEVOLUCAO_ROMANEIO("Devolu��o"), 				// 16
	SUBSTITUICAO_FATURA("Substitui��o de Fatura"), 				// 17
	DADOS_ATUALIZADOS("Dados Atualizados"), 				// 18
	ARQUIVO_RETORNO_CONTRATO("Processamento de arquivo retorno de contrato"), 				// 19
	GERAR_OS("Gerar OS") 				// 20
	; 
	
	private Contratoacao (String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
