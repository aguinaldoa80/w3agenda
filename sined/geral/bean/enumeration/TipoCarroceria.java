package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoCarroceria {

	NAO_APLICAVEL			(0, "00", "00 - N�o Aplic�vel"),
	ABERTA					(1, "01", "01 - Aberta"),
	FECHADA_BAU				(2, "02", "02 - Fechada/Ba�"),
	GRANELERA				(3, "03", "03 - Graneleira"),
	PORTA_CONTAINER			(4, "04", "04 - Porta Cont�iner"),
	SIDER					(5, "05", "05 - Sider");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private TipoCarroceria(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
