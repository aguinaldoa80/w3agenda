package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoAgendamentoServico {
	
	AGENDADO	(0,"Agendado"),
	EM_ANDAMENTO(1,	"Em andamento"),
	ATENCAO		(2,	"Aten��o"),
	CANCELADO	(3,	"Cancelado"),
	REALIZADO	(4,	"Realizado");
	
	private Integer value;
	private String nome;
	
	private SituacaoAgendamentoServico(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<SituacaoAgendamentoServico> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoAgendamentoServico s : SituacaoAgendamentoServico.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoAgendamentoServico){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
	
}
