package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Retencao {

	SEMPRE_RETER  				(0, "Sempre reter"),
	NUNCA_RETER  				(1, "Nunca reter"),
	CONSIDERAR_FAIXA_IMPOSTO  	(2, "Considerar faixa imposto")
	; 
	
	private Integer value;
	private String nome;
	
	private Retencao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
