package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MdfeSituacao{

	EMITIDO			(0, "MDF-e Emitido"),
	ALTERADO		(1, "MDF-e Alterado"),
	SOLICITADO		(2, "MDF-e Solicitado"),
	ENVIADO			(3, "MDF-e Enviado"),
	AUTORIZADO		(4, "MDF-e Autorizado"),
	REJEITADO		(5, "MDF-e Rejeitado"),
	ENCERRADO		(6, "MDF-e Encerrado"),
	CANCELADO		(7, "MDF-e Cancelado"),
	EM_CANCELAMENTO	(8, "Cancelando MDF-e"),
	EM_ENCERRAMENTO	(9, "Encerrando MDF-e"),
	ESTORNADA		(10, "Estornado"),
	EM_INCLUSAO_CONDUTOR (11, "Incluindo Condutor MDF-e");
	
	private Integer value;
	private String nome;
	
	private MdfeSituacao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<MdfeSituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (MdfeSituacao bean : MdfeSituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof MdfeSituacao){
					if((situacoes.get(i)).equals(bean)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(bean.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(bean.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
}
