package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Pneureforma {
	
	R1	("R1"),
	R2	("R2"),
	R3	("R3");

	private String nome;
	
	private Pneureforma(String _nome) {
		this.nome = _nome;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
