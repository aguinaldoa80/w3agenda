package br.com.linkcom.sined.geral.bean.enumeration;

public enum MaterialRateioEstoqueTipoContaGerencial {
	
	CONSUMO					(1,"CONTA GERENCIAL CONSUMO"),
	REQUISICAO_MATERIAL		(2,"CONTA GERENCIAL REQUISI��O DE MATERIAL"),
	ENTRADA_MERCADORIA		(3,"CONTA GERENCIAL ENTRADA DE MERCADORIA"),
	VENDA					(4,"CONTA GERENCIAL VENDA"),
	PERDA					(5,"CONTA GERENCIAL PERDA"),
	AJUSTE_ENTRADA			(6,"CONTA GERENCIAL DE AJUSTE (ENTRADA)"),
	AJUSTE_SAIDA			(7,"CONTA GERENCIAL DE AJUSTE (SA�DA)"),
	ROMANEIO_ENTRADA		(8,"CONTA GERENCIAL DE ROMANEIO (ENTRADA)"),
	ROMANEIO_SAIDA			(9,"CONTA GERENCIAL DE ROMANEIO (SA�DA)"),
	DEVOLUCAO_ENTRADA		(10,"CONTA GERENCIAL DE DEVOLU��O (ENTRADA)"),
	DEVOLUCAO_SAIDA			(11,"CONTA GERENCIAL DE DEVOLU��O (SA�DA)"),
	;
	
	private Integer value;
	private String desricao;
	
	private MaterialRateioEstoqueTipoContaGerencial(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
