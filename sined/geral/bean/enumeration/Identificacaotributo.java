package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Identificacaotributo {

	GARE_SP_ICMS	(0, "GARE-SP ICMS"), 
	GARE_SP_DR		(1, "GARE-SP DR"),
	GARE_SP_ITCMD 	(2, "GARE-SP ITCMD"),
	; 
	
	private Integer value;
	private String descricao;
		
	private Identificacaotributo(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	public String toString() {
		return getDescricao();
	}	
}
