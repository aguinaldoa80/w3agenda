package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;

public enum MovimentacaocontabilTipoLancamentoEnum {
	
	CONTA_GERENCIAL_MOVIMENTACAO(0, "Conta gerencial (movimenta��o)", OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO),
	EMISSAO_NF(1, "Emiss�o de nota fiscal", OperacaoContabilTipoLancamento.EMISSAO_NF),
	RECOLHIMENTO_ISS_CLIENTE(2, "Recolhimento de ISS pelo cliente", OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS_CLIENTE),
	CONTA_RECEBER_BAIXADA_DINHEIRO(3, "Conta a receber baixada (Dinheiro)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_DINHEIRO),
	CONTA_RECEBER_BAIXADA_CHEQUE(4, "Conta a receber baixada (Cheque)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CHEQUE),
	CONTA_RECEBER_BAIXADA_CARTAO(5, "Conta a receber baixada (Cart�o)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CARTAO),
	CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO(6, "Conta a receber baixada (Retorno banc�rio)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_RETORNO_BANCARIO),
	CONCILIACAO_CARTAO(7, "Concilia��o de cart�o", OperacaoContabilTipoLancamento.CONCILIACAO_CARTAO),
	CREDITO_GERADO_RETORNO_BANCARIO(8, "Cr�dito gerado pelo retorno banc�rio", OperacaoContabilTipoLancamento.CREDITO_GERADO_RETORNO_BANCARIO),
	JUROS_RECEBIDOS(9, "Juros recebidos", OperacaoContabilTipoLancamento.JUROS_RECEBIDOS),
	DESCONTOS_CONCEDIDOS(10, "Descontos concedidos", OperacaoContabilTipoLancamento.DESCONTOS_CONCEDIDOS),
	REGISTROS_ENTRADA_FISCAL(11, "Registro de entrada fiscal", OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL),
	CONTA_PAGAR_BAIXADA_FORNECEDOR(12, "Conta a pagar baixada (Fornecedor)", OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_FORNECEDOR),
	JUROS_PAGOS(13, "Juros pagos", OperacaoContabilTipoLancamento.JUROS_PAGOS),
	DESCONTOS_OBTIDOS(14, "Descontos obtidos", OperacaoContabilTipoLancamento.DESCONTOS_OBTIDOS),
	DEPOSITO(15, "Dep�sito", OperacaoContabilTipoLancamento.DEPOSITO),
	DEPOSITO_CHEQUE(16, "Dep�sito de cheque", OperacaoContabilTipoLancamento.DEPOSITO_CHEQUE),
	SAQUE(17, "Saque", OperacaoContabilTipoLancamento.SAQUE),
	SAQUE_CHEQUE(18, "Saque com cheque", OperacaoContabilTipoLancamento.SAQUE_CHEQUE),
	TRANSFERENCIA_ENTRE_CONTAS(19, "Transfer�ncia entre contas", OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CONTAS),
	CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE(20, "Conta a receber baixada (Cr�dito em conta corrente)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_CREDITO_CONTA_CORRENTE),
	VENDA(21, "Venda", OperacaoContabilTipoLancamento.VENDA),
	CONTA_GERENCIAL_CONTA_PAGAR_RECEBER(22, "Conta gerencial  (conta a pagar/receber)", OperacaoContabilTipoLancamento.CONTA_GERENCIAL_CONTA_PAGAR_RECEBER),
	TRANSFERENCIA_ENTRE_CAIXAS(23, "Transfer�ncia entre caixas", OperacaoContabilTipoLancamento.TRANSFERENCIA_ENTRE_CAIXAS),
	CONTA_RECEBER_BAIXADA_PARCIAL(24, "Baixa parcial de conta a receber", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_PARCIAL),
	MOVIMENTACAO_ESTOQUE_ORIGEM(25, "Movimenta��o de estoque (origem)", OperacaoContabilTipoLancamento.MOVIMENTACAO_ESTOQUE_ORIGEM), 
	CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE(26, "Conta gerencial (movimenta��o de estoque)", OperacaoContabilTipoLancamento.CONTA_GERENCIAL_MOVIMENTACAO_ESTOQUE),
	CONTA_RECEBER_BAIXADA_ADIANTAMENTO(27, "Conta a receber baixada (Com adiantamento)", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_ADIANTAMENTO),
	CONTA_PAGAR_BAIXADA_ADIANTAMENTO(28, "Conta a pagar baixada (Com adiantamento)", OperacaoContabilTipoLancamento.CONTA_PAGAR_BAIXADA_ADIANTAMENTO),
    RECOLHIMENTO_IR(29, "Recolhimento de IR", OperacaoContabilTipoLancamento.RECOLHIMENTO_IR),
    RECOLHIMENTO_CSLL(30, "Recolhimento de CSLL", OperacaoContabilTipoLancamento.RECOLHIMENTO_CSLL),
    RECOLHIMENTO_INSS(31, "Recolhimento de INSS", OperacaoContabilTipoLancamento.RECOLHIMENTO_INSS),
    RECOLHIMENTO_PIS(32, "Recolhimento de PIS", OperacaoContabilTipoLancamento.RECOLHIMENTO_PIS),
    RECOLHIMENTO_COFINS(33, "Recolhimento de COFINS", OperacaoContabilTipoLancamento.RECOLHIMENTO_COFINS),
    RECOLHIMENTO_ICMS(34, "Recolhimento de ICMS cliente", OperacaoContabilTipoLancamento.RECOLHIMENTO_ICMS),
    RECOLHIMENTO_II(35, "Recolhimento de II", OperacaoContabilTipoLancamento.RECOLHIMENTO_II),
    RECOLHIMENTO_IPI(36, "Recolhimento de IPI", OperacaoContabilTipoLancamento.RECOLHIMENTO_IPI),
    RECOLHIMENTO_ISS(37, "Recolhimento de ISS", OperacaoContabilTipoLancamento.RECOLHIMENTO_ISS),
    PROVISIONAMENTO(38, "Provisionamento", OperacaoContabilTipoLancamento.PROVISIONAMENTO),
	DESPESA_COLABORADOR(39, "Despesas do colaborador", OperacaoContabilTipoLancamento.DESPESA_COLABORADOR),
	REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO(40, "Registro de entrada fiscal com apropria��o", OperacaoContabilTipoLancamento.REGISTROS_ENTRADA_FISCAL_COM_APROPRIACAO),
	CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO(41, "Conta a receber baixada por forma de pagamento", OperacaoContabilTipoLancamento.CONTA_RECEBER_BAIXADA_POR_FORMA_PAGAMENTO),;

	
	private Integer id;
	private String nome;
	private OperacaoContabilTipoLancamento operacaocontabilTipoLancamento; 
	
	private MovimentacaocontabilTipoLancamentoEnum(Integer id, String nome, OperacaoContabilTipoLancamento operacaocontabilTipoLancamento){
		this.id = id;
		this.nome = nome;
		this.operacaocontabilTipoLancamento = operacaocontabilTipoLancamento;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public OperacaoContabilTipoLancamento getOperacaocontabilTipoLancamento() {
		return operacaocontabilTipoLancamento;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
	public static List<MovimentacaocontabilTipoLancamentoEnum> getListaOrdenada(){
		List<MovimentacaocontabilTipoLancamentoEnum> listaOrdenada = Arrays.asList(MovimentacaocontabilTipoLancamentoEnum.values());
		
		Collections.sort(listaOrdenada, new Comparator<MovimentacaocontabilTipoLancamentoEnum>() {
			@Override
			public int compare(MovimentacaocontabilTipoLancamentoEnum o1, MovimentacaocontabilTipoLancamentoEnum o2) {
				return o1.getNome().compareToIgnoreCase(o2.getNome());
			}
		});
		
		return listaOrdenada;		
	}
	
	public static String listAndConcatenate(List<MovimentacaocontabilTipoLancamentoEnum> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (MovimentacaocontabilTipoLancamentoEnum tipoLancamento : MovimentacaocontabilTipoLancamentoEnum.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof MovimentacaocontabilTipoLancamentoEnum){
					if((situacoes.get(i)).equals(tipoLancamento)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(tipoLancamento.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(tipoLancamento.getId()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
	
	public static MovimentacaocontabilTipoLancamentoEnum getByOrdinal(Integer codigo){
			for (MovimentacaocontabilTipoLancamentoEnum tipoLancamento : MovimentacaocontabilTipoLancamentoEnum.values()){
				if(tipoLancamento.ordinal() == codigo){
					return tipoLancamento;
				}
			}
		
		return null;	
	}
}