package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoDespesaAvulsaRH {
	
	EM_ABERTO		(0,"Em Aberto"),
	AUTORIZADA		(1,"Autorizada"),
	PROCESSADA		(2,"Processada"),
	CANCELADA		(3,"Cancelada");
	
	private Integer value;
	private String nome;
	
	private SituacaoDespesaAvulsaRH(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<SituacaoDespesaAvulsaRH> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoDespesaAvulsaRH s : SituacaoDespesaAvulsaRH.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoDespesaAvulsaRH){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
