package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoitemsped {
	
	MERCADORIA_REVENDA		(0, "00", "Mercadoria para Revenda"),
	MATERIA_PRIMA			(1, "01", "Mat�ria-Prima"),
	EMBALAGEM				(2, "02", "Embalagem"),
	PRODUTO_PROCESSO		(3, "03", "Produto em Processo"),
	PRODUTO_ACABADO			(4, "04", "Produto Acabado"),
	SUBPRODUTO				(5, "05", "Subproduto"),
	PRODUTO_INTERMEDIARIO	(6, "06", "Produto Intermedi�rio"),
	USO_CONSUMO				(7, "07", "Material de Uso e Consumo"),
	ATIVO_IMOBILIZADO		(8, "08", "Ativo Imobilizado"),
	SERVICOS				(9, "09", "Servi�os"),
	OUTROS_INSUMOS			(10, "10", "Outros insumos"),
	OUTRAS					(11, "99", "Outras");
	
	private int id;
	private String value;
	private String descricao;
	
	private Tipoitemsped(int id, String value, String descricao){
		this.id = id;
		this.value = value;
		this.descricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	
	public int getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
