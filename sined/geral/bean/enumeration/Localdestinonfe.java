package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Localdestinonfe {
	
	INTERNA			(0, 1, "Opera��o interna"),
	INTERESTADUAL	(1, 2, "Opera��o interestadual"),
	EXTERIOR 		(2, 3, "Opera��o com exterior");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private Localdestinonfe(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
