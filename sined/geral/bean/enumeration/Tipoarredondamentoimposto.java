package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipoarredondamentoimposto {

	ROUND_FLOOR		(0, "ROUND FLOOR"), 
	ROUND_HALF_UP	(1, "ROUND_HALF_UP"),
	ROUND_HALF_DOWN	(2, "ROUND_HALF_DOWN"),
	ROUND_HALF_EVEN	(3, "ROUND_HALF_EVEN");
	
	private Integer id;
	private String nome;
		
	private Tipoarredondamentoimposto(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String toString() {
		return getNome();
	}	
}
