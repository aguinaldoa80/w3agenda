package br.com.linkcom.sined.geral.bean.enumeration;

public enum ModeloDocumentoFiscalEnum {

	NFE  ("55", "Nota Fiscal Eletrônica"),
	NFCE ("65", "Nota Fiscal Consumidor Eletrônica");
	
	private String value;
	private String descricao;
	
	private ModeloDocumentoFiscalEnum(String value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return value + " - " + descricao;
	}
	
}
