package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tiposanguineo {

	O_POS("O+"), 	// 0
	O_NEG("O-"), 	// 1
	A_POS("A+"), 	// 2
	A_NEG("A-"), 	// 3
	B_POS("B+"), 	// 4
	B_NEG("B-"), 	// 5
	AB_POS("AB+"), 	// 6
	AB_NEG("AB-"); 	// 7
		
	
	private Tiposanguineo (String descricao){
		this.descricao = descricao;
	}
	
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
