package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Prefixowebservice {
	
	// SEMPRE QUE CRIAR UM REGISTRO SE FOR OFICIAL TEM QUE TER O SUFIXO "PROD"
	// E SE N�O FOR TEM QUE TER O SUFIXO "HOM"
	
	BHISS_PROD 		(0, "Prefeitura de Belo Horizonte - Produ��o", "bhiss", true, false, false, false, ""),
	BHISS_HOM		(1, "Prefeitura de Belo Horizonte - Homologa��o", "bhiss", true, false, false, false, ""),
	SPISS_PROD		(2, "Prefeitura de S�o Paulo - Produ��o", "spiss", true, false, false, false, ""), 
	SPISS_HOM		(3, "Prefeitura de S�o Paulo - Homologa��o", "spiss", true, false, false, false, ""),
	SALVADORISS_PROD(4, "Prefeitura de Salvador - Produ��o", "salvadoriss", true, false, false, false, ""), 
	SALVADORISS_HOM	(5, "Prefeitura de Salvador - Homologa��o", "salvadoriss", true, false, false, false, ""),
	CONTAGEMISS_PROD(6, "Prefeitura de Contagem - Produ��o", "ginfesiss", true, false, false, false, ""), 
	CONTAGEMISS_HOM	(7, "Prefeitura de Contagem - Homologa��o", "ginfesiss", true, false, false, false, ""),
	MONLEVADEISS_PROD	(8, "Prefeitura de Jo�o Monlevade - Produ��o", "webiss", true, false, false, false, ""),
	MONLEVADEISS_HOM	(9, "Prefeitura de Jo�o Monlevade - Homologa��o", "webiss", true, false, false, false, ""),
	SEFAZMG_PROD	(10, "Sefaz Minas Gerais - Produ��o", "sefazmg", false, false, false, false, "MG"),
	SEFAZMG_HOM		(11, "Sefaz Minas Gerais - Homologa��o", "sefazmg", false, false, false, false, "MG"),
	SEFAZSP_PROD	(12, "Sefaz S�o Paulo - Produ��o", "sefazsp", false, false, false, false, "SP"),
	SEFAZSP_HOM		(13, "Sefaz S�o Paulo - Homologa��o", "sefazsp", false, false, false, false, "SP"),
	SEFAZBA_PROD	(14, "Sefaz Bahia - Produ��o", "sefazba", false, false, false, false, "BA"),
	SEFAZBA_HOM		(15, "Sefaz Bahia - Homologa��o", "sefazba", false, false, false, false, "BA"),
	SEFAZGO_PROD	(16, "Sefaz Goi�s - Produ��o", "sefazgo", false, false, false, false, "GO"),
	SEFAZGO_HOM		(17, "Sefaz Goi�s - Homologa��o", "sefazgo", false, false, false, false, "GO"),
	FORMIGAISS_PROD	(18, "Prefeitura de Formiga - Produ��o", "webiss", true, false, false, false, ""),
	FORMIGAISS_HOM	(19, "Prefeitura de Formiga - Homologa��o", "webiss", true, false, false, false, ""),
	SVAN_PROD		(20, "Sefaz Virtual Ambiente Nacional - Produ��o", "svan", false, false, false, false, "SVAN"),
	SVAN_HOM		(21, "Sefaz Virtual Ambiente Nacional - Homologa��o", "svan", false, false, false, false, "SVAN"),
	SVRS_PROD		(22, "Sefaz Virtual Rio Grande do Sul - Produ��o", "svrs", false, false, false, false, "SVRS"),
	SVRS_HOM		(23, "Sefaz Virtual Rio Grande do Sul - Homologa��o", "svrs", false, false, false, false, "SVRS"),
	LAVRASISS_PROD	(24, "Prefeitura de Lavras - Produ��o", "lavrasiss", true, false, false, false, ""),
	LAVRASISS_HOM	(25, "Prefeitura de Lavras - Homologa��o", "lavrasiss", true, false, false, false, ""),
	SAOBERNARDODOCAMPOISS_PROD 	(26, "Prefeitura de S�o Bernardo do Campo - Produ��o", "ginfesiss", true, false, false, false, ""),
	SAOBERNARDODOCAMPOISS_HOM 	(27, "Prefeitura de S�o Bernardo do Campo - Homologa��o", "ginfesiss", true, false, false, false, ""),
	BETIMISS_PROD	(28, "Prefeitura de Betim - Produ��o", "ginfesiss", true, false, false, false, ""),
	BETIMISS_HOM	(29, "Prefeitura de Betim - Homologa��o", "ginfesiss", true, false, false, false, ""),
	PARACATUISS_PROD(30, "Prefeitura de Paracatu - Produ��o", "paracatuiss", true, false, false, false, ""),
	ITAUNAISS_PROD	(31, "Prefeitura de Ita�na - Produ��o", "ginfesiss", true, false, false, false, ""),
	ITAUNAISS_HOM	(32, "Prefeitura de Ita�na - Homologa��o", "ginfesiss", true, false, false, false, ""),
	CAMPINASISS_PROD(33, "Prefeitura de Campinas - Produ��o", "dsfnetiss", true, false, false, false, ""),
	CAMPINASISS_HOM	(34, "Prefeitura de Campinas - Homologa��o", "dsfnetiss", true, false, false, false, ""),
	SAOJOSERIOPRETOISS_PROD (35, "Prefeitura de S�o Jos� do Rio Preto - Produ��o", "ginfesiss", true, false, false, false, ""),
	SAOJOSERIOPRETOISS_HOM 	(36, "Prefeitura de S�o Jos� do Rio Preto - Homologa��o", "ginfesiss", true, false, false, false, ""),
	EUNAPOLISISS_PROD 	(37, "Prefeitura de Eun�polis - Produ��o", "ginfesiss", true, false, false, false, ""),
	EUNAPOLISISS_HOM 	(38, "Prefeitura de Eun�polis - Homologa��o", "ginfesiss", true, false, false, false, ""),
	APARECIDAGOIANIAISS_PROD(39, "Prefeitura de Aparecida de Goi�nia - Produ��o", "aparecidaiss", true, false, false, false, ""),
	APARECIDAGOIANIAISS_HOM (40, "Prefeitura de Aparecida de Goi�nia - Homologa��o", "aparecidaiss", true, false, false, false, ""),
	NOVALIMAISS_PROD 	(41, "Prefeitura de Nova Lima - Produ��o", "abaco", true, false, false, false, ""),
	NOVALIMAISS_HOM 	(42, "Prefeitura de Nova Lima - Homologa��o", "abaco", true, false, false, false, ""),
	BRUMADINHOISS_PROD 	(43, "Prefeitura de Brumadinho - Produ��o", "sigiss", true, false, false, false, ""),
	JUIZDEFORAISS_PROD 	(44, "Prefeitura de Juiz de Fora - Produ��o", "portalfacil", true, false, false, false, ""),
	JUIZDEFORAISS_HOM	(45, "Prefeitura de Juiz de Fora - Homologa��o", "portalfacil", true, false, false, false, ""),
	BARBACENAISS_PROD	(46, "**DESATIVADO** Prefeitura de Barbacena - Produ��o", "webiss", true, false, false, false, ""),
	BARBACENAISS_HOM	(47, "**DESATIVADO** Prefeitura de Barbacena - Homologa��o", "webiss", true, false, false, false, ""),
	SANTALUZIAISS_PROD	(48, "**DESATIVADO** Prefeitura de Santa Luzia - Produ��o", "issonline", true, false, false, false, ""),
	SANTALUZIAISS_HOM	(49, "**DESATIVADO** Prefeitura de Santa Luzia - Homologa��o", "issonline", true, false, false, false, ""),
	CURITIBAISS_PROD	(50, "Prefeitura de Curitiba - Produ��o", "curitibaiss", true, false, false, false, ""),
	CURITIBAISS_HOM		(51, "Prefeitura de Curitiba - Homologa��o", "curitibaiss", true, false, false, false, ""),
	MARIANAISS_PROD		(52, "Prefeitura de Mariana - Produ��o", "flyenota", true, false, false, false, ""),
	MARIANAISS_HOM		(53, "Prefeitura de Mariana - Homologa��o", "flyenota", true, false, false, false, ""),
	SEFAZPR_PROD		(54, "Sefaz Paran� - Produ��o", "sefazpr", false, false, false, false, "PR"),
	SEFAZPR_HOM			(55, "Sefaz Paran� - Homologa��o", "sefazpr", false, false, false, false, "PR"),
	SIMOESFILHOISS_PROD	(56, "**DESATIVADO** Prefeitura de Sim�es Filho - Produ��o", "eliss", true, false, false, false, ""), 
	SIMOESFILHOISS_HOM	(57, "**DESATIVADO** Prefeitura de Sim�es Filho - Homologa��o", "eliss", true, false, false, false, ""),
	MACEIOISS_PROD 		(58, "Prefeitura de Macei� - Produ��o", "ginfesiss", true, false, false, false, ""),
	MACEIOISS_HOM 		(59, "Prefeitura de Macei� - Homologa��o", "ginfesiss", true, false, false, false, ""),
	RECIFEISS_PROD 		(60, "Prefeitura de Recife - Produ��o", "recifeiss", true, false, false, false, ""),
	SEFAZPE_PROD		(61, "Sefaz Pernambuco - Produ��o", "sefazpe", false, false, false, false, "PE"),
	SEFAZPE_HOM			(62, "Sefaz Pernambuco - Homologa��o", "sefazpe", false, false, false, false, "PE"),
	SAOLOURENCODASERRAISS_PROD 	(63, "Prefeitura de S�o Louren�o da Serra - Produ��o", "simpliss", true, false, false, false, ""),
	SAOLOURENCODASERRAISS_HOM 	(64, "Prefeitura de S�o Louren�o da Serra - Homologa��o", "simpliss", true, false, false, false, ""),
	RIBEIRAOPRETOISS_PROD 	(65, "Prefeitura de Ribeir�o Preto - Produ��o", "ginfesiss", true, false, false, false, ""),
	RIBEIRAOPRETOISS_HOM(66, "Prefeitura de Ribeir�o Preto - Homologa��o", "ginfesiss", true, false, false, false, ""),
	RIODEJANEIROISS_PROD(67, "Prefeitura de Rio de Janeiro - Produ��o", "rjiss", true, false, false, false, ""),
	RIODEJANEIROISS_HOM (68, "Prefeitura de Rio de Janeiro - Homologa��o", "rjiss", true, false, false, false, ""),
	CABEDELOISS_PROD 	(69, "Prefeitura de Cabedelo - Produ��o", "tinusiss", true, false, false, false, ""),
	CABEDELOISS_HOM 	(70, "Prefeitura de Cabedelo - Homologa��o", "tinusiss", true, false, false, false, ""),
	IGARAPEISS_PROD 	(71, "Prefeitura de Igarap� - Produ��o", "sigiss", true, false, false, false, ""),
	DIADEMAISS_PROD 	(72, "Prefeitura de Diadema - Produ��o", "ginfesiss", true, false, false, false, ""),
	DIADEMAISS_HOM 		(73, "Prefeitura de Diadema - Homologa��o", "ginfesiss", true, false, false, false, ""),
	COTIAISS_PROD 		(74, "Prefeitura de Cotia - Produ��o", "ginfesiss", true, false, false, false, ""),
	COTIAISS_HOM 		(75, "Prefeitura de Cotia - Homologa��o", "ginfesiss", true, false, false, false, ""),
	DIVINOPOLISISS_PROD (76, "Prefeitura de Divin�polis - Produ��o", "govdigital", true, false, false, false, ""),
	DIVINOPOLISISS_HOM 	(77, "Prefeitura de Divin�polis - Homologa��o", "govdigital", true, false, false, false, ""),
	VINHEDOISS_PROD 	(78, "Prefeitura de Vinhedo - Produ��o", "prescon", true, false, false, false, ""),
	SERRAISS_PROD		(79, "Prefeitura de Serra - Produ��o", "smarapd", true, false, false, false, ""),
	SERRAISS_HOM		(80, "Prefeitura de Serra - Homologa��o", "smarapd", true, false, false, false, ""),
	SAOJOSEDOSCAMPOSISS_PROD 	(81, "Prefeitura de S�o Jos� dos Campos - Produ��o", "ginfesiss", true, false, false, false, ""),
	SAOJOSEDOSCAMPOSISS_HOM 	(82, "Prefeitura de S�o Jos� dos Campos - Homologa��o", "ginfesiss", true, false, false, false, ""),
	MINEIROSISS_PROD 	(83, "Prefeitura de Mineiros - Produ��o", "ginfesiss", true, false, false, false, ""),
	MINEIROSISS_HOM 	(84, "Prefeitura de Mineiros - Homologa��o", "ginfesiss", true, false, false, false, ""),
	JABOTICABALISS_PROD (85, "Prefeitura de Jaboticabal - Produ��o", "ginfesiss", true, false, false, false, ""),
	JABOTICABALISS_HOM 	(86, "Prefeitura de Jaboticabal - Homologa��o", "ginfesiss", true, false, false, false, ""),
	BLUMENAUISS_PROD 	(87, "Prefeitura de Blumenau - Produ��o", "blumenauiss", true, false, false, false, ""),
	BLUMENAUISS_HOM 	(88, "Prefeitura de Blumenau - Homologa��o", "blumenauiss", true, false, false, false, ""),
	PORTOALEGRE_PROD	(89, "Prefeitura de Porto Alegre - Produ��o", "bhiss", true, false, false, false, ""),
	PORTOALEGRE_HOM		(90, "Prefeitura de Porto Alegre - Homologa��o", "bhiss", true, false, false, false, ""),
	ERECHIM_PROD		(91, "Prefeitura de Erechim - Produ��o", "systempro", true, false, false, false, ""),
	ERECHIM_HOM			(92, "Prefeitura de Erechim - Homologa��o", "systempro", true, false, false, false, ""),
	SOROCABAISS_PROD	(93, "Prefeitura de Sorocaba - Produ��o", "dsfnetiss", true, false, false, false, ""),
	SOROCABAISS_HOM		(94, "Prefeitura de Sorocaba - Homologa��o", "dsfnetiss", true, false, false, false, ""),
	REGISTROISS_PROD 	(95, "Prefeitura de Registro - Produ��o", "ginfesiss", true, false, false, false, ""),
	REGISTROISS_HOM 	(96, "Prefeitura de Registro - Homologa��o", "ginfesiss", true, false, false, false, ""),
	RIOCLAROISS_PROD 	(97, "Prefeitura de Rio Claro - Produ��o", "ginfesiss", true, false, false, false, ""),
	RIOCLAROISS_HOM 	(98, "Prefeitura de Rio Claro - Homologa��o", "ginfesiss", true, false, false, false, ""),
	ARARASISS_PROD 		(99, "Prefeitura de Araras - Produ��o", "simpliss", true, false, false, false, ""),
	ARARASISS_HOM 		(100, "Prefeitura de Araras - Homologa��o", "simpliss", true, false, false, false, ""),
	ITABIRITOISS_PROD	(101, "Prefeitura de Itabirito - Produ��o", "issonline", true, false, false, false, ""),
	ITABIRITOISS_HOM	(102, "Prefeitura de Itabirito - Homologa��o", "issonline", true, false, false, false, ""),
	VOTORANTIMISS_PROD	(103, "Prefeitura de Votorantim - Produ��o", "geisweb", true, false, false, false, ""),
	GUARULHOSISS_PROD 	(104, "Prefeitura de Guarulhos - Produ��o", "ginfesiss", true, false, false, false, ""),
	GUARULHOSISS_HOM 	(105, "Prefeitura de Guarulhos - Homologa��o", "ginfesiss", true, false, false, false, ""),
	SANTAREMISS_PROD 	(106, "Prefeitura de Santar�m - Produ��o", "ginfesiss", true, false, false, false, ""),
	SANTAREMISS_HOM 	(107, "Prefeitura de Santar�m - Homologa��o", "ginfesiss", true, false, false, false, ""),
	FRANCAISS_PROD 		(108, "Prefeitura de Franca - Produ��o", "ginfesiss", true, false, false, false, ""),
	FRANCAISS_HOM 		(109, "Prefeitura de Franca - Homologa��o", "ginfesiss", true, false, false, false, ""),
	SAOJOAODABOAVISTAISS_PROD	(110, "Prefeitura de S�o Jo�o da Boa Vista - Produ��o", "simpliss", true, false, false, false, ""),
	SAOJOAODABOAVISTAISS_HOM 	(111, "Prefeitura de S�o Jo�o da Boa Vista - Homologa��o", "simpliss", true, false, false, false, ""),
	MOGIMIRIMISS_PROD	(112, "**DESATIVADO** Prefeitura de Mogi Mirim - Produ��o", "geisweb", true, false, false, false, ""),
	ARACAJUISS_PROD		(113, "Prefeitura de Aracaju - Produ��o", "webiss2", true, false, false, false, ""),
	ARACAJUISS_HOM		(114, "Prefeitura de Aracaju - Homologa��o", "webiss2", true, false, false, false, ""),
	MOGIDASCRUZESISS_PROD(115, "Prefeitura de Mogi das Cruzes - Produ��o", "smarapd", true, false, false, false, ""),
	MOGIDASCRUZESISS_HOM(116, "Prefeitura de Mogi das Cruzes - Homologa��o", "smarapd", true, false, false, false, ""),
	SETELAGOAS_PROD 	(117, "Prefeitura de Sete Lagoas - Produ��o", "mi6", true, false, false, false, ""),
	SANTANADEPARNAIBA_PROD(118, "Prefeitura de Santana de Parna�ba - Produ��o", "nfeletronica", true, false, false, false, ""),
	BALNEARIOCAMBORIUISS_PROD	(119, "Prefeitura de Balne�rio Cambori� - Produ��o", "simpliss", true, false, false, false, ""),
	BALNEARIOCAMBORIUISS_HOM 	(120, "Prefeitura de Balne�rio Cambori� - Homologa��o", "simpliss", true, false, false, false, ""),
	SAOJOSEDALAPAISS_PROD	(121, "Prefeitura de S�o Jos� da Lapa - Produ��o", "govdigital", true, false, false, false, ""),
	SAOJOSEDALAPAISS_HOM	(122, "Prefeitura de S�o Jos� da Lapa - Homologa��o", "govdigital", true, false, false, false, ""),
	SEFAZRS_PROD		(123, "Sefaz Rio Grande do Sul - Produ��o", "sefazrs", false, false, false, false, "RS"),
	SEFAZRS_HOM			(124, "Sefaz Rio Grande do Sul - Homologa��o", "sefazrs", false, false, false, false, "RS"),
	MOGIMIRIMISS_NOVO_PROD	(125, "Prefeitura de Mogi Mirim (Novo) - Produ��o", "fisslex", true, false, false, false, ""),
	MOGIMIRIMISS_NOVO_HOM	(126, "Prefeitura de Mogi Mirim (Novo) - Homologa��o", "fisslex", true, false, false, false, ""),
	CURVELO_PROD		(127, "Prefeitura de Curvelo - Produ��o", "memory", true, false, false, false, ""),
	CURVELO_HOM			(128, "Prefeitura de Curvelo - Homologa��o", "memory", true, false, false, false, ""),
	LIMEIRA_PROD		(129, "Prefeitura de Limeira - Produ��o", "etransparencia", true, false, false, false, ""),
	LIMEIRA_HOM			(130, "Prefeitura de Limeira - Homologa��o", "etransparencia", true, false, false, false, ""),
	FORTALEZA_PROD		(131, "Prefeitura de Fortaleza - Produ��o", "ginfesiss", true, false, false, false, ""),
	FORTALEZA_HOM		(132, "Prefeitura de Fortaleza - Homologa��o", "ginfesiss", true, false, false, false, ""),
	JANDIRA_PROD		(133, "Prefeitura de Jandira - Produ��o", "smarapd", true, false, false, false, ""),
	JANDIRA_HOM			(134, "Prefeitura de Jandira - Homologa��o", "smarapd", true, false, false, false, ""),
	SIMOESFILHOISS_NOVO_PROD	(135, "Prefeitura de Sim�es Filho (Novo) - Produ��o", "webiss2", true, false, false, false, ""),
	SIMOESFILHOISS_NOVO_HOM		(136, "Prefeitura de Sim�es Filho (Novo) - Homologa��o", "webiss2", true, false, false, false, ""),
	
	ARANDU_PROD			(137, "Prefeitura de Arandu - Produ��o", "issmap", true, false, false, false, ""),
	ARANDU_HOM			(138, "Prefeitura de Arandu - Homologa��o", "issmap", true, false, false, false, ""),
	CAMACARI_PROD		(139, "Prefeitura de Cama�ari - Produ��o", "prodeb", true, false, false, false, ""),
	CAMACARI_HOM		(140, "Prefeitura de Cama�ari - Homologa��o", "prodeb", true, false, false, false, ""),
	SERRATALHADA_PROD	(141, "Prefeitura de Serra Talhada - Produ��o", "datapublic", true, false, false, false, ""),
	SERRATALHADA_HOM	(142, "Prefeitura de Serra Talhada - Homologa��o", "datapublic", true, false, false, false, ""),
	OUROPRETO_PROD		(143, "Prefeitura de Ouro Preto - Produ��o", "govdigital", true, false, false, false, ""),
	OUROPRETO_HOM		(144, "Prefeitura de Ouro Preto - Homologa��o", "govdigital", true, false, false, false, ""),
	CONGONHAS_PROD		(145, "Prefeitura de Congonhas - Produ��o", "flyenota", true, false, false, false, ""),
	CONGONHAS_HOM		(146, "Prefeitura de Congonhas - Homologa��o", "flyenota", true, false, false, false, ""),
	SETELAGOAS_NOVO_PROD(147, "Prefeitura de Sete Lagoas (Novo) - Produ��o", "portalfacil", true, false, false, false, ""),
	SETELAGOAS_NOVO_HOM	(148, "Prefeitura de Sete Lagoas (Novo) - Homologa��o", "portalfacil", true, false, false, false, ""),
	SALTO_PROD			(149, "Prefeitura de Salto - Produ��o", "obaratec", true, false, false, false, ""),
	SALTO_HOM			(150, "Prefeitura de Salto - Homologa��o", "obaratec", true, false, false, false, ""),
	LAGOADAPRATA_PROD	(151, "Prefeitura de Lagoa da Prata - Produ��o", "flyenota", true, false, false, false, ""),
	LAGOADAPRATA_HOM	(152, "Prefeitura de Lagoa da Prata - Homologa��o", "flyenota", true, false, false, false, ""),
	AVARE_PROD			(153, "Prefeitura de Avar� - Produ��o", "fiorilli", true, false, false, false, ""),
	AVARE_HOM			(154, "Prefeitura de Avar� - Homologa��o", "fiorilli", true, false, false, false, ""),
	SAOJOAQUIMBICAS_PROD(155, "Prefeitura de S�o Joaquim de Bicas - Produ��o", "govdigital", true, false, false, false, ""),
	SAOJOAQUIMBICAS_HOM (156, "Prefeitura de S�o Joaquim de Bicas - Homologa��o", "govdigital", true, false, false, false, ""),
	
	SEFAZMDFE_PROD		(157, "Sefaz MDF-e - Produ��o", "sefazmdfe", false, true, false, false, ""),
	SEFAZMDFE_HOM		(158, "Sefaz MDF-e - Homologa��o", "sefazmdfe", false, true, false, false, ""),
	
	UBERABA_PROD		(159, "Prefeitura de Uberaba - Produ��o", "webiss", true, false, false, false, ""),
	UBERABA_HOM			(160, "Prefeitura de Uberaba - Homologa��o", "webiss", true, false, false, false, ""),
	ITAJAI_PROD			(161, "Prefeitura de Itaja� - Produ��o", "itajaiiss", true, false, false, false, ""),
	ITAJAI_HOM			(162, "Prefeitura de Itaja� - Homologa��o", "itajaiiss", true, false, false, false, ""),
	IVOTI_PROD			(163, "Prefeitura de Ivoti - Produ��o", "tecnosistemas", true, false, false, false, ""),
	IVOTI_HOM			(164, "Prefeitura de Ivoti - Homologa��o", "tecnosistemas", true, false, false, false, ""),
	SANTALUZIAISS_NOVO_PROD	(165, "Prefeitura de Santa Luzia (Novo) - Produ��o", "supernova", true, false, false, false, ""),
	SANTALUZIAISS_NOVO_HOM	(166, "Prefeitura de Santa Luzia (Novo) - Homologa��o", "supernova", true, false, false, false, ""),
	VOTORANTIMISS_NOVO_PROD	(167, "Prefeitura de Votorantim (Novo) - Produ��o", "assessorpublico", true, false, false, false, ""),
	VOTORANTIMISS_NOVO_HOM	(168, "Prefeitura de Votorantim (Novo) - Homologa��o", "assessorpublico", true, false, false, false, ""),
	SAOJOSEDALAPA_NOVO_HOM	(169, "Prefeitura de S�o Jos� da Lapa - Homologa��o (Novo)", "gissonline", true, false, false, false, ""),
	SAOJOSEDALAPA_NOVO_PROD	(170, "Prefeitura de S�o Jos� da Lapa - Produ��o (Novo)", "gissonline", true, false, false, false, ""),
	AVARE_NOVO_PROD			(171, "Prefeitura de Avar� - Produ��o (Novo)", "sigbancos", true, false, false, false, ""),
	AVARE_NOVO_HOM			(172, "Prefeitura de Avar� - Homologa��o (Novo)", "sigbancos", true, false, false, false, ""),
	MANAUS_PROD				(173, "Prefeitura de Manaus - Produ��o", "manaus", true, false, false, false, ""),
	MANAUS_HOM				(174, "Prefeitura de Manaus - Homologa��o", "manaus", true, false, false, false, ""),
	BOA_VISTA_PROD			(175, "Prefeitura de Boa Vista - Produ��o", "saatri", true, false, false, false, ""),
	BOA_VISTA_HOM			(176, "Prefeitura de Boa Vista - Homologa��o", "saatri", true, false, false, false, ""),
	JUAZEIRO_PROD			(177, "Prefeitura de Juazeiro - Produ��o", "eireli", true, false, false, false, ""),
	JUAZEIRO_HOM			(178, "Prefeitura de Juazeiro - Homologa��o", "eireli", true, false, false, false, ""),
	SEFAZAM_PROD			(179, "Sefaz Amazonas - Produ��o", "sefazam", false, false, false, false, "AM"),
	SEFAZAM_HOM				(180, "Sefaz Amazonas - Homologa��o", "sefazam", false, false, false, false, "AM"),
	AN_EVENTO_PROD          (181, "Ambiente Nacional de Evento - Produ��o", "anevento", false, false, false, true, "AN"),
	AN_EVENTO_HOM   		(182, "Ambiente Nacional de Evento - Homologa��o", "anevento", false, false, false, true, "AN"),
    DDE_HOM                 (183, "Distribui��o de Documento Eletr�nico - Homologa��o", "dde", false, false, true, false, ""),
    IPATINGA_PROD           (184, "Prefeitura de Ipatinga - Produ��o", "portalfacilv1", true, false, false, false, ""),
    IPATINGA_HOM            (185, "Prefeitura de Ipatinga - Homologa��o", "portalfacilv1", true, false, false, false, ""),
    CAMBUI_PROD             (186, "Prefeitura de Cambu� - Produ��o", "simpliss", true, false, false, false, ""),
    CAMBUI_HOM              (187, "Prefeitura de Cambu� - Homologa��o", "simpliss", true, false, false, false, ""),
    DDE_PROD                (188, "Distribui��o de Documento Eletr�nico - Produ��o", "dde", false, false, true, false, ""),
    CHAPECO_PROD			(189, "Prefeitura de Chapec� - Produ��o", "publica", true, false, false, false, ""),
    CHAPECO_HOM  			(190, "Prefeitura de Chapec� - Homologa��o", "publica", true, false, false, false, ""),
    GOIANIA_PROD			(191, "Prefeitura de Goi�nia - Produ��o", "goianiaiss", true, false, false, false, ""),
    GOIANIA_HOM             (192, "Prefeitura de Goi�nia - Homologa��o", "goianiaiss", true, false, false, false, ""),
    JOAO_MONLEVADE_NOVO_PROD(193, "Prefeitura de Jo�o Monlevade (Novo) - Produ��o", "simpliss", true, false, false, false, ""),
    JOAO_MONLEVADE_NOVO_HOM (194, "Prefeitura de Jo�o Monlevade (Novo) - Homologa��o", "simpliss", true, false, false, false, ""),
    LIMEIRA_NOVO_PROD		(195, "Prefeitura de Limeira (Novo) - Produ��o", "iibrasil", true, false, false, false, ""),
    LIMEIRA_NOVO_HOM 		(196, "Prefeitura de Limeira (Novo) - Homologa��o", "iibrasil", true, false, false, false, ""),
    PETROLINA_PROD          (197, "Prefeitura de Petrolina - Produ��o", "eel", true, false, false, false, ""),
    PETROLINA_HOM           (198, "Prefeitura de Petrolina - Homologa��o", "eel", true, false, false, false, ""),
    CACHOEIRO_DO_ITAPEMIRIM_PROD (199, "Prefeitura de Cachoeiro do Itapemirim - Produ��o", "eel", true, false, false, false, ""),
    CACHOEIRO_DO_ITAPEMIRIM_HOM  (200, "Prefeitura de Cachoeiro do Itapemirim - Homologa��o", "eel", true, false, false, false, ""),
    CASTELO_PROD            (201, "Prefeitura de Castelo - Produ��o", "eel2", true, false, false, false, ""),
    CASTELO_HOM             (202, "Prefeitura de Castelo - Homologa��o", "eel2", true, false, false, false, ""),
    MARABA_PROD				(203, "Prefeitura de Marab�/PA - Produ��o", "marabaiss", true, false, false, false, ""),
    MARABA_HOM 				(204, "Prefeitura de Marab�/PA - Homologa��o", "marabaiss", true, false, false, false, ""),
    JOAO_PESSOA_PROD        (205, "Prefeitura de Jo�o Pessoa - Produ��o", "joaopessoaiss", true, false, false, false, ""),
    JOAO_PESSOA_HOM         (206, "Prefeitura de Jo�o Pessoa - Homologa��o", "joaopessoaiss", true, false, false, false, ""),
    ITUMBIARA_PROD          (207, "Prefeitura de Itumbiara - Produ��o", "sigep", true, false, false, false, ""),
    ITUMBIARA_HOM           (208, "Prefeitura de Itumbiara - Homologa��o", "sigep", true, false, false, false, ""),
    BARBACENA_NOVO_PROD		(209, "Prefeitura de Barbacena (Novo) - Produ��o", "flyenota", true, false, false, false, ""),
	BARBACENA_NOVO_HOM		(210, "Prefeitura de Barbacena (Novo) - Homologa��o", "flyenota", true, false, false, false, ""),
	ITUMBIARA_NOVO_PROD     (211, "Prefeitura de Itumbiara (NOVO) - Produ��o", "centi", true, false, false, false, ""),
    ITUMBIARA_NOVO_HOM      (212, "Prefeitura de Itumbiara (NOVO) - Homologa��o", "centi", true, false, false, false, ""),
    SAO_MIGUEL_DO_ARAGUAIA_PROD	(213, "S�o Miguel do Araguaia - Produ��o", "centi", true, false, false, false, ""),
    SAO_MIGUEL_DO_ARAGUAIA_HOM	(214, "S�o Miguel do Araguaia - Homologa��o", "centi", true, false, false, false, ""),
    ;
	
	private Integer value;
	private String descricao;
	private String prefixo;
	private boolean nfservico;
	private boolean mdfe;
	private boolean dde;
	private boolean md;
	private String estadoservico;
	
	private Prefixowebservice(Integer value, String descricao, String prefixo, boolean nfservico, boolean mdfe, boolean dde, boolean md, String estadoservico){
		this.value = value;
		this.descricao = descricao;
		this.prefixo = prefixo;
		this.nfservico = nfservico;
		this.mdfe = mdfe;
		this.dde = dde;
		this.estadoservico = estadoservico;
		this.md = md;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getPrefixo() {
		return prefixo;
	}
	
	public boolean isNfservico() {
		return nfservico;
	}
	
	public boolean isMdfe() {
		return mdfe;
	}
	
	public boolean isDde() {
		return dde;
	}
	
	public boolean isMd() {
		return md;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	public String getEstadoservico() {
		return estadoservico;
	}
	
	public static String listAndConcatenate(List<Prefixowebservice> prefixos){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < prefixos.size(); i++)
			for (Prefixowebservice s : Prefixowebservice.values()){
				boolean existe = false;
				if(prefixos.get(i) instanceof Prefixowebservice){
					if((prefixos.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)prefixos.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
	
	public static String getAllPrefixos(List<Prefixowebservice> prefixos){
		StringBuilder prefixosConcat = new StringBuilder();
		for (int i = 0; i < prefixos.size(); i++)
			for (Prefixowebservice s : Prefixowebservice.values()){
				boolean existe = false;
				if(prefixos.get(i) instanceof Prefixowebservice){
					if((prefixos.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)prefixos.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					prefixosConcat.append("&nbsp;&nbsp;&nbsp" + s.getDescricao() + " - " + s.getPrefixo()+" <br>");
					break;
				}
			}
		
		if(prefixosConcat.length() > 0) prefixosConcat.delete(prefixosConcat.length()-2, prefixosConcat.length());
		return prefixosConcat.toString();
	}
	
	public static String getValuesPrefixosSefazHomolgoacao(){
		return 	SEFAZAM_HOM.getValue() + ", "
			  + SEFAZBA_HOM.getValue() + ", "
			  + SEFAZGO_HOM.getValue() + ", "
			  + SEFAZMG_HOM.getValue() + ", "
			  + SEFAZPE_HOM.getValue() + ", "
			  + SEFAZPR_HOM.getValue() + ", "
			  + SEFAZRS_HOM.getValue() + ", "
			  + SEFAZSP_HOM.getValue()
			  ;
	}
	
}
