package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tiposalario {

	MENSAL		("Mensal", 1), 				
	QUINZENAL	("Quinzenal", 2), 	
	SEMANAL		("Semanal", 3), 					
	DIARIO		("Di�rio", 4), 					
	HORARIO		("Hor�rio", 5), 					
	TAREFA		("Tarefa", 6), 					
	OUTROS		("Outros", 7);					
	
	private Tiposalario(String descricao, Integer value){
		this.value = value;
		this.descricao = descricao;
	}
	
	private Integer value;
	private String descricao;
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
}
