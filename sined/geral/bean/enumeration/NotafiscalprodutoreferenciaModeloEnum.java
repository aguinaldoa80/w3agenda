package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum NotafiscalprodutoreferenciaModeloEnum {
	
	NF						(0, "Nota Fiscal", "01"),
	NF_DE_PRODUTOR			(1, "Nota Fiscal de Produtor", "04"),
	CUPOM_FISCAL_NAO_ECF	(2, "Cupom Fiscal emitido por m�quina registradora (n�o ECF)", "2B"),
	CUPOM_FISCAL_PDV		(3, "Cupom Fiscal PDV", "2C"),
	CUPOM_FISCAL_ECF 		(4, "Cupom Fiscal (emitido por ECF)", "2D"),
	NF_VENDA 				(5, "Nota Fiscal de Venda a Consumidor", "02"),
	;
	
	private String codigoNFe;
	private Integer value;
	private String nome;
	
	private NotafiscalprodutoreferenciaModeloEnum(Integer value, String nome, String codigoNFe){
		this.value = value;
		this.nome = nome;
		this.codigoNFe = codigoNFe;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}

	public String getCodigoNFe() {
		return codigoNFe;
	}
}
