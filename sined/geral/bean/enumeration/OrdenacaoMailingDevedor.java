package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoMailingDevedor {

	CLIENTE ("cliente",	"Cliente"), 		
	DATA_COBRANCA("dtultimacobranca", "Data de Cobran�a"),			
	DATA_VENCIMENTO ("dtvencimento", "Data de Vencimento"),
	VALOR ("valor", "Valor");
	
	private OrdenacaoMailingDevedor(String campo, String nome){
		this.nome = nome;
		this.campo = campo;
	}
	
	private String campo;
	private String nome;
	
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
