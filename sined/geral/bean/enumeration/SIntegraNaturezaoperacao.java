package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SIntegraNaturezaoperacao {

	INTERESTADUAIS_SOMENTE_OPERACOES_REGIME_SUBSTITUICAO_TRIBUTARIA	(0,"1","Interestaduais somente operações sujeitas ao regime de Substituição Tributária"),
	INTERESTADUAIS_COM_OU_SEM_SUBSTITUICAO_TRIBUTARIA				(1,"2","Interestaduais - operações com ou sem Substituição Tributária"),
	TOTALIDADE_OPERACOES_INFORMANTE									(2,"3","Totalidade das operações do informante");
	
	private Integer value;
	private String cdsintegra;
	private String nome;
	
	private SIntegraNaturezaoperacao(Integer value, String cdsintegra, String nome){
		this.value = value;
		this.cdsintegra = cdsintegra;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	public String getCdsintegra() {
		return cdsintegra;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override	
	public String toString(){
		return nome;
	}
}
