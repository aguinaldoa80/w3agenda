package br.com.linkcom.sined.geral.bean.enumeration;

public enum Vendedortipo {
	
	COLABORADOR		(0,"Colaborador"),
	FORNECEDOR		(1,"Fornecedor");
	
	private Integer cdvendedortipo;
	private String descricao;	
	
	private Vendedortipo(Integer cdvendedortipo, String descricao) {
		this.cdvendedortipo = cdvendedortipo;
		this.descricao = descricao;
	}
	
	
	public Integer getCdvendedortipo() {
		return cdvendedortipo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdvendedortipo(Integer cdvendedortipo) {
		this.cdvendedortipo = cdvendedortipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
	
	

}
