package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoVeiculouso {
	
	FINALIZADO(0,"Finalizado"),
	EM_USO(1,"Em uso"),
	CANCELADO(2,"Cancelado"),
	AGUARDANDO(3,"Aguardando uso");
	
	private Integer value;
	private String nome;
	
	private SituacaoVeiculouso(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<SituacaoVeiculouso> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoVeiculouso s : SituacaoVeiculouso.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoVeiculouso){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
	
}
