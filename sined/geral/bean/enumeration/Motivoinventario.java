package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Motivoinventario {
	
	FINAL_PERIODO							(0,"01","No final no per�odo"),
	MUDANCA_TRIBUTACAO_MERCADORIA_ICMS		(1,"02","Na mudan�a de forma de tributa��o da mercadoria (ICMS)"),
	SOLICITACAO_TEMPORARIA_OUTRAS_SITUACOES	(2,"03","Na solicita��o da baixa cadastral, paralisa��o	tempor�ria e outras situa��es"),
	ALTERACAO_REGIME_PAGAMENTO				(3,"04","Na altera��o de regime de pagamento - condi��o	do contribuinte"),
	POR_SOLICITACAO_FISCALIZACAO			(4,"05"," Por solicita��o da fiscaliza��o");
	
	private Integer value;
	private String cdSped; 
	private String desricao;
	
	private Motivoinventario(Integer value, String cdSped, String descricao){
		this.value = value;
		this.cdSped = cdSped;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getCdSped() {
		return cdSped;
	}
	@DescriptionProperty
	public String getDesricao() {
		return desricao;
	}

	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
