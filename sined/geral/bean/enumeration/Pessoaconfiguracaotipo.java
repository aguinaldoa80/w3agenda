package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Pessoaconfiguracaotipo {

	CLIENTE		(0, "Cliente"), 
	COLABORADOR	(1, "Colaborador"),
	EMPRESA		(2, "Empresa"),
	FORNECEDOR	(3, "Fornecedor")
	;
	
	private Pessoaconfiguracaotipo (Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	private Integer value;
	private String nome;
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
