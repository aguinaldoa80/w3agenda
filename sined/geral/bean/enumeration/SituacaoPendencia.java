package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoPendencia {

	NEGOCIACAO_PENDENTE					(0, "Negocia��o Pendente"),
	PENPENCIA_NEGOCIADA					(1, "Pend�ncia Negociada"),
	PEDENCIA_RESOLVIDA_SEM_NEGOCIACAO	(2, "Pend�ncia Resolvida Sem Negocia��o"),
	PEDIDO_PREVISTO						(3, "Pedido de venda indo para previsto");
	
	
	
	
	private Integer value;
	private String nome;
	
	private SituacaoPendencia(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<SituacaoPendencia> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoPendencia pendencia : SituacaoPendencia.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoPendencia){
					if((situacoes.get(i)).equals(pendencia)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(pendencia.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(pendencia.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
	
	public static String listAndConcatenateDescription(List<SituacaoPendencia> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (SituacaoPendencia s : SituacaoPendencia.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof SituacaoPendencia){
					if((situacoes.get(i).getValue()) == s.getValue()){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getNome()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
