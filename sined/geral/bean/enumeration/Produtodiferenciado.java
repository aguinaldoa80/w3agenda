package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Produtodiferenciado {
	
	NORMAL			(0, "Produto Normal"),
	CIGARRO			(1, "Cigarro"),
	VASILHAME		(2, "Vasilhame"),
	PIS_COFINS		(3, "PIS ou Cofins"),
	VALE_GAS 		(4, "Vale G�s"),
	TAXA_ENTREGA 	(5, "Taxa de Entrega");
	
	private Integer value;
	private String nome;
	
	private Produtodiferenciado(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
