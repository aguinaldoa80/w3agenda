package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Codigoregimetributario {
	
	SIMPLES_NACIONAL			(0, 1, "Simples Nacional"),
	SIMPLES_NACIONAL_EXCESSO	(1, 2, "Simples Nacional - excesso de sublimite de receita bruta"),
	NORMAL 						(2, 3, "Regime normal");
	
	private Integer value;
	private String descricao;
	private Integer cdnfe;
	
	private Codigoregimetributario(Integer value, Integer cdnfe, String descricao){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
