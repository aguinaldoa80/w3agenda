package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivofolhaorigem {
	
	EXPORTACAO_COLABORADOR	(0, "Exportação Colaborador"),
	IMPORTACAO_FOLHA		(1, "Importação de Folha");
	
	private Integer value;
	private String descricao;
	
	private Arquivofolhaorigem(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
