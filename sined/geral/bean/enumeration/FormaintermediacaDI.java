package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaintermediacaDI {
	
	CONTA_PROPRIA	(0, 1, "Importa��o por conta pr�pria", "Conta pr�pria"),
	CONTA_ORDEM		(1, 2, "Importa��o por conta e ordem", "Conta e ordem"),
	ENCOMENDA 		(2, 3, "Importa��o por encomenda", "Encomenda");
	
	private Integer value;
	private String descricao;
	private String descricaoReduzida;
	private Integer cdnfe;
	
	private FormaintermediacaDI(Integer value, Integer cdnfe, String descricao, String descricaoReduzida){
		this.value = value;
		this.cdnfe = cdnfe;
		this.descricao = descricao;
		this.descricaoReduzida = descricaoReduzida;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public String getDescricaoReduzida() {
		return descricaoReduzida;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	

}
