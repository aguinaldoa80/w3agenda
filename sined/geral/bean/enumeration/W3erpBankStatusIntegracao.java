package br.com.linkcom.sined.geral.bean.enumeration;

public enum W3erpBankStatusIntegracao {

	SINCRONIZAR,
	SINCRONIZADO,
	ERRO_CRIACAO,
	SINCRONIZAR_VENCIMENTO,
	CONSULTAR_VENCIMENTO,
	ERRO_VENCIMENTO,
	SINCRONIZAR_CANCELAMENTO,
	CONSULTAR_CANCELAMENTO,
	ERRO_CANCELAMENTO;
	
}
