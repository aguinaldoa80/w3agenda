package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Arquivofolhatipo {
	
	ENVIO_BANCARIO	(0, "Envio Banc�rio"),
	DEPOSITO		(1, "Dep�sito");
	
	private Integer value;
	private String descricao;
	
	private Arquivofolhatipo(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
