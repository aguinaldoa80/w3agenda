package br.com.linkcom.sined.geral.bean.enumeration;

public enum DocumentocomissaoOrigem {
	VENDA			(0,"Venda"),
	CONTRATO		(1,"Contrato"),
	PEDIDOVENDA		(2,"Pedido de Venda");
	
	private Integer cdDocumentoComissaoOrigem;
	private String descricao;	
	
	private DocumentocomissaoOrigem (Integer cdDocumentoComissaoOrigem, String descricao) {
		this.cdDocumentoComissaoOrigem = cdDocumentoComissaoOrigem;
		this.descricao = descricao;
	}
	
	
	public Integer getCdDocumentoComissaoOrigem() {
		return cdDocumentoComissaoOrigem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCdDocumentoComissaoOrigem(Integer cdDocumentoComissaoOrigem) {
		this.cdDocumentoComissaoOrigem = cdDocumentoComissaoOrigem;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return this.getDescricao();
	}
}
