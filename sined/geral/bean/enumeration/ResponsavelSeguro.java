package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ResponsavelSeguro {

	EMITENTE_MDFE								(0, "1", "1 - Emitente do MDFe"),
	RESPONSAVEL_CONTRATACAO_SERVICO_TRANSPORTE	(1, "2", "2 - Responsável pela Contratação do Serviço de Transporte (Contratante).");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private ResponsavelSeguro(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
