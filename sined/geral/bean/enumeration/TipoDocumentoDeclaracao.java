package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoDocumentoDeclaracao {
	
	
	DECLARACAO_EXPORTACAO	(0 ,"Declaração de Exportação(DE)"),
	DECLARACAO_SIMPLIFICADA	(1 ,"Declaração Simplificada de Exportação(DSE)"),
	DECLARACAO_UNICA		(2 ,"Declaração Única de Exportação");
	
	private Integer id;
	private String nome;
	private TipoDocumentoDeclaracao(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	public Integer getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String toString() {
		return getNome();
	}
}
