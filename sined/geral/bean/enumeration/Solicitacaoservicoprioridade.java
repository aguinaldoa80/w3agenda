package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Solicitacaoservicoprioridade {
	
	SEM_PRIORIDADE	(0, "Sem prioridade"),
	BAIXA			(1, "Baixa"),
	MEDIA			(2, "M�dia"),
	ALTA			(3, "Alta"),
	URGENTE			(4, "Urgente");
	
	private Integer value;
	private String nome;
	
	private Solicitacaoservicoprioridade(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Solicitacaoservicoprioridade> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Solicitacaoservicoprioridade s : Solicitacaoservicoprioridade.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Solicitacaoservicoprioridade){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
