package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoUnidadeTransporte {

	RODOVIARIO_TRACAO_TIPO_CAMINHAO(0, "1", "1 - Rodovi�rio Tra��o do Tipo Caminh�o"),
	RODOVIARIO_REBOQUE_TIPO_CARRETA(1, "2", "2 - Rodovi�rio Reboque do Tipo Carreta");
	
	private Integer value;
	private String cdnfe;
	private String nome;
	
	private TipoUnidadeTransporte(Integer _value, String _cdnfe, String _nome) {
		this.value = _value;
		this.cdnfe = _cdnfe;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getCdnfe() {
		return cdnfe;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
