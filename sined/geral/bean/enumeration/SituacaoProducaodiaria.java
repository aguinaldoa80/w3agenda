package br.com.linkcom.sined.geral.bean.enumeration;

import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoProducaodiaria {

	FATURADA(true,"Faturada"),
	A_FATURAR(false,"Em aberto");
	
	private SituacaoProducaodiaria (Boolean value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	private Boolean value;
	private String nome;
	
	@Id
	public Boolean getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
