package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tabelaandroid {
	
	PARAMETROGERAL					(0, ""),
	ENDERECOTIPO					(1, ""),
	MATERIALGRUPO					(2, ""),
	UNIDADEMEDIDA 					(3, ""),
	UF								(4, ""),
	MUNICIPIO						(5, ""),
	CONTATOTIPO						(6, ""),
	COLABORADOR						(7, ""),
	FORNECEDOR						(8, ""),
	EMPRESA							(9, ""),
	CLIENTE							(10, ""),
	RESPONSAVELFRETE				(11, ""),
	CATEGORIA						(12, ""),
	BANCO							(13, ""),
	CONTA							(14, ""),
	DOCUMENTOTIPO					(15, ""),
	FORMAPAGAMENTO					(16, ""),
	INDICECORRECAO					(17, ""),
	PEDIDOVENDATIPO					(18, ""),
	PRAZOPAGAMENTO					(19, ""),
	PROJETO							(20, ""),
	MATERIAL						(21, ""),
	MATERIALTABELAPRECO				(22, ""),
	ENDERECO						(23, ""),
	CONTATO							(24, ""),
	VALECOMPRA						(25, ""),
	MATERIALTABELAPRECOITEM			(26, ""),
	MATERIALTABELAPRECOCLIENTE		(27, ""),
	MATERIALUNIDADEMEDIDA			(28, ""),
	MATERIALSIMILAR					(29, ""),
	MATERIALRELACIONADO				(30, ""),
	PRAZOPAGAMENTOITEM				(31, ""),
	USUARIOEMPRESA					(32, ""),
	CLIENTEVENDEDOR					(33, ""),
	PEDIDOVENDAHISTORICO			(34, ""),
	MATERIALTABELAPRECOEMPRESA		(35, ""),
	PESSOACATEGORIA					(36, ""),
	ATIVIDADETIPO					(37, ""),
	CLIENTEDOCUMENTOTIPO			(38, ""),
	CLIENTEHISTORICO				(39, ""),
	CLIENTEPRAZOPAGAMENTO			(40, ""),
	DOCUMENTO						(41, ""),
	DOCUMENTOHISTORICO				(42, ""),
	LOCALARMAZENAGEM				(43, ""),
	MEIOCONTATO						(44, ""),
	PNEU							(45, ""),
	PNEUMARCA						(46, ""),
	PNEUMEDIDA						(47, ""),
	PNEUMODELO						(48, ""),
	SITUACAOHISTORICO				(49, ""),
	LOCALARMAZENAGEMEMPRESA			(50, ""),
	MATERIALPRODUCAO				(51, ""),
	MATERIALFORMULAVALORVENDA		(52, ""),
	TABELAVALOR						(53, ""),
	CONTAEMPRESA					(54, ""),
	MATERIALEMPRESA					(55, "");
	
	private Integer value;
	private String desricao;
	
	private Tabelaandroid(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}

	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
