package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum OrdenacaoNotafiscalservicoReport {

	NUMERO_NOTA	("N�mero Nota"),		
	NUMERO_NFSE	("N�mero NFS-e");		
	
	private OrdenacaoNotafiscalservicoReport(String nome){
		this.nome = nome;
	}
	
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
