package br.com.linkcom.sined.geral.bean.enumeration;

public enum Tabelaw3producao {

	USUARIO							(0, ""),
	EMPRESA							(1, ""),
	CLIENTE							(2, ""),
	MATERIAL						(3, ""),
	MATERIALSIMILAR					(4, ""),
	MATERIALPRODUCAO				(5, ""),
	PATRIMONIOITEM	 				(6, ""),
	UNIDADEMEDIDA					(7, ""),
	PRODUCAOETAPA	 				(8, ""),
	PRODUCAOETAPAITEM 				(9, ""),
	MOTIVODEVOLUCAO					(10, ""),
	PNEU							(11, ""),
	PNEUMARCA						(12, ""),
	PNEUMODELO						(13, ""),
	PNEUMEDIDA						(14, ""),
	MATERIALGRUPO					(15, ""),
	PRODUCAOETAPANOME				(16, ""),
	PRODUCAOETAPANOMECAMPO			(17, ""),
	MATERIALUNIDADEMEDIDA			(18, ""),
	PEDIDOVENDA						(19, ""),
	MATERIALPNEUMODELO				(20, ""),
	PNEUQUALIFICACAO				(21, ""),
	GARANTIATIPO					(22, ""),
	GARANTIATIPOPERCENTUAL			(23, ""),
	CLIENTEVENDEDOR					(24, ""),
	;
	
	private Integer value;
	private String desricao;
	
	private Tabelaw3producao(Integer value, String desricao) {
		this.value = value;
		this.desricao = desricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}

	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}	
}
