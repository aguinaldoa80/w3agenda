package br.com.linkcom.sined.geral.bean.enumeration;

public enum FinalidadeDOC {
	
	CREDITO_CONTA("01", "Cr�dito em Conta"),
	PAGAMENTO_ALUGUEL("02", "Pagamento de Aluguel / Condom�nio"),
	PAGAMENTO_DUPLICATA("03", "Pagamento de Duplicata / T�tulos"),
	PAGAMENTO_DIVIDENDO("04", "Pagamento de Dividendos"),
	PEGAMENTO_MENSALIDADE_ESCOLAR("05", "Pagamento de Mensalidade Escolar"),
	PAGAMENTO_SALARIOS("06", "Pagamento de Sal�rios"),
	PAGAMENTO_FORNECEDORES("07", "Pagamento de Fornecedores / Honor�rios"),
	OPERACOES_CAMBIO("08", "Opera��es de C�mbio / Fundos"),
	PAGAMENTO_TRIBUTOS("09", "Repasse de Arrecada��o / Pagamento de Tributos"),
	TRANSFERENCIA_INTERNACIONAL("10", "Transfer�ncia Internacional de Reais"),
	DOC_POUPANCA("11", "DOC para Poupan�a"),
	DOC_DEPOSITO_JUDICIAL("12", "DOC para Dep�sito Judicial"),
	PENSAO_ALIMENTICIA("13", "Pens�o Aliment�cia"),
	OUTROS("99", "Outros");
	
	private String codigo;
	private String descricao;
	
	private FinalidadeDOC(String codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	@Override
	public String toString() {
		return descricao;
	}

}
