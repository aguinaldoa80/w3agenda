package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FreteGratisEnum {
	
	SEMPRE					(0, "Sempre", "sempre"),
	NUNCA				(1, "Nunca", "nunca"),
	NEUTRO				(2, "Neutro", "neutro"),
	DESCONSIDERARREGRAS	(2, "Desconsiderar Regras", "desconsiderar_regras");

	private Integer value;
	private String nome;
	private String label;
	
	private FreteGratisEnum(Integer _value, String _nome, String _label) {
		this.value = _value;
		this.nome = _nome;
		this.label = _label;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getNome() {
		return nome;
	}
	
	@DescriptionProperty
	public String getLabel() {
		return label;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}