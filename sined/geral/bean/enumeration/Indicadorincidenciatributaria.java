package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indicadorincidenciatributaria {

	INCIDENCIA_EXCLUSIVAMENTE_REGIME_NAO_CUMULATIVO 				(1, "Escritura��o de opera��es com incid�ncia exclusivamente no regime n�o-cumulativo"),
	INCIDENCIA_EXCLUSIVAMENTE_REGIME_CUMULATIVO						(2, "Escritura��o de opera��es com incid�ncia exclusivamente no regime cumulativo"),
	INCIDENCIA_EXCLUSIVAMENTE_REGIME_CUMULATIVO_E_NAO_CUMULATIVO	(3, "Escritura��o de opera��es com incid�ncia nos regimes n�o-cumulativo e cumulativo");
	
	private Integer value;
	private String desricao;
	
	private Indicadorincidenciatributaria(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getDesricao() {
		return desricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
