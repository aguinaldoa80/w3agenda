package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contratofaturalocacaosituacao {

	EMITIDA(0, "Emitida"), 			// 0 
	CANCELADA(1, "Cancelada"), 		// 1
	FATURADA(2, "Faturada")			// 2
	; 
	
	private Contratofaturalocacaosituacao (Integer codigo, String nome){
		this.codigo = codigo;
		this.nome = nome;
	}
	
	private Integer codigo;
	private String nome;
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
	
	public static String listAndConcatenate(List<Contratofaturalocacaosituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Contratofaturalocacaosituacao s : Contratofaturalocacaosituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Contratofaturalocacaosituacao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.ordinal()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
