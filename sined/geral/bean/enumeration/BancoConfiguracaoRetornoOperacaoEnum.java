package br.com.linkcom.sined.geral.bean.enumeration;

public enum BancoConfiguracaoRetornoOperacaoEnum {

	PROCESSAR("Processar"),
	REGISTRAR("Registrar"),
	IDENTIFICAR("Identificar");	
		
	private String nome;
		
	private BancoConfiguracaoRetornoOperacaoEnum(String nome){
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
		
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}
		
}
