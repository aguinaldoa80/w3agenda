package br.com.linkcom.sined.geral.bean.enumeration;

public enum EnumArquivoConciliacaoOperadora {
	HEADER(0,250,250),
	DETALHE_RESUMO_OPERACAO(0,250,250),
	DETALHE_COMPROVANTE_VENDA(0,250,250),
	OPCAO_DO_EXTRATO(47,49,2),
	ESTABELECIMENTO(1,11,10),
	NUMERO_RO(11,18,7),
	TIPO_TRANSACAO(23,25,2),
	DATA_APRESENTACAO(25,31,6),
	DATA_PREVISTA_PAGAMENTO(31,37,6),
	DATA_ENVIO_BANCO(37,43,6),
	VALOR_BRUTO(44,57,13),
	VALOR_LIQUIDO(86,99,13),
	STATUS_PAGAMENTO(122,124,2),
	DATA_VENDA(37,45,8),
	VALOR_VENDA(46,59,13),
	PARCELA(59,61,2),
	TOTAL_PARCELAS(61,63,2),
	MOTIVO_REJEICAO(63,66,3),
	CODIGO_AUTORIZACAO(66,72,6),
	TID(72,92,20),
	NSU_DOC(92,98,6);
	
	private Integer inicio;
	private Integer fim;
	private Integer tamanho;
	
	private EnumArquivoConciliacaoOperadora(Integer inicio, Integer fim, Integer tamanho) {
		this.inicio = inicio;
		this.fim = fim;
		this.tamanho = tamanho;
	}
	
	public Integer getInicio() {
		return inicio;
	}
	public Integer getFim() {
		return fim;
	}
	public Integer getTamanho() {
		return tamanho;
	}
	
	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}
	public void setFim(Integer fim) {
		this.fim = fim;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	
}
