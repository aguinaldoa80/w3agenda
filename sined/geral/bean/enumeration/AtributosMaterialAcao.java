package br.com.linkcom.sined.geral.bean.enumeration;

public enum AtributosMaterialAcao {
	CRIADA		(0, "Criada"),
	ALTERADA	(1, "Alterada"),
	;
	
	private Integer value;
	private String nome;
	
	
	private AtributosMaterialAcao(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}


	public Integer getValue() {
		return value;
	}


	public void setValue(Integer value) {
		this.value = value;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
}
