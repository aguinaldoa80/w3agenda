package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipocoluna {

	COLUNA_FIXA(0, "Coluna Fixa"),
	COLUNA_MOVEL(1, "Coluna M�vel");
	
	private Tipocoluna (Integer value, String tipo){
		this.tipo = tipo;
		this.value = value;
	}
	
	private Integer value;
	private String tipo;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}
}
