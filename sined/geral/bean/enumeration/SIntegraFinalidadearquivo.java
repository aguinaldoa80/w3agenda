package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SIntegraFinalidadearquivo {

	NORMAL						(0,"1", "Normal"),
	RETIFICACAO_TOTAL_ARQUIVO	(1,"2", "Retifica��o total de arquivo: substitui��o total de informa��es prestadas pelo contribuinte referentes a este per�odo "),
	RETIFICACAO_ADITIVA_ARQUIVO	(2,"3", "Retifica��o aditiva de arquivo: acr�scimo de informa��o n�o inclu�da em arquivos j� apresentados"),
	DESFAZIMENTO				(3,"5","Desfazimento: arquivo de informa��o referente a opera��es/presta��es n�o efetivadas");
	
	private Integer value;
	private String cdsintegra;
	private String nome;
	
	private SIntegraFinalidadearquivo(Integer value, String cdsintegra, String nome){
		this.value = value;
		this.cdsintegra = cdsintegra;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	public String getCdsintegra() {
		return cdsintegra;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override	
	public String toString(){
		return nome;
	}
}
