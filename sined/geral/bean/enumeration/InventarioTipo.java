package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum InventarioTipo {
	
	INVENTARIO			(0,"Inventário"),
	ESTOQUEESTRUTURADO	(1,"Estoque Escriturado"),
	CORRECAO			(2,"Ajuste de Estoque Escriturado");
	
	
	private int value;
	private String nome;
	
	InventarioTipo(){}
	
	private InventarioTipo(int value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	
	public int getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
	
}
