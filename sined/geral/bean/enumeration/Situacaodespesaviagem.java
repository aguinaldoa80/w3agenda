package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaodespesaviagem {
	
	EM_ABERTO(0,"EM ABERTO"),
	AUTORIZADA(1,"AUTORIZADA"),
	REALIZADA(2,"REALIZADA"),
	BAIXADA(3,"BAIXADA"),
	CANCELADA(4,"CANCELADA");
	
	private Situacaodespesaviagem (Integer codigo){
		this.codigo = codigo;
	}
	
	private Situacaodespesaviagem (Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	private Integer codigo;
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<Situacaodespesaviagem> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Situacaodespesaviagem s : Situacaodespesaviagem.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Situacaodespesaviagem){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getCodigo()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
	
	/**
	 * Retorna todos os valores do enum em uma lista.
	 *
	 * @return
	 * @author Rodrigo Freitas
	 */
	public static List<Situacaodespesaviagem> findAll(){
		List<Situacaodespesaviagem> lista = new ArrayList<Situacaodespesaviagem>();
		for (int i = 0; i < Situacaodespesaviagem.values().length; i++) {
			lista.add(Situacaodespesaviagem.values()[i]);
		}
		return lista;
	}
}
