package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoEnvioNFCliente {
	SERVICO_PDF, SERVICO_XML, PRODUTO_PDF, PRODUTO_XML
}
