package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Camponota {
	
	CAMPO_LIVRE 			(0, "Campo livre", false),
	NUMERO_NOTA 			(1, "N�mero da nota", false), 
	DTEMISSAO_NOTA 			(2, "Data de emiss�o da nota", false), 
	END_CLIENTE 			(3, "Endere�o do cliente", false), 
	ENDNUM_CLIENTE 			(4, "Endere�o com n�mero do cliente", false), 
	ENDNUMCOMP_CLIENTE 		(5, "Endere�o com n�mero e complemento do cliente", false), 
	ENDNUMCOMPBAI_CLIENTE 	(6, "Endere�o com n�mero, complemento e bairro do cliente", false), 
	NUMERO_CLIENTE 			(7, "N�mero do endere�o do cliente", false), 
	COMPLEMENTO_CLIENTE 	(8, "Complemento do endere�o do cliente", false), 
	BAIRRO_CLIENTE 			(9, "Bairro do endere�o do cliente", false), 
	CEP_CLIENTE 			(10, "Cep do cliente", false), 
	MUNICIPIO_CLIENTE 		(11, "Munic�pio do cliente", false), 
	MUNICIPIOUF_CLIENTE 	(12, "Munic�pio e sigla da UF do cliente", false), 
	UFDESCRICAO_CLIENTE 	(13, "Descri��o da UF do cliente", false), 
	UFSIGLA_CLIENTE 		(14, "Sigla da UF do cliente", false), 
	CPFCNPJ_CLIENTE 		(15, "CPF/CNPJ do cliente", false), 
	INSCESTADUAL_CLIENTE 	(16, "Inscri��o estadual do cliente", false),
	INSCMUNICIPAL_CLIENTE 	(17, "Inscri��o municipal do cliente", false),
	RAZAOSOCIALNOME_CLIENTE	(18, "Raz�o social ou nome do cliente", false),
	RAZAOSOCIAL_CLIENTE		(19, "Raz�o social do cliente", false),
	NOME_CLIENTE			(20, "Nome do cliente", false),
	DIAEMISSAO_NOTA			(21, "Dia da emiss�o da nota", false),
	MESNUMEMISSAO_NOTA		(22, "N�mero do m�s da emiss�o da nota", false),
	MESDESCRICAOEMISSAO_NOTA(23, "Descri��o do m�s da emiss�o da nota", false),
	ANOEMISSAO_NOTA			(24, "Ano da emiss�o da nota", false),
	VALORSERVICOS_NOTA		(25, "Valor dos servi�os da nota sem impostos", false),
	VALORRETENCAO_NOTA		(26, "Valor de reten��o total da nota", false),
	VALORIMPOSTOS_NOTA		(27, "Valor total dos impostos retidos e n�o retidos da nota", false),
	VALORISS_NOTA			(28, "Valor do ISS da nota", false),
	PERCENTUALISS_NOTA		(29, "Percentual do ISS da nota", false),
	VALORIR_NOTA			(30, "Valor do IR da nota", false),
	PERCENTUALIR_NOTA		(31, "Percentual do IR da nota", false),
	VALORINSS_NOTA			(32, "Valor do INSS da nota", false),
	PERCENTUALINSS_NOTA		(33, "Percentual do INSS da nota", false),
	VALORPIS_NOTA			(34, "Valor do PIS da nota", false),
	PERCENTUALPIS_NOTA		(35, "Percentual do PIS da nota", false),
	VALORCOFINS_NOTA		(36, "Valor do COFINS da nota", false),
	PERCENTUALCOFINS_NOTA	(37, "Percentual do COFINS da nota", false),
	VALORCSLL_NOTA			(38, "Valor do CSLL da nota", false),
	PERCENTUALCSLL_NOTA		(39, "Percentual do CSLL da nota", false),
	VALORICMS_NOTA			(40, "Valor do ICMS da nota", false),
	PERCENTUALICMS_NOTA		(41, "Percentual do ICMS da nota", false),
	DUPLICATA_NOTA			(42, "N�mero de ordem da duplicata da nota", false),
	FATURA_NOTA				(43, "N�mero da fatura da nota", false),
	PRACAPAGAMENTO_NOTA		(44, "Pra�a de pagemento da nota", false),
	VALORTOTALEXTENSO_NOTA	(45, "Valor total da nota por extenso", false),
	VOLORTOTAL_NOTA			(46, "Valor total da nota", false),
	DTVENCIMENTO_NOTA		(47, "Data de vencimento da nota", false),
	QTDE_NOTAITEM			(48, "Quantidade do item", true),
	DISCRIMINACAOSERVICO_NOTAITEM	(49, "Discrimina��o do servi�o do item", true),
	VALORUNITARIO_NOTAITEM	(50, "Valor unit�rio do item", true),
	VALORTOTALITEM_NOTAITEM	(51, "Valor total do item", true),
	BASE_CALCULO			(52, "Base de c�lculo para o c�lculo dos impostos", false),
	DESCONTO_CONDICIONADO	(53, "Desconto condicionado", false),
	DESCONTO_INCONDICIONADO	(54, "Desconto incondicionado", false),
	DEDUCOES				(55, "Dedu��es", false),
	OUTRAS_RETENCOES		(56, "Outras reten��es", false),
	CDCONTRATO				(57, "C�digo do Contrato", false),
	TIPO_VENCIMENTO			(58, "Tipo de Vencimento", false),
	DTVENCIMENTO_NOTA_TIPO_VENCIMENTO (59, "Data de vencimento/Tipo de Vencimento", false),
	NATUREZA_OPERACAO		(60, "Natureza da opera��o", false),
	TELEFONE				(61, "Telefone", false),
	DADOS_ADICIONAIS		(62, "Dados adicionais", false),
	CDDOCUMENTO				(63, "C�digo do boleto", false),;
	
	private Integer value;
	private String descricao;
	private boolean corpo;
	
	private Camponota(Integer value, String descricao, boolean corpo){
		this.value = value;
		this.descricao = descricao;
		this.corpo = corpo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public boolean isCorpo() {
		return corpo;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
	public static List<Camponota> getCampos(boolean corpo){
		List<Camponota> lista = new ArrayList<Camponota>();
		for (int i = 0; i < Camponota.values().length; i++) {
			if(corpo){
				if(Camponota.values()[i].isCorpo()) lista.add(Camponota.values()[i]);
			} else {
				if(!Camponota.values()[i].isCorpo()) lista.add(Camponota.values()[i]);
			}
		}
		
		Collections.sort(lista,new Comparator<Camponota>(){
			public int compare(Camponota o1, Camponota o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		
		return lista;
	}
	
}
