package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ExigibilidadeIssNfe {
	
	EXIGIVEL				(1, "Exigivel"),
	NAO_INCIDENCIA			(2, "N�o Incidencia"),
	ISENCAO					(3, "Isen��o"),
	EXPORTACAO				(4, "Exporta��o"),
	IMUNIDADE				(8, "Imunidade"),
	SUSPENSA_JUDICIAL		(6, "Exigibilidade Suspensa por Decis�o Judicial"),
	SUSPENSA_ADMINISTRATIVO	(7, "Exigibilidade Suspensa por Processo Administrativo");
	
	private Integer value;
	private String nome;
	
	private ExigibilidadeIssNfe(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
