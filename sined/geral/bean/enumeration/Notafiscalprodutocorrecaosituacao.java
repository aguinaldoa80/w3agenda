package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Notafiscalprodutocorrecaosituacao {
	
	GERADA					(0, "Gerada"),
	PROCESSADA_COM_ERRO		(1, "Processada com erro"),
	PROCESSADA_COM_SUCESSO	(2, "Processada com sucesso");
	
	private Integer value;
	private String nome;
	
	private Notafiscalprodutocorrecaosituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
