package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Colaboradordespesasituacao {
	
	EM_ABERTO		(0,"Em aberto"),
	AUTORIZADO		(1,"Autorizado"),
	PROCESSADO		(2,"Processado"),	
	CANCELADO		(3,"Cancelado");
	
	private Integer value;
	private String nome;
	
	private Colaboradordespesasituacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override	
	public String toString(){
		return nome;
	}
	
	public static String listAndConcatenate(List<Colaboradordespesasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		if(situacoes != null){
			for (int i = 0; i < situacoes.size(); i++)
				for (Colaboradordespesasituacao s : Colaboradordespesasituacao.values()){
					boolean existe = false;
					if(situacoes.get(i) instanceof Colaboradordespesasituacao){
						if((situacoes.get(i)).equals(s)){
							existe = true;
						}
					} else{
						if(((Object)situacoes.get(i)).equals(s.name())){
							existe = true;
						}
					}
					if(existe){
						codigos.append(s.getValue()+", ");
						break;
					}
				}
			
			if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		}
		return codigos.toString();
	}
	
}
