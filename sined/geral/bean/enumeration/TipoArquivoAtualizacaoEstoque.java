package br.com.linkcom.sined.geral.bean.enumeration;

public enum TipoArquivoAtualizacaoEstoque {

	CSV_PADRAO("CSV Padr�o"),
	TXT_COLETOR("TXT do Coletor");
	
	private TipoArquivoAtualizacaoEstoque(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;
	
	@Override
	public String toString() {
		return descricao;
	}
	
}
