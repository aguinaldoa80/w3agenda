package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Agendainteracaorelacionado {

	MATERIAL_SERVICO (0, "Material/Servi�o"), 	
	CONTRATO		 (1, "Contrato"),
	AVULSO			 (2, "Avulso");			
	
	private Integer value;
	private String nome;
	
	private Agendainteracaorelacionado(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
