package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoPeriodoBalancoPatrimonial {
	
	ANUAL 	(0, "Anual"), 					
	MENSAL	(1, "Mensal");
	
	private TipoPeriodoBalancoPatrimonial (Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	private Integer value;
	private String nome;
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}
