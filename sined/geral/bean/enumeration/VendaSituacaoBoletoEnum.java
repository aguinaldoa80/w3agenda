package br.com.linkcom.sined.geral.bean.enumeration;

public enum VendaSituacaoBoletoEnum {

	TODAS(0, "Todas"),
	BOLETOSENVIADOS(1, "Boletos enviados"), 
	BOLETOSNAOENVIADOS(2, "Boletos n�o enviados");
	
	
	private Integer value;
	private String desricao;
	
	private VendaSituacaoBoletoEnum(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
	
}