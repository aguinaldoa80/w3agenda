package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum TipoPessoaEnum {

	PESSOA_FISICA(0, "F�sica"),
	PESSOA_JURIDICA(1, "Jur�dica");
	
	private TipoPessoaEnum (Integer value, String tipo){
		this.tipo = tipo;
		this.value = value;
	}
	
	private Integer value;
	private String tipo;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}
}