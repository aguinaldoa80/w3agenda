package br.com.linkcom.sined.geral.bean.enumeration;


public enum Situacaopedidosolicitacaocompra {
	
	SOLICITADO			(1,"Pedido de compra solicitado"),
	COMPRA_PARCIAL		(2,"Compra parcial"),
	COMPRA_REALIZADA	(3,"Compra realizada");
	
	private Integer value;
	private String desricao;
	
	private Situacaopedidosolicitacaocompra(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}

	public Integer getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
}
