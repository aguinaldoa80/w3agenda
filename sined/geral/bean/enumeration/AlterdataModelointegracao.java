package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum AlterdataModelointegracao {

	MODELO1						(1,"Modelo 1"),
	MODELO2						(2,"Modelo 2"),
	MODELO3						(3,"Modelo 3");
	
	private Integer value;
	private String nome;
	
	private AlterdataModelointegracao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
