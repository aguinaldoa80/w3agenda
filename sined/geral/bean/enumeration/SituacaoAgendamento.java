package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoAgendamento {
	
	NORMAL(1,"Normal"),
	A_CONSOLIDAR(2,"A Consolidar"),
	ATENCAO(3,"Aten��o"),
	ATRASADO(4,"Atrasado"), 
	FINALIZADO(5, "Finalizado");
	
	private Integer value;
	private String nome;
	
	private SituacaoAgendamento(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
