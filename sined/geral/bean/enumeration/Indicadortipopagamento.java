package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indicadortipopagamento {
	
	A_VISTA			(0, "� vista"),
	A_PRAZO			(1, "A prazo"),
	SEM_PAGAMENTO	(9, "Sem pagamento");

	private Integer value;
	private String nome;
	
	private Indicadortipopagamento(Integer _value, String _nome) {		
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}	
	
	@Override
	public String toString() {
		return nome;
	}
	
}

