package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Producaochaofabricasituacao {

	EM_ESPERA		(0, "Em espera"),
	EM_ANDAMENTO	(1, "Em andamento"),
	CONCLUIDA		(2, "Conclu�da"),
	CANCELADA 		(3, "Cancelada");

	private Integer value;
	private String nome;
	
	private Producaochaofabricasituacao(Integer _value, String _nome) {
		this.value = _value;
		this.nome = _nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Producaochaofabricasituacao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Producaochaofabricasituacao situacao : Producaochaofabricasituacao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Producaochaofabricasituacao){
					if((situacoes.get(i)).equals(situacao)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(situacao.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(situacao.getValue()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();	
	}
	
}
