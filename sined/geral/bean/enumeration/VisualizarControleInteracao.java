package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum VisualizarControleInteracao {

	SEMANAL (1, "Semanal"),
	QUINZENAL (2, "Quinzenal"),
	MENSAL (3, "Mensal");
	
	private Integer value;
	private String nome;
	
	private VisualizarControleInteracao(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}

	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
