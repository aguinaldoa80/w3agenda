package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum MeioPagamentoNFe {
	
	DINHEIRO				(1, "Dinheiro"),
	CHQUE					(2, "Cheque"),
	CARTAO_CREDITO			(3, "Cart�o de Cr�dito"),
	CARTAO_DEBITO			(4, "Cart�o de D�bito"),
	CREDITO_LOJA			(5, "Cr�dito Loja"),
	VALE_ALIMENTACAO		(10, "Vale Alimenta��o"),
	VALE_REFEICAO			(11, "Vale Refei��o"),
	VALE_PRESENTE			(12, "Vale Presente"),
	VALE_COMBUSTIVEL		(13, "Vale Combust�vel"),
	BOLETO_BANCARIO			(15, "Boleto Banc�rio"),
	SEM_PAGAMENTO			(90, "Sem pagamento"),
	OUTROS					(99, "Outros"),
	;
	
	private Integer value;
	private String nome;
	
	private MeioPagamentoNFe(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
