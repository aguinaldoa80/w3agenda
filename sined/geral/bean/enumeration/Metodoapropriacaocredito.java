package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Metodoapropriacaocredito {

	APROPRIACAO_DIRETA		(1,"M�todo de Apropria��o Direta"),
	RATEIO_PROPORCIONAL		(2,"M�todo de Rateio Proporcional (Receita Bruta)");		
	
	private Integer value;
	private String desricao;
	
	private Metodoapropriacaocredito(Integer value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	@DescriptionProperty
	public String getDesricao() {
		return desricao;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
