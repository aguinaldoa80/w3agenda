package br.com.linkcom.sined.geral.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Faixaimpostocontrole {
	
	ISS		(0,"ISS"),
	IPI		(1,"IPI"),
	COFINS	(2,"COFINS"),
	IR		(3,"IR"), 
	PIS		(4,"PIS"),
	CSLL	(5,"CSLL"),
	INSS	(6,"INSS"),
	ICMS	(7,"ICMS"),
	FCP		(8,"FCP"),
	FCPST	(9,"FCP ST")
	;
	
	private Integer value;
	private String nome;
	
	private Faixaimpostocontrole(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static String listAndConcatenate(List<Faixaimpostocontrole> situacoes){
		StringBuilder codigos = new StringBuilder();
		if(situacoes == null){
			return null;
		}
		for (int i = 0; i < situacoes.size(); i++)
			for (Faixaimpostocontrole s : Faixaimpostocontrole.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Faixaimpostocontrole){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getValue()+", ");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-2, codigos.length());
		return codigos.toString();
	}
}
