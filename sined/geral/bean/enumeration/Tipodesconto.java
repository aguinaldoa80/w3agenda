package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipodesconto {
	
	NF_SERVICO		(0, "Nota Fiscal de Servi�o"),
	NF_PRODUTO		(1, "Nota Fiscal de Produto"),
	CONTA_RECEBER 	(2, "Conta a receber");
	
	private Integer value;
	private String descricao;
	
	private Tipodesconto(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}
