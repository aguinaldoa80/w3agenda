package br.com.linkcom.sined.geral.bean.enumeration;


public enum RelatorioColaboradorAgendamentoOrderByEnum {
	
	CLIENTE("cliente_nome", 0, "Cliente"),
	SERVICO("servico_nome", 0, "Servi�o"),
	QTDE("quantidade", 0, "Quantidade"),
	CANCELADOS("cancelados", 0, "Cancelados"), 
	PREVISTO("previsto", 0, "Previsto"),
	DEVIDO("devido", 1, "Devido"),
	RECEBIDO("recebido", 1, "Recebido"),
	SALDO("saldo", 1, "Saldo");
	
	private String campo;
	private int query;
	private String descricao;
	
	private RelatorioColaboradorAgendamentoOrderByEnum(String campo, int query, String descricao){
		this.campo = campo;
		this.query = query;
		this.descricao = descricao;
	}
	
	public String getCampo() {
		return campo;
	}
	
	public int getQuery() {
		return query;
	}
	
	@Override
	public String toString() {
		return descricao;
	}
	
}
