package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum RetornoWMSEnum {
	
	CONFERIDA_OK			(0, "Conferida Ok"),
	CONFERIDA_DIVERGENCIA	(1, "Conferida Ok com Divergência"),
	CONFERIDA_REJEITADA		(2, "Conferida Rejeitada"),
	DEVOLVIDA				(3, "Devolvida");
	
	private Integer value;
	private String nome;
	
	private RetornoWMSEnum(Integer value, String nome) {
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
}
