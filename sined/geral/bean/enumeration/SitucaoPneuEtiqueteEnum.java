package br.com.linkcom.sined.geral.bean.enumeration;

public enum SitucaoPneuEtiqueteEnum {
	
	
	PRODUZIDO  	(0,"produzido"),
	CANCELADO 	(1,"cancelado"),
	INCOMPLETO	(2,"ordem em execu��o");
	
	private String nome;
	private Integer valor; 
	
	private SitucaoPneuEtiqueteEnum(Integer valor,String nome){
		this.nome = nome;
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	
	
}