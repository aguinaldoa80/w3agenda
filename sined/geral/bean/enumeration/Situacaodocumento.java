package br.com.linkcom.sined.geral.bean.enumeration;

public enum Situacaodocumento {
	
	DOCUMENTO_REGULAR									("00","Documento regular"),
	ESCRITURACAO_EXTEMP_DOC_REGULAR						("01","Escrituração extemporânea de documento regular"),
	DOCUMENTO_CANCELADO									("02","Documento cancelado"),
	ESCRITURACAO_EXTEMP_DOC_CANCELADO					("03","Escrituração extemporânea de documento cancelado"),
	NFE_CTE_DENEGADO									("04","NF-e ou CT-e - Denegado"),
	NFE_CTE_NUMERACAO_INUTILIZADA						("05","NF-e ou CT-e - Numeração inutilizada"),
	DOCUMENTO_FISCAL_COMPLEMENTAR						("06","Documento fiscal complementar"),
	ESCRITURACAO_EXTEMP_DOC_COMPLEMENTAR				("07","Escrituração extemporânea de documento complementar"),
	DOC_FISCAL_EMITIDO_REGIME_ESPECIAL_NORMA_ESPECIFICA	("08","Documento fiscal emitido com base em Regime Especial ou Norma Específica");
	
	private String value;
	private String desricao;
	
	private Situacaodocumento(String value, String descricao){
		this.value = value;
		this.desricao = descricao;
	}
	
	public String getValue() {
		return value;
	}
	public String getDesricao() {
		return desricao;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setDesricao(String desricao) {
		this.desricao = desricao;
	}
	
	@Override
	public String toString() {
		return getDesricao();
	}
}
