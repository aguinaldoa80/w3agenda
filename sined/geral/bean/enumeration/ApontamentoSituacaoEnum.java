package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum ApontamentoSituacaoEnum {

	REGISTRADO	(0, "Registrado", "2"), 
	AUTORIZADO	(1, "Autorizado", "1"),
	PAGO 		(2, "Pago", "3"); 
	
	private Integer id;
	private String nome;
	private String codigoicon;
		
	private ApontamentoSituacaoEnum(Integer id, String nome, String codigoicon){
		this.id = id;
		this.nome = nome;
		this.codigoicon = codigoicon;
	}
	
	public Integer getId() {
		return id;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCodigoicon() {
		return codigoicon;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCodigoicon(String codigoicon) {
		this.codigoicon = codigoicon;
	}

	public String toString() {
		return getNome();
	}	
}
