package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Indicadorpropriedade {

	PROPRIEDADE_PODER_DO_INFORMANTE					(0, 1, "Propriedade do informante e em seu poder"),
	PROPRIEDADE_DO_INFORMANTE__POSSE_TERCEIROS		(1, 2, "Propriedade do informante em posse de terceiros"),
	PROPRIEDADE_DE_TERCEIROS__POSSE_INFORMANTE		(2, 3, "Propriedade de terceiros em posse do informante");
	
	private Integer value;
	private Integer cdSIntegra; 
	private String desricao;
	
	private Indicadorpropriedade(Integer value, Integer cdSIntegra, String descricao){
		this.value = value;
		this.cdSIntegra = cdSIntegra;
		this.desricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}
	public Integer getCdSIntegra() {
		return cdSIntegra;
	}
	@DescriptionProperty
	public String getDesricao() {
		return desricao;
	}

	@Override
	public String toString() {
		return getDesricao().toUpperCase();
	}
}
