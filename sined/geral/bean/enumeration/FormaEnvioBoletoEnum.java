package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FormaEnvioBoletoEnum {

	POR_ANEXO 			(1, "Por anexo"),
	DOWNLOAD_CAPTCHA 	(2, "Download com Captcha"), 
	EMAIL_CONFIGURAVEL  (3, "E-mail configurável");
	
	private Integer value;
	private String descricao;
	
	private FormaEnvioBoletoEnum(Integer value, String descricao){
		this.value = value;
		this.descricao = descricao;
	}
	
	public Integer getValue() {
		return value;
	}

	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}
