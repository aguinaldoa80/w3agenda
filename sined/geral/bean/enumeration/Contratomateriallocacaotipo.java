package br.com.linkcom.sined.geral.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Contratomateriallocacaotipo {
	
	ENTRADA 	(0,"Entrada"),
	SAIDA 		(1,"Saida");
	
	private Integer value;
	private String nome;
	
	private Contratomateriallocacaotipo(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}
