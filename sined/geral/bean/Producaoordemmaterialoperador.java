package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_producaoordemmaterialoperador", sequenceName="sq_producaoordemmaterialoperador")
public class Producaoordemmaterialoperador {
	
	protected Integer cdproducaoordemmaterialoperador;
	protected Producaoordemmaterial producaoordemmaterial;
	protected Colaborador operador;
	protected Double qtdeproduzido;
	protected Double qtdeperdadescarte;
	protected List<Producaoordemmaterialperdadescarte> listaProducaoordemmaterialperdadescarte;
	
	//TRANSIENTS
	protected Integer index;
	protected String qtdeperdadescartetrans;
	protected String qtdeproduzidotrans;
	
	@Id
	@GeneratedValue(generator="sq_producaoordemmaterialoperador", strategy=GenerationType.AUTO)
	public Integer getCdproducaoordemmaterialoperador() {
		return cdproducaoordemmaterialoperador;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordemmaterial")
	@DisplayName("Operador")
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperador")
	public Colaborador getOperador() {
		return operador;
	}

	@MaxLength(9)
	@DisplayName("Produzido")
	public Double getQtdeproduzido() {
		return qtdeproduzido;
	}
	@MaxLength(9)
	@DisplayName("Perda/Descarte")
	public Double getQtdeperdadescarte() {
		return qtdeperdadescarte;
	}
	@OneToMany(mappedBy="producaoordemmaterialoperador")
	public List<Producaoordemmaterialperdadescarte> getListaProducaoordemmaterialperdadescarte() {
		return listaProducaoordemmaterialperdadescarte;
	}
	
	public void setCdproducaoordemmaterialoperador(Integer cdproducaoordemmaterialoperador) {
		this.cdproducaoordemmaterialoperador = cdproducaoordemmaterialoperador;
	}
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	public void setOperador(Colaborador operador) {
		this.operador = operador;
	}
	public void setQtdeproduzido(Double qtdeproduzido) {
		this.qtdeproduzido = qtdeproduzido;
	}
	public void setQtdeperdadescarte(Double qtdeperdadescarte) {
		this.qtdeperdadescarte = qtdeperdadescarte;
	}
	public void setListaProducaoordemmaterialperdadescarte(
			List<Producaoordemmaterialperdadescarte> listaProducaoordemmaterialperdadescarte) {
		this.listaProducaoordemmaterialperdadescarte = listaProducaoordemmaterialperdadescarte;
	}
	
	@Transient
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	@Transient
	public String getQtdeperdadescartetrans() {
		qtdeperdadescartetrans = this.qtdeperdadescarte != null? SinedUtil.descriptionDecimal(this.qtdeperdadescarte): "";
		return qtdeperdadescartetrans;
	}
	public void setQtdeperdadescartetrans(String qtdeperdadescartetrans) {
		this.qtdeperdadescartetrans = qtdeperdadescartetrans;
	}
	
	@Transient
	public String getQtdeproduzidotrans() {
		qtdeproduzidotrans = this.qtdeproduzido != null? SinedUtil.descriptionDecimal(this.qtdeproduzido): "";
		return qtdeproduzidotrans;
	}
	public void setQtdeproduzidotrans(String qtdeproduzidotrans) {
		this.qtdeproduzidotrans = qtdeproduzidotrans;
	}
}