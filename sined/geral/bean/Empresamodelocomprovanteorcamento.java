package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoComprovanteEnum;

@Entity
@SequenceGenerator(name="sq_empresamodelocomprovanteorcamento", sequenceName="sq_empresamodelocomprovanteorcamento")
public class Empresamodelocomprovanteorcamento {

	private Integer cdempresamodelocomprovanteorcamento;
	private Empresa empresa;
	private String nome;
	private Arquivo arquivo;
	private TipoComprovanteEnum tipoComprovante;
	
	@Id
	@GeneratedValue(generator="sq_empresamodelocomprovanteorcamento",strategy=GenerationType.AUTO)
	public Integer getCdempresamodelocomprovanteorcamento() {
		return cdempresamodelocomprovanteorcamento;
	}
	public void setCdempresamodelocomprovanteorcamento(
			Integer cdempresamodelocomprovanteorcamento) {
		this.cdempresamodelocomprovanteorcamento = cdempresamodelocomprovanteorcamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Required
	@DisplayName("Modelo")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Required
	@DisplayName("Arquivo RTF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@Required
	@DisplayName("Tipo de Comprovante")
	public TipoComprovanteEnum getTipoComprovante() {
		return tipoComprovante;
	}
	public void setTipoComprovante(TipoComprovanteEnum tipoComprovante) {
		this.tipoComprovante = tipoComprovante;
	}
	
}
