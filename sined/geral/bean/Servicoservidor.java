package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servicoservidor", sequenceName = "sq_servicoservidor")
public class Servicoservidor implements Log{
	
	protected Integer cdservicoservidor;
	protected Servidor servidor;
	protected Servicoservidortipo servicoservidortipo;
	protected String nome;
	protected Boolean primariodns;
	protected Boolean primariocx;
	protected String parametro;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;		
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicoservidor")
	public Integer getCdservicoservidor() {
		return cdservicoservidor;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")	
	public Servidor getServidor() {
		return servidor;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidortipo")
	@DisplayName("Tipo do servi�o")
	public Servicoservidortipo getServicoservidortipo() {
		return servicoservidortipo;
	}
	
	@Required
	@MaxLength(40)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("DNS prim�rio")
	public Boolean getPrimariodns() {
		return primariodns;
	}
	
	@DisplayName("CX prim�rio")
	public Boolean getPrimariocx() {
		return primariocx;
	}
	
	@Required
	@MaxLength(255)
	@DisplayName("Par�metro")
	public String getParametro() {
		return parametro;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdservicoservidor(Integer cdservicoservidor) {
		this.cdservicoservidor = cdservicoservidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public void setServicoservidortipo(Servicoservidortipo servicoservidortipo) {
		this.servicoservidortipo = servicoservidortipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPrimariodns(Boolean primariodns) {
		this.primariodns = primariodns;
	}

	public void setPrimariocx(Boolean primariocx) {
		this.primariocx = primariocx;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}		
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
}
