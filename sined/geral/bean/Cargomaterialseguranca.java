package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_cargomaterialseguranca", sequenceName = "sq_cargomaterialseguranca")
public class Cargomaterialseguranca implements Log{

	protected Integer cdcargomaterialseguranca;
	protected Cargo cargo;
	protected Epi epi;
	protected Double quantidade;
	protected Riscotrabalho riscotrabalho;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargomaterialseguranca")
	public Integer getCdcargomaterialseguranca() {
		return cdcargomaterialseguranca;
	}
	public void setCdcargomaterialseguranca(Integer id) {
		this.cdcargomaterialseguranca = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@Required
	public Epi getEpi() {
		return epi;
	}
	
	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Risco")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdriscotrabalho")
	public Riscotrabalho getRiscotrabalho() {
		return riscotrabalho;
	}
	
	public void setRiscotrabalho(Riscotrabalho riscotrabalho) {
		this.riscotrabalho = riscotrabalho;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setEpi(Epi epi) {
		this.epi = epi;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
