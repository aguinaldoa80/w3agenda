package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dominiozona", sequenceName = "sq_dominiozona")
@DisplayName("Zona")
public class Dominiozona{
	
	protected Integer cddominiozona;
	protected Dominiozonatipo dominiozonatipo;
	protected Servicoservidor servicoservidor;
	protected Dominio dominio;
	protected String subdominio;
	protected String destino;
			
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominiozona")
	public Integer getCddominiozona() {
		return cddominiozona;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominiozonatipo")
	@DisplayName("Tipo de apontamento")
	public Dominiozonatipo getDominiozonatipo() {
		return dominiozonatipo;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidor")
	@DisplayName("Servi�o de servidor")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Sub-dom�nio")
	@DescriptionProperty
	public String getSubdominio() {
		return subdominio;
	}
	
	@Required
	@MaxLength(50)	
	public String getDestino() {
		return destino;
	}
	
	public void setCddominiozona(Integer cddominiozona) {
		this.cddominiozona = cddominiozona;
	}

	public void setDominiozonatipo(Dominiozonatipo dominiozonatipo) {
		this.dominiozonatipo = dominiozonatipo;
	}

	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setSubdominio(String subdominio) {
		this.subdominio = subdominio;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}		
	
}
