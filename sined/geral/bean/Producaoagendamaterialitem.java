package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_producaoagendamaterialitem", sequenceName = "sq_producaoagendamaterialitem")
public class Producaoagendamaterialitem {
	
	protected Integer cdproducaoagendamaterialitem; 
	protected Producaoagenda producaoagenda;
	protected Material material;
	protected Double qtde;
	protected Double volume;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendamaterialitem")
	public Integer getCdproducaoagendamaterialitem() {
		return cdproducaoagendamaterialitem;
	}

	@JoinColumn(name="cdproducaoagenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}
	
	public Double getVolume() {
		return volume;
	}
	
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public void setCdproducaoagendamaterialitem(Integer cdproducaoagendamaterialitem) {
		this.cdproducaoagendamaterialitem = cdproducaoagendamaterialitem;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
}
