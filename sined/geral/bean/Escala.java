package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_escala", sequenceName = "sq_escala")
public class Escala implements Log{

	protected Integer cdescala;
	protected String nome;
	protected Integer diastrabalho;
	protected Integer diasfolga;
	protected Boolean naotrabalhadomingo;
	protected Boolean naotrabalhaferiado;
	protected String observacao;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean horariotodosdias = Boolean.TRUE;
	
	//Transient
	protected Hora horaInicio;
	protected Hora horaFim;
	protected Integer intervalo;

	protected List<Escalahorario> listaescalahorario = new ListSet<Escalahorario>(Escalahorario.class);
	protected List<DiaSemana> listaDiassemana = new ListSet<DiaSemana>(DiaSemana.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_escala")
	public Integer getCdescala() {
		return cdescala;
	}

	@Required
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Required
	@DisplayName("Dias de trabalho")
	@MaxLength(9)
	public Integer getDiastrabalho() {
		return diastrabalho;
	}

	@Required
	@DisplayName("Dias de folga")
	@MaxLength(9)
	public Integer getDiasfolga() {
		return diasfolga;
	}

	@DisplayName("N�o trabalha no domingo")
	public Boolean getNaotrabalhadomingo() {
		return naotrabalhadomingo;
	}

	@DisplayName("N�o trabalha no feriado")
	public Boolean getNaotrabalhaferiado() {
		return naotrabalhaferiado;
	}

	@MaxLength(255)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	@OneToMany(mappedBy="escala")
	public List<Escalahorario> getListaescalahorario() {
		return listaescalahorario;
	}

	public void setListaescalahorario(List<Escalahorario> listaescalahorario) {
		this.listaescalahorario = listaescalahorario;
	}
	
	public void setCdescala(Integer cdescala) {
		this.cdescala = cdescala;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDiastrabalho(Integer diastrabalho) {
		this.diastrabalho = diastrabalho;
	}

	public void setDiasfolga(Integer diasfolga) {
		this.diasfolga = diasfolga;
	}

	public void setNaotrabalhadomingo(Boolean naotrabalhadomingo) {
		this.naotrabalhadomingo = naotrabalhadomingo;
	}

	public void setNaotrabalhaferiado(Boolean naotrabalhaferiado) {
		this.naotrabalhaferiado = naotrabalhaferiado;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	@Required
	@DisplayName("Hor�rio v�lido para todos dias?")
	public Boolean getHorariotodosdias() {
		return horariotodosdias;
	}

	public void setHorariotodosdias(Boolean horariotodosdias) {
		this.horariotodosdias = horariotodosdias;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Transient
	public Hora getHoraInicio() {
		return horaInicio;
	}
	@Transient
	public Hora getHoraFim() {
		return horaFim;
	}
	@Transient
	public Integer getIntervalo() {
		return intervalo;
	}
	
	public void setHoraInicio(Hora horaInicio) {
		this.horaInicio = horaInicio;
	}

	public void setHoraFim(Hora horaFim) {
		this.horaFim = horaFim;
	}
	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}
	
	//Transient
	@Transient
	public List<DiaSemana> getListaDiassemana() {
		return listaDiassemana;
	}
	public void setListaDiassemana(List<DiaSemana> listaDiassemana) {
		this.listaDiassemana = listaDiassemana;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Escala)
			return ((Escala)obj).cdescala.equals(this.cdescala);
		return super.equals(obj);
	}
}
