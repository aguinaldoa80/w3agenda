package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
@Entity
@SequenceGenerator(name="sq_comunicado",sequenceName="sq_comunicado")
public class Comunicado implements Log{
	
	protected Integer cdcomunicado;
	protected Colaborador remetente;
	protected String assunto;
	protected String descricao;
	protected Timestamp dtenvio;
	
	protected List<Grupoemail> listagrupo=new ArrayList<Grupoemail>();
	
	//Tabela comunicadodestinatario
	protected List<Comunicadodestinatario> listadestinatario;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transient
	protected String whereInPlanejamento;
	
	public Comunicado(){
	}

	public Comunicado(String assunto, String whereInPlanejamento){
		this.assunto = assunto;
		this.whereInPlanejamento = whereInPlanejamento;
	}
	
	@DisplayName("Enviado em")
	public Timestamp getDtenvio() {
		return dtenvio;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_comunicado",strategy=GenerationType.AUTO)
	public Integer getCdcomunicado() {
		return cdcomunicado;
	}
	
	@Required	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdremetente")	
	public Colaborador getRemetente() {
		return remetente;
	}
	
	@Required
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	
	@DisplayName("Descrição")
	@MaxLength(1000)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Destinatários")
	@OneToMany(mappedBy="comunicado")
	public List<Comunicadodestinatario> getListadestinatario() {
		return listadestinatario;
	}
	
	@Transient
	@DisplayName("Grupo")
	public List<Grupoemail> getListagrupo() {
		return listagrupo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdcomunicado(Integer cdcomunicado) {
		this.cdcomunicado = cdcomunicado;
	}
	
	public void setRemetente(Colaborador remetente) {
		this.remetente = remetente;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setListadestinatario(List<Comunicadodestinatario> listadestinatario) {
		this.listadestinatario = listadestinatario;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}
	
	public void setListagrupo(List<Grupoemail> listagrupo) {
		this.listagrupo = listagrupo;
	}
	
	@Transient
	public String getWhereInPlanejamento() {
		return whereInPlanejamento;
	}
	public void setWhereInPlanejamento(String whereInPlanejamento) {
		this.whereInPlanejamento = whereInPlanejamento;
	}

}
