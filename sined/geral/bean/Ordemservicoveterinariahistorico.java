package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name="sq_ordemservicoveterinariahistorico", sequenceName="sq_ordemservicoveterinariahistorico")
public class Ordemservicoveterinariahistorico {

	private Integer cdordemservicoveterinariahistorico;
	private Ordemservicoveterinaria ordemservicoveterinaria;
	private Timestamp data;
	private Usuario usuario;
	private Arquivo arquivo;
	private String observacao;

	public Ordemservicoveterinariahistorico(){
	}
	
	public Ordemservicoveterinariahistorico(Ordemservicoveterinaria ordemservicoveterinaria, Usuario usuario, Arquivo arquivo, String observacao){
		this.data = new Timestamp(System.currentTimeMillis());
		this.ordemservicoveterinaria = ordemservicoveterinaria;
		this.usuario = usuario;
		this.arquivo = arquivo;
		this.observacao = observacao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemservicoveterinariahistorico")
	public Integer getCdordemservicoveterinariahistorico() {
		return cdordemservicoveterinariahistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	public Timestamp getData() {
		return data;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@DisplayName("Responsável")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdordemservicoveterinariahistorico(Integer cdordemservicoveterinariahistorico) {
		this.cdordemservicoveterinariahistorico = cdordemservicoveterinariahistorico;
	}
	public void setOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	public void setData(Timestamp data) {
		this.data = data;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	
}