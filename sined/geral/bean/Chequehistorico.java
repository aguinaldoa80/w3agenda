package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_chequehistorico", sequenceName = "sq_chequehistorico")
public class Chequehistorico implements Log{
	
	protected Integer cdchequehistorico;
	protected String observacao;
	protected Cheque cheque;
	protected Chequesituacao chequesituacao;
	protected Chequedevolucaomotivo chequedevolucaomotivo;
	
	protected String observacaorelatorio;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_chequehistorico")
	public Integer getCdchequehistorico() {
		return cdchequehistorico;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Cheque")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	public Cheque getCheque() {
		return cheque;
	}

	public void setCdchequehistorico(Integer cdchequehistorico) {
		this.cdchequehistorico = cdchequehistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getResponsavel(){
		if(getCdusuarioaltera() != null){
			return TagFunctions.findUserByCd(getCdusuarioaltera());
		} else return null;
	}
	
	
	public String getObservacaorelatorio() {
		return observacaorelatorio;
	}
	public void setObservacaorelatorio(String observacaorelatorio) {
		this.observacaorelatorio = observacaorelatorio;
	}
	@DisplayName("Situa��o")
	public Chequesituacao getChequesituacao() {
		return chequesituacao;
	}
	@DisplayName("Motivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdchequedevolucaomotivo")
	public Chequedevolucaomotivo getChequedevolucaomotivo() {
		return chequedevolucaomotivo;
	}
	
	public void setChequesituacao(Chequesituacao chequesituacao) {
		this.chequesituacao = chequesituacao;
	}
	public void setChequedevolucaomotivo(Chequedevolucaomotivo chequedevolucaomotivo) {
		this.chequedevolucaomotivo = chequedevolucaomotivo;
	}
}
