package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servicoftp", sequenceName = "sq_servicoftp")
@DisplayName("FTP")
public class Servicoftp implements Log{
	
	protected Integer cdservicoftp;
	protected Servicoservidor servicoservidor;
	protected String usuario;
	protected String senha;
	protected String diretorio;
	protected String cota;
	protected Integer identificador = 65500;
	protected Boolean ativo = true;
	protected Contratomaterial contratomaterial;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	

	//Transient
	protected Cliente cliente;
	protected Servidor servidor;
	
	public Servicoftp() {
	}
	
	public Servicoftp(Integer cdservicoftp) {
		this.cdservicoftp = cdservicoftp;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicoftp")
	public Integer getCdservicoftp() {
		return cdservicoftp;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidor")
	@DisplayName("Servi�o de servidor")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}
	
	@Required
	@MaxLength(50)
	public String getSenha() {
		return senha;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Diret�rio")
	public String getDiretorio() {
		return diretorio;
	}
	
	@MaxLength(50)
	public String getCota() {
		return cota;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Identificador")
	@DescriptionProperty
	public Integer getIdentificador() {
		return identificador;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
			
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}

	public void setCdservicoftp(Integer cdservicoftp) {
		this.cdservicoftp = cdservicoftp;
	}

	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	public void setCota(String cota) {
		this.cota = cota;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
		
	@Transient
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@Required
	public Servidor getServidor() {
		return servidor;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}	
}
