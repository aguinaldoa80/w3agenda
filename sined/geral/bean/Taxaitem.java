package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculodias;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_taxaitem",sequenceName="sq_taxaitem")
public class Taxaitem implements Log, Cloneable{

	protected Integer cdtaxaitem;
	protected Taxa taxa;
	protected Tipotaxa tipotaxa;
	protected Money valor;
	protected boolean percentual;
	protected Date dtlimite;
	protected Boolean aodia;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean gerarmensagem;
	
	protected String motivo;
	protected Tipocalculodias tipocalculodias;
	protected Integer dias;
	
	protected Integer cdconta;
	
	public Taxaitem() {}
	
	public Taxaitem(Taxaitem outraTaxaItem) {
		this.cdtaxaitem = outraTaxaItem.cdtaxaitem;
		this.tipotaxa = outraTaxaItem.tipotaxa;
		this.valor = outraTaxaItem.valor;
		this.percentual =  outraTaxaItem.percentual;
		this.dtlimite = new Date(outraTaxaItem.dtlimite.getTime());
		this.motivo = outraTaxaItem.motivo;
	}
	
	public Taxaitem(Tipotaxa tipotaxa, boolean percentual, Money valor, Boolean gerarmensagem) {
		this.tipotaxa = tipotaxa;
		this.percentual = percentual;
		this.valor = valor;
		this.gerarmensagem = gerarmensagem;
	}
	
	@Override
	public Taxaitem clone() throws CloneNotSupportedException {
		Taxaitem item = (Taxaitem) super.clone();
		item.setCdtaxaitem(null);
		return item;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_taxaitem")
	public Integer getCdtaxaitem() {
		return cdtaxaitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxa")
	public Taxa getTaxa() {
		return taxa;
	}
	@Required
	@DisplayName("Tipo de taxa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipotaxa")
	public Tipotaxa getTipotaxa() {
		return tipotaxa;
	}
	@Required
	@DisplayName("Valor / Percentual")
	public Money getValor() {
		return valor;
	}
	@Required
	@DisplayName("Tipo de c�lculo")
	public boolean isPercentual() {
		return percentual;
	}
	@Required
	@DisplayName("Data limite")
	public Date getDtlimite() {
		return dtlimite;
	}
	
	@DisplayName("Ao dia")
	public Boolean getAodia() {
		return aodia;
	}
	@DisplayName("Gerar mensagem?")
	public Boolean getGerarmensagem() {
		return gerarmensagem;
	}
	@MaxLength(50)
	@DisplayName("Motivo")
	public String getMotivo() {
		return motivo;
	}
	@DisplayName("C�lculo de Dias")
	public Tipocalculodias getTipocalculodias() {
		return tipocalculodias;
	}
	public Integer getDias() {
		return dias;
	}
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setGerarmensagem(Boolean gerarmensagem) {
		this.gerarmensagem = gerarmensagem;
	}
	public void setAodia(Boolean aodia) {
		this.aodia = aodia;
	}
	
	public void setCdtaxaitem(Integer cdtaxaitem) {
		this.cdtaxaitem = cdtaxaitem;
	}
	public void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}
	public void setTipotaxa(Tipotaxa tipotaxa) {
		this.tipotaxa = tipotaxa;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setPercentual(boolean percentual) {
		this.percentual = percentual;
	}
	public void setDtlimite(Date dtlimite) {
		this.dtlimite = dtlimite;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setTipocalculodias(Tipocalculodias tipocalculodias) {
		this.tipocalculodias = tipocalculodias;
	}
	public void setDias(Integer dias) {
		this.dias = dias;
	}

	@Transient
	public Integer getCdconta() {
		return cdconta;
	}

	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}
}
