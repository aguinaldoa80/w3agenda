package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Finalidadenfe;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoVinculoExpedicao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipooperacaonota;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialInfoadicional;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.ProducaoordemdevolucaoBean;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@DisplayName("Nota fiscal de produto")
@JoinEmpresa("notafiscalproduto.empresa")
public class Notafiscalproduto extends Nota implements PermissaoProjeto {
	
	protected Timestamp datahorasaidaentrada;
	protected Hora hremissao = new Hora(SinedDateUtils.currentTimestamp().getTime());
	protected String naturezaoperacaoantigo;
	protected Money valoricms;
	protected Money valorfrete;
	protected Money valorseguro;
	protected Money valordesconto;
	protected Money valornaoincluido;
	protected Money valortotaltributos;
	private Money valorTotalImpostoFederal;
	private Money valorTotalImpostoEstadual;
	private Money valorTotalImpostoMunicipal;
	protected Money outrasdespesas;
	protected Money valoripi;
	protected Fornecedor transportador;
	protected Endereco enderecotransportador;
	protected String telefonetransportador;
	protected ResponsavelFrete responsavelfrete;
	protected String placaveiculo;
	protected Uf ufveiculo;
	protected Municipio municipiogerador;
	protected Boolean retencaoICMSSTUFdestino;
	protected Tipooperacaonota tipooperacaonota;
	protected Finalidadenfe finalidadenfe = Finalidadenfe.NORMAL;
	protected Localdestinonfe localdestinonfe = Localdestinonfe.INTERNA;
	protected Operacaonfe operacaonfe = Operacaonfe.NORMAL;
	protected Presencacompradornfe presencacompradornfe;
	protected Boolean cadastrartransportador;
	protected Boolean totalcalculado = true;
	protected Codigocnae codigocnae;
	protected Naturezaoperacao naturezaoperacao;
	protected String recopi;
	protected Emporiumvenda emporiumVenda;
	
	protected String numeronotaempenho;
	protected String numeropedido;
	protected String numerocontrato;
	
	protected Formapagamentonfe formapagamentonfe;
	protected Money valorbcicms;
	protected Money valorbcicmsst;
	protected Money valoricmsst;
	protected Money valorprodutos;
	
	protected Money valordesoneracao;
	protected Money valorii;
	protected Money valorpis;
	protected Money valorcofins;
	protected Money valorpisretido;
	protected Money valorcofinsretido;
	
	protected Money valor;
	
	protected Money valorservicos;
	protected Money valorbciss;
	protected Money valoriss;
	protected Money valorpisservicos;
	protected Money valorcofinsservicos;
	protected Money valoripidevolvido;
	
	protected Date dtcompetenciaservico;
	protected Money valordeducaoservico;
	protected Money valoroutrasretencoesservico;
	protected Money valordescontocondicionadoservico;
	protected Money valordescontoincondicionadoservico;
	protected Money valorissretido;
	protected Regimetributacao regimetributacao;
	
	protected Money valorbccsll;
	protected Money valorcsll;
	
	protected Money valorbcirrf;
	protected Money valorirrf;
	
	protected Money valorbcprevsocial;
	protected Money valorprevsocial;
	
	protected Boolean transporteveiculo;
	protected String rntc;
	
	protected Integer qtdevolume;
	protected String espvolume;
	protected String marcavolume;
	protected String numvolume;
	protected Double pesoliquido;
	protected Double pesobruto;

	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	
	protected Boolean cadastrarautorizacaoadicional;
	protected Boolean cadastrarreferencia;
	protected Boolean cadastrarcobranca;
	
	protected Boolean enderecodiferenteentrega;
	protected Boolean enderecodiferenteretirada;
	
	protected Tipopessoa tipopessoaentrega;
	protected Cnpj cnpjentrega;
	protected Cpf cpfentrega;
	protected String logradouroentrega;
	protected String numeroentrega;
	protected String complementoentrega;
	protected String bairroentrega;
	protected Municipio municipioentrega;
	private Cliente clienteEntrega;
	private String telefoneEntrega;
	private Pais paisEntrega;
	private String emailEntrega;
	private String inscricaoEstadualEntrega;
	
	protected Tipopessoa tipopessoaretirada;
	protected Cnpj cnpjretirada;
	protected Cpf cpfretirada;
	protected String logradouroretirada;
	protected String numeroretirada;
	protected String complementoretirada;
	protected String bairroretirada;
	protected Municipio municipioretirada;
	private Fornecedor fornecedorRetirada;
	private String telefoneRetirada;
	private Pais paisRetirada;
	private String emailRetirada;
	private String inscricaoEstadualRetirada;
	
	protected Uf ufembarque;
	protected String localembarque;
	
	protected Money valorfcp;
	protected Money valorfcpretidost;
	protected Money valorfcpretidostanteriormente;
	protected Money valorbcfcpdestinatario;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	
	protected List<Notafiscalprodutoitem> listaItens;
	protected List<Notafiscalprodutoduplicata> listaDuplicata;
	protected List<Notafiscalprodutoreferenciada> listaReferenciada;
	protected List<Notafiscalprodutocorrecao> listaCorrecao;
	protected List<Notafiscalprodutoautorizacao> listaAutorizacao;
	protected Set<Ajustefiscal> listaAjustefiscal;
	protected List<NotaFiscalProdutoRestricao> listaRestricao;
	protected List<NotafiscalprodutoColeta> listaNotafiscalprodutoColeta;
	
	protected String razaosocialalternativa;
	protected SituacaoVinculoExpedicao situacaoExpedicao;
	
	private ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	
	//Transientes:
	protected String identificadortela;
	protected Boolean isCriar;
	protected String controller;
	protected Integer cdvendaassociada;
	protected String whereInCdexpedicaoassociada;
	protected Telefone telefoneTransportadorTransient;
	protected String chaveAcesso;
	protected List<Contrato> listaContrato;
	protected List<Venda> listaVenda = new ArrayList<Venda>();
	protected List<Expedicao> listaExpedicao = new ArrayList<Expedicao>();
	protected Boolean usarSequencial = Boolean.TRUE;
	protected String situacaosped;
	protected Money valorNota;
	protected Boolean haveArquivonfnota = Boolean.FALSE;
	protected Boolean haveGnre = Boolean.FALSE;
	protected String nfpMaterialnumeroserie;
	protected String xml;
	protected Arquivonfnota arquivonfnota;
	protected Documento documento;
	protected String romaneios;
	protected String whereInEntradaFiscal;
	protected String whereInExpedicaoproducao;
	protected String whereInColeta;
	protected String whereInPedidovendaRetorno;
	protected String whereInVendaRetorno;
	protected String whereInProducaoordem;
	protected String whereInPedidovenda;
	protected Boolean producaoautomatica;
	protected Boolean devolucao;
	protected String expedicoes; 
	protected String pagina;
	protected Material material;
	protected Cfop cfop;
	protected List<MaterialInfoadicional> listaMaterialInfoadicional;
	protected Boolean faturamentolote = Boolean.FALSE;
	protected Conta conta;
	protected String legenda;
	protected boolean gerarNotaIndustrializacaoRetorno = false;
	protected boolean referenciarNotaEntrada = false;
	protected Boolean somenteproduto;
	protected String numeroAntigo;
	protected Empresa empresaAntiga;
	protected Boolean inutilizarNumero = false;
	protected String motivodevolucaocoleta;
	protected String inscricaoestadual;
	protected String inscricaosuframa;
	protected Boolean isExportacao;
	protected Boolean isStatusValido;
	protected Boolean isExisteMemorandoexportacao;
	protected ProducaoordemdevolucaoBean producaoordemdevolucaoBean;
	protected Boolean haveDIFAL;
	protected Boolean devolucaoColetaConfirmada;
	protected Boolean registrarDevolucaoColeta;
	protected Boolean possuiTemplateInfoContribuinte;
	protected String codigoCfopNaturezaoperacao;
	protected String origemOperacaoStr;
	protected Boolean interromperProducao;
	protected String razaosocialalternativaOrRazaosocial;
	protected Boolean gerarE300;
	protected Boolean agruparContas;
	protected Integer qtdParcelas;
	protected String whereInCdvenda;
	protected String modeloNF;
	protected Boolean origemCupom;
	
	public Notafiscalproduto() {
		super();
	}
	
	public Notafiscalproduto(Integer cdNota) {
		super(cdNota);
	}

	public Notafiscalproduto(Nota nota) {
		super(nota);
	}

	@DisplayName("Data e hora de entrada / sa�da")
	public Timestamp getDatahorasaidaentrada() {
		return datahorasaidaentrada;
	}
	
	@DisplayName("Hora de emiss�o")
	public Hora getHremissao() {
		return hremissao;
	}

//	@MaxLength(60)
//	@DisplayName("Natureza da opera��o")
	@Column(name="naturezaoperacao")
	public String getNaturezaoperacaoantigo() {
		return naturezaoperacaoantigo;
	}

	@DisplayName("Produtos")
	@OneToMany(mappedBy="notafiscalproduto")
	public List<Notafiscalprodutoitem> getListaItens() {
		return listaItens;
	}
	
	@OneToMany(mappedBy="notafiscalproduto")
	public List<Notafiscalprodutoduplicata> getListaDuplicata() {
		return listaDuplicata;
	}
	
	@DisplayName("Autoriza��es Adicionais")
	@OneToMany(mappedBy="notafiscalproduto")
	public List<Notafiscalprodutoautorizacao> getListaAutorizacao() {
		return listaAutorizacao;
	}
	
	@OneToMany(mappedBy="notafiscalproduto")
	@DisplayName("Documentos referenciados")
	public List<Notafiscalprodutoreferenciada> getListaReferenciada() {
		return listaReferenciada;
	}
	
	@OneToMany(mappedBy="notafiscalproduto")
	@DisplayName("Cartas de corre��o")
	public List<Notafiscalprodutocorrecao> getListaCorrecao() {
		return listaCorrecao;
	}
	
	@OneToMany(mappedBy="notafiscalproduto")
	@DisplayName("Restri��o de acesso")
	public List<NotaFiscalProdutoRestricao> getListaRestricao() {
		return listaRestricao;
	}
	
	@OneToMany(mappedBy="notafiscalproduto")
	public List<NotafiscalprodutoColeta> getListaNotafiscalprodutoColeta() {
		return listaNotafiscalprodutoColeta;
	}
	
	@DisplayName("Valor Total do ICMS")
	public Money getValoricms() {
		return valoricms;
	}

	@DisplayName("Valor do frete")
	public Money getValorfrete() {
		return valorfrete;
	}
	
	@DisplayName("Valor do desconto")
	public Money getValordesconto() {
		return valordesconto;
	}
	
	@DisplayName("Valor n�o inclu�do na nota")
	public Money getValornaoincluido() {
		return valornaoincluido;
	}

	@DisplayName("Valor do seguro")
	public Money getValorseguro() {
		return valorseguro;
	}

	@DisplayName("Outras despesas acess�rias")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	
	@DisplayName("Valor Total do IPI")
	public Money getValoripi() {
		return valoripi;
	}

	@DisplayName("Transportador")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdtransportador")
	public Fornecedor getTransportador() {
		return transportador;
	}

	@DisplayName("Endere�o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdenderecotransportador")
	public Endereco getEnderecotransportador() {
		return enderecotransportador;
	}
	
	@DisplayName("Munic�pio de Ocorr�ncia do Fato Gerador")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipiogerador")
	public Municipio getMunicipiogerador() {
		return municipiogerador;
	}
	
	@DisplayName("Reten��o do ICMS ST para a UF de destino")
	public Boolean getRetencaoICMSSTUFdestino() {
		return retencaoICMSSTUFdestino;
	}

	@DisplayName("Frete")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdresponsavelfrete")
	public ResponsavelFrete getResponsavelfrete() {
		return responsavelfrete;
	}
	
	@MaxLength(10)
	@DisplayName("Placa do Ve�culo")
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	
	@DisplayName("UF")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdufveiculo")
	public Uf getUfveiculo() {
		return ufveiculo;
	}
	
	@DisplayName("Forma de Pagamento")
	public Formapagamentonfe getFormapagamentonfe() {
		return formapagamentonfe;
	}

	@DisplayName("Valor Total da Base de C�lculo do ICMS")
	public Money getValorbcicms() {
		return valorbcicms;
	}

	@DisplayName("Valor Total da Base de C�lculo do ICMS ST")
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	@DisplayName("Valor Total do ICMS ST")
	public Money getValoricmsst() {
		return valoricmsst;
	}

	@DisplayName("Valor Total dos Produtos")
	public Money getValorprodutos() {
		return valorprodutos;
	}

	@DisplayName("Valor Total do PIS")
	public Money getValorpis() {
		return valorpis;
	}

	@DisplayName("Valor Total do COFINS")
	public Money getValorcofins() {
		return valorcofins;
	}

	@DisplayName("Valor da Nota")
	public Money getValor() {
		return valor;
	}

	@DisplayName("Valor Total dos Servi�os")
	public Money getValorservicos() {
		return valorservicos;
	}

	@DisplayName("Valor Total da Base de C�lculo do ISS")
	public Money getValorbciss() {
		return valorbciss;
	}
	
	@DisplayName("Valor do ISS")
	public Money getValoriss() {
		return valoriss;
	}

	@DisplayName("Valor Total do PIS em Servi�os")
	public Money getValorpisservicos() {
		return valorpisservicos;
	}

	@DisplayName("Valor Total do COFINS em Servi�os")
	public Money getValorcofinsservicos() {
		return valorcofinsservicos;
	}
	
	@DisplayName("Data da Presta��o do Servi�o")
	public Date getDtcompetenciaservico() {
		return dtcompetenciaservico;
	}

	@DisplayName("Valor Total de Dedu��o em Servi�os")
	public Money getValordeducaoservico() {
		return valordeducaoservico;
	}

	@DisplayName("Valor Total de Outras Reten��es em Servi�os")
	public Money getValoroutrasretencoesservico() {
		return valoroutrasretencoesservico;
	}

	@DisplayName("Valor Total de Desconto Condicionado em Servi�os")
	public Money getValordescontocondicionadoservico() {
		return valordescontocondicionadoservico;
	}

	@DisplayName("Valor Total de Desconto Incodicionado em Servi�os")
	public Money getValordescontoincondicionadoservico() {
		return valordescontoincondicionadoservico;
	}

	@DisplayName("Valor do ISS Retido")
	public Money getValorissretido() {
		return valorissretido;
	}

	@DisplayName("Regime Especial de Tributa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdregimetributacao")
	public Regimetributacao getRegimetributacao() {
		return regimetributacao;
	}

	@DisplayName("Valor do CSLL retido")
	public Money getValorcsll() {
		return valorcsll;
	}

	@DisplayName("Valor da BC do IRRF retido")
	public Money getValorbcirrf() {
		return valorbcirrf;
	}

	@DisplayName("Valor do IRRF retido")
	public Money getValorirrf() {
		return valorirrf;
	}

	@DisplayName("Valor da BC do INSS retido")
	public Money getValorbcprevsocial() {
		return valorbcprevsocial;
	}

	@DisplayName("Valor do INSS retido")
	public Money getValorprevsocial() {
		return valorprevsocial;
	}

	@DisplayName("Transportado por ve�culo")
	public Boolean getTransporteveiculo() {
		return transporteveiculo;
	}

	public String getRntc() {
		return rntc;
	}

	@DisplayName("Informa��es Adicionais de Interesse do Fisco")
	@MaxLength(2000)
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}

	@DisplayName("Informa��es Adicionais de Interesse do Contribuinte")
	@MaxLength(5000)
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}
	
	public String getTelefonetransportador() {
		return telefonetransportador;
	}
	
	@DisplayName("Qtde.")
	public Integer getQtdevolume() {
		return qtdevolume;
	}
	
	@MaxLength(60)
	@DisplayName("Esp�cie")
	public String getEspvolume() {
		return espvolume;
	}

	@DisplayName("Marca")
	@MaxLength(60)
	public String getMarcavolume() {
		return marcavolume;
	}

	@DisplayName("Numera��o")
	@MaxLength(60)
	public String getNumvolume() {
		return numvolume;
	}

	@DisplayName("Peso l�quido (KG)")
	public Double getPesoliquido() {
		return pesoliquido;
	}

	@DisplayName("Peso bruto (KG)")
	public Double getPesobruto() {
		return pesobruto;
	}
	
	@DisplayName("Cadastrar dados de cobran�a")
	public Boolean getCadastrarcobranca() {
		return cadastrarcobranca;
	}
	
	@DisplayName("Cadastrar autoriza��o adicional")
	public Boolean getCadastrarautorizacaoadicional() {
		return cadastrarautorizacaoadicional;
	}
	
	@Required
	@DisplayName("Tipo de opera��o")
	public Tipooperacaonota getTipooperacaonota() {
		return tipooperacaonota;
	}
	
	@DisplayName("Cadastrar dados de documentos referenciados")
	public Boolean getCadastrarreferencia() {
		return cadastrarreferencia;
	}
	
	@Required
	@DisplayName("Finalidade de emiss�o")
	public Finalidadenfe getFinalidadenfe() {
		return finalidadenfe;
	}
	
	@Required
	@DisplayName("Consumidor final?")
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}
	
	@Required
	@DisplayName("Origem da opera��o")
	public Presencacompradornfe getPresencacompradornfe() {
		return presencacompradornfe;
	}
	
	@Required
	@DisplayName("Local de destino")
	public Localdestinonfe getLocaldestinonfe() {
		return localdestinonfe;
	}
	
	@DisplayName("Endere�o de entrega diferente do endere�o do destinat�rio")
	public Boolean getEnderecodiferenteentrega() {
		return enderecodiferenteentrega;
	}

	@DisplayName("Endere�o de retirada diferente do endere�o do remetente")
	public Boolean getEnderecodiferenteretirada() {
		return enderecodiferenteretirada;
	}

	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoaentrega() {
		return tipopessoaentrega;
	}

	@DisplayName("CNPJ")
	public Cnpj getCnpjentrega() {
		return cnpjentrega;
	}

	@DisplayName("CPF")
	public Cpf getCpfentrega() {
		return cpfentrega;
	}

	@MaxLength(60)
	@DisplayName("Logradouro")
	public String getLogradouroentrega() {
		return logradouroentrega;
	}

	@MaxLength(60)
	@DisplayName("N�mero")
	public String getNumeroentrega() {
		return numeroentrega;
	}

	@MaxLength(60)
	@DisplayName("Complemento")
	public String getComplementoentrega() {
		return complementoentrega;
	}

	@MaxLength(60)
	@DisplayName("Bairro")
	public String getBairroentrega() {
		return bairroentrega;
	}

	@DisplayName("Munic�pio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipioentrega")
	public Municipio getMunicipioentrega() {
		return municipioentrega;
	}

	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoaretirada() {
		return tipopessoaretirada;
	}

	@DisplayName("CNPJ")
	public Cnpj getCnpjretirada() {
		return cnpjretirada;
	}

	@DisplayName("CPF")
	public Cpf getCpfretirada() {
		return cpfretirada;
	}

	@MaxLength(60)
	@DisplayName("Logradouro")
	public String getLogradouroretirada() {
		return logradouroretirada;
	}

	@MaxLength(60)
	@DisplayName("N�mero")
	public String getNumeroretirada() {
		return numeroretirada;
	}

	@MaxLength(60)
	@DisplayName("Complemento")
	public String getComplementoretirada() {
		return complementoretirada;
	}

	@MaxLength(60)
	@DisplayName("Bairro")
	public String getBairroretirada() {
		return bairroretirada;
	}

	@DisplayName("Munic�pio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipioretirada")
	public Municipio getMunicipioretirada() {
		return municipioretirada;
	}
	
	@DisplayName("Valor do PIS retido")
	public Money getValorpisretido() {
		return valorpisretido;
	}

	@DisplayName("Valor do COFINS retido")
	public Money getValorcofinsretido() {
		return valorcofinsretido;
	}
	
	@DisplayName("Valor da BC do CSLL retido")
	public Money getValorbccsll() {
		return valorbccsll;
	}

	@DisplayName("Cadastrar dados de transporte")
	public Boolean getCadastrartransportador() {
		return cadastrartransportador;
	}
	
	@DisplayName("Valor total calculado?")
	public Boolean getTotalcalculado() {
		return totalcalculado;
	}
	
	@DisplayName("C�digo CNAE")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigocnae")
	public Codigocnae getCodigocnae() {
		return codigocnae;
	}
	
	@DisplayName("Valor Total do II")
	public Money getValorii() {
		return valorii;
	}
	
	@DisplayName("Valor Total do ICMS Desonerado")
	public Money getValordesoneracao() {
		return valordesoneracao;
	}
	
	@DisplayName("Valor Total dos Tributos")
	public Money getValortotaltributos() {
		return valortotaltributos;
	}
	
	public Money getValorTotalImpostoFederal() {
		return valorTotalImpostoFederal;
	}
	
	public Money getValorTotalImpostoEstadual() {
		return valorTotalImpostoEstadual;
	}
	
	public Money getValorTotalImpostoMunicipal() {
		return valorTotalImpostoMunicipal;
	}
	
	@MaxLength(22)
	@DisplayName("Nota de empenho")
	public String getNumeronotaempenho() {
		return numeronotaempenho;
	}

	@MaxLength(60)
	@DisplayName("Pedido")
	public String getNumeropedido() {
		return numeropedido;
	}

	@MaxLength(60)
	@DisplayName("Contrato")
	public String getNumerocontrato() {
		return numerocontrato;
	}
	
	@DisplayName("UF de embarque dos produtos")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufembarque")
	public Uf getUfembarque() {
		return ufembarque;
	}
	
	@DisplayName("Local de embarque")
	@MaxLength(60)
	public String getLocalembarque() {
		return localembarque;
	}
	
	@MaxLength(20)
	@DisplayName("RECOPI")
	public String getRecopi() {
		return recopi;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdemporiumvenda")
	public Emporiumvenda getEmporiumVenda() {
		return emporiumVenda;
	}
	
	@DisplayName("Valor Total FCP Destinat�rio")
	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}

	@DisplayName("Valor Total ICMS Destinat�rio")
	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}

	@DisplayName("Valor Total ICMS Remetente")
	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}
	
	@MaxLength(200)
	@DisplayName("Raz�o social alternativa")
	public String getRazaosocialalternativa() {
		return razaosocialalternativa;
	}
	
	@DisplayName("Valor Total do FCP")
	public Money getValorfcp() {
		return valorfcp;
	}

	@DisplayName("Valor Total do FCP Retido por Substitui��o Tribut�ria")
	public Money getValorfcpretidost() {
		return valorfcpretidost;
	}

	@DisplayName("Valor Total do FCP Retido Anteriormente por Substitui��o Tribut�ria")
	public Money getValorfcpretidostanteriormente() {
		return valorfcpretidostanteriormente;
	}

	@DisplayName("Valor Total da BC FCP na UF de Destino")
	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}
	
	@DisplayName("Valor Total do IPI Devolvido")
	public Money getValoripidevolvido() {
		return valoripidevolvido;
	}

	@DisplayName("Fornecedor")
	@JoinColumn(name="cdfornecedorretirada")
	@ManyToOne(fetch=FetchType.LAZY)
	public Fornecedor getFornecedorRetirada() {
		return fornecedorRetirada;
	}

	@DisplayName("Telefone")
	public String getTelefoneRetirada() {
		return telefoneRetirada;
	}

	@DisplayName("Pa�s")
	@JoinColumn(name="cdpaisretirada")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pais getPaisRetirada() {
		return paisRetirada;
	}

	@DisplayName("E-Mail")
	public String getEmailRetirada() {
		return emailRetirada;
	}

	@DisplayName("Inscri��o Estadual")
	public String getInscricaoEstadualRetirada() {
		return inscricaoEstadualRetirada;
	}
	
	@DisplayName("Cliente")
	@JoinColumn(name="cdclienteentrega")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getClienteEntrega() {
		return clienteEntrega;
	}

	@DisplayName("Telefone")
	public String getTelefoneEntrega() {
		return telefoneEntrega;
	}

	@DisplayName("Pa�s")
	@JoinColumn(name="cdpaisentrega")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pais getPaisEntrega() {
		return paisEntrega;
	}

	@DisplayName("E-Mail")
	public String getEmailEntrega() {
		return emailEntrega;
	}

	@DisplayName("Inscri��o Estadual")
	public String getInscricaoEstadualEntrega() {
		return inscricaoEstadualEntrega;
	}

	@Required
	@DisplayName("Modelo")
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}
	
	public Boolean getOrigemCupom() {
		return origemCupom;
	}
	
	public void setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}
	
	public void setClienteEntrega(Cliente clienteEntrega) {
		this.clienteEntrega = clienteEntrega;
	}
	
	public void setTelefoneEntrega(String telefoneEntrega) {
		this.telefoneEntrega = telefoneEntrega;
	}
	
	public void setPaisEntrega(Pais paisEntrega) {
		this.paisEntrega = paisEntrega;
	}
	
	public void setEmailEntrega(String emailEntrega) {
		this.emailEntrega = emailEntrega;
	}
	
	public void setInscricaoEstadualEntrega(String inscricaoEstadualEntrega) {
		this.inscricaoEstadualEntrega = inscricaoEstadualEntrega;
	}

	public void setFornecedorRetirada(Fornecedor fornecedorRetirada) {
		this.fornecedorRetirada = fornecedorRetirada;
	}

	public void setTelefoneRetirada(String telefoneRetirada) {
		this.telefoneRetirada = telefoneRetirada;
	}

	public void setPaisRetirada(Pais paisRetirada) {
		this.paisRetirada = paisRetirada;
	}

	public void setEmailRetirada(String emailRetirada) {
		this.emailRetirada = emailRetirada;
	}

	public void setInscricaoEstadualRetirada(String inscricaoEstadualRetirada) {
		this.inscricaoEstadualRetirada = inscricaoEstadualRetirada;
	}
	
	public SituacaoVinculoExpedicao getSituacaoExpedicao() {
		return situacaoExpedicao;
	}
	
	public void setSituacaoExpedicao(SituacaoVinculoExpedicao situacaoExpedicao) {
		this.situacaoExpedicao = situacaoExpedicao;
	}
	
	public void setValoripidevolvido(Money valoripidevolvido) {
		this.valoripidevolvido = valoripidevolvido;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setValorfcpretidost(Money valorfcpretidost) {
		this.valorfcpretidost = valorfcpretidost;
	}

	public void setValorfcpretidostanteriormente(Money valorfcpretidostanteriormente) {
		this.valorfcpretidostanteriormente = valorfcpretidostanteriormente;
	}

	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}

	public void setRecopi(String recopi) {
		this.recopi = recopi;
	}
	
	public void setEmporiumVenda(Emporiumvenda emporiumVenda) {
		this.emporiumVenda = emporiumVenda;
	}
	
	public void setDtcompetenciaservico(Date dtcompetenciaservico) {
		this.dtcompetenciaservico = dtcompetenciaservico;
	}

	public void setValordeducaoservico(Money valordeducaoservico) {
		this.valordeducaoservico = valordeducaoservico;
	}

	public void setValoroutrasretencoesservico(Money valoroutrasretencoesservico) {
		this.valoroutrasretencoesservico = valoroutrasretencoesservico;
	}

	public void setValordescontocondicionadoservico(
			Money valordescontocondicionadoservico) {
		this.valordescontocondicionadoservico = valordescontocondicionadoservico;
	}

	public void setValordescontoincondicionadoservico(
			Money valordescontoincondicionadoservico) {
		this.valordescontoincondicionadoservico = valordescontoincondicionadoservico;
	}

	public void setValorissretido(Money valorissretido) {
		this.valorissretido = valorissretido;
	}

	public void setRegimetributacao(Regimetributacao regimetributacao) {
		this.regimetributacao = regimetributacao;
	}

	public void setUfembarque(Uf ufembarque) {
		this.ufembarque = ufembarque;
	}
	
	public void setLocalembarque(String localembarque) {
		this.localembarque = localembarque;
	}

	public void setNumeronotaempenho(String numeronotaempenho) {
		this.numeronotaempenho = numeronotaempenho;
	}

	public void setNumeropedido(String numeropedido) {
		this.numeropedido = numeropedido;
	}

	public void setNumerocontrato(String numerocontrato) {
		this.numerocontrato = numerocontrato;
	}

	public void setValortotaltributos(Money valortotaltributos) {
		this.valortotaltributos = valortotaltributos;
	}
	
	public void setValorTotalImpostoFederal(Money valorTotalImpostoFederal) {
		this.valorTotalImpostoFederal = valorTotalImpostoFederal;
	}
	
	public void setValorTotalImpostoEstadual(Money valorTotalImpostoEstadual) {
		this.valorTotalImpostoEstadual = valorTotalImpostoEstadual;
	}
	
	public void setValorTotalImpostoMunicipal(Money valorTotalImpostoMunicipal) {
		this.valorTotalImpostoMunicipal = valorTotalImpostoMunicipal;
	}
	
	public void setValordesoneracao(Money valordesoneracao) {
		this.valordesoneracao = valordesoneracao;
	}
	
	public void setListaCorrecao(List<Notafiscalprodutocorrecao> listaCorrecao) {
		this.listaCorrecao = listaCorrecao;
	}
	
	public void setListaAutorizacao(
			List<Notafiscalprodutoautorizacao> listaAutorizacao) {
		this.listaAutorizacao = listaAutorizacao;
	}
	
	public void setValorii(Money valorii) {
		this.valorii = valorii;
	}
	
	public void setCodigocnae(Codigocnae codigocnae) {
		this.codigocnae = codigocnae;
	}

	public void setTotalcalculado(Boolean totalcalculado) {
		this.totalcalculado = totalcalculado;
	}

	public void setCadastrartransportador(Boolean cadastrartransportador) {
		this.cadastrartransportador = cadastrartransportador;
	}
	
	public void setCadastrarautorizacaoadicional(
			Boolean cadastrarautorizacaoadicional) {
		this.cadastrarautorizacaoadicional = cadastrarautorizacaoadicional;
	}

	public void setValorbccsll(Money valorbccsll) {
		this.valorbccsll = valorbccsll;
	}

	public void setValorpisretido(Money valorpisretido) {
		this.valorpisretido = valorpisretido;
	}

	public void setValorcofinsretido(Money valorcofinsretido) {
		this.valorcofinsretido = valorcofinsretido;
	}

	public void setEnderecodiferenteentrega(Boolean enderecodiferenteentrega) {
		this.enderecodiferenteentrega = enderecodiferenteentrega;
	}

	public void setEnderecodiferenteretirada(Boolean enderecodiferenteretirada) {
		this.enderecodiferenteretirada = enderecodiferenteretirada;
	}

	public void setTipopessoaentrega(Tipopessoa tipopessoaentrega) {
		this.tipopessoaentrega = tipopessoaentrega;
	}

	public void setCnpjentrega(Cnpj cnpjentrega) {
		this.cnpjentrega = cnpjentrega;
	}

	public void setCpfentrega(Cpf cpfentrega) {
		this.cpfentrega = cpfentrega;
	}

	public void setLogradouroentrega(String logradouroentrega) {
		this.logradouroentrega = logradouroentrega;
	}

	public void setNumeroentrega(String numeroentrega) {
		this.numeroentrega = numeroentrega;
	}

	public void setComplementoentrega(String complementoentrega) {
		this.complementoentrega = complementoentrega;
	}

	public void setBairroentrega(String bairroentrega) {
		this.bairroentrega = bairroentrega;
	}

	public void setMunicipioentrega(Municipio municipioentrega) {
		this.municipioentrega = municipioentrega;
	}

	public void setTipopessoaretirada(Tipopessoa tipopessoaretirada) {
		this.tipopessoaretirada = tipopessoaretirada;
	}

	public void setCnpjretirada(Cnpj cnpjretirada) {
		this.cnpjretirada = cnpjretirada;
	}

	public void setCpfretirada(Cpf cpfretirada) {
		this.cpfretirada = cpfretirada;
	}

	public void setLogradouroretirada(String logradouroretirada) {
		this.logradouroretirada = logradouroretirada;
	}

	public void setNumeroretirada(String numeroretirada) {
		this.numeroretirada = numeroretirada;
	}

	public void setComplementoretirada(String complementoretirada) {
		this.complementoretirada = complementoretirada;
	}

	public void setBairroretirada(String bairroretirada) {
		this.bairroretirada = bairroretirada;
	}

	public void setMunicipioretirada(Municipio municipioretirada) {
		this.municipioretirada = municipioretirada;
	}

	public void setFinalidadenfe(Finalidadenfe finalidadenfe) {
		this.finalidadenfe = finalidadenfe;
	}
	
	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe!=null? operacaonfe: Operacaonfe.NORMAL;
	}
	
	public void setPresencacompradornfe(
			Presencacompradornfe presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	
	public void setLocaldestinonfe(Localdestinonfe localdestinonfe) {
		this.localdestinonfe = localdestinonfe;
	}
	
	public void setCadastrarreferencia(Boolean cadastrarreferencia) {
		this.cadastrarreferencia = cadastrarreferencia;
	}
	
	public void setTipooperacaonota(Tipooperacaonota tipooperacaonota) {
		this.tipooperacaonota = tipooperacaonota;
	}
	
	public void setCadastrarcobranca(Boolean cadastrarcobranca) {
		this.cadastrarcobranca = cadastrarcobranca;
	}
	
	public void setQtdevolume(Integer qtdevolume) {
		this.qtdevolume = qtdevolume;
	}

	public void setEspvolume(String espvolume) {
		this.espvolume = espvolume;
	}

	public void setMarcavolume(String marcavolume) {
		this.marcavolume = marcavolume;
	}

	public void setNumvolume(String numvolume) {
		this.numvolume = numvolume;
	}

	public void setPesoliquido(Double pesoliquido) {
		this.pesoliquido = pesoliquido;
	}

	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}

	public void setTelefonetransportador(String telefonetransportador) {
		this.telefonetransportador = telefonetransportador;
	}

	public void setFormapagamentonfe(Formapagamentonfe formapagamentonfe) {
		this.formapagamentonfe = formapagamentonfe;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setValorprodutos(Money valorprodutos) {
		this.valorprodutos = valorprodutos;
	}

	public void setValorpis(Money valorpis) {
		this.valorpis = valorpis;
	}

	public void setValorcofins(Money valorcofins) {
		this.valorcofins = valorcofins;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValorservicos(Money valorservicos) {
		this.valorservicos = valorservicos;
	}

	public void setValorbciss(Money valorbciss) {
		this.valorbciss = valorbciss;
	}

	public void setValoriss(Money valoriss) {
		this.valoriss = valoriss;
	}

	public void setValorpisservicos(Money valorpisservicos) {
		this.valorpisservicos = valorpisservicos;
	}

	public void setValorcofinsservicos(Money valorcofinsservicos) {
		this.valorcofinsservicos = valorcofinsservicos;
	}

	public void setValorcsll(Money valorcsll) {
		this.valorcsll = valorcsll;
	}

	public void setValorbcirrf(Money valorbcirrf) {
		this.valorbcirrf = valorbcirrf;
	}

	public void setValorirrf(Money valorirrf) {
		this.valorirrf = valorirrf;
	}

	public void setValorbcprevsocial(Money valorbcprevsocial) {
		this.valorbcprevsocial = valorbcprevsocial;
	}
	
	public void setMunicipiogerador(Municipio municipiogerador) {
		this.municipiogerador = municipiogerador;
	}
	
	public void setRetencaoICMSSTUFdestino(Boolean retencaoICMSSTUFdestino) {
		this.retencaoICMSSTUFdestino = retencaoICMSSTUFdestino;
	}

	public void setValorprevsocial(Money valorprevsocial) {
		this.valorprevsocial = valorprevsocial;
	}


	public void setTransporteveiculo(Boolean transporteveiculo) {
		this.transporteveiculo = transporteveiculo;
	}

	public void setRntc(String rntc) {
		this.rntc = rntc;
	}

	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}

	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}

	public void setDatahorasaidaentrada(Timestamp datahorasaidaentrada) {
		this.datahorasaidaentrada = datahorasaidaentrada;
	}
	
	public void setHremissao(Hora hremissao) {
		this.hremissao = hremissao;
	}

	public void setNaturezaoperacaoantigo(String naturezaoperacaoantigo) {
		this.naturezaoperacaoantigo = naturezaoperacaoantigo;
	}
	
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}

	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}

	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}

	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}

	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}

	public void setEnderecotransportador(Endereco enderecotransportador) {
		this.enderecotransportador = enderecotransportador;
	}

	public void setResponsavelfrete(ResponsavelFrete responsavelfrete) {
		this.responsavelfrete = responsavelfrete;
	}
	
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	
	public void setValornaoincluido(Money valornaoincluido) {
		this.valornaoincluido = valornaoincluido;
	}

	public void setPlacaveiculo(String placaveiculo) {
		if(placaveiculo!=null){
			this.placaveiculo = placaveiculo.toUpperCase();			
		}else{
			this.placaveiculo = placaveiculo;			
		}
	}

	public void setUfveiculo(Uf ufveiculo) {
		this.ufveiculo = ufveiculo;
	}

	public void setListaItens(List<Notafiscalprodutoitem> listaItens) {
		this.listaItens = listaItens;
	}
	
	public void setListaDuplicata(
			List<Notafiscalprodutoduplicata> listaDuplicata) {
		this.listaDuplicata = listaDuplicata;
	}
	
	public void setListaReferenciada(
			List<Notafiscalprodutoreferenciada> listaReferenciada) {
		this.listaReferenciada = listaReferenciada;
	}
	public void setListaRestricao(List<NotaFiscalProdutoRestricao> listaRestricao) {
		this.listaRestricao = listaRestricao;
	}

	public void setListaNotafiscalprodutoColeta(List<NotafiscalprodutoColeta> listaNotafiscalprodutoColeta) {
		this.listaNotafiscalprodutoColeta = listaNotafiscalprodutoColeta;
	}
	
	public void setNotaTipo(NotaTipo notaTipo) {
		super.setNotaTipo(notaTipo);
	}
	
	public void setRazaosocialalternativa(String razaosocialalternativa) {
		this.razaosocialalternativa = razaosocialalternativa;
	}
	
	public void setOrigemCupom(Boolean origemCupom) {
		this.origemCupom = origemCupom;
	}
	
	@Override
	public void copiarItensDeValor(Nota nota) {
		if (nota == null || !(nota instanceof Notafiscalproduto)) {
			throw new SinedException("N�o foi poss�vel copiar os itens de valor na nota.");
		}
		
		Notafiscalproduto outra = (Notafiscalproduto)nota;
		
		this.listaItens = outra.listaItens;
		this.listaNotaDocumento = outra.listaNotaDocumento;
		this.valor = outra.valor;
		this.valorprodutos = outra.valorprodutos;
	}

	public void setNumeroAntigo(String numeroAntigo) {
		this.numeroAntigo = numeroAntigo;
	}

	public void setEmpresaAntiga(Empresa empresaAntiga) {
		this.empresaAntiga = empresaAntiga;
	}

	public void setInutilizarNumero(Boolean inutilizarNumero) {
		this.inutilizarNumero = inutilizarNumero;
	}

	@DisplayName("Ajuste Fiscal")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="notafiscalproduto")
	public Set<Ajustefiscal> getListaAjustefiscal() {
		return listaAjustefiscal;
	}

	public void setListaAjustefiscal(Set<Ajustefiscal> listaAjustefiscal) {
		this.listaAjustefiscal = listaAjustefiscal;
	}
	
	@Transient
	public Boolean getIsCriar() {
		return isCriar;
	}

	public void setIsCriar(Boolean isCriar) {
		this.isCriar = isCriar;
	}
	
	@Transient
	public Integer getCdvendaassociada() {
		return cdvendaassociada;
	}
	@Transient
	public String getWhereInCdexpedicaoassociada() {
		return whereInCdexpedicaoassociada;
	}

	@Transient
	public String getExpedicoes() {
		return expedicoes;
	}

	public void setExpedicoes(String expedicoes) {
		this.expedicoes = expedicoes;
	}
	
	public void setCdvendaassociada(Integer cdvendaassociada) {
		this.cdvendaassociada = cdvendaassociada;
	}
	public void setWhereInCdexpedicaoassociada(String whereInCdexpedicaoassociada) {
		this.whereInCdexpedicaoassociada = whereInCdexpedicaoassociada;
	}
	
	@Override
	@Transient
	public NotaTipo getNotaTipo() {
		return NotaTipo.NOTA_FISCAL_PRODUTO;
	}
	
	@Transient
	public String getController() {
		return controller;
	}
	
	public void setController(String controller) {
		this.controller = controller;
	}
	
	@Transient
	@DisplayName("FONE/FAX")
	public Telefone getTelefoneTransportadorTransient() {
		return telefoneTransportadorTransient;
	}
	
	public void setTelefoneTransportadorTransient(
			Telefone telefoneTransportadorTransient) {
		this.telefoneTransportadorTransient = telefoneTransportadorTransient;
	}

	@Transient
	@DisplayName("Chave de acesso NF-e")
	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	@Override
	@Transient
	@DisplayName("Valor da Nota")
	public Money getValorNota() {
		Money totalNota = new Money(0D);
		totalNota = totalNota.add(this.valor != null ? this.valor : new Money(0D));
		
		valorNota = totalNota;
		
		return valorNota;
	}

	@Transient
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}

	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}

	@Transient
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}

	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}

	@Transient
	public String getSituacaosped() {
		return situacaosped;
	}

	public void setSituacaosped(String situacaosped) {
		this.situacaosped = situacaosped;
	}
	
	@Transient
	public String getStringWhereInNfe(){
		return cdNota + ";" + chaveAcesso;
	}
	
	@Transient
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	@Transient
	public List<Expedicao> getListaExpedicao() {
		return listaExpedicao;
	}

	public void setListaExpedicao(List<Expedicao> listaExpedicao) {
		this.listaExpedicao = listaExpedicao;
	}

	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	
	@Transient
	public Boolean getHaveArquivonfnota() {
		return haveArquivonfnota;
	}
	
	@Transient
	public Boolean getHaveGnre() {
		return haveGnre;
	}
	
	public void setHaveGnre(Boolean haveGnre) {
		this.haveGnre = haveGnre;
	}
	
	public void setHaveArquivonfnota(Boolean haveArquivonfnota) {
		this.haveArquivonfnota = haveArquivonfnota;
	}
	
	@Transient
	public String getXml() {
		return xml;
	}
	
	public void setXml(String xml) {
		this.xml = xml;
	}
	
	@Transient
	public Arquivonfnota getArquivonfnota() {
		return arquivonfnota;
	}
	
	public void setArquivonfnota(Arquivonfnota arquivonfnota) {
		this.arquivonfnota = arquivonfnota;
	}
	
	@DisplayName("Venda")
	@Transient
	public String getNfpMaterialnumeroserie() {
		StringBuilder s = new StringBuilder();
		if(dtEmissao != null)
			s.append(new SimpleDateFormat("dd/MM/yyyy").format(dtEmissao));
		
		if(cliente != null && cliente.getNome() != null && !"".equals(cliente.getNome())){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(cliente.getNome());
		}
		
		return s.length()>0 ? s.toString() : null;
	}
	
	public String subQueryProjeto() {
		return "select notafiscalprodutosubQueryProjeto.cdNota " +
				"from Notafiscalproduto notafiscalprodutosubQueryProjeto " +
				"left outer join notafiscalprodutosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where (projetosubQueryProjeto.cdprojeto is null or projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")) ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public Documento getDocumento() {
		return documento;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	@Transient
	public String getRomaneios() {
		return romaneios;
	}
	
	public void setRomaneios(String romaneios) {
		this.romaneios = romaneios;
	}
	
	@Transient
	public String getWhereInEntradaFiscal() {
		return whereInEntradaFiscal;
	}

	public void setWhereInEntradaFiscal(String whereInEntradaFiscal) {
		this.whereInEntradaFiscal = whereInEntradaFiscal;
	}
	
	@Transient
	public String getWhereInExpedicaoproducao() {
		return whereInExpedicaoproducao;
	}

	public void setWhereInExpedicaoproducao(String whereInExpedicaoproducao) {
		this.whereInExpedicaoproducao = whereInExpedicaoproducao;
	}

	@Transient
	public String getWhereInColeta() {
		return whereInColeta;
	}
	public void setWhereInColeta(String whereInColeta) {
		this.whereInColeta = whereInColeta;
	}

	@Transient
	public String getWhereInPedidovendaRetorno() {
		return whereInPedidovendaRetorno;
	}

	public void setWhereInPedidovendaRetorno(String whereInPedidovendaRetorno) {
		this.whereInPedidovendaRetorno = whereInPedidovendaRetorno;
	}

	@Transient
	public String getWhereInVendaRetorno() {
		return whereInVendaRetorno;
	}

	public void setWhereInVendaRetorno(String whereInVendaRetorno) {
		this.whereInVendaRetorno = whereInVendaRetorno;
	}

	@Transient
	public String getWhereInProducaoordem() {
		return whereInProducaoordem;
	}

	public void setWhereInProducaoordem(String whereInProducaoordem) {
		this.whereInProducaoordem = whereInProducaoordem;
	}

	@Transient
	public String getWhereInPedidovenda() {
		return whereInPedidovenda;
	}
	public void setWhereInPedidovenda(String whereInPedidovenda) {
		this.whereInPedidovenda = whereInPedidovenda;
	}

	@Transient
	public Boolean getDevolucao() {
		return devolucao;
	}
	public void setDevolucao(Boolean devolucao) {
		this.devolucao = devolucao;
	}

	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	@Transient
	public String getPagina() {
		return pagina;
	}
	public void setPagina(String pagina) {
		this.pagina = pagina;
	}

	@DisplayName("Natureza da Opera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	
	@Transient
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Transient
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	@Transient
	public List<MaterialInfoadicional> getListaMaterialInfoadicional() {
		return listaMaterialInfoadicional;
	}

	public void setListaMaterialInfoadicional(List<MaterialInfoadicional> listaMaterialInfoadicional) {
		this.listaMaterialInfoadicional = listaMaterialInfoadicional;
	}
	
	@Transient
	public Boolean getFaturamentolote() {
		return faturamentolote;
	}
	
	public void setFaturamentolote(Boolean faturamentolote) {
		this.faturamentolote = faturamentolote;
	}
	
	@Transient
	public String getLegenda() {
		return legenda;
	}
	
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	
	@Transient
	public String getEnderecoEntregaCompleto(){
		String logradouroCompleto =  this.logradouroentrega != null && !this.logradouroentrega.trim().equals("") ? this.logradouroentrega : "";
		logradouroCompleto += this.numeroentrega != null && !this.numeroentrega.trim().equals("") ? ", " + this.numeroentrega : "";
		logradouroCompleto += this.complementoentrega != null && !this.complementoentrega.trim().equals("") ? ", " + this.complementoentrega : "";
		logradouroCompleto += this.bairroentrega != null && !this.bairroentrega.trim().equals("") ? ", " + this.bairroentrega : "";
		logradouroCompleto += this.municipioentrega != null ? ", " + this.municipioentrega.getNome() : "";
		logradouroCompleto += this.municipioentrega != null && this.municipioentrega.getUf() != null ? "/ " + this.municipioentrega.getUf().getSigla() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getEnderecoRetiradaCompleto(){
		String logradouroCompleto =  this.logradouroretirada != null && !this.logradouroretirada.trim().equals("") ? this.logradouroretirada : "";
		logradouroCompleto += this.numeroretirada != null && !this.numeroretirada.trim().equals("") ? ", " + this.numeroretirada : "";
		logradouroCompleto += this.complementoretirada != null && !this.complementoretirada.trim().equals("") ? ", " + this.complementoretirada : "";
		logradouroCompleto += this.bairroretirada != null && !this.bairroretirada.trim().equals("") ? ", " + this.bairroretirada : "";
		logradouroCompleto += this.municipioretirada != null ? ", " + this.municipioretirada.getNome() : "";
		logradouroCompleto += this.municipioretirada != null && this.municipioretirada.getUf() != null ? "/ " + this.municipioretirada.getUf().getSigla() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public boolean isGerarNotaIndustrializacaoRetorno() {
		return gerarNotaIndustrializacaoRetorno;
	}
	
	public void setGerarNotaIndustrializacaoRetorno(boolean gerarNotaIndustrializacaoRetorno) {
		this.gerarNotaIndustrializacaoRetorno = gerarNotaIndustrializacaoRetorno;
	}
	
	@Transient
	public boolean isReferenciarNotaEntrada() {
		return referenciarNotaEntrada;
	}
	
	public void setReferenciarNotaEntrada(boolean referenciarNotaEntrada) {
		this.referenciarNotaEntrada = referenciarNotaEntrada;
	}
	
	@Transient
	public Boolean getSomenteproduto() {
		return somenteproduto;
	}
	public void setSomenteproduto(Boolean somenteproduto) {
		this.somenteproduto = somenteproduto;
	}

	@Transient
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	
	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	
	@Transient
	public String getNumeroAntigo() {
		return numeroAntigo;
	}

	@Transient
	public Empresa getEmpresaAntiga() {
		return empresaAntiga;
	}

	@Transient
	public Boolean getInutilizarNumero() {
		return inutilizarNumero;
	}


	@Transient
	public boolean getExisteDiferencaduplicata(){
		return Boolean.TRUE.equals(cadastrarcobranca) && getDiferencaduplicata() != null && getDiferencaduplicata().compareTo(new Money()) != 0;
	}
	
	@Transient
	public String getMotivodevolucaocoleta() {
		return motivodevolucaocoleta;
	}
	
	public void setMotivodevolucaocoleta(String motivodevolucaocoleta) {
		this.motivodevolucaocoleta = motivodevolucaocoleta;
	}
	
	@Transient
	public String getInscricaoestadual() {
		if(inscricaoestadual == null){
			if(enderecoCliente != null && 
					enderecoCliente.getInscricaoestadual() != null &&
					!enderecoCliente.getInscricaoestadual().trim().equals("")){
				inscricaoestadual = enderecoCliente.getInscricaoestadual();
			} else if(cliente != null && 
					cliente.getInscricaoestadual() != null &&
					!cliente.getInscricaoestadual().trim().equals("")){
				inscricaoestadual = cliente.getInscricaoestadual();
			}
		}
		return inscricaoestadual;
	}
	
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	
	@Transient
	public String getInscricaosuframa() {
		if(inscricaosuframa == null){
			if(cliente != null && 
					cliente.getInscricaosuframa() != null &&
					!cliente.getInscricaosuframa().trim().equals("")){
				inscricaosuframa = cliente.getInscricaosuframa();
			}
		}
		return inscricaosuframa;
	}
	
	public void setInscricaosuframa(String inscricaosuframa) {
		this.inscricaosuframa = inscricaosuframa;
	}

	@Transient
	public Boolean getIsExportacao() {
		return isExportacao;
	}

	public void setIsExportacao(Boolean isExportacao) {
		this.isExportacao = isExportacao;
	}
	@Transient
	public Boolean getIsStatusValido() {
		return isStatusValido;
	}

	public void setIsStatusValido(Boolean isStatusValido) {
		this.isStatusValido = isStatusValido;
	}
	@Transient
	public Boolean getIsExisteMemorandoexportacao() {
		return isExisteMemorandoexportacao;
	}

	public void setIsExisteMemorandoexportacao(Boolean isExisteMemorandoexportacao) {
		this.isExisteMemorandoexportacao = isExisteMemorandoexportacao;
	}

	@Transient
	public ProducaoordemdevolucaoBean getProducaoordemdevolucaoBean() {
		return producaoordemdevolucaoBean;
	}

	public void setProducaoordemdevolucaoBean(ProducaoordemdevolucaoBean producaoordemdevolucaoBean) {
		this.producaoordemdevolucaoBean = producaoordemdevolucaoBean;
	}
	
	@Transient
	public Boolean getHaveDIFAL() {
		return haveDIFAL;
	}
	
	public void setHaveDIFAL(Boolean haveDIFAL) {
		this.haveDIFAL = haveDIFAL;
	}
	
	@Transient
	public Boolean getDevolucaoColetaConfirmada() {
		return devolucaoColetaConfirmada;
	}

	public void setDevolucaoColetaConfirmada(Boolean devolucaoColetaConfirmada) {
		this.devolucaoColetaConfirmada = devolucaoColetaConfirmada;
	}
	
	@Transient
	public Boolean getRegistrarDevolucaoColeta() {
		return registrarDevolucaoColeta;
	}

	public void setRegistrarDevolucaoColeta(Boolean registrarDevolucaoColeta) {
		this.registrarDevolucaoColeta = registrarDevolucaoColeta;
	}

	@Transient
	public Boolean getPossuiTemplateInfoContribuinte(){
		return possuiTemplateInfoContribuinte;
	}
	
	public void setPossuiTemplateInfoContribuinte(
			Boolean possuiTemplateInfoContribuinte) {
		this.possuiTemplateInfoContribuinte = possuiTemplateInfoContribuinte;
	}
	
	@Transient
	@MaxLength(4)
	public String getCodigoCfopNaturezaoperacao() {
		return codigoCfopNaturezaoperacao;
	}
	
	public void setCodigoCfopNaturezaoperacao(String codigoCfopNaturezaoperacao) {
		this.codigoCfopNaturezaoperacao = codigoCfopNaturezaoperacao;
	}
	
	@Transient
	@DisplayName("Origem da opera��o")
	public String getOrigemOperacaoStr() {
		return origemOperacaoStr;
	}
	
	public void setOrigemOperacaoStr(String origemOperacaoStr) {
		this.origemOperacaoStr = origemOperacaoStr;
	}
	
	@Transient
	public Boolean getInterromperProducao() {
		return interromperProducao;
	}

	public void setInterromperProducao(Boolean interromperProducao) {
		this.interromperProducao = interromperProducao;
	}

	@Transient
	public String getNotaAutocompleteForExpedicao(){
		StringBuilder s = new StringBuilder();
		
		if(this.getNumero() != null){
			s.append(this.getNumero());
			
			if(this.getDtEmissao() != null){					
				s.append(" - ").append(new SimpleDateFormat("dd/MM/yyyy").format(this.getDtEmissao()));
			}
			
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
			
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}
	
	@Override
	@Transient
	public Money getValorBruto() {
		return this.getValorprodutos();
	}
	
	@Transient
	public String getDescriptionAutocomplete(){
		StringBuilder s = new StringBuilder();
		
		if(this.getNumero() != null){
			s.append(this.getNumero());
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}
	
	@Transient
	public String getRazaosocialalternativaOrRazaosocial() {
		if(Util.strings.isNotEmpty(this.getRazaosocialalternativa())){
			razaosocialalternativaOrRazaosocial = this.getRazaosocialalternativa();
		}else if(cliente != null){
			razaosocialalternativaOrRazaosocial = cliente.getRazaosocial() != null && !cliente.getRazaosocial().equals("") ? cliente.getRazaosocial() : cliente.getNome();
		}
		return Util.strings.emptyIfNull(razaosocialalternativaOrRazaosocial);
	}

	@Transient
	public Boolean getGerarE300() {
		return gerarE300;
	}

	public void setGerarE300(Boolean gerarE300) {
		this.gerarE300 = gerarE300;
	}
	
	@Transient
	public Boolean getAgruparContas() {
		return agruparContas;
	}
	
	public void setAgruparContas(Boolean agruparContas) {
		this.agruparContas = agruparContas;
	}
	
	@Transient
	public Integer getQtdParcelas() {
		return qtdParcelas;
	}
	
	public void setQtdParcelas(Integer qtdParcelas) {
		this.qtdParcelas = qtdParcelas;
	}
	
	@Transient
	public String getWhereInCdvenda() {
		return whereInCdvenda;
	}
	
	public void setWhereInCdvenda(String whereInCdvenda) {
		this.whereInCdvenda = whereInCdvenda;
	}

	@Transient
	public String getModeloNF() {
		return modeloNF;
	}
	public void setModeloNF(String modeloNF) {
		this.modeloNF = modeloNF;
	}
	
	@Transient
	public Money getValorTotalBcipi(){
		Money totalIpi = new Money();
		
		if(listaItens != null && !listaItens.isEmpty()){
			for(Notafiscalprodutoitem nfpi : listaItens){
				if(nfpi.getValorbcipi() != null){
					totalIpi = totalIpi.add(nfpi.getValorbcipi());
				}
			}
		}
		return totalIpi;
	}
	
	@Transient
	public Money getValorTotalBcpis(){
		Money totalPis = new Money();
		
		if(listaItens != null && !listaItens.isEmpty()){
			for(Notafiscalprodutoitem nfpi : listaItens){
				if(nfpi.getValorbcpis() != null){
					totalPis = totalPis.add(nfpi.getValorbcpis());
				}
			}
		}
		return totalPis;
	}
	
	@Transient
	public Money getValorTotalBccofins(){
		Money totalCofins = new Money();
		
		if(listaItens != null && !listaItens.isEmpty()){
			for(Notafiscalprodutoitem nfpi : listaItens){
				if(nfpi.getValorbccofins() != null){
					totalCofins = totalCofins.add(nfpi.getValorbccofins());
				}
			}
		}
		return totalCofins;
	}
	
	@Transient
	public Money getValorTotalBcii(){
		Money totalIi = new Money();
		
		if(listaItens != null && !listaItens.isEmpty()){
			for(Notafiscalprodutoitem nfpi : listaItens){
				if(nfpi.getValorbcii() != null){
					totalIi = totalIi.add(nfpi.getValorbcii());
				}
			}
		}
		return totalIi;
	}
}