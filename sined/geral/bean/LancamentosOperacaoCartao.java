package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_lancamentosoperacaocartao", sequenceName = "sq_lancamentosoperacaocartao")
public class LancamentosOperacaoCartao implements Log {
	
	private Integer cdLancamentosOperacaoCartao;
	private Date mesAno;
	private Empresa empresa;
	private Fornecedor credenciadora;
	private Money valorCredito;
	private Money valorDebito;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	protected String  mesanoAux;
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_lancamentosoperacaocartao")
	public Integer getCdLancamentosOperacaoCartao() {
		return cdLancamentosOperacaoCartao;
	}
	
	@DisplayName("M�s/Ano")
	public Date getMesAno() {
		return mesAno;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getCredenciadora() {
		return credenciadora;
	}
	
	@DisplayName("Valor de cr�dito")
	public Money getValorCredito() {
		return valorCredito;
	}
	
	@DisplayName("Valor de d�bito")
	public Money getValorDebito() {
		return valorDebito;
	}
	
	public void setCdLancamentosOperacaoCartao(
			Integer cdLancamentosOperacaoCartao) {
		this.cdLancamentosOperacaoCartao = cdLancamentosOperacaoCartao;
	}
	
	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setCredenciadora(Fornecedor credenciadora) {
		this.credenciadora = credenciadora;
	}
	
	public void setValorCredito(Money valorCredito) {
		this.valorCredito = valorCredito;
	}
	
	public void setValorDebito(Money valorDebito) {
		this.valorDebito = valorDebito;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;	
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesAno != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		return null;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
}
