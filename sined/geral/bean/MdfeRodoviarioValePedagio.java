package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdferodoviariovalepedagio", sequenceName = "sq_mdferodoviariovalepedagio")
public class MdfeRodoviarioValePedagio {

	protected Integer cdMdfeRodoviarioValePedagio;
	protected Mdfe mdfe;
	protected Cnpj cnpjEmpresaFornecedora;
	protected String numeroComprovante;
	protected Tipopessoa tipoPessoa = Tipopessoa.PESSOA_JURIDICA;
	protected Cpf cpfResponsavelPagamento;
	protected Cnpj cnpjResponsavelPagamento;
	protected Money valor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdferodoviariovalepedagio")
	public Integer getCdMdfeRodoviarioValePedagio() {
		return cdMdfeRodoviarioValePedagio;
	}
	public void setCdMdfeRodoviarioValePedagio(
			Integer cdMdfeRodoviarioValePedagio) {
		this.cdMdfeRodoviarioValePedagio = cdMdfeRodoviarioValePedagio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@DisplayName("CNPJ da empresa fornecedora")
	public Cnpj getCnpjEmpresaFornecedora() {
		return cnpjEmpresaFornecedora;
	}
	public void setCnpjEmpresaFornecedora(Cnpj cnpjEmpresaFornecedora) {
		this.cnpjEmpresaFornecedora = cnpjEmpresaFornecedora;
	}
	
	@Required
	@DisplayName("N� comprovante")
	@MaxLength(value=20)
	public String getNumeroComprovante() {
		return numeroComprovante;
	}
	public void setNumeroComprovante(String numeroComprovante) {
		this.numeroComprovante = numeroComprovante;
	}
	
	@Required
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(Tipopessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public Cpf getCpfResponsavelPagamento() {
		return cpfResponsavelPagamento;
	}
	public void setCpfResponsavelPagamento(Cpf cpfResponsavelPagamento) {
		this.cpfResponsavelPagamento = cpfResponsavelPagamento;
	}
	
	public Cnpj getCnpjResponsavelPagamento() {
		return cnpjResponsavelPagamento;
	}
	public void setCnpjResponsavelPagamento(Cnpj cnpjResponsavelPagamento) {
		this.cnpjResponsavelPagamento = cnpjResponsavelPagamento;
	}
	
	@Required
	@DisplayName("Valor ped�gio")
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
