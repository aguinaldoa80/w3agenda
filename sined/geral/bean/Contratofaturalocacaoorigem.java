package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_contratofaturalocacaoorigem", sequenceName = "sq_contratofaturalocacaoorigem")
public class Contratofaturalocacaoorigem {

	protected Integer cdcontratofaturalocacaoorigem;
	protected Contratofaturalocacao contratofaturalocacao;
	protected Contrato contrato;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratofaturalocacaoorigem")
	public Integer getCdcontratofaturalocacaoorigem() {
		return cdcontratofaturalocacaoorigem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratofaturalocacao")
	public Contratofaturalocacao getContratofaturalocacao() {
		return contratofaturalocacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}

	public void setCdcontratofaturalocacaoorigem(
			Integer cdcontratofaturalocacaoorigem) {
		this.cdcontratofaturalocacaoorigem = cdcontratofaturalocacaoorigem;
	}
	
	public void setContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		this.contratofaturalocacao = contratofaturalocacao;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
}
