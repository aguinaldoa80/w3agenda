package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
public class Categoriacnh {

	protected Integer cdcategoriacnh;
	protected String nome;
	
	public static final Integer A = 1;
	public static final Integer B = 2;
	public static final Integer C = 3;
	public static final Integer D = 4;
	public static final Integer E = 5;
	

	@Id
	@DisplayName("Id")
	public Integer getCdcategoriacnh() {
		return cdcategoriacnh;
	}
	public void setCdcategoriacnh(Integer id) {
		this.cdcategoriacnh = id;
	}

	
	@Required
	@MaxLength(2)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

}
