package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_pedidovendasincronizacao", sequenceName = "sq_pedidovendasincronizacao")
public class Pedidovendasincronizacao implements Log{

	protected Integer cdpedidovendasincronizacao;
	protected Pedidovendasituacao pedidovendasituacao;
	
	//Cliente
	protected Cliente cliente;
	protected Integer clienteId;
	protected String clienteNome;
	protected String clienteNatureza;
	protected String clienteDocumento;
	protected String clienteFilial;
	protected String clienteEndereco;
	
	//Filial
	protected Empresa empresa;
	protected Integer filialwmsId;
	protected String filialwmsNome;
	protected String filialwmsNatureza;
	protected String filialwmsDocumento;
	protected String filialwmsFilial;
	
	//Endereco
	protected Endereco endereco;
	protected Integer enderecoId;
	protected Integer enderecoIdCliente;
	protected String enderecoLogradouro;
	protected String enderecoNumero;
	protected String enderecoComplemento;
	protected String enderecoBairro;
	protected String enderecoCep;
	protected String enderecoPontoreferencia;
	protected String enderecoMunicipio;
	protected String enderecoUf;
	
	//Pedidovenda
	protected Pedidovenda pedidovenda;
	protected Integer pedidovendaId;
	protected Integer pedidovendaIdFilialVenda;
	protected String pedidovendaNumero;
	protected Integer pedidovendaIdCliente;
	protected Date pedidovendaDataEmissao;
	protected Date pedidovendaDataLancamento;
	protected String pedidovendaFlagTroca;
	protected String pedidovendaIdExterno;
	protected String pedidovendaObservacaointerna;
	
	protected List<Pedidovendamaterialsincronizacao> listaPedidovendamaterialsincronizacao;
	protected List<Pedidovendamaterialunidademedidasincronizacao> listaPedidovendamaterialunidademedidasincronizacao;
	protected Boolean sincronizado = Boolean.FALSE;
	protected Boolean reenvio;
	protected Timestamp dtsincronizacao;
	protected Timestamp dttentativasincronizacao;
	
	protected String erro;
	protected String errolog;
	protected Boolean desmarcaritens = Boolean.FALSE;
	protected List<Pedidovendasincronizacaohistorico> listaHistorico;
	
	//TRANSPORTADOR
	protected Fornecedor transportador;
	protected Integer transportadorId;
	protected String transportadorNome;
	protected String transportadorNatureza;
	protected String transportadorCpfCnpj;
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected Integer contadorenvio;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendasincronizacao")
	public Integer getCdpedidovendasincronizacao() {
		return cdpedidovendasincronizacao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}

	
	@DisplayName("Nome")
	public String getClienteNome() {
		return clienteNome;
	}
	@DisplayName("Natureza")
	public String getClienteNatureza() {
		return clienteNatureza;
	}
	@DisplayName("CPF/CNPJ")
	public String getClienteDocumento() {
		return clienteDocumento;
	}
	@DisplayName("Filial")
	public String getClienteFilial() {
		return clienteFilial;
	}
	@DisplayName("Endereco")
	public String getClienteEndereco() {
		return clienteEndereco;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Nome")
	public String getFilialwmsNome() {
		return filialwmsNome;
	}
	@DisplayName("Natureza")
	public String getFilialwmsNatureza() {
		return filialwmsNatureza;
	}
	@DisplayName("CPF/CNPj")
	public String getFilialwmsDocumento() {
		return filialwmsDocumento;
	}
	@DisplayName("Filial")
	public String getFilialwmsFilial() {
		return filialwmsFilial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecoentrega")
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	@DisplayName("Logradouro")
	public String getEnderecoLogradouro() {
		return enderecoLogradouro;
	}
	@DisplayName("N�mero")
	public String getEnderecoNumero() {
		return enderecoNumero;
	}
	@DisplayName("Complemento")
	public String getEnderecoComplemento() {
		return enderecoComplemento;
	}
	@DisplayName("Bairro")
	public String getEnderecoBairro() {
		return enderecoBairro;
	}
	@DisplayName("Cep")
	public String getEnderecoCep() {
		return enderecoCep;
	}
	@DisplayName("Ponto de Referencia")
	public String getEnderecoPontoreferencia() {
		return enderecoPontoreferencia;
	}
	@DisplayName("Munic�pio")
	public String getEnderecoMunicipio() {
		return enderecoMunicipio;
	}
	@DisplayName("Uf")
	public String getEnderecoUf() {
		return enderecoUf;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	@DisplayName("Pedido de Venda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}		
	@DisplayName("Data de Emiss�o")
	public Date getPedidovendaDataEmissao() {
		return pedidovendaDataEmissao;
	}
	@DisplayName("Data do Lan�amento")
	public Date getPedidovendaDataLancamento() {
		return pedidovendaDataLancamento;
	}
	@DisplayName("Flag Troca")
	public String getPedidovendaFlagTroca() {
		return pedidovendaFlagTroca;
	}
	@DisplayName("Id Externo")
	public String getPedidovendaIdExterno() {
		return pedidovendaIdExterno;
	}
	@DisplayName("Observa��o interna")
	public String getPedidovendaObservacaointerna() {
		return pedidovendaObservacaointerna;
	}

	@DisplayName("Materiais")
	@OneToMany(mappedBy="pedidovendasincronizacao")
	public List<Pedidovendamaterialsincronizacao> getListaPedidovendamaterialsincronizacao() {
		return listaPedidovendamaterialsincronizacao;
	}
	@DisplayName("Unidades de medida")
	@OneToMany(mappedBy="pedidovendasincronizacao")
	public List<Pedidovendamaterialunidademedidasincronizacao> getListaPedidovendamaterialunidademedidasincronizacao() {
		return listaPedidovendamaterialunidademedidasincronizacao;
	}
	public void setListaPedidovendamaterialunidademedidasincronizacao(List<Pedidovendamaterialunidademedidasincronizacao> listaPedidovendamaterialunidademedidasincronizacao) {
		this.listaPedidovendamaterialunidademedidasincronizacao = listaPedidovendamaterialunidademedidasincronizacao;
	}
	
	public void setCdpedidovendasincronizacao(Integer cdpedidovendasincronizacao) {
		this.cdpedidovendasincronizacao = cdpedidovendasincronizacao;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}

	public void setClienteNatureza(String clienteNatureza) {
		this.clienteNatureza = clienteNatureza;
	}

	public void setClienteDocumento(String clienteDocumento) {
		this.clienteDocumento = clienteDocumento;
	}

	public void setClienteFilial(String clienteFilial) {
		this.clienteFilial = clienteFilial;
	}

	public void setClienteEndereco(String clienteEndereco) {
		this.clienteEndereco = clienteEndereco;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setFilialwmsNome(String filialwmsNome) {
		this.filialwmsNome = filialwmsNome;
	}

	public void setFilialwmsNatureza(String filialwmsNatureza) {
		this.filialwmsNatureza = filialwmsNatureza;
	}

	public void setFilialwmsDocumento(String filialwmsDocumento) {
		this.filialwmsDocumento = filialwmsDocumento;
	}

	public void setFilialwmsFilial(String filialwmsFilial) {
		this.filialwmsFilial = filialwmsFilial;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setEnderecoLogradouro(String enderecoLogradouro) {
		this.enderecoLogradouro = enderecoLogradouro;
	}

	public void setEnderecoNumero(String enderecoNumero) {
		this.enderecoNumero = enderecoNumero;
	}

	public void setEnderecoComplemento(String enderecoComplemento) {
		this.enderecoComplemento = enderecoComplemento;
	}

	public void setEnderecoBairro(String enderecoBairro) {
		this.enderecoBairro = enderecoBairro;
	}

	public void setEnderecoCep(String enderecoCep) {
		this.enderecoCep = enderecoCep;
	}

	public void setEnderecoPontoreferencia(String enderecoPontoreferencia) {
		this.enderecoPontoreferencia = enderecoPontoreferencia;
	}

	public void setEnderecoMunicipio(String enderecoMunicipio) {
		this.enderecoMunicipio = enderecoMunicipio;
	}

	public void setEnderecoUf(String enderecoUf) {
		this.enderecoUf = enderecoUf;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setPedidovendaDataEmissao(Date pedidovendaDataEmissao) {
		this.pedidovendaDataEmissao = pedidovendaDataEmissao;
	}

	public void setPedidovendaDataLancamento(Date pedidovendaDataLancamento) {
		this.pedidovendaDataLancamento = pedidovendaDataLancamento;
	}

	public void setPedidovendaFlagTroca(String pedidovendaFlagTroca) {
		this.pedidovendaFlagTroca = pedidovendaFlagTroca;
	}

	public void setPedidovendaIdExterno(String pedidovendaIdExterno) {
		this.pedidovendaIdExterno = pedidovendaIdExterno;
	}

	public void setPedidovendaObservacaointerna(String pedidovendaObservacaointerna) {
		this.pedidovendaObservacaointerna = pedidovendaObservacaointerna;
	}
	
	public void setListaPedidovendamaterialsincronizacao(List<Pedidovendamaterialsincronizacao> listaPedidovendamaterialsincronizacao) {
		this.listaPedidovendamaterialsincronizacao = listaPedidovendamaterialsincronizacao;
	}

	public Boolean getSincronizado() {
		return sincronizado;
	}

	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}
	
	@Transient
	public Integer getClienteId() {
		if(clienteId == null && cliente != null){
			return cliente.getCdpessoa();
		}
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	@Transient
	public Integer getFilialwmsId() {
		if(filialwmsId == null && empresa != null){
			return empresa.getCdpessoa();
		}
		return filialwmsId;
	}
	public void setFilialwmsId(Integer filialwmsId) {
		this.filialwmsId = filialwmsId;
	}
	
	@Transient
	public Integer getEnderecoId() {
		if(enderecoId == null && endereco != null){
			return endereco.getCdendereco();
		}
		return enderecoId;
	}
	@Transient
	public Integer getEnderecoIdCliente() {
		if(enderecoIdCliente == null && cliente != null){
			return cliente.getCdpessoa();
		}
		return enderecoIdCliente;
	}
	
	public void setEnderecoId(Integer enderecoId) {
		this.enderecoId = enderecoId;
	}
	public void setEnderecoIdCliente(Integer enderecoIdCliente) {
		this.enderecoIdCliente = enderecoIdCliente;
	}
	@DisplayName("Id")
	@Transient
	public Integer getPedidovendaId() {
		if(pedidovendaId == null && pedidovenda != null){
			return pedidovenda.getCdpedidovenda();
		}
		return pedidovendaId;
	}
	@DisplayName("Id Filial Venda")
	@Transient
	public Integer getPedidovendaIdFilialVenda() {
		if(pedidovendaIdFilialVenda == null && empresa != null){
			return empresa.getCdpessoa();
		}
		return pedidovendaIdFilialVenda;
	}
	@DisplayName("Id Cliente")
	@Transient
	public Integer getPedidovendaIdCliente() {
		if(pedidovendaIdCliente == null && cliente != null){
			return cliente.getCdpessoa();
		}
		return pedidovendaIdCliente;
	}
	
	public void setPedidovendaId(Integer pedidovendaId) {
		this.pedidovendaId = pedidovendaId;
	}

	public void setPedidovendaIdFilialVenda(Integer pedidovendaIdFilialVenda) {
		this.pedidovendaIdFilialVenda = pedidovendaIdFilialVenda;
	}

	public void setPedidovendaIdCliente(Integer pedidovendaIdCliente) {
		this.pedidovendaIdCliente = pedidovendaIdCliente;
	}
	
	@DisplayName("N�mero")
	@Transient
	public String getPedidovendaNumero() {
		if(pedidovendaNumero == null && pedidovenda != null && pedidovenda.getCdpedidovenda() != null){
			return pedidovenda.getCdpedidovenda().toString();
		}
		return pedidovendaNumero;
	}
	public void setPedidovendaNumero(String pedidovendaNumero) {
		this.pedidovendaNumero = pedidovendaNumero;
	}
	
	@DisplayName("Data da Sincroniza��o")
	public Timestamp getDtsincronizacao() {
		return dtsincronizacao;
	}
	public void setDtsincronizacao(Timestamp dtsincronizacao) {
		this.dtsincronizacao = dtsincronizacao;
	}
	
	@DisplayName("Data da Tentativa de Sincroniza��o")
	public Timestamp getDttentativasincronizacao() {
		return dttentativasincronizacao;
	}
	public void setDttentativasincronizacao(Timestamp dttentativasincronizacao) {
		this.dttentativasincronizacao = dttentativasincronizacao;
	}

	@Transient
	@DisplayName("Situa��o do Pedido de Venda")
	public Pedidovendasituacao getPedidovendasituacao() {
		return pedidovendasituacao;
	}

	public void setPedidovendasituacao(Pedidovendasituacao pedidovendasituacao) {
		this.pedidovendasituacao = pedidovendasituacao;
	}

	//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Erro")
	public String getErro() {
		return erro;
	}
	@DisplayName("Erro")
	public String getErrolog() {
		return errolog;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
	public void setErrolog(String errolog) {
		this.errolog = errolog;
	}

	@DisplayName("Desmarcar item")
	public Boolean getDesmarcaritens() {
		return desmarcaritens;
	}

	public void setDesmarcaritens(Boolean desmarcaritens) {
		this.desmarcaritens = desmarcaritens;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="pedidovendasincronizacao")
	public List<Pedidovendasincronizacaohistorico> getListaHistorico() {
		return listaHistorico;
	}
	public void setListaHistorico(List<Pedidovendasincronizacaohistorico> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}

	@DisplayName("Contador de Tentativa de envio")
	public Integer getContadorenvio() {
		return contadorenvio;
	}
	public void setContadorenvio(Integer contadorenvio) {
		this.contadorenvio = contadorenvio;
	}

	public Boolean getReenvio() {
		return reenvio;
	}
	public void setReenvio(Boolean reenvio) {
		this.reenvio = reenvio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtransportador")
	@DisplayName("Transportador")
	public Fornecedor getTransportador() {
		return transportador;
	}
	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}

	@Transient
	@DisplayName("Id")
	public Integer getTransportadorId() {
		if(transportadorId == null && transportador != null){
			transportadorId = transportador.getCdpessoa();
		}
		return transportadorId;
	}
	public void setTransportadorId(Integer transportadorId) {
		this.transportadorId = transportadorId;
	}

	@DisplayName("Nome")
	public String getTransportadorNome() {
		return transportadorNome;
	}
	public void setTransportadorNome(String transportadorNome) {
		this.transportadorNome = transportadorNome;
	}

	@DisplayName("Natureza")
	public String getTransportadorNatureza() {
		return transportadorNatureza;
	}
	public void setTransportadorNatureza(String transportadorNatureza) {
		this.transportadorNatureza = transportadorNatureza;
	}
	
	@DisplayName("CPF/CNPJ")
	public String getTransportadorCpfCnpj() {
		return transportadorCpfCnpj;
	}
	public void setTransportadorCpfCnpj(String transportadorCpfCnpj) {
		this.transportadorCpfCnpj = transportadorCpfCnpj;
	}	
}
