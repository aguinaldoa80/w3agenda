package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_garantiareformaitemconstatacao", sequenceName="sq_garantiareformaitemconstatacao")
public class Garantiareformaitemconstatacao {

	private Integer cdgarantiareformaitemconstatacao;
	private Garantiareformaitem garantiareformaitem;
	private Motivodevolucao motivodevolucao;
	
	@Id
	@GeneratedValue(generator="sq_garantiareformaitemconstatacao", strategy=GenerationType.AUTO)
	public Integer getCdgarantiareformaitemconstatacao() {
		return cdgarantiareformaitemconstatacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareformaitem")
	public Garantiareformaitem getGarantiareformaitem() {
		return garantiareformaitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivodevolucao")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public void setCdgarantiareformaitemconstatacao(Integer cdgarantiareformaitemconstatacao) {
		this.cdgarantiareformaitemconstatacao = cdgarantiareformaitemconstatacao;
	}
	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
}