package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculocombustivel", sequenceName = "sq_veiculocombustivel")
public class Veiculocombustivel implements Log{

	protected Integer cdveiculocombustivel;
	protected String descricao;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculocombustivel")
	public Integer getCdveiculocombustivel() {
		return cdveiculocombustivel;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(20)
	@DescriptionProperty
	@Required
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao.trim();
	}
	
	public void setCdveiculocombustivel(Integer cdveiculocombustivel) {
		this.cdveiculocombustivel = cdveiculocombustivel;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
}