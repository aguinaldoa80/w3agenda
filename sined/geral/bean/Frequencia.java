package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Frequencia {

	public static final Integer UNICA = 1;
	public static final Integer DIARIA = 2;
	public static final Integer SEMANAL = 3;
	public static final Integer QUINZENAL = 4;
	public static final Integer MENSAL = 5;
	public static final Integer ANUAL = 6;
	public static final Integer SEMESTRE = 7;
	public static final Integer HORA = 8;
	public static final Integer TRIMESTRAL = 9;
	
	protected Integer cdfrequencia;
	protected String nome;
	protected Boolean todastelas;
	
	public Frequencia(){}
	
	public Frequencia(Integer cdfrequencia){
		this.cdfrequencia = cdfrequencia;
	}
	
	public Frequencia(Integer cdfrequencia, String nome){
		this.cdfrequencia = cdfrequencia;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdfrequencia() {
		return cdfrequencia;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getTodastelas() {
		return todastelas;
	}

	public void setTodastelas(Boolean todastelas) {
		this.todastelas = todastelas;
	}

	public void setCdfrequencia(Integer cdfrequencia) {
		this.cdfrequencia = cdfrequencia;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdfrequencia == null) ? 0 : cdfrequencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frequencia other = (Frequencia) obj;
		if (cdfrequencia == null) {
			if (other.cdfrequencia != null)
				return false;
		} else if (!cdfrequencia.equals(other.cdfrequencia))
			return false;
		return true;
	}
	
}
