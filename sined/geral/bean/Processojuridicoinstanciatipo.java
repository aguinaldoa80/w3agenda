package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Processojuridicoinstanciatipo {
	
	public static Processojuridicoinstanciatipo PRIMEIRA = new Processojuridicoinstanciatipo(1, "Primeira");
	public static Processojuridicoinstanciatipo SEGUNDA = new Processojuridicoinstanciatipo(2, "Segunda");
	public static Processojuridicoinstanciatipo TERCEIRA = new Processojuridicoinstanciatipo(3, "Terceira");
	
	protected Integer cdprocessojuridicoinstanciatipo;
	protected String nome;
	
	public Processojuridicoinstanciatipo(){
		
	}
	public Processojuridicoinstanciatipo(Integer cdprocessojuridicoinstanciatipo, String nome){
		this.cdprocessojuridicoinstanciatipo = cdprocessojuridicoinstanciatipo;
		this.nome = nome;		
	}
	@Id
	public Integer getCdprocessojuridicoinstanciatipo() {
		return cdprocessojuridicoinstanciatipo;
	}
	@DescriptionProperty
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	
	public void setCdprocessojuridicoinstanciatipo(Integer cdprocessojuridicoinstanciatipo) {
		this.cdprocessojuridicoinstanciatipo = cdprocessojuridicoinstanciatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Processojuridicoinstanciatipo){
			return this.getCdprocessojuridicoinstanciatipo() != null && this.getCdprocessojuridicoinstanciatipo().equals(((Processojuridicoinstanciatipo)obj).getCdprocessojuridicoinstanciatipo());
		} else
			return false;
	}

}
