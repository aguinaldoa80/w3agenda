package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_grupoquestionario", sequenceName = "sq_grupoquestionario")
@DisplayName("Grupo Question�rio")
public class Grupoquestionario implements Log{
	
	protected Integer cdgrupoquestionario;
	protected String descricao;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer mediapontuacao;
	protected List<Questionarioquestao> listaquestionarioquestao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Questionariogrupo> listaQuestionariogrupo;
	protected Boolean naoseaplica = Boolean.FALSE;
	protected Atividadetipo atividadetipo;
	
	public Grupoquestionario() {
	}
	
	public Grupoquestionario(Integer cdgrupoquestionario){
		this.cdgrupoquestionario = cdgrupoquestionario;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupoquestionario")
	public Integer getCdgrupoquestionario() {
		return cdgrupoquestionario;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("M�dia Aprova��o")
	public Integer getMediapontuacao() {
		return mediapontuacao;
	}

	@DisplayName("Quest�es")
	@OneToMany(mappedBy="grupoquestionario")
	public List<Questionarioquestao> getListaquestionarioquestao() {
		return listaquestionarioquestao;
	}
	
	@DisplayName("Question�rio Grupo")
	@OneToMany(mappedBy="grupoquestionario")
	public List<Questionariogrupo> getListaQuestionariogrupo() {
		return listaQuestionariogrupo;
	}
	
	@DisplayName("Tipo de atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	@Transient
	@DisplayName("N�o se aplica")
	public Boolean getNaoseaplica() {
		return naoseaplica;
	}
	
	public void setCdgrupoquestionario(Integer cdgrupoquestionario) {
		this.cdgrupoquestionario = cdgrupoquestionario;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setMediapontuacao(Integer mediapontuacao) {
		this.mediapontuacao = mediapontuacao;
	}

	public void setListaquestionarioquestao(
			List<Questionarioquestao> listaquestionarioquestao) {
		this.listaquestionarioquestao = listaquestionarioquestao;
	}
	
	public void setListaQuestionariogrupo(List<Questionariogrupo> listaQuestionariogrupo) {
		this.listaQuestionariogrupo = listaQuestionariogrupo;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	public void setNaoseaplica(Boolean naoseaplica) {
		this.naoseaplica = naoseaplica;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Grupoquestionario) {
			Grupoquestionario grupoquestionario = (Grupoquestionario) obj;
			return grupoquestionario.getCdgrupoquestionario().equals(this.getCdgrupoquestionario());
		}
		return super.equals(obj);
	}
}
