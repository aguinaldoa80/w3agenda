package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoFinanceiraVinculoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OrigemMovimentacaoEstoqueEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Opera��o cont�bil")
@SequenceGenerator(name = "sq_operacaocontabil", sequenceName = "sq_operacaocontabil")
public class Operacaocontabil implements Log {

	private Integer cdoperacaocontabil;
	private String nome;
	private OperacaocontabilTipoEnum tipo;
	private OperacaoContabilTipoLancamento tipoLancamento;
	private OrigemMovimentacaoEstoqueEnum origemMovimentacaoEstoque;
	private Contagerencial contadebito;
	private Contagerencial contacredito;
	private Empresa empresa;
	private Integer codigointegracao;
	private String historico;
	private Contagerencial contagerencial;
	private Formapagamento formaPagamento;

	private Boolean agruparvalormesmaconta;
	private List<Operacaocontabildebitocredito> listaDebito;
	private List<Operacaocontabildebitocredito> listaCredito;
	private List<OperacaoContabilNaturezaOperacao> listaOperacaoContabilNaturezaOperacao;
	
	private List<OperacaoContabilEventoPagamento> listaEvento;
	
	private MovimentacaoFinanceiraVinculoEnum movimentacaoFinanceiraVinculo;

	// LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_operacaocontabil")
	public Integer getCdoperacaocontabil() {
		return cdoperacaocontabil;
	}

	@Required
	@MaxLength(150)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@DisplayName("Tipo")
	public OperacaocontabilTipoEnum getTipo() {
		return tipo;
	}
	
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdoperacaocontabiltipolancamento")
	@DisplayName("Tipo de lan�amento")
	public OperacaoContabilTipoLancamento getTipoLancamento() {
		return tipoLancamento;
	}

	@DisplayName("Origem")
	public OrigemMovimentacaoEstoqueEnum getOrigemMovimentacaoEstoque() {
		return origemMovimentacaoEstoque;
	}

	@DisplayName("Conta D�bito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontadebito")
	public Contagerencial getContadebito() {
		return contadebito;
	}

	@DisplayName("Conta Cr�dito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontacredito")
	public Contagerencial getContacredito() {
		return contacredito;
	}

	@DisplayName("Hist�rico Principal")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	
	@DisplayName("Agrupar valores para mesma conta")
	public Boolean getAgruparvalormesmaconta() {
		return agruparvalormesmaconta;
	}
	
	@DisplayName("D�bito")
	@OneToMany(mappedBy="operacaocontabildebito")
	public List<Operacaocontabildebitocredito> getListaDebito() {
		return listaDebito;
	}
	
	@DisplayName("Cr�dito")
	@OneToMany(mappedBy="operacaocontabilcredito")
	public List<Operacaocontabildebitocredito> getListaCredito() {
		return listaCredito;
	}
	
	@DisplayName("Eventos (Despesas do colaborador)")
	@OneToMany(mappedBy="operacaoContabil")
	public List<OperacaoContabilEventoPagamento> getListaEvento() {
		return listaEvento;
	}
	
	@DisplayName("Contas cont�beis d�bito")
	@Transient
	public String getContascontabeisDebito(){
		/*if (contadebito!=null){
			return contadebito.getNome();
		}else if (listaDebito!=null && !listaDebito.isEmpty()){
			return CollectionsUtil.listAndConcatenate(listaDebito, "contagerencial.nome", ", ");
		}else {
			return null;
		}*/
		
		String retorno = "";
		
		if (listaDebito!=null && !listaDebito.isEmpty()){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: listaDebito){
				if (operacaocontabildebitocredito.getContaContabil()!=null){
					retorno += operacaocontabildebitocredito.getContaContabil().getNome();
					retorno += ", ";
				}
			}
			if (!retorno.equals("")){
				retorno = retorno.substring(0, retorno.length()-2);
			}
		}
		
		return retorno;
	}
	
	@DisplayName("Contas cont�beis cr�dito")
	@Transient
	public String getContascontabeisCredito(){
		/*if (contacredito!=null){
			return contacredito.getNome();
		}else if (listaCredito!=null && !listaCredito.isEmpty()){
			return CollectionsUtil.listAndConcatenate(listaCredito, "contagerencial.nome", ", ");
		}else {
			return null;
		}*/
		
		String retorno = "";
		
		if (listaCredito!=null && !listaCredito.isEmpty()){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: listaCredito){
				if (operacaocontabildebitocredito.getContaContabil()!=null){
					retorno += operacaocontabildebitocredito.getContaContabil().getNome();
					retorno += ", ";
				}
			}
			if (!retorno.equals("")){
				retorno = retorno.substring(0, retorno.length()-2);
			}
		}
		
		return retorno;
	}

	public void setCdoperacaocontabil(Integer cdoperacaocontabil) {
		this.cdoperacaocontabil = cdoperacaocontabil;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipo(OperacaocontabilTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public void setTipoLancamento(OperacaoContabilTipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	public void setOrigemMovimentacaoEstoque(OrigemMovimentacaoEstoqueEnum origemMovimentacaoEstoque) {
		this.origemMovimentacaoEstoque = origemMovimentacaoEstoque;
	}

	public void setContadebito(Contagerencial contadebito) {
		this.contadebito = contadebito;
	}

	public void setContacredito(Contagerencial contacredito) {
		this.contacredito = contacredito;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	public void setAgruparvalormesmaconta(Boolean agruparvalormesmaconta) {
		this.agruparvalormesmaconta = agruparvalormesmaconta;
	}
	
	public void setListaDebito(List<Operacaocontabildebitocredito> listaDebito) {
		this.listaDebito = listaDebito;
	}
	
	public void setListaCredito(List<Operacaocontabildebitocredito> listaCredito) {
		this.listaCredito = listaCredito;
	}
	
	public void setListaEvento(List<OperacaoContabilEventoPagamento> listaEvento) {
		this.listaEvento = listaEvento;
	}
	
	@MaxLength(9)
	@DisplayName("C�digo Integra��o")
	public Integer getCodigointegracao() {
		return codigointegracao;
	}

	public void setCodigointegracao(Integer codigointegracao) {
		this.codigointegracao = codigointegracao;
	}

	// LOG

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Transient
	public String getDescricaoAutocomplete(){
		return getCdoperacaocontabil() + " - " + getNome();
	}
	
	@Transient
	public boolean isPossuiCentroCustoDebitoCredito(){
		boolean retorno = false;
		
		if (SinedUtil.isListNotEmpty(getListaDebito())){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: getListaDebito()){
				if (Boolean.TRUE.equals(operacaocontabildebitocredito.getCentrocusto())){
					retorno = true;
					break;
				}
			}
		}
		
		if (SinedUtil.isListNotEmpty(getListaCredito())){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: getListaCredito()){
				if (Boolean.TRUE.equals(operacaocontabildebitocredito.getCentrocusto())){
					retorno = true;
					break;
				}
			}
		}
		
		return retorno;
	}	
	
	@Transient
	public boolean isPossuiProjetoDebitoCredito(){
		boolean retorno = false;
		
		if (SinedUtil.isListNotEmpty(getListaDebito())){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: getListaDebito()){
				if (Boolean.TRUE.equals(operacaocontabildebitocredito.getProjeto())){
					retorno = true;
					break;
				}
			}
		}
		
		if (SinedUtil.isListNotEmpty(getListaCredito())){
			for (Operacaocontabildebitocredito operacaocontabildebitocredito: getListaCredito()){
				if (Boolean.TRUE.equals(operacaocontabildebitocredito.getProjeto())){
					retorno = true;
					break;
				}
			}
		}
		
		return retorno;
	}	
	
	@Override
	public boolean equals(Object obj) {
		try {
			return getCdoperacaocontabil().equals(((Operacaocontabil) obj).getCdoperacaocontabil()); 
		}catch(Exception e){
			return false;
		}
	}
	
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	@DisplayName("Forma de pagamento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdformapagamento")
	public Formapagamento getFormaPagamento() {
		return formaPagamento;
	}
	
	public void setFormaPagamento(Formapagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Tipo de V�nculo da Movimenta��o")
	public MovimentacaoFinanceiraVinculoEnum getMovimentacaoFinanceiraVinculo() {
		return movimentacaoFinanceiraVinculo;
	}
	
	public void setMovimentacaoFinanceiraVinculo(MovimentacaoFinanceiraVinculoEnum movimentacaoFinanceiraVinculo) {
		this.movimentacaoFinanceiraVinculo = movimentacaoFinanceiraVinculo;
	}

	@DisplayName("Natureza opera��o")
	@OneToMany(mappedBy="operacaocontabil")
	public List<OperacaoContabilNaturezaOperacao> getListaOperacaoContabilNaturezaOperacao() {
		return listaOperacaoContabilNaturezaOperacao;
	}

	public void setListaOperacaoContabilNaturezaOperacao(
			List<OperacaoContabilNaturezaOperacao> listaOperacaoContabilNaturezaOperacao) {
		this.listaOperacaoContabilNaturezaOperacao = listaOperacaoContabilNaturezaOperacao;
	}
	
	@Transient
	public String getWhereInNaturezaOperacao(){
		StringBuilder sb = new StringBuilder();
		if(SinedUtil.isListNotEmpty(getListaOperacaoContabilNaturezaOperacao())){
			 for(OperacaoContabilNaturezaOperacao operacaoContabilNaturezaOperacao : getListaOperacaoContabilNaturezaOperacao()){
				 if(operacaoContabilNaturezaOperacao.getNaturezaoperacao() != null && operacaoContabilNaturezaOperacao.getNaturezaoperacao().getCdnaturezaoperacao() != null){
					 sb.append(operacaoContabilNaturezaOperacao.getNaturezaoperacao().getCdnaturezaoperacao()).append(",");
				 }
			 }
		}
		return sb.length() > 0 ? sb.substring(0, sb.length()-1) : null;
	}
}