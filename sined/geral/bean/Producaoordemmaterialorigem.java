package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_producaoordemmaterialorigem", sequenceName = "sq_producaoordemmaterialorigem")
public class Producaoordemmaterialorigem {
	
	protected Integer cdproducaoordemmaterialorigem; 
	protected Producaoordemmaterial producaoordemmaterial;
	protected Producaoagenda producaoagenda;
	protected Producaoagendamaterial producaoagendamaterial;
	
	public Producaoordemmaterialorigem() {
	}

	public Producaoordemmaterialorigem(Producaoagenda producaoagenda, Producaoagendamaterial producaoagendamaterial) {
		this.producaoagenda = producaoagenda;
		this.producaoagendamaterial = producaoagendamaterial;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoordemmaterialorigem")
	public Integer getCdproducaoordemmaterialorigem() {
		return cdproducaoordemmaterialorigem;
	}

	@JoinColumn(name="cdproducaoordemmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	
	@JoinColumn(name="cdproducaoagenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	@JoinColumn(name="cdproducaoagendamaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagendamaterial getProducaoagendamaterial() {
		return producaoagendamaterial;
	}
	
	public void setProducaoagendamaterial(
			Producaoagendamaterial producaoagendamaterial) {
		this.producaoagendamaterial = producaoagendamaterial;
	}

	public void setCdproducaoordemmaterialorigem(
			Integer cdproducaoordemmaterialorigem) {
		this.cdproducaoordemmaterialorigem = cdproducaoordemmaterialorigem;
	}

	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}

	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	
}
