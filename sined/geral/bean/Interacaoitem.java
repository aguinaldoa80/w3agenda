package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_interacaoitem", sequenceName = "sq_interacaoitem")
public class Interacaoitem implements Log{

	protected Integer cdinteracaoitem;
	protected Date data;
	protected Interacao interacao;
	protected String de;
	protected String para;
	protected String assunto;
	protected Arquivo arquivo;
	protected String resumo;
	protected Interacaotipo interacaoTipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Proposta proposta;
	protected Prospeccao prospeccao;
	protected Arquivo anexo;
	protected Long indexarquivo;
	protected Integer indexlista;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_interacaoitem")
	public Integer getCdinteracaoitem() {
		return cdinteracaoitem;
	}
	@Required
	public Date getData() {
		return data;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinteracao")
	public Interacao getInteracao() {
		return interacao;
	}
	@MaxLength(50)
	@Required
	public String getDe() {
		return de;
	}
	@MaxLength(50)
	public String getPara() {
		return para;
	}
	@MaxLength(100)
	@Required
	public String getAssunto() {
		return assunto;
	}
	@DisplayName("Anexo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@MaxLength(1000)
	public String getResumo() {
		return resumo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinteracaotipo")
	@Required
	public Interacaotipo getInteracaoTipo() {
		return interacaoTipo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdinteracaoitem(Integer cdinteracaoitem) {
		this.cdinteracaoitem = cdinteracaoitem;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setInteracao(Interacao interacao) {
		this.interacao = interacao;
	}
	public void setDe(String de) {
		this.de = de;
	}
	public void setPara(String para) {
		this.para = para;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public void setInteracaoTipo(Interacaotipo interacaoTipo) {
		this.interacaoTipo = interacaoTipo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	@Transient
	public Prospeccao getProspeccao() {
		return prospeccao;
	}
	public void setProspeccao(Prospeccao prospeccao) {
		this.prospeccao = prospeccao;
	}
	@Transient
	public Proposta getProposta() {
		return proposta;
	}
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	@Transient
	public Arquivo getAnexo() {
		return anexo;
	}
	public void setAnexo(Arquivo anexo) {
		this.anexo = anexo;
	}
	@Transient
	public Long getIndexarquivo() {
		return indexarquivo;
	}
	public void setIndexarquivo(Long indexarquivo) {
		this.indexarquivo = indexarquivo;
	}
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}

}
