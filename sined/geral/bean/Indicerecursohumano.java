package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_indicerecursohumano", sequenceName = "sq_indicerecursohumano")
public class Indicerecursohumano implements Log {

	protected Integer cdindicerecursohumano;
	protected Indice indice;
	protected Cargo cargo;
	protected Double qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_indicerecursohumano")
	public Integer getCdindicerecursohumano() {
		return cdindicerecursohumano;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindice")
	@Required
	public Indice getIndice() {
		return indice;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	
	@Required
	@DisplayName("Qtde. H/H")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setIndice(Indice indice) {
		this.indice = indice;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setCdindicerecursohumano(Integer cdindicerecursohumano) {
		this.cdindicerecursohumano = cdindicerecursohumano;
	}

	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	

}
