package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_exameresponsavel", sequenceName = "sq_exameresponsavel")
@DisplayName("Profissional responsável")
public class Exameresponsavel implements Log{

	protected Integer cdexameresponsavel;
	protected String nome;
	protected String nit;
	protected String registroconselho;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Exameconvenioresponsavel> listaExameconvenioresponsavel = new ListSet<Exameconvenioresponsavel>(Exameconvenioresponsavel.class);
	
	//TRANSIENTS
	protected String nitformatado;
	protected String regimeconselhoformatado;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exameresponsavel")
	public Integer getCdexameresponsavel() {
		return cdexameresponsavel;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@MaxLength(20)
	@DisplayName("NIT")
	public String getNit() {
		return nit;
	}
	
	@MaxLength(20) 
	@DisplayName("Cadastro conselho da classe")
	public String getRegistroconselho() {
		return registroconselho;
	}
	
	@DisplayName("Convênios")
	@OneToMany(mappedBy="exameresponsavel")
	public List<Exameconvenioresponsavel> getListaExameconvenioresponsavel() {
		return listaExameconvenioresponsavel;
	}
	
	public void setListaExameconvenioresponsavel(List<Exameconvenioresponsavel> listaExameconvenioresponsavel) {
		this.listaExameconvenioresponsavel = listaExameconvenioresponsavel;
	}
	
	
	public void setNit(String nit) {
		this.nit = nit;
	}
	
	public void setRegistroconselho(String registroconselho) {
		this.registroconselho = registroconselho;
	}
	
	public void setCdexameresponsavel(Integer cdexameresponsavel) {
		this.cdexameresponsavel = cdexameresponsavel;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getNitformatado() {
		if(this.nit != null && this.nit.length() == 11)
			return this.nit.substring(0, 3)+"."+this.nit.substring(3, 8)+"."+this.nit.substring(8, 10)+"-"+this.nit.substring(10, 11);
		else 
			return this.nit;
	}
	
	@Transient
	public String getRegimeconselhoformatado() {
		if(this.registroconselho != null && this.registroconselho.length() == 9)
			return this.registroconselho.substring(0, 7)+"/"+this.registroconselho.substring(7, 9);
		else if(this.registroconselho != null && this.registroconselho.length() == 10 && this.registroconselho.indexOf("/") != -1){
			return this.registroconselho;
		}else return this.nit;
	}
	
}
