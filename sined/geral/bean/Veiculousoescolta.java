package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculousoescolta", sequenceName = "sq_veiculousoescolta")
public class Veiculousoescolta implements Log{

	protected Integer cdveiculousoescolta;
	protected Veiculouso veiculouso;
	protected Colaborador vigilante1;
	protected Colaborador vigilante2;
	protected Colaborador vigilante3;
	protected String origem;
	protected String destino;
	protected String celular1;
	protected String celular2;
	protected Boolean checoutelefone;
	protected Boolean checouradio;
	protected Boolean checoudocumentos;
	protected Boolean checouarmasmunicao;
	protected Boolean checoubateria;
	protected Boolean checourastreador;
	protected Veiculousoescoltatipo veiculousoescoltatipo;
	protected Veiculocor veiculocor;
	protected Veiculomodelo veiculomodelo;
	protected String placa;
	protected Uf uf;
	protected Municipio municipio;
	protected String motorista;
	protected String rgmotorista;
	protected String anotacoes;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculousoescolta")
	public Integer getCdveiculousoescolta() {
		return cdveiculousoescolta;
	}
	@Required
	@DisplayName("Ve�culo uso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso")
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	@DisplayName("Vigilante 1")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvigilante1")
	public Colaborador getVigilante1() {
		return vigilante1;
	}
	@DisplayName("Vigilante 2")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvigilante2")
	public Colaborador getVigilante2() {
		return vigilante2;
	}
	@DisplayName("Vigilante 3")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvigilante3")
	public Colaborador getVigilante3() {
		return vigilante3;
	}
	@Required
	@MaxLength(50)
	public String getOrigem() {
		return origem;
	}
	@Required
	@MaxLength(50)
	public String getDestino() {
		return destino;
	}
	@MaxLength(20)
	@DisplayName("Celular 1")
	public String getCelular1() {
		return celular1;
	}
	@MaxLength(20)
	@DisplayName("Celular 2")
	public String getCelular2() {
		return celular2;
	}
	@DisplayName("Telefone")
	public Boolean getChecoutelefone() {
		return checoutelefone;
	}
	@DisplayName("Radio")
	public Boolean getChecouradio() {
		return checouradio;
	}
	@DisplayName("Documentos")
	public Boolean getChecoudocumentos() {
		return checoudocumentos;
	}
	@DisplayName("Armas e muni��es")
	public Boolean getChecouarmasmunicao() {
		return checouarmasmunicao;
	}
	@DisplayName("Bateria")
	public Boolean getChecoubateria() {
		return checoubateria;
	}
	@DisplayName("Rastreador")
	public Boolean getChecourastreador() {
		return checourastreador;
	}
	@DisplayName("Cor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculocor")
	public Veiculocor getVeiculocor() {
		return veiculocor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@MaxLength(30)
	public String getMotorista() {
		return motorista;
	}
	@MaxLength(20)
	@DisplayName("RG do motorista")
	public String getRgmotorista() {
		return rgmotorista;
	}
	@MaxLength(255)
	@DisplayName("Anota��es")
	public String getAnotacoes() {
		return anotacoes;
	}
	@DisplayName("Tipo de escolta")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoescolta")
	public Veiculousoescoltatipo getVeiculousoescoltatipo() {
		return veiculousoescoltatipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculomodelo")
	public Veiculomodelo getVeiculomodelo() {
		return veiculomodelo;
	}
	@Required
	@MaxLength(10)
	public String getPlaca() {
		return placa;
	}
	public void setVeiculomodelo(Veiculomodelo veiculomodelo) {
		this.veiculomodelo = veiculomodelo;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setVeiculousoescoltatipo(
			Veiculousoescoltatipo veiculousoescoltatipo) {
		this.veiculousoescoltatipo = veiculousoescoltatipo;
	}
	public void setVigilante1(Colaborador vigilante1) {
		this.vigilante1 = vigilante1;
	}
	public void setVigilante2(Colaborador vigilante2) {
		this.vigilante2 = vigilante2;
	}
	public void setVigilante3(Colaborador vigilante3) {
		this.vigilante3 = vigilante3;
	}
	public void setOrigem(String origem) {
		this.origem = origem.trim();
	}
	public void setDestino(String destino) {
		this.destino = destino.trim();
	}
	public void setCelular1(String celular1) {
		this.celular1 = celular1 != null ? celular1.trim() : null;
	}
	public void setCelular2(String celular2) {
		this.celular2 = celular2 != null ? celular2.trim() : null;
	}
	public void setChecoutelefone(Boolean checoutelefone) {
		this.checoutelefone = checoutelefone;
	}
	public void setChecouradio(Boolean checouradio) {
		this.checouradio = checouradio;
	}
	public void setChecoudocumentos(Boolean checoudocumentos) {
		this.checoudocumentos = checoudocumentos;
	}
	public void setChecouarmasmunicao(Boolean checouarmasmunicao) {
		this.checouarmasmunicao = checouarmasmunicao;
	}
	public void setChecoubateria(Boolean checoubateria) {
		this.checoubateria = checoubateria;
	}
	public void setChecourastreador(Boolean checourastreador) {
		this.checourastreador = checourastreador;
	}
	public void setVeiculocor(Veiculocor veiculocor) {
		this.veiculocor = veiculocor;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}
	public void setRgmotorista(String rgmotorista) {
		this.rgmotorista = rgmotorista;
	}
	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}
	public void setCdveiculousoescolta(Integer cdveiculousoescolta) {
		this.cdveiculousoescolta = cdveiculousoescolta;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
}