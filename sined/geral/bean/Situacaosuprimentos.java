package br.com.linkcom.sined.geral.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaosuprimentos {
	
	EM_ABERTO(0,"Em aberto"),
	AUTORIZADA(1,"Autorizada"),
	EM_COTACAO(2,"Em cota��o"),
	FATURADA(3,"Faturada"),
	ORDEMCOMPRA_GERADA(4,"Ordem de compra gerada"),
	EM_PROCESSOCOMPRA(5,"Em processo de compra"),
	PEDIDO_ENVIADO(6,"Pedido enviado"),
	ENTREGA_PARCIAL(7,"Entrega parcial"),
	BAIXADA(8,"Baixada"),
	CANCELADA(9,"Cancelada"),
	ESTORNAR(10,"Estornar"),
	RATEIO_PENDENTE(11, "Rateio pendente"),
	APROVADA(12, "Aprovada"),
	NAO_AUTORIZADA(13, "N�o autorizada"),
	ROMANEIO_COMPLETO(14, "Romaneio completo");
	
	private Situacaosuprimentos (Integer codigo){
		this.codigo = codigo;
	}
	
	private Situacaosuprimentos (Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	private Integer codigo;
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<Situacaosuprimentos> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Situacaosuprimentos s : Situacaosuprimentos.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Situacaosuprimentos){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getCodigo()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
	
}
