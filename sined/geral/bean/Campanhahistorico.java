package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_campanhahistorico", sequenceName = "sq_campanhahistorico")
public class Campanhahistorico implements Log{
	
	protected Integer cdcampanhahistorico;
	protected String observacao;
	protected Campanha campanha;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campanhahistorico")
	public Integer getCdcampanhahistorico() {
		return cdcampanhahistorico;
	}

	@MaxLength(1024)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Campanha")
	@JoinColumn(name="cdcampanha")
	@ManyToOne(fetch=FetchType.LAZY)
	public Campanha getCampanha() {
		return campanha;
	}
	
	
	public void setCdcampanhahistorico(Integer cdcampanhahistorico) {
		this.cdcampanhahistorico = cdcampanhahistorico;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCampanha(Campanha campanha) {
		this.campanha = campanha;
	}
	
	
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}


	
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}

}
