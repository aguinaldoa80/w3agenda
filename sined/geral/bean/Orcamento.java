package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Or�amento")
@SequenceGenerator(name = "sq_orcamento", sequenceName = "sq_orcamento")
public class Orcamento implements Log{
	
	protected Integer cdorcamento;
	protected String nome;
	protected Cliente cliente;
	protected Municipio municipio;
	protected String endereco;
	protected Projetotipo projetotipo;
	protected Date data;
	protected Integer prazosemana;
	protected String observacao;
	protected Boolean calcularmaterialseguranca;
	protected Boolean calcularhistograma;
	protected Contagerencial contagerencialmod;
	protected Contagerencial contagerencialmoi;
	protected Bdicalculotipo bdicalculotipo;
	protected Oportunidade oportunidade;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<Calendarioorcamento> listaCalendarioorcamento;
	protected List<Planejamento> listaPlanejamento;
	protected Orcamentosituacao orcamentosituacao;	
	
//	TRANSIENTE
	protected Uf uf;
	protected Boolean copiartarefa;
	protected Boolean copiarrelacao;
	protected Boolean copiarcustomaoobraitem;
	protected Boolean copiarcomposicaomaoobra;
	protected Boolean manterprecoorcamento;
	protected List<Composicaoorcamento> listaCopiarComposicao;
	protected List<Orcamentomaterial> listaCopiarListaMaterial;
	protected Integer cdorcamentoantigo;
	protected Integer orcamentosituacaoValue;
	protected Integer calculobdiOrdinal;
	
	public Orcamento() {
	}
	
	public Orcamento(Integer cdorcamento) {
		this.cdorcamento = cdorcamento;
	}
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_orcamento")	
	public Integer getCdorcamento() {
		return cdorcamento;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	} 	
	
	@DisplayName("Munic�pio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	@Required
	public Date getData() {
		return data;
	}

	@Required
	@DisplayName("Prazo (semanas)")
	public Integer getPrazosemana() {
		return prazosemana;
	}

	@DisplayName("Observa��o")
	@MaxLength(255)
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Calcular EPI")
	public Boolean getCalcularmaterialseguranca() {
		return calcularmaterialseguranca;
	}
	
	@OneToMany(mappedBy="orcamento")
	public List<Calendarioorcamento> getListaCalendarioorcamento() {
		return listaCalendarioorcamento;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialmod")
	public Contagerencial getContagerencialmod() {
		return contagerencialmod;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialmoi")
	public Contagerencial getContagerencialmoi() {
		return contagerencialmoi;
	}
	
	@OneToMany(mappedBy="orcamento")
	public List<Planejamento> getListaPlanejamento() {
		return listaPlanejamento;
	}
	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamentosituacao")
	public Orcamentosituacao getOrcamentosituacao() {
		return orcamentosituacao;
	}
	
	public Boolean getCalcularhistograma() {
		return calcularhistograma;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbdicalculotipo")
	public Bdicalculotipo getBdicalculotipo() {
		return bdicalculotipo;
	}
	
	public void setBdicalculotipo(Bdicalculotipo bdicalculotipo) {
		this.bdicalculotipo = bdicalculotipo;
	}

	public void setCalcularhistograma(Boolean calcularhistograma) {
		this.calcularhistograma = calcularhistograma;
	}

	public void setListaPlanejamento(List<Planejamento> listaPlanejamento) {
		this.listaPlanejamento = listaPlanejamento;
	}
	
	public void setContagerencialmod(Contagerencial contagerencialmod) {
		this.contagerencialmod = contagerencialmod;
	}

	public void setContagerencialmoi(Contagerencial contagerencialmoi) {
		this.contagerencialmoi = contagerencialmoi;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}

	public void setListaCalendarioorcamento(
			List<Calendarioorcamento> listaCalendarioorcamento) {
		this.listaCalendarioorcamento = listaCalendarioorcamento;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setPrazosemana(Integer prazosemana) {
		this.prazosemana = prazosemana;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setCalcularmaterialseguranca(Boolean calcularmaterialseguranca) {
		this.calcularmaterialseguranca = calcularmaterialseguranca;
	}

	public void setCdorcamento(Integer cdorcamento) {
		this.cdorcamento = cdorcamento;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setOrcamentosituacao(Orcamentosituacao orcamentosituacao) {
		this.orcamentosituacao = orcamentosituacao;
	}

	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
//	TRASIENTE
	
	@Transient
	public Uf getUf() {
		return uf;
	}
	
	@Transient
	public Boolean getCopiartarefa() {
		return copiartarefa;
	}

	@Transient
	public Boolean getCopiarrelacao() {
		return copiarrelacao;
	}

	@Transient
	public List<Composicaoorcamento> getListaCopiarComposicao() {
		return listaCopiarComposicao;
	}

	@Transient
	public List<Orcamentomaterial> getListaCopiarListaMaterial() {
		return listaCopiarListaMaterial;
	}
	
	@Transient
	public Integer getCdorcamentoantigo() {
		return cdorcamentoantigo;
	}
	
	public void setCdorcamentoantigo(Integer cdorcamentoantigo) {
		this.cdorcamentoantigo = cdorcamentoantigo;
	}

	public void setCopiartarefa(Boolean copiartarefa) {
		this.copiartarefa = copiartarefa;
	}

	public void setCopiarrelacao(Boolean copiarrelacao) {
		this.copiarrelacao = copiarrelacao;
	}

	public void setListaCopiarComposicao(
			List<Composicaoorcamento> listaCopiarComposicao) {
		this.listaCopiarComposicao = listaCopiarComposicao;
	}
	
	public void setListaCopiarListaMaterial(
			List<Orcamentomaterial> listaCopiarListaMaterial) {
		this.listaCopiarListaMaterial = listaCopiarListaMaterial;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Transient
	public Integer getOrcamentosituacaoValue() {
		return orcamentosituacaoValue;
	}
	
	public void setOrcamentosituacaoValue(Integer orcamentosituacaoValue) {
		this.orcamentosituacaoValue = orcamentosituacaoValue;
	}	
	
	@Transient
	public Boolean getManterprecoorcamento() {
		return manterprecoorcamento;
	}
	
	public void setManterprecoorcamento(Boolean manterprecoorcamento) {
		this.manterprecoorcamento = manterprecoorcamento;
	}

	@Transient
	public Boolean getCopiarcustomaoobraitem() {
		return copiarcustomaoobraitem;
	}
	@Transient
	public Boolean getCopiarcomposicaomaoobra() {
		return copiarcomposicaomaoobra;
	}

	public void setCopiarcustomaoobraitem(Boolean copiarcustomaoobraitem) {
		this.copiarcustomaoobraitem = copiarcustomaoobraitem;
	}

	public void setCopiarcomposicaomaoobra(Boolean copiarcomposicaomaoobra) {
		this.copiarcomposicaomaoobra = copiarcomposicaomaoobra;
	}

	@DisplayName("Endere�o")
	@MaxLength(200)
	public String getEndereco() {
		return endereco;
	}

	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprojetotipo")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}
}
