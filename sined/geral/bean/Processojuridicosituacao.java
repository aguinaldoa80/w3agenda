package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Processojuridicosituacao {
	
	public static Processojuridicosituacao EMANDAMENTO = new Processojuridicosituacao(1, "Em andamento");
	public static Processojuridicosituacao CANCELADO = new Processojuridicosituacao(2, "Cancelado");
	public static Processojuridicosituacao VENCIDO = new Processojuridicosituacao(3, "Vencido");
	public static Processojuridicosituacao PERDIDO = new Processojuridicosituacao(4, "Perdido");
	
	protected Integer cdprocessojuridicosituacao;
	protected String nome;
	protected Boolean termino;
	
	public Processojuridicosituacao(){
		
	}
	
	public Processojuridicosituacao(Integer cdprocessojuridicosituacao, String nome){
		this.cdprocessojuridicosituacao = cdprocessojuridicosituacao;
		this.nome = nome;
	}

	@Id
	public Integer getCdprocessojuridicosituacao() {
		return cdprocessojuridicosituacao;
	}
	
	@DescriptionProperty
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	
	public Boolean getTermino() {
		return termino;
	}
	
	public void setTermino(Boolean termino) {
		this.termino = termino;
	}
	public void setCdprocessojuridicosituacao(Integer cdprocessojuridicosituacao) {
		this.cdprocessojuridicosituacao = cdprocessojuridicosituacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof Processojuridicosituacao){
		return this.getCdprocessojuridicosituacao() != null && this.getCdprocessojuridicosituacao().equals(((Processojuridicosituacao)obj).getCdprocessojuridicosituacao());
	} else
		return false;
	}

}
