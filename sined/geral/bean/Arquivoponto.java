package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Arquivo de ponto")
@SequenceGenerator(name = "sq_arquivoponto", sequenceName = "sq_arquivoponto")
public class Arquivoponto implements Log {

	protected Integer cdarquivoponto;
	protected Arquivo arquivo;
	protected Arquivo arquivosalario;
	protected Projeto projeto;
	protected Date dtinicio;
	protected Date dtfim;
	protected Usuario usuarioinsercao;
	protected Timestamp dtinsercao;
	protected Boolean processado;
	protected Usuario usuarioprocessado;
	protected Timestamp dtprocessamento;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	// TRANSIENTE
	protected String[] stringArquivo;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivoponto")
	public Integer getCdarquivoponto() {
		return cdarquivoponto;
	}

	@DisplayName("Arquivo de ponto")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Arquivo de sal�rio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivosalario")
	public Arquivo getArquivosalario() {
		return arquivosalario;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@Required
	@DisplayName("De")
	public Date getDtinicio() {
		return dtinicio;
	}

	@Required
	@DisplayName("At�")
	public Date getDtfim() {
		return dtfim;
	}

	@Required
	@DisplayName("Inserido por")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuarioinsercao")
	public Usuario getUsuarioinsercao() {
		return usuarioinsercao;
	}

	@Required
	@DisplayName("Data de inser��o")
	public Timestamp getDtinsercao() {
		return dtinsercao;
	}

	public Boolean getProcessado() {
		return processado;
	}

	@DisplayName("Processado por")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuarioprocessado")
	public Usuario getUsuarioprocessado() {
		return usuarioprocessado;
	}

	@DisplayName("Data de processamento")
	public Timestamp getDtprocessamento() {
		return dtprocessamento;
	}

	@DisplayName("Observa��o")
	@MaxLength(100)
	public String getObservacao() {
		return observacao;
	}
	
	public void setArquivosalario(Arquivo arquivosalario) {
		this.arquivosalario = arquivosalario;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setUsuarioinsercao(Usuario usuarioinsercao) {
		this.usuarioinsercao = usuarioinsercao;
	}

	public void setDtinsercao(Timestamp dtinsercao) {
		this.dtinsercao = dtinsercao;
	}

	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}

	public void setUsuarioprocessado(Usuario usuarioprocessado) {
		this.usuarioprocessado = usuarioprocessado;
	}

	public void setDtprocessamento(Timestamp dtprocessamento) {
		this.dtprocessamento = dtprocessamento;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdarquivoponto(Integer cdarquivoponto) {
		this.cdarquivoponto = cdarquivoponto;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTE
	
	@Transient
	public String[] getStringArquivo() {
		return stringArquivo;
	}

	public void setStringArquivo(String[] stringArquivo) {
		this.stringArquivo = stringArquivo;
	}
	
}
