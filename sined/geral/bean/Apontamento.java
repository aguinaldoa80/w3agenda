package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.sined.geral.bean.enumeration.ApontamentoSituacaoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_apontamento", sequenceName="sq_apontamento")
public class Apontamento implements Log, PermissaoProjeto{

	protected Integer cdapontamento;
	protected Date dtapontamento;
	protected Cargo cargo;
	protected Colaborador colaborador;
	protected Planejamento planejamento;
	protected Tarefa tarefa;
	protected ApontamentoTipo apontamentoTipo;
	protected Material material;
	protected Double qtdehoras;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<ApontamentoHoras> listaApontamentoHoras;
	protected Contrato contrato;
	protected Cliente cliente;
	protected Atividadetipo atividadetipo;
	protected String descricao;
	protected Boolean somentetotalhoras;
	protected Money valorhora;
	protected Tipocargo tipocargo;
	protected Requisicao requisicao;
	protected ApontamentoSituacaoEnum situacao;
	protected Integer cdautorizacaotrabalho; //Campo do MPSBR. Apenas utilizado para verificar o salvamento dos apontamentos
	
	//Transient
	protected Colaborador colaboradoratual;
	protected String listaHoras;
	protected String listaHorasReport;
	protected Hora hrinicioApontamento;
	protected Hora hrfimApontamento;
	protected Integer cdapontamentohoras;
	protected Money valorbrutoReceber;
//	protected Projeto projeto;

	@Id
	@GeneratedValue(generator="sq_apontamento", strategy=GenerationType.AUTO)
	public Integer getCdapontamento() {
		return cdapontamento;
	}
	@Required
	@DisplayName("Data")
	public Date getDtapontamento() {
		return dtapontamento;
	}
	@DisplayName("Fun��o")
	@JoinColumn(name="cdcargo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cargo getCargo() {
		return cargo;
	}
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@JoinColumn(name="cdplanejamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@JoinColumn(name="cdtarefa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tarefa getTarefa() {
		return tarefa;
	}
	@Required
	@DisplayName("Tipo de apontamento")
	@JoinColumn(name="cdapontamentotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public ApontamentoTipo getApontamentoTipo() {
		return apontamentoTipo;
	}
	@ValidationOverride(field="material.materialgrupo", required={})
	@DisplayName("Recurso geral")
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("Horas")
	@OneToMany(mappedBy="apontamento")
	public List<ApontamentoHoras> getListaApontamentoHoras() {
		return listaApontamentoHoras;
	}
	
	@Transient
	public Colaborador getColaboradoratual() {
		return colaboradoratual;
	}
	@DisplayName("Quantidade de horas")
	public Double getQtdehoras() {
		return qtdehoras;
	}
	
	@DisplayName("Somente total de horas")
	public Boolean getSomentetotalhoras() {
		return somentetotalhoras;
	}
	
	@DisplayName("Valor Unit�rio")
	public Money getValorhora() {
		return valorhora;
	}
	
	@DisplayName("Tipo de cargo")
	@JoinColumn(name="cdtipocargo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tipocargo getTipocargo() {
		return tipocargo;
	}
	
	public void setValorhora(Money valorhora) {
		this.valorhora = valorhora;
	}
	
	public void setTipocargo(Tipocargo tipocargo) {
		this.tipocargo = tipocargo;
	}
	
	public void setSomentetotalhoras(Boolean somentetotalhoras) {
		this.somentetotalhoras = somentetotalhoras;
	}
	
	public void setCdapontamento(Integer cdapontamento) {
		this.cdapontamento = cdapontamento;
	}

	public void setDtapontamento(Date dtapontamento) {
		this.dtapontamento = dtapontamento;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}

	public void setApontamentoTipo(ApontamentoTipo apontamentoTipo) {
		this.apontamentoTipo = apontamentoTipo;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaApontamentoHoras(
			List<ApontamentoHoras> listaApontamentoHoras) {
		this.listaApontamentoHoras = listaApontamentoHoras;
	}
	
	public void setColaboradoratual(Colaborador colaboradoratual) {
		this.colaboradoratual = colaboradoratual;
	}
	public void setQtdehoras(Double quantidade) {
		this.qtdehoras = quantidade;
	}
	
	@DisplayName("Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@ValidationOverride(field="contrato.cliente", required={})
	@DisplayName("Contrato")
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Tipo de atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Transient
	@DisplayName("Hor�rios")
	public String getListaHoras() {
		if(getListaApontamentoHoras() != null && getListaApontamentoHoras().size() > 0){
			String str = "";
			for (ApontamentoHoras apontamentoHoras : getListaApontamentoHoras()) {
				str += apontamentoHoras.getHrinicio().toString() + "-" + apontamentoHoras.getHrfim().toString() + "<BR>";
			}
			return str;
		} else {
			return null;
		}
	}
	public void setListaHoras(String listaHoras) {
		this.listaHoras = listaHoras;
	}
	@Transient
	public String getListaHorasReport() {
		if(getListaApontamentoHoras() != null && getListaApontamentoHoras().size() > 0){
			String str = "";
			for (ApontamentoHoras apontamentoHoras : getListaApontamentoHoras()) {
				str += apontamentoHoras.getHrinicio().toString() + " - " + apontamentoHoras.getHrfim().toString() + "\n";
			}
			return str.substring(0, str.length()-1);
		} else {
			return null;
		}
	}
	public void setListaHorasReport(String listaHorasReport) {
		this.listaHorasReport = listaHorasReport;
	}
	
	@Transient
	public String getQtdeStr(){
		Double qtdehoras = getQtdehoras();
		if(getApontamentoTipo().equals(ApontamentoTipo.FUNCAO_HORA)){
			return SinedUtil.transformDecimalHourString(qtdehoras);
		} else {
			return new DecimalFormat("#,##0.00").format(qtdehoras);
		}
	}
	
	@Transient
	public String getQtdeStrTotal(){
		String qtdeStr = getQtdehoras().toString();
		String[] hours = qtdeStr.split("\\.");
		
		Integer hora = Integer.parseInt(hours[0]);
		String horaStr = hora.toString();
		if(hora < 10) horaStr = "0" + hora.toString();
		
		Double min = getQtdehoras() - Double.parseDouble(hora.toString());
		min = min * 60;
		long minInt = Math.round(min);
		String minStr = minInt + "";
		if(minInt < 10)  minStr = "0" + minInt;
		
		return horaStr + ":" + minStr;
	}
	
	public String subQueryProjeto() {
		return "select apontamentosubQueryProjeto.cdapontamento " +
				"from Apontamento apontamentosubQueryProjeto " +
				"join apontamentosubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	@Required
	public Hora getHrinicioApontamento() {
		return hrinicioApontamento;
	}
	@Transient
	@Required
	public Hora getHrfimApontamento() {
		return hrfimApontamento;
	}
	
	@Transient
	@Required
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return planejamento != null ? planejamento.getProjeto() : null;
	}
	
	public void setProjeto(Projeto projeto) {
		if(this.planejamento != null){
			this.planejamento.setProjeto(projeto);
		}
	}
	
	public void setHrinicioApontamento(Hora hrinicioApontamento) {
		this.hrinicioApontamento = hrinicioApontamento;
	}
	public void setHrfimApontamento(Hora hrfimApontamento) {
		this.hrfimApontamento = hrfimApontamento;
	}
	
	@JoinColumn(name="cdrequisicao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Requisicao getRequisicao() {
		return requisicao;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	
	@Transient
	public Integer getCdapontamentohoras() {
		return cdapontamentohoras;
	}
	public void setCdapontamentohoras(Integer cdapontamentohoras) {
		this.cdapontamentohoras = cdapontamentohoras;
	}
	
	@Required
	@DisplayName("Situa��o")
	public ApontamentoSituacaoEnum getSituacao() {
		return situacao;
	}
	public void setSituacao(ApontamentoSituacaoEnum situacao) {
		this.situacao = situacao;
	}

	@Transient
	public Money getValorbrutoReceber() {
		return valorbrutoReceber;
	}
	public void setValorbrutoReceber(Money valorbrutoReceber) {
		this.valorbrutoReceber = valorbrutoReceber;
	}
	
	@Column(insertable=false, updatable=false)
	public Integer getCdautorizacaotrabalho() {
		return cdautorizacaotrabalho;
	}
	public void setCdautorizacaotrabalho(Integer cdautorizacaotrabalho) {
		this.cdautorizacaotrabalho = cdautorizacaotrabalho;
	}
}
