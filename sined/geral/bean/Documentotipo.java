package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BandeiraCartaoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.MeioPagamentoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDocumentoLCDPR;
import br.com.linkcom.sined.geral.bean.enumeration.TipoIntegracaoNFe;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_documentotipo", sequenceName = "sq_documentotipo")
@DisplayName("Tipo de Documento")
public class Documentotipo implements Log{

	protected Integer cddocumentotipo;
	protected String nome;
	protected Centrocusto centrocusto;
	protected Boolean percentual;
	protected Double taxavenda;
	protected Boolean notafiscal;
	protected Boolean ordemcompra;
	protected Boolean antecipacao;
	protected Boolean boleto;
	protected Integer finalizadoraemporium;
	protected Conta contadestino;
	protected Integer diasreceber;
	protected Boolean permitirvendasemanalisecredito;
	protected Tipotributo tipotributo;
	protected Formapagamento formapagamentocredito;
	protected Formapagamento formapagamentodebito;
	protected Boolean cartao;
	protected Contagerencial contagerencialtaxa;
	protected Boolean gerarmovimentacaoseparadataxa;
	protected Boolean preencherdatabanco;
	protected Boolean exibirnavenda;
	protected Boolean exibirnacompra;
	protected Tipotaxa tipotaxacontareceber;
	protected Boolean taxanoprocessamentoextratocartaocredito;
	protected Prazopagamento prazoApropriacao;
	
	protected MeioPagamentoNFe meiopagamentonfe;
	protected TipoIntegracaoNFe tipointegracaonfe;
	protected Cnpj cnpjcredenciadoranfe;
	protected BandeiraCartaoNFe bandeiracartaonfe;
	protected TipoDocumentoLCDPR tipoDocumentoLCDPR;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Documentotipopapel> listaDocumentotipopapel = new ListSet<Documentotipopapel>(Documentotipopapel.class);
	private List<ClienteDocumentoTipo> listaClienteDocumentoTipo;
	private List<PrazoPagamentoECF> listaPrazoPagamentoECF; 
	
	
	//Transient
	protected Empresa empresa;
	protected Money valorTotal;
	
	
	//////////////////////////////////////////////|
	/// INICIO construdor e variaveis estaticas///|
	//////////////////////////////////////////////|
	
	public static final Documentotipo BOLETO = new Documentotipo(2);
	
	public Documentotipo() {
	}
	
	public Documentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	
	public Documentotipo(String nome) {
		this.nome = nome;
	}
	public Documentotipo(Integer cddocumentotipo, String nome) {
		this.cddocumentotipo = cddocumentotipo;
		this.nome = nome;
	}
	public Documentotipo(Integer cddocumentotipo, String nome, Boolean antecipacao) {
		this.cddocumentotipo = cddocumentotipo;
		this.nome = nome;
		this.antecipacao = antecipacao;
	}

	////////////////////////|
	/// INICIO DOS get   ///|
	////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentotipo")
	public Integer getCddocumentotipo() {
		return cddocumentotipo;
	}
	
	@DisplayName("Visualizar ordem de compra?")
	public Boolean getOrdemcompra() {
		return ordemcompra;
	}
	
	@DisplayName("Conta Gerencial da taxa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialtaxa")
	public Contagerencial getContagerencialtaxa() {
		return contagerencialtaxa;
	}

	@DisplayName("Gerar movimenta��o separada da taxa")
	public Boolean getGerarmovimentacaoseparadataxa() {
		return gerarmovimentacaoseparadataxa;
	}
	
	@DisplayName("Preencher data do banco")
	public Boolean getPreencherdatabanco() {
		return preencherdatabanco;
	}
	
	@DisplayName("Exibir na compra")
	public Boolean getExibirnacompra() {
		return exibirnacompra;
	}
	
	@DisplayName("Exibir na venda/NF")
	public Boolean getExibirnavenda() {
		return exibirnavenda;
	}
	@DisplayName("Tipo de Taxa da Conta a Receber")
	@JoinColumn(name="cdtipotaxacontareceber")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tipotaxa getTipotaxacontareceber() {
		return tipotaxacontareceber;
	}
	@DisplayName("Considerar taxa apenas no processamento do extrato do cart�o de cr�dito")
	public Boolean getTaxanoprocessamentoextratocartaocredito() {
		return taxanoprocessamentoextratocartaocredito;
	}
	@DisplayName("Prazo de apropria��o")
	@JoinColumn(name="cdprazoapropriacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazoApropriacao() {
		return prazoApropriacao;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@DisplayName("Nota fiscal")
	public Boolean getNotafiscal() {
		return notafiscal;
	}
	@DisplayName("Taxa")
	@MaxLength(9)
	public Double getTaxavenda() {
		return taxavenda;
	}
	
	@JoinColumn(name="cdcentrocusto")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Centro de custo da taxa")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Adiantamento")
	public Boolean getAntecipacao() {
		return antecipacao;
	}
	@DisplayName("Boleto")
	public Boolean getBoleto() {
		return boleto;
	}
	
	@DisplayName("C�digo da Finalizadora (ECF)")
	public Integer getFinalizadoraemporium() {
		return finalizadoraemporium;
	}
	
	@DisplayName("Dias para Receber (Previs�o)")
	public Integer getDiasreceber() {
		return diasreceber;
	}
	
	@DisplayName("Conta Destino (Baixa)")
	@JoinColumn(name="cdcontadestino")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getContadestino() {
		return contadestino;
	}
	
	@DisplayName("Permitir venda sem an�lise de cr�dito")
	public Boolean getPermitirvendasemanalisecredito() {
		return permitirvendasemanalisecredito;
	}
	
	@DisplayName("Meio de Pagamento")
	public MeioPagamentoNFe getMeiopagamentonfe() {
		return meiopagamentonfe;
	}

	@DisplayName("Tipo de Integra��o para o Pagamento")
	public TipoIntegracaoNFe getTipointegracaonfe() {
		return tipointegracaonfe;
	}

	@DisplayName("CNPJ da Credenciadora de Cart�o de Cr�dito e/ou D�bito")
	public Cnpj getCnpjcredenciadoranfe() {
		return cnpjcredenciadoranfe;
	}

	@DisplayName("Bandeira da Operadora de Cart�o de Cr�dito e/ou D�bito")
	public BandeiraCartaoNFe getBandeiracartaonfe() {
		return bandeiracartaonfe;
	}
	
	@DisplayName("Tipo de Documento (LCDPR)")
	public TipoDocumentoLCDPR getTipoDocumentoLCDPR() {
		return tipoDocumentoLCDPR;
	}
	
	@OneToMany(mappedBy="documentotipo")
	public List<ClienteDocumentoTipo> getListaClienteDocumentoTipo() {
		return listaClienteDocumentoTipo;
	}
	
	@OneToMany(mappedBy="documentotipo")
	@DisplayName("N�vel de Acesso")
	public List<Documentotipopapel> getListaDocumentotipopapel() {
		return listaDocumentotipopapel;
	}
	
	@OneToMany(mappedBy="documentoTipo")
	@DisplayName("Prazos de Pagamento por Finalizadora (ECF)")
	public List<PrazoPagamentoECF> getListaPrazoPagamentoECF() {
		return listaPrazoPagamentoECF;
	}
	
	////////////////////////|
	/// INICIO DOS set   ///|
	////////////////////////|
	
	public void setListaPrazoPagamentoECF(List<PrazoPagamentoECF> listaPrazoPagamentoECF) {
		this.listaPrazoPagamentoECF = listaPrazoPagamentoECF;
	}
	
	public void setOrdemcompra(Boolean ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	
	
	public void setNotafiscal(Boolean notafiscal) {
		this.notafiscal = notafiscal;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setMeiopagamentonfe(MeioPagamentoNFe meiopagamentonfe) {
		this.meiopagamentonfe = meiopagamentonfe;
	}

	public void setTipointegracaonfe(TipoIntegracaoNFe tipointegracaonfe) {
		this.tipointegracaonfe = tipointegracaonfe;
	}

	public void setCnpjcredenciadoranfe(Cnpj cnpjcredenciadoranfe) {
		this.cnpjcredenciadoranfe = cnpjcredenciadoranfe;
	}

	public void setBandeiracartaonfe(BandeiraCartaoNFe bandeiracartaonfe) {
		this.bandeiracartaonfe = bandeiracartaonfe;
	}
	
	public void setTipoDocumentoLCDPR(TipoDocumentoLCDPR tipoDocumentoLCDPR) {
		this.tipoDocumentoLCDPR = tipoDocumentoLCDPR;
	}

	public void setPermitirvendasemanalisecredito(
			Boolean permitirvendasemanalisecredito) {
		this.permitirvendasemanalisecredito = permitirvendasemanalisecredito;
	}
	
	public void setContadestino(Conta contadestino) {
		this.contadestino = contadestino;
	}
	
	public void setDiasreceber(Integer diasreceber) {
		this.diasreceber = diasreceber;
	}
	
	public void setFinalizadoraemporium(Integer finalizadoraemporium) {
		this.finalizadoraemporium = finalizadoraemporium;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setTaxavenda(Double taxavenda) {
		this.taxavenda = taxavenda;
	}
	public void setCddocumentotipo(Integer cddocumentotipo) {
		this.cddocumentotipo = cddocumentotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setBoleto(Boolean boleto) {
		this.boleto = boleto;
	}


	public void setListaDocumentotipopapel(
			List<Documentotipopapel> listaDocumentotipopapel) {
		this.listaDocumentotipopapel = listaDocumentotipopapel;
	}
	
	public void setAntecipacao(Boolean antecipacao) {
		this.antecipacao = antecipacao;
	}

	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@DisplayName("Tipo de c�lculo")
	public Boolean getPercentual() {
		return percentual;
	}
	public void setPercentual(Boolean percentual) {
		this.percentual = percentual;
	}	
		
	
	
	public void setListaClienteDocumentoTipo(List<ClienteDocumentoTipo> listaClienteDocumentoTipo) {
		this.listaClienteDocumentoTipo = listaClienteDocumentoTipo;
	}

	@DisplayName("Tipo de tributo")
	public Tipotributo getTipotributo() {
		return tipotributo;
	}

	public void setTipotributo(Tipotributo tipotributo) {
		this.tipotributo = tipotributo;
	}

	@DisplayName("Forma de pagamento (Cr�dito)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamentocredito")
	public Formapagamento getFormapagamentocredito() {
		return formapagamentocredito;
	}

	public void setFormapagamentocredito(Formapagamento formapagamentocredito) {
		this.formapagamentocredito = formapagamentocredito;
	}
	
	@DisplayName("Forma de pagamento (D�bito)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamentodebito")
	public Formapagamento getFormapagamentodebito() {
		return formapagamentodebito;
	}

	public void setFormapagamentodebito(Formapagamento formapagamentodebito) {
		this.formapagamentodebito = formapagamentodebito;
	}

	@DisplayName("Cart�o")
	public Boolean getCartao() {
		return cartao;
	}

	public void setCartao(Boolean cartao) {
		this.cartao = cartao;
	}
	
	
	public void setContagerencialtaxa(Contagerencial contagerencialtaxa) {
		this.contagerencialtaxa = contagerencialtaxa;
	}

	public void setGerarmovimentacaoseparadataxa(Boolean gerarmovimentacaoseparadataxa) {
		this.gerarmovimentacaoseparadataxa = gerarmovimentacaoseparadataxa;
	}
	
	public void setPreencherdatabanco(Boolean preencherdatabanco) {
		this.preencherdatabanco = preencherdatabanco;
	}
	
	public void setExibirnacompra(Boolean exibirnacompra) {
		this.exibirnacompra = exibirnacompra;
	}
	
	public void setExibirnavenda(Boolean exibirnavenda) {
		this.exibirnavenda = exibirnavenda;
	}
	public void setTipotaxacontareceber(Tipotaxa tipotaxacontareceber) {
		this.tipotaxacontareceber = tipotaxacontareceber;
	}
	public void setTaxanoprocessamentoextratocartaocredito(
			Boolean taxanoprocessamentoextratocartaocredito) {
		this.taxanoprocessamentoextratocartaocredito = taxanoprocessamentoextratocartaocredito;
	}
	public void setPrazoApropriacao(Prazopagamento prazoApropriacao) {
		this.prazoApropriacao = prazoApropriacao;
	}
	
/////////////////////////////////////|
/// INICIO DOS transient e outros ///|
/////////////////////////////////////|
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cddocumentotipo == null) ? 0 : cddocumentotipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documentotipo other = (Documentotipo) obj;
		if (cddocumentotipo == null) {
			if (other.cddocumentotipo != null)
				return false;
		} else if (!cddocumentotipo.equals(other.cddocumentotipo))
			return false;
		return true;
	}
	
	@Transient
	public Money getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	
	
}