package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_atividadetipoveterinaria",sequenceName="sq_atividadetipoveterinaria")
public class Atividadetipoveterinaria implements Log {

	private Integer cdatividadetipoveterinaria;
	private String nome;
	private Boolean ativo = Boolean.TRUE;
	private Colaborador responsavel;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Boolean internacao;
	private Boolean anamneseObrigatorio;
	private Boolean reserva;
	private Localarmazenagem localarmazenagem;
	
	@Id
	@GeneratedValue(generator="sq_atividadetipoveterinaria", strategy=GenerationType.AUTO)
	public Integer getCdatividadetipoveterinaria() {
		return cdatividadetipoveterinaria;
	}
	@DescriptionProperty
	@DisplayName("Descrição")
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Responsável")
	@JoinColumn(name="cdresponsavel")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getResponsavel() {
		return responsavel;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Internação")
	public Boolean getInternacao() {
		return internacao;
	}
	@DisplayName("Obrigar anamnese")
	public Boolean getAnamneseObrigatorio() {
		return anamneseObrigatorio;
	}
	public Boolean getReserva() {
		return reserva;
	}
	@DisplayName("Local")
	@JoinColumn(name="cdlocalarmazenagem")
	@ManyToOne(fetch=FetchType.LAZY)
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setReserva(Boolean reserva) {
		this.reserva = reserva;
	}
	public void setCdatividadetipoveterinaria(Integer cdatividadetipoveterinaria) {
		this.cdatividadetipoveterinaria = cdatividadetipoveterinaria;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setInternacao(Boolean internacao) {
		this.internacao = internacao;
	}
	public void setAnamneseObrigatorio(Boolean anamneseObrigatorio) {
		this.anamneseObrigatorio = anamneseObrigatorio;
	}
	
	@Override
	public boolean equals(Object obj) {
		return getCdatividadetipoveterinaria().equals(((Atividadetipoveterinaria) obj).getCdatividadetipoveterinaria());
	}
}