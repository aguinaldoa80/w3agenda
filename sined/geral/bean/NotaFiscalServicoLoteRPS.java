package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_NotaFiscalServicoLoteRPS",sequenceName="sq_NotaFiscalServicoLoteRPS")
public class NotaFiscalServicoLoteRPS implements Log {

	protected Integer cdNotaFiscalServicoLoteRPS;
	protected Arquivo arquivo;
	protected Arquivo arquivotomador;
	protected Timestamp datacriacao;
	
	protected List<NotaFiscalServicoRPS> listaNotaFiscalServicoRPS = new ArrayList<NotaFiscalServicoRPS>();
	
	@Id
	@GeneratedValue(generator="sq_NotaFiscalServicoLoteRPS",strategy=GenerationType.AUTO)
	public Integer getCdNotaFiscalServicoLoteRPS() {
		return cdNotaFiscalServicoLoteRPS;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Data de cria��o")
	public Timestamp getDatacriacao() {
		return datacriacao;
	}
	
	@DisplayName("Arquivo de Tomador")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivotomador")
	public Arquivo getArquivotomador() {
		return arquivotomador;
	}
	
	@OneToMany(mappedBy="notaFiscalServicoLoteRPS")
	public List<NotaFiscalServicoRPS> getListaNotaFiscalServicoRPS() {
		return listaNotaFiscalServicoRPS;
	}
	
	public void setListaNotaFiscalServicoRPS(
			List<NotaFiscalServicoRPS> listaNotaFiscalServicoRPS) {
		this.listaNotaFiscalServicoRPS = listaNotaFiscalServicoRPS;
	}
	
	public void setArquivotomador(Arquivo arquivotomador) {
		this.arquivotomador = arquivotomador;
	}
	
	public void setDatacriacao(Timestamp datacriacao) {
		this.datacriacao = datacriacao;
	}
	
	public void setCdNotaFiscalServicoLoteRPS(Integer cdNotaFiscalServicoLoteRPS) {
		this.cdNotaFiscalServicoLoteRPS = cdNotaFiscalServicoLoteRPS;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	@Transient
	@Override
	public Integer getCdusuarioaltera() {
		return null;
	}
	
	@Transient
	@Override
	public Timestamp getDtaltera() {
		return null;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		
	}

}