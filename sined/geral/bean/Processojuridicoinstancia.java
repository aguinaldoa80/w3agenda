package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_processojuridicoinstancia", sequenceName = "sq_processojuridicoinstancia")
public class Processojuridicoinstancia {
	
	protected Integer cdprocessojuridicoinstancia;
	protected Processojuridicoinstanciatipo processojuridicoinstanciatipo;
	protected String numero;
	protected Date data;
	protected Vara vara;
	protected Date dtultimaatualizacao;
	protected String descultimaatualizacao;
	protected Processojuridico processojuridico;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridicoinstancia")
	public Integer getCdprocessojuridicoinstancia() {
		return cdprocessojuridicoinstancia;
	}
	@DisplayName("Tipo")
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridicoinstanciatipo")
	public Processojuridicoinstanciatipo getProcessojuridicoinstanciatipo() {
		return processojuridicoinstanciatipo;
	}
	@MaxLength(50)
	@Required
	@DescriptionProperty
	@DisplayName("N�mero do processo")
	public String getNumero() {
		return numero;
	}
	@Required
	public Date getData() {
		return data;
	}
	@Required
	@DisplayName("Vara")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdvara")
	public Vara getVara() {
		return vara;
	}
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@DisplayName("Processo Jur�dico")
	@JoinColumn(name="cdprocessojuridico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}
	
	@DisplayName("Data da �ltima atualiza��o")
	public Date getDtultimaatualizacao() {
		return dtultimaatualizacao;
	}
	
	@DisplayName("Descri��o da �ltima atualiza��o")
	public String getDescultimaatualizacao() {
		return descultimaatualizacao;
	}
	
	
	public void setCdprocessojuridicoinstancia(Integer cdprocessojuridicoinstancia) {
		this.cdprocessojuridicoinstancia = cdprocessojuridicoinstancia;
	}
	public void setProcessojuridicoinstanciatipo(
			Processojuridicoinstanciatipo processojuridicoinstanciatipo) {
		this.processojuridicoinstanciatipo = processojuridicoinstanciatipo;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setVara(Vara vara) {
		this.vara = vara;
	}
	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}
	public void setDtultimaatualizacao(Date dtultimaatualizacao) {
		this.dtultimaatualizacao = dtultimaatualizacao;
	}
	public void setDescultimaatualizacao(String descultimaatualizacao) {
		this.descultimaatualizacao = descultimaatualizacao;
	}	
	
}
