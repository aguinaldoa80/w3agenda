package br.com.linkcom.sined.geral.bean;



import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_colaboradorocorrencia", sequenceName = "sq_colaboradorocorrencia")
public class ColaboradorOcorrencia {

	protected Integer cdcolaboradorocorrencia;
	protected String descricao;
	protected Date dtocorrencia = new Date(System.currentTimeMillis());
	protected Colaborador colaborador;
	protected ColaboradorOcorrenciaTipo colaboradorOcorrenciaTipo;
	protected Usuario responsavel = SinedUtil.getUsuarioLogado();
	
	@ManyToOne
	@JoinColumn(name="cdusuario")
	@DisplayName("Responsável")
	public Usuario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	@ManyToOne
	@JoinColumn(name="cdcolaborador")	
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorocorrencia")	
	public Integer getCdcolaboradorocorrencia() {
		return cdcolaboradorocorrencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public Date getDtocorrencia() {
		return dtocorrencia;
	}
	@ManyToOne
	@JoinColumn(name="cdcolaboradorocorrenciatipo")
	@DisplayName("Tipo ocorrência")
	public ColaboradorOcorrenciaTipo getColaboradorOcorrenciaTipo() {
		return colaboradorOcorrenciaTipo;
	}	
	public void setCdcolaboradorocorrencia(Integer cdcolaboradorocorrencia) {
		this.cdcolaboradorocorrencia = cdcolaboradorocorrencia;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}
	public void setColaboradorOcorrenciaTipo(
			ColaboradorOcorrenciaTipo colaboradorOcorrenciaTipo) {
		this.colaboradorOcorrenciaTipo = colaboradorOcorrenciaTipo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	
}
