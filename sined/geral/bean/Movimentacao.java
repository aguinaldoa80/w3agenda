package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CollectionOfElements;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_movimentacao;
import br.com.linkcom.sined.geral.bean.state.MovimentacaoState;
import br.com.linkcom.sined.geral.bean.state.movimentacao.Cancelada;
import br.com.linkcom.sined.geral.bean.state.movimentacao.Conciliada;
import br.com.linkcom.sined.geral.bean.state.movimentacao.Normal;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name="sq_movimentacao",sequenceName="sq_movimentacao")
@DisplayName("Movimenta��o")
@JoinEmpresa("movimentacao.empresa,movimentacao.conta.listaContaempresa.empresa")
public class Movimentacao implements Log, Comparable<Movimentacao>, PermissaoProjeto{

	protected Integer cdmovimentacao;
	protected Conta conta;
	protected Empresa empresa;
	protected Money valor;
	protected Money valortaxa;
	protected Date dtmovimentacao;
	protected Date dtbanco;
	protected Tipooperacao tipooperacao;
	protected Movimentacaoacao movimentacaoacao;
	protected Rateio rateio;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Set<Movimentacaohistorico> listaMovimentacaohistorico;
	protected List<Movimentacaoorigem> listaMovimentacaoorigem;
	protected Formapagamento formapagamento;	
	protected Cheque cheque;
	protected String checknum;
	protected String historico;
	protected String observacaocopiacheque;
	protected String fitid;
	protected Aux_movimentacao aux_movimentacao;
	protected Arquivo comprovante;
	protected Boolean antecipada;
	protected Operacaocontabil operacaocontabil;
	protected String bordero;
	protected Boolean taxa;
	protected Boolean taxageradaprocessamentocartao;
//	protected List<Movimentacaoorigem> listaMovimentacaoorigemrelacionada; //Criado para a tela de Gerar Lan�amento Cont�bil
	protected List<Movimentacaoorigem> listaMovimentacaoorigemcartaocredito; //Criado para a tela de Gerar Lan�amento Cont�bil
	protected List<Movimentacaoorigem> movimentacaoorigemTaxa;
	
	//Estados da movimenta��o
	protected MovimentacaoState movimentacaoState;
	protected Normal normal;
	protected Cancelada cancelada;
	protected Conciliada conciliada;
	
	//TRANSIENTES
	protected String observacao;
	protected String origem;
	protected Integer cdAgendamento;
	protected Money saldo;
	protected String origemStr;
	protected Boolean readonly;
	protected Movimentacao movimentacao;
	protected boolean movCartao = false;
	protected Money credito = new Money();
	protected Money debito = new Money();
	protected String movimentacaodescricao;
	protected Double valorDouble;
	protected String whereInDespesaviagem;
	protected Boolean adiantamento;
	protected String controller;
	protected List<Movimentacao> listaMovimentacoes = new ListSet<Movimentacao>(Movimentacao.class);
	protected String idsCheck;
	protected Money valorTarifa;
	protected String motivoTarifa;
	protected String stringMovimentacao;
	protected String historicoObservacao;
	protected boolean conciliacao;
	protected Money valorantecipacao;
	protected Fornecedor fornecedorAntecipacao;
	protected Money taxatransacaopercentual;
	protected Money taxatransacaovalor;
	protected Money taxavalor;
	protected String whereIn;
	protected String valorstr;
	protected List<Documento> listaDocumento;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected Boolean gerarMovimentacaoPorCheque = Boolean.TRUE;
	protected String nomepessoa;
	protected String descricaodocumento;
	protected Money valorrateioreportdebito;
	protected Money valorrateioreportcredito;
	protected Date dataAtrasada;
	protected Movimentacao movimentacaoAnterior;
	protected Boolean associada;
	protected Boolean selecao;
	protected Movimentacaohistorico movimentacaohistoricoTransient;
	
	protected Integer bancochequeorigem;
	protected Integer agenciachequeorigem;
	protected Integer contachequeorigem;
	protected String numerochequeorigem;
	protected String emitentechequeorigem;
	protected Date dtbomparachequeorigem;
	protected Date ultimoFechamento;
	protected Date dtdebitotarifa;
	protected List<Documento> listaDocumentoNovo;
	protected String observacaoConciliacao;
	protected boolean transferencia = false;
	
	protected boolean percentual;
	
	public Movimentacao(){
		initState();
	}
	public Movimentacao(Integer cdmovimentacao){
		this.cdmovimentacao = cdmovimentacao;
		initState();
	}
	public Movimentacao(Movimentacao mov){
		this.cdmovimentacao = mov.cdmovimentacao;
		this.dtmovimentacao = mov.dtmovimentacao;
		this.dtbanco = mov.dtbanco;
		this.valor = mov.valor;
		this.aux_movimentacao = mov.aux_movimentacao;
		this.conta = mov.conta;
		this.tipooperacao = mov.tipooperacao;
		this.movCartao = true;
		if(this.tipooperacao.equals(Tipooperacao.TIPO_CREDITO)){
			this.credito = valor;
		}
		if(this.tipooperacao.equals(Tipooperacao.TIPO_DEBITO)){
			this.debito = valor;
		}
		initState();
	}

	/**
	 * Usado na query do relat�rio de extrato de conta
	 * 
	 * @param dtmovimentacao
	 * @param historico
	 * @param checknum
	 * @param valor
	 * @param tipooperacao
	 * @author Hugo Ferreira
	 */
	public Movimentacao(java.util.Date dtmovimentacao, String historico,
			String checknum, Money valor, String tipooperacao) {
		this.dtmovimentacao = new java.sql.Date(dtmovimentacao.getTime());
		this.historico = historico;
		this.checknum = checknum;
		this.valor = valor;
		this.tipooperacao = new Tipooperacao(null, tipooperacao);
	}
	
	
	/*
	 * CONSTRUTOR USADO NO updateListagemQuery
	 */
	public Movimentacao(
			Integer movimentacao_cdmovimentacao,
			String conta_nome, 
			java.util.Date movimentacao_dtmovimentacao, 
			String movimentacao_historico, 
			Money movimentacao_valor,
			Integer tipooperacao_cdtipooperacao, 
			String tipooperacao_nome, 
			String movimentacao_checknum, 
			java.util.Date movimentacao_dtbanco,
			Integer movimentacaoacao_cdmovimentacaoacao,
			String movimentacaoacao_nome,
			String cheque_numero, 
			java.util.Date coalesce_data,
			String movimentacao_observacaocopiacheque,
			String movimentacao_historicoObservacao){
		
		cdmovimentacao = movimentacao_cdmovimentacao;
		conta = new Conta(conta_nome);
		dtmovimentacao = movimentacao_dtmovimentacao != null ? new Date(movimentacao_dtmovimentacao.getTime()) : null;
		historico = movimentacao_historico;
		valor = movimentacao_valor;
		tipooperacao = new Tipooperacao(tipooperacao_cdtipooperacao, tipooperacao_nome);
		checknum = movimentacao_checknum;
		dtbanco = movimentacao_dtbanco != null ? new Date(movimentacao_dtbanco.getTime()) : null;
		movimentacaoacao = new Movimentacaoacao(movimentacaoacao_cdmovimentacaoacao, movimentacaoacao_nome);
		cheque = new Cheque(cheque_numero);	
		observacaocopiacheque = movimentacao_observacaocopiacheque;
	}
	
	
	
	@Id
	@GeneratedValue(generator="sq_movimentacao",strategy=GenerationType.AUTO)
	@DescriptionProperty
	public Integer getCdmovimentacao() {
		return cdmovimentacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	@DisplayName("Conta")
	public Conta getConta() {
		return conta;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	public Money getValortaxa() {
		return valortaxa;
	}
	@MaxLength(50)
	public String getChecknum() {
		return checknum;
	}
	@Required
	@DisplayName("Data da movimenta��o")
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	@DisplayName("Data do banco")
	public Date getDtbanco() {
		return dtbanco;
	}
	@DisplayName("Tipo de opera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	@Required
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoacao")
	@DisplayName("Situa��o")
	public Movimentacaoacao getMovimentacaoacao() {
		return movimentacaoacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="movimentacao")
	public Set<Movimentacaohistorico> getListaMovimentacaohistorico() {
		return listaMovimentacaohistorico;
	}
	@CollectionOfElements
//	@IndexColumn(name="cddocumentacaoorigem")
	@OneToMany(mappedBy="movimentacao")
	public List<Movimentacaoorigem> getListaMovimentacaoorigem() {
		return listaMovimentacaoorigem;
	}
	@Transient
	@MaxLength(100)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@Transient
	public String getOrigem() {
		return origem;
	}
	@MaxLength(100)
	@Transient
	@DisplayName("Descri��es das contas")
	public String getDescricaocontas() {
		return SinedUtil.listAndConcatenate(listaMovimentacaoorigem, "documento.descricao", ", ");
	}
	
	@Transient
	@DisplayName("Fornecedores")
	public String getTodosFornecedores(){
		return SinedUtil.listAndConcatenate(listaMovimentacaoorigem, "documento.pessoa.nome", ", ");
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	public Cheque getCheque() {
		return cheque;
	}
	@DisplayName("Hist�rico")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacao", insertable=false, updatable=false)
	public Aux_movimentacao getAux_movimentacao() {
		return aux_movimentacao;
	}
	@Transient
	public Money getSaldo() {
		return saldo;
	}
	@Transient
	public Conciliada getConciliada() {
		return conciliada;
	}
	@Transient
	public Cancelada getCancelada() {
		return cancelada;
	}
	@Transient
	public Normal getNormal() {
		return normal;
	}
	
	@MaxLength(50)
	@Column(name="observacao")
	@DisplayName("Observa��o")
	public String getObservacaocopiacheque() {
		return observacaocopiacheque;
	}
	
	public String getFitid() {
		return fitid;
	}	
	
	@Transient
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	
	public void setFitid(String fitid) {
		this.fitid = fitid;
	}
	
	public void setObservacaocopiacheque(String observacaocopiacheque) {
		this.observacaocopiacheque = observacaocopiacheque;
	}
	
	public void setAux_movimentacao(Aux_movimentacao aux_movimentacao) {
		this.aux_movimentacao = aux_movimentacao;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	@Transient
	public Integer getCdAgendamento() {
		return cdAgendamento;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public void setChecknum(String checknum) {
		this.checknum = checknum;
	}
	public void setCdmovimentacao(Integer cdmovimentacao) {
		this.cdmovimentacao = cdmovimentacao;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValortaxa(Money valortaxa) {
		this.valortaxa = valortaxa;
	}
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}
	public void setDtbanco(Date dtconciliacao) {
		this.dtbanco = dtconciliacao;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	public void setMovimentacaoacao(Movimentacaoacao movimentacaoacao) {
		this.movimentacaoacao = movimentacaoacao;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaMovimentacaohistorico(Set<Movimentacaohistorico> listaMovimentacaohistorico) {
		this.listaMovimentacaohistorico = listaMovimentacaohistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public void setListaMovimentacaoorigem(List<Movimentacaoorigem> listaMovimentacaoorigem) {
		this.listaMovimentacaoorigem = listaMovimentacaoorigem;
	}
	public void setCdAgendamento(Integer cdAgendamento) {
		this.cdAgendamento = cdAgendamento;
	}
	
	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	
	@Transient
	public String getOrigemStr() {
		return origemStr;
	}
	
	public void setOrigemStr(String origemStr) {
		this.origemStr = origemStr;
	}
	
	@Transient
	public Boolean getReadonly() {
		return readonly;
	}
	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}
	
	@Transient
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	@Transient
	public boolean isMovCartao() {
		return movCartao;
	}
	@Transient
	public Money getCredito() {
		return credito;
	}
	@Transient
	public Money getDebito() {
		return debito;
	}
	
	/**
	 * Obt�m a data da movimenta��o que deve ser utilizada.
	 * Se existir <tt>dtbanco</tt>, ent�o esta � retornada, do contr�rio, retorna-se <tt>dtmovimentacao</tt>.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Transient
	public String getDtBancoFormatado(){
		
		return SinedDateUtils.toString(dtbanco);
	}
	@Transient
	public Date getDataMovimentacao(){
		return dtbanco != null ? dtbanco : dtmovimentacao;
	}
	@Transient
	public String getMovimentacaodescricao() {
		return getAux_movimentacao() != null ? getAux_movimentacao().getMovimentacaodescricao() : null;
	}
	@Transient
	public Double getValorDouble() {
//		Double valorDouble = 0d;
//		if(getRateio() != null && getRateio().getListaRateioitem() != null && getRateio().getListaRateioitem().size() > 0){
//			for (Rateioitem ri : getRateio().getListaRateioitem()) {
//				valorDouble += ri.getValor().getValue().doubleValue();
//			}
//		}
//		return valorDouble;
		return getValor() != null ? getValor().getValue().doubleValue() : null;
	}
	@Transient
	public Boolean getAdiantamento() {
		return adiantamento;
	}
	
	@Transient
	public String getController() {
		return controller;
	}
	@Transient
	public String getWhereInDespesaviagem() {
		return whereInDespesaviagem;
	}
	
	public void setWhereInDespesaviagem(String whereInDespesaviagem) {
		this.whereInDespesaviagem = whereInDespesaviagem;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public void setAdiantamento(Boolean adiantamento) {
		this.adiantamento = adiantamento;
	}
	public void setValorDouble(Double valorDouble) {
		this.valorDouble = valorDouble;
	}
	public void setMovimentacaodescricao(String movimentacaodescricao) {
		this.movimentacaodescricao = movimentacaodescricao;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
	public void setMovCartao(boolean movCartao) {
		this.movCartao = movCartao;
	}
	public void setCredito(Money credito) {
		this.credito = credito;
	}
	public void setDebito(Money debito) {
		this.debito = debito;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Movimentacao) {
			Movimentacao mov = (Movimentacao) obj;
			return this.cdmovimentacao.equals(mov.cdmovimentacao);
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		return "ID: " + cdmovimentacao +  
				" Conta: " + conta +
				(getDataMovimentacao() != null ? " Data: " + SinedDateUtils.toString(getDataMovimentacao()) : "") +
				" Valor: " + valor + " Historico: ";
	}

	/**
	 * M�todo para concatenar hist�ricos de movimenta��es usando o separador "|".
	 * 
	 * @param historico
	 * @author Fl�vio Tavares
	 */
	public void appendHistorico(String historico){
		if(this.historico == null){
			if(StringUtils.isNotEmpty(historico)){
				this.historico = historico;
			}
		}else{
			if(StringUtils.isNotEmpty(historico)){
				this.historico = this.historico + " | " + historico;
			}
		}
	}
	
	public int compareTo(Movimentacao m) {
		if(m.getDataMovimentacao() != null && this.getDataMovimentacao() != null){
			return m.getDataMovimentacao().compareTo(this.getDataMovimentacao());
		}
		return 0;
	}
	
	/**
	 * M�todo para verificar se as origens da movimenta�ao est�o autorizadas, se for 
	 * conta a pagar, ou definitivas, se for conta a receber.
	 * 
	 * @param movimentacao
	 * @return true - se todos os documentos da origem est�o com situa��o Autorizada/Definitiva.
	 * 			false - se pelo menos um documento est� com situa��o diferente de Autorizada/Definitiva.
	 * @author Fl�vio Tavares
	 */
	@Transient
	public boolean isOrigensAutorizadasDefinitivas(){
		boolean autorizadaDefinitiva = true;
		if(SinedUtil.isListNotEmpty(listaMovimentacaoorigem)){
			for (Movimentacaoorigem origem : listaMovimentacaoorigem) {
				Documento doc = origem.getDocumento();
				if(doc != null){
					if(Documentoclasse.OBJ_PAGAR.equals(doc.getDocumentoclasse())){
						autorizadaDefinitiva = Documentoacao.AUTORIZADA.equals(doc.getDocumentoacao()) 
												|| Documentoacao.AUTORIZADA_PARCIAL.equals(doc.getDocumentoacao());
					}else{
						autorizadaDefinitiva = Documentoacao.DEFINITIVA.equals(doc.getDocumentoacao());
					}
					if(!autorizadaDefinitiva) break;
				}
			}
		}
		return autorizadaDefinitiva;
	}
	
	// .. M�todos relacionados � mudan�a de estado da movienta��o .. //
	
	/**
	 * Interface para se cancelar uma movimenta��o.
	 * @see #initState()
	 * @see br.com.linkcom.sined.geral.bean.state.MovimentacaoState
	 * @return true - se a movimenta��o foi cancelada
	 * 			false - se a movimenta��o n�o pode ser cancelada
	 * @author Fl�vio Tavares
	 */
	public boolean cancelar(){
		initState();
		return this.movimentacaoState.cancelar();
	}
	
	/**
	 * Interface para se estornar uma movimenta��o.
	 * @see #initState()
	 * @see br.com.linkcom.sined.geral.bean.state.MovimentacaoState
	 * @return true - se a movimenta��o foi estornada
	 * 			false - se a movimenta��o n�o pode ser estornada
	 * @author Fl�vio Tavares
	 */
	public boolean estornar(){
		initState();
		return this.movimentacaoState.estornar();
	}
	
	/**
	 * Interface para se conciliar uma movimenta��o.
	 * @see #initState()
	 * @see br.com.linkcom.sined.geral.bean.state.MovimentacaoState
	 * @return true - se a movimenta��o foi conciliada
	 * 			false - se a movimenta��o n�o pode ser conciliada
	 * @author Fl�vio Tavares
	 */
	public boolean conciliar(){
		initState();
		return this.movimentacaoState.conciliar();
	}
	
	/**
	 * M�todo para alterar o estado da movimenta��o.
	 * 
	 * @param state
	 */
	public void alterarEstado(MovimentacaoState state){
		this.movimentacaoState = state;
	}
	
	/**
	 * M�todo para iniciar o estado da movimenta��o de acordo com a a��o.
	 * @see #movimentacaoacao
	 */
	private void initState(){
		this.conciliada = this.conciliada == null ? new Conciliada(this) : this.conciliada;
		this.cancelada = this.cancelada == null ? new Cancelada(this) : this.cancelada;
		this.normal = this.normal == null ? new Normal(this) : this.normal;
		this.movimentacaoState = null;
		
		if(SinedUtil.isObjectValid(this.movimentacaoacao)){
			if(Movimentacaoacao.CANCELADA.equals(this.movimentacaoacao)){
				this.movimentacaoState = this.cancelada;
			}else if(Movimentacaoacao.CONCILIADA.equals(this.movimentacaoacao)){
				this.movimentacaoState = this.conciliada;
			}else if(Movimentacaoacao.NORMAL.equals(this.movimentacaoacao)){
				this.movimentacaoState = this.normal;
			}
		}
	}
	
	public String subQueryProjeto() {
		return "select movimentacaosubQueryProjeto.cdmovimentacao " +
				"from Movimentacao movimentacaosubQueryProjeto " +
				"join movimentacaosubQueryProjeto.rateio rateiosubQueryProjeto " +
				"join rateiosubQueryProjeto.listaRateioitem listaRateioitemsubQueryProjeto " +
				"left outer join listaRateioitemsubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto is null or projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public List<Movimentacao> getListaMovimentacoes() {
		return listaMovimentacoes;
	}
	public void setListaMovimentacoes(List<Movimentacao> listaMovimentacoes) {
		this.listaMovimentacoes = listaMovimentacoes;
	}

	@Transient
	public String getIdsCheck() {
		return idsCheck;
	}
	public void setIdsCheck(String idsCheck) {
		this.idsCheck = idsCheck;
	}
	
	@Transient
	public String getMotivoTarifa() {
		return motivoTarifa;
	}
	@Transient
	public Money getValorTarifa() {
		return valorTarifa;
	}
	
	public void setMotivoTarifa(String motivoTarifa) {
		this.motivoTarifa = motivoTarifa;
	}
	public void setValorTarifa(Money valorTarifa) {
		this.valorTarifa = valorTarifa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomprovante")
	public Arquivo getComprovante() {
		return comprovante;
	}
	public void setComprovante(Arquivo comprovante) {
		this.comprovante = comprovante;
	}
	
	@Transient
	public String getStringMovimentacao() {
		return stringMovimentacao;
	}
	public void setStringMovimentacao(String stringMovimentacao) {
		this.stringMovimentacao = stringMovimentacao;
	}

	@Transient
	@DisplayName("Hist�rico / Observa��o")
	public String getHistoricoObservacao() {
		historicoObservacao = "";
 		if (historico != null && !historico.equals("")){
			historicoObservacao += historico;
		}
 		if (observacaocopiacheque != null && !observacaocopiacheque.equals("")){
 			if (historico != null && !historico.equals("")){
 				historicoObservacao += " / " + observacaocopiacheque;
 			} else {
 				historicoObservacao += observacaocopiacheque;
 			}
 		}
		return historicoObservacao;
	}
	public void setHistoricoObservacao(String historicoObservacao) {
		this.historicoObservacao = historicoObservacao;
	}
	
	@Transient
	public boolean isConciliacao() {
		return conciliacao;
	}
	public void setConciliacao(boolean conciliacao) {
		this.conciliacao = conciliacao;
	}
	
	@Transient
	@DisplayName("Valor de Antecipa��o")
	public Money getValorantecipacao() {
		return valorantecipacao;
	}
	public void setValorantecipacao(Money valorantecipacao) {
		this.valorantecipacao = valorantecipacao;
	}
	@Transient
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedorAntecipacao() {
		return fornecedorAntecipacao;
	}
	public void setFornecedorAntecipacao(Fornecedor fornecedorAntecipacao) {
		this.fornecedorAntecipacao = fornecedorAntecipacao;
	}
	@Transient
	@DisplayName("Percentual")
	public Money getTaxatransacaopercentual() {
		return taxatransacaopercentual;
	}	
	public void setTaxatransacaopercentual(Money taxatransacaopercentual) {
		this.taxatransacaopercentual = taxatransacaopercentual;
	}
	@Transient
	@DisplayName("Valor")
	public Money getTaxatransacaovalor() {
		return taxatransacaovalor;
	}
	public void setTaxatransacaovalor(Money taxatransacaovalor) {
		this.taxatransacaovalor = taxatransacaovalor;
	}
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@DisplayName("Antecipada")
	public Boolean getAntecipada() {
		return antecipada;
	}
	public void setAntecipada(Boolean antecipada) {
		this.antecipada = antecipada;
	}
	@Transient
	public Money getTaxavalor() {
		return taxavalor;
	}
	public void setTaxavalor(Money taxavalor) {
		this.taxavalor = taxavalor;
	}
	
	@Transient
	@DisplayName("Banco")
	public Integer getBancochequeorigem() {
		return bancochequeorigem;
	}
	@Transient
	@DisplayName("Ag�ncia")
	public Integer getAgenciachequeorigem() {
		return agenciachequeorigem;
	}
	@Transient
	@DisplayName("Conta")
	public Integer getContachequeorigem() {
		return contachequeorigem;
	}
	@Transient
	@DisplayName("N�mero")
	@MaxLength(100)
	public String getNumerochequeorigem() {
		return numerochequeorigem;
	}
	@Transient
	@DisplayName("Emitente")
	public String getEmitentechequeorigem() {
		return emitentechequeorigem;
	}
	@Transient
	@DisplayName("Bom para")
	public Date getDtbomparachequeorigem() {
		return dtbomparachequeorigem;
	}
	
	public void setBancochequeorigem(Integer bancochequeorigem) {
		this.bancochequeorigem = bancochequeorigem;
	}
	public void setAgenciachequeorigem(Integer agenciachequeorigem) {
		this.agenciachequeorigem = agenciachequeorigem;
	}
	public void setContachequeorigem(Integer contachequeorigem) {
		this.contachequeorigem = contachequeorigem;
	}
	public void setNumerochequeorigem(String numerochequeorigem) {
		this.numerochequeorigem = numerochequeorigem;
	}
	public void setEmitentechequeorigem(String emitentechequeorigem) {
		this.emitentechequeorigem = emitentechequeorigem;
	}
	public void setDtbomparachequeorigem(Date dtbomparachequeorigem) {
		this.dtbomparachequeorigem = dtbomparachequeorigem;
	}
	
	@Transient
	public String getValorstr() {
		if(valor != null)
			return "R$ " + valor.toString();
		
		return "R$ 0,00";
	}
	
	@Transient
	public Date getUltimoFechamento() {
		return ultimoFechamento;
	}
	public void setUltimoFechamento(Date ultimoFechamento) {
		this.ultimoFechamento = ultimoFechamento;
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}

	@Transient
	@DisplayName("Gerar uma movimenta��o por cheque")
	public Boolean getGerarMovimentacaoPorCheque() {
		return gerarMovimentacaoPorCheque;
	}
	public void setGerarMovimentacaoPorCheque(Boolean gerarMovimentacaoPorCheque) {
		this.gerarMovimentacaoPorCheque = gerarMovimentacaoPorCheque;
	}
	
	@Transient
	public Date getDtdebitotarifa() {
		return dtdebitotarifa;
	}
	public void setDtdebitotarifa(Date dtdebitotarifa) {
		this.dtdebitotarifa = dtdebitotarifa;
	}

	@Transient
	public String getNomepessoa() {
		return nomepessoa;
	}
	@Transient
	public String getDescricaodocumento() {
		return descricaodocumento;
	}
	public void setNomepessoa(String nomepessoa) {
		this.nomepessoa = nomepessoa;
	}
	public void setDescricaodocumento(String descricaodocumento) {
		this.descricaodocumento = descricaodocumento;
	}

	@Transient	
	public Money getValorrateioreportdebito() {
		return valorrateioreportdebito;
	}
	@Transient
	public Money getValorrateioreportcredito() {
		return valorrateioreportcredito;
	}
	public void setValorrateioreportdebito(Money valorrateioreportdebito) {
		this.valorrateioreportdebito = valorrateioreportdebito;
	}
	public void setValorrateioreportcredito(Money valorrateioreportcredito) {
		this.valorrateioreportcredito = valorrateioreportcredito;
	}
	@Required
	@DisplayName("Opera��o Cont�bil")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	@Transient
	public Date getDataAtrasada() {
		return dataAtrasada;
	}
	public void setDataAtrasada(Date dataAtrasada) {
		this.dataAtrasada = dataAtrasada;
	}
	
	@Transient
	public List<Documento> getListaDocumentoNovo() {
		return listaDocumentoNovo;
	}
	public void setListaDocumentoNovo(List<Documento> listaDocumentoNovo) {
		this.listaDocumentoNovo = listaDocumentoNovo;
	}

	@Transient
	public Movimentacao getMovimentacaoAnterior() {
		return movimentacaoAnterior;
	}
	public void setMovimentacaoAnterior(Movimentacao movimentacaoAnterior) {
		this.movimentacaoAnterior = movimentacaoAnterior;
	}
	
	@Transient
	public String getObservacaoConciliacao() {
		return observacaoConciliacao;
	}
	public void setObservacaoConciliacao(String observacaoConciliacao) {
		this.observacaoConciliacao = observacaoConciliacao;
	}
	
	@Transient
	public Boolean getAssociada() {
		return associada;
	}
	public void setAssociada(Boolean associada) {
		this.associada = associada;
	}
	
	//Criado para a tela de Gerar Lan�amento Cont�bil
//	@OneToMany(mappedBy="movimentacaorelacionada")
//	public List<Movimentacaoorigem> getListaMovimentacaoorigemrelacionada() {
//		return listaMovimentacaoorigemrelacionada;
//	}
//	
//	public void setListaMovimentacaoorigemrelacionada(List<Movimentacaoorigem> listaMovimentacaoorigemrelacionada) {
//		this.listaMovimentacaoorigemrelacionada = listaMovimentacaoorigemrelacionada;
//	}
	
	//Criado para a tela de Gerar Lan�amento Cont�bil
	@OneToMany(mappedBy="movimentacaorelacionadacartaocredito")
	public List<Movimentacaoorigem> getListaMovimentacaoorigemcartaocredito() {
		return listaMovimentacaoorigemcartaocredito;
	}
	public void setListaMovimentacaoorigemcartaocredito(List<Movimentacaoorigem> listaMovimentacaoorigemcartaocredito) {
		this.listaMovimentacaoorigemcartaocredito = listaMovimentacaoorigemcartaocredito;
	}
	
	@MaxLength(10)
	@DisplayName("Border�")
	public String getBordero() {
		return bordero;
	}
	
	public void setBordero(String bordero) {
		this.bordero = bordero;
	}
	
	public Boolean getTaxa() {
		return taxa;
	}
	
	public void setTaxa(Boolean taxa) {
		this.taxa = taxa;
	}
	
	public Boolean getTaxageradaprocessamentocartao() {
		return taxageradaprocessamentocartao;
	}
	
	public void setTaxageradaprocessamentocartao(
			Boolean taxageradaprocessamentocartao) {
		this.taxageradaprocessamentocartao = taxageradaprocessamentocartao;
	}
	
	@Transient
	public Boolean getSelecao() {
		return selecao;
	}
	
	public void setSelecao(Boolean selecao) {
		this.selecao = selecao;
	}
	
	@Transient
	public boolean isPercentual() {
		return percentual;
	}
	public void setPercentual(boolean percentual) {
		this.percentual = percentual;
	}
	
	@Transient
	public Money getValorTaxa() {
		Money valorArquivoTaxa = new Money();
		
		if (getListaMovimentacaoorigem()!=null){
			for (Movimentacaoorigem movimentacaoorigem: getListaMovimentacaoorigem()){
				Movimentacao movimentacaorelacionada = movimentacaoorigem.getMovimentacaorelacionada();
				if (movimentacaorelacionada!=null 
						&& Tipooperacao.TIPO_DEBITO.equals(movimentacaorelacionada.getTipooperacao()) 
						&& !Movimentacaoacao.CANCELADA.equals(movimentacaorelacionada.getMovimentacaoacao())){
					valorArquivoTaxa = valorArquivoTaxa.add(movimentacaorelacionada.getValor());
				}
			}
		}
		
		return valorArquivoTaxa;
	}
	
	@Transient
	public Money getValorTaxaGeradaNoExtrato() {
		Money valorTaxa = new Money();
		
		if (getListaMovimentacaoorigem()!=null){
			for (Movimentacaoorigem movimentacaoorigem: getListaMovimentacaoorigem()){
				Movimentacao movimentacaorelacionada = movimentacaoorigem.getMovimentacaorelacionada();
				if (movimentacaorelacionada!=null 
						&& Tipooperacao.TIPO_DEBITO.equals(movimentacaorelacionada.getTipooperacao())
						&& Boolean.TRUE.equals(movimentacaorelacionada.getTaxageradaprocessamentocartao())
						&& !Movimentacaoacao.CANCELADA.equals(movimentacaorelacionada.getMovimentacaoacao())){
					valorTaxa = valorTaxa.add(movimentacaorelacionada.getValor());
				}
			}
		}
		
		return valorTaxa;
	}
	
	@Transient
	public Movimentacaohistorico getMovimentacaohistoricoTransient() {
		return movimentacaohistoricoTransient;
	}
	public void setMovimentacaohistoricoTransient(
			Movimentacaohistorico movimentacaohistoricoTransient) {
		this.movimentacaohistoricoTransient = movimentacaohistoricoTransient;
	}
	
	@Transient
	public String getWhereInCddocumento() {
		String whereInCddocumento = "";
		
		if (listaMovimentacaoorigem!=null && !listaMovimentacaoorigem.isEmpty()){
			for (Movimentacaoorigem movimentacaoorigem: listaMovimentacaoorigem){
				if (movimentacaoorigem.getDocumento()!=null && movimentacaoorigem.getDocumento().getCddocumento()!=null){
					whereInCddocumento += movimentacaoorigem.getDocumento().getCddocumento() + ",";
				}
			}
		}
		
		if (whereInCddocumento.endsWith(",")){
			whereInCddocumento = whereInCddocumento.substring(0, whereInCddocumento.length()-1);
		}
		
		return whereInCddocumento;
	}
	
	@Transient
	public boolean isTransferencia() {
		return transferencia;
	}
	
	public void setTransferencia(boolean transferencia) {
		this.transferencia = transferencia;
	}
	
	@Transient
	public boolean isCartaocredito(){
		if(Hibernate.isInitialized(getListaMovimentacaoorigem())){
			for(Movimentacaoorigem movimentacaoorigem : getListaMovimentacaoorigem()){
				if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getDocumentotipo() != null && 
						Boolean.TRUE.equals(movimentacaoorigem.getDocumento().getDocumentotipo().getCartao())){
					return true;
				}
			}
		}
		return false;
	}
	
	@OneToMany(mappedBy="movimentacaorelacionada")
	public List<Movimentacaoorigem> getMovimentacaoorigemTaxa() {
		return movimentacaoorigemTaxa;
	}
	public void setMovimentacaoorigemTaxa(List<Movimentacaoorigem> movimentacaoorigemTaxa) {
		this.movimentacaoorigemTaxa = movimentacaoorigemTaxa;
	}
}