package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dominioemailalias", sequenceName = "sq_dominioemailalias")
public class Dominioemailalias{
	
	protected Integer cddominioemailalias;
	protected Dominio dominio;
	protected String usuarioorigem;
	protected String emaildestino;
	
	// Transient
	protected String emailorigem;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominioemailalias")
	public Integer getCddominioemailalias() {
		return cddominioemailalias;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cddominio")
	public Dominio getDominio() {
		return dominio;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Usu�rio de origem")
	public String getUsuarioorigem() {
		return usuarioorigem;
	}

	@Required
	@MaxLength(150)
	@DisplayName("E-mail de destino")
	public String getEmaildestino() {
		return emaildestino;
	}

	public void setCddominioemailalias(Integer cddominioemailalias) {
		this.cddominioemailalias = cddominioemailalias;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setUsuarioorigem(String usuarioorigem) {
		this.usuarioorigem = usuarioorigem;
	}

	public void setEmaildestino(String emaildestino) {
		this.emaildestino = emaildestino;
	}
	
	@Transient
	@DisplayName("E-mail de origem")
	public String getEmailorigem() {
		if (getDominio() != null && getUsuarioorigem() != null)
			return getUsuarioorigem() + '@' + getDominio().getNome();
		else
			return null;
	}
	
	public void setEmailorigem(String emailorigem) {
		this.emailorigem = emailorigem;
	}
}
