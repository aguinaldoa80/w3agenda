package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_cargoriscointensidade", sequenceName = "sq_cargoriscointensidade")
@DisplayName("Intensidade do risco")
public class Cargoriscointensidade implements Log{

	protected Integer cdcargoriscointensidade;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargoriscointensidade")
	public Integer getCdcargoriscointensidade() {
		return cdcargoriscointensidade;
	}
	
	@Required
	@MaxLength(40)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCdcargoriscointensidade(Integer cdcargoriscointensidade) {
		this.cdcargoriscointensidade = cdcargoriscointensidade;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
