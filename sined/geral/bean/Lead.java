package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.util.Log;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@SequenceGenerator(name = "sq_lead", sequenceName = "sq_lead")
public class Lead implements Log{

	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdlead;
	protected String nome;
	protected String empresa;
	protected String website;
	protected String email;
	protected String logradouro;
	protected String numero;
	protected String complemento;
	protected String bairro;
	protected Cep cep;
	protected String observacao;
	protected Atividadetipo atividadetipotrans;	
	protected String site;
	protected String estrategiaMarketing;
	protected String forcas;
	protected String fraquezas;
	protected Sexo sexo;
	protected Uf uf;
	protected Municipio municipio;
	protected Tiporesponsavel tiporesponsavel;
	protected Fornecedor agencia;
	protected Colaborador responsavel;
	protected Cliente Cliente;
	
	protected Leadqualificacao leadqualificacao;
	protected Leadsituacao leadsituacao;
	protected List<Leadhistorico> listLeadhistorico = new ListSet<Leadhistorico>(Leadhistorico.class);
	protected List<Leadsegmento> listLeadsegmento;
	protected Set<Leadtelefone> listLeadtelefone = new ListSet<Leadtelefone>(Leadtelefone.class);
	protected Set<Leademail> listleademail;
	protected Set<Contacrm> listaContacrm;
	protected Set<CampoExtraLead> campoExtraLeadList = new ListSet<CampoExtraLead>(CampoExtraLead.class);
	//
	protected Set<Campanhalead> listLeadcampanha;
	protected Date dtretorno;	
	protected String proximopasso;
	protected Concorrente concorrente;
	
	protected Integer qtdehistorico;
	protected Timestamp dtultimohistorico;
	
	protected Date dtconversao;
	
	//Transients
	protected String listaTelefones;
	protected String listaEmails;
	protected String listaCampanhas;
	protected String telefones;
	protected String nomeempresa;
	protected String emails;
	protected String ultimoHistorico;
	protected String nomeComEmpresa;
	protected String situacaoQualificacao;
	protected String dtRetornoString;
	protected Boolean convertido;
	protected String totaisatividadetipo;
	protected List<Leadhistorico> listLeadhistoricoTotais;
	protected Integer ciclo;
	protected String whereIn;
	protected Campanha campanhaPopup;
	protected Colaborador colaboradorPopup;
	protected Boolean permitidoEdicao;
	protected Segmento segmento;
	private Integer cdconcorrente;
	
	public Lead() {
	}
	
	public Lead(Integer cdlead, String nome, String empresa, java.util.Date dtretorno, String proximopasso, java.util.Date dtultimohistorico, 
			Integer qtdehistorico, String email,
			Integer cdleadsituacao, String leadsituacaonome, 
			String responsavelnome, String agencianome, Tiporesponsavel tiporesponsavel, 
			String leadqualificacaonome, String responsavelnomeagencianome){
		this.cdlead = cdlead;
		this.empresa = empresa;
		this.dtretorno = dtretorno != null ? new Date(dtretorno.getTime()) : null;
		this.proximopasso = proximopasso;
		this.dtultimohistorico = dtultimohistorico != null ? new Timestamp(dtultimohistorico.getTime()) : null;
		this.qtdehistorico = qtdehistorico;
		this.email = email;
		
		if(cdleadsituacao != null){
			this.leadsituacao = new Leadsituacao();
			this.leadsituacao.setCdleadsituacao(cdleadsituacao);
			this.leadsituacao.setNome(leadqualificacaonome);
		}
		
		if(StringUtils.isNotEmpty(responsavelnome)){
			this.responsavel = new Colaborador(null, responsavelnome);
		}
		if(StringUtils.isNotEmpty(agencianome)){
			this.agencia = new Fornecedor(null, agencianome);
		}
		
		this.tiporesponsavel = tiporesponsavel; 
		
		if(StringUtils.isNotEmpty(leadqualificacaonome)){
			this.leadqualificacao = new Leadqualificacao();
			this.leadqualificacao.setNome(leadqualificacaonome);
		}
	}

	public Lead(Integer _cdlead) {
		this.cdlead = _cdlead;
	}
	
	public Lead(Integer cdlead, String nome) {
		this.cdlead = cdlead;
		this.nome = nome;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_lead")
	public Integer getCdlead() {
		return cdlead;
	}
	
	@Required	
	@MaxLength(80)
	@DescriptionProperty
	@DisplayName("Contato")
	public String getNome() {
		return nome;
	}

		
	@MaxLength(80)
	@DisplayName("Empresa")
	public String getEmpresa() {
		return empresa;
	}

		
	@MaxLength(80)
	@DisplayName("Website")
	public String getWebsite() {
		return website;
	}
	
	@MaxLength(100)
	@DisplayName("Email")
	public String getEmail() {
		return email;
	}

	@DisplayName("Sexo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")
	public Sexo getSexo() {
		return sexo;
	}

	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	@Required	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdleadsituacao")
	public Leadsituacao getLeadsituacao() {
		return leadsituacao;
	}
	
	@MaxLength(100)
	@DisplayName("Logradouro")
	public String getLogradouro() {
		return logradouro;
	}

	@MaxLength(5)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}

	@MaxLength(50)
	@DisplayName("Complemento")
	public String getComplemento() {
		return complemento;
	}

	@MaxLength(50)
	@DisplayName("Bairro")
	public String getBairro() {
		return bairro;
	}

	@MaxLength(50)
	@DisplayName("CEP")
	public Cep getCep() {
		return cep;
	}

	@Transient
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdmunicipio")
	@ValidationOverride(field="municipio.uf")
	public Municipio getMunicipio() {
		return municipio;
	}

	@Transient
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@Transient
	@DisplayName("Tipo de Atividade")
	public Atividadetipo getAtividadetipotrans() {
		return atividadetipotrans;
	}

	@Required	
	@DisplayName("Qualifica��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdleadqualificacao")
	public Leadqualificacao getLeadqualificacao() {
		return leadqualificacao;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="lead")
	public List<Leadhistorico> getListLeadhistorico() {
		return listLeadhistorico;
	}
	
	@DisplayName("Segmento")
	@OneToMany(mappedBy="lead")
	public List<Leadsegmento> getListLeadsegmento() {
		return listLeadsegmento;
	}
	
	@DisplayName("Telefone")
	@OneToMany(mappedBy="lead")
	public Set<Leadtelefone> getListLeadtelefone() {
		return listLeadtelefone;
	}
	
	@Email
	@DisplayName("E-mail")
	@OneToMany(mappedBy="lead")
	public Set<Leademail> getListleademail() {
		return listleademail;
	}
	
	@OneToMany(mappedBy="lead")
	public Set<Contacrm> getListaContacrm() {
		return listaContacrm;
	}
	@DisplayName("Campanha")
	@OneToMany(mappedBy="lead")
	public Set<Campanhalead> getListLeadcampanha() {
		return listLeadcampanha;
	}
	@DisplayName("Data de Retorno")	
	public Date getDtretorno() {
		return dtretorno;
	}
	@DisplayName("Pr�ximo Passo")
	@MaxLength(500)
	public String getProximopasso() {
		return proximopasso;
	}

	@DisplayName("�ltimo Hist�rico")
	public Timestamp getDtultimohistorico() {
		return dtultimohistorico;
	}
	
	@DisplayName("Hist�ricos")
	public Integer getQtdehistorico() {
		return qtdehistorico;
	}
	
	@Required
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}

	@ManyToOne
	@JoinColumn(name="cdagencia")
	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="lead")
	public Set<CampoExtraLead> getCampoExtraLeadList() {
		return campoExtraLeadList;
	}
	
	@DisplayName("Site")
	@MaxLength(100)
	public String getSite() {
		return site;
	}
	
	@DisplayName("Estrat�gia e Marketing")
	@MaxLength(2000)
	public String getEstrategiaMarketing() {
		return estrategiaMarketing;
	}
	
	@DisplayName("For�as")
	@MaxLength(2000)
	public String getForcas() {
		return forcas;
	}
	
	@DisplayName("Fraquezas")
	@MaxLength(2000)
	public String getFraquezas() {
		return fraquezas;
	}
	
	public Date getDtconversao() {
		return dtconversao;
	}
	
	public void setFraquezas(String fraquezas) {
		this.fraquezas = fraquezas;
	}
	
	public void setForcas(String forcas) {
		this.forcas = forcas;
	}
	
	public void setEstrategiaMarketing(String estrategiaMarketing) {
		this.estrategiaMarketing = estrategiaMarketing;
	}
	
	public void setSite(String site) {
		this.site = site;
	}
	
	public void setCampoExtraLeadList(Set<CampoExtraLead> campoExtraLeadList) {
		this.campoExtraLeadList = campoExtraLeadList;
	}

	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}

	public void setDtretorno(Date dtretorno) {
		this.dtretorno = dtretorno;
	}

	public void setProximopasso(String proximopasso) {
		this.proximopasso = proximopasso;
	}

	public void setListLeadcampanha(Set<Campanhalead> listLeadcampanha) {
		this.listLeadcampanha = listLeadcampanha;
	}
	
	
	public void setListaContacrm(Set<Contacrm> listaContacrm) {
		this.listaContacrm = listaContacrm;
	}
	public void setCdlead(Integer cdlead) {
		this.cdlead = cdlead;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	
	public void setLeadsituacao(Leadsituacao leadsituacao) {
		this.leadsituacao = leadsituacao;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(Cep cep) {
		this.cep = cep;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setAtividadetipotrans(Atividadetipo atividadetipotrans) {
		this.atividadetipotrans = atividadetipotrans;
	}
	
	public void setLeadqualificacao(Leadqualificacao leadqualificacao) {
		this.leadqualificacao = leadqualificacao;
	}
	
	public void setListLeadhistorico(List<Leadhistorico> listLeadhistorico) {
		this.listLeadhistorico = listLeadhistorico;
	}
	
	public void setListLeadsegmento(List<Leadsegmento> listLeadsegmento) {
		this.listLeadsegmento = listLeadsegmento;
	}
	
	public void setListLeadtelefone(Set<Leadtelefone> listLeadtelefone) {
		this.listLeadtelefone = listLeadtelefone;
	}
	
	public void setListleademail(Set<Leademail> listleademail) {
		this.listleademail = listleademail;
	}
	
	public void setDtultimohistorico(Timestamp dtultimohistorico) {
		this.dtultimohistorico = dtultimohistorico;
	}
	
	public void setQtdehistorico(Integer qtdehistorico) {
		this.qtdehistorico = qtdehistorico;
	}
	
	public void setDtconversao(Date dtconversao) {
		this.dtconversao = dtconversao;
	}
	
	@Transient
	@DisplayName("Telefones")
	public String getListaTelefones() {
		return listaTelefones;
	}
	@Transient
	@DisplayName("E-mails")
	public String getListaEmails() {
		return listaEmails;
	}
	@Transient
	@DisplayName("Campanhas")
	public String getListaCampanhas() {
		return listaCampanhas;
	}
	@Transient
	@DisplayName("Telefones")
	public String getTelefones() {
		return telefones;
	}
	@Transient
	@DisplayName("Empresa")
	public String getNomeempresa() {
		return nomeempresa;
	}
	@Transient
	@DisplayName("E-mails")
	public String getEmails() {
		return emails;
	}
	
	@Transient
	@DisplayName("E-mails")
	public String getEmailsCSV(){
		StringBuilder sb = new StringBuilder();
		if (listleademail != null) {
			for (Leademail leademail : listleademail){
				if (sb.length() > 0)
					sb.append(", ");
				sb.append(leademail.email);
			}
		}
		return sb.toString();
	}
	
	@Transient
	@DisplayName("Telefones")
	public String getTelefonesCSV(){
		StringBuilder sb = new StringBuilder();
		if (listLeadtelefone != null) {
			for (Leadtelefone leadtelefone : listLeadtelefone){
				if (sb.length() > 0)
					sb.append(", ");
				sb.append(leadtelefone.telefone);
			}
		}
		return sb.toString();
	}
	
	@Transient
	public Segmento getSegmento() {
		return segmento;
	}
	
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	
	public void setListaTelefones(String listaTelefones) {
		this.listaTelefones = listaTelefones;
	}
	public void setListaEmails(String listaEmails) {
		this.listaEmails = listaEmails;
	}
	public void setListaCampanhas(String listaCampanhas) {
		this.listaCampanhas = listaCampanhas;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public void setNomeempresa(String nomeempresa) {
		this.nomeempresa = nomeempresa;
	}
	public void setEmails(String emails) {
		this.emails = emails;
	}
	
	
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getUltimoHistorico() {
		StringBuilder historico = new StringBuilder("");
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy hh:mm"); 
		if(this.getListLeadhistorico() != null && this.getListLeadhistorico().size() > 0){
			Leadhistorico leadhistorico = this.getListLeadhistorico().get(this.getListLeadhistorico().size() - 1);
			if( leadhistorico != null ){
				if(leadhistorico.getDtaltera() != null){
					historico.append(data.format(leadhistorico.getDtaltera())).append(" / ");
				}
				historico.append(leadhistorico.getResponsavel() != null ? leadhistorico.getResponsavel() + " / " : "");
				historico.append(leadhistorico.getObservacao() != null ? leadhistorico.getObservacao() : "");
			}
		}
		return historico.toString();
	}

	public void setUltimoHistorico(String ultimoHistorico) {
		this.ultimoHistorico = ultimoHistorico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdlead == null) ? 0 : cdlead.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Lead other = (Lead) obj;
		if (cdlead == null) {
			if (other.cdlead != null)
				return false;
		} else if (!cdlead.equals(other.cdlead))
			return false;
		return true;
	}

	@Transient
	@DisplayName("Nome/Empresa")
	public String getNomeComEmpresa() {
		nomeComEmpresa = getNome() + (getEmpresa() != null && !"".equals(getEmpresa()) ? "/ " + getEmpresa() : "");
		return nomeComEmpresa;
	}

	public void setNomeComEmpresa(String nomeComEmpresa) {
		this.nomeComEmpresa = nomeComEmpresa;
	}

	@Transient
	@DisplayName("Situa��o/Qualifica��o")
	public String getSituacaoQualificacao() {
		situacaoQualificacao = getLeadsituacao().getNome() + " / " + getLeadqualificacao().getNome();
		return situacaoQualificacao;
	}

	public void setSituacaoQualificacao(String situacaoQualificacao) {
		this.situacaoQualificacao = situacaoQualificacao;
	}

	@Transient
	public String getDtRetornoString() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if(dtretorno != null)
		dtRetornoString = format.format(dtretorno);
		return dtRetornoString;
	}

	public void setDtRetornoString(String dtRetornoString) {
		this.dtRetornoString = dtRetornoString;
	}
	
	@Transient
	public Boolean getConvertido() {
		return convertido;
	}
	
	public void setConvertido(Boolean convertido) {
		this.convertido = convertido;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconcorrente")
	public Concorrente getConcorrente() {
		return concorrente;
	}

	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
	@Transient
	@DisplayName("Segmentos")
	public String getSegmentosCSV(){
		StringBuilder sb = new StringBuilder();
		if (listLeadsegmento != null) {
			for (Leadsegmento leadsegmento : listLeadsegmento){
				if (sb.length() > 0)
					sb.append(", ");
				sb.append(leadsegmento.segmento.nome);
			}
		}
		return sb.toString();
	}

	@Transient
	public String getTotaisatividadetipo() {
		StringBuilder s = new StringBuilder();
		if(listLeadhistoricoTotais != null && !listLeadhistoricoTotais.isEmpty()){
			for(Leadhistorico lh : listLeadhistoricoTotais){
				if(lh.getAtividadenome() != null && !"".equals(lh.getAtividadenome())){
					s.append(!"".equals(s.toString()) ? "\n" : "");
					s.append(lh.getAtividadenome() + " - " + (lh.getQtdetotal() != null ? lh.getQtdetotal().intValue() : "0"));
				}
			}
			totaisatividadetipo = s.toString();
		}
		return totaisatividadetipo;
	}
	public void setTotaisatividadetipo(String totaisatividadetipo) {
		this.totaisatividadetipo = totaisatividadetipo;
	}
	@Transient
	public List<Leadhistorico> getListLeadhistoricoTotais() {
		return listLeadhistoricoTotais;
	}
	public void setListLeadhistoricoTotais(List<Leadhistorico> listLeadhistoricoTotais) {
		this.listLeadhistoricoTotais = listLeadhistoricoTotais;
	}
	
	@Transient
	@DisplayName("Ciclo")
	public Integer getCiclo() {
		return ciclo;
	}
	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}
	
	@Transient
	@DisplayName("Empresa/Contato")
	public String getEmpresaNome(){
		
		String retorno = "";
		
		if (getEmpresa()!=null){
			retorno = getEmpresa() + "/"; 
		}
		
		retorno += getNome();
		
		return retorno;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@Required
	@Transient
	@DisplayName("Campanha")
	public Campanha getCampanhaPopup() {
		return campanhaPopup;
	}
	
	@Required
	@Transient
	@DisplayName("Respons�vel")
	public Colaborador getColaboradorPopup() {
		return colaboradorPopup;
	}

	public void setCampanhaPopup(Campanha campanhaPopup) {
		this.campanhaPopup = campanhaPopup;
	}

	public void setColaboradorPopup(Colaborador colaboradorPopup) {
		this.colaboradorPopup = colaboradorPopup;
	}
	
	@Transient
	public String getEnderecoCompleto(){
		String enderecoCompleto =  this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		enderecoCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		enderecoCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		enderecoCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		enderecoCompleto += this.municipio != null ? ", " + this.municipio.getNome() : "";
		enderecoCompleto += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		return enderecoCompleto;
	}
	
	@Transient
	public Boolean getPermitidoEdicao() {
		return permitidoEdicao;
	}
	public void setPermitidoEdicao(Boolean permitidoEdicao) {
		this.permitidoEdicao = permitidoEdicao;
	}
	
	@Transient
	public Integer getCdconcorrente() {
		return cdconcorrente;
	}
	
	public void setCdconcorrente(Integer cdconcorrente) {
		this.cdconcorrente = cdconcorrente;
	}
	
}