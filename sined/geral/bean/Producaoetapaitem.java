package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_producaoetapaitem", sequenceName = "sq_producaoetapaitem")
@DisplayName("Etapas")
public class Producaoetapaitem {
	
	protected Integer cdproducaoetapaitem;
	protected Producaoetapa producaoetapa;
	protected Producaoetapanome producaoetapanome;
	protected Integer ordem;
	protected Departamento departamento;
	protected Integer qtdediasprevisto;
	protected Money percentualComissao;
	
	public Producaoetapaitem() {
	}
	
	public Producaoetapaitem(Integer cdproducaoetapaitem) {
		this.cdproducaoetapaitem = cdproducaoetapaitem;
	}
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoetapaitem")
	public Integer getCdproducaoetapaitem() {
		return cdproducaoetapaitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapa")
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}
	@Required
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdproducaoetapanome")
	public Producaoetapanome getProducaoetapanome() {
		return producaoetapanome;
	}
	public Integer getOrdem() {
		return ordem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	@DisplayName("Qtde Dias Previstos")
	public Integer getQtdediasprevisto() {
		return qtdediasprevisto;
	}
	@DisplayName("Percentual de Comiss�o")
	public Money getPercentualComissao() {
		return percentualComissao;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	public void setProducaoetapanome(Producaoetapanome producaoetapanome) {
		this.producaoetapanome = producaoetapanome;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}	

	public void setQtdediasprevisto(Integer qtdediasprevisto) {
		this.qtdediasprevisto = qtdediasprevisto;
	}
	public void setPercentualComissao(Money percentualComissao) {
		this.percentualComissao = percentualComissao;
	}
	public void setCdproducaoetapaitem(Integer cdproducaoetapaitem) {
		this.cdproducaoetapaitem = cdproducaoetapaitem;
	}
	
	@Transient
	@DescriptionProperty
	public String getDescricaoCombo(){
		return producaoetapanome != null && Hibernate.isInitialized(producaoetapanome) ? producaoetapanome.getNome() : "";
	}
}
