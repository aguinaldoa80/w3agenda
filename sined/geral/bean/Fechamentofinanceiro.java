package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@DisplayName("Fechamento Financeiro")
@SequenceGenerator(name="sq_fechamentofinanceiro", sequenceName="sq_fechamentofinanceiro")
public class Fechamentofinanceiro implements Log{
	
	protected Integer cdfechamentofinanceiro;
	protected Empresa empresa;
	protected Date datainicio = SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), -30);
	protected Date datalimite = SinedDateUtils.currentDate();
	protected String motivo;
	protected Contatipo contatipo;
	protected Conta conta;
	protected Boolean ativo = Boolean.TRUE;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<FechamentoFinanceiroHistorico> listaFechamentoFinanceiroHistorico = new ListSet<FechamentoFinanceiroHistorico>(FechamentoFinanceiroHistorico.class);
	
	//Transient
	protected Pessoa responsavel;
	protected Date proxDatalimite;
	protected String historico;
	
	@Id
	@Required
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fechamentofinanceiro")
	public Integer getCdfechamentofinanceiro() {
		return cdfechamentofinanceiro;
	}
	public void setCdfechamentofinanceiro(Integer cdfechamentofinanceiro) {
		this.cdfechamentofinanceiro = cdfechamentofinanceiro;
	}
	
	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	@JoinColumn(name="cdconta")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getConta() {
		return conta;
	}
	@JoinColumn(name="cdcontatipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contatipo getContatipo() {
		return contatipo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Required
	@DisplayName("Data Fim")
	public Date getDatalimite() {
		return datalimite;
	}
	@Required
	@DisplayName("Data Início")
	public Date getDatainicio() {
		return datainicio;
	}
	public void setDatainicio(Date datainicio) {
		this.datainicio = datainicio;
	}
	public void setDatalimite(Date datalimite) {
		this.datalimite = datalimite;
	}
	
	@MaxLength(1000)
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@DisplayName("Data do Fechamento")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
	//Transient
	
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Responsável")
	public Pessoa getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Pessoa responsavel) {
		this.responsavel = responsavel;
	}
	
	@Transient
	public Date getProxDatalimite() {
		return proxDatalimite;
	}
	public void setProxDatalimite(Date proxDatalimite) {
		this.proxDatalimite = proxDatalimite;
	}
	
	@DisplayName("Histórico")
	@OneToMany(mappedBy="fechamentofinanceiro")
	public List<FechamentoFinanceiroHistorico> getListaFechamentoFinanceiroHistorico() {
		return listaFechamentoFinanceiroHistorico;
	}
	public void setListaFechamentoFinanceiroHistorico(List<FechamentoFinanceiroHistorico> listaFechamentoFinanceiroHistorico) {
		this.listaFechamentoFinanceiroHistorico = listaFechamentoFinanceiroHistorico;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Transient
	@DisplayName("Histórico")
	public String getHistorico() {
		return historico;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
}
