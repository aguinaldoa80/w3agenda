package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivobancario", sequenceName = "sq_arquivobancario")
public class Arquivobancario implements Log {

	protected Integer cdarquivobancario;
	protected String nome;
	protected Arquivobancariotipo arquivobancariotipo;
	protected Arquivo arquivo;
	protected Date dtgeracao;
	protected Date dtprocessa;
	protected Arquivobancario arquivobancariocorrespondente;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Date dtcancelamento;
	protected Arquivobancariosituacao arquivobancariosituacao;
	protected Set<Arquivobancariodocumento> listaArqBancarioDoc;
	protected String sequencial;
	
//	TRANSIENTES
	protected Date data;
	private String clientes;
	
	public Arquivobancario() {}	
	
	public Arquivobancario(String nome, Arquivobancariotipo arquivobancariotipo, Arquivo arquivo, Date dtgeracao, Arquivobancariosituacao arquivobancariosituacao) {
		this.nome = nome;
		this.arquivobancariotipo = arquivobancariotipo;
		this.arquivo = arquivo;
		this.dtgeracao = dtgeracao;
		this.arquivobancariosituacao = arquivobancariosituacao;
	}
	
	public Arquivobancario(String nome, Arquivobancariotipo arquivobancariotipo, Arquivo arquivo, Date dtprocessa, Arquivobancariosituacao arquivobancariosituacao, Arquivobancario arquivobancariocorrespondente) {
		this.nome = nome;
		this.arquivobancariotipo = arquivobancariotipo;
		this.arquivo = arquivo;
		this.dtprocessa = dtprocessa;
		this.arquivobancariosituacao = arquivobancariosituacao;
		this.arquivobancariocorrespondente = arquivobancariocorrespondente;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivobancario")
	public Integer getCdarquivobancario() {
		return cdarquivobancario;
	}
	
	@MaxLength(100)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivobancariotipo")
	public Arquivobancariotipo getArquivobancariotipo() {
		return arquivobancariotipo;
	}
	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivobancariosituacao")
	public Arquivobancariosituacao getArquivobancariosituacao() {
		return arquivobancariosituacao;
	}
	
	@DisplayName("Arquivo remessa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Data de gera��o")
	public Date getDtgeracao() {
		return dtgeracao;
	}
	
	@DisplayName("Data de processamento")
	public Date getDtprocessa() {
		return dtprocessa;
	}
	
	@DisplayName("Arquivo correspondente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivocorrespondente")
	public Arquivobancario getArquivobancariocorrespondente() {
		return arquivobancariocorrespondente;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public java.sql.Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	
	@OneToMany(mappedBy="arquivobancario")
	public Set<Arquivobancariodocumento> getListaArqBancarioDoc() {
		return listaArqBancarioDoc;
	}
	@DisplayName("Sequencial do Arquivo")
	public String getSequencial() {
		return sequencial;
	}
	
	public void setListaArqBancarioDoc(
			Set<Arquivobancariodocumento> listaArqBancarioDoc) {
		this.listaArqBancarioDoc = listaArqBancarioDoc;
	}
	
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	
	public void setArquivobancariosituacao(
			Arquivobancariosituacao arquivobancariosituacao) {
		this.arquivobancariosituacao = arquivobancariosituacao;
	}
	
	public void setCdarquivobancario(Integer cdarquivobancario) {
		this.cdarquivobancario = cdarquivobancario;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setArquivobancariotipo(Arquivobancariotipo arquivobancariotipo) {
		this.arquivobancariotipo = arquivobancariotipo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDtgeracao(Date dtgeracao) {
		this.dtgeracao = dtgeracao;
	}
	public void setDtprocessa(Date dtprocessa) {
		this.dtprocessa = dtprocessa;
	}
	public void setArquivobancariocorrespondente(
			Arquivobancario arquivobancariocorrespondente) {
		this.arquivobancariocorrespondente = arquivobancariocorrespondente;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(java.sql.Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setSequencial(String sequencial) {
		this.sequencial = sequencial;
	}
//	TRANSIENTES
	
	@Transient
	@DisplayName("Data")
	public Date getData() {
		if (arquivobancariotipo.equals(Arquivobancariotipo.REMESSA)) {
			return getDtgeracao();
		} else if (arquivobancariotipo.equals(Arquivobancariotipo.RETORNO)) {
			return getDtprocessa();
		} else return null;
	}
	
	public void setData(Date data) {
		this.data = data;
	}

	@Transient
	@DisplayName("Clientes")
	public String getClientes() {
		return clientes;
	}
	
	public void setClientes(String clientes) {
		this.clientes = clientes;
	}	
}
