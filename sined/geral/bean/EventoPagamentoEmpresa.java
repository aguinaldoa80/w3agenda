package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_eventopagamentoempresa", sequenceName="sq_eventopagamentoempresa")
public class EventoPagamentoEmpresa {

	private Integer cdEventoPagamentoEmpresa;
	private EventoPagamento evento;
	private Empresa empresa;
	private String codigoIntegracao;
	private ContaContabil contaContabil;	
	
	@Id
	@GeneratedValue(generator="sq_eventopagamentoempresa", strategy=GenerationType.AUTO)
	public Integer getCdEventoPagamentoEmpresa() {
		return cdEventoPagamentoEmpresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEvento() {
		return evento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("C�digo de integra��o")
	public String getCodigoIntegracao() {
		return codigoIntegracao;
	}
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	
	public void setCdEventoPagamentoEmpresa(Integer cdEventoPagamentoEmpresa) {
		this.cdEventoPagamentoEmpresa = cdEventoPagamentoEmpresa;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCodigoIntegracao(String codigoIntegracao) {
		this.codigoIntegracao = codigoIntegracao;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
}
