package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.NotafiscalprodutoreferenciaModeloEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tiponotareferencia;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutoreferenciada",sequenceName="sq_notafiscalprodutoreferenciada")
public class Notafiscalprodutoreferenciada {
	
	protected Integer cdnotafiscalprodutoreferenciada;
	protected Notafiscalproduto notafiscalproduto;
	protected Tiponotareferencia tiponotareferencia;
	protected String chaveacesso;
	protected Uf uf;
	protected String mesanoemissao;
	protected Tipopessoa tipopessoa;
	protected Cnpj cnpj;
	protected Cpf cpf;
	protected String ie;
//	protected String modelo;
	protected NotafiscalprodutoreferenciaModeloEnum modeloNFe;
	protected String serie;
	protected String numero;
	
//	  "chaveacesso" VARCHAR(44), 
//	  "mesanoemissao" VARCHAR(7), 
//	  "cnpj" VARCHAR(14), 
//	  "modelo" VARCHAR(2), 
//	  "serie" VARCHAR(3), 
//	  "numero" VARCHAR(9), 
//	  "cpf" VARCHAR(11), 
//	  "ie" VARCHAR(14), 
	
	//Transient
	protected NotafiscalprodutoreferenciaModeloEnum modeloNFeAux;

	@Id
	@GeneratedValue(generator="sq_notafiscalprodutoreferenciada", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutoreferenciada() {
		return cdnotafiscalprodutoreferenciada;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	@MaxLength(44)
	public String getChaveacesso() {
		return chaveacesso;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduf")
	public Uf getUf() {
		return uf;
	}

	public String getMesanoemissao() {
		return mesanoemissao;
	}

	public Cnpj getCnpj() {
		return cnpj;
	}

	public Cpf getCpf() {
		return cpf;
	}

	@MaxLength(14)
	public String getIe() {
		return ie;
	}

//	@MaxLength(2)
//	public String getModelo() {
//		return modelo;
//	}
	
	public NotafiscalprodutoreferenciaModeloEnum getModeloNFe() {
		return modeloNFe;
	}

	@MaxLength(3)
	public String getSerie() {
		return serie;
	}

	@MaxLength(9)
	public String getNumero() {
		return numero;
	}
	
	@Required
	@DisplayName("Tipo de documento referenciado")
	public Tiponotareferencia getTiponotareferencia() {
		return tiponotareferencia;
	}
	
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}
	
	@Transient
	public NotafiscalprodutoreferenciaModeloEnum getModeloNFeAux() {
		return this.getModeloNFe();
	}

	public void setModeloNFeAux(NotafiscalprodutoreferenciaModeloEnum modeloNFeAux) {
		this.modeloNFeAux = modeloNFeAux;
	}
	
	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	
	public void setTiponotareferencia(Tiponotareferencia tiponotareferencia) {
		this.tiponotareferencia = tiponotareferencia;
	}

	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMesanoemissao(String mesanoemissao) {
		this.mesanoemissao = mesanoemissao;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

//	public void setModelo(String modelo) {
//		this.modelo = modelo;
//	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setCdnotafiscalprodutoreferenciada(Integer cdnotafiscalprodutoreferenciada) {
		this.cdnotafiscalprodutoreferenciada = cdnotafiscalprodutoreferenciada;
	}

	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}

	public void setModeloNFe(NotafiscalprodutoreferenciaModeloEnum modeloNFe) {
		this.modeloNFe = modeloNFe;
	}

		
}