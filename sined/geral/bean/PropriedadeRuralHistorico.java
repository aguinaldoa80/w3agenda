package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.TipoExploracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTerceiros;

@Entity
@SequenceGenerator(name="sq_propriedaderuralhistorico", sequenceName="sq_propriedaderuralhistorico")
public class PropriedadeRuralHistorico {
	protected Integer cdPropriedadeRuralHistorico;
	protected PropriedadeRural propriedadeRural;
	protected Usuario usuarioaltera;
	protected Timestamp dtaltera;
	protected String acao;
	protected String observacao;
	
	//CAMPOS DA PROPRIEDADE RURAL
	protected Integer codigoImovel;
	protected String nome;
	protected String cafir;
	protected String caepf;
	protected Empresa empresa;
	protected TipoExploracao tipoExploracao;
	protected TipoTerceiros tipoTerceiros;
	protected String participantesPercentuais;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator="sq_propriedaderuralhistorico")
	public Integer getCdPropriedadeRuralHistorico() {
		return cdPropriedadeRuralHistorico;
	}
	
	@JoinColumn(name="cdPropriedadeRural")
	@ManyToOne(fetch=FetchType.LAZY)
	public PropriedadeRural getPropriedadeRural() {
		return propriedadeRural;
	}
	
	@DisplayName("Usu�rio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setCdPropriedadeRuralHistorico(Integer cdPropriedadeRuralHistorico) {
		this.cdPropriedadeRuralHistorico = cdPropriedadeRuralHistorico;
	}
	public void setPropriedadeRural(PropriedadeRural propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	//geters e setters da propriedade rural
	
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	public String getNome() {
		return nome;
	}
	public String getCafir() {
		return cafir;
	}
	public String getCaepf() {
		return caepf;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public TipoExploracao getTipoExploracao() {
		return tipoExploracao;
	}
	public TipoTerceiros getTipoTerceiros() {
		return tipoTerceiros;
	}
	public String getParticipantesPercentuais() {
		return participantesPercentuais;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCafir(String cafir) {
		this.cafir = cafir;
	}
	public void setCaepf(String caepf) {
		this.caepf = caepf;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoExploracao(TipoExploracao tipoExploracao) {
		this.tipoExploracao = tipoExploracao;
	}
	public void setTipoTerceiros(TipoTerceiros tipoTerceiros) {
		this.tipoTerceiros = tipoTerceiros;
	}
	public void setParticipantesPercentuais(String participantesPercentuais) {
		this.participantesPercentuais = participantesPercentuais;
	}	
	
}
