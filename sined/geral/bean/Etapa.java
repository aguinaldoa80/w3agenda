package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_etapa", sequenceName = "sq_etapa")
public class Etapa {

	protected Integer cdetapa;
	protected Integer ordem;
	protected String descricao;
	protected List<Autorizacaotrabalho> listaAutorizacaoTrabalho;
	
	@OneToMany(mappedBy="etapa")
	public List<Autorizacaotrabalho> getListaAutorizacaoTrabalho() {
		return listaAutorizacaoTrabalho;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_etapa")
	public Integer getCdetapa() {
		return cdetapa;
	}
	public void setCdetapa(Integer id) {
		this.cdetapa = id;
	}

	@Required
	@DisplayName("Ordem")
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	@MaxLength(40)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setListaAutorizacaoTrabalho(
			List<Autorizacaotrabalho> listaAutorizacaoTrabalho) {
		this.listaAutorizacaoTrabalho = listaAutorizacaoTrabalho;
	}
	
}
