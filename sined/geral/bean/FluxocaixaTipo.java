package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_fluxocaixatipo",sequenceName="sq_fluxocaixatipo")
@DisplayName("Tipo de fluxo de caixa")
public class FluxocaixaTipo implements Log{

	protected Integer cdfluxocaixatipo;
	protected String nome;
	protected String sigla;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public FluxocaixaTipo(){
	}
	
	public FluxocaixaTipo(Integer cdfluxocaixatipo){
		this.cdfluxocaixatipo = cdfluxocaixatipo;
	}
	
	public FluxocaixaTipo(String nome, String sigla){
		this.nome = nome;
		this.sigla = sigla;
	}

	public FluxocaixaTipo(Integer cdfluxocaixatipo, String nome, String sigla){
		this.cdfluxocaixatipo = cdfluxocaixatipo;
		this.nome = nome;
		this.sigla = sigla;
	}
	
	@Id
	@GeneratedValue(generator="sq_fluxocaixatipo", strategy=GenerationType.AUTO)
	public Integer getCdfluxocaixatipo() {
		return cdfluxocaixatipo;
	}
	@DisplayName("Descri��o")
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(2)
	public String getSigla() {
		return sigla;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdfluxocaixatipo(Integer cdfluxocaixatipo) {
		this.cdfluxocaixatipo = cdfluxocaixatipo;
	}
	public void setNome(String descricao) {
		this.nome = StringUtils.trimToNull(descricao);
	}
	public void setSigla(String sigla) {
		this.sigla = StringUtils.trimToNull(sigla);
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
}
