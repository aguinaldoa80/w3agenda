package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Solicitacaoservicosituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_solicitacaoservicohistorico",sequenceName="sq_solicitacaoservicohistorico")
public class Solicitacaoservicohistorico implements Log{
	
	protected Integer cdsolicitacaoservicohistorico;
	protected Solicitacaoservico solicitacaoservico;
	protected String observacao;
	protected Solicitacaoservicosituacao solicitacaoservicosituacao;
	
	//LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTES
	protected String whereIn;
	
	@Id
	@DisplayName("Solicita��o")
	@GeneratedValue(generator="sq_solicitacaoservicohistorico",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservicohistorico() {
		return cdsolicitacaoservicohistorico;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservico")
	public Solicitacaoservico getSolicitacaoservico() {
		return solicitacaoservico;
	}

	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("A��o")
	@Column(name="situacao")
	public Solicitacaoservicosituacao getSolicitacaoservicosituacao() {
		return solicitacaoservicosituacao;
	}

	public void setCdsolicitacaoservicohistorico(
			Integer cdsolicitacaoservicohistorico) {
		this.cdsolicitacaoservicohistorico = cdsolicitacaoservicohistorico;
	}

	public void setSolicitacaoservico(Solicitacaoservico solicitacaoservico) {
		this.solicitacaoservico = solicitacaoservico;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setSolicitacaoservicosituacao(
			Solicitacaoservicosituacao solicitacaoservicosituacao) {
		this.solicitacaoservicosituacao = solicitacaoservicosituacao;
	}
	
	
	//LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	//TRANSIENTES
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return TagFunctions.findUserByCd(getCdusuarioaltera());
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

}
