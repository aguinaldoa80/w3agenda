package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_codobrigacaoicmsrecolher", sequenceName = "sq_codobrigacaoicmsrecolher")
public class Codobrigacaoicmsrecolher {

	protected Integer cdcodobrigacaoicmsrecolher;
	protected String codigo;
	protected String nome;
	protected Date data;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_codobrigacaoicmsrecolher")
	public Integer getCdcodobrigacaoicmsrecolher() {
		return cdcodobrigacaoicmsrecolher;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getNome() {
		return nome;
	}
	public Date getData() {
		return data;
	}
	public void setCdcodobrigacaoicmsrecolher(Integer cdcodobrigacaoicmsrecolher) {
		this.cdcodobrigacaoicmsrecolher = cdcodobrigacaoicmsrecolher;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setData(Date data) {
		this.data = data;
	}
	
	@Transient
	@DescriptionProperty
	public String getDescricaoCombo(){
		String s = "";
		
		if(StringUtils.isNotEmpty(getCodigo()))
			s = getCodigo(); 
		if(StringUtils.isNotEmpty(getNome()))
			s += " - " + getNome();
		if(getData() != null)
			s += " - " + new SimpleDateFormat("dd/MM/yyyy").format(getData());
		return s;
	}
}
