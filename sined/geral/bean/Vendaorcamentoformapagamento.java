package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_vendaorcamentoformapagamento", sequenceName = "sq_vendaorcamentoformapagamento")
public class Vendaorcamentoformapagamento {

	protected Integer cdvendaorcamentoformapagamento;
	protected Vendaorcamento vendaorcamento;
	protected Money valorparcela;
	protected Money valortotal;
	protected Date dtparcela;
	protected Prazopagamento prazopagamento;
	protected Boolean prazomedio;
	protected Integer qtdeparcelas;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendaorcamentoformapagamento")
	public Integer getCdvendaorcamentoformapagamento() {
		return cdvendaorcamentoformapagamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}

	@DisplayName("Condi��o de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	@Required
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@DisplayName("Valor da parcela")
	public Money getValorparcela() {
		return valorparcela;
	}

	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}
	
	@DisplayName("Prazo m�dio")
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	
	@DisplayName("Qtde. de parcelas")
	public Integer getQtdeparcelas() {
		return qtdeparcelas;
	}
	
	@DisplayName("Data da 1� parcela")
	public Date getDtparcela() {
		return dtparcela;
	}
	
	public void setDtparcela(Date dtparcela) {
		this.dtparcela = dtparcela;
	}
	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public void setQtdeparcelas(Integer qtdeparcelas) {
		this.qtdeparcelas = qtdeparcelas;
	}
	public void setValorparcela(Money valorparcela) {
		this.valorparcela = valorparcela;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setCdvendaorcamentoformapagamento(
			Integer cdvendaorcamentoformapagamento) {
		this.cdvendaorcamentoformapagamento = cdvendaorcamentoformapagamento;
	}
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"prazopagamento"})
	public String getDescricaoCombo(){
		if(this.prazopagamento != null){
			return this.prazopagamento.getNome();
		}
		return "<SEM PRAZO DE PAGAMENTO>";
	}
	
}
