package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorpropriedade;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopessoainventario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Item de inventário")
@SequenceGenerator(name = "sq_inventariomaterial", sequenceName = "sq_inventariomaterial")
public class Inventariomaterial implements Log, ValidateLoteestoqueObrigatorioInterface {

	protected Integer cdinventariomaterial;
	protected Inventario inventario;
	protected Material material;
	protected Double qtde;
	protected Double qtdeOrigem;
	protected Double valorUnitario;
	protected Indicadorpropriedade indicadorpropriedade;
	protected Tipopessoainventario tipopessoainventario;
	protected Pessoa pessoa;
	protected String contaanaliticacontabil; 
	protected Loteestoque loteestoque;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean gerarBlocoK;
	protected Boolean gerarBlocoH;
	protected List<InventarioMaterialHistorico> listaInventarioMaterialHistorico = new ListSet<InventarioMaterialHistorico>(InventarioMaterialHistorico.class);;
	

	
	//TRANSIENT
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Boolean hasAjuste = Boolean.FALSE;
	
	public Inventariomaterial(){}
	
	public Inventariomaterial(Material material, Loteestoque loteestoque,
			Double valorUnitario, Double qtde) {
		this.material = material;
		this.loteestoque = loteestoque;
		this.valorUnitario = valorUnitario;
		this.qtde = qtde;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inventariomaterial")
	public Integer getCdinventariomaterial() {
		return cdinventariomaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinventario")	
	public Inventario getInventario() {
		return inventario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Quantidade")
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("Valor Unitário")
	public Double getValorUnitario() {
		return valorUnitario;
	}
	@DisplayName("Indicador de Propriedade")
	public Indicadorpropriedade getIndicadorpropriedade() {
		return indicadorpropriedade;
	}
	@DisplayName("Pessoa")
	public Tipopessoainventario getTipopessoainventario() {
		return tipopessoainventario;
	}
	@DisplayName("Cliente/Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	@DisplayName("Conta analítica contábil")
	public String getContaanaliticacontabil() {
		return contaanaliticacontabil;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public Double getQtdeOrigem() {
		return qtdeOrigem;
	}
	public void setQtdeOrigem(Double qtdeOrigem) {
		this.qtdeOrigem = qtdeOrigem;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdinventariomaterial(Integer cdinventariomaterial) {
		this.cdinventariomaterial = cdinventariomaterial;
	}
	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setValorUnitario(Double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}
	public void setIndicadorpropriedade(Indicadorpropriedade indicadorpropriedade) {
		this.indicadorpropriedade = indicadorpropriedade;
	}
	public void setTipopessoainventario(Tipopessoainventario tipopessoainventario) {
		this.tipopessoainventario = tipopessoainventario;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setContaanaliticacontabil(String contaanaliticacontabil) {
		this.contaanaliticacontabil = contaanaliticacontabil;
	}

	@OneToMany(mappedBy="inventarioMaterial")
	@DisplayName("Histórico")
	public List<InventarioMaterialHistorico> getListaInventarioMaterialHistorico() {
		return listaInventarioMaterialHistorico;
	}

	public void setListaInventarioMaterialHistorico(
			List<InventarioMaterialHistorico> listaInventarioMaterialHistorico) {
		this.listaInventarioMaterialHistorico = listaInventarioMaterialHistorico;
	}

	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@Transient
	public Double getTotalPesoLiquido(){
		if(getMaterial() != null && getMaterial().getPeso() != null && getQtde() != null){
			return SinedUtil.round(getQtde() * getMaterial().getPeso(), 4);
		}
		return null;
	}
	
	@Transient
	public Double getTotalPesoBruto(){
		if(getMaterial() != null && getMaterial().getPesobruto() != null && getQtde() != null){
			return SinedUtil.round(getQtde() * getMaterial().getPesobruto(), 4);
		}
		return null;
	}
	
	@Transient
	public Double getTotal(){
		if(getValorUnitario() != null && getQtde() != null){
			return SinedUtil.round(getQtde() * getValorUnitario(), 2);
		}
		return null;
	}
	
	@Transient
	public Boolean getHasAjuste() {
		return hasAjuste;
	}
	
	public void setHasAjuste(Boolean hasAjuste) {
		this.hasAjuste = hasAjuste;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Boolean getGerarBlocoK() {
		return gerarBlocoK;
	}

	public void setGerarBlocoK(Boolean gerarBlocoK) {
		this.gerarBlocoK = gerarBlocoK;
	}

	public Boolean getGerarBlocoH() {
		return gerarBlocoH;
	}

	public void setGerarBlocoH(Boolean gerarBlocoH) {
		this.gerarBlocoH = gerarBlocoH;
	}

	public void ajustaBeanToSave(){
		if(Tipopessoainventario.CLIENTE.equals(getTipopessoainventario()) && getCliente() != null){
			pessoa = new Pessoa(getCliente().getCdpessoa(),null);
		}else if(Tipopessoainventario.FORNECEDOR.equals(getTipopessoainventario()) && getFornecedor() != null){
			pessoa = new Pessoa(getFornecedor().getCdpessoa(),null);
		}
		setQtde(SinedUtil.roundByParametro(getQtde()));
	}
	
	public void ajustaBeanToLoad(){
		if(getPessoa() != null){
			if(Tipopessoainventario.CLIENTE.equals(getTipopessoainventario())){
				setCliente(new Cliente(getPessoa().getCdpessoa(), getPessoa().getNome()));
			}else if(Tipopessoainventario.FORNECEDOR.equals(getTipopessoainventario())){
				setFornecedor(new Fornecedor(getPessoa().getCdpessoa(), getPessoa().getNome()));
			}
		}
		setValorUnitario(SinedUtil.roundByParametro(getValorUnitario()));
		setQtde(SinedUtil.roundByParametro(getQtde()));
	}
	
}
