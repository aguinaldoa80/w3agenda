package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfeferroviariovagao", sequenceName = "sq_mdfeferroviariovagao")
public class MdfeFerroviarioVagao {

	protected Integer cdMdfeFerroviarioVagao;
	protected Mdfe mdfe;
	protected Double pesoBaseCalculoFreteToneladas;
	protected Double pesoRealToneladas;
	protected String tipoVagao;
	protected String serieIdentificacao;
	protected String numeroIdentificacao;
	protected String sequenciaNaComposicao;
	protected Double toneladaUtil;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeferroviariovagao")
	public Integer getCdMdfeFerroviarioVagao() {
		return cdMdfeFerroviarioVagao;
	}
	public void setCdMdfeFerroviarioVagao(Integer cdMdfeFerroviarioVagao) {
		this.cdMdfeFerroviarioVagao = cdMdfeFerroviarioVagao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@DisplayName("Peso base de c�lculo de frete em toneladas")
	public Double getPesoBaseCalculoFreteToneladas() {
		return pesoBaseCalculoFreteToneladas;
	}
	public void setPesoBaseCalculoFreteToneladas(
			Double pesoBaseCalculoFreteToneladas) {
		this.pesoBaseCalculoFreteToneladas = pesoBaseCalculoFreteToneladas;
	}
	
	@Required
	@DisplayName("Peso real em toneladas")
	public Double getPesoRealToneladas() {
		return pesoRealToneladas;
	}
	public void setPesoRealToneladas(Double pesorealtoneladas) {
		this.pesoRealToneladas = pesorealtoneladas;
	}
	
	@MaxLength(value=3)
	@DisplayName("Tipo de vag�o")
	public String getTipoVagao() {
		return tipoVagao;
	}
	public void setTipoVagao(String tipovagao) {
		this.tipoVagao = tipovagao;
	}
	
	@Required
	@MaxLength(value=3)
	@DisplayName("S�rie de identifica��o do vag�o")
	public String getSerieIdentificacao() {
		return serieIdentificacao;
	}
	public void setSerieIdentificacao(String serieidentificacao) {
		this.serieIdentificacao = serieidentificacao;
	}
	
	@Required
	@DisplayName("N�mero de identifica��o do vag�o")
	public String getNumeroIdentificacao() {
		return numeroIdentificacao;
	}
	public void setNumeroIdentificacao(String numeroidentificacao) {
		this.numeroIdentificacao = numeroidentificacao;
	}
	
	@DisplayName("Sequ�ncia do vag�o na composi��o")
	public String getSequenciaNaComposicao() {
		return sequenciaNaComposicao;
	}
	public void setSequenciaNaComposicao(String sequencianacomposicao) {
		this.sequenciaNaComposicao = sequencianacomposicao;
	}
	
	@Required
	@DisplayName("Tonelada �til")
	public Double getToneladaUtil() {
		return toneladaUtil;
	}
	public void setToneladaUtil(Double toneladautil) {
		this.toneladaUtil = toneladautil;
	}
}
