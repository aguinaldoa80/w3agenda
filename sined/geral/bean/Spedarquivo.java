package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_spedarquivo", sequenceName = "sq_spedarquivo")
public class Spedarquivo implements Log {

	protected Integer cdspedarquivo;
	protected Arquivo arquivo;
	protected Date mesano;
	protected Empresa empresa;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected String mesanoAux;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_spedarquivo")
	public Integer getCdspedarquivo() {
		return cdspedarquivo;
	}
	
	@DisplayName("Arquivo SPED")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public Date getMesano() {
		return mesano;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdspedarquivo(Integer cdspedarquivo) {
		this.cdspedarquivo = cdspedarquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Required
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
	
}
