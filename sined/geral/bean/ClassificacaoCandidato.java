package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_classificacaocandidato", sequenceName = "sq_classificacaocandidato")
@DisplayName("Classificação Candidato")
public class ClassificacaoCandidato implements Log{

	protected Integer cdclassificacaocandidato;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_classificacaocandidato")
	public Integer getCdclassificacaocandidato() {
		return cdclassificacaocandidato;
	}
	
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	
	public void setCdclassificacaocandidato(Integer cdclassificacaocandidato) {
		this.cdclassificacaocandidato = cdclassificacaocandidato;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Integer getCdusuarioaltera() {
		
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
	
}
