package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;


@Entity
@SequenceGenerator(name = "sq_naturezaoperacaocst", sequenceName = "sq_naturezaoperacaocst")
public class Naturezaoperacaocst {

	private Integer cdnaturezaoperacaocst;
	private Naturezaoperacao naturezaoperacao;
	private Tipocobrancapis tipocobrancapis;
	private Tipocobrancaicms tipocobrancaicms;
	private Tipocobrancacofins tipocobrancacofins;
	private Tipocobrancaipi tipocobrancaipi;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_naturezaoperacaocst")
	public Integer getCdnaturezaoperacaocst() {
		return cdnaturezaoperacaocst;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	@Required
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	@DisplayName("CST PIS")
	@Required
	public Tipocobrancapis getTipocobrancapis() {
		return tipocobrancapis;
	}
	@DisplayName("CST ICMS")
	@Required
	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}
	@DisplayName("CST COFINS")
	@Required
	public Tipocobrancacofins getTipocobrancacofins() {
		return tipocobrancacofins;
	}
	@DisplayName("CST IPI")
	@Required
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	
	public void setCdnaturezaoperacaocst(Integer cdnaturezaoperacaocst) {
		this.cdnaturezaoperacaocst = cdnaturezaoperacaocst;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setTipocobrancapis(Tipocobrancapis tipocobrancapis) {
		this.tipocobrancapis = tipocobrancapis;
	}
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	public void setTipocobrancacofins(Tipocobrancacofins tipocobrancacofins) {
		this.tipocobrancacofins = tipocobrancacofins;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}	
}
