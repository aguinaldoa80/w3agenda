package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_clientebancodadosmodulo", sequenceName = "sq_clientebancodadosmodulo")
public class ClienteBancoDadosModulo {

	protected Integer cdclientebancodadosmodulo;
	protected ClienteBancoDados clienteBancoDados;
	protected String modulo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientebancodadosmodulo")
	public Integer getCdclientebancodadosmodulo() {
		return cdclientebancodadosmodulo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclientebancodados")
	public ClienteBancoDados getClienteBancoDados() {
		return clienteBancoDados;
	}
	
	@Required
	@DisplayName("M�dulo")
	public String getModulo() {
		return modulo;
	}

	public void setCdclientebancodadosmodulo(Integer cdclientebancodadosmodulo) {
		this.cdclientebancodadosmodulo = cdclientebancodadosmodulo;
	}

	public void setClienteBancoDados(ClienteBancoDados clienteBancoDados) {
		this.clienteBancoDados = clienteBancoDados;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	
}
