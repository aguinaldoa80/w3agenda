package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_meiocontato", sequenceName = "sq_meiocontato")
@DisplayName("Meio de contato")
public class Meiocontato implements Log{

	protected Integer cdmeiocontato;
	protected String nome;
	protected Boolean ativo;
	protected Boolean padrao;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	public Meiocontato(){}
	
	public Meiocontato(Integer cdmeiocontato){
		this.cdmeiocontato = cdmeiocontato;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_meiocontato")
	public Integer getCdmeiocontato() {
		return cdmeiocontato;
	}
		
	@MaxLength(100)
	@DisplayName("Descri��o")
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdmeiocontato(Integer cdmeiocontato) {
		this.cdmeiocontato = cdmeiocontato;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Required
	@DisplayName("Padr�o")
	public Boolean getPadrao() {
		return padrao;
	}
	public void setPadrao(Boolean padrao) {
		this.padrao = padrao;
	}
	
	public Integer getCdusuarioaltera() {
		return this.cdUsuarioAltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
		
	}

	public Timestamp getDtaltera() {
		return this.dtAltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
}
