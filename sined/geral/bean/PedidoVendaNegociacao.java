package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_pedidovendanegociacao", sequenceName = "sq_pedidovendanegociacao")
public class PedidoVendaNegociacao {

	private Integer cdPedidoVendaNegociacao;
	private Pedidovenda pedidovenda;
	private Documento documento;
	private Documentotipo documentoTipo;
	private Integer banco;
	private Integer agencia;
	private Integer conta;
	private String numero;
	private Money valorOriginal;
	private Date dataParcela;
	private Money valorJuros;
	private Documento documentoAntecipacao;
	private String emitente;
	private String cpfcnpj;
	private Cheque cheque;
	
	//////////////////////////////|
	///////////////Transient /////|
	//////////////////////////////|
	protected Boolean podeGerarCheque;
	
	
	///////////////|
	///INICIO GET /|
	///////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendanegociacao")
	public Integer getCdPedidoVendaNegociacao() {
		return cdPedidoVendaNegociacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")	
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformapagamento")
	public Documentotipo getDocumentoTipo() {
		return documentoTipo;
	}
	
	public Integer getBanco() {
		return banco;
	}
	@DisplayName("Ag�ncia")
	public Integer getAgencia() {
		return agencia;
	}
	@MaxLength(9)
	public Integer getConta() {
		return conta;
	}
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}
	public Money getValorOriginal() {
		return valorOriginal;
	}
	public Date getDataParcela() {
		return dataParcela;
	}
	public Money getValorJuros() {
		return valorJuros;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoantecipacao")
	@DisplayName("Pagamento adiantado")
	public Documento getDocumentoAntecipacao() {
		return documentoAntecipacao;
	}
	public String getEmitente() {
		return emitente;
	}
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	@DisplayName("Cheque")
	public Cheque getCheque() {
		return cheque;
	}
	///////////////|
	///INICIO SET /|
	///////////////|
	public void setCdPedidoVendaNegociacao(Integer cdPedidoVendaNegociacao) {
		this.cdPedidoVendaNegociacao = cdPedidoVendaNegociacao;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDocumentoTipo(Documentotipo documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setValorOriginal(Money valorOriginal) {
		this.valorOriginal = valorOriginal;
	}
	public void setDataParcela(Date dataParcela) {
		this.dataParcela = dataParcela;
	}
	public void setValorJuros(Money valorJuros) {
		this.valorJuros = valorJuros;
	}
	public void setDocumentoAntecipacao(Documento documentoAntecipacao) {
		this.documentoAntecipacao = documentoAntecipacao;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
		
	/////////////////////|
	///INICIO Transient /|
	/////////////////////|
	
	@Transient
	public Boolean getPodeGerarCheque() {
		if(documentoTipo == null || documentoTipo.getCddocumentotipo() == null){
			podeGerarCheque = false;
		}else{
			podeGerarCheque = StringUtils.isNotEmpty(this.getNumero()) && (!Boolean.TRUE.equals(documentoTipo.getCartao()));
		}
		return podeGerarCheque;
	}
	
}
