package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipopessoa {

	PESSOA_FISICA(0, "F�sica", "F"),
	PESSOA_JURIDICA(1, "Jur�dica", "J");
	
	private Tipopessoa (Integer value, String tipo, String prefix){
		this.tipo = tipo;
		this.value = value;
		this.prefix = prefix;
	}
	
	private Integer value;
	private String tipo;
	private String prefix;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}
}
