package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_campoentrada", sequenceName = "sq_campoentrada")
public class Campoentrada {
	
	protected Integer cdcampoentrada;
	protected Tela tela;
	protected String campo;
	protected Boolean exibir;	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campoentrada")
	public Integer getCdcampoentrada() {
		return cdcampoentrada;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtela")	
	public Tela getTela() {
		return tela;
	}
	
	@DisplayName("Campo")
	public String getCampo() {
		return campo;
	}
	@DisplayName("Exibir")
	public Boolean getExibir() {
		return exibir;
	}

	public void setCdcampoentrada(Integer cdcampoentrada) {
		this.cdcampoentrada = cdcampoentrada;
	}
	public void setTela(Tela tela) {
		this.tela = tela;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public void setExibir(Boolean exibir) {		
		this.exibir = exibir;
	}

}
