package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.search.annotations.DocumentId;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Gradeestoquetipo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.CompararCampo;


@Entity
@SequenceGenerator(name = "sq_materialgrupo", sequenceName = "sq_materialgrupo")
@DisplayName("Grupo")
public class Materialgrupo implements Log {

	protected Integer cdmaterialgrupo;
	protected String nome;
	protected Double valormvaespecifico;
	protected Boolean ativo = Boolean.TRUE;
	protected Boolean locacao;
	protected Arquivo arquivo;
	protected String email;
	protected Gradeestoquetipo gradeestoquetipo;
	protected Boolean pesquisasomentemestre;
	protected Boolean papelimune;
	protected Boolean bebidaalcoolica;
	protected Double rateiocustoproducao;
	private Money baseCalculoComissao;
	protected Boolean sincronizarwms;
	protected Boolean coleta;
	protected Boolean gerarBlocoH;
	protected Boolean gerarBlocoK;
	protected Boolean incluirLoteNaNfe;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	protected List<Material> listaMaterial = new ListSet<Material>(Material.class);
	protected List<Materialgrupousuario> listaMaterialgrupousuario = new ListSet<Materialgrupousuario>(Materialgrupousuario.class);
	protected Set<Materialgrupocomissaovenda> listaMaterialgrupocomissaovenda = new ListSet<Materialgrupocomissaovenda>(Materialgrupocomissaovenda.class);
	protected Set<Materialgrupotributacao> listaMaterialgrupotributacao = new ListSet<Materialgrupotributacao>(Materialgrupotributacao.class);
	protected List<MaterialGrupoHistorico> listaMaterialGrupoHistorico = new ListSet<MaterialGrupoHistorico>(MaterialGrupoHistorico.class);;
	
	protected Cfop cfop;
	protected Boolean animal;
	protected Boolean registrarpesomedio;
	
	protected Boolean acompanhaBanda;
	protected Boolean banda;
	
	protected Boolean medicamento;
	protected Boolean produtoControlado;
	
	
	
	public Materialgrupo(){
	}

	public Materialgrupo(Integer cdmaterialgrupo){
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	
	public Materialgrupo(String nome){
		this.nome = nome;
	}
	
	/////////////////////|
	// INICIO GET     ///|
	/////////////////////|
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialgrupo")
	public Integer getCdmaterialgrupo() {
		return cdmaterialgrupo;
	}
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Texto para Ordem de Compra")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Loca��o")
	public Boolean getLocacao() {
		return locacao;
	}
	
	@DisplayName("Bebida Alco�lica")
	public Boolean getBebidaalcoolica() {
		return bebidaalcoolica;
	}
	
	@DisplayName("Papel imune")
	public Boolean getPapelimune() {
		return papelimune;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	@MaxLength(50)
	@Email
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@OneToMany(mappedBy="materialgrupo")
	public List<Material> getListaMaterial() {
		return listaMaterial;
	}
	@DisplayName("Compradores")
	@OneToMany(mappedBy="materialgrupo")
	public List<Materialgrupousuario> getListaMaterialgrupousuario() {
		return listaMaterialgrupousuario;
	}
	public void setListaMaterialgrupousuario(List<Materialgrupousuario> listaMaterialgrupousuario) {
		this.listaMaterialgrupousuario = listaMaterialgrupousuario;
	}
	@DisplayName("Comiss�o de Venda")
	@OneToMany(mappedBy="materialgrupo")
	public Set<Materialgrupocomissaovenda> getListaMaterialgrupocomissaovenda() {
		return listaMaterialgrupocomissaovenda;
	}
	@DisplayName("Tributa��o")
	@OneToMany(mappedBy="materialgrupo")
	public Set<Materialgrupotributacao> getListaMaterialgrupotributacao() {
		return listaMaterialgrupotributacao;
	}
	@DisplayName("Estoque")
	public Gradeestoquetipo getGradeestoquetipo() {
		return gradeestoquetipo;
	}
	@DisplayName("Pesquisa de produto somente com o Mestre")
	public Boolean getPesquisasomentemestre() {
		return pesquisasomentemestre;
	}
	@DisplayName("MVA Espec�fico")
	public Double getValormvaespecifico() {
		return valormvaespecifico;
	}
	@DisplayName("Rateio no custo de produ��o")
	public Double getRateiocustoproducao() {
		return rateiocustoproducao;
	}
	@DisplayName("Base de c�lculo da Comiss�o %")
	public Money getBaseCalculoComissao() {
		return baseCalculoComissao;
	}
	@DisplayName("Sincronizar com WMS")
	public Boolean getSincronizarwms() {
		return sincronizarwms;
	}
	@DisplayName("Animal")
	public Boolean getAnimal() {
		return animal;
	}
	@DisplayName("Registrar peso m�dio")
	public Boolean getRegistrarpesomedio() {
		return registrarpesomedio;
	}
	@DisplayName("Material de Coleta")
	public Boolean getColeta() {
		return coleta;
	}
	@DisplayName("Acompanha Banda")
	public Boolean getAcompanhaBanda() {
		return acompanhaBanda;
	}
	@DisplayName("Banda")
	public Boolean getBanda() {
		return banda;
	}
	@DisplayName("Medicamento")
	public Boolean getMedicamento() {
		return medicamento;
	}
	
	/////////////////////|
	// INICIO SET     ///|
	/////////////////////|

	public void setMedicamento(Boolean medicamento) {
		this.medicamento = medicamento;
	}
	
	public void setAcompanhaBanda(Boolean acompanhaBanda) {
		this.acompanhaBanda = acompanhaBanda;
	}
	public void setBanda(Boolean banda) {
		this.banda = banda;
	}
	
	public void setPapelimune(Boolean papelimune) {
		this.papelimune = papelimune;
	}
	
	public void setBebidaalcoolica(Boolean bebidaalcoolica) {
		this.bebidaalcoolica = bebidaalcoolica;
	}
	
	public void setLocacao(Boolean locacao) {
		this.locacao = locacao;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdmaterialgrupo(Integer cdmaterialgrupo) {
		this.cdmaterialgrupo = cdmaterialgrupo;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public void setListaMaterial(List<Material> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	
	

	public void setListaMaterialgrupotributacao(Set<Materialgrupotributacao> listaMaterialgrupotributacao) {
		this.listaMaterialgrupotributacao = listaMaterialgrupotributacao;
	}

	public void setListaMaterialgrupocomissaovenda(Set<Materialgrupocomissaovenda> listaMaterialgrupocomissaovenda) {
		this.listaMaterialgrupocomissaovenda = listaMaterialgrupocomissaovenda;
	}

	/////////////////////////|
	/// Transient e outros //|
	/////////////////////////|
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterialgrupo == null) ? 0 : cdmaterialgrupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Materialgrupo other = (Materialgrupo) obj;
		if (cdmaterialgrupo == null) {
			if (other.cdmaterialgrupo != null)
				return false;
		} else if (!cdmaterialgrupo.equals(other.cdmaterialgrupo))
			return false;
		return true;
	}

	//RETORNO A TRIBUTA��O DE ACORDO COM A EMPRESA
	@Transient
	public Materialgrupotributacao getMaterialgrupotributacaoByEmpresa(Empresa empresa, Cfop cfop) {
		Materialgrupotributacao bean = new Materialgrupotributacao();
		Boolean achou = false;
		if(this.getListaMaterialgrupotributacao() != null && !this.getListaMaterialgrupotributacao().isEmpty()){
			if(cfop != null){
				for(Materialgrupotributacao materialgrupotributacao : this.getListaMaterialgrupotributacao()){
					if(empresa == null){
						if(materialgrupotributacao.getEmpresa() == null && 
								materialgrupotributacao.getCfop() != null && cfop.equals(materialgrupotributacao.getCfop())){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}else {
						if(materialgrupotributacao.getEmpresa() != null && empresa.equals(materialgrupotributacao.getEmpresa()) &&
								materialgrupotributacao.getCfop() != null && cfop.equals(materialgrupotributacao.getCfop())){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}
				}
				if(!achou){
					for(Materialgrupotributacao materialgrupotributacao : this.getListaMaterialgrupotributacao()){
						if(materialgrupotributacao.getEmpresa() == null &&
								cfop!=null &&
								materialgrupotributacao.getCfop() != null && 
								cfop.equals(materialgrupotributacao.getCfop())){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}
				}
			}
			
			if(!achou){
				for(Materialgrupotributacao materialgrupotributacao : this.getListaMaterialgrupotributacao()){
					if(empresa == null){
						if(materialgrupotributacao.getEmpresa() == null){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}else {
						if(materialgrupotributacao.getEmpresa() != null && empresa.equals(materialgrupotributacao.getEmpresa())){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}
				}
				if(!achou){
					for(Materialgrupotributacao materialgrupotributacao : this.getListaMaterialgrupotributacao()){
						if(materialgrupotributacao.getEmpresa() == null){
							bean = materialgrupotributacao;
							achou = true;
							break;
						}
					}
				}
			}
		}
		return bean;
	}	
	
	
	@Transient
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	
	
	public void setGradeestoquetipo(Gradeestoquetipo gradeestoquetipo) {
		this.gradeestoquetipo = gradeestoquetipo;
	}



	public void setPesquisasomentemestre(Boolean pesquisasomentemestre) {
		this.pesquisasomentemestre = pesquisasomentemestre;
	}
	
	
	public void setValormvaespecifico(Double valormvaespecifico) {
		this.valormvaespecifico = valormvaespecifico;
	}


	public void setRateiocustoproducao(Double rateiocustoproducao) {
		this.rateiocustoproducao = rateiocustoproducao;
	}
	
	public void setBaseCalculoComissao(Money baseCalculoComissao) {
		this.baseCalculoComissao = baseCalculoComissao;
	}

	public void setSincronizarwms(Boolean sincronizarwms) {
		this.sincronizarwms = sincronizarwms;
	}
	
	
	public void setAnimal(Boolean animal) {
		this.animal = animal;
	}
	
	
	public void setRegistrarpesomedio(Boolean registrarpesomedio) {
		this.registrarpesomedio = registrarpesomedio;
	}

	public void setColeta(Boolean coleta) {
		this.coleta = coleta;
	}
	@DisplayName("Bloco H")
	@CompararCampo
	public Boolean getGerarBlocoH() {
		return gerarBlocoH;
	}

	public void setGerarBlocoH(Boolean gerarBlocoH) {
		this.gerarBlocoH = gerarBlocoH;
	}
	@DisplayName("Bloco K")
	@CompararCampo
	public Boolean getGerarBlocoK() {
		return gerarBlocoK;
	}

	public void setGerarBlocoK(Boolean gerarBlocoK) {
		this.gerarBlocoK = gerarBlocoK;
	}
	
	@DisplayName("Incluir Lote na NF-e")
	@CompararCampo
	public Boolean getIncluirLoteNaNfe() {
		return incluirLoteNaNfe;
	}
	
	public void setIncluirLoteNaNfe(Boolean incluirLoteNaNfe) {
		this.incluirLoteNaNfe = incluirLoteNaNfe;
	}

	@OneToMany(mappedBy="cdMaterialGrupo")
	@DisplayName("Hist�rico")
	public List<MaterialGrupoHistorico> getListaMaterialGrupoHistorico() {
		return listaMaterialGrupoHistorico;
	}

	public void setListaMaterialGrupoHistorico(
			List<MaterialGrupoHistorico> listaMaterialGrupoHistorico) {
		this.listaMaterialGrupoHistorico = listaMaterialGrupoHistorico;
	}

	@DisplayName("PRODUTO CONTROLADO")
	public Boolean getProdutoControlado() {
		return produtoControlado;
	}

	public void setProdutoControlado(Boolean produtoControlado) {
		this.produtoControlado = produtoControlado;
	}
	
	
	
}