package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("ProvidÍncia")
public class Providencia {
	
	protected Integer cdprovidencia;
	protected String nome;
	
	@Id
	public Integer getCdprovidencia() {
		return cdprovidencia;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdprovidencia(Integer cdprovidencia) {
		this.cdprovidencia = cdprovidencia;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
