package br.com.linkcom.sined.geral.bean;


import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_autorizacaotrabalho", sequenceName = "sq_autorizacaotrabalho")
@DisplayName("Autoriza��o de trabalho")
public class Autorizacaotrabalho {

	protected Integer cdautorizacaotrabalho;
	protected Contrato contrato;
	protected Date dtautorizacao;
	protected Requisicaoestado autorizacaoestado;
	protected Requisicaoprioridade autorizacaoprioridade;
	protected String descricao;
	protected Colaborador colaboradorresponsavel;
	protected List<Autorizacaohistorico> listaAutorizacaohistorico; 
	protected Requisicao requisicao;
	protected Double estimativa;
	protected Integer peso;
	protected Date dtprevisao;
	protected Etapa etapa;
	protected Tipoiteracao tipoiteracao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_autorizacaotrabalho")
	public Integer getCdautorizacaotrabalho() {
		return cdautorizacaotrabalho;
	}
	
	public void setCdautorizacaotrabalho(Integer cdautorizacaotrabalho) {
		this.cdautorizacaotrabalho = cdautorizacaotrabalho;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	@Required
	@DisplayName("Contrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	
	@DisplayName("Data da autoriza��o")
	public Date getDtautorizacao() {
		return dtautorizacao;
	}
	
	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdautorizacaoestado")
	@Required
	public Requisicaoestado getAutorizacaoestado() {
		return autorizacaoestado;
	}
	
	@MaxLength(1024)
	@DescriptionProperty
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorresponsavel")
	@Required
	public Colaborador getColaboradorresponsavel() {
		return colaboradorresponsavel;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="autorizacaotrabalho")
	public List<Autorizacaohistorico> getListaAutorizacaohistorico() {
		return listaAutorizacaohistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdautorizacaoprioridade")
	@DisplayName("Prioridade")
	@Required
	public Requisicaoprioridade getAutorizacaoprioridade() {
		return autorizacaoprioridade;
	}
	
	public Double getEstimativa() {
		return estimativa;
	}
	
	@DisplayName("Previs�o")
	public Date getDtprevisao() {
		return dtprevisao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdetapa")
	@DisplayName("Etapa")
	public Etapa getEtapa() {
		return etapa;
	}
	
	public Integer getPeso() {
		return peso;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setColaboradorresponsavel(Colaborador colaboradorresponsavel) {
		this.colaboradorresponsavel = colaboradorresponsavel;
	}
	
	public void setListaAutorizacaohistorico(List<Autorizacaohistorico> listaAutorizacaohistorico) {
		this.listaAutorizacaohistorico = listaAutorizacaohistorico;
	}
	
	public void setDtautorizacao(Date dtautorizacao) {
		this.dtautorizacao = dtautorizacao;
	}

	public void setAutorizacaoestado(Requisicaoestado autorizacaoestado) {
		this.autorizacaoestado = autorizacaoestado;
	}

	public void setAutorizacaoprioridade(Requisicaoprioridade autorizacaoprioridade) {
		this.autorizacaoprioridade = autorizacaoprioridade;
	}

	public void setEstimativa(Double estimativa) {
		this.estimativa = estimativa;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public void setDtprevisao(Date dtprevisao) {
		this.dtprevisao = dtprevisao;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	@DisplayName("Tipo de itera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoiteracao")
	public Tipoiteracao getTipoiteracao() {
		return tipoiteracao;
	}
	public void setTipoiteracao(Tipoiteracao tipoiteracao) {
		this.tipoiteracao = tipoiteracao;
	}
}

