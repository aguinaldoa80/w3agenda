package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import br.com.linkcom.neo.types.Money;

public class AnaliseReceitaDespesaBean {
	private Integer cdmovimentacao;
	private Integer cddocumento;
	private Integer codigo;
	private String tipo;
	private String dtFormatado;
	private Date data;
	private String descricao;
	private Money valor;
	private Documentoclasse documentoclasse;
	
	public AnaliseReceitaDespesaBean() {}
	
	public AnaliseReceitaDespesaBean(Documento documento) {
		if(documento != null) {
			this.cddocumento = documento.getCddocumento();
			this.codigo = documento.getCddocumento();
			this.dtFormatado = documento.getDtVencimentoFormatado();
			this.data = documento.getDtvencimento();
			this.descricao = documento.getNumeroPessoaDescricao();
			this.valor = documento.getValor();
			
			if(documento.getDocumentoclasse() != null) {
				this.documentoclasse = documento.getDocumentoclasse();
				this.tipo = "Conta a " + (documento.getDocumentoclasse().getCddocumentoclasse().equals(Documentoclasse.CD_PAGAR) ? "pagar" : "receber");
			}
		}
	}
	
	public AnaliseReceitaDespesaBean(Movimentacao movimentacao) {
		if(movimentacao != null) {
			this.cdmovimentacao = movimentacao.getCdmovimentacao();
			this.codigo = movimentacao.getCdmovimentacao();
			this.dtFormatado = movimentacao.getDtBancoFormatado();
			this.data = movimentacao.getDtbanco();
			this.descricao = movimentacao.getAux_movimentacao().getMovimentacaodescricao();
			this.valor = movimentacao.getValor();
			this.tipo = "Movimentação";
		}
	}
	
	public Integer getCdmovimentacao() {
		return cdmovimentacao;
	}
	public Integer getCddocumento() {
		return cddocumento;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public String getDtFormatado() {
		return dtFormatado;
	}
	public Date getData() {
		return data;
	}
	public String getDescricao() {
		return descricao;
	}
	public Money getValor() {
		return valor;
	}
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}
	public void setCdmovimentacao(Integer cdmovimentacao) {
		this.cdmovimentacao = cdmovimentacao;
	}
	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setDtFormatado(String dtFormatado) {
		this.dtFormatado = dtFormatado;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
}
