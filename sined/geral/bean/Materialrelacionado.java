package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialrelacionado", sequenceName = "sq_materialrelacionado")
@DisplayName("Composi��o do Kit")
public class Materialrelacionado {

	protected Integer cdmaterialrelacionado;
	protected Integer ordemexibicao;
	protected Material material;
	protected Material materialpromocao;
	protected Money valorvenda;
	protected Double quantidade = 1d;
	protected Double quantidadeestoque = 0d;
	protected Boolean desmontarKitRecebimento;
	protected Boolean exibirKitObservacaoItemNota;
	protected Double porcentagemCustoGeral;
	protected Double porcentagemCustoUnitario;
	protected List<Materialrelacionadoformula> listaMaterialrelacionadoformula = new ListSet<Materialrelacionadoformula>(Materialrelacionadoformula.class);
	
//	TRANSIENTES
	protected String index;
	protected List<Empresa> listaMaterialEmpresas = new ListSet<Empresa>(Empresa.class);
	protected Boolean existematerialsimilar;
	protected Money valorvendasemdesconto;
	protected Double totaltrans;
	protected Double valorTotalvendaItem;
	protected List<Localarmazenagem> listalocalarmazenagem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialrelacionado")
	public Integer getCdmaterialrelacionado() {
		return cdmaterialrelacionado;
	}
	@DisplayName("Ordem")
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialpromocao")
	public Material getMaterialpromocao() {
		return materialpromocao;
	}
	
	@Required
	@DisplayName("Valor venda")
	public Money getValorvenda() {
		return valorvenda;
	}
	
	@OneToMany(mappedBy="materialrelacionado")
	public List<Materialrelacionadoformula> getListaMaterialrelacionadoformula() {
		return listaMaterialrelacionadoformula;
	}
	
	public void setCdmaterialrelacionado(Integer cdmaterialrelacionado) {
		this.cdmaterialrelacionado = cdmaterialrelacionado;
	}
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialpromocao(Material materialpromocao) {
		this.materialpromocao = materialpromocao;
	}
	public void setValorvenda(Money valorvenda) {
		this.valorvenda = valorvenda;
	}

	public void setListaMaterialrelacionadoformula(List<Materialrelacionadoformula> listaMaterialrelacionadoformula) {
		this.listaMaterialrelacionadoformula = listaMaterialrelacionadoformula;
	}

	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	@Transient
	public String getIndex() {
		return index;
	}
	@Transient
	@DisplayName("Quantidade Estoque")
	public Double getQuantidadeestoque() {
		return quantidadeestoque;
	}
	@Transient
	public List<Empresa> getListaMaterialEmpresas() {
		return listaMaterialEmpresas;
	}
	
	public void setIndex(String index) {
		this.index = index;
	}
	public void setListaMaterialEmpresas(List<Empresa> listaMaterialEmpresas) {
		this.listaMaterialEmpresas = listaMaterialEmpresas;
	}
	public void setQuantidadeestoque(Double quantidadeestoque) {
		this.quantidadeestoque = quantidadeestoque;
	}

	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	
	@Transient
	public Money getValorvendasemdesconto() {
		return valorvendasemdesconto;
	}
	public void setValorvendasemdesconto(Money valorvendasemdesconto) {
		this.valorvendasemdesconto = valorvendasemdesconto;
	}
	
	@Transient
	public Money getValorvendasemdescontoOuValorvenda() {
		return valorvendasemdesconto != null ? valorvendasemdesconto : valorvenda;
	}
	
	@Transient
	public Double getTotaltrans() {
		if(this.quantidade != null && this.valorvenda != null){
			totaltrans = this.quantidade * this.valorvenda.getValue().doubleValue();
		}
		return totaltrans;
	}
	public void setTotaltrans(Double totaltrans) {
		this.totaltrans = totaltrans;
	}
	
	@DisplayName("Desmontar o kit no recebimento")
	public Boolean getDesmontarKitRecebimento() {
		return desmontarKitRecebimento;
	}
	
	public void setDesmontarKitRecebimento(Boolean desmontarKitRecebimento) {
		this.desmontarKitRecebimento = desmontarKitRecebimento;
	}
	
	@DisplayName("Exibir kit na observa��o do item na nota")
	public Boolean getExibirKitObservacaoItemNota() {
		return exibirKitObservacaoItemNota;
	}
	
	public void setExibirKitObservacaoItemNota(
			Boolean exibirKitObservacaoItemNota) {
		this.exibirKitObservacaoItemNota = exibirKitObservacaoItemNota;
	}
	
	@DisplayName("% Custo Geral")
	public Double getPorcentagemCustoGeral() {
		return porcentagemCustoGeral;
	}
	
	public void setPorcentagemCustoGeral(Double porcentagemCustoGeral) {
		this.porcentagemCustoGeral = porcentagemCustoGeral;
	}
	
	@DisplayName("% Unit�rio de Custo")
	public Double getPorcentagemCustoUnitario() {
		return porcentagemCustoUnitario;
	}
	
	public void setPorcentagemCustoUnitario(Double porcentagemCustoUnitario) {
		this.porcentagemCustoUnitario = porcentagemCustoUnitario;
	}
	
	@Transient
	@DisplayName("Valor Total Venda")
	public Double getValorTotalvendaItem() {
		return valorTotalvendaItem;
	}
	
	public void setValorTotalvendaItem(Double valorTotalvendaItem) {
		this.valorTotalvendaItem = valorTotalvendaItem;
	}
	
	@Transient
	public List<Localarmazenagem> getListalocalarmazenagem() {
		return listalocalarmazenagem;
	}
	public void setListalocalarmazenagem(
			List<Localarmazenagem> listalocalarmazenagem) {
		this.listalocalarmazenagem = listalocalarmazenagem;
	}
}
