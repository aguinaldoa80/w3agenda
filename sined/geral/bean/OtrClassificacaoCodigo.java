package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("C�digo de Classifica��o OTR")
@SequenceGenerator(name = "sq_otrclassificacaocodigo", sequenceName = "sq_otrclassificacaocodigo")
public class OtrClassificacaoCodigo implements Log {
	protected Integer cdOtrClassificacaoCodigo;
	protected String codigo;
	protected OtrDesenho otrDesenho;
	protected OtrServicoTipo otrServicoTipo;
	protected String codigoIntegracao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrclassificacaocodigo")
	public Integer getCdOtrClassificacaoCodigo() {
		return cdOtrClassificacaoCodigo;
	}
	public void setCdOtrClassificacaoCodigo(Integer cdOtrClassificacaoCodigo) {
		this.cdOtrClassificacaoCodigo = cdOtrClassificacaoCodigo;
	}
	
	@Required
	@DisplayName("C�digo")
	@MaxLength(5)
	@DescriptionProperty
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@DisplayName("Desenho")
	@JoinColumn(name="cdOtrDesenho")
	@ManyToOne(fetch=FetchType.LAZY)
	public OtrDesenho getOtrDesenho() {
		return otrDesenho;
	}
	public void setOtrDesenho(OtrDesenho otrDesenho) {
		this.otrDesenho = otrDesenho;
	}
	
	@DisplayName("Tipo de servi�o")
	@JoinColumn(name="cdOtrServicoTipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public OtrServicoTipo getOtrServicoTipo() {
		return otrServicoTipo;
	}
	public void setOtrServicoTipo(OtrServicoTipo otrServicoTipo) {
		this.otrServicoTipo = otrServicoTipo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("C�digo p/ integra��o")
	@MaxLength(20)
	public String getCodigoIntegracao() {
		return codigoIntegracao;
	}
	public void setCodigoIntegracao(String codigoIntegracao) {
		this.codigoIntegracao = codigoIntegracao;
	}
	
	@Transient
	public String getCodigoDesenhoTipoServico(){
		StringBuilder s = new StringBuilder();
		if(this.codigo != null){
			s.append(this.codigo);
		}
		if(this.otrDesenho != null){
			s.append("<span><BR>Desenho: ").append(this.otrDesenho.desenho).append("</span>");
		}
		if(this.otrServicoTipo != null){
			s.append("<span><BR>Tipo Servi�o: ").append(this.otrServicoTipo.tipoServico).append("</span>");
		}
		return s.toString();
	}
	
}
