package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_tipoconta", sequenceName = "sq_tipoconta")
public class Tipoconta {

	protected Integer cdtipoconta;
	protected String nome;
	
	public static final Integer CORRENTE = 1;
	public static final Integer POUPANCA = 2;
	
	public Tipoconta() {
	}
	
	public Tipoconta(Integer cdtipoconta) {
		this.cdtipoconta = cdtipoconta;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoconta")
	public Integer getCdtipoconta() {
		return cdtipoconta;
	}
	public void setCdtipoconta(Integer id) {
		this.cdtipoconta = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
