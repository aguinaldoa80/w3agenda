package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name="sq_clienteespecialidade", sequenceName="sq_clienteespecialidade")
public class Clienteespecialidade {
	
	private Integer cdclienteespecialidade;
	private Cliente cliente;
	private Especialidade especialidade;
	private String registro;
	
	//Transient
	private String conselhoclasseprofissionalStr;
	
	public Clienteespecialidade(){}
	
	public Clienteespecialidade(Especialidade especialidade, String registro){
		this.especialidade = especialidade;
		this.registro = registro;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienteespecialidade")
	public Integer getCdclienteespecialidade() {
		return cdclienteespecialidade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Especialidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdespecialidade")
	public Especialidade getEspecialidade() {
		return especialidade;
	}
	@DisplayName("Registro")
	@MaxLength(50)
	public String getRegistro() {
		return registro;
	}
	@Transient
	@DisplayName("Conselho de Classe Profissional")
	public String getConselhoclasseprofissionalStr() {
		return conselhoclasseprofissionalStr;
	}
	
	public void setCdclienteespecialidade(Integer cdclienteespecialidade) {
		this.cdclienteespecialidade = cdclienteespecialidade;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public void setConselhoclasseprofissionalStr(String conselhoclasseprofissionalStr) {
		this.conselhoclasseprofissionalStr = conselhoclasseprofissionalStr;
	}
}