package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_ecommaterialidentificador", sequenceName = "sq_ecommaterialidentificador")
public class Ecommaterialidentificador {

	protected Integer cdecommaterialidentificador;
	protected Material material;
	protected String identificador;
	protected Configuracaoecom configuracaoecom;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ecommaterialidentificador")
	public Integer getCdecommaterialidentificador() {
		return cdecommaterialidentificador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public String getIdentificador() {
		return identificador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaoecom")
	public Configuracaoecom getConfiguracaoecom() {
		return configuracaoecom;
	}
	
	public void setConfiguracaoecom(Configuracaoecom configuracaoecom) {
		this.configuracaoecom = configuracaoecom;
	}

	public void setCdecommaterialidentificador(Integer cdecommaterialidentificador) {
		this.cdecommaterialidentificador = cdecommaterialidentificador;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
}
