package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesahistoricoacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@SequenceGenerator(name = "sq_colaboradordespesahistorico", sequenceName = "sq_colaboradordespesahistorico")
@DisplayName("Despesa")
@Entity
public class Colaboradordespesahistorico implements Log{

	protected Integer cdcolaboradordespesahistorico;
	protected Colaboradordespesa colaboradordespesa;
	protected Colaboradordespesahistoricoacao acao;
	protected String observacao;
	protected Integer cdusuarioaltera; 
	protected Timestamp dtaltera;
	
	// TRANSIENTES
	protected String whereIn;
	protected Boolean entrada;
	
	@Id
	@DisplayName ("Id")
	@GeneratedValue (strategy=GenerationType.AUTO, generator="sq_colaboradordespesahistorico")
	public Integer getCdcolaboradordespesahistorico() {
		return cdcolaboradordespesahistorico;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesa")
	public Colaboradordespesa getColaboradordespesa() {
		return colaboradordespesa;
	}	
	
	@DisplayName ("A��o")
	public Colaboradordespesahistoricoacao getAcao() {
		return acao;
	}

	@DisplayName ("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName ("Data altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdcolaboradordespesahistorico(
			Integer cdcolaboradordespesahistorico) {
		this.cdcolaboradordespesahistorico = cdcolaboradordespesahistorico;
	}
	public void setColaboradordespesa(Colaboradordespesa colaboradordespesa) {
		this.colaboradordespesa = colaboradordespesa;
	}

	public void setAcao(Colaboradordespesahistoricoacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	@Transient
	public Boolean getEntrada() {
		return entrada;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
		
	
}
