package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.util.CollectionsUtil;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class NotaTipo {
	public static final NotaTipo NOTA_FISCAL_PRODUTO = new NotaTipo(1, "Nota Fiscal de Produto");
	public static final NotaTipo NOTA_FISCAL_CONSUMIDOR = new NotaTipo(1, "Nota Fiscal do Consumidor");
	public static final NotaTipo NOTA_FISCAL_SERVICO = new NotaTipo(2, "Nota Fiscal de Servi�o");
	public static final NotaTipo ENTRADA_FISCAL = new NotaTipo(3, "Entrada Fiscal");
	
	protected Integer cdNotaTipo;
	protected String nome;
	
	public NotaTipo() {
		super();
	}
	
	
	public NotaTipo(Integer cdNotaTipo, String nome) {
		this.cdNotaTipo = cdNotaTipo;
		this.nome = nome;
	}

	public NotaTipo(Integer cdNotaTipo) {
		this.cdNotaTipo = cdNotaTipo;
	}


	@Id
	public Integer getCdNotaTipo() {
		return cdNotaTipo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdNotaTipo(Integer cdNotaTipo) {
		this.cdNotaTipo = cdNotaTipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
	public static List<NotaTipo> getTodosTipos() {
		List<NotaTipo> lista = new ArrayList<NotaTipo>();
		
		lista.add(NOTA_FISCAL_PRODUTO);
		lista.add(NOTA_FISCAL_SERVICO);
		lista.add(ENTRADA_FISCAL);
		
		return lista;
	}
	
	/**
	 * Retorna uma inst�ncia est�tica de NotaTipo que possua a mesma
	 * chave prim�ria da "outra"
	 *
	 * @param outra
	 * @return
	 * @throws NullPointerException : 
	 * 				Caso o par�metro outra chegue nulo ou com o cdNotaTipo nulo.
	 *
	 * @author Hugo Ferreira
	 */
	public static NotaTipo valueOf(NotaTipo outra) {
		if (outra == null || outra.getCdNotaTipo() == null) {
			throw new NullPointerException("O par�metro outra de NotaTipo.valueOf n�o deve ser nulo.");
		}
		
		if (outra.equals(NOTA_FISCAL_PRODUTO)) {
			return NOTA_FISCAL_PRODUTO;
		} else if (outra.equals(NOTA_FISCAL_SERVICO)) {
			return NOTA_FISCAL_SERVICO;
		} else if (outra.equals(ENTRADA_FISCAL)) {
			return ENTRADA_FISCAL;
		}
		    
		return null;
	}

	/**
	 * Retorna uma inst�ncia est�tica de NotaTipo que possua a mesma
	 * chave prim�ria da "outra"
	 *
	 * @param cdNotaTipo
	 * @return
	 *
	 * @author Hugo Ferreira
	 */
	public static NotaTipo valueOf(int cdNotaTipo) {
		if (NOTA_FISCAL_PRODUTO.cdNotaTipo.equals(cdNotaTipo)) {
			return NOTA_FISCAL_PRODUTO;
		} else if (NOTA_FISCAL_SERVICO.cdNotaTipo.equals(cdNotaTipo)) {
			return NOTA_FISCAL_SERVICO;
		} else if (ENTRADA_FISCAL.cdNotaTipo.equals(cdNotaTipo)) {
			return ENTRADA_FISCAL;
		}
		
		
		return null;
	}
	
	/**
	 * Retorna os nomes das NotaTipo's, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 * 
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String getDescriptionProperties(List<NotaTipo> lista) {
		if (lista == null || lista.isEmpty()) {
			lista = getTodosTipos();
		}
		
		for (int i = 0; i < lista.size(); i++) {
			NotaTipo notaTipo = lista.get(i);
			lista.remove(i);
			lista.add(i, valueOf(notaTipo));
		}
		
		String listAndConcatenate = CollectionsUtil.listAndConcatenate(lista, "nome", "; ");
		return listAndConcatenate + (StringUtils.isNotBlank(listAndConcatenate) ? ";" : "");
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdNotaTipo == null) ? 0 : cdNotaTipo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof NotaTipo))
			return false;
		final NotaTipo other = (NotaTipo) obj;
		if (cdNotaTipo == null) {
			if (other.cdNotaTipo != null)
				return false;
		} else if (!cdNotaTipo.equals(other.cdNotaTipo))
			return false;
		return true;
	}
	
	
}
