package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_contacarteiranossonumero", sequenceName = "sq_contacarteiranossonumero")
public class ContaCarteiraNossoNumero {
	
	private Integer cdcontacarteiranossonumero;
	private Contacarteira contacarteira;
	private Integer nossonumero;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacarteiranossonumero")
	public Integer getCdcontacarteiranossonumero() {
		return cdcontacarteiranossonumero;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacarteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	public Integer getNossonumero() {
		return nossonumero;
	}
	public void setCdcontacarteiranossonumero(Integer cdcontacarteiranossonumero) {
		this.cdcontacarteiranossonumero = cdcontacarteiranossonumero;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setNossonumero(Integer nossonumero) {
		this.nossonumero = nossonumero;
	}
	
	

}
