package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_etnia", sequenceName = "sq_etnia")
public class Etnia {

	public static final Etnia INDIGENA = new Etnia(1);
	public static final Etnia BRANCA = new Etnia(2);
	public static final Etnia NEGRA = new Etnia(3);
	public static final Etnia AMARELA = new Etnia(4);
	public static final Etnia PARDA = new Etnia(5);
	public static final Etnia NAO_INFORMADO = new Etnia(6);
	
	protected Integer cdetnia;
	protected String nome;
	
	public Etnia(Integer cdetnia) {
		this.cdetnia = cdetnia;
	}

	public Etnia() {
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_etnia")
	public Integer getCdetnia() {
		return cdetnia;
	}
	public void setCdetnia(Integer id) {
		this.cdetnia = id;
	}
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Etnia) {
			Etnia etnia = (Etnia) obj;
			return this.getCdetnia().equals(etnia.getCdetnia());
		}
		return super.equals(obj);
	}
}
