package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_propostacaixa",sequenceName="sq_propostacaixa")
public class Propostacaixa implements Log {
	
	protected Integer cdpropostacaixa;
	protected String nome;
	protected Arquivo arquivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Propostacaixatemplate> listaPropostacaixatemplate = new ListSet<Propostacaixatemplate>(Propostacaixatemplate.class);
	protected List<Proposta> listaProposta = new ListSet<Proposta>(Proposta.class);
	protected Set<PropostaCaixaItem> listaitem = new ListSet<PropostaCaixaItem>(PropostaCaixaItem.class);
	
	//Transients
	protected String ano;
	protected String descricaoPropostaString;
	protected String clientesPropostaString;
	protected String numeroPropostaString;
	
	
	@Id
	@GeneratedValue(generator="sq_propostacaixa",strategy=GenerationType.AUTO)
	public Integer getCdpropostacaixa() {
		return cdpropostacaixa;
	}
	
	@Required
	@DescriptionProperty
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Propostas")
	@OneToMany(mappedBy="propostacaixa")
	public List<Proposta> getListaProposta() {
		return listaProposta;
	}
	
	@DisplayName("Arquivo de Caixa de Proposta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@OneToMany(mappedBy="caixa")
	public Set<PropostaCaixaItem> getListaitem() {
		return listaitem;
	}
	
	@OneToMany(mappedBy="propostacaixa")
	public Set<Propostacaixatemplate> getListaPropostacaixatemplate() {
		return listaPropostacaixatemplate;
	}
	
	public void setListaPropostacaixatemplate(
			Set<Propostacaixatemplate> listaPropostacaixatemplate) {
		this.listaPropostacaixatemplate = listaPropostacaixatemplate;
	}

	public void setListaitem(Set<PropostaCaixaItem> listaitem) {
		this.listaitem = listaitem;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}

	public void setCdpropostacaixa(Integer cdpropostacaixa) {
		this.cdpropostacaixa = cdpropostacaixa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setListaProposta(List<Proposta> listaProposta) {
		this.listaProposta = listaProposta;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	@Transient
	public String getNumeroPropostas(){
		if (this.getListaProposta() == null || this.getListaProposta().size() == 0) 
			return "";
		else 
			return "<ol style=\"-moz-padding-start:20px;\"> <li>" + CollectionsUtil.listAndConcatenate(this.getListaProposta(), "numeroano", "</li><li>") + "</li></ol>";
	}
	@Transient
	public String getCliente(){
		if (this.getListaProposta() == null || this.getListaProposta().size() == 0) 
			return "";
		else 
			return "<ol style=\"-moz-padding-start:20px;\"> <li>" + CollectionsUtil.listAndConcatenate(this.getListaProposta(), "cliente.nome", "</li><li>") + "</li></ol>";
	}
	@Transient
	public String getPropostaDescrisao(){
		if (this.getListaProposta() == null || this.getListaProposta().size() == 0) 
			return "";
		else 
			return "<ol style=\"-moz-padding-start:20px;\"> <li>" + CollectionsUtil.listAndConcatenate(this.getListaProposta(), "descricao", "</li><li>") + "</li></ol>";
	}
	
	@Transient
	public String getDescricaoPropostaString(){
		descricaoPropostaString = "";
		Integer contador = 1;
		List<Proposta> listaProposta2 = getListaProposta();
		if (listaProposta != null && listaProposta2.size() > 10){
			descricaoPropostaString = "Diversos";
		} else{
			for (Proposta p : listaProposta2) {
				descricaoPropostaString += contador + ". " + p.getDescricao() + "<BR>";
				contador++;
			}
		}
		return descricaoPropostaString;
	}
	
	public void setDescricaoPropostaString(String descricaoPropostaString) {
		this.descricaoPropostaString = descricaoPropostaString;
	}

	@Transient
	public String getClientesPropostaString() {
		clientesPropostaString = "";
		Integer contador = 1;
		List<Proposta> listaProposta2 = getListaProposta();
		if (listaProposta != null && listaProposta2.size() > 10){
			clientesPropostaString = "Diversos";
		} else{
			for (Proposta p : listaProposta2) {
				clientesPropostaString += contador + ". " + p.getCliente().getNome() + "<BR>";
				contador++;
			}
		}
		return clientesPropostaString;
	}
	
	public void setClientesPropostaString(String clientesPropostaString) {
		this.clientesPropostaString = clientesPropostaString;
	}

	@Transient
	public String getNumeroPropostaString() {
		numeroPropostaString = "";
		Integer contador = 1;
		List<Proposta> listaProposta2 = getListaProposta();
		if (listaProposta != null && listaProposta2.size() > 10){
			numeroPropostaString = "Diversos";
		} else{
			for (Proposta p : listaProposta2) {
				numeroPropostaString += contador + ". " + p.getNumeroano() + "<BR>";
				contador++;
			}
		}
		return numeroPropostaString;
	}
	
	public void setNumeroPropostaString(String numeroPropostaString) {
		this.numeroPropostaString = numeroPropostaString;
	}
	
	@Transient
	public String getAno() {
		return ano;
	}
	
	public void setAno(String ano) {
		this.ano = ano;
	}
}
