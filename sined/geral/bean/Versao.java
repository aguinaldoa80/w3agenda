package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@DisplayName("Vers�o")
@SequenceGenerator(name = "sq_versao", sequenceName = "sq_versao")
public class Versao {

	protected Integer cdversao;
	protected Double numero;
	protected String descricao;
	protected Date data;

	protected List<Versaonovidade> listaVersaonovidade = new ListSet<Versaonovidade>(Versaonovidade.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_versao")
	public Integer getCdversao() {
		return cdversao;
	}
	
	@DescriptionProperty
	@DisplayName("N�mero")
	public Double getNumero() {
		return numero;
	}
	@DisplayName("Descri��o")
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	public Date getData() {
		return data;
	}

	@DisplayName("Novidades")
	@OneToMany(mappedBy="versao")
	public List<Versaonovidade> getListaVersaonovidade() {
		return listaVersaonovidade;
	}
	public void setNumero(Double numero) {
		this.numero = numero;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setCdversao(Integer cdversao) {
		this.cdversao = cdversao;
	}
	public void setListaVersaonovidade(List<Versaonovidade> listaVersaonovidade) {
		this.listaVersaonovidade = listaVersaonovidade;
	}
	
}
