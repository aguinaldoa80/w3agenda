package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Pneureforma;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_pneu", sequenceName = "sq_pneu")
public class Pneu implements Log {

	protected Integer cdpneu;
	protected Pneumarca pneumarca;
	protected Pneumedida pneumedida;
	protected Pneumodelo pneumodelo;
	protected String serie;
	protected String dot;
	protected Material materialbanda;
	protected Pneureforma numeroreforma;
	protected Pneuqualificacao pneuqualificacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Double profundidadesulco; 
	protected OtrPneuTipo otrPneuTipo;
	protected Integer lonas;
	protected PneuSegmento pneuSegmento;
	protected String descricao;
	protected Boolean acompanhaRoda;
	protected List<PneuItemVendaInterface> listaItens;
	
	// TRANSIENTE
	protected Integer cdmaterialbanda;
	protected Double valorColeta;
	protected String nome;
	protected boolean agendaProducaoGerada;
	protected Integer ordemTrans;
	
	public boolean existeDados(){
		return PneuService.getInstance().existeDados(this);
	}
	
	public Pneu() {
	}
	
	public Pneu(Integer cdpneu){
		this.cdpneu = cdpneu;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneu")
	public Integer getCdpneu() {
		return cdpneu;
	}
	
	@DisplayName("Marca")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumarca")
	public Pneumarca getPneumarca() {
		return pneumarca;
	}

	@DisplayName("Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumedida")
	public Pneumedida getPneumedida() {
		return pneumedida;
	}

	@DisplayName("Modelo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumodelo")
	public Pneumodelo getPneumodelo() {
		return pneumodelo;
	}

	@DisplayName("S�rie/Fogo")
	@MaxLength(20)
	public String getSerie() {
		return serie;
	}

	@DisplayName("DOT")
	@MaxLength(4)
	public String getDot() {
		return dot;
	}

	@DisplayName("Banda/Camelback")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialbanda")
	public Material getMaterialbanda() {
		return materialbanda;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("N�mero de reformas")
	public Pneureforma getNumeroreforma() {
		return numeroreforma;
	}
	
	public void setNumeroreforma(Pneureforma numeroreforma) {
		this.numeroreforma = numeroreforma;
	}

	public void setCdpneu(Integer cdpneu) {
		this.cdpneu = cdpneu;
	}

	public void setPneumarca(Pneumarca pneumarca) {
		this.pneumarca = pneumarca;
	}

	public void setPneumedida(Pneumedida pneumedida) {
		this.pneumedida = pneumedida;
	}

	public void setPneumodelo(Pneumodelo pneumodelo) {
		this.pneumodelo = pneumodelo;
	}

	public void setSerie(String serie) {
		this.serie = StringUtils.trimToNull(serie);
	}

	public void setDot(String dot) {
		this.dot = StringUtils.trimToNull(dot);
	}

	public void setMaterialbanda(Material materialbanda) {
		this.materialbanda = materialbanda;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Integer getCdmaterialbanda() {
		return cdmaterialbanda;
	}
	
	public void setCdmaterialbanda(Integer cdmaterialbanda) {
		this.cdmaterialbanda = cdmaterialbanda;
	}
	
	@DisplayName("Qualifica��o do Pneu")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneuqualificacao")
	public Pneuqualificacao getPneuqualificacao() {
		return pneuqualificacao;
	}

	public void setPneuqualificacao(Pneuqualificacao pneuqualificacao) {
		this.pneuqualificacao = pneuqualificacao;
	}
	
	@Transient
	public Double getProfundidadesulco() {
		return profundidadesulco;
	}
	
	public void setProfundidadesulco(Double profundidadesulco) {
		this.profundidadesulco = profundidadesulco;
	}
	
	@DisplayName("Tipo de Pneu OTR")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdOtrPneuTipo")
	public OtrPneuTipo getOtrPneuTipo() {
		return otrPneuTipo;
	}

	public void setOtrPneuTipo(OtrPneuTipo otrPneuTipo) {
		this.otrPneuTipo = otrPneuTipo;
	}

	public Integer getLonas() {
		return lonas;
	}

	public void setLonas(Integer lonas) {
		this.lonas = lonas;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdPneuSegmento")
	public PneuSegmento getPneuSegmento() {
		return pneuSegmento;
	}

	public void setPneuSegmento(PneuSegmento pneuSegmento) {
		this.pneuSegmento = pneuSegmento;
	}

	@DisplayName("Acompanha Roda?")
	public Boolean getAcompanhaRoda() {
		return acompanhaRoda;
	}

	public void setAcompanhaRoda(Boolean acompanhaRoda) {
		this.acompanhaRoda = acompanhaRoda;
	}
	
	@DescriptionProperty
	@Transient
	public String getNome() {
        nome = "PNEU "+br.com.linkcom.neo.util.StringUtils.coalesce(this.getPneumedida() != null ? this.getPneumedida().getNome() : "") +
                " " +br.com.linkcom.neo.util.StringUtils.coalesce(this.getPneumarca() != null? this.getPneumarca().getNome(): "", "") +
                " "+br.com.linkcom.neo.util.StringUtils.coalesce(this.getPneumodelo() != null? this.getPneumodelo().getNome(): "", "") +
                        " "+br.com.linkcom.neo.util.StringUtils.coalesce(this.getSerie(), "");
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

	@Transient
	public Double getValorColeta() {
		return valorColeta;
	}
	public void setValorColeta(Double valorColeta) {
		this.valorColeta = valorColeta;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pneu other = (Pneu) obj;
		if (cdpneu == null) {
			if (other.cdpneu != null)
				return false;
		} else if (!cdpneu.equals(other.cdpneu))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdpneu == null) ? 0 : cdpneu.hashCode());
		return result;
	}
	
	@Transient
	public String getDescriptionAutocompletePneu() {
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCdpneu() != null){
			s.append("<span><BR>C�digo: ").append(this.getCdpneu()).append("</span>");
		}
		if(Util.objects.isPersistent(this.getPneumedida())){
			s.append("<span><BR>Medida: ").append(this.getPneumedida().getNome()).append("</span>");
		}
		if(Util.objects.isPersistent(this.getPneumarca())){
			s.append("<span><BR>Marca: ").append(this.getPneumarca().getNome()).append("</span>");
		}
		if(Util.objects.isPersistent(this.getPneumodelo())){
			s.append("<span><BR>Modelo: ").append(this.getPneumodelo().getNome()).append("</span>");
		}
		if(StringUtils.isNotBlank(this.getSerie())){
			s.append("<span><BR>S�rie / Fogo: ").append(this.getSerie()).append("</span>");
		}
		if(StringUtils.isNotBlank(this.getDot())){
			s.append("<span><BR>DOT: ").append(this.getDot()).append("</span>");
		}
		return s.toString();
	}
	
	@Transient
	public List<PneuItemVendaInterface> getListaItens() {
		return listaItens;
	}
	public void setListaItens(List<PneuItemVendaInterface> listaItens) {
		this.listaItens = listaItens;
	}
	
	@Transient
	public boolean isAgendaProducaoGerada() {
		return agendaProducaoGerada;
	}
	public void setAgendaProducaoGerada(boolean agendaProducaoGerada) {
		this.agendaProducaoGerada = agendaProducaoGerada;
	}
	
	@Transient
	public Integer getOrdemTrans() {
		return ordemTrans;
	}
	public void setOrdemTrans(Integer ordemTrans) {
		this.ordemTrans = ordemTrans;
	}
}