package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de Servi�o")
public class Ordemservicotipo {
	
	public static final Ordemservicotipo AGENDAMENTO = new Ordemservicotipo(0);
	public static final Ordemservicotipo INSPECAO = new Ordemservicotipo(1);
	public static final Ordemservicotipo MANUTENCAO = new Ordemservicotipo(2);
	public static final Integer TIPO_SERVICO_MANUTENCAO =  Integer.valueOf(2);//new Integer(2);
	public static final Integer TIPO_SERVICO_INSPECAO =  Integer.valueOf(1);//new Integer(1);
	
	
	protected Integer cdordemservicotipo;
	protected String nome;
	
	public Ordemservicotipo() {
	}
	
	public Ordemservicotipo(Integer cdordemservicotipo) {
		this.cdordemservicotipo = cdordemservicotipo;
	}	
	
	@Id
	public Integer getCdordemservicotipo() {
		return cdordemservicotipo;
	}
	
	@DescriptionProperty
	@DisplayName("Tipo de servi�o")
	public String getNome() {
		return nome;
	}
	@Transient
	public static Integer getTIPO_SERVICO_INPECAO() {
		return TIPO_SERVICO_INSPECAO;
	}
	
	@Transient
	public static Integer getTIPO_SERVICO_MANUTENCAO() {
		return TIPO_SERVICO_MANUTENCAO;
	}
	
	public void setCdordemservicotipo(Integer cdordemservicotipo) {
		this.cdordemservicotipo = cdordemservicotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Ordemservicotipo) {
			Ordemservicotipo tipoServico = (Ordemservicotipo) obj;
			return tipoServico.getCdordemservicotipo().equals(this.getCdordemservicotipo());
		}
		return super.equals(obj);
	}
	

}
