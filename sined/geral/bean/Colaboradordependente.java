package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_colaboradordependente", sequenceName = "sq_colaboradordependente")
public class Colaboradordependente implements Log{

	protected Integer cdcolaboradordependente;
	protected Colaborador colaborador;
	
	protected String nome;
	protected Tipodependente tipodependente;
	protected Date dtbaixairrf;
		
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Date dtnascimento;
	protected String cartorio;
	protected String registro;
	protected String livro;
	protected String folha;
	protected Date dtbaixasf;
	protected Date dtentregacertidao;
	protected Date dtpensao;
	protected Date dtbaixapensao;
	protected Sexo sexo;
	protected Colaboradorformapagamento colaboradorformapagamento;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradordependente")
	public Integer getCdcolaboradordependente() {
		return cdcolaboradordependente;
	}
	public void setCdcolaboradordependente(Integer id) {
		this.cdcolaboradordependente = id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@MaxLength(50)
	@DescriptionProperty
	@DisplayName("Nome")
	@Required
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipodependente")
	@Required
	@DisplayName("Tipo de depend�ncia")
	public Tipodependente getTipodependente() {
		return tipodependente;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipodependente(Tipodependente tipodependente) {
		this.tipodependente = tipodependente;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	
	@DisplayName("Data de baixa do IRRF")
	public Date getDtbaixairrf() {
		return dtbaixairrf;
	}
	
	@Required
	@DisplayName("Data de nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}
	
	@DisplayName("Cart�rio")
	@MaxLength(15)
	public String getCartorio() {
		return cartorio;
	}
	
	@MaxLength(10)
	public String getRegistro() {
		return registro;
	}
	
	@MaxLength(10)
	public String getLivro() {
		return livro;
	}
	
	@MaxLength(5)
	public String getFolha() {
		return folha;
	}
	
	@DisplayName("Data baixa sal�rio")
	public Date getDtbaixasf() {
		return dtbaixasf;
	}
	
	@DisplayName("Data de entrega da certid�o")
	public Date getDtentregacertidao() {
		return dtentregacertidao;
	}
	
	@DisplayName("Data de aquisi��o da pens�o")
	public Date getDtpensao() {
		return dtpensao;
	}
	
	@DisplayName("Data de baixa da pens�o aliment�cia")
	public Date getDtbaixapensao() {
		return dtbaixapensao;
	}
	@Required
	@JoinColumn(name="cdsexo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Sexo getSexo() {
		return sexo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorformapagamento")
	@DisplayName("Forma de pagamento")
	public Colaboradorformapagamento getColaboradorformapagamento() {
		return colaboradorformapagamento;
	}
	
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setCartorio(String cartorio) {
		this.cartorio = cartorio;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public void setLivro(String livro) {
		this.livro = livro;
	}
	public void setFolha(String folha) {
		this.folha = folha;
	}
	public void setDtbaixairrf(Date dtbaixairrf) {
		this.dtbaixairrf = dtbaixairrf;
	}
	public void setDtbaixasf(Date dtbaixasalario) {
		this.dtbaixasf = dtbaixasalario;
	}
	public void setDtentregacertidao(Date dtentregacertidao) {
		this.dtentregacertidao = dtentregacertidao;
	}
	public void setDtpensao(Date dtaquisicaopensao) {
		this.dtpensao = dtaquisicaopensao;
	}
	public void setDtbaixapensao(Date dtbaixapensao) {
		this.dtbaixapensao = dtbaixapensao;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setColaboradorformapagamento(Colaboradorformapagamento colaboradorformapagamento) {
		this.colaboradorformapagamento = colaboradorformapagamento;
	}
	

}
