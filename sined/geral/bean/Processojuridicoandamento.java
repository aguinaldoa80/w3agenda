package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoenvolvido;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_processojuridicoandamento", sequenceName = "sq_processojuridicoandamento")
@DisplayName("Andamento")
public class Processojuridicoandamento implements Log {

	protected Integer cdprocessojuridicoandamento;
	protected Tipoenvolvido tipoenvolvido;
	protected Cliente cliente;
	protected Partecontraria partecontraria;
	protected Processojuridicoinstancia processojuridicoinstancia;
	protected Date data;
	protected String andamento;
	protected String providencia;
	protected String complemento;
	protected String identificador;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridicoandamento")
	public Integer getCdprocessojuridicoandamento() {
		return cdprocessojuridicoandamento;
	}
	@Required
	public Date getData() {
		return data;
	}
	@Required
	@MaxLength(2000)
	public String getAndamento() {
		return andamento;
	}
	@MaxLength(2000)
	@DisplayName("Providência")
	public String getProvidencia() {
		return providencia;
	}
	@DisplayName("Instância")
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridicoinstancia")
	public Processojuridicoinstancia getProcessojuridicoinstancia() {
		return processojuridicoinstancia;
	}
	
	@DisplayName("Envolvido")
	public Tipoenvolvido getTipoenvolvido() {
		return tipoenvolvido;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Parte contrária")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpartecontraria")
	public Partecontraria getPartecontraria() {
		return partecontraria;
	}
	
	@DisplayName("Complemento")
	public String getComplemento() {
		return complemento;
	}
	
	@DisplayName("Identificador")
	public String getIdentificador() {
		return identificador;
	}
	
	public void setTipoenvolvido(Tipoenvolvido tipoenvolvido) {
		this.tipoenvolvido = tipoenvolvido;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setPartecontraria(Partecontraria partecontraria) {
		this.partecontraria = partecontraria;
	}
	public void setCdprocessojuridicoandamento(Integer cdprocessojuridicoandamento) {
		this.cdprocessojuridicoandamento = cdprocessojuridicoandamento;
	}
	public void setData(java.util.Date dtandamento) {
		this.data = dtandamento;
	}
	public void setAndamento(String andamento) {
		this.andamento = andamento;
	}
	public void setProvidencia(String providencia) {
		this.providencia = providencia;
	}
	public void setProcessojuridicoinstancia(
			Processojuridicoinstancia processojuridicoinstancia) {
		this.processojuridicoinstancia = processojuridicoinstancia;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	/* Transient */
	protected String inserirandamento;
	
	@Transient
	public String getInserirandamento() {
		return inserirandamento;
	}
	public void setInserirandamento(String inserirandamento) {
		this.inserirandamento = inserirandamento;
	}

}
