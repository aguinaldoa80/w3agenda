package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_agendamentoservicohistorico", sequenceName = "sq_agendamentoservicohistorico")
@DisplayName("Agendamento Servi�o Hist�rico")
public class Agendamentoservicohistorico implements Log {

	protected Integer cdagendamentoservicohistorico;
	protected String observacao;
	protected Agendamentoservico agendamentoservico;
	protected Agendamentoservicoacao agendamentoservicoacao;
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	protected List<Agendamentoservicohistoricopapel> listaagendamentohistoricopapel = new ListSet<Agendamentoservicohistoricopapel>(Agendamentoservicohistoricopapel.class);
	
	protected Agendamentoservicocancela agendamentoservicocancela;
	
	public Agendamentoservicohistorico(){
	}

	public Agendamentoservicohistorico(Agendamentoservicoacao agendamentoservicoacao){
		this.agendamentoservicoacao = agendamentoservicoacao;
	}

	public Agendamentoservicohistorico(String observacao, Agendamentoservico agendamentoservico, Agendamentoservicoacao agendamentoservicoacao, List<Agendamentoservicohistoricopapel> listaagendamentohistoricopapel){
		this.observacao = observacao;
		this.agendamentoservico =agendamentoservico;
		this.agendamentoservicoacao = agendamentoservicoacao;
		this.listaagendamentohistoricopapel = listaagendamentohistoricopapel;
	}
	
	public Agendamentoservicohistorico(String observacao, String whereIn, Agendamentoservicoacao agendamentoservicoacao){
		this.observacao = observacao;
		this.whereIn = whereIn;
		this.agendamentoservicoacao = agendamentoservicoacao;
	}
	
	//Transient's
	protected String whereIn;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamentoservicohistorico")
	public Integer getCdagendamentoservicohistorico() {
		return cdagendamentoservicohistorico;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	@DisplayName("A��o")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservicoacao")
	public Agendamentoservicoacao getAgendamentoservicoacao() {
		return agendamentoservicoacao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoservico")
	public Agendamentoservico getAgendamentoservico() {
		return agendamentoservico;
	}
	@OneToMany(mappedBy="agendamentoservicohistorico")
	public List<Agendamentoservicohistoricopapel> getListaagendamentohistoricopapel() {
		return listaagendamentohistoricopapel;
	}
	
	public void setCdagendamentoservicohistorico(
			Integer cdagendamentoservicohistorico) {
		this.cdagendamentoservicohistorico = cdagendamentoservicohistorico;
	}
	public void setAgendamentoservicoacao(
			Agendamentoservicoacao agendamentoservicoacao) {
		this.agendamentoservicoacao = agendamentoservicoacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setAgendamentoservico(Agendamentoservico agendamentoservico) {
		this.agendamentoservico = agendamentoservico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setListaagendamentohistoricopapel(
			List<Agendamentoservicohistoricopapel> listaagendamentohistoricopapel) {
		this.listaagendamentohistoricopapel = listaagendamentohistoricopapel;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@Transient
	@DisplayName("Motivo do cancelamento")
	public Agendamentoservicocancela getAgendamentoservicocancela() {
		return agendamentoservicocancela;
	}
	
	public void setAgendamentoservicocancela(
			Agendamentoservicocancela agendamentoservicocancela) {
		this.agendamentoservicocancela = agendamentoservicocancela;
	}
}
