package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.util.Log;

@SequenceGenerator(name = "sq_colaboradordespesaitem", sequenceName = "sq_colaboradordespesaitem")
@DisplayName("Despesa")
@Entity
public class Colaboradordespesaitem implements Log{

	protected Integer cdcolaboradordespesaitem;	
	protected Colaboradordespesa colaboradordespesa;
	protected Colaboradordespesaitemtipo tipo;
	protected EventoPagamento evento;
	protected String motivo;
	protected Money valor;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName ("Id")
	@GeneratedValue (strategy=GenerationType.AUTO, generator="sq_colaboradordespesaitem")
	public Integer getCdcolaboradordespesaitem() {
		return cdcolaboradordespesaitem;
	}
	
	public void setCdcolaboradordespesaitem(Integer cdcolaboradordespesaitem) {
		this.cdcolaboradordespesaitem = cdcolaboradordespesaitem;
	}
	
	@DisplayName ("Tipo de opera��o")
	public Colaboradordespesaitemtipo getTipo() {
		return tipo;
	}
	public void setTipo(Colaboradordespesaitemtipo tipo) {
		this.tipo = tipo;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEvento() {
		return evento;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
	
	@DisplayName ("Motivo")
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	@Required
	@DisplayName ("Valor")
	public Money getValor() {
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesa")
	public Colaboradordespesa getColaboradordespesa() {
		return colaboradordespesa;
	}

	public void setColaboradordespesa(Colaboradordespesa colaboradordespesa) {
		this.colaboradordespesa = colaboradordespesa;
	}
	
	
	
}
