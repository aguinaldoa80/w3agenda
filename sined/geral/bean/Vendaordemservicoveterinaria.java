package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_vendaordemservicoveterinaria", sequenceName = "sq_vendaordemservicoveterinaria")
public class Vendaordemservicoveterinaria {

	protected Integer cdvendaordemservicoveterinaria;
	protected Venda venda;
	protected Ordemservicoveterinaria ordemservicoveterinaria;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendaordemservicoveterinaria")
	public Integer getCdvendaordemservicoveterinaria() {
		return cdvendaordemservicoveterinaria;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	
	public void setCdvendaordemservicoveterinaria(Integer cdvendaordemservicoveterinaria) {
		this.cdvendaordemservicoveterinaria = cdvendaordemservicoveterinaria;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setOrdemservicoveterinaria(Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	
}
