package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Camponotaproduto;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_empresaconfiguracaonfprodutocampo", sequenceName = "sq_empresaconfiguracaonfprodutocampo")
public class Empresaconfiguracaonfprodutocampo implements Log {
	
	protected Integer cdempresaconfiguracaonfprodutocampo;
	protected Empresaconfiguracaonfproduto empresaconfiguracaonfproduto;
	protected Boolean corpo;
	protected Camponotaproduto camponota;
	protected String campolivre;
	protected Integer tamanhomax;
	protected Integer coluna;
	protected Integer linha;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresaconfiguracaonfprodutocampo")
	public Integer getCdempresaconfiguracaonfprodutocampo() {
		return cdempresaconfiguracaonfprodutocampo;
	}
	
	@MaxLength(100)
	@DisplayName("Campo livre")
	public String getCampolivre() {
		return campolivre;
	}
	
	@JoinColumn(name="cdempresaconfiguracaonfproduto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresaconfiguracaonfproduto getEmpresaconfiguracaonfproduto() {
		return empresaconfiguracaonfproduto;
	}

	public Boolean getCorpo() {
		return corpo;
	}

	@DisplayName("Campo")
	@Required
	public Camponotaproduto getCamponota() {
		return camponota;
	}

	@DisplayName("Tamanho m�ximo")
	public Integer getTamanhomax() {
		return tamanhomax;
	}

	@Required
	public Integer getColuna() {
		return coluna;
	}

	public Integer getLinha() {
		return linha;
	}

	public void setEmpresaconfiguracaonfproduto(Empresaconfiguracaonfproduto empresaconfiguracaonfproduto) {
		this.empresaconfiguracaonfproduto = empresaconfiguracaonfproduto;
	}

	public void setCorpo(Boolean corpo) {
		this.corpo = corpo;
	}

	public void setCamponota(Camponotaproduto camponota) {
		this.camponota = camponota;
	}

	public void setCampolivre(String campolivre) {
		this.campolivre = campolivre;
	}

	public void setTamanhomax(Integer tamanhomax) {
		this.tamanhomax = tamanhomax;
	}

	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}

	public void setLinha(Integer linha) {
		this.linha = linha;
	}

	public void setCdempresaconfiguracaonfprodutocampo(
			Integer cdempresaconfiguracaonfprodutocampo) {
		this.cdempresaconfiguracaonfprodutocampo = cdempresaconfiguracaonfprodutocampo;
	}

	
	// LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}