package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servicoweb", sequenceName = "sq_servicoweb")
@DisplayName("Web")
public class Servicoweb implements Log{
	
	protected Integer cdservicoweb;
	protected Contratomaterial contratomaterial;
	protected Servicoftp servicoftp;
	protected Boolean php;
	protected Boolean jsp;
	protected Boolean cgiperl;
	protected Set<Servicowebalias> listaServicowebalias;
	protected Dominio dominio;
	protected Servidor servidor;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	

	//Transient
	protected Cliente cliente;
	
	public Servicoweb() {
	}
	
	public Servicoweb(Integer cdservicoweb) {
		this.cdservicoweb = cdservicoweb;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicoweb")
	public Integer getCdservicoweb() {
		return cdservicoweb;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoftp")
	@DisplayName("FTP")		
	public Servicoftp getServicoftp() {
		return servicoftp;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio principal")		
	public Dominio getDominio() {
		return dominio;
	}

	@DisplayName("PHP")
	public Boolean getPhp() {
		return php;
	}

	@DisplayName("JSP")
	public Boolean getJsp() {
		return jsp;
	}

	@DisplayName("Cgi/Perl")
	public Boolean getCgiperl() {
		return cgiperl;
	}

	@OneToMany(mappedBy="servicoweb")
	@DisplayName("Alias")
	public Set<Servicowebalias> getListaServicowebalias() {
		return listaServicowebalias;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")
	public Servidor getServidor() {
		return servidor;
	}
	
	public void setCdservicoweb(Integer cdservicoweb) {
		this.cdservicoweb = cdservicoweb;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setServicoftp(Servicoftp servicoftp) {
		this.servicoftp = servicoftp;
	}

	public void setPhp(Boolean php) {
		this.php = php;
	}

	public void setJsp(Boolean jsp) {
		this.jsp = jsp;
	}

	public void setCgiperl(Boolean cgiperl) {
		this.cgiperl = cgiperl;
	}
	
	public void setListaServicowebalias(Set<Servicowebalias> listaServicowebalias) {
		this.listaServicowebalias = listaServicowebalias;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
	
	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
		
	@Transient	
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
		
}
