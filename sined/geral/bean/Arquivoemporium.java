package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivoemporium", sequenceName = "sq_arquivoemporium")
public class Arquivoemporium implements Log {

	protected Integer cdarquivoemporium;
	protected Arquivoemporiumtipo arquivoemporiumtipo;
	protected Date dtmovimento;
	protected Arquivo arquivo;
	protected Arquivo arquivoresultado;
	protected Boolean processado;
	protected Boolean enviadoemporium = false;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivoemporium")
	public Integer getCdarquivoemporium() {
		return cdarquivoemporium;
	}
	
	@DisplayName("Tipo")
	public Arquivoemporiumtipo getArquivoemporiumtipo() {
		return arquivoemporiumtipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Arquivo de Resultado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoresultado")
	public Arquivo getArquivoresultado() {
		return arquivoresultado;
	}
	
	public Boolean getProcessado() {
		return processado;
	}
	
	@DisplayName("Data do movimento")
	public Date getDtmovimento() {
		return dtmovimento;
	}
	
	public Boolean getEnviadoemporium() {
		return enviadoemporium;
	}
	
	public void setEnviadoemporium(Boolean enviadoemporium) {
		this.enviadoemporium = enviadoemporium;
	}
	
	public void setDtmovimento(Date dtmovimento) {
		this.dtmovimento = dtmovimento;
	}
	
	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}
	
	public void setArquivoresultado(Arquivo arquivoresultado) {
		this.arquivoresultado = arquivoresultado;
	}

	public void setCdarquivoemporium(Integer cdarquivoemporium) {
		this.cdarquivoemporium = cdarquivoemporium;
	}

	public void setArquivoemporiumtipo(Arquivoemporiumtipo arquivoemporiumtipo) {
		this.arquivoemporiumtipo = arquivoemporiumtipo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
