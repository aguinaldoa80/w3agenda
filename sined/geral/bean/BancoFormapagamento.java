package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoformapagamento", sequenceName="sq_bancoformapagamento")
public class BancoFormapagamento {
	
	protected Integer cdbancoformapagamento;
	protected Banco banco;
	protected String descricao;
	protected Integer identificador;
	
	public BancoFormapagamento() {
		super();
	}
	
	public BancoFormapagamento(Integer cdbancoformapagamento) {
		super();
		this.cdbancoformapagamento = cdbancoformapagamento;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoformapagamento")
	public Integer getCdbancoformapagamento() {
		return cdbancoformapagamento;
	}
	public void setCdbancoformapagamento(Integer cdbancoformapagamento) {
		this.cdbancoformapagamento = cdbancoformapagamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	public Banco getBanco() {
		return banco;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Required
	@DisplayName("Identificador")
	@MaxLength(3)
	public Integer getIdentificador() {
		return identificador;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	
}
