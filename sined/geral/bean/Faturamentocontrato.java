package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_faturamentocontrato", sequenceName = "sq_faturamentocontrato")
public class Faturamentocontrato implements Log{

	protected Integer cdfaturamentocontrato;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<NotaContrato> listaNotascontrato;
	protected List<Despesaviagem> listaDespesaviagem;
	
	//TRANSIENT'S
	protected Contrato contrato;
	protected String notasservico;
	protected String notasproduto;
	protected String despesasviagens;
	protected Money valortotal;
	protected Money valortotalservico;
	protected Money valortotalproduto;
	protected Money valortotalaux;
	protected Money valortotaldespesaviagem;
	
	public Faturamentocontrato() {
	}
	
	public Faturamentocontrato(Integer cdfaturamentocontrato, String nomeCliente) {
		this.cdfaturamentocontrato = cdfaturamentocontrato;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_faturamentocontrato")
	public Integer getCdfaturamentocontrato() {
		return cdfaturamentocontrato;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setCdfaturamentocontrato(Integer cdfaturamentocontrato) {
		this.cdfaturamentocontrato = cdfaturamentocontrato;
	}
	
	@OneToMany(mappedBy="faturamentocontrato")
	public List<NotaContrato> getListaNotascontrato() {
		return listaNotascontrato;
	}
	
	public void setListaNotascontrato(List<NotaContrato> listaNotascontrato) {
		this.listaNotascontrato = listaNotascontrato;
	}
	
	@Transient
	public Contrato getContrato() {
		if(this.listaNotascontrato != null && !this.listaNotascontrato.isEmpty())
			return this.listaNotascontrato.get(0).getContrato();
		return contrato;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	@Transient
	public String getNotasservico() {
		setValortotalservico(new Money(0.0));
		
		StringBuilder sb = new StringBuilder("");
		if(this.listaNotascontrato != null && !this.listaNotascontrato.isEmpty()){
			for (NotaContrato notaContrato : this.listaNotascontrato){ 
				if(notaContrato.getNota() instanceof NotaFiscalServico){
					Money valorBruto = ((NotaFiscalServico)notaContrato.getNota()).getValorNota();
					sb.append(notaContrato.getNota().getNumero() != null && !notaContrato.getNota().getNumero().equals("") ? notaContrato.getNota().getNumero() : "<sem n�mero>").append(" (R$").append(valorBruto).append(") | ");
					valortotalservico = valortotalservico.add(valorBruto);
				}
			}
			if(sb.toString() != null && !sb.toString().equals(""))
				sb.delete(sb.length()-3, sb.length());
		}
		return sb.toString();
	}
	@Transient
	public String getNotasproduto() {
		setValortotalproduto(new Money(0.0));
		
		StringBuilder sb = new StringBuilder("");
		if(this.listaNotascontrato != null && !this.listaNotascontrato.isEmpty()){
			for (NotaContrato notaContrato : this.listaNotascontrato) {
				if(notaContrato.getNota() instanceof Notafiscalproduto){
					Money valor = ((Notafiscalproduto)notaContrato.getNota()).getValor();
					sb.append(notaContrato.getNota().getNumero() != null && !notaContrato.getNota().getNumero().equals("") ? notaContrato.getNota().getNumero() : "<sem n�mero>").append(" (R$").append(valor).append(") | ");
					valortotalproduto = valortotalproduto.add(valor);
				}
			}
			
			if(sb.toString() != null && !sb.toString().equals(""))
				sb.delete(sb.length()-3, sb.length());
		}
		return sb.toString();
	}
	@Transient
	public Money getValortotal() {
		return getValortotalservico().add(getValortotalproduto()).add(getValortotaldespesaviagem());
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setNotasservico(String notasservico) {
		this.notasservico = notasservico;
	}
	public void setNotasproduto(String notasproduto) {
		this.notasproduto = notasproduto;
	}
	@Transient
	public Money getValortotalservico() {
		if(this.valortotalservico == null) this.valortotalservico = new Money(0.0);
		return valortotalservico;
	}
	@Transient
	public Money getValortotalproduto() {
		if(this.valortotalproduto == null) this.valortotalproduto = new Money(0.0);
		return valortotalproduto;
	}
	public void setValortotalservico(Money valortotalservico) {
		this.valortotalservico = valortotalservico;
	}
	public void setValortotalproduto(Money valortotalproduto) {
		this.valortotalproduto = valortotalproduto;
	}
	@Transient
	public Money getValortotalaux() {
		Money valortotal = new Money(0.0);
		if(this.listaNotascontrato != null && !this.listaNotascontrato.isEmpty()){
			for (NotaContrato notaContrato : this.listaNotascontrato) {
				if(notaContrato.getNota() instanceof Notafiscalproduto)
					valortotal = valortotal.add(((Notafiscalproduto)notaContrato.getNota()).getValor());
				else
					valortotal = valortotal.add(((NotaFiscalServico)notaContrato.getNota()).getValorNota());
			}
		}
		if(this.listaDespesaviagem != null && !this.listaDespesaviagem.isEmpty()){
			Money valorrealizado = new Money();
			for (Despesaviagem despesaviagem : this.listaDespesaviagem) {
				valorrealizado = despesaviagem.getTotalrealizado();
				if(valorrealizado != null){
					valortotal = valortotal.add(valorrealizado);
				}
			}
		}
		return valortotal;
	}
	public void setValortotalaux(Money valortotalaux) {
		this.valortotalaux = valortotalaux;
	}

	@Transient
	public List<Despesaviagem> getListaDespesaviagem() {
		return listaDespesaviagem;
	}
	@Transient
	public String getDespesasviagens() {
		setValortotaldespesaviagem(new Money(0.0));
		
		StringBuilder sb = new StringBuilder("");
		Money valorrealizado = new Money();
		if(this.listaDespesaviagem != null && !this.listaDespesaviagem.isEmpty()){
			for (Despesaviagem despesaviagem : this.listaDespesaviagem) {
				valorrealizado = despesaviagem.getTotalrealizado();
				if(valorrealizado != null){
					if(!sb.toString().equals("")) sb.append(" | ");
					if(despesaviagem.getDescricao() != null && !despesaviagem.getDescricao().equals("")){
						sb.append(despesaviagem.getDescricao()).append(" (R$").append(valorrealizado).append(")");
					}else {
						sb.append(despesaviagem.getDestinoviagem() != null && !despesaviagem.getDestinoviagem().equals("") ? despesaviagem.getDestinoviagem() : "").append(" (R$").append(valorrealizado).append(")");
					}
					valortotaldespesaviagem = valortotaldespesaviagem.add(valorrealizado);
				}
			}
		}
		return sb.toString();
	}
	@Transient
	public Money getValortotaldespesaviagem() {
		return valortotaldespesaviagem;
	}

	public void setListaDespesaviagem(List<Despesaviagem> listaDespesaviagem) {
		this.listaDespesaviagem = listaDespesaviagem;
	}
	public void setDespesasviagens(String despesasviagens) {
		this.despesasviagens = despesasviagens;
	}
	public void setValortotaldespesaviagem(Money valortotaldespesaviagem) {
		this.valortotaldespesaviagem = valortotaldespesaviagem;
	}
	
	@Transient
	public List<Rateioitem> getListarateioitemWithProjeto(){
		List<Rateioitem> listaRateioitem = new ArrayList<Rateioitem>();
		Rateioitem rateioitem;
		if(this.listaDespesaviagem != null && !this.listaDespesaviagem.isEmpty()){
			for (Despesaviagem despesaviagem : this.listaDespesaviagem) {
				if(despesaviagem.getListaDespesaviagemprojeto() != null && !despesaviagem.getListaDespesaviagemprojeto().isEmpty()){
					for(Despesaviagemprojeto despesaviagemprojeto : despesaviagem.getListaDespesaviagemprojeto()){
						if(despesaviagemprojeto.getProjeto() != null && despesaviagemprojeto.getPercentual() != null){
							rateioitem = new Rateioitem();
							rateioitem.setProjeto(despesaviagemprojeto.getProjeto());
							if(despesaviagemprojeto.getPercentual() != null)
							rateioitem.setPercentual(despesaviagemprojeto.getPercentual().getValue().doubleValue());
							if(despesaviagem.getEmpresa() != null){
								rateioitem.setCentrocusto(despesaviagem.getEmpresa().getCentrocustodespesaviagem());
								rateioitem.setContagerencial(despesaviagem.getEmpresa().getContagerencialcredito());
								listaRateioitem.add(rateioitem);
							}
						}
					}
				}
			}
		}
		return listaRateioitem;
	}
}
