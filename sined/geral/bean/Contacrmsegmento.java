package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contacrmsegmento", sequenceName = "sq_contacrmsegmento")
public class Contacrmsegmento {
	
	protected Integer cdcontacrmsegmento;
	protected Segmento segmento;
	protected Contacrm contacrm;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrmsegmento")
	public Integer getCdcontacrmsegmento() {
		return cdcontacrmsegmento;
	}
	
	@Required
	@DisplayName("Segmento")
	@JoinColumn(name="cdsegmento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Segmento getSegmento() {
		return segmento;
	}
	
	@DisplayName("Conta")
	@JoinColumn(name="cdcontacrm")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrm getContacrm() {
		return contacrm;
	}
	
	
	
	
	public void setCdcontacrmsegmento(Integer cdcontacrmsegmento) {
		this.cdcontacrmsegmento = cdcontacrmsegmento;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	
	
	
	
	

}
