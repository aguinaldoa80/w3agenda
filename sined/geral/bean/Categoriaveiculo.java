package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontroleveiculo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_categoriaveiculo", sequenceName = "sq_categoriaveiculo")
public class Categoriaveiculo implements Log{

	protected Integer cdcategoriaveiculo;
	protected String nome;
	protected Tipocontroleveiculo tipocontroleveiculo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Categoriaiteminspecao> listaCategoriaiteminspecao = new ListSet<Categoriaiteminspecao>(Categoriaiteminspecao.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_categoriaveiculo")
	public Integer getCdcategoriaveiculo() {
		return cdcategoriaveiculo;
	}
	@Required
	@DisplayName("Nome")
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("Tipo de controle")
	public Tipocontroleveiculo getTipocontroleveiculo() {
		return tipocontroleveiculo;
	}
	@DisplayName("Itens de inspe��o")
	@OneToMany(mappedBy="categoriaveiculo")
	public List<Categoriaiteminspecao> getListaCategoriaiteminspecao() {
		return listaCategoriaiteminspecao;
	}
	
	public void setListaCategoriaiteminspecao(
			List<Categoriaiteminspecao> listaCategoriaiteminspecao) {
		this.listaCategoriaiteminspecao = listaCategoriaiteminspecao;
	}
	
	public void setCdcategoriaveiculo(Integer cdcategoriaveiculo) {
		this.cdcategoriaveiculo = cdcategoriaveiculo;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setTipocontroleveiculo(Tipocontroleveiculo tipocontroleveiculo) {
		this.tipocontroleveiculo = tipocontroleveiculo;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
