package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_coletamaterialmotivodevolucao", sequenceName = "sq_coletamaterialmotivodevolucao")
public class Coletamaterialmotivodevolucao {
	
	protected Integer cdcoletamaterialmotivodevolucao;
	protected ColetaMaterial coletaMaterial;
	protected Motivodevolucao motivodevolucao;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_coletamaterialmotivodevolucao")
	public Integer getCdcoletamaterialmotivodevolucao() {
		return cdcoletamaterialmotivodevolucao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoletamaterial")
	public ColetaMaterial getColetaMaterial() {
		return coletaMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivodevolucao")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public void setCdcoletamaterialmotivodevolucao(Integer cdcoletamaterialmotivodevolucao) {
		this.cdcoletamaterialmotivodevolucao = cdcoletamaterialmotivodevolucao;
	}
	public void setColetaMaterial(ColetaMaterial coletaMaterial) {
		this.coletaMaterial = coletaMaterial;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
}