package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name = "sq_agendamentoservicodocumento", sequenceName = "sq_agendamentoservicodocumento")
public class Agendamentoservicodocumento {

	protected Integer cdagendamentoservicodocumento;
	protected Date dtagendamento = new Date(System.currentTimeMillis());
	
	protected List<Documentoorigem> listaDocumentoorigem;
	protected List<Agendamentoservico> listaAgendamentoservico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamentoservicodocumento")
	public Integer getCdagendamentoservicodocumento() {
		return cdagendamentoservicodocumento;
	}
	
	public Date getDtagendamento() {
		return dtagendamento;
	}

	public void setDtagendamento(Date dtagendamento) {
		this.dtagendamento = dtagendamento;
	}

	public void setCdagendamentoservicodocumento(
			Integer cdagendamentoservicodocumento) {
		this.cdagendamentoservicodocumento = cdagendamentoservicodocumento;
	}
	
	@OneToMany(mappedBy="agendamentoservicodocumento")
	public List<Documentoorigem> getListaDocumentoorigem() {
		return listaDocumentoorigem;
	}
	
	@OneToMany(mappedBy="agendamentoservicodocumento")
	public List<Agendamentoservico> getListaAgendamentoservico() {
		return listaAgendamentoservico;
	}
	
	public void setListaAgendamentoservico(
			List<Agendamentoservico> listaAgendamentoservico) {
		this.listaAgendamentoservico = listaAgendamentoservico;
	}

	public void setListaDocumentoorigem(
			List<Documentoorigem> listaDocumentoorigem) {
		this.listaDocumentoorigem = listaDocumentoorigem;
	}
	
}
