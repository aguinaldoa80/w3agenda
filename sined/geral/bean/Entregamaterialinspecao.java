package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_entregamaterialinspecao", sequenceName = "sq_entregamaterialinspecao")
public class Entregamaterialinspecao implements Log {

	protected Integer cdentregamaterialinspecao;
	protected Entregamaterial entregamaterial;
	protected Inspecaoitem inspecaoitem;
	protected String observacao;
	protected Boolean conforme;	
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
//	TRANSIENT
	protected Boolean conformetrans;
	
	public Entregamaterialinspecao(){
	}
	
	public Entregamaterialinspecao(Entregamaterial entregamaterial, Inspecaoitem inspecaoitem){
		this.entregamaterial = entregamaterial;
		this.inspecaoitem = inspecaoitem;
	}
	
	@Id
	@GeneratedValue(generator="sq_entregamaterialinspecao",strategy=GenerationType.AUTO)
	public Integer getCdentregamaterialinspecao() {
		return cdentregamaterialinspecao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinspecaoitem")
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@MaxLength(500)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Conforme?")
	public Boolean getConforme() {
		return conforme;
	}
	
	public void setCdentregamaterialinspecao(Integer cdentregamaterialinspecao) {
		this.cdentregamaterialinspecao = cdentregamaterialinspecao;
	}
	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setConforme(Boolean conforme) {
		this.conforme = conforme;
	}
	
	//========================= TRANSIENT'S ================================
	
	@Transient
	public Boolean getConformetrans(){
		return this.conforme; 
	}
	public void setConformetrans(Boolean conformetrans) {
		this.conformetrans = conformetrans;
	}
}
