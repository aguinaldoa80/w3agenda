package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraConvenio;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraFinalidadearquivo;
import br.com.linkcom.sined.geral.bean.enumeration.SIntegraNaturezaoperacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_sintegra", sequenceName = "sq_sintegra")
public class Sintegra implements Log{

	protected Integer cdsintegra;
	protected Arquivo arquivo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Empresa empresa;
	protected SIntegraConvenio convenio;
	protected SIntegraNaturezaoperacao naturezaoperacao;
	protected SIntegraFinalidadearquivo finalidadearquivo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_sintegra")
	public Integer getCdsintegra() {
		return cdsintegra;
	}
	@DisplayName("Arquivo SIntegra")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Data de In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data Final")
	public Date getDtfim() {
		return dtfim;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Conv�nio")
	public SIntegraConvenio getConvenio() {
		return convenio;
	}
	@DisplayName("Natureza das opera��es identificadas")
	public SIntegraNaturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	@DisplayName("Finalidade do arquivo")
	public SIntegraFinalidadearquivo getFinalidadearquivo() {
		return finalidadearquivo;
	}	
	
	public void setCdsintegra(Integer cdsintegra) {
		this.cdsintegra = cdsintegra;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConvenio(SIntegraConvenio convenio) {
		this.convenio = convenio;
	}
	public void setNaturezaoperacao(SIntegraNaturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setFinalidadearquivo(SIntegraFinalidadearquivo finalidadearquivo) {
		this.finalidadearquivo = finalidadearquivo;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
