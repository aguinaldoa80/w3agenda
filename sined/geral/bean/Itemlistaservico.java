package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_itemlistaservico", sequenceName = "sq_itemlistaservico")
public class Itemlistaservico implements Log {

	protected Integer cditemlistaservico;
	protected String codigo;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_itemlistaservico")
	public Integer getCditemlistaservico() {
		return cditemlistaservico;
	}
	
	@Required
	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@Required
	@MaxLength(1000)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setCditemlistaservico(Integer cditemlistaservico) {
		this.cditemlistaservico = cditemlistaservico;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTE
	
	@Transient
	@DescriptionProperty(usingFields={"codigo","descricao"})
	public String getDescricaoCombo(){
		StringBuilder sb = new StringBuilder();
		if(this.codigo != null && this.descricao != null){
			sb.append(this.codigo).append(" - ").append(this.descricao);
		}
		return sb.toString();
	}
	
}
