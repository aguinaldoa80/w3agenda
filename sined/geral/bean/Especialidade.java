package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_especialidade", sequenceName="sq_especialidade")
public class Especialidade implements Log {
	
	private Integer cdespecialidade;
	private String descricao;
	private Conselhoclasseprofissional conselhoclasseprofissional;
	private Boolean ativo = Boolean.TRUE;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	public Especialidade() {
	}
	
	public Especialidade(Integer cdespecialidade) {
		super();
		this.cdespecialidade = cdespecialidade;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_especialidade")
	@DisplayName("C�digo")
	public Integer getCdespecialidade() {
		return cdespecialidade;
	}
	@Required
	@DisplayName("Especialidade")
	@MaxLength(200)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Conselho de Classe Profissional")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconselhoclasseprofissional")
	public Conselhoclasseprofissional getConselhoclasseprofissional() {
		return conselhoclasseprofissional;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdespecialidade(Integer cdespecialidade) {
		this.cdespecialidade = cdespecialidade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setConselhoclasseprofissional(Conselhoclasseprofissional conselhoclasseprofissional) {
		this.conselhoclasseprofissional = conselhoclasseprofissional;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdespecialidade == null) ? 0 : cdespecialidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Especialidade other = (Especialidade) obj;
		if (cdespecialidade == null) {
			if (other.cdespecialidade != null)
				return false;
		} else if (!cdespecialidade.equals(other.cdespecialidade))
			return false;
		return true;
	}	
	
}