package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_leadhistorico", sequenceName = "sq_leadhistorico")
public class Leadhistorico implements Log{
	
	protected Integer cdleadhistorico;
	protected String observacao;
	protected Lead lead;
	protected Atividadetipo atividadetipo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	//TRANSIENT
	protected Double qtdetotal;
	protected String atividadenome;
	protected String nomeusuarioaltera;
	
	public Leadhistorico() {}
	
	public Leadhistorico(Double qtdetotal, String atividadenome) {
		this.qtdetotal = qtdetotal;
		this.atividadenome = atividadenome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_leadhistorico")
	public Integer getCdleadhistorico() {
		return cdleadhistorico;
	}

	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Lead")
	@JoinColumn(name="cdlead")
	@ManyToOne(fetch=FetchType.LAZY)
	public Lead getLead() {
		return lead;
	}
	
	
	public void setCdleadhistorico(Integer cdleadhistorico) {
		this.cdleadhistorico = cdleadhistorico;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
	
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}


	
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
	@Transient
	@DisplayName("Responsável")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	@DisplayName("Tipo de Atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}

	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}

	@Transient
	public Double getQtdetotal() {
		return qtdetotal;
	}
	@Transient
	public String getAtividadenome() {
		return atividadenome;
	}

	public void setQtdetotal(Double qtdetotal) {
		this.qtdetotal = qtdetotal;
	}
	public void setAtividadenome(String atividadenome) {
		this.atividadenome = atividadenome;
	}

	@Transient
	public String getUsuarioaltera(){
		if(getCdusuarioaltera() != null){
			return TagFunctions.findUserByCd(getCdusuarioaltera());
		} else return null;
	}
	
	@Transient
	public String getNomeusuarioaltera() {
		return this.getUsuarioaltera();
	}

	public void setNomeusuarioaltera(String nomeusuarioaltera) {
		this.nomeusuarioaltera = nomeusuarioaltera;
	}

	
}
