package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_tipoexame", sequenceName = "sq_tipoexame")
public class Tipoexame {

	protected Integer cdtipoexame;
	protected String nome;
	
	public static final Integer PRE_ADMISSIONAL = 1;
	public static final Integer PERIODICO = 2;
	public static final Integer MUDANCA_FUNCAO = 3;
	public static final Integer DEMISSIONAL = 4;
	public static final Integer AVALIACAO = 5;
	public static final Integer RETORNO_TRABALHO = 6;
	public static final Integer AUDIOMETRIA = 7;
	public static final Integer HEMOGRAMA = 8;
	public static final Integer RAIOX = 9;
	public static final Integer OUTROS = 10;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoexame")
	public Integer getCdtipoexame() {
		return cdtipoexame;
	}
	public void setCdtipoexame(Integer id) {
		this.cdtipoexame = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
