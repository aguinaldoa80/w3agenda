package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.sined.util.Log;

@Entity
public class Cacheusuariooffline implements Log{
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Cacheusuariooffline(){
		
	}
	
	public Cacheusuariooffline(Integer cdusuarioaltera){
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Cacheusuariooffline(Integer cdusuarioaltera, Timestamp timestamp){
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = timestamp;
	}
	
	@Id
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
