package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_formularhvariavel", sequenceName="sq_formularhvariavel")
public class Formularhvariavel {
	
	protected Integer cdformularhvariavel;
	protected Boolean considerarintervalo;
	protected String nome;	
	protected Boolean ativa;
	protected Double valor;
	protected Integer cdusuarioaltera;
	protected Date dtaltera;
	protected Formularh formularh;
	protected List<Formularhvariavelintervalo> formularhvariavelintervalo;
	protected String funcaoClass;
	
	@Id
	@GeneratedValue(generator="sq_formularhvariavel",strategy=GenerationType.AUTO)
	public Integer getCdformularhvariavel() {
		return cdformularhvariavel;
	}
	public Boolean getConsiderarintervalo() {
		return considerarintervalo;
	}
	public String getNome() {
		return nome;
	}
	public Boolean getAtiva() {
		return ativa;
	}
	public Double getValor() {
		return valor;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Date getDtaltera() {
		return dtaltera;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformularh")
	public Formularh getFormularh() {
		return formularh;
	}		
	public String getFuncaoClass() {
		return funcaoClass;
	}
	@OneToMany(mappedBy="formularhvariavel")
	public List<Formularhvariavelintervalo> getFormularhvariavelintervalo() {
		return formularhvariavelintervalo;
	}
	public void setFuncaoClass(String funcaoClass) {
		this.funcaoClass = funcaoClass;
	}
	public void setFormularhvariavelintervalo(List<Formularhvariavelintervalo> formularhvariavelintervalo) {
		this.formularhvariavelintervalo = formularhvariavelintervalo;
	}
	public void setFormularh(Formularh formularh) {
		this.formularh = formularh;
	}
	public void setCdformularhvariavel(Integer cdformularhvariavel) {
		this.cdformularhvariavel = cdformularhvariavel;
	}
	public void setConsiderarintervalo(Boolean considerarintervalo) {
		this.considerarintervalo = considerarintervalo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Date dtaltera) {
		this.dtaltera = dtaltera;
	}		
}