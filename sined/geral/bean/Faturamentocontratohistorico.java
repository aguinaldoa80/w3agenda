package br.com.linkcom.sined.geral.bean;



import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_faturamentocontratohistorico", sequenceName = "sq_faturamentocontratohistorico")
public class Faturamentocontratohistorico implements Log {
	
	protected Integer cdfaturamentocontratohistorico; 
	protected Contrato contrato;
	protected Date dtvencimento;
	protected Boolean finalizado;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_faturamentocontratohistorico")
	public Integer getCdfaturamentocontratohistorico() {
		return cdfaturamentocontratohistorico;
	}
	
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public void setCdfaturamentocontratohistorico(
			Integer cdfaturamentocontratohistorico) {
		this.cdfaturamentocontratohistorico = cdfaturamentocontratohistorico;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}