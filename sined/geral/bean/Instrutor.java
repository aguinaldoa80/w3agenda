package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Ministrador")
@SequenceGenerator(name = "sq_instrutor", sequenceName = "sq_instrutor")
public class Instrutor implements Log{

	protected Integer cdinstrutor;
	protected String nome;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_instrutor")
	public Integer getCdinstrutor() {
		return cdinstrutor;
	}
	
	public void setCdinstrutor(Integer cdinstrutor) {
		this.cdinstrutor = cdinstrutor;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}	
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Instrutor) {
			Instrutor c = (Instrutor) obj;
			return c.getCdinstrutor().equals(this.getCdinstrutor());
		}
		return super.equals(obj);
	}
	
}
