package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contratosincronizacaoaa", sequenceName = "sq_contratosincronizacaoaa")
@Table(name="contratosincronizacaoaa")
public class ContratosincronizacaoAA {
	
	protected Integer cdcontratosincronizacaoaa;
	protected Cliente cliente;
	protected Date dtsincronizacao;
	protected String mensagem;
	protected Integer id;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratosincronizacaoaa")
	public Integer getCdcontratosincronizacaoaa() {
		return cdcontratosincronizacaoaa;
	}
	
	@DisplayName("Cliente")
	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	
	public Date getDtsincronizacao() {
		return dtsincronizacao;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public void setCdcontratosincronizacaoaa(Integer cdcontratosincronizacaoaa) {
		this.cdcontratosincronizacaoaa = cdcontratosincronizacaoaa;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setDtsincronizacao(Date dtsincronizacao) {
		this.dtsincronizacao = dtsincronizacao;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
}
