package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_configuracaoecomerro", sequenceName = "sq_configuracaoecomerro")
public class Configuracaoecomerro {

	protected Integer cdconfiguracaoecomerro;
	protected Configuracaoecom configuracaoecom;
	protected Timestamp dtinicioerro;
	protected Timestamp dtultimoerro;
	protected Integer contador;
	protected String mensagem;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaoecomerro")
	public Integer getCdconfiguracaoecomerro() {
		return cdconfiguracaoecomerro;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaoecom")	
	public Configuracaoecom getConfiguracaoecom() {
		return configuracaoecom;
	}

	public Timestamp getDtinicioerro() {
		return dtinicioerro;
	}

	public Timestamp getDtultimoerro() {
		return dtultimoerro;
	}

	public Integer getContador() {
		return contador;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setCdconfiguracaoecomerro(Integer cdconfiguracaoecomerro) {
		this.cdconfiguracaoecomerro = cdconfiguracaoecomerro;
	}

	public void setConfiguracaoecom(Configuracaoecom configuracaoecom) {
		this.configuracaoecom = configuracaoecom;
	}

	public void setDtinicioerro(Timestamp dtinicioerro) {
		this.dtinicioerro = dtinicioerro;
	}

	public void setDtultimoerro(Timestamp dtultimoerro) {
		this.dtultimoerro = dtultimoerro;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}