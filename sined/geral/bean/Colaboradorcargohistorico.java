package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradorcargohistoricoacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_colaboradorcargohistorico", sequenceName = "sq_colaboradorcargohistorico")
public class Colaboradorcargohistorico implements Log{
	
	protected Integer cdcolaboradorcargohistorico;
	protected Colaboradorcargo colaboradorcargo;
	protected Colaboradorcargohistoricoacao acao;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcargohistorico")
	public Integer getCdcolaboradorcargohistorico() {
		return cdcolaboradorcargohistorico;
	}
	@DisplayName("Colaborador Cargo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorcargo")
	public Colaboradorcargo getColaboradorcargo() {
		return colaboradorcargo;
	}
	@DisplayName("A��o")
	public Colaboradorcargohistoricoacao getAcao() {
		return acao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdcolaboradorcargohistorico(Integer cdcolaboradorcargohistorico) {
		this.cdcolaboradorcargohistorico = cdcolaboradorcargohistorico;
	}
	public void setColaboradorcargo(Colaboradorcargo colaboradorcargo) {
		this.colaboradorcargo = colaboradorcargo;
	}
	public void setAcao(Colaboradorcargohistoricoacao acao) {
		this.acao = acao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
		
}
