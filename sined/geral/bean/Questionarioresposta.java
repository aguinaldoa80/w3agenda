package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_questionarioresposta", sequenceName = "sq_questionarioresposta")
@DisplayName("Respostas")
public class Questionarioresposta {
	
	protected Integer cdquestionarioresposta;
	protected String resposta;
	protected Integer pontuacao;
	protected Questionarioquestao questionarioquestao;
	protected Boolean sim;
	
	public Questionarioresposta() {}
	
	public Questionarioresposta(Integer cdquestionarioresposta) {
		this.cdquestionarioresposta = cdquestionarioresposta;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_questionarioresposta")
	public Integer getCdquestionarioresposta() {
		return cdquestionarioresposta;
	}
	@Required
	@DisplayName("Resposta")
	@DescriptionProperty
	@MaxLength(200)
	public String getResposta() {
		return resposta;
	}
	@DisplayName("Pontua��o")
	public Integer getPontuacao() {
		return pontuacao;
	}
	@JoinColumn(name="cdquestionarioquestao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Questionarioquestao getQuestionarioquestao() {
		return questionarioquestao;
	}

	@Transient
	public Boolean getSim() {
		return sim;
	}
	
	public void setQuestionarioquestao(Questionarioquestao questionarioquestao) {
		this.questionarioquestao = questionarioquestao;
	}
	public void setCdquestionarioresposta(Integer cdquestionarioresposta) {
		this.cdquestionarioresposta = cdquestionarioresposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	public void setSim(Boolean sim) {
		this.sim = sim;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Questionarioresposta) {
			Questionarioresposta questionarioresposta = (Questionarioresposta) obj;
			return questionarioresposta.getCdquestionarioresposta().equals(this.getCdquestionarioresposta());
		}
		return super.equals(obj);
	}
}
