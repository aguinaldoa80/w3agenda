package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_notafiscalprodutoretornovenda", sequenceName = "sq_notafiscalprodutoretornovenda")
public class Notafiscalprodutoretornovenda{

	protected Integer cdnotafiscalprodutoretornovenda;
	protected Venda venda;
	protected Notafiscalproduto notafiscalproduto;
	
	public Notafiscalprodutoretornovenda() {}
	
	public Notafiscalprodutoretornovenda(Integer cdvenda, Integer cdnotafiscalproduto) {
		this.venda = new Venda(cdvenda);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutoretornovenda")
	public Integer getCdnotafiscalprodutoretornovenda() {
		return cdnotafiscalprodutoretornovenda;
	}
	public void setCdnotafiscalprodutoretornovenda(Integer cdnotafiscalprodutoretornovenda) {
		this.cdnotafiscalprodutoretornovenda = cdnotafiscalprodutoretornovenda;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
}