package br.com.linkcom.sined.geral.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Situacaorequisicao {
	
	EM_ABERTO				(0,"EM ABERTO"),
	AUTORIZADA				(1,"AUTORIZADA"),
	EM_PROCESSO_COMPRA		(2,"EM PROCESSO DE COMPRA"),
	COMPRA_FINALIZADA		(3,"COMPRA FINALIZADA"),
	BAIXADA_PARCIAL			(4,"BAIXADA PARCIAL"),
	BAIXADA					(5,"BAIXADA"),
	CANCELADA				(6,"CANCELADA"),
	ESTORNAR				(7,"ESTORNAR"),
	ROMANEIO_GERADO			(8,"ROMANEIO GERADO"),
	ROMANEIO_COMPLETO		(9,"ROMANEIO COMPLETO"),
	COMPRA_NAO_AUTORIZADA	(10,"Compra n�o autorizada"),;
	
	private Situacaorequisicao (Integer codigo){
		this.codigo = codigo;
	}
	
	private Situacaorequisicao (Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	private Integer codigo;
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
	/**
	 * Concatena os c�digos das situa��es
	 * @param situacoes
	 * @return
	 * @author Tom�s Rabelo
	 */
	public static String listAndConcatenate(List<Situacaorequisicao> situacoes){
		StringBuilder codigos = new StringBuilder();
		for (int i = 0; i < situacoes.size(); i++)
			for (Situacaorequisicao s : Situacaorequisicao.values()){
				boolean existe = false;
				if(situacoes.get(i) instanceof Situacaorequisicao){
					if((situacoes.get(i)).equals(s)){
						existe = true;
					}
				} else{
					if(((Object)situacoes.get(i)).equals(s.name())){
						existe = true;
					}
				}
				if(existe){
					codigos.append(s.getCodigo()+",");
					break;
				}
			}
		
		if(codigos.length() > 0) codigos.delete(codigos.length()-1, codigos.length());
		return codigos.toString();
	}
	
}
