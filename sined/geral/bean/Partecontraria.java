package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_partecontraria", sequenceName = "sq_partecontraria")
public class Partecontraria implements Log {

	protected Integer cdpartecontraria;
	protected String nome;
	protected Tipopessoa tipopessoa;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected Cep cep;
	protected String logradouro;
	protected String numero;
	protected String complemento;
	protected String bairro;
	protected Municipio municipio;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	// TRANSIENTE
	protected Integer qtdeProcessos = 0;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_partecontraria")
	public Integer getCdpartecontraria() {
		return cdpartecontraria;
	}
	
	@Required
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}

	@DisplayName("CEP")
	public Cep getCep() {
		return cep;
	}

	@MaxLength(100)
	public String getLogradouro() {
		return logradouro;
	}

	@DisplayName("N�mero")
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}

	@MaxLength(50)
	public String getComplemento() {
		return complemento;
	}

	@MaxLength(50)
	public String getBairro() {
		return bairro;
	}

	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setCdpartecontraria(Integer cdpartecontraria) {
		this.cdpartecontraria = cdpartecontraria;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setCep(Cep cep) {
		this.cep = cep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	// TRANSIENTE
	
	@Transient
	@DisplayName("CNPJ/CPF")
	public String getCpfcnpj() {
		return getCnpj() != null ? getCnpj().toString() : (getCpf() != null ? getCpf().toString() : "");
	}
	
	@Transient
	@DisplayName("Qtde. de processos")
	public Integer getQtdeProcessos() {
		return qtdeProcessos;
	}
	
	public void setQtdeProcessos(Integer qtdeProcessos) {
		this.qtdeProcessos = qtdeProcessos;
	}

	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
