package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_usuariodepartamento", sequenceName = "sq_usuariodepartamento")
public class Usuariodepartamento{

	protected Integer cdusuariodepartamento;
	protected Usuario usuario;
	protected Departamento departamento;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_usuariodepartamento")
	public Integer getCdusuariodepartamento() {
		return cdusuariodepartamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@Required
	public Usuario getUsuario() {
		return usuario;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	@Required
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setCdusuariodepartamento(Integer cdusuariodepartamento) {
		this.cdusuariodepartamento = cdusuariodepartamento;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
}
