package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_ambienterede", sequenceName = "sq_ambienterede")
public class Ambienterede implements Log{

	protected Integer cdambienterede;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ambienterede")
	public Integer getCdambienterede() {
		return cdambienterede;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setCdambienterede(Integer cdambienterede) {
		this.cdambienterede = cdambienterede;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
}
