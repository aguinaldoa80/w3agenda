package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_menu_chave", sequenceName = "sq_menu_chave")
public class Menu implements Log {

	protected Integer cdmenu;
	protected String nome;
	protected String url;
	protected Menu menupai;
	protected Menumodulo menumodulo;
	protected Integer ordem;
	protected String contexto;
	protected String chave;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Integer getCdmenu() {
		return cdmenu;
	}
	
	@Required
	@MaxLength(500)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	@MaxLength(500)
	@DisplayName("URL")
	public String getUrl() {
		return url;
	}

	public Integer getOrdem() {
		return ordem;
	}
	
	@DisplayName("Menu pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="chavepai")
	public Menu getMenupai() {
		return menupai;
	}

	@DisplayName("M�dulo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmenumodulo")
	public Menumodulo getMenumodulo() {
		return menumodulo;
	}

	@MaxLength(50)
	public String getContexto() {
		return contexto;
	}

	@Id
	@Required
	@MaxLength(50)
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_menu_chave")
	public String getChave() {
		return chave;
	}
	
	public void setChave(String chave) {
		this.chave = chave;
	}
	
	public void setMenupai(Menu menupai) {
		this.menupai = menupai;
	}

	public void setMenumodulo(Menumodulo menumodulo) {
		this.menumodulo = menumodulo;
	}

	public void setContexto(String contexto) {
		this.contexto = contexto;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCdmenu(
			Integer cdmenu) {
		this.cdmenu = cdmenu;
	}
	
	
//	LOG

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdmenu == null) ? 0 : cdmenu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menu other = (Menu) obj;
		if (cdmenu == null) {
			if (other.cdmenu != null)
				return false;
		} else if (!cdmenu.equals(other.cdmenu))
			return false;
		return true;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"nome","chave", "url"})
	public String getDescricaoCombo(){
		StringBuilder sb = new StringBuilder();
		
		if(this.chave != null){
			sb.append(this.chave).append(" - ");
		}
		if(this.nome != null){
			sb.append(this.nome);
		}
		if(this.url != null){
			sb.append(" (").append(this.url).append(")");
		}
		
		return sb.toString();
	}
	
}
