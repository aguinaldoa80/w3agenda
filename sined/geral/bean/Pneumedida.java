package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Medida de Pneu")
@SequenceGenerator(name = "sq_pneumedida", sequenceName = "sq_pneumedida")
public class Pneumedida implements Log {

	protected Integer cdpneumedida;
	protected String nome;
	protected String codigointegracao;
	protected Double circunferencia;
	protected Boolean otr;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Pneumedida(){}
	
	public Pneumedida(Integer cdpneumedida){
		this.cdpneumedida = cdpneumedida;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneumedida")
	public Integer getCdpneumedida() {
		return cdpneumedida;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(20)
	@DisplayName("Código p/ integração")
	public String getCodigointegracao() {
		return codigointegracao;
	}
	
	@MaxLength(20)
	@DisplayName("Circunferência do Pneu")
	public Double getCircunferencia() {
		return circunferencia;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdpneumedida(Integer cdpneumedida) {
		this.cdpneumedida = cdpneumedida;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}

	public void setCodigointegracao(String codigointegracao) {
		this.codigointegracao = codigointegracao;
	}

	public void setCircunferencia(Double circunferencia) {
		this.circunferencia = circunferencia;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@DisplayName("OTR")
	public Boolean getOtr() {
		return otr;
	}

	public void setOtr(Boolean otr) {
		this.otr = otr;
	}
	
	
}