package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_formularhcargo", sequenceName="sq_formularhcargo")
public class Formularhcargo {

	protected Integer cdformularhcargo;
	protected Formularh formularh;
	protected Cargo cargo;
	
	@Id
	@GeneratedValue(generator="sq_formularhcargo",strategy=GenerationType.AUTO)
	public Integer getCdformularhcargo() {
		return cdformularhcargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformularh")
	public Formularh getFormularh() {
		return formularh;
	}
	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}	
	public void setCdformularhcargo(Integer cdformularhcargo) {
		this.cdformularhcargo = cdformularhcargo;
	}
	public void setFormularh(Formularh formularh) {
		this.formularh = formularh;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
		
}