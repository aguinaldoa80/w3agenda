package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
public class Servicoservidortipo implements Log{

	protected Integer cdservicoservidortipo;
	protected String nome;
	protected Materialtipo materialtipo;
	protected Boolean servicorede;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	

	public static Integer FTP = 1;
	public static Integer WEB = 2;
	public static Integer CAIXAPOSTAL = 3;
	public static Integer SMTP = 4;
	public static Integer SMTPAV = 5;		
	public static Integer DOMINIO = 6;
	public static Integer DHCP = 7;
	public static Integer ARP = 8;
	public static Integer DHCP_ARP = 9;
	public static Integer AUTENTICACAO_RADIUS = 10;
	public static Integer DHCP_RADIUS = 11;
	public static Integer BANCO_DADOS = 12;
	public static Integer FIREWALL = 13;
	public static Integer NAT = 14;
	public static Integer BANDA = 15;	
	public static Integer EMAIL = 16;	
	public static Integer DOMINIO_REDE = 17;	
	public static Integer SERVICO_SERVIDOR = 18;	
	public static Integer ROTA = 19;	
	public static Integer HOTSPOT = 20;

	public static Servicoservidortipo HOTSPOT_OBJ = new Servicoservidortipo(HOTSPOT);
	public static Servicoservidortipo FTP_OBJ = new Servicoservidortipo(FTP);
	public static Servicoservidortipo WEB_OBJ = new Servicoservidortipo(WEB);
	public static Servicoservidortipo CAIXAPOSTAL_OBJ = new Servicoservidortipo(CAIXAPOSTAL);
	public static Servicoservidortipo SMTP_OBJ = new Servicoservidortipo(SMTP);
	public static Servicoservidortipo SMTPAV_OBJ = new Servicoservidortipo(SMTPAV);		
	public static Servicoservidortipo DOMINIO_OBJ = new Servicoservidortipo(DOMINIO);
	public static Servicoservidortipo DHCP_OBJ = new Servicoservidortipo(DHCP);
	public static Servicoservidortipo ARP_OBJ = new Servicoservidortipo(ARP);
	public static Servicoservidortipo DHCP_ARP_OBJ = new Servicoservidortipo(DHCP_ARP);
	public static Servicoservidortipo AUTENTICACAO_RADIUS_OBJ = new Servicoservidortipo(AUTENTICACAO_RADIUS);
	public static Servicoservidortipo DHCP_RADIUS_OBJ = new Servicoservidortipo(DHCP_RADIUS);
	public static Servicoservidortipo BANCO_DADOS_OBJ = new Servicoservidortipo(BANCO_DADOS);
	public static Servicoservidortipo FIREWALL_OBJ = new Servicoservidortipo(FIREWALL);
	public static Servicoservidortipo NAT_OBJ = new Servicoservidortipo(NAT);
	public static Servicoservidortipo BANDA_OBJ = new Servicoservidortipo(BANDA);	
	public static Servicoservidortipo EMAIL_OBJ = new Servicoservidortipo(EMAIL);	
	public static Servicoservidortipo DOMINIO_REDE_OBJ = new Servicoservidortipo(DOMINIO_REDE);	
	public static Servicoservidortipo SERVICO_SERVIDOR_OBJ = new Servicoservidortipo(SERVICO_SERVIDOR);	
	public static Servicoservidortipo ROTA_OBJ = new Servicoservidortipo(ROTA);	

	public Servicoservidortipo(){
	}

	public Servicoservidortipo(Integer cdservicoservidortipo){
		this.cdservicoservidortipo = cdservicoservidortipo;
	}
	
	@Id
	@DisplayName("Id")
	public Integer getCdservicoservidortipo() {
		return cdservicoservidortipo;
	}

	@Required
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtipo")
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	
	@DisplayName("Servi�o de rede")
	public Boolean getServicorede() {
		return servicorede;
	}
	
	public void setCdservicoservidortipo(Integer cdservicoservidortipo) {
		this.cdservicoservidortipo = cdservicoservidortipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	
	public void setServicorede(Boolean servicorede) {
		this.servicorede = servicorede;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdservicoservidortipo == null) ? 0 : cdservicoservidortipo
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Servicoservidortipo other = (Servicoservidortipo) obj;
		if (cdservicoservidortipo == null) {
			if (other.cdservicoservidortipo != null)
				return false;
		} else if (!cdservicoservidortipo.equals(other.cdservicoservidortipo))
			return false;
		return true;
	}
	
	
}
