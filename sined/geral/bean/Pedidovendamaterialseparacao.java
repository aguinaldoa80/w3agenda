package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_pedidovendamaterialseparacao", sequenceName = "sq_pedidovendamaterialseparacao")
public class Pedidovendamaterialseparacao {

	protected Integer cdpedidovendamaterialseparacao;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Unidademedida unidademedida;
	protected Double quantidade;
	
	//Transientes
	protected Double fracao;
	protected Double qtdereferencia;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendamaterialseparacao")
	public Integer getCdpedidovendamaterialseparacao() {
		return cdpedidovendamaterialseparacao;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setCdpedidovendamaterialseparacao(
			Integer cdpedidovendamaterialseparacao) {
		this.cdpedidovendamaterialseparacao = cdpedidovendamaterialseparacao;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	//Transientes
	
	@Transient
	public Double getFracao() {
		return fracao;
	}

	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}

	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
}
