package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_aviso", sequenceName = "sq_aviso")
@DisplayName("Aviso geral")
@JoinEmpresa("aviso.empresa")
public class Aviso implements Log {

	protected Integer cdaviso;
	protected String assunto;
	protected String complemento;
	protected Date dtaviso;
	protected Date dtfim;
	protected Tipoaviso tipoaviso;
	protected AvisoOrigem avisoOrigem;
	protected Integer idOrigem;
	protected Motivoaviso motivoaviso;
	protected Boolean notifica;
	
	protected Usuario usuario;
	protected Papel papel;
	
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	protected Empresa empresa;
	protected Categoria categoria;
	protected Cliente cliente;
	
	protected List<Avisousuario> listaAvisousuario; 
	
	// TRANSIENT'S
	protected Integer cdavisousuario;
	protected String lido;
	protected Integer avisoOrigemCodigo;
	protected String whereInProjeto;
	protected List<Usuario> listaEnvolvidos;
	
	public Aviso(){
	}
	
	public Aviso(String assunto, String complemento, Tipoaviso tipoaviso, Papel papel, AvisoOrigem avisoOrigem, Integer idOrigem, Empresa empresa, Motivoaviso motivoAviso){
		this.assunto = assunto;
		this.complemento= complemento;
		this.tipoaviso = tipoaviso;
		this.papel = papel;
		this.dtaviso = new Date(System.currentTimeMillis());
		this.avisoOrigem = avisoOrigem;
		this.idOrigem = idOrigem;
		this.empresa = empresa;
		this.motivoaviso = motivoAviso;
	}
	
	public Aviso(String assunto, String complemento, Tipoaviso tipoaviso, Papel papel, AvisoOrigem avisoOrigem, Integer idOrigem, Empresa empresa, Date dtaviso, Motivoaviso motivoaviso){
		this.assunto = assunto;
		this.complemento= complemento;
		this.tipoaviso = tipoaviso;
		this.papel = papel;
		this.dtaviso = dtaviso;
		this.avisoOrigem = avisoOrigem;
		this.idOrigem = idOrigem;
		this.empresa = empresa;
		this.motivoaviso = motivoaviso;
	}
	
	public Aviso(String assunto, String complemento, Tipoaviso tipoaviso, Papel papel, AvisoOrigem avisoOrigem, Integer idOrigem, Empresa empresa, Date dtaviso, Motivoaviso motivoaviso, Boolean notifica){
		this.assunto = assunto;
		this.complemento= complemento;
		this.tipoaviso = tipoaviso;
		this.papel = papel;
		this.dtaviso = dtaviso;
		this.avisoOrigem = avisoOrigem;
		this.idOrigem = idOrigem;
		this.empresa = empresa;
		this.motivoaviso = motivoaviso;
		this.notifica = notifica;
	}
	
	public Aviso(String assunto, String complemento, Tipoaviso tipoaviso, Papel papel, Usuario usuario, AvisoOrigem avisoOrigem, Integer idOrigem, Empresa empresa, String whereInProjeto, Motivoaviso motivoAviso){
		this.assunto = assunto;
		this.complemento= complemento;
		this.tipoaviso = tipoaviso;
		this.papel = papel;
		this.usuario = usuario;
		this.dtaviso = new Date(System.currentTimeMillis());
		this.avisoOrigem = avisoOrigem;
		this.idOrigem = idOrigem;
		this.empresa = empresa;
		this.whereInProjeto = whereInProjeto;
		this.motivoaviso = motivoAviso;
	}

	public Aviso(String assunto, String complemento, Tipoaviso tipoaviso, AvisoOrigem avisoOrigem, Integer idOrigem, Motivoaviso motivoAviso, Empresa empresa){
		this.assunto = assunto;
		this.complemento= complemento;
		this.tipoaviso = tipoaviso;
		this.dtaviso = new Date(System.currentTimeMillis());
		this.avisoOrigem = avisoOrigem;
		this.idOrigem = idOrigem;
		this.motivoaviso = motivoAviso;
		this.empresa = empresa;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_aviso")
	@DisplayName("Aviso geral")
	public Integer getCdaviso() {
		return cdaviso;
	}
	@MaxLength(100)
	@Required
	public String getAssunto() {
		return assunto;
	}
	@MaxLength(1000)
	@Required
	public String getComplemento() {
		return complemento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@DisplayName("Usu�rio")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	@DisplayName("N�vel")
	public Papel getPapel() {
		return papel;
	}
	@Required
	@DisplayName("Data de aviso")
	public Date getDtaviso() {
		return dtaviso;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoria")
	public Categoria getCategoria() {
		return categoria;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Notifica��es")
	@OneToMany(mappedBy="aviso")
	public List<Avisousuario> getListaAvisousuario() {
		return listaAvisousuario;
	}

	@Required
	@DisplayName("Tipo de aviso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoaviso")
	public Tipoaviso getTipoaviso() {
		return tipoaviso;
	}

	@Required
	@DisplayName("Motivo do aviso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivoaviso")
	public Motivoaviso getMotivoaviso() {
		return motivoaviso;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public AvisoOrigem getAvisoOrigem() {
		return avisoOrigem;
	}
	public Integer getIdOrigem() {
		return idOrigem;
	}
	public Boolean getNotifica() {
		return notifica;
	}
	public void setNotifica(Boolean notifica) {
		this.notifica = notifica;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setListaAvisousuario(List<Avisousuario> listaAvisousuario) {
		this.listaAvisousuario = listaAvisousuario;
	}
	public void setIdOrigem(Integer idOrigem) {
		this.idOrigem = idOrigem;
	}	
	public void setAvisoOrigem(AvisoOrigem avisoOrigem) {
		this.avisoOrigem = avisoOrigem;
	}
	public void setCdaviso(Integer cdaviso) {
		this.cdaviso = cdaviso;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public void setDtaviso(Date dtaviso) {
		this.dtaviso = dtaviso;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setTipoaviso(Tipoaviso tipoaviso) {
		this.tipoaviso = tipoaviso;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setMotivoaviso(Motivoaviso motivoaviso) {
		this.motivoaviso = motivoaviso;
	}

	@Transient
	public Integer getCdavisousuario() {
		return cdavisousuario;
	}
	@Transient
	public String getLido() {
		return lido;
	}
	@Transient
	public Integer getAvisoOrigemCodigo() {
		return avisoOrigem != null ? avisoOrigem.getCodigo() : null;
	}
	public void setAvisoOrigemCodigo(Integer avisoOrigemCodigo) {
		this.avisoOrigemCodigo = avisoOrigemCodigo;
	}
	public void setCdavisousuario(Integer cdavisousuario) {
		this.cdavisousuario = cdavisousuario;
	}
	public void setLido(String lido) {
		this.lido = lido;
	}

	@Transient
	public String getWhereInProjeto() {
		return whereInProjeto;
	}

	public void setWhereInProjeto(String whereInProjeto) {
		this.whereInProjeto = whereInProjeto;
	}

	@Transient
	public String getTextoNotificacao(){
		String texto = "";
		if(StringUtils.isNotBlank(assunto)){
			texto = assunto;
		}
		if(StringUtils.isNotBlank(complemento)){
			if(StringUtils.isNotBlank(texto)) texto += " ";
			texto += complemento;
		}
		
		return texto;
	}
	@Transient
	public List<Usuario> getListaEnvolvidos() {
		return listaEnvolvidos;
	}

	public void setListaEnvolvidos(List<Usuario> listaEnvolvidos) {
		this.listaEnvolvidos = listaEnvolvidos;
	}
	
	
	
	
}
