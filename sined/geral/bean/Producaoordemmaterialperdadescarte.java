package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_producaoordemmaterialperdadescarte", sequenceName = "sq_producaoordemmaterialperdadescarte")
public class Producaoordemmaterialperdadescarte {

	private Integer cdproducaoordemmaterialperdadescarte;
	private Motivodevolucao motivodevolucao;
	private Double qtdeperdadescarte;
	private Producaoordemmaterial producaoordemmaterial;
	private Producaoordemmaterialoperador producaoordemmaterialoperador;
	
	//TRANSIENTS
	private Boolean isPorOperador;
	private String qtdeperdadescartetrans;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_producaoordemmaterialperdadescarte",strategy=GenerationType.AUTO)
	public Integer getCdproducaoordemmaterialperdadescarte() {
		return cdproducaoordemmaterialperdadescarte;
	}
	public void setCdproducaoordemmaterialperdadescarte(
			Integer cdproducaoordemmaterialperdadescarte) {
		this.cdproducaoordemmaterialperdadescarte = cdproducaoordemmaterialperdadescarte;
	}
	
	@Required
	@DisplayName("Motivo de devolu��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivodevolucao")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	
	@Required
	@DisplayName("Quantidade por motivo")
	public Double getQtdeperdadescarte() {
		return qtdeperdadescarte;
	}
	public void setQtdeperdadescarte(Double qtdeperdadescarte) {
		this.qtdeperdadescarte = qtdeperdadescarte;
	}
	
	@JoinColumn(name="cdproducaoordemmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	public void setProducaoordemmaterial(
			Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	
	@JoinColumn(name="cdproducaoordemmaterialoperador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordemmaterialoperador getProducaoordemmaterialoperador() {
		return producaoordemmaterialoperador;
	}
	public void setProducaoordemmaterialoperador(
			Producaoordemmaterialoperador producaoordemmaterialoperador) {
		this.producaoordemmaterialoperador = producaoordemmaterialoperador;
	}
	
	@Transient
	public Boolean getIsPorOperador() {
		if(isPorOperador == null)
			isPorOperador = this.producaoordemmaterialoperador != null;
		return isPorOperador;
	}
	public void setIsPorOperador(Boolean isPorOperador) {
		this.isPorOperador = isPorOperador;
	}
	
	@Transient
	public String getQtdeperdadescartetrans() {
		qtdeperdadescartetrans = this.qtdeperdadescarte != null? SinedUtil.descriptionDecimal(this.qtdeperdadescarte): "";
		return qtdeperdadescartetrans;
	}
	public void setQtdeperdadescartetrans(String qtdeperdadescartetrans) {
		this.qtdeperdadescartetrans = qtdeperdadescartetrans;
	}
}
