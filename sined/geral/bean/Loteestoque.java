package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_loteestoque;
import br.com.linkcom.sined.geral.service.LotematerialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_loteestoque", sequenceName="sq_loteestoque")
public class Loteestoque implements PermissaoFornecedorEmpresa {

	protected Integer cdloteestoque;
	protected String numero;
	protected Date validade;
	protected Fornecedor fornecedor;
	protected List<Lotematerial> listaLotematerial;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Aux_loteestoque aux_loteestoque;
	
//	TRANSIENT
	protected String numerovalidade;
	protected Double qtde;
	protected Double qtdeReservada;
	protected String numerovalidadeqtde;
	protected Boolean naoExibirQtde;
	protected Integer cdmaterial;
	protected String materialStr;
	protected String validadeStr;
	protected String DescriptionAutocomplete;
	protected Fornecedor fornecedorTrans;
	private String fabricacaoStr;
	protected Boolean fromImportacaoXml;

	
	public Loteestoque(){}
	
	public Loteestoque(Integer cdloteestoque){
		this.cdloteestoque = cdloteestoque;
	}
	
	public Loteestoque(Integer cdloteestoque, String numero) {
		this.cdloteestoque = cdloteestoque;
		this.numero = numero;
	}
	
	public Loteestoque(Integer cdloteestoque, String numero, Date validade) {
		this.cdloteestoque = cdloteestoque;
		this.numero = numero;
		this.validade = validade;
		this.aux_loteestoque = new Aux_loteestoque();
		this.aux_loteestoque.setCdloteestoque(cdloteestoque);
		this.aux_loteestoque.setValidade(validade);
	}
	
	public Loteestoque(Integer cdloteestoque, String numero, Date validade, Double qtde) {
		this.cdloteestoque = cdloteestoque;
		this.numero = numero;
		this.aux_loteestoque = new Aux_loteestoque();
		this.aux_loteestoque.setCdloteestoque(cdloteestoque);
		this.aux_loteestoque.setValidade(validade);
		this.qtde = qtde;
	}
	
	public Loteestoque(Integer cdmaterial, Integer cdloteestoque, String numero, Date validade, Double qtde) {
		this.cdloteestoque = cdloteestoque;
		this.numero = numero;
		this.aux_loteestoque = new Aux_loteestoque();
		this.aux_loteestoque.setCdloteestoque(cdloteestoque);
		this.aux_loteestoque.setValidade(validade);
		this.qtde = qtde;
		this.cdmaterial = cdmaterial;
		
		listaLotematerial = new ArrayList<Lotematerial>();
	}
	
	public Loteestoque(Integer cdmaterial, Integer cdloteestoque, String numero, Date validade, Double qtde, Double qtdeReservada) {
		this.cdloteestoque = cdloteestoque;
		this.numero = numero;
		this.aux_loteestoque = new Aux_loteestoque();
		this.aux_loteestoque.setCdloteestoque(cdloteestoque);
		this.aux_loteestoque.setValidade(validade);
		this.qtde = qtde;
		this.qtdeReservada = qtdeReservada;
		this.cdmaterial = cdmaterial;
		
		listaLotematerial = new ArrayList<Lotematerial>();
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_loteestoque")
	public Integer getCdloteestoque() {
		return cdloteestoque;
	}
	@Required
	@DisplayName("N�mero")
	@MaxLength(20)
	public String getNumero() {
		return numero;
	}
	public Date getValidade() {
		return validade;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@DisplayName("Materiais")
	@OneToMany(mappedBy="loteestoque")
	public List<Lotematerial> getListaLotematerial() {
		return listaLotematerial;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdloteestoque", insertable=false, updatable=false)
	public Aux_loteestoque getAux_loteestoque() {
		return aux_loteestoque;
	}
	public void setAux_loteestoque(Aux_loteestoque aux_loteestoque) {
		this.aux_loteestoque = aux_loteestoque;
	}
	public void setListaLotematerial(List<Lotematerial> listaLotematerial) {
		this.listaLotematerial = listaLotematerial;
	}

	public void setCdloteestoque(Integer cdloteestoque) {
		this.cdloteestoque = cdloteestoque;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}


	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Double getQtde() {
		return qtde;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	@Transient
	public Boolean getNaoExibirQtde() {
		return naoExibirQtde;
	}

	public void setNaoExibirQtde(Boolean naoExibirQtde) {
		this.naoExibirQtde = naoExibirQtde;
	}

	@Transient
	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	
	@Transient
	@DisplayName("Materiais")
	public String getMaterialStr() {
		if(materialStr == null){
			preencheCamposListagem();
		}
		return materialStr;
	}

	@Transient
	@DisplayName("Validades")
	public String getValidadeStr() {
		if(validadeStr == null){
			preencheCamposListagem();
		}
		return validadeStr;
	}
	
	@Transient
	@DisplayName("Fabrica��es")
	public String getFabricacaoStr() {
		if(fabricacaoStr == null){
			preencheCamposListagem();
		}
		return fabricacaoStr;
	}
	
	private void preencheCamposListagem(){
		StringBuilder materialStrSB = new StringBuilder();
		StringBuilder validadeStrSB = new StringBuilder();
		StringBuilder fabricacaoStrSB = new StringBuilder();
		
		List<String> listaValidadeStr = new ArrayList<String>();
		List<String> listaFabricacaoStr = new ArrayList<String>();
		
		if(listaLotematerial != null && !listaLotematerial.isEmpty()){
			if(listaLotematerial.size() > 10){
				materialStrSB.append("Diversos");
			}
			for(Lotematerial lotematerial : listaLotematerial){
				if(listaLotematerial.size() <= 10){
					materialStrSB.append(lotematerial.getMaterial() != null && lotematerial.getMaterial().getNome() != null ? lotematerial.getMaterial().getNome() : "")
							.append("<br>");
				}
				
				if(lotematerial.getValidade() != null){
					String validadeStr = SinedDateUtils.toString(lotematerial.getValidade());
					if(listaValidadeStr.size() > 10){
						if(!validadeStr.toString().equals("")){
							validadeStrSB = new StringBuilder("Diversos");
							if(listaLotematerial.size() >= 10){
								break;
							}
						}
					}else {
						validadeStrSB.append(validadeStr).append("<br>");
						if(!listaValidadeStr.contains(validadeStr)){
							listaValidadeStr.add(validadeStr);
						}
					}
				}
				
				if(lotematerial.getFabricacao() != null){
					String fabricacaoStr = SinedDateUtils.toString(lotematerial.getFabricacao());
					if(listaFabricacaoStr.size() > 10){
						if(!fabricacaoStr.toString().equals("")){
							fabricacaoStrSB = new StringBuilder("Diversos");
							if(listaLotematerial.size() >= 10){
								break;
							}
						}
					}else {
						fabricacaoStrSB.append(fabricacaoStr).append("<br>");
						if(!listaFabricacaoStr.contains(fabricacaoStr)){
							listaFabricacaoStr.add(fabricacaoStr);
						}
					}
				}
			}
		}
		
		fabricacaoStr = fabricacaoStrSB.toString();
		validadeStr = validadeStrSB.toString();
		materialStr = materialStrSB.toString();
	}
	
	@Transient
	@DescriptionProperty(usingFields={"numero","aux_loteestoque"})
	public String getNumerovalidade() {	
		if(this.cdloteestoque != null && (this.numerovalidade == null || this.numerovalidade.trim().isEmpty())){
			StringBuilder s = new StringBuilder();
			if(getNumero() != null){
				s.append(getNumero());
			}
			Date dtValidade = getValidade();
			if(Hibernate.isInitialized(getAux_loteestoque()) && getAux_loteestoque() != null && getAux_loteestoque().getValidade() != null){
				dtValidade = getAux_loteestoque().getValidade();
			}
			if(dtValidade != null){
				s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(dtValidade));
			}
			this.numerovalidade = s.toString();
		}
		return this.numerovalidade;	
	}
	
	@Transient
	public String getNumerovalidadeByMaterial() {	
		if(this.cdloteestoque != null && (this.numerovalidade == null || this.numerovalidade.trim().isEmpty())){
			StringBuilder s = new StringBuilder();
			if(getNumero() != null){
				s.append(getNumero());
			}
			Date dtValidade = getValidade();

			if(dtValidade != null){
				s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(dtValidade));
			}
			this.numerovalidade = s.toString();
		}
		return this.numerovalidade;	
	}
	
	public void setNumerovalidade(String numerovalidade) {
		this.numerovalidade = numerovalidade;
	}
	
	@Transient
	public String getDescriptionAutocomplete(){
		if(StringUtils.isBlank(DescriptionAutocomplete)){
			return getNumerovalidade();
		}
		return DescriptionAutocomplete;	
	}
	public void setDescriptionAutocomplete(String descriptionAutocomplete) {
		DescriptionAutocomplete = descriptionAutocomplete;
	}
	
	public void montaDescriptionAutocomplete(Material material){
		if(this.cdloteestoque != null && (this.DescriptionAutocomplete == null || this.DescriptionAutocomplete.trim().isEmpty())){
			StringBuilder s = new StringBuilder();
			if(getNumero() != null){
				s.append(getNumero());
			}
			
			try {
				if(!Hibernate.isInitialized(listaLotematerial) || (listaLotematerial == null || listaLotematerial.isEmpty())){
					this.setListaLotematerial(LotematerialService.getInstance().findAllByLoteestoque(this));
				}
				if(material != null && this.listaLotematerial != null && !this.listaLotematerial.isEmpty()){
					for(Lotematerial lotematerial : this.listaLotematerial){
						if(material != null && material.equals(lotematerial.getMaterial()) && lotematerial.getValidade() != null){
							s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(lotematerial.getValidade()));
						}
					}
				}	
			} catch (Exception e) {}
			
			this.DescriptionAutocomplete = s.toString();
		}
	}
	
	
	@Transient
	public String getNumerovalidadeqtde() {
		StringBuilder s = new StringBuilder();
		if(getNumero() != null){
			s.append(getNumero());
		}
		
		Date validadeLote = getValidade();
		if(Hibernate.isInitialized(getAux_loteestoque()) && getAux_loteestoque() != null && getAux_loteestoque().getValidade() != null){
			validadeLote = getAux_loteestoque().getValidade();
		}
		
		if(validadeLote != null && (SinedUtil.isUserHasAction("ALTERAR_LOTE_FATURAMENTO") || !Boolean.TRUE.equals(ParametrogeralService.getInstance().getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO)))){
			s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(validadeLote));
		}
		if((getNaoExibirQtde() == null || !getNaoExibirQtde()) && getQtde() != null && !Boolean.TRUE.equals(ParametrogeralService.getInstance().getBoolean(Parametrogeral.SELECAO_AUTOMATICA_LOTE_FATURAMENTO)))
			s.append(" - Qtde: " + (getQtde() != null ? SinedUtil.descriptionDecimal(getQtde()) : ""));
		
		if(s.length() == 0 && getNumerovalidade() != null && getNumerovalidade().length() > 0)
			return getNumerovalidade();
		
		return s.toString();
	}
	
	@Transient
	public String getNumerovalidadeqtdeVenda() {
		StringBuilder s = new StringBuilder();
		if(getNumero() != null){
			s.append(getNumero());
		}
		
		Date validadeLote = getValidade();
		
		if(validadeLote != null){
			s.append(" - " +new SimpleDateFormat("dd/MM/yyyy").format(validadeLote));
		}
		if((getNaoExibirQtde() == null || !getNaoExibirQtde()) && getQtde() != null)
			s.append("<span> - Qtde: " + (getQtde() != null ? SinedUtil.descriptionDecimal(getQtde()) : "") + "</span>");
		
		return s.toString();
	}
	
	@Transient
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedorTrans() {
		return fornecedorTrans;
	}
	public void setFornecedorTrans(Fornecedor fornecedorTrans) {
		this.fornecedorTrans = fornecedorTrans;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loteestoque other = (Loteestoque) obj;
		if (cdloteestoque == null) {
			if (other.cdloteestoque != null)
				return false;
		} else if (!cdloteestoque.equals(other.cdloteestoque))
			return false;
		return true;
	}
	
	public String subQueryFornecedorEmpresa() {
		return "select loteestoqueSubQueryFornecedorEmpresa.cdloteestoque " +
				"from Loteestoque loteestoqueSubQueryFornecedorEmpresa " +
				"left outer join loteestoqueSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public Double getQtdeReservada() {
		return qtdeReservada;
	}
	
	public void setQtdeReservada(Double qtdeReservada) {
		this.qtdeReservada = qtdeReservada;
	}

	@Transient
	public Boolean getFromImportacaoXml() {
		return fromImportacaoXml;
	}
	public void setFromImportacaoXml(Boolean fromImportacaoXml) {
		this.fromImportacaoXml = fromImportacaoXml;
	}
	
	@Transient
	public String getNumeroLoteSoNumeroAndLetra(){
		return br.com.linkcom.neo.util.StringUtils.soNumeroAndLetra(this.numero);
	}
}
