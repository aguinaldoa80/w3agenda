package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import org.hibernate.validator.Length;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.NotaerrocorrecaoHistoricoEnum;


@Entity
@DisplayName("Hist�rico")
@SequenceGenerator(name="sq_notaerrocorrecaohistorico", sequenceName = "sq_notaerrocorrecaohistorico")
public class NotaErroCorrecaoHistorico {
	
	protected Integer cdnotaerrocorrecaoHistorico;
	protected NotaerrocorrecaoHistoricoEnum acaoexecutada;
	protected Notaerrocorrecao notaerrocorrecao;
	protected Timestamp dataaltera;
	protected String responsavel;
	protected String observacao;
	
	//GET
	@Id
	@DisplayName("ID")
	@GeneratedValue(generator="sq_notaerrocorrecaohistorico",strategy=GenerationType.AUTO)
	public Integer getCdnotaerrocorrecaoHistorico() {
		return cdnotaerrocorrecaoHistorico;
	}

	@DisplayName("A��o")
	public NotaerrocorrecaoHistoricoEnum getAcaoexecutada() {
		return acaoexecutada;
	}

	@DisplayName("Data")
	public Timestamp getDataaltera() {
		return dataaltera;
	}
	
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		return responsavel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotaerrocorrecao")
	public Notaerrocorrecao getNotaerrocorrecao() {
		return notaerrocorrecao;
	}
	
	@Length(max = 100)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	//SET
	public void setCdnotaerrocorrecaoHistorico(Integer cdnotaerrocorrecaoHistorico) {
		this.cdnotaerrocorrecaoHistorico = cdnotaerrocorrecaoHistorico;
	}

	public void setNotaerrocorrecao(Notaerrocorrecao notaerrocorrecao) {
		this.notaerrocorrecao = notaerrocorrecao;
	}
	
	public void setAcaoexecutada(NotaerrocorrecaoHistoricoEnum acaoexecutada) {
		this.acaoexecutada = acaoexecutada;
	}
	
	public void setDataaltera(Timestamp dataaltera) {
		this.dataaltera = dataaltera;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
