package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_arquivognreitem", sequenceName = "sq_arquivognreitem")
public class Arquivognreitem {

	protected Integer cdarquivognreitem;
	protected Arquivognre arquivognre;
	protected Gnre gnre;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivognreitem")
	public Integer getCdarquivognreitem() {
		return cdarquivognreitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivognre")
	public Arquivognre getArquivognre() {
		return arquivognre;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgnre")
	public Gnre getGnre() {
		return gnre;
	}

	public void setGnre(Gnre gnre) {
		this.gnre = gnre;
	}

	public void setCdarquivognreitem(Integer cdarquivognreitem) {
		this.cdarquivognreitem = cdarquivognreitem;
	}

	public void setArquivognre(Arquivognre arquivognre) {
		this.arquivognre = arquivognre;
	}
	
}
