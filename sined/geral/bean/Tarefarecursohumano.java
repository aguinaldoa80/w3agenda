package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_tarefarecursohumano",sequenceName="sq_tarefarecursohumano")
public class Tarefarecursohumano implements Log, PermissaoProjeto{

	protected Integer cdtarefarecursohumano;
	protected Tarefa tarefa;
	protected Cargo cargo;
	protected Double qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_tarefarecursohumano",strategy=GenerationType.AUTO)
	public Integer getCdtarefarecursohumano() {
		return cdtarefarecursohumano;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefa")
	public Tarefa getTarefa() {
		return tarefa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@Required
	public Double getQtde() {
		return qtde;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdtarefarecursohumano(Integer cdtarefarecursohumano) {
		this.cdtarefarecursohumano = cdtarefarecursohumano;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public String subQueryProjeto() {
		return "select tarefarecursohumanosubQueryProjeto.cdtarefarecursohumano " +
				"from Tarefarecursohumano tarefarecursohumanosubQueryProjeto " +
				"join tarefarecursohumanosubQueryProjeto.tarefa tarefasubQueryProjeto " +
				"join tarefasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
}
