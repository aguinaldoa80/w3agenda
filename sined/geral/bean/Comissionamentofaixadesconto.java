package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_comissionamentofaixadesconto", sequenceName = "sq_comissionamentofaixadesconto")
public class Comissionamentofaixadesconto {

	protected Integer cdcomissionamentofaixadesconto;
	protected Comissionamento comissionamento;
	protected Double percentualdescontoate;
	protected Double percentualcomissao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comissionamentofaixadesconto")
	public Integer getCdcomissionamentofaixadesconto() {
		return cdcomissionamentofaixadesconto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")	
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@Required
	@DisplayName("% de Desconto (At�)")
	public Double getPercentualdescontoate() {
		return percentualdescontoate;
	}
	@Required
	@DisplayName("% de Comiss�o")
	public Double getPercentualcomissao() {
		return percentualcomissao;
	}
	
	public void setCdcomissionamentofaixadesconto(Integer cdcomissionamentofaixadesconto) {
		this.cdcomissionamentofaixadesconto = cdcomissionamentofaixadesconto;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setPercentualdescontoate(Double percentualdescontoate) {
		this.percentualdescontoate = percentualdescontoate;
	}
	public void setPercentualcomissao(Double percentualcomissao) {
		this.percentualcomissao = percentualcomissao;
	}
}
