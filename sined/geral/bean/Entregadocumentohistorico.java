package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_entregadocumentohistorico", sequenceName = "sq_entregadocumentohistorico")
public class Entregadocumentohistorico implements Log {
	
	protected Integer cdentregadocumentohistorico;
	protected Entregadocumento entregadocumento;	
	protected String observacao;
	protected Entregadocumentosituacao entregadocumentosituacao;
	protected Notafiscalproduto notafiscalproduto;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentohistorico")
	public Integer getCdentregadocumentohistorico() {
		return cdentregadocumentohistorico;
	}
	@DisplayName("Documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Situação")
	public Entregadocumentosituacao getEntregadocumentosituacao() {
		return entregadocumentosituacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	
	public void setCdentregadocumentohistorico(Integer cdentregadocumentohistorico) {
		this.cdentregadocumentohistorico = cdentregadocumentohistorico;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setEntregadocumentosituacao(Entregadocumentosituacao entregadocumentosituacao) {
		this.entregadocumentosituacao = entregadocumentosituacao;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
		
	//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}	
	
	@Transient
	@DisplayName("Responsável")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
}
