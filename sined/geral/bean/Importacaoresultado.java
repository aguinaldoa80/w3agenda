package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoDadosBasicosTipo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_importacaoresultado", sequenceName="sq_importacaoresultado")
public class Importacaoresultado implements Log {

	protected Integer cdimportacaoresultado;
	protected ImportacaoDadosBasicosTipo tipo;
	protected Arquivo arquivoimportado;
	protected Arquivo arquivorelatorioresultado;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_importacaoresultado")
	public Integer getCdimportacaoresultado() {
		return cdimportacaoresultado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoimportado")
	public Arquivo getArquivoimportado() {
		return arquivoimportado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivorelatorioresultado")
	public Arquivo getArquivorelatorioresultado() {
		return arquivorelatorioresultado;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public ImportacaoDadosBasicosTipo getTipo() {
		return tipo;
	}
	
	public void setTipo(ImportacaoDadosBasicosTipo tipo) {
		this.tipo = tipo;
	}

	public void setCdimportacaoresultado(Integer cdimportacaoresultado) {
		this.cdimportacaoresultado = cdimportacaoresultado;
	}

	public void setArquivoimportado(Arquivo arquivoimportado) {
		this.arquivoimportado = arquivoimportado;
	}

	public void setArquivorelatorioresultado(Arquivo arquivorelatorioresultado) {
		this.arquivorelatorioresultado = arquivorelatorioresultado;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
