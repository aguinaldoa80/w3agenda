package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_producaoordemhistorico", sequenceName = "sq_producaoordemhistorico")
public class Producaoordemhistorico implements Log {
	
	protected Integer cdproducaoordemhistorico; 
	protected Producaoordem producaoordem;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoordemhistorico")
	public Integer getCdproducaoordemhistorico() {
		return cdproducaoordemhistorico;
	}

	@JoinColumn(name="cdproducaoordem")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordem getProducaoordem() {
		return producaoordem;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdproducaoordemhistorico(Integer cdproducaoordemhistorico) {
		this.cdproducaoordemhistorico = cdproducaoordemhistorico;
	}
	
	public void setProducaoordem(Producaoordem producaoordem) {
		this.producaoordem = producaoordem;
	}

	@Transient
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
