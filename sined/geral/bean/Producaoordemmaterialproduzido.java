package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_producaoordemmaterialproduzido", sequenceName = "sq_producaoordemmaterialproduzido")
public class Producaoordemmaterialproduzido {
	
	protected Integer cdproducaoordemmaterialproduzido; 
	protected Producaoordemmaterial producaoordemmaterial;
	protected Date data;
	protected Double quantidade;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoordemmaterialproduzido")
	public Integer getCdproducaoordemmaterialproduzido() {
		return cdproducaoordemmaterialproduzido;
	}

	@JoinColumn(name="cdproducaoordemmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	
	public Date getData() {
		return data;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setCdproducaoordemmaterialproduzido(
			Integer cdproducaoordemmaterialproduzido) {
		this.cdproducaoordemmaterialproduzido = cdproducaoordemmaterialproduzido;
	}

	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}

	
}
