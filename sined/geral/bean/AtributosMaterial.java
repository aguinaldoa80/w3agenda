package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Atributos do Material")
@SequenceGenerator(name = "sq_atributosmaterial", sequenceName = "sq_atributosmaterial")
public class AtributosMaterial implements Log{
	protected Integer cdatributosmaterial;
	protected String nome;
	
	protected List<AtributoMaterialHistorico> listaAtributosMaterialHistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_atributosmaterial")
	public Integer getCdatributosmaterial() {
		return cdatributosmaterial;
	}
	public void setCdatributosmaterial(Integer cdatributosmaterial) {
		this.cdatributosmaterial = cdatributosmaterial;
	}
	
	@Required	
	@MaxLength(100)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Histórico")
	@OneToMany(mappedBy="atributosMaterial")
	public List<AtributoMaterialHistorico> getListaAtributosMaterialHistorico() {
		return listaAtributosMaterialHistorico;
	}
	public void setListaAtributosMaterialHistorico(
			List<AtributoMaterialHistorico> listaAtributosMaterialHistorico) {
		this.listaAtributosMaterialHistorico = listaAtributosMaterialHistorico;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
