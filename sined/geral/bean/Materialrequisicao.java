package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_materialrequisicao", sequenceName="sq_materialrequisicao")
@DisplayName("Materiais e Serviços")
public class Materialrequisicao implements Log{

	protected Integer cdmaterialrequisicao;
	protected Requisicao requisicao;
	protected Material material;
	protected Double quantidade;
	protected Timestamp dtdisponibilizacao;
	protected Timestamp dtretirada;
	protected Double valortotal;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String item;
	protected String autorizacaoFaturamento;
	protected Boolean faturado;
	protected Double percentualfaturado;
	protected Integer cdnotafiscalservicoitem;
	protected String observacao;
	protected Arquivo arquivo;
	
	//Transient
	protected Double valorunitario;
	protected Date dtvencimento;
	protected Boolean checado;
	protected Double percentualafaturar;
	protected Double valorfaturado;

	
	@Id
	@Required
	@GeneratedValue(generator="sq_materialrequisicao", strategy=GenerationType.AUTO)
	public Integer getCdmaterialrequisicao() {
		return cdmaterialrequisicao;
	}
	
	@Required
	@JoinColumn(name="cdrequisicao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Requisicao getRequisicao() {
		return requisicao;
	}
	
	@Required
	@DisplayName("Material ou Serviço")
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}
	
	@Required
	@MaxLength(9)
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Data disponibilização")
	public Timestamp getDtdisponibilizacao() {
		return dtdisponibilizacao;
	}

	@DisplayName("Data retirada")
	public Timestamp getDtretirada() {
		return dtretirada;
	}
	
	@DisplayName("Valor total")
	public Double getValortotal() {
		return valortotal;
	}
	
	@DisplayName("Observação")	
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("% Faturado")
	public Double getPercentualfaturado() {
		return percentualfaturado;
	}
	
	public void setPercentualfaturado(Double percentualfaturado) {
		this.percentualfaturado = percentualfaturado;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCdnotafiscalservicoitem() {
		return cdnotafiscalservicoitem;
	}

	public void setCdnotafiscalservicoitem(Integer cdnotafiscalservicoitem) {
		this.cdnotafiscalservicoitem = cdnotafiscalservicoitem;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	
	public void setCdmaterialrequisicao(Integer cdmaterialrequisicao) {
		this.cdmaterialrequisicao = cdmaterialrequisicao;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDtdisponibilizacao(Timestamp dtdisponibilizacao) {
		this.dtdisponibilizacao = dtdisponibilizacao;
	}

	public void setDtretirada(Timestamp dtretirada) {
		this.dtretirada = dtretirada;
	}

	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}

	@DisplayName("Valor unitário")
	public Double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@DisplayName("Autorização para Faturamento")
	@MaxLength(100)
	public String getAutorizacaoFaturamento() {
		return autorizacaoFaturamento;
	}

	public void setAutorizacaoFaturamento(String autorizacaoFaturamento) {
		this.autorizacaoFaturamento = autorizacaoFaturamento;
	}

	public Boolean getFaturado() {
		return faturado;
	}

	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}

	@Transient
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	@Transient
	public Boolean getChecado() {
		return checado;
	}

	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	
	@Transient
	@DisplayName("% a Faturar")
	public Double getPercentualafaturar() {
		return percentualafaturar;
	}
	
	public void setPercentualafaturar(Double percentualafaturar) {
		this.percentualafaturar = percentualafaturar;
	}
	
	@Transient
	@DisplayName("Valor faturado")
	public Double getValorfaturado() {
		return valorfaturado;
	}
	
	public void setValorfaturado(Double valorfaturado) {
		this.valorfaturado = valorfaturado;
	}
	
	@DisplayName("Arquivos")
	@JoinColumn(name="cdarquivo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	
	
	
	
}
