package br.com.linkcom.sined.geral.bean;



import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_autorizacaohistorico", sequenceName = "sq_autorizacaohistorico")
public class Autorizacaohistorico {

	protected Integer cdautorizacaohistorico;
	protected Requisicaoestado autorizacaoestado;
	protected Requisicaoestado autorizacaoestadodestino;
	protected Autorizacaotrabalho autorizacaotrabalho;
	protected Colaborador colaborador;
	protected Date dtautorizacaoalterada;
	protected String observacao;
	protected Hora datahora;
	protected Arquivo arquivo;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_autorizacaohistorico")
	public Integer getCdautorizacaohistorico() {
		return cdautorizacaohistorico;
	}
	
	public void setCdautorizacaohistorico(Integer cdautorizacaohistorico) {
		this.cdautorizacaohistorico = cdautorizacaohistorico;
	}

	@DisplayName("Autorização de trabalho")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdautorizacaotrabalho")
	@Required
	public Autorizacaotrabalho getAutorizacaotrabalho() {
		return autorizacaotrabalho;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Responsável")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@DisplayName("Data")
	public Date getDtautorizacaoalterada() {
		return dtautorizacaoalterada;
	}
	
	@MaxLength(1000)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("Situação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdautorizacaoestado")
	public Requisicaoestado getAutorizacaoestado() {
		return autorizacaoestado;
	}
	
	@DisplayName("Situação Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdautorizacaoestadodestino")
	public Requisicaoestado getAutorizacaoestadodestino() {
		return autorizacaoestadodestino;
	}
	
	@DisplayName("Hora")
	public Hora getDatahora() {
		return datahora;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setAutorizacaoestado(Requisicaoestado autorizacaoestado) {
		this.autorizacaoestado = autorizacaoestado;
	}

	public void setAutorizacaotrabalho(Autorizacaotrabalho autorizacaotrabalho) {
		this.autorizacaotrabalho = autorizacaotrabalho;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setDtautorizacaoalterada(Date dtautorizacaoalterada) {
		this.dtautorizacaoalterada = dtautorizacaoalterada;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDatahora(Hora datahora) {
		this.datahora = datahora;
	}
	
	public void setAutorizacaoestadodestino(Requisicaoestado autorizacaoestadodestino) {
		this.autorizacaoestadodestino = autorizacaoestadodestino;
	}

}
