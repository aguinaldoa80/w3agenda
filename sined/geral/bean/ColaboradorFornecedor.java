package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_colaboradorfornecedor", sequenceName = "sq_colaboradorfornecedor")
public class ColaboradorFornecedor {

	protected Integer cdcolaboradorfornecedor;
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	
	public ColaboradorFornecedor (){}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorfornecedor")
	public Integer getCdcolaboradorfornecedor() {
		return cdcolaboradorfornecedor;
	}

	public void setCdcolaboradorfornecedor(Integer cdcolaboradorfornecedor) {
		this.cdcolaboradorfornecedor = cdcolaboradorfornecedor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	@Required
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
}