package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_despesaviagemprojeto", sequenceName = "sq_despesaviagemprojeto")
public class Despesaviagemprojeto {

	protected Integer cddespesaviagemprojeto;
	protected Despesaviagem despesaviagem;
	protected Projeto projeto;
	protected Money percentual;

	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_despesaviagemprojeto")
	public Integer getCddespesaviagemprojeto() {
		return cddespesaviagemprojeto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagem")
	public Despesaviagem getDespesaviagem() {
		return despesaviagem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Money getPercentual() {
		return percentual;
	}

	public void setCddespesaviagemprojeto(Integer cddespesaviagemprojeto) {
		this.cddespesaviagemprojeto = cddespesaviagemprojeto;
	}
	public void setDespesaviagem(Despesaviagem despesaviagem) {
		this.despesaviagem = despesaviagem;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPercentual(Money percentual) {
		this.percentual = percentual;
	}
}
