package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_motivocancelamento", sequenceName = "sq_motivocancelamento")
public class Motivocancelamento implements Log {

	protected Integer cdmotivocancelamento;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_motivocancelamento")
	public Integer getCdmotivocancelamento() {
		return cdmotivocancelamento;
	}
	
	@Required
	@MaxLength(1000)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdmotivocancelamento(Integer cdmotivocancelamento) {
		this.cdmotivocancelamento = cdmotivocancelamento;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
