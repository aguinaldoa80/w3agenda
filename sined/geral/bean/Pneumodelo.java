package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Modelo de Pneu")
@SequenceGenerator(name = "sq_pneumodelo", sequenceName = "sq_pneumodelo")
public class Pneumodelo implements Log {

	protected Integer cdpneumodelo;
	protected Pneumarca pneumarca;
	protected String nome;
	protected String codigointegracao;
	protected Boolean otr;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Pneumodelo(){}
	
	public Pneumodelo(Integer cdpneumodelo){
		this.cdpneumodelo = cdpneumodelo;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneumodelo")
	public Integer getCdpneumodelo() {
		return cdpneumodelo;
	}
	
	@DisplayName("Marca")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumarca")
	public Pneumarca getPneumarca() {
		return pneumarca;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(20)
	@DisplayName("C�digo p/ integra��o")
	public String getCodigointegracao() {
		return codigointegracao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdpneumodelo(Integer cdpneumodelo) {
		this.cdpneumodelo = cdpneumodelo;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	
	public void setPneumarca(Pneumarca pneumarca) {
		this.pneumarca = pneumarca;
	}

	public void setCodigointegracao(String codigointegracao) {
		this.codigointegracao = codigointegracao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("OTR")
	public Boolean getOtr() {
		return otr;
	}

	public void setOtr(Boolean otr) {
		this.otr = otr;
	}
	
	
	
}