package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_lotematerial", sequenceName="sq_lotematerial")
public class Lotematerial {

	protected Integer cdlotematerial;
	protected Loteestoque loteestoque;
	protected Date validade;
	protected Material material;
	private Date fabricacao;
	private String codigoAgregacao;
	
	//Transient
	protected Boolean bloqueado;
	protected Material materialTrans;
	
	public Lotematerial(){}
	
	public Lotematerial(Integer cdlotematerial){
		this.cdlotematerial = cdlotematerial;
	}
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_lotematerial")
	public Integer getCdlotematerial() {
		return cdlotematerial;
	}
	public void setCdlotematerial(Integer cdlotematerial) {
		this.cdlotematerial = cdlotematerial;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}


	public Date getValidade() {
		return validade;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}

	@MaxLength(20)
	@DisplayName("C�digo de Agrega��o")
	public String getCodigoAgregacao() {
		return codigoAgregacao;
	}
	public void setCodigoAgregacao(String codigoAgregacao) {
		this.codigoAgregacao = codigoAgregacao;
	}
	
	@DisplayName("Fabrica��o")
	public Date getFabricacao() {
		return fabricacao;
	}
	public void setFabricacao(Date fabricacao) {
		this.fabricacao = fabricacao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Transient
	public Boolean getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	@Transient
	public Material getMaterialTrans() {
		return materialTrans;
	}
	public void setMaterialTrans(Material materialTrans) {
		this.materialTrans = materialTrans;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Lotematerial other = (Lotematerial) obj;
		if (cdlotematerial == null) {
			if (other.cdlotematerial != null)
				return false;
		} else if (!cdlotematerial.equals(other.cdlotematerial)){
			return false;
		}
		
		return true;
	}
}
