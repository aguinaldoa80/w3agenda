package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.lkbanco.bank.Account;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.view.Vconta;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@SequenceGenerator(name = "sq_conta", sequenceName = "sq_conta")
@JoinEmpresa("conta.listaContaempresa.empresa")
public class Conta implements Log, Comparable<Conta>, Account<Banco>, PermissaoProjeto {

	protected Integer cdconta;
	protected Contatipo contatipo;
	protected String nome;
	protected String numero;
	protected String dvnumero;
	protected String agencia;
	protected String dvagencia;
	protected Banco banco;
	protected Date dtsaldo;
	protected Money saldo;
	protected Boolean ativo;
	protected Money limite;
	protected Integer diavencimento;
	protected Integer diafechamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Set<Agendamento> listaAgendamento = new ListSet<Agendamento>(Agendamento.class);
	protected Vconta vconta;
	protected Empresa empresatitular;
	protected String titular;
	protected String localpagamento;
	protected Integer sequencial;
	protected Integer sequencialpagamento;
	protected Integer sequencialcheque;	
	protected Boolean conciliacaoautomatica;
	protected Boolean baixaautomaticafaturamento;
	protected Money taxaboleto;
	protected Boolean naogerarboleto;
	protected String codigoempresasispag;
	protected Boolean nossonumerointervalo;
	protected Integer nossonumeroinicial;
	protected Integer nossonumerofinal;
	protected Operacaocontabil operacaocontabil;
	protected ContaContabil contaContabil;
	protected Taxa taxa;
	protected Boolean considerarnumerodocumentoremessa;
	protected Boolean desconsiderarsaldoinicialfluxocaixa;
	protected Set<Contadigital> listaContadigital;

	protected List<Contapapel> listaContapapel = new ListSet<Contapapel>(Contapapel.class);
	protected List<Contacarteira> listaContacarteira = new ListSet<Contacarteira>(Contacarteira.class);
	protected Set<Contaempresa> listaContaempresa = new ListSet<Contaempresa>(Contaempresa.class);
	protected Set<Contaprojeto> listaContaprojeto = new ListSet<Contaprojeto>(Contaprojeto.class);
	
	//Transients
	protected Money saldoatual;
	protected String descricaobancaria;
	protected Boolean bancoemiteboleto = true;
	protected List<Contacarteira> listaContacarteiraQueGeraBoleto = new ListSet<Contacarteira>(Contacarteira.class);

	public Conta() {
	}

	public Conta(Integer cdconta) {
		this.cdconta = cdconta;
	}

	public Conta(Integer cdconta, String nome) {
		this.cdconta = cdconta;
		this.nome = nome;
	}

	public Conta(Integer cdconta, String nome, Long saldo, Integer cdcontatipo,
			Boolean ativo, Long saldoatual) {
		this.cdconta = cdconta;
		this.nome = nome;
		this.saldo = saldo != null ? new Money(saldo) : null;
		this.contatipo = new Contatipo(cdcontatipo);
		this.ativo = ativo;
		this.saldoatual = new Money(saldoatual);
	}

	public Conta(String nome) {
		this.nome = nome;
	}

	@DisplayName("Saldo atual")
	@Transient
	public Money getSaldoatual() {
		return saldoatual;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_conta")
	@DisplayName("Id")
	public Integer getCdconta() {
		return cdconta;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontatipo")
	public Contatipo getContatipo() {
		return contatipo;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@DisplayName("N�mero")
	@Required
	@MaxLength(25)
	public String getNumero() {
		return numero;
	}

	@DisplayName("-")
	@MaxLength(2)
	public String getDvnumero() {
		return dvnumero;
	}

	@Required
	@DisplayName("Ag�ncia")
	@MaxLength(5)
	public String getAgencia() {
		return agencia;
	}

	@DisplayName("-")
	@MaxLength(2)
	public String getDvagencia() {
		return dvagencia;
	}

	/**
	 * Esta propriedade est� anotada com FetchType.EAGER por ser usada como
	 * DescriptionProperty.
	 * 
	 * @return
	 */
	@Required
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cdbanco")
	public Banco getBanco() {
		return banco;
	}

	@DisplayName("Data do saldo inicial")
	public Date getDtsaldo() {
		return dtsaldo;
	}

	@DisplayName("Saldo inicial")
	public Money getSaldo() {
		return saldo;
	}

	@Transient
	@DisplayName("Descri��o")
	public String getDescricao() {
		if (StringUtils.isNotEmpty(this.nome)) {
			return this.nome;
		}
		String descricao = "";
		if (this.banco != null && StringUtils.isNotEmpty(this.banco.getNome())) {
			descricao = this.banco.getNome();
			if (StringUtils.isNotEmpty(this.agencia)) {
				descricao += " " + this.agencia;
				if (StringUtils.isNotEmpty(this.dvagencia)) {
					descricao += "-" + this.dvagencia;
				}
			}
			if (StringUtils.isNotEmpty(this.numero)) {
				descricao += "/" + this.numero;
				if (StringUtils.isNotEmpty(this.dvnumero)) {
					descricao += "-" + this.dvnumero;
				}
			}
		}

		return descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public Money getLimite() {
		return limite;
	}

	@DisplayName("Dia de fechamento")
	@Required
	@MaxLength(2)
	@MaxValue(28)
	@MinValue(1)
	public Integer getDiafechamento() {
		return diafechamento;
	}

	@DisplayName("Dia de vencimento")
	@Required
	@MaxLength(2)
	@MaxValue(28)
	@MinValue(1)
	public Integer getDiavencimento() {
		return diavencimento;
	}

	@OneToMany(mappedBy = "conta")
	public Set<Agendamento> getListaAgendamento() {
		return listaAgendamento;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdconta", insertable = false, updatable = false)
	public Vconta getVconta() {
		return vconta;
	}

	@Transient
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricaobancaria() {
		return descricaobancaria;
	}

	@MaxLength(100)
	public String getTitular() {
		return titular;
	}
	
	@DisplayName("Empresa Titular")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresatitular")
	public Empresa getEmpresatitular() {
		return empresatitular;
	}

	@DisplayName("Local de pagamento")
	@MaxLength(100)
	public String getLocalpagamento() {
		return localpagamento;
	}

	@DisplayName("Concilia��o autom�tica")
	public Boolean getConciliacaoautomatica() {
		return conciliacaoautomatica;
	}

	@DisplayName("Banco emite boleto")
	@Transient
	public Boolean getBancoemiteboleto() {
		return bancoemiteboleto;
	}
	
	@DisplayName("Baixa autom�tica no faturamento")
	public Boolean getBaixaautomaticafaturamento() {
		return baixaautomaticafaturamento;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="conta")
	public Set<Contadigital> getListaContadigital() {
		return listaContadigital;
	}
	
	public void setListaContadigital(Set<Contadigital> listaContadigital) {
		this.listaContadigital = listaContadigital;
	}
	
	public void setListaContacarteiraQueGeraBoleto(
			List<Contacarteira> listaContacarteiraQueGeraBoleto) {
		this.listaContacarteiraQueGeraBoleto = listaContacarteiraQueGeraBoleto;
	}
	
	public void setBaixaautomaticafaturamento(Boolean baixaautomaticafaturamento) {
		this.baixaautomaticafaturamento = baixaautomaticafaturamento;
	}

	public void setBancoemiteboleto(Boolean bancoemiteboleto) {
		this.bancoemiteboleto = bancoemiteboleto;
	}

	public void setConciliacaoautomatica(Boolean conciliacaoautomatica) {
		this.conciliacaoautomatica = conciliacaoautomatica;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public void setEmpresatitular(Empresa empresatitular) {
		this.empresatitular = empresatitular;
	}

	public void setLocalpagamento(String localpagamento) {
		this.localpagamento = localpagamento;
	}
	
	public void setDescricaobancaria(String descricaobancaria) {
		this.descricaobancaria = descricaobancaria;
	}

	public void setCdconta(Integer cdconta) {
		this.cdconta = cdconta;
	}

	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setDvnumero(String dvnumero) {
		this.dvnumero = dvnumero;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public void setDtsaldo(Date dtsaldo) {
		this.dtsaldo = dtsaldo;
	}

	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setSaldoatual(Money saldoatual) {
		this.saldoatual = saldoatual;
	}

	public void setLimite(Money limite) {
		this.limite = limite;
	}

	public void setDiafechamento(Integer diafechamento) {
		this.diafechamento = diafechamento;
	}

	public void setDiavencimento(Integer diavencimento) {
		this.diavencimento = diavencimento;
	}

	public void setListaAgendamento(Set<Agendamento> listaAgendamento) {
		this.listaAgendamento = listaAgendamento;
	}

	public void setVconta(Vconta vconta) {
		this.vconta = vconta;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (cdconta == null) {
			if (other.cdconta != null)
				return false;
		} else if (!cdconta.equals(other.cdconta))
			return false;
		return true;
	}

	public int compareTo(Conta o) {
		int dif = 0;
		if (this.getDescricao() != null && o.getDescricao() != null) {
			dif = this.getDescricao().compareToIgnoreCase(o.getDescricao());
		} else {
			dif = this.numero.compareTo(o.numero);
		}
		return dif;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdconta == null) ? 0 : cdconta.hashCode());
		return result;
	}

	// GERACAO DO ARQUIVO BANC�RIO

	@Transient
	public String getAgency() {
		return getAgencia();
	}

	@Transient
	public String getAgreementNumber() {
		return getContacarteira() != null ? getContacarteira().getConvenio()
				: null;
	}

	@Transient
	public Banco getBank() {
		return getBanco();
	}

	@Transient
	public String getCpfCnpj() {
		return "04395040000130";
	}

	@Transient
	public String getDvAgency() {
		return getDvagencia();
	}

	@Transient
	public String getDvnumber() {
		return getDvnumero();
	}

	@Transient
	public String getEnderecoRuaAvenida() {
		return "Av do Contorno";
	}

	@Transient
	public String getEnderecoCep() {
		return "30110180";
	}

	@Transient
	public String getEnderecoCidade() {
		return "BELO HORIZONTE";
	}

	@Transient
	public String getEnderecoComplemento() {
		return "SALA 705";
	}

	@Transient
	public String getEnderecoNumero() {
		return "2905";
	}

	@Transient
	public String getEnderecoSiglaEstado() {
		return "MG";
	}

	@Transient
	public String getNumber() {
		return getNumero();
	}

	@Transient
	public String getRazaoSocial() {
		return "Daltec Constru��es e Montagens Industriais Ltda";
	}

	@Override
	public String toString() {
		return this.cdconta + " " + this.nome;
	}

	@DisplayName("Sequencial (Cobran�a)")
	@MaxLength(7)
	public Integer getSequencial() {
		return sequencial;
	}
	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}
	
	@DisplayName("Sequencial (Pagamento)")
	@MaxLength(7)
	public Integer getSequencialpagamento() {
		return sequencialpagamento;
	}
	
	@DisplayName("Sequencial (Cheque)")
	@MaxLength(7)
	public Integer getSequencialcheque() {
		return sequencialcheque;
	}
	
	public void setSequencialcheque(Integer sequencialcheque) {
		this.sequencialcheque = sequencialcheque;
	}
	
	public void setSequencialpagamento(Integer sequencialpagamento) {
		this.sequencialpagamento = sequencialpagamento;
	}
	
	@DisplayName("C�digo da Empresa SISPAG")
	public String getCodigoempresasispag() {
		return codigoempresasispag;
	}

	@DisplayName("Gerar nosso n�mero por intervalo")
	public Boolean getNossonumerointervalo() {
		return nossonumerointervalo;
	}
	@DisplayName("Nosso n�mero inicial")
	public Integer getNossonumeroinicial() {
		return nossonumeroinicial;
	}
	@DisplayName("Nosso n�mero final")
	public Integer getNossonumerofinal() {
		return nossonumerofinal;
	}	
	public void setCodigoempresasispag(String codigoempresasispag) {
		this.codigoempresasispag = codigoempresasispag;
	}
	public void setNossonumerointervalo(Boolean nossonumerointervalo) {
		this.nossonumerointervalo = nossonumerointervalo;
	}
	public void setNossonumeroinicial(Integer nossonumeroinicial) {
		this.nossonumeroinicial = nossonumeroinicial;
	}
	public void setNossonumerofinal(Integer nossonumerofinal) {
		this.nossonumerofinal = nossonumerofinal;
	}
	@DisplayName("Taxa do Boleto")
	public Money getTaxaboleto() {
		return taxaboleto;
	}

	public void setTaxaboleto(Money taxaboleto) {
		this.taxaboleto = taxaboleto;
	}

	@DisplayName("N�o gerar boleto")
	public Boolean getNaogerarboleto() {
		return naogerarboleto;
	}

	public void setNaogerarboleto(Boolean naogerarboleto) {
		this.naogerarboleto = naogerarboleto;
	}

	@OneToMany(mappedBy = "conta")
	@DisplayName("N�vel de Acesso")
	public List<Contapapel> getListaContapapel() {
		return listaContapapel;
	}

	public void setListaContapapel(List<Contapapel> listaContapapel) {
		this.listaContapapel = listaContapapel;
	}

	@OneToMany(mappedBy = "conta")
	@DisplayName("Cobran�a")
	public List<Contacarteira> getListaContacarteira() {
		return listaContacarteira;
	}

	public void setListaContacarteira(List<Contacarteira> listaContacarteira) {
		this.listaContacarteira = listaContacarteira;
	}
	
	@OneToMany(mappedBy = "conta")
	@DisplayName("Empresas")
	public Set<Contaempresa> getListaContaempresa() {
		return listaContaempresa;
	}
	@DisplayName("Restri��o por Projeto")
	@OneToMany(mappedBy="conta")
	public Set<Contaprojeto> getListaContaprojeto() {
		return listaContaprojeto;
	}

	public void setListaContaempresa(Set<Contaempresa> listaContaempresa) {
		this.listaContaempresa = listaContaempresa;
	}
	public void setListaContaprojeto(Set<Contaprojeto> listaContaprojeto) {
		this.listaContaprojeto = listaContaprojeto;
	}



	/**
	 * Se houver somente uma carteira ela � retornada.
	 * Se houver mais de uma carteira, � retornada a carteira marcada como padr�o.
	 * Se n�o houver carteira padr�o, retorna a primeira.
	 */
	@Transient
	public Contacarteira getContacarteira() {
		Contacarteira contacarteira = null;
		
		if (listaContacarteira != null && listaContacarteira.size() > 0){
			if (listaContacarteira.size() == 1){
				contacarteira = listaContacarteira.get(0);
			} else {				
				for (Contacarteira cc : listaContacarteira){
					if (cc.isPadrao()){
						contacarteira = cc;
						break;
					}						
				}
				if (contacarteira == null)
					contacarteira = listaContacarteira.get(0);
			}
		}
		
		if (contacarteira == null)
			contacarteira = new Contacarteira();
		
		return contacarteira;
	}

	@DisplayName("Opera��o Cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}

	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setContaContabil(ContaContabil contacontabil) {
		this.contaContabil = contacontabil;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxa")
	public Taxa getTaxa() {
		return taxa;
	}
	public void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}
	
	@DisplayName("Considerar n�mero do documento na remessa")
	public Boolean getConsiderarnumerodocumentoremessa() {
		return considerarnumerodocumentoremessa;
	}

	public void setConsiderarnumerodocumentoremessa(Boolean considerarnumerodocumentoremessa) {
		this.considerarnumerodocumentoremessa = considerarnumerodocumentoremessa;
	}
	
	@DisplayName("Desconsiderar saldo inicial no fluxo de caixa")
	public Boolean getDesconsiderarsaldoinicialfluxocaixa() {
		return desconsiderarsaldoinicialfluxocaixa;
	}
	
	public void setDesconsiderarsaldoinicialfluxocaixa(
			Boolean desconsiderarsaldoinicialfluxocaixa) {
		this.desconsiderarsaldoinicialfluxocaixa = desconsiderarsaldoinicialfluxocaixa;
	}
	
	@Transient
	public Empresa getEmpresa(){
		if(this.listaContaempresa != null && this.listaContaempresa.size() > 0){
			return this.listaContaempresa.iterator().next().getEmpresa();
		} 
		return null;
	}
	
	@Transient
	public List<Contacarteira> getListaContacarteiraQueNaoAssociamcontareceber() {
		List<Contacarteira> retorno = new ArrayList<Contacarteira>();
		if(this.listaContacarteira != null){
			for(Contacarteira contacarteira: this.listaContacarteira){
				if(Boolean.TRUE.equals(contacarteira.getNaoassociarcontareceber())){
					retorno.add(contacarteira);
				}
			}
		}
		return retorno;
	}
	
	@Transient
	public List<Contacarteira> getListaContacarteiraQueGeraBoleto() {
		if(listaContacarteiraQueGeraBoleto == null){
			listaContacarteiraQueGeraBoleto = new ArrayList<Contacarteira>();
		}else{
			listaContacarteiraQueGeraBoleto.clear();
		}
		
		if(this.listaContacarteira != null){
			for(Contacarteira contacarteira: this.listaContacarteira){
				if(!Boolean.TRUE.equals(contacarteira.getNaoassociarcontareceber())){
					listaContacarteiraQueGeraBoleto.add(contacarteira);
				}
			}
		}
		return listaContacarteiraQueGeraBoleto;
	}

	@Override
	public String subQueryProjeto() {
		return "select contasubQueryProjeto.cdconta " +
				"from Conta contasubQueryProjeto " +
				"left outer join contasubQueryProjeto.listaContaprojeto listaContaprojetosubQueryProjeto " +
				"left outer join listaContaprojetosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where (listaContaprojetosubQueryProjeto is null or projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") )";
	}	
 
	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public String getAgenciaComDVSomenteNumero(){
		if(getAgencia() != null && getDvagencia() != null) return getAgencia() + getDvagencia(); 
		if(getAgencia() != null) return getAgencia();
		return "";
	}
	
	@Transient
	public String getNumeroComDVSomenteNumero(){
		if(getNumero() != null && getDvnumero() != null) return getNumero() + getDvnumero(); 
		if(getNumero() != null) return getNumero();
		return "";
	}
}
