package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Fator de m�o-de-obra")
@SequenceGenerator(name = "sq_fatormdo", sequenceName = "sq_fatormdo")
public class Fatormdo {
	
	protected Integer cdfatormdo;
	protected Orcamento orcamento;
	protected String descricao;
	protected Double total;
	protected String formula;
	protected List<Fatormdoitem> listaFatormdoitem = new ArrayList<Fatormdoitem>();	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fatormdo")
	public Integer getCdfatormdo() {
		return cdfatormdo;
	}
	
	@DisplayName("Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required	
	public Orcamento getOrcamento() {
		return orcamento;
	}

	@DisplayName("Descri��o")
	@MaxLength(30)
	@DescriptionProperty
	@Required	
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@MinValue(0)
	@MaxValue(999999999)
	public Double getTotal() {
		return total;
	}
	
	@DisplayName("F�rmula")
	@MaxLength(50)
	@Required	
	public String getFormula() {
		return formula;
	}
	
	@OneToMany(mappedBy="fatormdo")
	@DisplayName("Itens do fator de m�o-de-obra")	
	public List<Fatormdoitem> getListaFatormdoitem() {
		return listaFatormdoitem;
	}

	public void setCdfatormdo(Integer cdfatormdo) {
		this.cdfatormdo = cdfatormdo;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}	

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}
	
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	public void setListaFatormdoitem(List<Fatormdoitem> listaFatormdoitem) {
		this.listaFatormdoitem = listaFatormdoitem;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Fatormdo) {
			Fatormdo that = (Fatormdo) obj;
			return this.getCdfatormdo().equals(that.getCdfatormdo());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdfatormdo != null) {
			return cdfatormdo.hashCode();
		}
		return super.hashCode();
	}
}
