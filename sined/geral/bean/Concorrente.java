package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_concorrente",sequenceName="sq_concorrente")
public class Concorrente implements Log{
	
	private Integer cdconcorrente;
	private String nome;
	private String telefone;
	private String site;
	private String email;
	private String produtos;
	private String estrategiamarketing;
	private String forcas;
	private String fraquezas;

	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Concorrente() {
	}
	
	public Concorrente(Integer cdconcorrente) {
		this.cdconcorrente = cdconcorrente;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_concorrente",strategy=GenerationType.AUTO)
	public Integer getCdconcorrente() {
		return cdconcorrente;
	}
	
	public void setCdconcorrente(Integer cdconcorrente) {
		this.cdconcorrente = cdconcorrente;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@MaxLength(20)
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	@MaxLength(100)
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	
	@MaxLength(100)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@MaxLength(2000)
	public String getProdutos() {
		return produtos;
	}
	
	public void setProdutos(String produtos) {
		this.produtos = produtos;
	}
	
	@MaxLength(2000)
	@DisplayName("Estrat�gia e marketing")
	public String getEstrategiamarketing() {
		return estrategiamarketing;
	}
	
	public void setEstrategiamarketing(String estrategiamarketing) {
		this.estrategiamarketing = estrategiamarketing;
	}
	
	@MaxLength(2000)
	@DisplayName("For�as")
	public String getForcas() {
		return forcas;
	}
	
	public void setForcas(String forcas) {
		this.forcas = forcas;
	}
	
	@MaxLength(2000)
	public String getFraquezas() {
		return fraquezas;
	}
	
	public void setFraquezas(String fraquezas) {
		this.fraquezas = fraquezas;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
