package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
//import br.com.linkcom.sined.geral.bean.enumeration.TipoEventoEnum;

@Entity
@SequenceGenerator(name = "sq_eventoscorreio", sequenceName = "sq_eventoscorreio")
public class EventosCorreios {
	protected Integer cdeventoscorreios;
	//protected TipoEventoEnum tipoEvento;
	protected TipoEventoCorreios tipoEventoCorreios;
	protected Integer codigo;
	protected String descricao;
	protected String detalhe;
	protected String descricaoCompleta;
	protected String descricaoComplta;
	protected Boolean eventoFinal;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_eventoscorreio")
	public Integer getCdeventoscorreios() {
		return cdeventoscorreios;
	}
	public void setCdeventoscorreios(Integer cdeventoscorreios) {
		this.cdeventoscorreios = cdeventoscorreios;
	}
	/*
	public TipoEventoEnum getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(TipoEventoEnum tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	*/
	@DisplayName("Tipo de evento")
	@ManyToOne(fetch=FetchType.LAZY)
	//@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="cdtipoeventocorreios")
	public TipoEventoCorreios getTipoEventoCorreios() {
		return tipoEventoCorreios;
	}
	public void setTipoEventoCorreios(TipoEventoCorreios tipoEventoCorreios) {
		this.tipoEventoCorreios = tipoEventoCorreios;
	}
	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDetalhe() {
		return detalhe;
	}
	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}
	@Transient
	@DescriptionProperty
	public String getDescricaoCompleta() {
		if(!this.detalhe.isEmpty()){
			descricaoCompleta = this.descricao + "(" + this.detalhe + ")";
		}else{
			descricaoCompleta = this.descricao;
		}
		return descricaoCompleta + " - " + this.codigo + "";
	}
	public void setDescricaoCompleta(String descricaoCompleta) {
		this.descricaoCompleta = descricaoCompleta;
	}
	public Boolean getEventoFinal() {
		return eventoFinal;
	}
	public void setEventoFinal(Boolean eventoFinal) {
		this.eventoFinal = eventoFinal;
	}
}
