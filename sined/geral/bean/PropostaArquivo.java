package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_propostaarquivo", sequenceName = "sq_propostaarquivo")
public class PropostaArquivo implements Log{
	
	protected Integer cdpropostaarquivo;
	protected String descricao;
	protected Proposta proposta;
	protected Arquivo arquivo;
	
//	CAMPOS DE LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_propostaarquivo")
	public Integer getCdpropostaarquivo() {
		return cdpropostaarquivo;
	}
	
	public void setCdpropostaarquivo(Integer cdpropostaarquivo) {
		this.cdpropostaarquivo = cdpropostaarquivo;
	}
	
	@Required
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproposta")
	public Proposta getProposta() {
		return proposta;
	}
	
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
