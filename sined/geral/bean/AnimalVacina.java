package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_animalvacina", sequenceName="sq_animalvacina")
public class AnimalVacina {
	
	private Integer cdanimalvacina;
	private Date proximaaplicacao;
	private Date data;
	private Material produto;
	private Animal animal;
	private AnimalVacina animalVacinaProxima; //Se tiver vazio significa que n�o foi feita aplica��o. Se tiver preenchido � que foi feita a aplica��o, sendo que este campo indica qual a vacina que foi gerada.
	
	@Id
	@GeneratedValue(generator="sq_animalvacina", strategy=GenerationType.AUTO)
	public Integer getCdanimalvacina() {
		return cdanimalvacina;
	}
	
	public Date getProximaaplicacao() {
		return proximaaplicacao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmaterial")
	public Material getProduto() {
		return produto;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdanimal")
	public Animal getAnimal() {
		return animal;
	}
	
	public Date getData() {
		return data;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdanimalvacinaproxima")
	public AnimalVacina getAnimalVacinaProxima() {
		return animalVacinaProxima;
	}
	
	public void setCdanimalvacina(Integer cdanimalvacina) {
		this.cdanimalvacina = cdanimalvacina;
	}
	
	public void setProduto(Material produto) {
		this.produto = produto;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	public void setProximaaplicacao(Date proximaaplicacao) {
		this.proximaaplicacao = proximaaplicacao;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	public void setAnimalVacinaProxima(AnimalVacina animalVacinaProxima) {
		this.animalVacinaProxima = animalVacinaProxima;
	}
}
