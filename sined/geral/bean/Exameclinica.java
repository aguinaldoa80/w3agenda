package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_exameclinica", sequenceName = "sq_exameclinica")
@DisplayName("Cl�nica")
public class Exameclinica implements Log {

	protected Integer cdexameclinica;
	protected String nome;
	protected Arquivo logotipo;
	protected String endereco;
	protected br.com.linkcom.neo.types.Telefone telefone;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected List<Exameconvenioclinica> listaExameconvenioclinica = new ListSet<Exameconvenioclinica>(Exameconvenioclinica.class);

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exameclinica")
	public Integer getCdexameclinica() {
		return cdexameclinica;
	}
	public void setCdexameclinica(Integer id) {
		this.cdexameclinica = id;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome.trim();
	}
	
	@DisplayName("Conv�nios")
	@OneToMany(mappedBy="exameclinica")
	public List<Exameconvenioclinica> getListaExameconvenioclinica() {
		return listaExameconvenioclinica;
	}
	
	public void setListaExameconvenioclinica(
			List<Exameconvenioclinica> listaExameconvenioclinica) {
		this.listaExameconvenioclinica = listaExameconvenioclinica;
	}
	
	@DisplayName("Logo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlogotipo")
	public Arquivo getLogotipo() {
		return logotipo;
	}

	
	public void setLogotipo(Arquivo logotipo) {
		this.logotipo = logotipo;
	}
	
	@Required
	@MaxLength(100)
	@DisplayName("Endere�o completo")
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	@Required
	@MaxLength(20)
	public br.com.linkcom.neo.types.Telefone getTelefone() {
		return telefone;
	}
	
	public void setTelefone(br.com.linkcom.neo.types.Telefone telefone) {
		this.telefone = telefone;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
