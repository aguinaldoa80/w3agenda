package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_planejamentorecursohumano",sequenceName="sq_planejamentorecursohumano")
public class Planejamentorecursohumano implements Log, PermissaoProjeto{

	protected Integer cdplanejamentorecursohumano;
	protected Cargo cargo;
	protected Double qtde;
	protected Date dtinicio;
	protected Date dtfim;
	protected Planejamento planejamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_planejamentorecursohumano",strategy=GenerationType.AUTO)
	public Integer getCdplanejamentorecursohumano() {
		return cdplanejamentorecursohumano;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	@DisplayName("Qtde. HH")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdplanejamentorecursohumano(Integer cdplanejamentorecursohumano) {
		this.cdplanejamentorecursohumano = cdplanejamentorecursohumano;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public String subQueryProjeto() {
		return "select planejamentorecursohumanosubQueryProjeto.cdplanejamentorecursohumano " +
				"from Planejamentorecursohumano planejamentorecursohumanosubQueryProjeto " +
				"join planejamentorecursohumanosubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
}
