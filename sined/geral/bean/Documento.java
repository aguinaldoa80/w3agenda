package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.neo.validation.annotation.ValidationOverrides;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_documento;
import br.com.linkcom.sined.geral.bean.enumeration.Identificacaotributo;
import br.com.linkcom.sined.geral.bean.enumeration.Opcaopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoremessaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipopagamento;
import br.com.linkcom.sined.geral.bean.view.Vdocumentonegociado;
import br.com.linkcom.sined.geral.bean.view.Vwcountmovimentacaonormal;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_documento", sequenceName = "sq_documento")
@JoinEmpresa("documento.empresa")
public class Documento implements Log, PermissaoProjeto, PermissaoClienteEmpresa, Cloneable{

	protected Integer cddocumento;
	protected Integer cddocumentorelacionado;
	protected Documentoclasse documentoclasse;
	protected Documentotipo documentotipo;
	protected Documentoacao documentoacao = Documentoacao.DEFINITIVA;
	protected String descricao;
	protected String numero;
	protected Cliente cliente;
	protected Fornecedor fornecedor;
	protected Colaborador colaborador;
	protected Pessoa pessoa;
	protected Date dtemissao;
	protected Date dtvencimento;
	protected Date dtdevolucao;
	protected Money valor;
	protected Rateio rateio;
	protected Taxa taxa;
	protected String observacao;
	protected Referencia referencia = Referencia.MES_ANO_CORRENTE;
	protected Set<Parcela> listaParcela;
	protected Set<Documentohistorico> listaDocumentohistorico;
	protected List<HistoricoAntecipacao> listaHistoricoAntecipacao;
	protected List<HistoricoAntecipacao> listaHistoricoAntecipacaoDestino;
	protected Set<Arquivobancariodocumento> listaArqBancarioDoc;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Date dtcartorio;
	protected Documentoacao documentoacaoprotesto;
	protected List<Documentoautori> listaDocumentoautori;
	protected Aux_documento aux_documento;
	protected Empresa empresa;
	protected Tipopagamento tipopagamento;
	protected String outrospagamento;
	protected Indicecorrecao indicecorrecao;
	protected Set<Movimentacaoorigem> listaMovimentacaoOrigem = new ListSet<Movimentacaoorigem>(Movimentacaoorigem.class);
	protected List<Documentoorigem> listaDocumentoOrigem;
	protected List<NotaDocumento> listaNotaDocumento;
	protected Set<Speddocumento> listaSpeddocumento;
	protected Set<Valecompraorigem> listaValecompraorigem;
	protected Set<Boletodigital> listaBoletodigital;
	protected Arquivo imagem;
	protected String nossonumero;
	protected String codigobarras;
	protected Cheque cheque;
	protected Cheque chequeBaixarconta;
	protected Boolean conciliacaoPendente = false;
	protected Date dtvencimentooriginal;
	protected List<Vwcountmovimentacaonormal> listaVwcountmovimentacaonormal;
	protected Boolean somentenossonumero;
	protected Boolean boletogerado;	
	protected Boolean rejeitadobanco;	
	protected Operacaocontabil operacaocontabil;
	protected Conta vinculoProvisionado;
	protected Date dtultimacobranca;
	protected Money valorPrevista;
	protected String descriacaoCorrecao;
	protected Boolean associadomanualmente;
	
	protected String codigoreceita;
	protected Date mesano;
	protected Money valoroutrasentidades;
	protected Money valoratualizacaomonetaria;
	protected Date dtapuracao;
	protected String numeroreferencia;
	protected Money valorreceitabrutaacumulada;
	protected Money percentualreceitabrutaacumulada;
	protected Identificacaotributo identificacaotributo;
	protected String iecodmunnumdec;
	protected String dividaativanumetiqueta;
	protected String numparcnotificacao;
	protected Integer anobase;
	protected String renavam;
	protected Uf uf;
	protected Municipio municipio;
	protected String placaveiculo;
	protected Opcaopagamento opcaopagamento;
	protected Date dtvencimentoantiga;//Sempre pen�ltima data de vencimento
	protected Boolean cancelamentocobranca;
	protected Boolean emissaoboletobloqueado;
	protected Date dtvencimentooinicial;
	protected Documentoacao documentoacaoanteriornegociacao;
	protected Boolean antecipado;
	protected Date dtantecipacao;
	protected Date dtcompetencia;
	protected List<DocumentoApropriacao> listaApropriacao;
	
	//Boleto
	protected Conta conta;
	protected Contacarteira contacarteira;
	protected Endereco endereco;
	protected String mensagem1;
	protected String mensagem2;
	protected String mensagem3;
	protected String mensagem4;
	protected String mensagem5;
	protected String mensagem6;
	
	//Transient
	protected Agendamentoservicodocumento agendamentoservicodocumento;
	protected boolean vencida;
	protected boolean protestada;
	protected Documentoacao acaoanterior;
	protected Documentoacao acaohistorico;
	protected boolean edicao;
	protected Integer repeticoes;
	protected Prazopagamento prazo;
	protected Integer parcela;
	protected Boolean financiamento;
	protected List<Documento> listaDocumento;
	protected String ids;
	protected Money valorautorizado;
	protected Integer cdAgendamento;
	protected Integer cdNota;
	protected String numeroNota;
	protected Long diferenca;
	protected Money valoratual;
	protected Money valoratualbaixa;
	protected Money valoratualbaixaSemTaxa = null;
	protected Money moneyTaxas = new Money();
	protected String origemStr;
	protected Entrega entrega;
	protected String numParcela;
	protected String idsFornecimento;
	protected String idsVeiculosDespesa;
	protected Boolean fromFornecimento;
	protected Boolean fromDespesaVeiculo;
	protected Boolean fromPagamentoComissao = false;
	protected Boolean fromPagamentoComissaoVenda = false;
	protected Boolean fromPagamentoComissaoDesempenho = false;
	protected Boolean fromPagamentoComissaoPorFaixas = false;
	protected String whereInDocumentoorigemDesempenho;
	protected String whereInNotadocumentoDesempenho;
	protected String whereInVendaDesempenho;
	protected Boolean selected;
	protected String controller;
	protected String whereInDespesaviagem;
	protected String whereInDespesaviagemReembolso;
	protected String whereInOrdemcompra;
	protected Boolean adiatamentodespesa;
	protected Nota nota;
	protected String strDtbaixa;
	protected Boolean fromRetorno = false;
	protected Boolean fromWebservice = false;
	protected Boolean fromBaixaautomatica = false;
	protected Money valorparcela;
	protected Date dataparcela;
	protected String banco;
	protected String numerocheque;
	protected String contacorrente;
	protected Integer agencia;
	protected Formapagamento formapagamento;
	protected Contrato contrato;
	protected Contratojuridico contratojuridico;
	protected String nomeProcessoFluxoAnterior;
	protected String contaReceberDevolucao;
	protected Boolean reciboProjeto;
	protected Boolean existeProjetoRateio;
	protected String informacoescedente;
	protected Money valorboleto;
	protected List<Documentocomissao> listaDocumentocomissao = new ListSet<Documentocomissao>(Documentocomissao.class);
	protected Double valorDouble;
	protected String whereIn;
	protected String whereInEntrega;
	protected String whereInEntregadocumento;
	protected String whereInNota;
	protected String tipo;
	protected String selecteditens;
	protected java.util.Date dataAtrasada;
	protected String dtvencimentooriginalString;
	protected String numeroValor;
	protected String numeroCddocumento;
	protected Date ultimoFechamento;
	protected Boolean emitirCarne;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected Boolean fromGerarreceitanota;
	protected Boolean fromFechamento;
	protected String numeroPessoaDescricao;
	protected String descricaoOrdemcompraEntrega;
	protected String codigobarrascheque;
	protected String historicoBaixaRetorno;
	protected Boolean confirmadobanco;
	protected Date dtagendamentobanco;
	protected Boolean faturarlocacao;
	protected Boolean finalizarContrato;
	protected Money valorRestante;
	protected Boolean faturamentolote;
	protected List<Contrato> listaContrato = new ArrayList<Contrato>();
	protected Boolean gerarreceitafaturalocacao;
	protected Integer cdcontratofaturalocacao;
	protected List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento;
	protected Documento contaRestanteDe;
	protected Money valorPago;
	protected Boolean fromAcaoProtestar;
	protected Contagerencial contagerencialDespesaRepresentacao;
	protected Centrocusto centrocustoDespesaRepresentacao;
	protected Coleta coleta;
	protected String observacaoHistorico;
	protected Money saldoAntecipacao;
	protected Money valorAntecipacao;
	protected String idAndDescricao;
	protected Documento contareceberRepresentacao;
	protected Date datapagamento;
	protected String whereInChequeDevolucao;
	protected Integer cdveiculo;
	protected List<Venda> listaVenda;
	protected List<Pedidovenda> listaPedidovenda;
	protected Boolean isDtVencimentoAlterada;
	protected String dtvencimentoAntigaStr;
	protected Date dtvencimentoAux;
	protected Boolean copiadocumeto = Boolean.FALSE;
	protected String numerofaturalocacao;
	protected String datasBaixa;
	protected Boolean existeDuplicata;
	protected Documentotipo documentotipoTransient;
	protected Money valortaxadespesa;
	protected SituacaoremessaEnum situacaoremessa;
	protected Nota notaTransient;
	protected List<Nota> listaNotaTransient;
	protected Money valorTotalTaxaMovimentacao;
	protected Rateiomodelo rateiomodelo;
	protected String identificadorpessoa;
	protected Boolean podeAlterarValor;
	protected Documentohistorico documentohistoricoBaixa;
	protected Pedidovenda origemPedidovenda;
	protected Venda origemVenda;
	protected Integer cddocumentoOrigemVenda;
	protected Money valorTituloValeCompra;
	protected Money valorPagoValeCompra;
	protected Money valorJurosMultaValeCompra;
	protected Money valorDescontoOutroDescontoValeCompra;
	protected Integer diferencaDias;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Money valortotalpago;
	protected Set<Vdocumentonegociado> listaDocumentonegociado;
	protected Set<Vdocumentonegociado> listaDocumentonegociacao;
	protected String nfgts;
	protected String lacreconectividade;
	protected String digitolacre;
	protected String whereInDocumentoAdiantamento;
	protected String documentoAdiantamentoValores;
	protected Boolean possuiSaldoAdiantamento;
	protected Boolean possuiSaldoNaoCompensado;
	protected Boolean isCompensarSaldo;
	protected boolean isDescricaoHistorico;
	protected String razaosocialPessoa;
	protected String whereInGnre;
	
	// An�lise de Receitas e Despesas
	protected Boolean isMovimentacao;
	protected String tipoRegistro;
	
	public Documento() {
	}
	public Documento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}
	
	/*
	 * CONSTRUTOR PARA O SELECT DO M�TODO br.com.linkcom.sined.geral.dao.DocumentoDAO.loadForListagem
	 */
	public Documento(
			Integer documento_cddocumento, 
			String documento_descricao, 
			Referencia documento_referencia, 
			java.util.Date documento_dtcompetencia,
			String cliente_razaosocial,
			java.util.Date documento_dtcartorio, 
			String documento_numero, 
			java.util.Date documento_dtdevolucao,
			java.util.Date documento_dtemissao,
			java.util.Date documento_dtvencimento,
			Money documento_valor, 
			Integer classe_cddocumentoclasse, 
			String classe_nome, 
			String documento_outrospagamento, 
			java.util.Date documento_dtvencimentooriginal,
			String pessoa_nome,
			Integer pessoa_cdpessoa,
			Integer documentotipo_cddocumentotipo,
			String documentotipo_nome,
			Boolean documentotipo_antecipacao,
			Integer documentoacao_cddocumentoacao, 
			String documentoacao_nome, 
			Money aux_documento_valoratual, 
			String documento_codigobarras,
			Integer parcela_cdparcela, 
			Integer parcela_ordem, 
			Integer parcelamento_cdparcelamento, 
			Integer parcelamento_iteracoes, 
			String coalesce,
			Long countMovimentacaoNormal,
			Boolean documento_rejeitadobanco,
			Integer documentoacaoprotesto_cddocumentoacao,
			String documentoacaoprotesto_nome,
			Tipopessoa pessoa_tipopessoa,
			java.util.Date documento_dtvencimentoantiga,
			Boolean documento_cancelamentocobranca,
			Boolean documento_emissaoboletobloqueado,
			Tipopagamento documento_tipopagamento,
			String fornecedor_razaosocial,
			Boolean antecipado,
			java.util.Date dtantecipacao
	){
		cddocumento = documento_cddocumento;
		descricao = documento_descricao;
		referencia = documento_referencia;
		dtcartorio = documento_dtcartorio != null ? new Date(documento_dtcartorio.getTime()) : null;
		numero = documento_numero;
		dtdevolucao = documento_dtdevolucao != null ? new Date(documento_dtdevolucao.getTime()) : null;
		dtemissao = documento_dtemissao != null ? new Date(documento_dtemissao.getTime()) : null;
		dtvencimento = documento_dtvencimento != null ? new Date(documento_dtvencimento.getTime()) : null;
		dtcompetencia = documento_dtcompetencia != null ? new Date(documento_dtcompetencia.getTime()) : null;
		valor = documento_valor;
		documentoclasse = new Documentoclasse(classe_cddocumentoclasse, classe_nome);
		outrospagamento = documento_outrospagamento;
		dtvencimentooriginal = documento_dtvencimentooriginal != null ? new Date(documento_dtvencimentooriginal.getTime()) : null;
		dtvencimentoantiga = documento_dtvencimentoantiga != null ? new Date(documento_dtvencimentoantiga.getTime()) : null;
		pessoa = new Pessoa(pessoa_cdpessoa, pessoa_nome, pessoa_tipopessoa);
		documentotipo = new Documentotipo(documentotipo_cddocumentotipo, documentotipo_nome, documentotipo_antecipacao);
		documentoacao = new Documentoacao(documentoacao_cddocumentoacao, documentoacao_nome);
		aux_documento = new Aux_documento(aux_documento_valoratual);
		codigobarras = documento_codigobarras;
		cliente = new Cliente(pessoa_nome, pessoa_tipopessoa);
		cliente.setRazaosocial(cliente_razaosocial);
		rejeitadobanco = documento_rejeitadobanco;
		if(parcela_cdparcela != null){
			listaParcela = new ListSet<Parcela>(Parcela.class);
			Parcelamento parcelamento = new Parcelamento(parcelamento_cdparcelamento, parcelamento_iteracoes);
			Parcela parcela = new Parcela(parcela_cdparcela, parcela_ordem, parcelamento);
			listaParcela.add(parcela);
		}		
		if (countMovimentacaoNormal != null && countMovimentacaoNormal > 0)
			conciliacaoPendente = true;		
		if(documentoacaoprotesto_cddocumentoacao != null){
			documentoacaoprotesto = new Documentoacao(documentoacaoprotesto_cddocumentoacao, documentoacaoprotesto_nome);
		}
		cancelamentocobranca = documento_cancelamentocobranca;
		emissaoboletobloqueado = documento_emissaoboletobloqueado;
		tipopagamento = documento_tipopagamento;
		fornecedor = new Fornecedor(pessoa_nome);
		fornecedor.setRazaosocial(fornecedor_razaosocial);
		setAntecipado(antecipado);
		setDtantecipacao(dtantecipacao != null ? new Date(dtantecipacao.getTime()) : null);
	}
	
	/*
	 * CONSTRUTOR PARA O SELECT DO M�TODO br.com.linkcom.sined.geral.dao.DocumentoDAO.updateListagemQuery
	 */
	public Documento(
			Integer documento_cddocumento, 
			java.util.Date documento_dtemissao,
			java.util.Date documento_dtvencimento,
			java.util.Date documento_dtcompetencia,
			String cliente_razaosocial,
			String coalesce,
			String documento_numero, 
			String documento_descricao, 
			Integer documentoacao_cddocumentoacao, 
			Money aux_documento_valoratual,
			Money documento_valor,
			java.util.Date documento_dtvencimentoantiga
	){
		cddocumento = documento_cddocumento;
		descricao = documento_descricao;
		numero = documento_numero;
		valor = documento_valor;
		dtemissao = documento_dtemissao != null ? new Date(documento_dtemissao.getTime()) : null;
		dtvencimento = documento_dtvencimento != null ? new Date(documento_dtvencimento.getTime()) : null;
		dtcompetencia = documento_dtcompetencia != null ? new Date(documento_dtcompetencia.getTime()) : null;
		documentoacao = new Documentoacao(documentoacao_cddocumentoacao);
		aux_documento = new Aux_documento(aux_documento_valoratual);
		cliente = new Cliente();
		cliente.setRazaosocial(cliente_razaosocial);
		dtvencimentoantiga = documento_dtvencimentoantiga != null ? new Date(documento_dtvencimentoantiga.getTime()) : null;
	}
	
	public Documento(Integer cddocumento, Integer cddocumentoacao, Date dtemissao) {
		this.cddocumento = cddocumento;
		this.documentoacao = new Documentoacao(cddocumentoacao);
		this.dtemissao = dtemissao;
	}
	
	public Documento(Integer cddocumento, Integer cddocumentoacao, Date dtvencimento, Date dtemissao) {
		this.cddocumento = cddocumento;
		this.documentoacao = new Documentoacao(cddocumentoacao);
		this.dtemissao = dtemissao;
		this.dtvencimento = dtvencimento;
	}
	
	@Id
	@DisplayName("ID")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documento")
	public Integer getCddocumento() {
		return cddocumento;
	}
	public void setCddocumento(Integer id) {
		this.cddocumento = id;
	}
	public void setCddocumentorelacionado(Integer cddocumentorelacionado) {
		this.cddocumentorelacionado = cddocumentorelacionado;
	}
	public Integer getCddocumentorelacionado() {
		return cddocumentorelacionado;
	}
	@Required
	@DisplayName("Classe")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoclasse")
	public Documentoclasse getDocumentoclasse() {
		return documentoclasse;
	}
	@Required
	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(500)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Documento")
	@MaxLength(200)
	@DescriptionProperty
	public String getNumero() {
		return numero;
	}
	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Transient
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	@Required
	@DisplayName("Data de emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@Required
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@DisplayName("Data de devolu��o")
	public Date getDtdevolucao() {
		return dtdevolucao;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxa")
	public Taxa getTaxa() {
		return taxa;
	}
	
	@Transient
	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacaoHistorico() {
		return observacaoHistorico;
	}
	@Required
	@DisplayName("Refer�ncia")
	public Referencia getReferencia() {
		return referencia;
	}
	@OneToMany(mappedBy="documento")
	public Set<Parcela> getListaParcela() {
		return listaParcela;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="documento")
	public Set<Documentohistorico> getListaDocumentohistorico() {
		return listaDocumentohistorico;
	}
	@DisplayName("Hist�rico Antecipa��o")
	@OneToMany(mappedBy="documento")
	public List<HistoricoAntecipacao> getListaHistoricoAntecipacao() {
		return listaHistoricoAntecipacao;
	}
	@DisplayName("Hist�rico Antecipa��o Destino")
	@OneToMany(mappedBy="documentoReferencia")
	public List<HistoricoAntecipacao> getListaHistoricoAntecipacaoDestino() {
		return listaHistoricoAntecipacaoDestino;
	}
	
	@OneToMany(mappedBy="documento")
	public Set<Arquivobancariodocumento> getListaArqBancarioDoc() {
		return listaArqBancarioDoc;
	}
	@DisplayName("Data de entrada no cart�rio")
	public Date getDtcartorio() {
		return dtcartorio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacaoprotesto")
	public Documentoacao getDocumentoacaoprotesto() {
		return documentoacaoprotesto;
	}
	@OneToMany(mappedBy="documento")
	public List<Documentoautori> getListaDocumentoautori() {
		return listaDocumentoautori;
	}
	@OneToMany(mappedBy="documento")
	public Set<Speddocumento> getListaSpeddocumento() {
		return listaSpeddocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento", insertable=false, updatable=false)
	public Aux_documento getAux_documento() {
		return aux_documento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Pagamento a")
	public Tipopagamento getTipopagamento() {
		return tipopagamento;
	}
	@DisplayName("Outros")
	@MaxLength(100)
	public String getOutrospagamento() {
		return outrospagamento;
	}
	@OneToMany(mappedBy="documento")
	public Set<Movimentacaoorigem> getListaMovimentacaoOrigem() {
		return listaMovimentacaoOrigem;
	}
	@OneToMany(mappedBy="documento")
	public List<Documentoorigem> getListaDocumentoOrigem() {
		return listaDocumentoOrigem;
	}
	@OneToMany(mappedBy="documento")
	public List<NotaDocumento> getListaNotaDocumento() {
		return listaNotaDocumento;
	}
	
	@DisplayName("Conta banc�ria")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}
	@DisplayName("Carteira")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacarteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	@DisplayName("Endere�o cobran�a")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecocobranca")
	public Endereco getEndereco() {
		return endereco;
	}
	@DisplayName("�ndice de corre��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindicecorrecao")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	@DisplayName("Mensagem 1")
	@MaxLength(80)
	public String getMensagem1() {
		return mensagem1;
	}
	@DisplayName("Mensagem 2")
	@MaxLength(80)
	public String getMensagem2() {
		return mensagem2;
	}
	@DisplayName("Mensagem 3")
	@MaxLength(80)
	public String getMensagem3() {
		return mensagem3;
	}
	@DisplayName("Mensagem 4")
	@MaxLength(80)
	public String getMensagem4() {
		return mensagem4;
	}
	@DisplayName("Mensagem 5")
	@MaxLength(80)
	public String getMensagem5() {
		return mensagem5;
	}
	@DisplayName("Mensagem 6")
	@MaxLength(80)
	public String getMensagem6() {
		return mensagem6;
	}
	@DisplayName("Nosso n�mero")
	public String getNossonumero() {
		return nossonumero;
	}
	@MaxLength(200)
	@DisplayName("C�digo de barras")
	public String getCodigobarras() {
		return codigobarras;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcheque")
	public Cheque getCheque() {
		return cheque;
	}
	
	@OneToMany(mappedBy="documento")
	public List<Vwcountmovimentacaonormal> getListaVwcountmovimentacaonormal() {
		return listaVwcountmovimentacaonormal;
	}

	@DisplayName("V�nculo Provisionado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvinculoprovisionado")
	public Conta getVinculoProvisionado() {
		return vinculoProvisionado;
	}
	public void setVinculoProvisionado(Conta vinculoProvisionado) {
		this.vinculoProvisionado = vinculoProvisionado;
	}
	public void setListaVwcountmovimentacaonormal(
			List<Vwcountmovimentacaonormal> listaVwcountmovimentacaonormal) {
		this.listaVwcountmovimentacaonormal = listaVwcountmovimentacaonormal;
	}
	
	@Column(insertable=false, updatable=false)
	public Date getDtvencimentooriginal() {
		return dtvencimentooriginal;
	}
	public void setDtvencimentooriginal(Date dtvencimentooriginal) {
		this.dtvencimentooriginal = dtvencimentooriginal;
	}
	public void setNumeroValor(String numeroValor) {
		this.numeroValor = numeroValor;
	}	
	public void setNumeroCddocumento(String numeroCddocumento) {
		this.numeroCddocumento = numeroCddocumento;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setMensagem1(String mensagem1) {
		this.mensagem1 = mensagem1;
	}
	public void setMensagem2(String mensagem2) {
		this.mensagem2 = mensagem2;
	}
	public void setMensagem3(String mensagem3) {
		this.mensagem3 = mensagem3;
	}
	public void setMensagem4(String mensagem4) {
		this.mensagem4 = mensagem4;
	}
	public void setMensagem5(String mensagem5) {
		this.mensagem5 = mensagem5;
	}
	public void setMensagem6(String mensagem6) {
		this.mensagem6 = mensagem6;
	}
	public void setOutrospagamento(String outrospagamento) {
		this.outrospagamento = outrospagamento;
	}
	public void setListaSpeddocumento(Set<Speddocumento> listaSpeddocumento) {
		this.listaSpeddocumento = listaSpeddocumento;
	}
	public void setTipopagamento(Tipopagamento tipopagamento) {
		this.tipopagamento = tipopagamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaDocumentoOrigem(
			List<Documentoorigem> listaDocumentoOrigem) {
		this.listaDocumentoOrigem = listaDocumentoOrigem;
	}
	public void setListaNotaDocumento(List<NotaDocumento> listaNotaDocumento) {
		this.listaNotaDocumento = listaNotaDocumento;
	}
	@Transient
	public Integer getCdAgendamento() {
		return cdAgendamento;
	}
	
	public void setAux_documento(Aux_documento aux_documento) {
		this.aux_documento = aux_documento;
	}
	public void setListaDocumentoautori(List<Documentoautori> listaDocumentoautori) {
		this.listaDocumentoautori = listaDocumentoautori;
	}
	public void setDtcartorio(Date dtcartorio) {
		this.dtcartorio = dtcartorio;
	}
	public void setDocumentoacaoprotesto(Documentoacao documentoacaoprotesto) {
		this.documentoacaoprotesto = documentoacaoprotesto;
	}
	
	public void setDocumentoclasse(Documentoclasse documentoclasse) {
		this.documentoclasse = documentoclasse;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setDtdevolucao(Date dtdevolucao) {
		this.dtdevolucao = dtdevolucao;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setTaxa(Taxa taxas) {
		this.taxa = taxas;
	}
	public void setObservacaoHistorico(String observacaoHistorico) {
		this.observacaoHistorico = observacaoHistorico;
	}
	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaParcela(Set<Parcela> listaParcela) {
		this.listaParcela = listaParcela;
	}
	public void setListaDocumentohistorico(Set<Documentohistorico> listaDocumentohistorico) {
		this.listaDocumentohistorico = listaDocumentohistorico;
	}
	public void setListaArqBancarioDoc(Set<Arquivobancariodocumento> listaArqBancarioDoc) {
		this.listaArqBancarioDoc = listaArqBancarioDoc;
	}
	
	public void setListaMovimentacaoOrigem(Set<Movimentacaoorigem> listaMovimentacaoOrigem) {
		this.listaMovimentacaoOrigem = listaMovimentacaoOrigem;
	}
	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}

	@OneToMany(mappedBy="documento")
	public List<Contratofaturalocacaodocumento> getListaContratofaturalocacaodocumento() {
		return listaContratofaturalocacaodocumento;
	}
	public void setListaContratofaturalocacaodocumento(
			List<Contratofaturalocacaodocumento> listaContratofaturalocacaodocumento) {
		this.listaContratofaturalocacaodocumento = listaContratofaturalocacaodocumento;
	}
	@Transient
	public Documento getContaRestanteDe() {
		return contaRestanteDe;
	}
	public void setContaRestanteDe(Documento contaRestanteDe) {
		this.contaRestanteDe = contaRestanteDe;
	}
	@Transient
	public Money getValorPago() {
		return valorPago;
	}
	public void setValorPago(Money valorPago) {
		this.valorPago = valorPago;
	}
	@Transient
	public boolean isVencida() {
		return vencida;
	}
	@Transient
	public boolean isProtestada() {
		return protestada;
	}
	@Transient
	public Documentoacao getAcaoanterior() {
		return acaoanterior;
	}
	@Transient
	public Documentoacao getAcaohistorico() {
		return acaohistorico;
	}
	@Transient
	public boolean isEdicao() {
		return edicao;
	}
	@Transient
	@MaxLength(3)
	@DisplayName("Repeti��es extras")
	public Integer getRepeticoes() {
		return repeticoes;
	}
	@Transient
	@DisplayName("Prazo de pagamento")
	public Prazopagamento getPrazo() {
		return prazo;
	}
	@Transient
	public String getIds() {
		return ids;
	}
	@ValidationOverrides({
			@ValidationOverride(field="centrocusto"),
			@ValidationOverride(field="projeto"),
			@ValidationOverride(field="contagerencial"),
			@ValidationOverride(field="valor")
	})
	@Transient
	public Integer getParcela() {
		return parcela;
	}
	@Transient
	public Boolean getFinanciamento() {
		return financiamento;
	}
	@Transient
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	@Transient
	@DisplayName("Valor autorizado")
	public Money getValorautorizado() {
		return valorautorizado;
	}
	@Transient
	public Long getDiferenca() {
		return diferenca;
	}
	@Transient
	@DisplayName("Valor atual")
	public Money getValoratual() {
		return valoratual;
	}
	@Transient
	public Money getValoratualbaixa() {
		return valoratualbaixa;
	}
	@Transient
	public Money getValoratualbaixaSemTaxa() {
		return valoratualbaixaSemTaxa;
	}
	@Transient
	@DisplayName("Taxa")
	public Money getMoneyTaxas() {
		return moneyTaxas;
	}
	@Transient
	@DisplayName("Forma de pagamento")
	public String getOrigemStr() {
		return origemStr;
	}
	@Transient
	public Entrega getEntrega() {
		return entrega;
	}
	@Transient
	@DisplayName("Parcela")
	public String getNumParcela() {
		return numParcela;
	}
	@Transient
	public String getIdsFornecimento() {
		return idsFornecimento;
	}
	@Transient
	public String getDtVencimentoFormatado() {
		try {
			if(dtvencimento != null){
				return SinedDateUtils.toString(dtvencimento);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	@Transient
	public Boolean getAdiatamentodespesa() {
		return adiatamentodespesa;
	}
	@Transient
	public Nota getNota() {
		return nota;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	public void setAdiatamentodespesa(Boolean adiatamentodespesa) {
		this.adiatamentodespesa = adiatamentodespesa;
	}
	public void setIdsFornecimento(String idsFornecimento) {
		this.idsFornecimento = idsFornecimento;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setNumParcela(String numParcela) {
		this.numParcela = numParcela;
	}
	public void setOrigemStr(String origemStr) {
		this.origemStr = origemStr;
	}
	public void setMoneyTaxas(Money taxa) {
		this.moneyTaxas = taxa;
	}
	public void setValoratual(Money valoratual) {
		this.valoratual = valoratual;
	}
	public void setValoratualbaixa(Money valoratualbaixa) {
		this.valoratualbaixa = valoratualbaixa;
	}
	public void setValoratualbaixaSemTaxa(Money valoratualbaixaSemTaxa) {
		this.valoratualbaixaSemTaxa = valoratualbaixaSemTaxa;
	}
	public void setDiferenca(Long diferenca) {
		this.diferenca = diferenca;
	}
	public void setValorautorizado(Money valorautorizado) {
		this.valorautorizado = valorautorizado;
	}
	public void setAcaoanterior(Documentoacao acaoanterior) {
		this.acaoanterior = acaoanterior;
	}
	public void setAcaohistorico(Documentoacao acaohistorico) {
		this.acaohistorico = acaohistorico;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setFinanciamento(Boolean financiamento) {
		this.financiamento = financiamento;
	}
	public void setRepeticoes(Integer repeticoes) {
		this.repeticoes = repeticoes;
	}
	public void setVencida(boolean vencida) {
		this.vencida = vencida;
	}
	public void setProtestada(boolean protestada) {
		this.protestada = protestada;
	}
	public void setEdicao(boolean edicao) {
		this.edicao = edicao;
	}
	public void setPrazo(Prazopagamento prazo) {
		this.prazo = prazo;
	}
	public void setCdAgendamento(Integer cdAgendamento) {
		this.cdAgendamento = cdAgendamento;
	}
	
	/**
	 * M�todo para verificar se esta conta est� vencida.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public boolean estaVencida(){
		if(this.dtvencimento != null){
			return SinedDateUtils.calculaDiferencaDias(new Date(System.currentTimeMillis()), this.dtvencimento) < 0;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Documento) {
			Documento documento = (Documento) obj;
			return documento.getCddocumento().equals(this.getCddocumento());
		}
		return super.equals(obj);
	}
	
	public boolean equalsToSave(Documento d){
		boolean test = this.numero.equals(d.numero)
			&& this.documentotipo.equals(d.documentotipo)
			&& this.descricao.equals(d.descricao)
			&& this.dtemissao.equals(d.dtemissao)
			&& this.dtvencimento.equals(d.dtvencimento)
			&& this.documentoacao.equals(d.documentoacao)
			&& this.referencia.equals(d.referencia)
			&& this.valor.compareTo(d.valor) == 0;
		
		if(this.fornecedor != null && d.fornecedor != null){
			test = test && this.fornecedor.equals(d.fornecedor);
		}
		if(this.cliente != null && d.cliente != null){
			test = test && this.cliente.equals(d.cliente);
		}
		if(this.colaborador != null && d.colaborador != null){
			test = test && this.colaborador.equals(d.colaborador);
		}
		if(this.outrospagamento != null && d.outrospagamento != null){
			test = test && this.outrospagamento.equals(d.outrospagamento);
		}
		if(this.empresa != null && d.empresa != null){
			test = test && this.empresa.equals(d.empresa);
		}
		
		return test;
	}
	
	/**
	 * M�todo para retornar uma identifica��o do benefici�rio ou agente de um documento.
	 * Retorna o nome da pessoa ou o campo outros.
	 * 
	 * @return
	 */
	@Transient
	public String getDescricaoPagamento(){
		if(pessoa != null && pessoa.getNome() != null){
			return pessoa.getNome();
		}
		return outrospagamento;
	}
	
	@Transient
	public Boolean getFromFornecimento() {
		return fromFornecimento;
	}
	
	public void setFromFornecimento(Boolean fromFornecimento) {
		this.fromFornecimento = fromFornecimento;
	}
	
	@Transient
	public Boolean getSelected() {
		return selected;
	}
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	/**
	 * M�todo para ajustar o Documento quanto ao tipo de pessoa definido pela propriedade Tipopagamento
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documento ajustaBeanToLoad(){
		if(Tipopagamento.CLIENTE.equals(this.tipopagamento)){
			this.cliente = new Cliente(this.pessoa.getCdpessoa());
		}else
		if(Tipopagamento.FORNECEDOR.equals(this.tipopagamento)){
			this.fornecedor = new Fornecedor(this.pessoa.getCdpessoa());
		}else
		if(Tipopagamento.COLABORADOR.equals(this.tipopagamento)){
			this.colaborador = new Colaborador(this.pessoa.getCdpessoa());
		}
		
		return this;
	}
	
	/**
	 * M�todo para ajustar o Documento setando a propriedade pessoa com a pessoa correspondente
	 * de acordo com a propriedade Tipopagamento
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	public Documento ajustaBeanToSave(){
		Pessoa pessoa = null;
		if(Tipopagamento.CLIENTE.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.cliente.getCdpessoa(),null);
		}else
		if(Tipopagamento.FORNECEDOR.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.fornecedor.getCdpessoa(),null);
		}else
		if(Tipopagamento.COLABORADOR.equals(this.tipopagamento)){
			pessoa = new Pessoa(this.colaborador.getCdpessoa(),null);
		}
		
		this.pessoa = pessoa;
		
		return this;
	}
	@Transient
	public String getController() {
		return controller;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	
	@Transient
	public Integer getCdNota() {
		return cdNota;
	}
	public void setCdNota(Integer cdNota) {
		this.cdNota = cdNota;
	}
	
	@Transient
	public String getNumeroNota() {
		return numeroNota;
	}
	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}
	
	@Transient
	public String getIdsVeiculosDespesa() {
		return idsVeiculosDespesa;
	}
	public void setIdsVeiculosDespesa(String idsVeiculosDespesa) {
		this.idsVeiculosDespesa = idsVeiculosDespesa;
	}
	
	@Transient
	public Boolean getFromDespesaVeiculo() {
		return fromDespesaVeiculo;
	}
	public void setFromDespesaVeiculo(Boolean fromDespesaVeiculo) {
		this.fromDespesaVeiculo = fromDespesaVeiculo;
	}
	@Transient
	public Boolean getFromPagamentoComissao() {
		return fromPagamentoComissao;
	}
	public void setFromPagamentoComissao(Boolean fromPagamentoComissao) {
		this.fromPagamentoComissao = fromPagamentoComissao;
	}
	@Transient
	public Boolean getFromPagamentoComissaoVenda() {
		return fromPagamentoComissaoVenda;
	}
	@Transient
	public Boolean getFromPagamentoComissaoDesempenho() {
		return fromPagamentoComissaoDesempenho;
	}
	public void setFromPagamentoComissaoVenda(Boolean fromPagamentoComissaoVenda) {
		this.fromPagamentoComissaoVenda = fromPagamentoComissaoVenda;
	}
	public void setFromPagamentoComissaoDesempenho(Boolean fromPagamentoComissaoDesempenho) {
		this.fromPagamentoComissaoDesempenho = fromPagamentoComissaoDesempenho;
	}
	
	@Transient
	public Boolean getFromPagamentoComissaoPorFaixas() {
		return fromPagamentoComissaoPorFaixas;
	}
	public void setFromPagamentoComissaoPorFaixas(Boolean fromPagamentoComissaoPorFaixas) {
		this.fromPagamentoComissaoPorFaixas = fromPagamentoComissaoPorFaixas;
	}
	
	@Transient
	public String getStrDtbaixa() {
		return strDtbaixa;
	}
	public void setStrDtbaixa(String strDtbaixa) {
		this.strDtbaixa = strDtbaixa;
	}
	@Transient
	public Boolean getFromRetorno() {
		return fromRetorno;
	}
	public void setFromRetorno(Boolean fromRetorno) {
		this.fromRetorno = fromRetorno;
	}
	@Transient
	public Boolean getFromWebservice() {
		return fromWebservice;
	}
	public void setFromWebservice(Boolean fromWebservice) {
		this.fromWebservice = fromWebservice;
	}
	@Transient
	public Boolean getFromBaixaautomatica() {
		return fromBaixaautomatica;
	}
	public void setFromBaixaautomatica(Boolean fromBaixaautomatica) {
		this.fromBaixaautomatica = fromBaixaautomatica;
	}
	@Transient
	@DisplayName("Valor")
	public Money getValorparcela() {
		return valorparcela;
	}
	@Transient
	@DisplayName("Vencimento")
	public Date getDataparcela() {
		return dataparcela;
	}
	@Transient
	@DisplayName("Banco")
	@MaxLength(3)
	public String getBanco() {
		return banco;
	}
	@Transient
	@DisplayName("Cheque")
	@MaxLength(15)
	public String getNumerocheque() {
		return numerocheque;
	}
	@Transient
	@DisplayName("Conta")
	@MaxLength(10)
	public String getContacorrente() {
		return contacorrente;
	}
	@Transient
	@DisplayName("Ag�ncia")
	@MaxLength(6)
	public Integer getAgencia() {
		return agencia;
	}
	@DisplayName("Antecipado")
	public Boolean getAntecipado() {
		return antecipado;
	}
	@DisplayName("Data de Antecipa��o")
	public Date getDtantecipacao() {
		return dtantecipacao;
	}
	@DisplayName("Data de Compet�ncia")
	public Date getDtcompetencia() {
		return dtcompetencia;
	}
	@DisplayName("Apropria��o")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="documento")
	public List<DocumentoApropriacao> getListaApropriacao() {
		return listaApropriacao;
	}
	
	public void setValorparcela(Money valorparcela) {
		this.valorparcela = valorparcela;
	}
	public void setDataparcela(Date dataparcela) {
		this.dataparcela = dataparcela;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public void setNumerocheque(String numerocheque) {
		this.numerocheque = numerocheque;
	}
	public void setContacorrente(String contacorrente) {
		this.contacorrente = contacorrente;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	@Transient
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	
	public String subQueryProjeto() {
		return "select documentoSubQueryProjeto.cddocumento " +
				"from Documento documentoSubQueryProjeto " +
				"join documentoSubQueryProjeto.rateio rateioSubQueryProjeto " +
				"join rateioSubQueryProjeto.listaRateioitem listaRateioitemSubQueryProjeto " +
				"left outer join listaRateioitemSubQueryProjeto.projeto projetoSubQueryProjeto " +
				"where projetoSubQueryProjeto.cdprojeto is null or projetoSubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Override
	public String subQueryClienteEmpresa() {
		return  "select documentoSubQueryClienteEmpresa.cddocumento " +
				"from Documento documentoSubQueryClienteEmpresa " +
				"left outer join documentoSubQueryClienteEmpresa.pessoa clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public Contrato getContrato() {
		return contrato;
	}
	
	@Transient
	public Contratojuridico getContratojuridico() {
		return contratojuridico;
	}
	
	public void setContratojuridico(Contratojuridico contratojuridico) {
		this.contratojuridico = contratojuridico;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	
	}
	
	@Transient
	public Boolean getReciboProjeto() {
		return reciboProjeto;
	}
	public void setReciboProjeto(Boolean reciboProjeto) {
		this.reciboProjeto = reciboProjeto;
	}
	
	@Transient
	public Boolean getExisteProjetoRateio() {
		return existeProjetoRateio;
	}
	public void setExisteProjetoRateio(Boolean existeProjetoRateio) {
		this.existeProjetoRateio = existeProjetoRateio;
	}
	
	@Transient
	public String getNomeProcessoFluxoAnterior() {
		return nomeProcessoFluxoAnterior;
	}
	public void setNomeProcessoFluxoAnterior(String nomeProcessoFluxoAnterior) {
		this.nomeProcessoFluxoAnterior = nomeProcessoFluxoAnterior;
	}
	
	@Transient
	public String getContaReceberDevolucao() {
		return contaReceberDevolucao;
	}
	
	public void setContaReceberDevolucao(String contaReceberDevolucao) {
		this.contaReceberDevolucao = contaReceberDevolucao;
	}
	
	@Transient
	public String getInformacoescedente() {
		return informacoescedente;
	}
	public void setInformacoescedente(String informacoescedente) {
		this.informacoescedente = informacoescedente;
	}
	
	@Transient
	public Agendamentoservicodocumento getAgendamentoservicodocumento() {
		return agendamentoservicodocumento;
	}
	
	public void setAgendamentoservicodocumento(
			Agendamentoservicodocumento agendamentoservicodocumento) {
		this.agendamentoservicodocumento = agendamentoservicodocumento;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Documento contaPagar = new Documento();
		
		
		contaPagar.setReferencia(this.getReferencia());
		contaPagar.setDocumentoclasse(this.getDocumentoclasse());
		contaPagar.setAcaoanterior(this.getAcaoanterior());
		contaPagar.setAcaohistorico(this.getAcaohistorico());
		contaPagar.setAdiatamentodespesa(this.getAdiatamentodespesa());
		contaPagar.setAgencia(this.getAgencia());
		contaPagar.setAux_documento(this.getAux_documento());
		contaPagar.setBanco(this.getBanco());
		contaPagar.setCdAgendamento(this.getCdAgendamento());
		contaPagar.setCddocumento(this.getCddocumento());
		contaPagar.setCdNota(this.getCdNota());
		contaPagar.setCdusuarioaltera(this.getCdusuarioaltera());
		contaPagar.setColaborador(this.getColaborador());
		contaPagar.setConta(this.getConta());
		contaPagar.setContacarteira(this.getContacarteira());
		contaPagar.setContacorrente(this.getContacorrente());
		contaPagar.setDtcartorio(this.getDtcartorio());
		contaPagar.setDtemissao(this.getDtemissao());
		contaPagar.setDtvencimento(this.getDtvencimento());
		contaPagar.setCliente(this.getCliente());
		contaPagar.setTipopagamento(Tipopagamento.CLIENTE);
		contaPagar.setEmpresa(this.getEmpresa());
		contaPagar.setDocumentoacao(this.getDocumentoacao());
		contaPagar.setDocumentotipo(this.getDocumentotipo());
		contaPagar.setValor(this.getValor());
		contaPagar.setDescricao(this.getDescricao());
		contaPagar.setContrato(this.getContrato());
		contaPagar.setListaDocumentohistorico(this.getListaDocumentohistorico());
		contaPagar.setListaDocumentoOrigem(this.getListaDocumentoOrigem());
		contaPagar.setListaMovimentacaoOrigem(this.getListaMovimentacaoOrigem());
		contaPagar.setListaNotaDocumento(this.getListaNotaDocumento());
		contaPagar.setEndereco(this.getEndereco());
		contaPagar.setEntrega(this.getEntrega());
		contaPagar.setFinanciamento(this.getFinanciamento());
		contaPagar.setFormapagamento(this.getFormapagamento());
		contaPagar.setPrazo(this.getPrazo());
		contaPagar.setRepeticoes(this.getRepeticoes());
		contaPagar.setRateio(this.getRateio());
		contaPagar.setTaxa(this.getTaxa());
		contaPagar.setTipopagamento(this.getTipopagamento());
		
		return contaPagar;
	}
	
	@Transient
	public Money getValorboleto() {
		return valorboleto;
	}
	public void setValorboleto(Money valorboleto) {
		this.valorboleto = valorboleto;
	}
	
	@Transient
	public List<Documentocomissao> getListaDocumentocomissao() {
		return listaDocumentocomissao;
	}
	
	public void setListaDocumentocomissao(List<Documentocomissao> listaDocumentocomissao) {
		this.listaDocumentocomissao = listaDocumentocomissao;
	}
	
	@DisplayName("C�pia da Conta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdimagem")
	public Arquivo getImagem() {
		return imagem;
	}
	
	public void setImagem(Arquivo imagem) {
		this.imagem = imagem;
	}
	
	@Transient
	public Double getValorDouble() {
		return getValor() != null ? getValor().getValue().doubleValue() : null;
	}
	public void setValorDouble(Double valorDouble) {
		this.valorDouble = valorDouble;
	}

	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	
	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}
	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	@Transient
	public String getTipo() {
		return tipo;
	}
	@Transient
	public String getSelecteditens() {
		return selecteditens;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setSelecteditens(String selecteditens) {
		this.selecteditens = selecteditens;
	}
	@Transient
	public java.util.Date getDataAtrasada() {
		return dataAtrasada;
	}
	public void setDataAtrasada(java.util.Date dataAtrasada) {
		this.dataAtrasada = dataAtrasada;
	}
	@Transient
	public Boolean getConciliacaoPendente() {
		return conciliacaoPendente;
	}
	
	public void setConciliacaoPendente(Boolean conciliacaoPendente) {
		this.conciliacaoPendente = conciliacaoPendente;
	}
	
	@Transient
	public String getDtvencimentooriginalString() {
		if (dtvencimentooriginal != null){
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			dtvencimentooriginalString = format.format(dtvencimentooriginal);
		}
		return dtvencimentooriginalString;
	}
	
	@Transient
	public String getNumeroValor() {
		if (this.numero != null && !this.numero.equals(""))
			numeroValor = this.numero;
		else if (this.cddocumento != null)
			numeroValor = this.cddocumento.toString();
		if (this.valor != null)
			numeroValor += " (" + this.getValor() + ")";
		return numeroValor;
	}
	
	@Transient
	public String getNumeroCddocumento() {
		if (this.numero != null && !this.numero.equals(""))
			numeroCddocumento = this.numero;
		else if (this.cddocumento != null)
			numeroCddocumento = this.cddocumento.toString();
		return numeroCddocumento;
	}
	
	@Transient
	public String getWhereInDocumentoorigemDesempenho() {
		return whereInDocumentoorigemDesempenho;
	}
	@Transient
	public String getWhereInNotadocumentoDesempenho() {
		return whereInNotadocumentoDesempenho;
	}
	@Transient
	public String getWhereInVendaDesempenho() {
		return whereInVendaDesempenho;
	}
	public void setWhereInDocumentoorigemDesempenho(
			String whereInDocumentoorigemDesempenho) {
		this.whereInDocumentoorigemDesempenho = whereInDocumentoorigemDesempenho;
	}
	public void setWhereInNotadocumentoDesempenho(
			String whereInNotadocumentoDesempenho) {
		this.whereInNotadocumentoDesempenho = whereInNotadocumentoDesempenho;
	}
	public void setWhereInVendaDesempenho(String whereInVendaDesempenho) {
		this.whereInVendaDesempenho = whereInVendaDesempenho;
	}
	
	@Transient
	public String getWhereInDespesaviagem() {
		return whereInDespesaviagem;
	}
	@Transient
	public String getWhereInDespesaviagemReembolso() {
		return whereInDespesaviagemReembolso;
	}
	@Transient
	public String getWhereInOrdemcompra() {
		return whereInOrdemcompra;
	}
	
	public void setWhereInDespesaviagem(String whereInDespesaviagem) {
		this.whereInDespesaviagem = whereInDespesaviagem;
	}
	public void setWhereInDespesaviagemReembolso(String whereInDespesaviagemReembolso) {
		this.whereInDespesaviagemReembolso = whereInDespesaviagemReembolso;
	}
	public void setWhereInOrdemcompra(String whereInOrdemcompra) {
		this.whereInOrdemcompra = whereInOrdemcompra;
	}
	
	@Transient
	public Date getUltimoFechamento() {
		return ultimoFechamento;
	}
	public void setUltimoFechamento(Date ultimoFechamento) {
		this.ultimoFechamento = ultimoFechamento;
	}
	@Transient
	public Boolean getEmitirCarne() {
		return emitirCarne;
	}
	public void setEmitirCarne(Boolean emitirCarne) {
		this.emitirCarne = emitirCarne;
	}
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
	@Transient
	public Money getValorPagoComissionamento(){
		Money valor;
		if(getAux_documento() != null && getAux_documento().getValoratual() != null){
			valor = getAux_documento().getValoratual();
		}else {
			valor = getValor();
		}
		return valor;
	}
	
	@Transient
	public String getWhereInEntrega() {
		return whereInEntrega;
	}
	public void setWhereInEntrega(String whereInEntrega) {
		this.whereInEntrega = whereInEntrega;
	}
	@Transient
	public String getWhereInNota() {
		return whereInNota;
	}
	public void setWhereInNota(String whereInNota) {
		this.whereInNota = whereInNota;
	}
	@Transient
	public Boolean getFromGerarreceitanota() {
		return fromGerarreceitanota;
	}
	public void setFromGerarreceitanota(Boolean fromGerarreceitanota) {
		this.fromGerarreceitanota = fromGerarreceitanota;
	}
	@Transient
	public Boolean getFromFechamento() {
		return fromFechamento;
	}
	public void setFromFechamento(Boolean fromFechamento) {
		this.fromFechamento = fromFechamento;
	}
	
	@Transient
	public String getNumeroPessoaDescricao() {
		return numeroPessoaDescricao;
	}
	public void setNumeroPessoaDescricao(String numeroPessoaDescricao) {
		this.numeroPessoaDescricao = numeroPessoaDescricao;
	}
	@Transient
	public String getDescricaoOrdemcompraEntrega() {
		return descricaoOrdemcompraEntrega;
	}
	public void setDescricaoOrdemcompraEntrega(String descricaoOrdemcompraEntrega) {
		this.descricaoOrdemcompraEntrega = descricaoOrdemcompraEntrega;
	}
	
	@OneToMany(mappedBy="documento")
	public Set<Vdocumentonegociado> getListaDocumentonegociado() {
		return listaDocumentonegociado;
	}
	
	public void setListaDocumentonegociado(
			Set<Vdocumentonegociado> listaDocumentonegociado) {
		this.listaDocumentonegociado = listaDocumentonegociado;
	}
	@Transient
	public String getWhereInEntregadocumento() {
		return whereInEntregadocumento;
	}
	public void setWhereInEntregadocumento(String whereInEntregadocumento) {
		this.whereInEntregadocumento = whereInEntregadocumento;
	}
	@OneToMany(mappedBy="documentonegociado")
	public Set<Vdocumentonegociado> getListaDocumentonegociacao() {
		return listaDocumentonegociacao;
	}
	@OneToMany(mappedBy="documento")
	public Set<Valecompraorigem> getListaValecompraorigem() {
		return listaValecompraorigem;
	}
	@OneToMany(mappedBy="documento")
	public Set<Boletodigital> getListaBoletodigital() {
		return listaBoletodigital;
	}
	public void setListaBoletodigital(Set<Boletodigital> listaBoletodigital) {
		this.listaBoletodigital = listaBoletodigital;
	}
	public void setListaValecompraorigem(
			Set<Valecompraorigem> listaValecompraorigem) {
		this.listaValecompraorigem = listaValecompraorigem;
	}
	public void setListaDocumentonegociacao(
			Set<Vdocumentonegociado> listaDocumentonegociacao) {
		this.listaDocumentonegociacao = listaDocumentonegociacao;
	}

	public Boolean getSomentenossonumero() {
		return somentenossonumero;
	}
	public void setSomentenossonumero(Boolean somentenossonumero) {
		this.somentenossonumero = somentenossonumero;
	}
	
	public Boolean getBoletogerado() {
		return boletogerado;
	}
	
	public void setBoletogerado(Boolean boletogerado) {
		this.boletogerado = boletogerado;
	}
	@Transient
	public Boolean getConfirmadobanco() {
		return confirmadobanco;
	}
	
	public void setConfirmadobanco(Boolean confirmadobanco) {
		this.confirmadobanco = confirmadobanco;
	}
	
	@Transient
	public Date getDtagendamentobanco() {
		return dtagendamentobanco;
	}
	
	public void setDtagendamentobanco(Date dtagendamentobanco) {
		this.dtagendamentobanco = dtagendamentobanco;
	}
	
	public Boolean getRejeitadobanco() {
		return rejeitadobanco;
	}
	
	public void setRejeitadobanco(Boolean rejeitadobanco) {
		this.rejeitadobanco = rejeitadobanco;
	}
	
	@Transient
	public String getCodigobarrascheque() {
		return codigobarrascheque;
	}
	public void setCodigobarrascheque(String codigobarrascheque) {
		this.codigobarrascheque = codigobarrascheque;
	}
	
	@Transient
	public String getHistoricoBaixaRetorno() {
		return historicoBaixaRetorno;
	}
	
	public void setHistoricoBaixaRetorno(String historicoBaixaRetorno) {
		this.historicoBaixaRetorno = historicoBaixaRetorno;
	}
	
	@Required
	@DisplayName("Opera��o Cont�bil")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	@Transient
	public Boolean getFaturarlocacao() {
		return faturarlocacao;
	}
	
	public void setFaturarlocacao(Boolean faturarlocacao) {
		this.faturarlocacao = faturarlocacao;
	}
	
	@Transient
	public Boolean getFinalizarContrato() {
		return finalizarContrato;
	}
	
	public void setFinalizarContrato(Boolean finalizarContrato) {
		this.finalizarContrato = finalizarContrato;
	}
	
	@Transient
	public Money getValorRestante() {
		return valorRestante;
	}
	
	public void setValorRestante(Money valorRestante) {
		this.valorRestante = valorRestante;
	}
	
	@Transient
	public Cheque getChequeBaixarconta() {
		return chequeBaixarconta;
	}
	public void setChequeBaixarconta(Cheque chequeBaixarconta) {
		this.chequeBaixarconta = chequeBaixarconta;
	}
	
	@Transient
	public Boolean getFaturamentolote() {
		return faturamentolote;
	}
	
	public void setFaturamentolote(Boolean faturamentolote) {
		this.faturamentolote = faturamentolote;
	}
	
	@Transient
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}
	
	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	
	@Transient
	public Boolean getGerarreceitafaturalocacao() {
		return gerarreceitafaturalocacao;
	}
	
	@Transient
	public Integer getCdcontratofaturalocacao() {
		return cdcontratofaturalocacao;
	}
	
	public void setGerarreceitafaturalocacao(Boolean gerarreceitafaturalocacao) {
		this.gerarreceitafaturalocacao = gerarreceitafaturalocacao;
	}
	
	public void setCdcontratofaturalocacao(Integer cdcontratofaturalocacao) {
		this.cdcontratofaturalocacao = cdcontratofaturalocacao;
	}

	@Transient
	public Boolean getFromAcaoProtestar() {
		return fromAcaoProtestar;
	}
	public void setFromAcaoProtestar(Boolean fromAcaoProtestar) {
		this.fromAcaoProtestar = fromAcaoProtestar;
	}
	
	
	@Transient
	public Contagerencial getContagerencialDespesaRepresentacao() {
		return contagerencialDespesaRepresentacao;
	}
	@Transient
	public Centrocusto getCentrocustoDespesaRepresentacao() {
		return centrocustoDespesaRepresentacao;
	}
	public void setContagerencialDespesaRepresentacao(Contagerencial contagerencialDespesaRepresentacao) {
		this.contagerencialDespesaRepresentacao = contagerencialDespesaRepresentacao;
	}
	public void setCentrocustoDespesaRepresentacao(Centrocusto centrocustoDespesaRepresentacao) {
		this.centrocustoDespesaRepresentacao = centrocustoDespesaRepresentacao;
	}
	
	@Transient
	public String getNumeroDescricao(){
		String result = "";
		if (this.getNumero() != null)
			result += this.getNumero();
		if (!result.isEmpty())
			result += " - ";
		if (this.getDescricao() != null)
			result += this.getDescricao();
		
		return result;
	}
	
	@Transient
	public Coleta getColeta() {
		return coleta;
	}
	
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(2000)
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Transient
	public Money getSaldoAntecipacao() {
		if (listaHistoricoAntecipacao != null && !listaHistoricoAntecipacao.isEmpty() && valor != null && valor.getValue().doubleValue() > 0d) {
			saldoAntecipacao = valor;
			for(HistoricoAntecipacao item: listaHistoricoAntecipacao){
				saldoAntecipacao = saldoAntecipacao.subtract(item.getValor());
			}
		}
		return saldoAntecipacao;
	}
	public void setSaldoAntecipacao(Money saldoAntecipacao) {
		this.saldoAntecipacao = saldoAntecipacao;
	}
	@Transient
	public Money getValorAntecipacao() {
		valorAntecipacao = new Money();
		if (SinedUtil.isListNotEmpty(listaHistoricoAntecipacao) && valor != null && valor.getValue().doubleValue() > 0d) {
			for(HistoricoAntecipacao item: listaHistoricoAntecipacao){
				valorAntecipacao = valorAntecipacao.add(item.getValor());
			}
		}
		return valorAntecipacao;
	}
	public void setValorAntecipacao(Money valorAntecipacao) {
		this.valorAntecipacao = valorAntecipacao;
	}

	@Transient
	public String getIdAndDescricao() {
		if(cddocumento != null)
			idAndDescricao = cddocumento.toString();
		if(StringUtils.isNotBlank(descricao))
			idAndDescricao += (" - " + descricao);
		return idAndDescricao;
	}
	public void setIdAndDescricao(String idAndDescricao) {
		this.idAndDescricao = idAndDescricao;
	}
	
	public void setListaHistoricoAntecipacao(List<HistoricoAntecipacao> listaHistoricoAntecipacao) {
		this.listaHistoricoAntecipacao = listaHistoricoAntecipacao;
	}
	
	public void setListaHistoricoAntecipacaoDestino(
			List<HistoricoAntecipacao> listaHistoricoAntecipacaoDestino) {
		this.listaHistoricoAntecipacaoDestino = listaHistoricoAntecipacaoDestino;
	}
	
	public Date getDtultimacobranca() {
		return dtultimacobranca;
	}
	public void setDtultimacobranca(Date dtultimacobranca) {
		this.dtultimacobranca = dtultimacobranca;
	}
	
	public Money getValorPrevista() {
		return valorPrevista;
	}
	public void setValorPrevista(Money valorPrevista) {
		this.valorPrevista = valorPrevista;
	}
	
	public String getDescriacaoCorrecao() {
		return descriacaoCorrecao;
	}
	public void setDescriacaoCorrecao(String descriacaoCorrecao) {
		this.descriacaoCorrecao = descriacaoCorrecao;
	}
	
	public Boolean getAssociadomanualmente() {
		return associadomanualmente;
	}
	public void setAssociadomanualmente(Boolean associadomanualmente) {
		this.associadomanualmente = associadomanualmente;
	}
	@Transient
	public Documento getContareceberRepresentacao() {
		return contareceberRepresentacao;
	}
	public void setContareceberRepresentacao(Documento contareceberRepresentacao) {
		this.contareceberRepresentacao = contareceberRepresentacao;
	}
	@Transient
	public Date getDatapagamento() {
		return datapagamento;
	}
	public void setDatapagamento(Date datapagamento) {
		this.datapagamento = datapagamento;
	}
	
	@Transient
	public String getWhereInChequeDevolucao() {
		return whereInChequeDevolucao;
	}
	
	public void setWhereInChequeDevolucao(String whereInChequeDevolucao) {
		this.whereInChequeDevolucao = whereInChequeDevolucao;
	}
	
	@Transient
	public Money getValorTotalMovimentacoes() {
		Money valorTotal = new Money();
		if(Hibernate.isInitialized(getListaMovimentacaoOrigem()) && SinedUtil.isListNotEmpty(getListaMovimentacaoOrigem())){
			for (Movimentacaoorigem item : getListaMovimentacaoOrigem()) {
				if(item.getMovimentacao() != null && 
						item.getMovimentacao().getValor() != null && 
						item.getMovimentacao().getMovimentacaoacao() != null &&
						!item.getMovimentacao().getMovimentacaoacao().equals(Movimentacaoacao.CANCELADA)){
					valorTotal = valorTotal.add(item.getMovimentacao().getValor());
				}
			}
		}
		return valorTotal;
	}
	
	@DisplayName("C�digo da receita")
	@MaxLength(6)
	public String getCodigoreceita() {
		return codigoreceita;
	}
	@DisplayName("M�s e ano de compet�ncia")
	public Date getMesano() {
		return mesano;
	}
	@DisplayName("Valor de outras entidades")
	public Money getValoroutrasentidades() {
		return valoroutrasentidades;
	}
	@DisplayName("Atualiza��o monet�ria")
	public Money getValoratualizacaomonetaria() {
		return valoratualizacaomonetaria;
	}
	@DisplayName("Per�odo de apura��o")
	public Date getDtapuracao() {
		return dtapuracao;
	}
	@DisplayName("N�mero de refer�ncia")
	@MaxLength(17)
	public String getNumeroreferencia() {
		return numeroreferencia;
	}
	@DisplayName("Valor da receita bruta acumulada")
	public Money getValorreceitabrutaacumulada() {
		return valorreceitabrutaacumulada;
	}
	@DisplayName("Percentual da receita bruta acumulada")
	public Money getPercentualreceitabrutaacumulada() {
		return percentualreceitabrutaacumulada;
	}
	@DisplayName("Identifica��o do tributo")
	public Identificacaotributo getIdentificacaotributo() {
		return identificacaotributo;
	}
	@DisplayName("Inscri��o estadual / C�digo do munic�pio / N�mero declara��o")
	@MaxLength(12)
	public String getIecodmunnumdec() {
		return iecodmunnumdec;
	}
	@DisplayName("D�vida ativa / N�mero etiqueta")
	@MaxLength(13)
	public String getDividaativanumetiqueta() {
		return dividaativanumetiqueta;
	}
	@DisplayName("N�mero da parcela / Notifica��o")
	public String getNumparcnotificacao() {
		return numparcnotificacao;
	}
	@DisplayName("Ano base / exerc�cio")
	@MaxLength(4)
	public Integer getAnobase() {
		return anobase;
	}
	@DisplayName("RENAVAM")
	@MaxLength(11)
	public String getRenavam() {
		return renavam;
	}
	@DisplayName("UF")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduf")
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Munic�pio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("Placa do ve�culo")
	@MaxLength(10)
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@DisplayName("Op��o de pagamento")
	public Opcaopagamento getOpcaopagamento() {
		return opcaopagamento;
	}
	
	public void setCodigoreceita(String codigoreceita) {
		this.codigoreceita = codigoreceita;
	}
	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}
	public void setValoroutrasentidades(Money valoroutrasentidades) {
		this.valoroutrasentidades = valoroutrasentidades;
	}
	public void setValoratualizacaomonetaria(Money valoratualizacaomonetaria) {
		this.valoratualizacaomonetaria = valoratualizacaomonetaria;
	}
	public void setDtapuracao(Date dtapuracao) {
		this.dtapuracao = dtapuracao;
	}
	public void setNumeroreferencia(String numeroreferencia) {
		this.numeroreferencia = numeroreferencia;
	}
	public void setValorreceitabrutaacumulada(Money valorreceitabrutaacumulada) {
		this.valorreceitabrutaacumulada = valorreceitabrutaacumulada;
	}
	public void setPercentualreceitabrutaacumulada(
			Money percentualreceitabrutaacumulada) {
		this.percentualreceitabrutaacumulada = percentualreceitabrutaacumulada;
	}
	public void setIdentificacaotributo(Identificacaotributo identificacaotributo) {
		this.identificacaotributo = identificacaotributo;
	}
	public void setIecodmunnumdec(String iecodmunnumdec) {
		this.iecodmunnumdec = iecodmunnumdec;
	}
	public void setDividaativanumetiqueta(String dividaativanumetiqueta) {
		this.dividaativanumetiqueta = dividaativanumetiqueta;
	}
	public void setNumparcnotificacao(String numparcnotificacao) {
		this.numparcnotificacao = numparcnotificacao;
	}
	public void setAnobase(Integer anobase) {
		this.anobase = anobase;
	}
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setOpcaopagamento(Opcaopagamento opcaopagamento) {
		this.opcaopagamento = opcaopagamento;
	}
	
	@DisplayName("M�s/ano de compet�ncia")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	
	public void setMesanoAux(String mesanoAux) {
		if(mesanoAux == null){
			this.mesano = null;
		}else {
			try {
				this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}
	}
	
	@Transient
	public Integer getCdveiculo() {
		return cdveiculo;
	}
	public void setCdveiculo(Integer cdveiculo) {
		this.cdveiculo = cdveiculo;
	}

	@Transient
	public List<Venda> getListaVenda() {
		return listaVenda;
	}
	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}
	@Transient
	public List<Pedidovenda> getListaPedidovenda() {
		return listaPedidovenda;
	}
	public void setListaPedidovenda(List<Pedidovenda> listaPedidovenda) {
		this.listaPedidovenda = listaPedidovenda;
	}
	
	@Transient
	public String getRazaoOrNomeByTipopessoa(){
		if(StringUtils.isNotBlank(outrospagamento)){
			return outrospagamento;
		}else if(cliente != null && cliente.getTipopessoa() != null){
			if(cliente.tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)) 
				return cliente.razaosocial;
			else 
				return cliente.nome;
		}else if(pessoa != null){
			return pessoa.getNome();
		}
		
		return null;
	}
	
	@Transient
	public String getPagamentoa(){
		if(StringUtils.isNotBlank(outrospagamento)){
			return outrospagamento;
		}else if(pessoa != null){
			return pessoa.getNome();
		}
		return null;
	}
	
	@Transient
	public String getRecebimentode(){
		if(StringUtils.isNotBlank(outrospagamento)){
			return outrospagamento;
		}else if(pessoa != null){
			return pessoa.getNome();
		}
		return null;
	}
	
	@Transient
	public String getRazaosocialAndNomefantasia(){
		if(StringUtils.isNotBlank(outrospagamento)){
			return outrospagamento;
		}else if(pessoa != null){
			String razaoSocialAndNome = this.getRazaosocialPessoa();
			razaoSocialAndNome = (Util.strings.isNotEmpty(razaoSocialAndNome)? razaoSocialAndNome + " - " : "") + Util.strings.emptyIfNull(pessoa.getNome());
			return razaoSocialAndNome;
		}
		return null;
	}
	
	@Transient
	public Boolean getIsDtVencimentoAlterada() {
		if((this.dtvencimento != null && this.dtvencimentoantiga != null 
				&& SinedDateUtils.equalsIgnoreHour(this.dtvencimento, this.dtvencimentoantiga)) || this.dtvencimentoantiga == null){
			return Boolean.FALSE;
		}else{
			return Boolean.TRUE;
		}
	}
	@Transient
	public String getDtvencimentoAntigaStr() {
		if (this.dtvencimentoantiga != null){
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			this.dtvencimentoAntigaStr = format.format(this.dtvencimentoantiga);
		}
		return dtvencimentoAntigaStr;
	}
	public Date getDtvencimentoantiga() {
		return dtvencimentoantiga;
	}
	@Transient
	public Date getDtvencimentoAux() {
		return dtvencimentoAux;
	}
	public void setDtvencimentoantiga(Date dtvencimentoantiga) {
		this.dtvencimentoantiga = dtvencimentoantiga;
	}
	public void setDtvencimentoAux(Date dtvencimentoAux) {
		this.dtvencimentoAux = dtvencimentoAux;
	}
	@Transient
	public Boolean getCopiadocumeto() {
		return copiadocumeto;
	}
	public void setCopiadocumeto(Boolean copiadocumeto) {
		this.copiadocumeto = copiadocumeto;
	}

	@Transient
	@DisplayName("Fatura de Loca��o")
	public String getNumerofaturalocacao() {
		return numerofaturalocacao;
	}
	public void setNumerofaturalocacao(String numerofaturalocacao) {
		this.numerofaturalocacao = numerofaturalocacao;
	}
	
	@Transient
	@DisplayName("Data da baixa")
	public String getDatasBaixa() {
		return datasBaixa;
	}
	
	public void setDatasBaixa(String datasBaixa) {
		this.datasBaixa = datasBaixa;
	}
	
	@Transient
	public Boolean getExisteDuplicata() {
		return existeDuplicata;
	}
	public void setExisteDuplicata(Boolean existeDuplicata) {
		this.existeDuplicata = existeDuplicata;
	}
	
	@Transient
	public Documentotipo getDocumentotipoTransient() {
		return documentotipoTransient;
	}
	public void setDocumentotipoTransient(Documentotipo documentotipoTransient) {
		this.documentotipoTransient = documentotipoTransient;
	}
	
	public Boolean getCancelamentocobranca() {
		return cancelamentocobranca;
	}
	public Boolean getEmissaoboletobloqueado() {
		return emissaoboletobloqueado;
	}
	public void setCancelamentocobranca(Boolean cancelamentocobranca) {
		this.cancelamentocobranca = cancelamentocobranca;
	}
	public void setEmissaoboletobloqueado(Boolean emissaoboletobloqueado) {
		this.emissaoboletobloqueado = emissaoboletobloqueado;
	}
	
	@Transient
	public Date getDtvencimentooinicial() {
		return dtvencimentooinicial;
	}
	
	public void setDtvencimentooinicial(Date dtvencimentooinicial) {
		this.dtvencimentooinicial = dtvencimentooinicial;
	}
	
	@Transient
	public Money getValortaxadespesa() {
		return valortaxadespesa;
	}
	
	public void setValortaxadespesa(Money valortaxadespesa) {
		this.valortaxadespesa = valortaxadespesa;
	}
	
	@Transient
	public SituacaoremessaEnum getSituacaoremessa() {
		return situacaoremessa;
	}
	public void setSituacaoremessa(SituacaoremessaEnum situacaoremessa) {
		this.situacaoremessa = situacaoremessa;
	}
	
	@Transient
	public String getTextoAlterouVencimento(){
		if (getDtvencimentooinicial()!=null
				&& getDtvencimento()!=null 
				&& SinedDateUtils.diferencaDias(getDtvencimentooinicial(), getDtvencimento())!=0){
			return "Altera��o da data de vencimento de " + NeoFormater.getInstance().format(getDtvencimentooinicial()) + " para " + NeoFormater.getInstance().format(getDtvencimento());
		}else {
			return null;
		}
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacaoanteriornegociacao", insertable=false, updatable=false)
	public Documentoacao getDocumentoacaoanteriornegociacao() { 
		return documentoacaoanteriornegociacao;
	}
	
	public void setDocumentoacaoanteriornegociacao(Documentoacao documentoacaoanteriornegociacao) {
		this.documentoacaoanteriornegociacao = documentoacaoanteriornegociacao;
	}
	
	/**
	 * AT 139578
	 * Altero o dtvencimento no BEAN para n�o precisar mexer nos selects nas queries do Fluxo de caixa.
	 * 
	 */
	public void alterarDtvencimentoFluxoCaixa() {
		alterarDtvencimentoFluxoCaixa(null, null, false);
	}
	
	public void alterarDtvencimentoFluxoCaixa(Empresa empresa, HashMap<String, List<Calendario>> mapCalendario, boolean isFiltroOrigem) {
		if(isFiltroOrigem && this.cheque != null && this.cheque.getDtbompara() != null){
			if (documentotipo!=null && documentotipo.getDiasreceber()!=null){
				this.cheque.setDtbompara(SinedDateUtils.addDiasData(this.cheque.getDtbompara(), documentotipo.getDiasreceber()));
			}
			this.cheque.setDtbompara(SinedDateUtils.getProximaDataUtil(this.cheque.getDtbompara(), empresa, mapCalendario));
		}
		if (documentotipo!=null && documentotipo.getDiasreceber()!=null){
			this.dtvencimento = SinedDateUtils.getProximaDataUtil(this.dtvencimento, empresa, mapCalendario);
			this.dtvencimento = SinedDateUtils.addDiasData(this.dtvencimento, documentotipo.getDiasreceber());
		}
		if (dtvencimento!=null){
			this.dtvencimento = SinedDateUtils.getProximaDataUtil(this.dtvencimento, empresa, mapCalendario);
		}
	}
	
	@Transient
	public Nota getNotaTransient() {
		return notaTransient;
	}
	public void setNotaTransient(Nota notaTransient) {
		this.notaTransient = notaTransient;
	}
	
	@Transient
	public List<Nota> getListaNotaTransient() {
		return listaNotaTransient;
	}
	public void setListaNotaTransient(List<Nota> listaNotaTransient) {
		this.listaNotaTransient = listaNotaTransient;
	}
	
	@Transient
	public Money getValorTotalTaxaMovimentacao() {
		return valorTotalTaxaMovimentacao;
	}
	public void setValorTotalTaxaMovimentacao(Money valorTotalTaxaMovimentacao) {
		this.valorTotalTaxaMovimentacao = valorTotalTaxaMovimentacao;
	}
	
	@Transient
	public Money getValorTotalMovimentacoesComTaxa() {
		Money valorTotal = getValorTotalMovimentacoes();
		if(getValorTotalTaxaMovimentacao() != null){
			valorTotal = valorTotal.subtract(getValorTotalTaxaMovimentacao());
		}
		return valorTotal;
	}
	
	@Transient
	public Rateiomodelo getRateiomodelo() {
		return rateiomodelo;
	}
	public void setRateiomodelo(Rateiomodelo rateiomodelo) {
		this.rateiomodelo = rateiomodelo;
	}
	
	@Transient
	public String getIdentificadorpessoa() {
		identificadorpessoa = "";
		if(this.pessoa != null){
			if(Tipopagamento.CLIENTE.equals(this.tipopagamento) && this.pessoa.getCliente() != null){
				identificadorpessoa = Util.strings.emptyIfNull(this.pessoa.getCliente().getIdentificador());
			}else
			if(Tipopagamento.FORNECEDOR.equals(this.tipopagamento) && this.pessoa.getFornecedorBean() != null){
				identificadorpessoa = Util.strings.emptyIfNull(this.pessoa.getFornecedorBean().getIdentificador());
			}
		}

		return identificadorpessoa;
	}
	public void setIdentificadorpessoa(String identificadorpessoa) {
		this.identificadorpessoa = identificadorpessoa;
	}
	
	@Transient
	public Boolean getPodeAlterarValor() {
		return podeAlterarValor;
	}
	
	public void setPodeAlterarValor(Boolean podeAlterarValor) {
		this.podeAlterarValor = podeAlterarValor;
	}
	
	@Transient
	public Documentohistorico getDocumentohistoricoBaixa() {
		return documentohistoricoBaixa;
	}
	public void setDocumentohistoricoBaixa(Documentohistorico documentohistoricoBaixa) {
		this.documentohistoricoBaixa = documentohistoricoBaixa;
	}
	
	@Transient
	public Pedidovenda getOrigemPedidovenda() {
		return origemPedidovenda;
	}
	@Transient
	public Venda getOrigemVenda() {
		return origemVenda;
	}
	@Transient
	public Integer getCddocumentoOrigemVenda() {
		return cddocumentoOrigemVenda;
	}
	
	public void setOrigemPedidovenda(Pedidovenda origemPedidovenda) {
		this.origemPedidovenda = origemPedidovenda;
	}
	public void setOrigemVenda(Venda origemVenda) {
		this.origemVenda = origemVenda;
	}
	public void setCddocumentoOrigemVenda(Integer cddocumentoOrigemVenda) {
		this.cddocumentoOrigemVenda = cddocumentoOrigemVenda;
	}
	@Transient
	public Money getValorTituloValeCompra() {
		return valorTituloValeCompra;
	}
	@Transient
	public Money getValorPagoValeCompra() {
		return valorPagoValeCompra;
	}
	@Transient
	public Money getValorJurosMultaValeCompra() {
		return valorJurosMultaValeCompra;
	}
	@Transient
	public Money getValorDescontoOutroDescontoValeCompra() {
		return valorDescontoOutroDescontoValeCompra;
	}
	@Transient
	public Integer getDiferencaDias() {
		return diferencaDias;
	}
	public void setValorTituloValeCompra(Money valorTituloValeCompra) {
		this.valorTituloValeCompra = valorTituloValeCompra;
	}
	public void setValorPagoValeCompra(Money valorPagoValeCompra) {
		this.valorPagoValeCompra = valorPagoValeCompra;
	}
	public void setValorJurosMultaValeCompra(Money valorJurosMultaValeCompra) {
		this.valorJurosMultaValeCompra = valorJurosMultaValeCompra;
	}
	public void setDiferencaDias(Integer diferencaDias) {
		this.diferencaDias = diferencaDias;
	}
	public void setValorDescontoOutroDescontoValeCompra(Money valorDescontoOutroDescontoValeCompra) {
		this.valorDescontoOutroDescontoValeCompra = valorDescontoOutroDescontoValeCompra;
	}
	@Transient
	public String getRazaosocialPessoa() {
		razaosocialPessoa = "";
		if(this.pessoa != null){
			if(Tipopagamento.CLIENTE.equals(tipopagamento)){
				razaosocialPessoa = this.getCliente() != null? this.getCliente().getRazaosocial(): "";
			}else if(Tipopagamento.FORNECEDOR.equals(tipopagamento)){
				razaosocialPessoa = this.getFornecedor() != null? this.getFornecedor().getRazaosocial(): "";
			}
		}
		
		return razaosocialPessoa;
	}
	public void setRazaosocialPessoa(String razaosocialPessoa) {
		this.razaosocialPessoa = razaosocialPessoa;
	}
	public void setAntecipado(Boolean antecipado) {
		this.antecipado = antecipado;
	}
	public void setDtantecipacao(Date dtantecipacao) {
		this.dtantecipacao = dtantecipacao;
	}
	public void setDtcompetencia(Date dtcompetencia) {
		this.dtcompetencia = dtcompetencia;
	}
	public void setListaApropriacao(List<DocumentoApropriacao> listaApropriacao) {
		this.listaApropriacao = listaApropriacao;
	}
	
	@Transient
	public Date getDtvencimentoFluxoCaixa(boolean filtroOrigem){
		if(filtroOrigem && this.getCheque() != null && this.getCheque().getDtbompara() != null){
			return this.getCheque().getDtbompara();
		}
		return getDtvencimento();
	}
	@Transient
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Transient
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@Transient
	public Money getValortotalpago() {
		return valortotalpago;
	}
	public void setValortotalpago(Money valortotalpago) {
		this.valortotalpago = valortotalpago;
	}
		
	@DisplayName("N�mero do FGTS")
	@MaxLength(16)
	public String getNfgts() {
		return nfgts;
	}
	public void setNfgts(String nfgts) {
		this.nfgts = nfgts;
	}
	
	@DisplayName("Lacre de conectividade social")
	@MaxLength(9)
	public String getLacreconectividade() {
		return lacreconectividade;
	}
	public void setLacreconectividade(String lacreconectividade) {
		this.lacreconectividade = lacreconectividade;
	}
	
	@DisplayName("D�gito do lacre")
	@MaxLength(2)
	public String getDigitolacre() {
		return digitolacre;
	}
	public void setDigitolacre(String digitolacre) {
		this.digitolacre = digitolacre;
	}
	
	@Transient
	public String getWhereInDocumentoAdiantamento() {
		return whereInDocumentoAdiantamento;
	}
	public void setWhereInDocumentoAdiantamento(String whereInDocumentoAdiantamento) {
		this.whereInDocumentoAdiantamento = whereInDocumentoAdiantamento;
	}
	
	@Transient
	public String getDocumentoAdiantamentoValores() {
		return documentoAdiantamentoValores;
	}
	public void setDocumentoAdiantamentoValores(String documentoAdiantamentoValores) {
		this.documentoAdiantamentoValores = documentoAdiantamentoValores;
	}
	
	@Transient
	public Boolean getPossuiSaldoAdiantamento() {
		return possuiSaldoAdiantamento;
	}
	public void setPossuiSaldoAdiantamento(Boolean possuiSaldoAdiantamento) {
		this.possuiSaldoAdiantamento = possuiSaldoAdiantamento;
	}
	
	@Transient
	public Boolean getPossuiSaldoNaoCompensado() {
		return possuiSaldoNaoCompensado;
	}
	public void setPossuiSaldoNaoCompensado(Boolean possuiSaldoNaoCompensado) {
		this.possuiSaldoNaoCompensado = possuiSaldoNaoCompensado;
	}
	
	@Transient
	public Boolean getIsCompensarSaldo() {
		return isCompensarSaldo;
	}
	
	public void setIsCompensarSaldo(Boolean isCompensarSaldo) {
		this.isCompensarSaldo = isCompensarSaldo;
	}
	
	@Transient
	public boolean getIsDescricaoHistorico() {
		return isDescricaoHistorico;
	}
	
	public void setIsDescricaoHistorico(boolean isDescricaoHistorico) {
		this.isDescricaoHistorico = isDescricaoHistorico;
	}
	
	@Transient
	public Date getDataForGeracaoContabil(){
		return dtcompetencia != null? dtcompetencia: dtemissao;
	}
	
	@Transient
	public ContaContabil getContaContabilForPessoa(){
		if(this.getPessoa()!=null){
			if(this.getPessoa() != null && this.getPessoa().getCliente() != null && this.getPessoa().getCliente().getContaContabil() != null){
				return this.getPessoa().getCliente().getContaContabil();
			}else if(this.getPessoa() != null && this.getPessoa().getFornecedorBean() != null && this.getPessoa().getFornecedorBean().getContaContabil() != null){
				return this.getPessoa().getFornecedorBean().getContaContabil();
			}else if(this.getPessoa().getColaborador() != null && this.getPessoa().getColaborador().getContaContabil() != null){
				return this.getPessoa().getColaborador().getContaContabil();
			}
		}
		return null;
	}
	
	@Transient
	public Boolean getIsMovimentacao() {
		return isMovimentacao;
	}
	
	public void setIsMovimentacao(Boolean isMovimentacao) {
		this.isMovimentacao = isMovimentacao;
	}
	
	@Transient
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	
	@Transient
	public String getWhereInGnre() {
		return whereInGnre;
	}
	
	public void setWhereInGnre(String whereInGnre) {
		this.whereInGnre = whereInGnre;
	}
}