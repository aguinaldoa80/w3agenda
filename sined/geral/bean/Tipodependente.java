package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_tipodependente", sequenceName = "sq_tipodependente")
public class Tipodependente {

	protected Integer cdtipodependente;
	protected String nome;
	
	public static final Integer CONJUGE = 1;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipodependente")
	public Integer getCdtipodependente() {
		return cdtipodependente;
	}
	public void setCdtipodependente(Integer id) {
		this.cdtipodependente = id;
	}

	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	@Transient
	@DescriptionProperty(usingFields={"cdtipodependente","nome"})
	public String getDescricao(){
		String cd = "";
		if(cdtipodependente < 100) cd += "0";
		if(cdtipodependente < 10) cd += "0";
		cd += cdtipodependente;
		return cd+" - "+nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
