package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
public class Cbo {

	protected Integer cdcbo;
	protected String nome;
	
	//TRANSIENTS
	protected String cdcboformatado;
	
	public Cbo() {
	}
	
	public Cbo(Integer cdcbo) {
		this.cdcbo = cdcbo;
	}
	
	@Id
	public Integer getCdcbo() {
		return cdcbo;
	}
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@Transient
	@DescriptionProperty
	@DisplayName("CBO")
	public String getCodNome(){
		return nome+" - "+cdcbo;
	}
	
	
	public void setCdcbo(Integer cdcbo) {
		this.cdcbo = cdcbo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Transient
	public String getCdcboformatado() {
		if(this.cdcbo != null && this.cdcbo.toString().length() < 6){
			String cbo = "";
			while(cbo.length()+this.cdcbo.toString().length() < 6)
				cbo += "0";
			return cbo += this.cdcbo;
		}else{
			return this.cdcbo.toString();
		}
	}
}
