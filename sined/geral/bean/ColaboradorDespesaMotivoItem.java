package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_colaboradordespesamotivoitem", sequenceName="sq_colaboradordespesamotivoitem")
public class ColaboradorDespesaMotivoItem {

	private Integer cdColaboradorDespesaMotivoItem;
	private Colaboradordespesamotivo colaboradorDespesaMotivo;
	private Empresa empresa;
	private Contagerencial contaGerencial;
	private String codigoIntegracao;
	
	
	@Id
	@GeneratedValue(generator="sq_colaboradordespesamotivoitem", strategy=GenerationType.AUTO)
	public Integer getCdColaboradorDespesaMotivoItem() {
		return cdColaboradorDespesaMotivoItem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesamotivo")
	public Colaboradordespesamotivo getColaboradorDespesaMotivo() {
		return colaboradorDespesaMotivo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContaGerencial() {
		return contaGerencial;
	}
	@DisplayName("C�digo de integra��o")
	public String getCodigoIntegracao() {
		return codigoIntegracao;
	}
	
	
	public void setCdColaboradorDespesaMotivoItem(
			Integer cdColaboradorDespesaMotivoItem) {
		this.cdColaboradorDespesaMotivoItem = cdColaboradorDespesaMotivoItem;
	}
	public void setColaboradorDespesaMotivo(
			Colaboradordespesamotivo colaboradorDespesaMotivo) {
		this.colaboradorDespesaMotivo = colaboradorDespesaMotivo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContaGerencial(Contagerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	public void setCodigoIntegracao(String codigoIntegracao) {
		this.codigoIntegracao = codigoIntegracao;
	}
}
