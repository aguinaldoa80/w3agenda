package br.com.linkcom.sined.geral.bean;

public class Notificacao {

	private String descricao;
	private String data;
	private Integer quantidade;
	private Integer cdmotivoaviso;
	private Integer cdusuario;
	
	public Notificacao() {
		
	}
	
	public Notificacao(String descricao, String data, Integer quantidade, Integer cdmotivoaviso, Integer cdusuario) {
		this.descricao = descricao;
		this.data = data;
		this.quantidade = quantidade;
		this.cdmotivoaviso = cdmotivoaviso;
		this.cdusuario = cdusuario;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public String getData() {
		return data;
	}
	
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Integer getQuantidade() {
		return quantidade;
	}
	
	public void setCdmotivoaviso(Integer cdmotivoaviso) {
		this.cdmotivoaviso = cdmotivoaviso;
	}
	
	public Integer getCdmotivoaviso() {
		return cdmotivoaviso;
	}
	
	public Integer getCdusuario() {
		return cdusuario;
	}
	
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
}
