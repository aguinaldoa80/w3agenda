package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servicobanco", sequenceName = "sq_servicobanco")
@DisplayName("Banco de dados")
public class Servicobanco implements Log{
	
	protected Integer cdservicobanco;
	protected Servicoservidor servicoservidor;
	protected String usuario;
	protected String senhainicial;
	protected String nome;
	protected String cota;
	protected Contratomaterial contratomaterial;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Transient
	protected Cliente cliente;
	protected Servidor servidor;
	
	public Servicobanco() {
	}
		
	public Servicobanco(Integer cdservicobanco) {
		this.cdservicobanco = cdservicobanco;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicobanco")
	public Integer getCdservicobanco() {
		return cdservicobanco;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidor")
	@DisplayName("Servi�o de servidor")
	public Servicoservidor getServicoservidor() {
		return servicoservidor;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Senha inicial")
	public String getSenhainicial() {
		return senhainicial;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Nome do banco")
	public String getNome() {
		return nome;
	}

	@Required
	@MaxLength(50)	
	public String getCota() {
		return cota;
	}
		
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")	
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdservicobanco(Integer cdservicobanco) {
		this.cdservicobanco = cdservicobanco;
	}

	public void setServicoservidor(Servicoservidor servicoservidor) {
		this.servicoservidor = servicoservidor;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setSenhainicial(String senhainicial) {
		this.senhainicial = senhainicial;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCota(String cota) {
		this.cota = cota;
	}
	
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@Required
	public Servidor getServidor() {
		return servidor;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}	
}
