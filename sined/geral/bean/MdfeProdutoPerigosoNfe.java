package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeprodutoperigosonfe", sequenceName = "sq_mdfeprodutoperigosonfe")
public class MdfeProdutoPerigosoNfe {

	protected Integer cdMdfeProdutoPerigosoNfe;
	protected Mdfe mdfe;
	protected String idNfe;
	protected String idProdutoPerigoso;
	protected String numeroOnuUn;
	protected String classe;
	protected String nomeApropriadoEmbarque;
	protected String grupoEmbalagem;
	protected String quantidadeTotalPorProdutos;
	protected String quantidadeAndTipoPorVolume;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeprodutoperigosonfe")
	public Integer getCdMdfeProdutoPerigosoNfe() {
		return cdMdfeProdutoPerigosoNfe;
	}
	public void setCdMdfeProdutoPerigosoNfe(Integer cdMdfeProdutoPerigoso) {
		this.cdMdfeProdutoPerigosoNfe = cdMdfeProdutoPerigoso;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID da NFe")
	public String getIdNfe() {
		return idNfe;
	}
	public void setIdNfe(String idNfe) {
		this.idNfe = idNfe;
	}
	
	@DisplayName("ID do produto perigoso")
	public String getIdProdutoPerigoso() {
		return idProdutoPerigoso;
	}
	public void setIdProdutoPerigoso(String idProdutoPerigoso) {
		this.idProdutoPerigoso = idProdutoPerigoso;
	}
	
	@Required
	@MaxLength(value=4)
	@DisplayName("N�mero ONU/UN")
	public String getNumeroOnuUn() {
		return numeroOnuUn;
	}
	public void setNumeroOnuUn(String numeroOnuUn) {
		this.numeroOnuUn = numeroOnuUn;
	}
	
	@MaxLength(value=40)
	@DisplayName("Classe ou subclasse/divis�o, e risco subsidi�rio/risco secund�rio")
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	@MaxLength(value=150)
	@DisplayName("Nome apropriado para embarque do produto")
	public String getNomeApropriadoEmbarque() {
		return nomeApropriadoEmbarque;
	}
	public void setNomeApropriadoEmbarque(String nomeApropriadoEmbarque) {
		this.nomeApropriadoEmbarque = nomeApropriadoEmbarque;
	}
	
	@MaxLength(value=6)
	@DisplayName("Grupo de embalagem")
	public String getGrupoEmbalagem() {
		return grupoEmbalagem;
	}
	public void setGrupoEmbalagem(String grupoEmbalagem) {
		this.grupoEmbalagem = grupoEmbalagem;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Quantidade total por produtos")
	public String getQuantidadeTotalPorProdutos() {
		return quantidadeTotalPorProdutos;
	}
	public void setQuantidadeTotalPorProdutos(String quantidadeTotalPorProdutos) {
		this.quantidadeTotalPorProdutos = quantidadeTotalPorProdutos;
	}
	
	@MaxLength(value=60)
	@DisplayName("Quantidade e tipo por volumes")
	public String getQuantidadeAndTipoPorVolume() {
		return quantidadeAndTipoPorVolume;
	}
	public void setQuantidadeAndTipoPorVolume(String quantidadeAndTipoPorVolume) {
		this.quantidadeAndTipoPorVolume = quantidadeAndTipoPorVolume;
	}
	
	@Transient
	public List<MdfeLacreUnidadeCargaNfe> getListaLacreUnidadeCargaNfe(MdfeNfe mdfeNfe, MdfeInformacaoUnidadeCargaNfe infUnTransNfe, Mdfe mdfe) {
		List<MdfeLacreUnidadeCargaNfe> list = new ArrayList<MdfeLacreUnidadeCargaNfe>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaNfe())){
			for(MdfeLacreUnidadeCargaNfe item : mdfe.getListaLacreUnidadeCargaNfe()){
				if(item.getIdUnidadeCarga().equals(infUnTransNfe.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
