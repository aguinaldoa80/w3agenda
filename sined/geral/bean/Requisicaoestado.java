package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_requisicaoestado", sequenceName = "sq_requisicaoestado")
public class Requisicaoestado {

	protected Integer cdrequisicaoestado;
	protected String descricao;
	
	//constantes
	public static final Integer EMESPERA = 1;
	public static final Integer EMANDAMENTO = 2;
	public static final Integer EMTESTE = 3;
	public static final Integer CONCLUIDA = 4;
	public static final Integer CANCELADA = 5;
	public static final Integer VISTO = 6;
	public static final Integer EMAGUARDO = 7;
	public static final Integer EM_ANDAMENTO_ESPERA = 8;
	public static final Integer FATURADO = 9;
	public static final Integer AGUARDANDO_RESPOSTA = 10;
	public static final Integer FATURADO_PARCIALMENTE = 11;
	

	public Requisicaoestado(){}
	
	public Requisicaoestado(Integer cdrequisicaoestado){
		this.cdrequisicaoestado = cdrequisicaoestado;
	}

	public Requisicaoestado(Integer cdrequisicaoestado, String descricao){
		this.cdrequisicaoestado = cdrequisicaoestado;
		this.descricao = descricao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_requisicaoestado")
	public Integer getCdrequisicaoestado() {
		return cdrequisicaoestado;
	}
	public void setCdrequisicaoestado(Integer id) {
		this.cdrequisicaoestado = id;
	}

	
	@MaxLength(20)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Requisicaoestado)
			return ((Requisicaoestado)obj).cdrequisicaoestado.equals(this.cdrequisicaoestado);
		return super.equals(obj);
	}

}
