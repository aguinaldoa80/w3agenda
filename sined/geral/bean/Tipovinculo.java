package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
public class Tipovinculo {

	public static final Integer CONTA_A_PAGAR = 1;
	public static final Integer DEBITO_LANCAMENTO_EM_CONTA_BANCARIA = 2;
	public static final Integer DEBITO_LANCAMENTO_EM_CAIXA = 3;
	public static final Integer DEBITO_LANCAMENTO_EM_CARTAO_DE_CREDITO = 4;
	public static final Integer CONTA_A_RECEBER = 5;
	public static final Integer CREDITO_LANCAMENTO_EM_CONTA_BANCARIA = 6;
	public static final Integer CREDITO_LANCAMENTO_EM_CAIXA = 7;
	
	protected Integer cdtipovinculo;
	protected String nome;
	protected Tipooperacao tipooperacao;
	protected Contatipo contatipo;

	public Tipovinculo() {}
	
	public Tipovinculo(String nome) {
		this.nome = nome;
	}
	
	public Tipovinculo(Integer cdTipovinculo, String nome) {
		this.cdtipovinculo = cdTipovinculo;
		this.nome = nome;
	}

	public Tipovinculo(Integer cdtipovinculo) {
		this.cdtipovinculo = cdtipovinculo;
	}

	@Id
	@DisplayName("Id")
	public Integer getCdtipovinculo() {
		return cdtipovinculo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontatipo")
	public Contatipo getContatipo() {
		return contatipo;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	
	public void setCdtipovinculo(Integer cdtipovinculo) {
		this.cdtipovinculo = cdtipovinculo;
	}
	
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
}
