package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_oportunidadeitem",sequenceName="sq_oportunidadeitem")
public class Oportunidadeitem {
	
	protected Integer cdoportunidadeitem;
	protected Oportunidade oportunidade;
	protected PropostaCaixaItem propostacaixaitem;
	protected String valor;
	
	@Id
	@GeneratedValue(generator="sq_oportunidadeitem",strategy=GenerationType.AUTO)
	public Integer getCdoportunidadeitem() {
		return cdoportunidadeitem;
	}
	public void setCdoportunidadeitem(Integer cdoportunidadeitem) {
		this.cdoportunidadeitem = cdoportunidadeitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpropostacaixaitem")
	public PropostaCaixaItem getPropostacaixaitem() {
		return propostacaixaitem;
	}
	
	public String getValor() {
		return valor;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}
	
	public void setPropostacaixaitem(PropostaCaixaItem propostacaixaitem) {
		this.propostacaixaitem = propostacaixaitem;
	}
}
