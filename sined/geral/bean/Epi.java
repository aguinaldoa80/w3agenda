package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;


@Entity
public class Epi extends Material implements Log {

	protected String nomereduzido;
	protected Integer duracao;
	protected String observacao;
	protected Integer caepi;
	
	//Devem pertencer somente a classe m�e:
	//protected Timestamp dtAltera;
	//protected Integer cdUsuarioAltera;
	
	public Epi() {
	}
	
	public Epi(String nomereduzido, Integer duracao, String observacao, Integer caepi) {
		this.nomereduzido = nomereduzido;
		this.duracao = duracao;
		this.observacao = observacao;
		this.caepi = caepi;
	}

	public String getNomereduzido() {
		return nomereduzido;
	}

	public String getObservacao() {
		return observacao;
	}

	@MaxLength(10)
	public Integer getDuracao() {
		return duracao;
	}
	
	@MaxLength(10)
	@DisplayName("CA epi")
	public Integer getCaepi() {
		return caepi;
	}
	
	public void setCaepi(Integer caepi) {
		this.caepi = caepi;
	}
	
	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}

	public void setNomereduzido(String nomereduzido) {
		this.nomereduzido = nomereduzido;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
	@Transient
	public String getNomeMaterialNomeReduzido(){
		if (getNomereduzido()==null){
			return getNome();
		}else {
			return getNomereduzido();
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		return result;
	}	
}
