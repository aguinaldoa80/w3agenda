package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_entregadocumentosincronizacao", sequenceName = "sq_entregadocumentosincronizacao")
public class Entregadocumentosincronizacao implements Log {

	protected Integer cdentregadocumentosincronizacao;	
	protected Entregadocumento entregadocumento;
	protected Venda venda;
	protected Pedidovenda pedidovenda;
	
	protected Integer notaEntradaId;
	protected Date notaEntradaDataChegada;
	protected Date notaEntradaDataEmissao;
	protected Date notaEntradaDataLancamento;
	protected String notaEntradaDeposito;
	protected Integer notaEntradaFornecedor;
	protected String notaEntradaNumero;
	protected String notaEntradaPlacaVeiculo;
	protected String notaEntradaTipo;
	protected String notaEntradaTransportadora;
	
	//Fornecedor
	protected Fornecedor fornecedor;
	protected Cliente cliente;
	protected Integer fornecedorId;
	protected String fornecedorNome;
	protected String fornecedorNatureza;
	protected String fornecedorDocumento;
		
	protected List<Entregamaterialsincronizacao> listaEntregamaterialsincronizacao;
	protected Boolean sincronizado;
	protected Timestamp dtsincronizacao;
	protected Timestamp dttentativasincronizacao;
	protected Boolean devolucao;
	protected Boolean devolucaoefetuada;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	public Entregadocumentosincronizacao(){}
	
	public Entregadocumentosincronizacao(Integer cdentregadocumentosincronizacao){
		this.cdentregadocumentosincronizacao = cdentregadocumentosincronizacao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentosincronizacao")
	public Integer getCdentregadocumentosincronizacao() {
		return cdentregadocumentosincronizacao;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@Transient
	@DisplayName("Id")
	public Integer getNotaEntradaId() {
		if(notaEntradaId == null && entregadocumento != null && entregadocumento.getNumero() != null && !"".equals(entregadocumento.getNumero())){
			try {				
				notaEntradaId =  Integer.parseInt(entregadocumento.getNumero());
			} catch (NumberFormatException e) {}
		}
		return notaEntradaId;
	}
	@DisplayName("Data de Chegada")
	public Date getNotaEntradaDataChegada() {
		return notaEntradaDataChegada;
	}
	@DisplayName("Data de Emiss�o")
	public Date getNotaEntradaDataEmissao() {
		return notaEntradaDataEmissao;
	}
	@DisplayName("Data de Lan�amento")
	public Date getNotaEntradaDataLancamento() {
		return notaEntradaDataLancamento;
	}
	@DisplayName("Dep�sito")
	public String getNotaEntradaDeposito() {
		return notaEntradaDeposito;
	}
	@Transient
	@DisplayName("Fornecedor")
	public Integer getNotaEntradaFornecedor() {
		if(notaEntradaFornecedor == null && fornecedor != null){
			return fornecedor.getCdpessoa();
		}
		return notaEntradaFornecedor;
	}
	@DisplayName("N�mero")
	public String getNotaEntradaNumero() {
		return notaEntradaNumero;
	}
	@DisplayName("Placa do Ve�culo")
	public String getNotaEntradaPlacaVeiculo() {
		return notaEntradaPlacaVeiculo;
	}
	@DisplayName("Tipo")
	public String getNotaEntradaTipo() {
		return notaEntradaTipo;
	}
	@DisplayName("Transportadora")
	public String getNotaEntradaTransportadora() {
		return notaEntradaTransportadora;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}	
	@Transient
	@DisplayName("Fornecedor Id")
	public Integer getFornecedorId() {
		if(fornecedorId == null && devolucao != null && devolucao && cliente != null){
			return cliente.getCdpessoa();
		}else if(fornecedorId == null && fornecedor != null){
			return fornecedor.getCdpessoa();
		}
		return fornecedorId;
	}
	@DisplayName("Fornecedor Nome")
	public String getFornecedorNome() {
		return fornecedorNome;
	}
	@DisplayName("Fornecedor Natureza")
	public String getFornecedorNatureza() {
		return fornecedorNatureza;
	}
	@DisplayName("Fornecedor Cpf/Cnpj")
	public String getFornecedorDocumento() {
		return fornecedorDocumento;
	}
	@DisplayName("Materiais")
	@OneToMany(mappedBy="entregadocumentosincronizacao")
	public List<Entregamaterialsincronizacao> getListaEntregamaterialsincronizacao() {
		return listaEntregamaterialsincronizacao;
	}	
	@DisplayName("Sincronizado")
	public Boolean getSincronizado() {
		return sincronizado;
	}
	@DisplayName("Data da Sincroniza��o")
	public Timestamp getDtsincronizacao() {
		return dtsincronizacao;
	}
	@DisplayName("Data da Tentativa de Sincroniza��o")
	public Timestamp getDttentativasincronizacao() {
		return dttentativasincronizacao;
	}
	@DisplayName("Devolu��o")
	public Boolean getDevolucao() {
		return devolucao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	@DisplayName("Venda")
	public Venda getVenda() {
		return venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	@DisplayName("Pedido de Venda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@DisplayName("Devolu��o efetuada")
	public Boolean getDevolucaoefetuada() {
		return devolucaoefetuada;
	}
	
	
	public void setCdentregadocumentosincronizacao(Integer cdentregadocumentosincronizacao) {
		this.cdentregadocumentosincronizacao = cdentregadocumentosincronizacao;
	}
	public void setNotaEntradaId(Integer notaEntradaId) {
		this.notaEntradaId = notaEntradaId;
	}
	public void setNotaEntradaDataChegada(Date notaEntradaDataChegada) {
		this.notaEntradaDataChegada = notaEntradaDataChegada;
	}
	public void setNotaEntradaDataEmissao(Date notaEntradaDataEmissao) {
		this.notaEntradaDataEmissao = notaEntradaDataEmissao;
	}
	public void setNotaEntradaDataLancamento(Date notaEntradaDataLancamento) {
		this.notaEntradaDataLancamento = notaEntradaDataLancamento;
	}
	public void setNotaEntradaDeposito(String notaEntradaDeposito) {
		this.notaEntradaDeposito = notaEntradaDeposito;
	}
	public void setNotaEntradaFornecedor(Integer notaEntradaFornecedor) {
		this.notaEntradaFornecedor = notaEntradaFornecedor;
	}
	public void setNotaEntradaNumero(String notaEntradaNumero) {
		this.notaEntradaNumero = notaEntradaNumero;
	}
	public void setNotaEntradaPlacaVeiculo(String notaEntradaPlacaVeiculo) {
		this.notaEntradaPlacaVeiculo = notaEntradaPlacaVeiculo;
	}
	public void setNotaEntradaTipo(String notaEntradaTipo) {
		this.notaEntradaTipo = notaEntradaTipo;
	}
	public void setNotaEntradaTransportadora(String notaEntradaTransportadora) {
		this.notaEntradaTransportadora = notaEntradaTransportadora;
	}	
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setFornecedorId(Integer fornecedorId) {
		this.fornecedorId = fornecedorId;
	}
	public void setFornecedorNome(String fornecedorNome) {
		this.fornecedorNome = fornecedorNome;
	}
	public void setFornecedorNatureza(String fornecedorNatureza) {
		this.fornecedorNatureza = fornecedorNatureza;
	}
	public void setFornecedorDocumento(String fornecedorDocumento) {
		this.fornecedorDocumento = fornecedorDocumento;
	}
	public void setListaEntregamaterialsincronizacao(
			List<Entregamaterialsincronizacao> listaEntregamaterialsincronizacao) {
		this.listaEntregamaterialsincronizacao = listaEntregamaterialsincronizacao;
	}
	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}
	public void setDtsincronizacao(Timestamp dtsincronizacao) {
		this.dtsincronizacao = dtsincronizacao;
	}
	public void setDttentativasincronizacao(Timestamp dttentativasincronizacao) {
		this.dttentativasincronizacao = dttentativasincronizacao;
	}
	public void setDevolucao(Boolean devolucao) {
		this.devolucao = devolucao;
	}
	public void setDevolucaoefetuada(Boolean devolucaoefetuada) {
		this.devolucaoefetuada = devolucaoefetuada;
	}

	@Transient
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Transient
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
