package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.FormaPagamentoTef;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivotef", sequenceName = "sq_arquivotef")
@DisplayName("Arquivo de TEF")
public class Arquivotef implements Log {

	protected Integer cdarquivotef;
	protected Timestamp dtenvio;
	protected Arquivotefsituacao situacao;
	protected Configuracaotef configuracaotef;
	protected Arquivo arquivoenvio;
	protected Arquivo arquivoretorno;
	protected Arquivo arquivoretornopagamento;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<Arquivotefitem> listaArquivotefitem = new ListSet<Arquivotefitem>(Arquivotefitem.class);
	protected List<Arquivotefhistorico> listaArquivotefhistorico = new ListSet<Arquivotefhistorico>(Arquivotefhistorico.class);
	
	// TRANSIENTES
	protected Integer quantidadeparcelas = 1;
	protected Configuracaotefterminal configuracaotefterminal;
	protected Money valortotal = new Money();
	protected FormaPagamentoTef formaPagamentoTef = FormaPagamentoTef.TEF_OUTROS;
	protected Configuracaotefadquirente configuracaotefadquirente;
	
	public Arquivotef() {
	}
	
	public Arquivotef(Integer cdarquivotef) {
		super();
		this.cdarquivotef = cdarquivotef;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivotef")
	public Integer getCdarquivotef() {
		return cdarquivotef;
	}
	
	@Required
	@DisplayName("Arquivo de Envio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoenvio")
	public Arquivo getArquivoenvio() {
		return arquivoenvio;
	}

	@Required
	@DisplayName("Configuração de TEF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaotef")
	public Configuracaotef getConfiguracaotef() {
		return configuracaotef;
	}

	@DisplayName("Arquivo de Retorno")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretorno")
	public Arquivo getArquivoretorno() {
		return arquivoretorno;
	}
	
	@DisplayName("Arquivo de Retorno de Pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornopagamento")
	public Arquivo getArquivoretornopagamento() {
		return arquivoretornopagamento;
	}

	@OneToMany(mappedBy="arquivotef", fetch=FetchType.LAZY)
	public List<Arquivotefitem> getListaArquivotefitem() {
		return listaArquivotefitem;
	}
	
	@OneToMany(mappedBy="arquivotef", fetch=FetchType.LAZY)
	public List<Arquivotefhistorico> getListaArquivotefhistorico() {
		return listaArquivotefhistorico;
	}
	
	@DisplayName("Situação")
	public Arquivotefsituacao getSituacao() {
		return situacao;
	}
	
	@DisplayName("Data/Hora de envio")
	public Timestamp getDtenvio() {
		return dtenvio;
	}
	
	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setSituacao(Arquivotefsituacao situacao) {
		this.situacao = situacao;
	}

	public void setListaArquivotefhistorico(
			List<Arquivotefhistorico> listaArquivotefhistorico) {
		this.listaArquivotefhistorico = listaArquivotefhistorico;
	}
	
	public void setArquivoretornopagamento(Arquivo arquivoretornopagamento) {
		this.arquivoretornopagamento = arquivoretornopagamento;
	}
	
	public void setListaArquivotefitem(List<Arquivotefitem> listaArquivotefitem) {
		this.listaArquivotefitem = listaArquivotefitem;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdarquivotef(Integer cdarquivotef) {
		this.cdarquivotef = cdarquivotef;
	}

	public void setConfiguracaotef(Configuracaotef configuracaotef) {
		this.configuracaotef = configuracaotef;
	}

	public void setArquivoenvio(Arquivo arquivoenvio) {
		this.arquivoenvio = arquivoenvio;
	}

	public void setArquivoretorno(Arquivo arquivoretorno) {
		this.arquivoretorno = arquivoretorno;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Transient
	@DisplayName("Quantidade de parcelas")
	public Integer getQuantidadeparcelas() {
		return quantidadeparcelas;
	}

	@Transient
	@DisplayName("Terminal")
	public Configuracaotefterminal getConfiguracaotefterminal() {
		return configuracaotefterminal;
	}

	@Transient
	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}
	
	@Transient
	@DisplayName("Forma de pagamento")
	public FormaPagamentoTef getFormaPagamentoTef() {
		return formaPagamentoTef;
	}
	
	@Transient
	@DisplayName("Adquirente")
	public Configuracaotefadquirente getConfiguracaotefadquirente() {
		return configuracaotefadquirente;
	}
	
	@Transient
	public List<Arquivotefitem> getListaArquivotefitemNota(){
		List<Arquivotefitem> lista = new ArrayList<Arquivotefitem>();
		for (Arquivotefitem arquivotefitem : listaArquivotefitem) {
			if(arquivotefitem.getNotafiscalprodutoduplicata() != null){
				lista.add(arquivotefitem);
			}
		}
		return lista;
	}
	
	@Transient
	public List<Arquivotefitem> getListaArquivotefitemVenda(){
		List<Arquivotefitem> lista = new ArrayList<Arquivotefitem>();
		for (Arquivotefitem arquivotefitem : listaArquivotefitem) {
			if(arquivotefitem.getVendapagamento() != null){
				lista.add(arquivotefitem);
			}
		}
		return lista;
	}
	
	public void setConfiguracaotefadquirente(
			Configuracaotefadquirente configuracaotefadquirente) {
		this.configuracaotefadquirente = configuracaotefadquirente;
	}
	
	public void setFormaPagamentoTef(FormaPagamentoTef formaPagamentoTef) {
		this.formaPagamentoTef = formaPagamentoTef;
	}

	public void setQuantidadeparcelas(Integer quantidadeparcelas) {
		this.quantidadeparcelas = quantidadeparcelas;
	}

	public void setConfiguracaotefterminal(
			Configuracaotefterminal configuracaotefterminal) {
		this.configuracaotefterminal = configuracaotefterminal;
	}

	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}


}
