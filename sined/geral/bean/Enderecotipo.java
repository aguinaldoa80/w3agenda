package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Enderecotipo {
	
	public static Enderecotipo UNICO = new Enderecotipo(1); 
	public static Enderecotipo FATURAMENTO = new Enderecotipo(2); 
	public static Enderecotipo COBRANCA = new Enderecotipo(3); 
	public static Enderecotipo ENTREGA = new Enderecotipo(4); 
	public static Enderecotipo CORRESPONDENCIA = new Enderecotipo(5); 
	public static Enderecotipo INSTALACAO = new Enderecotipo(6); 
	public static Enderecotipo OUTRO = new Enderecotipo(7); 
	public static Enderecotipo INATIVO = new Enderecotipo(8); 
	
	protected Integer cdenderecotipo;
	protected String nome;
	
	public Enderecotipo(){}
	public Enderecotipo(Integer cdenderecotipo){
		this.cdenderecotipo = cdenderecotipo;
	}
	
	@Id
	public Integer getCdenderecotipo() {
		return cdenderecotipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdenderecotipo(Integer cdenderecotipo) {
		this.cdenderecotipo = cdenderecotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Enderecotipo) {
			Enderecotipo et = (Enderecotipo) obj;
			return this.getCdenderecotipo().equals(et.getCdenderecotipo());
		}
		return super.equals(obj);
	}
}
