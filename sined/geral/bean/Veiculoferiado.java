package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_veiculoferiado", sequenceName="sq_veiculoferiado")
public class Veiculoferiado implements Log {
	
	protected Integer cdveiculoferiado;
	protected String nome;
	protected Date data;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("Data do feriado")
	@Required
	public Date getData() {
		return data;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculoferiado")
	public Integer getCdveiculoferiado() {
		return cdveiculoferiado;
	}
	
	@DisplayName("Nome")
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdveiculoferiado(Integer cdveiculoferiado) {
		this.cdveiculoferiado = cdveiculoferiado;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setData(Date data) {
		this.data = data;
	}

}
