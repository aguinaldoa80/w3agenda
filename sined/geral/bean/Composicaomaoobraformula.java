package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_composicaomaoobraformula", sequenceName = "sq_composicaomaoobraformula")
public class Composicaomaoobraformula {

	protected Integer cdcomposicaomaoobraformula;
	protected Composicaomaoobra composicaomaoobra;
	protected Integer ordem;
	protected String identificador;
	protected String formula;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_composicaomaoobraformula")
	public Integer getCdcomposicaomaoobraformula() {
		return cdcomposicaomaoobraformula;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomposicaomaoobra")
	public Composicaomaoobra getComposicaomaoobra() {
		return composicaomaoobra;
	}

	@Required
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	@MaxLength(30)
	public String getIdentificador() {
		return identificador;
	}

	@Required
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}

	public void setCdcomposicaomaoobraformula(Integer cdcomposicaomaoobraformula) {
		this.cdcomposicaomaoobraformula = cdcomposicaomaoobraformula;
	}

	public void setComposicaomaoobra(Composicaomaoobra composicaomaoobra) {
		this.composicaomaoobra = composicaomaoobra;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

}
