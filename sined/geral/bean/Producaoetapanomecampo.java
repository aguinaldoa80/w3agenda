package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocampo;

@Entity
@SequenceGenerator(name="sq_producaoetapanomecampo",sequenceName="sq_producaoetapanomecampo")
public class Producaoetapanomecampo {

	protected Integer cdproducaoetapanomecampo;
	protected Producaoetapanome producaoetapanome;
	protected String nome;
	protected Tipocampo tipo;
	protected Boolean obrigatorio;
	
	public Producaoetapanomecampo() {
	}
	
	public Producaoetapanomecampo(Integer cdproducaoetapanomecampo) {
		this.cdproducaoetapanomecampo = cdproducaoetapanomecampo;
	}
	
	@Id
	@GeneratedValue(generator="sq_producaoetapanomecampo",strategy=GenerationType.AUTO)
	public Integer getCdproducaoetapanomecampo() {
		return cdproducaoetapanomecampo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapanome")
	public Producaoetapanome getProducaoetapanome() {
		return producaoetapanome;
	}
	@Required
	@MaxLength(30)
	public String getNome() {
		return nome;
	}
	@Required
	public Tipocampo getTipo() {
		return tipo;
	}
	@DisplayName("Obrigatório")
	public Boolean getObrigatorio() {
		return obrigatorio;
	}
	public void setCdproducaoetapanomecampo(Integer cdproducaoetapanomecampo) {
		this.cdproducaoetapanomecampo = cdproducaoetapanomecampo;
	}
	public void setProducaoetapanome(Producaoetapanome producaoetapanome) {
		this.producaoetapanome = producaoetapanome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipo(Tipocampo tipo) {
		this.tipo = tipo;
	}
	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
}
