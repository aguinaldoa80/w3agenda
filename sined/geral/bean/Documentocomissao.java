package br.com.linkcom.sined.geral.bean;
 
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.DocumentocomissaoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobccomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.service.ContapagarService;
import br.com.linkcom.sined.geral.service.ParcelamentoService;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentocomissao",sequenceName="sq_documentocomissao")
public class Documentocomissao implements Log{
	
	protected Integer cddocumentocomissao;
	protected Comissionamento comissionamento;
	protected Vendedortipo vendedortipo;
	protected Pessoa pessoa;	
	protected Documento documento;
	protected Nota nota;
	protected Contrato contrato;
	protected Boolean vendedor;
	protected Boolean indicacao;
	protected Boolean agencia;
	protected Boolean representacao;
	protected Colaboradorcomissao colaboradorcomissao;
	protected Money valorcomissao;
	protected Money percentualcomissao;
	protected Venda venda;
	protected Pedidovenda pedidovenda;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Money origemvalorhora;
	protected Vendamaterial vendamaterial;
	
	//Transient
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected Cliente cliente;
	protected Money comissao;
	protected String strComissao;
	protected Money valor;
	protected Boolean pago;
	protected Money totalvendas;
	protected Money valorcomissaoListagem;
	protected List<Documento> documentosNegociados;
	protected String documentoNota;
	protected Money valorrepassadoreport;
	protected DocumentocomissaoOrigem documentocomissaoOrigem;
	protected Boolean pagoDocumentoComissaoVendedor;
	protected String whereIn;
	protected Boolean repassado;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentocomissao")
	public Integer getCddocumentocomissao() {
		return cddocumentocomissao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	
	@DisplayName("Tipo do vendedor")
	public Vendedortipo getVendedortipo() {
		return vendedortipo;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorcomissao")
	public Colaboradorcomissao getColaboradorcomissao() {
		return colaboradorcomissao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	public Boolean getVendedor() {
		return vendedor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	@DisplayName("Valor")
	public Money getValorcomissao() {
		return valorcomissao;
	}
	@DisplayName("Percentual")
	public Money getPercentualcomissao() {
		return percentualcomissao;
	}
	@DisplayName("Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	@DisplayName("Pedido de Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")	
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}

	@Transient
	@DisplayName("Origem")
	public DocumentocomissaoOrigem getDocumentocomissaoOrigem() {
		return documentocomissaoOrigem;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}
	public void setVendedor(Boolean vendedor) {
		this.vendedor = vendedor;
	}
	public void setCddocumentocomissao(Integer cddocumentocomissao) {
		this.cddocumentocomissao = cddocumentocomissao;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setVendedortipo(Vendedortipo vendedortipo) {
		this.vendedortipo = vendedortipo;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setColaboradorcomissao(Colaboradorcomissao colaboradorcomissao) {
		this.colaboradorcomissao = colaboradorcomissao;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setValorcomissao(Money valorcomissao) {
		this.valorcomissao = valorcomissao;
	}
	public void setPercentualcomissao(Money percentualcomissao) {
		this.percentualcomissao = percentualcomissao;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	public Boolean getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(Boolean indicacao) {
		this.indicacao = indicacao;
	}

	/*TRANSIENTES*/
	@DisplayName("Comiss�o")
	@Transient
	public Money getComissao() {
		Money valor = getValor();
		
		if(this.comissionamento.getPercentual() != null)
			this.comissao = new Money(this.comissionamento.getPercentual() * valor.getValue().doubleValue() / 100);
		else
			this.comissao = new Money(this.comissionamento.getValor());
		
		return comissao;
	}
	
	public void setComissao(Money comissao) {
		this.comissao = comissao;
	}
	
	@DisplayName("Comiss�o")
	@Transient
	public String getStrComissao() {
		String str = new Money().toString();
		if(this.comissionamento != null){
			if(this.comissionamento.getPercentual() != null)
				str = this.comissionamento.getPercentual() + "%";
			else if(this.comissionamento.getValor() != null)
				str = new Money(this.comissionamento.getValor()).toString();
		}
		
		if(getContrato() != null && getContrato().getListaContratocolaborador() != null && 
				!getContrato().getListaContratocolaborador().isEmpty() && getPessoa() != null && getPessoa().getCdpessoa() != null){
			for(Contratocolaborador cc : getContrato().getListaContratocolaborador()){
				if(cc.getColaborador() != null && cc.getColaborador().getCdpessoa() != null && cc.getOrdemcomissao() != null &&
						getPessoa().getCdpessoa().equals(cc.getColaborador().getCdpessoa())){
					str += " (Ordem " + cc.getOrdemcomissao() +")";
					break;
				}
			}
		}
		
		return str;
	}
	
	public void setComissao(String strComissao) {
		this.strComissao = strComissao;
	}
	
	@Transient
	public Money getValor() {
		Money valor = null;
		if(this.getContrato() != null && this.getContrato().getValordedutivocomissionamento() != null){
			 if(this.nota != null && this.nota.getCdNota() != null && this.nota instanceof NotaFiscalServico){
				if(this.comissionamento != null && this.comissionamento.getTipobccomissionamento() != null){
					if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
						Integer numParcela = 1;
						if(this.documento != null && this.documento.getCddocumento() != null){
							numParcela = ParcelamentoService.getInstance().getNumeroParcelaByDocumento(documento);
						}
						if(this.documento != null && this.documento.getCddocumento() != null && this.comissionamento.getConsiderardiferencapagamento() != null && this.comissionamento.getConsiderardiferencapagamento()){
							valor = ParcelamentoService.getInstance().getValorDividoNumParcelasDiferencapagamento(documento, 
									((NotaFiscalServico)this.nota).getValorBruto(), 
									this.documento.getValorPagoComissionamento(),
									((NotaFiscalServico)this.nota).getValorNota(),
									numParcela);
						}else {
							valor = ((NotaFiscalServico)this.nota).getValorBruto().divide(new Money(numParcela)).subtract(this.getContrato().getValordedutivocomissionamento());
						}
					}else if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
						if(this.documento != null && this.documento.getCddocumento() != null){
							valor = this.documento.getAux_documento().getValoratual();
						}
					}else {
						valor = ((NotaFiscalServico)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
					}
				} else {
					valor = ((NotaFiscalServico)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
				}
			 } else if(this.nota != null && this.nota.getCdNota() != null && this.nota instanceof Notafiscalproduto){
				if(this.comissionamento != null && this.comissionamento.getTipobccomissionamento() != null){
					if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
						Integer numParcela = 1;
						if(this.documento != null && this.documento.getCddocumento() != null){
							numParcela = ParcelamentoService.getInstance().getNumeroParcelaByDocumento(documento);
						}
						if(this.documento != null && this.documento.getCddocumento() != null && this.comissionamento.getConsiderardiferencapagamento() != null && this.comissionamento.getConsiderardiferencapagamento()){
							if(nota instanceof NotaFiscalServico){
								valor = ParcelamentoService.getInstance().getValorDividoNumParcelasDiferencapagamento(documento, 
										((NotaFiscalServico)this.nota).getValorBruto(), 
										this.documento.getValorPagoComissionamento(),
										((NotaFiscalServico)this.nota).getValorNota(),
										numParcela);
							} else if(nota instanceof Notafiscalproduto){
								valor = ParcelamentoService.getInstance().getValorDividoNumParcelasDiferencapagamento(documento, 
										((Notafiscalproduto)this.nota).getValorBruto(), 
										this.documento.getValorPagoComissionamento(),
										((Notafiscalproduto)this.nota).getValorNota(),
										numParcela);
							}
						}else {
							if(nota instanceof NotaFiscalServico){
								valor = ((NotaFiscalServico)this.nota).getValorBruto().divide(new Money(numParcela)).subtract(this.getContrato().getValordedutivocomissionamento());
							} else if(nota instanceof Notafiscalproduto){
								valor = ((Notafiscalproduto)this.nota).getValorBruto().divide(new Money(numParcela)).subtract(this.getContrato().getValordedutivocomissionamento());
							}
						}
					}else if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
						if(this.documento != null && this.documento.getCddocumento() != null){
							valor = this.documento.getAux_documento().getValoratual();
						}
					}else {	
						valor = ((Notafiscalproduto)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
					}
				} else {
					valor = ((Notafiscalproduto)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
				}
			 } else if(this.documento != null && this.documento.getCddocumento() != null){
				valor = this.documento.getAux_documento().getValoratual().subtract(this.getContrato().getValordedutivocomissionamento());
			 } 
		}else {
			if(this.nota != null && this.nota.getCdNota() != null && this.nota instanceof NotaFiscalServico){
				if(this.comissionamento != null && this.comissionamento.getTipobccomissionamento() != null){
					if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
						Integer numParcela = 1;
						if(this.documento != null && this.documento.getCddocumento() != null){
							numParcela = ParcelamentoService.getInstance().getNumeroParcelaByDocumento(documento);
						}
						if(this.documento != null && this.documento.getCddocumento() != null && this.comissionamento.getConsiderardiferencapagamento() != null && this.comissionamento.getConsiderardiferencapagamento()){
							valor = ParcelamentoService.getInstance().getValorDividoNumParcelasDiferencapagamento(documento, 
									((NotaFiscalServico)this.nota).getValorBruto(), 
									this.documento.getValorPagoComissionamento(),
									((NotaFiscalServico)this.nota).getValorNota(),
									numParcela);
						}else {
							valor = ((NotaFiscalServico)this.nota).getValorBruto().divide(new Money(numParcela));
						}
					}else if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
						if(this.documento != null && this.documento.getCddocumento() != null){
							valor = this.documento.getAux_documento().getValoratual();
						}
					}else {	
						valor = ((Notafiscalproduto)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
					}
				} else {
					valor = ((NotaFiscalServico)this.nota).getValorNota();
				}
			} else if(this.nota != null && this.nota.getCdNota() != null && this.nota instanceof Notafiscalproduto){
				if(this.comissionamento != null && this.comissionamento.getTipobccomissionamento() != null){
					if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_BRUTO)){
						Integer numParcela = 1;
						if(this.documento != null && this.documento.getCddocumento() != null){
							numParcela = ParcelamentoService.getInstance().getNumeroParcelaByDocumento(documento);
						}
						if(this.documento != null && this.documento.getCddocumento() != null && this.comissionamento.getConsiderardiferencapagamento() != null && this.comissionamento.getConsiderardiferencapagamento()){
							valor = ParcelamentoService.getInstance().getValorDividoNumParcelasDiferencapagamento(documento, 
									((NotaFiscalServico)this.nota).getValorBruto(), 
									this.documento.getValorPagoComissionamento(),
									((NotaFiscalServico)this.nota).getValorNota(),
									numParcela);
						}else {
							valor = ((Notafiscalproduto)this.nota).getValorBruto().divide(new Money(numParcela));
						}
					}else if(this.comissionamento.getTipobccomissionamento().equals(Tipobccomissionamento.VALOR_PAGO)){
						if(this.documento != null && this.documento.getCddocumento() != null){
							valor = this.documento.getAux_documento().getValoratual();
						}
					}else {	
						valor = ((Notafiscalproduto)this.nota).getValorNota().subtract(this.getContrato().getValordedutivocomissionamento());
					}
				} else {
					valor = ((Notafiscalproduto)this.nota).getValorNota();
				}
			} else if(this.documento != null && this.documento.getCddocumento() != null){
				valor = this.documento.getAux_documento().getValoratual();
			} 
		}
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Transient
	public Boolean getPago() {
		Boolean pago = null;
		if(this.documento != null && this.documento.getCddocumento() != null){
			pago = this.documento.getDocumentoacao().equals(Documentoacao.BAIXADA);
			
			if(!pago && this.documentosNegociados != null && this.documentosNegociados.size() > 0){
				pago = true;
				for (Documento d : this.documentosNegociados) {
					if(!d.getDocumentoacao().equals(Documentoacao.BAIXADA)){
						pago = false;
						break;
					}
				}
			}
		} else if(this.nota != null && this.nota.getCdNota() != null){
			if(this.nota.getListaNotaDocumento() != null && !this.nota.getListaNotaDocumento().isEmpty()){
				pago = true;
				for (NotaDocumento notaDocumento : this.nota.getListaNotaDocumento()) {
					if(!notaDocumento.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA) && !notaDocumento.getDocumento().getDocumentoacao().equals(Documentoacao.NEGOCIADA)){
						pago = false;
						break;
					}
				}
			}
		}
		
		return pago;
	}
	
	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	@Transient
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Transient
	public Cliente getCliente() {
		return cliente;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@DisplayName("Total de vendas")
	public Money getTotalvendas() {
		return totalvendas;
	}

	public void setTotalvendas(Money totalvendas) {
		this.totalvendas = totalvendas;
	}

	@Transient
	public Money getValorcomissaoListagem() {
		if(valorcomissao != null)
			return valorcomissao;
		else {
			Money valor = new Money();
			if(this.getContrato() != null && this.getContrato().getListaContratocolaborador() != null && 
					!this.getContrato().getListaContratocolaborador().isEmpty() && getDocumento() != null && getDocumento().getAux_documento() != null){
				valor = ContapagarService.getInstance().calculaComissao(
						this.getContrato().getListaContratocolaborador(), getPessoa(), getDocumento().getAux_documento().getValoratual(), getContrato());
			}
			
			return valor;
		}
	}
	public void setValorcomissaoListagem(Money valorcomissaoListagem) {
		this.valorcomissaoListagem = valorcomissaoListagem;
	}
	
	@Transient
	public List<Documento> getDocumentosNegociados() {
		return documentosNegociados;
	}
	
	public void setDocumentosNegociados(List<Documento> documentosNegociados) {
		this.documentosNegociados = documentosNegociados;
	}
	
	@Transient
	public String getDocumentoNota() {
		return documentoNota;
	}
	
	public void setDocumentoNota(String documentoNota) {
		this.documentoNota = documentoNota;
	}

	@Transient
	public Money getValorrepassadoreport() {
		return valorrepassadoreport;
	}
	public void setValorrepassadoreport(Money valorrepassadoreport) {
		this.valorrepassadoreport = valorrepassadoreport;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cddocumentocomissao == null) ? 0 : cddocumentocomissao
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documentocomissao other = (Documentocomissao) obj;
		if (cddocumentocomissao == null) {
			if (other.cddocumentocomissao != null)
				return false;
		} else if (!cddocumentocomissao.equals(other.cddocumentocomissao))
			return false;
		return true;
	}
	
	@Transient
	public String getOrigem (){
		if (pedidovenda != null)  return "Pedido de Venda";
		if (venda != null) return "Venda";
		if (contrato != null) return "Contrato";
		return "";
	}
	
	public void setDocumentocomissaoOrigem(DocumentocomissaoOrigem documentocomissaoOrigem) {
		this.documentocomissaoOrigem = documentocomissaoOrigem;
	}

	@Transient
	@DisplayName("Pago")
	public Boolean getPagoDocumentoComissaoVendedor() {
		return pagoDocumentoComissaoVendedor;
	}

	public void setPagoDocumentoComissaoVendedor(
			Boolean pagoDocumentoComissaoVendedor) {
		this.pagoDocumentoComissaoVendedor = pagoDocumentoComissaoVendedor;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public Boolean getAgencia() {
		return agencia;
	}
	public Boolean getRepresentacao() {
		return representacao;
	}
	
	public void setRepresentacao(Boolean representacao) {
		this.representacao = representacao;
	}
	public void setAgencia(Boolean agencia) {
		this.agencia = agencia;
	}
	
	public Money getOrigemvalorhora() {
		return origemvalorhora;
	}
	public void setOrigemvalorhora(Money origemvalorhora) {
		this.origemvalorhora = origemvalorhora;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	
	@Transient
	public Boolean getRepassado() {
		return !(this.colaboradorcomissao == null
				|| this.colaboradorcomissao.getCdcolaboradorcomissao() == null
				|| this.colaboradorcomissao.getDocumento() == null
				|| this.colaboradorcomissao.getDocumento().getDocumentoacao() == null
				|| this.colaboradorcomissao.getDocumento().getDocumentoacao().getCddocumentoacao() == null);
	}
	
	public void setRepassado(Boolean repassado) {
		this.repassado = repassado;
	}
}
