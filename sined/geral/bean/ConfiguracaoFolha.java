package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_configuracaofolha", sequenceName="sq_configuracaofolha")
public class ConfiguracaoFolha implements Log{

	private Integer cdConfiguracaoFolha;
	private String descricao;
	private Empresa empresa;
	private Agendamento agendamento;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private List<ConfiguracaoFolhaEvento> listaEvento;
	

	@Id
	@GeneratedValue(generator="sq_configuracaofolha", strategy=GenerationType.AUTO)
	public Integer getCdConfiguracaoFolha() {
		return cdConfiguracaoFolha;
	}
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamento")
	public Agendamento getAgendamento() {
		return agendamento;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="configuracaoFolha")
	public List<ConfiguracaoFolhaEvento> getListaEvento() {
		return listaEvento;
	}
	public void setCdConfiguracaoFolha(Integer cdConfiguracaoFolha) {
		this.cdConfiguracaoFolha = cdConfiguracaoFolha;
	}
	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaEvento(List<ConfiguracaoFolhaEvento> listaEvento) {
		this.listaEvento = listaEvento;
	}
}
