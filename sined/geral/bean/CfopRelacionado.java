package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_cfoprelacionado", sequenceName = "sq_cfoprelacionado")
public class CfopRelacionado {
	
	private Integer cdcfoprelacionado;
	private Cfop cfop;
	private Cfop cfopdestino;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cfoprelacionado")
	public Integer getCdcfoprelacionado() {
		return cdcfoprelacionado;
	}
	public void setCdcfoprelacionado(Integer cdcfoprelacionado) {
		this.cdcfoprelacionado = cdcfoprelacionado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfop")
	public Cfop getCfop() {
		return cfop;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	@DisplayName("CFOP Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfopdestino")
	public Cfop getCfopdestino() {
		return cfopdestino;
	}
	public void setCfopdestino(Cfop cfopdestino) {
		this.cfopdestino = cfopdestino;
	}
}
