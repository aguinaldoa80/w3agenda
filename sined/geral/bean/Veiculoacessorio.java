package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculoacessorio", sequenceName = "sq_veiculoacessorio")
public class Veiculoacessorio implements Log{

	protected Integer cdveiculoacessorio;
	protected Veiculo veiculo;
	protected Integer quantidade;
	protected String acessorio;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculoacessorio")
	public Integer getCdveiculoacessorio() {
		return cdveiculoacessorio;
	}
	@Required
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	@MaxLength(9)
	public Integer getQuantidade() {
		return quantidade;
	}
	@Required
	@MaxLength(50)
	public String getAcessorio() {
		return acessorio;
	}

	public void setCdveiculoacessorio(Integer cdveiculoacessorio) {
		this.cdveiculoacessorio = cdveiculoacessorio;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setAcessorio(String acessorio) {
		this.acessorio = acessorio;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
}