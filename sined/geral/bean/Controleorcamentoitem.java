package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@DisplayName("Itens")
@Entity
@SequenceGenerator(name="sq_controleorcamentoitem",sequenceName="sq_controleorcamentoitem")
public class Controleorcamentoitem implements Log{

	protected Integer cdcontroleorcamentoitem;
	protected Controleorcamento controleorcamento;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Money valor;
	protected Date mesano;
	
	protected Projetotipo projetoTipo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENT
	protected String  mesanoAux;
	protected Money valorOrigem = new Money(0.00);
	protected String identificadorNomeContaGerencial;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_controleorcamentoitem")
	public Integer getCdcontroleorcamentoitem() {
		return cdcontroleorcamentoitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontroleorcamento")
	public Controleorcamento getControleorcamento() {
		return controleorcamento;
	}
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Required
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	@DisplayName("M�s/ano")
	public Date getMesano() {
		return mesano;
	}
	@DisplayName("Tipo De Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetotipo")
	public Projetotipo getProjetoTipo() {
		return projetoTipo;
	}
//////////////////////////////////////////////|
//  INICIO SET						 /////////|
//////////////////////////////////////////////|
	
	public void setProjetoTipo(Projetotipo projetoTipo) {
		this.projetoTipo = projetoTipo;
	}
	
	public void setCdcontroleorcamentoitem(Integer cdcontroleorcamentoitem) {
		this.cdcontroleorcamentoitem = cdcontroleorcamentoitem;
	}
	public void setControleorcamento(Controleorcamento controleorcamento) {
		this.controleorcamento = controleorcamento;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}
	
	//////////////////////////////////////////////|
	//  INICIO DOS TRANSIENTES	E OUTROS /////////|
	//////////////////////////////////////////////|
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	@Transient
	@DisplayName("Valor Or�ado")
	public Money getValorOrigem() {
		return valorOrigem;
	}
	public void setValorOrigem(Money valorOrigem) {
		this.valorOrigem = valorOrigem;
	}
	
	public void setMesanoAux(String mesanoAux) {
		if(mesanoAux!=null){
			try {
				this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}
	}
	
	@Transient
	@DisplayName("Conta gerencial")
	public String getIdentificadorNomeContaGerencial(){
		if(this.contagerencial != null){
			this.identificadorNomeContaGerencial = contagerencial.getVcontagerencial().getIdentificador() + " - " + contagerencial.getVcontagerencial().getNome();
		}
		
		return identificadorNomeContaGerencial;
		
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
}
