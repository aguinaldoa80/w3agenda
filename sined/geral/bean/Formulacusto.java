package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_formulacusto", sequenceName = "sq_formulacusto")
public class Formulacusto implements Log{
	
	protected Integer cdformulacusto;
	protected String nome;
	protected Boolean ativo;
	protected List<Formulacustoitem> listaFormulacustoitem;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_formulacusto")
	public Integer getCdformulacusto() {
		return cdformulacusto;
	}
	@MaxLength(40)
	@Required   
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("F�rmula")
	@OneToMany(mappedBy="formulacusto")
	public List<Formulacustoitem> getListaFormulacustoitem() {
		return listaFormulacustoitem;
	}
	
	public void setCdformulacusto(Integer cdformulacusto) {
		this.cdformulacusto = cdformulacusto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaFormulacustoitem(List<Formulacustoitem> listaFormulacustoitem) {
		this.listaFormulacustoitem = listaFormulacustoitem;
	}
	
//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}