package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.BounceTypeEnum;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.geral.bean.enumeration.SendgridEvent;

@Entity
@SequenceGenerator(sequenceName="sq_envioemailitemhistorico", name="sq_envioemailitemhistorico")
public class Envioemailitemhistorico {

	private Integer cdenvioemailitemhistorico;
	private SendgridEvent event;
	private String response;
	private Integer attempt;
	private String urlclicked;
	private String status;
	private String reason;
	private BounceTypeEnum type;
	private String ip;
	private String useragent;
	private Envioemailitem envioemailitem;
	private Timestamp ultimaverificacao;
	private EmailStatusEnum situacaoemail;
	
	
	@Id
	@GeneratedValue(generator="sq_envioemailitemhistorico", strategy=GenerationType.AUTO)
	public Integer getCdenvioemailitemhistorico() {
		return cdenvioemailitemhistorico;
	}
	public void setCdenvioemailitemhistorico(Integer cdenvioemailitemhistorico) {
		this.cdenvioemailitemhistorico = cdenvioemailitemhistorico;
	}
	public SendgridEvent getEvent() {
		return event;
	}
	public void setEvent(SendgridEvent event) {
		this.event = event;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Integer getAttempt() {
		return attempt;
	}
	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}
	public String getUrlclicked() {
		return urlclicked;
	}
	public void setUrlclicked(String urlclicked) {
		this.urlclicked = urlclicked;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public BounceTypeEnum getType() {
		return type;
	}
	public void setType(BounceTypeEnum type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getUseragent() {
		return useragent;
	}
	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenvioemailitem")
	public Envioemailitem getEnvioemailitem() {
		return envioemailitem;
	}
	public void setEnvioemailitem(Envioemailitem envioemailitem) {
		this.envioemailitem = envioemailitem;
	}
	@DisplayName("Data de verifica��o")
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
	@DisplayName("Situa��o")
	public EmailStatusEnum getSituacaoemail() {
		return situacaoemail;
	}
	public void setSituacaoemail(EmailStatusEnum situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
}
