package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioMaterialEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_inventariomaterialhistorico", sequenceName = "sq_inventariomaterialhistorico")
@DisplayName("Hist�rico")
public class InventarioMaterialHistorico implements Log{
	protected Integer cdInventarioMaterialHistorico;
	protected Inventariomaterial inventarioMaterial;
	protected InventarioMaterialEnum acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inventariomaterialhistorico")
	public Integer getCdInventarioMaterialHistorico() {
		return cdInventarioMaterialHistorico;
	}
	public void setCdInventarioMaterialHistorico(
			Integer cdInventarioMaterialHistorico) {
		this.cdInventarioMaterialHistorico = cdInventarioMaterialHistorico;
	}
	
	@DisplayName("A��o")
	public InventarioMaterialEnum getAcao() {
		return acao;
	}
	public void setAcao(InventarioMaterialEnum acao) {
		this.acao = acao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinventariomaterial")
	public Inventariomaterial getInventarioMaterial() {
		return inventarioMaterial;
	}
	
	public void setInventarioMaterial(Inventariomaterial inventarioMaterial) {
		this.inventarioMaterial = inventarioMaterial;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
}
