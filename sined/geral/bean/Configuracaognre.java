package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.GnreCodigoReceitaEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Configura��o da GNRE")
@SequenceGenerator(name = "sq_configuracaognre", sequenceName = "sq_configuracaognre")
public class Configuracaognre implements Log {
	
	protected Integer cdconfiguracaognre;
	protected Empresa empresa;
	protected GnreCodigoReceitaEnum codigoreceita;
	protected Fornecedor fornecedor;
	protected Documentotipo documentotipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Configuracaognrerateio> listaConfiguracaognrerateio = new ListSet<Configuracaognrerateio>(Configuracaognrerateio.class);
	protected List<Configuracaognreuf> listaConfiguracaognreuf = new ListSet<Configuracaognreuf>(Configuracaognreuf.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaognre")
	public Integer getCdconfiguracaognre() {
		return cdconfiguracaognre;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@Required
	@DisplayName("C�digo da receita")
	public GnreCodigoReceitaEnum getCodigoreceita() {
		return codigoreceita;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Rateio")
	@OneToMany(mappedBy="configuracaognre")
	public List<Configuracaognrerateio> getListaConfiguracaognrerateio() {
		return listaConfiguracaognrerateio;
	}
	
	@DisplayName("UF")
	@OneToMany(mappedBy="configuracaognre")
	public List<Configuracaognreuf> getListaConfiguracaognreuf() {
		return listaConfiguracaognreuf;
	}
	
	public void setListaConfiguracaognreuf(
			List<Configuracaognreuf> listaConfiguracaognreuf) {
		this.listaConfiguracaognreuf = listaConfiguracaognreuf;
	}
	
	public void setListaConfiguracaognrerateio(
			List<Configuracaognrerateio> listaConfiguracaognrerateio) {
		this.listaConfiguracaognrerateio = listaConfiguracaognrerateio;
	}

	public void setCdconfiguracaognre(Integer cdconfiguracaognre) {
		this.cdconfiguracaognre = cdconfiguracaognre;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCodigoreceita(GnreCodigoReceitaEnum codigoreceita) {
		this.codigoreceita = codigoreceita;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
