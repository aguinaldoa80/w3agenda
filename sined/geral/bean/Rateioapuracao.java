package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Apura��o de rateio")
@SequenceGenerator(name="sq_rateioapuracao",sequenceName="sq_rateioapuracao")
public class Rateioapuracao implements Log {

	private Integer cdrateioapuracao;
	private String nome;
	private Centrocusto centrocusto;
	private Contagerencial contagerencial;
	private List<Rateioapuracaoempresa> listaRateioapuracaoempresa = new ArrayList<Rateioapuracaoempresa>();
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_rateioapuracao")
	public Integer getCdrateioapuracao() {
		return cdrateioapuracao;
	}
	
	@Required
	@DisplayName("Nome")
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Empresa(s)")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="rateioapuracao")
	public List<Rateioapuracaoempresa> getListaRateioapuracaoempresa() {
		return listaRateioapuracaoempresa;
	}
	
	public void setListaRateioapuracaoempresa(
			List<Rateioapuracaoempresa> listaRateioapuracaoempresa) {
		this.listaRateioapuracaoempresa = listaRateioapuracaoempresa;
	}

	public void setCdrateioapuracao(Integer cdrateioapuracao) {
		this.cdrateioapuracao = cdrateioapuracao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
