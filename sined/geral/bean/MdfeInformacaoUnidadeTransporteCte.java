package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeTipoTransporte;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeinformacaounidadetransportecte", sequenceName = "sq_mdfeinformacaounidadetransportecte")
public class MdfeInformacaoUnidadeTransporteCte {

	protected Integer cdMdfeInformacaoUnidadeTransporteCte;
	protected Mdfe mdfe;
	protected String idCte;
	protected String idUnidadeTransporte;
	protected MdfeTipoTransporte tipoTransporte;
	protected String identificacaoTransporte;
	protected Money quantidadeRateada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeinformacaounidadetransportecte")
	public Integer getCdMdfeInformacaoUnidadeTransporteCte() {
		return cdMdfeInformacaoUnidadeTransporteCte;
	}
	public void setCdMdfeInformacaoUnidadeTransporteCte(
			Integer cdMdfeInformacaoUnidadeTransporte) {
		this.cdMdfeInformacaoUnidadeTransporteCte = cdMdfeInformacaoUnidadeTransporte;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}

	@DisplayName("ID do CTe")
	public String getIdCte() {
		return idCte;
	}
	public void setIdCte(String idCte) {
		this.idCte = idCte;
	}
	
	@DisplayName("ID da unidade de transporte")
	public String getIdUnidadeTransporte() {
		return idUnidadeTransporte;
	}
	public void setIdUnidadeTransporte(String idUnidadeTransporte) {
		this.idUnidadeTransporte = idUnidadeTransporte;
	}
	
	@Required
	@DisplayName("Tipo de transporte")
	public MdfeTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}
	public void setTipoTransporte(MdfeTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação do transporte")
	public String getIdentificacaoTransporte() {
		return identificacaoTransporte;
	}
	public void setIdentificacaoTransporte(String identificacaoTransporte) {
		this.identificacaoTransporte = identificacaoTransporte;
	}

	@DisplayName("Quantidade rateada")
	public Money getQuantidadeRateada() {
		return quantidadeRateada;
	}
	public void setQuantidadeRateada(Money quantidadeRateada) {
		this.quantidadeRateada = quantidadeRateada;
	}
	
	@Transient
	public List<MdfeLacreUnidadeTransporteCte> getListaLacreUnidadeTransporteCte(MdfeInformacaoUnidadeTransporteCte infUnTransCte, Mdfe mdfe) {
		List<MdfeLacreUnidadeTransporteCte> list = new ArrayList<MdfeLacreUnidadeTransporteCte>(); 
		if(SinedUtil.isListNotEmpty(mdfe.getListaLacreUnidadeTransporteCte())){
			for(MdfeLacreUnidadeTransporteCte item : mdfe.getListaLacreUnidadeTransporteCte()){
				if(item.getIdUnidadeTransporte().equals(infUnTransCte.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeCargaCte> getListaInformacaoUnidadeCargaCte(MdfeInformacaoUnidadeTransporteCte infUnTransCte, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeCargaCte> list = new ArrayList<MdfeInformacaoUnidadeCargaCte>(); 
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaCte())){
			for(MdfeInformacaoUnidadeCargaCte item : mdfe.getListaInformacaoUnidadeCargaCte()){
				if(item.getIdUnidadeTransporte().equals(infUnTransCte.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
