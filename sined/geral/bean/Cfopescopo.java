package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_cfopescopo", sequenceName = "sq_cfopescopo")
public class Cfopescopo {

	public static final Cfopescopo INTERNACIONAL = new Cfopescopo(1,"Internacional"); 
	public static final Cfopescopo ESTADUAL = new Cfopescopo(2,"Estadual"); 
	public static final Cfopescopo FORA_DO_ESTADO = new Cfopescopo(3,"Fora do estado"); 
	
	protected Integer cdcfopescopo;
	protected String descricao;
	
	public Cfopescopo(){
	}
	
	public Cfopescopo(Integer cdcfopescopo, String descricao){
		this.cdcfopescopo = cdcfopescopo;
		this.descricao = descricao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cfopescopo")
	public Integer getCdcfopescopo() {
		return cdcfopescopo;
	}
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdcfopescopo(Integer cdcfopescopo) {
		this.cdcfopescopo = cdcfopescopo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcfopescopo == null) ? 0 : cdcfopescopo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cfopescopo other = (Cfopescopo) obj;
		if (cdcfopescopo == null) {
			if (other.cdcfopescopo != null)
				return false;
		} else if (!cdcfopescopo.equals(other.cdcfopescopo))
			return false;
		return true;
	}
	
	
	
}
