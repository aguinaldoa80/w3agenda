package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TefParcelamentoAdmin;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_configuracaotef", sequenceName = "sq_configuracaotef")
@DisplayName("Configura��o do TEF")
public class Configuracaotef implements Log {
	
	protected Integer cdconfiguracaotef;
	protected Empresa empresa;
	protected TefParcelamentoAdmin parcelamentoadmin = TefParcelamentoAdmin.PERGUNTAR;
	protected Boolean obrigarpagamentocartaonfe = true;
	protected Boolean obrigarpagamentocartaonfce = true;
	protected Boolean impressaoautomatica = false;
	protected String chaveintegracao;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected Set<Configuracaotefterminal> listaConfiguracaotefterminal = new ListSet<Configuracaotefterminal>(Configuracaotefterminal.class);
	protected Set<Configuracaotefadquirente> listaConfiguracaotefadquirente = new ListSet<Configuracaotefadquirente>(Configuracaotefadquirente.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaotef")
	public Integer getCdconfiguracaotef() {
		return cdconfiguracaotef;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Modo de parcelamento")
	public TefParcelamentoAdmin getParcelamentoadmin() {
		return parcelamentoadmin;
	}
	
	@DisplayName("Terminais")
	@OneToMany(mappedBy="configuracaotef", fetch=FetchType.LAZY)
	public Set<Configuracaotefterminal> getListaConfiguracaotefterminal() {
		return listaConfiguracaotefterminal;
	}
	
	@DisplayName("Adquirentes")
	@OneToMany(mappedBy="configuracaotef", fetch=FetchType.LAZY)
	public Set<Configuracaotefadquirente> getListaConfiguracaotefadquirente() {
		return listaConfiguracaotefadquirente;
	}
	
	@DisplayName("Obrigar pagamento de cart�o NF-e")
	public Boolean getObrigarpagamentocartaonfe() {
		return obrigarpagamentocartaonfe;
	}

	@DisplayName("Obrigar pagamento de cart�o NFC-e")
	public Boolean getObrigarpagamentocartaonfce() {
		return obrigarpagamentocartaonfce;
	}
	
	@DisplayName("Chave de integra��o")
	public String getChaveintegracao() {
		return chaveintegracao;
	}
	
	@DisplayName("Impress�o de comprovante autom�tico")
	public Boolean getImpressaoautomatica() {
		return impressaoautomatica;
	}
	
	public void setImpressaoautomatica(Boolean impressaoautomatica) {
		this.impressaoautomatica = impressaoautomatica;
	}
	
	public void setListaConfiguracaotefadquirente(
			Set<Configuracaotefadquirente> listaConfiguracaotefadquirente) {
		this.listaConfiguracaotefadquirente = listaConfiguracaotefadquirente;
	}
	
	public void setChaveintegracao(String chaveintegracao) {
		this.chaveintegracao = chaveintegracao;
	}

	public void setObrigarpagamentocartaonfe(Boolean obrigarpagamentocartaonfe) {
		this.obrigarpagamentocartaonfe = obrigarpagamentocartaonfe;
	}

	public void setObrigarpagamentocartaonfce(Boolean obrigarpagamentocartaonfce) {
		this.obrigarpagamentocartaonfce = obrigarpagamentocartaonfce;
	}

	public void setListaConfiguracaotefterminal(
			Set<Configuracaotefterminal> listaConfiguracaotefterminal) {
		this.listaConfiguracaotefterminal = listaConfiguracaotefterminal;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdconfiguracaotef(Integer cdconfiguracaotef) {
		this.cdconfiguracaotef = cdconfiguracaotef;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setParcelamentoadmin(TefParcelamentoAdmin parcelamentoadmin) {
		this.parcelamentoadmin = parcelamentoadmin;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}
