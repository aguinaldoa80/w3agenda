package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_entregadocumentofrete", sequenceName="sq_entregadocumentofrete")
public class Entregadocumentofrete {
	
	protected Integer cdentregadocumentofrete;
	protected Entregadocumento entregadocumento;
	protected Entregadocumento entregadocumentovinculo;
	
	public Entregadocumentofrete(){
	}
	
	public Entregadocumentofrete(Entregadocumento entregadocumentovinculo){
		this.entregadocumentovinculo = entregadocumentovinculo;
	}
	
	public Entregadocumentofrete(Entregadocumento entregadocumento, Entregadocumento entregadocumentovinculo){
		this.entregadocumento = entregadocumento;
		this.entregadocumentovinculo = entregadocumentovinculo;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentofrete")
	public Integer getCdentregadocumentofrete() {
		return cdentregadocumentofrete;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumentovinculo")
	public Entregadocumento getEntregadocumentovinculo() {
		return entregadocumentovinculo;
	}
	
	public void setCdentregadocumentofrete(Integer cdentregadocumentofrete) {
		this.cdentregadocumentofrete = cdentregadocumentofrete;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setEntregadocumentovinculo(Entregadocumento entregadocumentovinculo) {
		this.entregadocumentovinculo = entregadocumentovinculo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Entregadocumentofrete) {
			Entregadocumentofrete entregadocumentofrete = (Entregadocumentofrete) obj;
			return entregadocumentofrete.getCdentregadocumentofrete().equals(this.getCdentregadocumentofrete());
		}
		return super.equals(obj);
	}
}