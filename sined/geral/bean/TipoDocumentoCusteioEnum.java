package br.com.linkcom.sined.geral.bean;

public enum TipoDocumentoCusteioEnum {
	
	RECEBER		(0,"Conta a receber"),
	PAGAR		(1,"Conta a pagar"),
	FINANCEIRA	(2,"Movimentação financeira"),
	ESTOQUE		(3,"Movimentação de estoque");

	private String nome;
	private Integer valor; 
	private TipoDocumentoCusteioEnum(Integer valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return getNome().toUpperCase();
	}
	
}
