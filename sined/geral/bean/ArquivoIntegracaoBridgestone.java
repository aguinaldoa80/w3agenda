package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.TipoArquivoIntegracaoBridgestone;

@Entity
@SequenceGenerator(name = "sq_arquivointegracaobridgestone", sequenceName = "sq_arquivointegracaobridgestone")
public class ArquivoIntegracaoBridgestone {
	
	private Integer cdarquivointegracaobridgestone;
	private TipoArquivoIntegracaoBridgestone tipo;
	private Boolean processada;
	private Date dtCriacao;
	private Arquivo arquivo;
	private Nota nota;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy hh:mm");
	protected NotaFiscalServico notaFiscalServico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivointegracaobridgestone")
	public Integer getCdarquivointegracaobridgestone() {
		return cdarquivointegracaobridgestone;
	}
	public TipoArquivoIntegracaoBridgestone getTipo() {
		return tipo;
	}
	public Boolean getProcessada() {
		return processada;
	}
	@DisplayName("Data de cria��o")
	public Date getDtCriacao() {
		return dtCriacao;
	}
	@ManyToOne
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnota")
	public Nota getNota() {
		return nota;
	}
	public void setCdarquivointegracaobridgestone(
			Integer cdarquivointegracaobridgestone) {
		this.cdarquivointegracaobridgestone = cdarquivointegracaobridgestone;
	}
	public void setTipo(TipoArquivoIntegracaoBridgestone tipo) {
		this.tipo = tipo;
	}
	public void setProcessada(Boolean processada) {
		this.processada = processada;
	}
	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	@Transient
	@DisplayName("Data de cria��o")
	public String getDataStr(){
		return sdf.format(getDtCriacao());
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota", insertable=false, updatable=false)
	public NotaFiscalServico getNotaFiscalServico() {
		return notaFiscalServico;
	}
	public void setNotaFiscalServico(NotaFiscalServico notaFiscalServico) {
		this.notaFiscalServico = notaFiscalServico;
	}
}
