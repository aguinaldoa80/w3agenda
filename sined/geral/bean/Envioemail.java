package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_envioemail", sequenceName = "sq_envioemail")
public class Envioemail implements Log {

	protected Integer cdenvioemail;
	protected Timestamp dtenvio;
	protected String remetente;
	protected String assunto;
	protected String mensagem;
	protected Envioemailtipo envioemailtipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Envioemailitem> listaEnvioemailitem;
	protected EmailStatusEnum situacaoemail;
	protected Timestamp ultimaverificacao;

	public Envioemail() {
	}

	public Envioemail(Integer cdenvioemail) {
		this.cdenvioemail = cdenvioemail;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_envioemail")
	@DisplayName("Id")
	public Integer getCdenvioemail() {
		return cdenvioemail;
	}

	@DisplayName("Data de envio")
	public Timestamp getDtenvio() {
		return dtenvio;
	}

	public String getRemetente() {
		return remetente;
	}

	public String getAssunto() {
		return assunto;
	}

	public String getMensagem() {
		return mensagem;
	}

	@DisplayName("Tipo de envio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdenvioemailtipo")
	public Envioemailtipo getEnvioemailtipo() {
		return envioemailtipo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@OneToMany(mappedBy = "envioemail")
	public List<Envioemailitem> getListaEnvioemailitem() {
		return listaEnvioemailitem;
	}

	public void setCdenvioemail(Integer cdenvioemail) {
		this.cdenvioemail = cdenvioemail;
	}

	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setEnvioemailtipo(Envioemailtipo envioemailtipo) {
		this.envioemailtipo = envioemailtipo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaEnvioemailitem(List<Envioemailitem> listaEnvioemailitem) {
		this.listaEnvioemailitem = listaEnvioemailitem;
	}
	
	@Transient
	@DisplayName("E-mail do destinat�rio")
	public String getEmaildestinatario(){
		StringBuilder emails = new StringBuilder();
		if(this.getListaEnvioemailitem() != null && !this.getListaEnvioemailitem().isEmpty()){
			for(Envioemailitem envioemailitem : this.getListaEnvioemailitem()){
				if(envioemailitem.getEmail() != null && !"".equals(envioemailitem.getEmail())){
					if(!emails.toString().equals("")) emails.append(" | ");
					emails.append(envioemailitem.getEmail());
				}
			}
		}
		return emails.toString();
	}
	
	@Transient
	public EmailStatusEnum getStatusemail(){
		List<EmailStatusEnum> listaStatus = new ArrayList<EmailStatusEnum>();
		if(this.getListaEnvioemailitem() != null && !this.getListaEnvioemailitem().isEmpty()){
			for(Envioemailitem envioemailitem : this.getListaEnvioemailitem()){
				if(envioemailitem.getSituacaoemail() != null &&
					!listaStatus.contains(envioemailitem.getSituacaoemail())){
					listaStatus.add(envioemailitem.getSituacaoemail());
				}
			}
		}
		EmailStatusEnum statusEmail = listaStatus.contains(EmailStatusEnum.EMAIL_INVALIDO)? EmailStatusEnum.EMAIL_INVALIDO:
										listaStatus.contains(EmailStatusEnum.CAIXA_DE_SPAM)? EmailStatusEnum.CAIXA_DE_SPAM:
											listaStatus.contains(EmailStatusEnum.LIDO)? EmailStatusEnum.LIDO:
												listaStatus.contains(EmailStatusEnum.ENTREGUE)? EmailStatusEnum.ENTREGUE:
													EmailStatusEnum.ENTREGUE;
		return statusEmail;
	}
	
	@DisplayName("Situa��o do e-mail")
	public EmailStatusEnum getSituacaoemail() {
		return situacaoemail;
	}
	public void setSituacaoemail(EmailStatusEnum situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
	
	@DisplayName("�ltima verifica��o")
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
}