package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Candidatosituacao {

	public static Candidatosituacao EM_ABERTO = new Candidatosituacao(1,"Em Aberto");
	public static Candidatosituacao ELIMINADO = new Candidatosituacao(2,"Eliminado");
	public static Candidatosituacao CONTRATADO = new Candidatosituacao(3, "Contratado");

	protected Integer cdcandidatosituacao;
	protected String descricao;
	

	
	public Candidatosituacao() {
	}
	
	public Candidatosituacao(Integer cdcandidatosituacao, String descricao){
		this.cdcandidatosituacao = cdcandidatosituacao;
		this.descricao = descricao;
	}
	
	@Id
	public Integer getCdcandidatosituacao() {
		return cdcandidatosituacao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	
	public void setCdcandidatosituacao(Integer cdcandidatosituacao) {
		this.cdcandidatosituacao = cdcandidatosituacao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}

