package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.types.Money;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Formapagamento {
	
	public static Formapagamento DINHEIRO = new Formapagamento(1,"Dinheiro");
	public static Formapagamento CHEQUE = new Formapagamento(2, "Cheque");
	public static Formapagamento CAIXA = new Formapagamento(3, "Caixa");
	public static Formapagamento CARTAOCREDITO = new Formapagamento(4, "Cart�o de cr�dito");
	public static Formapagamento DEBITOCONTACORRENTE = new Formapagamento(5, "D�bito em conta corrente");
	public static Formapagamento CREDITOCONTACORRENTE = new Formapagamento(6, "Cr�dito em conta corrente");
	public static Formapagamento VALECOMPRA = new Formapagamento(7, "Vale compra");
	public static Formapagamento ANTECIPACAO = new Formapagamento(8, "Adiantamento");
	
	protected Integer cdformapagamento;
	protected String nome;
	protected Boolean credito;
	protected Boolean debito;
	protected Boolean conciliacaoautomatica;
	
	protected Empresa empresa;
	protected String empresaWhereIn;

	protected Money valorTotal;
	
	public Formapagamento(){}
	
	public Formapagamento(Integer cdformapagamento){
		this.cdformapagamento = cdformapagamento;
	}
	
	public Formapagamento(Integer cdformapagamento, String nome) {
		this.cdformapagamento = cdformapagamento;
		this.nome = nome;
	}

	@Id
	public Integer getCdformapagamento() {
		return cdformapagamento;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Boolean getDebito() {
		return debito;
	}
	
	public Boolean getCredito() {
		return credito;
	}
	
	public Boolean getConciliacaoautomatica() {
		return conciliacaoautomatica;
	}
	
	public void setConciliacaoautomatica(Boolean conciliacaoautomatica) {
		this.conciliacaoautomatica = conciliacaoautomatica;
	}
	
	public void setCredito(Boolean credito) {
		this.credito = credito;
	}
	
	public void setDebito(Boolean debito) {
		this.debito = debito;
	}
	
	public void setCdformapagamento(Integer cdformapagamento) {
		this.cdformapagamento = cdformapagamento;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public String getEmpresaWhereIn() {
		return empresaWhereIn;
	}
	
	public void setEmpresaWhereIn(String empresaWhereIn) {
		this.empresaWhereIn = empresaWhereIn;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Formapagamento) {
			Formapagamento documentoacao = (Formapagamento) obj;
			return documentoacao.getCdformapagamento().equals(this.getCdformapagamento());
		}
		return super.equals(obj);
	}

	@Transient
	public Money getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}

	
	
	
}
