package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_arquivotefitem", sequenceName = "sq_arquivotefitem")
@DisplayName("Parcela")
public class Arquivotefitem {

	protected Integer cdarquivotefitem;
	protected Arquivotef arquivotef;
	protected Notafiscalprodutoduplicata notafiscalprodutoduplicata;
	protected Vendapagamento vendapagamento;
	
	// TRANSIENTE
	protected Boolean marcado = Boolean.TRUE;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivotefitem")
	public Integer getCdarquivotefitem() {
		return cdarquivotefitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivotef")
	public Arquivotef getArquivotef() {
		return arquivotef;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalprodutoduplicata")
	public Notafiscalprodutoduplicata getNotafiscalprodutoduplicata() {
		return notafiscalprodutoduplicata;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendapagamento")
	public Vendapagamento getVendapagamento() {
		return vendapagamento;
	}
	
	public void setVendapagamento(Vendapagamento vendapagamento) {
		this.vendapagamento = vendapagamento;
	}

	public void setCdarquivotefitem(Integer cdarquivotefitem) {
		this.cdarquivotefitem = cdarquivotefitem;
	}

	public void setArquivotef(Arquivotef arquivotef) {
		this.arquivotef = arquivotef;
	}

	public void setNotafiscalprodutoduplicata(
			Notafiscalprodutoduplicata notafiscalprodutoduplicata) {
		this.notafiscalprodutoduplicata = notafiscalprodutoduplicata;
	}

	@Transient
	public Boolean getMarcado() {
		return marcado;
	}

	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	
	
	
}
