package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_emporiumvendaarquivo", sequenceName = "sq_emporiumvendaarquivo")
public class Emporiumvendaarquivo {

	protected Integer cdemporiumvendaarquivo;
	protected Emporiumvenda emporiumvenda;
	protected Arquivoemporium arquivoemporium;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumvendaarquivo")
	public Integer getCdemporiumvendaarquivo() {
		return cdemporiumvendaarquivo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemporiumvenda")
	public Emporiumvenda getEmporiumvenda() {
		return emporiumvenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoemporium")
	public Arquivoemporium getArquivoemporium() {
		return arquivoemporium;
	}
	
	public void setArquivoemporium(Arquivoemporium arquivoemporium) {
		this.arquivoemporium = arquivoemporium;
	}

	public void setCdemporiumvendaarquivo(Integer cdemporiumvendaarquivo) {
		this.cdemporiumvendaarquivo = cdemporiumvendaarquivo;
	}

	public void setEmporiumvenda(Emporiumvenda emporiumvenda) {
		this.emporiumvenda = emporiumvenda;
	}

}
