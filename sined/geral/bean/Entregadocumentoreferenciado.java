package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Documentoreferenciadoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;


@Entity
@SequenceGenerator(name = "sq_entregadocumentoreferenciado", sequenceName = "sq_entregadocumentoreferenciado")
public class Entregadocumentoreferenciado{

	protected Integer cdentregadocumentoreferenciado;
	protected Entregadocumento entregadocumento;
	protected String identificadorinternoentregamaterial;
	protected ModelodocumentoEnum modelodocumento;
	protected Documentoreferenciadoimposto imposto;
	protected Uf uf;
	protected String numero;
	protected String autenticacaobancaria;
	protected Money valor;
	protected Date dtvencimento;
	protected Date dtpagamento;
	
	//Transientes
	protected Boolean inserircontapagar;
	protected Documentotipo documentotipo;
	protected Fornecedor fornecedor;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Boolean contapaga;
	protected Formapagamento formapagamento;
	protected Conta vinculo;
	protected Documento documento;
	
	public Entregadocumentoreferenciado(){}
	
	public Entregadocumentoreferenciado(Documentoreferenciado documentoreferenciado){
		this.identificadorinternoentregamaterial = documentoreferenciado.getIdentificadorinternoentregamaterial();
		this.modelodocumento = documentoreferenciado.getModelodocumento();
		this.imposto = documentoreferenciado.getImposto();
		this.valor = documentoreferenciado.getValor();
		this.dtvencimento = documentoreferenciado.getDatavencimento();
		this.documentotipo = documentoreferenciado.getDocumentotipo();
		this.fornecedor = documentoreferenciado.getFornecedor();
		this.contagerencial = documentoreferenciado.getContagerencial();
		this.centrocusto = documentoreferenciado.getCentrocusto();
		this.projeto = documentoreferenciado.getProjeto();
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentoreferenciado")
	public Integer getCdentregadocumentoreferenciado() {
		return cdentregadocumentoreferenciado;
	}
	public void setCdentregadocumentoreferenciado(
			Integer cdentregadocumentoreferenciado) {
		this.cdentregadocumentoreferenciado = cdentregadocumentoreferenciado;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	
	public String getIdentificadorinternoentregamaterial() {
		return identificadorinternoentregamaterial;
	}
	
	public void setIdentificadorinternoentregamaterial(String identificadorinternoentregamaterial) {
		this.identificadorinternoentregamaterial = identificadorinternoentregamaterial;
	}
	
	@Required
	@DisplayName("Modelo do documento")
	public ModelodocumentoEnum getModelodocumento() {
		return modelodocumento;
	}
	public void setModelodocumento(ModelodocumentoEnum modelodocumento) {
		this.modelodocumento = modelodocumento;
	}
	
	public Documentoreferenciadoimposto getImposto() {
		return imposto;
	}
	public void setImposto(Documentoreferenciadoimposto imposto) {
		this.imposto = imposto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	@MaxLength(20)
	@DisplayName("N�mero do documento de arrecada��o")
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@MaxLength(50)
	@DisplayName("Autentica��o banc�ria")
	public String getAutenticacaobancaria() {
		return autenticacaobancaria;
	}
	public void setAutenticacaobancaria(String autenticacaobancaria) {
		this.autenticacaobancaria = autenticacaobancaria;
	}
	
	@Required
	public Money getValor() {
		return valor;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Required
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	
	@DisplayName("Data de pagamento")
	public Date getDtpagamento() {
		return dtpagamento;
	}
	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}
	
	//Trasientes
	@Transient
	@DisplayName("Inserir conta a pagar")
	public Boolean getInserircontapagar() {
		return inserircontapagar;
	}
	public void setInserircontapagar(Boolean inserircontapagar) {
		this.inserircontapagar = inserircontapagar;
	}
	
	@Transient
	@DisplayName("Tipo de documento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	@Transient
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	@Transient
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	@Transient
	@DisplayName("Esta conta j� foi paga?")
	public Boolean getContapaga() {
		return contapaga;
	}
	public void setContapaga(Boolean contapaga) {
		this.contapaga = contapaga;
	}
	
	@Transient
	@DisplayName("Forma de pagamento")
	public Formapagamento getFormapagamento() {
		return formapagamento;
	}
	public void setFormapagamento(Formapagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	
	@Transient
	public Conta getVinculo() {
		return vinculo;
	}
	public void setVinculo(Conta vinculo) {
		this.vinculo = vinculo;
	}
	
	@Transient
	public Documento getDocumento() {
		return documento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
}