package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_usuarioprojeto", sequenceName = "sq_usuarioprojeto")
public class Usuarioprojeto{

	protected Integer cdusuarioprojeto;
	protected Usuario usuario;
	protected Projeto projeto;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_usuarioprojeto")
	public Integer getCdusuarioprojeto() {
		return cdusuarioprojeto;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@Required
	public Usuario getUsuario() {
		return usuario;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setCdusuarioprojeto(Integer cdusuarioprojeto) {
		this.cdusuarioprojeto = cdusuarioprojeto;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

}
