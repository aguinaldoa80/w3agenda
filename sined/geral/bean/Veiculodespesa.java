package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Despesa do ve�culo")
@SequenceGenerator(name = "sq_veiculodespesa", sequenceName = "sq_veiculodespesa")
public class Veiculodespesa implements Log{
	
	protected Integer cdveiculodespesa;
	protected Veiculodespesatipo veiculodespesatipo;
	protected Veiculo veiculo;
	protected String motivo;
	protected Integer km;
	protected Date dtentrada;
	protected Money valortotal;
	protected Fornecedor fornecedor;
	protected Empresa empresa;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Boolean baixado; //O label Baixado foi renomeado para Faturado.
	protected Boolean faturado; //O campo FATURADO foi criado pois o label Baixado foi renomeado para Faturado. Da� foi criado um UPDATE setando FATURADO=BAIXADO
	protected Colaborador colaborador;
	protected Double valorrealgasolina;
	protected Veiculodespesadefinirprojeto veiculodespesadefinirprojeto;
	
	protected List<Veiculodespesaitem> listaveiculodespesaitem = new ListSet<Veiculodespesaitem>(Veiculodespesaitem.class); 
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
//	TRANSIENT
	protected Double kmLitro;
	protected Integer kmAnterior;
	
	public Veiculodespesa(){
	}
	
	public Veiculodespesa(Integer cdveiculodespesa){
		this.cdveiculodespesa = cdveiculodespesa;
	}
	
	public Veiculodespesa(Integer cdveiculodespesa, Integer km_anterior){
		this.cdveiculodespesa = cdveiculodespesa;
		this.kmAnterior = km_anterior; 
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculodespesa")
	public Integer getCdveiculodespesa() {
		return cdveiculodespesa;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Required
	@DisplayName("Tipo de despesa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculodespesatipo")
	public Veiculodespesatipo getVeiculodespesatipo() {
		return veiculodespesatipo;
	}
	@Required
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	@Required
	@MaxLength(50)
	public String getMotivo() {
		return motivo;
	}
	@MaxLength(9)
	public Integer getKm() {
		return km;
	}
	@DisplayName("Data de entrada")
	public Date getDtentrada() {
		return dtentrada;
	}
	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Itens")
	@OneToMany(mappedBy="veiculodespesa")
	public List<Veiculodespesaitem> getListaveiculodespesaitem() {
		return listaveiculodespesaitem;
	}
	public Boolean getFaturado() {
		return faturado;
	}
	public Boolean getBaixado() {
		return baixado;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@DisplayName("Valor real gasolina")
	public Double getValorrealgasolina() {
		return valorrealgasolina;
	}
	@Required
	@DisplayName("Definir Projeto Por")
	public Veiculodespesadefinirprojeto getVeiculodespesadefinirprojeto() {
		return veiculodespesadefinirprojeto;
	}
	public void setValorrealgasolina(Double valorrealgasolina) {
		this.valorrealgasolina = valorrealgasolina;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setVeiculodespesatipo(Veiculodespesatipo veiculodespesatipo) {
		this.veiculodespesatipo = veiculodespesatipo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo.trim();
	}
	public void setKm(Integer km) {
		this.km = km;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setCdveiculodespesa(Integer cdveiculodespesa) {
		this.cdveiculodespesa = cdveiculodespesa;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setListaveiculodespesaitem(
			List<Veiculodespesaitem> listaveiculodespesaitem) {
		this.listaveiculodespesaitem = listaveiculodespesaitem;
	}
	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}
	public void setBaixado(Boolean baixado) {
		this.baixado = baixado;
	}
	public void setVeiculodespesadefinirprojeto(Veiculodespesadefinirprojeto veiculodespesadefinirprojeto) {
		this.veiculodespesadefinirprojeto = veiculodespesadefinirprojeto;
	}

	@Transient
	public Double getKmLitro() {
		return kmLitro;
	}

	public void setKmLitro(Double kmLitro) {
		this.kmLitro = kmLitro;
	}

	@Transient
	public Integer getKmAnterior() {
		return kmAnterior;
	}
	public void setKmAnterior(Integer kmAnterior) {
		this.kmAnterior = kmAnterior;
	}
}