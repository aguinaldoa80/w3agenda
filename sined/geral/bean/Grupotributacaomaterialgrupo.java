package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
@Entity
@SequenceGenerator(name="sq_grupotributacaomaterialgrupo",sequenceName="sq_grupotributacaomaterialgrupo")
public class Grupotributacaomaterialgrupo{
	
	protected Integer cdgrupotributacaomaterialgrupo;
	protected Grupotributacao grupotributacao;
	protected Materialgrupo materialgrupo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacaomaterialgrupo")
	public Integer getCdgrupotributacaomaterialgrupo() {
		return cdgrupotributacaomaterialgrupo;
	}
	@DisplayName("Grupo tributação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	public void setCdgrupotributacaomaterialgrupo(Integer cdgrupotributacaomaterialgrupo) {
		this.cdgrupotributacaomaterialgrupo = cdgrupotributacaomaterialgrupo;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
}
