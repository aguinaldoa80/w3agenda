package br.com.linkcom.sined.geral.bean;

/** 
 * Transfer Object utilizado para enviar
 * consistência de dados no processo de
 * Importação de Quilometragemdo Memphis
 * 
 * @author Rafael Odon de Alencar 
 */
public class ConsistenciaKmTO {
	
	private String placa;
	private String mensagem;
	private Boolean importado;
	
	public ConsistenciaKmTO(String placa, String mensagem, Boolean importado) {
		super();
		this.placa = placa;
		this.mensagem = mensagem;
		this.importado = importado;
	}

	public ConsistenciaKmTO() {
	}
	
	public String getPlaca() {
		return placa;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public Boolean getImportado() {
		return importado;
	}
	
	// -------- Setters -----------

	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public void setImportado(Boolean importado) {
		this.importado = importado;
	}
	
}
