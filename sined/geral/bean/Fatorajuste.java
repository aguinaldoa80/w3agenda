package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_fatorajuste", sequenceName="sq_fatorajuste")
@DisplayName("Fator de ajuste")
public class Fatorajuste implements Log{
	
	protected Integer cdfatorajuste;
	protected String sigla;
	protected String descricao;
	
	protected List<Fatorajustevalor> listaFatorajustevalor = new ListSet<Fatorajustevalor>(Fatorajustevalor.class); 
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	public Fatorajuste() {}	
	
	public Fatorajuste(Integer cdfatorajuste) {		
		this.cdfatorajuste = cdfatorajuste;
	}
	
	@DisplayName("Id")
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fatorajuste")
	public Integer getCdfatorajuste() {
		return cdfatorajuste;
	}	
	@DescriptionProperty
	@DisplayName("Sigla")
	@MaxLength(30)
	@Required
	public String getSigla() {
		return sigla;
	}
	@DisplayName("Descri��o")
	@MaxLength(80)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Valores")
	@OneToMany(mappedBy="fatorajuste")
	public List<Fatorajustevalor> getListaFatorajustevalor() {
		return listaFatorajustevalor;
	}
	
	public void setCdfatorajuste(Integer cdfatorajuste) {
		this.cdfatorajuste = cdfatorajuste;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setListaFatorajustevalor(
			List<Fatorajustevalor> listaFatorajustevalor) {
		this.listaFatorajustevalor = listaFatorajustevalor;
	}
	
	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
