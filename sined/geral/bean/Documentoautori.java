package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentoautori",sequenceName="sq_documentoautori")
public class Documentoautori implements Log{

	protected Integer cddocumentoautori;
	protected Documento documento;
	protected Date data;
	protected Money valor;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(generator="sq_documentoautori",strategy=GenerationType.AUTO)
	public Integer getCddocumentoautori() {
		return cddocumentoautori;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	
	public Date getData() {
		return data;
	}
	
	public String getObservacao() {
		return observacao;
	}
	
	public Money getValor() {
		return valor;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCddocumentoautori(Integer cddocumentoautori) {
		this.cddocumentoautori = cddocumentoautori;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
	//Log
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
