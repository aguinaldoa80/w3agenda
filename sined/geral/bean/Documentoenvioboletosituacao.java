package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_documentoenvioboletosituacao", sequenceName="sq_documentoenvioboletosituacao")
public class Documentoenvioboletosituacao {

	private Integer cddocumentoenvioboletosituacao;
	private Documentoenvioboleto documentoenvioboleto;
	private Documentoacao documentoacao;
	
	@Id
	@GeneratedValue(generator="sq_documentoenvioboletosituacao", strategy=GenerationType.AUTO)
	public Integer getCddocumentoenvioboletosituacao() {
		return cddocumentoenvioboletosituacao;
	}
	public void setCddocumentoenvioboletosituacao(
			Integer cddocumentoenvioboletosituacao) {
		this.cddocumentoenvioboletosituacao = cddocumentoenvioboletosituacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoenvioboleto")
	public Documentoenvioboleto getDocumentoenvioboleto() {
		return documentoenvioboleto;
	}
	public void setDocumentoenvioboleto(
			Documentoenvioboleto documentoenvioboleto) {
		this.documentoenvioboleto = documentoenvioboleto;
	}
	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}
	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}	
}
