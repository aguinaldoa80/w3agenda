package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_locacaomaterial", sequenceName = "sq_locacaomaterial")
public class Locacaomaterial implements Log, PermissaoFornecedorEmpresa {

	protected Integer cdlocacaomaterial;
	protected Fornecedor fornecedor;
	protected Bempatrimonio bempatrimonio;
	protected Integer qtde = 1;
	protected String numerooc;
	protected String numerocontrato;
	protected String numeropatrimonio;
	protected String marca;
	protected Money valorlocacao;
	protected Locacaomaterialtipo locacaomaterialtipo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Money valorequipamento;
	protected Boolean indenizacao = Boolean.FALSE;
	protected String descricaoindenizado;
	protected Money valorindenizacao;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_locacaomaterial")
	public Integer getCdlocacaomaterial() {
		return cdlocacaomaterial;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@Required
	@DisplayName("Material (Patrim�nio)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbempatrimonio")
	public Bempatrimonio getBempatrimonio() {
		return bempatrimonio;
	}
	
	@Required
	@DisplayName("Qtde.")
	@MaxLength(9)
	public Integer getQtde() {
		return qtde;
	}
	
	@MaxLength(30)
	@DisplayName("N� O.C.")
	public String getNumerooc() {
		return numerooc;
	}
	
	@MaxLength(30)
	@DisplayName("N� Contrato")
	public String getNumerocontrato() {
		return numerocontrato;
	}
	
	@MaxLength(30)
	@DisplayName("N� Patrim�nio")
	public String getNumeropatrimonio() {
		return numeropatrimonio;
	}
	
	@MaxLength(70)
	public String getMarca() {
		return marca;
	}
	
	@DisplayName("Valor de loca��o")
	@Required
	public Money getValorlocacao() {
		return valorlocacao;
	}
	
	@Required
	@DisplayName("Tipo de loca��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocacaomaterialtipo")
	public Locacaomaterialtipo getLocacaomaterialtipo() {
		return locacaomaterialtipo;
	}
	
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName("Data de t�rmino")
	public Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Valor do equipamento")
	public Money getValorequipamento() {
		return valorequipamento;
	}
	
	@DisplayName("Indeniza��o")
	public Boolean getIndenizacao() {
		return indenizacao;
	}
	
	@DisplayName("Descri��o do material indenizado")
	@MaxLength(150)
	public String getDescricaoindenizado() {
		return descricaoindenizado;
	}
	
	@DisplayName("Valor da indeniza��o")
	public Money getValorindenizacao() {
		return valorindenizacao;
	}
	
	public void setValorindenizacao(Money valorindenizacao) {
		this.valorindenizacao = valorindenizacao;
	}
	
	public void setDescricaoindenizado(String descricaoindenizado) {
		this.descricaoindenizado = descricaoindenizado;
	}
	
	public void setIndenizacao(Boolean indenizacao) {
		this.indenizacao = indenizacao;
	}
	
	public void setValorequipamento(Money valorequipamento) {
		this.valorequipamento = valorequipamento;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setLocacaomaterialtipo(Locacaomaterialtipo locacaomaterialtipo) {
		this.locacaomaterialtipo = locacaomaterialtipo;
	}
	
	public void setValorlocacao(Money valorlocacao) {
		this.valorlocacao = valorlocacao;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public void setNumeropatrimonio(String numeropatrimonio) {
		this.numeropatrimonio = numeropatrimonio;
	}
	
	public void setNumerocontrato(String numerocontrato) {
		this.numerocontrato = numerocontrato;
	}
	
	public void setNumerooc(String numerooc) {
		this.numerooc = numerooc;
	}
	
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
	
	public void setBempatrimonio(Bempatrimonio bempatrimonio) {
		this.bempatrimonio = bempatrimonio;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setCdlocacaomaterial(Integer cdlocacaomaterial) {
		this.cdlocacaomaterial = cdlocacaomaterial;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}	
	
	public String subQueryFornecedorEmpresa() {
		return "select locacaomaterialSubQueryFornecedorEmpresa.cdlocacaomaterial " +
				"from Locacaomaterial locacaomaterialSubQueryFornecedorEmpresa " +
				"left outer join locacaomaterialSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
}
