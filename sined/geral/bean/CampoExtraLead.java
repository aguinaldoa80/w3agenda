package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_campoextralead", sequenceName = "sq_campoextralead")
public class CampoExtraLead {

	protected Integer cdcampoextralead;
	protected Lead lead;
	protected CampoExtraSegmento campoExtraSegmento;
	protected String valor;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campoextralead")
	public Integer getCdcampoextralead() {
		return cdcampoextralead;
	}

	public void setCdcampoextralead(Integer cdcampoextralead) {
		this.cdcampoextralead = cdcampoextralead;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdlead")
	public Lead getLead() {
		return lead;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcampoextrasegmento")
	public CampoExtraSegmento getCampoExtraSegmento() {
		return campoExtraSegmento;
	}

	public void setCampoExtraSegmento(CampoExtraSegmento campoExtraSegmento) {
		this.campoExtraSegmento = campoExtraSegmento;
	}

	@MaxLength(8000)
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
