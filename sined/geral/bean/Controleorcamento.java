package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipolancamento;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@DisplayName("Or�amento")
@Entity
@SequenceGenerator(name = "sq_controleorcamento", sequenceName = "sq_controleorcamento")
public class Controleorcamento implements Log{

	
	//variaveis
	protected Integer cdcontroleorcamento;
	protected Empresa empresa;
//	protected Integer exercicio;
	protected String descricao;
	protected Tipolancamento tipolancamento;
	protected Controleorcamentosituacao situacao;
	protected List<Controleorcamentoitem> listaControleorcamentoitem = new ListSet<Controleorcamentoitem>(Controleorcamentoitem.class);
	protected List<ControleOrcamentoHistorico> listacontroleorcamentohistorico = new ListSet<ControleOrcamentoHistorico>(ControleOrcamentoHistorico.class);;
	
	protected Projeto projeto;
	protected Projetotipo projetoTipo;
	protected Date dtExercicioInicio;
	protected Date dtExercicioFim;
	
	public Controleorcamento(Integer cdControOrcamento) {
		this.cdcontroleorcamento = cdControOrcamento;
	}
	public Controleorcamento(){}
	//LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENT
	protected String situacaoTransient;
	protected String contagerencialTransient;
	protected String valorTransient;
	private Integer  cdReplanejado;
	private Tipooperacao tipoOperacao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_controleorcamento")
	public Integer getCdcontroleorcamento() {
		return cdcontroleorcamento;
	}
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Item")
	@OneToMany(mappedBy="controleorcamento")
	public List<Controleorcamentoitem> getListaControleorcamentoitem() {
		return listaControleorcamentoitem;
	}	
	@Required
	@DisplayName("Situa��o")
	public Controleorcamentosituacao getSituacao() {
		return situacao;
	}
	@Required
	@DisplayName("Tipo de Lan�amento")
	public Tipolancamento getTipolancamento() {
		return tipolancamento;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="controleorcamento")
	public List<ControleOrcamentoHistorico> getListacontroleorcamentohistorico() {
		return listacontroleorcamentohistorico;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Tipo De Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetotipo")
	public Projetotipo getProjetoTipo() {
		return projetoTipo;
	}
	@DisplayName("Inicio do Exercicio")
	@Required
	public Date getDtExercicioInicio() {
		return dtExercicioInicio;
	}
	@DisplayName("Fim do Exercicio")
	@Required
	public Date getDtExercicioFim() {
		return dtExercicioFim;
	}
	//////////////////////////////////////////////|
	//  INICIO SET						 /////////|
	//////////////////////////////////////////////|
	
	public void setTipolancamento(Tipolancamento tipolancamento) {
		this.tipolancamento = tipolancamento;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setProjetoTipo(Projetotipo projetoTipo) {
		this.projetoTipo = projetoTipo;
	}
	public void setDtExercicioInicio(Date dtExercicioInicio) {
		this.dtExercicioInicio = dtExercicioInicio;
	}
	public void setDtExercicioFim(Date dtExercicioFim) {
		this.dtExercicioFim = dtExercicioFim != null ? SinedDateUtils.lastDateOfMonth(dtExercicioFim) : null;
	}
	public void setCdcontroleorcamento(Integer cdcontroleorcamento) {
		this.cdcontroleorcamento = cdcontroleorcamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setListaControleorcamentoitem(
			List<Controleorcamentoitem> listaControleorcamentoitem) {
		this.listaControleorcamentoitem = listaControleorcamentoitem;
	}
	public void setSituacao(
			Controleorcamentosituacao situacao) {
		this.situacao = situacao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListacontroleorcamentohistorico(
			List<ControleOrcamentoHistorico> listacontroleorcamentohistorico) {
		this.listacontroleorcamentohistorico = listacontroleorcamentohistorico;
	}
	
	
	//////////////////////////////////////////////|
	//  INICIO DOS TRANSIENTES	E OUTROS /////////|
	//////////////////////////////////////////////|
	
	@Transient
	public Tipooperacao getTipoOperacao() {
		return tipoOperacao;
	}
	public void setTipoOperacao(Tipooperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	
	@Transient
	@DisplayName("Conta Gerencial")
	public String getContagerencialTransient() {		
		String listAndConcatenateSort = SinedUtil.listAndConcatenateSort(getListaControleorcamentoitem(), "contagerencial.descricaoCombo", "/");		
		return listAndConcatenateSort;
	}
	
	@Transient
	@DisplayName("Valor")
	public String getValorTransient() {
		StringBuilder s = new StringBuilder();
		if(getListaControleorcamentoitem() != null && !getListaControleorcamentoitem().isEmpty()){
			for(Controleorcamentoitem controleorcamentoitem : getListaControleorcamentoitem()){				
				if(controleorcamentoitem.getValor() != null){
					if(!s.toString().equals(""))
						s.append(" / " + controleorcamentoitem.getValor());
					else
						s.append(controleorcamentoitem.getValor());
				}
			}
		}
		return s.toString();
	}
	@Transient
	public Integer getCdReplanejado() {
		return cdReplanejado;
	}
	public void setCdReplanejado(Integer cdReplanejado) {
		this.cdReplanejado = cdReplanejado;
	}
	public void setContagerencialTransient(String contagerencialTransient) {
		this.contagerencialTransient = contagerencialTransient;
	}
	public void setValorTransient(String valorTransient) {
		this.valorTransient = valorTransient;
	}
	@Transient
	public String getSituacaoTransient() {
		return situacaoTransient;
	}
	@Transient
	@DescriptionProperty
	public String getDescricaoClasse() {
		if(this.getDtOrcamento()!= null && this.getDescricao()!=null && this.getSituacao()!=null){
			return  this.getDtOrcamento() + " - " + this.getDescricao() +" - "+ this.getSituacao().getNome();
		}else if (this.getDtOrcamento()!= null){
			return this.getDtOrcamento() + " - "  + this.getSituacao().getNome();
		}else{
			return this.getDescricao() +" - "+ this.getSituacao().getNome();
		}
	}
	public void setSituacaoTransient(String situacaoTransient) {
		this.situacaoTransient = situacaoTransient;
	}
	@DisplayName("In�cio do Exerc�cio")
	@Transient
	@Required
	public String getDtInicio() {
		if(this.dtExercicioInicio != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtExercicioInicio);
		return null;
	}
	public void setDtInicio(String dtInicio) {
		if(dtInicio!=null){
			try {
				this.dtExercicioInicio = new Date(new SimpleDateFormat("MM/yyyy").parse(dtInicio).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}
	}
	@DisplayName("Fim do Exerc�cio")
	@Transient
	@Required
	public String getDtFim() {
		if(this.dtExercicioFim != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtExercicioFim);
		return null;
	}
	public void setDtFim(String dtFim) {
		if(dtFim!=null){
			try {
				this.dtExercicioFim = new Date(new SimpleDateFormat("MM/yyyy").parse(dtFim).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}
	}
	@DisplayName("Per�odo do Exerc�cio")
	@Transient
	public String getDtOrcamento() {
		return this.getDtInicio()+" at� "+this.getDtFim();
	}
	@Transient
	public Integer getQtnMesesExercicio(){
		final double MES_EM_MILISEGUNDOS = 30.0 * 24.0 * 60.0 * 60.0 * 1000.0;
		Double valor = (double)(this.getDtExercicioFim().getTime() - this.getDtExercicioInicio().getTime())/MES_EM_MILISEGUNDOS ;
		
		Integer diaDoMesInicio = Integer.parseInt(this.getDtExercicioInicio().toString().split("-")[2]);
		Integer diaDoMesFim = Integer.parseInt(this.getDtExercicioFim().toString().split("-")[2]);
		
		if(diaDoMesFim <= diaDoMesInicio)
			return valor.intValue() + 1;  
		else
			return valor.intValue();
		
	}
}
