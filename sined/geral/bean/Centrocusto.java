package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_centrocusto", sequenceName = "sq_centrocusto")
public class Centrocusto implements Log{

	protected Integer cdcentrocusto;
	protected String nome;
	protected Boolean ativo;
	protected String codigoalternativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Boolean participarCusteio;
	
	
	//Transiente
	protected Money valorTotal;
	protected Money valorTotalDebito = new Money(0.0);
	protected Money valorTotalCredito = new Money(0.0);
	protected Money valorSaldoInicial = new Money(0.0);
	protected Money valorSaldoTotal = new Money(0.0);
 

	
	public Centrocusto() {}
	
	public Centrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	
	public Centrocusto(String nome) {
		this.nome = nome;
	}
	
	public Centrocusto(Integer cdcentrocusto, String nome) {
		this.cdcentrocusto = cdcentrocusto;
		this.nome = nome;
	}
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_centrocusto")
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}


	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}



	@DisplayName("C�digo Alternativo")
	@MaxLength(20)
	public String getCodigoalternativo() {
		return codigoalternativo;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Participar Custeio")
	public Boolean getParticiparCusteio() {
		return participarCusteio;
	}
	
	// set
	
	public void setParticiparCusteio(Boolean participarCusteio) {
		this.participarCusteio = participarCusteio;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setCdcentrocusto(Integer id) {
		this.cdcentrocusto = id;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCodigoalternativo(String codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	//Transientes e outros 

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcentrocusto == null) ? 0 : cdcentrocusto.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Centrocusto other = (Centrocusto) obj;
		if (cdcentrocusto == null) {
			if (other.cdcentrocusto != null)
				return false;
		} else if (!cdcentrocusto.equals(other.cdcentrocusto))
			return false;
		return true;
	}
	
		
	@Transient
	public Money getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}

	@Transient
	public Money getValorTotalDebito() {
		if(valorTotalDebito == null){
			valorTotalDebito = new Money(0);
		}
		return valorTotalDebito;
	}

	@Transient
	public Money getValorTotalCredito() {
		if(valorTotalCredito == null){
			valorTotalCredito = new Money(0);
		}
		return valorTotalCredito;
	}
	
	@Transient
	public Money getValorSaldoInicial() {
		if(valorSaldoInicial == null){
			valorSaldoInicial = new Money(0);
		}
		return valorSaldoInicial;
	}
	
	@Transient
	public Money getValorSaldoTotal() {
		valorSaldoTotal = getValorTotalCredito().subtract(getValorTotalDebito()).add(getValorSaldoInicial());
		return valorSaldoTotal;
	}
	
	public void setValorTotalDebito(Money valorTotalDebito) {
		this.valorTotalDebito = valorTotalDebito;
	}

	public void setValorTotalCredito(Money valorTotalCredito) {
		this.valorTotalCredito = valorTotalCredito;
	}
	
	public void setValorSaldoInicial(Money valorSaldoInicial) {
		this.valorSaldoInicial = valorSaldoInicial;
	}
	
	public void setValorSaldoTotal(Money valorSaldoTotal) {
		this.valorSaldoTotal = valorSaldoTotal;
	}
}
