package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_valorplanoassistencial", sequenceName = "sq_valorplanoassistencial")
public class Valorplanoassistencial 
{
	protected Integer cdvalorplanoassistencial;
	protected Integer idadeinicio;
	protected Integer idadefim;
	protected Double valor;
	protected Planoassistencial planoassistencial;
	
	public Valorplanoassistencial()
	{
		
	}
	
	public Valorplanoassistencial(Integer cdvalorplanoassistencial)
	{
		this.cdvalorplanoassistencial= cdvalorplanoassistencial; 
	}
	
	
	@Id
	public Integer getCdvalorplanoassistencial() {
		return cdvalorplanoassistencial;
	}
	
	@Required
	@DisplayName("Idade de in�cio")
	public Integer getIdadeinicio() {
		return idadeinicio;
	}
	
	@Required
	@DisplayName("Idade final")
	public Integer getIdadefim() {
		return idadefim;
	}
	
	@Required
	public Double getValor() {
		return valor;
	}
	
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdplanoassistencial")
	public Planoassistencial getPlanoassistencial() {
		return planoassistencial;
	}
	
	public void setCdvalorplanoassistencial(Integer cdvalorplanoassistencial) {
		this.cdvalorplanoassistencial = cdvalorplanoassistencial;
	}
	public void setIdadeinicio(Integer idadeinicio) {
		this.idadeinicio = idadeinicio;
	}
	public void setIdadefim(Integer idadefim) {
		this.idadefim = idadefim;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPlanoassistencial(Planoassistencial planoassistencial) {
		this.planoassistencial = planoassistencial;
	}

}
