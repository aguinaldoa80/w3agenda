package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfelacreunidadecargamdfereferenciado", sequenceName = "sq_mdfelacreunidadecargamdfereferenciado")
public class MdfeLacreUnidadeCargaMdfeReferenciado {

	protected Integer cdMdfeLacreUnidadeCargaMdfeReferenciado;
	protected Mdfe mdfe;
	protected String numeroLacre;
	protected String idUnidadeCarga;
	protected String idLacre;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelacreunidadecargamdfereferenciado")
	public Integer getCdMdfeLacreUnidadeCargaMdfeReferenciado() {
		return cdMdfeLacreUnidadeCargaMdfeReferenciado;
	}
	public void setCdMdfeLacreUnidadeCargaMdfeReferenciado(Integer cdMdfeLacreUnidadeCarga) {
		this.cdMdfeLacreUnidadeCargaMdfeReferenciado = cdMdfeLacreUnidadeCarga;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("N�mero do lacre")
	public String getNumeroLacre() {
		return numeroLacre;
	}
	public void setNumeroLacre(String numeroLacre) {
		this.numeroLacre = numeroLacre;
	}
	
	@DisplayName("ID da unidade de carga")
	public String getIdUnidadeCarga() {
		return idUnidadeCarga;
	}
	public void setIdUnidadeCarga(String idUnidadeCarga) {
		this.idUnidadeCarga = idUnidadeCarga;
	}
	
	@DisplayName("ID do lacre")
	public String getIdLacre() {
		return idLacre;
	}
	public void setIdLacre(String idLacre) {
		this.idLacre = idLacre;
	}
}