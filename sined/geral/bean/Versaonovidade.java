package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@DisplayName("Novidade")
@SequenceGenerator(name = "sq_versaonovidade", sequenceName = "sq_versaonovidade")
public class Versaonovidade {

	protected Integer cdversaonovidade;
	protected Versao versao;
	protected String descricao;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_versaonovidade")
	public Integer getCdversaonovidade() {
		return cdversaonovidade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdversao")
	public Versao getVersao() {
		return versao;
	}
	@DisplayName("Descri��o")
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setVersao(Versao versao) {
		this.versao = versao;
	}
	public void setCdversaonovidade(Integer cdversaonovidade) {
		this.cdversaonovidade = cdversaonovidade;
	}
	
}
