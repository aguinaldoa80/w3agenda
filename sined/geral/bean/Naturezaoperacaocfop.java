package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_naturezaoperacaocfop", sequenceName = "sq_naturezaoperacaocfop")
public class Naturezaoperacaocfop {

	private Integer cdnaturezaoperacaocfop;
	private Naturezaoperacao naturezaoperacao;
	private Cfop cfop;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_naturezaoperacaocfop")
	public Integer getCdnaturezaoperacaocfop() {
		return cdnaturezaoperacaocfop;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	@DisplayName("CFOP")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfop")
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCdnaturezaoperacaocfop(Integer cdnaturezaoperacaocfop) {
		this.cdnaturezaoperacaocfop = cdnaturezaoperacaocfop;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}	
}
