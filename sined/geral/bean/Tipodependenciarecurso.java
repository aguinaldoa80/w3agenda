package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de dependÍncia entre recursos")
public class Tipodependenciarecurso {

	public static final Tipodependenciarecurso MOD = new Tipodependenciarecurso(1);
	public static final Tipodependenciarecurso SELECAO_CARGOS = new Tipodependenciarecurso(2);
	public static final Tipodependenciarecurso SEM_DEPENDENCIA = new Tipodependenciarecurso(3);	
	public static final Tipodependenciarecurso TODOS_CARGOS = new Tipodependenciarecurso(4);	
	
	protected Integer cdtipodependenciarecurso;
	protected String nome;
	
	public Tipodependenciarecurso() {
		
	}
	
	public Tipodependenciarecurso(Integer cdtipodependenciarecurso) {
		this.cdtipodependenciarecurso = cdtipodependenciarecurso;
	}
	
	@Id	
	public Integer getCdtipodependenciarecurso() {
		return cdtipodependenciarecurso;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdtipodependenciarecurso(Integer cdtipodependenciarecurso) {
		this.cdtipodependenciarecurso = cdtipodependenciarecurso;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tipodependenciarecurso){
			Tipodependenciarecurso that = (Tipodependenciarecurso) obj;
			return this.getCdtipodependenciarecurso().equals(that.getCdtipodependenciarecurso());
		}
		return super.equals(obj);
	}
}
