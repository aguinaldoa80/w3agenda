package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.ProducaoordemmaterialMaterialprimaPerdaBean;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.MateriaprimaConsumoProducaoBean;

@Entity
@SequenceGenerator(name = "sq_producaoordemmaterial", sequenceName = "sq_producaoordemmaterial")
public class Producaoordemmaterial implements Cloneable {
	
	protected Integer cdproducaoordemmaterial; 
	protected Producaoordem producaoordem;
	protected Material material;
	protected Double qtde;
	protected Double qtdeperdadescarte;
	protected Double qtdeproduzido;
	protected Producaoetapa producaoetapa;
	protected Producaoetapaitem producaoetapaitem;
	protected Pedidovendamaterial pedidovendamaterial;
	protected String observacao;
	protected Unidademedida unidademedida;
	protected Pneu pneu;
	protected String identificadorinterno;
	protected Double residuobanda;
	protected Garantiatipo garantiatipo;
	protected Garantiatipopercentual garantiatipopercentual;
	
	protected Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem = new ListSet<Producaoordemmaterialorigem>(Producaoordemmaterialorigem.class);
	protected Set<Producaoordemmaterialcampo> listaCampo = new ListSet<Producaoordemmaterialcampo>(Producaoordemmaterialcampo.class);
	protected Set<Producaoordemmaterialproduzido> listaProducaoordemmaterialproduzido = new ListSet<Producaoordemmaterialproduzido>(Producaoordemmaterialproduzido.class);
	protected Set<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima = new ListSet<Producaoordemmaterialmateriaprima>(Producaoordemmaterialmateriaprima.class);
	protected Set<Producaoordemequipamento> listaProducaoordemequipamento = new ListSet<Producaoordemequipamento>(Producaoordemequipamento.class);
	protected Set<Producaoordemmaterialoperador> listaProducaoordemmaterialoperador = new ListSet<Producaoordemmaterialoperador>(Producaoordemmaterialoperador.class);
	protected List<Producaoordemmaterialperdadescarte> listaProducaoordemmaterialperdadescarte;
	
	//TRANSIENTES
	protected Empresa empresa;
	protected String departamentosprevisoes;
	protected Double qtdeprevista;
	protected Double qtderegistro;
	protected Double qtderegistroperda;
	protected Departamento departamento;
	protected Date dtprevisaoentrega;
	protected Localarmazenagem localarmazenagem;
	protected Loteestoque loteestoque;
	protected Boolean ultimaetapa;
	protected Boolean haveColeta = Boolean.FALSE;
	protected Boolean haveCemobra = Boolean.FALSE;
	protected Producaoagendamaterial producaoagendamaterial;
	protected List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima;
	protected Boolean naoagrupamaterial;
	protected Boolean encontrado;
	protected Boolean concluido;
	protected Boolean considerarProducaoordem;
	protected Boolean baixarmateriaprimacascata;
	protected String whereInProducaoordem;
	protected String whereInProducaoordemmaterial;
	protected Double qtdeTotalProduzido;
	protected Localarmazenagem localbaixamateriaprima;
	protected Producaoagenda producaoagenda;
	protected Double qtdereferencia;
	protected Double quantidadedevolvida; 
	protected List<MateriaprimaConsumoProducaoBean> listaMateriaprima = new ListSet<MateriaprimaConsumoProducaoBean>(MateriaprimaConsumoProducaoBean.class);
	protected String whereInProducaoagendamaterial;
	protected String nomeBanda;
	protected String profundidadesulcoBanda;
	protected Double qtdeBaixaEstoque;
	protected List<ProducaoordemmaterialMaterialprimaPerdaBean> listaMateriaprimaperda;
	
	public Producaoordemmaterial() {}
	
	public Producaoordemmaterial(Integer cdproducaoordemmaterial) {
		this.cdproducaoordemmaterial = cdproducaoordemmaterial;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoordemmaterial")
	public Integer getCdproducaoordemmaterial() {
		return cdproducaoordemmaterial;
	}

	@JoinColumn(name="cdproducaoordem")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoordem getProducaoordem() {
		return producaoordem;
	}
	
	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}
	
	@DisplayName("Qtde. Perda/Descarte")
	public Double getQtdeperdadescarte() {
		return qtdeperdadescarte;
	}
	public void setQtdeperdadescarte(Double qtdeperdadescarte) {
		this.qtdeperdadescarte = qtdeperdadescarte;
	}

	@DisplayName("Produzido")
	public Double getQtdeproduzido() {
		return qtdeproduzido;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemmaterialorigem> getListaProducaoordemmaterialorigem() {
		return listaProducaoordemmaterialorigem;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemmaterialcampo> getListaCampo() {
		return listaCampo;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemmaterialmateriaprima> getListaProducaoordemmaterialmateriaprima() {
		return listaProducaoordemmaterialmateriaprima;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemmaterialproduzido> getListaProducaoordemmaterialproduzido() {
		return listaProducaoordemmaterialproduzido;
	}

	@JoinColumn(name="cdproducaoetapaitem")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoetapaitem getProducaoetapaitem() {
		return producaoetapaitem;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@JoinColumn(name="cdpedidovendamaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	@JoinColumn(name="cdproducaoetapa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}
	
	@DisplayName("U.M.")
	@JoinColumn(name="cdunidademedida")
	@ManyToOne(fetch=FetchType.LAZY)
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@JoinColumn(name="cdpneu")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pneu getPneu() {
		return pneu;
	}
	
	public void setListaProducaoordemmaterialproduzido(
			Set<Producaoordemmaterialproduzido> listaProducaoordemmaterialproduzido) {
		this.listaProducaoordemmaterialproduzido = listaProducaoordemmaterialproduzido;
	}
	
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setProducaoetapaitem(Producaoetapaitem producaoetapaitem) {
		this.producaoetapaitem = producaoetapaitem;
	}
	
	public void setListaProducaoordemmaterialorigem(
			Set<Producaoordemmaterialorigem> listaProducaoordemmaterialorigem) {
		this.listaProducaoordemmaterialorigem = listaProducaoordemmaterialorigem;
	}

	public void setListaCampo(Set<Producaoordemmaterialcampo> listaCampo) {
		this.listaCampo = listaCampo;
	}
	
	public void setListaProducaoordemmaterialmateriaprima(Set<Producaoordemmaterialmateriaprima> listaProducaoordemmaterialmateriaprima) {
		this.listaProducaoordemmaterialmateriaprima = listaProducaoordemmaterialmateriaprima;
	}

	public void setQtdeproduzido(Double qtdeproduzido) {
		this.qtdeproduzido = qtdeproduzido;
	}

	public void setCdproducaoordemmaterial(Integer cdproducaoordemmaterial) {
		this.cdproducaoordemmaterial = cdproducaoordemmaterial;
	}
	
	public void setProducaoordem(Producaoordem producaoordem) {
		this.producaoordem = producaoordem;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}

	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}

	// TRANSIENTE
	
	@Transient
	public String getDepartamentosprevisoes() {
		return departamentosprevisoes;
	}
	
	@Transient
	@DisplayName("Qtde. Prevista")
	public Double getQtdeprevista() {
		return qtdeprevista;
	}
	
	@Transient
	public Date getDtprevisaoentrega() {
		return dtprevisaoentrega;
	}
	
	@Transient
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@Transient
	public Double getQtderegistro() {
		return qtderegistro;
	}
	
	@Transient
	public Double getQtderegistroperda() {
		return qtderegistroperda;
	}

	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@Transient
	public Boolean getUltimaetapa() {
		return ultimaetapa;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Transient
	public Boolean getNaoagrupamaterial() {
		return naoagrupamaterial;
	}
	
	public void setNaoagrupamaterial(Boolean naoagrupamaterial) {
		this.naoagrupamaterial = naoagrupamaterial;
	}
	
	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setUltimaetapa(Boolean ultimaetapa) {
		this.ultimaetapa = ultimaetapa;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public void setQtderegistro(Double qtderegistro) {
		this.qtderegistro = qtderegistro;
	}
	
	public void setQtderegistroperda(Double qtderegistroperda) {
		this.qtderegistroperda = qtderegistroperda;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setDtprevisaoentrega(Date dtprevisaoentrega) {
		this.dtprevisaoentrega = dtprevisaoentrega;
	}
	
	public void setDepartamentosprevisoes(String departamentosprevisoes) {
		this.departamentosprevisoes = departamentosprevisoes;
	}
	
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	
	@Transient
	public Boolean getHaveColeta() {
		return haveColeta;
	}
	
	public void setHaveColeta(Boolean haveColeta) {
		this.haveColeta = haveColeta;
	}
	
	@Transient
	public Boolean getHaveCemobra() {
		return haveCemobra;
	}
	
	public void setHaveCemobra(Boolean haveCemobra) {
		this.haveCemobra = haveCemobra;
	}

	@Transient
	public List<Producaoagendamaterialmateriaprima> getListaProducaoagendamaterialmateriaprima() {
		return listaProducaoagendamaterialmateriaprima;
	}

	public void setListaProducaoagendamaterialmateriaprima(List<Producaoagendamaterialmateriaprima> listaProducaoagendamaterialmateriaprima) {
		this.listaProducaoagendamaterialmateriaprima = listaProducaoagendamaterialmateriaprima;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	@Transient
	public Producaoagendamaterial getProducaoagendamaterial() {
		return producaoagendamaterial;
	}
	
	public void setProducaoagendamaterial(
			Producaoagendamaterial producaoagendamaterial) {
		this.producaoagendamaterial = producaoagendamaterial;
	}

	@Transient
	public Boolean getEncontrado() {
		return encontrado;
	}

	public void setEncontrado(Boolean encontrado) {
		this.encontrado = encontrado;
	}

	@Transient
	public Boolean getConcluido() {
		return concluido;
	}

	public void setConcluido(Boolean concluido) {
		this.concluido = concluido;
	}

	@Transient
	public String getWhereInProducaoordem() {
		return whereInProducaoordem;
	}

	public void setWhereInProducaoordem(String whereInProducaoordem) {
		this.whereInProducaoordem = whereInProducaoordem;
	}
	
	@Transient
	public String getWhereInProducaoordemmaterial() {
		return whereInProducaoordemmaterial;
	}

	public void setWhereInProducaoordemmaterial(String whereInProducaoordemmaterial) {
		this.whereInProducaoordemmaterial = whereInProducaoordemmaterial;
	}

	@Transient
	public Double getQtdeTotalProduzido() {
		return qtdeTotalProduzido;
	}

	public void setQtdeTotalProduzido(Double qtdeTotalProduzido) {
		this.qtdeTotalProduzido = qtdeTotalProduzido;
	}

	@Transient
	public Boolean getConsiderarProducaoordem() {
		return considerarProducaoordem;
	}

	public void setConsiderarProducaoordem(Boolean considerarProducaoordem) {
		this.considerarProducaoordem = considerarProducaoordem;
	}

	@Transient
	public Boolean getBaixarmateriaprimacascata() {
		return baixarmateriaprimacascata;
	}

	public void setBaixarmateriaprimacascata(Boolean baixarmateriaprimacascata) {
		this.baixarmateriaprimacascata = baixarmateriaprimacascata;
	}

	@Transient
	public Localarmazenagem getLocalbaixamateriaprima() {
		return localbaixamateriaprima;
	}

	public void setLocalbaixamateriaprima(Localarmazenagem localbaixamateriaprima) {
		this.localbaixamateriaprima = localbaixamateriaprima;
	}

	@Transient
	public List<MateriaprimaConsumoProducaoBean> getListaMateriaprima() {
		return listaMateriaprima;
	}

	public void setListaMateriaprima(List<MateriaprimaConsumoProducaoBean> listaMateriaprima) {
		this.listaMateriaprima = listaMateriaprima;
	}

	@Transient
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}

	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	@Transient
	public Double getQuantidadedevolvida() {
		return quantidadedevolvida;
	}

	public void setQuantidadedevolvida(Double quantidadedevolvida) {
		this.quantidadedevolvida = quantidadedevolvida;
	}

	@Transient
	public String getWhereInProducaoagendamaterial() {
		return whereInProducaoagendamaterial;
	}

	public void setWhereInProducaoagendamaterial(String whereInProducaoagendamaterial) {
		this.whereInProducaoagendamaterial = whereInProducaoagendamaterial;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemequipamento> getListaProducaoordemequipamento() {
		return listaProducaoordemequipamento;
	}
	
	public void setListaProducaoordemequipamento(Set<Producaoordemequipamento> listaProducaoordemequipamento) {
		this.listaProducaoordemequipamento = listaProducaoordemequipamento;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public Set<Producaoordemmaterialoperador> getListaProducaoordemmaterialoperador() {
		return listaProducaoordemmaterialoperador;
	}
	
	public void setListaProducaoordemmaterialoperador(Set<Producaoordemmaterialoperador> listaProducaoordemmaterialoperador) {
		this.listaProducaoordemmaterialoperador = listaProducaoordemmaterialoperador;
	}
	
	@OneToMany(mappedBy="producaoordemmaterial")
	public List<Producaoordemmaterialperdadescarte> getListaProducaoordemmaterialperdadescarte() {
		return listaProducaoordemmaterialperdadescarte;
	}
	public void setListaProducaoordemmaterialperdadescarte(
			List<Producaoordemmaterialperdadescarte> listaProducaoordemmaterialperdadescarte) {
		this.listaProducaoordemmaterialperdadescarte = listaProducaoordemmaterialperdadescarte;
	}
	
	@Transient
	public Producaoordemmaterial getClone() {
		try {
			return (Producaoordemmaterial) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	@DisplayName("Res�duo da Banda")
	@MaxLength(9)
	public Double getResiduobanda() {
		return residuobanda;
	}

	public void setResiduobanda(Double residuobanda) {
		this.residuobanda = residuobanda;
	}
	
	@DisplayName("Tipo de Garantia")
	@JoinColumn(name="cdgarantiatipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Garantiatipo getGarantiatipo() {
		return garantiatipo;
	}

	public void setGarantiatipo(Garantiatipo garantiatipo) {
		this.garantiatipo = garantiatipo;
	}
	
	@DisplayName("% de Garantia")
	@JoinColumn(name="cdgarantiatipopercentual")
	@ManyToOne(fetch=FetchType.LAZY)
	public Garantiatipopercentual getGarantiatipopercentual() {
		return garantiatipopercentual;
	}

	public void setGarantiatipopercentual(Garantiatipopercentual garantiatipopercentual) {
		this.garantiatipopercentual = garantiatipopercentual;
	}
	
	@Transient
	public String getBanda(){
		if (Hibernate.isInitialized(pedidovendamaterial) 
				&& pedidovendamaterial!=null
				&& Hibernate.isInitialized(pedidovendamaterial.getPedidovendamaterialgarantido()) 
				&& pedidovendamaterial.getPedidovendamaterialgarantido()!=null){
			Set<Materialproducao> listaProducao = pedidovendamaterial.getPedidovendamaterialgarantido().getMaterial().getListaProducao();
			if (Hibernate.isInitialized(listaProducao) && listaProducao!=null){
				for (Materialproducao materialproducao: listaProducao){
					if (Boolean.TRUE.equals(materialproducao.getExibirvenda()) && materialproducao.getMaterial()!=null){
						return materialproducao.getMaterial().getNome();
					}
				}
			}
		}
		
		return "";
	}
	
	@Transient
	public String getProfundidadeSulco(){
		if (Hibernate.isInitialized(pedidovendamaterial) 
				&& pedidovendamaterial!=null
				&& Hibernate.isInitialized(pedidovendamaterial.getPedidovendamaterialgarantido()) 
				&& pedidovendamaterial.getPedidovendamaterialgarantido()!=null){
			Set<Materialproducao> listaProducao = pedidovendamaterial.getPedidovendamaterialgarantido().getMaterial().getListaProducao();
			if (Hibernate.isInitialized(listaProducao) && listaProducao!=null){
				for (Materialproducao materialproducao: listaProducao){
					if (Boolean.TRUE.equals(materialproducao.getExibirvenda()) 
							&& materialproducao.getMaterial()!=null
							&& materialproducao.getMaterial().getProfundidadesulco()!=null){
						return new DecimalFormat("###,##0.##").format(materialproducao.getMaterial().getProfundidadesulco());
					}
				}
			}
		}
		
		return "";
	}

	@Transient
	public Double getQtdeBaixaEstoque() {
		return qtdeBaixaEstoque;
	}

	public void setQtdeBaixaEstoque(Double qtdeBaixaEstoque) {
		this.qtdeBaixaEstoque = qtdeBaixaEstoque;
	}
	
	@Transient
	public List<ProducaoordemmaterialMaterialprimaPerdaBean> getListaMateriaprimaperda() {
		return listaMateriaprimaperda;
	}
	public void setListaMateriaprimaperda(
			List<ProducaoordemmaterialMaterialprimaPerdaBean> listaMateriaprimaperda) {
		this.listaMateriaprimaperda = listaMateriaprimaperda;
	}
	
}