package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name="sq_documentodigitaldestinatario",sequenceName="sq_documentodigitaldestinatario")
public class Documentodigitaldestinatario {

	private Integer cddocumentodigitaldestinatario;
	private Documentodigital documentodigital;
	private Documentodigitalusuario documentodigitalusuario;
	private Timestamp dtenvio;
	private String token;
	private Timestamp dtaceite;
	private String ipaceite;
	private String useragentaceite;
	
	// TRANSIENTE
	private String email;
	private String nome;
	private Cpf cpf;
	private Date dtnascimento;

	@Id
	@GeneratedValue(generator = "sq_documentodigitaldestinatario", strategy = GenerationType.AUTO)
	public Integer getCddocumentodigitaldestinatario() {
		return cddocumentodigitaldestinatario;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentodigital")
	public Documentodigital getDocumentodigital() {
		return documentodigital;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentodigitalusuario")
	public Documentodigitalusuario getDocumentodigitalusuario() {
		return documentodigitalusuario;
	}

	@DisplayName("Data/Hora de envio")
	public Timestamp getDtenvio() {
		return dtenvio;
	}
	
	@MaxLength(500)
	@DisplayName("Token")
	public String getToken() {
		return token;
	}

	@DisplayName("Data/Hora do aceite")
	public Timestamp getDtaceite() {
		return dtaceite;
	}

	@DisplayName("IP do aceite")
	public String getIpaceite() {
		return ipaceite;
	}

	@DisplayName("User-agent do aceite")
	public String getUseragentaceite() {
		return useragentaceite;
	}

	public void setCddocumentodigitaldestinatario(
			Integer cddocumentodigitaldestinatario) {
		this.cddocumentodigitaldestinatario = cddocumentodigitaldestinatario;
	}

	public void setDocumentodigital(Documentodigital documentodigital) {
		this.documentodigital = documentodigital;
	}

	public void setDocumentodigitalusuario(
			Documentodigitalusuario documentodigitalusuario) {
		this.documentodigitalusuario = documentodigitalusuario;
	}
	
	public void setDtenvio(Timestamp dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setDtaceite(Timestamp dtaceite) {
		this.dtaceite = dtaceite;
	}

	public void setIpaceite(String ipaceite) {
		this.ipaceite = ipaceite;
	}

	public void setUseragentaceite(String useragentaceite) {
		this.useragentaceite = useragentaceite;
	}

	@Transient
	public String getEmail() {
		return documentodigitalusuario != null ? documentodigitalusuario.getEmail() : email;
	}

	@Transient
	public String getNome() {
		return documentodigitalusuario != null ? documentodigitalusuario.getNome() : nome;
	}

	@Transient
	public Cpf getCpf() {
		return documentodigitalusuario != null ? documentodigitalusuario.getCpf() : cpf;
	}

	@Transient
	public Date getDtnascimento() {
		return documentodigitalusuario != null ? documentodigitalusuario.getDtnascimento() : dtnascimento;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	
	
}
