package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedException;


@Entity
@SequenceGenerator(name = "sq_arquivo", sequenceName = "sq_arquivo")
public class Arquivo implements File {

	protected Long cdarquivo;
	protected String nome;
	protected String tipoconteudo;
	protected Long tamanho;
	protected Timestamp dtmodificacao;
	protected byte[] content;
	protected Set<Clientearquivo> listaClientearquivo;

	public Arquivo() {}
	
	public Arquivo(Long cdarquivo) {
		this.cdarquivo = cdarquivo;
	}
	
	public Arquivo(byte[] byteArray, String nomearquivo, String contentType) {
		if (byteArray == null) throw new SinedException("O par�metro byteArray n�o pode ser null.");
		if (nomearquivo == null) throw new SinedException("O par�metro nomearquivo n�o pode ser null.");
		if (StringUtils.isEmpty(contentType)) throw new SinedException("O par�metro contentType n�o pode ser nulo.");
		
		this.setNome(nomearquivo);
		this.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
		this.setTamanho(Long.parseLong("" + byteArray.length));
		this.setContenttype(contentType);
		this.setContent(byteArray);
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivo")
	public Long getCdarquivo() {
		return cdarquivo;
	}
	public void setCdarquivo(Long id) {
		this.cdarquivo = id;
	}

	
	@Required
	@MaxLength(500)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@MaxLength(50)
	public String getTipoconteudo() {
		return tipoconteudo;
	}
	
	public Long getTamanho() {
		return tamanho;
	}
	
	public Timestamp getDtmodificacao() {
		return dtmodificacao;
	}
	
	@OneToMany(mappedBy="arquivo")
	public Set<Clientearquivo> getListaClientearquivo() {
		return listaClientearquivo;
	}

	public void setListaClientearquivo(Set<Clientearquivo> listaClientearquivo) {
		this.listaClientearquivo = listaClientearquivo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipoconteudo(String tipoconteudo) {
		this.tipoconteudo = tipoconteudo;
	}
	
	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}
	
	public void setDtmodificacao(Timestamp dtmodificacao) {
		this.dtmodificacao = dtmodificacao;
	}
	
// -------------------------------------------------------
	
	protected Arquivo arquivo;
	
	@Transient
	public byte[] getContent() {
		return this.content;
	}

	@Transient
	public String getName() {
		return getNome();
	}

	@Transient
	public Long getCdfile() {
		if(getCdarquivo() == null){
			return null;
		}
		return new Long(getCdarquivo().intValue());
	}

	@Transient
	public String getContenttype() {
		return getTipoconteudo();
	}

	@Transient
	public Long getSize() {
		return getTamanho();
	}

	@Transient
	public Timestamp getTsmodification() {
		return getDtmodificacao();
	}

	public void setContenttype(String contenttype) {
		setTipoconteudo(contenttype);
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setCdfile(Long cdfile) {
		setCdarquivo(cdfile);
	}

	public void setName(String name) {
		setNome(name);
	}

	public void setSize(Long size) {
		setTamanho(size);
	}

	public void setTsmodification(Timestamp tsmodification) {
		setDtmodificacao(tsmodification);
	}
	
	@Transient
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

}
