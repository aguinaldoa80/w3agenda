package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@DisplayName("Tipo de Pneu OTR")
@Entity
@SequenceGenerator(name = "sq_otrpneutipo", sequenceName = "sq_otrpneutipo")
public class OtrPneuTipo implements Log{
	protected Integer cdOtrPneuTipo;
	protected String nome;
	protected OtrConstrucao otrConstrucao;
	protected String codigoIntegracao;
	protected List<OtrPneuTipoOtrClassificacaoCodigo> listaOtrPneuTipoOtrClassificacaoCodigo;
	protected String codigoStr;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrpneutipo")
	public Integer getCdOtrPneuTipo() {
		return cdOtrPneuTipo;
	}
	public void setCdOtrPneuTipo(Integer cdOtrPneuTipo) {
		this.cdOtrPneuTipo = cdOtrPneuTipo;
	}
	@Required
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("OTR Constru��o")
	@JoinColumn(name="cdOtrConstrucao")
	@ManyToOne(fetch=FetchType.LAZY)
	public OtrConstrucao getOtrConstrucao() {
		return otrConstrucao;
	}
	public void setOtrConstrucao(OtrConstrucao otrConstrucao) {
		this.otrConstrucao = otrConstrucao;
	}
	@DisplayName("C�digo p/ integra��o")
	@MaxLength(20)
	public String getCodigoIntegracao() {
		return codigoIntegracao;
	}
	public void setCodigoIntegracao(String codigoIntegracao) {
		this.codigoIntegracao = codigoIntegracao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("C�digo de Classifica��o OTR")
	@OneToMany(mappedBy="otrPneuTipo")
	public List<OtrPneuTipoOtrClassificacaoCodigo> getListaOtrPneuTipoOtrClassificacaoCodigo() {
		return listaOtrPneuTipoOtrClassificacaoCodigo;
	}
	public void setListaOtrPneuTipoOtrClassificacaoCodigo(
			List<OtrPneuTipoOtrClassificacaoCodigo> listaOtrPneuTipoOtrClassificacaoCodigo) {
		this.listaOtrPneuTipoOtrClassificacaoCodigo = listaOtrPneuTipoOtrClassificacaoCodigo;
	}
	
	/*public String getCodigoStr() {
		return codigoStr;
	}*/
	public void setCodigoStr(String codigoStr) {
		this.codigoStr = codigoStr;
	}

	@Transient
	public String getCodigoStr(){
		List<OtrPneuTipoOtrClassificacaoCodigo> lista = getListaOtrPneuTipoOtrClassificacaoCodigo();
		StringBuilder s = new StringBuilder();
		
		if(lista != null && lista.size()>0){
			for(OtrPneuTipoOtrClassificacaoCodigo otrpneutipootrclassificacaocodigo : lista){
				if(otrpneutipootrclassificacaocodigo.otrClassificacaoCodigo.codigo != null){
					s.append(otrpneutipootrclassificacaocodigo.otrClassificacaoCodigo.codigo);
				}
			}
		}
		
		return s.toString();
	}
	
	
}
