package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_embalagem", sequenceName="sq_embalagem")
public class Embalagem {
	private Integer cdembalagem;
	private Material material;
	private Unidademedida unidadeMedida;
	private String codigoBarras;
	
	//TRANSIENTE
	private Double qtde;
	private Boolean isConferenciaCdMaterial;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_embalagem")
	public Integer getCdembalagem() {
		return cdembalagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@DisplayName("Unidade de medida")
	@Required
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	@DisplayName("C�digo de barras")
	public String getCodigoBarras() {
		return codigoBarras;
	}
	public void setCdembalagem(Integer cdembalagem) {
		this.cdembalagem = cdembalagem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}
	@Transient
	public Double getQtde() {
		return qtde;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	@Transient
	public Boolean getIsConferenciaCdMaterial() {
		return isConferenciaCdMaterial;
	}
	public void setIsConferenciaCdMaterial(Boolean isConferenciaCdMaterial) {
		this.isConferenciaCdMaterial = isConferenciaCdMaterial;
	}
}
