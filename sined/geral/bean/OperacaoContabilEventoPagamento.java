package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_operacaocontabileventopagamento", sequenceName="sq_operacaocontabileventopagamento")
public class OperacaoContabilEventoPagamento {

	private Integer cdOperacaoContabilEventoPagamento;
	private EventoPagamento evento;
	private Operacaocontabil operacaoContabil;
	
	
	@Id
	@GeneratedValue(generator="sq_operacaocontabileventopagamento", strategy=GenerationType.AUTO)
	public Integer getCdOperacaoContabilEventoPagamento() {
		return cdOperacaoContabilEventoPagamento;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEvento() {
		return evento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaoContabil() {
		return operacaoContabil;
	}
	public void setCdOperacaoContabilEventoPagamento(
			Integer cdOperacaoContabilEventoPagamento) {
		this.cdOperacaoContabilEventoPagamento = cdOperacaoContabilEventoPagamento;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
	public void setOperacaoContabil(Operacaocontabil operacaoContabil) {
		this.operacaoContabil = operacaoContabil;
	}
}
