package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfeaquaviarioterminalcarregamento", sequenceName = "sq_mdfeaquaviarioterminalcarregamento")
public class MdfeAquaviarioTerminalCarregamento {

	protected Integer cdMdfeAquaviarioTerminalCarregamento;
	protected Mdfe mdfe;
	protected String codigoTerminal;
	protected String nomeTerminal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeaquaviarioterminalcarregamento")
	public Integer getCdMdfeAquaviarioTerminalCarregamento() {
		return cdMdfeAquaviarioTerminalCarregamento;
	}
	public void setCdMdfeAquaviarioTerminalCarregamento(
			Integer cdMdfeAquaviarioTerminalCarregamento) {
		this.cdMdfeAquaviarioTerminalCarregamento = cdMdfeAquaviarioTerminalCarregamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=8)
	@DisplayName("C�digo do terminal de carregamento")
	public String getCodigoTerminal() {
		return codigoTerminal;
	}
	public void setCodigoTerminal(
			String codigoTerminal) {
		this.codigoTerminal = codigoTerminal;
	}
	
	@Required
	@MaxLength(value=60)
	@DisplayName("Nome do terminal de carregamento")
	public String getNomeTerminal() {
		return nomeTerminal;
	}
	public void setNomeTerminal(String nomeTerminal) {
		this.nomeTerminal = nomeTerminal;
	}
}
