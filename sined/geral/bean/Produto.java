package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.sined.util.Log;


@Entity
public class Produto extends Material implements Log {

	protected String nomereduzido;
	protected Integer qtdeminima;
	protected String observacao;
	protected Double largura;
	protected Double comprimento;
	protected Double altura;
	protected Boolean materialcorte;
	protected Double fatorconversao;
	protected Boolean arredondamentocomprimento;
	protected Double margemarredondamento;
	protected Double fatorconversaoproducao;
	
	//Devem pertencer somente a classe m�e:
	//protected Timestamp dtAltera;
	//protected Integer cdUsuarioAltera;
	
	//TRANSIENTS
	protected Double quantidade;
	
	public Produto() {
	}
	
	public Produto(Material material) {
		super(material != null ? material.getCdmaterial() : null);
	}
	
	public Produto(String nomereduzido, Integer qtdeminima, String observacao, Double largura, Double comprimento) {
		this.nomereduzido = nomereduzido;
		this.qtdeminima = qtdeminima;
		this.observacao = observacao;
		this.largura = largura;
		this.comprimento = comprimento;
	}
	
	public Produto(String nomereduzido, Integer qtdeminima, String observacao, Double largura, Double comprimento,
						Double altura, Boolean materialcorte) {
		this.nomereduzido = nomereduzido;
		this.qtdeminima = qtdeminima;
		this.observacao = observacao;
		this.largura = largura;
		this.comprimento = comprimento;
		this.altura = altura;
		this.materialcorte = materialcorte;
	}
	
	public Produto(String nomereduzido, Integer qtdeminima, String observacao, Double largura, Double comprimento,
			Double altura, Boolean materialcorte, Double fatorconversao, Boolean arredondamentocomprimento, 
			Double margemarredondamento) {
		this.nomereduzido = nomereduzido;
		this.qtdeminima = qtdeminima;
		this.observacao = observacao;
		this.largura = largura;
		this.comprimento = comprimento;
		this.altura = altura;
		this.materialcorte = materialcorte;
		this.fatorconversao = fatorconversao;
		this.arredondamentocomprimento = arredondamentocomprimento;
		this.margemarredondamento = margemarredondamento;
	}
	
	public Produto(Integer cdmaterial, String nome, Double quantidade){
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.quantidade = quantidade;
	}

	public String getNomereduzido() {
		return nomereduzido;
	}
	
	@MaxLength(10)
	public Integer getQtdeminima() {
		return qtdeminima;
	}

	public String getObservacao() {
		return observacao;
	}
	
	@MinValue(0)
	public Double getLargura() {
		return largura;
	}
	
	@MinValue(0)
	public Double getComprimento() {
		return comprimento;
	}
	
	@DisplayName("Convers�o para a venda")
	public Double getFatorconversao() {
		return fatorconversao;
	}
	
	@DisplayName("Convers�o para a produ��o")
	public Double getFatorconversaoproducao() {
		return fatorconversaoproducao;
	}

	@DisplayName("Arredondamento comprimento")
	public Boolean getArredondamentocomprimento() {
		return arredondamentocomprimento;
	}

	@DisplayName("Margem de Arredondamento (mm)")
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}


	public void setNomereduzido(String nomereduzido) {
		this.nomereduzido = nomereduzido;
	}

	public void setQtdeminima(Integer qtdeminima) {
		this.qtdeminima = qtdeminima;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}
	
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}

	public void setFatorconversaoproducao(Double fatorconversaoproducao) {
		this.fatorconversaoproducao = fatorconversaoproducao;
	}

	public void setArredondamentocomprimento(Boolean arredondamentocomprimento) {
		this.arredondamentocomprimento = arredondamentocomprimento;
	}

	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}
	
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	@Transient
	@MaxLength(9)
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public Double getAltura() {
		return altura;
	}

	public Boolean getMaterialcorte() {
		return materialcorte;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setMaterialcorte(Boolean materialcorte) {
		this.materialcorte = materialcorte;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		return result;
	}
}
