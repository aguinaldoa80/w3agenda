package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_motivoreprovacaofaturamento", sequenceName="sq_motivoreprovacaofaturamento")
@DisplayName("Motivo de reprova��o")
public class MotivoReprovacaoFaturamento implements Log {
	
	protected Integer cdmotivoreprovacaofaturamento;
	protected String descricao;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_motivoreprovacaofaturamento")
	public Integer getCdmotivoreprovacaofaturamento() {
		return cdmotivoreprovacaofaturamento;
	}
	public void setCdmotivoreprovacaofaturamento(
			Integer cdmotivoreprovacaofaturamento) {
		this.cdmotivoreprovacaofaturamento = cdmotivoreprovacaofaturamento;
	}
	
	@Required
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
