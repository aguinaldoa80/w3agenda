package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Entregaacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_entregahistorico", sequenceName = "sq_entregahistorico")
public class Entregahistorico implements Log {

	protected Integer cdentregahistorico;
	protected Entrega entrega;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected Entregaacao entregaacao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregahistorico")
	public Integer getCdentregahistorico() {
		return cdentregahistorico;
	}
	public void setCdentregahistorico(Integer cdentregahistorico) {
		this.cdentregahistorico = cdentregahistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("A��o")
	public Entregaacao getEntregaacao() {
		return entregaacao;
	}
	
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setEntregaacao(Entregaacao entregaacao) {
		this.entregaacao = entregaacao;
	}

	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
		
}
