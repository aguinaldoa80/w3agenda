package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_requisicaonota", sequenceName = "sq_requisicaonota")
public class Requisicaonota {

	protected Integer cdrequisicaonota;
	protected Requisicao requisicao;
	protected Materialrequisicao materialrequisicao;
	protected Double percentualfaturado;
	protected Nota nota;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_requisicaonota")
	public Integer getCdrequisicaonota() {
		return cdrequisicaonota;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialrequisicao")
	public Materialrequisicao getMaterialrequisicao() {
		return materialrequisicao;
	}
	
	@DisplayName("% Faturado")
	public Double getPercentualfaturado() {
		return percentualfaturado;
	}

	public void setPercentualfaturado(Double percentualfaturado) {
		this.percentualfaturado = percentualfaturado;
	}
	
	public void setMaterialrequisicao(Materialrequisicao materialrequisicao) {
		this.materialrequisicao = materialrequisicao;
	}

	public void setCdrequisicaonota(Integer cdrequisicaonota) {
		this.cdrequisicaonota = cdrequisicaonota;
	}

	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
}