package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_area", sequenceName = "sq_area")
public class Area implements Log, PermissaoProjeto{

	protected Integer cdarea;
	protected String descricao;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Areatipo> listaAreatipo = new ListSet<Areatipo>(Areatipo.class);
	protected List<Areaquestionario> listaAreaquestionario = new ListSet<Areaquestionario>(Areaquestionario.class);
	
	protected Boolean irListagemDireto = Boolean.FALSE;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_area")
	public Integer getCdarea() {
		return cdarea;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	@OneToMany(mappedBy="area")
	@DisplayName("Tipo de �rea")
	public List<Areatipo> getListaAreatipo() {
		return listaAreatipo;
	}
	
	@OneToMany(mappedBy="area")
	@DisplayName("Question�rio")
	public List<Areaquestionario> getListaAreaquestionario() {
		return listaAreaquestionario;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	
	public void setCdarea(Integer cdarea) {
		this.cdarea = cdarea;
	}
	public void setListaAreatipo(List<Areatipo> listaAreatipo) {
		this.listaAreatipo = listaAreatipo;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	public void setListaAreaquestionario(
			List<Areaquestionario> listaAreaquestionario) {
		this.listaAreaquestionario = listaAreaquestionario;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdarea == null) ? 0 : cdarea.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Area))
			return false;
		final Area other = (Area) obj;
		if (cdarea == null) {
			if (other.cdarea != null)
				return false;
		} else if (!cdarea.equals(other.cdarea))
			return false;
		return true;
	}	
	
	public String subQueryProjeto() {
		return "select areasubQueryProjeto.cdarea " +
				"from Area areasubQueryProjeto " +
				"join areasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
}
