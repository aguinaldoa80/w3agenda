package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_exameconvenioresponsavel", sequenceName = "sq_exameconvenioresponsavel")
public class Exameconvenioresponsavel {

		protected Integer cdexameconvenioresponsavel;
		protected Exameresponsavel exameresponsavel;
		protected Exameconvenio exameconvenio;
		
		
		@Id
		@DisplayName("Id")
		@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exameconvenioresponsavel")
		public Integer getCdexameconvenioresponsavel() {
			return cdexameconvenioresponsavel;
		}
		
		@DisplayName("Exame profissional")
		@JoinColumn(name="cdexameresponsavel")
		@ManyToOne(fetch=FetchType.LAZY)
		@Required
		public Exameresponsavel getExameresponsavel() {
			return exameresponsavel;
		}
		
		@DisplayName("Conv�nio")
		@JoinColumn(name="cdexameconvenio")
		@ManyToOne(fetch=FetchType.LAZY)
		@Required
		public Exameconvenio getExameconvenio() {
			return exameconvenio;
		}
		
		
		public void setCdexameconvenioresponsavel(Integer cdexameconvenioresponsavel) {
			this.cdexameconvenioresponsavel = cdexameconvenioresponsavel;
		}
		public void setExameresponsavel(Exameresponsavel exameresponsavel) {
			this.exameresponsavel = exameresponsavel;
		}
		public void setExameconvenio(Exameconvenio exameconvenio) {
			this.exameconvenio = exameconvenio;
		}
		
}
