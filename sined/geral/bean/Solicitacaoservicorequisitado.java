package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_solicitacaoservicorequisitado",sequenceName="sq_solicitacaoservicorequisitado")
public class Solicitacaoservicorequisitado {
	
	protected Integer cdsolicitacaoservicorequisitado;
	protected Solicitacaoservico solicitacaoservico;
	protected Colaborador colaborador;
	protected String email;
	
	@Id
	@DisplayName("Solicitação")
	@GeneratedValue(generator="sq_solicitacaoservicorequisitado",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservicorequisitado() {
		return cdsolicitacaoservicorequisitado;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservico")
	public Solicitacaoservico getSolicitacaoservico() {
		return solicitacaoservico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@Required
	@MaxLength(50)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCdsolicitacaoservicorequisitado(
			Integer cdsolicitacaoservicorequisitado) {
		this.cdsolicitacaoservicorequisitado = cdsolicitacaoservicorequisitado;
	}

	public void setSolicitacaoservico(Solicitacaoservico solicitacaoservico) {
		this.solicitacaoservico = solicitacaoservico;
	}

}
