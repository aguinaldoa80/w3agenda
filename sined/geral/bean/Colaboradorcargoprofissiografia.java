package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorcargoprofissiografia", sequenceName = "sq_colaboradorcargoprofissiografia")
public class Colaboradorcargoprofissiografia{

	protected Integer cdcolaboradorcargoprofissiografia;
	protected Colaboradorcargo colaboradorcargo;
	protected Cargoatividade cargoatividade;
	protected Date dtinicio;
	protected Date dtfim;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcargoprofissiografia")
	public Integer getCdcolaboradorcargoprofissiografia() {
		return cdcolaboradorcargoprofissiografia;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorcargo")
	public Colaboradorcargo getColaboradorcargo() {
		return colaboradorcargo;
	}

	@Required
	@DisplayName("Atividade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargoatividade")
	public Cargoatividade getCargoatividade() {
		return cargoatividade;
	}

	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}

	public void setCdcolaboradorcargoprofissiografia(
			Integer cdcolaboradorcargoprofissiografia) {
		this.cdcolaboradorcargoprofissiografia = cdcolaboradorcargoprofissiografia;
	}

	public void setColaboradorcargo(Colaboradorcargo colaboradorcargo) {
		this.colaboradorcargo = colaboradorcargo;
	}

	public void setCargoatividade(Cargoatividade cargoatividade) {
		this.cargoatividade = cargoatividade;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

}
