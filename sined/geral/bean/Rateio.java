package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_rateio",sequenceName="sq_rateio")
public class Rateio implements Log, Cloneable{
	
	
	protected Integer cdrateio;
	protected List<Rateioitem> listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Ordemcompra> listaOrdemcompra;
	protected Set<Documento> listaDocumento;
	protected Set<Contrato> listaContrato;
	protected Set<Movimentacaoestoque> listaMovimentacaoEstoque;
	
	
	
	//Transient
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Contagerencial contagerencialcredito;
	protected Contagerencial contagerencialdebito;
	protected Double percentual;
	protected Money valor;
	protected String valoraux;
	protected String valortotaux;
	protected Documento documento;
	
	public Rateio() {
	}
	public Rateio(Integer cdRateiro) {
		this.cdrateio = cdRateiro;
	}
	public Rateio(Rateio rateio) {
		this.cdrateio = rateio.cdrateio != null ? new Integer(rateio.cdrateio) : null;
		if(rateio.listaRateioitem != null){
			this.listaRateioitem = new ListSet<Rateioitem>(Rateioitem.class);
			for (Rateioitem rateioitem : rateio.listaRateioitem) {
				this.listaRateioitem.add(new Rateioitem(rateioitem));
			}
		}
		this.cdusuarioaltera = rateio.cdusuarioaltera != null ? new Integer(rateio.cdusuarioaltera) : null;
		this.dtaltera = rateio.dtaltera != null ? new Timestamp(rateio.dtaltera.getTime()) : null;
	}
	
	@Id
	@GeneratedValue(generator="sq_rateio",strategy=GenerationType.AUTO)
	public Integer getCdrateio() {
		return cdrateio;
	}
	@DisplayName("Rateio")
	@OneToMany(mappedBy="rateio")
	@CollectionOfElements
	public List<Rateioitem> getListaRateioitem() {
		return listaRateioitem;
	}
	public void setCdrateio(Integer cdrateio) {
		this.cdrateio = cdrateio;
	}
	public void setListaRateioitem(List<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	@Transient
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@Transient
	public Projeto getProjeto() {
		return projeto;
	}
	@Transient
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Transient
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencialcredito() {
		return contagerencialcredito;
	}
	@Transient
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencialdebito() {
		return contagerencialdebito;
	}
	@Transient
	@MinValue(1)
	@MaxValue(100)
	public Double getPercentual() {
		return percentual;
	}
	@Transient
	public Money getValor() {
		return valor;
	}
	@Transient
	@DisplayName("Valor restante")
	public String getValoraux() {
		return valoraux;
	}
	@Transient
	@DisplayName("Valor total")
	public String getValortotaux() {
		return valortotaux;
	}
	@Transient
	public Documento getDocumento() {
		return documento;
	}
	@OneToMany(mappedBy="rateio")
	public Set<Documento> getListaDocumento() {
		return listaDocumento;
	}
	@OneToMany(mappedBy="rateio")
	public Set<Movimentacaoestoque> getListaMovimentacaoEstoque() {
		return listaMovimentacaoEstoque;
	}
	@OneToMany(mappedBy="rateio")
	public Set<Contrato> getListaContrato() {
		return listaContrato;
	}
	@OneToMany(mappedBy="rateio")
	public Set<Ordemcompra> getListaOrdemcompra() {
		return listaOrdemcompra;
	}
	public void setListaMovimentacaoEstoque(Set<Movimentacaoestoque> listaMovimentacaoEstoque) {
		this.listaMovimentacaoEstoque = listaMovimentacaoEstoque;
	}
	public void setListaOrdemcompra(Set<Ordemcompra> listaOrdemcompra) {
		this.listaOrdemcompra = listaOrdemcompra;
	}
	public void setListaContrato(Set<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	public void setListaDocumento(Set<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setContagerencialcredito(Contagerencial contagerencialcredito) {
		this.contagerencialcredito = contagerencialcredito;
	}
	public void setContagerencialdebito(Contagerencial contagerencialdebito) {
		this.contagerencialdebito = contagerencialdebito;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setValoraux(String valoraux) {
		this.valoraux = valoraux;
	}
	public void setValortotaux(String valortotaux) {
		this.valortotaux = valortotaux;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	/* API */
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdrateio == null) ? 0 : cdrateio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rateio other = (Rateio) obj;
		if (cdrateio == null) {
			if (other.cdrateio != null)
				return false;
		} else if (!cdrateio.equals(other.cdrateio))
			return false;
		return true;
	}
	
}
