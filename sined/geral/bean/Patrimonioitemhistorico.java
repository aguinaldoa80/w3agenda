package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_patrimonioitemhistorico", sequenceName = "sq_patrimonioitemhistorico")
public class Patrimonioitemhistorico implements Log {

	protected Integer cdpatrimonioitemhistorico;
	protected Patrimonioitem patrimonioitem;
	protected Double horimetro;
	protected String observacao;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_patrimonioitemhistorico")
	public Integer getCdpatrimonioitemhistorico() {
		return cdpatrimonioitemhistorico;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	@DisplayName("Hor�metro")
	public Double getHorimetro() {
		return horimetro;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdpatrimonioitemhistorico(Integer cdpatrimonioitemhistorico) {
		this.cdpatrimonioitemhistorico = cdpatrimonioitemhistorico;
	}
	
	public void setHorimetro(Double horimetro) {
		this.horimetro = horimetro;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}

}
