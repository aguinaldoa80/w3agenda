package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_documentoenvioboletohistorico", sequenceName = "sq_documentoenvioboletohistorico")
@DisplayName("Histórico")
public class Documentoenvioboletohistorico implements Log{
	
	private Integer cddocumentoenvioboletohistorico;
	private Documentoenvioboleto documentoenvioboleto;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Documentoenvioboletoacao documentoenvioboletoacao;
	private Usuario usuarioaltera;
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}

	@Id
	@GeneratedValue(generator="sq_documentoenvioboletohistorico", strategy=GenerationType.AUTO)
	public Integer getCddocumentoenvioboletohistorico() {
		return cddocumentoenvioboletohistorico;
	}

	public void setCddocumentoenvioboletohistorico(
			Integer cddocumentoenvioboletohistorico) {
		this.cddocumentoenvioboletohistorico = cddocumentoenvioboletohistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoenvioboleto")
	public Documentoenvioboleto getDocumentoenvioboleto() {
		return documentoenvioboleto;
	}

	public void setDocumentoenvioboleto(Documentoenvioboleto documentoenvioboleto) {
		this.documentoenvioboleto = documentoenvioboleto;
	}

	public Documentoenvioboletoacao getDocumentoenvioboletoacao() {
		return documentoenvioboletoacao;
	}

	public void setDocumentoenvioboletoacao(
			Documentoenvioboletoacao documentoenvioboletoacao) {
		this.documentoenvioboletoacao = documentoenvioboletoacao;
	}

	@DisplayName("Responsável")
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
}