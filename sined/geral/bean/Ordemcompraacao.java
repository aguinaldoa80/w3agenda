package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Entity
public class Ordemcompraacao {
	
	public static final Ordemcompraacao CRIADA = new Ordemcompraacao(0, "Criada");
	public static final Ordemcompraacao AUTORIZADA = new Ordemcompraacao(1, "Autorizada");
	public static final Ordemcompraacao PEDIDO_ENVIADO = new Ordemcompraacao(2, "Pedido enviado");
	public static final Ordemcompraacao ENTEGA_RECEBIDA = new Ordemcompraacao(3, "Entrega recebida");
	public static final Ordemcompraacao BAIXADA = new Ordemcompraacao(4, "Baixada");
	public static final Ordemcompraacao ESTORNADA = new Ordemcompraacao(5, "Estornada");
	public static final Ordemcompraacao CANCELADA = new Ordemcompraacao(6, "Cancelada");
	public static final Ordemcompraacao ALTERADA = new Ordemcompraacao(7, "Alterada");
	public static final Ordemcompraacao APROVADA = new Ordemcompraacao(9, "Aprovada");
	
	protected Integer cdordemcompraacao;
	protected String nome;
	
	public Ordemcompraacao(){}
	
	public Ordemcompraacao(Integer cdordemcompraacao){
		this.cdordemcompraacao = cdordemcompraacao;
	}
	
	public Ordemcompraacao(Integer cdordemcompraacao,String nome){
		this.cdordemcompraacao = cdordemcompraacao;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdordemcompraacao() {
		return cdordemcompraacao;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCdordemcompraacao(Integer cdordemcompraacao) {
		this.cdordemcompraacao = cdordemcompraacao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Ordemcompraacao) {
			Ordemcompraacao ordemcompraacao = (Ordemcompraacao) obj;
			return ordemcompraacao.getCdordemcompraacao().equals(this.getCdordemcompraacao());
		}
		return super.equals(obj);
	}
	
	@Transient
	public static Ordemcompraacao getDescricaoAcao(Integer cdordemcompraacao){
		switch (cdordemcompraacao) {
		case 0:
			return CRIADA;
		case 1:
			return AUTORIZADA;
		case 2:
			return PEDIDO_ENVIADO;
		case 3:
			return ENTEGA_RECEBIDA;
		case 4:
			return BAIXADA;
		case 5:
			return ESTORNADA;
		case 6:
			return CANCELADA;
		case 7:
			return ALTERADA;
		default:
			return null;
		}
	}
	
}
