package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentoprocessohistorico",sequenceName="sq_documentoprocessohistorico")
@DisplayName("Hist�rico")
public class Documentoprocessohistorico implements Log{
	
	//Tabela documentoprocessohistorico	
	protected Integer cddocumentoprocessohistorico;
	protected Documentoprocesso documentoprocesso;
	protected Documentoprocessosituacao documentoprocessosituacao;
	protected Arquivo arquivo;
	protected String observacao;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Id
	@GeneratedValue(generator="sq_documentoprocessohistorico",strategy=GenerationType.AUTO)
	public Integer getCddocumentoprocessohistorico() {
		return cddocumentoprocessohistorico;
	}
	
	@DisplayName("Documento/processo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocesso")
	@Required
	public Documentoprocesso getDocumentoprocesso() {
		return documentoprocesso;
	}
	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocessosituacao")
	public Documentoprocessosituacao getDocumentoprocessosituacao() {
		return documentoprocessosituacao;
	}
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@MaxLength(100)
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCddocumentoprocessohistorico(Integer cddocumentoprocessohistorico) {
		this.cddocumentoprocessohistorico = cddocumentoprocessohistorico;
	}
	
	public void setDocumentoprocesso(Documentoprocesso documentoprocesso) {
		this.documentoprocesso = documentoprocesso;
	}
	
	public void setDocumentoprocessosituacao(
			Documentoprocessosituacao documentoprocessosituacao) {
		this.documentoprocessosituacao = documentoprocessosituacao;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
