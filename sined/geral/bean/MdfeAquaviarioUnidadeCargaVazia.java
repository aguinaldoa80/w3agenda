package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoUnidadeCarga;

@Entity
@SequenceGenerator(name = "sq_mdfeaquaviariounidadecargavazia", sequenceName = "sq_mdfeaquaviariounidadecargavazia")
public class MdfeAquaviarioUnidadeCargaVazia {

	protected Integer cdMdfeAquaviarioUnidadeCargaVazia;
	protected Mdfe mdfe;
	protected String identificacaoUnidadeCarga;
	protected TipoUnidadeCarga tipoUnidadeCarga;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeaquaviariounidadecargavazia")
	public Integer getCdMdfeAquaviarioUnidadeCargaVazia() {
		return cdMdfeAquaviarioUnidadeCargaVazia;
	}
	public void setCdMdfeAquaviarioUnidadeCargaVazia(
			Integer cdMdfeAquaviarioUnidadeCargaVazia) {
		this.cdMdfeAquaviarioUnidadeCargaVazia = cdMdfeAquaviarioUnidadeCargaVazia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação da unidade de carga")
	public String getIdentificacaoUnidadeCarga() {
		return identificacaoUnidadeCarga;
	}
	public void setIdentificacaoUnidadeCarga(String identificacaoUnidadeCarga) {
		this.identificacaoUnidadeCarga = identificacaoUnidadeCarga;
	}
	
	@Required
	@DisplayName("Tipo da carga de unidade")
	public TipoUnidadeCarga getTipoUnidadeCarga() {
		return tipoUnidadeCarga;
	}
	public void setTipoUnidadeCarga(TipoUnidadeCarga tipoUnidadeCarga) {
		this.tipoUnidadeCarga = tipoUnidadeCarga;
	}
}
