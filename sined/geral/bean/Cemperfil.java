package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;


@Entity
@SequenceGenerator(name = "sq_cemperfil", sequenceName = "sq_cemperfil")
public class Cemperfil implements ImportacaoproducaoMaterial {

	protected Integer cdcemperfil;
	protected Cemtipologia cemtipologia;
	protected String ref;
	protected String codigo;
	protected String trat;
	protected String descricao;
	protected Double peso;
	protected Double qtde;
	protected Double tam; 
	protected String ang_esq;
	protected String ang_dir;
	protected Double custo;
	protected String linha;
	protected Double tamanhos_barras;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cemperfil")
	public Integer getCdcemperfil() {
		return cdcemperfil;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcemtipologia")
	public Cemtipologia getCemtipologia() {
		return cemtipologia;
	}

	@MaxLength(20)
	public String getRef() {
		return ref;
	}

	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public Double getQtde() {
		return qtde;
	}

	public Double getTam() {
		return tam;
	}

	public Double getCusto() {
		return custo;
	}
	
	@MaxLength(20)
	public String getTrat() {
		return trat;
	}

	public Double getPeso() {
		return peso;
	}

	public String getAng_esq() {
		return ang_esq;
	}

	public String getAng_dir() {
		return ang_dir;
	}
	
	@MaxLength(20)
	public String getLinha() {
		return linha;
	}

	public Double getTamanhos_barras() {
		return tamanhos_barras;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public void setTamanhos_barras(Double tamanhosBarras) {
		tamanhos_barras = tamanhosBarras;
	}

	public void setTrat(String trat) {
		this.trat = trat;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public void setAng_esq(String angEsq) {
		ang_esq = angEsq;
	}

	public void setAng_dir(String angDir) {
		ang_dir = angDir;
	}

	public void setCdcemperfil(Integer cdcemperfil) {
		this.cdcemperfil = cdcemperfil;
	}

	public void setCemtipologia(Cemtipologia cemtipologia) {
		this.cemtipologia = cemtipologia;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setTam(Double tam) {
		this.tam = tam;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}
	
	@Transient
	@Override
	public String getUnidademedidaMaterial() {
		return "BARRA";
	}

	@Transient
	@Override
	public Double getAlturaMaterial() {
		return null;
	}

	@Transient
	@Override
	public Double getLarguraMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public Double getComprimentoMaterial() {
		return this.tam;
	}
	
	@Transient
	@Override
	public Double getQuantidade() {
		return this.qtde != null ? this.qtde : 1;
	}
	
	@Transient
	@Override
	public TipoImportacaoProducao getTipoImportacao() {
		return TipoImportacaoProducao.PERFIL;
	}
	
	@Transient
	@Override
	public String getNomeMaterial() {
		return this.descricao + 
		(this.codigo != null ? (" " + this.codigo) : "") + 
		(this.trat != null ? (" - " + this.trat) : "");
	}
	
	@Transient
	@Override
	public Double getCustoMaterial() {
		return this.custo;
	}
	
	@Transient
	@Override
	public Double getPesoMaterial() {
		return this.peso/(this.qtde != null ? this.qtde : 1);
	}
	
	@Transient
	@Override
	public Double getVolume() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public String getCorMaterial() {
		return this.trat;
	}

	@Transient	
	@Override
	public String getGrupoMaterial() {
		return this.ref;
	}

	@Transient	
	@Override
	public String getTipoMaterial() {
		return "PERFIL";
	}
}
