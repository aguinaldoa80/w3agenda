package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoUnidadeTransporte;

@Entity
@SequenceGenerator(name = "sq_mdfeaquaviariounidadetransportevazia", sequenceName = "sq_mdfeaquaviariounidadetransportevazia")
public class MdfeAquaviarioUnidadeTransporteVazia {

	protected Integer cdMdfeAquaviarioUnidadeTransporteVazia;
	protected Mdfe mdfe;
	protected String identificacaoUnidadeTransporte;
	protected TipoUnidadeTransporte tipoUnidadeTransporte;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeaquaviariounidadetransportevazia")
	public Integer getCdMdfeAquaviarioUnidadeTransporteVazia() {
		return cdMdfeAquaviarioUnidadeTransporteVazia;
	}
	public void setCdMdfeAquaviarioUnidadeTransporteVazia(
			Integer cdMdfeAquaviarioUnidadeTransporteVazia) {
		this.cdMdfeAquaviarioUnidadeTransporteVazia = cdMdfeAquaviarioUnidadeTransporteVazia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação da unidade transporte")
	public String getIdentificacaoUnidadeTransporte() {
		return identificacaoUnidadeTransporte;
	}
	public void setIdentificacaoUnidadeTransporte(
			String identificacaoUnidadeTransporte) {
		this.identificacaoUnidadeTransporte = identificacaoUnidadeTransporte;
	}
	
	@Required
	@DisplayName("Tipo da unidade transporte")
	public TipoUnidadeTransporte getTipoUnidadeTransporte() {
		return tipoUnidadeTransporte;
	}
	public void setTipoUnidadeTransporte(
			TipoUnidadeTransporte tipoUnidadeTransporte) {
		this.tipoUnidadeTransporte = tipoUnidadeTransporte;
	}
}
