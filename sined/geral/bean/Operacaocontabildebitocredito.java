package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoControleEnum;
import br.com.linkcom.sined.geral.bean.enumeration.OperacaocontabildebitocreditoTipoEnum;

@Entity
@SequenceGenerator(name = "sq_operacaocontabildebitocredito", sequenceName = "sq_operacaocontabildebitocredito")
public class Operacaocontabildebitocredito {

	protected Integer cdoperacaocontabildebitocredito;
	protected OperacaocontabildebitocreditoTipoEnum tipo;
	protected Operacaocontabil operacaocontabildebito;
	protected Operacaocontabil operacaocontabilcredito;
	protected ContaContabil contaContabil;
	protected OperacaocontabildebitocreditoControleEnum controle;
	protected Boolean centrocusto;
	protected Boolean projeto;
	protected Boolean sped;
	protected Integer codigointegracao;
	protected String historico;
	protected String formula;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_operacaocontabildebitocredito")
	public Integer getCdoperacaocontabildebitocredito() {
		return cdoperacaocontabildebitocredito;
	}
	@Required
	@DisplayName("Tipo")
	public OperacaocontabildebitocreditoTipoEnum getTipo() {
		return tipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabildebito")
	public Operacaocontabil getOperacaocontabildebito() {
		return operacaocontabildebito;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabilcredito")
	public Operacaocontabil getOperacaocontabilcredito() {
		return operacaocontabilcredito;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	@Required
	@DisplayName("Valor")
	public OperacaocontabildebitocreditoControleEnum getControle() {
		return controle;
	}
	@DisplayName("Centro de custo")
	public Boolean getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Boolean getProjeto() {
		return projeto;
	}
	public Boolean getSped() {
		return sped;
	}
	@DisplayName("Hist�rico Espec�fico")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}
	@MaxLength(9)
	@DisplayName("C�digo Integra��o")
	public Integer getCodigointegracao() {
		return codigointegracao;
	}
	public void setCodigointegracao(Integer codigointegracao) {
		this.codigointegracao = codigointegracao;
	}
	public void setCdoperacaocontabildebitocredito(Integer cdoperacaocontabildebitocredito) {
		this.cdoperacaocontabildebitocredito = cdoperacaocontabildebitocredito;
	}
	public void setTipo(OperacaocontabildebitocreditoTipoEnum tipo) {
		this.tipo = tipo;
	}
	public void setOperacaocontabildebito(Operacaocontabil operacaocontabildebito) {
		this.operacaocontabildebito = operacaocontabildebito;
	}
	public void setOperacaocontabilcredito(Operacaocontabil operacaocontabilcredito) {
		this.operacaocontabilcredito = operacaocontabilcredito;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setControle(OperacaocontabildebitocreditoControleEnum controle) {
		this.controle = controle;
	}
	public void setCentrocusto(Boolean centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Boolean projeto) {
		this.projeto = projeto;
	}
	public void setSped(Boolean sped) {
		this.sped = sped;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
}