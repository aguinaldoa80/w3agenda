package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Romaneiotipo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_romaneio", sequenceName = "sq_romaneio")
@JoinEmpresa("romaneio.empresa")
public class Romaneio implements Log {

	protected Integer cdromaneio;
	protected Empresa empresa;
	protected String descricao;
	protected Localarmazenagem localarmazenagemorigem;
	protected Localarmazenagem localarmazenagemdestino;
	protected Romaneiosituacao romaneiosituacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String placa;
	protected String motorista;
	protected Romaneiotipo romaneiotipo;
	protected Date dtromaneio;
	protected Cliente cliente;
	protected Endereco endereco;
	protected Projeto projeto;
	protected String solicitante;
	protected String conferente;
	protected Double qtdeajudantes;
	protected Romaneio romaneiosubstituido;
	protected Boolean indenizacao;
	protected Centrocusto centroCusto;
	
	
	protected List<Notaromaneio> listaNotaromaneio = new ListSet<Notaromaneio>(Notaromaneio.class);
	protected List<Romaneioitem> listaRomaneioitem = new ListSet<Romaneioitem>(Romaneioitem.class);
	protected Set<Romaneioorigem> listaRomaneioorigem = new ListSet<Romaneioorigem>(Romaneioorigem.class);
	protected Set<Movimentacaoestoqueorigem> listaMovimentacaoestoqueorigem = new ListSet<Movimentacaoestoqueorigem>(Movimentacaoestoqueorigem.class);
	
	// TRANSIENTES
	protected String whereInEntrega;
	protected String whereInTarefa;
	protected String whereInRequisicao;
	protected Date dtcriacao;
	protected List<Movimentacaoestoque> listaMovimentacaoestoqueTrans;
	protected Boolean existContratotrans;
	protected List<Contrato> listaContrato;
	protected Boolean cancelamentoRomaneiosubstituido;
	protected Boolean considerarProjetoorigem;
	protected Boolean existsNotaromaneioNotCancelada = Boolean.FALSE;
	
	public Romaneio() {
	}
	
	public Romaneio(Integer cdromaneio,
			String descricao,
			String localarmazenagemorigem_nome,
			String localarmazenagemdestino_nome,
			String romaneiosituacao_descricao,
			Integer romaneiosituacao_cdromaneiosituacao,
			String empresa_nome,
			String empresa_razaosocial,
			String empresa_nomefantasia,
			java.util.Date dtromaneio,
			Long countNotaromaneioNotCancelada) {
		this.cdromaneio = cdromaneio;
		this.descricao = descricao;
		this.localarmazenagemorigem = new Localarmazenagem(localarmazenagemorigem_nome);
		this.localarmazenagemdestino = new Localarmazenagem(localarmazenagemdestino_nome);
		this.romaneiosituacao = new Romaneiosituacao(romaneiosituacao_cdromaneiosituacao, romaneiosituacao_descricao);
		this.empresa = new Empresa(empresa_nome, empresa_razaosocial, empresa_nomefantasia);
		this.dtromaneio = dtromaneio != null ? SinedDateUtils.castUtilDateForSqlDate(dtromaneio) : null;
		this.existsNotaromaneioNotCancelada = countNotaromaneioNotCancelada > 0;
	}
	
	public Romaneio(Integer cdromaneio) {
		this.cdromaneio = cdromaneio;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_romaneio")
	public Integer getCdromaneio() {
		return cdromaneio;
	}
	@Required
	@DisplayName("Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalorigem")
	public Localarmazenagem getLocalarmazenagemorigem() {
		return localarmazenagemorigem;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneiosituacao")
	public Romaneiosituacao getRomaneiosituacao() {
		return romaneiosituacao;
	}
	@Required
	@DisplayName("Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocaldestino")
	public Localarmazenagem getLocalarmazenagemdestino() {
		return localarmazenagemdestino;
	}
	@OneToMany(mappedBy="romaneio")
	public List<Notaromaneio> getListaNotaromaneio() {
		return listaNotaromaneio;
	}
	@OneToMany(mappedBy="romaneio")
	public List<Romaneioitem> getListaRomaneioitem() {
		return listaRomaneioitem;
	}
	@OneToMany(mappedBy="romaneio")
	public Set<Romaneioorigem> getListaRomaneioorigem() {
		return listaRomaneioorigem;
	}
	@OneToMany(mappedBy="romaneio")
	public Set<Movimentacaoestoqueorigem> getListaMovimentacaoestoqueorigem() {
		return listaMovimentacaoestoqueorigem;
	}
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Data do Romaneio")
	public Date getDtromaneio() {
		return dtromaneio;
	}
	
	public Romaneiotipo getRomaneiotipo() {
		return romaneiotipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdendereco")
	public Endereco getEndereco() {
		return endereco;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@MaxLength(100)
	@DisplayName("Solicitante")
	public String getSolicitante() {
		return solicitante;
	}

	@MaxLength(100)
	@DisplayName("Conferente")
	public String getConferente() {
		return conferente;
	}
	
	@DisplayName("Quantidade de Ajudantes")
	public Double getQtdeajudantes() {
		return qtdeajudantes;
	}
	@Required
	@DisplayName("Centro Custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public void setConferente(String conferente) {
		this.conferente = conferente;
	}

	public void setQtdeajudantes(Double qtdeajudantes) {
		this.qtdeajudantes = qtdeajudantes;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaNotaromaneio(List<Notaromaneio> listaNotaromaneio) {
		this.listaNotaromaneio = listaNotaromaneio;
	}
	public void setListaMovimentacaoestoqueorigem(
			Set<Movimentacaoestoqueorigem> listaMovimentacaoestoqueorigem) {
		this.listaMovimentacaoestoqueorigem = listaMovimentacaoestoqueorigem;
	}
	public void setListaRomaneioorigem(Set<Romaneioorigem> listaRomaneioorigem) {
		this.listaRomaneioorigem = listaRomaneioorigem;
	}
	public void setLocalarmazenagemdestino(
			Localarmazenagem localarmazenagemdestino) {
		this.localarmazenagemdestino = localarmazenagemdestino;
	}
	public void setCdromaneio(Integer cdromaneio) {
		this.cdromaneio = cdromaneio;
	}
	public void setLocalarmazenagemorigem(
			Localarmazenagem localarmazenagemorigem) {
		this.localarmazenagemorigem = localarmazenagemorigem;
	}
	public void setRomaneiosituacao(Romaneiosituacao romaneiosituacao) {
		this.romaneiosituacao = romaneiosituacao;
	}
	public void setListaRomaneioitem(
			List<Romaneioitem> listaRomaneioitem) {
		this.listaRomaneioitem = listaRomaneioitem;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setRomaneiotipo(Romaneiotipo romaneiotipo) {
		this.romaneiotipo = romaneiotipo;
	}
	
	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTE
	
	public void setDtromaneio(Date dtromaneio) {
		this.dtromaneio = dtromaneio;
	}
	@Transient
	public String getWhereInEntrega() {
		return whereInEntrega;
	}
	
	public void setWhereInEntrega(String whereInEntrega) {
		this.whereInEntrega = whereInEntrega;
	}
	
	@MaxLength(8)
	@DisplayName("Placa")
	public String getPlaca() {
		return placa;
	}
	@MaxLength(100)
	@DisplayName("Motorista")
	public String getMotorista() {
		return motorista;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setMotorista(String motorista) {
		this.motorista = motorista;
	}

	@Transient
	public Date getDtcriacao() {
		if(this.getDtaltera() != null){
			return new Date(this.getDtaltera().getTime());
		}
		return dtcriacao;
	}
	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}
	@Transient
	public List<Movimentacaoestoque> getListaMovimentacaoestoqueTrans() {
		return listaMovimentacaoestoqueTrans;
	}
	public void setListaMovimentacaoestoqueTrans(List<Movimentacaoestoque> listaMovimentacaoestoqueTrans) {
		this.listaMovimentacaoestoqueTrans = listaMovimentacaoestoqueTrans;
	}	
	
	@Transient
	public Boolean getExistContratotrans() {
		return existContratotrans;
	}
	public void setExistContratotrans(Boolean existContratotrans) {
		this.existContratotrans = existContratotrans;
	}
	
	@Transient
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}
	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}
	
	@Transient
	public String getWhereInTarefa() {
		return whereInTarefa;
	}
	
	public void setWhereInTarefa(String whereInTarefa) {
		this.whereInTarefa = whereInTarefa;
	}
	
	@Transient
	public String getWhereInRequisicao() {
		return whereInRequisicao;
	}
	
	public void setWhereInRequisicao(String whereInRequisicao) {
		this.whereInRequisicao = whereInRequisicao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneiosubstituido")
	public Romaneio getRomaneiosubstituido() {
		return romaneiosubstituido;
	}

	public void setRomaneiosubstituido(Romaneio romaneiosubstituido) {
		this.romaneiosubstituido = romaneiosubstituido;
	}
	
	public Boolean getIndenizacao() {
		return indenizacao;
	}
	
	public void setIndenizacao(Boolean indenizacao) {
		this.indenizacao = indenizacao;
	}

	@Transient
	public Boolean getCancelamentoRomaneiosubstituido() {
		return cancelamentoRomaneiosubstituido;
	}
	public void setCancelamentoRomaneiosubstituido(Boolean cancelamentoRomaneiosubstituido) {
		this.cancelamentoRomaneiosubstituido = cancelamentoRomaneiosubstituido;
	}

	@Transient
	public Boolean getConsiderarProjetoorigem() {
		return considerarProjetoorigem;
	}

	public void setConsiderarProjetoorigem(Boolean considerarProjetoorigem) {
		this.considerarProjetoorigem = considerarProjetoorigem;
	}
	
	@Transient
	public Boolean getExistsNotaromaneioNotCancelada() {
		return existsNotaromaneioNotCancelada;
	}
	
	public void setExistsNotaromaneioNotCancelada(
			Boolean existsNotaromaneioNotCancelada) {
		this.existsNotaromaneioNotCancelada = existsNotaromaneioNotCancelada;
	}
}