package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dominiospfip", sequenceName = "sq_dominiospfip")
public class Dominiospfip{
	
	protected Integer cddominiospfip;
	protected Dominiospf dominiospf;
	protected Integer versao;
	protected String endereco;
	protected Redemascara redemascara;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominiospfip")
	public Integer getCddominiospfip() {
		return cddominiospfip;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cddominiospf")
	public Dominiospf getDominiospf() {
		return dominiospf;
	}

	@Required
	@DisplayName("Vers�o")
	@MaxLength(6)
	public Integer getVersao() {
		return versao;
	}

	@Required
	@DisplayName("Endere�o")
	@MaxLength(25)
	public String getEndereco() {
		return endereco;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdredemascara")
    @DisplayName("M�scara")
	public Redemascara getRedemascara() {
		return redemascara;
	}

	public void setCddominiospfip(Integer cddominiospfip) {
		this.cddominiospfip = cddominiospfip;
	}

	public void setDominiospf(Dominiospf dominiospf) {
		this.dominiospf = dominiospf;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setRedemascara(Redemascara redemascara) {
		this.redemascara = redemascara;
	}
	
}
