package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_NotaFiscalServicoItem",sequenceName="sq_NotaFiscalServicoItem")
public class NotaFiscalServicoItem implements Log {

	protected Integer cdNotaFiscalServicoItem;
	protected NotaFiscalServico notaFiscalServico;
	protected Material material;
	protected Double qtde;
	protected String descricao;
	protected Double precoUnitario;
	protected Money desconto;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Vendamaterial vendamaterial;
	private Pedidovendamaterial pedidovendamaterial;
	
	//Transientes
	// Auxiliares para relat�rio
	protected Money precoTotal; 
	protected Integer cdmaterialrequisicao;
	protected String numeroautorizacaofaturamentoitemos;
	protected String item;
	protected String nomematerial;
	protected String nomematerialnf;
	protected Money deducao;
	protected Money basecalculoPis;
	protected Money valorPis;
	protected Money basecalculoCofins;
	protected Money valorCofins;
	private Money baseCalculoIss;
    private Money valorIss;
	
	public NotaFiscalServicoItem(){
	}
	
	public NotaFiscalServicoItem(Material material, Double qtde) {
		this.material = material;
		this.qtde = qtde;
	}

	@Id
	@GeneratedValue(generator="sq_NotaFiscalServicoItem",strategy=GenerationType.AUTO)
	public Integer getCdNotaFiscalServicoItem() {
		return cdNotaFiscalServicoItem;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdNotaFiscalServico")
	public NotaFiscalServico getNotaFiscalServico() {
		return notaFiscalServico;
	}

	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}

	@MaxLength(5000)
	@DisplayName("Discrimina��o dos servi�os")
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Valor Unit�rio")
	public Double getPrecoUnitario() {
		return precoUnitario;
	}

	@Transient
	public Money getPrecoTotal() {
		return precoTotal;
	}
	
	public Money getDesconto() {
		return desconto;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	
	public void setNotaFiscalServico(NotaFiscalServico notaFiscalServico) {
		this.notaFiscalServico = notaFiscalServico;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setPrecoUnitario(Double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}

	public void setCdNotaFiscalServicoItem(Integer cdNotaFiscalServicoItem) {
		this.cdNotaFiscalServicoItem = cdNotaFiscalServicoItem;
	}
	
	//Log:
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setPrecoTotal(Money precoTotal) {
		this.precoTotal = precoTotal;
	}
	
	/**
	 * Retorna o valor unit�rio multiplicado pela quantidade
	 * 
	 * @return
	 * @author Hugo Ferreira
	 */
	@Transient
	public Money getTotalParcial() {
		if (this.precoUnitario == null) {
			return null;
		}
		double qtde = this.qtde != null ? this.qtde : 1;
//		Money total = this.precoUnitario.multiply(new Money(qtde, false));
		Money total = new Money(this.precoUnitario).multiply(new Money(qtde, false));
		
		if(this.desconto != null){
			total = total.subtract(this.desconto);
		}
		
		return total;
	}

	@Transient
	public Integer getCdmaterialrequisicao() {
		return cdmaterialrequisicao;
	}
	public void setCdmaterialrequisicao(Integer cdmaterialrequisicao) {
		this.cdmaterialrequisicao = cdmaterialrequisicao;
	}
	
	@Transient
	public String getNumeroautorizacaofaturamentoitemos() {
		return numeroautorizacaofaturamentoitemos;
	}
	
	public void setNumeroautorizacaofaturamentoitemos(String numeroautorizacaofaturamentoitemos) {
		this.numeroautorizacaofaturamentoitemos = numeroautorizacaofaturamentoitemos;
	}
	
	@Transient
	public String getItem() {
		return item;
	}
	
	public void setItem(String item) {
		this.item = item;
	}
	
	@Transient
	public String getNomematerial() {
		return nomematerial;
	}
	public void setNomematerial(String nomematerial) {
		this.nomematerial = nomematerial;
	}

	@Transient
	public String getNomematerialnf() {
		return nomematerialnf;
	}
	public void setNomematerialnf(String nomematerialnf) {
		this.nomematerialnf = nomematerialnf;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}

	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}

	@Transient
	public Money getDeducao() {
		return deducao;
	}
	@Transient
	public Money getBasecalculoPis() {
		return basecalculoPis;
	}
	@Transient
	public Money getValorPis() {
		return valorPis;
	}
	@Transient
	public Money getBasecalculoCofins() {
		return basecalculoCofins;
	}
	@Transient
	public Money getValorCofins() {
		return valorCofins;
	}
    @Transient
    public Money getBaseCalculoIss() {
        return baseCalculoIss;
    }
    @Transient
    public Money getValorIss() {
        return valorIss;
    }
    
    public void setBaseCalculoIss(Money baseCalculoIss) {
        this.baseCalculoIss = baseCalculoIss;
    }
    public void setValorIss(Money valorIss) {
        this.valorIss = valorIss;
    }
	public void setDeducao(Money deducao) {
		this.deducao = deducao;
	}
	public void setBasecalculoPis(Money basecalculoPis) {
		this.basecalculoPis = basecalculoPis;
	}
	public void setValorPis(Money valorPis) {
		this.valorPis = valorPis;
	}
	public void setBasecalculoCofins(Money basecalculoCofins) {
		this.basecalculoCofins = basecalculoCofins;
	}
	public void setValorCofins(Money valorCofins) {
		this.valorCofins = valorCofins;
	}
}