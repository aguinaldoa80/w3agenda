package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Contratomateriallocacaotipo;


@Entity
@SequenceGenerator(name = "sq_contratomateriallocacao", sequenceName = "sq_contratomateriallocacao")
public class Contratomateriallocacao {

	protected Integer cdcontratomateriallocacao;
	protected Contratomaterial contratomaterial;
	protected Date dtmovimentacao;
	protected Double qtde;
	protected Contratomateriallocacaotipo contratomateriallocacaotipo;
	protected Timestamp dtaltera;
	protected Integer cdromaneio;
	protected Boolean substituicao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratomateriallocacao")
	public Integer getCdcontratomateriallocacao() {
		return cdcontratomateriallocacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")	
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	public Date getDtmovimentacao() {
		return dtmovimentacao;
	}
	public Double getQtde() {
		return qtde;
	}
	public Contratomateriallocacaotipo getContratomateriallocacaotipo(){
		return contratomateriallocacaotipo;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdromaneio() {
		return cdromaneio;
	}
	public Boolean getSubstituicao() {
		return substituicao;
	}
	
	public void setCdcontratomateriallocacao(Integer cdcontratomateriallocacao) {
		this.cdcontratomateriallocacao = cdcontratomateriallocacao;
	}
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}
	public void setDtmovimentacao(Date dtmovimentacao) {
		this.dtmovimentacao = dtmovimentacao;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setContratomateriallocacaotipo(Contratomateriallocacaotipo contratomateriallocacaotipo) {
		this.contratomateriallocacaotipo = contratomateriallocacaotipo;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdromaneio(Integer cdromaneio) {
		this.cdromaneio = cdromaneio;
	}
	public void setSubstituicao(Boolean substituicao) {
		this.substituicao = substituicao;
	}
}