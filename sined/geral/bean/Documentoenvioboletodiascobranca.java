package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_documentoenvioboletodiascobranca", sequenceName="sq_documentoenvioboletodiascobranca")
public class Documentoenvioboletodiascobranca {

	private Integer cddocumentoenvioboletodiascobranca;
	private Documentoenvioboleto documentoenvioboleto;
	private Integer qtdedias;
	
	@Id
	@GeneratedValue(generator="sq_documentoenvioboletodiascobranca", strategy=GenerationType.AUTO)
	public Integer getCddocumentoenvioboletodiascobranca() {
		return cddocumentoenvioboletodiascobranca;
	}
	public void setCddocumentoenvioboletodiascobranca(
			Integer cddocumentoenvioboletodiascobranca) {
		this.cddocumentoenvioboletodiascobranca = cddocumentoenvioboletodiascobranca;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoenvioboleto")
	public Documentoenvioboleto getDocumentoenvioboleto() {
		return documentoenvioboleto;
	}
	public void setDocumentoenvioboleto(
			Documentoenvioboleto documentoenvioboleto) {
		this.documentoenvioboleto = documentoenvioboleto;
	}

	@Required
	@DisplayName("Dias P/ Cobran�a")
	public Integer getQtdedias() {
		return qtdedias;
	}
	public void setQtdedias(Integer qtdedias) {
		this.qtdedias = qtdedias;
	}
}
