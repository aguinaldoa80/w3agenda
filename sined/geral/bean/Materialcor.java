package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialcor", sequenceName = "sq_materialcor")
@DisplayName("Cor")
public class Materialcor implements Log{

	protected Integer cdmaterialcor;
	protected String nome;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialcor")
	public Integer getCdmaterialcor() {
		return cdmaterialcor;
	}
	
	@DescriptionProperty
	@DisplayName("Descri��o")
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setCdmaterialcor(Integer cdmaterialcor) {
		this.cdmaterialcor = cdmaterialcor;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}	
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
}
