package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.lkbanco.arquivoiss.ArquivoIss;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadortipopagamento;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoissbrumadinho;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoissparacatu;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImpostoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Aux_apuracaoimposto;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@DisplayName("Nota fiscal de servi�o")
@JoinEmpresa("notaFiscalServico.empresa")
public class NotaFiscalServico extends Nota implements ArquivoIss, PermissaoProjeto {

	protected String fatura;
	protected String duplicata;
	protected Date dtVencimento;
	protected Tipovencimento tipovencimento;
	protected String pracaPagamento;
	protected Double iss;
	protected Double ir;
	protected Double inss;
	protected Double pis;
	protected Double cofins;
	protected Double pisretido;
	protected Double cofinsretido;
	protected Double csll;
	protected Double icms;

	protected Municipio municipioissqn;
	protected Codigocnae codigocnaeBean;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservicoBean;
	protected Naturezaoperacao naturezaoperacao;
	protected Regimetributacao regimetributacao;

	protected Boolean incideiss;
	protected Boolean incideir;
	protected Boolean incideinss;
	protected Boolean incidepis;
	protected Boolean incidecofins;
	protected Boolean incidecsll;
	protected Boolean incideicms;

	protected Money basecalculo;
	protected Money basecalculoir;
	protected Money basecalculoiss;
	protected Money basecalculopis;
	protected Money basecalculocofins;
	protected Money basecalculocsll;
	protected Money basecalculoinss;
	protected Money basecalculoicms;
	protected Money descontocondicionado;
	protected Money descontoincondicionado;
	protected Money deducao;
	protected Money outrasretencoes;
	private Double incentivoFiscalIss;

	protected Situacaoissbrumadinho situacaoissbrumadinho;
	protected Situacaoissparacatu situacaoissparacatu;
	protected Tipocobrancapis cstpis;
	protected Tipocobrancacofins cstcofins;
	protected Indicadortipopagamento indicadortipopagamento;

	protected Integer cddocumento;
	protected String whereInCddocumento;

	protected List<NotaFiscalServicoItem> listaItens;
	protected List<Notafiscalservicoduplicata> listaDuplicata;

	protected String inscricaomunicipal;
	protected String infocomplementar;

	protected Hora hremissao;

	// Auxiliares para relat�rio
	protected Money valorIr;
	protected Money valorInss;
	protected Money valorIss;
	protected Money valorPis;
	protected Money valorCofins;
	protected Money valorPisRetido;
	protected Money valorCofinsRetido;
	protected Money valorCsll;
	protected Money valorIcms;
	protected Money valorTotalServicos;
	protected Money valorNota;
	protected Money valorImposto;
	// campo para n�o ocorrer erro ao imprimir nota
	protected Money valorImpostoNota;
	protected String extenso;
	protected Frequencia frequencia;
	protected List<Contrato> listaContrato;
	protected Boolean faturamentolote;
	protected Conta conta;
	protected String idcontrato;
	protected Date dtvencimentocontareceber;

	protected Boolean naogeranota;
	protected Boolean haveArquivonfnota = Boolean.FALSE;

	protected Boolean fluxoFaturarlocacao;
	protected Boolean fluxoFaturarPorMedicao;
	protected Boolean fromRequisicao;
	protected Boolean fromProducaodiaria;
	protected Boolean fromProposta;
	protected String whereIn;
	protected String numeroNfse;
	protected String codVerificacao;

	protected String diaEmissao;
	protected String mesEmissao;
	protected String anoEmissao;

	protected String inscricaoestadual;
	protected String nomeNF;

	protected Boolean usarSequencial;
	protected String situacaosped;
	protected String contas;

	protected List<ImpostoBean> listaImpostos = new ArrayList<ImpostoBean>();

	// transient
	protected String vendas;
	protected Aux_apuracaoimposto aux_apuracaoimposto;
	protected Documento documento;
	protected Integer cdvendaassociada;
	protected List<Venda> listaVenda = new ArrayList<Venda>();
	protected Boolean clienteIncidirISS;
	protected String romaneios;
	protected Boolean somenteservico;
	protected Requisicao requisicao;
	protected Materialrequisicao materialrequisicao;
	protected Integer cdNotaFiscalServicoLoteRPS;
	protected Date dtinicio;
	protected Date dtfim;
	protected List<NotaStatus> listaSituacao;
	protected Boolean cadastrarcobranca = Boolean.TRUE;
	protected Boolean agruparContas;
	protected Integer qtdParcelas;
	protected String whereInCdvenda;
	private String pneusJaAssociado;

	public NotaFiscalServico() {
		super();
	}

	public NotaFiscalServico(Nota nota) {
		super(nota);
	}

	public NotaFiscalServico(Integer cdNota) {
		super(cdNota);
	}

	public NotaFiscalServico(Integer cdNota, String numeroNfse, String nomeCliente) {
		super(cdNota);
		this.setNomeCliente(nomeCliente);
		this.setNumeroNfse(numeroNfse);
	}

	@MaxLength(30)
	@DisplayName("Nota Fiscal Fatura N�")
	public String getFatura() {
		return fatura;
	}

	@MaxLength(30)
	@DisplayName("N� de Ordem da Duplicata")
	public String getDuplicata() {
		return duplicata;
	}

	@DisplayName("Vencimento")
	public Date getDtVencimento() {
		return dtVencimento;
	}

	@DisplayName("Tipo de vencimento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdtipovencimento")
	public Tipovencimento getTipovencimento() {
		return tipovencimento;
	}

	@MaxLength(30)
	@DisplayName("Pra�a de pagamento")
	public String getPracaPagamento() {
		return pracaPagamento;
	}

	@DisplayName("ISS(%)")
	public Double getIss() {
		return iss;
	}

	@DisplayName("IR(%)")
	public Double getIr() {
		return ir;
	}

	@DisplayName("INSS(%)")
	public Double getInss() {
		return inss;
	}

	@DisplayName("PIS(%)")
	public Double getPis() {
		return pis;
	}

	@DisplayName("COFINS(%)")
	public Double getCofins() {
		return cofins;
	}

	@DisplayName("CSLL(%)")
	public Double getCsll() {
		return csll;
	}

	@DisplayName("ICMS(%)")
	public Double getIcms() {
		return icms;
	}

	@DisplayName("Servi�os")
	@OneToMany(mappedBy = "notaFiscalServico")
	public List<NotaFiscalServicoItem> getListaItens() {
		return listaItens;
	}

	@DisplayName("Servi�os")
	@OneToMany(mappedBy = "notafiscalservico")
	public List<Notafiscalservicoduplicata> getListaDuplicata() {
		return listaDuplicata;
	}

	@Transient
	public String getExtenso() {
		return extenso;
	}

	@Transient
	public List<Contrato> getListaContrato() {
		return listaContrato;
	}

	@DisplayName("Incide ISS")
	public Boolean getIncideiss() {
		return incideiss;
	}

	@DisplayName("Incide IR")
	public Boolean getIncideir() {
		return incideir;
	}

	@DisplayName("Incide INSS")
	public Boolean getIncideinss() {
		return incideinss;
	}

	@DisplayName("Incide PIS")
	public Boolean getIncidepis() {
		return incidepis;
	}

	@DisplayName("Incide COFINS")
	public Boolean getIncidecofins() {
		return incidecofins;
	}

	@DisplayName("Incide CSLL")
	public Boolean getIncidecsll() {
		return incidecsll;
	}

	@DisplayName("Incide ICMS")
	public Boolean getIncideicms() {
		return incideicms;
	}

	public Situacaoissparacatu getSituacaoissparacatu() {
		return situacaoissparacatu;
	}

	public Situacaoissbrumadinho getSituacaoissbrumadinho() {
		return situacaoissbrumadinho;
	}

	@DisplayName("Informa��es complementares")
	public String getInfocomplementar() {
		return infocomplementar;
	}

	public void setInfocomplementar(String infocomplementar) {
		this.infocomplementar = infocomplementar;
	}

	public void setSituacaoissbrumadinho(Situacaoissbrumadinho situacaoissbrumadinho) {
		this.situacaoissbrumadinho = situacaoissbrumadinho;
	}

	public void setSituacaoissparacatu(Situacaoissparacatu situacaoissparacatu) {
		this.situacaoissparacatu = situacaoissparacatu;
	}

	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}

	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}

	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}

	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}

	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}

	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}

	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}

	public void setInss(Double inss) {
		this.inss = inss;
	}

	public void setFatura(String fatura) {
		this.fatura = fatura;
	}

	public void setIr(Double ir) {
		this.ir = ir;
	}

	public void setDuplicata(String duplicata) {
		this.duplicata = duplicata;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public void setTipovencimento(Tipovencimento tipovencimento) {
		this.tipovencimento = tipovencimento;
	}

	public void setPracaPagamento(String pracaPagamento) {
		this.pracaPagamento = pracaPagamento;
	}

	public void setIss(Double iss) {
		this.iss = iss;
	}

	public void setListaItens(List<NotaFiscalServicoItem> listaItens) {
		this.listaItens = listaItens;
	}

	public void setListaDuplicata(List<Notafiscalservicoduplicata> listaDuplicata) {
		this.listaDuplicata = listaDuplicata;
	}

	public void setExtenso(String extenso) {
		this.extenso = Util.strings.captalize(extenso);
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setCsll(Double csll) {
		this.csll = csll;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	@Transient
	public Money getValorImpostosFederais() {
		BigDecimal totalImposto = new BigDecimal(0);

		BigDecimal valorInssLocal = (this.getValorInss() != null ? this.getValorInss().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorIrLocal = (this.getValorIr() != null ? this.getValorIr().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorCofinsLocal = (this.getValorCofins() != null ? this.getValorCofins().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorCofinsRetidoLocal = (this.getValorCofinsRetido() != null ? this.getValorCofinsRetido().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorPisLocal = (this.getValorPis() != null ? this.getValorPis().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorPisRetidoLocal = (this.getValorPisRetido() != null ? this.getValorPisRetido().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal valorCsllLocal = (this.getValorCsll() != null ? this.getValorCsll().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		BigDecimal outrasRetencoes = (this.getOutrasretencoes() != null ? this.getOutrasretencoes().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));

		if (valorInssLocal != null && getIncideinss() != null && getIncideinss())
			totalImposto = totalImposto.add(valorInssLocal);

		if (valorIrLocal != null && getIncideir() != null && getIncideir())
			totalImposto = totalImposto.add(valorIrLocal);

		if ((valorCofinsLocal != null || valorCofinsRetidoLocal != null) && getIncidecofins() != null && getIncidecofins()) {
			if (valorCofinsRetidoLocal != null && valorCofinsRetidoLocal.doubleValue() > 0.0) {
				totalImposto = totalImposto.add(valorCofinsRetidoLocal);
			} else if (valorCofinsLocal != null) {
				totalImposto = totalImposto.add(valorCofinsLocal);
			}
		}

		if ((valorPisLocal != null || valorPisRetidoLocal != null) && getIncidepis() != null && getIncidepis()) {
			if (valorPisRetidoLocal != null && valorPisRetidoLocal.doubleValue() > 0.0) {
				totalImposto = totalImposto.add(valorPisRetidoLocal);
			} else if (valorPisLocal != null) {
				totalImposto = totalImposto.add(valorPisLocal);
			}
		}

		if (valorCsllLocal != null && getIncidecsll() != null && getIncidecsll())
			totalImposto = totalImposto.add(valorCsllLocal);

		if (outrasRetencoes != null)
			totalImposto = totalImposto.add(outrasRetencoes);

		return new Money(totalImposto.doubleValue());
	}

	@Transient
	public Money getValorImpostoNota() {
		return getValorImposto();
	}

	@Transient
	public Money getValorImposto() {
		return getValorImposto(BigDecimal.ROUND_HALF_EVEN);
	}

	@Transient
	public Money getValorImposto(int round_mode) {
		BigDecimal totalImposto = new BigDecimal(0);

		BigDecimal valorInssLocal = (this.getValorInss() != null ? this.getValorInss().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));
		BigDecimal valorIrLocal = (this.getValorIr() != null ? this.getValorIr().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));
		BigDecimal valorIssLocal = (this.getValorIss() != null ? this.getValorIss().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));
		BigDecimal valorIcmsLocal = (this.getValorIcms() != null ? this.getValorIcms().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));
		BigDecimal valorCofinsLocal = (this.getValorCofins() != null ? this.getValorCofins().getValue().abs()
				.setScale(2, round_mode) : new BigDecimal(0));
		BigDecimal valorCofinsRetidoLocal = (this.getValorCofinsRetido() != null ? this.getValorCofinsRetido().getValue().abs()
				.setScale(2, round_mode) : new BigDecimal(0));
		BigDecimal valorPisLocal = (this.getValorPis() != null ? this.getValorPis().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));
		BigDecimal valorPisRetidoLocal = (this.getValorPisRetido() != null ? this.getValorPisRetido().getValue().abs()
				.setScale(2, round_mode) : new BigDecimal(0));
		BigDecimal valorCsllLocal = (this.getValorCsll() != null ? this.getValorCsll().getValue().abs().setScale(2, round_mode)
				: new BigDecimal(0));

		if (valorInssLocal != null && getIncideinss() != null && getIncideinss())
			totalImposto = totalImposto.add(valorInssLocal);

		if (valorIrLocal != null && getIncideir() != null && getIncideir())
			totalImposto = totalImposto.add(valorIrLocal);

		if (valorIssLocal != null && getIncideiss() != null && getIncideiss())
			totalImposto = totalImposto.add(valorIssLocal);

		if (valorIcmsLocal != null && getIncideicms() != null && getIncideicms())
			totalImposto = totalImposto.add(valorIcmsLocal);

		if ((valorCofinsLocal != null || valorCofinsRetidoLocal != null) && getIncidecofins() != null && getIncidecofins()) {
			if (valorCofinsRetidoLocal != null && valorCofinsRetidoLocal.doubleValue() > 0.0) {
				totalImposto = totalImposto.add(valorCofinsRetidoLocal);
			} else if (valorCofinsLocal != null) {
				totalImposto = totalImposto.add(valorCofinsLocal);
			}
		}

		if ((valorPisLocal != null || valorPisRetidoLocal != null) && getIncidepis() != null && getIncidepis()) {
			if (valorPisRetidoLocal != null && valorPisRetidoLocal.doubleValue() > 0.0) {
				totalImposto = totalImposto.add(valorPisRetidoLocal);
			} else if (valorPisLocal != null) {
				totalImposto = totalImposto.add(valorPisLocal);
			}
		}

		if (valorCsllLocal != null && getIncidecsll() != null && getIncidecsll())
			totalImposto = totalImposto.add(valorCsllLocal);

		valorImposto = new Money(totalImposto.doubleValue());

		return valorImposto;
	}

	@Transient
	public Money getValorImpostoTotal() {

		Money totalImposto = new Money();

		Money valorInssLocal = this.getValorInss();
		Money valorIrLocal = this.getValorIr();
		Money valorIssLocal = this.getValorIss();
		Money valorIcmsLocal = this.getValorIcms();
		Money valorCofinsLocal = this.getValorCofins();
		Money valorCofinsRetidoLocal = this.getValorCofinsRetido();
		Money valorPisLocal = this.getValorPis();
		Money valorPisRetidoLocal = this.getValorPisRetido();
		Money valorCsllLocal = this.getValorCsll();

		if (valorInssLocal != null)
			totalImposto = totalImposto.add(valorInssLocal);

		if (valorIrLocal != null)
			totalImposto = totalImposto.add(valorIrLocal);

		if (valorIssLocal != null)
			totalImposto = totalImposto.add(valorIssLocal);

		if (valorIcmsLocal != null)
			totalImposto = totalImposto.add(valorIcmsLocal);

		if (valorCofinsRetidoLocal != null && valorCofinsRetidoLocal.getValue().doubleValue() > 0) {
			totalImposto = totalImposto.add(valorCofinsRetidoLocal);
		} else if (valorCofinsLocal != null) {
			totalImposto = totalImposto.add(valorCofinsLocal);
		}

		if (valorPisRetidoLocal != null && valorPisRetidoLocal.getValue().doubleValue() > 0) {
			totalImposto = totalImposto.add(valorPisRetidoLocal);
		} else if (valorPisLocal != null) {
			totalImposto = totalImposto.add(valorPisLocal);
		}

		if (valorCsllLocal != null)
			totalImposto = totalImposto.add(valorCsllLocal);

		valorImposto = totalImposto;

		return valorImposto;
	}

	@Transient
	public Money getValorCofins() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculocofins();
		if (valorBaseCalculo != null && this.cofins != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.cofins)).divide(cem).getValue();
			// valorCofins = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorCofins = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_COFINS));
		}

		return valorCofins;
	}

	@Transient
	public Money getValorCsll() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculocsll();
		if (valorBaseCalculo != null && this.csll != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.csll)).divide(cem).getValue();
			// valorCsll = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorCsll = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_CSLL));
		}

		return valorCsll;
	}

	@Transient
	public Money getValorIcms() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculoicms();
		if (valorBaseCalculo != null && this.icms != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.icms)).divide(cem).getValue();
			// valorIcms = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorIcms = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_ICMS));
		}

		return valorIcms;
	}

	@Transient
	public Money getValorPis() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculopis();
		if (valorBaseCalculo != null && this.pis != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.pis)).divide(cem).getValue();
			// valorPis = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorPis = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_PIS));
		}

		return valorPis;
	}

	@Transient
	public Money getValorIss() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculoiss();
		if (valorBaseCalculo != null && this.iss != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.iss)).divide(cem).getValue();
			// valorIss = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorIss = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_ISS));
		}

		return valorIss;
	}

	@Override
	@Transient
	public Money getValoriss() {
		return this.getValorIss();
	}

	@Transient
	public Money getValorInss() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculoinss();
		if (valorBaseCalculo != null && this.inss != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.inss)).divide(cem).getValue();
			// valorInss = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorInss = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_INSS));
		}

		return valorInss;
	}

	@Transient
	public Money getValorIr() {
		// Money valorBaseCalculo = this.getBasecalculo();
		Money valorBaseCalculo = this.getBasecalculoir();
		if (valorBaseCalculo != null && this.ir != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.ir)).divide(cem).getValue();
			// valorIr = new
			// Money(LkNfeUtil.arredodamentoValorDuasCasas(valor.getValue().doubleValue()));
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorIr = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_IR));
		}

		return valorIr;
	}

	@Transient
	@DisplayName("Valor dos Servi�os")
	public Money getValorTotalServicos() {
		if (this.listaItens == null || this.listaItens.isEmpty()) {
			return null;
		}

		Money total = new Money(0D);

		for (NotaFiscalServicoItem itemServico : this.listaItens) {
			Money totalParcial = itemServico.getTotalParcial();
			if (totalParcial != null)
				total = total.add(totalParcial);
		}
		valorTotalServicos = total;
		return valorTotalServicos;
	}

	@Transient
	public String getDescricaoServico() {
		if (this.listaItens == null || this.listaItens.isEmpty()) {
			return null;
		}
		StringBuilder discriminacao = new StringBuilder();
		for (NotaFiscalServicoItem it : this.listaItens) {
			if (it.getDescricao() != null && !it.getDescricao().equals("")) {
				discriminacao.append(it.getDescricao() + " ");
			} else {
				if (it.getMaterial() != null && it.getMaterial().getNome() != null && !it.getMaterial().getNome().equals("")) {
					discriminacao.append(it.getMaterial().getNome() + " ");
				}
			}
		}
		
		return discriminacao.toString();
	}

	@Transient
	@Override
	@DisplayName("Total desta Nota")
	public Money getValorNota() {
		Money totalNota = new Money(0D);

		Money valorTotalServicos = this.getValorTotalServicos();
		if (valorTotalServicos != null)
			totalNota = valorTotalServicos;

		Money valorImpostoLocal = this.getValorImposto();
		if (valorImpostoLocal != null)
			totalNota = totalNota.subtract(valorImpostoLocal);

		Money outrasRetencoes = this.getOutrasretencoes();
		if (outrasRetencoes != null)
			totalNota = totalNota.subtract(outrasRetencoes);

		Money descontoincondicionado = this.getDescontoincondicionado();
		if (descontoincondicionado != null)
			totalNota = totalNota.subtract(descontoincondicionado);

		Money descontocondicionado = this.getDescontocondicionado();
		if (descontocondicionado != null)
			totalNota = totalNota.subtract(descontocondicionado);

		valorNota = totalNota;

		return valorNota;
	}

	@Transient
	public Money getValorNotaGinfes() {
		Money totalNota = new Money(0D);

		Money valorTotalServicos = this.getValorTotalServicos();
		if (valorTotalServicos != null)
			totalNota = valorTotalServicos;

		Money valorImpostoLocal = this.getValorImposto(BigDecimal.ROUND_HALF_EVEN);
		if (valorImpostoLocal != null)
			totalNota = totalNota.subtract(valorImpostoLocal);

		Money outrasRetencoes = this.getOutrasretencoes();
		if (outrasRetencoes != null)
			totalNota = totalNota.subtract(outrasRetencoes);

		Money descontoincondicionado = this.getDescontoincondicionado();
		if (descontoincondicionado != null)
			totalNota = totalNota.subtract(descontoincondicionado);

		Money descontocondicionado = this.getDescontocondicionado();
		if (descontocondicionado != null)
			totalNota = totalNota.subtract(descontocondicionado);

		valorNota = totalNota;

		return valorNota;
	}

	@Transient
	public Money getValorNotaRoundFloor() {
		Money totalNota = new Money(0D);

		Money valorTotalServicos = this.getValorTotalServicos();
		if (valorTotalServicos != null)
			totalNota = valorTotalServicos;

		Money valorImpostoLocal = this.getValorImposto(BigDecimal.ROUND_FLOOR);
		if (valorImpostoLocal != null)
			totalNota = totalNota.subtract(valorImpostoLocal);

		Money outrasRetencoes = this.getOutrasretencoes();
		if (outrasRetencoes != null)
			totalNota = totalNota.subtract(outrasRetencoes);

		Money descontoincondicionado = this.getDescontoincondicionado();
		if (descontoincondicionado != null)
			totalNota = totalNota.subtract(descontoincondicionado);

		Money descontocondicionado = this.getDescontocondicionado();
		if (descontocondicionado != null)
			totalNota = totalNota.subtract(descontocondicionado);

		valorNota = totalNota;

		return valorNota;
	}

	@Transient
	public Money getValorNotaMunicipal() {
		Money totalNota = new Money(0D);

		Money valorTotalServicos = this.getValorTotalServicos();
		if (valorTotalServicos != null)
			totalNota = valorTotalServicos;

		Money valorImpostoFederais = this.getValorImpostosFederais();
		if (valorImpostoFederais != null)
			totalNota = totalNota.subtract(valorImpostoFederais);

		BigDecimal valorIssLocal = (this.getValorIss() != null ? this.getValorIss().getValue().abs()
				.setScale(2, BigDecimal.ROUND_HALF_UP) : new BigDecimal(0));
		if (valorIssLocal != null && getIncideiss() != null && getIncideiss())
			totalNota = totalNota.subtract(new Money(valorIssLocal));

		if (getDescontocondicionado() != null)
			totalNota = totalNota.subtract(getDescontocondicionado());

		if (getDescontoincondicionado() != null)
			totalNota = totalNota.subtract(getDescontoincondicionado());

		return totalNota;
	}

	@Transient
	public Money getValorDescontoNotaMunicipal() {
		Money totalDesconto = new Money(0D);

		if (getDescontocondicionado() != null)
			totalDesconto = totalDesconto.add(getDescontocondicionado());

		if (getDescontoincondicionado() != null)
			totalDesconto = totalDesconto.add(getDescontoincondicionado());

		return totalDesconto;
	}

	@Transient
	public Money getBaseCalculoNotaMunicipal() {
		Money baseCalculo = new Money(0D);
		baseCalculo = baseCalculo.add(getValorBruto());

		if (getDescontoincondicionado() != null)
			baseCalculo = baseCalculo.subtract(getDescontoincondicionado());

		if (getDeducao() != null)
			baseCalculo = baseCalculo.subtract(getDeducao());

		return baseCalculo;
	}

	/**
	 * Copia apenas os itens para c�lculo de valor de outra nota fiscal de
	 * servi�o.
	 * 
	 * @param nota
	 * @author Hugo Ferreira
	 */
	@Override
	public void copiarItensDeValor(Nota nota) {
		if (nota == null || !(nota instanceof NotaFiscalServico)) {
			throw new SinedException("N�o foi poss�vel copiar os itens de valor na nota.");
		}

		NotaFiscalServico outra = (NotaFiscalServico) nota;

		this.valorNotaTransient = outra.valorNotaTransient;

		this.listaItens = outra.listaItens;
		this.listaNotaDocumento = outra.listaNotaDocumento;
		this.iss = outra.iss;
		this.inss = outra.inss;
		this.ir = outra.ir;
		this.csll = outra.csll;
		this.icms = outra.icms;
		this.pis = outra.pis;
		this.cofins = outra.cofins;
		this.pisretido = outra.pisretido;
		this.cofinsretido = outra.cofinsretido;
		this.incideiss = outra.incideiss;
		this.incideinss = outra.incideinss;
		this.incideir = outra.incideir;
		this.incidecsll = outra.incidecsll;
		this.incideicms = outra.incideicms;
		this.incidepis = outra.incidepis;
		this.incidecofins = outra.incidecofins;

		this.basecalculo = outra.getBasecalculo();
		this.basecalculocofins = outra.getBasecalculocofins();
		this.basecalculocsll = outra.getBasecalculocsll();
		this.basecalculoicms = outra.getBasecalculoicms();
		this.basecalculoinss = outra.getBasecalculoinss();
		this.basecalculoir = outra.getBasecalculoir();
		this.basecalculoiss = outra.getBasecalculoiss();
		this.basecalculopis = outra.getBasecalculopis();
	}

	@Transient
	public Frequencia getFrequencia() {
		return frequencia;
	}

	@Transient
	public Boolean getFaturamentolote() {
		return faturamentolote;
	}

	@Transient
	public Conta getConta() {
		return conta;
	}

	public void setFaturamentolote(Boolean faturamentolote) {
		this.faturamentolote = faturamentolote;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setListaContrato(List<Contrato> listaContrato) {
		this.listaContrato = listaContrato;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	@Transient
	@Override
	public Money getValorBruto() {
		return this.getValorTotalServicos();
	}

	public Double aliquotaIss_Iss() {
		return getIss() != null ? getIss() : 0.0;
	}

	public String bairroCliente_Iss() {
		return getEnderecoCliente().getBairro() != null ? getEnderecoCliente().getBairro() : "";
	}

	public String cepCliente_Iss() {
		return getEnderecoCliente().getCep() != null ? getEnderecoCliente().getCep().getValue() : "";
	}

	public String cidadeCliente_Iss() {
		return getEnderecoCliente().getMunicipio() != null && getEnderecoCliente().getMunicipio().getNome() != null ? getEnderecoCliente()
				.getMunicipio().getNome() : "";
	}

	public String cnpjCliente_Iss() {
		return getCliente().getCnpj() != null ? getCliente().getCnpj().getValue() : "";
	}

	public String complementoCliente_Iss() {
		return getEnderecoCliente().getComplemento() != null ? getEnderecoCliente().getComplemento() : "";
	}

	public String cpfCliente_Iss() {
		return getCliente().getCpf() != null ? getCliente().getCpf().toString() : "";
	}

	public Date dataEmissao_Iss() {
		return getDtEmissao();
	}

	public Boolean incideIss_Iss() {
		return getIncideiss() != null ? getIncideiss() : false;
	}

	public String inscMunicipalCliente_Iss() {
		return getCliente().getInscricaomunicipal() != null ? getCliente().getInscricaomunicipal() : "";
	}

	public String nomeCliente_Iss() {
		return getCliente().getRazaosocial() != null ? getCliente().getRazaosocial() : getCliente().getNome();
	}

	public String numeroNota_Iss() {
		return getNumero() != null ? getNumero() : "";
	}

	public String numeroResidenciaCliente_Iss() {
		return getEnderecoCliente().getNumero() != null ? getEnderecoCliente().getNumero() : "";
	}

	public String ruaCliente_Iss() {
		return getEnderecoCliente().getLogradouro() != null ? getEnderecoCliente().getLogradouro() : "";
	}

	public String siglaEstadoCliente_Iss() {
		return getEnderecoCliente().getMunicipio() != null ? getEnderecoCliente().getMunicipio().getUf().getSigla() : "";
	}

	public String tipoRuaCliente_Iss() {
		return "RUA";
	}

	public Double valorBrutoNota_Iss() {
		return getValorBruto().getValue().doubleValue();
	}

	public Double valorServico_Iss() {
		return getValorBruto().getValue().doubleValue();
	}

	@Transient
	public Boolean getNaogeranota() {
		return naogeranota;
	}

	public void setNaogeranota(Boolean naogeranota) {
		this.naogeranota = naogeranota;
	}

	@Transient
	public Boolean getFromRequisicao() {
		return fromRequisicao;
	}

	public void setFromRequisicao(Boolean fromRequisicao) {
		this.fromRequisicao = fromRequisicao;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	@Transient
	@DisplayName("N� NFS-e")
	public String getNumeroNfse() {
		return numeroNfse;
	}

	@Transient
	@DisplayName("C�digo de verifica��o da NFS-e")
	public String getCodVerificacao() {
		return codVerificacao;
	}

	public void setNumeroNfse(String numeroNfse) {
		this.numeroNfse = numeroNfse;
	}

	public void setCodVerificacao(String codVerificacao) {
		this.codVerificacao = codVerificacao;
	}

	@Transient
	public String getDiaEmissao() {
		return diaEmissao;
	}

	@Transient
	public String getMesEmissao() {
		return mesEmissao;
	}

	@Transient
	public String getAnoEmissao() {
		return anoEmissao;
	}

	@Transient
	public List<ImpostoBean> getListaImpostos() {
		return listaImpostos;
	}

	public void setListaImpostos(List<ImpostoBean> listaImpostos) {
		this.listaImpostos = listaImpostos;
	}

	@Required
	@DisplayName("Base de c�lculo geral")
	public Money getBasecalculo() {
		return basecalculo;
	}

	@DisplayName("Desconto condicionado")
	public Money getDescontocondicionado() {
		return descontocondicionado;
	}

	@DisplayName("Desconto incondicionado")
	public Money getDescontoincondicionado() {
		return descontoincondicionado;
	}

	@DisplayName("Valor das dedu��es")
	public Money getDeducao() {
		return deducao;
	}

	@DisplayName("Outras reten��es")
	public Money getOutrasretencoes() {
		return outrasretencoes;
	}
	
	@DisplayName("Incentivo Fiscal ISS (%)")
	public Double getIncentivoFiscalIss() {
		return incentivoFiscalIss;
	}

	@DisplayName("C�digo CNAE")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcodigocnae")
	public Codigocnae getCodigocnaeBean() {
		return codigocnaeBean;
	}

	@DisplayName("C�digo de tributa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcodigotributacao")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	@DisplayName("Item da lista de servi�o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cditemlistaservico")
	public Itemlistaservico getItemlistaservicoBean() {
		return itemlistaservicoBean;
	}

	public void setCodigocnaeBean(Codigocnae codigocnaeBean) {
		this.codigocnaeBean = codigocnaeBean;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setItemlistaservicoBean(Itemlistaservico itemlistaservicoBean) {
		this.itemlistaservicoBean = itemlistaservicoBean;
	}

	public void setBasecalculo(Money basecalculo) {
		this.basecalculo = basecalculo;
	}

	public void setDescontocondicionado(Money descontocondicionado) {
		this.descontocondicionado = descontocondicionado;
	}

	public void setDescontoincondicionado(Money descontoincondicionado) {
		this.descontoincondicionado = descontoincondicionado;
	}

	public void setDeducao(Money deducao) {
		this.deducao = deducao;
	}

	public void setOutrasretencoes(Money outrasretencoes) {
		this.outrasretencoes = outrasretencoes;
	}
	
	public void setIncentivoFiscalIss(Double incentivoFiscalIss) {
		this.incentivoFiscalIss = incentivoFiscalIss;
	}

	public void setDiaEmissao(String diaEmissao) {
		this.diaEmissao = diaEmissao;
	}

	public void setMesEmissao(String mesEmissao) {
		this.mesEmissao = mesEmissao;
	}

	public void setAnoEmissao(String anoEmissao) {
		this.anoEmissao = anoEmissao;
	}

	@Transient
	public String getInscricaoestadual() {
		if (inscricaoestadual == null) {
			if (enderecoCliente != null && enderecoCliente.getInscricaoestadual() != null
					&& !enderecoCliente.getInscricaoestadual().trim().equals("")) {
				inscricaoestadual = enderecoCliente.getInscricaoestadual();
			} else if (cliente != null && cliente.getInscricaoestadual() != null
					&& !cliente.getInscricaoestadual().trim().equals("")) {
				inscricaoestadual = cliente.getInscricaoestadual();
			}
		}
		return inscricaoestadual;
	}

	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}

	@Transient
	@DisplayName("Usar n�mero sequencial?")
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}

	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}

	@Transient
	public String getNomeNF() {
		return nomeNF;
	}

	public void setNomeNF(String nomeNF) {
		this.nomeNF = nomeNF;
	}

	@DisplayName("Regime especial de tributa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdregimetributacao")
	public Regimetributacao getRegimetributacao() {
		return regimetributacao;
	}

	@DisplayName("Natureza da opera��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	@DisplayName("Munic�pio da incid�ncia do ISSQN")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmunicipioissqn")
	public Municipio getMunicipioissqn() {
		return municipioissqn;
	}

	public void setRegimetributacao(Regimetributacao regimetributacao) {
		this.regimetributacao = regimetributacao;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setMunicipioissqn(Municipio municipioissqn) {
		this.municipioissqn = municipioissqn;
	}

	@Transient
	public Integer getCddocumento() {
		return cddocumento;
	}

	public void setCddocumento(Integer cddocumento) {
		this.cddocumento = cddocumento;
	}

	@Transient
	public String getWhereInCddocumento() {
		return whereInCddocumento;
	}

	public void setWhereInCddocumento(String whereInCddocumento) {
		this.whereInCddocumento = whereInCddocumento;
	}

	@Transient
	public Boolean getFromProducaodiaria() {
		return fromProducaodiaria;
	}

	public void setFromProducaodiaria(Boolean fromProducaodiaria) {
		this.fromProducaodiaria = fromProducaodiaria;
	}

	@DisplayName("Inscri��o Municipal")
	@MaxLength(20)
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}

	@Transient
	public Boolean getFluxoFaturarPorMedicao() {
		return fluxoFaturarPorMedicao;
	}

	public void setFluxoFaturarPorMedicao(Boolean fluxoFaturarPorMedicao) {
		this.fluxoFaturarPorMedicao = fluxoFaturarPorMedicao;
	}

	@Required
	@DisplayName("Base de c�lculo IR")
	public Money getBasecalculoir() {
		return basecalculoir;
	}

	@Required
	@DisplayName("Base de c�lculo ISS")
	public Money getBasecalculoiss() {
		return basecalculoiss;
	}

	@Required
	@DisplayName("Base de c�lculo PIS")
	public Money getBasecalculopis() {
		return basecalculopis;
	}

	@Required
	@DisplayName("Base de c�lculo COFINS")
	public Money getBasecalculocofins() {
		return basecalculocofins;
	}

	@Required
	@DisplayName("Base de c�lculo CSLL")
	public Money getBasecalculocsll() {
		return basecalculocsll;
	}

	@Required
	@DisplayName("Base de c�lculo INSS")
	public Money getBasecalculoinss() {
		return basecalculoinss;
	}

	@Required
	@DisplayName("Base de c�lculo ICMS")
	public Money getBasecalculoicms() {
		return basecalculoicms;
	}

	public void setBasecalculoir(Money basecalculoir) {
		this.basecalculoir = basecalculoir;
	}

	public void setBasecalculoiss(Money basecalculoiss) {
		this.basecalculoiss = basecalculoiss;
	}

	public void setBasecalculopis(Money basecalculopis) {
		this.basecalculopis = basecalculopis;
	}

	public void setBasecalculocofins(Money basecalculocofins) {
		this.basecalculocofins = basecalculocofins;
	}

	public void setBasecalculocsll(Money basecalculocsll) {
		this.basecalculocsll = basecalculocsll;
	}

	public void setBasecalculoinss(Money basecalculoinss) {
		this.basecalculoinss = basecalculoinss;
	}

	public void setBasecalculoicms(Money basecalculoicms) {
		this.basecalculoicms = basecalculoicms;
	}

	@Transient
	public String getSituacaosped() {
		return situacaosped;
	}

	public void setSituacaosped(String situacaosped) {
		this.situacaosped = situacaosped;
	}

	@DisplayName("C�d. Sit. Trib. PIS")
	public Tipocobrancapis getCstpis() {
		return cstpis;
	}

	@DisplayName("C�d. Sit. Trib. COFINS")
	public Tipocobrancacofins getCstcofins() {
		return cstcofins;
	}

	public void setCstpis(Tipocobrancapis cstpis) {
		this.cstpis = cstpis;
	}

	public void setCstcofins(Tipocobrancacofins cstcofins) {
		this.cstcofins = cstcofins;
	}

	@DisplayName("Tipo de pagamento")
	public Indicadortipopagamento getIndicadortipopagamento() {
		return indicadortipopagamento;
	}

	public void setIndicadortipopagamento(Indicadortipopagamento indicadortipopagamento) {
		this.indicadortipopagamento = indicadortipopagamento;
	}

	@Transient
	public Boolean getHaveArquivonfnota() {
		return haveArquivonfnota;
	}

	public void setHaveArquivonfnota(Boolean haveArquivonfnota) {
		this.haveArquivonfnota = haveArquivonfnota;
	}

	@Transient
	public String getVendas() {
		return vendas;
	}

	public void setVendas(String vendas) {
		this.vendas = vendas;
	}

	@Transient
	public String getContas() {
		return contas;
	}

	public void setContas(String contas) {
		this.contas = contas;
	}

	@Transient
	public String getIdcontrato() {
		return idcontrato;
	}

	public void setIdcontrato(String idcontrato) {
		this.idcontrato = idcontrato;
	}

	@Transient
	public Date getDtvencimentocontareceber() {
		return dtvencimentocontareceber;
	}

	public void setDtvencimentocontareceber(Date dtvencimentocontareceber) {
		this.dtvencimentocontareceber = dtvencimentocontareceber;
	}

	public String subQueryProjeto() {
		return "select notafiscalservicosubQueryProjeto.cdNota " + "from NotaFiscalServico notafiscalservicosubQueryProjeto "
				+ "left outer join notafiscalservicosubQueryProjeto.projeto projetosubQueryProjeto "
				+ "where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public Aux_apuracaoimposto getAux_apuracaoimposto() {
		return aux_apuracaoimposto;
	}

	public void setAux_apuracaoimposto(Aux_apuracaoimposto auxApuracaoimposto) {
		aux_apuracaoimposto = auxApuracaoimposto;
	}

	@Transient
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@Transient
	public String getPneusJaAssociado() {
		return pneusJaAssociado;
	}
	
	public void setPneusJaAssociado(String pneusJaAssociado) {
		this.pneusJaAssociado = pneusJaAssociado;
	}
	
	@DisplayName("PIS A PAGAR(%)")
	public Double getPisretido() {
		return pisretido;
	}

	@DisplayName("COFINS A PAGAR(%)")
	public Double getCofinsretido() {
		return cofinsretido;
	}

	public void setPisretido(Double pisretido) {
		this.pisretido = pisretido;
	}

	public void setCofinsretido(Double cofinsretido) {
		this.cofinsretido = cofinsretido;
	}

	@Transient
	@DisplayName("VALOR PIS A PAGAR")
	public Money getValorPisRetido() {
		Money valorBaseCalculo = this.getBasecalculopis();
		if (valorBaseCalculo != null && this.pisretido != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.pisretido)).divide(cem).getValue();
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorPisRetido = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_PIS));
		}

		return valorPisRetido;
	}

	@Transient
	@DisplayName("VALOR COFINS A PAGAR")
	public Money getValorCofinsRetido() {
		Money valorBaseCalculo = this.getBasecalculocofins();
		if (valorBaseCalculo != null && this.cofinsretido != null) {
			Money cem = new Money(100D);
			BigDecimal valor = valorBaseCalculo.multiply(new Money(this.cofinsretido)).divide(cem).getValue();
			valor = valor.setScale(5, BigDecimal.ROUND_HALF_UP);
			valorCofinsRetido = new Money(SinedUtil.roundImpostoByParametro(valor, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_COFINS));
		}

		return valorCofinsRetido;
	}

	@Transient
	public Boolean getFluxoFaturarlocacao() {
		return fluxoFaturarlocacao;
	}

	public void setFluxoFaturarlocacao(Boolean fluxoFaturarlocacao) {
		this.fluxoFaturarlocacao = fluxoFaturarlocacao;
	}

	@Transient
	public Boolean getFromProposta() {
		return fromProposta;
	}

	public void setFromProposta(Boolean fromProposta) {
		this.fromProposta = fromProposta;
	}

	@Transient
	public String getCodigonaturezareceitaTrans(Faixaimpostocontrole faixaimpostocontrole) {
		if (this.getGrupotributacao() != null && this.getGrupotributacao().getListaGrupotributacaoimposto() != null
				&& !this.getGrupotributacao().getListaGrupotributacaoimposto().isEmpty()) {
			for (Grupotributacaoimposto grupotributacaoimposto : this.getGrupotributacao().getListaGrupotributacaoimposto()) {
				if (grupotributacaoimposto.getControle() != null
						&& faixaimpostocontrole.equals(grupotributacaoimposto.getControle())) {
					return grupotributacaoimposto.getCodigonaturezareceita();
				}
			}
		}
		return "999";
	}

	@Transient
	public Integer getCdvendaassociada() {
		return cdvendaassociada;
	}

	@Transient
	public List<Venda> getListaVenda() {
		return listaVenda;
	}

	@Transient
	public Boolean getClienteIncidirISS() {
		return clienteIncidirISS;
	}

	public void setCdvendaassociada(Integer cdvendaassociada) {
		this.cdvendaassociada = cdvendaassociada;
	}

	public void setListaVenda(List<Venda> listaVenda) {
		this.listaVenda = listaVenda;
	}

	public void setClienteIncidirISS(Boolean clienteIncidirISS) {
		this.clienteIncidirISS = clienteIncidirISS;
	}

	@Transient
	public String getRomaneios() {
		return romaneios;
	}

	public void setRomaneios(String romaneios) {
		this.romaneios = romaneios;
	}

	@Transient
	public Boolean getSomenteservico() {
		return somenteservico;
	}

	public void setSomenteservico(Boolean somenteservico) {
		this.somenteservico = somenteservico;
	}

	@Transient
	public Requisicao getRequisicao() {
		return requisicao;
	}

	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}

	@Transient
	public Materialrequisicao getMaterialrequisicao() {
		return materialrequisicao;
	}

	public void setMaterialrequisicao(Materialrequisicao materialrequisicao) {
		this.materialrequisicao = materialrequisicao;
	}

	@Transient
	public Integer getCdNotaFiscalServicoLoteRPS() {
		return cdNotaFiscalServicoLoteRPS;
	}

	public void setCdNotaFiscalServicoLoteRPS(Integer cdNotaFiscalServicoLoteRPS) {
		this.cdNotaFiscalServicoLoteRPS = cdNotaFiscalServicoLoteRPS;
	}

	@DisplayName("Hora de emiss�o")
	public Hora getHremissao() {
		return hremissao;
	}

	public void setHremissao(Hora hremissao) {
		this.hremissao = hremissao;
	}

	@Transient
	public Date getDtinicio() {
		return dtinicio;
	}

	@Transient
	public Date getDtfim() {
		return dtfim;
	}

	@Transient
	public List<NotaStatus> getListaSituacao() {
		return listaSituacao;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setListaSituacao(List<NotaStatus> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}

	@DisplayName("Cadastrar dados de cobran�a")
	public Boolean getCadastrarcobranca() {
		return cadastrarcobranca;
	}

	public void setCadastrarcobranca(Boolean cadastrarcobranca) {
		this.cadastrarcobranca = cadastrarcobranca;
	}
	
	@Transient
	public Boolean getAgruparContas() {
		return agruparContas;
	}

	@Transient
	public Integer getQtdParcelas() {
		return qtdParcelas;
	}
	
	@Transient
	public String getWhereInCdvenda() {
		return whereInCdvenda;
	}

	public void setAgruparContas(Boolean agruparContas) {
		this.agruparContas = agruparContas;
	}
	
	public void setQtdParcelas(Integer qtdParcelas) {
		this.qtdParcelas = qtdParcelas;
	}
	
	public void setWhereInCdvenda(String whereInCdvenda) {
		this.whereInCdvenda = whereInCdvenda;
	}

	@Transient
	public boolean getExisteDiferencaduplicata() {
		return Boolean.TRUE.equals(cadastrarcobranca) && getDiferencaduplicata() != null
				&& getDiferencaduplicata().compareTo(new Money()) != 0;
	}

	@Transient
	public String getDescriptionAutocomplete() {
		return getNumeroNfse() + " - " + getNomeCliente();
	}

	@Transient
	public Money getValorTotalDescontoNotaMunicipal() {
		Money totalDesconto = new Money(0D);

		if (getDescontocondicionado() != null)
			totalDesconto = totalDesconto.add(getDescontocondicionado());

		if (getDescontoincondicionado() != null)
			totalDesconto = totalDesconto.add(getDescontoincondicionado());

		if (SinedUtil.isListNotEmpty(getListaItens())) {
			for (NotaFiscalServicoItem item : getListaItens()) {
				if (item.getDesconto() != null) {
					totalDesconto = totalDesconto.add(item.getDesconto());
				}
			}
		}

		return totalDesconto;
	}

	@Transient
	public Material getMaterialSPED() {
		// M�todo que retorna o Material da Nota Fiscal de Servi�o. A Nota pode
		// vir da Venda ou Contrato. Pegar o primeiro Material que possui Conta
		// Cont�bil e adicionar seu Identificador no arquivo
		if (getListaItens() != null) {
			for (NotaFiscalServicoItem nfsi : getListaItens()) {
				if (nfsi.getVendamaterial() != null && nfsi.getVendamaterial().getMaterial() != null
						&& nfsi.getVendamaterial().getMaterial().getContaContabil() != null) {
					return nfsi.getVendamaterial().getMaterial();
				} else if (getListaNotaContrato() != null && !getListaNotaContrato().isEmpty()) {
					Contrato contrato = getListaNotaContrato().get(0).getContrato();
					if (contrato.getListaContratomaterial() != null && !contrato.getListaContratomaterial().isEmpty()) {
						for (Contratomaterial contratomaterial : contrato.getListaContratomaterial()) {
							if (contratomaterial.getServico() != null
									&& contratomaterial.getServico().getContaContabil() != null) {
								return contratomaterial.getServico();
							}
						}
					}
				}
			}
		}

		return null;
	}

	@Transient
	public ContaContabil getContagerencialContabilSPED() {
		// M�todo que retorna o Material da Nota Fiscal de Servi�o. A Nota pode
		// vir da Venda ou Contrato. Pegar o primeiro Material que possui Conta
		// Cont�bil e adicionar seu Identificador no arquivo
		if (getListaItens() != null) {
			for (NotaFiscalServicoItem nfsi : getListaItens()) {
				if (nfsi.getVendamaterial() != null && nfsi.getVendamaterial().getMaterial() != null
						&& nfsi.getVendamaterial().getMaterial().getContaContabil() != null) {
					return nfsi.getVendamaterial().getMaterial().getContaContabil();
				} else if (getListaNotaContrato() != null && !getListaNotaContrato().isEmpty()) {
					Contrato contrato = getListaNotaContrato().get(0).getContrato();
					return contrato.getContagerencialContabilSPED();
				} else if (Hibernate.isInitialized(getListaNotaDocumento()) && SinedUtil.isListNotEmpty(getListaNotaDocumento())) {
					NotaDocumento notaDocumento = getListaNotaDocumento().get(0);
					if (notaDocumento.getDocumento() != null
							&& SinedUtil.isListNotEmpty(notaDocumento.getDocumento().getListaDocumentoOrigem())) {
						Documentoorigem documentoorigem = notaDocumento.getDocumento().getListaDocumentoOrigem().get(0);
						if (documentoorigem.getContrato() != null) {
							Contrato contrato = documentoorigem.getContrato();
							return contrato.getContagerencialContabilSPED();
						}
					}
				}
			}
		}

		return null;
	}
}