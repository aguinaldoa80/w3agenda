package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_arquivonfnotaerrocancelamento", sequenceName = "sq_arquivonfnotaerrocancelamento")
public class Arquivonfnotaerrocancelamento {
	
	protected Integer cdarquivonfnotaerrocancelamento;
	protected Arquivonfnota arquivonfnota;
	protected String mensagem;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivonfnotaerrocancelamento")
	public Integer getCdarquivonfnotaerrocancelamento() {
		return cdarquivonfnotaerrocancelamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivonfnota")
	public Arquivonfnota getArquivonfnota() {
		return arquivonfnota;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setArquivonfnota(Arquivonfnota arquivonfnota) {
		this.arquivonfnota = arquivonfnota;
	}
	
	public void setCdarquivonfnotaerrocancelamento(Integer cdarquivonfnotaerrocancelamento) {
		this.cdarquivonfnotaerrocancelamento = cdarquivonfnotaerrocancelamento;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
