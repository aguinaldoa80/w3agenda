package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_lcdprarquivo", sequenceName="sq_lcdprarquivo")
public class LcdprArquivo implements Log {
	private Integer cdLcdprArquivo;
	private Arquivo arquivo;
	private Date dtinicio;
	private Date dtfim;
	private Colaborador colaborador;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_lcdprarquivo")
	public Integer getCdLcdprArquivo() {
		return cdLcdprArquivo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Data de início")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName("Data final")
	public Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Proprietário")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setCdLcdprArquivo(Integer cdLcdprArquivo) {
		this.cdLcdprArquivo = cdLcdprArquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
}
