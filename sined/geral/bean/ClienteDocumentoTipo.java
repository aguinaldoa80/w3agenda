package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_ClienteDocumentoTipo", sequenceName = "sq_ClienteDocumentoTipo")
public class ClienteDocumentoTipo {
	
	protected Integer cdclientedocumentotipo;
	protected Cliente cliente;
	protected Documentotipo documentotipo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ClienteDocumentoTipo")
	public Integer getCdclientedocumentotipo() {
		return cdclientedocumentotipo;
	}
	
	
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Cliente getCliente() {
		return cliente;
	}

	@JoinColumn(name="cddocumentotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	@DisplayName("Forma de Pagamento")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	public void setCdclientedocumentotipo(Integer cdclientedocumentotipo) {
		this.cdclientedocumentotipo = cdclientedocumentotipo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
}
