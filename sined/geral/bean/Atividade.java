package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_atividade", sequenceName="sq_atividade")
public class Atividade implements Log, PermissaoProjeto{

	protected Integer cdatividade;
	protected String nome;
	protected String descricao;
	protected Date dtinicio;
	protected Date dtfim;
	protected Planejamento planejamento;
	protected Tarefa tarefa;
	protected Colaborador colaborador;
	protected Atividadetipo atividadetipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Projeto projeto;
	
	// TRANSIENTE
	protected Date dtdia;
	protected Integer indice;
	
	@Id
	@GeneratedValue(generator="sq_atividade", strategy=GenerationType.AUTO)
	public Integer getCdatividade() {
		return cdatividade;
	}
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	@DisplayName("Descri��o")
	@MaxLength(2000)
	@Required
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data de fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	@JoinColumn(name="cdplanejamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@JoinColumn(name="cdtarefa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tarefa getTarefa() {
		return tarefa;
	}
	@DisplayName("Respons�vel")
	@Required
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Tipo de atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@Transient
	@DescriptionProperty(usingFields={"descricao"})
	public String getDescription(){
		return descricao != null ? descricao.length() > 50 ? descricao.substring(0, 50) : descricao : null;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdatividade(Integer cdatividade) {
		this.cdatividade = cdatividade;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public String subQueryProjeto() {
		return "select atividadesubQueryProjeto.cdatividade " +
				"from Atividade atividadesubQueryProjeto " +
				"join atividadesubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public Integer getCdatividadeaux() {
		return getCdatividade();
	}
	
	@Transient
	public Date getDtdia() {
		return dtdia;
	}
	
	@DisplayName ("Projeto")
	@Required
	@JoinColumn(name="cdprojeto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Transient
	public Integer getIndice() {
		return indice;
	}
	
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	
	public void setDtdia(Date dtdia) {
		this.dtdia = dtdia;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCdatividadeaux(Integer cdatividadeaux) {
		setCdatividade(cdatividadeaux);
	}
}
