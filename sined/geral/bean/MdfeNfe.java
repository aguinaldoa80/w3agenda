package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfenfe", sequenceName = "sq_mdfenfe")
public class MdfeNfe {

	protected Integer cdMdfeNfe;
	protected Mdfe mdfe;
	protected String chaveAcesso;
	protected Boolean indicadorReentrega;
	protected String idNfe;
	protected Municipio municipio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfenfe")
	public Integer getCdMdfeNfe() {
		return cdMdfeNfe;
	}
	public void setCdMdfeNfe(Integer cdMdfeNfe) {
		this.cdMdfeNfe = cdMdfeNfe;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=44)
	@DisplayName("Chave de acesso")
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	
	@DisplayName("Indicador de reentrega")
	public Boolean getIndicadorReentrega() {
		return indicadorReentrega;
	}
	public void setIndicadorReentrega(Boolean indicadorReentrega) {
		this.indicadorReentrega = indicadorReentrega;
	}

	@DisplayName("ID do NFe")
	public String getIdNfe() {
		return idNfe;
	}
	public void setIdNfe(String idNfe) {
		this.idNfe = idNfe;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeTransporteNfe> getListaInformacaoUnidadeTransporteNfe(MdfeNfe mdfeNfe, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeTransporteNfe> list = new ArrayList<MdfeInformacaoUnidadeTransporteNfe>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteNfe())){
			for(MdfeInformacaoUnidadeTransporteNfe item : mdfe.getListaInformacaoUnidadeTransporteNfe()){
				if(item.getIdNfe().equals(mdfeNfe.getIdNfe())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeProdutoPerigosoNfe> getListaProdutoPerigosoNfe(MdfeNfe mdfeNfe, Mdfe mdfe) {
		List<MdfeProdutoPerigosoNfe> list = new ArrayList<MdfeProdutoPerigosoNfe>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeTransporteNfe())){
			for(MdfeProdutoPerigosoNfe item : mdfe.getListaProdutoPerigosoNfe()){
				if(item.getIdNfe().equals(mdfeNfe.getIdNfe())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@DisplayName("Município de descarregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
