package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@DisplayName("Tipo")
@Entity
public class Beneficiotipo {
	
	private Integer cdbeneficiotipo;
	private String nome;
	
	@Id
	public Integer getCdbeneficiotipo() {
		return cdbeneficiotipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdbeneficiotipo(Integer cdbeneficiotipo) {
		this.cdbeneficiotipo = cdbeneficiotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		try {
			return getCdbeneficiotipo().equals(((Beneficiotipo) obj).getCdbeneficiotipo());
		} catch (Exception e) {
			return false;
		}
	}
}