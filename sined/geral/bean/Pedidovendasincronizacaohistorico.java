package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_pedidovendasincronizacaohistorico", sequenceName = "sq_pedidovendasincronizacaohistorico")
public class Pedidovendasincronizacaohistorico implements Log{

	protected Integer cdpedidovendasincronizacaohistorico;
	protected Pedidovendasincronizacao pedidovendasincronizacao;
	protected String observacao;
	protected String msglog;
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendasincronizacaohistorico")
	@DisplayName("C�digo")
	public Integer getCdpedidovendasincronizacaohistorico() {
		return cdpedidovendasincronizacaohistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendasincronizacao")
	public Pedidovendasincronizacao getPedidovendasincronizacao() {
		return pedidovendasincronizacao;
	}
	@DisplayName("Observa��es")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdpedidovendasincronizacaohistorico(Integer cdpedidovendasincronizacaohistorico) {
		this.cdpedidovendasincronizacaohistorico = cdpedidovendasincronizacaohistorico;
	}
	public void setPedidovendasincronizacao(Pedidovendasincronizacao pedidovendasincronizacao) {
		this.pedidovendasincronizacao = pedidovendasincronizacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	public String getMsglog() {
		return msglog;
	}
	public void setMsglog(String msglog) {
		this.msglog = msglog;
	}
}
