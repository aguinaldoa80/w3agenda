package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_pedidovendaoffline", sequenceName = "sq_pedidovendaoffline")
@DisplayName("Pedido de venda")
public class Pedidovendaoffline implements Log{

	private Integer cdpedidovendaoffline;
	private String json;
	private String msgErro;
	private String protocolooffline;
	private Pedidovenda pedidovenda;//pedido de venda resultante
	private Timestamp dtcriacaooffline;
	
	private List<Materialvenda> listaSugestaovenda;
	
	//log
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;



	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendaoffline")
	public Integer getCdpedidovendaoffline() {
		return cdpedidovendaoffline;
	}
	
	
	public String getJson() {
		return json;
	}
	
	@DisplayName("Mensagem de erro")
	public String getMsgErro() {
		return msgErro;
	}

	@DisplayName("Protocolo")
	public String getProtocolooffline() {
		return protocolooffline;
	}

	@DisplayName("Data de Cria��o Offline")
	public Timestamp getDtcriacaooffline() {
		return dtcriacaooffline;
	}
	
	@ManyToOne
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	public void setCdpedidovendaoffline(Integer cdpedidovendaoffline) {
		this.cdpedidovendaoffline = cdpedidovendaoffline;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}
	public void setProtocolooffline(String protocolooffline) {
		this.protocolooffline = protocolooffline;
	}
	public void setDtcriacaooffline(Timestamp dtcriacaooffline) {
		this.dtcriacaooffline = dtcriacaooffline;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	public List<Materialvenda> getListaSugestaovenda() {
		return listaSugestaovenda;
	}
	public void setListaSugestaovenda(List<Materialvenda> listaSugestaovenda) {
		this.listaSugestaovenda = listaSugestaovenda;
	}
}
