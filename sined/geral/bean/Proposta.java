package br.com.linkcom.sined.geral.bean;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_proposta;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.rtf.bean.PropostaRTF;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_proposta", sequenceName = "sq_proposta")
public class Proposta implements Log, PermissaoClienteEmpresa{
	
	protected Integer cdproposta;
	protected Integer numero;
	protected String sufixo;
	protected Propostasituacao propostasituacao;
	protected Cliente cliente;
	protected Municipio municipio;
	protected String descricao;
	protected Contato contato;
	protected Propostacaixa propostacaixa;
	protected String observacao;
	protected Interacao interacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Prospeccao prospeccao;
	protected List<Propostarevisao> listaPropostarevisao = new ListSet<Propostarevisao>(Propostarevisao.class);
	protected Aux_proposta aux_proposta;
	protected Date dtvisita;
	protected Date dtcarta;
	protected Projeto projeto;
	protected String numeroconvite;
	protected Set<PropostaItem> listaItem = new ListSet<PropostaItem>(PropostaItem.class);
	protected Set<PropostaArquivo> listaArquivos = new ListSet<PropostaArquivo>(PropostaArquivo.class);
	protected Empresa empresa;
	

//	TRANSIENTES
	protected Uf uf;
	protected Boolean fromprospeccao;
	private Money valor;
	private Money mobilizacao;
	private Money mdo;
	private Money materiais;
	private String revisaoptm;
	private String revisaopcm;
	private String status;
	@SuppressWarnings("unused")
	private String entrega;
	private String numerorevisao;
	private Integer cdorcamento;
	private Boolean corvermelho;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_proposta")
	public Integer getCdproposta() {
		return cdproposta;
	}

	@Required
	@MaxLength(9)
	public Integer getNumero() {
		return numero;
	}

	@Required
	@MaxLength(2)
	public String getSufixo() {
		return sufixo;
	}
	
	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpropostasituacao")
	public Propostasituacao getPropostasituacao() {
		return propostasituacao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprospeccao")
	public Prospeccao getProspeccao() {
		return prospeccao;
	}
	
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	@DisplayName("Descri��o")	
	@Required
	@MaxLength(500)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Caixa da proposta")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpropostacaixa")
	public Propostacaixa getPropostacaixa() {
		return propostacaixa;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdinteracao")
	public Interacao getInteracao() {
		return interacao;
	}

	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Revis�es")
	@OneToMany(mappedBy="proposta")
	public List<Propostarevisao> getListaPropostarevisao() {
		return listaPropostarevisao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproposta", insertable=false, updatable=false)
	public Aux_proposta getAux_proposta() {
		return aux_proposta;
	}
	@DisplayName("Carta convite")
	public Date getDtcarta() {
		return dtcarta;
	}
	@DisplayName("Visita t�cnica")
	public Date getDtvisita() {
		return dtvisita;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	public Contato getContato() {
		return contato;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName ("Convite")
	@MaxLength (10)
	public String getNumeroconvite() {
		return numeroconvite;
	}
	

	//============================================================================
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public void setDtcarta(Date dtcarta) {
		this.dtcarta = dtcarta;
	}
	public void setDtvisita(Date dtvisita) {
		this.dtvisita = dtvisita;
	}
	public void setAux_proposta(Aux_proposta aux_proposta) {
		this.aux_proposta = aux_proposta;
	}
	
	public void setListaPropostarevisao(
			List<Propostarevisao> listaPropostarevisao) {
		this.listaPropostarevisao = listaPropostarevisao;
	}
	public void setCdproposta(Integer cdproposta) {
		this.cdproposta = cdproposta;
	}

	public void setInteracao(Interacao interacao) {
		this.interacao = interacao;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
	}


	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}

	public void setProspeccao(Prospeccao prospeccao) {
		this.prospeccao = prospeccao;
	}
	
	public void setPropostasituacao(Propostasituacao propostasituacao) {
		this.propostasituacao = propostasituacao;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public void setPropostacaixa(Propostacaixa propostacaixa) {
		this.propostacaixa = propostacaixa;
	}


	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setNumeroconvite(String numeroconvite) {
		this.numeroconvite = numeroconvite;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select propostaSubQueryClienteEmpresa.cdproposta " +
				"from Proposta propostaSubQueryClienteEmpresa " +
				"left outer join propostaSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}


	//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	TRANSIENTES
	@Transient
	public String getNumeroano() {
		return getNumero() + "/" + getSufixo();
	}
	@Transient
	public String getLocalidade() {
		return getMunicipio() != null ? getMunicipio().getNome() + "/" + getMunicipio().getUf().getSigla() : "";
	}
	@Transient
	public Uf getUf(){
		return getMunicipio() != null ? getMunicipio().getUf() : null;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	@Transient
	public Boolean getFromprospeccao() {
		return fromprospeccao;
	}
	public void setFromprospeccao(Boolean fromprospeccao) {
		this.fromprospeccao = fromprospeccao;
	}
	@Transient
	public String getEntrega() {
		String string = "";
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		if (getAux_proposta() != null && getAux_proposta().getDtentregaptm() != null) {
			string += "PTM: " + format.format(getAux_proposta().getDtentregaptm());
		}	
		if (getAux_proposta() != null && getAux_proposta().getDtentregapcm() != null) {
			if (!string.equals("")) {
				string += "<br>";
			}
			string += "PCM: " + format.format(getAux_proposta().getDtentregapcm());
		}
		return string;
	}
	
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	
	@Transient
	public String getNumerorevisao() {
		return numerorevisao;
	}
	
	public void setNumerorevisao(String numerorevisao) {
		this.numerorevisao = numerorevisao;
	}

	@Transient		
	public Money getValor() {
		return valor;
	}
	
	@Transient
	public Money getMobilizacao() {
		return mobilizacao;
	}
	
	@Transient
	public Money getMdo() {
		return mdo;
	}
	
	@Transient
	public Money getMateriais() {
		return materiais;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setMobilizacao(Money mobilizacao) {
		this.mobilizacao = mobilizacao;
	}
	
	public void setMateriais(Money materiais) {
		this.materiais = materiais;
	}
	
	public void setMdo(Money mdo) {
		this.mdo = mdo;
	}

	@Transient
	public String getRevisaoptm() {
		return revisaoptm;
	}
	@Transient
	public String getRevisaopcm() {
		return revisaopcm;
	}

	public void setRevisaoptm(String revisaoptm) {
		this.revisaoptm = revisaoptm;
	}
	
	public void setRevisaopcm(String revisaopcm) {
		this.revisaopcm = revisaopcm;
	}
	
	@Transient
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public String getContatostring(){
		String string = "";
		if (getContato() != null) {
			Contato contato = getContato();
			string += contato.getNome();
			if (contato.getListaTelefone() != null && contato.getListaTelefone().size() > 0) {
				string += "\n";
				string += contato.getTelefones().replaceAll("<br>", "\n");
			}
			if (contato.getEmailcontato() != null && !contato.getEmailcontato().equals("")) {
				string += "\n";
				string += contato.getEmailcontato();
			}
		}
		return string;
	}
	
	@Transient
	@DescriptionProperty (usingFields={"numero","sufixo"})
	public String getDescricaoCombo(){
		StringBuilder s = new StringBuilder();
		if(this.getNumero() != null){
			s.append(this.getNumero());
		}
		if(this.getSufixo() != null){
			if(!s.toString().equals("")){
				s.append("/").append(this.getSufixo());
			}else{
				s.append(this.getSufixo());
			}
		}
		return s.toString();
	}
	
	@Transient
	public Integer getCdorcamento() {
		return cdorcamento;
	}
	
	public void setCdorcamento(Integer cdorcamento) {
		this.cdorcamento = cdorcamento;
	}

	@OneToMany(fetch=FetchType.LAZY,mappedBy="proposta")
	public Set<PropostaItem> getListaItem() {
		return listaItem;
	}

	public void setListaItem(Set<PropostaItem> listaItem) {
		this.listaItem = listaItem;
	}

	@DisplayName("Arquivos")
	@OneToMany(fetch=FetchType.LAZY,mappedBy="proposta")
	public Set<PropostaArquivo> getListaArquivos() {
		return listaArquivos;
	}

	public void setListaArquivos(Set<PropostaArquivo> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}

	@Transient
	public PropostaRTF getPropostaRTF() {
		
		PropostaRTF propostaRTF = new PropostaRTF();

		Cliente cliente = getCliente();
		
		if (cliente != null)
			propostaRTF.setNomeCliente(cliente.getNome());
		
		Endereco endereco = cliente.getEndereco();
		
		if (endereco != null){
			
			propostaRTF.setEndereco(cliente.getEndereco() != null ? cliente.getEndereco().getLogradouro() : "");
			
			propostaRTF.setNumero(endereco.getNumero() != null ? endereco.getNumero() : "");
			
			propostaRTF.setComplemento(endereco != null && endereco.getComplemento() != null ? endereco.getComplemento() : "");
			
			propostaRTF.setBairro(endereco != null && endereco.getBairro() != null ? endereco.getBairro():"");
			
			propostaRTF.setMunicipio(endereco != null && endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "");
			
			propostaRTF.setUf(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? endereco.getMunicipio().getUf().getSigla() : "");
			
		}
		
		propostaRTF.setCpf(cliente != null && cliente.getCpf() != null ? cliente.getCpf().toString():"");
		
		propostaRTF.setCnpj(cliente != null &&  cliente.getCnpj() != null?cliente.getCnpj().toString():"");
		
		if(getContato() != null){
			propostaRTF.setEmail_contato(getContato().getEmailcontato() != null ? contato.getEmailcontato() : "");
		
			propostaRTF.setTelefone_contato(getContato().getTelefones() != null ? contato.getTelefones() : "");
			
			propostaRTF.setNome_responsavelproposta(getContato().getNome() != null ? getContato().getNome() : "");
		}
		
		propostaRTF.setDescricaoProposta(getDescricao() != null ? getDescricao() : "");
		propostaRTF.setRazaosocial_empresa(getEmpresa() != null && getEmpresa().getRazaoSocial() != null ? getEmpresa().getRazaoSocial() : "");
		
		propostaRTF.setNome_projeto(this.getProjeto() != null ? this.getProjeto().getNome() : "");
		
		Money valormateriais = new Money();
		Money valormdo = new Money();
		if (getListaPropostarevisao() != null){
			Money valorTotal = new Money();
			Propostarevisao prultima = null;
			for (Propostarevisao pr : getListaPropostarevisao()) {	
				if(prultima == null){
					if(pr.getCdpropostarevisao() != null)
						prultima = pr;
				}else if(pr.getCdpropostarevisao() != null && pr.getCdpropostarevisao() > prultima.getCdpropostarevisao()){
					prultima = pr;
				}
				propostaRTF.setPrazoPagamento(pr.getPrazo());
				valorTotal = new Money();
				valorTotal = valorTotal.add(pr.getMobilizacao());
				valorTotal = valorTotal.add(pr.getMdo());
				valorTotal = valorTotal.add(pr.getMateriais());
				
				if(pr.getMateriais() != null){
					valormateriais = valormateriais.add(pr.getMateriais());
				}
				if(pr.getMdo() != null){
					valormdo = valormdo.add(pr.getMdo());
				}
			}
			propostaRTF.setValorTotal(valorTotal.toString());
			
			if(prultima != null){
				if(prultima.getDtvalidade() != null){
					propostaRTF.setRevisao_validade(new SimpleDateFormat("dd/MM/yyyy").format(prultima.getDtvalidade()));
				}
				if(prultima.getPrazo() != null){
					propostaRTF.setRevisao_prazoexecucao(prultima.getPrazo());
				}
				if(prultima.getMateriais() != null){
					propostaRTF.setValormateriais(prultima.getMateriais().toString());
				}
				if(prultima.getMdo() != null){
					propostaRTF.setValormaodeobra(prultima.getMdo().toString());
				}
			}
			
			if(valormateriais != null)
				propostaRTF.setValormateriais(valormateriais.toString());
			}
			if(valormdo != null){
				propostaRTF.setValormaodeobra(valormdo.toString());
			}
		
		propostaRTF.setNumeroProposta(getCdproposta() != null ? getCdproposta().toString() : "" );
		
		propostaRTF.setContatoCliente(getContato() != null && getContato().getNome() != null ? getContato().getNome() : "");
		propostaRTF.setObservacao(getObservacao() != null ? getObservacao() : "");
		propostaRTF.setDataAtualExtenso(SinedDateUtils.dataExtenso(SinedDateUtils.currentDate()));
		
		Empresa empresa = getEmpresa();
		if(empresa == null || 
				empresa.getCdpessoa() == null || 
				empresa.getLogomarca() == null || 
				empresa.getLogomarca().getCdarquivo() == null)
			empresa = EmpresaService.getInstance().loadArquivoPrincipal();
		
		if(empresa.getLogomarca() != null && empresa.getLogomarca().getCdarquivo() != null){
			Arquivo logo = empresa.getLogomarca();
			try {
				File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(logo));
				BufferedImage image = ImageIO.read(input);
				
				File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(logo));
				ImageIO.write(image, "png", output);  
				
				propostaRTF.setLogo(new FileInputStream(output));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return propostaRTF;
	}

	@DisplayName("Empresa")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdproposta == null) ? 0 : cdproposta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proposta other = (Proposta) obj;
		if (cdproposta == null) {
			if (other.cdproposta != null)
				return false;
		} else if (!cdproposta.equals(other.cdproposta))
			return false;
		return true;
	}

	@Transient
	public Boolean getCorvermelho() {
		return corvermelho;
	}
	public void setCorvermelho(Boolean corvermelho) {
		this.corvermelho = corvermelho;
	}
}
