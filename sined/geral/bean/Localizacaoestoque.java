package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_localizacaoestoque", sequenceName = "sq_localizacaoestoque")
@DisplayName("Localização")
public class Localizacaoestoque implements Log{

	protected Integer cdlocalizacaoestoque;
	protected String descricao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_localizacaoestoque")
	public Integer getCdlocalizacaoestoque() {
		return cdlocalizacaoestoque;
	}
	
	@DescriptionProperty
	@DisplayName("Descrição")
	@Required
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdlocalizacaoestoque(Integer cdlocalizacaoestoque) {
		this.cdlocalizacaoestoque = cdlocalizacaoestoque;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {}
	public void setDtaltera(Timestamp dtaltera) {}

}
