package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_grupoemail", sequenceName="sq_grupoemail")
@DisplayName("Grupo")
public class Grupoemail implements Log{

	protected Integer cdgrupoemail;
	protected String nome;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<GrupoemailColaborador> listaGrupoemailColaborador;
	
	{
		this.ativo = Boolean.TRUE;
	}
	
	@Id
	@GeneratedValue(generator="sq_grupoemail",strategy=GenerationType.AUTO)
	public Integer getCdgrupoemail() {
		return cdgrupoemail;
	}
	@DescriptionProperty
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Destinatários")
	@OneToMany(mappedBy="grupoemail")
	public List<GrupoemailColaborador> getListaGrupoemailColaborador() {
		return listaGrupoemailColaborador;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdgrupoemail(Integer cdgrupoemail) {
		this.cdgrupoemail = cdgrupoemail;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaGrupoemailColaborador(List<GrupoemailColaborador> listaGrupoemailColaborador) {
		this.listaGrupoemailColaborador = listaGrupoemailColaborador;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Grupoemail) {
			Grupoemail grupoemail = (Grupoemail) obj;
			return grupoemail.cdgrupoemail.equals(this.cdgrupoemail);
		}
		return super.equals(obj);
	}
	
}
