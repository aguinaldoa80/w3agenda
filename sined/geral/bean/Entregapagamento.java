package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_entregapagamento", sequenceName = "sq_entregapagamento")
public class Entregapagamento implements Log{

	protected Integer cdentregapagamento;
//	protected Entrega entrega;
	protected Entregadocumento entregadocumento;
	protected Money valortotalpagamentos;
	protected String numero;
	protected Date dtemissao;
	protected Date dtvencimento;
	protected Money valor;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer parcela;
	protected Documento documentoantecipacao;
	protected Documento documentoorigem;
	
	protected Integer repeticoes;
	protected Prazopagamento prazo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregapagamento")
	public Integer getCdentregapagamento() {
		return cdentregapagamento;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}	
	@MaxLength(50)
	@DisplayName("Documento")
	public String getNumero() {
		return numero;
	}	
	@DisplayName("Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}	
	@Required
	@DisplayName("Data de Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}	
	@Required
	public Money getValor() {
		return valor;
	}
	public Integer getParcela() {
		return parcela;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoantecipacao")
	@DisplayName("Pagamento adiantado")
	public Documento getDocumentoantecipacao() {
		return documentoantecipacao;
	}
	

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setCdentregapagamento(Integer cdentregapagamento) {
		this.cdentregapagamento = cdentregapagamento;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setDocumentoantecipacao(Documento documentoantecipacao) {
		this.documentoantecipacao = documentoantecipacao;
	}
	
	//	LOG	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

//	Transient's	
	@Transient
	public Prazopagamento getPrazo() {
		return prazo;
	}	
	@Transient
	public Integer getRepeticoes() {
		return repeticoes;
	}
	@Transient
	@DisplayName("Valor dos Documentos")
	public Money getValortotalpagamentos() {
		return valortotalpagamentos;
	}

	public void setPrazo(Prazopagamento prazo) {
		this.prazo = prazo;
	}	
	public void setRepeticoes(Integer repeticoes) {
		this.repeticoes = repeticoes;
	}
	public void setValortotalpagamentos(Money valortotalpagamentos) {
		this.valortotalpagamentos = valortotalpagamentos;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoorigem")
	public Documento getDocumentoorigem() {
		return documentoorigem;
	}
	public void setDocumentoorigem(Documento documentoorigem) {
		this.documentoorigem = documentoorigem;
	}	
}
