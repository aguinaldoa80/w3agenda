package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dashboardespecifico", sequenceName = "sq_dashboardespecifico")
public class Dashboardespecifico implements Log {

	protected Integer cddashboardespecifico;
	protected String nome;
	protected String url;
	protected Integer id;
	protected String usuariosistema;
	protected String senhasistema;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dashboardespecifico")
	public Integer getCddashboardespecifico() {
		return cddashboardespecifico;
	}

	@DescriptionProperty
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@Required
	@MaxLength(500)
	public String getUrl() {
		return url;
	}

	@Required
	public Integer getId() {
		return id;
	}

	@Required
	@MaxLength(100)
	@DisplayName("Usu�rio do sistema")
	public String getUsuariosistema() {
		return usuariosistema;
	}

	@Required
	@MaxLength(100)
	@Password
	@DisplayName("Senha do sistema")
	public String getSenhasistema() {
		return senhasistema;
	}

	public void setCddashboardespecifico(Integer cddashboardespecifico) {
		this.cddashboardespecifico = cddashboardespecifico;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsuariosistema(String usuariosistema) {
		this.usuariosistema = usuariosistema;
	}

	public void setSenhasistema(String senhasistema) {
		this.senhasistema = senhasistema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cddashboardespecifico == null) ? 0 : cddashboardespecifico.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Dashboardespecifico))
			return false;
		final Dashboardespecifico other = (Dashboardespecifico) obj;
		if (cddashboardespecifico == null) {
			if (other.cddashboardespecifico != null)
				return false;
		} else if (!cddashboardespecifico.equals(other.cddashboardespecifico))
			return false;
		return true;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		
	}

	@Transient
	@Override
	public Integer getCdusuarioaltera() {
		return null;
	}

	@Transient
	@Override
	public Timestamp getDtaltera() {
		return null;
	}	
		
}
