package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_mdfeufpercurso", sequenceName = "sq_mdfeufpercurso")
public class MdfeUfPercurso {

	protected Integer cdMdfeUfPercurso;
	protected Mdfe mdfe;
	protected Uf uf;
	protected Integer ordem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeufpercurso")
	public Integer getCdMdfeUfPercurso() {
		return cdMdfeUfPercurso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	@Required
	@DisplayName("UF do percurso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	public Integer getOrdem() {
		return ordem;
	}
	
	public void setCdMdfeUfPercurso(Integer cdMdfeUfPercurso) {
		this.cdMdfeUfPercurso = cdMdfeUfPercurso;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
}
