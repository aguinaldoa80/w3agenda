package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Inutilizacaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_inutilizacaonfe", sequenceName = "sq_inutilizacaonfe")
public class Inutilizacaonfe implements Log {

	protected Integer cdinutilizacaonfe;
	protected Empresa empresa;
	protected Configuracaonfe configuracaonfe;
	protected Date dtinutilizacao;
	protected Integer numinicio;
	protected Integer numfim;
	protected String justificativa;
	protected String protocoloinutilizacao;
	protected Timestamp dtprocessamento;
	protected String motivoerro;
	protected Inutilizacaosituacao situacao;
	protected Arquivo arquivoxmlenvio;
	protected Arquivo arquivoxmlretorno;
	protected Boolean enviando;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected Integer serienfe;
	private ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum;
	
	// TRANSIENTES
	protected String xml;
	protected Double numeronfe;
	
	public Inutilizacaonfe(Integer cdinutilizacaonfe, Double numeronfe, Inutilizacaosituacao situacao) {
		this.cdinutilizacaonfe = cdinutilizacaonfe;
		this.numeronfe = numeronfe;
		this.situacao = situacao;
	}

	public Inutilizacaonfe(){}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inutilizacaonfe")
	public Integer getCdinutilizacaonfe() {
		return cdinutilizacaonfe;
	}
	
	@DisplayName("XML de envio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlenvio")
	public Arquivo getArquivoxmlenvio() {
		return arquivoxmlenvio;
	}

	@DisplayName("XML de retorno")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlretorno")
	public Arquivo getArquivoxmlretorno() {
		return arquivoxmlretorno;
	}
	
	@Required
	@DisplayName("Configura��o de NF-e")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfe")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data da inutiliza��o")
	public Date getDtinutilizacao() {
		return dtinutilizacao;
	}

	@Required
	@DisplayName("N�mero inicial")
	public Integer getNuminicio() {
		return numinicio;
	}
	
	@Required
	@DisplayName("N�mero final")
	public Integer getNumfim() {
		return numfim;
	}

	@Required
	@MaxLength(255)
	public String getJustificativa() {
		return justificativa;
	}

	@DisplayName("Protocolo")
	public String getProtocoloinutilizacao() {
		return protocoloinutilizacao;
	}

	@DisplayName("Data do processamento")
	public Timestamp getDtprocessamento() {
		return dtprocessamento;
	}

	@DisplayName("Motivo do erro")
	public String getMotivoerro() {
		return motivoerro;
	}

	@DisplayName("Situa��o")
	public Inutilizacaosituacao getSituacao() {
		return situacao;
	}
	
	@Transient
	public Double getNumeronfe() {
		return numeronfe;
	}
	
	@Required
	@DisplayName("Modelo")
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscalEnum() {
		return modeloDocumentoFiscalEnum;
	}
	
	public void setModeloDocumentoFiscalEnum(ModeloDocumentoFiscalEnum modeloDocumentoFiscalEnum) {
		this.modeloDocumentoFiscalEnum = modeloDocumentoFiscalEnum;
	}
	
	public Boolean getEnviando() {
		return enviando;
	}
	
	public void setEnviando(Boolean enviando) {
		this.enviando = enviando;
	}

	public void setCdinutilizacaonfe(Integer cdinutilizacaonfe) {
		this.cdinutilizacaonfe = cdinutilizacaonfe;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public void setDtinutilizacao(Date dtinutilizacao) {
		this.dtinutilizacao = dtinutilizacao;
	}

	public void setNuminicio(Integer numinicio) {
		this.numinicio = numinicio;
	}

	public void setNumfim(Integer numfim) {
		this.numfim = numfim;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public void setProtocoloinutilizacao(String protocoloinutilizacao) {
		this.protocoloinutilizacao = protocoloinutilizacao;
	}

	public void setDtprocessamento(Timestamp dtprocessamento) {
		this.dtprocessamento = dtprocessamento;
	}

	public void setMotivoerro(String motivoerro) {
		this.motivoerro = motivoerro;
	}

	public void setSituacao(Inutilizacaosituacao situacao) {
		this.situacao = situacao;
	}

	public void setArquivoxmlenvio(Arquivo arquivoxmlenvio) {
		this.arquivoxmlenvio = arquivoxmlenvio;
	}

	public void setArquivoxmlretorno(Arquivo arquivoxmlretorno) {
		this.arquivoxmlretorno = arquivoxmlretorno;
	}
	
	public void setNumeronfe(Double numeronfe) {
		this.numeronfe = numeronfe;
	}
	
	@DisplayName("S�rie da Nf-e")
	public Integer getSerienfe() {
		return serienfe;
	}
	
	public void setSerienfe(Integer serienfe) {
		this.serienfe = serienfe;
	}

//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
//	TRANSIENTES
	
	@Transient
	public String getXml() {
		return xml;
	}
	
	public void setXml(String xml) {
		this.xml = xml;
	}
}
