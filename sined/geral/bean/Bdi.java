package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name="sq_bdi",sequenceName="sq_bdi")
public class Bdi implements Log {

	protected Integer cdbdi;
	protected Orcamento orcamento;
	protected Double folga;
	protected Boolean incluimod;
	protected Boolean incluimoi;
	protected List<Bdicomposicaoorcamento> listaBdicomposicaoorcamento = new ArrayList<Bdicomposicaoorcamento>();
	protected List<Bdiitem> listaBdiitem = new ArrayList<Bdiitem>();
	protected List<Bditributo> listaBditributo = new ArrayList<Bditributo>();
	protected Double valorrecursogeraldireto;
	protected Double valoroutrastarefas;
	protected Double valoracrescimo;
	protected Double valoritenscustomo;
	protected Boolean incluirecursogeraldireto;
	protected Boolean incluioutrastarefas;
	protected Boolean incluiacrescimo;
	protected Boolean incluiitenscustomo;
	
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Bdi() {
	}
	
	public Bdi(Integer cdbdi) {
		this.cdbdi = cdbdi;
	}

	@Id
	@GeneratedValue(generator="sq_bdi", strategy=GenerationType.AUTO)	
	public Integer getCdbdi() {
		return cdbdi;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@DisplayName("Orçamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	@Required
	public Double getFolga() {
		return folga;
	}
	
	@Required	
	public Boolean getIncluimod() {
		return incluimod;
	}
	
	@Required	
	public Boolean getIncluimoi() {
		return incluimoi;
	}	
	
	public Double getValorrecursogeraldireto() {
		return valorrecursogeraldireto;
	}

	public Double getValoroutrastarefas() {
		return valoroutrastarefas;
	}
	
	public Boolean getIncluirecursogeraldireto() {
		return incluirecursogeraldireto;
	}

	public Boolean getIncluioutrastarefas() {
		return incluioutrastarefas;
	}
	
	public Double getValoracrescimo() {
		return valoracrescimo;
	}
	
	public Double getValoritenscustomo() {
		return valoritenscustomo;
	}

	public Boolean getIncluiitenscustomo() {
		return incluiitenscustomo;
	}

	public Boolean getIncluiacrescimo() {
		return incluiacrescimo;
	}
	
	public void setIncluiacrescimo(Boolean incluiacrescimo) {
		this.incluiacrescimo = incluiacrescimo;
	}
	
	public void setValoracrescimo(Double valoracrescimo) {
		this.valoracrescimo = valoracrescimo;
	}

	public void setIncluirecursogeraldireto(Boolean incluirecursogeraldireto) {
		this.incluirecursogeraldireto = incluirecursogeraldireto;
	}

	public void setIncluioutrastarefas(Boolean incluioutrastarefas) {
		this.incluioutrastarefas = incluioutrastarefas;
	}
	
	public void setValoritenscustomo(Double valoritenscustomo) {
		this.valoritenscustomo = valoritenscustomo;
	}

	public void setIncluiitenscustomo(Boolean incluiitenscustomo) {
		this.incluiitenscustomo = incluiitenscustomo;
	}

	public void setValorrecursogeraldireto(Double valorrecursogeraldireto) {
		this.valorrecursogeraldireto = valorrecursogeraldireto;
	}

	public void setValoroutrastarefas(Double valoroutrastarefas) {
		this.valoroutrastarefas = valoroutrastarefas;
	}

	public void setCdbdi(Integer cdbdi) {
		this.cdbdi = cdbdi;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public void setFolga(Double folga) {
		this.folga = folga;
	}

	public void setIncluimod(Boolean incluimod) {
		this.incluimod = incluimod;
	}

	public void setIncluimoi(Boolean incluimoi) {
		this.incluimoi = incluimoi;
	}
	
	/************/
	/** Listas **/
	/************/
	
	@OneToMany(mappedBy="bdi")
	@DisplayName("Itens")	
	public List<Bdiitem> getListaBdiitem() {
		return listaBdiitem;
	}
	
	@OneToMany(mappedBy="bdi")
	@DisplayName("Tributos")
	public List<Bditributo> getListaBditributo() {
		return listaBditributo;
	}

	@OneToMany(mappedBy="bdi")
	@DisplayName("Composições")	
	public List<Bdicomposicaoorcamento> getListaBdicomposicaoorcamento() {
		return listaBdicomposicaoorcamento;
	}

	public void setListaBdiitem(List<Bdiitem> listaBdiitem) {
		this.listaBdiitem = listaBdiitem;
	}
	
	public void setListaBditributo(List<Bditributo> listaBditributo) {
		this.listaBditributo = listaBditributo;
	}

	public void setListaBdicomposicaoorcamento(List<Bdicomposicaoorcamento> listaBdicomposicaoorcamento) {
		this.listaBdicomposicaoorcamento = listaBdicomposicaoorcamento;
	}	
	
	/*************/
	/**** Log ****/
	/*************/
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bdi) {
			Bdi that = (Bdi) obj;
			return this.getCdbdi().equals(that.getCdbdi());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdbdi != null) {
			return cdbdi.hashCode();
		}
		return super.hashCode();		
	}

}
