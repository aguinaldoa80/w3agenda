package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Banda Comercial")
@SequenceGenerator(name = "sq_bandaredecomercial", sequenceName = "sq_bandaredecomercial")
public class Bandaredecomercial implements Log{

	protected Integer cdbandaredecomercial;
	protected String nome;	
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Bandaredematerial> listaMaterial;
	protected Set<Bandarederegra> listaRegra;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bandaredecomercial")
	public Integer getCdbandaredecomercial() {
		return cdbandaredecomercial;
	}

	@Required
	@DescriptionProperty
	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	@Required
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@OneToMany(mappedBy="bandaredecomercial")
	public Set<Bandaredematerial> getListaMaterial(){
		return listaMaterial;
	}
	
	@OneToMany(mappedBy="bandaredecomercial")
	public Set<Bandarederegra> getListaRegra() {
		return listaRegra;
	}
	
	public void setCdbandaredecomercial(Integer cdbandaredecomercial) {
		this.cdbandaredecomercial = cdbandaredecomercial;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setListaMaterial(Set<Bandaredematerial> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	
	public void setListaRegra(Set<Bandarederegra> listaRegra) {
		this.listaRegra = listaRegra;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
}
