package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_acaojudicialnatureza", sequenceName = "sq_acaojudicialnatureza")
public class AcaoJudicialNatureza {
	protected Integer cdAcaoJudicialNatureza;
	protected String codigo;
	protected String descricao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_acaojudicialnatureza")
	public Integer getCdAcaoJudicialNatureza() {
		return cdAcaoJudicialNatureza;
	}
	public void setCdAcaoJudicialNatureza(Integer cdAcaoJudicialNatureza) {
		this.cdAcaoJudicialNatureza = cdAcaoJudicialNatureza;
	}

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"codigo", "descricao"})
	public String getAutocompleteDescription(){
		return this.codigo+" - " + this.descricao;
		
	}
	
	
}
