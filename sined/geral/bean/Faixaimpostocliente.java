package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_faixaimpostocliente",sequenceName="sq_faixaimpostocliente")
public class Faixaimpostocliente {
	
	protected Integer cdfaixaimpostocliente;
	protected Faixaimposto faixaimposto;
	protected Cliente cliente;
	
	@Id
	@GeneratedValue(generator="sq_faixaimpostocliente", strategy=GenerationType.AUTO)
	public Integer getCdfaixaimpostocliente() {
		return cdfaixaimpostocliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixaimposto")
	public Faixaimposto getFaixaimposto() {
		return faixaimposto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	public void setCdfaixaimpostocliente(Integer cdfaixaimpostocliente) {
		this.cdfaixaimpostocliente = cdfaixaimpostocliente;
	}

	public void setFaixaimposto(Faixaimposto faixaimposto) {
		this.faixaimposto = faixaimposto;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
