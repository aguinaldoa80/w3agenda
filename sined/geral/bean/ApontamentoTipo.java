package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class ApontamentoTipo {

	public static final ApontamentoTipo FUNCAO_HORA = new ApontamentoTipo(1);
	public static final ApontamentoTipo FUNCAO_ATIVIDADE = new ApontamentoTipo(3);
	public static final ApontamentoTipo MATERIAL = new ApontamentoTipo(2);
	
	protected Integer cdapontamentotipo;
	protected String nome;
	
	public ApontamentoTipo() {}
	public ApontamentoTipo(Integer cdapontamentotipo) {
		this.cdapontamentotipo = cdapontamentotipo;
	}
	
	@Id
	public Integer getCdapontamentotipo() {
		return cdapontamentotipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdapontamentotipo(Integer cdapontamentotipo) {
		this.cdapontamentotipo = cdapontamentotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdapontamentotipo == null) ? 0 : cdapontamentotipo
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ApontamentoTipo other = (ApontamentoTipo) obj;
		if (cdapontamentotipo == null) {
			if (other.cdapontamentotipo != null)
				return false;
		} else if (!cdapontamentotipo.equals(other.cdapontamentotipo))
			return false;
		return true;
	}
	
}
