package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@DisplayName("Empresa")
@Entity
@SequenceGenerator(name="sq_clienteempresa", sequenceName="sq_clienteempresa")
public class ClienteEmpresa {
	
	protected Integer cdclienteempresa;
	protected Cliente cliente;
	protected Empresa empresa;
	
	protected boolean contemListaEmpresa = true;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clienteempresa")
	public Integer getCdclienteempresa() {
		return cdclienteempresa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Transient
	public boolean isContemListaEmpresa() {
		return contemListaEmpresa;
	}
	
	public void setCdclienteempresa(Integer cdclienteempresa) {
		this.cdclienteempresa = cdclienteempresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setContemListaEmpresa(boolean contemListaEmpresa) {
		this.contemListaEmpresa = contemListaEmpresa;
	}
}