package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Composi��o do or�amento")
@SequenceGenerator(name = "sq_composicaoorcamento", sequenceName = "sq_composicaoorcamento")
public class Composicaoorcamento {
	
	protected Integer cdcomposicaoorcamento;
	protected String nome;
	protected Orcamento orcamento;
	protected Boolean materialseguranca = false;
	protected List<Recursocomposicao> listaRecursocomposicao = new ArrayList<Recursocomposicao>();	
	protected Contagerencial contagerencial;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_composicaoorcamento")	
	public Integer getCdcomposicaoorcamento() {
		return cdcomposicaoorcamento;
	}

	
	@DisplayName("Nome")
	@MaxLength(30)
	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	public Boolean getMaterialseguranca() {
		return materialseguranca;
	}
	
	@OneToMany(mappedBy="composicaoorcamento")
	@DisplayName("Itens da composi��o do or�amento")	
	public List<Recursocomposicao> getListaRecursocomposicao() {
		return listaRecursocomposicao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setCdcomposicaoorcamento(Integer cdcomposicaoorcamento) {
		this.cdcomposicaoorcamento = cdcomposicaoorcamento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setMaterialseguranca(Boolean materialseguranca) {
		this.materialseguranca = materialseguranca;
	}
	
	public void setListaRecursocomposicao(List<Recursocomposicao> listaRecursocomposicao) {
		this.listaRecursocomposicao = listaRecursocomposicao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Composicaoorcamento) {
			Composicaoorcamento that = (Composicaoorcamento) obj;
			return this.getCdcomposicaoorcamento().equals(that.getCdcomposicaoorcamento());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdcomposicaoorcamento != null) {
			return cdcomposicaoorcamento.hashCode();
		}
		return super.hashCode();
	}
}
