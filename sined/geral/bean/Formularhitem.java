package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_formularhitem", sequenceName="sq_formularhitem")
public class Formularhitem {
	
	protected Integer cdformularhitem;
	protected Formularh formularh;
	protected Integer ordem;
	protected String identificador;
	protected String formula;
	
	@Id
	@GeneratedValue(generator="sq_formularhitem",strategy=GenerationType.AUTO)
	public Integer getCdformularhitem() {
		return cdformularhitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformularh")
	public Formularh getFormularh() {
		return formularh;
	}
	@DisplayName("Ordem")
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	@DisplayName("Identificador")
	@Required
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("F�rmula")
	@MaxLength(1000)
	@Required
	public String getFormula() {
		return formula;
	}
	
	public void setCdformularhitem(Integer cdformularhitem) {
		this.cdformularhitem = cdformularhitem;
	}
	public void setFormularh(Formularh formularh) {
		this.formularh = formularh;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}	
}
