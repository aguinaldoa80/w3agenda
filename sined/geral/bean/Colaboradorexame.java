package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorexame", sequenceName = "sq_colaboradorexame")
public class Colaboradorexame{

	protected Integer cdcolaboradorexame;
	protected Colaborador colaborador;
	protected Exameresponsavel exameresponsavel;
	protected Exametipo exametipo;
	protected Examenatureza examenatureza;
	protected Date dtexame;
	protected Date dtproximoexame;
	protected Boolean examers;
	protected Boolean resultadonormal;
	protected Boolean resultadoalterado;
	protected Boolean resultadoestavel;
	protected Boolean resultadoagravamento;
	protected Boolean resultadoocupacional;
	protected Boolean resultadonaoocupacional;
	
	//TRANSIENTS
	protected String indicacaoresultados;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorexame")
	public Integer getCdcolaboradorexame() {
		return cdcolaboradorexame;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Profissional respons�vel")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdexameresponsavel")
	public Exameresponsavel getExameresponsavel() {
		return exameresponsavel;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdexametipo")
	public Exametipo getExametipo() {
		return exametipo;
	}

	@Required
	@DisplayName("Natureza")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdexamenatureza")
	public Examenatureza getExamenatureza() {
		return examenatureza;
	}

	@Required
	@DisplayName("Data")
	public Date getDtexame() {
		return dtexame;
	}
	
	@DisplayName("Data pr�ximo exame")
	public Date getDtproximoexame(){
		return dtproximoexame;
	}
	
	@Required
	@DisplayName("Exame (R/S)")
	public Boolean getExamers() {
		return examers;
	}

	@DisplayName("Normal")
	public Boolean getResultadonormal() {
		return resultadonormal;
	}
	
	@DisplayName("Alterado")
	public Boolean getResultadoalterado() {
		return resultadoalterado;
	}

	@DisplayName("Est�vel")
	public Boolean getResultadoestavel() {
		return resultadoestavel;
	}

	@DisplayName("Agravamento")
	public Boolean getResultadoagravamento() {
		return resultadoagravamento;
	}

	@DisplayName("Ocupacional")
	public Boolean getResultadoocupacional() {
		return resultadoocupacional;
	}

	@DisplayName("N�o ocupacional")
	public Boolean getResultadonaoocupacional() {
		return resultadonaoocupacional;
	}

	public void setExameresponsavel(Exameresponsavel exameresponsavel) {
		this.exameresponsavel = exameresponsavel;
	}

	public void setResultadonormal(Boolean resultadonormal) {
		this.resultadonormal = resultadonormal;
	}
	
	public void setExametipo(Exametipo exametipo) {
		this.exametipo = exametipo;
	}

	public void setExamenatureza(Examenatureza examenatureza) {
		this.examenatureza = examenatureza;
	}

	public void setDtexame(Date dtexame) {
		this.dtexame = dtexame;
	}

	public void setDtproximoexame(Date dtproximoexame){
		this.dtproximoexame = dtproximoexame;
	}
	
	public void setExamers(Boolean examers) {
		this.examers = examers;
	}

	public void setResultadoalterado(Boolean resultadoalterado) {
		this.resultadoalterado = resultadoalterado;
	}

	public void setResultadoestavel(Boolean resultadoestavel) {
		this.resultadoestavel = resultadoestavel;
	}

	public void setResultadoagravamento(Boolean resultadoagravamento) {
		this.resultadoagravamento = resultadoagravamento;
	}

	public void setResultadoocupacional(Boolean resultadoocupacional) {
		this.resultadoocupacional = resultadoocupacional;
	}

	public void setResultadonaoocupacional(Boolean resultadonaoocupacional) {
		this.resultadonaoocupacional = resultadonaoocupacional;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setCdcolaboradorexame(Integer cdcolaboradorexame) {
		this.cdcolaboradorexame = cdcolaboradorexame;
	}
	
	@Transient
	public String getIndicacaoresultados() {
		StringBuilder sb = new StringBuilder("(")
		.append(this.resultadonormal != null && this.resultadonormal ? "X" : "").append(") Normal (")
		.append(this.resultadoalterado != null && this.resultadoalterado ? "X" : "").append(") Alterado (")
		.append(this.resultadoestavel != null && this.resultadoestavel ? "X" : "").append(") Est�vel (")
		.append(this.resultadoagravamento != null && this.resultadoagravamento ? "X" : "").append(") Agravamento (")
		.append(this.resultadoocupacional != null && this.resultadoocupacional ? "X" : "").append(") Ocupacional (")
		.append(this.resultadonaoocupacional != null && this.resultadonaoocupacional ? "X" : "").append(") N�o ocupacional");
		return sb.toString();
	}
	
	public void setIndicacaoresultados(String indicacaoresultados) {
		this.indicacaoresultados = indicacaoresultados;
	}
	
}
