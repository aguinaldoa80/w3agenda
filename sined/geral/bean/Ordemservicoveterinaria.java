package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_ordemservicoveterinaria", sequenceName = "sq_ordemservicoveterinaria")
@DisplayName("Ordem de Servi�o")
public class Ordemservicoveterinaria {

	private Integer cdordemservicoveterinaria;
	private Date data = new Date(System.currentTimeMillis());
	private Date dtprevisao;
	private String numero;
	private Empresa empresa;
	private Cliente cliente;
	private String descricao;
	private Requisicaoestado requisicaoestado = new Requisicaoestado(Requisicaoestado.EMESPERA);
	private Colaborador responsavel;
	private List<Ordemservicoveterinariamaterial> listaOrdemservicoveterinariamaterial;
	private List<Ordemservicoveterinariarotina> listaOrdemservicoveterinariarotina;
	private Atividadetipoveterinaria atividadetipoveterinaria;

	public Ordemservicoveterinaria() {
	}

	public Ordemservicoveterinaria(Integer cdordemservicoveterinaria) {
		this.cdordemservicoveterinaria = cdordemservicoveterinaria;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_ordemservicoveterinaria")
	public Integer getCdordemservicoveterinaria() {
		return cdordemservicoveterinaria;
	}

	@DisplayName("Data")
	@Required
	public Date getData() {
		return data;
	}

	@DisplayName("Previs�o")
	public Date getDtprevisao() {
		return dtprevisao;
	}

	@DisplayName("Ordem de Servi�o")
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}

	@DisplayName("Empresa")
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Descri��o")
	@MaxLength(1000)
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Materiais e Servi�os")
	@OneToMany(mappedBy = "ordemservicoveterinaria")
	public List<Ordemservicoveterinariamaterial> getListaOrdemservicoveterinariamaterial() {
		return listaOrdemservicoveterinariamaterial;
	}
	@DisplayName("Rotina")
	@OneToMany(mappedBy="ordemservicoveterinaria")
	public List<Ordemservicoveterinariarotina> getListaOrdemservicoveterinariarotina() {
		return listaOrdemservicoveterinariarotina;
	}

	@DisplayName("Situa��o")
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdrequisicaoestado")
	public Requisicaoestado getRequisicaoestado() {
		return requisicaoestado;
	}  

	public void setRequisicaoestado(Requisicaoestado requisicaoestado) {
		this.requisicaoestado = requisicaoestado;
	}

	public void setCdordemservicoveterinaria(Integer cdordemservicoveterinaria) {
		this.cdordemservicoveterinaria = cdordemservicoveterinaria;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setDtprevisao(Date dtprevisao) {
		this.dtprevisao = dtprevisao;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setListaOrdemservicoveterinariamaterial(List<Ordemservicoveterinariamaterial> listaOrdemservicoveterinariamaterial) {
		this.listaOrdemservicoveterinariamaterial = listaOrdemservicoveterinariamaterial;
	}
	public void setListaOrdemservicoveterinariarotina(List<Ordemservicoveterinariarotina> listaOrdemservicoveterinariarotina) {
		this.listaOrdemservicoveterinariarotina = listaOrdemservicoveterinariarotina;
	}
	
	@DisplayName("Respons�vel")
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdatividadetipoveterinaria")
	public Atividadetipoveterinaria getAtividadetipoveterinaria() {
		return atividadetipoveterinaria;
	}

	public void setAtividadetipoveterinaria(Atividadetipoveterinaria atividadetipoveterinaria) {
		this.atividadetipoveterinaria = atividadetipoveterinaria;
	}
}