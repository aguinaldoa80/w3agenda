package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_colaboradorarea", sequenceName = "sq_colaboradorarea")
public class Colaboradorarea{
	protected Integer cdcolaboradorarea;
	protected Colaborador colaborador;
	protected Area area;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorarea")
	public Integer getCdcolaboradorarea() {
		return cdcolaboradorarea;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarea")
	@Required
	@DisplayName("�rea")
	public Area getArea() {
		return area;
	}
	public void setCdcolaboradorarea(Integer cdcolaboradorarea) {
		this.cdcolaboradorarea = cdcolaboradorarea;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setArea(Area area) {
		this.area = area;
	}
}
