package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoProducaodiaria;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_producaodiaria",sequenceName="sq_producaodiaria")
@DisplayName("Medi��o")
public class Producaodiaria implements Log, PermissaoProjeto{

	protected Integer cdproducaodiaria;
	protected Tarefa tarefa;
	protected Date dtproducao;
	protected Double qtde;
	protected Boolean faturar;
	protected Date dtfatura;
	protected String observacao;
	protected Nota nota;
	protected Atividade atividade;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Double percentual;
	
	//Transientes
	protected SituacaoProducaodiaria situacaoProducaodiaria;
	protected Date dataApontamento;
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Double qtderealizada;
	protected Double qtderestante;
	
	{
		this.faturar = Boolean.TRUE;
	}
	
	public Producaodiaria() {
	}
	public Producaodiaria(Integer cdproducaodiaria) {
		this.cdproducaodiaria = cdproducaodiaria;
	}
	
	@Id
	@GeneratedValue(generator="sq_producaodiaria",strategy=GenerationType.AUTO)
	public Integer getCdproducaodiaria() {
		return cdproducaodiaria;
	}
	@Required
	@JoinColumn(name="cdtarefa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tarefa getTarefa() {
		return tarefa;
	}
	@DisplayName("Data")
	@Required
	public Date getDtproducao() {
		return dtproducao;
	}
	@Required
	@DisplayName("Qtde.")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	public Boolean getFaturar() {
		return faturar;
	}
	@DisplayName("Data da fatura")
	public Date getDtfatura() {
		return dtfatura;
	}
	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	@JoinColumn(name="cdnota")
	@ManyToOne(fetch=FetchType.LAZY)
	public Nota getNota() {
		return nota;
	}
	@JoinColumn(name="cdatividade")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividade getAtividade() {
		return atividade;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdproducaodiaria(Integer cdproducaodiaria) {
		this.cdproducaodiaria = cdproducaodiaria;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setDtproducao(Date dtproducao) {
		this.dtproducao = dtproducao;
	}
	public void setQtde(Double quantidade) {
		this.qtde = quantidade;
	}
	public void setFaturar(Boolean faturar) {
		this.faturar = faturar;
	}
	public void setDtfatura(Date dtfatura) {
		this.dtfatura = dtfatura;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Transient
	@DisplayName("Situa��o")
	public SituacaoProducaodiaria getSituacaoProducaodiaria() {
		if(dtfatura != null){
			situacaoProducaodiaria = SituacaoProducaodiaria.FATURADA;
		}else{
			situacaoProducaodiaria = SituacaoProducaodiaria.A_FATURAR;
		}
		return situacaoProducaodiaria;
	}
	public void setSituacaoProducaodiaria(SituacaoProducaodiaria situacao) {
		this.situacaoProducaodiaria = situacao;
	}
	
	@Transient
	public Date getDataApontamento() {
		return dataApontamento;
	}
	
	public void setDataApontamento(Date dataApontamento) {
		this.dataApontamento = dataApontamento;
	}
	
	public String subQueryProjeto() {
		return "select producaodiariasubQueryProjeto.cdproducaodiaria " +
				"from Producaodiaria producaodiariasubQueryProjeto " +
				"join producaodiariasubQueryProjeto.tarefa tarefasubQueryProjeto " +
				"join tarefasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Required
	public Double getPercentual() {
		return percentual;
	}
	
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	@Required
	@Transient
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@Required
	@Transient
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Transient
	@DisplayName("Qtde. realizada")
	public Double getQtderealizada() {
		return qtderealizada;
	}
	
	@Transient
	@DisplayName("Qtde. restante")
	public Double getQtderestante() {
		return qtderestante;
	}
	
	public void setQtderealizada(Double qtderealizada) {
		this.qtderealizada = qtderealizada;
	}
	public void setQtderestante(Double qtderestante) {
		this.qtderestante = qtderestante;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Transient
	public Unidademedida getUnidademedida (){
		if (tarefa!=null && tarefa.getUnidademedida()!=null){
			return tarefa.getUnidademedida();
		}
		return null;
	}
}
