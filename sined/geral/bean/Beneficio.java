package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@DisplayName("Benef�cio")
@Entity
@SequenceGenerator(name="sq_beneficio",sequenceName="sq_beneficio")
public class Beneficio implements Log {
	
	private Integer cdbeneficio;
	private String nome;
	private Beneficiotipo beneficiotipo;
	private Boolean ativo = Boolean.TRUE;
	private List<Beneficiovalor> listaBeneficiovalor;
	
	//Log
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_beneficio", strategy=GenerationType.AUTO)
	public Integer getCdbeneficio() {
		return cdbeneficio;
	}
	@Required
	@DisplayName("Nome")
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbeneficiotipo")	
	public Beneficiotipo getBeneficiotipo() {
		return beneficiotipo;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Valores")
	@OneToMany(mappedBy="beneficio")
	public List<Beneficiovalor> getListaBeneficiovalor() {
		return listaBeneficiovalor;
	}
	@DisplayName("Valores")
	@Transient
	public String getValores(){
		if (listaBeneficiovalor!=null && !listaBeneficiovalor.isEmpty()){
			return CollectionsUtil.listAndConcatenate(listaBeneficiovalor, "valor", ", ");
		}else {
			return null;
		}
	}
	
	public void setCdbeneficio(Integer cdbeneficio) {
		this.cdbeneficio = cdbeneficio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBeneficiotipo(Beneficiotipo beneficiotipo) {
		this.beneficiotipo = beneficiotipo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaBeneficiovalor(List<Beneficiovalor> listaBeneficiovalor) {
		this.listaBeneficiovalor = listaBeneficiovalor;
	}
	
	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}