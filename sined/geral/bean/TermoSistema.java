package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TermoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_termosistema",sequenceName="sq_termosistema")
@DisplayName("Termo do sistema")
public class TermoSistema implements Log{

	protected Integer cdtermosistema;
	protected TermoEnum termo;
	protected String novotermo;
	protected String urlcadastro;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@GeneratedValue(generator = "sq_termosistema", strategy = GenerationType.AUTO)
	public Integer getCdtermosistema() {
		return cdtermosistema;
	}

	public void setCdtermosistema(Integer cdtermosistema) {
		this.cdtermosistema = cdtermosistema;
	}

	@Required
	public TermoEnum getTermo() {
		return termo;
	}

	public void setTermo(TermoEnum termo) {
		this.termo = termo;
	}

	@Required
	@DisplayName("Novo termo")
	public String getNovotermo() {
		return novotermo;
	}

	public void setNovotermo(String novotermo) {
		this.novotermo = novotermo;
	}

	@DisplayName("Url do cadastro")
	public String getUrlcadastro() {
		return urlcadastro;
	}

	public void setUrlcadastro(String urlcadastro) {
		this.urlcadastro = urlcadastro;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
}
