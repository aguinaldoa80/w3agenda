package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_veiculousoparada", sequenceName = "sq_veiculousoparada")
public class Veiculousoparada {

	protected Integer cdveiculousoparada;
	protected Veiculouso veiculouso;
	protected String local;
	protected Timestamp inicioparada;
	protected Timestamp fimparada;
	protected Integer km;
	protected Double horimetro;
	protected Projeto projeto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculousoparada")
	public Integer getCdveiculousoparada() {
		return cdveiculousoparada;
	}
	@Required
	@DisplayName("Ve�culo uso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso")
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	@Required
	@DisplayName("In�cio")
	public Timestamp getInicioparada() {
		return inicioparada;
	}
	@Required
	@DisplayName("Fim")
	public Timestamp getFimparada() {
		return fimparada;
	}
	@MaxLength(9)
	public Integer getKm() {
		return km;
	}
	@DisplayName("Hor�metro")
	public Double getHorimetro() {
		return horimetro;
	}
	@MaxLength(200)
	@Required
	public String getLocal() {
		return local;
	}
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setLocal(String local) {
		this.local = local;
	}	
	public void setCdveiculousoparada(Integer cdveiculousoparada) {
		this.cdveiculousoparada = cdveiculousoparada;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}
	public void setInicioparada(Timestamp inicioparada) {
		this.inicioparada = inicioparada;
	}
	public void setFimparada(Timestamp fimparada) {
		this.fimparada = fimparada;
	}
	public void setKm(Integer km) {
		this.km = km;
	}
	public void setHorimetro(Double horimetro) {
		this.horimetro = horimetro;
	}
	
}