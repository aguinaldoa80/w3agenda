package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.util.CollectionsUtil;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class NotaStatus {
	
	public static final NotaStatus EMITIDA = new NotaStatus(1, "Emitida");
	public static final NotaStatus LIQUIDADA = new NotaStatus(2, "Liquidada");
	public static final NotaStatus CANCELADA = new NotaStatus(3, "Cancelada");
	
	public static final NotaStatus NFSE_SOLICITADA = new NotaStatus(4, "NFS-e Solicitada");
	public static final NotaStatus NFSE_ENVIADA = new NotaStatus(5, "NFS-e Enviada");
	public static final NotaStatus NFSE_EMITIDA = new NotaStatus(6, "NFS-e Emitida");
	public static final NotaStatus NFSE_LIQUIDADA = new NotaStatus(7, "NFS-e Liquidada");
	
	public static final NotaStatus NFE_SOLICITADA = new NotaStatus(8, "NF-e Solicitada");
	public static final NotaStatus NFE_ENVIADA = new NotaStatus(9, "NF-e Enviada");
	public static final NotaStatus NFE_EMITIDA = new NotaStatus(10, "NF-e Emitida");
	public static final NotaStatus NFE_LIQUIDADA = new NotaStatus(11, "NF-e Liquidada");
	
	public static final NotaStatus EM_ESPERA = new NotaStatus(12, "Em Espera");
	
	public static final NotaStatus NFSE_CANCELANDO = new NotaStatus(13, "Cancelando NFS-e");
	public static final NotaStatus NFE_CANCELANDO = new NotaStatus(14, "Cancelando NF-e");
	public static final NotaStatus NFE_DENEGADA = new NotaStatus(15, "NF-e Denegada");
	
	/**
	 * Este atributo n�o � um estado poss�vel para Nota, mas representa
	 * uma a��o poss�vel de se executar para uma Nota.
	 */
	public static final NotaStatus ESTORNADA = new NotaStatus(101, "Estornada");
	public static final NotaStatus ALTERADA = new NotaStatus(102, "Alterada");
	public static final NotaStatus BAIXA_ESTOQUE = new NotaStatus(103, "Baixa no Estoque");
	public static final NotaStatus ENVIO_EMAIL = new NotaStatus(104, "Envio de E-mail");
	public static final NotaStatus CANHOTO_REGISTRADO = new NotaStatus(105, "Canhoto Registrado");
	public static final NotaStatus ESTORNO_CANHOTO = new NotaStatus(106, "Estorno de Canhoto");
	public static final NotaStatus PAGAMENTO_TEF_REALIZADO = new NotaStatus(110, "Pagamento com TEF Realizado");
	public static final NotaStatus GNRE_GERADA = new NotaStatus(111, "GNRE gerada");
	
	
	/**
	 * Este atributo n�o � um estado poss�vel para Nota, mas representa a expedi��o
	 * 
	 */
	public static final NotaStatus ENTREGA = new NotaStatus(107, "Entrega");
	public static final NotaStatus ADICIONADA = new NotaStatus(108, "Adicionada");
	public static final NotaStatus REMOVIDA = new NotaStatus(109, "Removida");
	
	protected Integer cdNotaStatus;
	protected String nome;
	
	
	//Construtores
	public NotaStatus() {
		super();
	}
	
	public NotaStatus(Integer cdNotaStatus, String nome) {
		this.cdNotaStatus = cdNotaStatus;
		this.nome = nome;
	}
	//Fim construtores
	
	

	@Id
	public Integer getCdNotaStatus() {
		return cdNotaStatus;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdNotaStatus(Integer cdNotaStatus) {
		this.cdNotaStatus = cdNotaStatus;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Transient
	public static List<NotaStatus> getTodosEstados() {
		List<NotaStatus> lista = new ArrayList<NotaStatus>();
		
		lista.add(EMITIDA);
		lista.add(LIQUIDADA);
		lista.add(CANCELADA);
		lista.add(NFSE_SOLICITADA);
		lista.add(NFSE_ENVIADA);
		lista.add(NFSE_EMITIDA);
		lista.add(NFSE_LIQUIDADA);
		lista.add(EM_ESPERA);
		lista.add(NFSE_CANCELANDO);
		
		return lista;
	}
	
	@Transient
	public static List<NotaStatus> getTodosEstadosProduto() {
		List<NotaStatus> lista = new ArrayList<NotaStatus>();
		
		lista.add(EMITIDA);
		lista.add(LIQUIDADA);
		lista.add(CANCELADA);
		
		lista.add(NFE_SOLICITADA);
		lista.add(NFE_ENVIADA);
		lista.add(NFE_EMITIDA);
		lista.add(NFE_LIQUIDADA);
		lista.add(NFE_CANCELANDO);
		lista.add(NFE_DENEGADA);
		
		lista.add(EM_ESPERA);
		
		return lista;
	}
	
	@Transient
	public static List<NotaStatus> getTodosSaidaFiscal() {
		List<NotaStatus> lista = new ArrayList<NotaStatus>();
		
		lista.add(EMITIDA);
		lista.add(LIQUIDADA);
		lista.add(CANCELADA);
		
		
		lista.add(NFSE_SOLICITADA);
		lista.add(NFSE_ENVIADA);
		lista.add(NFSE_EMITIDA);
		lista.add(NFSE_LIQUIDADA);
		lista.add(NFSE_CANCELANDO);		
		
		lista.add(NFE_SOLICITADA);
		lista.add(NFE_ENVIADA);
		lista.add(NFE_EMITIDA);
		lista.add(NFE_LIQUIDADA);
		lista.add(NFE_CANCELANDO);
		lista.add(NFE_DENEGADA);
		
		lista.add(EM_ESPERA);
		
		return lista;
	}
	
	/**
	 * Retorna uma inst�ncia est�tica de NotaTipo que possua a mesma
	 * chave prim�ria da "outra"
	 *
	 * @param outra
	 * @return
	 * @throws NullPointerException : 
	 * 				Caso o par�metro outra chegue nulo ou com o cdNotaStatus nulo.
	 *
	 * @author Hugo Ferreira
	 */
	public static NotaStatus valueOf(NotaStatus outra) {
		if (outra == null || outra.getCdNotaStatus() == null) {
			throw new NullPointerException("O par�metro outra de NotaStatus.valueOf n�o deve ser nulo.");
		}
		
		if (outra.equals(EMITIDA)) {
			return EMITIDA;
		} else if (outra.equals(LIQUIDADA)) {
			return LIQUIDADA;
		} else if (outra.equals(CANCELADA)) {
			return CANCELADA;
		} else if (outra.equals(NFSE_SOLICITADA)) {
			return NFSE_SOLICITADA;
		} else if (outra.equals(NFSE_ENVIADA)) {
			return NFSE_ENVIADA;
		} else if (outra.equals(NFSE_EMITIDA)) {
			return NFSE_EMITIDA;
		} else if (outra.equals(NFSE_LIQUIDADA)) {
			return NFSE_LIQUIDADA;
		} else if (outra.equals(NFE_SOLICITADA)) {
			return NFE_SOLICITADA;
		} else if (outra.equals(NFE_ENVIADA)) {
			return NFE_ENVIADA;
		} else if (outra.equals(NFE_EMITIDA)) {
			return NFE_EMITIDA;
		} else if (outra.equals(NFE_LIQUIDADA)) {
			return NFE_LIQUIDADA;
		} else if (outra.equals(NFE_DENEGADA)) {
			return NFE_DENEGADA;
		} else if (outra.equals(NFE_CANCELANDO)) {
			return NFE_CANCELANDO;
		} else if (outra.equals(NFSE_CANCELANDO)) {
			return NFSE_CANCELANDO;
		} else if (outra.equals(EM_ESPERA)) {
			return EM_ESPERA;
		}
		
		return null;
	}

	/**
	 * Retorna uma inst�ncia est�tica de NotaTipo que possua a mesma
	 * chave prim�ria da "outra"
	 *
	 * @param cdNotaStatus
	 * @return
	 *
	 * @author Hugo Ferreira
	 */
	public static NotaStatus valueOf(int cdNotaStatus) {
		if (EMITIDA.cdNotaStatus.equals(cdNotaStatus)) {
			return EMITIDA;
		} else if (LIQUIDADA.cdNotaStatus.equals(cdNotaStatus)) {
			return LIQUIDADA;
		} else if (CANCELADA.cdNotaStatus.equals(cdNotaStatus)) {
			return CANCELADA;
		} else if (NFSE_SOLICITADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFSE_SOLICITADA;
		} else if (NFSE_ENVIADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFSE_ENVIADA;
		} else if (NFSE_EMITIDA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFSE_EMITIDA;
		} else if (NFSE_LIQUIDADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFSE_LIQUIDADA;
		} else if (NFE_SOLICITADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_SOLICITADA;
		} else if (NFE_ENVIADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_ENVIADA;
		} else if (NFE_EMITIDA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_EMITIDA;
		} else if (NFE_LIQUIDADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_LIQUIDADA;
		} else if (NFE_CANCELANDO.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_CANCELANDO;
		} else if (NFE_DENEGADA.cdNotaStatus.equals(cdNotaStatus)) {
			return NFE_DENEGADA;
		} else if (NFSE_CANCELANDO.cdNotaStatus.equals(cdNotaStatus)) {
			return NFSE_CANCELANDO;
		} else if (EM_ESPERA.cdNotaStatus.equals(cdNotaStatus)) {
			return EM_ESPERA;
		}
		
		return null;
	}
	
	/**
	 * Retorna os nomes das NotaStatus's, separados por v�rgula,
	 * presentes na lista do par�metro.
	 * Usado para apresentar os dados nos cabe�alhos dos relat�rios
	 * 
	 * @param lista
	 * @return
	 * @author Hugo Ferreira
	 */
	public static String getDescriptionProperties(List<NotaStatus> lista) {
		if (lista == null || lista.isEmpty()) {
			lista = getTodosEstados();
		}
		
		for (int i = 0; i < lista.size(); i++) {
			NotaStatus notaStatus = lista.get(i);
			lista.remove(i);
			lista.add(i, valueOf(notaStatus));
		}
		
		String listAndConcatenate = CollectionsUtil.listAndConcatenate(lista, "nome", "; ");
		return listAndConcatenate + (StringUtils.isNotBlank(listAndConcatenate) ? ";" : "");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdNotaStatus == null) ? 0 : cdNotaStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof NotaStatus))
			return false;
		final NotaStatus other = (NotaStatus) obj;
		if (cdNotaStatus == null) {
			if (other.cdNotaStatus != null)
				return false;
		} else if (!cdNotaStatus.equals(other.cdNotaStatus))
			return false;
		return true;
	}

	
}