package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.GeraMovimentacaocheque;

@DisplayName("Gerar Movimenta��o Financeira")
public class GeraMovimentacao {

	protected Movimentacao movimentacaoorigem;
	protected Movimentacao movimentacaodestino;
	protected Rateio rateio;
	protected Rateio rateio2;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean gerarConciliacaoCredito;
	protected Boolean naoBuscarHistoricoPadrao;
	protected Boolean naoBuscarHistoricoComplementarPadrao;
	
	protected List<GeraMovimentacaocheque> listaGeraMovimentacaocheque;
	
	/**
	 * Chama o m�todo {@link #instance()}
	 */
	public GeraMovimentacao() {
		instance();
	}
	
	/**
	 * O construtor padr�o j� cria inst�ncias de <code>movimentacaoorigem</code>,<code>movimentacaodestino</code>
	 * <code>rateio</code> e <code>rateio2</code> dependendo do par�metro.
	 * 
	 * @param preinstance
	 */
	public GeraMovimentacao(boolean preinstance) {
		if(preinstance) instance();
	}
	/**
	 * Inicializador de objetos.
	 */
	private void instance(){
		this.movimentacaodestino = new Movimentacao();
		this.movimentacaoorigem = new Movimentacao();
		this.rateio = new Rateio();
		this.rateio2 = new Rateio();
		this.gerarConciliacaoCredito = false;
	}
	
	
	public Movimentacao getMovimentacaoorigem() {
		return movimentacaoorigem;
	}
	public Movimentacao getMovimentacaodestino() {
		return movimentacaodestino;
	}
	public Rateio getRateio() {
		return rateio;
	}
	public Rateio getRateio2() {
		return rateio2;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setMovimentacaoorigem(Movimentacao movimentacaoorigem) {
		this.movimentacaoorigem = movimentacaoorigem;
	}
	public void setMovimentacaodestino(Movimentacao movimentacaodestino) {
		this.movimentacaodestino = movimentacaodestino;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setRateio2(Rateio rateio2) {
		this.rateio2 = rateio2;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public List<GeraMovimentacaocheque> getListaGeraMovimentacaocheque() {
		return listaGeraMovimentacaocheque;
	}
	public void setListaGeraMovimentacaocheque(List<GeraMovimentacaocheque> listaGeraMovimentacaocheque) {
		this.listaGeraMovimentacaocheque = listaGeraMovimentacaocheque;
	}
	
	public Boolean getGerarConciliacaoCredito() {
		return gerarConciliacaoCredito;
	}

	public void setGerarConciliacaoCredito(Boolean gerarConciliacaoCredito) {
		this.gerarConciliacaoCredito = gerarConciliacaoCredito;
	}
	public Boolean getNaoBuscarHistoricoPadrao() {
		return naoBuscarHistoricoPadrao;
	}
	public void setNaoBuscarHistoricoPadrao(Boolean naoBuscarHistoricoPadrao) {
		this.naoBuscarHistoricoPadrao = naoBuscarHistoricoPadrao;
	}
	public Boolean getNaoBuscarHistoricoComplementarPadrao() {
		return naoBuscarHistoricoComplementarPadrao;
	}
	public void setNaoBuscarHistoricoComplementarPadrao(
			Boolean naoBuscarHistoricoComplementarPadrao) {
		this.naoBuscarHistoricoComplementarPadrao = naoBuscarHistoricoComplementarPadrao;
	}
}
