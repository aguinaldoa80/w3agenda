package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_acaopapel", sequenceName = "sq_acaopapel")
@DisplayName("Permiss�o de a��es")
public class Acaopapel{

	protected Integer cdacaopapel;
	protected Papel papel;
	protected Acao acao;
	protected Boolean permitido;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_acaopapel")
	public Integer getCdacaopapel() {
		return cdacaopapel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	@DisplayName("Papel")
	@Required
	public Papel getPapel() {
		return papel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdacao")
	@DisplayName("A��o")
	@Required
	public Acao getAcao() {
		return acao;
	}
	
	@DisplayName("Permitido")
	public Boolean getPermitido() {
		return permitido;
	}

	public void setCdacaopapel(Integer cdacaopapel) {
		this.cdacaopapel = cdacaopapel;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}

	public void setPermitido(Boolean permitido) {
		this.permitido = permitido;
	}
	
	//log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
		
	
}
