package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contratoparcela", sequenceName = "sq_contratoparcela")
public class Contratoparcela {

	protected Integer cdcontratoparcela;
	protected Contrato contrato;
	protected Date dtvencimento;
	protected Money desconto;
	protected Money valor;
	protected Money valornfservico;
	protected Money valornfproduto;
	protected Boolean aplicarindice;
	protected Boolean cobrada;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratoparcela")
	public Integer getCdcontratoparcela() {
		return cdcontratoparcela;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@Required
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	@Required
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}

	@Required
	@DisplayName("Aplicar �ndice?")
	public Boolean getAplicarindice() {
		return aplicarindice;
	}

	@DisplayName("Cobrada?")
	public Boolean getCobrada() {
		return cobrada;
	}
	
	@DisplayName("Desconto")
	public Money getDesconto() {
		return desconto;
	}
	
	@DisplayName("Valor da NF de Servi�o")
	public Money getValornfservico() {
		return valornfservico;
	}

	@DisplayName("Valor da NF de Produto")
	public Money getValornfproduto() {
		return valornfproduto;
	}

	public void setValornfservico(Money valornfservico) {
		this.valornfservico = valornfservico;
	}

	public void setValornfproduto(Money valornfproduto) {
		this.valornfproduto = valornfproduto;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setAplicarindice(Boolean aplicarindice) {
		this.aplicarindice = aplicarindice;
	}

	public void setCobrada(Boolean cobrada) {
		this.cobrada = cobrada;
	}

	public void setCdcontratoparcela(Integer cdcontratoparcela) {
		this.cdcontratoparcela = cdcontratoparcela;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	@Transient
	public Money getValorContratoParcelaComDesconto(Money totalDescontos){
		Money valor = new Money(this.getValor());
		valor = valor.subtract(totalDescontos);
		valor = valor.subtract(this.getDesconto() != null ? this.getDesconto() : new Money(0.0));
		if(valor.getValue().doubleValue() < 0.0)
			valor = new Money(0.0);
		return valor;
	}
	
}
