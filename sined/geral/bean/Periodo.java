package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_periodo", sequenceName = "sq_periodo")
public class Periodo implements Log {
	
	protected Integer cdperiodo;
	protected Cargo cargo;
	protected Planejamento planejamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Periodocargo> listaPeriodocargo = new ArrayList<Periodocargo>();
	
	protected Integer total;
	protected List<Double> listaCargo = new ArrayList<Double>();
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_periodo")
	public Integer getCdperiodo() {
		return cdperiodo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCdperiodo(Integer cdperiodo) {
		this.cdperiodo = cdperiodo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	@OneToMany(mappedBy="periodo")
	public List<Periodocargo> getListaPeriodocargo() {
		return listaPeriodocargo;
	}
	public void setListaPeriodocargo(List<Periodocargo> listaPeriodocargo) {
		this.listaPeriodocargo = listaPeriodocargo;
	}
	
	@Transient
	public Integer getTotal() {
		return total;
	}
	@Transient
	public List<Double> getListaCargo() {
		return listaCargo;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public void setListaCargo(List<Double> listaCargo) {
		this.listaCargo = listaCargo;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
