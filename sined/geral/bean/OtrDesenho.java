package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_otrdesenho", sequenceName = "sq_otrdesenho")
public class OtrDesenho implements Log{
	protected Integer cdOtrDesenho;
	protected String desenho;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrdesenho")
	public Integer getCdOtrDesenho() {
		return cdOtrDesenho;
	}
	public void setCdOtrDesenho(Integer cdOtrDesenho) {
		this.cdOtrDesenho = cdOtrDesenho;
	}
	@DescriptionProperty
	public String getDesenho() {
		return desenho;
	}
	public void setDesenho(String desenho) {
		this.desenho = desenho;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
