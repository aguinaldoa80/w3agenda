package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_notafiscalprodutocoleta", sequenceName = "sq_notafiscalprodutocoleta")
public class NotafiscalprodutoColeta{

	protected Integer cdnotafiscalprodutocoleta;
	protected Coleta coleta;
	protected Notafiscalproduto notafiscalproduto;
	protected Boolean devolucao;
	
	public NotafiscalprodutoColeta() {}
	public NotafiscalprodutoColeta(Integer cdcoleta, Integer cdnotafiscalproduto) {
		this.coleta = new Coleta(cdcoleta);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
		this.devolucao = Boolean.FALSE;
	}
	public NotafiscalprodutoColeta(Integer cdcoleta, Integer cdnotafiscalproduto, Boolean devolucao) {
		this.coleta = new Coleta(cdcoleta);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
		this.devolucao = devolucao != null ? devolucao : Boolean.FALSE;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutocoleta")
	public Integer getCdnotafiscalprodutocoleta() {
		return cdnotafiscalprodutocoleta;
	}
	public void setCdnotafiscalprodutocoleta(Integer cdnotafiscalprodutocoleta) {
		this.cdnotafiscalprodutocoleta = cdnotafiscalprodutocoleta;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	
	public Boolean getDevolucao() {
		return devolucao;
	}
	public void setDevolucao(Boolean devolucao) {
		this.devolucao = devolucao;
	}
}