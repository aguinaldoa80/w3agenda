package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_documentoenvioboletocliente", sequenceName="sq_documentoenvioboletocliente")
public class Documentoenvioboletocliente {

	private Integer cddocumentoenvioboletocliente;
	private Documentoenvioboleto documentoenvioboleto;
	private Cliente cliente;
	
	@Id
	@GeneratedValue(generator="sq_documentoenvioboletocliente", strategy=GenerationType.AUTO)
	public Integer getCddocumentoenvioboletocliente() {
		return cddocumentoenvioboletocliente;
	}
	public void setCddocumentoenvioboletocliente(
			Integer cddocumentoenvioboletocliente) {
		this.cddocumentoenvioboletocliente = cddocumentoenvioboletocliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoenvioboleto")
	public Documentoenvioboleto getDocumentoenvioboleto() {
		return documentoenvioboleto;
	}
	public void setDocumentoenvioboleto(
			Documentoenvioboleto documentoenvioboleto) {
		this.documentoenvioboleto = documentoenvioboleto;
	}
	
	@Required
	@DisplayName("Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
