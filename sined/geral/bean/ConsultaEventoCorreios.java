package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_consultaeventocorreios", sequenceName="sq_consultaeventocorreios")
public class ConsultaEventoCorreios {

	private Integer cdConsultaEventoCorreios;
	private String objeto;
	private Date dtConsulta;
	private Integer quantidadeConsultas;
	private Boolean consultaManualRealizada;
	
	
	@Id
	@GeneratedValue(generator="sq_consultaeventocorreios", strategy=GenerationType.AUTO)
	public Integer getCdConsultaEventoCorreios() {
		return cdConsultaEventoCorreios;
	}
	public void setCdConsultaEventoCorreios(Integer cdConsultaEventoCorreios) {
		this.cdConsultaEventoCorreios = cdConsultaEventoCorreios;
	}
	public String getObjeto() {
		return objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}
	public Date getDtConsulta() {
		return dtConsulta;
	}
	public void setDtConsulta(Date data) {
		this.dtConsulta = data;
	}
	public Integer getQuantidadeConsultas() {
		return quantidadeConsultas;
	}
	public void setQuantidadeConsultas(Integer quantidadeConsultas) {
		this.quantidadeConsultas = quantidadeConsultas;
	}
	public Boolean getConsultaManualRealizada() {
		return consultaManualRealizada;
	}
	public void setConsultaManualRealizada(Boolean consultaManualRealizada) {
		this.consultaManualRealizada = consultaManualRealizada;
	}
}
