package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_projetotipo",sequenceName="sq_projetotipo")
public class Projetotipo implements Log {
	
	protected Integer cdprojetotipo;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Projetotipoitem> listaProjetotipoitem = new ListSet<Projetotipoitem>(Projetotipoitem.class);
	
	@Id
	@GeneratedValue(generator="sq_projetotipo",strategy=GenerationType.AUTO)
	public Integer getCdprojetotipo() {
		return cdprojetotipo;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	@OneToMany(mappedBy="projetotipo")
	public List<Projetotipoitem> getListaProjetotipoitem() {
		return listaProjetotipoitem;
	}
	
	public void setListaProjetotipoitem(
			List<Projetotipoitem> listaProjetotipoitem) {
		this.listaProjetotipoitem = listaProjetotipoitem;
	}

	public void setCdprojetotipo(Integer cdprojetotipo) {
		this.cdprojetotipo = cdprojetotipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
