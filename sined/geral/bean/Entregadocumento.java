package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentoemitente;
import br.com.linkcom.sined.geral.bean.enumeration.Entregadocumentosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formapagamentonfe;
import br.com.linkcom.sined.geral.bean.enumeration.RetornoWMSEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodocumento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotitulocredito;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoiss;
import br.com.linkcom.sined.geral.bean.view.Vwentregadocumentoimposto;
import br.com.linkcom.sined.modulo.fiscal.controller.process.bean.Aux_apuracaoimposto;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_entregadocumento", sequenceName = "sq_entregadocumento")
@JoinEmpresa("entregadocumento.empresa")
public class Entregadocumento implements Log, PermissaoFornecedorEmpresa {

	protected Integer cdentregadocumento;
	protected Empresa empresa;
	protected Fornecedor fornecedor;
	protected Entrega entrega;
	protected Date dtentrada;
	protected Date dtemissao;	
	protected String numero;
	protected Integer especie;
	protected String serie;
	protected String subserie;
	protected Situacaodocumento situacaodocumento;
	protected Modelodocumentofiscal modelodocumentofiscal;
	protected String natop;
	protected Formapagamentonfe indpag;
	protected Municipio municipioorigem;
	protected Boolean municipioorigemexterior;
	protected Municipio municipiodestino;
	protected Boolean municipiodestinoexterior;
	protected Money valormercadoria;
	protected Money valoroutrasdespesas;
	protected Money valordesconto;
	protected Money valorfrete;
	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	protected ResponsavelFrete responsavelfrete;
	protected String transportadora;
	protected String placaveiculo;	
	protected Money valorseguro;	
	protected Money valor;
	protected Rateio rateio;
	protected String chaveacesso;
	protected Documentotipo documentotipo;
	protected Entregadocumentosituacao entregadocumentosituacao;
	protected Naturezaoperacao naturezaoperacao;
	protected Entregadocumentoemitente emitente = Entregadocumentoemitente.EMISSAO_PROPRIA;
	protected Money valorfretepeso;
	protected Money valortaxacoleta;
	protected Money valortaxaentrega;
	protected Money valorseccat;
	protected Money valorpedagio;
	protected Money diferencaduplicata;
	protected List<EntregaDocumentoApropriacao> listaApropriacao;
	
	protected Set<Entregamaterial> listaEntregamaterial;
	protected Set<Entregapagamento> listadocumento = new ListSet<Entregapagamento>(Entregapagamento.class);
	protected Set<Entregadocumentohistorico> listaEntregadocumentohistorico;
	protected Set<Entregadocumentofrete> listaEntregadocumentofrete;
	protected Set<Entregadocumentofreterateio> listaEntregadocumentofreterateio;
	protected Set<Entregadocumentoreferenciado> listaEntregadocumentoreferenciado;
	protected List<EntregadocumentoColeta> listaEntregadocumentoColeta;
	protected Set<Ajustefiscal> listaAjustefiscal;
	
	protected Tipotitulocredito tipotitulo;
	protected String numerotitulo;
	protected String descricaotitulo;
	protected Prazopagamento prazopagamento;
	protected Boolean simplesremessa;
	protected RetornoWMSEnum retornowms;
	protected Boolean fornecedorAvaliado;
	protected Boolean vincularOutrosDocumentos;
	
//	TRANSIENT
	protected String identificadortela;
	protected List<Vwentregadocumentoimposto> listaImposto;
	protected Integer repeticoes;
	protected Money valorpagamento;
	protected Money valortotal;
	protected Money valordocumento;
	protected boolean validaQtdeEntrega = true;
	protected Date dtemissaotrans;
	protected Integer indexlista;
	protected Integer indexlistaforrateio;
	protected Money valortotaldocumento;
	protected String numerotrans;
	protected Money valortotalimposto;
	protected List<Entregamaterial> listaEntregamaterialTrans;
	protected String acao;
	protected String situacaosped;
	protected String fornecedornome;
	protected String fornecedorcdpessoa;
	protected String cdentrega;
	protected String idFornecimento;
	protected Boolean avaliado;
	protected String idDocumentoRegistrarEntrada;
	protected String listDocumentoRegistrarEntrada;
	protected Arquivo arquivoxmlnfe;
	protected Money valoricmsst;
	protected Money valorfcpst;
	protected Money valoripi;
	protected Boolean fromPedidoColeta;
	protected Boolean fromColeta;
	protected String whereInPedido;
	protected String whereInColeta;
	protected String whereInDevolucoes;
	protected Money valoricmsstTotaldocumento;
	private Money valoricmsdesoneradoTotaldocumento;
	protected Money valorfcpstTotaldocumento;
	protected String cdsentregadocumento;
	protected String cdentregadocumentofreteRemover;
	protected String posicaoListaForRateioParam;
	protected String posicaoListaParam;
	protected String cdentregaParam;
	protected Set<Entregadocumentoreferenciado> listaEntregadocumentoreferenciadoTrans;
	protected Boolean producaoautomatica = Boolean.FALSE;
	protected Money valorreceita;
	protected Money valormercadoriareceita;
	protected Boolean existeNotaDevolucao;
	protected String isFaturar;
	protected String observacao;
	protected String whereIn;
	protected Boolean fromRecebimento;
	protected String identificadortelaRecebimento;
	protected String cdempresaentrega;
	protected List<Material> listaMaterialitemgrade;

	//	TRANSIENT PARA TOTALIZADORES
	protected Money valortotalicms;
	protected Money valortotalicmsst;
	private Money valortotalicmsdesonerado;
	protected Money valortotalfcp;
	protected Money valortotalfcpst;
	protected Money valortotalipi;
	protected Money valortotalpis;
	protected Money valortotalcofins;
	protected Money valortotalpisretido;
	protected Money valortotalcofinsretido;
	protected Money valortotalbcicms;
	protected Money valortotalbcicmsst;
	protected Money valortotalbcfcp;
	protected Money valortotalbcfcpst;
	protected Money valortotalbcipi;
	protected Money valortotalbcpis;
	protected Money valortotalbccofins;
	protected Money valortotalbcii;
	protected Money valortotalbciss;
	protected Money valortotalir;
	protected Money valortotalcsll;
	protected Money valortotalinss;
	protected Money valortotalimpostoimportacao;
	protected Money valortotaliof;
	protected Money valortotaliss;
	protected Aux_apuracaoimposto aux_apuracaoimposto;
	protected Money valorTotalDocumentoCTRC;
	protected Money valorTotalRateioCTRC;
	protected Money valorRestanteCTRC;
	protected Boolean calculoRateio = Boolean.TRUE;
	protected Money valorImportacao;
	protected Boolean isReceberEntrega;
	protected String htmlChaveacesso;
	protected List<Pedidovendamaterial> listaPedidovendamaterial;
	protected Date dataForGeracaoContabil;
	
//	TRANSIENT INTEGRACAO
	protected Entregadocumentosincronizacao entregadocumentosincronizacao;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	
	
	public Entregadocumento(){}
	
	public Entregadocumento(Integer cdentregadocumento){
		this.cdentregadocumento = cdentregadocumento;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumento")
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}	
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Data de Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("Esp�cie")
	public Integer getEspecie() {
		return especie;
	}
	@DisplayName("Outras despesas (+)")
	public Money getValoroutrasdespesas() {
		return valoroutrasdespesas;
	}
	@DisplayName("Valor do Desconto (-)")
	public Money getValordesconto() {
		return valordesconto;
	}
	@DisplayName("Valor do Frete (+)")
	public Money getValorfrete() {
		return valorfrete;
	}
	@DisplayName("Inf. Ad. Fisco")
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	@DisplayName("Inf. Ad. Contribuinte")
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}
	@DisplayName("Respons�vel pelo Frete")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavelfrete")
	public ResponsavelFrete getResponsavelfrete() {
		return responsavelfrete;
	}
	@DisplayName("Transportadora")
	public String getTransportadora() {
		return transportadora;
	}
	@DisplayName("Placa do Ve�culo")
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@DisplayName("Valor do Seguro (+)")
	public Money getValorseguro() {
		return valorseguro;
	}
	@DisplayName("Valor do Documento")
	public Money getValor() {
		return valor;
	}
	@DisplayName("Itens")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregamaterial> getListaEntregamaterial() {
		return listaEntregamaterial;
	}
	@DisplayName("Pagamentos")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregapagamento> getListadocumento() {
		return listadocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}	
	@DescriptionProperty
	@Required
	@MaxLength(50)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}	
	@MaxLength(9)
	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}	
	@DisplayName("Natureza da Opera��o")
	@MaxLength(60)
	public String getNatop() {
		return natop;
	}	
	@DisplayName("Forma de pagamento")
	public Formapagamentonfe getIndpag() {
		return indpag;
	}
	@DisplayName("Munic�pio de Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipioorigem")
	public Municipio getMunicipioorigem() {
		return municipioorigem;
	}
	@DisplayName("Munic�pio de Origem - Exterior")
	public Boolean getMunicipioorigemexterior() {
		return municipioorigemexterior;
	}
	@DisplayName("Munic�pio de Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipiodestino")
	public Municipio getMunicipiodestino() {
		return municipiodestino;
	}
	@DisplayName("Munic�pio de Destino - Exterior")
	public Boolean getMunicipiodestinoexterior() {
		return municipiodestinoexterior;
	}
	@DisplayName("Situa��o Fiscal do Documento")
	public Situacaodocumento getSituacaodocumento() {
		return situacaodocumento;
	}	
	@Required
	@DisplayName("Modelo da NF-e")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmodelodocumentofiscal")
	public Modelodocumentofiscal getModelodocumentofiscal() {
		return modelodocumentofiscal;
	}
	@DisplayName("Data entrada")
	public Date getDtentrada() {
		return dtentrada;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}	
	@DisplayName("Tipo de t�tulo de cr�dito")
	public Tipotitulocredito getTipotitulo() {
		return tipotitulo;
	}	
	@DisplayName("N�mero do t�tulo")
	@MaxLength(50)
	public String getNumerotitulo() {
		return numerotitulo;
	}
	@DisplayName("Descri��o(�es) do(s) t�tulo(s)")
	@MaxLength(200)
	public String getDescricaotitulo() {
		
		return descricaotitulo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@DisplayName("Simples remessa")
	public Boolean getSimplesremessa() {
		return simplesremessa;
	}
	@MaxLength(100)
	@DisplayName("CHAVE DE ACESSO")
	public String getChaveacesso() {
		return chaveacesso;
	}
	@Required
	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@DisplayName("Situa��o")
	public Entregadocumentosituacao getEntregadocumentosituacao() {
		return entregadocumentosituacao;
	}
	@DisplayName("Hist�rico")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregadocumentohistorico> getListaEntregadocumentohistorico() {
		return listaEntregadocumentohistorico;
	}
	public RetornoWMSEnum getRetornowms() {
		return retornowms;
	}
	@DisplayName("Natureza da Opera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}		
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setEspecie(Integer especie) {
		this.especie = especie;
	}
	public void setValoroutrasdespesas(Money valoroutrasdespesas) {
		this.valoroutrasdespesas = valoroutrasdespesas;
	}
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
	public void setResponsavelfrete(ResponsavelFrete responsavelfrete) {
		this.responsavelfrete = responsavelfrete;
	}
	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}
	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setValorseguro(Money valorseguro) {
		this.valorseguro = valorseguro;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setListaEntregamaterial(Set<Entregamaterial> listaEntregamaterial) {
		this.listaEntregamaterial = listaEntregamaterial;
	}	
	public void setListadocumento(Set<Entregapagamento> listadocumento) {
		this.listadocumento = listadocumento;
	}
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}	
	public void setNatop(String natop) {
		this.natop = natop;
	}
	public void setIndpag(Formapagamentonfe indpag) {
		this.indpag = indpag;
	}
	public void setMunicipioorigem(Municipio municipioorigem) {
		this.municipioorigem = municipioorigem;
	}
	public void setMunicipioorigemexterior(Boolean municipioorigemexterior) {
		this.municipioorigemexterior = municipioorigemexterior;
	}
	public void setMunicipiodestino(Municipio municipiodestino) {
		this.municipiodestino = municipiodestino;
	}
	public void setMunicipiodestinoexterior(Boolean municipiodestinoexterior) {
		this.municipiodestinoexterior = municipiodestinoexterior;
	}
	public void setSituacaodocumento(Situacaodocumento situacaodocumento) {
		this.situacaodocumento = situacaodocumento;
	}
	public void setModelodocumentofiscal(
			Modelodocumentofiscal modelodocumentofiscal) {
		this.modelodocumentofiscal = modelodocumentofiscal;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setNumero(String numero) {
		this.numero = StringUtils.trimToNull(numero);
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}	
	public void setTipotitulo(Tipotitulocredito tipotitulo) {
		this.tipotitulo = tipotitulo;
	}
	public void setNumerotitulo(String numerotitulo) {
		this.numerotitulo = numerotitulo;
	}
	public void setDescricaotitulo(String descricaotitulo) {
		this.descricaotitulo = descricaotitulo;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}	
	public void setSimplesremessa(Boolean simplesremessa) {
		this.simplesremessa = simplesremessa;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setEntregadocumentosituacao(Entregadocumentosituacao entregadocumentosituacao) {
		this.entregadocumentosituacao = entregadocumentosituacao;
	}
	public void setListaEntregadocumentohistorico(Set<Entregadocumentohistorico> listaEntregadocumentohistorico) {
		this.listaEntregadocumentohistorico = listaEntregadocumentohistorico;
	}
	public void setRetornowms(RetornoWMSEnum retornowms) {
		this.retornowms = retornowms;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}	
	
	public Boolean getFornecedorAvaliado() {
		return fornecedorAvaliado;
	}
	public void setFornecedorAvaliado(Boolean fornecedorAvaliado) {
		this.fornecedorAvaliado = fornecedorAvaliado;
	}

	public Boolean getVincularOutrosDocumentos() {
		return vincularOutrosDocumentos;
	}
	public void setVincularOutrosDocumentos(Boolean vincularOutrosDocumentos) {
		this.vincularOutrosDocumentos = vincularOutrosDocumentos;
	}

	//	LOG	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	TRANSIENT
	@Transient
	@DisplayName("Valor Mercadoria (Valor bruto)")
	public Money getValormercadoria() {
		return getValorCalculadoMercadoria(false);
	}
	
	public Money getValorCalculadoMercadoria(Boolean verificaCfop) {
		Money valor = new Money();		
		Double qtde = 1.0;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			criaTotaisimposto();
			for(Entregamaterial em : listaEntregamaterial){
				if(verificaCfop != null && verificaCfop && em.getCfop() != null && em.getCfop().getNaoconsiderarreceita() != null && 
						em.getCfop().getNaoconsiderarreceita()){
					continue;
				}
				
				qtde = em.getQtde() != null ? em.getQtde() : 1.0;
				if(em.getValorunitario() != null){
					valor = valor.add(new Money(SinedUtil.round(SinedUtil.round(new BigDecimal(em.getValorunitario()).multiply(new BigDecimal(qtde)), 4),2)));
					
					if(em.getValorpisretido() != null && em.getValorpisretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorpisretido());
					}
					if(em.getValorcofinsretido() != null && em.getValorcofinsretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorcofinsretido());
					}
				}				
			}
//			valor = valor.add(this.getValortotalicmsst() != null ? this.getValortotalicmsst():new Money());
//			valor = valor.add(this.getValortotalipi() != null ? this.getValortotalipi():new Money());
			valor = valor.add(this.getValortotalimpostoimportacao() != null ? this.getValortotalimpostoimportacao():new Money());
			
			valor = valor.subtract(this.getValortotalir() != null ? this.getValortotalir():new Money());
			valor = valor.subtract(this.getValortotalcsll() != null ? this.getValortotalcsll():new Money());
			valor = valor.subtract(this.getValortotalinss() != null ? this.getValortotalinss():new Money());			
			//valor = valor.subtract(this.getValortotaliss() != null ? this.getValortotaliss():new Money());
			
		}
		return valor;
	}
	@Transient
	public Money getValormercadoriaForDominio() {
		Money valor = new Money();		
		Double qtde = 1.0;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				qtde = em.getQtde() != null ? em.getQtde() : 1.0;
				if(em.getValorunitario() != null){
					valor = valor.add(new Money(em.getValorunitario()*qtde));
					
					if(em.getValorpisretido() != null && em.getValorpisretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorpisretido());
					}
					if(em.getValorcofinsretido() != null && em.getValorcofinsretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorcofinsretido());
					}
				}				
			}
			valor = valor.add(this.getValortotalicmsst() != null ? this.getValortotalicmsst():new Money());
			valor = valor.add(this.getValortotalipi() != null ? this.getValortotalipi():new Money());
			valor = valor.add(this.getValortotalimpostoimportacao() != null ? this.getValortotalimpostoimportacao():new Money());
			
			valor = valor.subtract(this.getValortotalir() != null ? this.getValortotalir():new Money());
			valor = valor.subtract(this.getValortotalcsll() != null ? this.getValortotalcsll():new Money());
			valor = valor.subtract(this.getValortotalinss() != null ? this.getValortotalinss():new Money());			
			valor = valor.subtract(this.getValortotaliss() != null ? this.getValortotaliss():new Money());
			
		}
		return valor;
	}	
	@Transient
	public Money getValormercadoriaSPED() {
		Money valor = new Money();		
		Double qtde = 1.0;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				qtde = em.getQtde() != null ? em.getQtde() : 1.0;
				if(em.getValorunitario() != null){
					valor = valor.add(new Money(em.getValorunitario()*qtde));
					
					if(em.getValorpisretido() != null && em.getValorpisretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorpisretido());
					}
					if(em.getValorcofinsretido() != null && em.getValorcofinsretido().getValue().doubleValue() > 0){
						valor = valor.subtract(em.getValorcofinsretido());
					}
				}
			}
			
			valor = valor.add(this.getValortotalicmsst() != null ? this.getValortotalicmsst():new Money());
			valor = valor.add(this.getValortotalipi() != null ? this.getValortotalipi():new Money());
			valor = valor.add(this.getValoroutrasdespesas() != null ? this.getValoroutrasdespesas():new Money());
//			valor = valor.add(this.getValorfrete() != null ? this.getValorfrete():new Money());
			valor = valor.add(this.getValortotalimpostoimportacao() != null ? this.getValortotalimpostoimportacao():new Money());
			
			valor = valor.subtract(this.getValordesconto() != null ? this.getValordesconto():new Money());			
			valor = valor.subtract(this.getValortotalir() != null ? this.getValortotalir():new Money());
			valor = valor.subtract(this.getValortotalcsll() != null ? this.getValortotalcsll():new Money());
			valor = valor.subtract(this.getValortotalinss() != null ? this.getValortotalinss():new Money());
			valor = valor.subtract(this.getValortotaliss() != null ? this.getValortotaliss():new Money());
		}
		return valor;
	}
	@Transient
	public Money getValormercadoriaSPEDPiscofins() {
		Money valor = new Money();		
		Double qtde = 1.0;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				qtde = em.getQtde() != null ? em.getQtde() : 1.0;
				if(em.getValorunitario() != null){
					valor = valor.add(new Money(em.getValorunitario()*qtde));
				}
			}
			
		}
		return valor;
	}
	@Transient
	@MaxLength(3)
	@DisplayName("Repeti��es extras")
	public Integer getRepeticoes() {
		return repeticoes;
	}
	@Transient
	@DisplayName("Valor dos Pagamentos")
	public Money getValorpagamento() {
		return valorpagamento;
	}
	@Transient
	public Money getValortotal() {
		return valortotal;
	}
	@Transient
	public Money getValordocumento() {
		return valordocumento;
	}
	@Transient
	public boolean isValidaQtdeEntrega() {
		return validaQtdeEntrega;
	}
	@Transient
	@DisplayName("Data de Emiss�o")
	public Date getDtemissaotrans() {
		return dtemissaotrans;
	}
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	@Transient
	public Integer getIndexlistaforrateio() {
		return indexlistaforrateio;
	}
	@Transient
	@MaxLength(50)
	@DisplayName("N�mero")
	public String getNumerotrans() {
		return numero;
	}	
	
	public void setValormercadoria(Money valormercadoria) {
		this.valormercadoria = valormercadoria;
	}	
	public void setRepeticoes(Integer repeticoes) {
		this.repeticoes = repeticoes;
	}
	public void setValorpagamento(Money valorpagamento) {
		this.valorpagamento = valorpagamento;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setValordocumento(Money valordocumento) {
		this.valordocumento = valordocumento;
	}
	public void setValidaQtdeEntrega(boolean validaQtdeEntrega) {
		this.validaQtdeEntrega = validaQtdeEntrega;
	}
	public void setDtemissaotrans(Date dtemissaotrans) {
		this.dtemissaotrans = dtemissaotrans;
	}
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	public void setIndexlistaforrateio(Integer indexlistaforrateio) {
		this.indexlistaforrateio = indexlistaforrateio;
	}

	public void setNumerotrans(String numerotrans) {
		this.numerotrans = numerotrans;
	}	
	
	
//	TOTAIS
	@Transient
	@DisplayName("Valor Total")
	public Money getValortotaldocumento() {
		return getValortotaldocumentoCalculado(false);
	}
	
	@Transient
	public Money getValortotaldocumentoreceita() {
		return getValortotaldocumentoCalculado(true);
	}
	
	public Money getValortotaldocumentoCalculado(Boolean verificaCfop) {
		Double valortotal = 0.0;
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for (Entregamaterial entregamaterial : this.getListaEntregamaterial()) {
				if(verificaCfop != null && verificaCfop && entregamaterial.getCfop() != null && 
						entregamaterial.getCfop().getNaoconsiderarreceita() != null && 
						entregamaterial.getCfop().getNaoconsiderarreceita()){
					continue;
				}
				valortotal = valortotal + SinedUtil.round((entregamaterial.getValortotalmaterial() != null ? entregamaterial.getValortotalmaterial() : 0.0), 2);
				valortotal = SinedUtil.round(valortotal, 2);
			}
			valortotal = valortotal + (this.getValorimpostos() != null ? this.getValorimpostos().getValue().doubleValue() : 0.0) + (this.getValorfrete() != null ? this.getValorfrete().getValue().doubleValue() : 0.0) + (this.getValoroutrasdespesas() != null ? this.getValoroutrasdespesas().getValue().doubleValue() : 0.0) + (this.getValorseguro() != null ? this.getValorseguro().getValue().doubleValue() : 0.0)  - (this.getValordesconto() != null ? this.getValordesconto().getValue().doubleValue() : 0.0) + (this.getValorfretepeso() != null ? this.getValorfretepeso().getValue().doubleValue() : 0.0) + (this.getValortaxacoleta() != null ? this.getValortaxacoleta().getValue().doubleValue() : 0.0) + (this.getValortaxaentrega() != null ? this.getValortaxaentrega().getValue().doubleValue() : 0.0) + (this.getValorseccat() != null ? this.getValorseccat().getValue().doubleValue() : 0.0) + (this.getValorpedagio() != null ? this.getValorpedagio().getValue().doubleValue() : 0.0);
			valortotal = SinedUtil.round(valortotal, 2);
		}
		return new Money(valortotal);
	}
	@Transient	
	@DisplayName("Impostos")
	public Money getValortotalimposto() {
		Money total = new Money();
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValoriss() != null)
					total = total.add(em.getValoriss());
				if(em.getValoricms() != null)
					total = total.add(em.getValoricms());
				if(em.getValoricmsst() != null)
					total = total.add(em.getValoricmsst());
				if(em.getValorfcpst() != null)
					total = total.add(em.getValorfcpst());
				if(em.getValoripi() != null)
					total = total.add(em.getValoripi());
				if(em.getValorpis() != null)
					total = total.add(em.getValorpis());
				if(em.getValorcofins() != null)
					total = total.add(em.getValorcofins());					
			}
		}
		return total;
	}
	
	@Transient	
	public Money getValorimpostos() {
		Money total = new Money();
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValoricmsst() != null && (em.getNaoconsideraricmsst() == null || !em.getNaoconsideraricmsst()))
					total = total.add(em.getValoricmsst());
				if(em.getValoripi() != null)
					total = total.add(em.getValoripi());
				if(em.getValorfcpst() != null)
					total = total.add(em.getValorfcpst());
			}
		}
		return total;
	}
	
	public void setValortotaldocumento(Money valortotaldocumento) {
		this.valortotaldocumento = valortotaldocumento;
	}
	public void setValortotalimposto(Money valortotalimposto) {
		this.valortotalimposto = valortotalimposto;
	}
	
//	CRIA LISTA DE IMPSOTO
	@Transient
	public List<Vwentregadocumentoimposto> getListaImposto() {
		return listaImposto;
	}
	public void setListaImposto(List<Vwentregadocumentoimposto> listaImposto) {
		this.listaImposto = listaImposto;
	}
	
	@Transient
	public Money getValortotalicms() {
		if(valortotalicms == null)
			criaTotaisimposto();
		return valortotalicms;
	}
	@Transient
	public Money getValortotalicmsst() {
		if(valortotalicmsst == null)
			criaTotaisimposto();
		return valortotalicmsst;
	}
	@Transient
	public Money getValortotalicmsdesonerado() {
		if(valortotalicmsdesonerado == null)
			criaTotaisimposto();
		return valortotalicmsdesonerado;
	}
	@Transient
	public Money getValortotalfcp() {
		if(valortotalfcp == null)
			criaTotaisimposto();
		return valortotalfcp;
	}
	@Transient
	public Money getValortotalfcpst() {
		if(valortotalfcpst == null)
			criaTotaisimposto();
		return valortotalfcpst;
	}
	@Transient
	public Money getValortotalipi() {
		if(valortotalipi == null)
			criaTotaisimposto();
		return valortotalipi;
	}
	@Transient
	public Money getValortotalpis() {
		if(valortotalpis == null)
			criaTotaisimposto();
		return valortotalpis;
	}
	@Transient
	public Money getValortotalpisretido() {
		if(valortotalpisretido == null)
			criaTotaisimposto();
		return valortotalpisretido;
	}
	@Transient
	public Money getValortotalcofins() {
		if(valortotalcofins == null)
			criaTotaisimposto();
		return valortotalcofins;
	}
	@Transient
	public Money getValortotalcofinsretido() {
		if(valortotalcofinsretido == null)
			criaTotaisimposto();
		return valortotalcofinsretido;
	}
	@Transient
	public Money getValortotalbcicms() {
		if(valortotalbcicms == null)
			criaTotaisimposto();
		return valortotalbcicms;
	}
	@Transient
	public Money getValortotalbcicmsst() {
		if(valortotalbcicmsst == null)
			criaTotaisimposto();
		return valortotalbcicmsst;
	}
	@Transient
	public Money getValortotalbcfcp() {
		if(valortotalbcfcp == null)
			criaTotaisimposto();
		return valortotalbcfcp;
	}
	@Transient
	public Money getValortotalbcfcpst() {
		if(valortotalbcfcpst == null)
			criaTotaisimposto();
		return valortotalbcfcpst;
	}
	@Transient
	public Money getValortotalbcipi() {
		if(valortotalbcipi == null)
			criaTotaisimposto();
		return valortotalbcipi;
	}
	@Transient
	public Money getValortotalbcpis() {
		if(valortotalbcpis == null)
			criaTotaisimposto();
		return valortotalbcpis;
	}
	@Transient
	public Money getValortotalbccofins() {
		if(valortotalbccofins == null)
			criaTotaisimposto();
		return valortotalbccofins;
	}
	@Transient
	public Money getValortotalbcii() {
		if(valortotalbcii == null)
			criaTotaisimposto();
		return valortotalbcii;
	}
	@Transient
	public Money getValortotalir() {
		return valortotalir;
	}
	@Transient
	public Money getValortotalcsll() {
		return valortotalcsll;
	}
	@Transient
	public Money getValortotalinss() {
		return valortotalinss;
	}
	@Transient
	public Money getValortotalimpostoimportacao() {
		return valortotalimpostoimportacao;
	}
	@Transient
	public Money getValortotaliof() {
		return valortotaliof;
	}
	@Transient
	@DisplayName("Valor ISS")
	public Money getValortotaliss() {
		if(valortotaliss == null || valortotaliss.getValue().doubleValue() == 0)
			criaTotaisimposto();
		return valortotaliss;
	}
	@Transient
	public List<Entregamaterial> getListaEntregamaterialTrans() {
		return listaEntregamaterialTrans;
	}
	@Transient
	public String getAcao() {
		return acao;
	}
	@Transient
	public Entregadocumentosincronizacao getEntregadocumentosincronizacao() {
		return entregadocumentosincronizacao;
	}
	@Transient
	public String getSituacaosped() {
		return situacaosped;
	}
	@Transient
	public String getFornecedornome() {
		if(fornecedor != null && fornecedor.getNome() != null){
			return fornecedor.getNome();
		}
		return "";
	}
	@Transient
	public String getFornecedorcdpessoa() {
		if(fornecedor != null && fornecedor.getCdpessoa() != null){
			return fornecedor.getCdpessoa().toString();
		}
		return "";
	}


	@Transient
	public boolean getHaveOrdemcompra(){
		if(getIsReceberEntrega() != null && getIsReceberEntrega()){
			return getIsReceberEntrega();
		}
		if(entrega != null){
			return entrega.getHaveOrdemcompra();
		}
		return false;
	}
	@Transient
	public Boolean getIsReceberEntrega() {
		return isReceberEntrega;
	}
	public void setIsReceberEntrega(Boolean isReceberEntrega) {
		this.isReceberEntrega = isReceberEntrega;
	}
	
	public void setValortotalicms(Money valortotalicms) {
		this.valortotalicms = valortotalicms;
	}
	public void setValortotalicmsst(Money valortotalicmsst) {
		this.valortotalicmsst = valortotalicmsst;
	}
	public void setValortotalicmsdesonerado(Money valortotalicmsdesonerado) {
		this.valortotalicmsdesonerado = valortotalicmsdesonerado;
	}
	public void setValortotalfcp(Money valortotalfcp) {
		this.valortotalfcp = valortotalfcp;
	}
	public void setValortotalfcpst(Money valortotalfcpst) {
		this.valortotalfcpst = valortotalfcpst;
	}
	public void setValortotalipi(Money valortotalipi) {
		this.valortotalipi = valortotalipi;
	}
	public void setValortotalpis(Money valortotalpis) {
		this.valortotalpis = valortotalpis;
	}
	public void setValortotalcofins(Money valortotalcofins) {
		this.valortotalcofins = valortotalcofins;
	}
	public void setValortotalbcicms(Money valortotalbcicms) {
		this.valortotalbcicms = valortotalbcicms;
	}
	public void setValortotalbcicmsst(Money valortotalbcicmsst) {
		this.valortotalbcicmsst = valortotalbcicmsst;
	}
	public void setValortotalbcfcp(Money valortotalbcfcp) {
		this.valortotalbcfcp = valortotalbcfcp;
	}
	public void setValortotalbcfcpst(Money valortotalbcfcpst) {
		this.valortotalbcfcpst = valortotalbcfcpst;
	}
	public void setValortotalbcipi(Money valortotalbcipi) {
		this.valortotalbcipi = valortotalbcipi;
	}
	public void setValortotalbcpis(Money valortotalbcpis) {
		this.valortotalbcpis = valortotalbcpis;
	}
	public void setValortotalbccofins(Money valortotalbccofins) {
		this.valortotalbccofins = valortotalbccofins;
	}
	public void setValortotalbcii(Money valortotalbcii) {
		this.valortotalbcii = valortotalbcii;
	}
	public void setListaEntregamaterialTrans(List<Entregamaterial> listaEntregamaterialTrans) {
		this.listaEntregamaterialTrans = listaEntregamaterialTrans;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setEntregadocumentosincronizacao(Entregadocumentosincronizacao entregadocumentosincronizacao) {
		this.entregadocumentosincronizacao = entregadocumentosincronizacao;
	}
	public void setSituacaosped(String situacaosped) {
		this.situacaosped = situacaosped;
	}
	public void setFornecedornome(String fornecedornome) {
		this.fornecedornome = fornecedornome;
	}
	public void setFornecedorcdpessoa(String fornecedorcdpessoa) {
		this.fornecedorcdpessoa = fornecedorcdpessoa;
	}
	public void setValortotalir(Money valortotalir) {
		this.valortotalir = valortotalir;
	}
	public void setValortotalcsll(Money valortotalcsll) {
		this.valortotalcsll = valortotalcsll;
	}
	public void setValortotalinss(Money valortotalinss) {
		this.valortotalinss = valortotalinss;
	}
	public void setValortotalimpostoimportacao(Money valortotalimpostoimportacao) {
		this.valortotalimpostoimportacao = valortotalimpostoimportacao;
	}
	public void setValortotaliof(Money valortotaliof) {
		this.valortotaliof = valortotaliof;
	}
	public void setValortotaliss(Money valortotaliss) {
		this.valortotaliss = valortotaliss;
	}

	public void criaTotaisimposto(){
		Money totalicms = new Money();
		Money totalicmsst = new Money();
		Money totalfcp = new Money();
		Money totalfcpst = new Money();
		Money totalipi = new Money();
		Money totalpis = new Money();
		Money totalpisretido = new Money();
		Money totalcofins = new Money();
		Money totalcofinsretido = new Money();
		Money totalir = new Money();
		Money totalcsll = new Money();
		Money totalinss = new Money();
		Money totalimpostoimportacao = new Money();
		Money totaliss = new Money();
		Money totaliof = new Money();
		Money totalIcmsDesonerado = new Money();
		
		Money totalbcicms = new Money();
		Money totalbcicmsst = new Money();
		Money totalbcfcp = new Money();
		Money totalbcfcpst = new Money();
		Money totalbcipi = new Money();
		Money totalbcpis = new Money();
		Money totalbccofins = new Money();
		Money totalbcii = new Money();
		Money totalbciss = new Money();
		
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValoricms() != null)
					totalicms = totalicms.add(em.getValoricms());
				if(em.getValoricmsst() != null && (em.getNaoconsideraricmsst() == null || !em.getNaoconsideraricmsst()))
					totalicmsst = totalicmsst.add(em.getValoricmsst());
				if(em.getValorfcp() != null)
					totalfcp = totalfcp.add(em.getValorfcp());
				if(em.getValorfcpst() != null && (em.getNaoconsideraricmsst() == null || !em.getNaoconsideraricmsst()))
					totalfcpst = totalfcpst.add(em.getValorfcpst());
				if(em.getValorcofins() != null)
					totalcofins = totalcofins.add(em.getValorcofins());
				if(em.getValorpis() != null)
					totalpis = totalpis.add(em.getValorpis());
				if(em.getValoripi() != null)
					totalipi = totalipi.add(em.getValoripi());
				if(em.getValorpisSped() != null)
					totalpisretido = totalpisretido.add(em.getValorpisSped());
				if(em.getValorcofinsSped() != null)
					totalcofinsretido = totalcofinsretido.add(em.getValorcofinsSped());
				if(em.getValorIcmsDesonerado() != null)
					totalIcmsDesonerado = totalIcmsDesonerado.add(em.getValorIcmsDesonerado());
				
				if(em.getValorbcicms() != null)
					totalbcicms = totalbcicms.add(em.getValorbcicms());
				if(em.getValorbcicmsst() != null)
					totalbcicmsst = totalbcicmsst.add(em.getValorbcicmsst());
				if(em.getValorbcfcp() != null)
					totalbcfcp = totalbcfcp.add(em.getValorbcfcp());
				if(em.getValorbcfcpst() != null)
					totalbcfcpst = totalbcfcpst.add(em.getValorbcfcpst());
				if(em.getValorbccofins() != null)
					totalbccofins = totalbccofins.add(em.getValorbccofins());
				if(em.getValorbcpis() != null)
					totalbcpis = totalbcpis.add(em.getValorbcpis());
				if(em.getValorbcipi() != null)
					totalbcipi = totalbcipi.add(em.getValorbcipi());
				if(em.getValorbcii() != null)
					totalbcii = totalbcii .add(em.getValorbcii());
				if(em.getValorbciss() != null)
					totalbciss = totalbciss.add(em.getValorbciss());
				
				if(em.getValorir() != null)
					totalir = totalir.add(em.getValorir());
				if(em.getValorcsll() != null)
					totalcsll = totalcsll.add(em.getValorcsll());
				if(em.getValorinss() != null)
					totalinss = totalinss.add(em.getValorinss());
				if(em.getValorimpostoimportacao() != null)
					totalimpostoimportacao = totalimpostoimportacao.add(em.getValorimpostoimportacao());
				if(em.getIof() != null)
					totaliof = totaliof.add(em.getIof());
				
				if(em.getTipotributacaoiss() != null && Tipotributacaoiss.RETIDA.equals(em.getTipotributacaoiss()) &&
						em.getValoriss() != null){
					totaliss = totaliss.add(em.getValoriss());
				}
			}
		}
		
		this.valortotalicms = totalicms;
		this.valortotalicmsst = totalicmsst;
		this.valortotalipi = totalipi;
		this.valortotalpis = totalpis;
		this.valortotalcofins = totalcofins;
		this.valortotalpisretido = totalpisretido;
		this.valortotalcofinsretido = totalcofinsretido;
		
		this.valortotalbcicms = totalbcicms;
		this.valortotalbcicmsst = totalbcicmsst;
		this.valortotalbcipi = totalbcipi;
		this.valortotalbcpis = totalbcpis;
		this.valortotalbccofins = totalbccofins;
		this.valortotalbcii = totalbcii;
		this.valortotalicmsdesonerado = totalIcmsDesonerado;
		
		this.valortotalir = totalir;
		this.valortotalcsll = totalcsll;
		this.valortotalinss = totalinss;
		this.valortotalimpostoimportacao = totalimpostoimportacao;
		this.valortotaliof = totaliof;
		this.valortotaliss = totaliss;
	}

	@Transient
	public String getIdFornecimento() {
		return idFornecimento;
	}
	public void setIdFornecimento(String idFornecimento) {
		this.idFornecimento = idFornecimento;
	}
	@Transient
	public String getCdentrega() {
		return cdentrega;
	}
	public void setCdentrega(String cdentrega) {
		this.cdentrega = cdentrega;
	}
	@Transient
	public Boolean getAvaliado() {
		return avaliado;
	}
	public void setAvaliado(Boolean avaliado) {
		this.avaliado = avaliado;
	}

	@Transient
	public String getIdDocumentoRegistrarEntrada() {
		return idDocumentoRegistrarEntrada;
	}
	public void setIdDocumentoRegistrarEntrada(String idDocumentoRegistrarEntrada) {
		this.idDocumentoRegistrarEntrada = idDocumentoRegistrarEntrada;
	}
	
	@Transient
	public String getListDocumentoRegistrarEntrada() {
		return listDocumentoRegistrarEntrada;
	}

	public void setListDocumentoRegistrarEntrada(
			String listDocumentoRegistrarEntrada) {
		this.listDocumentoRegistrarEntrada = listDocumentoRegistrarEntrada;
	}

	@Transient
	public Money getValorTotalPagamento(){
		Money valor = new Money();
		for(Entregapagamento ep : this.getListadocumento()){
			if(ep.getValor() != null)
				valor = valor.add(ep.getValor());
		}
		
		return valor;
	}

	@Transient
	public Money getValorTotalFrete(){
		Money valor = new Money();
		if(this.getListaEntregamaterial() != null && !this.getListaEntregamaterial().isEmpty()){
			for(Entregamaterial em : this.getListaEntregamaterial()){
				if(em.getValorfrete() != null)
					valor = valor.add(em.getValorfrete());
			}
		}
		
		return valor;
	}

	@Transient
	public Arquivo getArquivoxmlnfe() {
		return arquivoxmlnfe;
	}
	public void setArquivoxmlnfe(Arquivo arquivoxmlnfe) {
		this.arquivoxmlnfe = arquivoxmlnfe;
	}
	
	@Transient
	public Aux_apuracaoimposto getAux_apuracaoimposto() {
		return aux_apuracaoimposto;
	}
	public void setAux_apuracaoimposto(Aux_apuracaoimposto auxApuracaoimposto) {
		aux_apuracaoimposto = auxApuracaoimposto;
	}
	
	@DisplayName("Valor ICMSST (+)")
	public Money getValoricmsst() {
		return valoricmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	
	@DisplayName("Valor FCPST (+)")
	public Money getValorfcpst() {
		return valorfcpst;
	}
	
	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}
	
	@DisplayName("Valor IPI (+)")
	public Money getValoripi() {
		return valoripi;
	}

	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	
	@Transient
	@DisplayName("Valor ICMSST (+)")
	public Money getValoricmsstTotaldocumento() {
		Money total = new Money();
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValoricmsst() != null && (em.getNaoconsideraricmsst() == null || !em.getNaoconsideraricmsst()))
					total = total.add(em.getValoricmsst());
			}
		}
		return total;
	}
	
	@Transient
	@DisplayName("Valor FCPST (+)")
	public Money getValorfcpstTotaldocumento() {
		Money total = new Money();
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValorfcpst() != null && (em.getNaoconsideraricmsst() == null || !em.getNaoconsideraricmsst()))
					total = total.add(em.getValorfcpst());
			}
		}
		return total;
	}
	
	@Transient
	@DisplayName("Valor ICMS Desonerado (-)")
	public Money getValoricmsdesoneradoTotaldocumento() {
		Money total = new Money();
		if(listaEntregamaterial != null && !listaEntregamaterial.isEmpty()){
			for(Entregamaterial em : listaEntregamaterial){
				if(em.getValorIcmsDesonerado() != null)
					total = total.add(em.getValorIcmsDesonerado());
			}
		}
		return total;
	}
	
	public void setValoricmsdesoneradoTotaldocumento(Money valoricmsdesoneradoTotaldocumento) {
		this.valoricmsdesoneradoTotaldocumento = valoricmsdesoneradoTotaldocumento;
	}
	
	public void setValorfcpstTotaldocumento(Money valorfcpstTotaldocumento) {
		this.valorfcpstTotaldocumento = valorfcpstTotaldocumento;
	}

	public void setValoricmsstTotaldocumento(Money valoricmsstTotaldocumento) {
		this.valoricmsstTotaldocumento = valoricmsstTotaldocumento;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Entregadocumento) {
			Entregadocumento entregadocumento = (Entregadocumento) obj;
			return entregadocumento.getCdentregadocumento().equals(this.getCdentregadocumento());
		}
		return super.equals(obj);
	}	
	
	public String subQueryFornecedorEmpresa() {
		return "select entregadocumentoSubQueryFornecedorEmpresa.cdentregadocumento " +
				"from Entregadocumento entregadocumentoSubQueryFornecedorEmpresa " +
				"left outer join entregadocumentoSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public Boolean getFromPedidoColeta() {
		return fromPedidoColeta;
	}
	
	public void setFromPedidoColeta(Boolean fromPedidoColeta) {
		this.fromPedidoColeta = fromPedidoColeta;
	}
	
	@Transient
	public Boolean getFromColeta() {
		return fromColeta;
	}
	
	public void setFromColeta(Boolean fromColeta) {
		this.fromColeta = fromColeta;
	}
	@Transient
	public String getWhereInPedido() {
		return whereInPedido;
	}
	
	public void setWhereInPedido(String whereInPedido) {
		this.whereInPedido = whereInPedido;
	}
	
	@Transient
	public String getWhereInColeta() {
		return whereInColeta;
	}
	
	public void setWhereInColeta(String whereInColeta) {
		this.whereInColeta = whereInColeta;
	}
	
	@Transient
	public String getDtentradaStr(){
		if (getDtentrada()!=null){
			return new SimpleDateFormat("dd/MM/yyyy").format(getDtentrada());
		}else {
			return null;
		}
	}
	
	@Transient
	public String getDtemissaoStr(){
		if (getDtemissao()!=null){
			return new SimpleDateFormat("dd/MM/yyyy").format(getDtemissao());
		}else {
			return null;
		}
	}

	@DisplayName("Subs�rie")
	@MaxLength(3)
	public String getSubserie() {
		return subserie;
	}
	
	@DisplayName("Emitente")
	public Entregadocumentoemitente getEmitente() {
		return emitente;
	}
	
	@DisplayName("Valor frete peso")
	public Money getValorfretepeso() {
		return valorfretepeso;
	}
	
	@DisplayName("Valor taxa coleta")
	public Money getValortaxacoleta() {
		return valortaxacoleta;
	}
	
	@DisplayName("Valor taxa entrega")
	public Money getValortaxaentrega() {
		return valortaxaentrega;
	}
	
	@DisplayName("Valor SEC/CAT")
	public Money getValorseccat() {
		return valorseccat;
	}
	
	@DisplayName("Valor ped�gio")
	public Money getValorpedagio() {
		return valorpedagio;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregadocumentofrete> getListaEntregadocumentofrete() {
		return listaEntregadocumentofrete;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregadocumentofreterateio> getListaEntregadocumentofreterateio() {
		return listaEntregadocumentofreterateio;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Entregadocumentoreferenciado> getListaEntregadocumentoreferenciado() {
		return listaEntregadocumentoreferenciado;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public List<EntregadocumentoColeta> getListaEntregadocumentoColeta() {
		return listaEntregadocumentoColeta;
	}
	public void setListaEntregadocumentoColeta(List<EntregadocumentoColeta> listaEntregadocumentoColeta) {
		this.listaEntregadocumentoColeta = listaEntregadocumentoColeta;
	}

	public void setSubserie(String subserie) {
		this.subserie = subserie;
	}

	public void setEmitente(Entregadocumentoemitente emitente) {
		this.emitente = emitente;
	}

	public void setValorfretepeso(Money valorfretepeso) {
		this.valorfretepeso = valorfretepeso;
	}

	public void setValortaxacoleta(Money valortaxacoleta) {
		this.valortaxacoleta = valortaxacoleta;
	}

	public void setValortaxaentrega(Money valortaxaentrega) {
		this.valortaxaentrega = valortaxaentrega;
	}

	public void setValorseccat(Money valorseccat) {
		this.valorseccat = valorseccat;
	}

	public void setValorpedagio(Money valorpedagio) {
		this.valorpedagio = valorpedagio;
	}

	public void setListaEntregadocumentofrete(Set<Entregadocumentofrete> listaEntregadocumentofrete) {
		this.listaEntregadocumentofrete = listaEntregadocumentofrete;
	}

	public void setListaEntregadocumentofreterateio(Set<Entregadocumentofreterateio> listaEntregadocumentofreterateio) {
		this.listaEntregadocumentofreterateio = listaEntregadocumentofreterateio;
	}
	
	public void setListaEntregadocumentoreferenciado(Set<Entregadocumentoreferenciado> listaEntregadocumentoreferenciado) {
		this.listaEntregadocumentoreferenciado = listaEntregadocumentoreferenciado;
	}
	
	@Transient
	public Money getValorTotalDocumentoCTRC() {
		return valorTotalDocumentoCTRC;
	}
	
	@Transient
	public Money getValorTotalRateioCTRC() {
		return valorTotalRateioCTRC;
	}
	
	@Transient
	public Money getValorRestanteCTRC() {
		return valorRestanteCTRC;
	}
	
	@Transient
	public Boolean getCalculoRateio() {
		return calculoRateio;
	}

	public void setValorTotalDocumentoCTRC(Money valorTotalDocumentoCTRC) {
		this.valorTotalDocumentoCTRC = valorTotalDocumentoCTRC;
	}

	public void setValorTotalRateioCTRC(Money valorTotalRateioCTRC) {
		this.valorTotalRateioCTRC = valorTotalRateioCTRC;
	}

	public void setValorRestanteCTRC(Money valorRestanteCTRC) {
		this.valorRestanteCTRC = valorRestanteCTRC;
	}

	public void setCalculoRateio(Boolean calculoRateio) {
		this.calculoRateio = calculoRateio;
	}
	
	@Transient
	public String getCdsentregadocumento() {
		return cdsentregadocumento;
	}
	
	public void setCdsentregadocumento(String cdsentregadocumento) {
		this.cdsentregadocumento = cdsentregadocumento;
	}
	
	@Transient
	public String getCdentregadocumentofreteRemover() {
		return cdentregadocumentofreteRemover;
	}
	public void setCdentregadocumentofreteRemover(String cdentregadocumentofreteRemover) {
		this.cdentregadocumentofreteRemover = cdentregadocumentofreteRemover;
	}

	
	@Transient
	public String getPosicaoListaForRateioParam() {
		return posicaoListaForRateioParam;
	}
	
	@Transient
	public String getPosicaoListaParam() {
		return posicaoListaParam;
	}

	@Transient
	public String getCdentregaParam() {
		return cdentregaParam;
	}

	public void setPosicaoListaForRateioParam(String posicaoListaForRateioParam) {
		this.posicaoListaForRateioParam = posicaoListaForRateioParam;
	}

	public void setPosicaoListaParam(String posicaoListaParam) {
		this.posicaoListaParam = posicaoListaParam;
	}

	public void setCdentregaParam(String cdentregaParam) {
		this.cdentregaParam = cdentregaParam;
	}
	
	@Transient
	public Set<Entregadocumentoreferenciado> getListaEntregadocumentoreferenciadoTrans() {
		return listaEntregadocumentoreferenciadoTrans;
	}
	public void setListaEntregadocumentoreferenciadoTrans(
			Set<Entregadocumentoreferenciado> listaEntregadocumentoreferenciadoTrans) {
		this.listaEntregadocumentoreferenciadoTrans = listaEntregadocumentoreferenciadoTrans;
	}

	@Transient
	@DisplayName("Valor Importa��o (+)")
	public Money getValorImportacao() {
		return valorImportacao;
	}
	public void setValorImportacao(Money valorImportacao) {
		this.valorImportacao = valorImportacao;
	}
	
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	@Transient
	public String getWhereInDevolucoes() {
		return whereInDevolucoes;
	}
	public void setWhereInDevolucoes(String whereInDevolucoes) {
		this.whereInDevolucoes = whereInDevolucoes;
	}
	
	@Transient
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	@Transient
	public Money getValorreceita() {
		return valorreceita;
	}
	@Transient
	public Money getValormercadoriareceita() {
		return getValorCalculadoMercadoria(true);
	}

	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	public void setValorreceita(Money valorreceita) {
		this.valorreceita = valorreceita;
	}
	public void setValormercadoriareceita(Money valormercadoriareceita) {
		this.valormercadoriareceita = valormercadoriareceita;
	}
	
	@Transient
	public String getHtmlChaveacesso() {
		return htmlChaveacesso;
	}
	
	public void setHtmlChaveacesso(String htmlChaveacesso) {
		this.htmlChaveacesso = htmlChaveacesso;
	}
	
	@Transient
	public Money getValortotalDocReferenciado(){
		Money total = new Money();
		if(SinedUtil.isListNotEmpty(getListaEntregadocumentoreferenciado())){
			for(Entregadocumentoreferenciado edr : getListaEntregadocumentoreferenciado()){
				if(edr.getValor() != null){
					total = total.add(edr.getValor());
				}
			}
		}
		return total;
	}
	
	@Transient
	public Money getValortotalFreteDocCarga(){
		Money total = new Money();
		if(SinedUtil.isListNotEmpty(getListaEntregadocumentofrete())){
			for(Entregadocumentofrete edf : getListaEntregadocumentofrete()){
				if(edf.getEntregadocumentovinculo() != null && edf.getEntregadocumentovinculo().getValorfrete() != null){
					total = total.add(edf.getEntregadocumentovinculo().getValorfrete());
				}
			}
		}
		return total;
	}
	
	@Transient
	public Money getValortotalOutrasdespesasDocCarga(){
		Money total = new Money();
		if(SinedUtil.isListNotEmpty(getListaEntregadocumentofrete())){
			for(Entregadocumentofrete edf : getListaEntregadocumentofrete()){
				if(edf.getEntregadocumentovinculo() != null && edf.getEntregadocumentovinculo().getValoroutrasdespesas() != null){
					total = total.add(edf.getEntregadocumentovinculo().getValoroutrasdespesas());
				}
			}
		}
		return total;
	}

	@DisplayName("Ajuste Fiscal")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregadocumento")
	public Set<Ajustefiscal> getListaAjustefiscal() {
		return listaAjustefiscal;
	}

	public void setListaAjustefiscal(Set<Ajustefiscal> listaAjustefiscal) {
		this.listaAjustefiscal = listaAjustefiscal;
	}

	@Transient
	public List<Pedidovendamaterial> getListaPedidovendamaterial() {
		return listaPedidovendamaterial;
	}

	public void setListaPedidovendamaterial(List<Pedidovendamaterial> listaPedidovendamaterial) {
		this.listaPedidovendamaterial = listaPedidovendamaterial;
	}
	@Transient
	public Date getDataForGeracaoContabil() {
		return dtentrada != null? dtentrada: dtemissao;
	}
	@Transient
	public Money getValortotalbciss() {
		if(valortotalbciss == null)
			criaTotaisimposto();
		return valortotalbciss;
	}
	
	@Transient
	public boolean existeItemSincronizarWms(){
		if(Hibernate.isInitialized(getListaEntregamaterial()) && SinedUtil.isListNotEmpty(getListaEntregamaterial())){
			for(Entregamaterial entregamaterial : getListaEntregamaterial()){
				if(entregamaterial.getMaterial() != null && entregamaterial.getMaterial().getMaterialgrupo() != null &&
						entregamaterial.getMaterial().getMaterialgrupo().getSincronizarwms() != null &&
						entregamaterial.getMaterial().getMaterialgrupo().getSincronizarwms()){
					return true;
				}
			}
		}
		return false;
	}
	
	@DisplayName("Diferen�a das parcelas")
	public Money getDiferencaduplicata() {
		return diferencaduplicata;
	}
	
	public void setDiferencaduplicata(Money diferencaduplicata) {
		this.diferencaduplicata = diferencaduplicata;
	}
	
	@DisplayName("Apropria��o")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="entregaDocumento")
	public List<EntregaDocumentoApropriacao> getListaApropriacao() {
		return listaApropriacao;
	}
	
	public void setListaApropriacao(
			List<EntregaDocumentoApropriacao> listaApropriacao) {
		this.listaApropriacao = listaApropriacao;
	}
	
	@Transient
	public boolean getExisteDiferencaduplicata(){
		return getDiferencaduplicata() != null && getDiferencaduplicata().compareTo(new Money()) != 0; 
	}

	@Transient
	public Boolean getExisteNotaDevolucao() {
		return existeNotaDevolucao;
	}

	public void setExisteNotaDevolucao(Boolean existeNotaDevolucao) {
		this.existeNotaDevolucao = existeNotaDevolucao;
	}
	
	@Transient
	public Money getValorImportacaoCalculado() {
		Money valor = new Money();
		
		if (SinedUtil.isListNotEmpty(listaEntregamaterial)){
			for (Entregamaterial entregamaterial: listaEntregamaterial){
				valor = valor.add(entregamaterial.getValorImportacaoCalculado());
			}
		}
		return valor;
	}

	@Transient
	public String getIsFaturar() {
		return isFaturar;
	}

	public void setIsFaturar(String isFaturar) {
		this.isFaturar = isFaturar;
	}
	
	@Transient
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@Transient
	public Boolean getFromRecebimento() {
		return fromRecebimento;
	}
	public void setFromRecebimento(Boolean fromRecebimento) {
		this.fromRecebimento = fromRecebimento;
	}
	
	@Transient
	public String getIdentificadortelaRecebimento() {
		return identificadortelaRecebimento;
	}
	public void setIdentificadortelaRecebimento(String identificadortelaRecebimento) {
		this.identificadortelaRecebimento = identificadortelaRecebimento;
	}
	
	@Transient
	public String getCdempresaentrega() {
		return cdempresaentrega;
	}
	public void setCdempresaentrega(String cdempresaentrega) {
		this.cdempresaentrega = cdempresaentrega;
	}

	@Transient
	public List<Material> getListaMaterialitemgrade() {
		return listaMaterialitemgrade;
	}

	public void setListaMaterialitemgrade(List<Material> listaMaterialitemgrade) {
		this.listaMaterialitemgrade = listaMaterialitemgrade;
	}
}