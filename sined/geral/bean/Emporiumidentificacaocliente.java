package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Identificacaocliente;


@Entity
@SequenceGenerator(name = "sq_emporiumidentificacaocliente", sequenceName = "sq_emporiumidentificacaocliente")
public class Emporiumidentificacaocliente {

	protected Integer cdemporiumidentificacaocliente;
	protected Identificacaocliente identificacaocliente;
	protected Integer chaveemporium;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumidentificacaocliente")
	public Integer getCdemporiumidentificacaocliente() {
		return cdemporiumidentificacaocliente;
	}

	public Identificacaocliente getIdentificacaocliente() {
		return identificacaocliente;
	}

	public Integer getChaveemporium() {
		return chaveemporium;
	}

	public void setCdemporiumidentificacaocliente(Integer cdemporiumidentificacaocliente) {
		this.cdemporiumidentificacaocliente = cdemporiumidentificacaocliente;
	}

	public void setIdentificacaocliente(Identificacaocliente identificacaocliente) {
		this.identificacaocliente = identificacaocliente;
	}

	public void setChaveemporium(Integer chaveemporium) {
		this.chaveemporium = chaveemporium;
	}
	
	
}
