package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_questionario", sequenceName = "sq_questionario")
@DisplayName("Question�rio")
public class Questionario implements Log{
	
	protected Integer cdquestionario;
	protected String nome;
	protected Integer mediapontuacao;
	protected Tipopessoaquestionario tipopessoaquestionario;
	protected Boolean ativo;
	protected Boolean avaliarrecebimento;
	protected Boolean obrigaavaliacaoreceber;
	protected List<Questionarioquestao> listaquestionarioquestao;
	protected Arquivo arquivo;
	protected Integer quantidadeNotasMedia;
	protected List<Questionariogrupo> listaQuestionariogrupo;
	protected Boolean definirgrupo;
	
//	CAMPOS DE LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_questionario")
	public Integer getCdquestionario() {
		return cdquestionario;
	}
	public void setCdquestionario(Integer cdquestionario) {
		this.cdquestionario = cdquestionario;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Required
	@DisplayName("Tipo de pessoa")
	public Tipopessoaquestionario getTipopessoaquestionario() {
		return tipopessoaquestionario;
	}
	public void setTipopessoaquestionario(Tipopessoaquestionario tipopessoaquestionario) {
		this.tipopessoaquestionario = tipopessoaquestionario;
	}
	
	@Required
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Required
	@DisplayName("M�dia Aprova��o")
	public Integer getMediapontuacao() {
		return mediapontuacao;
	}
	public void setMediapontuacao(Integer mediapontuacao) {
		this.mediapontuacao = mediapontuacao;
	}
	
	@DisplayName("Quest�es")
	@OneToMany(mappedBy="questionario")
	public List<Questionarioquestao> getListaquestionarioquestao() {
		return listaquestionarioquestao;
	}
	
	public void setListaquestionarioquestao(
			List<Questionarioquestao> listaquestionarioquestao) {
		this.listaquestionarioquestao = listaquestionarioquestao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("M�dia da nota das �ltimas avalia��es")
	public Integer getQuantidadeNotasMedia() {
		return quantidadeNotasMedia;
	}
	
	@DisplayName("Obrigar avalia��o no receber")
	public Boolean getObrigaavaliacaoreceber() {
		return obrigaavaliacaoreceber;
	}

	@DisplayName("Avaliar no Recebimento")
	public Boolean getAvaliarrecebimento() {
		return avaliarrecebimento;
	}
	
	@DisplayName("Question�rio Grupo")
	@OneToMany(mappedBy="questionario")
	public List<Questionariogrupo> getListaQuestionariogrupo() {
		return listaQuestionariogrupo;
	}
	@DisplayName("Definir Grupo")
	public Boolean getDefinirgrupo() {
		return definirgrupo;
	}
	
	public void setObrigaavaliacaoreceber(Boolean obrigaavaliacaoreceber) {
		this.obrigaavaliacaoreceber = obrigaavaliacaoreceber;
	}
	
	public void setQuantidadeNotasMedia(Integer quantidadeNotasMedia) {
		this.quantidadeNotasMedia = quantidadeNotasMedia;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Texto Complementar")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setAvaliarrecebimento(Boolean avaliarrecebimento) {
		this.avaliarrecebimento = avaliarrecebimento;
	}
	
	public void setListaQuestionariogrupo(List<Questionariogrupo> listaQuestionariogrupo) {
		this.listaQuestionariogrupo = listaQuestionariogrupo;
	}
	
	public void setDefinirgrupo(Boolean definirgrupo) {
		this.definirgrupo = definirgrupo;
	}
}
