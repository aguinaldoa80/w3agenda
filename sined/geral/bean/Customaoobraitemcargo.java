package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_customaoobraitemcargo", sequenceName = "sq_customaoobraitemcargo")
public class Customaoobraitemcargo {

	protected Integer cdcustomaoobraitemcargo;
	protected Customaoobraitem customaoobraitem;
	protected Cargo cargo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_customaoobraitemcargo")
	public Integer getCdcustomaoobraitemcargo() {
		return cdcustomaoobraitemcargo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcustomaoobraitem")
	public Customaoobraitem getCustomaoobraitem() {
		return customaoobraitem;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}

	public void setCdcustomaoobraitemcargo(Integer cdcustomaoobraitemcargo) {
		this.cdcustomaoobraitemcargo = cdcustomaoobraitemcargo;
	}

	public void setCustomaoobraitem(Customaoobraitem customaoobraitem) {
		this.customaoobraitem = customaoobraitem;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}
