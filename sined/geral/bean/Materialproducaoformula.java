package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialproducaoformula", sequenceName = "sq_materialproducaoformula")
public class Materialproducaoformula {

	protected Integer cdmaterialproducaoformula;
	protected Materialproducao materialproducao;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialproducaoformula")
	public Integer getCdmaterialproducaoformula() {
		return cdmaterialproducaoformula;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialproducao")
	public Materialproducao getMaterialproducao() {
		return materialproducao;
	}

	@Required
	public Integer getOrdem() {
		return ordem;
	}

	@Required
	@MaxLength(5)
	public String getIdentificador() {
		return identificador;
	}

	@Required
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}

	public void setCdmaterialproducaoformula(Integer cdmaterialproducaoformula) {
		this.cdmaterialproducaoformula = cdmaterialproducaoformula;
	}

	public void setMaterialproducao(Materialproducao materialproducao) {
		this.materialproducao = materialproducao;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
	
}
