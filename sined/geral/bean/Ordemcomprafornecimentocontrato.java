package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_ordemcomprafornecimentocontrato", sequenceName = "sq_ordemcomprafornecimentocontrato")
public class Ordemcomprafornecimentocontrato {
	
	private Integer cdordemcomprafornecimentocontrato;
	private Ordemcompra ordemcompra;
	private Fornecimentocontrato fornecimentocontrato;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcomprafornecimentocontrato")
	public Integer getCdordemcomprafornecimentocontrato() {
		return cdordemcomprafornecimentocontrato;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentocontrato")
	public Fornecimentocontrato getFornecimentocontrato() {
		return fornecimentocontrato;
	}
	
	public void setCdordemcomprafornecimentocontrato(Integer cdordemcomprafornecimentocontrato) {
		this.cdordemcomprafornecimentocontrato = cdordemcomprafornecimentocontrato;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	public void setFornecimentocontrato(Fornecimentocontrato fornecimentocontrato) {
		this.fornecimentocontrato = fornecimentocontrato;
	}
}