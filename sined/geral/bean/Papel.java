package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.authorization.Role;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_papel", sequenceName = "sq_papel")
@DisplayName("N�vel")
public class Papel implements Role,Log {

	protected Integer cdpapel;
	protected String nome;
	protected Boolean administrador;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Acaopapel> listaAcaopapel = new ListSet<Acaopapel>(Acaopapel.class);
	protected List<Permissao> listaPermissoes = new ListSet<Permissao>(Permissao.class);
	protected Set<Dashboardpapel> listaDashboardpapel = new ListSet<Dashboardpapel>(Dashboardpapel.class);
	protected Set<Dashboardespecificopapel> listaDashboardespecificopapel = new ListSet<Dashboardespecificopapel>(Dashboardespecificopapel.class);
	
	//transient
	protected String atualiza;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_papel")
	public Integer getCdpapel() {
		return cdpapel;
	}
	public void setCdpapel(Integer id) {
		this.cdpapel = id;
	}

	
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Boolean getAdministrador() {
		return administrador;
	}
	
	@MaxLength(1000)
	@DisplayName("Observa��o")
	public String getDescricao() {
		return descricao;
	}
	
	@OneToMany(mappedBy="papel")
	public List<Acaopapel> getListaAcaopapel() {
		return listaAcaopapel;
	}
	
	@OneToMany(mappedBy="papel")
	public List<Permissao> getListaPermissoes() {
		return listaPermissoes;
	}
	
	@DisplayName("Dashboards geral")
	@OneToMany(mappedBy="papel")
	public Set<Dashboardpapel> getListaDashboardpapel() {
		return listaDashboardpapel;
	}
	
	@DisplayName("Dashboards espec�ficos")
	@OneToMany(mappedBy="papel")
	public Set<Dashboardespecificopapel> getListaDashboardespecificopapel() {
		return listaDashboardespecificopapel;
	}
	
	public void setListaDashboardespecificopapel(
			Set<Dashboardespecificopapel> listaDashboardespecificopapel) {
		this.listaDashboardespecificopapel = listaDashboardespecificopapel;
	}
	
	public void setListaDashboardpapel(Set<Dashboardpapel> listaDashboardpapel) {
		this.listaDashboardpapel = listaDashboardpapel;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setListaAcaopapel(List<Acaopapel> listaAcaopapel) {
		this.listaAcaopapel = listaAcaopapel;
	}
	
	public void setListaPermissoes(List<Permissao> listaPermissoes) {
		this.listaPermissoes = listaPermissoes;
	}
	
	/* API */
	@Transient
	public String getDescription() {
		return this.descricao;
	}
	@Transient
	public String getName() {
		return this.nome;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getAtualiza() {
		return atualiza;
	}
	
	public void setAtualiza(String atualiza) {
		this.atualiza = atualiza;
	}
	@Transient
	public Boolean isAdmin() {
		return getAdministrador();
	}

}
