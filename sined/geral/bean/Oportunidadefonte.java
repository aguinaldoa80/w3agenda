package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_oportunidadefonte", sequenceName = "sq_oportunidadefonte")
@DisplayName("Fonte")
public class Oportunidadefonte implements Log{
	
	protected Integer cdoportunidadefonte;
	protected String nome;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidadefonte")	
	public Integer getCdoportunidadefonte() {
		return cdoportunidadefonte;
	}
	
	@Required
	@MaxLength(30)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdoportunidadefonte(Integer cdoportunidadefonte) {
		this.cdoportunidadefonte = cdoportunidadefonte;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {}
	public void setDtaltera(Timestamp dtaltera) {}
}
