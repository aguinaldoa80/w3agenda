package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_usuarioempresa", sequenceName = "sq_usuarioempresa")
public class Usuarioempresa implements Log {

	protected Integer cdusuarioempresa;
	protected Usuario usuario;
	protected Empresa empresa;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_usuarioempresa")
	public Integer getCdusuarioempresa() {
		return cdusuarioempresa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@Required
	public Usuario getUsuario() {
		return usuario;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}


	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCdusuarioempresa(Integer cdusuarioempresa) {
		this.cdusuarioempresa = cdusuarioempresa;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
