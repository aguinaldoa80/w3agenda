package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Processojuridicoclientetipo {
	public static Processojuridicoclientetipo REU = new Processojuridicoclientetipo(1, "R�u");
	public static Processojuridicoclientetipo AUTOR = new Processojuridicoclientetipo(2, "Autor");
	
	protected Integer cdprocessojuridicoclientetipo;
	protected String nome;
	
	public Processojuridicoclientetipo(){
		
	}
	public Processojuridicoclientetipo(Integer cdprocessojuridicoclientetipo, String nome){
		this.cdprocessojuridicoclientetipo = cdprocessojuridicoclientetipo;
		this.nome = nome;		
	}
	@Id
	public Integer getCdprocessojuridicoclientetipo() {
		return cdprocessojuridicoclientetipo;
	}
	@DescriptionProperty
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	
	public void setCdprocessojuridicoclientetipo(Integer cdprocessojuridicoclientetipo) {
		this.cdprocessojuridicoclientetipo = cdprocessojuridicoclientetipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Processojuridicoclientetipo){
			return this.getCdprocessojuridicoclientetipo() != null && this.getCdprocessojuridicoclientetipo().equals(((Processojuridicoclientetipo)obj).getCdprocessojuridicoclientetipo());
		} else
			return false;
	}
	
}
