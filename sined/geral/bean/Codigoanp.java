package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Codigoanp {

	protected Integer cdcodigoanp;
	protected String codigo;
	protected String familia;
	protected String grupo;
	protected String subgrupo;
	protected String subsubgrupo;
	protected String produto;
	protected String unidadegrandeza;
	protected String unidademedidasimp;
	
	@Id
	public Integer getCdcodigoanp() {
		return cdcodigoanp;
	}
	@DescriptionProperty
	public String getCodigo() {
		return codigo;
	}
	public String getFamilia() {
		return familia;
	}
	public String getGrupo() {
		return grupo;
	}
	public String getSubgrupo() {
		return subgrupo;
	}
	public String getSubsubgrupo() {
		return subsubgrupo;
	}
	public String getProduto() {
		return produto;
	}
	public String getUnidadegrandeza() {
		return unidadegrandeza;
	}
	public String getUnidademedidasimp() {
		return unidademedidasimp;
	}
	public void setFamilia(String familia) {
		this.familia = familia;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public void setSubgrupo(String subgrupo) {
		this.subgrupo = subgrupo;
	}
	public void setSubsubgrupo(String subsubgrupo) {
		this.subsubgrupo = subsubgrupo;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public void setUnidadegrandeza(String unidadegrandeza) {
		this.unidadegrandeza = unidadegrandeza;
	}
	public void setUnidademedidasimp(String unidademedidasimp) {
		this.unidademedidasimp = unidademedidasimp;
	}
	public void setCdcodigoanp(Integer cdcodigoanp) {
		this.cdcodigoanp = cdcodigoanp;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
