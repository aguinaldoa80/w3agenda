package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Movimentação de estoque origem")
@SequenceGenerator(name = "sq_movimentacaoestoqueorigem", sequenceName = "sq_movimentacaoestoqueorigem")
public class Movimentacaoestoqueorigem implements Log{

	protected Integer cdmovimentacaoestoqueorigem;
	protected Requisicaomaterial requisicaomaterial;
	protected Entrega entrega;
	protected Entregamaterial entregamaterial;
	protected Venda venda;
	protected Usuario usuario;
	protected Romaneio romaneio;
	protected Boolean romaneiomanual;
	protected Expedicao expedicao;
	protected Notafiscalproduto notafiscalproduto;
	protected Requisicao requisicao;
	protected Boolean consumo;
	protected Expedicaoproducao expedicaoproducao;
	protected Producaoordem producaoordem;
	protected Producaoordemmaterial producaoOrdemMaterial;
	protected Producaoagenda producaoagenda;
	protected Producaoagendamaterial producaoAgendaMaterial;
	protected Pedidovenda pedidovenda;
	protected Ordemservicoveterinaria ordemservicoveterinaria;
	protected Coleta coleta;
	protected Colaborador colaborador;
	protected Boolean registrarproducao;
	protected Boolean estornoproducao;
	protected Boolean ajusteautomaticowms;
	protected Boolean ajustemanualwms;
	protected Veiculodespesa veiculodespesa;
	protected Inventario inventario;
	protected Notafiscalprodutoitem notaFiscalProdutoItem;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	protected Boolean movimentacaoAvulsa;
	
	public Movimentacaoestoqueorigem(){
	}

	public Movimentacaoestoqueorigem(Romaneio romaneio){
		this.romaneio = romaneio;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_movimentacaoestoqueorigem")
	@DisplayName("Movimentação estoque origem")
	public Integer getCdmovimentacaoestoqueorigem() {
		return cdmovimentacaoestoqueorigem;
	}
	@DisplayName("Requisição material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial")
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneio")
	public Romaneio getRomaneio() {
		return romaneio;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Boolean getRomaneiomanual() {
		return romaneiomanual;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinventario")
	public Inventario getInventario() {
		return inventario;
	}
	
	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
	
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
	public void setRomaneiomanual(Boolean romaneiomanual) {
		this.romaneiomanual = romaneiomanual;
	}
	public void setCdmovimentacaoestoqueorigem(
			Integer cdmovimentacaoestoqueorigem) {
		this.cdmovimentacaoestoqueorigem = cdmovimentacaoestoqueorigem;
	}
	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setRomaneio(Romaneio romaneio) {
		this.romaneio = romaneio;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}

	public Boolean getConsumo() {
		return consumo;
	}
	public void setConsumo(Boolean consumo) {
		this.consumo = consumo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicaoproducao")
	public Expedicaoproducao getExpedicaoproducao() {
		return expedicaoproducao;
	}

	public void setExpedicaoproducao(Expedicaoproducao expedicaoproducao) {
		this.expedicaoproducao = expedicaoproducao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordem")
	public Producaoordem getProducaoordem() {
		return producaoordem;
	}
	
	public void setProducaoordem(Producaoordem producaoordem) {
		this.producaoordem = producaoordem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordemmaterial")
	public Producaoordemmaterial getProducaoOrdemMaterial() {
		return producaoOrdemMaterial;
	}
	
	public void setProducaoOrdemMaterial(
			Producaoordemmaterial producaoOrdemMaterial) {
		this.producaoOrdemMaterial = producaoOrdemMaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagenda")
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagendamaterial")
	public Producaoagendamaterial getProducaoAgendaMaterial() {
		return producaoAgendaMaterial;
	}
	
	public void setProducaoAgendaMaterial(
			Producaoagendamaterial producaoAgendaMaterial) {
		this.producaoAgendaMaterial = producaoAgendaMaterial;
	}
	
	public void setOrdemservicoveterinaria(
			Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Boolean getRegistrarproducao() {
		return registrarproducao;
	}

	public void setRegistrarproducao(Boolean registrarproducao) {
		this.registrarproducao = registrarproducao;
	}

	public Boolean getEstornoproducao() {
		return estornoproducao;
	}

	public void setEstornoproducao(Boolean estornoproducao) {
		this.estornoproducao = estornoproducao;
	}
	
	public Boolean getAjusteautomaticowms() {
		return ajusteautomaticowms;
	}
	public void setAjusteautomaticowms(Boolean ajusteautomaticowms) {
		this.ajusteautomaticowms = ajusteautomaticowms;
	}
	
	public Boolean getAjustemanualwms() {
		return ajustemanualwms;
	}
	public void setAjustemanualwms(Boolean ajustemanualwms) {
		this.ajustemanualwms = ajustemanualwms;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculodespesa")
	public Veiculodespesa getVeiculodespesa() {
		return veiculodespesa;
	}
	public void setVeiculodespesa(Veiculodespesa veiculodespesa) {
		this.veiculodespesa = veiculodespesa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotaFiscalProdutoItem() {
		return notaFiscalProdutoItem;
	}

	public void setNotaFiscalProdutoItem(Notafiscalprodutoitem notaFiscalProdutoItem) {
		this.notaFiscalProdutoItem = notaFiscalProdutoItem;
	}
	
	@Transient
	public Boolean getMovimentacaoAvulsa(){
		return movimentacaoAvulsa;
	}
	public void setMovimentacaoAvulsa(Boolean movimentacaoAvulsa) {
		this.movimentacaoAvulsa = movimentacaoAvulsa;
	}
}