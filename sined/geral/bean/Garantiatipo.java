package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_garantiatipo", sequenceName="sq_garantiatipo")
@DisplayName("Tipo de garantia")
public class Garantiatipo implements Log{

	protected Integer cdgarantiatipo;
	protected String descricao;
	protected Boolean ativo;
	protected Boolean geragarantia;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Garantiatipopercentual> listaGarantiatipopercentual;
	
	public Garantiatipo(){
	}
	
	public Garantiatipo(Integer cdgarantiatipo){
		this.cdgarantiatipo = cdgarantiatipo;
	}
	
	@Id
	@GeneratedValue(generator="sq_garantiatipo", strategy=GenerationType.AUTO)
	@DisplayName("Id")
	public Integer getCdgarantiatipo() {
		return cdgarantiatipo;
	}
	public void setCdgarantiatipo(Integer cdgarantiatipo) {
		this.cdgarantiatipo = cdgarantiatipo;
	}

	@Required
	@DisplayName("Descri��o")
	@DescriptionProperty
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Required
	@DisplayName("Gera garantia?")
	public Boolean getGeragarantia() {
		return geragarantia;
	}
	public void setGeragarantia(Boolean geragarantia) {
		this.geragarantia = geragarantia;
	}
	
	@OneToMany(mappedBy="garantiatipo")
	@DisplayName("% de Garantia")
	public List<Garantiatipopercentual> getListaGarantiatipopercentual() {
		return listaGarantiatipopercentual;
	}
	public void setListaGarantiatipopercentual(
			List<Garantiatipopercentual> listaGarantiatipopercentual) {
		this.listaGarantiatipopercentual = listaGarantiatipopercentual;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdgarantiatipo == null) ? 0 : cdgarantiatipo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Garantiatipo other = (Garantiatipo) obj;
		if (cdgarantiatipo == null) {
			if (other.cdgarantiatipo != null)
				return false;
		} else if (!cdgarantiatipo.equals(other.cdgarantiatipo))
			return false;
		return true;
	}
}
