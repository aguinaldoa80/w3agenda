package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.PainelInteracaoVendaInt;
import br.com.linkcom.sined.util.Log;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@DisplayName("Conta")
@SequenceGenerator(name = "sq_contacrm", sequenceName = "sq_contacrm")
public class Contacrm implements Log, PainelInteracaoVendaInt {
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdcontacrm;
	protected String nome;
	protected Tipopessoa tipo;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String website;
	
	protected Lead lead;
	protected Tiporesponsavel tiporesponsavel;
	protected Fornecedor agencia;
	protected Colaborador responsavel;
	protected Concorrente concorrente;
	
	protected List<Contacrmcontato> listcontacrmcontato;
	protected List<Contacrmhistorico> listcontacrmhistorico;
	protected List<Contacrmsegmento> listcontacrmsegmento;
	protected List<Oportunidade> listoportunidade;
	
//	Transients
	protected String observacao;
	protected String listaEmails;
	protected String strtipopessoa;
	protected String emailsreport;
	protected String listacontatos;
	protected Boolean isLead;
	protected String listaFones;
	protected String contatos;	
	protected String oportunidadenome;
	protected Date oportunidadedtinicio;
	protected Colaborador oportunidaderesponsavel;
	protected Oportunidadesituacao oportunidadesituacao;
	protected String ultimaInteracaoVenda;
	protected String observacaoGerarconta;
	protected String observacaoConverterlead;
	protected String nomeEscape;	
	protected Boolean converterlead;
	protected String identificadortela;
	protected String contatoAndTelefone;
	protected Boolean permitidoEdicao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrm")
	public Integer getCdcontacrm() {
		return cdcontacrm;
	}

	@Required	
	@MaxLength(150)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipo() {
		return tipo;
	}
	
	@MaxLength(14)
	public Cpf getCpf() {
		return cpf;
	}

	@MaxLength(21)
	public Cnpj getCnpj() {
		return cnpj;
	}

	@MaxLength(80)
	@DisplayName("WebSite")
	public String getWebsite() {
		return website;
	}

	@DisplayName("Lead")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlead")
	public Lead getLead() {
		return lead;
	}

	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	@DisplayName("Contatos")
	@OneToMany(mappedBy="contacrm")
	public List<Contacrmcontato> getListcontacrmcontato() {
		return listcontacrmcontato;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="contacrm")
	public List<Contacrmhistorico> getListcontacrmhistorico() {
		return listcontacrmhistorico;
	}
	
	@DisplayName("Segmento")
	@OneToMany(mappedBy="contacrm")
	public List<Contacrmsegmento> getListcontacrmsegmento() {
		return listcontacrmsegmento;
	}
	
	@DisplayName("Oportunidade")
	@OneToMany(mappedBy="contacrm")
	public List<Oportunidade> getListoportunidade() {
		return listoportunidade;
	}
	
	
	@Required
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}
	
	@ManyToOne
	@JoinColumn(name="cdagencia")
	@DisplayName("Ag�ncia")
	public Fornecedor getAgencia() {
		return agencia;
	}

	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	
	public void setCdcontacrm(Integer cdcontacrm) {
		this.cdcontacrm = cdcontacrm;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipo(Tipopessoa tipo) {
		this.tipo = tipo;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	
	public void setListcontacrmcontato(List<Contacrmcontato> listcontacrmcontato) {
		this.listcontacrmcontato = listcontacrmcontato;
	}
	
	public void setListcontacrmhistorico(
			List<Contacrmhistorico> listcontacrmhistorico) {
		this.listcontacrmhistorico = listcontacrmhistorico;
	}
	
	public void setListcontacrmsegmento(
			List<Contacrmsegmento> listcontacrmsegmento) {
		this.listcontacrmsegmento = listcontacrmsegmento;
	}
	
	public void setListoportunidade(List<Oportunidade> listoportunidade) {
		this.listoportunidade = listoportunidade;
	}
	
	
	
	
	@Transient
	@DisplayName("E-mails")
	public String getListaEmails() {
		return listaEmails;
	}
	@Transient
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@Transient
	@DisplayName("Tipo pessoa")
	public String getStrtipopessoa() {
		return strtipopessoa;
	}
	@Transient
	@DisplayName("E-mails")
	public String getEmailsreport() {
		return emailsreport;
	}
	@Transient
	@DisplayName("Contatos")
	public String getListacontatos() {
		return listacontatos;
	}
	
//	Atributo para dizer se est� conta ser� criada por um Lead
	@Transient
	public Boolean getIsLead() {
		return isLead;
	}
	
	@Transient
	@DisplayName("Telefones")
	public String getListaFones() {
		return listaFones;
	}
	
	
	public void setListaEmails(String listaEmails) {
		this.listaEmails = listaEmails;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setStrtipopessoa(String strtipopessoa) {
		this.strtipopessoa = strtipopessoa;
	}
	public void setListacontatos(String listacontatos) {
		this.listacontatos = listacontatos;
	}
	public void setEmailsreport(String emailsreport) {
		this.emailsreport = emailsreport;
	}
	public void setIsLead(Boolean isLead) {
		this.isLead = isLead;
	}
	public void setListaFones(String listaFones) {
		this.listaFones = listaFones;
	}
	
	
	@Transient
	public String getContatos() {
		if(this.listcontacrmcontato != null && !this.listcontacrmcontato.isEmpty()){
			StringBuilder sb = new StringBuilder("");
			for (Contacrmcontato contato : this.listcontacrmcontato) 
				sb.append(contato.getNome()).append(", ");
			return sb.substring(0, sb.length()-2).toString();
		}
		return contatos;
	}
	public void setContatos(String contatos) {
		this.contatos = contatos;
	}
	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getNomeEscape() {
		return nomeEscape;
	}
	
	public void setNomeEscape(String nomeEscape) {
		this.nomeEscape = nomeEscape;
	}

	@Transient
	public String getOportunidadenome() {
		return oportunidadenome;
	}	
	public void setOportunidadenome(String oportunidadenome) {
		this.oportunidadenome = oportunidadenome;
	}
	@Transient
	public Date getOportunidadedtinicio() {
		return oportunidadedtinicio;
	}
	@Transient
	public Colaborador getOportunidaderesponsavel() {
		return oportunidaderesponsavel;
	}
	@Transient
	public Oportunidadesituacao getOportunidadesituacao() {
		return oportunidadesituacao;
	}

	public void setOportunidadedtinicio(Date oportunidadedtinicio) {
		this.oportunidadedtinicio = oportunidadedtinicio;
	}

	public void setOportunidaderesponsavel(Colaborador oportunidaderesponsavel) {
		this.oportunidaderesponsavel = oportunidaderesponsavel;
	}

	public void setOportunidadesituacao(Oportunidadesituacao oportunidadesituacao) {
		this.oportunidadesituacao = oportunidadesituacao;
	}

	@Transient
	public String getUltimaInteracaoVenda() {
		StringBuilder historico = new StringBuilder("");
		SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		if(this.getListcontacrmhistorico() != null && !this.getListcontacrmhistorico().isEmpty()){
			Contacrmhistorico contacrmhistorico = this.getListcontacrmhistorico().get(this.getListcontacrmhistorico().size() - 1);
			if(contacrmhistorico.getDtaltera() != null){
				historico.append(dataFormatada.format(contacrmhistorico.getDtaltera()));
			}
			historico.append(contacrmhistorico.getUsuarioaltera() != null ? " / " + contacrmhistorico.getUsuarioaltera() : "");
			historico.append(contacrmhistorico.getObservacao() != null ? " / " + contacrmhistorico.getObservacao() : "");
			
		}
		return historico.toString();
	}

	public void setUltimaInteracaoVenda(String ultimaInteracaoVenda) {
		this.ultimaInteracaoVenda = ultimaInteracaoVenda;
	}

	@Transient
	public String getObservacaoGerarconta() {
		return observacaoGerarconta;
	}
	public void setObservacaoGerarconta(String observacaoGerarconta) {
		this.observacaoGerarconta = observacaoGerarconta;
	}
	@Transient
	public String getObservacaoConverterlead() {
		return observacaoConverterlead;
	}
	public void setObservacaoConverterlead(String observacaoConverterlead) {
		this.observacaoConverterlead = observacaoConverterlead;
	}

	@Transient
	public Boolean getConverterlead() {
		return converterlead;
	}

	public void setConverterlead(Boolean converterlead) {
		this.converterlead = converterlead;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconcorrente")
	public Concorrente getConcorrente() {
		return concorrente;
	}

	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}

	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	@Transient
	public String getContatoAndTelefone() {
		contatoAndTelefone = "";
		for(Contacrmcontato contatocrm: this.getListcontacrmcontato()){
			String telefones = contatocrm.getTelefones();
			if(contatocrm.getNome()!=null && telefones!=null){
				contatoAndTelefone = contatocrm.getNome()+" - "+telefones;
				break;
			}
		}
		
		//O primeiro contato encontrado com n�mero de telefone preenchido ser� retornado.
		//Caso nenhum possua telefone preenchido, retorna o primeiro contato da lista.
		if(contatoAndTelefone == "" &&
			this.getListcontacrmcontato() != null && this.getListcontacrmcontato().size()>0){
			contatoAndTelefone = this.getListcontacrmcontato().get(0).getNome();
		}
		return contatoAndTelefone;
	}
	public void setContatoAndTelefone(String contatoAndTelefone) {
		this.contatoAndTelefone = contatoAndTelefone;
	}
	
	@Transient
	public Boolean getPermitidoEdicao() {
		return permitidoEdicao;
	}
	
	public void setPermitidoEdicao(Boolean permitidoEdicao) {
		this.permitidoEdicao = permitidoEdicao;
	}
	
	@Transient
	public String getIdentificadorNome(){
		return (this.identificadortela != null ? this.identificadortela+" - " : "")+this.nome;
	}
}
