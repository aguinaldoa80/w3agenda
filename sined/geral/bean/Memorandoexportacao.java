package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@SequenceGenerator(name="sq_memorandoexportacao", sequenceName="sq_memorandoexportacao")
public class Memorandoexportacao {
	
	private Integer cdmemorandoexportacao;
	private Notafiscalproduto notafiscalproduto;
	private String numero;
	private String registroexportacao;
	private String declaracaoexportacao;
	private Date dtdeclaracaoexportacao;
	private Date dtregistroexportacao;
	private Date dtconhecimentoembarque;
	private String conhecimentoembarque;
	private Fornecedor remetente;	
	private List<Memorandoexportacaonotafiscalprodutoitem> listaitens;
	private Date dtemissao;
	
	//Transient
	private List<Memorandoexportacao> listaMemorando;
	private Colaborador representante;
	private String whereIn;
	
	
	{
		this.dtemissao = SinedDateUtils.currentDate();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_memorandoexportacao")
	public Integer getCdmemorandoexportacao() {
		return cdmemorandoexportacao;
	}
	
	@ManyToOne
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	@Required
	public String getNumero() {
		return numero;
	}
	@DisplayName("Registro de Exporta��o")
	@Required
	public String getRegistroexportacao() {
		return registroexportacao;
	}
	@DisplayName("Declara��o de Exporta��o")
	@Required
	public String getDeclaracaoexportacao() {
		return declaracaoexportacao;
	}
	@DisplayName("Conhecimento de Embarque")
	@Required
	public String getConhecimentoembarque() {
		return conhecimentoembarque;
	}
	
	@ManyToOne
	@JoinColumn(name="cdfornecedor")
	@DisplayName("Remetente de Exporta��o")	
	public Fornecedor getRemetente() {
		return remetente;
	}
	
	@OneToMany(mappedBy="memorandoexportacao")	
	public List<Memorandoexportacaonotafiscalprodutoitem> getListaitens() {
		return listaitens;
	}
	
	public void setCdmemorandoexportacao(Integer cdmemorandoexportacao) {
		this.cdmemorandoexportacao = cdmemorandoexportacao;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setRegistroexportacao(String registroexportacao) {
		this.registroexportacao = registroexportacao;
	}
	public void setDeclaracaoexportacao(String declaracaoexportacao) {
		this.declaracaoexportacao = declaracaoexportacao;
	}
	public void setConhecimentoembarque(String conhecimentoembarque) {
		this.conhecimentoembarque = conhecimentoembarque;
	}
	public void setRemetente(Fornecedor remetente) {
		this.remetente = remetente;
	}
	public void setRepresentante(Colaborador representante) {
		this.representante = representante;
	}
	@DisplayName("Data DE")
	@Required
	public Date getDtdeclaracaoexportacao() {
		return dtdeclaracaoexportacao;
	}

	public void setDtdeclaracaoexportacao(Date dtdeclaracaoexportacao) {
		this.dtdeclaracaoexportacao = dtdeclaracaoexportacao;
	}
	@DisplayName("Data RE")
	@Required
	public Date getDtregistroexportacao() {
		return dtregistroexportacao;
	}

	public void setDtregistroexportacao(Date dtregistroexportacao) {
		this.dtregistroexportacao = dtregistroexportacao;
	}
	@DisplayName("Data CE")
	@Required
	public Date getDtconhecimentoembarque() {
		return dtconhecimentoembarque;
	}

	public void setDtconhecimentoembarque(Date dtconhecimentoembarque) {
		this.dtconhecimentoembarque = dtconhecimentoembarque;
	}

	public void setListaitens(List<Memorandoexportacaonotafiscalprodutoitem> listaitens) {
		this.listaitens = listaitens;
	}
	@DisplayName("Dt. Emiss�o")
	@Required
	public Date getDtemissao() {
		return dtemissao;
	}

	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}


	@Transient
	public List<Memorandoexportacao> getListaMemorando() {
		return listaMemorando;
	}
	@DisplayName("Representante Legal do Exportador")
	@Required
	@Transient
	public Colaborador getRepresentante() {
		return representante;
	}

	public void setListaMemorando(List<Memorandoexportacao> listaMemorando) {
		this.listaMemorando = listaMemorando;
	}
	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
}
