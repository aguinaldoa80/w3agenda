package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Mdfetipoproprietarioveiculo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoCarroceria;

@Entity
@SequenceGenerator(name = "sq_mdferodoviarioreboque", sequenceName = "sq_mdferodoviarioreboque")
public class MdfeRodoviarioReboque {

	protected Integer cdMdfeRodoviarioReboque;
	protected Mdfe mdfe;
	protected String codigoInternoVeiculo;
	protected String placa;
	protected String capacidadeKg;
	protected String capacidadeM3;
	protected TipoCarroceria tipoCarroceria;
	protected String tara;
	protected Uf ufLicenciamento;
	protected String renavam;
	protected Boolean pertenceEmpresaEmitente;
	protected Tipopessoa tipoPessoaProprietario = Tipopessoa.PESSOA_JURIDICA;
	protected String nomeProprietario;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String inscEstProprietario;
	protected Boolean isento;
	protected String rntrcProprietario;
	protected Uf ufProprietario;
	protected Mdfetipoproprietarioveiculo tipoProprietario;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdferodoviarioreboque")
	public Integer getCdMdfeRodoviarioReboque() {
		return cdMdfeRodoviarioReboque;
	}
	public void setCdMdfeRodoviarioReboque(Integer cdMdfeRodoviarioReboque) {
		this.cdMdfeRodoviarioReboque = cdMdfeRodoviarioReboque;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("C�digo interno do ve�culo")
	@MaxLength(value=10)
	public String getCodigoInternoVeiculo() {
		return codigoInternoVeiculo;
	}
	public void setCodigoInternoVeiculo(String codigoInternoVeiculo) {
		this.codigoInternoVeiculo = codigoInternoVeiculo;
	}
	
	@Required
	@MinLength(value=7)
	@MaxLength(value=7)
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	@Required
	@MaxLength(value=6)
	@DisplayName("Capacidade (KG)")
	public String getCapacidadeKg() {
		return capacidadeKg;
	}
	public void setCapacidadeKg(String capacidadeKg) {
		this.capacidadeKg = capacidadeKg;
	}
	
	@MaxLength(value=3)
	@DisplayName("Capacidade (M3)")
	public String getCapacidadeM3() {
		return capacidadeM3;
	}
	public void setCapacidadeM3(String capacidadeM3) {
		this.capacidadeM3 = capacidadeM3;
	}
	
	@Required
	@DisplayName("Tipo de Carroceria")
	public TipoCarroceria getTipoCarroceria() {
		return tipoCarroceria;
	}
	public void setTipoCarroceria(TipoCarroceria tipoCarroceria) {
		this.tipoCarroceria = tipoCarroceria;
	}
	
	@Required
	@DisplayName("Tara (KG)")
	@MaxLength(value=6)
	public String getTara() {
		return tara;
	}
	public void setTara(String tara) {
		this.tara = tara;
	}
	
	@Required
	@DisplayName("UF de Licenciamento do Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduflicenciamento")
	public Uf getUfLicenciamento() {
		return ufLicenciamento;
	}
	public void setUfLicenciamento(Uf ufLicenciamento) {
		this.ufLicenciamento = ufLicenciamento;
	}
	
	@MinLength(value=9)
	@MaxLength(value=11)
	public String getRenavam() {
		return renavam;
	}
	public void setRenavam(String renavam) {
		this.renavam = renavam;
	}
	
	@Required
	@DisplayName("Reboque Pertence � Empresa Emitente do MDFe")
	public Boolean getPertenceEmpresaEmitente() {
		return pertenceEmpresaEmitente;
	}
	public void setPertenceEmpresaEmitente(Boolean pertenceEmpresaEmitente) {
		this.pertenceEmpresaEmitente = pertenceEmpresaEmitente;
	}
	
	@DisplayName("Tipo de Propriet�rio")
	public Tipopessoa getTipoPessoaProprietario() {
		return tipoPessoaProprietario;
	}
	public void setTipoPessoaProprietario(Tipopessoa tipoPessoaProprietario) {
		this.tipoPessoaProprietario = tipoPessoaProprietario;
	}
	
	@DisplayName("Nome / Raz�o Social")
	@MaxLength(value=60)
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
	@DisplayName("Inscri��o Estadual")
	public String getInscEstProprietario() {
		return inscEstProprietario;
	}
	public void setInscEstProprietario(
			String inscricaoestadualproprietario) {
		this.inscEstProprietario = inscricaoestadualproprietario;
	}
	
	public Boolean getIsento() {
		return isento;
	}
	public void setIsento(Boolean isento) {
		this.isento = isento;
	}
	
	@DisplayName("RNTRC")
	public String getRntrcProprietario() {
		return rntrcProprietario;
	}
	public void setRntrcProprietario(String rntrcProprietario) {
		this.rntrcProprietario = rntrcProprietario;
	}
	
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufproprietario")
	public Uf getUfProprietario() {
		return ufProprietario;
	}
	public void setUfProprietario(Uf ufProprietario) {
		this.ufProprietario = ufProprietario;
	}
	
	@DisplayName("Tipo de Propriet�rio")
	public Mdfetipoproprietarioveiculo getTipoProprietario() {
		return tipoProprietario;
	}
	public void setTipoProprietario(Mdfetipoproprietarioveiculo tipoProprietario) {
		this.tipoProprietario = tipoProprietario;
	}
}
