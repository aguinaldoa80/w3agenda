package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.FormaintermediacaDI;
import br.com.linkcom.sined.geral.bean.enumeration.ViatransporteDI;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_declaracaoimportacao",sequenceName="sq_declaracaoimportacao")
public class Declaracaoimportacao implements Log {
	
	protected Integer cddeclaracaoimportacao;
	protected String numerodidsida;
	protected Date dtregistro;
	protected String localdesembaraco;
	protected Date data;
	protected Uf uf;
	protected String codigoexportador;
	protected ViatransporteDI viatransporte;
	protected Money valorafrmm;
	protected FormaintermediacaDI formaintermediacao;
	protected Cnpj cnpjadquirente;
	protected Uf ufadquirente;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Declaracaoimportacaoadicao> listaDeclaracaoimportacaoadicao = new ListSet<Declaracaoimportacaoadicao>(Declaracaoimportacaoadicao.class);
	
	//TRANSIENT
	protected Boolean importacaoXML;
	
	@Id
	@GeneratedValue(generator="sq_declaracaoimportacao", strategy=GenerationType.AUTO)
	public Integer getCddeclaracaoimportacao() {
		return cddeclaracaoimportacao;
	}
	
	@Required
	@DisplayName("N�mero DI/DSI/DA/DRI-E")
	@MaxLength(12)
	@DescriptionProperty
	public String getNumerodidsida() {
		return numerodidsida;
	}

	@Required
	@DisplayName("Data registro")
	public Date getDtregistro() {
		return dtregistro;
	}
	
	@Required
	@DisplayName("Local")
	public String getLocaldesembaraco() {
		return localdesembaraco;
	}
	
	@Required
	public Date getData() {
		return data;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cduf")
	public Uf getUf() {
		return uf;
	}

	@Required
	@DisplayName("Cod. exportador")
	public String getCodigoexportador() {
		return codigoexportador;
	}
	
	@OneToMany(mappedBy="declaracaoimportacao")
	public List<Declaracaoimportacaoadicao> getListaDeclaracaoimportacaoadicao() {
		return listaDeclaracaoimportacaoadicao;
	}
	
	@Required
	@DisplayName("Via de transporte")
	public ViatransporteDI getViatransporte() {
		return viatransporte;
	}

	@DisplayName("Valor AFRMM")
	public Money getValorafrmm() {
		return valorafrmm;
	}

	@Required
	@DisplayName("Forma de intermedia��o")
	public FormaintermediacaDI getFormaintermediacao() {
		return formaintermediacao;
	}

	@DisplayName("CNPJ do adquirente")
	public Cnpj getCnpjadquirente() {
		return cnpjadquirente;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdufadquirente")
	@DisplayName("UF do adquirente")
	public Uf getUfadquirente() {
		return ufadquirente;
	}

	public void setViatransporte(ViatransporteDI viatransporte) {
		this.viatransporte = viatransporte;
	}

	public void setValorafrmm(Money valorafrmm) {
		this.valorafrmm = valorafrmm;
	}

	public void setFormaintermediacao(FormaintermediacaDI formaintermediacao) {
		this.formaintermediacao = formaintermediacao;
	}

	public void setCnpjadquirente(Cnpj cnpjadquirente) {
		this.cnpjadquirente = cnpjadquirente;
	}

	public void setUfadquirente(Uf ufadquirente) {
		this.ufadquirente = ufadquirente;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaDeclaracaoimportacaoadicao(
			List<Declaracaoimportacaoadicao> listaDeclaracaoimportacaoadicao) {
		this.listaDeclaracaoimportacaoadicao = listaDeclaracaoimportacaoadicao;
	}

	public void setNumerodidsida(String numerodidsida) {
		this.numerodidsida = numerodidsida;
	}

	public void setDtregistro(Date dtregistro) {
		this.dtregistro = dtregistro;
	}

	public void setLocaldesembaraco(String localdesembaraco) {
		this.localdesembaraco = localdesembaraco;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setCodigoexportador(String codigoexportador) {
		this.codigoexportador = codigoexportador;
	}

	public void setCddeclaracaoimportacao(Integer cddeclaracaoimportacao) {
		this.cddeclaracaoimportacao = cddeclaracaoimportacao;
	}

	@Transient
	public Boolean getImportacaoXML() {
		return importacaoXML;
	}

	public void setImportacaoXML(Boolean importacaoXML) {
		this.importacaoXML = importacaoXML;
	}
	
	@Transient
	public String getNumerodidsidaFormatado(){
		if(getNumerodidsida() != null){
			StringBuilder s = new StringBuilder();
			for(int i = 0; i < getNumerodidsida().length(); i++){
				if(i == 2) s.append("/");
				if(i == 9) s.append("-");
				s.append(getNumerodidsida().charAt(i));
			}
			return s.toString();
			
		}
		return null; 
	}
}