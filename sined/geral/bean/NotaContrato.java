package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_notacontrato", sequenceName = "sq_notacontrato")
public class NotaContrato {

	protected Integer cdNotaContrato;
	protected Nota nota;
	protected Contrato contrato;
	protected Faturamentocontrato faturamentocontrato;
	
	public NotaContrato() {
		super();
	}
	
	public NotaContrato(Nota nota, Contrato contrato) {
		this.nota = nota;
		this.contrato = contrato;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notacontrato")
	public Integer getCdNotaContrato() {
		return cdNotaContrato;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Nota getNota() {
		return nota;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaturamento")
	public Faturamentocontrato getFaturamentocontrato() {
		return faturamentocontrato;
	}
	
	public void setFaturamentocontrato(Faturamentocontrato faturamentocontrato) {
		this.faturamentocontrato = faturamentocontrato;
	}
	
	public void setCdNotaContrato(Integer cdNotaContrato) {
		this.cdNotaContrato = cdNotaContrato;
	}
	
	public void setNota(Nota nota) {
		this.nota = nota;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
}
