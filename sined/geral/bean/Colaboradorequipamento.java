package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorequipamento", sequenceName = "sq_colaboradorequipamento")
public class Colaboradorequipamento {

	protected Integer cdcolaboradorequipamento;
	protected Colaborador colaborador;
	protected Epi epi;
	protected Integer qtde = 1;
	protected Date dtentrega;
	protected Date dtdevolucao;
	protected String certificadoaprovacao;
	protected Date dtvencimento;
	
	//TRANSIENT
	protected Integer cdlocalarmazenagem;
	protected Date dtvencimentotrans;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorequipamento")
	public Integer getCdcolaboradorequipamento() {
		return cdcolaboradorequipamento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@Required
	@DisplayName("Equipamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdepi")
	public Epi getEpi() {
		return epi;
	}
	
	@Required
	@DisplayName("Data de entrega")
	public Date getDtentrega() {
		return dtentrega;
	}
	
	@Required
	@DisplayName("Quantidade")
	public Integer getQtde() {
		return qtde;
	}
	
	@DisplayName("Data da devolu��o")
	public Date getDtdevolucao() {
		return dtdevolucao;
	}
	
	public void setDtdevolucao(Date dtdevolucao) {
		this.dtdevolucao = dtdevolucao;
	}
	
	public void setEpi(Epi epi) {
		this.epi = epi;
	}
	public void setDtentrega(Date dtentrega) {
		this.dtentrega = dtentrega;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}	
	public void setCdcolaboradorequipamento(Integer cdcolaboradorequipamento) {
		this.cdcolaboradorequipamento = cdcolaboradorequipamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@DisplayName("Certificado de aprova��o")
	public String getCertificadoaprovacao() {
		return certificadoaprovacao;
	}
	
	public void setCertificadoaprovacao(String certificadoaprovacao) {
		this.certificadoaprovacao = certificadoaprovacao;
	}
	
	@DisplayName("Data de vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	@Transient
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}

	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	
	@Transient
	public String getEpiStr(){
		StringBuilder str = new StringBuilder();
		
		if(epi!=null){
			if(epi.getIdentificacao()!=null && !"".equals(epi.getIdentificacao()))
				str.append(epi.getIdentificacao()).append(" - ");
			
			if(epi.getNome()!=null)
				str.append(epi.getNome());
		}
		
		return str.toString();
	}
	
	@Transient
	public Date getDtvencimentotrans() {
		return dtvencimentotrans;
	}
	public void setDtvencimentotrans(Date dtvencimentotrans) {
		this.dtvencimentotrans = dtvencimentotrans;
	}
}
