package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Recurso humano do or�amento")
@SequenceGenerator(name = "sq_orcamentorecursohumano", sequenceName = "sq_orcamentorecursohumano")
public class Orcamentorecursohumano {
	
	protected Integer cdorcamentorecursohumano;
	protected Orcamento orcamento;
	protected Cargo cargo;
	protected Double totalhoras;
	protected Money custohora;
	protected Fatormdo fatormdo;	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_orcamentorecursohumano")	
	public Integer getCdorcamentorecursohumano() {
		return cdorcamentorecursohumano;
	}

	@DisplayName("Or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@Required	
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@DisplayName("Cargo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required	
	public Cargo getCargo() {
		return cargo;
	}

	@DisplayName("Total de horas")
	@MinValue(0)
	public Double getTotalhoras() {
		return totalhoras;
	}
	
	@DisplayName("Custo por hora")
	public Money getCustohora() {
		return custohora;
	}

	@DisplayName("Fator de m�o-de-obra")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdfatormdo")
	public Fatormdo getFatormdo() {
		return fatormdo;
	}

	public void setCdorcamentorecursohumano(Integer cdorcamentorecursohumano) {
		this.cdorcamentorecursohumano = cdorcamentorecursohumano;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setTotalhoras(Double totalhoras) {
		this.totalhoras = totalhoras;
	}
	
	public void setCustohora(Money custohora) {
		this.custohora = custohora;
	}

	public void setFatormdo(Fatormdo fatormdo) {
		this.fatormdo = fatormdo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Orcamentorecursohumano) {
			Orcamentorecursohumano that = (Orcamentorecursohumano) obj;
			return this.getCdorcamentorecursohumano().equals(that.getCdorcamentorecursohumano());
		}		
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdorcamentorecursohumano != null) {
			return cdorcamentorecursohumano.hashCode();
		}
		return super.hashCode();
	}
}
