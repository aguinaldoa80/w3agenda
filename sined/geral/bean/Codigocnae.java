package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_codigocnae", sequenceName = "sq_codigocnae")
public class Codigocnae implements Log {

	protected Integer cdcodigocnae;
	protected String cnae;
	protected String descricaocnae;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_codigocnae")
	public Integer getCdcodigocnae() {
		return cdcodigocnae;
	}

	@DisplayName("C�digo")
	public String getCnae() {
		return cnae;
	}

	@DisplayName("Descri��o")
	public String getDescricaocnae() {
		return descricaocnae;
	}
	
	public void setCdcodigocnae(Integer cdcodigocnae) {
		this.cdcodigocnae = cdcodigocnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public void setDescricaocnae(String descricaocnae) {
		this.descricaocnae = descricaocnae;
	}
	
	// LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// TRANSIENTE
	
	@Transient
	@DescriptionProperty(usingFields={"cnae","descricaocnae"})
	public String getDescricaoCombo(){
		StringBuilder sb = new StringBuilder();
		if(this.cnae != null && this.descricaocnae != null){
			sb.append(this.cnae).append(" - ").append(this.descricaocnae);
		}
		return sb.toString();
	}
	
}
