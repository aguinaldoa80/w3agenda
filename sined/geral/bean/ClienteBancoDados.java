package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_clientebancodados", sequenceName = "sq_clientebancodados")
public class ClienteBancoDados implements Log{

	protected Integer cdclientebancodados;
	protected Cliente cliente;
	protected Contrato contrato;
	protected String nomebanco;
	protected Integer versaoatual;
	
	protected Set<ClienteBancoDadosModulo> listaModulos = new ListSet<ClienteBancoDadosModulo>(ClienteBancoDadosModulo.class);
	protected Set<ClienteBancoDadosTela> listaTelas = new ListSet<ClienteBancoDadosTela>(ClienteBancoDadosTela.class);
	
	public ClienteBancoDados() {
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientebancodados")
	public Integer getCdclientebancodados() {
		return cdclientebancodados;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}

	@MaxLength(100)
	public String getNomebanco() {
		return nomebanco;
	}

	@DisplayName("M�dulos")
	@OneToMany(mappedBy="clienteBancoDados")
	public Set<ClienteBancoDadosModulo> getListaModulos() {
		return listaModulos;
	}
	
	@DisplayName("Telas Avulsas")
	@OneToMany(mappedBy="clienteBancoDados")
	public Set<ClienteBancoDadosTela> getListaTelas() {
		return listaTelas;
	}
	
	@DisplayName("Vers�o Atual")
	public Integer getVersaoatual() {
		return versaoatual;
	}
	
	public void setVersaoatual(Integer versaoatual) {
		this.versaoatual = versaoatual;
	}
	
	public void setListaTelas(Set<ClienteBancoDadosTela> listaTelas) {
		this.listaTelas = listaTelas;
	}
	
	public void setListaModulos(Set<ClienteBancoDadosModulo> listaModulos) {
		this.listaModulos = listaModulos;
	}
	
	public void setCdclientebancodados(Integer cdclientebancodados) {
		this.cdclientebancodados = cdclientebancodados;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setNomebanco(String nomebanco) {
		this.nomebanco = nomebanco;
	}

	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}

	@Transient
	public Timestamp getDtaltera() {
		return null;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
	}	
	
	
}
