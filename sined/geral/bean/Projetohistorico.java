package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_projetohistorico", sequenceName = "sq_projetohistorico")
public class Projetohistorico {
	
	protected Integer cdprojetohistorico;
	protected Timestamp dthistorico;
	protected Usuario usuarioresponsavel;
	protected String observacao;
	protected Projeto projeto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_projetohistorico")
	public Integer getCdprojetohistorico() {
		return cdprojetohistorico;
	}
	
	@DisplayName("Data")
	public Timestamp getDthistorico() {
		return dthistorico;
	}
	
	@DisplayName("Responsável")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuarioresponsavel")
	public Usuario getUsuarioresponsavel() {
		return usuarioresponsavel;
	}
	
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setCdprojetohistorico(Integer cdprojetohistorico) {
		this.cdprojetohistorico = cdprojetohistorico;
	}
	public void setDthistorico(Timestamp dthistorico) {
		this.dthistorico = dthistorico;
	}
	public void setUsuarioresponsavel(Usuario usuarioresponsavel) {
		this.usuarioresponsavel = usuarioresponsavel;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	
	

}
