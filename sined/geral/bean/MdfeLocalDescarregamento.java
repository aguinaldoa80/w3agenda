package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdfelocaldescarregamento", sequenceName = "sq_mdfelocaldescarregamento")
@DisplayName("Loca de descarregamento")
public class MdfeLocalDescarregamento {

	protected Integer cdMdfeLocalDescarregamento;
	protected Mdfe mdfe;
	protected Municipio municipio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfelocaldescarregamento")
	public Integer getCdMdfeLocalDescarregamento() {
		return cdMdfeLocalDescarregamento;
	}
	public void setCdMdfeLocalDescarregamento(Integer cdMdfeLocalDescarregamento) {
		this.cdMdfeLocalDescarregamento = cdMdfeLocalDescarregamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@DisplayName("Município de descarregamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
