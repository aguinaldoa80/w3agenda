package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_comissionamentopessoa", sequenceName = "sq_comissionamentopessoa")
public class Comissionamentopessoa {

	protected Integer cdcomissionamentopessoa;
	protected Comissionamento comissionamento;
	protected Colaborador colaborador;
	protected Boolean recebecomissao = Boolean.TRUE;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comissionamentopessoa")
	public Integer getCdcomissionamentopessoa() {
		return cdcomissionamentopessoa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")	
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")	
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Recebe comiss�o")
	public Boolean getRecebecomissao() {
		return recebecomissao;
	}
	
	
	public void setCdcomissionamentopessoa(Integer cdcomissionamentopessoa) {
		this.cdcomissionamentopessoa = cdcomissionamentopessoa;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setRecebecomissao(Boolean recebecomissao) {
		this.recebecomissao = recebecomissao;
	}
}
