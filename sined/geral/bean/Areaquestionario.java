package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesposta;


@Entity
@SequenceGenerator(name = "sq_areaquestionario", sequenceName = "sq_areaquestionario")
public class Areaquestionario {

	protected Integer cdareaquestionario;
	protected Area area;
	protected Integer ordem;
	protected String pergunta;
	protected Tiporesposta tiporesposta;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_areaquestionario")
	public Integer getCdareaquestionario() {
		return cdareaquestionario;
	}

	@Required
	@MaxLength(200)
	@DisplayName("Pergunta")
	public String getPergunta() {
		return pergunta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarea")
	public Area getArea() {
		return area;
	}
	
	@MaxLength(10)
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	@DisplayName("Tipo de resposta")
	public Tiporesposta getTiporesposta() {
		return tiporesposta;
	}

	public void setCdareaquestionario(Integer cdareaquestionario) {
		this.cdareaquestionario = cdareaquestionario;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public void setTiporesposta(Tiporesposta tiporesposta) {
		this.tiporesposta = tiporesposta;
	}
	
}
