package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_cfoptipo", sequenceName = "sq_cfoptipo")
public class Cfoptipo {

	public static final Cfoptipo ENTRADA = new Cfoptipo(1,"Entrada"); 
	public static final Cfoptipo SAIDA = new Cfoptipo(2,"Sa�da"); 
	
	protected Integer cdcfoptipo;
	protected String descricao;
	
	public Cfoptipo(){
	}
	
	public Cfoptipo(Integer cdcfoptipo, String descricao){
		this.cdcfoptipo = cdcfoptipo;
		this.descricao = descricao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cfoptipo")
	public Integer getCdcfoptipo() {
		return cdcfoptipo;
	}
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdcfoptipo(Integer cdcfoptipo) {
		this.cdcfoptipo = cdcfoptipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcfoptipo == null) ? 0 : cdcfoptipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Cfoptipo other = (Cfoptipo) obj;
		if (cdcfoptipo == null) {
			if (other.cdcfoptipo != null)
				return false;
		} else if (!cdcfoptipo.equals(other.cdcfoptipo))
			return false;
		return true;
	}
	
	
	
}
