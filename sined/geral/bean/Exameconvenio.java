package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@SequenceGenerator(name = "sq_exameconvenio", sequenceName = "sq_exameconvenio")
@DisplayName("Conv�nio")
@Entity
public class Exameconvenio implements Log{
	
	protected Integer cdexameconvenio;
	protected String nome;
	protected Arquivo logotipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Exameconvenioclinica> listaExameconvenioclinica = new ListSet<Exameconvenioclinica>(Exameconvenioclinica.class);
	protected List<Exameconvenioresponsavel> listaExameconvenioresponsavel = new ListSet<Exameconvenioresponsavel>(Exameconvenioresponsavel.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exameconvenio")
	public Integer getCdexameconvenio() {
		return cdexameconvenio;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Logo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlogotipo")
	public Arquivo getLogotipo() {
		return logotipo;
	}

	@DisplayName("Cl�nicas")
	@OneToMany(mappedBy="exameconvenio")
	public List<Exameconvenioclinica> getListaExameconvenioclinica() {
		return listaExameconvenioclinica;
	}
	
	
	
	public void setListaExameconvenioclinica(
			List<Exameconvenioclinica> listaExameconvenioclinica) {
		this.listaExameconvenioclinica = listaExameconvenioclinica;
	}
	
	@DisplayName("Profissional")
	@OneToMany(mappedBy="exameconvenio")
	public List<Exameconvenioresponsavel> getListaExameconvenioresponsavel() {
		return listaExameconvenioresponsavel;
	}
	
	public void setListaExameconvenioresponsavel(List<Exameconvenioresponsavel> listaExameconvenioresponsavel) {
		this.listaExameconvenioresponsavel = listaExameconvenioresponsavel;
	}
	

	public void setCdexameconvenio(Integer cdexameconvenio) {
		this.cdexameconvenio = cdexameconvenio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLogotipo(Arquivo logotipo) {
		this.logotipo = logotipo;
	}


	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
