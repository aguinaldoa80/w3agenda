package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhaorigem;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivofolhatipo;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Arquivo de folha")
@SequenceGenerator(name = "sq_arquivofolha", sequenceName = "sq_arquivofolha")
public class Arquivofolha implements Log {

	protected Integer cdarquivofolha;
	protected Arquivofolhaorigem arquivofolhaorigem;
	protected Arquivofolhatipo arquivofolhatipo;
	protected Arquivofolhamotivo arquivofolhamotivo;
	protected Arquivo arquivo;
	protected Date dtinsercao;
	protected Arquivofolhasituacao arquivofolhasituacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Arquivofolhahistorico> listaArquivofolhahistorico;
	
	// TRANSIENTE
	protected String[] stringArquivo;
	
	public Arquivofolha() {
	}
	
	public Arquivofolha(Integer cdarquivofolha) {
		this.cdarquivofolha = cdarquivofolha;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivofolha")
	public Integer getCdarquivofolha() {
		return cdarquivofolha;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Required
	@DisplayName("Origem")
	public Arquivofolhaorigem getArquivofolhaorigem() {
		return arquivofolhaorigem;
	}

	@DisplayName("Tipo")
	public Arquivofolhatipo getArquivofolhatipo() {
		return arquivofolhatipo;
	}

	@Required
	@DisplayName("Motivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivofolhamotivo")
	public Arquivofolhamotivo getArquivofolhamotivo() {
		return arquivofolhamotivo;
	}

	@Required
	@DisplayName("Data inser��o")
	public Date getDtinsercao() {
		return dtinsercao;
	}

	@Required
	@DisplayName("Situa��o")
	public Arquivofolhasituacao getArquivofolhasituacao() {
		return arquivofolhasituacao;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="arquivofolha")
	public List<Arquivofolhahistorico> getListaArquivofolhahistorico() {
		return listaArquivofolhahistorico;
	}

	public void setListaArquivofolhahistorico(
			List<Arquivofolhahistorico> listaArquivofolhahistorico) {
		this.listaArquivofolhahistorico = listaArquivofolhahistorico;
	}

	public void setArquivofolhaorigem(Arquivofolhaorigem arquivofolhaorigem) {
		this.arquivofolhaorigem = arquivofolhaorigem;
	}

	public void setArquivofolhatipo(Arquivofolhatipo arquivofolhatipo) {
		this.arquivofolhatipo = arquivofolhatipo;
	}

	public void setArquivofolhamotivo(Arquivofolhamotivo arquivofolhamotivo) {
		this.arquivofolhamotivo = arquivofolhamotivo;
	}

	public void setDtinsercao(Date dtinsercao) {
		this.dtinsercao = dtinsercao;
	}

	public void setArquivofolhasituacao(Arquivofolhasituacao arquivofolhasituacao) {
		this.arquivofolhasituacao = arquivofolhasituacao;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setCdarquivofolha(Integer cdarquivofolha) {
		this.cdarquivofolha = cdarquivofolha;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Transient
	public String[] getStringArquivo() {
		return stringArquivo;
	}

	public void setStringArquivo(String[] stringArquivo) {
		this.stringArquivo = stringArquivo;
	}
	
	
	
	
}
