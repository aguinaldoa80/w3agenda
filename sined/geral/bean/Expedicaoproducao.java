package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_expedicaoproducao", sequenceName = "sq_expedicaoproducao")
public class Expedicaoproducao implements Log, PermissaoClienteEmpresa {

	protected Integer cdexpedicaoproducao;
	protected Integer identificadorcarregamento;
	protected Fornecedor transportadora;
	protected Veiculo veiculo;
	protected Colaborador motorista;
	protected Date dtexpedicao;
	protected Expedicaosituacao expedicaosituacao = Expedicaosituacao.EM_ABERTO; 
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Expedicaoproducaoitem> listaExpedicaoproducaoitem = new ListSet<Expedicaoproducaoitem>(Expedicaoproducaoitem.class);
	protected List<Expedicaoproducaohistorico> listaExpedicaoproducaohistorico = new ListSet<Expedicaoproducaohistorico>(Expedicaoproducaohistorico.class);
	
	// TRANSIENTE
	protected Boolean isCriar;
	protected Producaoagenda producaoagenda;
	
	public Expedicaoproducao() {
	}
	
	public Expedicaoproducao(Integer cdexpedicaoproducao) {
		this.cdexpedicaoproducao = cdexpedicaoproducao;
	}

	@Id
	@DisplayName("Id da expedi��o")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicaoproducao")
	public Integer getCdexpedicaoproducao() {
		return cdexpedicaoproducao;
	}
	
	@Required
	@MaxLength(9)
	@DisplayName("Id do carregamento")
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtransportadora")
	public Fornecedor getTransportadora() {
		return transportadora;
	}

	@Required
	@DisplayName("Data")
	public Date getDtexpedicao() {
		return dtexpedicao;
	}

	@DisplayName("Situa��o")
	public Expedicaosituacao getExpedicaosituacao() {
		return expedicaosituacao;
	}
	
	@OneToMany(mappedBy="expedicaoproducao")
	public List<Expedicaoproducaoitem> getListaExpedicaoproducaoitem() {
		return listaExpedicaoproducaoitem;
	}
	
	@OneToMany(mappedBy="expedicaoproducao")
	public List<Expedicaoproducaohistorico> getListaExpedicaoproducaohistorico() {
		return listaExpedicaoproducaohistorico;
	}
	
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotorista")
	public Colaborador getMotorista() {
		return motorista;
	}
	
	public void setMotorista(Colaborador motorista) {
		this.motorista = motorista;
	}
	
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setListaExpedicaoproducaohistorico(
			List<Expedicaoproducaohistorico> listaExpedicaoproducaohistorico) {
		this.listaExpedicaoproducaohistorico = listaExpedicaoproducaohistorico;
	}
	
	public void setListaExpedicaoproducaoitem(List<Expedicaoproducaoitem> listaExpedicaoproducaoitem) {
		this.listaExpedicaoproducaoitem = listaExpedicaoproducaoitem;
	}

	public void setCdexpedicaoproducao(Integer cdexpedicaoproducao) {
		this.cdexpedicaoproducao = cdexpedicaoproducao;
	}

	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}

	public void setTransportadora(Fornecedor transportadora) {
		this.transportadora = transportadora;
	}

	public void setDtexpedicao(Date dtexpedicao) {
		this.dtexpedicao = dtexpedicao;
	}

	public void setExpedicaosituacao(Expedicaosituacao expedicaosituacao) {
		this.expedicaosituacao = expedicaosituacao;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select expedicaoproducaoSubQueryClienteEmpresa.cdexpedicaoproducao " +
				"from Expedicaoproducao expedicaoproducaoSubQueryClienteEmpresa " +
				"left outer join expedicaoproducaoSubQueryClienteEmpresa.listaExpedicaoproducaoitem listaExpedicaoproducaoitemSubQueryClienteEmpresa " +
				"left outer join listaExpedicaoproducaoitemSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}

//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	// TRANSIENTES
	
	@Transient
	public Boolean getIsCriar() {
		return isCriar;
	}
	
	public void setIsCriar(Boolean isCriar) {
		this.isCriar = isCriar;
	}	
	
	@Transient
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
}
