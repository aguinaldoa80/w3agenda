package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_cemobra", sequenceName = "sq_cemobra")
public class Cemobra implements Log {

	protected Integer cdcemobra;
	protected String codigo;
	protected String nome;
	protected String cliente_nome;
	protected String cliente_cnpjcpf;
	protected String cliente_logradouro;
	protected String cliente_numero;
	protected String cliente_complemento;
	protected String cliente_bairro;
	protected String cliente_cidade;
	protected String cliente_cep;
	protected String cliente_uf;
	protected Boolean importado;
	protected Arquivo arquivo;
	
	protected Set<Cemtipologia> listaCemtipologia = new ListSet<Cemtipologia>(Cemtipologia.class);
	protected Set<Cemcomponenteacumulado> listaCemcomponenteacumulado = new ListSet<Cemcomponenteacumulado>(Cemcomponenteacumulado.class);
	protected Set<Cemperfilacumulado> listaCemperfilacumulado = new ListSet<Cemperfilacumulado>(Cemperfilacumulado.class);
	
	protected List<Cemtipologia> listaCemtipologiaLIST = new ListSet<Cemtipologia>(Cemtipologia.class);
	protected List<Cemcomponenteacumulado> listaCemcomponenteacumuladoLIST = new ListSet<Cemcomponenteacumulado>(Cemcomponenteacumulado.class);
	protected List<Cemperfilacumulado> listaCemperfilacumuladoLIST = new ListSet<Cemperfilacumulado>(Cemperfilacumulado.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cemobra")
	public Integer getCdcemobra() {
		return cdcemobra;
	}

	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	
	@MaxLength(250)
	public String getNome() {
		return nome;
	}

	@MaxLength(80)
	@DisplayName("Nome")
	public String getCliente_nome() {
		return cliente_nome;
	}

	@MaxLength(18)
	@DisplayName("CNPJ/CPF")
	public String getCliente_cnpjcpf() {
		return cliente_cnpjcpf;
	}

	@MaxLength(200)
	@DisplayName("Logradouro")
	public String getCliente_logradouro() {
		return cliente_logradouro;
	}

	@MaxLength(50)
	@DisplayName("N�mero")
	public String getCliente_numero() {
		return cliente_numero;
	}

	@MaxLength(50)
	@DisplayName("Complemento")
	public String getCliente_complemento() {
		return cliente_complemento;
	}

	@MaxLength(100)
	@DisplayName("Bairro")
	public String getCliente_bairro() {
		return cliente_bairro;
	}

	@MaxLength(50)
	@DisplayName("Cidade")
	public String getCliente_cidade() {
		return cliente_cidade;
	}

	@MaxLength(30)
	@DisplayName("CEP")
	public String getCliente_cep() {
		return cliente_cep;
	}

	@MaxLength(2)
	@DisplayName("UF")
	public String getCliente_uf() {
		return cliente_uf;
	}
	
	@OneToMany(mappedBy="cemobra")
	public Set<Cemtipologia> getListaCemtipologia() {
		return listaCemtipologia;
	}
	
	@OneToMany(mappedBy="cemobra")
	public Set<Cemcomponenteacumulado> getListaCemcomponenteacumulado() {
		return listaCemcomponenteacumulado;
	}
	
	@OneToMany(mappedBy="cemobra")
	public Set<Cemperfilacumulado> getListaCemperfilacumulado() {
		return listaCemperfilacumulado;
	}
	
	public Boolean getImportado() {
		return importado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setImportado(Boolean importado) {
		this.importado = importado;
	}
	
	public void setListaCemperfilacumulado(
			Set<Cemperfilacumulado> listaCemperfilacumulado) {
		this.listaCemperfilacumulado = listaCemperfilacumulado;
	}
	
	public void setListaCemcomponenteacumulado(
			Set<Cemcomponenteacumulado> listaCemcomponenteacumulado) {
		this.listaCemcomponenteacumulado = listaCemcomponenteacumulado;
	}
	
	public void setListaCemtipologia(Set<Cemtipologia> listaCemtipologia) {
		this.listaCemtipologia = listaCemtipologia;
	}

	public void setCdcemobra(Integer cdcemobra) {
		this.cdcemobra = cdcemobra;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCliente_nome(String clienteNome) {
		cliente_nome = clienteNome;
	}

	public void setCliente_cnpjcpf(String clienteCnpjcpf) {
		cliente_cnpjcpf = clienteCnpjcpf;
	}

	public void setCliente_logradouro(String clienteLogradouro) {
		cliente_logradouro = clienteLogradouro;
	}

	public void setCliente_numero(String clienteNumero) {
		cliente_numero = clienteNumero;
	}

	public void setCliente_complemento(String clienteComplemento) {
		cliente_complemento = clienteComplemento;
	}

	public void setCliente_bairro(String clienteBairro) {
		cliente_bairro = clienteBairro;
	}

	public void setCliente_cidade(String clienteCidade) {
		cliente_cidade = clienteCidade;
	}

	public void setCliente_cep(String clienteCep) {
		cliente_cep = clienteCep;
	}

	public void setCliente_uf(String clienteUf) {
		cliente_uf = clienteUf;
	}
	
	@Transient
	public List<Cemtipologia> getListaCemtipologiaLIST() {
		if(this.listaCemtipologia != null && this.listaCemtipologia.size() > 0){
			listaCemtipologiaLIST = new ArrayList<Cemtipologia>(this.listaCemtipologia);
			try{
				Collections.sort(listaCemtipologiaLIST,new Comparator<Cemtipologia>(){
					public int compare(Cemtipologia o1, Cemtipologia o2) {
						if(o1.getDescr() == null) return 1; 
						if(o2.getDescr() == null) return -1;
						return o1.getDescr().compareTo(o2.getDescr());
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return listaCemtipologiaLIST;
	}
	
	@Transient
	public List<Cemcomponenteacumulado> getListaCemcomponenteacumuladoLIST() {
		if(this.listaCemcomponenteacumulado != null && this.listaCemcomponenteacumulado.size() > 0){
			listaCemcomponenteacumuladoLIST = new ArrayList<Cemcomponenteacumulado>(this.listaCemcomponenteacumulado);
			
			Collections.sort(listaCemcomponenteacumuladoLIST,new Comparator<Cemcomponenteacumulado>(){
				public int compare(Cemcomponenteacumulado o1, Cemcomponenteacumulado o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCemcomponenteacumuladoLIST;
	}
	
	@Transient
	public List<Cemperfilacumulado> getListaCemperfilacumuladoLIST() {
		if(this.listaCemperfilacumulado != null && this.listaCemperfilacumulado.size() > 0){
			listaCemperfilacumuladoLIST = new ArrayList<Cemperfilacumulado>(this.listaCemperfilacumulado);
			
			Collections.sort(listaCemperfilacumuladoLIST,new Comparator<Cemperfilacumulado>(){
				public int compare(Cemperfilacumulado o1, Cemperfilacumulado o2) {
					if(o1.getDescricao() == null) return 1;
					if(o2.getDescricao() == null) return -1;
					return o1.getDescricao().compareTo(o2.getDescricao());
				}
			});
		}
		
		return listaCemperfilacumuladoLIST;
	}

	public void setListaCemtipologiaLIST(List<Cemtipologia> listaCemtipologiaLIST) {
		this.listaCemtipologiaLIST = listaCemtipologiaLIST;
	}

	public void setListaCemcomponenteacumuladoLIST(
			List<Cemcomponenteacumulado> listaCemcomponenteacumuladoLIST) {
		this.listaCemcomponenteacumuladoLIST = listaCemcomponenteacumuladoLIST;
	}

	public void setListaCemperfilacumuladoLIST(
			List<Cemperfilacumulado> listaCemperfilacumuladoLIST) {
		this.listaCemperfilacumuladoLIST = listaCemperfilacumuladoLIST;
	}

	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {}
	public void setDtaltera(Timestamp dtaltera) {}
	
}
