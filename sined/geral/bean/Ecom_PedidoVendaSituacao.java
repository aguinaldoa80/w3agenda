package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_ecom_pedidovendasituacao", sequenceName="sq_ecom_pedidovendasituacao")
public class Ecom_PedidoVendaSituacao {

	private Integer cdEcom_PedidoVendaSituacao;
	private Integer id_situacao;
	private String situacao;
	private String texto;
	private String controle_interno;
	private Ecom_PedidoVenda ecomPedidoVenda;
	
	

	@Id
	@GeneratedValue(generator="sq_ecom_pedidovendasituacao", strategy=GenerationType.AUTO)
	public Integer getCdEcom_PedidoVendaSituacao() {
		return cdEcom_PedidoVendaSituacao;
	}
	public void setCdEcom_PedidoVendaSituacao(Integer cdEcom_PedidoVendaSituacao) {
		this.cdEcom_PedidoVendaSituacao = cdEcom_PedidoVendaSituacao;
	}
	public Integer getId_situacao() {
		return id_situacao;
	}
	public void setId_situacao(Integer id_situacao) {
		this.id_situacao = id_situacao;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getControle_interno() {
		return controle_interno;
	}
	public void setControle_interno(String controle_interno) {
		this.controle_interno = controle_interno;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_pedidovenda")
	public Ecom_PedidoVenda getEcomPedidoVenda() {
		return ecomPedidoVenda;
	}
	public void setEcomPedidoVenda(Ecom_PedidoVenda ecomPedidoVenda) {
		this.ecomPedidoVenda = ecomPedidoVenda;
	}
}
