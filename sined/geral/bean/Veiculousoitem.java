package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_veiculousoitem", sequenceName = "sq_veiculousoitem")
public class Veiculousoitem {

	protected Integer cdveiculousoitem;
	protected Veiculouso veiculouso;
	protected Patrimonioitem patrimonioitem;
	
	protected String plaqueta;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculousoitem")
	public Integer getCdveiculousoitem() {
		return cdveiculousoitem;
	}
	@Required
	@DisplayName("Ve�culo uso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso")
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	@Required
	@DisplayName("Item de patrim�nio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	public void setCdveiculousoitem(Integer cdveiculousoitem) {
		this.cdveiculousoitem = cdveiculousoitem;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	
	//===================== TRANSIENT's
	
	@MaxLength(50)
	@Transient
	public String getPlaqueta() {
		return getPatrimonioitem() != null ? getPatrimonioitem().getPlaqueta() : plaqueta;
	}
	
	public void setPlaqueta(String plaqueta) {
		this.plaqueta = plaqueta;
	}
	
}