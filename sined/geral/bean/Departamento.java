package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_departamento", sequenceName = "sq_departamento")
public class Departamento implements Log{

	protected Integer cddepartamento;
	protected String nome;
	protected String descricao;
	protected String codigofolha;
	protected List<Cargodepartamento> listaCargodepartamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Colaborador responsavel;
	
//	TRANSIENTES
	protected Departamento departamento;
	
	public Departamento(){}
	
	public Departamento(Integer cddepartamento){
		this.cddepartamento = cddepartamento;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_departamento")
	public Integer getCddepartamento() {
		return cddepartamento;
	}
	public void setCddepartamento(Integer id) {
		this.cddepartamento = id;
	}

	
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
		
	@OneToMany(mappedBy = "departamento")
	@DisplayName("Cargo")
	public List<Cargodepartamento> getListaCargodepartamento() {
		return listaCargodepartamento;
	}
	
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setListaCargodepartamento(List<Cargodepartamento> listaCargodepartamento) {
		this.listaCargodepartamento = listaCargodepartamento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("C�digo Folha")
	@MaxLength(20)
	public String getCodigofolha() {
		return codigofolha;
	}
	
	public void setCodigofolha(String codigofolha) {
		this.codigofolha = codigofolha;
	}
	
	@Transient
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"nome","codigofolha"})
	public String getDescricaoCombo(){
		return this.nome + (this.codigofolha != null ? " - " + this.codigofolha : "");
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cddepartamento == null) ? 0 : cddepartamento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departamento other = (Departamento) obj;
		if (cddepartamento == null) {
			if (other.cddepartamento != null)
				return false;
		} else if (!cddepartamento.equals(other.cddepartamento))
			return false;
		return true;
	}
	
	@Transient
	public String getDescricaoAutocomplete(){
		return this.nome;
	}	
}
