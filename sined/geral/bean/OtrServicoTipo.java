package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_otrservicotipo", sequenceName = "sq_otrservicotipo")
public class OtrServicoTipo implements Log{
	protected Integer cdOtrServicoTipo;
	protected String tipoServico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrservicotipo")
	public Integer getCdOtrServicoTipo() {
		return cdOtrServicoTipo;
	}
	public void setCdOtrServicoTipo(Integer cdOtrServicoTipo) {
		this.cdOtrServicoTipo = cdOtrServicoTipo;
	}
	@DescriptionProperty
	public String getTipoServico() {
		return tipoServico;
	}
	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
