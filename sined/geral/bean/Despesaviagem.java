package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Formula;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemdestino;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaodespesaviagem;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_despesaviagem", sequenceName = "sq_despesaviagem")
@DisplayName("Despesa de viagem")
@JoinEmpresa("despesaviagem.empresa")
public class Despesaviagem implements Log, PermissaoProjeto{

	protected Integer cddespesaviagem;
	protected Empresa empresa;
	protected String origemviagem;
	protected String destinoviagem;
	protected String descricao;
	protected String motivo;
	protected Date dtsaida;
	protected Date dtretornoprevisto;
	protected Date dtretornorealizado;
	protected Colaborador colaborador;
	protected String observacao;
	protected Despesaviagemdestino despesaviagemdestino;
	protected Contacrm contacrm;
	protected Cliente cliente;
	protected Endereco endereco;
	protected Contacrmcontato contacrmcontato;
	protected Fornecedor fornecedor;
	protected Boolean reembolsavel;
	protected Boolean reembolsogerado;
	protected String projetos;
	
	protected Situacaodespesaviagem situacaodespesaviagem = Situacaodespesaviagem.EM_ABERTO;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	protected List<Despesaviagemitem> listaDespesaviagemitem = new ListSet<Despesaviagemitem>(Despesaviagemitem.class);
	protected List<Despesaviagemprojeto> listaDespesaviagemprojeto = new ListSet<Despesaviagemprojeto>(Despesaviagemprojeto.class);
	protected List<DespesaviagemHistorico> listaDespesaviagemHistorico;
	
//	TRANSIENTES
	protected Boolean formapagamento;
	protected Boolean pagar;
	protected Money valoracerto;
	protected Money totalprevisto;
	protected Money totalrealizado;
	protected String controller;
	protected String whereIn;
	protected Rateio rateio;
	protected Tipooperacao tipooperacao;
	protected Money totalacertoadiantamento;
	protected List<Despesaviagem> listaDespesaviagem;
	protected String whereInDespesas;
	protected Money totaladiantamento;
	
	public Despesaviagem() {
	}
	
	public Despesaviagem(Integer cddespesaviagem) {
		this.cddespesaviagem = cddespesaviagem;
	}


	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_despesaviagem")
	public Integer getCddespesaviagem() {
		return cddespesaviagem;
	}
	
	@Required
	@DisplayName("Origem da viagem")
	@MaxLength(100)
	public String getOrigemviagem() {
		return origemviagem;
	}

	@DisplayName("Destino da viagem")
	@MaxLength(100)
	public String getDestinoviagem() {
		return destinoviagem;
	}

	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}

	@MaxLength(100)
	public String getMotivo() {
		return motivo;
	}

	@Required
	@DisplayName("Data da sa�da")
	public Date getDtsaida() {
		return dtsaida;
	}
	
	@Required
	@DisplayName("Data de retorno previsto")
	public Date getDtretornoprevisto() {
		return dtretornoprevisto;
	}

	@DisplayName("Data de retorno realizado")
	public Date getDtretornorealizado() {
		return dtretornorealizado;
	}

	@Required
	@DisplayName("Respons�vel pela despesa de viagem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@MaxLength(1000)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Situa��o")
	@Column(name="situacao")
	public Situacaodespesaviagem getSituacaodespesaviagem() {
		return situacaodespesaviagem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("Destino �")
	public Despesaviagemdestino getDespesaviagemdestino() {
		return despesaviagemdestino;
	}
	
	@DisplayName("Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Conta CRM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrm")
	public Contacrm getContacrm() {
		return contacrm;
	}
	
	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdendereco")
	public Endereco getEndereco() {
		return endereco;
	}
	
	@DisplayName("Reembols�vel?")
	public Boolean getReembolsavel() {
		return reembolsavel;
	}
	
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@DisplayName("Reembolso gerado?")
	public Boolean getReembolsogerado() {
		return reembolsogerado;
	}
	
	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrmcontato")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setOrigemviagem(String origemviagem) {
		this.origemviagem = StringUtils.trimToNull(origemviagem);
	}

	public void setDestinoviagem(String destinoviagem) {
		this.destinoviagem = StringUtils.trimToNull(destinoviagem);
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	public void setMotivo(String motivo) {
		this.motivo = StringUtils.trimToNull(motivo);
	}

	public void setDtsaida(Date dtsaida) {
		this.dtsaida = dtsaida;
	}

	public void setDtretornoprevisto(Date dtretornoprevisto) {
		this.dtretornoprevisto = dtretornoprevisto;
	}

	public void setDtretornorealizado(Date dtretornorealizado) {
		this.dtretornorealizado = dtretornorealizado;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCddespesaviagem(Integer cddespesaviagem) {
		this.cddespesaviagem = cddespesaviagem;
	}
	
	public void setSituacaodespesaviagem(
			Situacaodespesaviagem situacaodespesaviagem) {
		this.situacaodespesaviagem = situacaodespesaviagem;
	}
	
	public void setDespesaviagemdestino(Despesaviagemdestino despesaviagemdestino) {
		this.despesaviagemdestino = despesaviagemdestino;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void setReembolsavel(Boolean reembolsavel) {
		this.reembolsavel = reembolsavel;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setReembolsogerado(Boolean reembolsogerado) {
		this.reembolsogerado = reembolsogerado;
	}
	
//	LISTAS
	
	@OneToMany(mappedBy="despesaviagem")
	@DisplayName("Itens de despesa")
	public List<Despesaviagemitem> getListaDespesaviagemitem() {
		return listaDespesaviagemitem;
	}
	@OneToMany(mappedBy="despesaviagem")
	@DisplayName("Projetos")
	public List<Despesaviagemprojeto> getListaDespesaviagemprojeto() {
		return listaDespesaviagemprojeto;
	}

	public void setListaDespesaviagemprojeto(List<Despesaviagemprojeto> listaDespesaviagemprojeto) {
		this.listaDespesaviagemprojeto = listaDespesaviagemprojeto;
	}
	public void setListaDespesaviagemitem(List<Despesaviagemitem> listaDespesaviagemitem) {
		this.listaDespesaviagemitem = listaDespesaviagemitem;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	TRASIENTES
	
	@Transient
	@DisplayName("Forma de pagamento")
	public Boolean getFormapagamento() {
		return formapagamento;
	}
	
	@Transient
	public Boolean getPagar() {
		return pagar;
	}
	
	@Transient
	public Money getValoracerto() {
		return valoracerto;
	}
	
	@Transient
	public Money getTotalprevisto() {
		Money resultado = new Money(); 
		if (listaDespesaviagemitem != null && listaDespesaviagemitem.size() > 0) {
			for (Despesaviagemitem di : listaDespesaviagemitem) {
				resultado = resultado.add(di.getValorprevisto() != null ? di.getValorprevisto() : new Money());				
			}			
		}
		return resultado;
	}
	
	@Transient
	public Money getTotalrealizado() {
		Money resultado = new Money(); 
		if (listaDespesaviagemitem != null && listaDespesaviagemitem.size() > 0) {
			for (Despesaviagemitem di : listaDespesaviagemitem) {
				resultado = resultado.add(di.getValorrealizado() != null ? di.getValorrealizado() : new Money());				
			}			
		}
		return resultado;
	}
	
	@Transient
	public String getController() {
		return controller;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	@Transient
	public Rateio getRateio() {
		return rateio;
	}
	
	@Transient
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	public void setController(String controller) {
		this.controller = controller;
	}
	
	public void setTotalprevisto(Money totalprevisto) {
		this.totalprevisto = totalprevisto;
	}
	
	public void setTotalrealizado(Money totalrealizado) {
		this.totalrealizado = totalrealizado;
	}
	
	public void setValoracerto(Money valoracerto) {
		this.valoracerto = valoracerto;
	}
	
	public void setPagar(Boolean pagar) {
		this.pagar = pagar;
	}
	
	public void setFormapagamento(Boolean formapagamento) {
		this.formapagamento = formapagamento;
	}
	
    @Transient
	public Money getTotalacertoadiantamento() {
		return totalacertoadiantamento;
	}
	public void setTotalacertoadiantamento(Money totalacertoadiantamento) {
		this.totalacertoadiantamento = totalacertoadiantamento;
	}
	
	@Transient
	public List<Despesaviagem> getListaDespesaviagem() {
		return listaDespesaviagem;
	}
	
	public void setListaDespesaviagem(List<Despesaviagem> listaDespesaviagem) {
		this.listaDespesaviagem = listaDespesaviagem;
	}
	
	@Transient
	public String getWhereInDespesas() {
		return whereInDespesas;
	}
	public void setWhereInDespesas(String whereInDespesas) {
		this.whereInDespesas = whereInDespesas;
	}

	@OneToMany(mappedBy="despesaviagem")
	@DisplayName("Hist�rico")
	public List<DespesaviagemHistorico> getListaDespesaviagemHistorico() {
		return listaDespesaviagemHistorico;
	}
	public void setListaDespesaviagemHistorico(List<DespesaviagemHistorico> listaDespesaviagemHistorico) {
		this.listaDespesaviagemHistorico = listaDespesaviagemHistorico;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cddespesaviagem == null) ? 0 : cddespesaviagem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Despesaviagem other = (Despesaviagem) obj;
		if (cddespesaviagem == null) {
			if (other.cddespesaviagem != null)
				return false;
		} else if (!cddespesaviagem.equals(other.cddespesaviagem))
			return false;
		return true;
	}
	
	@Override
	public String subQueryProjeto() {
		return "select despesaviagemsubQueryProjeto.cddespesaviagem " +
			   "from Despesaviagem despesaviagemsubQueryProjeto " +
			   "left outer join despesaviagemsubQueryProjeto.listaDespesaviagemprojeto listaDespesaviagemprojetosubQueryProjeto " +
			   "left outer join listaDespesaviagemprojetosubQueryProjeto.projeto projetosubQueryProjeto " +
			   "where (projetosubQueryProjeto.cdprojeto is null or projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") )";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public Money getTotaladiantamento() {
		return totaladiantamento;
	}

	public void setTotaladiantamento(Money totaladiantamento) {
		this.totaladiantamento = totaladiantamento;
	}

	@DisplayName("Destino da viagem")
	@Transient
	public String getDestinoDaViagem(){
		String destinodaviagem="";
		if(StringUtils.isNotBlank(destinoviagem)){
			destinodaviagem = destinoviagem;
		}
		if(getCliente()!=null){
			if(StringUtils.isNotBlank(cliente.getNome())){
				destinodaviagem = cliente.getNome();
			}
		}
		if(fornecedor!=null){
			if(StringUtils.isNotBlank(fornecedor.getNome())){
				destinodaviagem = fornecedor.getNome();
			}
		}
		if(contacrm != null){
			if(StringUtils.isNotBlank(contacrm.getNome())){
				destinodaviagem = contacrm.getNome();
			}
		}
		return destinodaviagem;
	}
	
	@Formula("(select string_agg(p.nome, ', ') from despesaviagemprojeto dvp join projeto p on p.cdprojeto = dvp.cdprojeto where dvp.cddespesaviagem = cddespesaviagem)")
	public String getProjetos(){
		return this.projetos;
	}
	
	public void setProjetos(String projetos) {
		this.projetos = projetos;
	}
}
