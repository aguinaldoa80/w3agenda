package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.TipoExploracao;
import br.com.linkcom.sined.geral.bean.enumeration.TipoTerceiros;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_propriedaderural", sequenceName="sq_propriedaderural")
public class PropriedadeRural implements Log {
	protected Integer cdPropriedadeRural;
	protected String nome;
	protected Integer codigoImovel;
	protected Boolean usarSequencial;
	protected String cafir;
	protected String caepf;
	protected Empresa empresa;
	protected TipoExploracao tipoExploracao;
	protected TipoTerceiros tipoTerceiros;
	protected Boolean ativo;
	protected Boolean principal;
	
	protected List<Endereco> listaEndereco;
	protected List<PropriedadeRuralTelefone> listaPropriedadeRuralTelefone;
	protected List<ParticipantePropriedadeRural> listaParticipantePropriedadeRural;
	protected List<PropriedadeRuralHistorico> listaPropriedadeRuralHistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_propriedaderural")
	@DisplayName("C�digo")
	public Integer getCdPropriedadeRural() {
		return cdPropriedadeRural;
	}
	
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	@DisplayName("C�digo do im�vel")
	public Integer getCodigoImovel() {
		return codigoImovel;
	}
	
	@DisplayName("Usar Sequencial")
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}
	
	@DisplayName("CAFIR")
	public String getCafir() {
		return cafir;
	}
	
	@DisplayName("CAEPF")
	public String getCaepf() {
		return caepf;
	}
	
	@DisplayName("Fazenda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Tipo de Explora��o")
	public TipoExploracao getTipoExploracao() {
		return tipoExploracao;
	}
	
	@DisplayName("Tipo de Terceiros")
	public TipoTerceiros getTipoTerceiros() {
		return tipoTerceiros;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Principal")
	public Boolean getPrincipal() {
		return principal;
	}
	
	@OneToMany(mappedBy="propriedadeRural")
	@DisplayName("Endere�o")
	public List<Endereco> getListaEndereco() {
		return listaEndereco;
	}
	
	@OneToMany(mappedBy="propriedadeRural")
	public List<PropriedadeRuralTelefone> getListaPropriedadeRuralTelefone() {
		return listaPropriedadeRuralTelefone;
	}
	
	@OneToMany(mappedBy="propriedadeRural")
	@DisplayName("Percentual de Participa��o")
	public List<ParticipantePropriedadeRural> getListaParticipantePropriedadeRural() {
		return listaParticipantePropriedadeRural;
	}
	
	@OneToMany(mappedBy="propriedadeRural")
	public List<PropriedadeRuralHistorico> getListaPropriedadeRuralHistorico() {
		return listaPropriedadeRuralHistorico;
	}
	
	public void setCdPropriedadeRural(Integer cdPropriedadeRural) {
		this.cdPropriedadeRural = cdPropriedadeRural;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoImovel(Integer codigoImovel) {
		this.codigoImovel = codigoImovel;
	}
	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}
	public void setCafir(String cafir) {
		this.cafir = cafir;
	}
	public void setCaepf(String caepf) {
		this.caepf = caepf;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipoExploracao(TipoExploracao tipoExploracao) {
		this.tipoExploracao = tipoExploracao;
	}
	public void setTipoTerceiros(TipoTerceiros tipoTerceiros) {
		this.tipoTerceiros = tipoTerceiros;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
	public void setListaEndereco(List<Endereco> listaEndereco) {
		this.listaEndereco = listaEndereco;
	}
	public void setListaPropriedadeRuralTelefone(List<PropriedadeRuralTelefone> listaPropriedadeRuralTelefone) {
		this.listaPropriedadeRuralTelefone = listaPropriedadeRuralTelefone;
	}
	public void setListaParticipantePropriedadeRural(
			List<ParticipantePropriedadeRural> listaParticipantePropriedadeRural) {
		this.listaParticipantePropriedadeRural = listaParticipantePropriedadeRural;
	}
	public void setListaPropriedadeRuralHistorico(List<PropriedadeRuralHistorico> listaPropriedadeRuralHistorico) {
		this.listaPropriedadeRuralHistorico = listaPropriedadeRuralHistorico;
	}
	// LOG
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	/**
	 * Seleciona um endereco por enderecotipo, seguindo a ordem de prioridade. A � de ordem
	 * decrescente partindo do come�o da lista 'listaEnderecotipoPrioridade'.
	 * @param enderecos
	 * @param listaEnderecotipoPrioridade
	 * @return
	 */
	public Endereco filterEndereco(List<Endereco> enderecos, Enderecotipo...listaEnderecotipoPrioridade){
		if(enderecos == null || enderecos.isEmpty()) return null;
		if(listaEnderecotipoPrioridade == null) return enderecos.get(0); 
		for (Endereco endereco: enderecos){
			if(endereco.getEnderecotipo() == null || endereco.getEnderecotipo().getCdenderecotipo() == null)
				continue;
			for(Enderecotipo tipo: listaEnderecotipoPrioridade)
				if(tipo.equals(endereco.getEnderecotipo())) return endereco;
		}
		return enderecos.get(0);
	}
	
	@Transient
	public Endereco getEnderecoWithPrioridade(Enderecotipo ...listaEnderecotipoPrioridade) {
		if(Hibernate.isInitialized(listaEndereco) && this.listaEndereco != null && this.listaEndereco.size() > 0){
			
			List<Endereco> listaEnderecoAux = new ArrayList<Endereco>(this.listaEndereco);
			
			//A ordem para selecionar o endere�o deve ser a seguinte: "�nico", "Faturamento", "Cobran�a" e depois qualquer um.
			Collections.sort(listaEnderecoAux, new Comparator<Endereco>(){

				public int compare(Endereco o1, Endereco o2) {
					
					if(o1.getEnderecotipo() == null
							|| o1.getEnderecotipo().getCdenderecotipo() == null
							|| o2.getEnderecotipo() == null
							|| o2.getEnderecotipo().getCdenderecotipo() == null){
						return 0;
					}else{
					
						Integer cdenderecotipo = o1.getEnderecotipo().getCdenderecotipo();
						Integer cdenderecotipo2 = o2.getEnderecotipo().getCdenderecotipo();
						
						return cdenderecotipo.compareTo(cdenderecotipo2);
					}
				}
			});
			return filterEndereco(listaEnderecoAux, listaEnderecotipoPrioridade);
		}
		return null;
	}
	
	
}
