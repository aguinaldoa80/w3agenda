package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Orgaojuridico {
	
	public static final Integer ORGAO_TJ = 1;
	public static final Integer ORGAO_TRT = 2;
	public static final Integer ORGAO_TRF = 3;
	
	protected Integer cdorgaojuridico;
	protected String nome;
	protected String endereco;
	
	@Id
	public Integer getCdorgaojuridico() {
		return cdorgaojuridico;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setCdorgaojuridico(Integer cdorgaojuridico) {
		this.cdorgaojuridico = cdorgaojuridico;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
}
