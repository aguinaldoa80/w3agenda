package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@SequenceGenerator(name="sq_tarefaarquivo", sequenceName="sq_tarefaarquivo")
public class Tarefaarquivo {
	
	protected Integer cdtarefaarquivo;
	protected Tarefa tarefa;
	protected Arquivo arquivo;
	protected Date data = SinedDateUtils.currentDate();
	protected String observacao;
	
	@Id
	@GeneratedValue(generator="sq_tarefaarquivo", strategy=GenerationType.AUTO)
	public Integer getCdtarefaarquivo() {
		return cdtarefaarquivo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefa")	
	public Tarefa getTarefa() {
		return tarefa;
	}
	
	@Required
	@MaxLength(100)
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@Required
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdtarefaarquivo(Integer cdtarefaarquivo) {
		this.cdtarefaarquivo = cdtarefaarquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	
	
}
