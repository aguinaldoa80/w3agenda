package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.bean.enumeration.Tipobccomissionamento;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_comissionamento", sequenceName = "sq_comissionamento")
public class Comissionamento implements Log{

	protected Integer cdcomissionamento;
	protected String nome;
	protected Comissionamentotipo comissionamentotipo;
	protected Double valor;
	protected Double percentual;
	protected Integer quantidade;
	protected Boolean deduzirvalor;
	protected Boolean ativo;
	protected Tipobccomissionamento tipobccomissionamento = Tipobccomissionamento.VALOR_LIQUIDO;
	protected Criteriocomissionamento criteriocomissionamento;
	protected Boolean considerardiferencapagamento;
	protected Integer cdusuarioaltera;	
	protected Timestamp dtaltera;
	protected Double valordivisao;
	protected Double percentualdivisao;
	
	// comiss�o para vendas realizadas fora da faixa
	protected Double percentualFaixaVendedor;
	protected Double percentualFaixaVendedorPrincipal;
	protected Double percentualFaixaTotal;
	
	protected Boolean desempenhocumulativo;
	protected Boolean considerardiferencaentremetas;
	protected Boolean considerardiferencapreco;
	protected Boolean agencia; 
	protected Boolean calcularComissaoPorFaixa;
	
	protected List<Comissionamentopessoa> listaComissionamentopessoa;
	protected List<Comissionamentometa> listaComissionamentometa;
	protected List<Comissionamentofaixadesconto> listaComissionamentofaixadesconto;
	protected List<ComissionamentoFaixaMarkup> listaComissionamentoFaixaMarkup;
 	
	public Comissionamento() {}
	
	public Comissionamento(Integer cdcomissionamento) {
		this.cdcomissionamento = cdcomissionamento;
	}

	//TRANSIENT'S
	protected Boolean forma;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comissionamento")
	public Integer getCdcomissionamento() {
		return cdcomissionamento;
	}
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Tipo")
	public Comissionamentotipo getComissionamentotipo() {
		return comissionamentotipo;
	}
	public Double getValor() {
		return valor;
	}
	@MaxLength(10)
	public Double getPercentual() {
		return percentual;
	}
	@MaxLength(9)
	public Integer getQuantidade() {
		return quantidade;
	}	
	@DisplayName("Deduzir Valor")
	public Boolean getDeduzirvalor() {
		return deduzirvalor;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Colaborador")
	@OneToMany(mappedBy="comissionamento")
	public List<Comissionamentopessoa> getListaComissionamentopessoa() {
		return listaComissionamentopessoa;
	}
	@DisplayName("Meta")
	@OneToMany(mappedBy="comissionamento")
	public List<Comissionamentometa> getListaComissionamentometa() {
		return listaComissionamentometa;
	}
	@DisplayName("Faixa de Desconto na Venda")
	@OneToMany(mappedBy="comissionamento")
	public List<Comissionamentofaixadesconto> getListaComissionamentofaixadesconto() {
		return listaComissionamentofaixadesconto;
	}
	@DisplayName("Base de c�lculo")
	public Tipobccomissionamento getTipobccomissionamento() {
		return tipobccomissionamento;
	}
	@DisplayName("Acumulativo")
	public Boolean getDesempenhocumulativo() {
		return desempenhocumulativo;
	}
	
	public void setDesempenhocumulativo(Boolean desempenhocumulativo) {
		this.desempenhocumulativo = desempenhocumulativo;
	}
	public void setCdcomissionamento(Integer cdcomissionamento) {
		this.cdcomissionamento = cdcomissionamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setComissionamentotipo(Comissionamentotipo comissionamentotipo) {
		this.comissionamentotipo = comissionamentotipo;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setDeduzirvalor(Boolean deduzirvalor) {
		this.deduzirvalor = deduzirvalor;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaComissionamentopessoa(List<Comissionamentopessoa> listaComissionamentopessoa) {
		this.listaComissionamentopessoa = listaComissionamentopessoa;
	}
	public void setListaComissionamentometa(List<Comissionamentometa> listaComissionamentometa) {
		this.listaComissionamentometa = listaComissionamentometa;
	}
	public void setListaComissionamentofaixadesconto(List<Comissionamentofaixadesconto> listaComissionamentofaixadesconto) {
		this.listaComissionamentofaixadesconto = listaComissionamentofaixadesconto;
	}
	public void setTipobccomissionamento(Tipobccomissionamento tipobccomissionamento) {
		this.tipobccomissionamento = tipobccomissionamento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Boolean getForma() {
		return forma;
	}
	public void setForma(Boolean forma) {
		this.forma = forma;
	}

	@DisplayName("Considerar diferen�a no pagamento")
	public Boolean getConsiderardiferencapagamento() {
		return considerardiferencapagamento;
	}
	@DisplayName("Crit�rio")
	public Criteriocomissionamento getCriteriocomissionamento() {
		return criteriocomissionamento;
	}
	@DisplayName("Valor (Vendedor Principal)")
	public Double getValordivisao() {
		return valordivisao;
	}
	@MaxLength(10)
	@DisplayName("Percentual (Vendedor Principal)")
	public Double getPercentualdivisao() {
		return percentualdivisao;
	}
	@DisplayName("Considerar diferen�a entre metas")
	public Boolean getConsiderardiferencaentremetas() {
		return considerardiferencaentremetas;
	}
	@DisplayName("Considerar diferen�a de pre�os")
	public Boolean getConsiderardiferencapreco() {
		return considerardiferencapreco;
	}

	public void setConsiderardiferencapagamento(Boolean considerardiferencapagamento) {
		this.considerardiferencapagamento = considerardiferencapagamento;
	}
	public void setCriteriocomissionamento(Criteriocomissionamento criteriocomissionamento) {
		this.criteriocomissionamento = criteriocomissionamento;
	}
	public void setValordivisao(Double valordivisao) {
		this.valordivisao = valordivisao;
	}
	public void setPercentualdivisao(Double percentualdivisao) {
		this.percentualdivisao = percentualdivisao;
	}
	public void setConsiderardiferencaentremetas(Boolean considerardiferencaentremetas) {
		this.considerardiferencaentremetas = considerardiferencaentremetas;
	}
	public void setConsiderardiferencapreco(Boolean considerardiferencapreco) {
		this.considerardiferencapreco = considerardiferencapreco;
	}
	
	@DisplayName("Ag�ncia")
	public Boolean getAgencia() {
		return agencia;
	}
	
	public void setAgencia(Boolean agencia) {
		this.agencia = agencia;
	}
	
	@DisplayName("Comiss�o por Faixa")
	public Boolean getCalcularComissaoPorFaixa() {
		return calcularComissaoPorFaixa;
	}
	
	public void setCalcularComissaoPorFaixa(Boolean calcularComissaoPorFaixa) {
		this.calcularComissaoPorFaixa = calcularComissaoPorFaixa;
	}
	
	@DisplayName("% do Vendedor")
	public Double getPercentualFaixaVendedor() {
		return percentualFaixaVendedor;
	}
	
	public void setPercentualFaixaVendedor(Double percentualFaixaVendedor) {
		this.percentualFaixaVendedor = percentualFaixaVendedor;
	}
	
	@DisplayName("% do Vendedor Principal")
	public Double getPercentualFaixaVendedorPrincipal() {
		return percentualFaixaVendedorPrincipal;
	}
	
	public void setPercentualFaixaVendedorPrincipal(
			Double percentualFaixaVendedorPrincipal) {
		this.percentualFaixaVendedorPrincipal = percentualFaixaVendedorPrincipal;
	}
	
	@DisplayName("% Total")
	public Double getPercentualFaixaTotal() {
		return percentualFaixaTotal;
	}
	
	public void setPercentualFaixaTotal(Double percentualFaixaTotal) {
		this.percentualFaixaTotal = percentualFaixaTotal;
	}
	
	@DisplayName("Faixas de Comissionamento")
	@OneToMany(mappedBy="comissionamento")
	public List<ComissionamentoFaixaMarkup> getListaComissionamentoFaixaMarkup() {
		return listaComissionamentoFaixaMarkup;
	}
	
	public void setListaComissionamentoFaixaMarkup(
			List<ComissionamentoFaixaMarkup> listaComissionamentoFaixaMarkup) {
		this.listaComissionamentoFaixaMarkup = listaComissionamentoFaixaMarkup;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comissionamento other = (Comissionamento) obj;
		if (cdcomissionamento == null) {
			if (other.cdcomissionamento != null)
				return false;
		} else if (!cdcomissionamento.equals(other.cdcomissionamento))
			return false;
		return true;
	}
}
