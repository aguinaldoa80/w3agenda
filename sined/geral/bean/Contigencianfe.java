package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontigencia;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contigencianfe", sequenceName = "sq_contigencianfe")
public class Contigencianfe implements Log{

	protected Integer cdcontigencianfe;
	protected Tipocontigencia tipocontigencia;
	protected Uf uf;
	protected Timestamp dtentrada;
	protected Timestamp dtsaida;
	protected String justificativa;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contigencianfe")
	public Integer getCdcontigencianfe() {
		return cdcontigencianfe;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}

	@Required
	@DisplayName("Data de entrada")
	public Timestamp getDtentrada() {
		return dtentrada;
	}
	
	@Required
	@DisplayName("Data de sa�da")
	public Timestamp getDtsaida() {
		return dtsaida;
	}

	@Required
	@MaxLength(256)
	public String getJustificativa() {
		return justificativa;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	@Required
	@DisplayName("Tipo de conting�ncia")
	public Tipocontigencia getTipocontigencia() {
		return tipocontigencia;
	}
	
	public void setTipocontigencia(Tipocontigencia tipocontigencia) {
		this.tipocontigencia = tipocontigencia;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setDtsaida(Timestamp dtsaida) {
		this.dtsaida = dtsaida;
	}

	public void setCdcontigencianfe(Integer cdcontigencianfe) {
		this.cdcontigencianfe = cdcontigencianfe;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setDtentrada(Timestamp dtentrada) {
		this.dtentrada = dtentrada;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
}