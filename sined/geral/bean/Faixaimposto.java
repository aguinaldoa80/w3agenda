package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Cumulativotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Faixa de imposto")
@SequenceGenerator(name="sq_faixaimposto",sequenceName="sq_faixaimposto")
public class Faixaimposto implements Log, PermissaoClienteEmpresa {
	
	protected Integer cdfaixaimposto;
	protected String nome;
	protected Faixaimpostocontrole controle;
	protected Money valoraliquota;
	protected Money icmsst;
	protected Double mva;
	protected Money valorminimoincidir;
	protected Money valorminimocobrar;
	protected Uf uf;
	protected Municipio municipio;
	protected Uf ufdestino;
	protected Boolean cumulativo;
	protected Cumulativotipo cumulativotipo;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Boolean ativo = Boolean.TRUE;
	protected Codigotributacao codigotributacao;
	protected Boolean exibiremissaonfse;
	protected Money aliquotaretencao;
	protected Double basecalculopercentual;
	protected String observacao;
	
	protected Set<Faixaimpostoempresa> listaFaixaimpostoempresa = new ListSet<Faixaimpostoempresa>(Faixaimpostoempresa.class);
	protected Set<Faixaimpostocliente> listaFaixaimpostocliente = new ListSet<Faixaimpostocliente>(Faixaimpostocliente.class);
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_faixaimposto", strategy=GenerationType.AUTO)
	public Integer getCdfaixaimposto() {
		return cdfaixaimposto;
	}
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@Column(name="faixaimpostocontrole")
	public Faixaimpostocontrole getControle() {
		return controle;
	}
	@Required
	@DisplayName("Valor al�quota")
	public Money getValoraliquota() {
		return valoraliquota;
	}
	@DisplayName("Valor m�nimo para incidir")
	public Money getValorminimoincidir() {
		return valorminimoincidir;
	}
	@DisplayName("Valor m�nimo para cobrar")
	public Money getValorminimocobrar() {
		return valorminimocobrar;
	}
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public Boolean getCumulativo() {
		return cumulativo;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("C�digo de tributa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigotributacao")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}
	
	@DisplayName("Exibir na emiss�o de NFS-e")
	public Boolean getExibiremissaonfse() {
		return exibiremissaonfse;
	}
	
	@DisplayName("Tipo de cumulativo")
	public Cumulativotipo getCumulativotipo() {
		return cumulativotipo;
	}
	public void setCumulativotipo(Cumulativotipo cumulativotipo) {
		this.cumulativotipo = cumulativotipo;
	}
	
	public void setExibiremissaonfse(Boolean exibiremissaonfse) {
		this.exibiremissaonfse = exibiremissaonfse;
	}
	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}
	public void setCdfaixaimposto(Integer cdfaixaimposto) {
		this.cdfaixaimposto = cdfaixaimposto;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setControle(Faixaimpostocontrole controle) {
		this.controle = controle;
	}
	public void setValoraliquota(Money valoraliquota) {
		this.valoraliquota = valoraliquota;
	}
	public void setValorminimoincidir(Money valorminimoincidir) {
		this.valorminimoincidir = valorminimoincidir;
	}
	public void setValorminimocobrar(Money valorminimocobrar) {
		this.valorminimocobrar = valorminimocobrar;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setCumulativo(Boolean cumulativo) {
		this.cumulativo = cumulativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	@Transient
	public String getLocalidade(){
		if(getMunicipio() != null && getUf() != null){
			return getMunicipio().getNome() + "/" + getUf().getSigla();
		}else if(getUf() != null){
			return getUf().getSigla();
		}else{
			return "";
		}
	}
	@DisplayName("ICMS ST")
	public Money getIcmsst() {
		return icmsst;
	}
	@DisplayName("MVA")
	public Double getMva() {
		return mva;
	}
	@DisplayName("UF Destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufdestino")
	public Uf getUfdestino() {
		return ufdestino;
	}
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro de Custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setIcmsst(Money icmsst) {
		this.icmsst = icmsst;
	}
	public void setMva(Double mva) {
		this.mva = mva;
	}

	public void setUfdestino(Uf ufdestino) {
		this.ufdestino = ufdestino;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	@DisplayName("Al�quota de Reten��o")
	public Money getAliquotaretencao() {
		return aliquotaretencao;
	}
	
	@DisplayName("Empresas")
	@OneToMany(mappedBy="faixaimposto")
	public Set<Faixaimpostoempresa> getListaFaixaimpostoempresa() {
		return listaFaixaimpostoempresa;
	}
	
	@DisplayName("Clientes")
	@OneToMany(mappedBy="faixaimposto")
	public Set<Faixaimpostocliente> getListaFaixaimpostocliente() {
		return listaFaixaimpostocliente;
	}
	
	@DisplayName("% Base de c�lculo")
	public Double getBasecalculopercentual() {
		return basecalculopercentual;
	}
	
	public void setListaFaixaimpostocliente(
			Set<Faixaimpostocliente> listaFaixaimpostocliente) {
		this.listaFaixaimpostocliente = listaFaixaimpostocliente;
	}
	
	public void setBasecalculopercentual(Double basecalculopercentual) {
		this.basecalculopercentual = basecalculopercentual;
	}
	
	public void setListaFaixaimpostoempresa(
			Set<Faixaimpostoempresa> listaFaixaimpostoempresa) {
		this.listaFaixaimpostoempresa = listaFaixaimpostoempresa;
	}
	
	public void setAliquotaretencao(Money aliquotaretencao) {
		this.aliquotaretencao = aliquotaretencao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public String subQueryClienteEmpresa() {
		return "select faixaimpostoSubQueryClienteEmpresa.cdfaixaimposto " +
				"from Faixaimposto faixaimpostoSubQueryClienteEmpresa " +
				"left outer join faixaimpostoSubQueryClienteEmpresa.listaFaixaimpostocliente listaFaixaimpostoclienteSubQueryClienteEmpresa " +
				"left outer join listaFaixaimpostoclienteSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
}
