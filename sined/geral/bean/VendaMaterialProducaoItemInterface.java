package br.com.linkcom.sined.geral.bean;

import java.util.Date;



public interface VendaMaterialProducaoItemInterface {

	public Date getDtprazoentrega();
	public Date getPrazoentrega();
	public Loteestoque getLoteestoque();
	public Material getMaterial();
}
