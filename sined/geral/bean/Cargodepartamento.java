package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_cargodepartamento", sequenceName = "sq_cargodepartamento")
public class Cargodepartamento implements Log{

	protected Integer cdcargodepartamento;
	protected Cargo cargo;
	protected Departamento departamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargodepartamento")
	public Integer getCdcargodepartamento() {
		return cdcargodepartamento;
	}
	public void setCdcargodepartamento(Integer id) {
		this.cdcargodepartamento = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	@Required
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
