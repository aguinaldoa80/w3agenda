package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContaCompensacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.view.Vcontagerencial;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_contagerencial", sequenceName = "sq_contagerencial")
public class Contagerencial implements Log{

	protected Integer cdcontagerencial;
	protected String nome;
	protected Contagerencial contagerencialpai;
	protected Tipooperacao tipooperacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer item;	
	protected String formato = "00";	
	protected Vcontagerencial vcontagerencial;
	protected NaturezaContagerencial natureza;
	protected String observacao;
	protected Operacaocontabil operacaocontabil;
	protected Integer codigoalternativo;
	protected Boolean usadocalculocustooperacionalproducao;
	protected Boolean usadocalculocustocomercialproducao;
	protected Boolean contaretificadora;
	protected NaturezaContaCompensacaoEnum naturezaContaCompensacao;
	
	protected Set<Rateioitem> listaRateioitem;
	protected Set<Material> listaMaterial;
	
	public Contagerencial() {
	}
	
	public Contagerencial(String nome){
		this.nome = nome;
	}
	
	public Contagerencial(Integer cdcontagerencial){
		this.cdcontagerencial = cdcontagerencial;
	}
	
	public Contagerencial(Integer cdcontagerencial, String nome){
		this.cdcontagerencial = cdcontagerencial;
		this.nome = nome;
	}
	
	public Contagerencial(Integer cdcontagerencial, String nome, String identificador){
		this.cdcontagerencial = cdcontagerencial;
		this.nome = nome;
		
		Vcontagerencial vcontagerencial = new Vcontagerencial();
		vcontagerencial.setIdentificador(identificador);
		this.vcontagerencial = vcontagerencial;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contagerencial")
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Conta gerencial superior")
	@JoinColumn(name="cdcontagerencialpai")
	public Contagerencial getContagerencialpai() {
		return contagerencialpai;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Opera��o Contabil")
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	@MaxLength(9)
	@DisplayName("C�digo Alternativo")
	public Integer getCodigoalternativo() {
		return codigoalternativo;
	}
	
	@OneToMany(mappedBy="contagerencial")
	public Set<Rateioitem> getListaRateioitem() {
		return listaRateioitem;
	}
	
	@OneToMany(mappedBy="contagerencial")
	public Set<Material> getListaMaterial() {
		return listaMaterial;
	}
	
	public void setListaMaterial(Set<Material> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	
	public void setListaRateioitem(Set<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setContagerencialpai(Contagerencial contagerencialpai) {
		this.contagerencialpai = contagerencialpai;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	public void setCodigoalternativo(Integer codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	@DisplayName("Opera��o")
	@Required
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
	@MaxLength(9)
	@DisplayName("Item")
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	
	@Required
	@MaxLength(10)
	@DisplayName("Formato (Zeros)")
	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial", insertable=false, updatable=false)
	public Vcontagerencial getVcontagerencial() {
		return vcontagerencial;
	}
	
	public void setVcontagerencial(Vcontagerencial vcontagerencial) {
		this.vcontagerencial = vcontagerencial;
	}
	
	@Required
	public NaturezaContagerencial getNatureza() {
		return natureza;
	}
	
	@MaxLength(500)
	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setNatureza(NaturezaContagerencial natureza) {
		this.natureza = natureza;
	}
	
	@DisplayName("Custo")
	public Boolean getUsadocalculocustooperacionalproducao() {
		return usadocalculocustooperacionalproducao;
	}
	
	@DisplayName("Despesa")
	public Boolean getUsadocalculocustocomercialproducao() {
		return usadocalculocustocomercialproducao;
	}
	
	public void setUsadocalculocustocomercialproducao(Boolean usadocalculocustocomercialproducao) {
		this.usadocalculocustocomercialproducao = usadocalculocustocomercialproducao;
	}
	
	public void setUsadocalculocustooperacionalproducao(Boolean usadocalculocustooperacionalproducao) {
		this.usadocalculocustooperacionalproducao = usadocalculocustooperacionalproducao;
	}

	@DisplayName("Conta Retificadora")
	public Boolean getContaretificadora() {
		return contaretificadora;
	}
	
	public void setContaretificadora(Boolean contaretificadora) {
		this.contaretificadora = contaretificadora;
	}
	
	public NaturezaContaCompensacaoEnum getNaturezaContaCompensacao() {
		return naturezaContaCompensacao;
	}
	
	public void setNaturezaContaCompensacao(
			NaturezaContaCompensacaoEnum naturezaContaCompensacao) {
		this.naturezaContaCompensacao = naturezaContaCompensacao;
	}
	
	//========================= TRANSIENT ==================================//
	
	private Contagerencial pai;
	private Tipooperacao tipooperacao2;
	private List<Contagerencial> filhos = new ArrayList<Contagerencial>();
	private Boolean analiticacomregistros;
	private String descricaoInputPersonalizado;
	private Boolean adicionarCodigoalternativo;
	
	@Transient
	@DescriptionProperty(usingFields={"nome","vcontagerencial"})
	public String getDescricaoCombo(){
		return (getVcontagerencial() == null || getVcontagerencial().getArvorepai() == null || getVcontagerencial().getArvorepai().equals("") ? getNome() : (getNome()+" ("+getVcontagerencial().getArvorepai()+")"));
	}
	
	
	@Transient
	public String getDescricaoInputPersonalizado(){
		if(getVcontagerencial() != null){
			this.descricaoInputPersonalizado = getVcontagerencial().getIdentificador() + " - " + getNome() + (getVcontagerencial().getArvorepai() != null ? " (" + getVcontagerencial().getArvorepai() + ")" : "");
			if (Boolean.TRUE.equals(getAdicionarCodigoalternativo()) && getCodigoalternativo()!=null){
				this.descricaoInputPersonalizado += "<br><b>C�digo alt.</b>: " + getCodigoalternativo();
			}
		}
		return this.descricaoInputPersonalizado;
	}
	
	public void setDescricaoInputPersonalizado(String descricaoInputPersonalizado) {
		this.descricaoInputPersonalizado = descricaoInputPersonalizado;
	}
	
//	@SuppressWarnings("unused")
//	private Integer nivel;
//	@SuppressWarnings("unused")
//	private String identificador;
	
	@Transient
	public List<Contagerencial> getFilhos() {
		return filhos;
	}
	
	public void setFilhos(List<Contagerencial> filhos) {
		this.filhos = filhos;
	}

//	@DisplayName("N�vel")
//	@Transient
//	@MaxLength(2)
//	public Integer getNivel() {
//		Contagerencial contagerencial = this;
//		Integer retorno = 1;
//		while (contagerencial.getContagerencialpai() != null) {
//			contagerencial = contagerencial.getContagerencialpai();
//			retorno ++;
//		}
//		return retorno;
//	}
//
//	public void setNivel(Integer nivel) {
//		this.nivel = nivel;
//	}
//	
//	
//	@Transient
//	@DisplayName("Identificador")
//	@MaxLength(40)
//	public String getIdentificador() {
//		Contagerencial contagerencial = this;
//		String retorno = StringUtils.stringCheia(contagerencial.getItem() + "", "0", 2, false);
//		while (contagerencial.getContagerencialpai() != null) {
//			contagerencial = contagerencial.getContagerencialpai();
//			retorno = StringUtils.stringCheia(contagerencial.getItem() + ".","0",3,false) + retorno;
//		}
//		return retorno;
//	}
//	
//	public void setIdentificador(String identificador) {
//		this.identificador = identificador;
//	}
	
	
	@Transient
	@DisplayName("Conta gerencial superior")
	public Contagerencial getPai() {
		return pai;
	}
	
	public void setPai(Contagerencial pai) {
		this.pai = pai;
	}
	@Transient
	public Boolean getAnaliticacomregistros() {
		return analiticacomregistros;
	}
	
	public void setAnaliticacomregistros(Boolean analiticacomregistros) {
		this.analiticacomregistros = analiticacomregistros;
	}
	
	@Transient
	public Tipooperacao getTipooperacao2() {
		return tipooperacao2;
	}
	
	public void setTipooperacao2(Tipooperacao tipooperacao2) {
		this.tipooperacao2 = tipooperacao2;
	}
	
	@Transient
	public Boolean getAdicionarCodigoalternativo() {
		return adicionarCodigoalternativo;
	}

	public void setAdicionarCodigoalternativo(Boolean adicionarCodigoalternativo) {
		this.adicionarCodigoalternativo = adicionarCodigoalternativo;
	}

	@Transient	
	public String getDescricao() {
		String pais = "";
		Contagerencial pai = this.getContagerencialpai();
		if(!Hibernate.isInitialized(pai)){
			pai = null;
		}
		while(pai != null){
			if (!pais.equals("")) {
				pais = pai.getNome() + ">" + pais;
			} else {
				pais = pai.getNome();
			}
			pai = pai.getContagerencialpai();
			if(!Hibernate.isInitialized(pai)){
				pai = null;
			}
		}
		return this.getNome() + (!pais.equals("") ? " (" + pais + ")" : "");
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcontagerencial == null) ? 0 : cdcontagerencial.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Contagerencial other = (Contagerencial) obj;
		if (cdcontagerencial == null) {
			if (other.cdcontagerencial != null)
				return false;
		} else if (!cdcontagerencial.equals(other.cdcontagerencial))
			return false;
		return true;
	}
	
	

	
}
