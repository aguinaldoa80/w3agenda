package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Tipo de fornecimento")
@SequenceGenerator(name = "sq_fornecimentotipo", sequenceName = "sq_fornecimentotipo")
public class Fornecimentotipo implements Log{

	protected Integer cdfornecimentotipo;
	protected String nome;
	protected Boolean ativo = true;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;	
	
	public Fornecimentotipo() {}
	
	public Fornecimentotipo(Integer cdfornecimentotipo, String nome) {
		this.cdfornecimentotipo = cdfornecimentotipo;
		this.nome = nome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fornecimentotipo")
	public Integer getCdfornecimentotipo() {
		return cdfornecimentotipo;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCdfornecimentotipo(Integer cdfornecimentotipo) {
		this.cdfornecimentotipo = cdfornecimentotipo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Fornecimentotipo) {
			Fornecimentotipo fornecimentotipo = (Fornecimentotipo) obj;
			return fornecimentotipo.getCdfornecimentotipo().equals(this.getCdfornecimentotipo());
		}
		return super.equals(obj);
	}
	
	
}
