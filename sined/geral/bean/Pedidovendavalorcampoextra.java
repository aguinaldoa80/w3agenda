package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_pedidovendavalorcampoextra", sequenceName = "sq_pedidovendavalorcampoextra")
public class Pedidovendavalorcampoextra {

	protected Integer cdpedidovendavalorcampoextra;
	protected Pedidovenda pedidovenda;
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valor;

	@Id
	@GeneratedValue(generator = "sq_pedidovendavalorcampoextra", strategy = GenerationType.AUTO)
	public Integer getCdpedidovendavalorcampoextra() {
		return cdpedidovendavalorcampoextra;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcampoextrapedidovendatipo")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}

	@MaxLength(500)
	public String getValor() {
		return valor;
	}

	public void setCdpedidovendavalorcampoextra(
			Integer cdpedidovendavalorcampoextra) {
		this.cdpedidovendavalorcampoextra = cdpedidovendavalorcampoextra;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
