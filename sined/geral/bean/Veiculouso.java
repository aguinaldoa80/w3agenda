package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_veiculouso;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@DisplayName("Uso do ve�culo")
@Entity
@SequenceGenerator(name = "sq_veiculouso", sequenceName = "sq_veiculouso")
@JoinEmpresa("veiculouso.empresa")
public class Veiculouso implements Log{

	protected Integer cdveiculouso;
	protected Veiculo veiculo;
	protected Cliente cliente;
	protected Contato contato;
	protected Localarmazenagem localarmazenagem;
	protected Date dtiniciouso;
	protected Date dtfimuso;
	protected Integer kminicio;
	protected Integer kmfim;
	protected Double horimetroinicio;
	protected Double horimetrofim;
	protected Empresa empresa;
	protected Projeto projeto;
	protected Centrocusto centrocusto;
	protected Contagerencial contagerencial;
	protected Veiculousotipo veiculousotipo;
	protected Colaborador condutor;
	protected Date dtcancela;
	protected Escalahorario escalahorario;

	protected Aux_veiculouso aux_veiculouso;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	protected List<Veiculousoescolta> listaescolta = new ListSet<Veiculousoescolta>(Veiculousoescolta.class);
	protected List<Veiculousoparada> listaveiculousoparada = new ListSet<Veiculousoparada>(Veiculousoparada.class);
	protected List<Veiculousoitem> listaveiculousoitem = new ListSet<Veiculousoitem>(Veiculousoitem.class);
	protected List<VeiculoImplemento> listaVeiculoImplemento = new ListSet<VeiculoImplemento>(VeiculoImplemento.class);;

	/*TRANSIENT'S*/
	protected String kmTotal;
	protected String tempoTotal;
	protected Boolean isFeriado = false;
	protected Boolean isDomingo = false;
	protected String escala;
	protected Boolean possuiUsoVariosDias = false;
	protected String aula;
	protected Boolean isManutencao = false;
	protected Boolean isInspecao = false;
	protected Boolean isHorarioDisponivel = true;

	public Veiculouso(){
	}

	public Veiculouso(Escalahorario escalahorario){
		this.escalahorario = escalahorario;
	}
	
	public Veiculouso(Integer cdveiculouso){
		this.cdveiculouso = cdveiculouso;
	}
	
	public Veiculouso(Veiculo veiculo, Cliente cliente, Date dtiniciouso, Date dtfimuso, Escalahorario escalahorario){
		this.veiculo = veiculo;
		this.cliente = cliente;
		this.dtiniciouso = dtiniciouso;
		this.dtfimuso = dtfimuso;
		this.escalahorario = escalahorario;
	}
	
	public Veiculouso(Date dtiniciouso, Empresa empresa, Veiculo veiculo, Colaborador colaborador, Escalahorario escalahorario, Cliente cliente, String aula){
		this.dtiniciouso = dtiniciouso;
		this.empresa = empresa;
		this.veiculo = veiculo;
		this.escalahorario = escalahorario;
		this.cliente = cliente;
		this.aula = aula;
		this.condutor = colaborador;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculouso")
	public Integer getCdveiculouso() {
		return cdveiculouso;
	}
	
	@Required
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Solicitante")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitante")
	public Contato getContato() {
		return contato;
	}

	@DisplayName("Local de destino")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocaldestino")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	@Required
	@DisplayName("In�cio do uso")
	public Date getDtiniciouso() {
		return dtiniciouso;
	}

	@DisplayName("Fim do uso")
	public Date getDtfimuso() {
		return dtfimuso;
	}

	@MaxLength(9)
	@DisplayName("Km inicial")
	public Integer getKminicio() {
		return kminicio;
	}

	@MaxLength(9)
	@DisplayName("Km final")
	public Integer getKmfim() {
		return kmfim;
	}

	@DisplayName("Hor�metro inicial")
	public Double getHorimetroinicio() {
		return horimetroinicio;
	}

	@DisplayName("Hor�metro final")
	public Double getHorimetrofim() {
		return horimetrofim;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	@OneToMany(mappedBy="veiculouso")
	public List<Veiculousoescolta> getListaescolta() {
		return listaescolta;
	}
	
	@OneToMany(mappedBy="veiculouso")
	public List<Veiculousoparada> getListaveiculousoparada() {
		return listaveiculousoparada;
	}
	
	@OneToMany(mappedBy="veiculouso")
	public List<Veiculousoitem> getListaveiculousoitem() {
		return listaveiculousoitem;
	}
	@OneToMany(mappedBy="veiculouso")
	public List<VeiculoImplemento> getListaVeiculoImplemento() {
		return listaVeiculoImplemento;
	}
	public void setListaVeiculoImplemento(List<VeiculoImplemento> listaVeiculoImplemento) {
		this.listaVeiculoImplemento = listaVeiculoImplemento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso", insertable=false, updatable=false)
	public Aux_veiculouso getAux_veiculouso() {
		return aux_veiculouso;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcondutor")
	@DisplayName("Respons�vel")
	public Colaborador getCondutor() {
		return condutor;
	}
	
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@DisplayName("Tipo de uso") 
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculousotipo")
	public Veiculousotipo getVeiculousotipo() {
		return veiculousotipo;
	}
	
	public Date getDtcancela() {
		return dtcancela;
	}

	public void setDtcancela(Date dtcancela) {
		this.dtcancela = dtcancela;
	}

	public void setAux_veiculouso(Aux_veiculouso aux_veiculouso) {
		this.aux_veiculouso = aux_veiculouso;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public void setContato(Contato contato) {
		this.contato = contato;
	}


	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}


	public void setDtiniciouso(Date dtiniciouso) {
		this.dtiniciouso = dtiniciouso;
	}


	public void setDtfimuso(Date dtfimuso) {
		this.dtfimuso = dtfimuso;
	}


	public void setKminicio(Integer kminicio) {
		this.kminicio = kminicio;
	}


	public void setKmfim(Integer kmfim) {
		this.kmfim = kmfim;
	}

	public void setHorimetroinicio(Double horimetroinicio) {
		this.horimetroinicio = horimetroinicio;
	}

	public void setHorimetrofim(Double horimetrofim) {
		this.horimetrofim = horimetrofim;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}


	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	public void setListaescolta(List<Veiculousoescolta> listaescolta) {
		this.listaescolta = listaescolta;
	}
	
	public void setListaveiculousoparada(
			List<Veiculousoparada> listaveiculousoparada) {
		this.listaveiculousoparada = listaveiculousoparada;
	}
	
	public void setListaveiculousoitem(List<Veiculousoitem> listaveiculousoitem) {
		this.listaveiculousoitem = listaveiculousoitem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescalahorario")
	@DisplayName("Escala Hor�rio")
	public Escalahorario getEscalahorario() {
		return escalahorario;
	}
	
	public void setEscalahorario(Escalahorario escalahorario) {
		this.escalahorario = escalahorario;
	}
	public void setCdveiculouso(Integer cdveiculouso) {
		this.cdveiculouso = cdveiculouso;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
	public void setCondutor(Colaborador condutor) {
		this.condutor = condutor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setVeiculousotipo(Veiculousotipo veiculousotipo) {
		this.veiculousotipo = veiculousotipo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Veiculouso)
			return this.cdveiculouso.equals(((Veiculouso)obj).getCdveiculouso());
		return super.equals(obj);
	}
	
	//=================== TRANSIENT's
	
	
	@Transient
	public String getKmTotal(){
		if(this.kminicio != null && this.kmfim != null)
			return this.kmfim - this.kminicio+"";
		return "";
	}

	@Transient
	@DisplayName("Tempo total(h)")
	public String getTempoTotal(){
		if(this.dtiniciouso != null && this.dtfimuso != null){
			if(this.escalahorario != null && this.escalahorario.getCdescalahorario() != null){
				int dias = SinedDateUtils.diferencaDias(this.dtfimuso, this.dtiniciouso);
				Locale locale = new Locale("pt", "BR");
				NumberFormat nf = NumberFormat.getInstance(locale);
				nf.setMaximumFractionDigits(1);
				return nf.format((((double) this.escalahorario.getHorafim().getTime() - this.escalahorario.getHorainicio().getTime())/3600000) * (dias == 0 ? 1 : dias)) +"";
			}else
				return (this.dtfimuso.getTime() - this.dtiniciouso.getTime())/3600000+24+"";
		}
		return "";
	}
	
	@Transient
	@DisplayName("Hor�rio")
	public String getEscala() {
		return escala;
	}
	@Transient
	public Boolean getIsFeriado() {
		return isFeriado;
	}
	@Transient
	public Boolean getIsDomingo() {
		return isDomingo;
	}

	@Transient
	public Boolean getPossuiUsoVariosDias() {
		return possuiUsoVariosDias;
	}
	
	public void setIsFeriado(Boolean isFeriado) {
		this.isFeriado = isFeriado;
	}

	public void setIsDomingo(Boolean isDomingo) {
		this.isDomingo = isDomingo;
	}

	public void setEscala(String escala) {
		this.escala = escala;
	}
	
	public void setTempoTotal(String tempoTotal) {
		this.tempoTotal = tempoTotal;
	}
	
	public void setKmTotal(String kmTotal) {
		this.kmTotal = kmTotal;
	}
	
	public void setPossuiUsoVariosDias(Boolean possuiUsoVariosDias) {
		this.possuiUsoVariosDias = possuiUsoVariosDias;
	}
	
	@Transient
	public String getAula() {
		return aula;
	}
	
	public void setAula(String aula) {
		this.aula = aula;
	}
	@Transient
	public Boolean getIsManutencao() {
		return isManutencao;
	}

	public void setIsManutencao(Boolean isManutencao) {
		this.isManutencao = isManutencao;
	}
	@Transient
	public Boolean getIsInspecao() {
		return isInspecao;
	}

	public void setIsInspecao(Boolean isInspecao) {
		this.isInspecao = isInspecao;
	}
	
	@Transient
	public Boolean getIsHorarioDisponivel() {
		return isHorarioDisponivel;
	}
	
	public void setIsHorarioDisponivel(Boolean isHorarioDisponivel) {
		this.isHorarioDisponivel = isHorarioDisponivel;
	}
	
}