package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;


@Entity
@SequenceGenerator(name = "sq_bandarederegra", sequenceName = "sq_bandarederegra")
public class Bandarederegra{
	
	protected Integer cdbandarederegra;
	protected Bandaredecomercial bandaredecomercial;
	protected Bandarede bandaredeupload;
	protected Bandarede bandarededownload;
	protected DiaSemana diasemanainicio;
	protected DiaSemana diasemanafim;
	protected Hora horainicio;
	protected Hora horafim;
	protected Boolean feriado;
	protected Boolean ativo = true;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bandarederegra")
	public Integer getCdbandarederegra() {
		return cdbandarederegra;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdbandaredecomercial")
	public Bandaredecomercial getBandaredecomercial() {
		return bandaredecomercial;
	}

	@Required
	@DisplayName("Banda upload")
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdbandaredeupload")
	public Bandarede getBandaredeupload() {
		return bandaredeupload;
	}

	@Required
	@DisplayName("Banda download")
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdbandarededownload")
	public Bandarede getBandarededownload() {
		return bandarededownload;
	}

	@Required
	@DisplayName("Dia da semana de in�cio")
	public DiaSemana getDiasemanainicio() {
		return diasemanainicio;
	}

	@Required
	@DisplayName("Dia de semana de fim")
	public DiaSemana getDiasemanafim() {
		return diasemanafim;
	}

	@Required
	@DisplayName("Hora in�cio")
	public Hora getHorainicio() {
		return horainicio;
	}

	@Required
	@DisplayName("Hora fim")
	public Hora getHorafim() {
		return horafim;
	}

	public Boolean getFeriado() {
		return feriado;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdbandarederegra(Integer cdbandarederegra) {
		this.cdbandarederegra = cdbandarederegra;
	}

	public void setBandaredecomercial(Bandaredecomercial bandaredecomercial) {
		this.bandaredecomercial = bandaredecomercial;
	}

	public void setBandaredeupload(Bandarede bandaredeupload) {
		this.bandaredeupload = bandaredeupload;
	}

	public void setBandarededownload(Bandarede bandarededownload) {
		this.bandarededownload = bandarededownload;
	}

	public void setDiasemanainicio(DiaSemana diasemanainicio) {
		this.diasemanainicio = diasemanainicio;
	}

	public void setDiasemanafim(DiaSemana diasemanafim) {
		this.diasemanafim = diasemanafim;
	}

	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}

	public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}

	public void setFeriado(Boolean feriado) {
		this.feriado = feriado;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
