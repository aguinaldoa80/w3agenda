package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_emporiumfiscaistributacao", sequenceName = "sq_emporiumfiscaistributacao")
public class Emporiumfiscaistributacao {

	protected Integer cdemporiumfiscaistributacao;
	protected Date dtmovimento;
	protected String pdv;
	protected String legenda;
	protected String base;
	protected String imposto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumfiscaistributacao")
	public Integer getCdemporiumfiscaistributacao() {
		return cdemporiumfiscaistributacao;
	}

	public Date getDtmovimento() {
		return dtmovimento;
	}

	public String getPdv() {
		return pdv;
	}

	public String getLegenda() {
		return legenda;
	}

	public String getBase() {
		return base;
	}

	public String getImposto() {
		return imposto;
	}

	public void setCdemporiumfiscaistributacao(Integer cdemporiumfiscaistributacao) {
		this.cdemporiumfiscaistributacao = cdemporiumfiscaistributacao;
	}

	public void setDtmovimento(Date dtmovimento) {
		this.dtmovimento = dtmovimento;
	}

	public void setPdv(String pdv) {
		this.pdv = pdv;
	}

	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public void setImposto(String imposto) {
		this.imposto = imposto;
	}
	
}
