package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Contato")
@SequenceGenerator(name = "sq_contacrmcontato", sequenceName = "sq_contacrmcontato")
public class Contacrmcontato implements Log{
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	protected Integer cdcontacrmcontato;
	protected String nome;
	protected String logradouro;
	protected String numero;
	protected String complemento;
	protected String bairro;
	protected Cep cep;
	
	protected Sexo sexo;
	protected Uf uf;
	protected Municipio municipio;
	
	protected Contacrm contacrm;
	protected Lead lead;
	protected Set<Contacrmcontatoemail> listcontacrmcontatoemail;
	protected Set<Contacrmcontatofone> listcontacrmcontatofone;
	protected List<Campanhacontato> listcampanhacontato;
	
//	Transients
	protected Integer indexlista;
	protected String telefones;
	protected String listaEmail;
	protected String sexoNome;
	protected String telefonesSeparadosPorBarra;
	
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrmcontato")
	public Integer getCdcontacrmcontato() {
		return cdcontacrmcontato;
	}

	@Required	
	@MaxLength(80)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@MaxLength(100)
	@DisplayName("Logradouro")
	public String getLogradouro() {
		return logradouro;
	}

	@MaxLength(5)
	@DisplayName("N�mero")
	public String getNumero() {
		return numero;
	}

	@MaxLength(50)
	@DisplayName("Complemento")
	public String getComplemento() {
		return complemento;
	}

	@MaxLength(50)
	@DisplayName("Bairro")
	public String getBairro() {
		return bairro;
	}

	@MaxLength(50)
	@DisplayName("CEP")
	public Cep getCep() {
		return cep;
	}

	@DisplayName("Sexo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")
	public Sexo getSexo() {
		return sexo;
	}

	@Transient
	@DisplayName("Uf")
	public Uf getUf() {
		return uf;
	}

	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}

	
	@DisplayName("Conta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrm")
	public Contacrm getContacrm() {
		return contacrm;
	}

	@DisplayName("Lead")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlead")
	public Lead getLead() {
		return lead;
	}
	
	@Email
	@DisplayName("E-mail")
	@OneToMany(mappedBy="contacrmcontato")
	public Set<Contacrmcontatoemail> getListcontacrmcontatoemail() {
		return listcontacrmcontatoemail;
	}
	
	@DisplayName("Telefone")
	@OneToMany(mappedBy="contacrmcontato")
	public Set<Contacrmcontatofone> getListcontacrmcontatofone() {
		return listcontacrmcontatofone;
	}
	
	@OneToMany(mappedBy="contacrmcontato")
	public List<Campanhacontato> getListcampanhacontato() {
		return listcampanhacontato;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	
	@Transient
	public String getTelefones() {
		return getTelefones("<br>");
	}
	
	private String getTelefones(String separador){
		Set<Contacrmcontatofone> lista = getListcontacrmcontatofone();
		String string = "";
		String linha = "";
		if (lista != null && lista.size() > 0) {
			for (Contacrmcontatofone telefone : lista) {
				if (telefone.getTelefone() != null && !telefone.getTelefone().equals("")) {
					linha += telefone.getTelefone().toString();
					if (telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getNome() != null) {
						linha += "(" + telefone.getTelefonetipo().getNome() + ")";
					} 
					string += linha;
					if (!linha.equals("")) {
						string += separador;
						linha = "";
					} 
				}
			}
			
			if(string != null && string.length() > 4){
				return string.substring(0, string.length()-4);
			} else return string;
		}
		return string;
	}
	
	
	@Transient
	@DisplayName("Lista E-mails")
	public String getListaEmail(){
		StringBuilder sb = new StringBuilder();
		Set<Contacrmcontatoemail> lista = getListcontacrmcontatoemail();
		if(lista == null){
			return "";
		}
		
		for (Contacrmcontatoemail email : lista) {
			sb.append(email.getEmail().toString() + " ");
			sb.append("<br>");
		}
		
		return sb.toString();
	}
	
	@Transient
	public String getListaEmailRTF(){
		StringBuilder sb = new StringBuilder();
		Set<Contacrmcontatoemail> lista = getListcontacrmcontatoemail();
		if(lista == null){
			return "";
		}
		
		for (Contacrmcontatoemail email : lista) {
			sb.append(email.getEmail().toString() + "\n");
		}
		
		return sb.toString();
	}
	
	@Transient
	public String getSexoNome() {
		return sexoNome;
	}
	
	
	
	
	
	public void setListaEmail(String listaEmail) {
		this.listaEmail = listaEmail;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public void setSexoNome(String sexoNome) {
		this.sexoNome = sexoNome;
	}
	
	
	

	public void setCdcontacrmcontato(Integer cdcontacrmcontato) {
		this.cdcontacrmcontato = cdcontacrmcontato;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(Cep cep) {
		this.cep = cep;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
	public void setListcontacrmcontatoemail(
			Set<Contacrmcontatoemail> listcontacrmcontatoemail) {
		this.listcontacrmcontatoemail = listcontacrmcontatoemail;
	}
	
	public void setListcontacrmcontatofone(
			Set<Contacrmcontatofone> listcontacrmcontatofone) {
		this.listcontacrmcontatofone = listcontacrmcontatofone;
	}
	
	public void setListcampanhacontato(List<Campanhacontato> listcampanhacontato) {
		this.listcampanhacontato = listcampanhacontato;
	}
	
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	
	

	
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}


	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcontacrmcontato == null) ? 0 : cdcontacrmcontato
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Contacrmcontato other = (Contacrmcontato) obj;
		if (cdcontacrmcontato == null) {
			if (other.cdcontacrmcontato != null)
				return false;
		} else if (!cdcontacrmcontato.equals(other.cdcontacrmcontato))
			return false;
		return true;
	}
	
	@Transient
	public String getTelefoneComTipo(){
		if(this.getListcontacrmcontatofone() != null && !this.getListcontacrmcontatofone().isEmpty()){
			String telefones = "";
			for(Contacrmcontatofone item : this.getListcontacrmcontatofone()){
				telefones += item.getTelefone() + " (" + item.getTelefonetipo().getNome() + ")";
			}
			return telefones;
		}
		return "";
	}
	
	@Transient
	public String getTelefoneComTipoQuebraDeLinha(){
		if(this.getListcontacrmcontatofone() != null && !this.getListcontacrmcontatofone().isEmpty()){
			String telefones = "";
			for(Contacrmcontatofone item : this.getListcontacrmcontatofone()){
				telefones += item.getTelefone() + " (" + item.getTelefonetipo().getNome() + ")\n";
			}
			return telefones;
		}
		return "";
	}
	
	@Transient
	public String getEnderecoForCombo(){
		String logradouroCompleto = "";
		logradouroCompleto += this.logradouro != null && !this.logradouro.trim().equals("") ? this.logradouro : "";
		logradouroCompleto += this.numero != null && !this.numero.trim().equals("") ? ", " + this.numero : "";
		logradouroCompleto += this.complemento != null && !this.complemento.trim().equals("") ? ", " + this.complemento : "";
		logradouroCompleto += this.bairro != null && !this.bairro.trim().equals("") ? ", " + this.bairro : "";
		logradouroCompleto += this.municipio != null ? ", " + this.municipio.getNome() : "";
		logradouroCompleto += this.municipio != null && this.municipio.getUf() != null ? "/ " + this.municipio.getUf().getSigla() : "";
		logradouroCompleto += this.cep != null ? " - CEP: " + this.cep.toString() : "";
		return logradouroCompleto;
	}
	
	@Transient
	public String getTelefonesSeparadosPorBarra() {
		telefonesSeparadosPorBarra = getTelefones(" / ");
		return telefonesSeparadosPorBarra;
	}
	public void setTelefonesSeparadosPorBarra(String telefonesSeparadosPorBarra) {
		this.telefonesSeparadosPorBarra = telefonesSeparadosPorBarra;
	}
	
}
