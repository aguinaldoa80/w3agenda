package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;


@Entity
@SequenceGenerator(name = "sq_escalahorario", sequenceName = "sq_escalahorario")
public class Escalahorario {

	protected Integer cdescalahorario;
	protected Escala escala;
	protected Hora horainicio;
	protected Hora horafim;
	protected DiaSemana diasemana;
	
	protected String descriptionProperty;
	protected String horainiciostring;
	protected String horafimstring;
	protected String horario;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_escalahorario")
	public Integer getCdescalahorario() {
		return cdescalahorario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescala")
	public Escala getEscala() {
		return escala;
	}
	@Required
	@DisplayName("Hora in�cio")
	public Hora getHorainicio() {
		return horainicio;
	}
	@Required
	@DisplayName("Hora fim")
	public Hora getHorafim() {
		return horafim;
	}
	@DisplayName("Dia da semana")
	public DiaSemana getDiasemana() {
		return diasemana;
	}
	public void setDiasemana(DiaSemana diasemana) {
		this.diasemana = diasemana;
	}
	public void setCdescalahorario(Integer cdescalahorario) {
		this.cdescalahorario = cdescalahorario;
	}
	public void setEscala(Escala escala) {
		this.escala = escala;
	}
	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}
	public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}

	@DescriptionProperty
	@Transient
	public String getDescriptionProperty(){
		return (this.diasemana != null ? this.diasemana.getNome()+" - " : "") + (this.horainicio != null ? this.horainicio.toString() : this.horainiciostring) +" - "+(this.horafim != null ? this.horafim.toString() : this.horafimstring);
	}
	
	@Transient
	public String getHorario(){
		return (this.horainicio != null ? this.horainicio.toString() : this.horainiciostring) +" - "+(this.horafim != null ? this.horafim.toString() : this.horafimstring);
	}
	
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	public void setDescriptionProperty(String descriptionProperty) {
		this.descriptionProperty = descriptionProperty;
	}
	
	@Transient
	public String getHorainiciostring() {
		return horainiciostring;
	}
	
	@Transient
	public String getHorafimstring() {
		return horafimstring;
	}
	
	public void setHorafimstring(String horafimstring) {
		this.horafimstring = horafimstring;
	}
	
	public void setHorainiciostring(String horainiciostring) {
		this.horainiciostring = horainiciostring;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Escalahorario)
			return this.cdescalahorario.equals(((Escalahorario)obj).getCdescalahorario());
		return super.equals(obj);
	}	
}
