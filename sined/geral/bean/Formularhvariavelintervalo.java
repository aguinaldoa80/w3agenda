package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_formularhvariavelintervalo", sequenceName="sq_formularhvariavelintervalo")
public class Formularhvariavelintervalo {
	
	protected Integer cdformularhvariavelintervalo;
	protected Formularhvariavel formularhvariavel;
	protected Double resultadoate;
	protected Double aliquota;
	protected Integer cdusuarioaltera;
	protected Date dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_formularhvariavelintervalo",strategy=GenerationType.AUTO)
	public Integer getCdformularhvariavelintervalo() {
		return cdformularhvariavelintervalo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdformularhvariavel")
	public Formularhvariavel getFormularhvariavel() {
		return formularhvariavel;
	}
	public Double getResultadoate() {
		return resultadoate;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Date getDtaltera() {
		return dtaltera;
	}	
	public Double getAliquota() {
		return aliquota;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public void setCdformularhvariavelintervalo(Integer cdformularhvariavelintervalo) {
		this.cdformularhvariavelintervalo = cdformularhvariavelintervalo;
	}
	public void setFormularhvariavel(Formularhvariavel formularhvariavel) {
		this.formularhvariavel = formularhvariavel;
	}
	public void setResultadoate(Double resultadoate) {
		this.resultadoate = resultadoate;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Date dtaltera) {
		this.dtaltera = dtaltera;
	}
}
