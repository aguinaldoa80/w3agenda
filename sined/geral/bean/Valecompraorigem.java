package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_valecompraorigem", sequenceName = "sq_valecompraorigem")
public class Valecompraorigem {

	protected Integer cdvalecompraorigem;
	protected Valecompra valecompra;
	protected Documento documento;
	protected Venda venda;
	protected Pedidovenda pedidovenda;
	protected Coleta coleta;
	protected Garantiareforma garantiareforma;
	protected Materialdevolucao materialDevolucao;
	////////////////////////|
	/// INICIO DOS GET   ///|
	////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_valecompraorigem")
	public Integer getCdvalecompraorigem() {
		return cdvalecompraorigem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvalecompra")
	public Valecompra getValecompra() {
		return valecompra;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareforma")
	public Garantiareforma getGarantiareforma() {
		return garantiareforma;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialdevolucao")
	public Materialdevolucao getMaterialDevolucao() {
		return materialDevolucao;
	}
	////////////////////////|
	/// INICIO DOS SET   ///|
	////////////////////////|
	public void setCdvalecompraorigem(Integer cdvalecompraorigem) {
		this.cdvalecompraorigem = cdvalecompraorigem;
	}

	public void setValecompra(Valecompra valecompra) {
		this.valecompra = valecompra;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	public void setGarantiareforma(Garantiareforma garantiareforma) {
		this.garantiareforma = garantiareforma;
	}
	
	public void setMaterialDevolucao(Materialdevolucao materialDevolucao) {
		this.materialDevolucao = materialDevolucao;
	}
}