package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Notafiscalprodutocorrecaosituacao;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutocorrecao",sequenceName="sq_notafiscalprodutocorrecao")
public class Notafiscalprodutocorrecao {
	
	protected Integer cdnotafiscalprodutocorrecao;
	protected Notafiscalproduto notafiscalproduto;
	protected Timestamp data;
	protected String texto;
	protected Notafiscalprodutocorrecaosituacao notafiscalprodutocorrecaosituacao;
	protected Arquivo arquivoenvio;
	protected Arquivo arquivoretorno;
	protected Arquivo arquivoxml;
	protected Boolean enviando;
	protected String motivo;
	protected Integer sequencialevento;
	
	// TRANSIENTE
	protected Empresa empresa;
	
	@Id
	@GeneratedValue(generator="sq_notafiscalprodutocorrecao", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutocorrecao() {
		return cdnotafiscalprodutocorrecao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	public Timestamp getData() {
		return data;
	}

	@DisplayName("Corre��o")
	public String getTexto() {
		return texto;
	}

	@DisplayName("Situa��o")
	public Notafiscalprodutocorrecaosituacao getNotafiscalprodutocorrecaosituacao() {
		return notafiscalprodutocorrecaosituacao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivoenvio")
	public Arquivo getArquivoenvio() {
		return arquivoenvio;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivoretorno")
	public Arquivo getArquivoretorno() {
		return arquivoretorno;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	
	public Boolean getEnviando() {
		return enviando;
	}
	
	public String getMotivo() {
		return motivo;
	}
	
	public Integer getSequencialevento() {
		return sequencialevento;
	}
	
	public void setSequencialevento(Integer sequencialevento) {
		this.sequencialevento = sequencialevento;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	public void setEnviando(Boolean enviando) {
		this.enviando = enviando;
	}

	public void setArquivoenvio(Arquivo arquivoenvio) {
		this.arquivoenvio = arquivoenvio;
	}

	public void setArquivoretorno(Arquivo arquivoretorno) {
		this.arquivoretorno = arquivoretorno;
	}

	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setNotafiscalprodutocorrecaosituacao(
			Notafiscalprodutocorrecaosituacao notafiscalprodutocorrecaosituacao) {
		this.notafiscalprodutocorrecaosituacao = notafiscalprodutocorrecaosituacao;
	}

	public void setCdnotafiscalprodutocorrecao(Integer cdnotafiscalprodutocorrecao) {
		this.cdnotafiscalprodutocorrecao = cdnotafiscalprodutocorrecao;
	}

	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
		
}