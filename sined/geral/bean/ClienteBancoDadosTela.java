package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_clientebancodadostela", sequenceName = "sq_clientebancodadostela")
public class ClienteBancoDadosTela {

	protected Integer cdclientebancodadostela;
	protected ClienteBancoDados clienteBancoDados;
	protected Tela tela;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientebancodadostela")
	public Integer getCdclientebancodadostela() {
		return cdclientebancodadostela;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclientebancodados")
	public ClienteBancoDados getClienteBancoDados() {
		return clienteBancoDados;
	}
	
	@Required
	@DisplayName("Tela")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtela")
	public Tela getTela() {
		return tela;
	}

	public void setCdclientebancodadostela(Integer cdclientebancodadostela) {
		this.cdclientebancodadostela = cdclientebancodadostela;
	}

	public void setClienteBancoDados(ClienteBancoDados clienteBancoDados) {
		this.clienteBancoDados = clienteBancoDados;
	}

	public void setTela(Tela tela) {
		this.tela = tela;
	}
	
}
