package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.view.Vmaterialcategoria;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_materialcategoria", sequenceName = "sq_materialcategoria")
public class Materialcategoria implements Log{

	protected Integer cdmaterialcategoria;
	protected Materialcategoria materialcategoriapai;
	protected String descricao;
	protected Integer item;
	protected Boolean ativo = true;
	protected Vmaterialcategoria vmaterialcategoria;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected String formato = "00";
	protected boolean criarCategoriaSuperior = false;
	
	//Transientes
	private List<Materialcategoria> filhos;
	private String identificador;
	
	public Materialcategoria() {
	}
	public Materialcategoria(String descricao) {
		this.descricao = descricao;
	}
	public Materialcategoria(Integer cdmaterialcategoria) {
		this.cdmaterialcategoria = cdmaterialcategoria;
	}

	@Id
	@GeneratedValue(generator = "sq_materialcategoria", strategy = GenerationType.AUTO)
	public Integer getCdmaterialcategoria() {
		return cdmaterialcategoria;
	}
	@JoinColumn(name="cdmaterialcategoriapai")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Categoria pai")
	public Materialcategoria getMaterialcategoriapai() {
		return materialcategoriapai;
	}

	@Required
	@DescriptionProperty
	@MaxLength(50)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialcategoria", insertable=false, updatable=false)
	public Vmaterialcategoria getVmaterialcategoria() {
		return vmaterialcategoria;
	}
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public void setVmaterialcategoria(Vmaterialcategoria vmaterialcategoria) {
		this.vmaterialcategoria = vmaterialcategoria;
	}
	public void setCdmaterialcategoria(Integer cdmaterialcategoria) {
		this.cdmaterialcategoria = cdmaterialcategoria;
	}
	public void setMaterialcategoriapai(Materialcategoria materialcategoriapai) {
		this.materialcategoriapai = materialcategoriapai;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Required
	@MaxLength(4)
	@DisplayName("Formato (Zeros)")
	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	@Transient
	public List<Materialcategoria> getFilhos() {
		return filhos;
	}
	
	public void setFilhos(List<Materialcategoria> filhos) {
		this.filhos = filhos;
	}
	
	@Transient
	public String getDescriptionAutocomplete(){
		if(this.getVmaterialcategoria() != null){
			return this.getVmaterialcategoria().getIdentificador() != null && !this.getVmaterialcategoria().getIdentificador().equals("") ? 
					this.getVmaterialcategoria().getIdentificador() + " - " + this.descricao : this.descricao;
		} else if(StringUtils.isNotBlank(getIdentificador())){
			return getIdentificador() + " - " + this.descricao;
		}else {
			return this.descricao;
		}		
	}
	
	@Transient
	public String getIdentificador() {
		if(identificador == null && vmaterialcategoria != null && vmaterialcategoria.getIdentificador() != null){
			identificador = vmaterialcategoria.getIdentificador();
		}
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	@Transient
	public String getDescricaoAutocompleteTreeView(){	
		
		if(this.getVmaterialcategoria() != null){
			if(getVmaterialcategoria().getIdentificador() != null && !getVmaterialcategoria().getIdentificador().equals("") && getVmaterialcategoria().getArvorepai() != null){
				return getVmaterialcategoria().getIdentificador() + " - " + descricao + " (" + getVmaterialcategoria().getArvorepai() + ")";
			}else if (getVmaterialcategoria().getIdentificador() != null && !getVmaterialcategoria().getIdentificador().equals("")){
				return getVmaterialcategoria().getIdentificador() + " - " + descricao;
			}else if(getVmaterialcategoria().getArvorepai() != null){
				return descricao + " (" + getVmaterialcategoria().getArvorepai() + ")";
			}else {
				return descricao;
			}
		} else if(StringUtils.isNotBlank(getIdentificador())){
			return getIdentificador() + " - " + descricao;
		}else {
			return descricao;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Materialcategoria other = (Materialcategoria) obj;
		if (cdmaterialcategoria == null) {
			if (other.cdmaterialcategoria != null)
				return false;
		} else if (!cdmaterialcategoria.equals(other.cdmaterialcategoria))
			return false;
		return true;
	}
	
	public boolean equalsIdentificador(Object obj) {
		if (obj instanceof Materialcategoria){
			Materialcategoria materialcategoriaObj = (Materialcategoria) obj;
			String identificadorObj = materialcategoriaObj.getIdentificador();
			String identificador = this.getIdentificador();			
			if (identificadorObj!=null && identificador!=null){
				return identificadorObj.equals(identificador) || identificadorObj.startsWith(identificador + ".");
			}
		}
		
		return false;
	}
	
	@Transient
	public boolean isCriarCategoriaSuperior() {
		return criarCategoriaSuperior;
	}
	
	public void setCriarCategoriaSuperior(boolean criarCategoriaSuperior) {
		this.criarCategoriaSuperior = criarCategoriaSuperior;
	}	
}