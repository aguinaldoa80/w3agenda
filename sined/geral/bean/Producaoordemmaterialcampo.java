package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_producaoordemmaterialcampo",sequenceName="sq_producaoordemmaterialcampo")
public class Producaoordemmaterialcampo {
	
	protected Integer cdproducaoordemmaterialcampo;
	protected Producaoordemmaterial producaoordemmaterial;
	protected Producaoetapanomecampo producaoetapanomecampo;
	protected String valor;
	protected Boolean valorboleano;
	
	@Id
	@GeneratedValue(generator="sq_producaoordemmaterialcampo",strategy=GenerationType.AUTO)
	public Integer getCdproducaoordemmaterialcampo() {
		return cdproducaoordemmaterialcampo;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordemmaterial")
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapanomecampo")
	public Producaoetapanomecampo getProducaoetapanomecampo() {
		return producaoetapanomecampo;
	}
	@MaxLength(50)
	public String getValor() {
		return valor;
	}
	public Boolean getValorboleano() {
		return valorboleano;
	}
	
	public void setCdproducaoordemmaterialcampo(Integer cdproducaoordemmaterialcampo) {
		this.cdproducaoordemmaterialcampo = cdproducaoordemmaterialcampo;
	}
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	public void setProducaoetapanomecampo(Producaoetapanomecampo producaoetapanomecampo) {
		this.producaoetapanomecampo = producaoetapanomecampo;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public void setValorboleano(Boolean valorboleano) {
		this.valorboleano = valorboleano;
	}
}
