package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Bdicalculotipo {
	
	public static final Bdicalculotipo PERCENTUAL_SIMPLES = new Bdicalculotipo(1, "Percentual simples");
	public static final Bdicalculotipo PERCENTUAL_INVERTIDO = new Bdicalculotipo(2, "Percentual invertido");

	protected Integer cdbdicalculotipo;
	protected String nome;
	
	public Bdicalculotipo() {
	}
	
	public Bdicalculotipo(Integer cdbdicalculotipo, String nome) {
		this.cdbdicalculotipo = cdbdicalculotipo;
		this.nome = nome;
	}

	@Id
	public Integer getCdbdicalculotipo() {
		return cdbdicalculotipo;
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setCdbdicalculotipo(Integer id) {
		this.cdbdicalculotipo = id;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdbdicalculotipo == null) ? 0 : cdbdicalculotipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bdicalculotipo other = (Bdicalculotipo) obj;
		if (cdbdicalculotipo == null) {
			if (other.cdbdicalculotipo != null)
				return false;
		} else if (!cdbdicalculotipo.equals(other.cdbdicalculotipo))
			return false;
		return true;
	}
	
}
