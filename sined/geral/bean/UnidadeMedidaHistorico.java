package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.UnidadeMedidaacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_unidademedidahistorico",sequenceName="sq_unidademedidahistorico")
public class UnidadeMedidaHistorico implements Log{

	protected Integer cdUnidadeMedidaHistorico;
	protected Unidademedida unidadeMedida;
	protected String unidadeMedidaConversaoHistorico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected UnidadeMedidaacao acao;
	protected String observacao;
	
	// CAMPOS DA UNIDADE MEDIDA HIST�RICO
	protected String nome;
	protected String simbolo;
	protected Boolean ativo;
	protected Integer casasdecimaisestoque;
	protected Boolean etiquetaunica;

	
	@Id
	@GeneratedValue(generator="sq_unidademedidahistorico",strategy=GenerationType.AUTO)
	public Integer getCdUnidadeMedidaHistorico() {
		return cdUnidadeMedidaHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	
	@DisplayName("Convers�o")
	public String getUnidadeMedidaConversaoHistorico() {
		return unidadeMedidaConversaoHistorico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@DisplayName("A��o")
	public UnidadeMedidaacao getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	@DisplayName("S�mbolo")
	public String getSimbolo() {
		return simbolo;
	}

	@DisplayName("Ativos")
	public Boolean getAtivo() {
		return ativo;
	}

	@DisplayName("Casas Decimais - Estoque")
	public Integer getCasasdecimaisestoque() {
		return casasdecimaisestoque;
	}

	@DisplayName("Etiqueta �nica do material no recebimento?")
	public Boolean getEtiquetaunica() {
		return etiquetaunica;
	}

	//SETs
	
	public void setCdUnidadeMedidaHistorico(Integer cdUnidadeMedidaHistorico) {
		this.cdUnidadeMedidaHistorico = cdUnidadeMedidaHistorico;
	}
	
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	
	public void setUnidadeMedidaConversaoHistorico(
			String unidadeMedidaConversaoHistorico) {
		this.unidadeMedidaConversaoHistorico = unidadeMedidaConversaoHistorico;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setAcao(UnidadeMedidaacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setCasasdecimaisestoque(Integer casasdecimaisestoque) {
		this.casasdecimaisestoque = casasdecimaisestoque;
	}

	public void setEtiquetaunica(Boolean etiquetaunica) {
		this.etiquetaunica = etiquetaunica;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
