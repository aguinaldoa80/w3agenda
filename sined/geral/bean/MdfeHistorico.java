package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeSituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_mdfehistorico", sequenceName = "sq_mdfehistorico")
public class MdfeHistorico implements Log{

	protected Integer cdMdfeHistorico;
	protected Mdfe mdfe;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Usuario usuarioAltera;
	protected String observacao;
	protected MdfeSituacao mdfeSituacao;
	protected String ids;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfehistorico")
	public Integer getCdMdfeHistorico() {
		return cdMdfeHistorico;
	}
	public void setCdMdfeHistorico(Integer cdMdfeHistorico) {
		this.cdMdfeHistorico = cdMdfeHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}

	@DisplayName("Data")
	@Override
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;	
	}
	
	@DisplayName("Respons�vel")
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioAltera() {
		return usuarioAltera;
	}
	public void setUsuarioAltera(Usuario usuarioAltera) {
		this.usuarioAltera = usuarioAltera;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@DisplayName("A��o")
	public MdfeSituacao getMdfeSituacao() {
		return mdfeSituacao;
	}
	public void setMdfeSituacao(MdfeSituacao mdfeSituacao) {
		this.mdfeSituacao = mdfeSituacao;
	}
	
	@Transient
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
}
