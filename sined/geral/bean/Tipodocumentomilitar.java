package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Tipodocumentomilitar {

	protected Integer cdtipodocumentomilitar;
	protected String nome;
	

	@Id
	@DisplayName("Id")
	public Integer getCdtipodocumentomilitar() {
		return cdtipodocumentomilitar;
	}
	
	public void setCdtipodocumentomilitar(Integer cdtipodocumentomilitar) {
		this.cdtipodocumentomilitar = cdtipodocumentomilitar;
	}

	
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

}
