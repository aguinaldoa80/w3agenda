package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.CollectionOfElements;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_interacao", sequenceName = "sq_interacao")
public class Interacao implements Log{

	protected Integer cdinteracao;
	protected List<Interacaoitem> listaInteracaoitem;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_interacao")
	public Integer getCdinteracao() {
		return cdinteracao;
	}
	@DisplayName("Intera��o")
	@OneToMany(mappedBy="interacao")
	@CollectionOfElements
	public List<Interacaoitem> getListaInteracaoitem() {
		return listaInteracaoitem;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdinteracao(Integer cdinteracao) {
		this.cdinteracao = cdinteracao;
	}
	public void setListaInteracaoitem(List<Interacaoitem> listaInteracaoitem) {
		this.listaInteracaoitem = listaInteracaoitem;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
