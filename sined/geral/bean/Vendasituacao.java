package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Vendasituacao {

//	public static Vendasituacao ORCAMENTO = new Vendasituacao(1, "Or�amento");
	public static Vendasituacao FATURADA = new Vendasituacao(2, "Faturada");
	public static Vendasituacao EMPRODUCAO = new Vendasituacao(3, "Produ��o");
	public static Vendasituacao CANCELADA = new Vendasituacao(4, "Cancelada");
	public static Vendasituacao REALIZADA = new Vendasituacao(5, "Realizada");
	public static Vendasituacao AGUARDANDO_APROVACAO = new Vendasituacao(6, "Aguardando aprova��o");
	
	protected Integer cdvendasituacao;
	protected String descricao;
	
	public Vendasituacao() {
	}
	
	public Vendasituacao(Integer cdvendasituacao, String descricao) {
		this.cdvendasituacao = cdvendasituacao;
		this.descricao = descricao;
	}
	
	@Id
	public Integer getCdvendasituacao() {
		return cdvendasituacao;
	}
	
	@DisplayName("Nome")
	@DescriptionProperty
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdvendasituacao(Integer cdvendasituacao) {
		this.cdvendasituacao = cdvendasituacao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Vendasituacao){
			return this.getCdvendasituacao() != null && this.getCdvendasituacao().equals(((Vendasituacao)obj).getCdvendasituacao());
		} else
			return false;
	}
}
