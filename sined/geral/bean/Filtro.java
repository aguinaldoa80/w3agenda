package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.FiltroTipoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_filtro", sequenceName="sq_filtro")
@DefaultOrderBy(value="upper(retira_acento(nome)) asc")
public class Filtro implements Log{

	private Integer cdfiltro;
	private FiltroTipoEnum tipo;
	private String nome;
	private Tela tela;
	private String parametros;
	private Usuario usuario;
//	private List<FiltroUsuariopapel> listaFiltrousuariopapel;
	private List<Filtropapel> listaFiltropapel;
	
	public Filtro() {}
	
	public Filtro(Integer cdfiltro) {
		this.cdfiltro = cdfiltro;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_filtro")
	public Integer getCdfiltro() {
		return cdfiltro;
	}
	
	@Required
	@DescriptionProperty
	@MinLength(value=4)
	@MaxLength(value=50)
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="cdtela")
	@Required
	public Tela getTela() {
		return tela;
	}

	@Required
	public String getParametros() {
		return parametros;
	}

	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}

	@OneToMany(mappedBy="filtro", fetch= FetchType.LAZY)
	public List<Filtropapel> getListaFiltropapel() {
		return listaFiltropapel;
	}

	@Required
	public FiltroTipoEnum getTipo() {
		return tipo;
	}
	
	public void setTipo(FiltroTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public void setCdfiltro(Integer cdfiltro) {
		this.cdfiltro = cdfiltro;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTela(Tela tela) {
		this.tela = tela;
	}

	public void setParametros(String parametros) {
		this.parametros = parametros;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setListaFiltropapel(List<Filtropapel> listaFiltropapel) {
		this.listaFiltropapel = listaFiltropapel;
	}
	
	//log
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera=dtaltera;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdfiltro == null) ? 0 : cdfiltro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Filtro other = (Filtro) obj;
		if (cdfiltro == null) {
			if (other.cdfiltro != null)
				return false;
		} else if (!cdfiltro.equals(other.cdfiltro))
			return false;
		return true;
	}
	
	//TRANSIENT
	private List<Papel> listaPapel;
	private Object classeFiltro;
	
	@Transient
	public List<Papel> getListaPapel() {
		return listaPapel;
	}
	
	public void setListaPapel(List<Papel> listaPapel) {
		this.listaPapel = listaPapel;
	}

	@Transient
	public Object getClasseFiltro() {
		return classeFiltro;
	}
	
	public void setClasseFiltro(Object classeFiltro) {
		this.classeFiltro = classeFiltro;
	}
	
	
}
