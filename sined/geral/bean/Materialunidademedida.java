package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialunidademedida", sequenceName = "sq_materialunidademedida")
@DisplayName("Convers�o de Unidades")
public class Materialunidademedida {

	protected Integer cdmaterialunidademedida;
	protected Material material;
	protected Unidademedida unidademedida;
	protected Double fracao;
	protected Double qtdereferencia;
	protected Double valorunitario;	
	protected Double valormaximo;
	protected Double valorminimo;
	protected Boolean mostrarconversao;
	protected Boolean prioridadevenda;
	protected Boolean prioridadecompra;
	protected Boolean prioridadeproducao;
	protected Boolean considerardimensoesvendaconversao;
	protected Boolean pesoliquidocalculadopelaformula;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialunidademedida")
	public Integer getCdmaterialunidademedida() {
		return cdmaterialunidademedida;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Unidade de Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@Required
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}	
	@Required
	@DisplayName("Fra��o")
	public Double getFracao() {
		return fracao;
	}
	@Required
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	@DisplayName("Mostrar a convers�o no estoque/venda")
	public Boolean getMostrarconversao() {
		return mostrarconversao;
	}	
	@DisplayName("Priorit�rio na Venda")
	public Boolean getPrioridadevenda() {
		return prioridadevenda;
	}
	@DisplayName("Priorit�rio na Produ��o")
	public Boolean getPrioridadeproducao() {
		return prioridadeproducao;
	}
	@DisplayName("Qtde Refer�ncia")
	@Required
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	@DisplayName("Valor m�ximo")
	public Double getValormaximo() {
		return valormaximo;
	}
	@DisplayName("Valor m�nimo")
	public Double getValorminimo() {
		return valorminimo;
	}
	@DisplayName("Priorit�rio em Compras")
	public Boolean getPrioridadecompra() {
		return prioridadecompra;
	}
	@DisplayName("Considerar somente dimens�es da venda na convers�o")
	public Boolean getConsiderardimensoesvendaconversao() {
		return considerardimensoesvendaconversao;
	}
	@DisplayName("Peso calculado pela f�rmula")
	public Boolean getPesoliquidocalculadopelaformula() {
		return pesoliquidocalculadopelaformula;
	}
	
	public void setPrioridadecompra(Boolean prioridadecompra) {
		this.prioridadecompra = prioridadecompra;
	}
	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}
	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}
	public void setPrioridadevenda(Boolean prioridadevenda) {
		this.prioridadevenda = prioridadevenda;
	}
	public void setPrioridadeproducao(Boolean prioridadeproducao) {
		this.prioridadeproducao = prioridadeproducao;
	}
	public void setCdmaterialunidademedida(Integer cdmaterialunidademedida) {
		this.cdmaterialunidademedida = cdmaterialunidademedida;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setMostrarconversao(Boolean mostrarconversao) {
		this.mostrarconversao = mostrarconversao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setConsiderardimensoesvendaconversao(Boolean considerardimensoesvendaconversao) {
		this.considerardimensoesvendaconversao = considerardimensoesvendaconversao;
	}
	public void setPesoliquidocalculadopelaformula(Boolean pesoliquidocalculadopelaformula) {
		this.pesoliquidocalculadopelaformula = pesoliquidocalculadopelaformula;
	}	

	@Transient
	public Double getFracaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fracao != null)
			valor = this.fracao / (this.qtdereferencia != null && this.qtdereferencia != 0 ? this.qtdereferencia : 1.0);
		return valor;
	}
}
