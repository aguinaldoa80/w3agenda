package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_treinamento", sequenceName = "sq_treinamento")
public class Treinamento{

	protected Integer cdtreinamento;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean ativo = new Boolean(true);

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Treinamento) {
			Treinamento c = (Treinamento) obj;
			return c.getCdtreinamento().equals(this.getCdtreinamento());
		}
		return super.equals(obj);
	}
	public Boolean getAtivo() {
		return ativo;
	}

	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_treinamento")
	public Integer getCdtreinamento() {
		return cdtreinamento;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
		
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Transient
	public String getName() {
		return getNome();
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setCdtreinamento(Integer id) {
		this.cdtreinamento = id;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
