package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_novidadeversaofuncionalidade", sequenceName = "sq_novidadeversaofuncionalidade")
	public class NovidadeVersaoFuncionalidade {

	protected Integer cdnovidadeversaofuncionalidade;
	protected String titulo;
	protected String funcionalidade;
	protected String urlBaseConhecimento;
	protected String urlVideo;
	protected String modulo;

	protected Segmento segmento;
	
	protected NovidadeVersao novidadeversao;


	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_novidadeversaofuncionalidade")
	public Integer getCdnovidadeversaofuncionalidade() {
		return cdnovidadeversaofuncionalidade;
	}
	
	@Required
	public String getTitulo() {
		return titulo;
	}
	
	@Required
	public String getFuncionalidade() {
		return funcionalidade;
	}
	
	public String getUrlBaseConhecimento() {
		return urlBaseConhecimento;
	}

	public String getUrlVideo() {
		return urlVideo;
	}
	public String getModulo() {
		return modulo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsegmento")
	public Segmento getSegmento() {
		return segmento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnovidadeversao")
	public NovidadeVersao getNovidadeversao() {
		return novidadeversao;
	}

	public void setCdnovidadeversaofuncionalidade(
			Integer cdnovidadeversaofuncionalidade) {
		this.cdnovidadeversaofuncionalidade = cdnovidadeversaofuncionalidade;
	}

	public void setUrlBaseConhecimento(String urlBaseConhecimento) {
		this.urlBaseConhecimento = urlBaseConhecimento;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}
	
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public void setFuncionalidade(String funcionalidade) {
		this.funcionalidade = funcionalidade;
	}

	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	
	public void setNovidadeversao(NovidadeVersao novidadeversao) {
		this.novidadeversao = novidadeversao;
	}

}
