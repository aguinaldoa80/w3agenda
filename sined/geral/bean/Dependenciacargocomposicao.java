package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Rela��o de depend�ncia entre cargos")
@SequenceGenerator(name = "sq_dependenciacargocomposicao", sequenceName = "sq_dependenciacargocomposicao")
public class Dependenciacargocomposicao {
	
	protected Integer cddependenciacargocomposicao;
	protected Recursocomposicao recursocomposicao;
	protected Cargo cargo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dependenciacargocomposicao")	
	public Integer getCddependenciacargocomposicao() {
		return cddependenciacargocomposicao;
	}
	
	@DisplayName("Itens da composi��o do or�amento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdrecursocomposicao")
	@Required	
	public Recursocomposicao getRecursocomposicao() {
		return recursocomposicao;
	}
	
	@DisplayName("Cargo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required	
	public Cargo getCargo() {
		return cargo;
	}

	public void setCddependenciacargocomposicao(Integer cddependenciacargocomposicao) {
		this.cddependenciacargocomposicao = cddependenciacargocomposicao;
	}

	public void setRecursocomposicao(Recursocomposicao recursocomposicao) {
		this.recursocomposicao = recursocomposicao;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}
