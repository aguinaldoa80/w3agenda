package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_fechamentofinanceirohistorico", sequenceName = "sq_fechamentofinanceirohistorico")
@DisplayName("Hist�rico do Fechamento Financeiro")
public class FechamentoFinanceiroHistorico implements Log{

	protected Integer cdfechamentofinanceirohistorico;
	protected String observacao;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected Usuario usuarioAltera; 
	protected Fechamentofinanceiro fechamentofinanceiro;
	
	public FechamentoFinanceiroHistorico(){
		
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fechamentofinanceirohistorico")
	public Integer getCdfechamentofinanceirohistorico() {
		return cdfechamentofinanceirohistorico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfechamentofinanceiro")
	public Fechamentofinanceiro getFechamentofinanceiro() {
		return fechamentofinanceiro;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	public Usuario getUsuarioAltera() {
		return usuarioAltera;
	}
	
	public void setCdfechamentofinanceirohistorico(Integer cdfechamentofinanceirohistorico) {
		this.cdfechamentofinanceirohistorico = cdfechamentofinanceirohistorico;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;	
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setFechamentofinanceiro(Fechamentofinanceiro fechamentofinanceiro) {
		this.fechamentofinanceiro = fechamentofinanceiro;
	}
	public void setUsuarioAltera(Usuario usuarioAltera) {
		this.usuarioAltera = usuarioAltera;
	}
}
