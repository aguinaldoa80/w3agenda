package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.TipoRespostaEnum;

@Entity
@SequenceGenerator(name = "sq_questionarioquestao", sequenceName = "sq_questionarioquestao")
@DisplayName("Quest�es")
public class Questionarioquestao{
	
	protected Integer cdquestionarioquestao;
	protected String questao;
	protected TipoRespostaEnum tiporesposta;
	protected Integer pontuacaosim;
	protected Integer pontuacaonao;
	protected Integer pontuacaoquestao;
	protected Questionario questionario;
	protected Boolean exibirquestaoordemcompra;
	protected List<Questionarioresposta> listaquestionarioresposta;
	protected Grupoquestionario grupoquestionario;
	protected Questionarioresposta questionarioresposta = new Questionarioresposta();
	protected Boolean obrigarresposta;
	protected Atividadetipo atividadetipo;
	
	public Questionarioquestao(){
	}
	public Questionarioquestao(Integer cdquestionarioquestao){
		this.cdquestionarioquestao = cdquestionarioquestao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_questionarioquestao")
	public Integer getCdquestionarioquestao() {
		return cdquestionarioquestao;
	}
	public void setCdquestionarioquestao(Integer cdquestionarioquestao) {
		this.cdquestionarioquestao = cdquestionarioquestao;
	}
	
	@Required
	@DisplayName("Quest�o")
	@DescriptionProperty
	@MaxLength(200)
	public String getQuestao() {
		return questao;
	}
	public void setQuestao(String questao) {
		this.questao = questao;
	}
	
	@Required
	@DisplayName("Tipo campo")
	public TipoRespostaEnum getTiporesposta() {
		return tiporesposta;
	}
	public void setTiporesposta(TipoRespostaEnum tiporesposta) {
		this.tiporesposta = tiporesposta;
	}
	
	@DisplayName("Pontua��o para sim")
	public Integer getPontuacaosim() {
		return pontuacaosim;
	}
	public void setPontuacaosim(Integer pontuacaosim) {
		this.pontuacaosim = pontuacaosim;
	}
	
	@DisplayName("Pontua��o para n�o")
	public Integer getPontuacaonao() {
		return pontuacaonao;
	}
	@DisplayName("Respostas")
	@OneToMany(mappedBy="questionarioquestao")
	public List<Questionarioresposta> getListaquestionarioresposta() {
		return listaquestionarioresposta;
	}
	
	public void setListaquestionarioresposta(
			List<Questionarioresposta> listaquestionarioresposta) {
		this.listaquestionarioresposta = listaquestionarioresposta;
	}
	public void setPontuacaonao(Integer pontuacaonao) {
		this.pontuacaonao = pontuacaonao;
	}
	
	@JoinColumn(name="cdquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	public Questionario getQuestionario() {
		return questionario;
	}
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}
	@DisplayName("Pontua��o Quest�o")
	public Integer getPontuacaoquestao() {
		return pontuacaoquestao;
	}
	public void setPontuacaoquestao(Integer pontuacaoquestao) {
		this.pontuacaoquestao = pontuacaoquestao;
	}
	@DisplayName("Exibir apenas na Ordem de Compra")
	public Boolean getExibirquestaoordemcompra() {
		return exibirquestaoordemcompra;
	}
	public void setExibirquestaoordemcompra(Boolean exibirquestaoordemcompra) {
		this.exibirquestaoordemcompra = exibirquestaoordemcompra;
	}

	@DisplayName("Grupo Question�rio")
	@JoinColumn(name="cdgrupoquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	public Grupoquestionario getGrupoquestionario() {
		return grupoquestionario;
	}
	
	public void setGrupoquestionario(Grupoquestionario grupoquestionario) {
		this.grupoquestionario = grupoquestionario;
	}
	
	@Transient
	public Questionarioresposta getQuestionarioresposta() {
		return questionarioresposta;
	}
	
	public void setQuestionarioresposta(Questionarioresposta questionarioresposta) {
		this.questionarioresposta = questionarioresposta;
	}
	
	@DisplayName("Obrigar resposta?")
	public Boolean getObrigarresposta() {
		return obrigarresposta;
	}
	
	public void setObrigarresposta(Boolean obrigarresposta) {
		this.obrigarresposta = obrigarresposta;
	}
	
	@DisplayName("Tipo de atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
}
