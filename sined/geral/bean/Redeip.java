package br.com.linkcom.sined.geral.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_redeip", sequenceName = "sq_redeip")
public class Redeip{
	
	protected Integer cdredeip;
	protected Rede rede;
	protected Bandaredecomercial bandaredecomercial;
	protected String ip;
	
	protected Set<Contratomaterialrede> listaContratomaterialrede = new ListSet<Contratomaterialrede>(Contratomaterialrede.class);
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_redeip")
	public Integer getCdredeip() {
		return cdredeip;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrede")	
	public Rede getRede() {
		return rede;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbandaredecomercial")
	@DisplayName("Banda comercial")
	public Bandaredecomercial getBandaredecomercial() {
		return bandaredecomercial;
	}
	
	@Required
	@MaxLength(25)
	@DisplayName("Endere�o IP")
	@DescriptionProperty
	public String getIp() {
		return ip;
	}

	public void setCdredeip(Integer cdredeip) {
		this.cdredeip = cdredeip;
	}

	public void setRede(Rede rede) {
		this.rede = rede;
	}

	public void setBandaredecomercial(Bandaredecomercial bandaredecomercial) {
		this.bandaredecomercial = bandaredecomercial;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@OneToMany(mappedBy="redeip")
	public Set<Contratomaterialrede> getListaContratomaterialrede() {
		return listaContratomaterialrede;
	}

	public void setListaContratomaterialrede(Set<Contratomaterialrede> listaContratomaterialrede) {
		this.listaContratomaterialrede = listaContratomaterialrede;
	}
}