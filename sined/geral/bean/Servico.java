package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;

import br.com.linkcom.sined.util.Log;


@Entity
public class Servico extends Material implements Log {

	protected String nomereduzido;
	protected Integer qtdeminima;
	protected String observacao;
	
	//Devem pertencer somente a classe m�e:
	//protected Timestamp dtAltera;
	//protected Integer cdUsuarioAltera;
	
	public Servico() {
	}
	
	public Servico(String nomereduzido, Integer qtdeminima, String observacao) {
		this.nomereduzido = nomereduzido;
		this.qtdeminima = qtdeminima;
		this.observacao = observacao;
	}

	public String getNomereduzido() {
		return nomereduzido;
	}

	public Integer getQtdeminima() {
		return qtdeminima;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setNomereduzido(String nomereduzido) {
		this.nomereduzido = nomereduzido;
	}

	public void setQtdeminima(Integer qtdeminima) {
		this.qtdeminima = qtdeminima;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		return result;
	}	
}
