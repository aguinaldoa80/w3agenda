package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaoEstoqueAcao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@DisplayName("Hist�rico")
@SequenceGenerator(name = "sq_movimentacaoestoquehistorico", sequenceName = "sq_movimentacaoestoquehistorico")
public class MovimentacaoEstoqueHistorico implements Log{

	private Integer cdMovimentacaoEstoqueHistorico;
	private Movimentacaoestoque movimentacaoEstoque;
	private MovimentacaoEstoqueAcao movimentacaoEstoqueAcao;
	private String observacao;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
/////////////////////////////////////////////////////////////////////|
//  INICIO DOS GET 											/////////|
/////////////////////////////////////////////////////////////////////|	
	public MovimentacaoEstoqueHistorico(){
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_movimentacaoestoquehistorico")
	public Integer getCdMovimentacaoEstoqueHistorico() {
		return cdMovimentacaoEstoqueHistorico;
	}
	@DisplayName("Movimenta��o estoque")
	@JoinColumn(name = "cdmovimentacaoestoque")
	@ManyToOne(fetch = FetchType.LAZY)
	public Movimentacaoestoque getMovimentacaoEstoque() {
		return movimentacaoEstoque;
	}
	@DisplayName("A��o")
	public MovimentacaoEstoqueAcao getMovimentacaoEstoqueAcao() {
		return movimentacaoEstoqueAcao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
/////////////////////////////////////////////////////////////////////|
//  INICIO DOS SET 											/////////|
/////////////////////////////////////////////////////////////////////|
	public void setCdMovimentacaoEstoqueHistorico(
			Integer cdMovimentacaoEstoqueHistorico) {
		this.cdMovimentacaoEstoqueHistorico = cdMovimentacaoEstoqueHistorico;
	}

	public void setMovimentacaoEstoque(Movimentacaoestoque movimentacaoEstoque) {
		this.movimentacaoEstoque = movimentacaoEstoque;
	}

	public void setMovimentacaoEstoqueAcao(MovimentacaoEstoqueAcao movimentacaoEstoqueAcao) {
	 this.movimentacaoEstoqueAcao = movimentacaoEstoqueAcao;
 }
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}


	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	
}