package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialinspecaoitem", sequenceName = "sq_materialinspecaoitem")
@DisplayName("Material inspe��o item")
public class Materialinspecaoitem implements Log {
	
	protected Integer cdmaterialinspecaoitem;
	protected Material material;
	protected Inspecaoitem inspecaoitem;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialinspecaoitem")
	public Integer getCdmaterialinspecaoitem() {
		return cdmaterialinspecaoitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Item de Inspe��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinspecaoitem")
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setCdmaterialinspecaoitem(Integer cdmaterialinspecaoitem) {
		this.cdmaterialinspecaoitem = cdmaterialinspecaoitem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}
	

}
