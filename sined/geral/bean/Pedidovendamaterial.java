package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupItemInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoTicketMedioPorFornecedor;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValidateLoteestoqueObrigatorioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.util.MoneyUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_pedidovendamaterial", sequenceName = "sq_pedidovendamaterial")
@DisplayName("Pedidovendamaterial")
public class Pedidovendamaterial implements VendaMaterialProducaoItemInterface, PneuItemVendaInterface, PopupImpostoVendaInterface, CalculoMarkupItemInterface, ValidateLoteestoqueObrigatorioInterface, InclusaoLoteVendaInterface, CalculoTicketMedioPorFornecedor{

	protected Integer cdpedidovendamaterial;
	protected Integer ordem;
	protected Pedidovenda pedidovenda;
	protected Material material;
	protected Material materialmestre;
	protected Double quantidade;
	protected Double preco;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Unidademedida unidademedida;
	protected Date dtprazoentrega;
	protected String observacao;
	protected Loteestoque loteestoque;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Money saldo;
	protected Double comprimento;
	protected Double comprimentooriginal;
	protected Double largura;
	protected Double altura;
	protected Double multiplicador = 1.0;
	protected Boolean producaosemestoque;
	private Fornecedor fornecedor;
	protected Money percentualrepresentacao;
	protected Material materialcoleta;
	protected List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao = new ListSet<Pedidovendamaterialseparacao>(Pedidovendamaterialseparacao.class);
	protected List<Entregamaterial> listaEntregamaterial;
	protected Set<Vendamaterial> listaVendamaterial;
	protected String identificadorespecifico;
	protected String identificadorinterno;
	protected Pneu pneu;
	protected Double custooperacional;
	protected Double percentualcomissaoagencia;
	protected Double qtdevolumeswms;
	protected Money valorimposto;
	protected Double ipi;
	protected Money valoripi;
	protected Tipocobrancaipi tipocobrancaipi;
	protected Tipocalculo tipocalculoipi;
	protected Money aliquotareaisipi;
	protected Grupotributacao grupotributacao;
	protected Double peso;
	protected Integer identificadorintegracao;
	protected Double valorcoleta;
	protected Boolean bandaalterada;
	protected Boolean servicoalterado;
	protected Double valorvalecomprapropocional;
	protected Double valordescontosemvalecompra;
	protected Double pesomedio;
	protected Comissionamento comissionamento;
	protected Pedidovendamaterial pedidovendamaterialgarantido;
	protected Money descontogarantiareforma;
	protected Garantiareformaitem garantiareformaitem;
	protected Double valorMinimo;
	protected MaterialFaixaMarkup materialFaixaMarkup;
	protected FaixaMarkupNome faixaMarkupNome;
	protected Money valorSeguro;
	protected Money outrasdespesas;
	
	//IMPOSTOS
	/*protected Boolean tributadoicms;
	
	protected Boolean incluirissvalor;
	protected Boolean incluiricmsvalor;
	protected Boolean incluiripivalor;
	protected Boolean incluirpisvalor;
	protected Boolean incluircofinsvalor;*/
	
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms;
	protected Modalidadebcicms modalidadebcicms;
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Modalidadebcicmsst modalidadebcicmsst;
	
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	protected Double difal;
	protected Money valordifal;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
	
	//Transient
	protected String whereInOSVM;
	protected Money total;
	protected Double desconto_it;
	protected Double valordescontocustopedidovenda;
	protected Double percentualdescontocustopedidovenda;
	protected Double margemcustopedidovenda;
	protected Money totalproduto;
	protected Produto produtotrans;
	protected Integer ordemexibicao;
	protected Double valorvendaminimo;
	protected Vendaorcamentomaterial vendaorcamentomaterial;
	protected Boolean isMaterialmestregrade;
	protected String materialcoleta_label;
	protected Double quantidadecoleta;
	protected Double quantidadecoletada;
	protected Localarmazenagem localarmazenagemcoleta;
	protected String observacaocoleta;
	protected Material materialAnterior;
	protected Boolean existematerialsimilar;
	protected Boolean existematerialsimilarColeta;
	protected String historicoTroca;
	protected Boolean existeVinculoColetaProducao = Boolean.FALSE;
	protected Double descontoProporcional;
	protected Boolean considerarDesconto;
	protected Boolean considerarValeCompra;
	protected String calcularmargempor;
	protected Money totalprodutoReport;
	protected Money totalprodutoDescontoReport;
	protected String fornecedorStr;
	protected Boolean somentesaidaMaterialproducao;
	protected Boolean registraPesomedioMaterialgrupo;
	protected Double pesoLiquidoVendaOuMaterial;
	protected Materialtabelapreco materialtabelapreco;
	protected Materialtabelaprecoitem materialtabelaprecoitem;
	protected Boolean considerarMultiplicadorCusto;
	protected Double qtdOriginal = 0.0;
	protected Integer indice;
	protected String dtprazoentregaStr;
	protected Money totalprodutoOtr;
	protected Boolean agendaProducaoGerada;
	protected Money totalImpostos;
	protected boolean exibirIpiPopup;
	protected boolean exibirIcmsPopup;
	protected boolean exibirDifalPopup;
	protected Money valorBruto;
	protected String whereInVincularColeta;
	
	////////////////////////|
	/// INICIO DOS get   ///|
	////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendamaterial")
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	@DisplayName("Venda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DisplayName("Produto")
	public Material getMaterial() {
		return material;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialmestre")
	public Material getMaterialmestre() {
		return materialmestre;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	public Double getQuantidade() {
		return quantidade;
	}
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	
	public Money getDesconto() {
		return desconto;
	}
	
	@DisplayName("(%) Desconto")
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	
	@DisplayName("Lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	public Double getMultiplicador() {
		return multiplicador;
	}
	
	@DisplayName("Material de coleta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialcoleta")
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	
	@DisplayName("Identificador")
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneu")
	public Pneu getPneu() {
		return pneu;
	}
	@OneToMany(mappedBy="pedidovendamaterial", fetch=FetchType.LAZY)
	public List<Pedidovendamaterialseparacao> getListaPedidovendamaterialseparacao() {
		return listaPedidovendamaterialseparacao;
	}
	
	@OneToMany(mappedBy="pedidovendamaterial", fetch=FetchType.LAZY)
	public Set<Vendamaterial> getListaVendamaterial() {
		return listaVendamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareformaitem")
	public Garantiareformaitem getGarantiareformaitem() {
		return garantiareformaitem;
	}
	
	public Double getValorMinimo() {
		return valorMinimo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialfaixamarkup")
	public MaterialFaixaMarkup getMaterialFaixaMarkup() {
		return materialFaixaMarkup;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixaMarkupNome() {
		return faixaMarkupNome;
	}
	@DisplayName("Seguro")
	public Money getValorSeguro() {
		return valorSeguro;
	}
	@DisplayName("Outras despesas")
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterialgarantido")
	public Pedidovendamaterial getPedidovendamaterialgarantido() {
		return pedidovendamaterialgarantido;
	}
	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	
	//IMPOSTOS
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}

	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}
	
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}

	public Money getValorbcicms() {
		return valorbcicms;
	}

	public Double getIcms() {
		return icms;
	}

	public Money getValoricms() {
		return valoricms;
	}
	
	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}
	
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}
	
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}
	
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}

	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}

	public Double getIcmsst() {
		return icmsst;
	}

	public Money getValoricmsst() {
		return valoricmsst;
	}

	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}

	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}

	public Money getValorbcfcp() {
		return valorbcfcp;
	}

	public Double getFcp() {
		return fcp;
	}

	public Money getValorfcp() {
		return valorfcp;
	}

	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}

	public Double getFcpst() {
		return fcpst;
	}

	public Money getValorfcpst() {
		return valorfcpst;
	}
	
	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}

	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}

	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}

	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}

	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}

	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}

	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}

	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}

	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}

	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}
	
	public Double getDifal() {
		return difal;
	}
	public Money getValordifal() {
		return valordifal;
	}
	
	@DisplayName("NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	@MaxLength(9)
	@DisplayName("NCM completo")
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	
	////////////////////////|
	/// INICIO DOS set   ///|
	////////////////////////|

	public void setValordescontocustopedidovenda(
			Double valordescontocustopedidovenda) {
		this.valordescontocustopedidovenda = valordescontocustopedidovenda;
	}
	
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	public void setTotalproduto(Money totalproduto) {
		this.totalproduto = totalproduto;
	}

	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	
	public void setListaVendamaterial(Set<Vendamaterial> listaVendamaterial) {
		this.listaVendamaterial = listaVendamaterial;
	}
	
	public void setListaPedidovendamaterialseparacao(List<Pedidovendamaterialseparacao> listaPedidovendamaterialseparacao) {
		this.listaPedidovendamaterialseparacao = listaPedidovendamaterialseparacao;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}

	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}


	public Double getFatorconversao() {
		return fatorconversao;
	}
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}

	
	
	@DisplayName("Prazo de entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Money getSaldo() {
		return saldo;
	}

	public void setSaldo(Money saldo) {
		this.saldo = saldo;
	}
	
	@Transient
	public Double getValordescontocustopedidovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null){
			if(this.getDesconto() != null && this.getConsiderarDesconto() != null && this.getConsiderarDesconto()){
				valordesconto += this.getDesconto().doubleValue();
			}
			if(this.getConsiderarDesconto() != null && this.getConsiderarDesconto()){
				valordesconto += this.getDescontoProporcional();
			}
		}		
		return valordesconto;
	}
	
	@Transient
	public Double getValordescontocustopedidovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getValordescontocustopedidovenda();
		}
		return 0d;
	}
	
	@Transient
	public Double getPercentualdescontocustopedidovendaReport() {
		if(getConsiderarDesconto() != null && getConsiderarDesconto()){
			return getPercentualdescontocustopedidovenda();
		}
		return 0d;
	}

	@Transient
	public Double getPercentualdescontocustopedidovenda() {
		Double valordesconto = 0.0;
		if(this.getMaterial() != null && this.getPreco() != null){
			if(this.getValorvendamaterial() != null){
				if(this.getPreco() < this.getValorvendamaterial()){
					valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}else if(this.getMaterial().getValorvenda() != null){
				if(this.getPreco() < this.getMaterial().getValorvenda()){
					valordesconto = (this.getQuantidade()*this.getMaterial().getValorvenda()) - this.getTotalproduto().getValue().doubleValue(); 
				}
			}
			if(this.getDesconto() != null){
				valordesconto += this.getDesconto().getValue().doubleValue();
			}
			if(this.getDescontoProporcional() != null){
				valordesconto += this.getDescontoProporcional();
			}
			
			if(this.getValorvendamaterial() != null){
				if(this.getPreco() < this.getValorvendamaterial() || valordesconto > 0){
					valordesconto = 100 - ((this.getTotalproduto().getValue().doubleValue()-valordesconto)*100)/(this.getQuantidade()*this.getPreco());
				}
			}else if(this.getMaterial().getValorvenda() != null  || (this.getMaterial().getValorvenda() != null && valordesconto > 0)){
				if(this.getPreco() < this.getMaterial().getValorvenda() && (this.getQuantidade()*this.getMaterial().getValorvenda()) != 0 ){
					valordesconto = 100 - ((this.getTotalproduto().getValue().doubleValue()-valordesconto)*100)/(this.getQuantidade()*this.getPreco()); 
				}
			}
		}		
		return valordesconto;
	}
	


	public void setPercentualdescontocustopedidovenda(Double percentualdescontocustopedidovenda) {
		this.percentualdescontocustopedidovenda = percentualdescontocustopedidovenda;
	}

	public void setMargemcustopedidovenda(Double margemcustopedidovenda) {
		this.margemcustopedidovenda = margemcustopedidovenda;
	}
	
	@Transient
	public Double getDescontoProporcional() {
		return descontoProporcional;
	}
	public void setDescontoProporcional(Double descontoProporcional) {
		this.descontoProporcional = descontoProporcional;
	}
	
	@Transient
	public Boolean getConsiderarDesconto() {
		return considerarDesconto;
	}
	public void setConsiderarDesconto(Boolean considerarDesconto) {
		this.considerarDesconto = considerarDesconto;
	}
	
	@Transient
	public String getCalcularmargempor() {
		return calcularmargempor != null ? calcularmargempor : "Custo";
	}
	public void setCalcularmargempor(String calcularmargempor) {
		this.calcularmargempor = calcularmargempor;
	}
	@Transient
	public Money getTotalprodutoReport() {
		return totalprodutoReport;
	}
	public void setTotalprodutoReport(Money totalprodutoReport) {
		this.totalprodutoReport = totalprodutoReport;
	}
	@Transient
	public Money getTotalprodutoDescontoReport() {
		return totalprodutoDescontoReport;
	}
	public void setTotalprodutoDescontoReport(Money totalprodutoDescontoReport) {
		this.totalprodutoDescontoReport = totalprodutoDescontoReport;
	}

	@Transient
	public Money getTotalproduto() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 2).doubleValue());
	}
	
	@Transient
	public Money getTotalprodutoOtr() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
            total += MoneyUtils.moneyToDouble(this.valorSeguro);
            total += MoneyUtils.moneyToDouble(this.outrasdespesas);
			if(this.desconto != null) {
				total -= this.desconto.getValue().doubleValue();
			} else if(this.percentualdesconto != null) {
				total -= this.percentualdesconto / 100d * total;
			}
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 2).doubleValue());
	}
	
	public void setTotalprodutoOtr(Money totalprodutoOtr) {
		this.totalprodutoOtr = totalprodutoOtr;
	}
	
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}

	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	@Transient
	public Double getFatorconversaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fatorconversao != null)
			valor = (this.fatorconversao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		return valor;
	}

	public Double getComprimento() {
		return comprimento;
	}
	public Double getComprimentooriginal() {
		return comprimentooriginal;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getAltura() {
		return altura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setComprimentooriginal(Double comprimentooriginal) {
		this.comprimentooriginal = comprimentooriginal;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	
	@Transient
	public Produto getProdutotrans() {
		return produtotrans;
	}
	public void setProdutotrans(Produto produtotrans) {
		this.produtotrans = produtotrans;
	}

	@Transient
	public Double getValorcustoRelatorio() {		
		Double valorretorno = 0.0;
		Double valorcusto = 0.0;
		Double multiplicador = 1d;
		
		if (this.getMultiplicador() != null && this.getMultiplicador() > 0) {
			multiplicador = this.getMultiplicador();
		}
		
		if (!Boolean.TRUE.equals(this.getConsiderarMultiplicadorCusto()) || (this.getMaterial() == null || Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda()))) {
			multiplicador = 1d;
		}
		
		if (this.getValorcustomaterial() != null) {
			valorcusto = this.getValorcustomaterial() * multiplicador; 
			valorretorno = valorcusto;
		} else if(this.material != null && this.material.getValorcusto() != null) {
			valorcusto = this.material.getValorcusto() * multiplicador;
			if (valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null) {
				valorcusto = valorcusto / getFatorconversaoQtdereferencia();
			}
			valorretorno = valorcusto;
		}
		
		/*if(valorcusto != null && this.produtotrans != null){
			boolean alturadiferente = false;
			boolean larguradiferente = false;
			boolean comprimentodiferente = false;
			
			if((this.produtotrans.getAltura() != null && this.altura == null) || (this.produtotrans.getAltura() == null && this.altura != null)){
				alturadiferente = true;
			} else if(this.produtotrans.getAltura() != null && this.altura != null && this.produtotrans.getAltura() != (this.altura*1000)){
				alturadiferente = true;
			}
			
			if((this.produtotrans.getLargura() != null && this.largura == null) || (this.produtotrans.getLargura() == null && this.largura != null)){
				larguradiferente = true;
			} else if(this.produtotrans.getLargura() != null && this.largura != null && this.produtotrans.getLargura() != (this.largura*1000)){
				larguradiferente = true;
			}
			
			if((this.produtotrans.getComprimento() != null && this.comprimento == null) || (this.produtotrans.getComprimento() == null && this.comprimento != null)){
				comprimentodiferente = true;
			} else if(this.produtotrans.getComprimento() != null && this.comprimento != null && this.produtotrans.getComprimento() != (this.comprimento*1000)){
				comprimentodiferente = true;
			}
			
			if(alturadiferente || larguradiferente || comprimentodiferente){
				valorretorno = (alturadiferente && this.altura != null && this.altura != 0 ? this.altura*1000 : 1.0) *
						   (larguradiferente && this.largura != null && this.largura != 0 ? this.largura*1000 : 1.0) *
						   (comprimentodiferente && this.comprimento != null && this.comprimento != 0 ? this.comprimento*1000 : 1.0) * 
						   (valorcusto != null ? valorcusto/1000 : 0.0);
			}
		}*/
		
		return valorretorno;
	}

	@Transient
	public Double getValorvendaRelatorio() {
		Double valorretorno = 0.0;
		Double valorvenda = 0.0;
		Double multiplicador = 1d;
		
		if (this.getMultiplicador() != null && this.getMultiplicador() > 0) {
			multiplicador = this.getMultiplicador();
		}
		
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if (this.getValorvendamaterial() != null) {
			valorvenda = this.getValorvendamaterial() * multiplicador; 
			valorretorno = valorvenda;
		} else if (this.material != null && this.material.getValorvenda() != null) {
			valorvenda = this.material.getValorvenda() * multiplicador;
			valorretorno = valorvenda;
		}
		
		return valorretorno;
	}
	
	@Transient
	public Double getQtdeMetrocunico(Double qtde){
		Double qtderelatorio = 1.0;
		if(qtde != null){
			qtderelatorio = qtde * 
					(this.getAltura() != null ? this.getAltura() : 1.0) * 
					(this.getLargura() != null ? this.getLargura() : 1.0) *
					(this.getComprimento() != null ? this.getComprimento() : 1.0);
		}
		return qtderelatorio;
	}
	
	@Transient
	public Double getDesconto_it() {
		return desconto_it;
	}
	
	public void setDesconto_it(Double descontoIt) {
		desconto_it = descontoIt;
	}
	
	@DisplayName("Fornecedor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Fornecedor")
	public Money getPercentualrepresentacao() {
		return percentualrepresentacao;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setPercentualrepresentacao(Money percentualrepresentacao) {
		this.percentualrepresentacao = percentualrepresentacao;
	}
	
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}

	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}

	@Transient
	public String getWhereInOSVM() {
		return whereInOSVM;
	}

	public void setWhereInOSVM(String whereInOSVM) {
		this.whereInOSVM = whereInOSVM;
	}

	@Transient
	public String getFornecedorStr() {
		if (getFornecedor()!=null && getFornecedor().getNome()!=null){
			return getFornecedor().getNome();
		}else {
			return fornecedorStr;
		}
	}
	public void setFornecedorStr(String fornecedorStr) {
		this.fornecedorStr = fornecedorStr;
	}
	
	@Transient
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	@Transient
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}

	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}

	@Transient
	public Vendaorcamentomaterial getVendaorcamentomaterial() {
		return vendaorcamentomaterial;
	}
	@Transient
	public Boolean getIsMaterialmestregrade() {
		return isMaterialmestregrade;
	}

	public void setVendaorcamentomaterial(Vendaorcamentomaterial vendaorcamentomaterial) {
		this.vendaorcamentomaterial = vendaorcamentomaterial;
	}
	public void setIsMaterialmestregrade(Boolean isMaterialmestregrade) {
		this.isMaterialmestregrade = isMaterialmestregrade;
	}
	
	@Transient
	public String getMaterialcoleta_label() {
		if(materialcoleta != null && materialcoleta.getNome() != null && !materialcoleta.getNome().equals("")){
			materialcoleta_label = materialcoleta.getNome();
		}
		return materialcoleta_label;
	}
	
	public void setMaterialcoleta_label(String materialcoletaLabel) {
		materialcoleta_label = materialcoletaLabel;
	}
	
	@Transient
	public Double getQuantidadecoleta() {
		return quantidadecoleta;
	}
	@Transient
	public Double getQuantidadecoletada() {
		return quantidadecoletada;
	}
	@Transient
	public Localarmazenagem getLocalarmazenagemcoleta() {
		return localarmazenagemcoleta;
	}
	@Transient
	public String getObservacaocoleta() {
		if(observacaocoleta == null){
			observacaocoleta = observacao;
		}
		return observacaocoleta;
	}

	public void setLocalarmazenagemcoleta(Localarmazenagem localarmazenagemcoleta) {
		this.localarmazenagemcoleta = localarmazenagemcoleta;
	}
	public void setQuantidadecoleta(Double quantidadecoleta) {
		this.quantidadecoleta = quantidadecoleta;
	}
	public void setQuantidadecoletada(Double quantidadecoletada) {
		this.quantidadecoletada = quantidadecoletada;
	}
	public void setObservacaocoleta(String observacaocoleta) {
		this.observacaocoleta = observacaocoleta;
	}
	@Transient
	public Material getMaterialAnterior() {
		return materialAnterior;
	}

	public void setMaterialAnterior(Material materialAnterior) {
		this.materialAnterior = materialAnterior;
	}

	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}

	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	
	@OneToMany(mappedBy="pedidovendamaterial", fetch=FetchType.LAZY)
	public List<Entregamaterial> getListaEntregamaterial() {
		return listaEntregamaterial;
	}
	
	public void setListaEntregamaterial(List<Entregamaterial> listaEntregamaterial) {
		this.listaEntregamaterial = listaEntregamaterial;
	}
	
	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	@Transient
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}
	public void setExistematerialsimilarColeta(Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	
	@Transient
	public String getHistoricoTroca() {
		return historicoTroca;
	}
	public void setHistoricoTroca(String historicoTroca) {
		this.historicoTroca = historicoTroca;
	}

	@Transient
	public Boolean getExisteVinculoColetaProducao() {
		return existeVinculoColetaProducao;
	}
	
	public void setExisteVinculoColetaProducao(
			Boolean existeVinculoColetaProducao) {
		this.existeVinculoColetaProducao = existeVinculoColetaProducao;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@DisplayName("Custo Operacional")
	public Double getCustooperacional() {
		return custooperacional;
	}

	public void setCustooperacional(Double custooperacional) {
		this.custooperacional = custooperacional;
	}
	@DisplayName("Percentual Comiss�o Ag�ncia")
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}

	public Double getQtdevolumeswms() {
		return qtdevolumeswms;
	}

	public void setQtdevolumeswms(Double qtdevolumeswms) {
		this.qtdevolumeswms = qtdevolumeswms;
	}

	public Money getValorimposto() {
		return valorimposto;
	}

	public void setValorimposto(Money valorimposto) {
		this.valorimposto = valorimposto;
	}

	@DisplayName("Valor IPI")
	public Money getValoripi() {
		return valoripi;
	}
	public Double getIpi() {
		return ipi;
	}
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public Tipocalculo getTipocalculoipi() {
		return tipocalculoipi;
	}
	public Money getAliquotareaisipi() {
		return aliquotareaisipi;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@DisplayName("Peso l�quido")
	public Double getPeso() {
		return peso;
	}
	
	public Integer getIdentificadorintegracao() {
		return identificadorintegracao;
	}
	public Double getValorcoleta() {
		return valorcoleta;
	}

	public void setIdentificadorintegracao(Integer identificadorintegracao) {
		this.identificadorintegracao = identificadorintegracao;
	}
	public void setValorcoleta(Double valorcoleta) {
		this.valorcoleta = valorcoleta;
	}
	public void setValoripi(Money valoripi) {
		this.valoripi = valoripi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipocobrancaipi) {
		this.tipocobrancaipi = tipocobrancaipi;
	}
	public void setTipocalculoipi(Tipocalculo tipocalculoipi) {
		this.tipocalculoipi = tipocalculoipi;
	}
	public void setAliquotareaisipi(Money aliquotareaisipi) {
		this.aliquotareaisipi = aliquotareaisipi;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	
	@Transient
	public Double getPesoVendaOuMaterial(){
		return getPeso() != null ? getPeso() : (getMaterial() != null ? getMaterial().getPeso() : null);
	}

	public Boolean getBandaalterada() {
		return bandaalterada;
	}

	public Boolean getServicoalterado() {
		return servicoalterado;
	}
	
	public void setServicoalterado(Boolean servicoalterado) {
		this.servicoalterado = servicoalterado;
	}
	
	public void setBandaalterada(Boolean bandaalterada) {
		this.bandaalterada = bandaalterada;
	}
	
	@Transient
	public Double getValorvalecomprapropocional() {
		return valorvalecomprapropocional;
	}
	
	@Transient
	public Double getValordescontosemvalecompra() {
		if(this.getDesconto() != null)
			return (valordescontosemvalecompra != null ? valordescontosemvalecompra : 0d) + this.getDesconto().getValue().doubleValue();
		else
			return valordescontosemvalecompra;
	}

	public void setValorvalecomprapropocional(Double valorvalecomprapropocional) {
		this.valorvalecomprapropocional = valorvalecomprapropocional;
	}

	public void setValordescontosemvalecompra(Double valordescontosemvalecompra) {
		this.valordescontosemvalecompra = valordescontosemvalecompra;
	}

	@Transient
	public Boolean getConsiderarValeCompra() {
		return considerarValeCompra;
	}

	public void setConsiderarValeCompra(Boolean considerarValeCompra) {
		this.considerarValeCompra = considerarValeCompra;
	}
	
	@Transient
	public Double getvalortabelaRelatorio(){
		if(this.getValorvendaRelatorio() != null && this.getQuantidade() != null){
 			return this.getValorvendaRelatorio() * this.getQuantidade(); 
		}else{
			return 0d;
		}
	}
	
	@Transient
	public Double getQuantidadeRelatorio() {
		Double qtd = 1d;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		
		//desconsidera o multiplicador caso o metro cubico n�o esteja marcado no cadastro do material
		if (this.getMaterial() != null && !Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if(this.getQuantidade() != null){
			qtd = this.getQuantidade() *  multiplicador;
		}
		
		return qtd;
	}
	
	@Transient
	public Double getPrecoRelatorio(){
		Double precoretorno = 0.0;
		Double multiplicador = 1d;
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0) {
			multiplicador = this.getMultiplicador();
		}
		
		if (this.getMaterial() != null && Boolean.TRUE.equals(this.getMaterial().getMetrocubicovalorvenda())) {
			multiplicador = 1d;
		}
		
		if (this.getPreco() != null) {
			precoretorno = this.getPreco() * multiplicador;
		}
		
		return precoretorno;
	}

	@Transient
	public Boolean getSomentesaidaMaterialproducao() {
		return somentesaidaMaterialproducao;
	}

	public void setSomentesaidaMaterialproducao(Boolean somentesaidaMaterialproducao) {
		this.somentesaidaMaterialproducao = somentesaidaMaterialproducao;
	}
	
	public void setPedidovendamaterialgarantido(
			Pedidovendamaterial pedidovendamaterialgarantido) {
		this.pedidovendamaterialgarantido = pedidovendamaterialgarantido;
	}
	
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	
	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	
	public void setValorMinimo(Double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}
	
	public void setMaterialFaixaMarkup(MaterialFaixaMarkup materialFaixaMarkup) {
		this.materialFaixaMarkup = materialFaixaMarkup;
	}
	
	public void setFaixaMarkupNome(FaixaMarkupNome faixaMarkupNome) {
		this.faixaMarkupNome = faixaMarkupNome;
	}
	
	public void setOutrasdespesas(Money valorOutrasDespesas) {
		this.outrasdespesas = valorOutrasDespesas;
	}
	
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}

	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}

	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}

	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}

	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}

	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}

	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}

	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}

	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}

	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}

	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}

	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}
	
	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}

	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}

	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}

	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}

	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}

	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}

	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}

	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}

	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}

	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}
	
	public void setDifal(Double difal) {
		this.difal = difal;
	}
	
	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}
	
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}

	//////////////////////////////|
	/// INICIO DOS TRANSIENT   ///|
	//////////////////////////////|
	@Transient
	public Boolean getRegistraPesomedioMaterialgrupo() {
		return registraPesomedioMaterialgrupo;
	}
	public void setRegistraPesomedioMaterialgrupo(
			Boolean registraPesomedioMaterialgrupo) {
		this.registraPesomedioMaterialgrupo = registraPesomedioMaterialgrupo;
	}
	
	@Transient
	public Double getPesoLiquidoVendaOuMaterial() {
		pesoLiquidoVendaOuMaterial = null;
		Double qtde = this.getQuantidade() == null || this.getQuantidade() == 0 ? 1 : this.getQuantidade();
		if(this.getPesoVendaOuMaterial() != null){
			Double pesoLiquido = this.getPesoVendaOuMaterial();
			if(this.getUnidademedida() != null && this.getMaterial() != null && 
					this.getMaterial().getUnidademedida() != null && 
					!this.getUnidademedida().equals(this.getMaterial().getUnidademedida())){
				pesoLiquido = pesoLiquido / this.getFatorconversaoQtdereferencia();
			}
			pesoLiquidoVendaOuMaterial = pesoLiquido * qtde;
		}
		return (pesoLiquidoVendaOuMaterial!=null)? SinedUtil.roundByParametro(pesoLiquidoVendaOuMaterial, 2): null;
	}
	public void setPesoLiquidoVendaOuMaterial(Double pesoLiquidoVendaOuMaterial) {
		this.pesoLiquidoVendaOuMaterial = pesoLiquidoVendaOuMaterial;
	}
	@Transient
	public String getLinkGarantiareformaVenda(){
		String link = "";
		if(getGarantiareformaitem() != null && getGarantiareformaitem().getGarantiareforma() != null && getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() != null){
			link = "<a href=\"javascript:visualizaGarantiareforma(" + getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() + ");\">" +
					"Garantia: " + getGarantiareformaitem().getGarantiareforma().getCdgarantiareforma() + "</a>"; 
		}
		return link;
	}
	
	@Transient
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	
	@Transient
	public Boolean getConsiderarMultiplicadorCusto() {
		return considerarMultiplicadorCusto;
	}
	
	public void setConsiderarMultiplicadorCusto(
			Boolean considerarMultiplicadorCusto) {
		this.considerarMultiplicadorCusto = considerarMultiplicadorCusto;
	}
	@Transient
	public Double getQtdOriginal() {
		return qtdOriginal;
	}
	
	public void setQtdOriginal(Double qtdOriginal) {
		this.qtdOriginal = qtdOriginal;
	}
	
	@Transient
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	@Transient
	public Materialtabelaprecoitem getMaterialtabelaprecoitem() {
		return materialtabelaprecoitem;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	public void setMaterialtabelaprecoitem(Materialtabelaprecoitem materialtabelaprecoitem) {
		this.materialtabelaprecoitem = materialtabelaprecoitem;
	}
	@Transient
	public Double getValordescontocustopedidovendaitem() {
		Double valordesconto = 0d;
		if(this.getValorvendamaterial() != null){
			if(this.getPreco() < this.getValorvendamaterial()){
				valordesconto = (this.getQuantidade()*this.getValorvendamaterial()) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}else if(this.getMaterial() != null && this.getMaterial().getValorvenda() != null){
			Double valorvenda = this.getMaterial().getValorvenda();
			if(this.getFatorconversao() != null && this.getQtdereferencia() != null){
				valorvenda = valorvenda / getFatorconversaoQtdereferencia();
			}
			if(this.getPreco() < valorvenda){
				valordesconto = (this.getQuantidade()*valorvenda) - this.getTotalproduto().getValue().doubleValue(); 
			}
		}
		return valordesconto + getValordescontocustopedidovenda();
	}
	
	@Transient
	public Double getMargemcustopedidovenda() {
		Double margem = 100.0;
		Double multiplicador = 1d;
		Double desconto = this.getDesconto() != null ? this.getDesconto().getValue().doubleValue() : 0d;
		Double valecompra = this.getValorvalecomprapropocional() != null ? this.getValorvalecomprapropocional().doubleValue() : 0d;
		if(getDescontoProporcional() != null){
			desconto += getDescontoProporcional();
		}

		if(this.getConsiderarValeCompra() != null && this.getConsiderarValeCompra() 
				&& (this.getConsiderarDesconto() == null || !this.getConsiderarDesconto())){
			desconto -= this.getValorvalecomprapropocional();
		}
		
		if(this.getMultiplicador() != null && this.getMultiplicador() > 0){
			multiplicador = this.getMultiplicador();
		}
		if(this.getMaterial() != null && this.getValorcustomaterial() != null && this.getValorcustomaterial() > 0 && this.getPreco() != null){
			if(getConsiderarDesconto() != null && getConsiderarDesconto()){
				margem = ((this.getPreco()*multiplicador-(desconto/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else if(this.getConsiderarValeCompra() != null && this.getConsiderarValeCompra()){
				margem = ((this.getPreco()*multiplicador-(valecompra/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else {
				margem = ((this.getPreco()*multiplicador)-this.getValorcustoRelatorio())*100;
			}
			if("Custo".equalsIgnoreCase(getCalcularmargempor())){
				margem = margem/this.getValorcustoRelatorio();
			}else {
				margem = margem/this.getPreco();
			}
		}else if(this.getMaterial() != null && this.getMaterial().getValorcusto() != null && this.getMaterial().getValorcusto() > 0 && this.getPreco() != null){
			Double valorcusto = this.material.getValorcusto();
			if(valorcusto != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
				valorcusto = valorcusto / getFatorconversaoQtdereferencia();
			}
			valorcusto = valorcusto * multiplicador;
			if(getConsiderarDesconto() != null && getConsiderarDesconto()){
				margem = ((this.getPreco()*multiplicador-(desconto/this.getQuantidade()))-(valorcusto))*100;
			}else if(this.getConsiderarValeCompra() != null && this.getConsiderarValeCompra()){
				margem = ((this.getPreco()*multiplicador-(valecompra/this.getQuantidade()))-this.getValorcustoRelatorio())*100;
			}else {
				margem = ((this.getPreco()*multiplicador)-(valorcusto))*100;
			}
			if("Custo".equalsIgnoreCase(getCalcularmargempor())){
				margem = margem/(valorcusto);
//			}else if(this.getMaterial().getValorvenda() != null) {
//				Double valorvenda = this.material.getValorvenda();
//				if(valorvenda != null && this.getFatorconversao() != null && this.getQtdereferencia() != null){
//					valorvenda = valorvenda / getFatorconversaoQtdereferencia();
//				}
//				margem = margem/(valorvenda*multiplicador);
//			}
			} else {
				margem = margem/(this.getPreco());
			}
		}
		return margem;
	}
	/////////////////////////|
	/// hashCode e equals ///|
	/////////////////////////|
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdpedidovendamaterial == null) ? 0 : cdpedidovendamaterial
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedidovendamaterial other = (Pedidovendamaterial) obj;
		if (cdpedidovendamaterial == null) {
			if (other.cdpedidovendamaterial != null)
				return false;
		} else if (!cdpedidovendamaterial.equals(other.cdpedidovendamaterial))
			return false;
		return true;
	}
	
	@Transient
	public Money getTotalprodutoItemSemArredondamento() {
		Double total = 0.0;
		if(this.getPreco() != null){
			total  = this.getPreco() * (this.getQuantidade() != null ? this.getQuantidade() : 1.0);
		}
		return new Money(SinedUtil.round(BigDecimal.valueOf(total), 10).doubleValue());
	}
	
	@Transient
	@Override
	public java.util.Date getPrazoentrega() {
		return getDtprazoentrega();
	}

	@Transient
	@Override
	public Integer getId() {
		return cdpedidovendamaterial;
	}

	@Override
	public void setId(Integer id) {
//		this.id = id;
	}
	
	@Transient
	@Override
	public Integer getIndice() {
		return indice;
	}
	@Override
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	
	@Transient
	@DisplayName("Prazo de entrega")
	public String getDtprazoentregaStr() {
		return dtprazoentregaStr;
	}
	public void setDtprazoentregaStr(String dtprazoentregaStr) {
		this.dtprazoentregaStr = dtprazoentregaStr;
	}
	
	@Transient
	public Boolean getAgendaProducaoGerada() {
		return agendaProducaoGerada;
	}
	public void setAgendaProducaoGerada(Boolean agendaProducaoGerada) {
		this.agendaProducaoGerada = agendaProducaoGerada;
	}
	
	@Transient
	public Money getTotalImpostos() {
		Money totalImpostos = new Money()
								.add(getValoripi())
								.add(getValoricms())
								.add(getValoricmsst())
								.add(getValorfcp())
								.add(getValorfcpst())
								.add(getValordifal());
		if(abaterdesoneracaoicms != null && abaterdesoneracaoicms){
			totalImpostos = totalImpostos.subtract(getValordesoneracaoicms());
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	@Transient
	@Override
	public Boolean getExibirIpiPopup() {
		return exibirIpiPopup;
	}
	@Override
	public void setExibirIpiPopup(Boolean exibirIpiPopup) {
		this.exibirIpiPopup = exibirIpiPopup;
	}
	@Transient
	@Override
	public Boolean getExibirIcmsPopup() {
		return exibirIcmsPopup;
	}
	@Override
	public void setExibirIcmsPopup(Boolean exibirIcmsPopup) {
		this.exibirIcmsPopup = exibirIcmsPopup;
	}
	@Transient
	@Override
	public Boolean getExibirDifalPopup() {
		return exibirDifalPopup;
	}
	@Override
	public void setExibirDifalPopup(Boolean exibirDifalPopup) {
		this.exibirDifalPopup = exibirDifalPopup;
	}
	@Transient
	@Override
	public Money getValorBruto() {
		if(this.quantidade != null && this.preco != null){
			valorBruto = new Money((this.quantidade * this.preco));
		}

		return valorBruto;
	}
	
	@Transient
	public String getWhereInVincularColeta() {
		return Util.strings.emptyIfNull(whereInVincularColeta);
	}
	public void setWhereInVincularColeta(String whereInVincularColeta) {
		this.whereInVincularColeta = whereInVincularColeta;
	}
}
