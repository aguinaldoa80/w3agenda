package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_ajustefiscal", sequenceName = "sq_ajustefiscal")
public class Ajustefiscal {

	protected Integer cdajustefiscal;
	protected Entregadocumento entregadocumento;
	protected Notafiscalproduto notafiscalproduto;
	protected Obslancamentofiscal obslancamentofiscal;
	protected String codigoajuste;
	protected Material material;
	protected String descricaocomplementar;
	protected Double aliquotaicms;
	protected Double basecalculoicms;
	protected Double valoricms;
	protected Double outrosvalores;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajustefiscal")
	public Integer getCdajustefiscal() {
		return cdajustefiscal;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNota")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("C�digo do Ajuste")
	@MaxLength(10)
	public String getCodigoajuste() {
		return codigoajuste;
	}
	@Required
	@DisplayName("Observa��o de Lan�amento Fiscal")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdobslancamentofiscal")
	public Obslancamentofiscal getObslancamentofiscal() {
		return obslancamentofiscal;
	}
	@MaxLength(1000)
	@DisplayName("Descri��o complementar")
	public String getDescricaocomplementar() {
		return descricaocomplementar;
	}
	@DisplayName("Al�quota ICMS")
	public Double getAliquotaicms() {
		return aliquotaicms;
	}
	@DisplayName("Base de c�lculo ICMS")
	public Double getBasecalculoicms() {
		return basecalculoicms;
	}
	@DisplayName("Valor do ICMS")
	public Double getValoricms() {
		return valoricms;
	}
	@DisplayName("Outros Valores")
	public Double getOutrosvalores() {
		return outrosvalores;
	}

	public void setCdajustefiscal(
			Integer cdajustefiscal) {
		this.cdajustefiscal = cdajustefiscal;
	}

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
	
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setObslancamentofiscal(Obslancamentofiscal obslancamentofiscal) {
		this.obslancamentofiscal = obslancamentofiscal;
	}

	public void setCodigoajuste(String codigoajuste) {
		this.codigoajuste = codigoajuste;
	}

	public void setDescricaocomplementar(String descricaocomplementar) {
		this.descricaocomplementar = descricaocomplementar;
	}

	public void setAliquotaicms(Double aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public void setBasecalculoicms(Double basecalculoicms) {
		this.basecalculoicms = basecalculoicms;
	}

	public void setValoricms(Double valoricms) {
		this.valoricms = valoricms;
	}

	public void setOutrosvalores(Double outrosvalores) {
		this.outrosvalores = outrosvalores;
	}
}
