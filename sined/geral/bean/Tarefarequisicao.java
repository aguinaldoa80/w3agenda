package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_tarefarequisicao", sequenceName="sq_tarefarequisicao")
public class Tarefarequisicao {

	protected Integer cdtarefarequisicao;
	protected Requisicao requisicao;
	protected Tarefa tarefa;

	@Id
	@Required
	@GeneratedValue(generator="sq_tarefarequisicao", strategy=GenerationType.AUTO)
	public Integer getCdtarefarequisicao() {
		return cdtarefarequisicao;
	}	
	@Required
	@JoinColumn(name="cdrequisicao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Requisicao getRequisicao() {
		return requisicao;
	}
	@Required
	@JoinColumn(name="cdtarefa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Tarefa getTarefa() {
		return tarefa;
	}	

	public void setCdtarefarequisicao(Integer cdtarefarequisicao) {
		this.cdtarefarequisicao = cdtarefarequisicao;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}	
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
}
