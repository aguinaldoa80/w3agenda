package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_reserva", sequenceName = "sq_reserva")
@DisplayName("Reserva")
public class Reserva {

	protected Integer cdreserva;
	protected Material material;
	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Double quantidade;
	protected Producaoagenda producaoagenda;
	protected Unidademedida unidademedida;
	protected Double fracaounidademedida;
	protected Double qtdereferenciaunidademedida;	
	protected Loteestoque loteestoque;
	protected Ordemservicoveterinaria ordemservicoveterinaria;
	protected Ordemservicoveterinariamaterial ordemservicoveterinariamaterial;
	protected Pedidovenda pedidovenda;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Venda venda;
	protected Vendamaterial vendamaterial;
	protected Expedicao expedicao;
	protected Expedicaoitem expedicaoitem;
	protected Projeto projeto;
	
	//TRANSIENTE
	
	protected Double qtdedisponivel;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_reserva")
	public Integer getCdreserva() {
		return cdreserva;
	}
	
	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Local Armazenagem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public Double getFracaounidademedida() {
		return fracaounidademedida;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagenda")
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	public Double getQtdereferenciaunidademedida() {
		return qtdereferenciaunidademedida;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinaria")
	public Ordemservicoveterinaria getOrdemservicoveterinaria() {
		return ordemservicoveterinaria;
	}
	
	public void setOrdemservicoveterinaria(
			Ordemservicoveterinaria ordemservicoveterinaria) {
		this.ordemservicoveterinaria = ordemservicoveterinaria;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemservicoveterinariamaterial")
	public Ordemservicoveterinariamaterial getOrdemservicoveterinariamaterial() {
		return ordemservicoveterinariamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicaoitem")
	public Expedicaoitem getExpedicaoitem() {
		return expedicaoitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setOrdemservicoveterinariamaterial(Ordemservicoveterinariamaterial ordemservicoveterinariamaterial) {
		this.ordemservicoveterinariamaterial = ordemservicoveterinariamaterial;
	}
	
	public void setCdreserva(Integer cdreserva) {
		this.cdreserva = cdreserva;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	public void setFracaounidademedida(Double fracaounidademedida) {
		this.fracaounidademedida = fracaounidademedida;
	}
	
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	
	public void setQtdereferenciaunidademedida(Double qtdereferenciaunidademedida) {
		this.qtdereferenciaunidademedida = qtdereferenciaunidademedida;
	}
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
	
	public void setExpedicaoitem(Expedicaoitem expedicaoitem) {
		this.expedicaoitem = expedicaoitem;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	
	//TRANSIENTE
	
	@Transient
	public Double getQtdedisponivel() {
		return qtdedisponivel;
	}
	
	public void setQtdedisponivel(Double qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
}
