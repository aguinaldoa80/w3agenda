package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_periodocargo", sequenceName = "sq_periodocargo")
public class Periodocargo implements Log {
	
	protected Integer cdperiodocargo;
	protected Periodo periodo;
	protected Integer numero;
	protected Integer qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_periodocargo")
	public Integer getCdperiodocargo() {
		return cdperiodocargo;
	}
	public Integer getNumero() {
		return numero;
	}
	public Integer getQtde() {
		return qtde;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdperiodo")
	public Periodo getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}
	public void setCdperiodocargo(Integer cdperiodocargo) {
		this.cdperiodocargo = cdperiodocargo;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}	
	
	@Transient
	public void setaQtde(Integer qtde, Integer divisao) {
		this.qtde = new Long(Math.round((qtde/divisao)+0.5)).intValue();
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
