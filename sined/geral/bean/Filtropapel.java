package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_filtropapel", sequenceName="sq_filtropapel")
public class Filtropapel {

	private Integer cdfiltropapel;
	private Filtro filtro;
	private Papel papel;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_filtropapel")
	public Integer getCdfiltropapel() {
		return cdfiltropapel;
	}
	
	@Required
	@ManyToOne
	@JoinColumn(name="cdfiltro")
	public Filtro getFiltro() {
		return filtro;
	}
	
	@Required
	@ManyToOne
	@JoinColumn(name="cdpapel")
	@DescriptionProperty
	public Papel getPapel() {
		return papel;
	}
	
	public void setCdfiltropapel(Integer cdfiltropapel) {
		this.cdfiltropapel = cdfiltropapel;
	}
	public void setFiltro(Filtro filtro) {
		this.filtro = filtro;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
}
