package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_emporiumvendafinalizadora", sequenceName = "sq_emporiumvendafinalizadora")
public class Emporiumvendafinalizadora {

	protected Integer cdemporiumvendafinalizadora;
	protected Emporiumvenda emporiumvenda;
	protected Integer sequencial;
	protected Integer finalizadora_id;
	protected Double valor;
	protected String flag;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumvendafinalizadora")
	public Integer getCdemporiumvendafinalizadora() {
		return cdemporiumvendafinalizadora;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemporiumvenda")
	public Emporiumvenda getEmporiumvenda() {
		return emporiumvenda;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	@DisplayName("Finalizadora (ID)")
	public Integer getFinalizadora_id() {
		return finalizadora_id;
	}

	public Double getValor() {
		return valor;
	}
	
	public String getFlag() {
		return flag;
	}
	
	public void setFlag(String flag) {
		this.flag = flag;
	}

	public void setCdemporiumvendafinalizadora(Integer cdemporiumvendafinalizadora) {
		this.cdemporiumvendafinalizadora = cdemporiumvendafinalizadora;
	}

	public void setEmporiumvenda(Emporiumvenda emporiumvenda) {
		this.emporiumvenda = emporiumvenda;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setFinalizadora_id(Integer finalizadoraId) {
		finalizadora_id = finalizadoraId;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
