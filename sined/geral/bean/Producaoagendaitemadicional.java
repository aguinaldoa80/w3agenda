package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_producaoagendaitemadicional", sequenceName = "sq_producaoagendaitemadicional")
public class Producaoagendaitemadicional {
	
	protected Integer cdproducaoagendaitemadicional; 
	protected Producaoagenda producaoagenda;
	protected Material material;
	protected Double quantidade;
	protected Pneu pneu; 
	
	// TRANSIENTE
	protected Double quantidadeDisponivel;
	protected Double valorvenda;
	protected Money valortotal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_producaoagendaitemadicional")
	public Integer getCdproducaoagendaitemadicional() {
		return cdproducaoagendaitemadicional;
	}

	@JoinColumn(name="cdproducaoagenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	@Required
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	
	@JoinColumn(name="cdpneu")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pneu getPneu() {
		return pneu;
	}

	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	public void setCdproducaoagendaitemadicional(Integer cdproducaoagendaitemadicional) {
		this.cdproducaoagendaitemadicional = cdproducaoagendaitemadicional;
	}

	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	// TRANSIENTE
	
	@Transient
	@DisplayName("Quantidade Dispon�vel")
	public Double getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}
	
	public void setQuantidadeDisponivel(Double quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}
	
	@Transient
	@DisplayName("Pre�o")
	public Double getValorvenda() {
		if(valorvenda == null && material != null && material.getValorvenda() != null){
			valorvenda = material.getValorvenda();
		}
		return valorvenda;
	}
	
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	@Transient
	@DisplayName("Total")
	public Money getValortotal() {
		if(valortotal == null){
			Double quantidadeLocal = getQuantidade();
			Double valorvendaLocal = getValorvenda();
			if(quantidadeLocal != null && valorvendaLocal != null){
				valortotal = new Money(SinedUtil.round(quantidadeLocal * valorvendaLocal, 2));
			}
		}
		return valortotal;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}

	@Transient
	public Integer getCodigomaterial(){
		return material != null ? material.getCdmaterial() : null;
	}
	@Transient
	public String getNomematerial(){
		return material != null ? material.getNome() : null;
	}
}
