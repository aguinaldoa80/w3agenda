package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_entregamaterialsincronizacao", sequenceName = "sq_entregamaterialsincronizacao")
public class Entregamaterialsincronizacao {

	protected Integer cdentregamaterialsincronizacao;
	protected Entregadocumentosincronizacao entregadocumentosincronizacao;
	
	//Material
	protected Material material;
	protected String materialClasseproduto;
	protected String materialCodigo;
	protected String materialDescricao;
	protected Integer materialId;
	protected Integer materialmestreId;
	protected Integer materialQuantidadeVolume;
	protected String materialReferencia;
	protected String materialDescricaoEmbalagem;
	protected Double materialPeso;
	protected Long materialAltura;
	protected Long materialLargura;
	protected Long materialComprimento;
	protected Integer materialcorId;
	
	
	//Classe Produto
	protected Materialgrupo materialgrupo;
	protected String materialgrupoNumero;
	protected String materialgrupoNome;
	
	//Entrega Material
	protected Entregamaterial entregamaterial;
	protected Integer entregamaterialProdutoId;
	protected Integer entregamaterialIdEntrega;
	protected Integer entregamaterialQuantidade;
	protected Double entregamaterialValorUnitario;
	protected String entregamaterialLote;
	
	//Material Devolucao
	protected Materialdevolucao materialdevolucao;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregamaterialsincronizacao")
	public Integer getCdentregamaterialsincronizacao() {
		return cdentregamaterialsincronizacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumentosincronizacao")
	public Entregadocumentosincronizacao getEntregadocumentosincronizacao() {
		return entregadocumentosincronizacao;
	}
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}	
	@DisplayName("Descri��o")
	public String getMaterialDescricao() {
		return materialDescricao;
	}
	@DisplayName("Refer�ncia")
	public String getMaterialReferencia() {
		return materialReferencia;
	}	
	@DisplayName("Descri��o das Embalagem")
	public String getMaterialDescricaoEmbalagem() {
		return materialDescricaoEmbalagem;
	}
	@DisplayName("Peso")
	public Double getMaterialPeso() {
		return materialPeso;
	}
	@DisplayName("Altura")
	public Long getMaterialAltura() {
		return materialAltura;
	}
	@DisplayName("Largura")
	public Long getMaterialLargura() {
		return materialLargura;
	}
	@DisplayName("Comprimento")
	public Long getMaterialComprimento() {
		return materialComprimento;
	}
	@DisplayName("Id. Cor Material")
	public Integer getMaterialcorId() {
		return materialcorId;
	}
	@DisplayName("Quantidade Volume")
	public Integer getMaterialQuantidadeVolume() {
		return materialQuantidadeVolume;
	}
	@DisplayName("Grupo do Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Nome do Grupo")
	public String getMaterialgrupoNome() {
		return materialgrupoNome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	@Transient
	@DisplayName("Nome do Grupo")
	public Integer getEntregamaterialProdutoId() {
		if(entregamaterialProdutoId == null && material != null){
			return material.getCdmaterial();
		}
		return entregamaterialProdutoId;
	}
	@Transient
	@DisplayName("Id Entrega")
	public Integer getEntregamaterialIdEntrega() {
		if(entregamaterialIdEntrega == null && entregadocumentosincronizacao != null && entregadocumentosincronizacao.getEntregadocumento() != null){
			return entregadocumentosincronizacao.getEntregadocumento().getCdentregadocumento();
		}	
		return entregamaterialIdEntrega;
	}
	@DisplayName("Quantidade")
	public Integer getEntregamaterialQuantidade() {
		return entregamaterialQuantidade;
	}
	@DisplayName("Valor unit�rio")
	public Double getEntregamaterialValorUnitario() {
		return entregamaterialValorUnitario;
	}
	@DisplayName("Lote")	
	public String getEntregamaterialLote() {
		return entregamaterialLote;
	}
	
	public void setCdentregamaterialsincronizacao(Integer cdentregamaterialsincronizacao) {
		this.cdentregamaterialsincronizacao = cdentregamaterialsincronizacao;
	}
	public void setEntregadocumentosincronizacao(Entregadocumentosincronizacao entregadocumentosincronizacao) {
		this.entregadocumentosincronizacao = entregadocumentosincronizacao;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialDescricao(String materialDescricao) {
		this.materialDescricao = materialDescricao;
	}
	public void setMaterialQuantidadeVolume(Integer materialQuantidadeVolume) {
		this.materialQuantidadeVolume = materialQuantidadeVolume;
	}
	public void setMaterialReferencia(String materialReferencia) {
		this.materialReferencia = materialReferencia;
	}
	public void setMaterialDescricaoEmbalagem(String materialDescricaoEmbalagem) {
		this.materialDescricaoEmbalagem = materialDescricaoEmbalagem;
	}
	public void setMaterialPeso(Double materialPeso) {
		this.materialPeso = materialPeso;
	}
	public void setMaterialAltura(Long materialAltura) {
		this.materialAltura = materialAltura;
	}
	public void setMaterialLargura(Long materialLargura) {
		this.materialLargura = materialLargura;
	}
	public void setMaterialComprimento(Long materialComprimento) {
		this.materialComprimento = materialComprimento;
	}
	public void setMaterialcorId(Integer materialcorId) {
		this.materialcorId = materialcorId;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialgrupoNumero(String materialgrupoNumero) {
		this.materialgrupoNumero = materialgrupoNumero;
	}
	public void setMaterialgrupoNome(String materialgrupoNome) {
		this.materialgrupoNome = materialgrupoNome;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setEntregamaterialProdutoId(Integer entregamaterialProdutoId) {
		this.entregamaterialProdutoId = entregamaterialProdutoId;
	}
	public void setEntregamaterialIdEntrega(Integer entregamaterialIdEntrega) {
		this.entregamaterialIdEntrega = entregamaterialIdEntrega;
	}
	public void setEntregamaterialQuantidade(Integer entregamaterialQuantidade) {
		this.entregamaterialQuantidade = entregamaterialQuantidade;
	}
	public void setEntregamaterialValorUnitario(Double entregamaterialValorUnitario) {
		this.entregamaterialValorUnitario = entregamaterialValorUnitario;
	}		
	public void setEntregamaterialLote(String entregamaterialLote) {
		this.entregamaterialLote = entregamaterialLote;
	}
	public Integer getMaterialmestreId() {
		return materialmestreId;
	}
	public void setMaterialmestreId(Integer materialmestreId) {
		this.materialmestreId = materialmestreId;
	}
	
	@DisplayName("Id Entrega Material")
	@Transient
	public Integer getMaterialId() {
		if(materialId == null && material != null){
			return material.getCdmaterial();
		}
		return materialId;
	}
	
	@DisplayName("C�digo")
	@Transient
	public String getMaterialCodigo() {
		if((materialCodigo == null || "".equals(materialCodigo)) && material != null && material.getCdmaterial() != null){
			materialCodigo = material.getCdmaterial().toString();
		}
		return materialCodigo;
	}
	
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}
	public void setMaterialCodigo(String materialCodigo) {
		this.materialCodigo = materialCodigo;
	}

	@DisplayName("Classe")
	@Transient
	public String getMaterialClasseproduto() {
		if((materialClasseproduto == null || "".equals(materialClasseproduto))){
			if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
				return materialgrupo.getCdmaterialgrupo().toString();
			}else if(material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getCdmaterialgrupo() != null){
				return material.getMaterialgrupo().getCdmaterialgrupo().toString();
			}
		}
		return materialClasseproduto;
	}
	
	public void setMaterialClasseproduto(String materialClasseproduto) {
		this.materialClasseproduto = materialClasseproduto;
	}

	@DisplayName("N�mero")
	@Transient
	public String getMaterialgrupoNumero() {
		if((materialgrupoNumero == null || "".equals(materialgrupoNumero))){
			if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
				return materialgrupo.getCdmaterialgrupo().toString();
			}
		}
		return materialgrupoNumero;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialdevolucao")
	public Materialdevolucao getMaterialdevolucao() {
		return materialdevolucao;
	}
	public void setMaterialdevolucao(Materialdevolucao materialdevolucao) {
		this.materialdevolucao = materialdevolucao;
	}	
}
