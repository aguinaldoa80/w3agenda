package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialvenda", sequenceName = "sq_materialvenda")
public class Materialvenda {

	protected Integer cdmaterialvenda;
	protected Material material;
	protected Material materialvendaitem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialvenda")
	public Integer getCdmaterialvenda() {
		return cdmaterialvenda;
	}	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}	
	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialvendaitem")
	public Material getMaterialvendaitem() {
		return materialvendaitem;
	}
	
	public void setCdmaterialvenda(Integer cdmaterialvenda) {
		this.cdmaterialvenda = cdmaterialvenda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialvendaitem(Material materialvendaitem) {
		this.materialvendaitem = materialvendaitem;
	}
}
