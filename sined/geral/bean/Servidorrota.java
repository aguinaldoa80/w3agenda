package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_servidorrota", sequenceName = "sq_servidorrota")
public class Servidorrota{

	protected Integer cdservidorrota;
	protected Servidor servidor;
	protected String endereco;
	protected Redemascara redemascara;
	protected String gateway;
	protected Boolean ativo = true;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidorrota")
	public Integer getCdservidorrota() {
		return cdservidorrota;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")
	public Servidor getServidor() {
		return servidor;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Endere�o")
	public String getEndereco() {
		return endereco;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdredemascara")
	@DisplayName("M�scara")
	public Redemascara getRedemascara() {
		return redemascara;
	}

	@Required
	@MaxLength(50)	
	public String getGateway() {
		return gateway;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setCdservidorrota(Integer cdservidorrota) {
		this.cdservidorrota = cdservidorrota;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setRedemascara(Redemascara redemascara) {
		this.redemascara = redemascara;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
