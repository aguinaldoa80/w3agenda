package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_planoassistencial", sequenceName = "sq_planoassistencial")
@DisplayName("Plano assistencial")
public class Planoassistencial
{
	protected Integer cdplanoassistenial;
	protected String nome;
	protected String observacao;
	protected Tipoplano tipoplano;
//	protected Set<Valorplanoassistencial> listavalorplanoassistencial = new ListSet<Valorplanoassistencial>(Valorplanoassistencial.class);
	
	public Planoassistencial() {
		
	}
	
	public Planoassistencial(Integer cdplanoassistencial) 
	{
		this.cdplanoassistenial= cdplanoassistencial;
	}
	
	@Id
	public Integer getCdplanoassistenial() {
		return cdplanoassistenial;
	}
	
	@Required
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Tipo de plano")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipoplano")
	public Tipoplano getTipoplano() {
		return tipoplano;
	}
	
/*	@OneToMany(mappedBy="Planoassistencial")
	@DisplayName("Tabela de valores")
	public Set<Valorplanoassistencial> getListavalorplanoassistencial() {
		return listavalorplanoassistencial;
	}*/
	
	public void setCdplanoassistenial(Integer cdplanoassistenial) {
		this.cdplanoassistenial = cdplanoassistenial;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setTipoplano(Tipoplano tipoplano) {
		this.tipoplano = tipoplano;
	}
	/*public void setListavalorplanoassistencial(
			Set<Valorplanoassistencial> listavalorplanoassistencial) {
		this.listavalorplanoassistencial = listavalorplanoassistencial;
	}*/
	
	
}
