package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_ecom_telefone", sequenceName="sq_ecom_telefone")
public class Ecom_Telefone {

	
	private Integer cdEcom_Telefone;
	private Integer id;
	private String telefone;
	private String tipo;
	private Ecom_Cliente ecomCliente;
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_telefone", strategy=GenerationType.AUTO)
	public Integer getCdEcom_Telefone() {
		return cdEcom_Telefone;
	}
	public void setCdEcom_Telefone(Integer cdEcom_Telefone) {
		this.cdEcom_Telefone = cdEcom_Telefone;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_cliente")
	public Ecom_Cliente getEcomCliente() {
		return ecomCliente;
	}
	public void setEcomCliente(Ecom_Cliente ecomCliente) {
		this.ecomCliente = ecomCliente;
	}
}
