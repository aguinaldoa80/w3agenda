package br.com.linkcom.sined.geral.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;

@Entity
public class Contatipo {

	public static final Integer CONTA_BANCARIA = 1;
	public static final Integer CAIXA = 2;
	public static final Integer CARTAO_DE_CREDITO = 3;
	
	public static Contatipo TIPO_CONTA_BANCARIA = new Contatipo(1);
	public static Contatipo TIPO_CAIXA = new Contatipo(2);
	public static Contatipo TIPO_CARTAO_CREDITO = new Contatipo(3);
	
	protected Integer cdcontatipo;
	protected String nome;
	protected Set<Tipovinculo> listaTipoVinculo = new ListSet<Tipovinculo>(Tipovinculo.class);
	protected Set<Conta> listaConta = new ListSet<Conta>(Conta.class);
	
	
	public Contatipo(){}
	
	public Contatipo(Integer cdcontatipo){
		this.cdcontatipo = cdcontatipo;
	}
	
	public Contatipo(Integer cdcontatipo, String nome) {
		this.cdcontatipo = cdcontatipo;
		this.nome = nome;
	}

	@Id
	@DisplayName("Id")
	public Integer getCdcontatipo() {
		return cdcontatipo;
	}

	@OneToMany(mappedBy="contatipo")
	public Set<Tipovinculo> getListaTipoVinculo() {
		return listaTipoVinculo;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@OneToMany(mappedBy="contatipo")
	public Set<Conta> getListaConta() {
		return listaConta;
	}
	
	public void setCdcontatipo(Integer cdcontatipo) {
		this.cdcontatipo = cdcontatipo;
	}
	public void setListaTipoVinculo(Set<Tipovinculo> listaTipoVinculo) {
		this.listaTipoVinculo = listaTipoVinculo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setListaConta(Set<Conta> listaConta) {
		this.listaConta = listaConta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcontatipo == null) ? 0 : cdcontatipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contatipo other = (Contatipo) obj;
		if (cdcontatipo == null) {
			if (other.cdcontatipo != null)
				return false;
		} else if (!cdcontatipo.equals(other.cdcontatipo))
			return false;
		return true;
	}
	
}
