package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_comissionamentofaixamarkup", sequenceName = "sq_comissionamentofaixamarkup")
public class ComissionamentoFaixaMarkup {
	protected Integer cdComissionamentoFaixaMarkup;
	protected Comissionamento comissionamento;
	protected FaixaMarkupNome faixa;
	protected Double percentualVendedor;
	protected Double percentualVendedorPrincipal;
	protected Double percentualTotal;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comissionamentofaixamarkup")
	public Integer getCdComissionamentoFaixaMarkup() {
		return cdComissionamentoFaixaMarkup;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")	
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	
	@DisplayName("Faixa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixamarkupnome")
	public FaixaMarkupNome getFaixa() {
		return faixa;
	}
	
	@DisplayName("% do Vendedor Responsável")
	public Double getPercentualVendedor() {
		return percentualVendedor;
	}
	
	@DisplayName("% do Vendedor Principal")
	public Double getPercentualVendedorPrincipal() {
		return percentualVendedorPrincipal;
	}
	
	@DisplayName("% Total")
	public Double getPercentualTotal() {
		return percentualTotal;
	}
	
	public void setCdComissionamentoFaixaMarkup(Integer cdComissionamentoFaixaMarkup) {
		this.cdComissionamentoFaixaMarkup = cdComissionamentoFaixaMarkup;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setFaixa(FaixaMarkupNome faixa) {
		this.faixa = faixa;
	}
	public void setPercentualVendedor(Double percentualVendedor) {
		this.percentualVendedor = percentualVendedor;
	}
	public void setPercentualVendedorPrincipal(Double percentualVendedorPrincipal) {
		this.percentualVendedorPrincipal = percentualVendedorPrincipal;
	}
	public void setPercentualTotal(Double percentualTotal) {
		this.percentualTotal = percentualTotal;
	}
}
