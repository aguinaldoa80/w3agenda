package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.CorValor;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_cotacaofornecedoritem",sequenceName="sq_cotacaofornecedoritem")
public class Cotacaofornecedoritem implements Log{
	
	protected Integer cdcotacaofornecedoritem;
	protected Cotacaofornecedor cotacaofornecedor;
	protected Material material;
	protected Double qtdesol;
	protected Double qtdecot;
	protected Double valor;
	protected Frequencia frequencia;
	protected Double qtdefrequencia;
	protected Localarmazenagem localarmazenagem;
	protected Localarmazenagem localarmazenagemsolicitacao;
	protected Double frete;
	protected Boolean icmsissincluso;
	protected Boolean ipiincluso;
	protected Double icmsiss;
	protected Double ipi;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Materialclasse materialclasse;
	protected Date dtentrega;
	protected Date dtgarantia;
	protected Unidademedida unidademedida;
	protected Money desconto;
	protected String observacao;
	protected Boolean naocotar;
	
	protected Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra = new ListSet<Cotacaofornecedoritemsolicitacaocompra>(Cotacaofornecedoritemsolicitacaocompra.class);
	
//	TRANSIENTES
	protected Boolean isServico;
	protected Double valorunitario;
	protected Boolean checado;
	protected String nomematerial;
	protected Money ultvalor;
	protected String tipo;
	protected CorValor corvalor;
	protected CorValor corvalorult;
	protected String nomeunidademedida;
	protected Integer indexFornecedor;
	protected Integer indexMaterial;
	protected String localarmazenagemtrans;
	protected Unidademedida unidademedidatrans;
	protected Boolean ordemcompragerada;
	protected String nomeclasseordemcompragerada;
	
	public Cotacaofornecedoritem(){
	}
	
	public Cotacaofornecedoritem(Cotacaofornecedor cotacaofornecedor, Double qtdesol, Localarmazenagem localarmazenagem, 
			Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagemsolicitacao, Frequencia frequencia, Double qtdefrequencia, Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra){
		this.cotacaofornecedor = cotacaofornecedor;
		this.qtdesol = qtdesol;
		this.localarmazenagem = localarmazenagem;
		this.localarmazenagemsolicitacao = localarmazenagemsolicitacao;
		this.material = material;
		this.materialclasse = materialclasse;
		this.icmsissincluso = Boolean.FALSE;
		this.ipiincluso = Boolean.FALSE;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequencia;
		this.listaCotacaofornecedoritemsolicitacaocompra = listaCotacaofornecedoritemsolicitacaocompra;
	}
	
	public Cotacaofornecedoritem(Cotacaofornecedor cotacaofornecedor, Double qtdesol, Localarmazenagem localarmazenagem, 
			Material material, Materialclasse materialclasse, Localarmazenagem localarmazenagemsolicitacao, Frequencia frequencia, 
			Double qtdefrequencia, Unidademedida unidademedida, String observacao, Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra){
		this.cotacaofornecedor = cotacaofornecedor;
		this.qtdesol = qtdesol;
		this.localarmazenagem = localarmazenagem;
		this.localarmazenagemsolicitacao = localarmazenagemsolicitacao;
		this.material = material;
		this.materialclasse = materialclasse;
		this.icmsissincluso = Boolean.FALSE;
		this.ipiincluso = Boolean.FALSE;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequencia;
		this.unidademedida = unidademedida;
		this.observacao = observacao;
		this.listaCotacaofornecedoritemsolicitacaocompra = listaCotacaofornecedoritemsolicitacaocompra;
	}
	
	
	
	public Cotacaofornecedoritem(Material material, Double qtdesolicitacao, Double qtdecotacao){
		this.qtdecot = qtdecotacao;
		this.material = material;
		this.qtdesol = qtdesolicitacao;
	}
	
	@Id
	@GeneratedValue(generator="sq_cotacaofornecedoritem",strategy=GenerationType.AUTO)
	public Integer getCdcotacaofornecedoritem() {
		return cdcotacaofornecedoritem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcotacaofornecedor")
	public Cotacaofornecedor getCotacaofornecedor() {
		return cotacaofornecedor;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}

	@DisplayName("Sol.")
	public Double getQtdesol() {
		return qtdesol;
	}

	@DisplayName("Cot.")
	public Double getQtdecot() {
		return qtdecot;
	}

	public Double getValor() {
		return valor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	@DisplayName("Local de entrega")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagemsolicitacao")
	@DisplayName("Local entrega escolhido solicita��o")
	public Localarmazenagem getLocalarmazenagemsolicitacao() {
		return localarmazenagemsolicitacao;
	}

	public Double getFrete() {
		return frete;
	}
	
	@DisplayName("?")
	public Boolean getIcmsissincluso() {
		return icmsissincluso;
	}

	@DisplayName("?")
	public Boolean getIpiincluso() {
		return ipiincluso;
	}
	
	@MaxLength(5)
	@DisplayName("ICMS/ISS")
	public Double getIcmsiss() {
		return icmsiss;
	}

	@MaxLength(5)
	@DisplayName("IPI")
	public Double getIpi() {
		return ipi;
	}
	@DisplayName("Entrega")
	public Date getDtentrega() {
		return dtentrega;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}

	public Double getQtdefrequencia() {
		return qtdefrequencia;
	}
	
	@DisplayName("Garantia")
	public Date getDtgarantia() {
		return dtgarantia;
	}
	
	@MaxLength(100)
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("N�o Cotar")
	public Boolean getNaocotar() {
		return naocotar;
	}
	
	@OneToMany(mappedBy="cotacaofornecedoritem")
	public Set<Cotacaofornecedoritemsolicitacaocompra> getListaCotacaofornecedoritemsolicitacaocompra() {
		return listaCotacaofornecedoritemsolicitacaocompra;
	}
	
	public void setListaCotacaofornecedoritemsolicitacaocompra(
			Set<Cotacaofornecedoritemsolicitacaocompra> listaCotacaofornecedoritemsolicitacaocompra) {
		this.listaCotacaofornecedoritemsolicitacaocompra = listaCotacaofornecedoritemsolicitacaocompra;
	}

	public void setNaocotar(Boolean naocotar) {
		this.naocotar = naocotar;
	}

	public void setDtgarantia(Date dtgarantia) {
		this.dtgarantia = dtgarantia;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setQtdefrequencia(Double qtdefrequencia) {
		this.qtdefrequencia = qtdefrequencia;
	}
	
	public void setIcmsissincluso(Boolean icmsissincluso) {
		this.icmsissincluso = icmsissincluso;
	}

	public void setIpiincluso(Boolean ipiincluso) {
		this.ipiincluso = ipiincluso;
	}

	public void setIcmsiss(Double icmsiss) {
		this.icmsiss = icmsiss;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}

	public void setCotacaofornecedor(Cotacaofornecedor cotacaofornecedor) {
		this.cotacaofornecedor = cotacaofornecedor;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setQtdesol(Double qtdesol) {
		this.qtdesol = qtdesol;
	}

	public void setQtdecot(Double qtdecot) {
		this.qtdecot = qtdecot;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	public void setFrete(Double frete) {
		this.frete = frete;
	}

	public void setCdcotacaofornecedoritem(Integer cdcotacaofornecedoritem) {
		this.cdcotacaofornecedoritem = cdcotacaofornecedoritem;
	}
	
	public void setLocalarmazenagemsolicitacao(
			Localarmazenagem localarmazenagemsolicitacao) {
		this.localarmazenagemsolicitacao = localarmazenagemsolicitacao;
	}
	
	/* API */
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setDtentrega(Date dtentrega) {
		this.dtentrega = dtentrega;
	}
	
//	TRANSIENTES
	
	@Transient
	@DisplayName("Frequ�ncia")
	public String getFrequenciaStr(){
		String string = "";
		if (getFrequencia() != null && getFrequencia().getCdfrequencia() != null){
			Integer cdfrequencia = getFrequencia().getCdfrequencia();
			
			switch (cdfrequencia) {
			case 1: //UNICA
				string = "�nica";
				break;
			case 2: //DI�RIA
				string = getQtdefrequencia() + " dia(s)";
				break;
			case 3: //SEMANAL
				string = getQtdefrequencia() + " semana(s)";
				break;
			case 4: //QUINZENAL
				string = getQtdefrequencia() + " quinzena(s)";
				break;
			case 5: //MENSAL
				string = getQtdefrequencia() + " mese(s)";
				break;
			case 6: //ANUAL
				string = getQtdefrequencia() + " ano(s)";
				break;
			case 7: //SEMESTRAL
				string = getQtdefrequencia() + " semestre(s)";
				break;
			case 8: //HORA
				string = getQtdefrequencia() + " hora(s)";
				break;
			case 9: //TRIMESTRAL
				string = getQtdefrequencia() + " trimestre(s)";
				break;
	
			default:
				break;
			}
		}
		
		return string;
	}
	
	@Transient
	public Boolean getIsServico() {
		return isServico;
	}
	
	public void setIsServico(Boolean isServico) {
		this.isServico = isServico;
	}

	@Transient
	public Boolean getChecado() {
		return checado;
	}
	
	public void setChecado(Boolean checado) {
		this.checado = checado;
	}
	
	@Transient
	public Double getValorunitarioForOrdemcompra() {
		if(qtdecot != null && valor != null && qtdefrequencia != null && qtdecot > 0 && valor > 0 && qtdefrequencia > 0){
			return (valor + (desconto != null ? desconto.getValue().doubleValue() : 0)) / (qtdecot*qtdefrequencia);
		} else return null;
	}
	
	@Transient
	public Double getValorunitario() {
		if(qtdecot != null && valor != null && qtdefrequencia != null && qtdecot > 0 && valor > 0 && qtdefrequencia > 0){
			return SinedUtil.roundByParametro((valor + (desconto != null ? desconto.getValue().doubleValue() : 0)) / (qtdecot*qtdefrequencia));
		} else return null;
	}
	
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	
	@Transient
	public String getNomematerial() {
		if(this.material != null)
			return this.material.getNome();
		return nomematerial;
	}
	
	public void setNomematerial(String nomematerial) {
		this.nomematerial = nomematerial;
	}
	
	@Transient
	public Money getUltvalor() {
		return ultvalor;
	}
	
	public void setUltvalor(Money ultvalor) {
		this.ultvalor = ultvalor;
	}
	
	@Transient
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Transient
	public CorValor getCorvalor() {
		return corvalor;
	}
	
	public void setCorvalor(CorValor corvalor) {
		this.corvalor = corvalor;
	}
	
	@Transient
	public CorValor getCorvalorult() {
		return corvalorult;
	}
	
	public void setCorvalorult(CorValor corvalorult) {
		this.corvalorult = corvalorult;
	}
	
	@Transient
	public String getNomeunidademedida() {
		if(this.material != null && this.material.getUnidademedida() != null)
			return this.material.getUnidademedida().getSimbolo();
		return nomeunidademedida;
	}
	
	public void setNomeunidademedida(String nomeunidademedida) {
		this.nomeunidademedida = nomeunidademedida;
	}

	@Transient
	public Integer getIndexFornecedor() {
		return indexFornecedor;
	}

	@Transient
	public Integer getIndexMaterial() {
		return indexMaterial;
	}

	public void setIndexFornecedor(Integer indexFornecedor) {
		this.indexFornecedor = indexFornecedor;
	}

	public void setIndexMaterial(Integer indexMaterial) {
		this.indexMaterial = indexMaterial;
	}

	@Transient	
	public String getLocalarmazenagemtrans() {
		
		String local = new String();
		if(this.localarmazenagemsolicitacao != null && this.localarmazenagemsolicitacao.getNome() != null)
			local = this.localarmazenagemsolicitacao.getNome();
		else if(this.localarmazenagem != null && this.localarmazenagem.getNome() != null)
			local = this.localarmazenagem.getNome();		
		return local ;
	}

	public void setLocalarmazenagemtrans(String localarmazenagemtrans) {
		this.localarmazenagemtrans = localarmazenagemtrans;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	@Transient
	public Unidademedida getUnidademedidatrans() {
		return unidademedidatrans;
	}

	public void setUnidademedidatrans(Unidademedida unidademedidatrans) {
		this.unidademedidatrans = unidademedidatrans;
	}

	public Money getDesconto() {
		return desconto;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}	
	
	@Transient
	public Double getValorSemDesconto() {
		if(this.desconto != null && this.valor != null){
			return this.valor + this.desconto.getValue().doubleValue();
		}
		return valor;
	}
	
	@Transient
	public Double getValorUnitarioSemDesconto() {
		if(qtdecot != null && valor != null && qtdefrequencia != null && qtdecot > 0 && valor > 0 && qtdefrequencia > 0){
			return (valor + (desconto != null ? desconto.getValue().doubleValue() : 0)) / (qtdecot*qtdefrequencia);
		} else return null;
	}

	@Transient
	public Boolean getOrdemcompragerada() {
		return ordemcompragerada;
	}
	public void setOrdemcompragerada(Boolean ordemcompragerada) {
		this.ordemcompragerada = ordemcompragerada;
	}
	@Transient
	public String getNomeclasseordemcompragerada() {
		if(this.ordemcompragerada == null || !this.ordemcompragerada) return "";
		return "ordemcompragerada";
	}
	
	public void setNomeclasseordemcompragerada(String nomeclasseordemcompragerada) {
		this.nomeclasseordemcompragerada = nomeclasseordemcompragerada;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
