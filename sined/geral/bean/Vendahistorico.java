package br.com.linkcom.sined.geral.bean;
 
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_vendahistorico",sequenceName="sq_vendahistorico")
public class Vendahistorico implements Log {

	protected Integer cdvendahistorico;
	protected Venda venda;
	protected String acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Usuario usuarioaltera;
	protected Empresa empresa;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_vendahistorico",strategy=GenerationType.AUTO)
	public Integer getCdvendahistorico() {
		return cdvendahistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	@Required
	public Venda getVenda() {
		return venda;
	}

	@DisplayName("A��o")
	@MaxLength(100)
	public String getAcao() {
		return acao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	public void setCdvendahistorico(Integer cdvendahistorico) {
		this.cdvendahistorico = cdvendahistorico;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	
	@Transient
	public Empresa getEmpresa() {
		if(venda != null && venda.getEmpresa() != null){
			empresa = venda.getEmpresa();
		}
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
