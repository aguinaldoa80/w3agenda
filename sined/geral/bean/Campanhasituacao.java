package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_campanhasituacao", sequenceName = "sq_campanhasituacao")
public class Campanhasituacao {
	
	protected Integer cdcampanhasituacao;
	protected String nome;
	
//	public static final Integer EM_ABERTO = 1;
//	public static final Integer FINALIZADA = 2;
//	public static final Integer CANCELADA = 3;
	public static Campanhasituacao EM_ABERTO = new Campanhasituacao(1, "Em Aberto");
	public static Campanhasituacao FINALIZADA = new Campanhasituacao(2, "Finalizada");
	public static Campanhasituacao CANCELADA = new Campanhasituacao(3, "Cancelada");
	
	public Campanhasituacao(){}
	
	public Campanhasituacao (Integer cdcampanhasituacao){
		this.cdcampanhasituacao = cdcampanhasituacao;
	}
	
	public Campanhasituacao(Integer cdcampanhasituacao, String nome) {
		this.cdcampanhasituacao = cdcampanhasituacao;
		this.nome = nome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campanhasituacao")
	public Integer getCdcampanhasituacao() {
		return cdcampanhasituacao;
	}
	
	@Required
	@MaxLength(20)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	
	public void setCdcampanhasituacao(Integer cdcampanhasituacao) {
		this.cdcampanhasituacao = cdcampanhasituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
