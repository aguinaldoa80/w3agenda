package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Veiculodespesadefinirprojeto {
	POR_DESPESA_DO_VEICULO(1, "Por Despesa do Ve�culo"),
	POR_ITEM_DA_DESPESA(2, "Por item da Despesa");
	
	private Integer cdveiculodespesadefinirprojeto;
	private String nome;
	
	private Veiculodespesadefinirprojeto(Integer cdveiculodespesadefinirprojeto, String nome) {
		this.cdveiculodespesadefinirprojeto = cdveiculodespesadefinirprojeto;
		this.nome = nome;
	}

	public Integer getCdveiculodespesadefinirprojeto() {
		return cdveiculodespesadefinirprojeto;
	}

	@DescriptionProperty
	public String getNome() {
		return nome;
	}


	public void setCdveiculodespesadefinirprojeto(
			Integer cdveiculodespesadefinirprojeto) {
		this.cdveiculodespesadefinirprojeto = cdveiculodespesadefinirprojeto;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome().toUpperCase();
	}	
}
