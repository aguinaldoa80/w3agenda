package br.com.linkcom.sined.geral.bean;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@Table(name="fornecedorhistorico")
@SequenceGenerator(name="sq_fornecedorhistorico", sequenceName="sq_fornecedorhistorico")
public class Fornecedorhistorico implements Serializable{

	private static final long serialVersionUID = 1L;
	protected Integer cdfornecedorhistorico;
	protected Usuario responsavel;
	protected Timestamp dtaltera;
	protected String observacao;
	protected Fornecedor fornecedor;
	
	public Fornecedorhistorico(){
		
	}
	
	@Id
	@Column(name="cdfornecedorhistorico")
	@GeneratedValue(generator = "sq_fornecedorhistorico", strategy = GenerationType.AUTO)
	public Integer getCdfornecedorhistorico() {
		return cdfornecedorhistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	@JoinColumn(name="cdusuarioaltera")
	@DisplayName("Responsável ")
	public Usuario getResponsavel() {
		return responsavel;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Column(name = "observacao")
	@DisplayName("Observação")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne
	@JoinColumn(name="cdfornecedor")
	@Required
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setCdfornecedorhistorico(Integer cdfornecedorhistorico) {
		this.cdfornecedorhistorico = cdfornecedorhistorico;
	}

	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
}
