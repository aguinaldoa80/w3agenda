package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_contadigital", sequenceName = "sq_contadigital")
public class Contadigital implements Log {

	protected Integer cdcontadigital;
	protected Conta conta;
	
	protected String apikey;
	protected String apisecret;
	protected String username;
	protected String pass;
	
	protected Double saldo;
	protected Timestamp dtsaldo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_contadigital")
	@DisplayName("Id")
	public Integer getCdcontadigital() {
		return cdcontadigital;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdconta")
	public Conta getConta() {
		return conta;
	}

	@DisplayName("API Key")
	public String getApikey() {
		return apikey;
	}

	@DisplayName("API Secret")
	public String getApisecret() {
		return apisecret;
	}

	@DisplayName("Usu�rio")
	public String getUsername() {
		return username;
	}

	@Password
	@DisplayName("Senha")
	public String getPass() {
		return pass;
	}

	@DisplayName("Saldo (R$)")
	public Double getSaldo() {
		return saldo;
	}

	@DisplayName("Data do saldo")
	public Timestamp getDtsaldo() {
		return dtsaldo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public void setDtsaldo(Timestamp dtsaldo) {
		this.dtsaldo = dtsaldo;
	}

	public void setCdcontadigital(Integer cdcontadigital) {
		this.cdcontadigital = cdcontadigital;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public void setApisecret(String apisecret) {
		this.apisecret = apisecret;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
}
