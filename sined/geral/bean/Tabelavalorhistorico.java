package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_tabelavalorhistorico", sequenceName = "sq_tabelavalorhistorico")
@DisplayName("Hist�rico da Tabela de valores")
public class Tabelavalorhistorico implements Log{
	
	protected Integer cdtabelavalorhistorico;
	protected Tabelavalor tabelavalor;
	protected String acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Usuario usuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tabelavalorhistorico")
	public Integer getCdtabelavalorhistorico() {
		return cdtabelavalorhistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtabelavalor")
	@Required
	public Tabelavalor getTabelavalor() {
		return tabelavalor;
	}
	@DisplayName("A��o")
	@MaxLength(100)
	public String getAcao() {
		return acao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}

	public void setCdtabelavalorhistorico(Integer cdtabelavalorhistorico) {
		this.cdtabelavalorhistorico = cdtabelavalorhistorico;
	}
	public void setTabelavalor(Tabelavalor tabelavalor) {
		this.tabelavalor = tabelavalor;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}

	//LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
