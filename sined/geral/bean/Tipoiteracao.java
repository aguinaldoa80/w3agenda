package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Tipoiteracao {
	private Integer cdtipoiteracao;
	private String descricao;
	
	@Id
	public Integer getCdtipoiteracao() {
		return cdtipoiteracao;
	}
	public void setCdtipoiteracao(Integer cdtipoiteracao) {
		this.cdtipoiteracao = cdtipoiteracao;
	}
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
