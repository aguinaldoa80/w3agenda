package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_rede", sequenceName = "sq_rede")
public class Rede implements Log{
	
	protected Integer cdrede;
	protected Bandaredecomercial bandaredecomercial;
	protected Redemascara redemascara;
	protected String enderecoip;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	protected Set<Redeservidorinterface> listaServidorinterface;
	protected Set<Redeip> listaRedeip;
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_rede")
	public Integer getCdrede() {
		return cdrede;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdbandaredecomercial")
    @DisplayName("Banda comercial")
	public Bandaredecomercial getBandaredecomercial() {
		return bandaredecomercial;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="cdredemascara")
    @DisplayName("M�scara")
	public Redemascara getRedemascara() {
		return redemascara;
	}

	@Required
	@MaxLength(25)
	@DisplayName("Endere�o IP")	
	@DescriptionProperty
	public String getEnderecoip() {
		return enderecoip;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	@OneToMany(mappedBy="rede")
	@DisplayName("Servidor interface")
	public Set<Redeservidorinterface> getListaServidorinterface() {
		return listaServidorinterface;
	}
	
	@OneToMany(mappedBy="rede")
	@DisplayName("IP")
	public Set<Redeip> getListaRedeip() {
		return listaRedeip;
	}

	public void setCdrede(Integer cdrede) {
		this.cdrede = cdrede;
	}

	public void setBandaredecomercial(Bandaredecomercial bandaredecomercial) {
		this.bandaredecomercial = bandaredecomercial;
	}

	public void setRedemascara(Redemascara redemascara) {
		this.redemascara = redemascara;
	}

	public void setEnderecoip(String enderecoip) {
		this.enderecoip = enderecoip;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	public void setListaServidorinterface(Set<Redeservidorinterface> listaServidorinterface) {
		this.listaServidorinterface = listaServidorinterface;
	}
	
	public void setListaRedeip(Set<Redeip> listaRedeip) {
		this.listaRedeip = listaRedeip;
	}
}
