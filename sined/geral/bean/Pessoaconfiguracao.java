package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Pessoaconfiguracaotipo;


@Entity
@SequenceGenerator(name = "sq_pessoaconfiguracao", sequenceName = "sq_pessoaconfiguracao")
public class Pessoaconfiguracao {

	protected Integer cdpessoaconfiguracao;
	protected Pessoa pessoa;
	protected Pessoaconfiguracaotipo tipo;
	protected Fornecedor transportador;

	public Pessoaconfiguracao() {}
	
	public Pessoaconfiguracao(Pessoa pessoa, Pessoaconfiguracaotipo tipo) {
		this.pessoa = pessoa;
		this.tipo = tipo;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoaconfiguracao")
	public Integer getCdpessoaconfiguracao() {
		return cdpessoaconfiguracao;
	}
	public void setCdpessoaconfiguracao(Integer cdpessoaconfiguracao) {
		this.cdpessoaconfiguracao = cdpessoaconfiguracao;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public Pessoaconfiguracaotipo getTipo() {
		return tipo;
	}

	public void setTipo(Pessoaconfiguracaotipo tipo) {
		this.tipo = tipo;
	}

	@DisplayName("Transportador")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdtransportador")
	public Fornecedor getTransportador() {
		return transportador;
	}
	public void setTransportador(Fornecedor transportador) {
		this.transportador = transportador;
	}
}