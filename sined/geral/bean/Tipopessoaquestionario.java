package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Tipopessoaquestionario {
	
	CLIENTE(0, "Cliente"),
	COLABORADOR(1, "Colaborador"),
	FORNECEDOR(2, "Fornecedor"),
	SERVICO(3, "Servi�o");
	
	private Tipopessoaquestionario (Integer value, String tipo){
		this.tipo = tipo;
		this.value = value;
	}
	
	private Integer value;
	private String tipo;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}

}
