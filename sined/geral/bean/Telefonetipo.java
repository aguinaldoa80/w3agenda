package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Telefonetipo {

	public static final Integer PRINCIPAL = 1;
	public static final Integer FAX = 2;
	public static final Integer CELULAR = 3;
	public static final Integer RESIDENCIAL = 4;
	public static final Integer COMERCIAL = 5;
	public static final Integer OUTROS = 6;
	
	protected Integer cdtelefonetipo;
	protected String nome;
	
	public Telefonetipo(){}
	
	public Telefonetipo(Integer cdtelefonetipo){
		this.cdtelefonetipo = cdtelefonetipo;
	}
	
	public Telefonetipo(Integer cdtelefonetipo, String nome){
		this.cdtelefonetipo = cdtelefonetipo;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdtelefonetipo() {
		return cdtelefonetipo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdtelefonetipo(Integer cdtelefonetipo) {
		this.cdtelefonetipo = cdtelefonetipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
