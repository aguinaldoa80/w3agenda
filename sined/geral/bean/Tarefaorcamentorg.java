package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporecursogeral;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_tarefaorcamentorg",sequenceName="sq_tarefaorcamentorg")
public class Tarefaorcamentorg implements Log{

	protected Integer cdtarefaorcamentorg;
	protected Tarefaorcamento tarefaorcamento;
	protected Material material;
	protected Double quantidade;
	protected Tiporecursogeral tiporecursogeral;
	protected String outro;
	protected Unidademedida unidademedida;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTE PARA O FLEX
	protected Integer tiporecursogeralflex;
	
	@Id
	@GeneratedValue(generator="sq_tarefaorcamentorg",strategy=GenerationType.AUTO)
	public Integer getCdtarefaorcamentorg() {
		return cdtarefaorcamentorg;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefaorcamento")
	public Tarefaorcamento getTarefaorcamento() {
		return tarefaorcamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public String getOutro() {
		return outro;
	}
	public Tiporecursogeral getTiporecursogeral() {
		return tiporecursogeral;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setTiporecursogeral(Tiporecursogeral tiporecursogeral) {
		this.tiporecursogeral = tiporecursogeral;
	}
	public void setOutro(String outro) {
		this.outro = outro;
	}
	public void setCdtarefaorcamentorg(Integer cdtarefaorcamentorg) {
		this.cdtarefaorcamentorg = cdtarefaorcamentorg;
	}
	public void setTarefaorcamento(Tarefaorcamento tarefaorcamento) {
		this.tarefaorcamento = tarefaorcamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public Integer getTiporecursogeralflex() {
		return tiporecursogeral != null ? tiporecursogeral.ordinal() : 0;
	}
	
	public void setTiporecursogeralflex(Integer tiporecursogeralflex) {
		if(tiporecursogeralflex == null) tiporecursogeralflex = 0;
		this.tiporecursogeral = Tiporecursogeral.values()[tiporecursogeralflex];
	}
}
