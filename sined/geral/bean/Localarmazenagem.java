package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_localarmazenagem", sequenceName = "sq_localarmazenagem")
@DisplayName("Local Armazenagem")
public class Localarmazenagem implements Log{

	protected Integer cdlocalarmazenagem;
	protected String nome;
	protected Endereco endereco;
	protected Boolean clientelocacao;
	protected Boolean ativo;
	protected Boolean principal;
	protected Boolean indenizacao;
	protected Double qtdeacimaestoque;
	protected String msgacimaestoque;
	protected Boolean permitirestoquenegativo;
	protected Centrocusto centrocusto;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	protected Set<Localarmazenagemempresa> listaempresa = new ListSet<Localarmazenagemempresa>(Localarmazenagemempresa.class);
	
	protected Double qtde;
	
	//TRANSIENT
	protected Double qtddisponivel;
	
	public Localarmazenagem(){
		if(this.cdlocalarmazenagem == null){
			this.ativo = true;
		}
	}
	
	public Localarmazenagem(Integer cdlocalarmazenagem){
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	
	public Localarmazenagem(String nome){
		this.nome = nome;
	}
	
	public Localarmazenagem(Integer cdlocalarmazenagem, String nome){
		this.cdlocalarmazenagem = cdlocalarmazenagem;
		this.nome = nome;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_localarmazenagem")
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinColumn(name="cdendereco")
	public Endereco getEndereco() {
		return endereco;
	}
	@DisplayName("Cliente/Loca��o")
	public Boolean getClientelocacao() {
		return clientelocacao;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@OneToMany(mappedBy="localarmazenagem")
	public Set<Localarmazenagemempresa> getListaempresa() {
		return listaempresa;
	}
	@DisplayName("Qtde. acima do estoque")
	public Double getQtdeacimaestoque() {
		return qtdeacimaestoque;
	}
	@MaxLength(100)
	@DisplayName("Mensagem se quantidade limite excedida no estoque")
	public String getMsgacimaestoque() {
		return msgacimaestoque;
	}
	@DisplayName("Centro de custo padr�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setQtdeacimaestoque(Double qtdeacimaestoque) {
		this.qtdeacimaestoque = qtdeacimaestoque;
	}
	public void setMsgacimaestoque(String msgacimaestoque) {
		this.msgacimaestoque = msgacimaestoque;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setClientelocacao(Boolean clientelocacao) {
		this.clientelocacao = clientelocacao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaempresa(Set<Localarmazenagemempresa> listaempresa) {
		this.listaempresa = listaempresa;
	}
	
	//===================== TRANSIENT'S ===================================
	
	protected List<Empresa> listaEmpresa;
	
	@Transient
	@DisplayName("Empresa")
	public List<Empresa> getListaEmpresa() {
		return listaEmpresa;
	}
	public void setListaEmpresa(List<Empresa> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	
	@Transient
	public Double getQtde() {
		return qtde;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	/** Concatena o endere�o para ser mostrado na listagem em 1 coluna
	 *  Logradour, n�mero / complemento
	 * 
	 * @return
	 * @author Tom�s Rabelo
	 */
	@Transient
	@DisplayName("Endere�o")
	public String getEnderecoAux(){
		String endereco = "";
		if(this.endereco != null){
			endereco += this.endereco.getLogradouro() != null ? this.endereco.getLogradouro() : "";
			endereco += this.endereco.getNumero() != null ? ", " + this.endereco.getNumero() : "";
			endereco += this.endereco.getComplemento() != null ? " / " + this.endereco.getComplemento() : "";
		}
		return endereco;
	}
	
	/** Concatena a localidade para ser mostrado na listagem em 1 coluna
	 *  Munic�pio + UF
	 *  
	 * @return
	 * @author Tom�s Rabelo
	 */
	@Transient
	public String getLocalidade(){
		return this.getEndereco() != null && this.getEndereco().getMunicipio() != null && this.getEndereco().getMunicipio().getUf() != null ? this.getEndereco().getMunicipio().getNome() + "/" +this.getEndereco().getMunicipio().getUf().getNome() : "";
	}
	
	@Transient
	public String getEnderecoCompleto(){
		if(this.getEndereco() != null){
			
			String logradouroCompleto = " - ";  
			logradouroCompleto += this.getEndereco().logradouro != null && !this.getEndereco().logradouro.trim().equals("") ? this.getEndereco().logradouro : "";
			logradouroCompleto += this.getEndereco().numero != null && !this.getEndereco().numero.trim().equals("") ? ", " + this.getEndereco().numero : "";
			logradouroCompleto += this.getEndereco().complemento != null && !this.getEndereco().complemento.trim().equals("") ? ", " + this.getEndereco().complemento : "";
			logradouroCompleto += this.getEndereco().bairro != null && !this.getEndereco().bairro.trim().equals("") ? ", " + this.getEndereco().bairro : "";
			logradouroCompleto += this.getEndereco().municipio != null ? ", " + this.getEndereco().municipio.getNome() : "";
			logradouroCompleto += this.getEndereco().municipio != null && this.getEndereco().municipio.getUf() != null ? "/ " + this.getEndereco().municipio.getUf().getSigla() : "";
			
			return logradouroCompleto;
		} else return "";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdlocalarmazenagem == null) ? 0 : cdlocalarmazenagem
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localarmazenagem other = (Localarmazenagem) obj;
		if (cdlocalarmazenagem == null) {
			if (other.cdlocalarmazenagem != null)
				return false;
		} else if (!cdlocalarmazenagem.equals(other.cdlocalarmazenagem))
			return false;
		return true;
	}
	
	
	public Boolean getPrincipal() {
		return principal;
	}

	@DisplayName("Indeniza��o")
	public Boolean getIndenizacao() {
		return indenizacao;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public void setIndenizacao(Boolean indenizacao) {
		this.indenizacao = indenizacao;
	}

	@DisplayName("Permitir estoque negativo")
	public Boolean getPermitirestoquenegativo() {
		return permitirestoquenegativo;
	}

	public void setPermitirestoquenegativo(Boolean permitirestoquenegativo) {
		this.permitirestoquenegativo = permitirestoquenegativo;
	}
	
	@Transient
	public Double getQtddisponivel() {
		return qtddisponivel;
	}

	public void setQtddisponivel(Double qtddisponivel) {
		this.qtddisponivel = qtddisponivel;
	}
	
	
}