package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dominioemailusuario", sequenceName = "sq_dominioemailusuario")
@DisplayName("E-mail")
public class Dominioemailusuario implements Log{
	
	protected Integer cddominioemailusuario;	
	protected Contratomaterial contratomaterial;
	protected Dominio dominio;
	
	protected String usuario;
	protected String email;
	protected String senha;
	protected String diretorio;
	protected String temporario;
	protected String maildir;
	protected String cota;
	protected String acesso;
	protected Boolean ativo = true;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
		
	//Transient
	protected Cliente cliente;
	protected String unidademedida;
	
	public Dominioemailusuario() {
	}
	
	public Dominioemailusuario(Integer cddominioemailusuario) {
		this.cddominioemailusuario = cddominioemailusuario;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominioemailusuario")
	public Integer getCddominioemailusuario() {
		return cddominioemailusuario;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominio")
	@DisplayName("Dom�nio")
	public Dominio getDominio() {
		return dominio;
	}
	
	@Required	
	@MaxLength(50)
	@DisplayName("Nome")
	public String getUsuario() {
		return usuario;
	}
	
	@MaxLength(150)
	public String getEmail() {
		return email;
	}

	@Required
	@MaxLength(50)
	public String getSenha() {
		return senha;
	}
	
	@MaxLength(255)
	public String getDiretorio() {
		return diretorio;
	}

	@MaxLength(50)
	public String getTemporario() {
		return temporario;
	}

	@MaxLength(10)
	public String getMaildir() {
		return maildir;
	}

	@Required
	@MaxLength(50)
	public String getCota() {
		return cota;
	}

	@MaxLength(50)
	public String getAcesso() {
		return acesso;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setCddominioemailusuario(Integer cddominioemailusuario) {
		this.cddominioemailusuario = cddominioemailusuario;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setDominio(Dominio dominio) {
		this.dominio = dominio;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	public void setTemporario(String temporario) {
		this.temporario = temporario;
	}

	public void setMaildir(String maildir) {
		this.maildir = maildir;
	}

	public void setCota(String cota) {
		this.cota = cota;
	}

	public void setAcesso(String acesso) {
		this.acesso = acesso;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	@Transient
	@Required
	public Cliente getCliente() {
		return cliente;
	}	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	public String getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}		
}
