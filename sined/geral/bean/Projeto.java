package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaoprojeto;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_projeto", sequenceName = "sq_projeto")
@JoinEmpresa("projeto.empresa")
public class Projeto implements Log, PermissaoProjeto, PermissaoClienteEmpresa{

	protected Integer cdprojeto;
	protected String nome;
	protected Cnpj cnpj;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Date dtprojeto;
	protected Date dtfimprojeto;
	protected Colaborador colaborador;
	protected Cliente cliente;
	protected Municipio municipio;
	protected String sigla;
	protected String endereco;
	protected Empresa empresa;
	protected Centrocusto centrocusto;
	protected String cei;
	protected String art;
	protected Proposta proposta;
	protected Localarmazenagem localarmazenagem;
	protected Situacaoprojeto situacao = Situacaoprojeto.EM_ESPERA;
	protected Projetotipo projetotipo;
	protected Oportunidade oportunidade;
	protected ReportTemplateBean modelodiarioobra;
	protected Double area;
	
	//TRANSIENTE
	protected Integer totalsemana;
	protected Double percentual;
	
	protected List<Projetoitem> listaProjetoitem = new ListSet<Projetoitem>(Projetoitem.class);
	protected List<Rateioitem> listaRateioitem;
	protected List<Projetoarquivo> listaProjetoarquivo = new ListSet<Projetoarquivo>(Projetoarquivo.class);
	protected List<Planejamento> listaPlanejamento;
	protected List<Projetohistorico> listaProjetohistorico = new ListSet<Projetohistorico>(Projetohistorico.class);
	protected List<Projetodespesa> listaProjetodespesa = new ListSet<Projetodespesa>(Projetodespesa.class);
	protected List<Usuarioprojeto> listaUsuarioprojeto = new ArrayList<Usuarioprojeto>();

	public Projeto() {
	}
	public Projeto(String nome) {
		this.nome = nome;
	}
	public Projeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_projeto")
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public void setCdprojeto(Integer id) {
		this.cdprojeto = id;
	}

	@Required
	@MaxLength(100)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getNome() {
		return nome;
	}
	
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}
	
	@DisplayName("Tipo de projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetotipo")
	public Projetotipo getProjetotipo() {
		return projetotipo;
	}
	
	public void setProjetotipo(Projetotipo projetotipo) {
		this.projetotipo = projetotipo;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}	
	
	@DisplayName("Data de in�cio")
	@Required
	public Date getDtprojeto() {
		return dtprojeto;
	}
	
	public void setDtprojeto(Date dtprojeto) {
		this.dtprojeto = dtprojeto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@DisplayName("CEI")
	public String getCei() {
		return cei;
	}
	@MaxLength(200)
	@DisplayName("ART")
	public String getArt() {
		return art;
	}
	public void setArt(String art) {
		this.art = art;
	}
	public void setCei(String cei) {
		this.cei = cei;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@OneToMany(mappedBy="projeto")
	@DisplayName("Arquivo")
	public List<Projetoarquivo> getListaProjetoarquivo() {
		return listaProjetoarquivo;
	}
	
	@OneToMany(mappedBy="projeto")
	public List<Projetoitem> getListaProjetoitem() {
		return listaProjetoitem;
	}
	
	@OneToMany(mappedBy="projeto")
	@DisplayName("Hist�rico")
	public List<Projetohistorico> getListaProjetohistorico() {
		return listaProjetohistorico;
	}
	
	@MaxLength(20)
	public String getSigla() {
		return sigla;
	}
	
	@DisplayName("Modelo do Di�rio de Obra")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmodelodiarioobra")
	public ReportTemplateBean getModelodiarioobra() {
		return modelodiarioobra;
	}
	
	public void setModelodiarioobra(ReportTemplateBean modelodiarioobra) {
		this.modelodiarioobra = modelodiarioobra;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	public void setListaProjetoitem(List<Projetoitem> listaProjetoitem) {
		this.listaProjetoitem = listaProjetoitem;
	}
	
	public void setListaProjetoarquivo(List<Projetoarquivo> listaProjetoarquivo) {
		this.listaProjetoarquivo = listaProjetoarquivo;
	}
	
	public void setListaProjetohistorico(List<Projetohistorico> listaProjetohistorico) {
		this.listaProjetohistorico = listaProjetohistorico;
	}
	
	@OneToMany(mappedBy="projeto")
	public List<Rateioitem> getListaRateioitem() {
		return listaRateioitem;
	}
	
	public void setListaRateioitem(List<Rateioitem> listaRateioitem) {
		this.listaRateioitem = listaRateioitem;
	}
	
	@OneToMany(mappedBy="projeto")
	public List<Planejamento> getListaPlanejamento() {
		return listaPlanejamento;
	}
	
	public void setListaPlanejamento(List<Planejamento> listaPlanejamento) {
		this.listaPlanejamento = listaPlanejamento;
	}
	
	@OneToMany(mappedBy="projeto")
	@DisplayName("Despesas")
	public List<Projetodespesa> getListaProjetodespesa() {
		return listaProjetodespesa;
	}
	public void setListaProjetodespesa(List<Projetodespesa> listaProjetodespesa) {
		this.listaProjetodespesa = listaProjetodespesa;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@DisplayName("Endere�o")
	@MaxLength(200)
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	@DisplayName("Data de t�rmino")
	public Date getDtfimprojeto() {
		return dtfimprojeto;
	}
	
	public void setDtfimprojeto(Date dtfimprojeto) {
		this.dtfimprojeto = dtfimprojeto;
	}
	
	@DisplayName("Proposta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproposta")
	public Proposta getProposta() {
		return proposta;
	}
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}	
	
	@DisplayName("Local de entrega de compras")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Situa��o")
	@Required
	public Situacaoprojeto getSituacao() {
		return situacao;
	}
	public void setSituacao(Situacaoprojeto situacao) {
		this.situacao = situacao;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	//TRANSIENTE
	@DisplayName("Total de semana(s)")
	@Transient
	public Integer getTotalsemana() {
		return totalsemana;		
	}
	public void setTotalsemana(Integer totalsemana) {
		this.totalsemana = totalsemana;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdprojeto == null) ? 0 : cdprojeto.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Projeto))
			return false;
		final Projeto other = (Projeto) obj;
		if (cdprojeto == null) {
			if (other.cdprojeto != null)
				return false;
		} else if (!cdprojeto.equals(other.cdprojeto))
			return false;
		return true;
	}
	
	@Transient
	public String getLocalidade() {
		return getMunicipio() != null ? getMunicipio().getNome() + "/" + getMunicipio().getUf().getSigla() : "";
	}
	
	@Override
	public String toString() {
		// Propriedade anotada com DescriptionProperty
		Object description = new BeanDescriptorFactoryImpl().createBeanDescriptor(this).getDescription();
		
		return description != null ? description.toString() : super.toString();
	}
	public String subQueryProjeto() {
		return SinedUtil.getListaProjeto();
//		return "select projeto.cdprojeto " +
//				"from Projeto projeto";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select projetoSubQueryClienteEmpresa.cdprojeto " +
				"from Projeto projetoSubQueryClienteEmpresa " +
				"left outer join projetoSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public Double getPercentual() {
		return percentual;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	@OneToMany(mappedBy="projeto")
	public List<Usuarioprojeto> getListaUsuarioprojeto() {
		return listaUsuarioprojeto;
	}
	public void setListaUsuarioprojeto(List<Usuarioprojeto> listaUsuarioprojeto) {
		this.listaUsuarioprojeto = listaUsuarioprojeto;
	}
	
	@DisplayName("Oportunidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	
	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}	
	
	@DisplayName("�rea (ha)")
	public Double getArea() {
		return area;
	}
	
	public void setArea(Double area) {
		this.area = area;
	}
}
