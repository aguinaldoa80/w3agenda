package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_atividadetipo",sequenceName="sq_atividadetipo")
public class Atividadetipo implements Log{

	protected Integer cdatividadetipo;
	protected String nome;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Colaborador colaborador;
	protected Boolean atividadepadrao;
	protected Boolean reclamacao;
	protected Boolean cobranca;
	protected List<Atividadetipoitem> listaAtividadetipoitem;
	protected List<Atividadetipomodelo> listaAtividadetipomodelo;
	protected List<Atividadetiporesponsavel> listaAtividadetiporesponsavel;
	protected List<Requisicao> listaRequisicao;
	protected Boolean naoenviaremailcliente;
	protected Integer prazoconclusao;
	protected String mensagememail;
	protected String emailextraenvioos;
	protected Boolean naoexibirnopainel;
	protected Boolean aplicacaoservico;
	protected Boolean aplicacaocrm;
	{
		this.ativo = Boolean.TRUE;
	}
	
	public Atividadetipo(){
	}
	
	public Atividadetipo(Integer cdatividadetipo){
		this.cdatividadetipo = cdatividadetipo;
	}
	
	public Atividadetipo(String nome){
		this.nome = nome;
	}
	
	@Id
	@GeneratedValue(generator="sq_atividadetipo", strategy=GenerationType.AUTO)
	public Integer getCdatividadetipo() {
		return cdatividadetipo;
	}
	@DisplayName("Descri��o")
	@DescriptionProperty
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Respons�vel")
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getColaborador() {
		return colaborador;
	}
	@DisplayName("Campos Extras para O.S.")
	@OneToMany(mappedBy="atividadetipo")
	public List<Atividadetipoitem> getListaAtividadetipoitem() {
		return listaAtividadetipoitem;
	}
	@DisplayName("Modelos")
	@OneToMany(mappedBy="atividadetipo")
	public List<Atividadetipomodelo> getListaAtividadetipomodelo() {
		return listaAtividadetipomodelo;
	}
	@DisplayName("Respons�vel")
	@OneToMany(mappedBy="atividadetipo")
	public List<Atividadetiporesponsavel> getListaAtividadetiporesponsavel() {
		return listaAtividadetiporesponsavel;
	}
	@OneToMany(mappedBy="atividadetipo")
	public List<Requisicao> getListaRequisicao() {
		return listaRequisicao;
	}
	@DisplayName("Atividade padr�o em OS")
	public Boolean getAtividadepadrao() {
		return atividadepadrao;
	}
	@DisplayName("Reclama��o")
	public Boolean getReclamacao() {
		return reclamacao;
	}
	@DisplayName("Cobran�a")
	public Boolean getCobranca() {
		return cobranca;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("N�o enviar e-mail ao cliente")
	public Boolean getNaoenviaremailcliente(){
		return naoenviaremailcliente;
	}
	@DisplayName("Prazo�para Conclus�o�(dias)")
	public Integer getPrazoconclusao() {
		return prazoconclusao;
	}
	@DisplayName("Email extra para envio de OS")
	@MaxLength(200)
	public String getEmailextraenvioos() {
		return emailextraenvioos;
	}

	public void setEmailextraenvioos(String emailextraenvioos) {
		this.emailextraenvioos = emailextraenvioos;
	}
	@DisplayName("Mensagem para Email")
	public String getMensagememail() {
		return mensagememail;
	}

	public void setPrazoconclusao(Integer prazoconclusao) {
		this.prazoconclusao = prazoconclusao;
	}

	public void setCdatividadetipo(Integer cdatividadetipo) {
		this.cdatividadetipo = cdatividadetipo;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setListaAtividadetipoitem(List<Atividadetipoitem> listaAtividadetipoitem) {
		this.listaAtividadetipoitem = listaAtividadetipoitem;
	}
	public void setListaAtividadetipomodelo(List<Atividadetipomodelo> listaAtividadetipomodelo) {
		this.listaAtividadetipomodelo = listaAtividadetipomodelo;
	}
	public void setListaAtividadetiporesponsavel(
			List<Atividadetiporesponsavel> listaAtividadetiporesponsavel) {
		this.listaAtividadetiporesponsavel = listaAtividadetiporesponsavel;
	}
	public void setListaRequisicao(List<Requisicao> listaRequisicao) {
		this.listaRequisicao = listaRequisicao;
	}
	public void setAtividadepadrao(Boolean atividadepadrao) {
		this.atividadepadrao = atividadepadrao;
	}
	public void setReclamacao(Boolean reclamacao) {
		this.reclamacao = reclamacao;
	}
	public void setCobranca(Boolean cobranca) {
		this.cobranca = cobranca;
	}
	public void setNaoenviaremailcliente(Boolean naoenviaremailcliente){
		this.naoenviaremailcliente = naoenviaremailcliente;
	}
	public void setMensagememail(String mensagememail) {
		this.mensagememail = mensagememail;
	}
	
	@DisplayName("N�o exibir no painel")
	public Boolean getNaoexibirnopainel() {
		return naoexibirnopainel;
	}
	public void setNaoexibirnopainel(Boolean naoexibirnopainel) {
		this.naoexibirnopainel = naoexibirnopainel;
	}
	
	@DisplayName("Servi�os")
	public Boolean getAplicacaoservico() {
		return aplicacaoservico;
	}
	public void setAplicacaoservico(Boolean aplicacaoservico) {
		this.aplicacaoservico = aplicacaoservico;
	}
	
	@DisplayName("CRM")
	public Boolean getAplicacaocrm() {
		return aplicacaocrm;
	}
	public void setAplicacaocrm(Boolean aplicacaocrm) {
		this.aplicacaocrm = aplicacaocrm;
	}

	@Override
	public boolean equals(Object obj) {
		try {
			return getCdatividadetipo().equals(((Atividadetipo) obj).getCdatividadetipo());
		} catch (Exception e) {
			return false;
		}
	}
}