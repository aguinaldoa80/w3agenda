package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_codigotributacaoitemlistaservico", sequenceName = "sq_codigotributacaoitemlistaservico")
public class Codigotributacaoitemlistaservico {

	protected Integer cdcodigotributacaoitemlistaservico;
	protected Codigotributacao codigotributacao;
	protected Itemlistaservico itemlistaservico;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_codigotributacaoitemlistaservico")
	public Integer getCdcodigotributacaoitemlistaservico() {
		return cdcodigotributacaoitemlistaservico;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigotributacao")
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cditemlistaservico")
	@DisplayName("Item da lista servi�o")
	public Itemlistaservico getItemlistaservico() {
		return itemlistaservico;
	}

	public void setCdcodigotributacaoitemlistaservico(
			Integer cdcodigotributacaoitemlistaservico) {
		this.cdcodigotributacaoitemlistaservico = cdcodigotributacaoitemlistaservico;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}

	public void setItemlistaservico(Itemlistaservico itemlistaservico) {
		this.itemlistaservico = itemlistaservico;
	}
	
	// TRANSIENTE
	
	@Transient
	public String getCodigoCodigotributacaoEscape(){
		return Util.strings.addScapesToDescription(codigotributacao.getCodigo());
	}
	
	@Transient
	public String getCodigoItemlistaservicoEscape(){
		return Util.strings.addScapesToDescription(itemlistaservico.getCodigo());
	}
	
	@Transient
	public String getDescricaoCodigotributacaoEscape(){
		return Util.strings.addScapesToDescription(codigotributacao.getDescricao());
	}
	
	@Transient
	public String getDescricaoItemlistaservicoEscape(){
		return Util.strings.addScapesToDescription(itemlistaservico.getDescricao());
	}
	
}
