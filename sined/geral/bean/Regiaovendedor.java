package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_regiaovendedor", sequenceName = "sq_regiaovendedor")
public class Regiaovendedor {

	protected Integer cdregiaovendedor;
	protected Regiao regiao;
	protected Colaborador colaborador;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_regiaovendedor")
	public Integer getCdregiaovendedor() {
		return cdregiaovendedor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdregiao")
	public Regiao getRegiao() {
		return regiao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	public void setCdregiaovendedor(Integer cdregiaovendedor) {
		this.cdregiaovendedor = cdregiaovendedor;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
}
