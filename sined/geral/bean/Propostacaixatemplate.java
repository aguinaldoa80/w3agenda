package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_propostacaixatemplate",sequenceName="sq_propostacaixatemplate")
public class Propostacaixatemplate {

	protected Integer cdpropostacaixatemplate;
	protected Propostacaixa propostacaixa;
	protected ReportTemplateBean reporttemplate;
	
	@Id
	@GeneratedValue(generator="sq_propostacaixatemplate",strategy=GenerationType.AUTO)
	public Integer getCdpropostacaixatemplate() {
		return cdpropostacaixatemplate;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpropostacaixa")
	public Propostacaixa getPropostacaixa() {
		return propostacaixa;
	}
	
	@Required
	@DisplayName("Template")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdreporttemplate")
	public ReportTemplateBean getReporttemplate() {
		return reporttemplate;
	}
	
	public void setReporttemplate(ReportTemplateBean reporttemplate) {
		this.reporttemplate = reporttemplate;
	}
	
	public void setCdpropostacaixatemplate(Integer cdpropostacaixatemplate) {
		this.cdpropostacaixatemplate = cdpropostacaixatemplate;
	}
	
	public void setPropostacaixa(Propostacaixa propostacaixa) {
		this.propostacaixa = propostacaixa;
	}
	
}
