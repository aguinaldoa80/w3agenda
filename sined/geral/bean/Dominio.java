package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_dominio", sequenceName = "sq_dominio")
@DisplayName("Dom�nio")
public class Dominio implements Log{
	
	protected Integer cddominio;
	protected Contratomaterial contratomaterial;
	protected Boolean reverso;
	protected Dominio dominioprincipal;
	protected String nome;
	protected Integer ttl;
	protected Integer versao;
	protected Integer atualizacao;
	protected Integer timeout;
	protected Integer expiracao;
	protected Integer ttlsecundario;	
	protected Boolean email;
	protected Boolean publico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Dominiozona> listaDominiozona;
	protected Set<Dominioemailalias> listaEmailalias;
	
	//Transient
	protected Cliente cliente;
		
	public Dominio() {
	}

	public Dominio(Integer cddominio) {
		this.cddominio = cddominio;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dominio")
	public Integer getCddominio() {
		return cddominio;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	public Boolean getReverso() {
		return reverso;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddominioprincipal")
	@DisplayName("Dom�nio principal")
	public Dominio getDominioprincipal() {
		return dominioprincipal;
	}

	@Required
	@DescriptionProperty
	@MaxLength(40)	
	public String getNome() {
		return nome;
	}

	@Required
	@MaxLength(6)
	@DisplayName("TTL")
	public Integer getTtl() {
		return ttl;
	}

	@DisplayName("Vers�o")
	public Integer getVersao() {
		return versao;
	}

	@Required
	@MaxLength(6)
	@DisplayName("Atualiza��o")
	public Integer getAtualizacao() {
		return atualizacao;
	}

	@Required
	@MaxLength(6)
	@DisplayName("TimeOut")
	public Integer getTimeout() {
		return timeout;
	}

	@Required
	@MaxLength(6)
	@DisplayName("Expira��o")
	public Integer getExpiracao() {
		return expiracao;
	}

	@Required
	@MaxLength(6)
	@DisplayName("TTL secund�rio")
	public Integer getTtlsecundario() {
		return ttlsecundario;
	}
	
	@DisplayName("E-mail")
	public Boolean getEmail() {
		return email;
	}
	
	@DisplayName("P�blico")
	public Boolean getPublico() {
		return publico;
	}

	@DisplayName("Zona")
	@OneToMany(mappedBy="dominio")
	public Set<Dominiozona> getListaDominiozona() {
		return listaDominiozona;
	}
	
	@DisplayName("E-mail alias")
	@OneToMany(mappedBy="dominio")
	public Set<Dominioemailalias> getListaEmailalias() {
		return listaEmailalias;
	}
	
	public void setCddominio(Integer cddominio) {
		this.cddominio = cddominio;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setDominioprincipal(Dominio dominioprincipal) {
		this.dominioprincipal = dominioprincipal;
	}
	
	public void setReverso(Boolean reverso) {
		this.reverso = reverso;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTtl(Integer ttl) {
		this.ttl = ttl;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public void setAtualizacao(Integer atualizacao) {
		this.atualizacao = atualizacao;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public void setExpiracao(Integer expiracao) {
		this.expiracao = expiracao;
	}

	public void setTtlsecundario(Integer ttlsecundario) {
		this.ttlsecundario = ttlsecundario;
	}
	
	public void setEmail(Boolean email) {
		this.email = email;
	}
	
	public void setPublico(Boolean publico) {
		this.publico = publico;
	}
	
	public void setListaDominiozona(Set<Dominiozona> listaDominiozona) {
		this.listaDominiozona = listaDominiozona;
	}
	
	public void setListaEmailalias(Set<Dominioemailalias> listaEmailalias) {
		this.listaEmailalias = listaEmailalias;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	@Transient
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
