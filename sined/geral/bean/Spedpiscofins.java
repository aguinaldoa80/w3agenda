package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Indicadorincidenciatributaria;
import br.com.linkcom.sined.geral.bean.enumeration.Metodoapropriacaocredito;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_spedpiscofins", sequenceName = "sq_spedpiscofins")
public class Spedpiscofins implements Log{

	protected Integer cdspedpiscofins;
	protected Arquivo arquivo;
	protected Date dtinicio;
	protected Date dtfim;
	protected Empresa empresa;
	protected Indicadorincidenciatributaria indicadorincidenciatributaria;
	protected Metodoapropriacaocredito metodoapropriacaocredito;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_spedpiscofins")
	public Integer getCdspedpiscofins() {
		return cdspedpiscofins;
	}
	
	@DisplayName("Arquivo SPED")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Data de In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data Final")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Indicador da incid�ncia tribut�ria no per�odo")
	public Indicadorincidenciatributaria getIndicadorincidenciatributaria() {
		return indicadorincidenciatributaria;
	}
	@DisplayName("M�todo de apropria��o de cr�ditos")
	public Metodoapropriacaocredito getMetodoapropriacaocredito() {
		return metodoapropriacaocredito;
	}	

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCdspedpiscofins(Integer cdspedpiscofins) {
		this.cdspedpiscofins = cdspedpiscofins;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setIndicadorincidenciatributaria(
			Indicadorincidenciatributaria indicadorincidenciatributaria) {
		this.indicadorincidenciatributaria = indicadorincidenciatributaria;
	}

	public void setMetodoapropriacaocredito(
			Metodoapropriacaocredito metodoapropriacaocredito) {
		this.metodoapropriacaocredito = metodoapropriacaocredito;
	}
}
