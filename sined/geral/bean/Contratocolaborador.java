package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_contratocolaborador",sequenceName="sq_contratocolaborador")
@DisplayName("Colaborador")
public class Contratocolaborador{

	protected Integer cdcontratocolaborador;
	protected Contrato contrato;
	protected Colaborador colaborador;
	protected Escala escala;
	protected Date dtinicio;
	protected Date dtfim;
	protected Hora horainicio;
	protected Hora horafim;
	protected Comissionamento comissionamento;
	protected Integer ordemcomissao;
	protected Money valorhora;
	
	// TRANSIENTE
	protected Boolean isVendedor;
	protected Double percentual;
	protected Double valor;
	
	public Contratocolaborador() {
	}
	
	public Contratocolaborador(Contrato contrato, Escala escala, Date dtinicio,
			Date dtfim, Hora horainicio, Hora horafim) {
		this.contrato = contrato;
		this.escala = escala;
		this.dtinicio = dtinicio;
		this.dtfim = dtfim;
		this.horainicio = horainicio;
		this.horafim = horafim;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratocolaborador")
	public Integer getCdcontratocolaborador() {
		return cdcontratocolaborador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescala")
	public Escala getEscala() {
		return escala;
	}
		
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
		
	@DisplayName("Hor�rio fim")
	public Hora getHorafim() {
		return horafim;
	}
	
	@DisplayName("Comiss�o")
	@JoinColumn(name="cdcomissionamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	
	
	@DisplayName("Hor�rio in�cio")
	public Hora getHorainicio() {
		return horainicio;
	}
	
	@DisplayName ("Ordem Comiss�o")
	public Integer getOrdemcomissao() {
		return ordemcomissao;
	}
	

	public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}
	
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	
	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}
	
	public void setCdcontratocolaborador(Integer cdcontratocolaborador) {
		this.cdcontratocolaborador = cdcontratocolaborador;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setEscala(Escala escala) {
		this.escala = escala;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setOrdemcomissao(Integer ordemcomissao) {
		this.ordemcomissao = ordemcomissao;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Contratocolaborador other = (Contratocolaborador) obj;
		if (colaborador == null) {
			if (other.colaborador != null)
				return false;
		} else if (!colaborador.equals(other.colaborador))
			return false;
		if (contrato == null) {
			if (other.contrato != null)
				return false;
		} else if (!contrato.equals(other.contrato))
			return false;
		return true;
	}
	
	@Transient
	public Boolean getIsVendedor() {
		return isVendedor;
	}
	
	public void setIsVendedor(Boolean isVendedor) {
		this.isVendedor = isVendedor;
	}
	
	@Transient
	public Double getValor() {
		return valor;
	}
	
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	@Transient
	public Double getPercentual() {
		return percentual;
	}
	
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	@DisplayName("Valor-Hora")
	public Money getValorhora() {
		return valorhora;
	}
	public void setValorhora(Money valorhora) {
		this.valorhora = valorhora;
	}
}
