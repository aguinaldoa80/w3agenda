package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_protocoloprovidencia",sequenceName="sq_protocoloprovidencia")
@DisplayName("ProvidÍncia")
public class Protocoloprovidencia implements Log {
	//Tabela protocoloprovidencia
	
	protected Integer cdprotocoloprovidencia;
	protected Protocolo protocolo;
	protected Providencia providencia;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("Id")
	@Id
	@GeneratedValue(generator="sq_protocoloprovidencia",strategy=GenerationType.AUTO)
	public Integer getCdprotocoloprovidencia() {
		return cdprotocoloprovidencia;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprotocolo")	
	public Protocolo getProtocolo() {
		return protocolo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprovidencia")	
	public Providencia getProvidencia() {
		return providencia;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdprotocoloprovidencia(Integer cdprotocoloprovidencia) {
		this.cdprotocoloprovidencia = cdprotocoloprovidencia;
	}
	public void setProtocolo(Protocolo protocolo) {
		this.protocolo = protocolo;
	}
	public void setProvidencia(Providencia providencia) {
		this.providencia = providencia;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
