package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_oportunidadesituacao", sequenceName = "sq_oportunidadesituacao")
public class Oportunidadesituacao implements Log{
	
	public static final Oportunidadesituacao PROPECTANDO = new Oportunidadesituacao(1, "Prospectando");
	public static final Oportunidadesituacao PROPOSTA_ENVIADA = new Oportunidadesituacao(2, "Proposta Enviada");
	public static final Oportunidadesituacao CONTRATO_ENVIADO = new Oportunidadesituacao(3, "Contrato Enviado");
	public static final Oportunidadesituacao CONTRATO_ASSINADO = new Oportunidadesituacao(4, "Contrato Assinado");
	public static final Oportunidadesituacao CANCELADA = new Oportunidadesituacao(5, "Cancelada");
	
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Integer cdoportunidadesituacao;
	protected String nome;
	protected Boolean defaultlistagem;
	protected Boolean situacaoantesfinal;
	protected Boolean situacaofinal;
	protected Boolean situacaocancelada;
	protected Boolean situacaoinicial;
	
	protected Arquivo imagem;
	protected String corletra;
	protected String corfundo;
	protected String letra;
	
	
	public Oportunidadesituacao() {}
	
	public Oportunidadesituacao(Integer cdoportunidadesituacao) {
		this.cdoportunidadesituacao = cdoportunidadesituacao;
	}
	
	public Oportunidadesituacao(Integer cdoportunidadesituacao, String nome) {
		this.cdoportunidadesituacao = cdoportunidadesituacao;
		this.nome = nome;
	}

	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidadesituacao")
	public Integer getCdoportunidadesituacao() {
		return cdoportunidadesituacao;
	}
	
	@Required	
	@MaxLength(20)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}	
	
	@DisplayName("Marcada como padrão na listagem")
	public Boolean getDefaultlistagem() {
		return defaultlistagem;
	}	
	@DisplayName("Imagem de representação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdimagem")
	public Arquivo getImagem() {
		return imagem;
	}
	@DisplayName("Situação antes da final")
	public Boolean getSituacaoantesfinal() {
		return situacaoantesfinal;
	}
	@DisplayName("Situação final")
	public Boolean getSituacaofinal() {
		return situacaofinal;
	}
	@DisplayName("Situação Cancelada")
	public Boolean getSituacaocancelada() {
		return situacaocancelada;
	}
	@DisplayName("Situação inicial")
	public Boolean getSituacaoinicial() {
		return situacaoinicial;
	}
	@MaxLength(20)
	@DisplayName("Cor da letra de representação")
	public String getCorletra() {
		return corletra;
	}
	@MaxLength(20)
	@DisplayName("Cor do fundo de representação")
	public String getCorfundo() {
		return corfundo;
	}
	@MaxLength(2)
	@DisplayName("Letra de representação")
	public String getLetra() {
		return letra;
	}

	public void setCorletra(String corletra) {
		this.corletra = corletra;
	}

	public void setCorfundo(String corfundo) {
		this.corfundo = corfundo;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public void setSituacaoinicial(Boolean situacaoinicial) {
		this.situacaoinicial = situacaoinicial;
	}

	public void setSituacaocancelada(Boolean situacaocancelada) {
		this.situacaocancelada = situacaocancelada;
	}

	public void setSituacaoantesfinal(Boolean situacaoantesfinal) {
		this.situacaoantesfinal = situacaoantesfinal;
	}

	public void setSituacaofinal(Boolean situacaofinal) {
		this.situacaofinal = situacaofinal;
	}

	public void setImagem(Arquivo imagem) {
		this.imagem = imagem;
	}

	public void setCdoportunidadesituacao(Integer cdoportunidadesituacao) {
		this.cdoportunidadesituacao = cdoportunidadesituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setDefaultlistagem(Boolean defaultlistagem) {
		this.defaultlistagem = defaultlistagem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdoportunidadesituacao == null) ? 0
						: cdoportunidadesituacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Oportunidadesituacao other = (Oportunidadesituacao) obj;
		if (cdoportunidadesituacao == null) {
			if (other.cdoportunidadesituacao != null)
				return false;
		} else if (!cdoportunidadesituacao.equals(other.cdoportunidadesituacao))
			return false;
		return true;
	}

}
