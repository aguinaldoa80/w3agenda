package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_importacaoprovisao", sequenceName="sq_importacaoprovisao")
public class ImportacaoProvisao implements Log{

	private Integer cdImportacaoProvisao;
	private Arquivo arquivo;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	
	@Id
	@GeneratedValue(generator="sq_importacaoprovisao", strategy=GenerationType.AUTO)
	public Integer getCdImportacaoProvisao() {
		return this.cdImportacaoProvisao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	
	public void setCdImportacaoProvisao(Integer cdImportacaoProvisao) {
		this.cdImportacaoProvisao = cdImportacaoProvisao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}



	

}
