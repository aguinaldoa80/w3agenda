package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Referenciacalculo {

	public static final Referenciacalculo HORAS = new Referenciacalculo(1, "Horas");
	public static final Referenciacalculo QUANTIDADE = new Referenciacalculo(2, "Quantidade");
	
	public static List<Referenciacalculo> findAll(){
		List<Referenciacalculo> lista = new ArrayList<Referenciacalculo>();
		lista.add(Referenciacalculo.HORAS);
		lista.add(Referenciacalculo.QUANTIDADE);
		return lista;
	}
	
	protected Integer cdreferenciacalculo;
	protected String nome;
	
	public Referenciacalculo() {
		
	}
	
	public Referenciacalculo(Integer cdreferenciacalculo, String nome) {
		this.cdreferenciacalculo = cdreferenciacalculo;
		this.nome = nome;
	}
	
	@Id	
	public Integer getCdreferenciacalculo() {
		return cdreferenciacalculo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdreferenciacalculo(Integer cdreferenciacalculo) {
		this.cdreferenciacalculo = cdreferenciacalculo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Referenciacalculo){
			Referenciacalculo that = (Referenciacalculo) obj;
			return this.getCdreferenciacalculo().equals(that.getCdreferenciacalculo());
		}
		return super.equals(obj);
	}
	
}
