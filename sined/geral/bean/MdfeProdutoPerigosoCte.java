package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeprodutoperigosocte", sequenceName = "sq_mdfeprodutoperigosocte")
public class MdfeProdutoPerigosoCte {

	protected Integer cdMdfeProdutoPerigosoCte;
	protected Mdfe mdfe;
	protected String idCte;
	protected String idProdutoPerigoso;
	protected String numeroOnuUn;
	protected String classe;
	protected String nomeApropriadoEmbarque;
	protected String grupoEmbalagem;
	protected String quantidadeTotalPorProdutos;
	protected String quantidadeAndTipoPorVolume;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeprodutoperigosocte")
	public Integer getCdMdfeProdutoPerigosoCte() {
		return cdMdfeProdutoPerigosoCte;
	}
	public void setCdMdfeProdutoPerigosoCte(Integer cdMdfeProdutoPerigoso) {
		this.cdMdfeProdutoPerigosoCte = cdMdfeProdutoPerigoso;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("ID do CTe")
	public String getIdCte() {
		return idCte;
	}
	public void setIdCte(String idCte) {
		this.idCte = idCte;
	}
	
	@DisplayName("ID do produto perigoso")
	public String getIdProdutoPerigoso() {
		return idProdutoPerigoso;
	}
	public void setIdProdutoPerigoso(String idProdutoPerigoso) {
		this.idProdutoPerigoso = idProdutoPerigoso;
	}
	
	@Required
	@MaxLength(value=4)
	@DisplayName("N�mero ONU/UN")
	public String getNumeroOnuUn() {
		return numeroOnuUn;
	}
	public void setNumeroOnuUn(String numeroOnuUn) {
		this.numeroOnuUn = numeroOnuUn;
	}
	
	@MaxLength(value=40)
	@DisplayName("Classe ou subclasse/divis�o, e risco subsidi�rio/risco secund�rio")
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	@MaxLength(value=150)
	@DisplayName("Nome apropriado para embarque do produto")
	public String getNomeApropriadoEmbarque() {
		return nomeApropriadoEmbarque;
	}
	public void setNomeApropriadoEmbarque(String nomeApropriadoEmbarque) {
		this.nomeApropriadoEmbarque = nomeApropriadoEmbarque;
	}
	
	@MaxLength(value=6)
	@DisplayName("Grupo de embalagem")
	public String getGrupoEmbalagem() {
		return grupoEmbalagem;
	}
	public void setGrupoEmbalagem(String grupoEmbalagem) {
		this.grupoEmbalagem = grupoEmbalagem;
	}
	
	@Required
	@MaxLength(value=20)
	@DisplayName("Quantidade total por produtos")
	public String getQuantidadeTotalPorProdutos() {
		return quantidadeTotalPorProdutos;
	}
	public void setQuantidadeTotalPorProdutos(String quantidadeTotalPorProdutos) {
		this.quantidadeTotalPorProdutos = quantidadeTotalPorProdutos;
	}
	
	@MaxLength(value=60)
	@DisplayName("Quantidade e tipo por volumes")
	public String getQuantidadeAndTipoPorVolume() {
		return quantidadeAndTipoPorVolume;
	}
	public void setQuantidadeAndTipoPorVolume(String quantidadeAndTipoPorVolume) {
		this.quantidadeAndTipoPorVolume = quantidadeAndTipoPorVolume;
	}
	
	@Transient
	public List<MdfeLacreUnidadeCargaCte> getListaMdfeProdutoPerigosoCte(MdfeCte mdfeCte, Mdfe mdfe) {
		List<MdfeLacreUnidadeCargaCte> list = new ArrayList<MdfeLacreUnidadeCargaCte>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaCte())){
			for(MdfeLacreUnidadeCargaCte item : mdfe.getListaLacreUnidadeCargaCte()){
				if(item.getIdUnidadeCarga().equals(mdfeCte.getIdentificadorCte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
