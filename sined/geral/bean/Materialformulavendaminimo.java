package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialformulavendaminimo", sequenceName = "sq_materialformulavendaminimo")
public class Materialformulavendaminimo {

	protected Integer cdmaterialformulavendaminimo;
	protected Material material;
	protected Integer ordem;
	protected String identificador;
	protected String formula;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialformulavendaminimo")
	public Integer getCdmaterialformulavendaminimo() {
		return cdmaterialformulavendaminimo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@Required
	public Integer getOrdem() {
		return ordem;
	}

	@Required
	@MaxLength(5)
	public String getIdentificador() {
		return identificador;
	}

	@Required
	@DisplayName("F�rmula")
	public String getFormula() {
		return formula;
	}

	public void setCdmaterialformulavendaminimo(Integer cdmaterialformulavendaminimo) {
		this.cdmaterialformulavendaminimo = cdmaterialformulavendaminimo;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
	
}
