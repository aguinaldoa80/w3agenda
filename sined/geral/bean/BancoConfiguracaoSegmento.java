package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaosegmento",sequenceName="sq_bancoconfiguracaosegmento")
public class BancoConfiguracaoSegmento implements Log {

	private Integer cdbancoconfiguracaosegmento;
	private String nome;
	private Banco banco;
	private BancoConfiguracaoTipoSegmentoEnum tipoSegmento;
	private Integer tamanho;
	
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	private List<BancoConfiguracaoSegmentoCampo> listaBancoConfiguracaoSegmentoCampo = new ArrayList<BancoConfiguracaoSegmentoCampo>();
		
	public BancoConfiguracaoSegmento() {}
	
	public BancoConfiguracaoSegmento(Integer cdbancoconfiguracaosegmento) {
		this.cdbancoconfiguracaosegmento = cdbancoconfiguracaosegmento;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaosegmento")
	public Integer getCdbancoconfiguracaosegmento() {
		return cdbancoconfiguracaosegmento;
	}
	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	@Required
	public Banco getBanco() {
		return banco;
	}
	@DisplayName("Tipo de segmento")
	@Required
	public BancoConfiguracaoTipoSegmentoEnum getTipoSegmento() {
		return tipoSegmento;
	}
	@Required
	public Integer getTamanho() {
		return tamanho;
	}
	@OneToMany(mappedBy="bancoConfiguracaoSegmento")
	public List<BancoConfiguracaoSegmentoCampo> getListaBancoConfiguracaoSegmentoCampo() {
		return listaBancoConfiguracaoSegmentoCampo;
	}
	public void setCdbancoconfiguracaosegmento(
			Integer cdbancoconfiguracaosegmento) {
		this.cdbancoconfiguracaosegmento = cdbancoconfiguracaosegmento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setTipoSegmento(BancoConfiguracaoTipoSegmentoEnum tipoSegmento) {
		this.tipoSegmento = tipoSegmento;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	public void setListaBancoConfiguracaoSegmentoCampo(
			List<BancoConfiguracaoSegmentoCampo> listaBancoConfiguracaoSegmentoCampo) {
		this.listaBancoConfiguracaoSegmentoCampo = listaBancoConfiguracaoSegmentoCampo;
	}
	
	//Log	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
