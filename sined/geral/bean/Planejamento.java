package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_planejamento",sequenceName="sq_planejamento")
public class Planejamento implements Log, PermissaoProjeto{

	protected Integer cdplanejamento;
	protected String descricao;
	protected Projeto projeto;
	protected Date dtinicio;
	protected Date dtfim;
	protected Integer versao;
	protected Divisaotempo divisaotempo = Divisaotempo.SEMANA;
	protected Integer tamanhotempo = 40;
	protected Planejamentosituacao planejamentosituacao = Planejamentosituacao.EM_ABERTO;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Money custo;
	protected Orcamento orcamento;
	
	protected Set<Planejamentorecursogeral> listaPlanejamentorecursogeral = new ListSet<Planejamentorecursogeral>(Planejamentorecursogeral.class);
	protected Set<Planejamentorecursohumano> listaPlanejamentorecursohumano = new ListSet<Planejamentorecursohumano>(Planejamentorecursohumano.class);
	protected Set<Planejamentodespesa> listaPlanejamentodespesa = new ListSet<Planejamentodespesa>(Planejamentodespesa.class);
	
	protected Set<Apontamento> listaApontamento;
	protected Set<Tarefa> listaTarefa;
	
	//TRANSIENT
	protected Boolean base;
	protected Boolean fim;
	
	public Planejamento() {
	}
	
	public Planejamento(Integer cdplanejamento) {
		this.cdplanejamento = cdplanejamento;
	}
	
	public Planejamento(Tarefa tarefa) {
		cdplanejamento = tarefa.getCdtarefa();
		descricao = tarefa.getDescricao();
		dtinicio = tarefa.getDtinicio();
		dtfim = tarefa.getDtfim();
	}
	
	@Id
	@GeneratedValue(generator="sq_planejamento",strategy=GenerationType.AUTO)
	public Integer getCdplanejamento() {
		return cdplanejamento;
	}
	@DisplayName("Descri��o")
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}
	@DisplayName("Data de in�cio")
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data de fim")
	@Required
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Vers�o")
	@Required
	@MaxLength(9)
	public Integer getVersao() {
		return versao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Money getCusto() {
		return custo;
	}
	@OneToMany(mappedBy="planejamento")
	@DisplayName("Recursos gerais")
	public Set<Planejamentorecursogeral> getListaPlanejamentorecursogeral() {
		return listaPlanejamentorecursogeral;
	}
	@OneToMany(mappedBy="planejamento")
	@DisplayName("Recursos humanos")
	public Set<Planejamentorecursohumano> getListaPlanejamentorecursohumano() {
		return listaPlanejamentorecursohumano;
	}
	
	@OneToMany(mappedBy="planejamento")
	@DisplayName("Despesas")
	public Set<Planejamentodespesa> getListaPlanejamentodespesa() {
		return listaPlanejamentodespesa;
	}
	@OneToMany(mappedBy="planejamento")
	public Set<Apontamento> getListaApontamento() {
		return listaApontamento;
	}
	@OneToMany(mappedBy="planejamento")
	public Set<Tarefa> getListaTarefa() {
		return listaTarefa;
	}

	@DisplayName("Divis�o do tempo")
	@Required
	public Divisaotempo getDivisaotempo() {
		return divisaotempo;
	}
	
	@DisplayName("Tamanho do tempo")
	@Required
	@MaxLength(9)
	public Integer getTamanhotempo() {
		return tamanhotempo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamentosituacao")
	@Required
	public Planejamentosituacao getPlanejamentosituacao() {
		return planejamentosituacao;
	}
	
	public void setPlanejamentosituacao(
			Planejamentosituacao planejamentosituacao) {
		this.planejamentosituacao = planejamentosituacao;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setTamanhotempo(Integer tamanhotempo) {
		this.tamanhotempo = tamanhotempo;
	}
	
	public void setDivisaotempo(Divisaotempo divisaotempo) {
		this.divisaotempo = divisaotempo;
	}
	
	public void setCdplanejamento(Integer cdplanejamento) {
		this.cdplanejamento = cdplanejamento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCusto(Money custo) {
		this.custo = custo;
	}
	public void setListaPlanejamentorecursogeral(
			Set<Planejamentorecursogeral> listaPlanejamentorecursogeral) {
		this.listaPlanejamentorecursogeral = listaPlanejamentorecursogeral;
	}
	public void setListaPlanejamentorecursohumano(
			Set<Planejamentorecursohumano> listaPlanejamentorecursohumano) {
		this.listaPlanejamentorecursohumano = listaPlanejamentorecursohumano;
	}
	public void setListaPlanejamentodespesa(
			Set<Planejamentodespesa> listaPlanejamentodespesa) {
		this.listaPlanejamentodespesa = listaPlanejamentodespesa;
	}
	public void setListaApontamento(Set<Apontamento> listaApontamento) {
		this.listaApontamento = listaApontamento;
	}
	public void setListaTarefa(Set<Tarefa> listaTarefa) {
		this.listaTarefa = listaTarefa;
	}

	@Transient
	public Boolean getBase() {
		return base;
	}
	@Transient
	public Boolean getFim() {
		return fim;
	}

	public void setBase(Boolean base) {
		this.base = base;
	}
	public void setFim(Boolean fim) {
		this.fim = fim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdplanejamento == null) ? 0 : cdplanejamento.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Planejamento other = (Planejamento) obj;
		if (cdplanejamento == null) {
			if (other.cdplanejamento != null)
				return false;
		} else if (!cdplanejamento.equals(other.cdplanejamento))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		// Propriedade anotada com DescriptionProperty
		Object description = new BeanDescriptorFactoryImpl().createBeanDescriptor(this).getDescription();
		
		return description != null ? description.toString() : super.toString();
	}
	
	public String subQueryProjeto() {
		return "select planejamentosubQueryProjeto.cdplanejamento " +
				"from Planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
}
