package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.lkbanco.bank.Account;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Nacionalidade;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_pessoa", sequenceName = "sq_pessoa")
public class Pessoa implements Account<Banco>{

	protected Integer cdpessoa;
	protected String nome;
	protected String email;
	protected String site;
	protected Tipopessoa tipopessoa;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String observacao;
	protected Date dtnascimento;
	protected Pessoatratamento pessoatratamento;
	protected Nacionalidade nacionalidade;
	protected Cliente cliente;
	protected Colaborador colaborador;
	protected Fornecedor fornecedorBean; //Tive que colocar o "Bean" no final pois o Hibernate estava fazendo o join como "cdfornecedor" e n�o como "cdpessoa". Existem alguns beans que extendem Pessoa que possui a coluna "cdfornecedor".
	protected Operacaocontabil operacaocontabil;
	
	protected Set<Pessoacategoria> listaPessoacategoria;
	protected Set<Dadobancario> listaDadobancario;
	protected Set<Endereco> listaEndereco;
	protected Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone;
	protected Set<Pessoaquestionario> listaQuestionario;
	protected List<Pessoadap> listaPessoadap = new ListSet<Pessoadap>(Pessoadap.class);
	protected List<Pessoamaterial> listaPessoamaterial;
	protected List<PessoaContato> listaContato = new ListSet<PessoaContato>(PessoaContato.class);
	
	//Transientes
	protected Dadobancario dadobancario;
	protected Endereco enderecoAux;
	protected String telefones;
	protected String razaosocialTrans;

	/* Construtores */
	public Pessoa() {
		
	}
	
	public Pessoa(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	
	public Pessoa(Integer cdpessoa, String nome) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
	}
	
	public Pessoa(String nome) {
		this.nome = nome;
	}
	
	public Pessoa(String nome, Tipopessoa tipopessoa) {
		this.nome = nome;
		this.tipopessoa = tipopessoa;
	}
	
	public Pessoa(Integer cdpessoa, String nome, Tipopessoa tipopessoa) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.tipopessoa = tipopessoa;
	}
	
	public Pessoa(Integer cdpessoa, String nome, String email) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.email = email;
	}
	
	/* Fim dos Construtores */
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoa")
	public Integer getCdpessoa() {
		return cdpessoa;
	}
	public void setCdpessoa(Integer id) {
		this.cdpessoa = id;
	}
	
	@MaxLength(100)
	@DescriptionProperty
	@Required
	public String getNome() {
		if(nome != null){
			return nome.replaceAll("&#8203;", "");
		}
		return nome;
	}
	
	@MaxLength(50)
	@Email
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipopessoa() {
		return tipopessoa;
	}
	
	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}
	
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}
	
	public Nacionalidade getNacionalidade() {
		return nacionalidade;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa", insertable=false, updatable=false)
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa", insertable=false, updatable=false)
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setNacionalidade(Nacionalidade nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	
	@MaxLength(8000)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public Set<Dadobancario> getListaDadobancario() {
		return listaDadobancario;
	}
	@DisplayName("Endere�os")
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public Set<Endereco> getListaEndereco() {
		return listaEndereco;
	}
	@DisplayName("Categorias")
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public Set<Pessoacategoria> getListaPessoacategoria() {
		return listaPessoacategoria;
	}
	
	@DisplayName("DAP")
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public List<Pessoadap> getListaPessoadap() {
		return listaPessoadap;
	}
	
	@DisplayName("Disponibilidade")
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public List<Pessoamaterial> getListaPessoamaterial() {
		return listaPessoamaterial;
	}
	
	@DisplayName("Data nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}
	
	@DisplayName("Tratamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoatratamento")
	public Pessoatratamento getPessoatratamento() {
		return pessoatratamento;
	}
	
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	
	public void setPessoatratamento(Pessoatratamento pessoatratamento) {
		this.pessoatratamento = pessoatratamento;
	}
	
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	@DisplayName("Telefones")
	public Set<br.com.linkcom.sined.geral.bean.Telefone> getListaTelefone() {
		return listaTelefone;
	}
	
	public void setListaTelefone(
			Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone) {
		this.listaTelefone = listaTelefone;
	}
	
	public void setListaPessoadap(List<Pessoadap> listaPessoadap) {
		this.listaPessoadap = listaPessoadap;
	}
	
	public void setListaPessoamaterial(List<Pessoamaterial> listaPessoamaterial) {
		this.listaPessoamaterial = listaPessoamaterial;
	}
	
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public List<PessoaContato> getListaContato() {
		return listaContato;
	}
	
	public void setListaContato(List<PessoaContato> listaContato) {
		this.listaContato = listaContato;
	}
	
	@Transient
	@DisplayName("CNPJ/CPF")
	public String getCpfOuCnpj() {
		if (this.getCpf() != null) {
			return this.getCpf().toString();
		} else if (this.getCnpj() != null) {
			return this.getCnpj().toString();
		}
		
		return null;
	}
	
	@Transient
	public String getCpfOuCnpjValue() {
		if (this.getCpf() != null) {
			return this.getCpf().getValue();
		} else if (this.getCnpj() != null) {
			return this.getCnpj().getValue();
		}
		
		return null;
	}
	
	@MaxLength(255)
	public String getSite() {
		return site;
	}
	
	public void setSite(String site) {
		this.site = site;
	}
	
	public void setListaPessoacategoria(
			Set<Pessoacategoria> listaPessoacategoria) {
		this.listaPessoacategoria = listaPessoacategoria;
	}
	public void setObservacao(String observacao) {
		this.observacao = StringUtils.trimToNull(observacao);
	}
	
	public void setNome(String nome) {
		if(nome != null){
			nome = nome.replaceAll("&#8203;", "");
		}
		this.nome = StringUtils.trimToNull(nome);
	}
	
	public void setTipopessoa(Tipopessoa tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setEmail(String email) {
		this.email = StringUtils.trimToNull(email);
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setListaDadobancario(Set<Dadobancario> listaDadobancario) {
		this.listaDadobancario = listaDadobancario;
	}
	public void setListaEndereco(Set<Endereco> listaEndereco) {
		this.listaEndereco = listaEndereco;
	}
	
	public static final Comparator<Colaborador> comparatorCpf = new Comparator<Colaborador>() {
		public int compare(Colaborador o1, Colaborador o2) {
			return o1.getCpf().getValue().compareTo(o2.getCpf().getValue());
		}
	};
	public static final Comparator<Colaborador> comparatorCnpj = new Comparator<Colaborador>() {
		public int compare(Colaborador o1, Colaborador o2) {
			return o1.getCnpj().getValue().compareTo(o1.getCnpj().getValue());
		}
	};
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pessoa) {
			Pessoa p = (Pessoa) obj;
			return p.cdpessoa.equals(this.cdpessoa);
		}
		return super.equals(obj);
	}
	
	@Transient
	@DisplayName("Dado banc�rio")
	public Dadobancario getDadobancario() {
		if(SinedUtil.isListNotEmpty(listaDadobancario)){
			dadobancario = listaDadobancario.iterator().next();
		}
		return dadobancario;
	}
	public void setDadobancario(Dadobancario dadobancario) {
		this.dadobancario = dadobancario;
	}
	
	@Transient
	public Telefone getTelefone(){
		if(this.listaTelefone != null && this.listaTelefone.size() > 0){
			List<Telefone> listaTelefoneAux = new ArrayList<Telefone>(this.listaTelefone);
			for (Telefone telefone : listaTelefoneAux) {
				if(telefone.getTelefonetipo() != null && 
						telefone.getTelefonetipo().getCdtelefonetipo() != null &&
						telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL)){
					return telefone;
				}
			}
			return listaTelefoneAux.iterator().next();
		}
		return null;
	}
	
	@Transient 
	public String getTelefonesComTipoSemQuebraLinha(){
		return getTelefonesComTipo(false);
	}
	
	@Transient 
	public String getTelefonesComTipoComQuebraLinha(){
		return getTelefonesComTipo(true);
	}
	
	@Transient 
	public String getTelefonesComTipo(boolean quebraLinha){
		StringBuilder stringBuilder = new StringBuilder("");
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				stringBuilder.append(telefone.getTelefone());
				
				if(telefone.getTelefonetipo() != null)
					stringBuilder.append("(").append(telefone.getTelefonetipo().getNome()).append(") ");
				
				if(quebraLinha) stringBuilder.append(" / ");
			}
			return stringBuilder.substring(0, (stringBuilder.length()<2 ? 0 : stringBuilder.length()-2)).toString();
		}
		return stringBuilder.toString();
	}
	
	@Transient
	public Telefone getTelefoneFax() {
		if(this.listaTelefone != null && this.listaTelefone.size() > 0){
			List<Telefone> listaTelefoneAux = new ArrayList<Telefone>(this.listaTelefone);
			for (Telefone telefone : listaTelefoneAux) {
				if(telefone.getTelefonetipo() != null && 
						telefone.getTelefonetipo().getCdtelefonetipo() != null &&
						telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.FAX)){
					return telefone;
				}
			}
		}
		return null;
	}
	
	@Transient
	public Endereco getEnderecoWithPrioridade(Enderecotipo ...listaEnderecotipoPrioridade) {
		if(Hibernate.isInitialized(listaEndereco) && this.listaEndereco != null && this.listaEndereco.size() > 0){
			
			List<Endereco> listaEnderecoAux = new ArrayList<Endereco>(this.listaEndereco);
			
			//A ordem para selecionar o endere�o deve ser a seguinte: "�nico", "Faturamento", "Cobran�a" e depois qualquer um.
			Collections.sort(listaEnderecoAux, new Comparator<Endereco>(){

				public int compare(Endereco o1, Endereco o2) {
					
					if(o1.getEnderecotipo() == null
							|| o1.getEnderecotipo().getCdenderecotipo() == null
							|| o2.getEnderecotipo() == null
							|| o2.getEnderecotipo().getCdenderecotipo() == null){
						return 0;
					}else{
					
						Integer cdenderecotipo = o1.getEnderecotipo().getCdenderecotipo();
						Integer cdenderecotipo2 = o2.getEnderecotipo().getCdenderecotipo();
						
						return cdenderecotipo.compareTo(cdenderecotipo2);
					}
				}
			});
			return filterEndereco(listaEnderecoAux, listaEnderecotipoPrioridade);
		}
		return null;
	}
	
	/**
	 * Seleciona um endereco por enderecotipo, seguindo a ordem de prioridade. A � de ordem
	 * decrescente partindo do come�o da lista 'listaEnderecotipoPrioridade'.
	 * @param enderecos
	 * @param listaEnderecotipoPrioridade
	 * @return
	 */
	public Endereco filterEndereco(List<Endereco> enderecos, Enderecotipo...listaEnderecotipoPrioridade){
		if(enderecos == null || enderecos.isEmpty()) return null;
		if(listaEnderecotipoPrioridade == null || listaEnderecotipoPrioridade.length == 0){
			if(enderecos.size() > 1){
				for (Endereco endereco : enderecos){
					if(endereco.getEnderecotipo() != null && endereco.getEnderecotipo().getCdenderecotipo() != null && 
							!Enderecotipo.INATIVO.equals(endereco.getEnderecotipo())){
						return endereco;
					}
				}
			}
			return enderecos.get(0); 
		}
		for (Endereco endereco: enderecos){
			if(endereco.getEnderecotipo() == null || endereco.getEnderecotipo().getCdenderecotipo() == null)
				continue;
			for(Enderecotipo tipo: listaEnderecotipoPrioridade)
				if(tipo.equals(endereco.getEnderecotipo())) return endereco;
		}
		return enderecos.get(0);
	}
	
	@Transient
	@DisplayName("Endere�o")
	public Endereco getEndereco() {
		return getEnderecoWithPrioridade();
	}
	
	@Transient
	public Endereco getEnderecoForDI() {
		return getEnderecoWithPrioridade(Enderecotipo.UNICO);
	}
	
	@Transient
	public String getAgency() {
		return getDadobancario() == null ? "" : getDadobancario().getAgencia();
	}
	@Transient
	public String getAgreementNumber() {
		return null;
	}
	@Transient
	public Banco getBank() {
		return getDadobancario() == null ? null : getDadobancario().getBanco();
	}
	@Transient
	public String getCpfCnpj() {
		return getCnpj() != null ? getCnpj().toString() : getCpf() != null ? getCpf().toString() : "";
	}
	@Transient
	public String getDvAgency() {
		return getDadobancario() == null ? "" : getDadobancario().getDvagencia() != null ? getDadobancario().getDvagencia() : "";
	}
	@Transient
	public String getDvnumber() {
		return getDadobancario() == null ? "" : getDadobancario().getDvconta() != null ? getDadobancario().getDvconta() : "";
	}
	@Transient
	public String getEnderecoCep() {
		return getEndereco() != null && getEndereco().getCep() != null ? getEndereco().getCep().toString() : "";
	}
	@Transient
	public Integer getEnderecoCdMunicipio() {
		return getEndereco() != null && getEndereco().getMunicipio() != null ? getEndereco().getMunicipio().getCdmunicipio() : 0;
	}
	@Transient
	public String getEnderecoCidade() {
		return getEndereco() != null && getEndereco().getMunicipio() != null ? getEndereco().getMunicipio().getNome() : "";
	}
	@Transient
	public String getEnderecoCidadeCodIbge() {
		return getEndereco() != null && getEndereco().getMunicipio() != null ? getEndereco().getMunicipio().getCdibge() : "";
	}
	@Transient
	public Integer getEnderecoCdPais() {
		return getEndereco() != null && getEndereco().getPais() != null ? getEndereco().getPais().getCdpais() : 0;
	}
	@Transient
	public String getEnderecoPais() {
		return getEndereco() != null && getEndereco().getPais() != null ? getEndereco().getPais().getNome() : "";
	}
	@Transient
	public String getEnderecoComplemento() {
		return getEndereco() != null ? getEndereco().getComplemento() : "";
	}
	@Transient
	public String getEnderecoBairro() {
		return getEndereco() != null ? getEndereco().getBairro() : "";
	}
	@Transient
	public String getEnderecoLogradouro() {
		return getEndereco() != null ? getEndereco().getLogradouro() : "";
	}
	@Transient
	public String getEnderecoNumero() {
		return getEndereco() != null ? getEndereco().getNumero() : "";
	}
	@Transient
	public Integer getEnderecoCdUf() {
		return getEndereco() != null && getEndereco().getMunicipio() != null && getEndereco().getMunicipio().getUf() != null ? getEndereco().getMunicipio().getUf().getCduf() : 0;
	}
	@Transient
	public String getEnderecoSiglaEstado() {
		return getEndereco() != null && getEndereco().getMunicipio() != null && getEndereco().getMunicipio().getUf() != null ? getEndereco().getMunicipio().getUf().getSigla() : "";
	}
	@Transient
	public Integer getEnderecoNumeroEstado() {
		Endereco endereco = getEndereco();
		if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && 
				endereco.getMunicipio().getUf().getCdibge() != null){
			return endereco.getMunicipio().getUf().getCdibge();
		}
		
		return 0;
	}
	@Transient
	public String getEnderecoSiglaEstadoIntegracaoDominio() {
		Endereco endereco = getEndereco();
		if(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null && 
				endereco.getMunicipio().getUf().getSigla() != null){
			return endereco.getMunicipio().getUf().getSigla();
		}else if(endereco != null && endereco.getPais() != null && !endereco.getPais().equals(Pais.BRASIL)){
			return "EX";
		}
		return "";
	}
	@Transient
	public String getNumber() {
		return getDadobancario() == null ? "" : getDadobancario().getConta();
	}
	@Transient
	public String getRazaoSocial() {
		return getNome();
	}
	@Transient
	public String getEnderecoRuaAvenida() {
		return getEndereco() != null ? getEndereco().getLogradouro() : "";
	}
	
	@Transient
	public String getCategorias(){
		StringBuilder sb = new StringBuilder();
		if(listaPessoacategoria != null && listaPessoacategoria.size() > 0){
			for (Pessoacategoria pc : listaPessoacategoria) {
				if(pc.getCategoria() != null){
					sb.append(pc.getCategoria().getNome());
					sb.append(", ");
				}
			}
			if(!sb.toString().equals("")){
				sb.delete(sb.length() - 2, sb.length());
			}
		}
		return sb.toString();
	}
	
	@Transient
	public String getEnderecos(){
		StringBuilder sb = new StringBuilder();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for(Endereco endereco : listaEndereco){
				if(StringUtils.isNotEmpty(endereco.getLogradouroCompletoComBairro())){
					sb.append(endereco.getLogradouroCompletoComBairro());
					sb.append("\n");
				}
			}
			if(!sb.toString().equals("")){
				sb.delete(sb.length() - 2, sb.length());
			}
		}
		return sb.toString();
	}
	@Transient
	public String getEnderecosSemQuebraLinha(){
		StringBuilder sb = new StringBuilder();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for(Endereco endereco : listaEndereco){
				if(endereco.getLogradouroCompletoComBairro() != null){
					sb.append(endereco.getLogradouroCompletoComBairro());
					sb.append(" | ");
				}
			}
			if(!sb.toString().equals("")){
				sb.delete(sb.length() - 3, sb.length());
			}
		}
		return sb.toString();
	}

	@Transient
	public Endereco getEnderecoAux() {
		return enderecoAux;
	}
	public void setEnderecoAux(Endereco enderecoAux) {
		this.enderecoAux = enderecoAux;
	}
	
	@Transient
	public String getTelefones() {
		if(this.telefones != null && !this.telefones.equals("")){
			return this.telefones;
		}else{
			StringBuilder stringBuilder = new StringBuilder("");
			if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
				for (Telefone telefone : this.listaTelefone) {
					stringBuilder.append(telefone.toString()).append(" | ");
				}
			}
			if(!stringBuilder.toString().equals(""))
				this.telefones = stringBuilder.substring(0, stringBuilder.length()-2).toString();
			return telefones;
		}
	}
	
	@Transient
	public String getTelefonesTipo() {
		StringBuilder stringBuilder = new StringBuilder("");
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				if(telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getCdtelefonetipo() != null && 
						Telefonetipo.FAX.equals(telefone.getTelefonetipo().getCdtelefonetipo())){
					stringBuilder.append(" Fax ");
				}
				stringBuilder.append(telefone.toString()).append(" | ");
			}
		}
		if(!stringBuilder.toString().equals(""))
			return stringBuilder.substring(0, stringBuilder.length()-2).toString();
		
		return "";
	}
	@Transient
	public String getTelefonesSemQuebraLinha() {
		if(this.telefones != null && !this.telefones.equals("")){
			return this.telefones;
		}else{
			StringBuilder stringBuilder = new StringBuilder("");
			if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
				for (Telefone telefone : this.listaTelefone) {
					stringBuilder.append(telefone.toString()).append(" | ");
				}
			}
			if(!stringBuilder.toString().equals(""))
				this.telefones = stringBuilder.substring(0, stringBuilder.length()-2).toString();
			return telefones;
		}
	}
	
	@Transient
	public String getTelefonesComQuebraLinha() {
		if(this.telefones != null && !this.telefones.equals("")){
			return this.telefones;
		}else{
			StringBuilder stringBuilder = new StringBuilder("");
			if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
				for (Telefone telefone : this.listaTelefone) {
					stringBuilder.append(telefone.toString()).append("\n");
				}
			}
			if(!stringBuilder.toString().equals(""))
				this.telefones = stringBuilder.substring(0, stringBuilder.length()-1).toString();
			return telefones;
		}
	}
	
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}

	@DisplayName("Question�rio")
	@OneToMany(mappedBy="pessoa", fetch=FetchType.LAZY)
	public Set<Pessoaquestionario> getListaQuestionario() {
		return listaQuestionario;
	}

	public void setListaQuestionario(Set<Pessoaquestionario> listaQuestionario) {
		this.listaQuestionario = listaQuestionario;
	}	

	@Transient
	public String getFoneDominiointegracao(){		
		StringBuilder fone = new StringBuilder("");
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				if(!Telefonetipo.FAX.equals(telefone.getTelefonetipo()) && telefone.getTelefone() != null && "".equals(telefone.getTelefone())){
					if(telefone.getTelefone().contains(")"))
						fone.append(telefone.getTelefone().trim().replace("-", "").substring(telefone.getTelefone().indexOf(")"+1)));
					else
						fone.append(telefone.getTelefone().trim().replace("-", ""));
					break;
				}
			}
		}		
		return fone.toString();
		
	}
	
	@Transient
	public Integer getDDDFoneDominiointegracao(){		
		StringBuilder fone = new StringBuilder("");
		Integer ddd = null;
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				if(!Telefonetipo.FAX.equals(telefone.getTelefonetipo()) && telefone.getTelefone() != null && !"".equals(telefone.getTelefone())){
					if(telefone.getTelefone().contains("(") && telefone.getTelefone().contains(")")){
						fone.append(telefone.getTelefone().trim().substring(telefone.getTelefone().indexOf("(")+1, telefone.getTelefone().indexOf(")")));
						break;
					}
				}
			}
			if(!"".equals(fone.toString())){
				try {
					ddd = Integer.parseInt(fone.toString());
				} catch (NumberFormatException e) {}				
			}
		}	
		
		return ddd;
		
	}
	
	@Transient
	public String getTelefoneprincipal(){
		String tel = "";
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				if(telefone.getTelefonetipo() != null && 
						telefone.getTelefonetipo().getCdtelefonetipo() != null &&
						telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL) &&
						telefone.getTelefone() != null && 
						!"".equals(telefone.getTelefone())){
					tel = telefone.getTelefone();
					break;
				}
			}
		}
		return tel;
	}
	
	@Transient
	public String getTelefonecelular(){
		String tel = "";
		if(this.listaTelefone != null && !this.listaTelefone.isEmpty()){
			for (Telefone telefone : this.listaTelefone) {
				if(telefone.getTelefonetipo() != null && 
						telefone.getTelefonetipo().getCdtelefonetipo() != null &&
						telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR) &&
						telefone.getTelefone() != null && 
						!"".equals(telefone.getTelefone())){
					tel = telefone.getTelefone();
					break;
				}
			}
		}
		return tel;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Opera��o Cont�bil")
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	//Tive que colocar o "Bean" no final pois o Hibernate estava fazendo o join como "cdfornecedor" e n�o como "cdpessoa". Existem alguns beans que extendem Pessoa que possui a coluna "cdfornecedor".
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa", insertable=false, updatable=false)
	public Fornecedor getFornecedorBean() {
		return fornecedorBean;
	}
	
	public void setFornecedorBean(Fornecedor fornecedorBean) {
		this.fornecedorBean = fornecedorBean;
	}
	
	@Transient
	public String getRazaosocialTrans() {
		return razaosocialTrans != null? razaosocialTrans: getNome();
	}
	public void setRazaosocialTrans(String razaosocialTrans) {
		this.razaosocialTrans = razaosocialTrans;
	}

	@Transient
	public Boolean getPessoaExterior() {
		try {
			if(SinedUtil.isListNotEmpty(getListaEndereco())){
				for(Endereco endereco : getListaEndereco()){
					if(endereco.getPais() != null && !Pais.BRASIL.equals(endereco.getPais())){
						return Boolean.TRUE; 
					}
				}
			}
			return Boolean.FALSE;
		} catch (Exception e) {
			return false;
		}
	}
	
}