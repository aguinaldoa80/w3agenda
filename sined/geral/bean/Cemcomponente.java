package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;


@Entity
@SequenceGenerator(name = "sq_cemcomponente", sequenceName = "sq_cemcomponente")
public class Cemcomponente implements ImportacaoproducaoMaterial {

	protected Integer cdcemcomponente;
	protected Cemtipologia cemtipologia;
	protected String ref;
	protected String codigo;
	protected String codigocor;
	protected String descricao;
	protected String unid;
	protected Double qtde;
	protected Double tam; 
	protected Double custo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cemcomponente")
	public Integer getCdcemcomponente() {
		return cdcemcomponente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcemtipologia")
	public Cemtipologia getCemtipologia() {
		return cemtipologia;
	}

	@MaxLength(20)
	public String getRef() {
		return ref;
	}

	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@MaxLength(20)
	@DisplayName("C�digo da Cor")
	public String getCodigocor() {
		return codigocor;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@MaxLength(3)
	public String getUnid() {
		return unid;
	}

	public Double getQtde() {
		return qtde;
	}

	public Double getTam() {
		return tam;
	}

	public Double getCusto() {
		return custo;
	}

	public void setCdcemcomponente(Integer cdcemcomponente) {
		this.cdcemcomponente = cdcemcomponente;
	}

	public void setCemtipologia(Cemtipologia cemtipologia) {
		this.cemtipologia = cemtipologia;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setCodigocor(String codigocor) {
		this.codigocor = codigocor;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setTam(Double tam) {
		this.tam = tam;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}

	@Transient
	@Override
	public Double getAlturaMaterial() {
		return null;
	}

	@Transient
	@Override
	public Double getLarguraMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public Double getPesoMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public Double getComprimentoMaterial() {
		return this.tam;
	}
	
	@Transient
	@Override
	public String getNomeMaterial() {
		return this.descricao + " " + this.codigo;
	}
	
	@Transient
	@Override
	public Double getQuantidade() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public TipoImportacaoProducao getTipoImportacao() {
		return TipoImportacaoProducao.COMPONENTE;
	}
	
	@Transient
	@Override
	public Double getCustoMaterial() {
		return this.custo;
	}
	
	@Transient
	@Override
	public Double getVolume() {
		return this.qtde;
	}

	@Transient
	@Override
	public String getCorMaterial() {
		return this.codigocor;
	}

	@Transient
	@Override
	public String getGrupoMaterial() {
		return this.ref;
	}

	@Transient
	@Override
	public String getTipoMaterial() {
		return "COMPONENTE";
	}

	@Transient
	@Override
	public String getUnidademedidaMaterial() {
		return this.unid;
	}
	
}
