package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.ParametrogeralHistoricoEnum;


@Entity
@SequenceGenerator(name="sq_parametrogeralhistorico",sequenceName="sq_parametrogeralhistorico")
public class ParametrogeralHistorico {
	
	private Integer cdparametrogeralhistorico;
	private Timestamp dataaltera;
	private ParametrogeralHistoricoEnum acaoexecutada;
	private String responsavel;
	private String observacao;
	private Parametrogeral parametrogeral;
		
		
	@Id
	@GeneratedValue(generator="sq_parametrogeralhistorico",strategy=GenerationType.AUTO)
	public Integer getCdparametrogeralhistorico() {
		return cdparametrogeralhistorico;
	}

	@DisplayName("Data Altera��o")
	public Timestamp getDataaltera() {
		return dataaltera;
	}

	@DisplayName("A��o")
	public ParametrogeralHistoricoEnum getAcaoexecutada() {
		return acaoexecutada;
	}
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return responsavel;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdparametrogeral")
	public Parametrogeral getParametrogeral() {
		return parametrogeral;
	}
	
	public void setCdparametrogeralhistorico(Integer cdparametrogeralhistorico) {
		this.cdparametrogeralhistorico = cdparametrogeralhistorico;
	}

	public void setDataaltera(Timestamp dataaltera) {
		this.dataaltera = dataaltera;
	}

	public void setAcaoexecutada(ParametrogeralHistoricoEnum acaoexecutada) {
		this.acaoexecutada = acaoexecutada;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setParametrogeral(Parametrogeral parametrogeral) {
		this.parametrogeral = parametrogeral;
	}
}
