package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GeracaocontareceberEnum;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.SituacaoNotaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.enumeration.TipoReservaEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.CompararCampo;


@Entity
@SequenceGenerator(name = "sq_pedidovendatipo", sequenceName = "sq_pedidovendatipo")
@DisplayName("Tipo de Pedido de venda")
public class Pedidovendatipo implements Log{
	
	protected Integer cdpedidovendatipo;
	protected String descricao;
	protected String sigla;
	protected Boolean principal = Boolean.FALSE;
	protected Boolean bonificacao;
	protected Boolean reserva;
	protected Boolean comodato;
	protected String infoadicionalfisco;
	protected String infoadicionalcontrib;
	protected Naturezaoperacao naturezaoperacao;
	protected Naturezaoperacao naturezaoperacaoservico;
	protected Naturezaoperacao naturezaoperacaoexpedicao;
	protected Boolean gerarnotaconfirmacaopedido;
	protected BaixaestoqueEnum baixaestoqueEnum = BaixaestoqueEnum.APOS_VENDAREALIZADA;
	protected GeracaocontareceberEnum geracaocontareceberEnum = GeracaocontareceberEnum.APOS_VENDAREALIZADA;
	protected Documentoacao documentoacao;
	protected Boolean naoatualizarvencimento;
	protected Boolean requeraprovacaopedido;
	protected Boolean requeraprovacaovalorvendaminimo;
	protected Boolean obrigarTerceiro;
	protected Boolean obrigarProjeto;
	protected Projeto projetoprincipal;
	protected Boolean obrigarPrazoEntrega;
	protected Boolean coletaautomatica;
	protected Boolean producaoautomatica;
	protected Producaoagendatipo producaoagendatipo;
	protected Localarmazenagem localarmazenagemcoleta;
	protected Naturezaoperacao naturezaoperacaonotafiscalentrada;
	protected Integer diascancelamento;
	protected Boolean gerarNotaIndustrializacaoRetorno;
	protected Cfop cfopindustrializacao;
	protected Cfop cfopretorno;
	protected List<Campoextrapedidovendatipo> listaCampoextrapedidovenda;
	protected Boolean obrigarLoteOrcamento;
	protected Boolean obrigarLotePedidovenda;
	protected Boolean obrigarLoteConfirmarpedido;
	protected Boolean obrigarLoteVenda;
	protected Boolean permitirvendasemestoque;
	protected Boolean sincronizarComWMS;
	protected Boolean confirmacaoManualWMS;
	protected Boolean atualizaPedidoVendaWMS;
	protected Boolean representacao;
	protected Presencacompradornfe presencacompradornfe;
	protected Boolean obrigarpresencacompradornfe;
	protected Boolean referenciarnotaentradananotasaida;
	protected Boolean obrigarinformarnotafiscalcoleta;
	protected Boolean gerarnotaretornovenda;
	protected Boolean exibirescolhaageciavendas;
	protected Boolean permitiralterarvendanaofaturada;
	protected Boolean gerarexpedicaovenda;
	protected Boolean criarExpedicaoComConferencia;
	protected Boolean considerarvalorcusto;
	protected Boolean consideraripiconta;
	protected Boolean consideraricmsstconta;
	protected Boolean considerarfcpstconta;
	protected Boolean considerardesoneracaoconta;
	protected Boolean obrigarEnderecocliente;
	protected Boolean garantia;
	protected ReportTemplateBean templatenotaservico;
	protected ReportTemplateBean templateinfcontribuintenfse;
	protected ReportTemplateBean templateinfcontribuintenfe;
	protected Presencacompradornfe presencacompradornfecoleta;
	protected Naturezaoperacao naturezaoperacaosaidafiscal;
	protected Boolean naopermitirenviarecf;
	protected Boolean validarEstoquePedidoEnviarECF;
	protected Boolean validarestoquepedidonaaprovacao;
	protected Boolean agruparContas;
	private Boolean requerAprovacaoVenda;
	private Boolean permitirMaterialMestreVenda;
	protected Boolean considerarFreteNoCalculoIpi;
	protected TipoReservaEnum reservarApartir;
	protected SituacaoNotaEnum situacaoNota;
	protected String observacaoPadrao;
	protected ModeloDocumentoFiscalEnum modeloDocumentoFiscal;
	protected Boolean entregaFutura;
	protected Boolean necessarioExpedicao;
	protected Boolean vendasEcommerce;
	protected Boolean enviarDadosNotaAutomaticamenteEcommerce;
	protected Boolean permitirConfirmarPedidoBaixado;

	protected Boolean atualizarStatusEcommerceAoConfirmar;
	protected Boolean atualizarStatusEcommerceAoCancelar;
	protected String observacaoParaEnvioConfirmacao;
	protected String observacaoParaEnvioCancelamento;
	
	protected Boolean bloqQuantidaMetragem;
	protected Boolean bloqAlteracaoPreco;
	protected Boolean atualizarPedidoEcommerceStatusAoAprovar;
	protected Boolean validarTicketMedioPorFornecedor;
	
	private List<PedidoVendaTipoHistorico> listaPedidoVendaTipoHistorico;
	
//	LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	protected Boolean exibirindicadorentregavenda= true;
	public Pedidovendatipo(){}
	
	public Pedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}

	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendatipo")
	public Integer getCdpedidovendatipo() {
		return cdpedidovendatipo;
	}
	@Required
	@DescriptionProperty
	@DisplayName("Descri��o")
	@MaxLength(100)
	@CompararCampo
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(2)
	@CompararCampo
	public String getSigla() {
		return sigla;
	}
	@DisplayName("Principal")
	@CompararCampo
	public Boolean getPrincipal() {
		return principal;
	}
	@DisplayName("Bonifica��o")
	@CompararCampo
	public Boolean getBonificacao() {
		return bonificacao;
	}
	@DisplayName("Reserva")
	@CompararCampo
	public Boolean getReserva() {
		return reserva;
	}
	@DisplayName("Comodato")
	@CompararCampo
	public Boolean getComodato() {
		return comodato;
	}
	@DisplayName("Realizar coleta ap�s salvar o pedido de venda")
	@CompararCampo
	public Boolean getColetaautomatica() {
		return coletaautomatica;
	}
	@DisplayName("Gerar produ��o autom�tica")
	@CompararCampo
	public Boolean getProducaoautomatica() {
		return producaoautomatica;
	}
	@DisplayName("Origem da opera��o")
	@CompararCampo
	public Presencacompradornfe getPresencacompradornfe() {
		return presencacompradornfe;
	}
	@DisplayName("Obrigar origem da opera��o")
	@CompararCampo
	public Boolean getObrigarpresencacompradornfe() {
		return obrigarpresencacompradornfe;
	}
	@DisplayName("Referenciar NF de entrada nos itens da NF de sa�da")
	@CompararCampo
	public Boolean getReferenciarnotaentradananotasaida() {
		return referenciarnotaentradananotasaida;
	}
	@DisplayName("Obrigar informar nota fiscal quando a coleta � registrada")
	@CompararCampo
	public Boolean getObrigarinformarnotafiscalcoleta() {
		return obrigarinformarnotafiscalcoleta;
	}
	public void setObrigarinformarnotafiscalcoleta(
			Boolean obrigarinformarnotafiscalcoleta) {
		this.obrigarinformarnotafiscalcoleta = obrigarinformarnotafiscalcoleta;
	}
	@DisplayName("Gerar nota fiscal de retorno na venda")
	@CompararCampo
	public Boolean getGerarnotaretornovenda() {
		return gerarnotaretornovenda;
	}
	
	@DisplayName("Sempre requer aprova��o na venda")
	@CompararCampo
	public Boolean getRequerAprovacaoVenda() {
		return requerAprovacaoVenda;
	}
	
	@DisplayName("Permitir material mestre (grade) na venda")
	@CompararCampo
	public Boolean getPermitirMaterialMestreVenda() {
		return permitirMaterialMestreVenda;
	}
	
	@DisplayName("Bloquear Altera��o do Pre�o")
	@CompararCampo
	public Boolean getBloqAlteracaoPreco() {
		return bloqAlteracaoPreco;
	}
	@DisplayName("Bloquear Altera��o da Quantidade dos Itens com Metragem.")
	@CompararCampo
	public Boolean getBloqQuantidaMetragem() {
		return bloqQuantidaMetragem;
	}
	@DisplayName("Considerar Desonera��o ICMS na Conta")
	@CompararCampo
	public Boolean getConsiderardesoneracaoconta() {
		return considerardesoneracaoconta;
	}
	
	public void setConsiderardesoneracaoconta(Boolean considerardesoneracaoconta) {
		this.considerardesoneracaoconta = considerardesoneracaoconta;
	}
	public void setBloqAlteracaoPreco(Boolean bloqAlteracaoPreco) {
		this.bloqAlteracaoPreco = bloqAlteracaoPreco;
	}
	public void setBloqQuantidaMetragem(Boolean bloqQuantidaMetragem) {
		this.bloqQuantidaMetragem = bloqQuantidaMetragem;
	}
	
	public void setPermitirMaterialMestreVenda(Boolean permitirMaterialMestreVenda) {
		this.permitirMaterialMestreVenda = permitirMaterialMestreVenda;
	}
	
	public void setRequerAprovacaoVenda(Boolean requerAprovacaoVenda) {
		this.requerAprovacaoVenda = requerAprovacaoVenda;
	}
	
	public void setGerarnotaretornovenda(Boolean gerarnotaretornovenda) {
		this.gerarnotaretornovenda = gerarnotaretornovenda;
	}
	public void setReferenciarnotaentradananotasaida(
			Boolean referenciarnotaentradananotasaida) {
		this.referenciarnotaentradananotasaida = referenciarnotaentradananotasaida;
	}
	public void setPresencacompradornfe(Presencacompradornfe presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	public void setObrigarpresencacompradornfe(Boolean obrigarpresencacompradornfe) {
		this.obrigarpresencacompradornfe = obrigarpresencacompradornfe;
	}
	public void setProducaoautomatica(Boolean producaoautomatica) {
		this.producaoautomatica = producaoautomatica;
	}
	public void setColetaautomatica(Boolean coletaautomatica) {
		this.coletaautomatica = coletaautomatica;
	}
	public void setBonificacao(Boolean bonificacao) {
		this.bonificacao = bonificacao;
	}
	public void setCdpedidovendatipo(Integer cdpedidovendatipo) {
		this.cdpedidovendatipo = cdpedidovendatipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
	public void setReserva(Boolean reserva) {
		this.reserva = reserva;
	}
	public void setComodato(Boolean comodato) {
		this.comodato = comodato;
	}
	
	@DisplayName("Informa��es Adicionais de Interesse do Fisco")
	@CompararCampo
	public String getInfoadicionalfisco() {
		return infoadicionalfisco;
	}
	@DisplayName("Informa��es Adicionais de Interesse do Contribuinte")
	@CompararCampo
	public String getInfoadicionalcontrib() {
		return infoadicionalcontrib;
	}

	public void setInfoadicionalfisco(String infoadicionalfisco) {
		this.infoadicionalfisco = infoadicionalfisco;
	}
	public void setInfoadicionalcontrib(String infoadicionalcontrib) {
		this.infoadicionalcontrib = infoadicionalcontrib;
	}
	
	@DisplayName("Natureza de Opera��o (Venda)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	@CompararCampo
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	@DisplayName("Natureza de Opera��o (Expedi��o)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacaoexpedicao")
	@CompararCampo
	public Naturezaoperacao getNaturezaoperacaoexpedicao() {
		return naturezaoperacaoexpedicao;
	}
	
	@DisplayName("Natureza de Opera��o (Servi�o)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacaoservico")
	@CompararCampo
	public Naturezaoperacao getNaturezaoperacaoservico() {
		return naturezaoperacaoservico;
	}

	@DisplayName("Tipo de Produ��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagendatipo")
	public Producaoagendatipo getProducaoagendatipo() {
		return producaoagendatipo;
	}
	
	@DisplayName("Local de Entrada Autom�tica da Coleta/Troca na confirma��o do pedido")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagemcoleta")
	public Localarmazenagem getLocalarmazenagemcoleta() {
		return localarmazenagemcoleta;
	}
	
	@DisplayName("Natureza de opera��o da nota fiscal de entrada (Emiss�o pr�pria)")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacaonotafiscalentrada")
	public Naturezaoperacao getNaturezaoperacaonotafiscalentrada() {
		return naturezaoperacaonotafiscalentrada;
	}

	@DisplayName("Gerar nota na confirma��o do pedido")
	@CompararCampo
	public Boolean getGerarnotaconfirmacaopedido() {
		return gerarnotaconfirmacaopedido;
	}
	@Required
	@DisplayName("Baixar no estoque")
	@CompararCampo
	public BaixaestoqueEnum getBaixaestoqueEnum() {
		return baixaestoqueEnum;
	}
	@Required
	@DisplayName("Gerar conta a receber")
	@CompararCampo
	public GeracaocontareceberEnum getGeracaocontareceberEnum() {
		return geracaocontareceberEnum;
	}

	@DisplayName("Situa��o da conta a receber")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoacao")
	@CompararCampo
	public Documentoacao getDocumentoacao() {
		return documentoacao;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}

	public void setNaturezaoperacaoexpedicao(Naturezaoperacao naturezaoperacaoexpedicao) {
		this.naturezaoperacaoexpedicao = naturezaoperacaoexpedicao;
	}

	public void setNaturezaoperacaoservico(Naturezaoperacao naturezaoperacaoservico) {
		this.naturezaoperacaoservico = naturezaoperacaoservico;
	}

	public void setGerarnotaconfirmacaopedido(Boolean gerarnotaconfirmacaopedido) {
		this.gerarnotaconfirmacaopedido = gerarnotaconfirmacaopedido;
	}
	
	public void setBaixaestoqueEnum(BaixaestoqueEnum baixaestoqueEnum) {
		this.baixaestoqueEnum = baixaestoqueEnum;
	}

	public void setGeracaocontareceberEnum(
			GeracaocontareceberEnum geracaocontareceberEnum) {
		this.geracaocontareceberEnum = geracaocontareceberEnum;
	}

	public void setDocumentoacao(Documentoacao documentoacao) {
		this.documentoacao = documentoacao;
	}
	
	@DisplayName("Sempre requer Aprova��o do Pedido")
	@CompararCampo
	public Boolean getRequeraprovacaopedido() {
		return requeraprovacaopedido;
	}

	@DisplayName("Requer aprova��o se valor de venda/pedido abaixo do m�nimo")
	@CompararCampo
	public Boolean getRequeraprovacaovalorvendaminimo() {
		return requeraprovacaovalorvendaminimo;
	}
	
	@DisplayName("Dias de prazo para cancelamento autom�tico")
	@CompararCampo
	public Integer getDiascancelamento() {
		return diascancelamento;
	}

	public void setDiascancelamento(Integer diascancelamento) {
		this.diascancelamento = diascancelamento;
	}

	public void setRequeraprovacaovalorvendaminimo(Boolean requeraprovacaovalorvendaminimo) {
		this.requeraprovacaovalorvendaminimo = requeraprovacaovalorvendaminimo;
	}
	public void setRequeraprovacaopedido(Boolean requeraprovacaopedido) {
		this.requeraprovacaopedido = requeraprovacaopedido;
	}
	
	public void setLocalarmazenagemcoleta(
			Localarmazenagem localarmazenagemcoleta) {
		this.localarmazenagemcoleta = localarmazenagemcoleta;
	}

	public void setNaturezaoperacaonotafiscalentrada(Naturezaoperacao naturezaoperacaonotafiscalentrada) {
		this.naturezaoperacaonotafiscalentrada = naturezaoperacaonotafiscalentrada;
	}
	
	public void setProducaoagendatipo(Producaoagendatipo producaoagendatipo) {
		this.producaoagendatipo = producaoagendatipo;
	}

	//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Obrigar Terceiro")
	@CompararCampo
	public Boolean getObrigarTerceiro() {
		return obrigarTerceiro;
	}

	@DisplayName("Obrigar Projeto")
	@CompararCampo
	public Boolean getObrigarProjeto() {
		return obrigarProjeto;
	}
	
	@DisplayName("Projeto Principal")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetoprincipal")
	public Projeto getProjetoprincipal() {
		return projetoprincipal;
	}

	@DisplayName("Obrigar Prazo de Entrega")
	@CompararCampo
	public Boolean getObrigarPrazoEntrega() {
		return obrigarPrazoEntrega;
	}

	@DisplayName("Campos Extras")
	@OneToMany(mappedBy="pedidovendatipo")
	public List<Campoextrapedidovendatipo> getListaCampoextrapedidovenda() {
		return listaCampoextrapedidovenda;
	}
	
	@DisplayName("N�o permitir enviar para ECF")
	@CompararCampo
	public Boolean getNaopermitirenviarecf() {
		return naopermitirenviarecf;
	}
	
	@DisplayName("Observa��o Padr�o para o Pedido de Venda")
	public String getObservacaoPadrao() {
		return observacaoPadrao;
	}
	
	@DisplayName("Modelo da Nota Fiscal")
	public ModeloDocumentoFiscalEnum getModeloDocumentoFiscal() {
		return modeloDocumentoFiscal;
	}
	
	public Boolean getEntregaFutura() {
		return entregaFutura;
	}
	
	public void setEntregaFutura(Boolean entregaFutura) {
		this.entregaFutura = entregaFutura;
	}
	
	public void setNaopermitirenviarecf(Boolean naopermitirenviarecf) {
		this.naopermitirenviarecf = naopermitirenviarecf;
	}

	public void setObrigarProjeto(Boolean obrigarProjeto) {
		this.obrigarProjeto = obrigarProjeto;
	}

	public void setProjetoprincipal(Projeto projetoprincipal) {
		this.projetoprincipal = projetoprincipal;
	}

	public void setObrigarTerceiro(Boolean obrigarTerceiro) {
		this.obrigarTerceiro = obrigarTerceiro;
	}

	public void setObrigarPrazoEntrega(Boolean obrigarPrazoEntrega) {
		this.obrigarPrazoEntrega = obrigarPrazoEntrega;
	}
	
	public void setListaCampoextrapedidovenda(List<Campoextrapedidovendatipo> listaCampoextrapedidovenda) {
		this.listaCampoextrapedidovenda = listaCampoextrapedidovenda;
	}
	
	@DisplayName("Obrigar Lote no or�amento")
	@CompararCampo
	public Boolean getObrigarLoteOrcamento() {
		return obrigarLoteOrcamento;
	}
	public void setObrigarLoteOrcamento(Boolean obrigarLoteOrcamento) {
		this.obrigarLoteOrcamento = obrigarLoteOrcamento;
	}

	@DisplayName("Obrigar Lote no pedido de venda")
	@CompararCampo
	public Boolean getObrigarLotePedidovenda() {
		return obrigarLotePedidovenda;
	}
	public void setObrigarLotePedidovenda(Boolean obrigarLotePedidovenda) {
		this.obrigarLotePedidovenda = obrigarLotePedidovenda;
	}

	
	@DisplayName("Obrigar Lote ao confirmar pedido")
	@CompararCampo
	public Boolean getObrigarLoteConfirmarpedido() {
		return obrigarLoteConfirmarpedido;
	}
	public void setObrigarLoteConfirmarpedido(Boolean obrigarLoteConfirmarpedido) {
		this.obrigarLoteConfirmarpedido = obrigarLoteConfirmarpedido;
	}
	
	@DisplayName("Obrigar Lote na venda")
	@CompararCampo
	public Boolean getObrigarLoteVenda() {
		return obrigarLoteVenda;
	}
	public void setObrigarLoteVenda(Boolean obrigarLoteVenda) {
		this.obrigarLoteVenda = obrigarLoteVenda;
	}

	@DisplayName("Sincronizar com WMS")
	@CompararCampo
	public Boolean getSincronizarComWMS() {
		return sincronizarComWMS;
	}
	public void setSincronizarComWMS(Boolean sincronizarComWMS) {
		this.sincronizarComWMS = sincronizarComWMS;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdpedidovendatipo == null) ? 0 : cdpedidovendatipo
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedidovendatipo other = (Pedidovendatipo) obj;
		if (cdpedidovendatipo == null) {
			if (other.cdpedidovendatipo != null)
				return false;
		} else if (!cdpedidovendatipo.equals(other.cdpedidovendatipo))
			return false;
		return true;
	}
	
	@DisplayName("Gerar Nota de Industrializa��o com Retorno")
	@CompararCampo
	public Boolean getGerarNotaIndustrializacaoRetorno() {
		return gerarNotaIndustrializacaoRetorno;
	}
	
	public void setGerarNotaIndustrializacaoRetorno(Boolean gerarNotaIndustrializacaoRetorno) {
		this.gerarNotaIndustrializacaoRetorno = gerarNotaIndustrializacaoRetorno;
	}
	
	@DisplayName("CFOP de Industrializa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfopindustrializacao")
	public Cfop getCfopindustrializacao() {
		return cfopindustrializacao;
	}
	
	public void setCfopindustrializacao(Cfop cfopindustrializacao) {
		this.cfopindustrializacao = cfopindustrializacao;
	}
	
	@DisplayName("CFOP de Retorno")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfopretorno")
	public Cfop getCfopretorno() {
		return cfopretorno;
	}
	
	public void setCfopretorno(Cfop cfopretorno) {
		this.cfopretorno = cfopretorno;
	}

	@DisplayName("Representa��o")
	@CompararCampo
	public Boolean getRepresentacao() {
		return representacao;
	}
	public void setRepresentacao(Boolean representacao) {
		this.representacao = representacao;
	}
	
	@DisplayName("Exibir escolha de ag�ncia de vendas")
	@CompararCampo
	public Boolean getExibirescolhaageciavendas() {
		return exibirescolhaageciavendas;
	}
	
	public void setExibirescolhaageciavendas(Boolean exibirescolhaageciavendas) {
		this.exibirescolhaageciavendas = exibirescolhaageciavendas;
	}
	
	@DisplayName("Confirma��o manual do pedido")
	@CompararCampo
	public Boolean getConfirmacaoManualWMS() {
		return confirmacaoManualWMS;
	}
	public void setConfirmacaoManualWMS(Boolean confirmacaoManualWMS) {
		this.confirmacaoManualWMS = confirmacaoManualWMS;
	}
	
	@DisplayName("Atualizar quantidades do pedido")
	@CompararCampo
	public Boolean getAtualizaPedidoVendaWMS() {
		return atualizaPedidoVendaWMS;
	}
	public void setAtualizaPedidoVendaWMS(Boolean atualizaPedidoVendaWMS) {
		this.atualizaPedidoVendaWMS = atualizaPedidoVendaWMS;
	}

	@DisplayName("Permitir alterar a venda n�o faturada")
	@CompararCampo
	public Boolean getPermitiralterarvendanaofaturada() {
		return permitiralterarvendanaofaturada;
	}

	public void setPermitiralterarvendanaofaturada(Boolean permitiralterarvendanaofaturada) {
		this.permitiralterarvendanaofaturada = permitiralterarvendanaofaturada;
	}
	
	@DisplayName("Considerar pre�o de custo do material")
	@CompararCampo
	public Boolean getConsiderarvalorcusto() {
		return considerarvalorcusto;
	}

	public void setConsiderarvalorcusto(Boolean considerarvalorcusto) {
		this.considerarvalorcusto = considerarvalorcusto;
	}

	@DisplayName("Considerar o IPI na conta")
	@CompararCampo
	public Boolean getConsideraripiconta() {
		return consideraripiconta;
	}

	public void setConsideraripiconta(Boolean consideraripiconta) {
		this.consideraripiconta = consideraripiconta;
	}
	
	@DisplayName("Considerar o ICMS ST na conta")
	@CompararCampo
	public Boolean getConsideraricmsstconta() {
		return consideraricmsstconta;
	}
	public void setConsideraricmsstconta(Boolean consideraricmsstconta) {
		this.consideraricmsstconta = consideraricmsstconta;
	}
	
	@DisplayName("Considerar o FCP ST na conta")
	@CompararCampo
	public Boolean getConsiderarfcpstconta() {
		return considerarfcpstconta;
	}
	public void setConsiderarfcpstconta(Boolean considerarfcpstconta) {
		this.considerarfcpstconta = considerarfcpstconta;
	}
	
	@DisplayName("Obrigar endere�o do cliente")
	@CompararCampo
	public Boolean getObrigarEnderecocliente() {
		return obrigarEnderecocliente;
	}

	public void setObrigarEnderecocliente(Boolean obrigarEnderecocliente) {
		this.obrigarEnderecocliente = obrigarEnderecocliente;
	}

	@DisplayName("N�o atualizar data de vencimento")
	@CompararCampo
	public Boolean getNaoatualizarvencimento() {
		return naoatualizarvencimento;
	}

	public void setNaoatualizarvencimento(Boolean naoatualizarvencimento) {
		this.naoatualizarvencimento = naoatualizarvencimento;
	}

	@DisplayName("Garantia")
	@CompararCampo
	public Boolean getGarantia() {
		return garantia;
	}

	public void setGarantia(Boolean garantia) {
		this.garantia = garantia;
	}

	@JoinColumn(name="cdreporttemplate")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Template de discrimina��o dos servi�os (NFS-e)")
	@CompararCampo
	public ReportTemplateBean getTemplatenotaservico() {
		return templatenotaservico;
	}

	public void setTemplatenotaservico(ReportTemplateBean templatenotaservico) {
		this.templatenotaservico = templatenotaservico;
	}

	@JoinColumn(name="cdreporttemplateinfcontribuintenfse")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Template de informa��es do contribunte (NFS-e)")
	@CompararCampo
	public ReportTemplateBean getTemplateinfcontribuintenfse() {
		return templateinfcontribuintenfse;
	}

	public void setTemplateinfcontribuintenfse(
			ReportTemplateBean templateinfcontribuintenfse) {
		this.templateinfcontribuintenfse = templateinfcontribuintenfse;
	}

	@JoinColumn(name="cdreporttemplateinfcontribuintenfe")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Template de informa��es do contribunte (NF-e)")
	@CompararCampo
	public ReportTemplateBean getTemplateinfcontribuintenfe() {
		return templateinfcontribuintenfe;
	}

	public void setTemplateinfcontribuintenfe(
			ReportTemplateBean templateinfcontribuintenfe) {
		this.templateinfcontribuintenfe = templateinfcontribuintenfe;
	}
	
	@DisplayName("Origem da opera��o")
	public Presencacompradornfe getPresencacompradornfecoleta() {
		return presencacompradornfecoleta;
	}
	public void setPresencacompradornfecoleta(Presencacompradornfe presencacompradornfecoleta) {
		this.presencacompradornfecoleta = presencacompradornfecoleta;
	}
	
	@DisplayName("Natureza de opera��o da sa�da fiscal")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacaosaidafiscal")
	public Naturezaoperacao getNaturezaoperacaosaidafiscal() {
		return naturezaoperacaosaidafiscal;
	}
	
	public void setNaturezaoperacaosaidafiscal(Naturezaoperacao naturezaoperacaosaidafiscal) {
		this.naturezaoperacaosaidafiscal = naturezaoperacaosaidafiscal;
	}

	@DisplayName("Permitir venda sem estoque (Entrega futura)")
	@CompararCampo
	public Boolean getPermitirvendasemestoque() {
		return permitirvendasemestoque;
	}
	@DisplayName("Gerar expedi��o da venda")
	@CompararCampo
	public Boolean getGerarexpedicaovenda() {
		return gerarexpedicaovenda;
	}

	@DisplayName("Criar expedi��o com confer�ncia")
	public Boolean getCriarExpedicaoComConferencia() {
		return criarExpedicaoComConferencia;
	}

	public void setPermitirvendasemestoque(Boolean permitirvendasemestoque) {
		this.permitirvendasemestoque = permitirvendasemestoque;
	}

	public void setGerarexpedicaovenda(Boolean gerarexpedicaovenda) {
		this.gerarexpedicaovenda = gerarexpedicaovenda;
	}

	public void setCriarExpedicaoComConferencia(Boolean criarExpedicaoComConferencia) {
		this.criarExpedicaoComConferencia = criarExpedicaoComConferencia;
	}

	@DisplayName("Validar estoque do pedido ao enviar para ECF")
	@CompararCampo
	public Boolean getValidarEstoquePedidoEnviarECF() {
		return validarEstoquePedidoEnviarECF;
	}

	public void setValidarEstoquePedidoEnviarECF(Boolean validarEstoquePedidoEnviarECF) {
		this.validarEstoquePedidoEnviarECF = validarEstoquePedidoEnviarECF;
	}
	
	@DisplayName("Validar estoque do pedido ao aprovar")
	@CompararCampo
	public Boolean getValidarestoquepedidonaaprovacao() {
		return validarestoquepedidonaaprovacao;
	}
	
	public void setValidarestoquepedidonaaprovacao(
			Boolean validarestoquepedidonaaprovacao) {
		this.validarestoquepedidonaaprovacao = validarestoquepedidonaaprovacao;
	}
	
	@DisplayName("Agrupar contas a receber ao gerar uma nota fiscal para mais de uma venda")
	@CompararCampo
	public Boolean getAgruparContas() {
		return agruparContas;
	}
	
	public void setAgruparContas(Boolean agruparContas) {
		this.agruparContas = agruparContas;
	}
	
	@DisplayName("Considerar Frete no c�lculo do IPI/ ICMS/ ICMS ST/ FCP/ FCP ST/DIFAL")
	@CompararCampo
	public Boolean getConsiderarFreteNoCalculoIpi() {
		return considerarFreteNoCalculoIpi;
	}
	
	public void setConsiderarFreteNoCalculoIpi(
			Boolean considerarFreteNoCalculoIpi) {
		this.considerarFreteNoCalculoIpi = considerarFreteNoCalculoIpi;
	}
	
	@DisplayName("Reservar a partir")
	@CompararCampo
	public TipoReservaEnum getReservarApartir() {
		return reservarApartir;
	}
	
	public void setReservarApartir(TipoReservaEnum reservarApartir) {
		this.reservarApartir = reservarApartir;
	}

	@DisplayName("Situa��o da nota")
	@CompararCampo
	public SituacaoNotaEnum getSituacaoNota() {
		return situacaoNota;
	}
	
	public void setSituacaoNota(SituacaoNotaEnum situacaoNota) {
		this.situacaoNota = situacaoNota;
	}


	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="pedidovendatipo")
	public List<PedidoVendaTipoHistorico> getListaPedidoVendaTipoHistorico() {
		return listaPedidoVendaTipoHistorico;
	}

	public void setListaPedidoVendaTipoHistorico(
			List<PedidoVendaTipoHistorico> listaPedidoVendaTipoHistorico) {
		this.listaPedidoVendaTipoHistorico = listaPedidoVendaTipoHistorico;
	}

	
	public void setObservacaoPadrao(String observacaoPadrao) {
		this.observacaoPadrao = observacaoPadrao;
	}
	
	public void setModeloDocumentoFiscal(
			ModeloDocumentoFiscalEnum modeloDocumentoFiscal) {
		this.modeloDocumentoFiscal = modeloDocumentoFiscal;
	}
	

	@DisplayName("Emitir nota com expedi��o confirmada")
	@CompararCampo
	public Boolean getNecessarioExpedicao() {
		return necessarioExpedicao;
	}

	public void setNecessarioExpedicao(Boolean necessarioExpedicao) {
		this.necessarioExpedicao = necessarioExpedicao;
	}
	@DisplayName("VENDAS E-COMMERCE")
	public Boolean getVendasEcommerce() {
		return vendasEcommerce;
	}

	public void setVendasEcommerce(Boolean vendasEcommerce) {
		this.vendasEcommerce = vendasEcommerce;
	}

	@DisplayName("PERMITIR  CONFIRMAR PEDIDO APENAS SE BAIXADA")
	public Boolean getPermitirConfirmarPedidoBaixado() {
		return permitirConfirmarPedidoBaixado;
	}

	public void setPermitirConfirmarPedidoBaixado(
			Boolean permitirConfirmarPedidoBaixado) {
		this.permitirConfirmarPedidoBaixado = permitirConfirmarPedidoBaixado;
	}
	
	public Boolean getEnviarDadosNotaAutomaticamenteEcommerce() {
		return enviarDadosNotaAutomaticamenteEcommerce;
	}
	
	public void setEnviarDadosNotaAutomaticamenteEcommerce(
			Boolean enviarDadosNotaAutomaticamenteEcommerce) {
		this.enviarDadosNotaAutomaticamenteEcommerce = enviarDadosNotaAutomaticamenteEcommerce;
	}
	
	@DisplayName("Atualizar status do pedido ao confirmar")
	public Boolean getAtualizarStatusEcommerceAoConfirmar() {
		return atualizarStatusEcommerceAoConfirmar;
	}
	
	public void setAtualizarStatusEcommerceAoConfirmar(
			Boolean atualizarStatusEcommerceAoConfirmar) {
		this.atualizarStatusEcommerceAoConfirmar = atualizarStatusEcommerceAoConfirmar;
	}
	
	@DisplayName("Atualizar status do pedido ao cancelar")
	public Boolean getAtualizarStatusEcommerceAoCancelar() {
		return atualizarStatusEcommerceAoCancelar;
	}

	public void setAtualizarStatusEcommerceAoCancelar(
			Boolean atualizarStatusEcommerceAoCancelar) {
		this.atualizarStatusEcommerceAoCancelar = atualizarStatusEcommerceAoCancelar;
	}	
	
	@DisplayName("Observa��o para envio (Confirma��o)")
	public String getObservacaoParaEnvioConfirmacao() {
		return observacaoParaEnvioConfirmacao;
	}

	public void setObservacaoParaEnvioConfirmacao(
			String observacaoParaEnvioConfirmacao) {
		this.observacaoParaEnvioConfirmacao = observacaoParaEnvioConfirmacao;
	}

	@DisplayName("Observa��o para envio (Cancelamento)")
	public String getObservacaoParaEnvioCancelamento() {
		return observacaoParaEnvioCancelamento;
	}

	public void setObservacaoParaEnvioCancelamento(
			String observacaoParaEnvioCancelamento) {
		this.observacaoParaEnvioCancelamento = observacaoParaEnvioCancelamento;
	}

	@DisplayName("Atualizar status no pedido ao aprovar")
	public Boolean getAtualizarPedidoEcommerceStatusAoAprovar() {
		return atualizarPedidoEcommerceStatusAoAprovar;
	}
	public void setAtualizarPedidoEcommerceStatusAoAprovar(Boolean atualizarPedidoEcommerceStatusAoAprovar) {
		this.atualizarPedidoEcommerceStatusAoAprovar = atualizarPedidoEcommerceStatusAoAprovar;
	}

	@DisplayName("Exibir indicador de entrega na venda")
	public Boolean getExibirindicadorentregavenda() {
		return exibirindicadorentregavenda;
	}

	public void setExibirindicadorentregavenda(Boolean exibirindicadorentregavenda) {
		this.exibirindicadorentregavenda = exibirindicadorentregavenda;
	}
	
	
	@DisplayName("Validar ticket m�dio por fornecedor")
	public Boolean getValidarTicketMedioPorFornecedor() {
		return validarTicketMedioPorFornecedor;
	}
	public void setValidarTicketMedioPorFornecedor(Boolean validarTicketMedioPorFornecedor) {
		this.validarTicketMedioPorFornecedor = validarTicketMedioPorFornecedor;
	}
}