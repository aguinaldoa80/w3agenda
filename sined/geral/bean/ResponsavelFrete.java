package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class ResponsavelFrete {
	
	public static final Integer CD_EMITENTE = 1;
	public static final Integer CD_DESTINATARIO = 2;
	public static final Integer CD_TERCEIROS = 3;
	public static final Integer CD_SEM_FRETE = 4;
	public static final Integer CD_PROPRIO_REMETENTE = 5;
	public static final Integer CD_PROPRIO_DESTINATARIO = 6;
	
	
	public static final ResponsavelFrete EMITENTE = new ResponsavelFrete(CD_EMITENTE, "Contratação do Frete por conta do Remetente (CIF)", 0);
	public static final ResponsavelFrete DESTINATARIO = new ResponsavelFrete(CD_DESTINATARIO, "Contratação do Frete por conta do Destinatário (FOB)", 1);
	public static final ResponsavelFrete TERCEIROS = new ResponsavelFrete(CD_TERCEIROS, "Contratação do Frete por conta de Terceiros", 2);
	public static final ResponsavelFrete SEM_FRETE = new ResponsavelFrete(CD_SEM_FRETE, "Sem Ocorrência de Transporte", 9);
	public static final ResponsavelFrete PROPRIO_REMETENTE = new ResponsavelFrete(CD_PROPRIO_REMETENTE, "Transporte Próprio por conta do Remetente", 3);
	public static final ResponsavelFrete PROPRIO_DESTINATARIO = new ResponsavelFrete(CD_PROPRIO_DESTINATARIO, "Transporte Próprio por conta do Destinatário", 4);
	
	protected Integer cdResponsavelFrete;
	protected String nome;
	protected Integer cdnfe;
	
	public ResponsavelFrete() {
	}
	
	public ResponsavelFrete(Integer cdResponsavelFrete) {
		this.cdResponsavelFrete = cdResponsavelFrete;
	}
	
	public ResponsavelFrete(Integer cdResponsavelFrete, String nome, Integer cdnfe) {
		this.cdResponsavelFrete = cdResponsavelFrete;
		this.nome = nome;
		this.cdnfe = cdnfe;
	}


	@Id
	public Integer getCdResponsavelFrete() {
		return cdResponsavelFrete;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Integer getCdnfe() {
		return cdnfe;
	}
	
	public void setCdnfe(Integer cdnfe) {
		this.cdnfe = cdnfe;
	}
	
	public void setCdResponsavelFrete(Integer cdResponsavelFrete) {
		this.cdResponsavelFrete = cdResponsavelFrete;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static ResponsavelFrete getBean(Integer codigo){
		if(codigo.equals(EMITENTE.getCdnfe())){
			return EMITENTE;
		} else if(codigo.equals(DESTINATARIO.getCdnfe())){
			return DESTINATARIO;
		} else if(codigo.equals(TERCEIROS.getCdnfe())){
			return TERCEIROS;
		} else if(codigo.equals(SEM_FRETE.getCdnfe())){
			return SEM_FRETE;
		} else if(codigo.equals(PROPRIO_REMETENTE.getCdnfe())){
			return PROPRIO_REMETENTE;
		} else if(codigo.equals(PROPRIO_DESTINATARIO.getCdnfe())){
			return PROPRIO_DESTINATARIO;
		} else return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ResponsavelFrete)
			return this.cdResponsavelFrete.equals(((ResponsavelFrete)obj).getCdResponsavelFrete());
		return super.equals(obj);
	}
}
