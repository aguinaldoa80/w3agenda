package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_metacomercialitem", sequenceName = "sq_metacomercialitem")
public class Metacomercialitem {
	
	protected Integer cdmetacomercialitem;
	protected Metacomercial metacomercial;
	protected Cargo cargo;
	protected Colaborador colaborador;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_metacomercialitem")
	public Integer getCdmetacomercialitem() {
		return cdmetacomercialitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmetacomercial")
	public Metacomercial getMetacomercial() {
		return metacomercial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	
	public void setCdmetacomercialitem(Integer cdmetacomercialitem) {
		this.cdmetacomercialitem = cdmetacomercialitem;
	}
	public void setMetacomercial(Metacomercial metacomercial) {
		this.metacomercial = metacomercial;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
}