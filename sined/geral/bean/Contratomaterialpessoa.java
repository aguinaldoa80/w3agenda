package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_contratomaterialpessoa", sequenceName = "sq_contratomaterialpessoa")
public class Contratomaterialpessoa {
	
	protected Integer cdcontratomaterialpessoa;
	protected Contratomaterial contratomaterial;
	protected Cargo cargo;
	protected Double qtde;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratomaterialpessoa")
	public Integer getCdcontratomaterialpessoa() {
		return cdcontratomaterialpessoa;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}

	public void setCdcontratomaterialpessoa(Integer cdcontratomaterialpessoa) {
		this.cdcontratomaterialpessoa = cdcontratomaterialpessoa;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
}
