package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_entregadocumentocoleta", sequenceName = "sq_entregadocumentocoleta")
public class EntregadocumentoColeta{

	protected Integer cdentregadocumentocoleta;
	protected Coleta coleta;
	protected Entregadocumento entregadocumento;
	
	public EntregadocumentoColeta() {}
	public EntregadocumentoColeta(Integer cdcoleta, Integer cdentregadocumento) {
		this.coleta = new Coleta(cdcoleta);
		this.entregadocumento = new Entregadocumento(cdentregadocumento);
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregadocumentocoleta")
	public Integer getCdentregadocumentocoleta() {
		return cdentregadocumentocoleta;
	}
	public void setCdentregadocumentocoleta(Integer cdentregadocumentocoleta) {
		this.cdentregadocumentocoleta = cdentregadocumentocoleta;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}
	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}
}