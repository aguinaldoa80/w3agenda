package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.CreditoDebitoEnum;

@Entity
@DisplayName("Saldo do IPI")
@SequenceGenerator(name = "sq_saldoipi", sequenceName = "sq_saldoipi")
public class SaldoIpi {

	private Integer cdSaldoIpi;
	private Date mesAno;
	private Empresa empresa;
	private Money valor;
	private CreditoDebitoEnum tipoUtilizacao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;

	protected String mesAnoAux;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_saldoipi")
	public Integer getCdSaldoIpi() {
		return cdSaldoIpi;
	}

	public void setCdSaldoIpi(Integer cdSaldoIpi) {
		this.cdSaldoIpi = cdSaldoIpi;
	}

	public Date getMesAno() {
		return mesAno;
	}

	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Required
	@DisplayName("Tipo de Utiliza��o")
	public CreditoDebitoEnum getTipoUtilizacao() {
		return tipoUtilizacao;
	}
	
	public void setTipoUtilizacao(CreditoDebitoEnum tipoUtilizacao) {
		this.tipoUtilizacao = tipoUtilizacao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Required
	@Transient
	@DisplayName("M�s/ano")
	public String getMesAnoAux() {
		if (this.mesAno != null) {
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		}
		
		return null;
	}

	public void setMesAnoAux(String mesAnoAux) {
		try {
			this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAnoAux).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
