package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Documentoacao {
	
	public static final Documentoacao CANCELADA = new Documentoacao(0, "Cancelada");
	public static final Documentoacao PREVISTA = new Documentoacao(1,"Prevista");
	public static final Documentoacao DEFINITIVA = new Documentoacao(2,"Definitiva"); 
	public static final Documentoacao AUTORIZADA = new Documentoacao(3,"Autorizada"); 
	public static final Documentoacao AUTORIZADA_PARCIAL = new Documentoacao(4,"Autorizada parcial");
	public static final Documentoacao NAO_AUTORIZADA = new Documentoacao(5,"N�o autorizada"); 
	public static final Documentoacao BAIXADA = new Documentoacao(6, "Baixada"); 
	public static final Documentoacao BAIXADA_PARCIAL = new Documentoacao(7, "Baixada parcial");
	
	public static final Documentoacao ALTERADA = new Documentoacao(101, "Alterada"); 
	public static final Documentoacao ESTORNADA = new Documentoacao(102, "Estornada"); 
	public static final Documentoacao PROTESTADA = new Documentoacao(103,"Protestada"); 
	public static final Documentoacao CANCEL_MOVIMENTACAO = new Documentoacao(104, "Cancel. Mov. Finan.");
	public static final Documentoacao REAGENDAMENTO = new Documentoacao(105, "Reagendada");
	public static final Documentoacao ENVIO_BOLETO = new Documentoacao(106, "Envio de boleto");
	public static final Documentoacao NEGOCIADA = new Documentoacao(107, "Negociada");
	public static final Documentoacao DEVOLVIDA = new Documentoacao(108, "Devolvida");
	public static final Documentoacao MUDANCA_CLIENTE = new Documentoacao(109, "Mudan�a de cliente");
	public static final Documentoacao ENVIO_MANUAL_BOLETO = new Documentoacao(110, "Envio manual de boleto");
	public static final Documentoacao ATRASADA = new Documentoacao(111, "Atrasada");
	public static final Documentoacao PAG_ATRASADO = new Documentoacao(112, "Pagamentos ap�s vencimento");
	public static final Documentoacao ASSOCIADA = new Documentoacao(113, "Associada");
	public static final Documentoacao PENDENTE = new Documentoacao(114, "Pendente");
	public static final Documentoacao ATRASADA_CONTRATO_ATIVO = new Documentoacao(115, "Atrasada com contrato ativo");
	public static final Documentoacao ATRASADA_CONTRATO_SUSPENSO = new Documentoacao(116, "Atrasada com contrato suspenso");
	public static final Documentoacao RETIRADA_PROTESTO = new Documentoacao(117, "Protesto retirado");
	public static final Documentoacao RATEIO_ATUALIZADO = new Documentoacao(118, "Rateio atualizado");
	public static final Documentoacao ATRASADA_SEM_PROTESTO = new Documentoacao(118, "Atrasada e sem protesto");
	public static final Documentoacao BOLETO_GERADO = new Documentoacao(119, "Boleto Gerado");
	public static final Documentoacao ATRASADA_CONTRATO_CANCELADO = new Documentoacao(120, "Atrasado com contrato cancelado");
	public static final Documentoacao ENVIADO_CARTORIO = new Documentoacao(121,"Enviado ao Cart�rio");
	public static final Documentoacao ENVIADO_JURIDICO = new Documentoacao(122,"Enviado ao Jur�dico");
	public static final Documentoacao COBRANCA_DEBITO = new Documentoacao(123, "Cobran�a de D�bito");
	public static final Documentoacao BOLETO_NAO_GERADO = new Documentoacao(124, "Boleto n�o Gerado");
	public static final Documentoacao ANTECIPACAO = new Documentoacao(125, "Adiantamento");
	public static final Documentoacao CANCELAMENTO_DE_ANTECIPACAO = new Documentoacao(126, "Cancelamento de Adiantamento");
	public static final Documentoacao ALTERADA_PELO_USUARIO = new Documentoacao(127, "Alterada pelo usu�rio");
	public static final Documentoacao ENVIO_AUTOMATICO = new Documentoacao(128, "Envio autom�tico");
	public static final Documentoacao ARQUIVO_MORTO = new Documentoacao(129, "Arquivo Morto");
	public static final Documentoacao CUSTEIO = new Documentoacao(130, "Alterado por custeio'");
	public static final Documentoacao COMPENSACAO_SALDO = new Documentoacao(131, "Compensa��o de Saldo");
	public static final Documentoacao NOTA_PROMISSORIA_GERADA = new Documentoacao(132, "Nota promiss�ria gerada");
	
	
	protected Integer cddocumentoacao;
	protected String nome;
	protected Boolean mostradocumento;
	
	public Documentoacao(){}
	
	public Documentoacao(Integer cddocumentoacao){
		this.cddocumentoacao = cddocumentoacao;
	}
	
	public Documentoacao(Integer cddocumentoacao,String nome){
		this.cddocumentoacao = cddocumentoacao;
		this.nome = nome;
	}
	
	@Id
	public Integer getCddocumentoacao() {
		return cddocumentoacao;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getMostradocumento() {
		return mostradocumento;
	}
	
	public void setCddocumentoacao(Integer cddocumentoacao) {
		this.cddocumentoacao = cddocumentoacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMostradocumento(Boolean mostradocumento) {
		this.mostradocumento = mostradocumento;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Documentoacao) {
			Documentoacao documentoacao = (Documentoacao) obj;
			return documentoacao.getCddocumentoacao().equals(this.getCddocumentoacao());
		}
		return super.equals(obj);
	}

	public String toSigla(){
		if(this.cddocumentoacao != null){
			if(PREVISTA.equals(this)){
				return "P";
			}else if(DEFINITIVA.equals(this)){
				return "D";
			}else if(CANCELADA.equals(this)){
				return "C";
			}else if(BAIXADA.equals(this)){
				return "B";
			}else if(BAIXADA_PARCIAL.equals(this)){
				return "BP";
			}else if(AUTORIZADA.equals(this)){
				return "A";
			}else if(AUTORIZADA_PARCIAL.equals(this)){
				return "AP";
			}else if(NAO_AUTORIZADA.equals(this)){
				return "NA";
			}else if(NEGOCIADA.equals(this)){
				return "N";
			}else return "";
		}else return "";
	}
}
