package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
@Entity
@SequenceGenerator(name="sq_grupotributacaoempresa",sequenceName="sq_grupotributacaoempresa")
public class Grupotributacaoempresa{
	
	protected Integer cdgrupotributacaoempresa;
	protected Grupotributacao grupotributacao;
	protected Empresa empresa;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacaoempresa")
	public Integer getCdgrupotributacaoempresa() {
		return cdgrupotributacaoempresa;
	}
	@DisplayName("Grupo tributação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setCdgrupotributacaoempresa(Integer cdgrupotributacaoempresa) {
		this.cdgrupotributacaoempresa = cdgrupotributacaoempresa;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
