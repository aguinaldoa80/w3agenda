package br.com.linkcom.sined.geral.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_contratojuridico;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohonorario;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratojuridicoParcelaRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratojuridicoRTF;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@DisplayName("Contrato")
@SequenceGenerator(name = "sq_contratojuridico", sequenceName = "sq_contratojuridico")
@JoinEmpresa("contratojuridico.empresa")
public class Contratojuridico implements Log {
	
	protected Integer cdcontratojuridico;
	protected Cliente cliente;
	protected String descricao;
	protected Date dtinicio;
	protected Empresa empresa;
	protected Conta conta;
	protected Processojuridico processojuridico;
	protected String anotacoes;
	protected Prazopagamento prazopagamento;
	protected Money valor;
	protected Rateio rateio;
	protected Taxa taxa;
	protected Money valorcausa;
	protected Tipohonorario tipohonorario;
	protected Money honorario;
	protected Date dtcancelado;
	protected Date dtsentenca;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Contratotipo contratotipo;
	
	protected Aux_contratojuridico aux_contratojuridico;
	
	protected List<Contratojuridicoparcela> listaContratojuridicoparcela = new ListSet<Contratojuridicoparcela>(Contratojuridicoparcela.class);
	protected List<Contratojuridicohistorico> listaContratojuridicohistorico = new ListSet<Contratojuridicohistorico>(Contratojuridicohistorico.class);
	
	//TRANSIENTE
	protected String historico;
	
	public Contratojuridico() {
	}
	
	public Contratojuridico(Integer cdcontratojuridico) {
		this.cdcontratojuridico = cdcontratojuridico;
	}


	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratojuridico")
	public Integer getCdcontratojuridico() {
		return cdcontratojuridico;
	}

	public Date getDtcancelado() {
		return dtcancelado;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@Required
	@MaxLength(100)
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Conta banc�ria")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}

	@DisplayName("Processo jur�dico")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}

	@DisplayName("Anota��es")
	@MaxLength(255)
	public String getAnotacoes() {
		return anotacoes;
	}

	@Required
	@DisplayName("Prazo de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}

	@Required
	@DisplayName("Valor do contrato")
	public Money getValor() {
		return valor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtaxa")
	public Taxa getTaxa() {
		return taxa;
	}
	
	public Date getDtsentenca() {
		return dtsentenca;
	}

	@DisplayName("Valor da causa")
	public Money getValorcausa() {
		return valorcausa;
	}

	@DisplayName("Honor�rio")
	public Money getHonorario() {
		return honorario;
	}

	@DisplayName("Tipo de honor�rio")
	public Tipohonorario getTipohonorario() {
		return tipohonorario;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Tipo de contrato")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratotipo")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}

	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratojuridico", insertable=false, updatable=false)
	public Aux_contratojuridico getAux_contratojuridico() {
		return aux_contratojuridico;
	}
	
	@DisplayName("Parcela(s)")
	@OneToMany(mappedBy="contratojuridico")
	public List<Contratojuridicoparcela> getListaContratojuridicoparcela() {
		return listaContratojuridicoparcela;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="contratojuridico")
	public List<Contratojuridicohistorico> getListaContratojuridicohistorico() {
		return listaContratojuridicohistorico;
	}
	
	public void setListaContratojuridicohistorico(
			List<Contratojuridicohistorico> listaContratojuridicohistorico) {
		this.listaContratojuridicohistorico = listaContratojuridicohistorico;
	}
	
	public void setListaContratojuridicoparcela(
			List<Contratojuridicoparcela> listaContratojuridicoparcela) {
		this.listaContratojuridicoparcela = listaContratojuridicoparcela;
	}
	
	public void setAux_contratojuridico(
			Aux_contratojuridico aux_contratojuridico) {
		this.aux_contratojuridico = aux_contratojuridico;
	}

	public void setCdcontratojuridico(Integer cdcontratojuridico) {
		this.cdcontratojuridico = cdcontratojuridico;
	}

	public void setDtcancelado(Date dtcancelado) {
		this.dtcancelado = dtcancelado;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}

	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}

	public void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}

	public void setDtsentenca(Date dtsentenca) {
		this.dtsentenca = dtsentenca;
	}

	public void setValorcausa(Money valorcausa) {
		this.valorcausa = valorcausa;
	}

	public void setHonorario(Money honorario) {
		this.honorario = honorario;
	}

	public void setTipohonorario(Tipohonorario tipohonorario) {
		this.tipohonorario = tipohonorario;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	//TRANSENTE
	
	@Transient
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	@Transient
	public ContratojuridicoRTF getContratojuridicoRTF() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date dataAtual = new java.util.Date(System.currentTimeMillis());
		
		Cliente cliente = getCliente();
		
		Endereco endereco = cliente.getEndereco();
		
		Set<Telefone> listaTelefone = cliente.getListaTelefone();
		
		Set<Telefone> listaCelular = new HashSet<Telefone>();
		Set<Telefone> listaNaoCelular = new HashSet<Telefone>();
		
		if (listaTelefone!=null){
			for (Telefone telefone: listaTelefone) {
				Telefonetipo telefonetipo = telefone.getTelefonetipo();
				if (telefonetipo!=null && telefonetipo.getCdtelefonetipo().equals(Telefonetipo.CELULAR)){
					listaCelular.add(telefone);
				}else {
					listaNaoCelular.add(telefone);
				}
			}
		}
		
		ContratojuridicoRTF contratojuridicoRTF = new ContratojuridicoRTF();
		
		contratojuridicoRTF.setId_contrato(getCdcontratojuridico() != null ? getCdcontratojuridico().toString():"");
		
		contratojuridicoRTF.setTipo_contrato(getContratotipo() != null && getContratotipo().getNome() != null ? getContratotipo().getNome():"");
			
		contratojuridicoRTF.setNome_cliente(cliente.getNome());		
		
		contratojuridicoRTF.setEndereco(endereco!=null?endereco.getLogradouro():"");
		
		contratojuridicoRTF.setComplemento(endereco!=null && endereco.getComplemento()!=null ? endereco.getComplemento():"");
		
		contratojuridicoRTF.setNumero(endereco!=null && endereco.getNumero()!=null ? endereco.getNumero():"");
		
		contratojuridicoRTF.setBairro(endereco!=null && endereco.getBairro()!=null ? endereco.getBairro():"");
		
		contratojuridicoRTF.setMunicipio(endereco!=null && endereco.getMunicipio()!=null ? endereco.getMunicipio().getNome():"");
		
		contratojuridicoRTF.setUf(endereco!=null && endereco.getMunicipio()!=null && endereco.getMunicipio().getUf()!=null ? endereco.getMunicipio().getUf().getSigla():"");
		
		contratojuridicoRTF.setCpf(cliente.getCpf()!=null ? cliente.getCpf().toString():"");
		
		contratojuridicoRTF.setCnpj(cliente.getCnpj()!=null ? cliente.getCnpj().toString():"");
		
		contratojuridicoRTF.setCelular(CollectionsUtil.listAndConcatenate(listaCelular, "telefone", "/"));
		
		contratojuridicoRTF.setTelefone(CollectionsUtil.listAndConcatenate(listaNaoCelular, "telefone", "/"));
		
		contratojuridicoRTF.setDescricao_contrato(getDescricao());
		
		contratojuridicoRTF.setData_inicio_contrato(getDtinicio()!=null?dateFormat.format(getDtinicio()):"");
		
		contratojuridicoRTF.setData_fim_contrato("");
		
		contratojuridicoRTF.setValor_contrato(getValor()!=null? getValor().toString():"");
		
		contratojuridicoRTF.setValor_contrato_extenso(getValor() != null ? new Extenso(getValor()).toString() :"");
		
		contratojuridicoRTF.setData_extenso(new SimpleDateFormat("d' de 'MMMM' de 'yyyy").format(dataAtual));

		contratojuridicoRTF.setProfissao(cliente.getClienteprofissao()!=null ? cliente.getClienteprofissao().getNome() : "");
		
		contratojuridicoRTF.setEstado_civil(cliente.getEstadocivil()!=null ? cliente.getEstadocivil().getNome() : "");
		
		contratojuridicoRTF.setIdentidade(cliente.getRg()!=null ? cliente.getRg(): "");
		
		contratojuridicoRTF.setNacionalidade(cliente.getNacionalidade()!=null ? cliente.getNacionalidade().getDescricao() : "");
		
		contratojuridicoRTF.setAnotacoes(getAnotacoes()!=null ? getAnotacoes() : "");
		
		contratojuridicoRTF.setTipo_honorario(getTipohonorario() != null && getTipohonorario().getDescricao() != null ? getTipohonorario().getDescricao():"");
		
		contratojuridicoRTF.setHonorario(getHonorario() != null ? getHonorario().toString() : "");
		
		List<ContratojuridicoParcelaRTF> lista = new ArrayList<ContratojuridicoParcelaRTF>();
		ContratojuridicoParcelaRTF contratojuridicoParcelaRTF;
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		if(listaContratojuridicoparcela != null && !listaContratojuridicoparcela.isEmpty()){			
			for(Contratojuridicoparcela p : listaContratojuridicoparcela){
				contratojuridicoParcelaRTF = new ContratojuridicoParcelaRTF();
				contratojuridicoParcelaRTF.setData_vencimento(p.getDtvencimento() != null ? formato.format(p.getDtvencimento()) :"");
				contratojuridicoParcelaRTF.setValor(p.getValor() != null ? p.getValor().toString() : "");
				
				lista.add(contratojuridicoParcelaRTF);
			}
		}
		contratojuridicoRTF.setParcelas(lista);
		
		Empresa empresa = getEmpresa();
		if(empresa == null || 
				empresa.getCdpessoa() == null || 
				empresa.getLogomarca() == null || 
				empresa.getLogomarca().getCdarquivo() == null)
			empresa = EmpresaService.getInstance().loadArquivoPrincipal();
		
		if(empresa.getLogomarca() != null && empresa.getLogomarca().getCdarquivo() != null){
			Arquivo logo = empresa.getLogomarca();
			try {
				File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(logo));
				BufferedImage image = ImageIO.read(input);
				
				File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(logo));
				ImageIO.write(image, "png", output);  
				
				contratojuridicoRTF.setLogo(new FileInputStream(output));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		return contratojuridicoRTF;
	}
	
	@Transient
	public String getNome() {
		return getDescricao();
	}
}