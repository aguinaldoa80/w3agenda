package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_propostacaixaitem",sequenceName="sq_propostacaixaitem")
public class PropostaCaixaItem implements Log{

	protected Integer cdpropostacaixaitem;
	protected Propostacaixa caixa;
	protected String nome;
	protected String conteudopadrao;
	protected boolean obrigatorio;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_propostacaixaitem",strategy=GenerationType.AUTO)
	public Integer getCdpropostacaixaitem() {
		return cdpropostacaixaitem;
	}
	
	public void setCdpropostacaixaitem(Integer cdpropostacaixaitem) {
		this.cdpropostacaixaitem = cdpropostacaixaitem;
	}
	
	@DisplayName("Caixa da proposta")
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdpropostacaixa")
	public Propostacaixa getCaixa() {
		return caixa;
	}
	
	public void setCaixa(Propostacaixa caixa) {
		this.caixa = caixa;
	}
	
	@Required
	@DescriptionProperty
	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Campo Obrigat�rio")
	public boolean isObrigatorio() {
		return obrigatorio;
	}
	
	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	
	@DisplayName("Conte�do padr�o")
	public String getConteudopadrao() {
		return conteudopadrao;
	}
	
	public void setConteudopadrao(String conteudopadrao) {
		this.conteudopadrao = conteudopadrao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
