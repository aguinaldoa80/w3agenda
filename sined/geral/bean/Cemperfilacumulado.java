package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.TipoImportacaoProducao;
import br.com.linkcom.sined.modulo.producao.controller.process.bean.ImportacaoproducaoMaterial;


@Entity
@SequenceGenerator(name = "sq_cemperfilacumulado", sequenceName = "sq_cemperfilacumulado")
public class Cemperfilacumulado implements ImportacaoproducaoMaterial {

	protected Integer cdcemperfilacumulado;
	protected Cemobra cemobra;
	protected String ref;
	protected String codigo;
	protected String trat;
	protected String linha;
	protected String classe_id;
	protected String descricao;
	protected Double qtde;
	protected Double tam; 
	protected Double peso;
	protected Double peso_sobra;
	protected Double custo;
	protected Double custo_trat;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cemperfilacumulado")
	public Integer getCdcemperfilacumulado() {
		return cdcemperfilacumulado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcemobra")
	public Cemobra getCemobra() {
		return cemobra;
	}

	@MaxLength(20)
	public String getRef() {
		return ref;
	}

	@MaxLength(20)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public Double getQtde() {
		return qtde;
	}

	public Double getTam() {
		return tam;
	}

	public Double getCusto() {
		return custo;
	}
	
	@MaxLength(20)
	public String getTrat() {
		return trat;
	}

	public Double getPeso() {
		return peso;
	}
	
	@MaxLength(20)
	public String getLinha() {
		return linha;
	}
	
	@MaxLength(20)
	public String getClasse_id() {
		return classe_id;
	}

	public Double getPeso_sobra() {
		return peso_sobra;
	}

	public Double getCusto_trat() {
		return custo_trat;
	}

	public void setClasse_id(String classeId) {
		classe_id = classeId;
	}

	public void setPeso_sobra(Double pesoSobra) {
		peso_sobra = pesoSobra;
	}

	public void setCusto_trat(Double custoTrat) {
		custo_trat = custoTrat;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public void setTrat(String trat) {
		this.trat = trat;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public void setCdcemperfilacumulado(Integer cdcemperfilacumulado) {
		this.cdcemperfilacumulado = cdcemperfilacumulado;
	}

	public void setCemobra(Cemobra cemobra) {
		this.cemobra = cemobra;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setTam(Double tam) {
		this.tam = tam;
	}

	public void setCusto(Double custo) {
		this.custo = custo;
	}
	
	@Transient
	@Override
	public Double getQuantidade() {
		return this.qtde != null ? this.qtde : 1;
	}
	
	@Transient
	@Override
	public String getUnidademedidaMaterial() {
		return "BARRA";
	}

	@Transient
	@Override
	public Double getAlturaMaterial() {
		return null;
	}

	@Transient
	@Override
	public Double getLarguraMaterial() {
		return null;
	}
	
	@Transient
	@Override
	public Double getComprimentoMaterial() {
		return this.tam;
	}
	
	@Transient
	@Override
	public TipoImportacaoProducao getTipoImportacao() {
		return TipoImportacaoProducao.PERFIL;
	}
	
	@Transient
	@Override
	public String getNomeMaterial() {
		return this.descricao + 
				(this.codigo != null ? (" " + this.codigo) : "") + 
				(this.trat != null ? (" - " + this.trat) : "");
	}
	
	@Transient
	@Override
	public Double getCustoMaterial() {
		return (this.custo + this.custo_trat)/this.qtde;
	}
	
	@Transient
	@Override
	public Double getPesoMaterial() {
		return this.peso/this.qtde;
	}
	
	@Transient
	@Override
	public Double getVolume() {
		return this.qtde;
	}
	
	@Transient
	@Override
	public String getCorMaterial() {
		return this.trat;
	}

	@Transient	
	@Override
	public String getGrupoMaterial() {
		return this.ref;
	}

	@Transient	
	@Override
	public String getTipoMaterial() {
		return "PERFIL";
	}
	
}
