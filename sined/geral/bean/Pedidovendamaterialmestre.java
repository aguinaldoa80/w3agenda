package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicms;
import br.com.linkcom.sined.geral.bean.enumeration.Modalidadebcicmsst;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;

@Entity
@SequenceGenerator(name = "sq_pedidovendamaterialmestre", sequenceName = "sq_pedidovendamaterialmestre")
public class Pedidovendamaterialmestre implements PopupImpostoVendaInterface{

	private Integer cdpedidovendamaterialmestre;
    private Double altura;
    private Double largura;
    private Double comprimento;
    private Double peso; 
    private Pedidovenda pedidovenda;
    private Material material;
    private Double qtde;
    private String identificadorinterno;
    protected Double preco;
	protected Money desconto;
	protected Unidademedida unidademedida;
	protected Date dtprazoentrega;
	protected String identificadorespecifico;
	protected String nomealternativo;
	protected Boolean exibiritenskitflexivel;
	private Double saldo;
	private Money valoripi;
	private Double ipi;
	private Tipocobrancaipi tipocobrancaipi;
	private Tipocalculo tipoCalculoIpi;
	private Money aliquotaReaisIpi;
	private Grupotributacao grupotributacao;
	
	protected Tipotributacaoicms tipotributacaoicms;
	protected Tipocobrancaicms tipocobrancaicms;
	protected Modalidadebcicms modalidadebcicms;
	protected Money valorbcicms;
	protected Double icms;
	protected Money valoricms;
	protected Double percentualdesoneracaoicms;
	protected Money valordesoneracaoicms;
	protected Boolean abaterdesoneracaoicms;
	
	protected Money valorbcicmsst;
	protected Double icmsst;
	protected Money valoricmsst;
	protected Modalidadebcicmsst modalidadebcicmsst;
	
	protected Double reducaobcicmsst;
	protected Double margemvaloradicionalicmsst;
	
	protected Money valorbcfcp;
	protected Double fcp;
	protected Money valorfcp;
	protected Money valorbcfcpst;
	protected Double fcpst;
	protected Money valorfcpst;
	
	protected Money valorSeguro;
	protected Money outrasdespesas;
	
	protected Boolean dadosicmspartilha;
	protected Money valorbcdestinatario;
	protected Money valorbcfcpdestinatario;
	protected Double fcpdestinatario;
	protected Double icmsdestinatario;
	protected Double icmsinterestadual;
	protected Double icmsinterestadualpartilha;
	protected Money valorfcpdestinatario;
	protected Money valoricmsdestinatario;
	protected Money valoricmsremetente;
	protected Double difal;
	protected Money valordifal;
	
	protected Cfop cfop;
	protected Ncmcapitulo ncmcapitulo;
	protected String ncmcompleto;
    
    //transient
    protected Boolean estoquerealizado;
    protected Boolean entradaSaidaRealizada;
    protected Double qtdeanterior;
	protected Integer indice;
	protected Money totalImpostos;
    
    protected Money total;
    protected String nomealternativoAnterior;
	protected boolean exibirIpiPopup;
	protected boolean exibirIcmsPopup;
	protected boolean exibirDifalPopup;
	protected Money valorBruto;
    
    @Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendamaterialmestre")
	public Integer getCdpedidovendamaterialmestre() {
		return cdpedidovendamaterialmestre;
	}
	public Double getAltura() {
		return altura;
	}
	public Double getLargura() {
		return largura;
	}
	public Double getComprimento() {
		return comprimento;
	}
	public Double getPeso() {
		return peso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public Double getQtde() {
		return qtde;
	}
	
	
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdpedidovendamaterialmestre(Integer cdpedidovendamaterialmestre) {
		this.cdpedidovendamaterialmestre = cdpedidovendamaterialmestre;
	}
	public void setAltura(Double altura) {
		this.altura = altura;
	}
	public void setLargura(Double largura) {
		this.largura = largura;
	}
	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}

	@Transient
	public Boolean getEstoquerealizado() {
		return estoquerealizado;
	}
	@Transient
	public Boolean getEntradaSaidaRealizada() {
		return entradaSaidaRealizada;
	}
	public void setEntradaSaidaRealizada(Boolean entradaSaidaRealizada) {
		this.entradaSaidaRealizada = entradaSaidaRealizada;
	}
	public void setEstoquerealizado(Boolean estoquerealizado) {
		this.estoquerealizado = estoquerealizado;
	}
	
	public String getIdentificadorinterno() {
		return identificadorinterno;
	}
	public void setIdentificadorinterno(String identificadorinterno) {
		this.identificadorinterno = identificadorinterno;
	}
	
	public Double getPreco() {
		return preco;
	}
	public Money getDesconto() {
		return desconto;
	}
	@Transient
	public Unidademedida getUnidademedida() {
		if(unidademedida == null && material != null)
			return material.getUnidademedida();
		return unidademedida;
	}
	@DisplayName("Prazo de Entrega")
	public Date getDtprazoentrega() {
		return dtprazoentrega;
	}
	@Transient
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setDtprazoentrega(Date dtprazoentrega) {
		this.dtprazoentrega = dtprazoentrega;
	}
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	@Transient
	public Double getQtdeanterior() {
		return qtdeanterior;
	}
	public void setQtdeanterior(Double qtdeanterior) {
		this.qtdeanterior = qtdeanterior;
	}
	@DisplayName("Nome Alternativo")
	public String getNomealternativo() {
		return nomealternativo;
	}

	public void setNomealternativo(String nomealternativo) {
		this.nomealternativo = nomealternativo;
	}
	@DisplayName("Exibir itens na nota")
	public Boolean getExibiritenskitflexivel() {
		return exibiritenskitflexivel;
	}
	public void setExibiritenskitflexivel(Boolean exibiritenskitflexivel) {
		this.exibiritenskitflexivel = exibiritenskitflexivel;
	}
	
	@Transient
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public Double getIpi() {
		return ipi;
	}
	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}
	
	public Money getValoripi() {
		return valoripi;
	}
	public void setValoripi(Money valorIpi) {
		this.valoripi = valorIpi;
	}
	
	public Tipocobrancaipi getTipocobrancaipi() {
		return tipocobrancaipi;
	}
	public void setTipocobrancaipi(Tipocobrancaipi tipoCobrancaIpi) {
		this.tipocobrancaipi = tipoCobrancaIpi;
	}
	
	public Tipocalculo getTipoCalculoIpi() {
		return tipoCalculoIpi;
	}
	public void setTipoCalculoIpi(Tipocalculo tipoCalculoIpi) {
		this.tipoCalculoIpi = tipoCalculoIpi;
	}
	
	public Money getAliquotaReaisIpi() {
		return aliquotaReaisIpi;
	}
	
	public void setAliquotaReaisIpi(Money aliquotaReaisIpi) {
		this.aliquotaReaisIpi = aliquotaReaisIpi;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	public void setGrupotributacao(Grupotributacao grupoTributacao) {
		this.grupotributacao = grupoTributacao;
	}
	
	@Transient
	public String getNomealternativoAnterior() {
		return nomealternativoAnterior;
	}
	public void setNomealternativoAnterior(String nomealternativoAnterior) {
		this.nomealternativoAnterior = nomealternativoAnterior;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdpedidovendamaterialmestre == null) ? 0
						: cdpedidovendamaterialmestre.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedidovendamaterialmestre other = (Pedidovendamaterialmestre) obj;
		if (cdpedidovendamaterialmestre == null) {
			if (other.cdpedidovendamaterialmestre != null)
				return false;
		} else if (!cdpedidovendamaterialmestre
				.equals(other.cdpedidovendamaterialmestre))
			return false;
		return true;
	}
	public Tipotributacaoicms getTipotributacaoicms() {
		return tipotributacaoicms;
	}
	public void setTipotributacaoicms(Tipotributacaoicms tipotributacaoicms) {
		this.tipotributacaoicms = tipotributacaoicms;
	}
	public Tipocobrancaicms getTipocobrancaicms() {
		return tipocobrancaicms;
	}
	public void setTipocobrancaicms(Tipocobrancaicms tipocobrancaicms) {
		this.tipocobrancaicms = tipocobrancaicms;
	}
	public Modalidadebcicms getModalidadebcicms() {
		return modalidadebcicms;
	}
	public void setModalidadebcicms(Modalidadebcicms modalidadebcicms) {
		this.modalidadebcicms = modalidadebcicms;
	}
	public Money getValorbcicms() {
		return valorbcicms;
	}
	public void setValorbcicms(Money valorbcicms) {
		this.valorbcicms = valorbcicms;
	}
	public Double getIcms() {
		return icms;
	}
	public void setIcms(Double icms) {
		this.icms = icms;
	}
	public Money getValoricms() {
		return valoricms;
	}
	public void setValoricms(Money valoricms) {
		this.valoricms = valoricms;
	}
	public Money getValordesoneracaoicms() {
		return valordesoneracaoicms;
	}
	
	public void setValordesoneracaoicms(Money valordesoneracaoicms) {
		this.valordesoneracaoicms = valordesoneracaoicms;
	}
	
	public Double getPercentualdesoneracaoicms() {
		return percentualdesoneracaoicms;
	}
	
	public void setPercentualdesoneracaoicms(Double percentualdesoneracaoicms) {
		this.percentualdesoneracaoicms = percentualdesoneracaoicms;
	}
	
	public Boolean getAbaterdesoneracaoicms() {
		return abaterdesoneracaoicms;
	}
	
	public void setAbaterdesoneracaoicms(Boolean abaterdesoneracaoicms) {
		this.abaterdesoneracaoicms = abaterdesoneracaoicms;
	}
	
	public Money getValorbcicmsst() {
		return valorbcicmsst;
	}
	public void setValorbcicmsst(Money valorbcicmsst) {
		this.valorbcicmsst = valorbcicmsst;
	}
	public Double getIcmsst() {
		return icmsst;
	}
	public void setIcmsst(Double icmsst) {
		this.icmsst = icmsst;
	}
	public Money getValoricmsst() {
		return valoricmsst;
	}
	public void setValoricmsst(Money valoricmsst) {
		this.valoricmsst = valoricmsst;
	}
	public Modalidadebcicmsst getModalidadebcicmsst() {
		return modalidadebcicmsst;
	}
	public void setModalidadebcicmsst(Modalidadebcicmsst modalidadebcicmsst) {
		this.modalidadebcicmsst = modalidadebcicmsst;
	}
	public Double getReducaobcicmsst() {
		return reducaobcicmsst;
	}
	public void setReducaobcicmsst(Double reducaobcicmsst) {
		this.reducaobcicmsst = reducaobcicmsst;
	}
	public Double getMargemvaloradicionalicmsst() {
		return margemvaloradicionalicmsst;
	}
	public void setMargemvaloradicionalicmsst(Double margemvaloradicionalicmsst) {
		this.margemvaloradicionalicmsst = margemvaloradicionalicmsst;
	}
	public Money getValorbcfcp() {
		return valorbcfcp;
	}
	public void setValorbcfcp(Money valorbcfcp) {
		this.valorbcfcp = valorbcfcp;
	}
	public Double getFcp() {
		return fcp;
	}
	public void setFcp(Double fcp) {
		this.fcp = fcp;
	}
	public Money getValorfcp() {
		return valorfcp;
	}
	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}
	public Money getValorbcfcpst() {
		return valorbcfcpst;
	}
	public void setValorbcfcpst(Money valorbcfcpst) {
		this.valorbcfcpst = valorbcfcpst;
	}
	public Double getFcpst() {
		return fcpst;
	}
	public void setFcpst(Double fcpst) {
		this.fcpst = fcpst;
	}
	public Money getValorfcpst() {
		return valorfcpst;
	}
	public void setValorfcpst(Money valorfcpst) {
		this.valorfcpst = valorfcpst;
	}
	public Boolean getDadosicmspartilha() {
		return dadosicmspartilha;
	}
	public void setDadosicmspartilha(Boolean dadosicmspartilha) {
		this.dadosicmspartilha = dadosicmspartilha;
	}
	public Money getValorbcdestinatario() {
		return valorbcdestinatario;
	}
	public void setValorbcdestinatario(Money valorbcdestinatario) {
		this.valorbcdestinatario = valorbcdestinatario;
	}
	public Money getValorbcfcpdestinatario() {
		return valorbcfcpdestinatario;
	}
	public void setValorbcfcpdestinatario(Money valorbcfcpdestinatario) {
		this.valorbcfcpdestinatario = valorbcfcpdestinatario;
	}
	public Double getFcpdestinatario() {
		return fcpdestinatario;
	}
	public void setFcpdestinatario(Double fcpdestinatario) {
		this.fcpdestinatario = fcpdestinatario;
	}
	public Double getIcmsdestinatario() {
		return icmsdestinatario;
	}
	public void setIcmsdestinatario(Double icmsdestinatario) {
		this.icmsdestinatario = icmsdestinatario;
	}
	public Double getIcmsinterestadual() {
		return icmsinterestadual;
	}
	public void setIcmsinterestadual(Double icmsinterestadual) {
		this.icmsinterestadual = icmsinterestadual;
	}
	public Double getIcmsinterestadualpartilha() {
		return icmsinterestadualpartilha;
	}
	public void setIcmsinterestadualpartilha(Double icmsinterestadualpartilha) {
		this.icmsinterestadualpartilha = icmsinterestadualpartilha;
	}
	public Money getValorfcpdestinatario() {
		return valorfcpdestinatario;
	}
	public void setValorfcpdestinatario(Money valorfcpdestinatario) {
		this.valorfcpdestinatario = valorfcpdestinatario;
	}
	public Money getValoricmsdestinatario() {
		return valoricmsdestinatario;
	}
	public void setValoricmsdestinatario(Money valoricmsdestinatario) {
		this.valoricmsdestinatario = valoricmsdestinatario;
	}
	public Money getValoricmsremetente() {
		return valoricmsremetente;
	}
	public void setValoricmsremetente(Money valoricmsremetente) {
		this.valoricmsremetente = valoricmsremetente;
	}
	public Double getDifal() {
		return difal;
	}
	public void setDifal(Double difal) {
		this.difal = difal;
	}
	public Money getValordifal() {
		return valordifal;
	}
	public void setValordifal(Money valordifal) {
		this.valordifal = valordifal;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	@DisplayName("NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}
	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}
	public String getNcmcompleto() {
		return ncmcompleto;
	}
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}
	@Override
	@Transient
	public Double getQuantidade() {
		return getQtde();
	}
	@Override
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	@Override
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	@Override
	@Transient
	public Integer getIndice() {
		return indice;
	}
	public void setIndice(Integer indice) {
		this.indice = indice;
	}
	@Transient
	public Money getTotalImpostos() {
		Money totalImpostos = new Money()
							.add(getValoripi())
							.add(getValoricms())
							.add(getValoricmsst())
							.add(getValorfcp())
							.add(getValorfcpst())
							.add(getValordifal());
		if(abaterdesoneracaoicms != null && abaterdesoneracaoicms){
			totalImpostos = totalImpostos.subtract(getValordesoneracaoicms());
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	@Transient
	@Override
	public Boolean getExibirIpiPopup() {
		return exibirIpiPopup;
	}
	@Override
	public void setExibirIpiPopup(Boolean exibirIpiPopup) {
		this.exibirIpiPopup = exibirIpiPopup;
	}
	@Transient
	@Override
	public Boolean getExibirIcmsPopup() {
		return exibirIcmsPopup;
	}
	@Override
	public void setExibirIcmsPopup(Boolean exibirIcmsPopup) {
		this.exibirIcmsPopup = exibirIcmsPopup;
	}
	@Transient
	@Override
	public Boolean getExibirDifalPopup() {
		return exibirDifalPopup;
	}
	@Override
	public void setExibirDifalPopup(Boolean exibirDifalPopup) {
		this.exibirDifalPopup = exibirDifalPopup;
	}
	@Transient
	@Override
	public Money getValorBruto() {
		if(this.qtde != null && this.preco != null){
			valorBruto = new Money((this.qtde * this.preco));
		}

		return valorBruto;
	}
}
