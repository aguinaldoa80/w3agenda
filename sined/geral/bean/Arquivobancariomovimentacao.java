package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_arquivobancariomovimentacao", sequenceName="sq_arquivobancariomovimentacao")
public class Arquivobancariomovimentacao {
	
	protected Integer cdarquivobancariomovimentacao;
	protected Arquivobancario arquivobancario;
	protected Movimentacao movimentacao;
	
	public Arquivobancariomovimentacao(){
	}
	
	public Arquivobancariomovimentacao(Arquivobancario arquivobancario, Movimentacao movimentacao){
		this.arquivobancario = arquivobancario;
		this.movimentacao = movimentacao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivobancariomovimentacao")
	public Integer getCdarquivobancariomovimentacao() {
		return cdarquivobancariomovimentacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivobancario")
	public Arquivobancario getArquivobancario() {
		return arquivobancario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacao")
	public Movimentacao getMovimentacao() {
		return movimentacao;
	}
	
	public void setCdarquivobancariomovimentacao(Integer cdarquivobancariomovimentacao) {
		this.cdarquivobancariomovimentacao = cdarquivobancariomovimentacao;
	}
	public void setArquivobancario(Arquivobancario arquivobancario) {
		this.arquivobancario = arquivobancario;
	}
	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}
}