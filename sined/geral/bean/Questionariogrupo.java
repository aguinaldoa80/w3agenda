package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_questionariogrupo", sequenceName = "sq_questionariogrupo")
@DisplayName("Questionário Grupo")
public class Questionariogrupo {
	
	protected Integer cdquestionariogrupo;
	protected Questionario questionario;
	protected Grupoquestionario grupoquestionario;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_questionariogrupo")
	public Integer getCdquestionariogrupo() {
		return cdquestionariogrupo;
	}
	
	@JoinColumn(name="cdquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	public Questionario getQuestionario() {
		return questionario;
	}
	
	@JoinColumn(name="cdgrupoquestionario")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Grupo Questionário")
	public Grupoquestionario getGrupoquestionario() {
		return grupoquestionario;
	}
	
	public void setCdquestionariogrupo(Integer cdquestionariogrupo) {
		this.cdquestionariogrupo = cdquestionariogrupo;
	}
	
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}
	
	public void setGrupoquestionario(Grupoquestionario grupoquestionario) {
		this.grupoquestionario = grupoquestionario;
	}

}
