package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_clienteprocesso",sequenceName="sq_clienteprocesso")
public class Clienteprocesso{
	
	protected Integer cdclienteprocesso;
	protected Cliente cliente;
	protected String categoria;
	protected String prontuario;
	protected String registro;
	protected Timestamp dtvencimento;
	
	@Id
	@GeneratedValue(generator="sq_clienteprocesso",strategy=GenerationType.AUTO)
	public Integer getCdclienteprocesso() {
		return cdclienteprocesso;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Categoria")
	@MaxLength(3)
	@Required
	public String getCategoria() {
		return categoria;
	}
	
	@DisplayName("Prontuário")
	@MaxLength(20)
	@Required
	public String getProntuario() {
		return prontuario;
	}
	
	@DisplayName("Registro")
	@MaxLength(20)
	@Required
	public String getRegistro() {
		return registro;
	}
	
	@DisplayName("Vencimento")
	@Required
	public Timestamp getDtvencimento() {
		return dtvencimento;
	}
	
	public void setCdclienteprocesso(Integer cdclienteprocesso) {
		this.cdclienteprocesso = cdclienteprocesso;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public void setDtvencimento(Timestamp dtvencimento) {
		this.dtvencimento = dtvencimento;
	}	
	
}
