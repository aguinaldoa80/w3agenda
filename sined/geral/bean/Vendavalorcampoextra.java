package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_vendavalorcampoextra", sequenceName = "sq_vendavalorcampoextra")
public class Vendavalorcampoextra {

	protected Integer cdvendavalorcampoextra;
	protected Venda venda;
	protected Campoextrapedidovendatipo campoextrapedidovendatipo;
	protected String valor;

	@Id
	@GeneratedValue(generator = "sq_vendavalorcampoextra", strategy = GenerationType.AUTO)
	public Integer getCdvendavalorcampoextra() {
		return cdvendavalorcampoextra;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdvenda")
	public Venda getVenda() {
		return venda;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcampoextrapedidovendatipo")
	public Campoextrapedidovendatipo getCampoextrapedidovendatipo() {
		return campoextrapedidovendatipo;
	}

	@MaxLength(500)
	public String getValor() {
		return valor;
	}

	public void setCdvendavalorcampoextra(
			Integer cdvendavalorcampoextra) {
		this.cdvendavalorcampoextra = cdvendavalorcampoextra;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setCampoextrapedidovendatipo(Campoextrapedidovendatipo campoextrapedidovendatipo) {
		this.campoextrapedidovendatipo = campoextrapedidovendatipo;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
