package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.PagamentoTEFSituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_nota", sequenceName = "sq_nota")
public class Nota implements Log {

	protected Integer cdNota;
	protected NotaTipo notaTipo;
	protected String numero;
	protected NotaStatus notaStatus = NotaStatus.EMITIDA;
	protected Date dtEmissao = SinedDateUtils.currentDate();
	protected Projeto projeto;
	protected Cliente cliente;
	protected String nomefantasiaCliente;
	protected String telefoneCliente;
	protected Boolean enderecoavulso;
	protected Endereco enderecoCliente;
	protected Endereco endereconota;
	protected String dadosAdicionais;
	protected Empresa empresa;
	protected Boolean notaimportadaxml;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<NotaHistorico> listaNotaHistorico;
	protected List<NotaDocumento> listaNotaDocumento;
	protected List<NotaContrato> listaNotaContrato;
	protected List<Notaromaneio> listaNotaromaneio;
	protected Boolean impostocumulativoiss;
	protected Boolean impostocumulativoinss;
	protected Boolean impostocumulativoir;
	protected Boolean impostocumulativoicms;
	protected Boolean impostocumulativopis;
	protected Boolean impostocumulativocofins;
	protected Boolean impostocumulativocsll;
	protected Grupotributacao grupotributacao;
	protected Boolean cancelarvenda;
	protected Double numeronota;
	protected Conta contaboleto;
	protected Contacarteira contacarteira;
	protected Money diferencaduplicata;
	protected Prazopagamento prazopagamentofatura;
	protected String numerofatura;
	protected Money valororiginalfatura;
	protected Money valordescontofatura;
	protected Money valorliquidofatura;
	protected Documentotipo documentotipo;
	protected Boolean retornocanhoto;
	protected Integer serienfe;
	protected Integer serieNfce;
	private Colaborador colaborador;
	private Money taxaVenda;

	protected List<Arquivonfnota> listaArquivonfnota;
	protected List<NotaVenda> listaNotavenda;
	protected List<Requisicaonota> listaRequisicaonota;
	
	//Transientes
	protected br.com.linkcom.sined.geral.bean.Telefone telefoneClienteTransiente;
	protected String codProducoesDiaria;
	protected Money valorBruto;
	protected String pago;
	protected String numeroNfse;
	protected Long numeroNfseOrder;
	protected String whereIn;
	protected String whereInRequisicao;
	protected Boolean acaoestornar;
	protected String observacaoCancelamento;
	protected Money valorLocacao;
	protected Boolean diferencaLancamentoConfirmado;
	protected Boolean existeImpostoCumulativo;
	protected Money valorNotaTransient;
	protected String msgerrocorrecao;
	protected Boolean existeErroRejeicao;
	protected Conta conta;
	protected Money valororiginalfaturaTransient;
	protected Money valorliquidofaturaTransient;
	protected Boolean denegada;
	protected String nomeCliente;
	protected String descriptionCliente;
	protected Date dataSaida;
	protected PagamentoTEFSituacao pagamentoTEFSituacao;
	
	
	public Nota() {
		this.impostocumulativoiss = false;
		this.impostocumulativoinss = false;
		this.impostocumulativoir = false;
		this.impostocumulativoicms = false;
		this.impostocumulativopis = false;
		this.impostocumulativocofins = false;
		this.impostocumulativocsll = false;
	}

	public Nota(Integer cdNota){
		this.cdNota = cdNota;
	}
	
	public Nota(Nota nota) {
		this.cdNota = nota.cdNota;
		this.notaTipo = nota.notaTipo;
		this.numero = nota.numero;
		this.notaStatus = nota.notaStatus;
		this.dtEmissao = nota.dtEmissao;
		this.projeto = nota.projeto;
		this.cliente = nota.cliente;
		this.telefoneCliente = nota.telefoneCliente;
		this.enderecoCliente = nota.enderecoCliente;
		this.cdusuarioaltera = nota.cdusuarioaltera;
		this.dtaltera = nota.dtaltera;
		this.telefoneClienteTransiente = nota.telefoneClienteTransiente;
		this.dadosAdicionais = nota.dadosAdicionais;
	}
	
	@Id
	@DisplayName("Nota")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_nota")
	public Integer getCdNota() {
		return cdNota;
	}

	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNotaTipo")
	public NotaTipo getNotaTipo() {
		return notaTipo;
	}
	
	@MaxLength(30)
	@DisplayName("Nota fiscal n�")
	public String getNumero() {
		return numero;
	}

	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdNotaStatus")
	public NotaStatus getNotaStatus() {
		return notaStatus;
	}

	@Required
	@DisplayName("Emiss�o")
	public Date getDtEmissao() {
		return dtEmissao;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdProjeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Required
	@DisplayName("Cliente")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdCliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Nome Fantasia")	
	public String getNomefantasiaCliente() {
		return nomefantasiaCliente;
	}

	@DisplayName("Endere�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdEndereco")
	public Endereco getEnderecoCliente() {
		return enderecoCliente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinColumn(name="cdendereconota")
	@DisplayName("Endere�o")
	public Endereco getEndereconota() {
		return endereconota;
	}
	
	@DisplayName("Fone/Fax")
	@Column(name = "telefone")
	@MaxLength(20)
	public String getTelefoneCliente() {
		return telefoneCliente;
	}
	
	
	@MaxLength(500)
	@DisplayName("Dados adicionais")
	public String getDadosAdicionais() {
		return dadosAdicionais;
	}
	
	@Transient
	@DisplayName("Fone/Fax")
	public br.com.linkcom.sined.geral.bean.Telefone getTelefoneClienteTransiente() {
		return telefoneClienteTransiente;
	}
	
	@Transient
	public String getCodProducoesDiaria() {
		return codProducoesDiaria;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="nota")
	public List<NotaHistorico> getListaNotaHistorico() {
		return listaNotaHistorico;
	}
	
	@OneToMany(mappedBy="nota")
	public List<NotaDocumento> getListaNotaDocumento() {
		return listaNotaDocumento;
	}
	
	@OneToMany(mappedBy="nota")
	public List<NotaContrato> getListaNotaContrato() {
		return listaNotaContrato;
	}
	
	@OneToMany(mappedBy="nota")
	public List<Arquivonfnota> getListaArquivonfnota() {
		return listaArquivonfnota;
	}
	
	public Boolean getCancelarvenda() {
		return cancelarvenda;
	}
	
	public void setCancelarvenda(Boolean cancelarvenda) {
		this.cancelarvenda = cancelarvenda;
	}
	
	public void setListaArquivonfnota(List<Arquivonfnota> listaArquivonfnota) {
		this.listaArquivonfnota = listaArquivonfnota;
	}
	
	public Boolean getImpostocumulativoinss() {
		return impostocumulativoinss;
	}
	
	public Boolean getImpostocumulativoir() {
		return impostocumulativoir;
	}
	
	public Boolean getImpostocumulativoiss() {
		return impostocumulativoiss;
	}
	
	public Boolean getImpostocumulativoicms() {
		return impostocumulativoicms;
	}
	
	public Boolean getImpostocumulativopis() {
		return impostocumulativopis;
	}
	
	public Boolean getImpostocumulativocofins() {
		return impostocumulativocofins;
	}
	
	public Boolean getImpostocumulativocsll() {
		return impostocumulativocsll;
	}
	
	@DisplayName("Grupo de tributa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@OneToMany(mappedBy="nota")
	public List<Notaromaneio> getListaNotaromaneio() {
		return listaNotaromaneio;
	}
	
	public Boolean getNotaimportadaxml() {
		return notaimportadaxml;
	}
	
	public void setNotaimportadaxml(Boolean notaimportadaxml) {
		this.notaimportadaxml = notaimportadaxml;
	}
	
	public void setListaNotaromaneio(List<Notaromaneio> listaNotaromaneio) {
		this.listaNotaromaneio = listaNotaromaneio;
	}
	@Transient
	public Boolean getExisteErroRejeicao() {
		return existeErroRejeicao;
	}
	
	// ... Setters ... //
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setImpostocumulativoicms(Boolean impostocumulativoicms) {
		this.impostocumulativoicms = impostocumulativoicms;
	}

	public void setImpostocumulativopis(Boolean impostocumulativopis) {
		this.impostocumulativopis = impostocumulativopis;
	}

	public void setImpostocumulativocofins(Boolean impostocumulativocofins) {
		this.impostocumulativocofins = impostocumulativocofins;
	}

	public void setImpostocumulativocsll(Boolean impostocumulativocsll) {
		this.impostocumulativocsll = impostocumulativocsll;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public void setImpostocumulativoinss(Boolean impostocumulativoinss) {
		this.impostocumulativoinss = impostocumulativoinss;
	}
	
	public void setImpostocumulativoir(Boolean impostocumulativoir) {
		this.impostocumulativoir = impostocumulativoir;
	}
	
	public void setImpostocumulativoiss(Boolean impostocumulativoiss) {
		this.impostocumulativoiss = impostocumulativoiss;
	}
	
	public void setDadosAdicionais(String dadosAdicionais) {
		this.dadosAdicionais = StringUtils.trimToNull(dadosAdicionais);
	}
	
	public void setCdNota(Integer cdNota) {
		this.cdNota = cdNota;
	}

	public void setNumero(String numero) {
		this.numero = StringUtils.trimToNull(numero);
	}
	
	public void setNotaTipo(NotaTipo notaTipo) {
		this.notaTipo = notaTipo;
	}

	public void setNotaStatus(NotaStatus notaStatus) {
		this.notaStatus = notaStatus;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setNomefantasiaCliente(String nomefantasiaCliente) {
		this.nomefantasiaCliente = nomefantasiaCliente;
	}

	public void setEnderecoCliente(Endereco enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}
	
	public void setEndereconota(Endereco endereconota) {
		this.endereconota = endereconota;
	}

	public void setTelefoneCliente(String telefoneCliente) {
		this.telefoneCliente = telefoneCliente;
	}

	public void setTelefoneClienteTransiente(br.com.linkcom.sined.geral.bean.Telefone telefoneClienteTransiente) {
		this.telefoneClienteTransiente = telefoneClienteTransiente;
	}

	public void setCodProducoesDiaria(String codProducoesDiaria) {
		this.codProducoesDiaria = codProducoesDiaria;
	}
	
	public void setListaNotaHistorico(List<NotaHistorico> listaNotaHistorico) {
		this.listaNotaHistorico = listaNotaHistorico;
	}
	
	public void setListaNotaDocumento(List<NotaDocumento> listaNotaDocumento) {
		this.listaNotaDocumento = listaNotaDocumento;
	}
	
	public void setListaNotaContrato(List<NotaContrato> listaNotaContrato) {
		this.listaNotaContrato = listaNotaContrato;
	}
	
	public void setExisteErroRejeicao(Boolean existeErroRejeicao) {
		this.existeErroRejeicao = existeErroRejeicao;
	}

	//Log:
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	//End Log;
	
	
	//Abstratas:
	/**
	 * Retorna o valor total da nota.
	 *  
	 * @see br.com.linkcom.sined.geral.bean.NotaTipo
	 * @return
	 * @author Hugo Ferreira
	 */
	@Transient
	@DisplayName("Valor da nota")
	public Money getValorNota() {
		return null;
	}
	
	//... Outras ...
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdNota == null) ? 0 : cdNota.hashCode());
		result = prime * result
		+ ((notaTipo == null) ? 0 : notaTipo.hashCode());
		return result;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"nota.dtEmissao, nota.numero"})
	public String getDescription(){
		return SinedDateUtils.toString(this.dtEmissao) + 
			(StringUtils.isNotBlank(this.numero) ? " - " + this.numero : "");
	}
	
	@Transient
	public String getDescriptionCliente(){
		String nomeCliente = " ";
		if(this.cliente !=null){
			nomeCliente = " " +this.cliente.getNome();
		}
		return  
			(StringUtils.isNotBlank(this.numero) ? "" + this.numero : "")
			+(StringUtils.isNotBlank(nomeCliente) ? " - " + nomeCliente: "");
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Nota))
			return false;
		final Nota other = (Nota) obj;
		if (cdNota == null) {
			if (other.cdNota != null)
				return false;
		} else if (!cdNota.equals(other.cdNota))
			return false;
		if (notaTipo == null) {
			if (other.notaTipo != null)
				return false;
		} else if (!notaTipo.equals(other.notaTipo))
			return false;
		return true;
	}
	
	/**
	 * Copia apenas os itens para c�lculo de valor de outra
	 * nota fiscal.
	 * <p> Deveria ser um m�todo abstrato, mas o CrudController n�o
	 * permite ser estendido recebendo uma classe abstrata como par�metro.
	 * 
	 * @param nota
	 * @author Hugo Ferreira
	 */
	public void copiarItensDeValor(Nota nota) {
		if (true) {
			throw new SinedException("O m�todo 'copiarItensDeValor' n�o pode ser chamado de a partir de uma inst�ncia de Nota. " +
					"Sobrescreva-o na classe herdeira e certifique-se de estar invocando-o a partir de um objeto filho de Nota.");
		}
	}
	
	@Transient
	public Boolean getIncideiss() {
		return false;
	}
	
	@Transient
	public Money getValorBruto() {
		return valorBruto;
	}
	
	public void setValorBruto(Money valorBruto) {
		this.valorBruto = valorBruto;
	}
	
	@Transient
	public Double getIss() {
		return null;
	}
	
	@Transient
	public Money getValoriss() {
		return null;
	}
	
	@Transient
	public String getPago() {
		if (listaNotaDocumento == null || listaNotaDocumento.size() == 0)
			return "N�o";
		else {		
			for (NotaDocumento documento: listaNotaDocumento) {
				if (documento.getDocumento().getDocumentoacao().equals(Documentoacao.BAIXADA)) 
					continue;
				else
					return "N�o";			
			}
			return "Sim";
		}
	}
	
	@OneToMany(mappedBy="nota")
	public List<NotaVenda> getListaNotavenda() {
		return listaNotavenda;
	}
	@OneToMany(mappedBy="nota")
	public List<Requisicaonota> getListaRequisicaonota() {
		return listaRequisicaonota;
	}
	
	public void setListaNotavenda(List<NotaVenda> listaNotavenda) {
		this.listaNotavenda = listaNotavenda;
	}
	
	public void setListaRequisicaonota(List<Requisicaonota> listaRequisicaonota) {
		this.listaRequisicaonota = listaRequisicaonota;
	}
	
	@Transient
	public String getNumeroNfse() {
		return numeroNfse;
	}
	public void setNumeroNfse(String numeroNfse) {
		this.numeroNfse = numeroNfse;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
	@Transient
	public String getWhereInRequisicao() {
		return whereInRequisicao;
	}
	
	public void setWhereInRequisicao(String whereInRequisicao) {
		this.whereInRequisicao = whereInRequisicao;
	}
	
	@Transient
	public Boolean getAcaoestornar() {
		return acaoestornar;
	}
	public void setAcaoestornar(Boolean acaoestornar) {
		this.acaoestornar = acaoestornar;
	}

	@DisplayName("Endere�o avulso")
	public Boolean getEnderecoavulso() {
		return enderecoavulso;
	}
	public void setEnderecoavulso(Boolean enderecoavulso) {
		this.enderecoavulso = enderecoavulso;
	}

	@Transient
	public Long getNumeroNfseOrder() {
		return numeroNfseOrder;
	}
	public void setNumeroNfseOrder(Long numeroNfseOrder) {
		this.numeroNfseOrder = numeroNfseOrder;
	}

	@Transient
	public String getObservacaoCancelamento() {
		return observacaoCancelamento;
	}

	public void setObservacaoCancelamento(String observacaoCancelamento) {
		this.observacaoCancelamento = observacaoCancelamento;
	}

	@Transient
	public Money getValorLocacao() {
		return valorLocacao;
	}

	public void setValorLocacao(Money valorLocacao) {
		this.valorLocacao = valorLocacao;
	}
	
	public Double getNumeronota() {
		return numeronota;
	}
	
	public void setNumeronota(Double numeronota) {
		this.numeronota = numeronota;
	}
	
	@JoinColumn(name="cdconta")
	@DisplayName("Conta para Boleto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getContaboleto() {
		return contaboleto;
	}
	
	@JoinColumn(name="cdcontacarteira")
	@DisplayName("Carteira")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	
	@DisplayName("Diferen�a das parcelas")
	public Money getDiferencaduplicata() {
		return diferencaduplicata;
	}
	
	@DisplayName("Prazo de pagamento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdprazopagamentofatura")
	public Prazopagamento getPrazopagamentofatura() {
		return prazopagamentofatura;
	}
	
	@DisplayName("N�mero")
	public String getNumerofatura() {
		return numerofatura;
	}

	@DisplayName("Valor Original")
	public Money getValororiginalfatura() {
		return valororiginalfatura;
	}

	@DisplayName("Desconto")
	public Money getValordescontofatura() {
		return valordescontofatura;
	}

	@DisplayName("Valor L�quido")
	public Money getValorliquidofatura() {
		return valorliquidofatura;
	}
	
	@DisplayName("Meio de Pagamento")
	@JoinColumn(name="cdformapagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	
	public Money getTaxaVenda() {
		return taxaVenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setTaxaVenda(Money taxaVenda) {
		this.taxaVenda = taxaVenda;
	}

	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}

	public void setDiferencaduplicata(Money diferencaduplicata) {
		this.diferencaduplicata = diferencaduplicata;
	}

	public void setPrazopagamentofatura(Prazopagamento prazopagamentofatura) {
		this.prazopagamentofatura = prazopagamentofatura;
	}

	public void setNumerofatura(String numerofatura) {
		this.numerofatura = numerofatura;
	}

	public void setValororiginalfatura(Money valororiginalfatura) {
		this.valororiginalfatura = valororiginalfatura;
	}

	public void setValordescontofatura(Money valordescontofatura) {
		this.valordescontofatura = valordescontofatura;
	}

	public void setValorliquidofatura(Money valorliquidofatura) {
		this.valorliquidofatura = valorliquidofatura;
	}
	
	public void setContaboleto(Conta contaboleto) {
		this.contaboleto = contaboleto;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}

	@Transient
	@DisplayName("Nota fiscal n�")
	public String getNumeroFormatado(){
		return SinedUtil.getNumeroNotaFormatado(getNumero());
	}

	@Transient
	public Boolean getDiferencaLancamentoConfirmado() {
		return diferencaLancamentoConfirmado;
	}
	public void setDiferencaLancamentoConfirmado(Boolean diferencaLancamentoConfirmado) {
		this.diferencaLancamentoConfirmado = diferencaLancamentoConfirmado;
	}
	
	@Transient
	public Boolean getExisteImpostoCumulativo() {
		return existeImpostoCumulativo;
	}
	public void setExisteImpostoCumulativo(Boolean existeImpostoCumulativo) {
		this.existeImpostoCumulativo = existeImpostoCumulativo;
	}

	@Transient
	public Money getValorNotaTransient() {
		return valorNotaTransient;
	}

	public void setValorNotaTransient(Money valorNotaTransient) {
		this.valorNotaTransient = valorNotaTransient;
	}

	@Transient
	public String getMsgerrocorrecao() {
		return msgerrocorrecao;
	}
	
	public void setMsgerrocorrecao(String msgerrocorrecao) {
		this.msgerrocorrecao = msgerrocorrecao;
	}
	
	@Transient
	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	@Transient
	public Money getValororiginalfaturaTransient() {
		return valororiginalfaturaTransient;
	}
	@Transient
	public Money getValorliquidofaturaTransient() {
		return valorliquidofaturaTransient;
	}

	public void setValororiginalfaturaTransient(Money valororiginalfaturaTransient) {
		this.valororiginalfaturaTransient = valororiginalfaturaTransient;
	}

	public void setValorliquidofaturaTransient(Money valorliquidofaturaTransient) {
		this.valorliquidofaturaTransient = valorliquidofaturaTransient;
	}
	
	@Transient
	public String getNumeroOrCdNota(){
		return StringUtils.isNotBlank(this.numero) ? this.numero :
			this.cdNota != null? this.cdNota.toString():
				"";
	}

	@Transient
	public Boolean getDenegada() {
		return denegada;
	}

	public void setDenegada(Boolean denegada) {
		this.denegada = denegada;
	}
	
	@Transient
	public String getNomeCliente() {
		if(this.getCliente()!=null && this.getCliente().getNome()!=null){
			nomeCliente = this.getCliente().getNome();
		}
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	
	@DisplayName("Canhoto devolvido?")
	public Boolean getRetornocanhoto() {
		if(retornocanhoto == null) return false;
		return retornocanhoto;
	}
	
	public void setRetornocanhoto(Boolean retornocanhoto) {
		this.retornocanhoto = retornocanhoto;
	}
	
	@DisplayName("S�rie da Nf-e")
	public Integer getSerienfe() {
		return serienfe;
	}
	
	public void setSerienfe(Integer serienfe) {
		this.serienfe = serienfe;
	}
	
	@Transient
	public Date getDataSaida() {
		return dataSaida;
	}
	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public void addTaxaVenda(Money taxaVenda) {
		this.taxaVenda = this.taxaVenda.add(taxaVenda);
	}
		
	@Transient
	public PagamentoTEFSituacao getPagamentoTEFSituacao() {
		return pagamentoTEFSituacao;
	}
	public void setPagamentoTEFSituacao(
			PagamentoTEFSituacao pagamentoTEFSituacao) {
		this.pagamentoTEFSituacao = pagamentoTEFSituacao;
	}
}
