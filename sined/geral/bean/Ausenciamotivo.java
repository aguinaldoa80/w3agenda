package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_ausenciamotivo", sequenceName = "sq_ausenciamotivo")
@DisplayName("Motivo da aus�ncia")
public class Ausenciamotivo implements Log{
	
	protected Integer cdausenciamotivo;
	protected String motivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ausenciamotivo")
	public Integer getCdausenciamotivo() {
		return cdausenciamotivo;
	}
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getMotivo() {
		return motivo;
	}
	public void setCdausenciamotivo(Integer cdausenciamotivo) {
		this.cdausenciamotivo = cdausenciamotivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
