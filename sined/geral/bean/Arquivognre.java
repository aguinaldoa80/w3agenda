package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivognre", sequenceName = "sq_arquivognre")
public class Arquivognre implements Log {

	protected Integer cdarquivognre;
	protected Arquivo arquivoxml;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Arquivognreitem> listaArquivognreitem = new ListSet<Arquivognreitem>(Arquivognreitem.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivognre")
	public Integer getCdarquivognre() {
		return cdarquivognre;
	}
	
	@DisplayName("XML")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@OneToMany(mappedBy="arquivognre", fetch=FetchType.LAZY)
	public List<Arquivognreitem> getListaArquivognreitem() {
		return listaArquivognreitem;
	}
	
	public void setListaArquivognreitem(
			List<Arquivognreitem> listaArquivognreitem) {
		this.listaArquivognreitem = listaArquivognreitem;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdarquivognre(Integer cdarquivognre) {
		this.cdarquivognre = cdarquivognre;
	}

	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}
	
	

}
