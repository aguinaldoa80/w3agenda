package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_ecom_endereco", sequenceName="sq_ecom_endereco")
public class Ecom_Endereco {

	private Integer cdEcom_Endereco;
	private Integer id;
	private String endereco;
	private String bairro;
	private String numero;
	private String cep;
	private String complemento;
	private String esta_cod;
	private String cidade;
	private String referencia;
	private Ecom_Cliente ecomCliente;
	
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_endereco", strategy=GenerationType.AUTO)
	public Integer getCdEcom_Endereco() {
		return cdEcom_Endereco;
	}
	public void setCdEcom_Endereco(Integer cdEcom_Endereco) {
		this.cdEcom_Endereco = cdEcom_Endereco;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getEsta_cod() {
		return esta_cod;
	}
	public void setEsta_cod(String esta_cod) {
		this.esta_cod = esta_cod;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_cliente")
	public Ecom_Cliente getEcomCliente() {
		return ecomCliente;
	}
	public void setEcomCliente(Ecom_Cliente ecomCliente) {
		this.ecomCliente = ecomCliente;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
}
