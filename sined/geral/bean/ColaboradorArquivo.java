package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

import java.sql.Date;

@Entity
@SequenceGenerator(name = "sq_colaboradorarquivo", sequenceName = "sq_colaboradorarquivo")
public class ColaboradorArquivo{
	
	private Integer cdColaboradorArquivo;
	private String descricao;
	private Arquivo arquivo;
	private Date data;
	private Colaborador colaborador;
	
	public ColaboradorArquivo(){}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorarquivo")
	public Integer getCdColaboradorArquivo() {
		return cdColaboradorArquivo;
	}

	public void setCdColaboradorArquivo(Integer cdColaboradorArquivo) {
		this.cdColaboradorArquivo = cdColaboradorArquivo;
	}

	@DisplayName("Descri��o")
	@MaxLength(200)
	@Required
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	@Required
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
}
