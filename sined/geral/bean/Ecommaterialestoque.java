package br.com.linkcom.sined.geral.bean;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_ecommaterialestoque", sequenceName = "sq_ecommaterialestoque")
public class Ecommaterialestoque implements Serializable {

	private static final long serialVersionUID = -2066580166403754758L;
	
	protected Integer cdecommaterialestoque;
	protected Material material;
	protected String identificador;
	protected Double qtde;
	protected Configuracaoecom configuracaoecom;
	
	//FLAGS
	protected Boolean sincronizado;
	protected Timestamp data;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ecommaterialestoque")
	public Integer getCdecommaterialestoque() {
		return cdecommaterialestoque;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public String getIdentificador() {
		return identificador;
	}

	public Double getQtde() {
		return qtde;
	}

	public Boolean getSincronizado() {
		return sincronizado;
	}

	public Timestamp getData() {
		return data;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaoecom")
	public Configuracaoecom getConfiguracaoecom() {
		return configuracaoecom;
	}
	
	public void setConfiguracaoecom(Configuracaoecom configuracaoecom) {
		this.configuracaoecom = configuracaoecom;
	}

	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public void setCdecommaterialestoque(Integer cdecommaterialestoque) {
		this.cdecommaterialestoque = cdecommaterialestoque;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
}
