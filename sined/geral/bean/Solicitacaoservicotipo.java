package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_solicitacaoservicotipo",sequenceName="sq_solicitacaoservicotipo")
@DisplayName("Tipo de solicita��o")
public class Solicitacaoservicotipo implements Log{
	
	//Tabela solicitacaoservicotipo
	protected Integer cdsolicitacaoservicotipo;
	protected String nome;
	protected String descricao;
	protected Boolean ativo=true;
	
	//Tabela solicitacaoservicotipoitem
	protected List<Solicitacaoservicotipoitem> listasolicitacaoitem;
	
	//log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("N�mero")
	@Id
	@GeneratedValue(generator="sq_solicitacaoservicotipo",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservicotipo() {
		return cdsolicitacaoservicotipo;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(500)
	public String getDescricao() {
		return descricao;
	}	
	@Required
	public Boolean getAtivo() {
		return ativo;
	}

	@DisplayName("Itens")
	@OneToMany(mappedBy="solicitacaoservicotipo")
	public List<Solicitacaoservicotipoitem> getListasolicitacaoitem(){
		return listasolicitacaoitem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdsolicitacaoservicotipo(Integer cdsolicitacaoservicotipo) {
		this.cdsolicitacaoservicotipo = cdsolicitacaoservicotipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setListasolicitacaoitem(List<Solicitacaoservicotipoitem> listasolicitacaoitem) {
		this.listasolicitacaoitem = listasolicitacaoitem;
	}
	

}
