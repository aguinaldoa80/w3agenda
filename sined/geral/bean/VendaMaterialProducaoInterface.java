package br.com.linkcom.sined.geral.bean;

import java.util.List;

import br.com.linkcom.neo.types.Money;


public interface VendaMaterialProducaoInterface {

	public String getCodigo();
	public Double getAlturas();
	public Double getLarguras();
	public Double getComprimentos();
	public Double getQuantidades();
	public Cliente getCliente();
	public Pedidovendatipo getPedidovendatipo();
	public Empresa getEmpresa();
	public Prazopagamento getPrazopagamento();
	public Materialtabelapreco getMaterialtabelapreco();
	public Double getValor();
	public Double getValorminimo();
	public Double getValormaximo();
	public Money getValorfrete();
	
	public void setMaterial(Material material);
	public List<VendaMaterialProducaoItemInterface> getItens();
	
	/*Material material = materialService.loadMaterialComMaterialproducao(new Material(Integer.parseInt(venda.getCodigo())));
	venda.setMaterial(material);
	material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), false));
	
	if(material.getProducao()){
		Vendamaterialmestre vmm = new Vendamaterialmestre();
		vmm.setAltura(venda.getAlturas());
		vmm.setComprimento(venda.getComprimentos());
		vmm.setLargura(venda.getLarguras());
//		vmm.setPeso(venda);
		vmm.setVenda(venda);
		vmm.setMaterial(material);
		venda.getListaVendamaterialmestre().add(vmm);
	}
	
	Double valorcusto = material.getValorcusto();
	try{
		material.setProduto_altura(venda.getAlturas());
		material.setProduto_largura(venda.getLarguras());
		material.setQuantidade(venda.getQuantidades());
		pedidovendaService.preecheValorVendaComTabelaPrecoItensProducao(material, venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), venda.getPrazopagamento());
		valorcusto = materialproducaoService.getValorvendaproducao(material);			
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	boolean recapagem = SinedUtil.isRecapagem();
	
	Materialproducao materialproducaoMestre = materialproducaoService.createItemProducaoByMaterialmestre(material.getListaProducao(), material);
	material.setListaProducao(materialproducaoService.ordernarListaProducao(material.getListaProducao(), true));
	
	Material materialbanda = null;
	
	if(recapagem){
		if(venda.getValor() != null){
			material.setValorvenda(venda.getValor());
			material.setValorvendasemdesconto(material.getValorvenda());
			material.setValorvendaminimo(venda.getValorminimo());
			material.setValorvendamaximo(venda.getValormaximo());
			materialproducaoMestre.getMaterial().setValorvenda(venda.getValor());
			materialproducaoMestre.getMaterial().setValorvendaminimo(venda.getValorminimo());
			materialproducaoMestre.getMaterial().setValorvendamaximo(venda.getValormaximo());
		}else {
			vendaService.preencheValorVendaComTabelaPreco(material, venda.getMaterialtabelapreco(), venda.getPedidovendatipo(), venda.getCliente(), venda.getEmpresa(), material.getUnidademedida(), venda.getPrazopagamento());
			materialproducaoMestre.getMaterial().setValorvenda(material.getValorvenda());
			materialproducaoMestre.getMaterial().setValorvendasemdesconto(material.getValorvendasemdesconto());
		}
		materialproducaoMestre.getMaterial().setValorcusto(valorcusto);
		
		for (Materialproducao materialproducao : material.getListaProducao()){
			if(materialproducao != null && materialproducao.getExibirvenda() != null && materialproducao.getExibirvenda()){
				materialbanda = materialproducao.getMaterial();
			}
		}
	}else if(materialproducaoMestre != null){
		materialproducaoMestre.setVendaaltura(venda.getAlturas());
		materialproducaoMestre.setVendacomprimento(venda.getComprimentos());
		materialproducaoMestre.setVendalargura(venda.getLarguras());
	}*/
}
