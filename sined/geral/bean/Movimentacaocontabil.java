package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.MovimentacaocontabilTipoLancamentoEnum;
import br.com.linkcom.sined.geral.service.MovimentacaocontabilorigemService;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName ("Movimenta��o Contabil")
@SequenceGenerator(name="sq_movimentacaocontabil", sequenceName="sq_movimentacaocontabil" )
public class Movimentacaocontabil implements Log {
	
	protected Integer cdmovimentacaocontabil;
	protected Empresa empresa;
	protected Date dtlancamento;
	protected Operacaocontabil operacaocontabil;
	protected MovimentacaocontabilTipoEnum tipo;
	protected MovimentacaocontabilTipoLancamentoEnum tipoLancamento;
	protected String numero;
	protected Contagerencial debito;
	protected Contagerencial credito;
	protected Centrocusto centrocustodebito;
	protected Centrocusto centrocustocredito;
	protected Money valor;
	protected String historico;
	protected List<Movimentacaocontabildebitocredito> listaDebito;
	protected List<Movimentacaocontabildebitocredito> listaCredito;
	protected List<Movimentacaocontabilorigem> listaMovimentacaocontabilorigem;
	protected LoteContabil loteContabil;
	
	// LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	//Transient
	protected boolean salvarMovimentacaocontabilorigem = true;
	protected Date dtReferencia;
	
	public Movimentacaocontabil(){
	}
	
	public Movimentacaocontabil(Date dtlancamento, Empresa empresa, Operacaocontabil operacaocontabil, MovimentacaocontabilTipoEnum tipo, MovimentacaocontabilTipoLancamentoEnum tipoLancamento, String numero, Money valor, String historico){
		this.dtlancamento = dtlancamento;
		this.empresa = empresa;
		this.operacaocontabil = operacaocontabil;
		this.tipo = tipo;
		this.tipoLancamento = tipoLancamento;
		this.numero = numero;
		this.valor = valor;
		this.historico = historico;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_movimentacaocontabil")
	public Integer getCdmovimentacaocontabil() {
		return cdmovimentacaocontabil;
	}
	
	@Required
	@DisplayName("Data")
	public Date getDtlancamento() {
		return dtlancamento;
	}
	
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Opera��o cont�bil")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	@Required
	@DisplayName("Tipo")
	public MovimentacaocontabilTipoEnum getTipo() {
		return tipo;
	}
	
	@DisplayName("Tipo de lan�amento")
	public MovimentacaocontabilTipoLancamentoEnum getTipoLancamento() {
		return tipoLancamento;
	}
	
	@DisplayName("Documento")
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}

	@DisplayName("Conta D�bito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontadebito")
	public Contagerencial getDebito() {
		return debito;
	}
	
	@DisplayName("Conta Cr�dito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontacredito")
	public Contagerencial getCredito() {
		return credito;
	}
	
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	
	@DisplayName("Hist�rico Principal")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	
	@DisplayName("Centro de Custo D�bito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustodebito")
	public Centrocusto getCentrocustodebito() {
		return centrocustodebito;
	}
	
	@DisplayName("Centro de Custo Cr�dito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocustocredito")
	public Centrocusto getCentrocustocredito() {
		return centrocustocredito;
	}
	
	@OneToMany(mappedBy="movimentacaocontabildebito")
	public List<Movimentacaocontabildebitocredito> getListaDebito() {
		return listaDebito;
	}
	
	@OneToMany(mappedBy="movimentacaocontabilcredito")
	public List<Movimentacaocontabildebitocredito> getListaCredito() {
		return listaCredito;
	}
	
	@OneToMany(mappedBy="movimentacaocontabil")
	public List<Movimentacaocontabilorigem> getListaMovimentacaocontabilorigem() {
		return listaMovimentacaocontabilorigem;
	}
	
	@Transient
	@DisplayName("Data de refer�ncia")
	public Date getDtReferencia() {
		return dtReferencia;
	}

	public void setCdmovimentacaocontabil(Integer cdmovimentacaocontabil) {
		this.cdmovimentacaocontabil = cdmovimentacaocontabil;
	}
	
	public void setDtlancamento(Date dtlancamento) {
		this.dtlancamento = dtlancamento;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	public void setTipo(MovimentacaocontabilTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public void setTipoLancamento(MovimentacaocontabilTipoLancamentoEnum tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setDebito(Contagerencial debito) {
		this.debito = debito;
	}
	
	public void setCredito(Contagerencial credito) {
		this.credito = credito;
	}
	
	public void setCentrocustodebito(Centrocusto centrocustodebito) {
		this.centrocustodebito = centrocustodebito;
	}

	public void setCentrocustocredito(Centrocusto centrocustocredito) {
		this.centrocustocredito = centrocustocredito;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	public void setListaDebito(List<Movimentacaocontabildebitocredito> listaDebito) {
		this.listaDebito = listaDebito;
	}
	
	public void setListaCredito(List<Movimentacaocontabildebitocredito> listaCredito) {
		this.listaCredito = listaCredito;
	}
	
	public void setListaMovimentacaocontabilorigem(List<Movimentacaocontabilorigem> listaMovimentacaocontabilorigem) {
		this.listaMovimentacaocontabilorigem = listaMovimentacaocontabilorigem;
	}
	
	//LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void addOrigem(Nota nota){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(nota));
	}
	
	public void addOrigem(Provisao provisao){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(provisao));
	}
	
	public void addOrigem(Colaboradordespesa colaboradorDespesa){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(colaboradorDespesa));
	}
	
	public void addOrigem(Colaboradordespesaitem colaboradorDespesaItem, EventoPagamento eventoPagamento){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(colaboradorDespesaItem, eventoPagamento));
	}
	
	public void addOrigem(Entregadocumento entregadocumento){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(entregadocumento));
	}
	
	public void addOrigem(Documento documento){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(documento));
	}
	
	public void addOrigem(Movimentacao movimentacao){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(movimentacao));
	}
	
	public void addOrigem(Venda venda){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(venda));
	}
	
	public void addOrigem(Rateioitem rateioitem){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(rateioitem));
	}
	
	public void addOrigem(Rateioitem rateioitem, Movimentacaoestoque movimentacaoEstoque) {
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(rateioitem, movimentacaoEstoque));
		
	}
	
	public void addOrigem(Movimentacaoestoque movimentacaoEstoque){
		if (this.listaMovimentacaocontabilorigem==null){
			this.listaMovimentacaocontabilorigem = new ArrayList<Movimentacaocontabilorigem>();
		}
		this.listaMovimentacaocontabilorigem.add(new Movimentacaocontabilorigem(movimentacaoEstoque));
	}
	
	@DisplayName("Conta D�bito")
	@Transient
	public String getContasgerenciaisDebito(){
		if (debito!=null){
			return debito.getNome();
		}else if (listaDebito!=null && !listaDebito.isEmpty()){
			return CollectionsUtil.listAndConcatenate(listaDebito, "contagerencial.nome", ", ");
		}else {
			return null;
		}
	}
	
	@DisplayName("Conta de Cr�dito")
	@Transient
	public String getContasgerenciaisCredito(){
		if (credito!=null){
			return credito.getNome();
		}else if (listaCredito!=null && !listaCredito.isEmpty()){
			return CollectionsUtil.listAndConcatenate(listaCredito, "contagerencial.nome", ", ");
		}else {
			return null;
		}
	}
	
	@Transient
	public String getNumeroValorHistorico(){
		String retorno;
		
		if (getNumero()!=null && !getNumero().trim().equals("")){
			retorno = getNumero();
		}else {
			retorno = "-";			
		}
		
		retorno += "<br>";
		
		if (getValor()!=null){
			retorno += getValor();
		}else {
			retorno += "-";			
		}
		
		retorno += "<br>";
		
		if (getHistorico()!=null && !getHistorico().trim().equals("")){
			retorno += getHistorico();
		}else {
			retorno += "-";			
		}
		
		return retorno;
	}
	
	@Transient
	public String getDebitos(){
		String retorno = "";
		
		if (listaDebito!=null && !listaDebito.isEmpty()){
			for (Movimentacaocontabildebitocredito movimentacaocontabildebitocredito: listaDebito){
				retorno += movimentacaocontabildebitocredito.getDescricaoGerarLancamentosContabeis();
				retorno += "<br><br>";
			}
			retorno = retorno.substring(0, retorno.length()-"<br><br>".length());
		}
		
		return retorno;
	}
	
	@Transient
	public String getCreditos(){
		String retorno = "";
		
		if (listaCredito!=null && !listaCredito.isEmpty()){
			for (Movimentacaocontabildebitocredito movimentacaocontabildebitocredito: listaCredito){
				retorno += movimentacaocontabildebitocredito.getDescricaoGerarLancamentosContabeis();
				retorno += "<br><br>";
			}
			retorno = retorno.substring(0, retorno.length() - "<br><br>".length());
		}
		
		return retorno;
	}
	
	@Transient
	public boolean isSalvarMovimentacaocontabilorigem() {
		return salvarMovimentacaocontabilorigem;
	}
	
	public void setSalvarMovimentacaocontabilorigem(boolean salvarMovimentacaocontabilorigem) {
		this.salvarMovimentacaocontabilorigem = salvarMovimentacaocontabilorigem;
	}
	
	@Transient
	public String getLinksOrigem(){
		return MovimentacaocontabilorigemService.getInstance().getLinksOrigem(listaMovimentacaocontabilorigem, true);
	}
	
	@Transient
	@DisplayName("Hist�rico")
	public String getHistoricoListagem(){
		StringBuilder sb = new StringBuilder();
		if(StringUtils.isNotBlank(getHistorico())){
			sb.append(getHistorico());
		}
		if(Hibernate.isInitialized(getListaCredito()) && SinedUtil.isListNotEmpty(getListaCredito())){
			for(Movimentacaocontabildebitocredito item : getListaCredito()){
				if(StringUtils.isNotBlank(item.getHistorico()) && !sb.toString().contains(item.getHistorico())){
					if(sb.length() > 0) sb.append("<br>");
					sb.append(item.getHistorico());
				}
			}
		}
		if(Hibernate.isInitialized(getListaDebito()) && SinedUtil.isListNotEmpty(getListaDebito())){
			for(Movimentacaocontabildebitocredito item : getListaDebito()){
				if(StringUtils.isNotBlank(item.getHistorico()) && !sb.toString().contains(item.getHistorico())){
					if(sb.length() > 0) sb.append("<br>");
					sb.append(item.getHistorico());
				}
			}
		}
		return sb.toString();
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdLoteContabil")
	public LoteContabil getLoteContabil() {
		return loteContabil;
	}
	public void setLoteContabil(LoteContabil loteContabil) {
		this.loteContabil = loteContabil;
	}
	
	public void setDtReferencia(Date dtReferencia) {
		this.dtReferencia = dtReferencia;
	}
}