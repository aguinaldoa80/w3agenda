package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_comprovanteconfiguravel", sequenceName = "sq_comprovanteconfiguravel")
public class ComprovanteConfiguravel implements Log {

	public static enum TipoComprovante {VENDA, PEDIDO_VENDA, ORCAMENTO, COLETA};
	
	protected Integer cdcomprovanteconfiguravel;
	protected String descricao;
	protected String layout;
	protected Boolean venda;
	protected Boolean pedidoVenda;
	protected Boolean orcamento;
	protected Boolean coleta;
	protected Boolean ativo = Boolean.TRUE;
	protected Boolean txt;
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	//transient
	protected String tipo;
	
	public ComprovanteConfiguravel(){
	}
	
	public ComprovanteConfiguravel(Integer cdcomprovanteconfiguravel){
		this.cdcomprovanteconfiguravel = cdcomprovanteconfiguravel;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_comprovanteconfiguravel")
	public Integer getCdcomprovanteconfiguravel() {
		return cdcomprovanteconfiguravel;
	}
	
	@DisplayName("Layout do Comprovante")
	@Required
	public String getLayout() {
		return layout;
	}
	
	@DescriptionProperty
	@Required
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	
	@DisplayName("Venda")
	public Boolean getVenda() {
		return venda;
	}

	@DisplayName("Pedido de Venda")
	public Boolean getPedidoVenda() {
		return pedidoVenda;
	}

	@DisplayName("Or�amento")
	public Boolean getOrcamento() {
		return orcamento;
	}
	
	@DisplayName("Coleta")
	public Boolean getColeta() {
		return coleta;
	}

	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Gerar TXT")
	public Boolean getTxt() {
		return txt;
	}
	
	public void setTxt(Boolean txt) {
		this.txt = txt;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setVenda(Boolean venda) {
		this.venda = venda;
	}

	public void setPedidoVenda(Boolean pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}

	public void setOrcamento(Boolean orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setColeta(Boolean coleta) {
		this.coleta = coleta;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public void setCdcomprovanteconfiguravel(Integer cdcomprovanteconfiguravel) {
		this.cdcomprovanteconfiguravel = cdcomprovanteconfiguravel;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	//transient
	@Transient
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
