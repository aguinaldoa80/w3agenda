package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialtipo", sequenceName = "sq_materialtipo")
@DisplayName("Tipo de material")
public class Materialtipo implements Log {

	protected Integer cdmaterialtipo;
	protected String nome;
	protected Boolean ativo = Boolean.TRUE;
	protected Boolean veiculo;
	protected String codigoreceita;
	protected Double largurade;
	protected Double larguraate;
	protected Double alturade;
	protected Double alturaate;
	protected Double pesoespecifico;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	protected List<Material> listaMaterial = new ListSet<Material>(Material.class);
	protected List<Servicoservidortipo> listaServico;
	protected Arquivo arquivoLegenda;
	
	public Materialtipo(){
	}
	
	public Materialtipo(Integer cdmaterialtipo){
		this.cdmaterialtipo = cdmaterialtipo;
	}

	public Materialtipo(String nome){
		this.nome = nome;
	}
	
	public Materialtipo(Integer cdmaterialtipo, String nome){
		this.cdmaterialtipo = cdmaterialtipo;
		this.nome = nome;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtipo")
	public Integer getCdmaterialtipo() {
		return cdmaterialtipo;
	}
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	
	@Required
	@DisplayName("Ve�culo")
	public Boolean getVeiculo() {
		return veiculo;
	}
	@MaxLength(2)
	@DisplayName("C�digo receita")
	public String getCodigoreceita() {
		return codigoreceita;
	}
	@DisplayName("De largura (mm)")
	public Double getLargurade() {
		return largurade;
	}
	@DisplayName("At� largura (mm)")
	public Double getLarguraate() {
		return larguraate;
	}
	@DisplayName("De altura (mm)")
	public Double getAlturade() {
		return alturade;
	}
	@DisplayName("At� altura (mm)")
	public Double getAlturaate() {
		return alturaate;
	}
	@DisplayName("Peso espec�fico")
	public Double getPesoespecifico() {
		return pesoespecifico;
	}
	
	public void setPesoespecifico(Double pesoespecifico) {
		this.pesoespecifico = pesoespecifico;
	}
	public void setCodigoreceita(String codigoreceita) {
		this.codigoreceita = codigoreceita;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdmaterialtipo(Integer cdmaterialtipo) {
		this.cdmaterialtipo = cdmaterialtipo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setVeiculo(Boolean veiculo) {
		this.veiculo = veiculo;
	}
	public void setLargurade(Double largurade) {
		this.largurade = largurade;
	}
	public void setLarguraate(Double larguraate) {
		this.larguraate = larguraate;
	}
	public void setAlturade(Double alturade) {
		this.alturade = alturade;
	}
	public void setAlturaate(Double alturaate) {
		this.alturaate = alturaate;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Materialtipo) {
			Materialtipo materialtipo = (Materialtipo) obj;
			return materialtipo.getCdmaterialtipo().equals(this.getCdmaterialtipo());
		}
		return super.equals(obj);
	}
	@Override
	public int hashCode() {
		if (cdmaterialtipo != null) {
			return cdmaterialtipo.hashCode();
		}
		return super.hashCode();
	}
	@OneToMany(mappedBy="materialtipo")
	public List<Material> getListaMaterial() {
		return listaMaterial;
	}
	public void setListaMaterial(List<Material> listaMaterial) {
		this.listaMaterial = listaMaterial;
	}
	@OneToMany(mappedBy="materialtipo")
	public List<Servicoservidortipo> getListaServico() {
		return listaServico;
	}
	public void setListaServico(List<Servicoservidortipo> listaServico) {
		this.listaServico = listaServico;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdarquivolegenda")
	public Arquivo getArquivoLegenda() {
		return arquivoLegenda;
	}
	public void setArquivoLegenda(Arquivo arquivoLegenda) {
		this.arquivoLegenda = arquivoLegenda;
	}
}
