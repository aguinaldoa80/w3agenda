package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_tela", sequenceName = "sq_tela")
public class Tela implements Log{

	protected Integer cdtela;
	protected String descricao;
	protected String path;
	protected Boolean descricaomenu;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Pessoaatalho> listaPessoaatalho = new ListSet<Pessoaatalho>(Pessoaatalho.class);
	protected List<Campolistagem> listaCampolistagem = new ListSet<Campolistagem>(Campolistagem.class);
	protected List<Campoentrada> listaCampoentrada = new ListSet<Campoentrada>(Campoentrada.class);
		
	public static final Integer USUARIO = 4;

	public Tela(){}
	
	public Tela(Integer cdtela) {
		this.cdtela = cdtela;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tela")
	public Integer getCdtela() {
		return cdtela;
	}
	public void setCdtela(Integer id) {
		this.cdtela = id;
	}

	
	@MaxLength(100)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@MaxLength(100)
	public String getPath() {
		return path;
	}
	
	@DisplayName("Utilizar descri��o no menu")
	public Boolean getDescricaomenu() {
		return descricaomenu;
	}
	
	@OneToMany(mappedBy="tela")
	public List<Pessoaatalho> getListaPessoaatalho() {
		return listaPessoaatalho;
	}	
	@OneToMany(mappedBy="tela")
	public List<Campolistagem> getListaCampolistagem() {
		return listaCampolistagem;
	}
	
	@OneToMany(mappedBy="tela")
	public List<Campoentrada> getListaCampoentrada() {
		return listaCampoentrada;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	public void setDescricaomenu(Boolean descricaomenu) {
		this.descricaomenu = descricaomenu;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaPessoaatalho(List<Pessoaatalho> listaPessoaatalho) {
		this.listaPessoaatalho = listaPessoaatalho;
	}

	public void setListaCampolistagem(List<Campolistagem> listaCampolistagem) {
		this.listaCampolistagem = listaCampolistagem;
	}

	public void setListaCampoentrada(List<Campoentrada> listaCampoentrada) {
		this.listaCampoentrada = listaCampoentrada;
	}
	
	public static List<String> getListaModulos(){
		List<String> listaModulos = new ArrayList<String>();
		listaModulos.add("adm");
		listaModulos.add("briefcase");
		listaModulos.add("crm");
		listaModulos.add("faturamento");
		listaModulos.add("financeiro");
		listaModulos.add("fiscal");
		listaModulos.add("contabil");
		listaModulos.add("fornecedor");
		listaModulos.add("juridico");
		listaModulos.add("offline");
		listaModulos.add("producao");
		listaModulos.add("projeto");
		listaModulos.add("pub");
		listaModulos.add("rh");
		listaModulos.add("servicointerno");
		listaModulos.add("sistema");
		listaModulos.add("suprimento");
		listaModulos.add("veiculo");
		
		return listaModulos;
	}
}
