package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Modelo do ve�culo")
@SequenceGenerator(name = "sq_veiculomodelo", sequenceName = "sq_veiculomodelo")
public class Veiculomodelo implements Log{
	
	protected Integer cdveiculomodelo;
	protected String nome;
	protected String descricao;
	protected Fornecedor fornecedor;
	protected Veiculotipo veiculotipo;
	protected Categoriaveiculo categoriaveiculo;
	protected Integer limiteodometro;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator="sq_veiculomodelo")
	public Integer getCdveiculomodelo() {
		return cdveiculomodelo;
	}
	
	@Required
	@DisplayName("Categoria de ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoriaveiculo")
	public Categoriaveiculo getCategoriaveiculo() {
		return categoriaveiculo;
	}
	
	@DisplayName("Tipo de ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculotipo")
	public Veiculotipo getVeiculotipo() {
		return veiculotipo;
	}
	
	@DisplayName("Fabricante")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@Required
	@DisplayName("Nome")
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@DisplayName("Descri��o")
	@MaxLength(200)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Limite do hod�metro")
	@MaxLength(7)
	public Integer getLimiteodometro() {
		return limiteodometro;
	}
	
	public void setCategoriaveiculo(Categoriaveiculo categoriaveiculo) {
		this.categoriaveiculo = categoriaveiculo;
	}
	public void setCdveiculomodelo(Integer cdveiculomodelo) {
		this.cdveiculomodelo = cdveiculomodelo;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setLimiteodometro(Integer limiteodometro) {
		this.limiteodometro = limiteodometro;
	}
	public void setVeiculotipo(Veiculotipo veiculotipo) {
		this.veiculotipo = veiculotipo;
	}

	//Log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
