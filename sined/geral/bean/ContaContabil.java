package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContaCompensacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.view.Vcontacontabil;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_contacontabil", sequenceName = "sq_contacontabil")
public class ContaContabil implements Log{

	protected Integer cdcontacontabil;
	protected String nome;
	protected ContaContabil contaContabilPai;
	protected Tipooperacao tipooperacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean ativo = Boolean.TRUE;
	protected Integer item;	
	protected String formato = "00";	
	protected Vcontacontabil vcontacontabil;
	protected NaturezaContagerencial natureza;
	protected NaturezaContaCompensacaoEnum naturezaContaCompensacao;
	protected String observacao;
	protected Operacaocontabil operacaocontabil;
	protected Integer codigoalternativo;
	protected Boolean usadocalculocustooperacionalproducao;
	protected Boolean usadocalculocustocomercialproducao;
	protected Empresa empresa;
	protected Money saldoInicial;
	protected Date dtSaldoInicial;
	protected Money saldoAtual;
	protected Boolean contaretificadora;


	public ContaContabil() {
	}
	
	public ContaContabil(String nome){
		this.nome = nome;
	}
	
	public ContaContabil(Integer cdcontacontabil){
		this.cdcontacontabil = cdcontacontabil;
	}
	
	public ContaContabil(Integer cdcontacontabil, String nome){
		this.cdcontacontabil = cdcontacontabil;
		this.nome = nome;
	}
	
	public ContaContabil(Integer cdcontacontabil, String nome, String identificador){
		this.cdcontacontabil = cdcontacontabil;
		this.nome = nome;
		
		Vcontacontabil vcontacontabil = new Vcontacontabil();
		vcontacontabil.setIdentificador(identificador);
		this.vcontacontabil = vcontacontabil;
	}
	
	public ContaContabil(Integer cdcontacontabil, String nome, Boolean contaretificadora, String identificador){
		this.cdcontacontabil = cdcontacontabil;
		this.nome = nome;
		this.contaretificadora = contaretificadora;
		
		Vcontacontabil vcontacontabil = new Vcontacontabil();
		vcontacontabil.setIdentificador(identificador);
		this.vcontacontabil = vcontacontabil;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacontabil")
	public Integer getCdcontacontabil() {
		return cdcontacontabil;
	}
	
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Conta Contabil Superior")
	@JoinColumn(name="cdcontacontabilpai")
	public ContaContabil getContaContabilPai() {
		return contaContabilPai;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Empresa")
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Opera��o Contabil")
	@JoinColumn(name="cdoperacaocontabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	
	@MaxLength(9)
	@DisplayName("C�digo Alternativo")
	public Integer getCodigoalternativo() {
		return codigoalternativo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdcontacontabil(Integer cdcontacontabil) {
		this.cdcontacontabil = cdcontacontabil;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setContaContabilPai(ContaContabil contaContabilPai) {
		this.contaContabilPai = contaContabilPai;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	
	public void setCodigoalternativo(Integer codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	@DisplayName("Opera��o")
	@Required
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
	@MaxLength(9)
	@DisplayName("Item")
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	
	@Required
	@MaxLength(10)
	@DisplayName("Formato (Zeros)")
	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil", insertable=false, updatable=false)
	public Vcontacontabil getVcontacontabil() {
		return vcontacontabil;
	}
	
	public void setVcontacontabil(Vcontacontabil vcontacontabil) {
		this.vcontacontabil = vcontacontabil;
	}
	
	@Required
	public NaturezaContagerencial getNatureza() {
		return natureza;
	}
	
	public NaturezaContaCompensacaoEnum getNaturezaContaCompensacao() {
		return naturezaContaCompensacao;
	}
	
	@MaxLength(500)
	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setNatureza(NaturezaContagerencial natureza) {
		this.natureza = natureza;
	}
	
	public void setNaturezaContaCompensacao(
			NaturezaContaCompensacaoEnum naturezaContaCompensacao) {
		this.naturezaContaCompensacao = naturezaContaCompensacao;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Custo")
	public Boolean getUsadocalculocustooperacionalproducao() {
		return usadocalculocustooperacionalproducao;
	}
	
	@DisplayName("Despesa")
	public Boolean getUsadocalculocustocomercialproducao() {
		return usadocalculocustocomercialproducao;
	}
	
	public void setUsadocalculocustocomercialproducao(
			Boolean usadocalculocustocomercialproducao) {
		this.usadocalculocustocomercialproducao = usadocalculocustocomercialproducao;
	}
	
	public void setUsadocalculocustooperacionalproducao(Boolean usadocalculocustooperacionalproducao) {
		this.usadocalculocustooperacionalproducao = usadocalculocustooperacionalproducao;
	}
	
	//========================= TRANSIENT ==================================//
	
	private ContaContabil pai;
	private Tipooperacao tipooperacao2;
	private List<ContaContabil> filhos = new ArrayList<ContaContabil>();
	private Boolean analiticacomregistros;
	private String descricaoInputPersonalizado;
	private Boolean adicionarCodigoalternativo;
	
	@Transient
	@DescriptionProperty(usingFields={"nome","vcontacontabil"})
	public String getDescricaoCombo(){
		return (getVcontacontabil() == null || getVcontacontabil().getArvorepai() == null || getVcontacontabil().getArvorepai().equals("") ? getNome() : (getNome()+" ("+getVcontacontabil().getArvorepai()+")"  + (empresa !=null && empresa.getNome() != null ?  empresa.getNome() : "")));
	}
	
	
	@Transient
	public String getDescricaoInputPersonalizado(){
		if(getVcontacontabil() != null){
			this.descricaoInputPersonalizado = (getVcontacontabil() !=null? getVcontacontabil().getIdentificador()  + " - " :"") + getNome() + (getVcontacontabil().getArvorepai() != null ? " (" + getVcontacontabil().getArvorepai() + ")" : "");
			if(empresa !=null && empresa.getNome()!=null){
				this.descricaoInputPersonalizado += "<BR><b>Empresa</b>: " + empresa.getNome();

			}
			if (Boolean.TRUE.equals(getAdicionarCodigoalternativo()) && getCodigoalternativo()!=null){
				this.descricaoInputPersonalizado += "<br><b>C�digo alt.</b>: " + getCodigoalternativo();
			}
		}
		return this.descricaoInputPersonalizado;
	}
	
	public void setDescricaoInputPersonalizado(String descricaoInputPersonalizado) {
		this.descricaoInputPersonalizado = descricaoInputPersonalizado;
	}
	
	@Transient
	public List<ContaContabil> getFilhos() {
		return filhos;
	}
	
	public void setFilhos(List<ContaContabil> filhos) {
		this.filhos = filhos;
	}
	
	@Transient
	@DisplayName("Conta gerencial superior")
	public ContaContabil getPai() {
		return pai;
	}
	
	public void setPai(ContaContabil pai) {
		this.pai = pai;
	}
	@Transient
	public Boolean getAnaliticacomregistros() {
		return analiticacomregistros;
	}
	
	public void setAnaliticacomregistros(Boolean analiticacomregistros) {
		this.analiticacomregistros = analiticacomregistros;
	}
	
	@Transient
	public Tipooperacao getTipooperacao2() {
		return tipooperacao2;
	}
	
	public void setTipooperacao2(Tipooperacao tipooperacao2) {
		this.tipooperacao2 = tipooperacao2;
	}
	
	@Transient
	public Boolean getAdicionarCodigoalternativo() {
		return adicionarCodigoalternativo;
	}

	public void setAdicionarCodigoalternativo(Boolean adicionarCodigoalternativo) {
		this.adicionarCodigoalternativo = adicionarCodigoalternativo;
	}
	
	public Money getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(Money saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	
	@DisplayName("Data do saldo inicial")
	public Date getDtSaldoInicial() {
		return dtSaldoInicial;
	}
	public void setDtSaldoInicial(Date dtSaldoInicial) {
		this.dtSaldoInicial = dtSaldoInicial;
	}
	
	@DisplayName("Conta Retificadora")
	public Boolean getContaretificadora() {
		return contaretificadora;
	}
	
	public void setContaretificadora(Boolean contaretificadora) {
		this.contaretificadora = contaretificadora;
	}
	
	@Transient
	public Money getSaldoAtual() {
		return saldoAtual;
	}
	public void setSaldoAtual(Money saldoAtual) {
		this.saldoAtual = saldoAtual;
	}

	@Transient	
	public String getDescricao() {
		String pais = "";
		ContaContabil pai = this.getContaContabilPai();
		if(!Hibernate.isInitialized(pai)){
			pai = null;
		}
		while(pai != null){
			if (!pais.equals("")) {
				pais = pai.getNome() + ">" + pais;
			} else {
				pais = pai.getNome();
			}
			pai = pai.getContaContabilPai();
			if(!Hibernate.isInitialized(pai)){
				pai = null;
			}
		}
		return this.getNome() + (!pais.equals("") ? " (" + pais + ")" : "");
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcontacontabil == null) ? 0 : cdcontacontabil.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final ContaContabil other = (ContaContabil) obj;
		if (cdcontacontabil == null) {
			if (other.cdcontacontabil != null)
				return false;
		} else if (!cdcontacontabil.equals(other.cdcontacontabil))
			return false;
		return true;
	}
}
