package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_emporiumtotalizadores", sequenceName = "sq_emporiumtotalizadores")
public class Emporiumtotalizadores implements Log {
	
	protected Integer cdemporiumtotalizadores;
	protected Date data;
	protected Emporiumpdv emporiumpdv;
	protected String legenda;
	protected Tributacaoecf tributacaoecf;
	protected Double aliqicms;
	protected Double valorbcicms;
	protected Double valoricms;
	
	// TRANSIENTE
	protected Double somaitem;
	protected Double diferencasomaitem;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumtotalizadores")
	public Integer getCdemporiumtotalizadores() {
		return cdemporiumtotalizadores;
	}
	
	public Date getData() {
		return data;
	}

	@DisplayName("PDV")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdemporiumpdv")
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}

	@DisplayName("Tributa��o do ECF")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdtributacaoecf")
	public Tributacaoecf getTributacaoecf() {
		return tributacaoecf;
	}

	@DisplayName("Base de c�lculo ICMS")
	public Double getValorbcicms() {
		return valorbcicms;
	}

	@DisplayName("Valor ICMS")
	public Double getValoricms() {
		return valoricms;
	}
	
	@DisplayName("Al�quota ICMS")
	public Double getAliqicms() {
		return aliqicms;
	}
	
	@MaxLength(10)
	@DisplayName("Legenda")
	public String getLegenda() {
		return legenda;
	}
	
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	
	public void setAliqicms(Double aliqicms) {
		this.aliqicms = aliqicms;
	}

	public void setCdemporiumtotalizadores(Integer cdemporiumtotalizadores) {
		this.cdemporiumtotalizadores = cdemporiumtotalizadores;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}

	public void setTributacaoecf(Tributacaoecf tributacaoecf) {
		this.tributacaoecf = tributacaoecf;
	}

	public void setValorbcicms(Double valorbcicms) {
		this.valorbcicms = valorbcicms;
	}

	public void setValoricms(Double valoricms) {
		this.valoricms = valoricms;
	}
	
	@Override
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}

	@Override
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
	}
	
	@Transient
	public Double getSomaitem() {
		return somaitem;
	}
	
	@Transient
	public Double getDiferencasomaitem() {
		return diferencasomaitem;
	}
	
	public void setDiferencasomaitem(Double diferencasomaitem) {
		this.diferencasomaitem = diferencasomaitem;
	}
	
	public void setSomaitem(Double somaitem) {
		this.somaitem = somaitem;
	}
	
}
