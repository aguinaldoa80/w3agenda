package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Registro de corre��o")
@SequenceGenerator(name = "sq_notaerrocorrecao", sequenceName = "sq_notaerrocorrecao")
public class Notaerrocorrecao implements Log{

	protected Integer cdnotaerrocorrecao;
	protected Notaerrocorrecaotipo tipo;
	protected String prefixo;
	protected String codigo;
	protected String url;
	protected String mensagem;
	protected String correcao;
	protected List<NotaErroCorrecaoHistorico> listaNotaErroCorrecaoHistorico = new ListSet<NotaErroCorrecaoHistorico>(NotaErroCorrecaoHistorico.class);

	//LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Notaerrocorrecao() {}

	public Notaerrocorrecao(String correcao) {
		this.correcao = correcao;
	}
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notaerrocorrecao")
	public Integer getCdnotaerrocorrecao() {
		return cdnotaerrocorrecao;
	}
	public Notaerrocorrecaotipo getTipo() {
		return tipo;
	}
	@MaxLength(20)
	public String getPrefixo() {
		return prefixo;
	}
	@MaxLength(10)
	public String getCodigo() {
		return codigo;
	}
	@MaxLength(500)
	public String getUrl() {
		return url;
	}
	@MaxLength(1000)
	public String getMensagem() {
		return mensagem;
	}
	@MaxLength(5000)
	public String getCorrecao() {
		return correcao;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="notaerrocorrecao")
	public List<NotaErroCorrecaoHistorico> getListaNotaErroCorrecaoHistorico() {
		return listaNotaErroCorrecaoHistorico;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	//SET
	public void setCdnotaerrocorrecao(Integer cdnotaerrocorrecao) {
		this.cdnotaerrocorrecao = cdnotaerrocorrecao;
	}
	public void setTipo(Notaerrocorrecaotipo tipo) {
		this.tipo = tipo;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public void setCorrecao(String correcao) {
		this.correcao = correcao;
	}
	public void setListaNotaErroCorrecaoHistorico(
			List<NotaErroCorrecaoHistorico> listaNotaErroCorrecaoHistorico) {
		this.listaNotaErroCorrecaoHistorico = listaNotaErroCorrecaoHistorico;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
