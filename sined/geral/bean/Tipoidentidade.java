package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_tipoidentidade", sequenceName = "sq_tipoidentidade")
public class Tipoidentidade implements Log{
	
	public static Tipoidentidade IDENTIDADE = new Tipoidentidade(1, "Identidade");
	
	protected Integer cdtipoidentidade;
	protected String descricao;
	
	public Tipoidentidade(){
		
	}
	public Tipoidentidade(Integer cdtipoidentidade, String descricao){
		this.cdtipoidentidade = cdtipoidentidade;
		this.descricao = descricao;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoidentidade")
	public Integer getCdtipoidentidade() {
		return cdtipoidentidade;
	}
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setCdtipoidentidade(Integer cdtipoidentidade) {
		this.cdtipoidentidade = cdtipoidentidade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {}
	public void setDtaltera(Timestamp dtaltera) {}

}
