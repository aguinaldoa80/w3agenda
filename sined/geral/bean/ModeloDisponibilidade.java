package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_modelodisponibilidade", sequenceName = "sq_modelodisponibilidade")
public class ModeloDisponibilidade implements Log{
	private Integer cdmodelodisponibilidade;
	private String descricao;
	private String textoMensagem;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@GeneratedValue(generator = "sq_modelodisponibilidade", strategy = GenerationType.AUTO)
	@DisplayName("Identificador")
	public Integer getCdmodelodisponibilidade() {
		return cdmodelodisponibilidade;
	}
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Texto da mensagem")
	public String getTextoMensagem() {
		return textoMensagem;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdmodelodisponibilidade(Integer cdmodelodisponibilidade) {
		this.cdmodelodisponibilidade = cdmodelodisponibilidade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTextoMensagem(String textoMensagem) {
		this.textoMensagem = textoMensagem;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}