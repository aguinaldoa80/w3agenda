package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_tributacaoecf", sequenceName = "sq_tributacaoecf")
public class Tributacaoecf implements Log {

	protected Integer cdtributacaoecf;
	protected String codigoecf;
	protected Tipocobrancapis cstpis;
	protected Double aliqpis;
	protected Tipocobrancacofins cstcofins;
	protected Double aliqcofins;
	protected Tipocobrancaicms csticms;
	protected Origemproduto origemproduto;
	protected Cfop cfop;
	protected Integer numerototalizador;
	protected String codigototalizador;
	protected String codigototalizadorcancelada;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tributacaoecf")
	public Integer getCdtributacaoecf() {
		return cdtributacaoecf;
	}
	
	@DisplayName("CST PIS")
	public Tipocobrancapis getCstpis() {
		return cstpis;
	}
	
	@DisplayName("CST COFINS")
	public Tipocobrancacofins getCstcofins() {
		return cstcofins;
	}
	
	@DisplayName("CST ICMS")
	public Tipocobrancaicms getCsticms() {
		return csticms;
	}
	
	@DisplayName("Origem do produto")
	public Origemproduto getOrigemproduto() {
		return origemproduto;
	}
	
	@DisplayName("CFOP")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	public Cfop getCfop() {
		return cfop;
	}
	
	@DisplayName("C�digo do totalizador")
	@MaxLength(7)
	public String getCodigototalizador() {
		return codigototalizador;
	}
	
	@DisplayName("C�digo do totalizador de cancelamento")
	public String getCodigototalizadorcancelada() {
		return codigototalizadorcancelada;
	}
	
	@DisplayName("N�mero do totalizador")
	public Integer getNumerototalizador() {
		return numerototalizador;
	}
	
	@DisplayName("Al�quota COFINS")
	public Double getAliqcofins() {
		return aliqcofins;
	}
	
	@DisplayName("Al�quota PIS")
	public Double getAliqpis() {
		return aliqpis;
	}
	
	@Required
	@DescriptionProperty
	@DisplayName("C�digo de integra��o")
	public String getCodigoecf() {
		return codigoecf;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setCodigoecf(String codigoecf) {
		this.codigoecf = codigoecf;
	}

	public void setCstpis(Tipocobrancapis cstpis) {
		this.cstpis = cstpis;
	}

	public void setAliqpis(Double aliqpis) {
		this.aliqpis = aliqpis;
	}

	public void setCstcofins(Tipocobrancacofins cstcofins) {
		this.cstcofins = cstcofins;
	}

	public void setAliqcofins(Double aliqcofins) {
		this.aliqcofins = aliqcofins;
	}

	public void setCsticms(Tipocobrancaicms csticms) {
		this.csticms = csticms;
	}

	public void setOrigemproduto(Origemproduto origemproduto) {
		this.origemproduto = origemproduto;
	}

	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}

	public void setNumerototalizador(Integer numerototalizador) {
		this.numerototalizador = numerototalizador;
	}

	public void setCodigototalizador(String codigototalizador) {
		this.codigototalizador = codigototalizador;
	}

	public void setCodigototalizadorcancelada(String codigototalizadorcancelada) {
		this.codigototalizadorcancelada = codigototalizadorcancelada;
	}

	public void setCdtributacaoecf(Integer cdtributacaoecf) {
		this.cdtributacaoecf = cdtributacaoecf;
	}
	
}
