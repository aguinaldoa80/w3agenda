package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_arquivonfnotaerro", sequenceName = "sq_arquivonfnotaerro")
public class Arquivonfnotaerro {
	
	protected Integer cdarquivonfnotaerro;
	protected Arquivonf arquivonf;
	protected Arquivonfnota arquivonfnota;
	protected Timestamp data;
	protected String mensagem;
	protected String correcao;
	protected String codigo;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivonfnotaerro")
	public Integer getCdarquivonfnotaerro() {
		return cdarquivonfnotaerro;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivonfnota")
	public Arquivonfnota getArquivonfnota() {
		return arquivonfnota;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	@DisplayName("Corre��o")
	public String getCorrecao() {
		return correcao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivonf")
	public Arquivonf getArquivonf() {
		return arquivonf;
	}
	
	@DisplayName("Data")
	public Timestamp getData() {
		return data;
	}
	
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}
	
	public void setArquivonf(Arquivonf arquivonf) {
		this.arquivonf = arquivonf;
	}
	
	public void setCorrecao(String correcao) {
		this.correcao = correcao;
	}
	
	public void setArquivonfnota(Arquivonfnota arquivonfnota) {
		this.arquivonfnota = arquivonfnota;
	}
	
	public void setCdarquivonfnotaerro(Integer cdarquivonfnotaerro) {
		this.cdarquivonfnotaerro = cdarquivonfnotaerro;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
