package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_rateiomodeloitem",sequenceName="sq_rateiomodeloitem")
public class Rateiomodeloitem {

	private Integer cdrateiomodeloitem;
	private Centrocusto centrocusto;
	private Contagerencial contagerencial;
	private Projeto projeto;
	private Double percentual;
	private Rateiomodelo rateiomodelo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_rateiomodeloitem")
	public Integer getCdrateiomodeloitem() {
		return cdrateiomodeloitem;
	}
	public void setCdrateiomodeloitem(Integer cdrateiomodeloitem) {
		this.cdrateiomodeloitem = cdrateiomodeloitem;
	}

	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Required
	@DisplayName("%")
	public Double getPercentual() {
		return percentual;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateiomodelo")
	public Rateiomodelo getRateiomodelo() {
		return rateiomodelo;
	}
	public void setRateiomodelo(Rateiomodelo rateiomodelo) {
		this.rateiomodelo = rateiomodelo;
	}
}
