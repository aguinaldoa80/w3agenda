package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@SequenceGenerator(name = "sq_clientearquivo", sequenceName = "sq_clientearquivo")
public class Clientearquivo {
	
	private Integer cdclientearquivo;
	private Cliente cliente;
	private Arquivo arquivo;
	private Date dtcriacao = SinedDateUtils.currentDate();
	private Date validade;
	private String descricao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_clientearquivo")
	public Integer getCdclientearquivo() {
		return cdclientearquivo;
	}
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	@JoinColumn(name="cdarquivo")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Data")
	public Date getDtcriacao() {
		return dtcriacao;
	}
	public Date getValidade() {
		return validade;
	}
	@DisplayName("Descri��o")
	@MaxLength(255)
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdclientearquivo(Integer cdclientearquivo) {
		this.cdclientearquivo = cdclientearquivo;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDtcriacao(Date dtcriacao) {
		this.dtcriacao = dtcriacao;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
