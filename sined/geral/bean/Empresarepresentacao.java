package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_empresarepresentacao", sequenceName = "sq_empresarepresentacao")
@DisplayName("Representa��o")
public class Empresarepresentacao {

	protected Integer cdempresarepresentacao;
	protected Empresa empresa;
	protected Fornecedor fornecedor;
	protected Materialgrupo materialgrupo;
	protected Money comissaovenda;
	protected Boolean repasseunico;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresarepresentacao")
	public Integer getCdempresarepresentacao() {
		return cdempresarepresentacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Material Grupo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@Required
	@DisplayName("Comiss�o de Venda %")
	public Money getComissaovenda() {
		return comissaovenda;
	}
	@DisplayName("Repasse �nico")
	public Boolean getRepasseunico() {
		return repasseunico;
	}
	
	public void setCdempresarepresentacao(Integer cdempresarepresentacao) {
		this.cdempresarepresentacao = cdempresarepresentacao;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setComissaovenda(Money comissaovenda) {
		this.comissaovenda = comissaovenda;
	}
	public void setRepasseunico(Boolean repasseunico) {
		this.repasseunico = repasseunico;
	}
	
	@DisplayName("Conta Despesa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro Custo Despesa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
}
