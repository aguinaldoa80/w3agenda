package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(sequenceName="sq_parcelamento",name="sq_parcelamento")
public class Parcelamento implements Log{

	protected Integer cdparcelamento;
	protected Prazopagamento prazopagamento;
	protected Integer iteracoes;
	protected Set<Parcela> listaParcela;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Parcelamento() {
	}
	
	public Parcelamento(Integer cdparcelamento, Integer iteracoes) {
		this.cdparcelamento = cdparcelamento;
		this.iteracoes = iteracoes;
	}
	@Id
	@GeneratedValue(generator="sq_parcelamento",strategy=GenerationType.AUTO)
	public Integer getCdparcelamento() {
		return cdparcelamento;
	}
	@DisplayName("Prazo de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@DisplayName("Itera��es")
	public Integer getIteracoes() {
		return iteracoes;
	}
	@OneToMany(mappedBy="parcelamento")
	public Set<Parcela> getListaParcela() {
		return listaParcela;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdparcelamento(Integer cdparcelamento) {
		this.cdparcelamento = cdparcelamento;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setIteracoes(Integer iteracoes) {
		this.iteracoes = iteracoes;
	}
	public void setListaParcela(Set<Parcela> listaParcela) {
		this.listaParcela = listaParcela;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
}
