package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessaagrupamento",sequenceName="sq_bancoconfiguracaoremessaagrupamento")
public class BancoConfiguracaoRemessaAgrupamento {

	private Integer cdbancoconfiguracaoremessaagrupamento;
	private Documento documento;
	private Integer identificador;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessaagrupamento")
	public Integer getCdbancoconfiguracaoremessaagrupamento() {
		return cdbancoconfiguracaoremessaagrupamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	public Integer getIdentificador() {
		return identificador;
	}

	public void setCdbancoconfiguracaoremessaagrupamento(
			Integer cdbancoconfiguracaoremessaagrupamento) {
		this.cdbancoconfiguracaoremessaagrupamento = cdbancoconfiguracaoremessaagrupamento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
}
