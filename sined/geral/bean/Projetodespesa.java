package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_projetodespesa", sequenceName = "sq_projetodespesa")
@DisplayName("Despesas")
public class Projetodespesa {
	
	protected Integer cdprojetodespesa;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Money valortotal;
	protected Money valorhora;
	protected Date mesano;
	
	//TRANSIENT
	protected String mesanoAux;
	
	public Projetodespesa() {
	}
	
	public Projetodespesa(Contagerencial contagerencial, Money valortotal, Money valorhora) {
		this.contagerencial = contagerencial;
		this.valortotal = valortotal;
		this.valorhora = valorhora;
	}
	
	public Projetodespesa(Integer cdprojeto, String nomeprojeto, Long valortotal) {
		this.projeto = new Projeto(cdprojeto);
		this.projeto.setNome(nomeprojeto);
		this.valortotal = new Money(valortotal, true);
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_projetodespesa")	
	public Integer getCdprojetodespesa() {
		return cdprojetodespesa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")	
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")	
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@Required
	@DisplayName("Total")
	public Money getValortotal() {
		return valortotal;
	}
	
	@Required
	@DisplayName("R$/h")
	public Money getValorhora() {
		return valorhora;
	}
	
	public Date getMesano() {
		return mesano;
	}
	
	public void setMesano(Date mesano) {
		this.mesano = mesano;
	}
	public void setCdprojetodespesa(Integer cdprojetodespesa) {
		this.cdprojetodespesa = cdprojetodespesa;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setValorhora(Money valorhora) {
		this.valorhora = valorhora;
	}	
	
	
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.mesano != null)
			return new SimpleDateFormat("MM/yyyy").format(this.mesano);
		return null;
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			if(mesanoAux != null && !"".equals(mesanoAux)){
				this.mesano = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}

}
