package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_dadobancario", sequenceName = "sq_dadobancario")
public class Dadobancario implements Log{

	protected Integer cddadobancario;
	protected Banco banco;
	protected Tipoconta tipoconta;
	protected String operacao;
	protected String agencia;
	protected String dvagencia;
	protected String conta;
	protected String dvconta;
	protected Pessoa pessoa;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dadobancario")
	public Integer getCddadobancario() {
		return cddadobancario;
	}
	public void setCddadobancario(Integer id) {
		this.cddadobancario = id;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	public Banco getBanco() {
		return banco;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoconta")
	@DisplayName("Tipo de conta")
	public Tipoconta getTipoconta() {
		return tipoconta;
	}
	
	@MaxLength(5)
	@DisplayName("Opera��o")
	public String getOperacao() {
		return operacao;
	}
	
	@MaxLength(10)
	@DisplayName("Ag�ncia")
	public String getAgencia() {
		return agencia;
	}
	
	@MaxLength(10)
	public String getConta() {
		return conta;
	}
	
	@MaxLength(2)
	public String getDvagencia() {
		return dvagencia;
	}
	
	@MaxLength(2)
	public String getDvconta() {
		return dvconta;
	}
	
	@JoinColumn(name="cdpessoa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	public void setTipoconta(Tipoconta tipoconta) {
		this.tipoconta = tipoconta;
	}
	
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	
	public void setConta(String conta) {
		this.conta = conta;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setDvagencia(String dvagencia) {
		this.dvagencia = dvagencia;
	}
	public void setDvconta(String dvconta) {
		this.dvconta = dvconta;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	/**
	 * Verifica se alguma das propriedades agencia, banco, conta, dtagencia e dvconta est�o preenchidas.
	 * Caso estejam o bean deve ser persistido.
	 * 
	 * @return
	 * @author Fl�vio Tavares
	 */
	@Transient
	public boolean mainFieldsNotNull(){
		return StringUtils.isNotBlank(agencia) || SinedUtil.isObjectValid(banco) || StringUtils.isNotBlank(conta) 
				|| StringUtils.isNotBlank(dvagencia) || StringUtils.isNotBlank(dvconta);
	}
}
