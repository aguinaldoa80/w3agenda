package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@DisplayName("Contrato de fornecimento")
@SequenceGenerator(name = "sq_fornecimentocontrato", sequenceName = "sq_fornecimentocontrato")
public class Fornecimentocontrato implements Log, PermissaoProjeto, PermissaoFornecedorEmpresa {

	protected Integer cdfornecimentocontrato;
	protected String descricao;
	protected Colaborador colaborador;
	protected Fornecedor fornecedor;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Money valor;
	protected Prazopagamento prazopagamento;
	protected Date dtproximovencimento;
	protected Date dtinicio;
	protected Date dtfim;
	protected String observacao;
	protected Boolean ativo = true;
	protected Fornecimentotipo fornecimentotipo;
	protected Fluxocaixa fluxocaixa;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;	
	protected Arquivo arquivo;
	protected Empresa empresa;
	protected Set<Fornecimentocontratoitem> listaFornecimentocontratoitem = new ListSet<Fornecimentocontratoitem>(Fornecimentocontratoitem.class);

	//TRIBUTOS
	protected Double ir;
	protected Boolean incideir;
	protected Double iss;
	protected Boolean incideiss;
	protected Double pis;
	protected Boolean incidepis;
	protected Double cofins;
	protected Boolean incidecofins;
	protected Double csll;
	protected Boolean incidecsll;
	protected Double inss;
	protected Boolean incideinss;
	protected Double icms;
	protected Boolean incideicms;
	protected Integer codigogps;
	protected Boolean corvermelho;
	
	//TRANSIENTE
	protected Money valorIr;
	protected Money valorInss;
	protected Money valorIss;
	protected Money valorPis;
	protected Money valorCofins;
	protected Money valorCsll;
	protected Money valorIcms;
	protected Money valorFinal;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected String whereInOrdemcompra;
	
	
	
	public Fornecimentocontrato(){}
	
	public Fornecimentocontrato(Integer cdfornecimentocontrato){
		this.cdfornecimentocontrato = cdfornecimentocontrato;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fornecimentocontrato")
	public Integer getCdfornecimentocontrato() {
		return cdfornecimentocontrato;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@DisplayName("Descri��o")
	@Required
	@MaxLength(150)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@Required
	@DisplayName("Prazo de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@Required
	public Money getValor() {
		return valor;
	}
	
	@Required
	@DisplayName("Pr�ximo vencimento")
	public Date getDtproximovencimento() {
		return dtproximovencimento;
	}

	@Required
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data de fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	@MaxLength(150)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	@DisplayName("Tipo de fornecimento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentotipo")
	public Fornecimentotipo getFornecimentotipo() {
		return fornecimentotipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfluxocaixa")
	public Fluxocaixa getFluxocaixa() {
		return fluxocaixa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setFluxocaixa(Fluxocaixa fluxocaixa) {
		this.fluxocaixa = fluxocaixa;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setDtproximovencimento(Date dtproximovencimento) {
		this.dtproximovencimento = dtproximovencimento;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setObservacao(String observacao) {
		this.observacao = StringUtils.trimToNull(observacao);
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setFornecimentotipo(Fornecimentotipo fornecimentotipo) {
		this.fornecimentotipo = fornecimentotipo;
	}

	public void setCdfornecimentocontrato(Integer cdfornecimentocontrato) {
		this.cdfornecimentocontrato = cdfornecimentocontrato;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Fornecimentocontrato) {
			Fornecimentocontrato fornecimentocontrato = (Fornecimentocontrato) obj;
			return this.getCdfornecimentocontrato().equals(fornecimentocontrato.getCdfornecimentocontrato());
		}
		return super.equals(obj);
	}
	
//	LISTA

	@OneToMany(mappedBy="fornecimentocontrato")
	@DisplayName("Itens")
	public Set<Fornecimentocontratoitem> getListaFornecimentocontratoitem() {
		return listaFornecimentocontratoitem;
	}
	
	public void setListaFornecimentocontratoitem(
			Set<Fornecimentocontratoitem> listaFornecimentocontratoitem) {
		this.listaFornecimentocontratoitem = listaFornecimentocontratoitem;
	}
	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public String subQueryProjeto() {
		return "select fornecimentocontratosubQueryProjeto.cdfornecimentocontrato " +
				"from Fornecimentocontrato fornecimentocontratosubQueryProjeto " +
				"join fornecimentocontratosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	
	
	public String subQueryFornecedorEmpresa() {
		return "select fornecimentocontratoSubQueryFornecedorEmpresa.cdfornecimentocontrato " +
				"from Fornecimentocontrato fornecimentocontratoSubQueryFornecedorEmpresa " +
				"left outer join fornecimentocontratoSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@MaxLength(20)
	@DisplayName("IR(%)")
	public Double getIr() {
		return ir;
	}

	@DisplayName("Incide IR")
	public Boolean getIncideir() {
		return incideir;
	}

	@MaxLength(20)
	@DisplayName("ISS(%)")
	public Double getIss() {
		return iss;
	}
	
	@DisplayName("Incide ISS")
	public Boolean getIncideiss() {
		return incideiss;
	}
	
	@MaxLength(20)
	@DisplayName("PIS(%)")
	public Double getPis() {
		return pis;
	}
	
	@DisplayName("Incide PIS")
	public Boolean getIncidepis() {
		return incidepis;
	}
	

	@MaxLength(20)
	@DisplayName("COFINS(%)")
	public Double getCofins() {
		return cofins;
	}
	
	@DisplayName("Incide COFINS")
	public Boolean getIncidecofins() {
		return incidecofins;
	}

	@MaxLength(20)
	@DisplayName("CSLL(%)")
	public Double getCsll() {
		return csll;
	}

	@DisplayName("Incide CSLL")
	public Boolean getIncidecsll() {
		return incidecsll;
	}
	
	@MaxLength(20)
	@DisplayName("INSS(%)")
	public Double getInss() {
		return inss;
	}

	@DisplayName("Incide INSS")
	public Boolean getIncideinss() {
		return incideinss;
	}
	
	@MaxLength(20)
	@DisplayName("ICMS(%)")
	public Double getIcms() {
		return icms;
	}
	
	@DisplayName("Incide ICMS")
	public Boolean getIncideicms() {
		return incideicms;
	}
	
	@MaxLength(9)
	@DisplayName("C�digo da GPS")
	public Integer getCodigogps() {
		return codigogps;
	}


	public void setIr(Double ir) {
		this.ir = ir;
	}

	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}

	public void setIss(Double iss) {
		this.iss = iss;
	}

	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}

	public void setCsll(Double csll) {
		this.csll = csll;
	}

	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}

	public void setInss(Double inss) {
		this.inss = inss;
	}

	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}

	public void setCodigogps(Integer codigogps) {
		this.codigogps = codigogps;
	}

	//Transient
	@Transient
	public Money getValorCofins() {
		if (this.cofins != null && valor!=null) {
			//valorCofins = new Money(this.cofins);
			valorCofins = valor.multiply(new Money(this.cofins)).divide(new Money(100));
		}
		return valorCofins != null ? valorCofins.round() : null;
	}
	
	@Transient
	public Money getValorCsll() {
		if (this.csll != null && valor!=null) {
			//valorCsll = new Money(this.csll);
			valorCsll = valor.multiply(new Money(this.csll)).divide(new Money(100));
		}
		return valorCsll != null ? valorCsll.round() : null;
	}
	
	@Transient
	public Money getValorIcms() {
		if (this.icms != null && valor!=null) {
			//valorIcms = new Money(this.icms);
			valorIcms = valor.multiply(new Money(this.icms)).divide(new Money(100));
		}
		return valorIcms != null ? valorIcms.round() : null;
	}
	
	@Transient
	public Money getValorPis() {
		if (this.pis != null && valor!=null) {
			//valorPis = new Money(this.pis);
			valorPis = valor.multiply(new Money(this.pis)).divide(new Money(100));
		}
		return valorPis != null ? valorPis.round() : null;
	}

	@Transient
	public Money getValorIss() {
		if (this.iss != null && valor!=null) {
			//valorIss = new Money(this.iss);
			valorIss = valor.multiply(new Money(this.iss)).divide(new Money(100));
		}
		return valorIss != null ? valorIss.round() : null;
	}
	
	@Transient
	public Money getValorInss() {
		if (this.inss != null && valor!=null) {
			//valorInss = new Money(this.inss);
			valorInss = valor.multiply(new Money(this.inss)).divide(new Money(100));
		}
		return valorInss != null ? valorInss.round() : null;
	}
	
	@Transient
	public Money getValorIr() {
		if (this.ir != null && valor!=null) {
			//valorIr = new Money(this.ir);
			valorIr = valor.multiply(new Money(this.ir)).divide(new Money(100));
		}
		return valorIr != null ? valorIr.round() : null;
	}
	
	@Transient
	public Money getValorFinal() {
		
		valorFinal = getValor();
		
				
		if (this.getValorInss() != null && this.incideinss)
			valorFinal = valorFinal.subtract(this.getValorInss());
		
		if (this.getValorIr() != null && this.incideir)
			valorFinal = valorFinal.subtract(this.getValorIr());
		
		if (this.getValorIss() != null && this.incideiss)
			valorFinal = valorFinal.subtract(this.getValorIss());
		
		if (this.getValorIcms() != null && this.incideicms)
			valorFinal = valorFinal.subtract(this.getValorIcms());
		
		if (this.getValorPis() != null && this.incidepis)
			valorFinal = valorFinal.subtract(this.getValorPis());
		
		if (this.getValorCofins() != null && this.incidecofins)
			valorFinal = valorFinal.subtract(this.getValorCofins());
		
		if (this.getValorCsll() != null && this.incidecsll)
			valorFinal = valorFinal.subtract(this.getValorCsll());
		
		return valorFinal;
	}

	public void setValorIr(Money valorIr) {
		this.valorIr = valorIr;
	}

	public void setValorInss(Money valorInss) {
		this.valorInss = valorInss;
	}

	public void setValorIss(Money valorIss) {
		this.valorIss = valorIss;
	}

	public void setValorPis(Money valorPis) {
		this.valorPis = valorPis;
	}

	public void setValorCofins(Money valorCofins) {
		this.valorCofins = valorCofins;
	}

	public void setValorCsll(Money valorCsll) {
		this.valorCsll = valorCsll;
	}

	public void setValorIcms(Money valorIcms) {
		this.valorIcms = valorIcms;
	}

	public void setValorFinal(Money valorFinal) {
		this.valorFinal = valorFinal;
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	@Transient
	public String getWhereInOrdemcompra() {
		return whereInOrdemcompra;
	}
	public void setWhereInOrdemcompra(String whereInOrdemcompra) {
		this.whereInOrdemcompra = whereInOrdemcompra;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	@Transient
	public Boolean getCorvermelho() {
		return corvermelho;
	}
	public void setCorvermelho(Boolean corvermelho) {
		this.corvermelho = corvermelho;
	}
}
