package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_calendarioprojeto", sequenceName = "sq_calendarioprojeto")
public class Calendarioprojeto{

	protected Integer cdcalendarioprojeto;
	protected Calendario calendario;
	protected Projeto projeto;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_calendarioprojeto")
	public Integer getCdcalendarioprojeto() {
		return cdcalendarioprojeto;
	}
	
	public void setCdcalendarioprojeto(Integer cdcalendarioprojeto) {
		this.cdcalendarioprojeto = cdcalendarioprojeto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	@Required
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcalendario")
	public Calendario getCalendario() {
		return calendario;
	}
	
	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}
	
}
