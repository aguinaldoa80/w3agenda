package br.com.linkcom.sined.geral.bean;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_requisicaoorigem", sequenceName = "sq_requisicaoorigem")
@DisplayName("Requisição Origem")
public class Requisicaoorigem{

	protected Integer cdrequisicaoorigem;
	protected Material material;
	protected Projeto projeto;
	protected Usuario usuario;
	protected Pedidovenda pedidovenda;
	protected Producaoagenda producaoagenda;
	protected Planejamentorecursogeral planejamentorecursogeral;
	
	protected Set<Requisicaomaterial> listaRequisicaomaterial;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_requisicaoorigem")
	@DescriptionProperty
	public Integer getCdrequisicaoorigem() {
		return cdrequisicaoorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoagenda")
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamentorecursogeral")
	public Planejamentorecursogeral getPlanejamentorecursogeral() {
		return planejamentorecursogeral;
	}
	@OneToMany(mappedBy="requisicaoorigem")
	public Set<Requisicaomaterial> getListaRequisicaomaterial() {
		return listaRequisicaomaterial;
	}
	
	public void setPlanejamentorecursogeral(Planejamentorecursogeral planejamentorecursogeral) {
		this.planejamentorecursogeral = planejamentorecursogeral;
	}
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setCdrequisicaoorigem(Integer cdrequisicaoorigem) {
		this.cdrequisicaoorigem = cdrequisicaoorigem;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setListaRequisicaomaterial(Set<Requisicaomaterial> listaRequisicaomaterial) {
		this.listaRequisicaomaterial = listaRequisicaomaterial;
	}
}
