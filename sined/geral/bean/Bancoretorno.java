package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_bancoretorno", sequenceName = "sq_bancoretorno")
public class Bancoretorno {

	protected Integer cdbancoretorno;
	protected Banco banco;
	protected String versao;
	
	protected Integer linhainicial;
	protected Integer linhafinal;
	
	protected Boolean duaslinhas;
	protected Boolean agenciacontacabecalho;
	
	protected Integer agenciacedenteposicaoinicial;
	protected Integer agenciacedentetamanho;
	protected Boolean agenciacedenteprimeiralinha;
	
	protected Integer contacedenteposicaoinicial;
	protected Integer contacedentetamanho;
	protected Boolean contacedenteprimeiralinha;
	
	protected Integer datacreditoposicaoinicial;
	protected Integer datacreditotamanho;
	protected Boolean datacreditoprimeiralinha;
	
	protected Integer nossonumeroposicaoinicial;
	protected Integer nossonumerotamanho;
	protected Boolean nossonumeroprimeiralinha;
	
	protected Integer valorcobradoposicaoinicial;
	protected Integer valorcobradotamanho;
	protected Boolean valorcobradoprimeiralinha;
	
	protected Integer valortarifaposicaoinicial;
	protected Integer valortarifatamanho;
	protected Boolean valortarifaprimeiralinha;
	
	protected Integer motivotarifaposicaoinicial;
	protected Integer motivotarifatamanho;
	protected Boolean motivotarifaprimeiralinha;
	
	protected Integer codigoinstrucaoposicaoinicial;
	protected Integer codigoinstrucaotamanho;
	protected Boolean codigoinstrucaoprimeiralinha;
	
	protected String formatodata;
	protected String codigoconfirmacaoentrada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoretorno")
	public Integer getCdbancoretorno() {
		return cdbancoretorno;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	public Banco getBanco() {
		return banco;
	}
	
	@Required
	@DisplayName("Vers�o")
	@MaxLength(10)
	@DescriptionProperty
	public String getVersao() {
		return versao;
	}
	
	@DisplayName("Ag�ncia cedente posi��o inicial")
	public Integer getAgenciacedenteposicaoinicial() {
		return agenciacedenteposicaoinicial;
	}

	@DisplayName("Ag�ncia cedente tamanho")
	public Integer getAgenciacedentetamanho() {
		return agenciacedentetamanho;
	}

	@DisplayName("Conta cedente posi��o inicial")
	public Integer getContacedenteposicaoinicial() {
		return contacedenteposicaoinicial;
	}

	@DisplayName("Conta cedente tamanho")
	public Integer getContacedentetamanho() {
		return contacedentetamanho;
	}

	@DisplayName("Data cr�dito posi��o inicial")
	public Integer getDatacreditoposicaoinicial() {
		return datacreditoposicaoinicial;
	}

	@DisplayName("Data cr�dito tamanho")
	public Integer getDatacreditotamanho() {
		return datacreditotamanho;
	}

	@DisplayName("Nosso n�mero posi��o inicial")
	public Integer getNossonumeroposicaoinicial() {
		return nossonumeroposicaoinicial;
	}

	@DisplayName("Nosso n�mero tamanho")
	public Integer getNossonumerotamanho() {
		return nossonumerotamanho;
	}
	
	@DisplayName("Valor tarifa posi��o inicial")
	public Integer getValortarifaposicaoinicial() {
		return valortarifaposicaoinicial;
	}
	@DisplayName("Valor tarifa tamanho")
	public Integer getValortarifatamanho() {
		return valortarifatamanho;
	}
	@DisplayName("Primeira linha?")
	public Boolean getValortarifaprimeiralinha() {
		return valortarifaprimeiralinha;
	}
	@DisplayName("Motivo tarifa posi��o inicial")
	public Integer getMotivotarifaposicaoinicial() {
		return motivotarifaposicaoinicial;
	}
	@DisplayName("Motivo tarifa tamanho")
	public Integer getMotivotarifatamanho() {
		return motivotarifatamanho;
	}
	@DisplayName("Primeira linha?")
	public Boolean getMotivotarifaprimeiralinha() {
		return motivotarifaprimeiralinha;
	}
	
	@DisplayName("Valor cobran�a posi��o inicial")
	public Integer getValorcobradoposicaoinicial() {
		return valorcobradoposicaoinicial;
	}

	@DisplayName("Valor cobrado tamanho")
	public Integer getValorcobradotamanho() {
		return valorcobradotamanho;
	}

	@DisplayName("Linha inicial")
	public Integer getLinhainicial() {
		return linhainicial;
	}

	@DisplayName("Linha final")
	public Integer getLinhafinal() {
		return linhafinal;
	}

	@DisplayName("Duas linhas")
	public Boolean getDuaslinhas() {
		return duaslinhas;
	}
	
	@DisplayName("Formato da data")
	public String getFormatodata() {
		return formatodata;
	}
	
	@DisplayName("Ag�ncia e conta no cabe�alho")
	public Boolean getAgenciacontacabecalho() {
		return agenciacontacabecalho;
	}
	
	@DisplayName("Primeira linha?")
	public Boolean getAgenciacedenteprimeiralinha() {
		return agenciacedenteprimeiralinha;
	}

	@DisplayName("Primeira linha?")
	public Boolean getContacedenteprimeiralinha() {
		return contacedenteprimeiralinha;
	}

	@DisplayName("Primeira linha?")
	public Boolean getDatacreditoprimeiralinha() {
		return datacreditoprimeiralinha;
	}

	@DisplayName("Primeira linha?")
	public Boolean getNossonumeroprimeiralinha() {
		return nossonumeroprimeiralinha;
	}

	@DisplayName("Primeira linha?")
	public Boolean getValorcobradoprimeiralinha() {
		return valorcobradoprimeiralinha;
	}
	
	@DisplayName("C�digo de intru��o posi��o inicial")
	public Integer getCodigoinstrucaoposicaoinicial() {
		return codigoinstrucaoposicaoinicial;
	}

	@DisplayName("C�digo de intru��o tamanho")
	public Integer getCodigoinstrucaotamanho() {
		return codigoinstrucaotamanho;
	}

	@DisplayName("Primeira linha?")
	public Boolean getCodigoinstrucaoprimeiralinha() {
		return codigoinstrucaoprimeiralinha;
	}
	
	@DisplayName("C�digo de confirma��o da entrada")
	public String getCodigoconfirmacaoentrada() {
		return codigoconfirmacaoentrada;
	}
	
	public void setCodigoconfirmacaoentrada(String codigoconfirmacaoentrada) {
		this.codigoconfirmacaoentrada = codigoconfirmacaoentrada;
	}

	public void setCodigoinstrucaoposicaoinicial(
			Integer codigoinstrucaoposicaoinicial) {
		this.codigoinstrucaoposicaoinicial = codigoinstrucaoposicaoinicial;
	}

	public void setCodigoinstrucaotamanho(Integer codigoinstrucaotamanho) {
		this.codigoinstrucaotamanho = codigoinstrucaotamanho;
	}

	public void setCodigoinstrucaoprimeiralinha(Boolean codigoinstrucaoprimeiralinha) {
		this.codigoinstrucaoprimeiralinha = codigoinstrucaoprimeiralinha;
	}

	public void setAgenciacedenteprimeiralinha(Boolean agenciacedenteprimeiralinha) {
		this.agenciacedenteprimeiralinha = agenciacedenteprimeiralinha;
	}

	public void setContacedenteprimeiralinha(Boolean contacedenteprimeiralinha) {
		this.contacedenteprimeiralinha = contacedenteprimeiralinha;
	}

	public void setDatacreditoprimeiralinha(Boolean datacreditoprimeiralinha) {
		this.datacreditoprimeiralinha = datacreditoprimeiralinha;
	}

	public void setNossonumeroprimeiralinha(Boolean nossonumeroprimeiralinha) {
		this.nossonumeroprimeiralinha = nossonumeroprimeiralinha;
	}

	public void setValorcobradoprimeiralinha(Boolean valorcobradoprimeiralinha) {
		this.valorcobradoprimeiralinha = valorcobradoprimeiralinha;
	}

	public void setAgenciacontacabecalho(Boolean agenciacontacabecalho) {
		this.agenciacontacabecalho = agenciacontacabecalho;
	}

	public void setFormatodata(String formatodata) {
		this.formatodata = formatodata;
	}

	public void setCdbancoretorno(Integer cdbancoretorno) {
		this.cdbancoretorno = cdbancoretorno;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public void setLinhainicial(Integer linhainicial) {
		this.linhainicial = linhainicial;
	}

	public void setLinhafinal(Integer linhafinal) {
		this.linhafinal = linhafinal;
	}
	
	public void setDuaslinhas(Boolean duaslinhas) {
		this.duaslinhas = duaslinhas;
	}

	public void setAgenciacedenteposicaoinicial(Integer agenciacedenteposicaoinicial) {
		this.agenciacedenteposicaoinicial = agenciacedenteposicaoinicial;
	}

	public void setAgenciacedentetamanho(Integer agenciacedentetamanho) {
		this.agenciacedentetamanho = agenciacedentetamanho;
	}

	public void setContacedenteposicaoinicial(Integer contacedenteposicaoinicial) {
		this.contacedenteposicaoinicial = contacedenteposicaoinicial;
	}

	public void setContacedentetamanho(Integer contacedentetamanho) {
		this.contacedentetamanho = contacedentetamanho;
	}

	public void setDatacreditoposicaoinicial(Integer datacreditoposicaoinicial) {
		this.datacreditoposicaoinicial = datacreditoposicaoinicial;
	}

	public void setDatacreditotamanho(Integer datacreditotamanho) {
		this.datacreditotamanho = datacreditotamanho;
	}

	public void setNossonumeroposicaoinicial(Integer nossonumeroposicaoinicial) {
		this.nossonumeroposicaoinicial = nossonumeroposicaoinicial;
	}

	public void setNossonumerotamanho(Integer nossonumerotamanho) {
		this.nossonumerotamanho = nossonumerotamanho;
	}

	public void setValorcobradoposicaoinicial(Integer valorcobradoposicaoinicial) {
		this.valorcobradoposicaoinicial = valorcobradoposicaoinicial;
	}

	public void setValorcobradotamanho(Integer valorcobradotamanho) {
		this.valorcobradotamanho = valorcobradotamanho;
	}

	public void setValortarifaposicaoinicial(Integer valortarifaposicaoinicial) {
		this.valortarifaposicaoinicial = valortarifaposicaoinicial;
	}

	public void setValortarifatamanho(Integer valortarifatamanho) {
		this.valortarifatamanho = valortarifatamanho;
	}

	public void setValortarifaprimeiralinha(Boolean valortarifaprimeiralinha) {
		this.valortarifaprimeiralinha = valortarifaprimeiralinha;
	}
	public void setMotivotarifaposicaoinicial(Integer motivotarifaposicaoinicial) {
		this.motivotarifaposicaoinicial = motivotarifaposicaoinicial;
	}
	public void setMotivotarifatamanho(Integer motivotarifatamanho) {
		this.motivotarifatamanho = motivotarifatamanho;
	}
	public void setMotivotarifaprimeiralinha(Boolean motivotarifaprimeiralinha) {
		this.motivotarifaprimeiralinha = motivotarifaprimeiralinha;
	}
}
