package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConciliacaoCartaoCreditoBean.TipoOperadoraCartaoCredito;

@Entity
@SequenceGenerator(name="sq_configuracaoconciliacaocartao", sequenceName="sq_configuracaoconciliacaocartao")
public class Configuracaoconciliacaocartao {

	private Integer cdconfiguracaoconciliacaocartao;
	private TipoOperadoraCartaoCredito tipooperadora;
	private Integer colunadtefetivacao;
	private Integer colunadtvenda;
	private Integer colunadescricao;
	private Integer colunatid;
	private Integer colunadoc;
	private Integer colunavalor;
	private Integer colunavalorliquido;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_configuracaoconciliacaocartao", strategy=GenerationType.AUTO)
	public Integer getCdconfiguracaoconciliacaocartao() {
		return cdconfiguracaoconciliacaocartao;
	}
	public void setCdconfiguracaoconciliacaocartao(
			Integer cdconfiguracaoconciliacaocartao) {
		this.cdconfiguracaoconciliacaocartao = cdconfiguracaoconciliacaocartao;
	}
	
	@DisplayName("Origem")
	public TipoOperadoraCartaoCredito getTipooperadora() {
		return tipooperadora;
	}
	public void setTipooperadora(TipoOperadoraCartaoCredito tipooperadora) {
		this.tipooperadora = tipooperadora;
	}
	
	@DisplayName("Data de efetiva��o do pagamento")
	public Integer getColunadtefetivacao() {
		return colunadtefetivacao;
	}
	public void setColunadtefetivacao(Integer colunadtefetivacao) {
		this.colunadtefetivacao = colunadtefetivacao;
	}
	
	@DisplayName("Data da venda")
	public Integer getColunadtvenda() {
		return colunadtvenda;
	}
	public void setColunadtvenda(Integer colunadtvenda) {
		this.colunadtvenda = colunadtvenda;
	}
	
	@DisplayName("Descri��o")
	public Integer getColunadescricao() {
		return colunadescricao;
	}
	public void setColunadescricao(Integer colunadescricao) {
		this.colunadescricao = colunadescricao;
	}
	
	@DisplayName("TID")
	public Integer getColunatid() {
		return colunatid;
	}
	public void setColunatid(Integer colunatid) {
		this.colunatid = colunatid;
	}
	
	@DisplayName("DOC")
	public Integer getColunadoc() {
		return colunadoc;
	}
	public void setColunadoc(Integer colunadoc) {
		this.colunadoc = colunadoc;
	}
	
	@DisplayName("Valor")
	public Integer getColunavalor() {
		return colunavalor;
	}
	public void setColunavalor(Integer colunavalor) {
		this.colunavalor = colunavalor;
	}
	
	@DisplayName("Valor l�quido")
	public Integer getColunavalorliquido() {
		return colunavalorliquido;
	}
	public void setColunavalorliquido(Integer colunavalorliquido) {
		this.colunavalorliquido = colunavalorliquido;
	}
}
