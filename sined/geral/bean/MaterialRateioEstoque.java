package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_materialrateioestoque", sequenceName="sq_materialrateioestoque")
public class MaterialRateioEstoque {
	
	private Integer 		cdMaterialRateioEstoque;
	private Contagerencial 	contaGerencialConsumo;
	private Contagerencial 	contaGerencialRequisicao;
	private Contagerencial 	contaGerencialEntrada;
	private Contagerencial 	contaGerencialVenda;
	private Contagerencial 	contaGerencialPerda;
	private Contagerencial 	contaGerencialAjusteEntrada;
	private Contagerencial 	contaGerencialAjusteSaida;
	private Contagerencial 	contaGerencialRomaneioEntrada;
	private Contagerencial 	contaGerencialRomaneioSaida;
	private Contagerencial 	contaGerencialDevolucaoEntrada;
	private Contagerencial 	contaGerencialDevolucaoSaida;
	private Projeto		   	projeto;

	
/////////////////////////////////////////////////////////////////////|
//INICIO DOS GET 											/////////|
/////////////////////////////////////////////////////////////////////|
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialrateioestoque")
	public Integer getCdMaterialRateioEstoque() {
		return cdMaterialRateioEstoque;
	}

	@DisplayName("Conta Gerencial Consumo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialconsumo")
	public Contagerencial getContaGerencialConsumo() {
		return contaGerencialConsumo;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialrequisicao")
	public Contagerencial getContaGerencialRequisicao() {
		return contaGerencialRequisicao;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialentrada")
	public Contagerencial getContaGerencialEntrada() {
		return contaGerencialEntrada;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialvenda")
	public Contagerencial getContaGerencialVenda() {
		return contaGerencialVenda;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialperda")
	public Contagerencial getContaGerencialPerda() {
		return contaGerencialPerda;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialajusteentrada")
	public Contagerencial getContaGerencialAjusteEntrada() {
		return contaGerencialAjusteEntrada;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialajustesaida")
	public Contagerencial getContaGerencialAjusteSaida() {
		return contaGerencialAjusteSaida;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialromaneioentrada")
	public Contagerencial getContaGerencialRomaneioEntrada() {
		return contaGerencialRomaneioEntrada;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialromaneiosaida")
	public Contagerencial getContaGerencialRomaneioSaida() {
		return contaGerencialRomaneioSaida;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialdevolucaoentrada")
	public Contagerencial getContaGerencialDevolucaoEntrada() {
		return contaGerencialDevolucaoEntrada;
	}

	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialdevolucaosaida")
	public Contagerencial getContaGerencialDevolucaoSaida() {
		return contaGerencialDevolucaoSaida;
	}
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

/////////////////////////////////////////////////////////////////////|
//INICIO DOS SET 											/////////|
/////////////////////////////////////////////////////////////////////|

	public void setCdMaterialRateioEstoque(Integer cdMaterialRateioEstoque) {
		this.cdMaterialRateioEstoque = cdMaterialRateioEstoque;
	}
	public void setContaGerencialConsumo(Contagerencial contaGerencialConsumo) {
		this.contaGerencialConsumo = contaGerencialConsumo;
	}
	public void setContaGerencialRequisicao(Contagerencial contaGerencialRequisicao) {
		this.contaGerencialRequisicao = contaGerencialRequisicao;
	}
	public void setContaGerencialEntrada(Contagerencial contaGerencialEntrada) {
		this.contaGerencialEntrada = contaGerencialEntrada;
	}
	public void setContaGerencialVenda(Contagerencial contaGerencialVenda) {
		this.contaGerencialVenda = contaGerencialVenda;
	}
	public void setContaGerencialPerda(Contagerencial contaGerencialPerda) {
		this.contaGerencialPerda = contaGerencialPerda;
	}
	public void setContaGerencialAjusteEntrada(Contagerencial contaGerencialAjusteEntrada) {
		this.contaGerencialAjusteEntrada = contaGerencialAjusteEntrada;
	}
	public void setContaGerencialAjusteSaida(Contagerencial contaGerencialAjusteSaida) {
		this.contaGerencialAjusteSaida = contaGerencialAjusteSaida;
	}
	public void setContaGerencialRomaneioEntrada(Contagerencial contaGerencialRomaneioEntrada) {
		this.contaGerencialRomaneioEntrada = contaGerencialRomaneioEntrada;
	}
	public void setContaGerencialRomaneioSaida(Contagerencial contaGerencialRomaneioSaida) {
		this.contaGerencialRomaneioSaida = contaGerencialRomaneioSaida;
	}
	public void setContaGerencialDevolucaoEntrada(Contagerencial contaGerencialDevolucaoEntrada) {
		this.contaGerencialDevolucaoEntrada = contaGerencialDevolucaoEntrada;
	}
	public void setContaGerencialDevolucaoSaida(Contagerencial contaGerencialDevolucaoSaida) {
		this.contaGerencialDevolucaoSaida = contaGerencialDevolucaoSaida;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	
	
}
