package br.com.linkcom.sined.geral.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Retencao;
@Entity
@SequenceGenerator(name="sq_grupotributacaoimposto",sequenceName="sq_grupotributacaoimposto")
public class Grupotributacaoimposto{
	
	protected Integer cdgrupotributacaoimposto;
	protected Grupotributacao grupotributacao;
	protected Faixaimpostocontrole controle;
	protected Money aliquotafixa;
	protected Double basecalculopercentual;
	protected String codigonaturezareceita;
	protected Boolean buscaraliquotafixafaixaimposto;
	protected Retencao retencao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacaoimposto")
	public Integer getCdgrupotributacaoimposto() {
		return cdgrupotributacaoimposto;
	}
	@DisplayName("Grupo tributa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@Required
	@Column(name="faixaimpostocontrole")
	public Faixaimpostocontrole getControle() {
		return controle;
	}
	@DisplayName("Al�quota fixa")
	public Money getAliquotafixa() {
		return aliquotafixa;
	}
	@DisplayName("Base de c�lculo (%)")
	public Double getBasecalculopercentual() {
		return basecalculopercentual;
	}
	@MaxLength(3)
	@DisplayName("C�digo Natureza da Receita")
	public String getCodigonaturezareceita() {
		return codigonaturezareceita;
	}
	@DisplayName("Buscar Al�quota pela Faixa de Imposto")
	public Boolean getBuscaraliquotafixafaixaimposto() {
		return buscaraliquotafixafaixaimposto;
	}
	@DisplayName("Reten��o")
	public Retencao getRetencao() {
		return retencao;
	}
	
	public void setBasecalculopercentual(Double basecalculopercentual) {
		this.basecalculopercentual = basecalculopercentual;
	}
	public void setCdgrupotributacaoimposto(Integer cdgrupotributacaoimposto) {
		this.cdgrupotributacaoimposto = cdgrupotributacaoimposto;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setControle(Faixaimpostocontrole controle) {
		this.controle = controle;
	}
	public void setAliquotafixa(Money aliquotafixa) {
		this.aliquotafixa = aliquotafixa;
	}
	public void setCodigonaturezareceita(String codigonaturezareceita) {
		this.codigonaturezareceita = codigonaturezareceita;
	}
	public void setBuscaraliquotafixafaixaimposto(Boolean buscaraliquotafixafaixaimposto) {
		this.buscaraliquotafixafaixaimposto = buscaraliquotafixafaixaimposto;
	}
	public void setRetencao(Retencao retencao) {
		this.retencao = retencao;
	}
}
