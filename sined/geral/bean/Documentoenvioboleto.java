package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.Documentoenvioboletosituacaoagendamento;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_documentoenvioboleto", sequenceName = "sq_documentoenvioboleto")
@DisplayName("Envio autom�tico de boleto")
public class Documentoenvioboleto implements Log{

	private Integer cddocumentoenvioboleto;
	private Empresa empresa;
	private Date dtinicio;
	private Date dtfim;
	private List<Documentoenvioboletocliente> listaCliente;
	private List<Documentoenvioboletosituacao> listaSituacao;
	private List<Documentoenvioboletodiascobranca> listaDiascobranca;
	private List<Documentoenvioboletohistorico> listaHistorico;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	private Documentoenvioboletosituacaoagendamento situacaoagendamento;
	private Usuario usuarioaltera;
	
	//TRANSIENTS
	private List<Documentoacao> listaSituacaoTrans;
	
	@Id
	@GeneratedValue(generator="sq_documentoenvioboleto", strategy=GenerationType.AUTO)
	public Integer getCddocumentoenvioboleto() {
		return cddocumentoenvioboleto;
	}
	public void setCddocumentoenvioboleto(Integer cddocumentoenvioboleto) {
		this.cddocumentoenvioboleto = cddocumentoenvioboleto;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Date getDtinicio() {
		return dtinicio;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public Date getDtfim() {
		return dtfim;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	@DisplayName("Clientes")
	@OneToMany(mappedBy="documentoenvioboleto", fetch=FetchType.LAZY)
	public List<Documentoenvioboletocliente> getListaCliente() {
		return listaCliente;
	}
	public void setListaCliente(List<Documentoenvioboletocliente> listaCliente) {
		this.listaCliente = listaCliente;
	}
	
	@DisplayName("Situa��o da conta")
	@OneToMany(mappedBy="documentoenvioboleto",fetch=FetchType.LAZY)
	public List<Documentoenvioboletosituacao> getListaSituacao() {
		return listaSituacao;
	}
	public void setListaSituacao(
			List<Documentoenvioboletosituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
	@DisplayName("Dias P/ Cobran�a")
	@OneToMany(mappedBy="documentoenvioboleto", fetch=FetchType.LAZY)
	public List<Documentoenvioboletodiascobranca> getListaDiascobranca() {
		return listaDiascobranca;
	}
	public void setListaDiascobranca(
			List<Documentoenvioboletodiascobranca> listaDiascobranca) {
		this.listaDiascobranca = listaDiascobranca;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="documentoenvioboleto", fetch=FetchType.LAZY)
	public List<Documentoenvioboletohistorico> getListaHistorico() {
		return listaHistorico;
	}
	public void setListaHistorico(
			List<Documentoenvioboletohistorico> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("Situa��o do agendamento")
	public Documentoenvioboletosituacaoagendamento getSituacaoagendamento() {
		return situacaoagendamento;
	}
	public void setSituacaoagendamento(
			Documentoenvioboletosituacaoagendamento situacaoagendamento) {
		this.situacaoagendamento = situacaoagendamento;
	}
	
	@Transient
	@DisplayName("Situa��o da conta")
	public List<Documentoacao> getListaSituacaoTrans() {
		if(listaSituacaoTrans == null){
			listaSituacaoTrans = new ArrayList<Documentoacao>();
		}
		return listaSituacaoTrans;
	}
	public void setListaSituacaoTrans(List<Documentoacao> listaSituacaoTrans) {
		this.listaSituacaoTrans = listaSituacaoTrans;
	}
	
	@JoinColumn(name="cdusuarioaltera", insertable=false, updatable=false)
	@ManyToOne(fetch=FetchType.LAZY)
	public Usuario getUsuarioaltera() {
		return usuarioaltera;
	}
	
	public void setUsuarioaltera(Usuario usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	
	@Transient
	@DisplayName("Cliente")
	public String getCliente(){
		if(listaCliente != null && listaCliente.size() > 5){
			return "Diversos";
		} else if(listaCliente != null){
			return CollectionsUtil.listAndConcatenate(listaCliente, "cliente.nome", "<BR>");
		} else {
			return null;
		}
	}
	
	@Transient
	@DisplayName("Per�odo agendamento")
	public String getPeriodoAgendamento(){
		return SinedUtil.getDescricaoPeriodo(getDtinicio(), getDtfim());
	}
	
	@Transient
	@DisplayName("Situa��o da conta")
	public String getSituacaoConta() {
		if(listaSituacao != null){
			return CollectionsUtil.listAndConcatenate(listaSituacao, "documentoacao.nome", ", ");
		} else {
			return null;
		}
	}
	
	@Transient
	@DisplayName("Dias para cobran�a")
	public String getDiasCobranca() {
		if(listaDiascobranca != null){
			return CollectionsUtil.listAndConcatenate(listaDiascobranca, "qtdedias", " - ");
		} else {
			return null;
		}
	}
}