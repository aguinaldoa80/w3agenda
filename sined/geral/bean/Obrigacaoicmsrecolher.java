package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Obriga��o do ICMS Recolhido ou a Recolher")
@SequenceGenerator(name = "sq_obrigacaoicmsrecolher", sequenceName = "sq_obrigacaoicmsrecolher")
public class Obrigacaoicmsrecolher implements Log{
	
	protected Integer cdobrigacaoicmsrecolher;
	protected Empresa empresa;
	protected Uf uf;
	protected Codobrigacaoicmsrecolher codobrigacaoicmsrecolher;
	protected Money valorobrigacao;
	protected Date dtvencimento;
	protected String codigoreceita;
	protected String numeroprocesso;
	protected Origemprocessosped origemprocessosped;
	protected String processo;
	protected String textocomplementar;
	protected String mesreferencia;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_obrigacaoicmsrecolher")
	public Integer getCdobrigacaoicmsrecolher() {
		return cdobrigacaoicmsrecolher;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	@Required
	@DisplayName("C�digo do ICMS/ ICMS ST recolhido ou a recolher")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodobrigacaoicmsrecolher")
	public Codobrigacaoicmsrecolher getCodobrigacaoicmsrecolher() {
		return codobrigacaoicmsrecolher;
	}
	@Required
	@DisplayName("Valor de ICMS/ICMS ST a recolher")
	public Money getValorobrigacao() {
		return valorobrigacao;
	}
	@Required
	@DisplayName("Data do vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@Required
	@DisplayName("C�digo de receita")
	public String getCodigoreceita() {
		return codigoreceita;
	}
	@MaxLength(15)
	@DisplayName("N�mero de processo")
	public String getNumeroprocesso() {
		return numeroprocesso;
	}
	@DisplayName("Indicador da origem do processo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorigemprocessosped")
	public Origemprocessosped getOrigemprocessosped() {
		return origemprocessosped;
	}
	@MaxLength(255)
	public String getProcesso() {
		return processo;
	}
	@MaxLength(255)
	@DisplayName("Texto complementar")
	public String getTextocomplementar() {
		return textocomplementar;
	}
	@Required
	@DisplayName("M�s de refer�ncia")
	public String getMesreferencia() {
		return mesreferencia;
	}
	
	public void setCdobrigacaoicmsrecolher(Integer cdobrigacaoicmsrecolher) {
		this.cdobrigacaoicmsrecolher = cdobrigacaoicmsrecolher;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setCodobrigacaoicmsrecolher(
			Codobrigacaoicmsrecolher codobrigacaoicmsrecolher) {
		this.codobrigacaoicmsrecolher = codobrigacaoicmsrecolher;
	}
	public void setValorobrigacao(Money valorobrigacao) {
		this.valorobrigacao = valorobrigacao;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setCodigoreceita(String codigoreceita) {
		this.codigoreceita = codigoreceita;
	}
	public void setNumeroprocesso(String numeroprocesso) {
		this.numeroprocesso = numeroprocesso;
	}
	public void setOrigemprocessosped(Origemprocessosped origemprocessosped) {
		this.origemprocessosped = origemprocessosped;
	}
	public void setProcesso(String processo) {
		this.processo = processo;
	}
	public void setTextocomplementar(String textocomplementar) {
		this.textocomplementar = textocomplementar;
	}
	public void setMesreferencia(String mesreferencia) {
		this.mesreferencia = mesreferencia;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
