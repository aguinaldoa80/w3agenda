package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_materialtabelaprecoitem", sequenceName = "sq_materialtabelaprecoitem")
public class Materialtabelaprecoitem {

	protected Integer cdmaterialtabelaprecoitem;
	protected Materialtabelapreco materialtabelapreco;
	protected Material material;
	protected Unidademedida unidademedida;
	protected Double valor;
	protected Double valorvendaminimo;
	protected Double valorvendamaximo;
	protected String identificadorespecifico;
	protected Integer identificadorfabricante;
	protected Double percentualdesconto;
	protected Comissionamento comissionamento;
	protected Pedidovendamaterial pedidoVendaMaterial;
	protected Vendamaterial vendaMaterial;
	protected Vendaorcamentomaterial vendaOrcamentoMaterial;
	
	//TRANSIENT
	protected String cdmaterialtabelaprecoParam;
	protected Integer indexlista;
	protected Double valorcomtaxa;
	protected Double valorvendaminimocomtaxa;
	protected Double valorvendamaximocomtaxa;
	protected Double aux_valor;
	protected Double aux_valorvendaminimo;
	protected Double aux_valorvendamaximo;
	protected Unidademedida unidademedidaAntiga;
	protected Double fracao;
	protected Double qtdereferencia;
	protected Boolean atualizarValor;
	protected Integer qtdeTabelaEncontrada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtabelaprecoitem")
	public Integer getCdmaterialtabelaprecoitem() {
		return cdmaterialtabelaprecoitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DescriptionProperty
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Unidade de Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public Double getValor() {
		return valor;
	}
	@DisplayName("M�nimo")
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}
	@DisplayName("M�ximo")
	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}
	@MaxLength(30)
	@DisplayName("Identificador Espec�fico")	
	public String getIdentificadorespecifico() {
		return identificadorespecifico;
	}
	@MaxLength(6)
	@DisplayName("Identificador Fabricante")
	public Integer getIdentificadorfabricante() {
		return identificadorfabricante;
	}
	@DisplayName("% de Desconto")
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	@DisplayName("Comissionamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomissionamento")
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidoVendaMaterial() {
		return pedidoVendaMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendaMaterial() {
		return vendaMaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamentomaterial")
	public Vendaorcamentomaterial getVendaOrcamentoMaterial() {
		return vendaOrcamentoMaterial;
	}
	
	public void setCdmaterialtabelaprecoitem(Integer cdmaterialtabelaprecoitem) {
		this.cdmaterialtabelaprecoitem = cdmaterialtabelaprecoitem;
	}
	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}
	public void setIdentificadorespecifico(String identificadorespecifico) {
		this.identificadorespecifico = identificadorespecifico;
	}
	public void setIdentificadorfabricante(Integer identificadorfabricante) {
		this.identificadorfabricante = identificadorfabricante;
	}
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	public void setPedidoVendaMaterial(Pedidovendamaterial pedidoVendaMaterial) {
		this.pedidoVendaMaterial = pedidoVendaMaterial;
	}
	public void setVendaMaterial(Vendamaterial vendaMaterial) {
		this.vendaMaterial = vendaMaterial;
	}
	public void setVendaOrcamentoMaterial(
			Vendaorcamentomaterial vendaOrcamentoMaterial) {
		this.vendaOrcamentoMaterial = vendaOrcamentoMaterial;
	}
	
	@Transient
	public String getCdmaterialtabelaprecoParam() {
		return cdmaterialtabelaprecoParam;
	}
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	public void setCdmaterialtabelaprecoParam(String cdmaterialtabelaprecoParam) {
		this.cdmaterialtabelaprecoParam = cdmaterialtabelaprecoParam;
	}
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	
	@Transient
	public Double getValorvendamaximocomtaxa(Double valorvendamaximo) {
		Double valormaximo = this.getValorvendamaximo();
		if(this.materialtabelapreco != null && this.materialtabelapreco.getTaxaacrescimo() != null){
			if(valormaximo != null)
				valormaximo += (valormaximo * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
			else if(valorvendamaximo != null)
				valormaximo = valorvendamaximo + (valorvendamaximo * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
		}
		return valormaximo;
	}
	
	@Transient
	public Double getValorvendaminimocomtaxa(Double valorvendaminimo) {
		Double valorminimo = this.getValorvendaminimo();
		if(this.materialtabelapreco != null && this.materialtabelapreco.getTaxaacrescimo() != null){
			if(valorminimo != null)
				valorminimo += (valorminimo * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
			else if(valorvendaminimo != null)
				valorminimo = valorvendaminimo + (valorvendaminimo * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
		}
		return valorminimo;
	}
	
	@Transient
	public Double getValorcomtaxa(Double valorvenda, Integer numCasasDecimais) {
		Double valorTabelapreco = null;
		if(this.materialtabelapreco != null && this.materialtabelapreco.getTaxaacrescimo() != null){
			Double totalAcrescimo = 0.0;
			if(this.getValor() != null && Tipocalculo.EM_VALOR.equals(this.getMaterialtabelapreco().getTabelaprecotipo())){
				valorTabelapreco = 0.0;
				if(this.materialtabelapreco.getTaxaacrescimo() != null && 
						this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue() > 0){
					totalAcrescimo += (this.getValor() * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
				}
				
				if(this.materialtabelapreco.getDescontopadrao() != null){
					totalAcrescimo -= this.getValor()*this.materialtabelapreco.getDescontopadrao()/100;
				}
				if(this.materialtabelapreco.getAcrescimopadrao() != null){
					totalAcrescimo += this.getValor()*this.materialtabelapreco.getAcrescimopadrao()/100;
				}
				
				valorTabelapreco = this.getValor() + totalAcrescimo;
			}else if(valorvenda != null){
				if(this.getPercentualdesconto() != null && Tipocalculo.PERCENTUAL.equals(this.getMaterialtabelapreco().getTabelaprecotipo())){
					valorTabelapreco = 0.0;
					if(!Boolean.TRUE.equals(this.materialtabelapreco.getDiscriminardescontovenda())){
						totalAcrescimo -= valorvenda*this.getPercentualdesconto()/100;
					}
					if(this.materialtabelapreco.getTaxaacrescimo() != null && 
							this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue() > 0){
						totalAcrescimo += (valorvenda * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
					}
					
					if(this.materialtabelapreco.getAcrescimopadrao() != null){
						totalAcrescimo += valorvenda*this.materialtabelapreco.getAcrescimopadrao()/100;
					}
					
					valorTabelapreco = valorvenda + totalAcrescimo;
				}else {
					valor = valorvenda + (valorvenda * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
					
					valorTabelapreco = 0.0;
					if(this.materialtabelapreco.getTaxaacrescimo() != null && 
							this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue() > 0){
						totalAcrescimo += (valorvenda * this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue()/100);
					}
					
					if(this.materialtabelapreco.getDescontopadrao() != null && !Boolean.TRUE.equals(this.materialtabelapreco.getDiscriminardescontovenda()) && Tipocalculo.PERCENTUAL.equals(this.getMaterialtabelapreco().getTabelaprecotipo())){
						totalAcrescimo -= valorvenda*this.materialtabelapreco.getDescontopadrao()/100;
					}
					if(this.materialtabelapreco.getAcrescimopadrao() != null){
						totalAcrescimo += valorvenda*this.materialtabelapreco.getAcrescimopadrao()/100;
					}
					
					valorTabelapreco = valorvenda + totalAcrescimo;
				}
			}
		}
		if(numCasasDecimais != null && numCasasDecimais > 0){
			valorTabelapreco = SinedUtil.round(valorTabelapreco, numCasasDecimais);
		}
		return valorTabelapreco;
	}
	
	@Transient
	public Double getValorcomtaxa() {
		Double valor = this.getValor();
		if(this.materialtabelapreco != null && this.materialtabelapreco.getTaxaacrescimo() != null){
			if(valor != null)
				valor += (valor *  this.materialtabelapreco.getTaxaacrescimo().getValue().doubleValue());
		}
		return valor;
	}
	
	public void setValorcomtaxa(Double valorcomtaxa) {
		this.valorcomtaxa = valorcomtaxa;
	}
	
	@Transient
	public Double getAux_valor() {
		if(this.getValor() != null && this.getValor() > 0){
			return this.getValor();
		}else if(this.getMaterial() != null && this.getMaterial().getValorvenda() != null && this.getMaterial().getValorvenda() > 0){
			return this.getMaterial().getValorvenda();
		}
		return aux_valor;
	}
	@Transient
	public Double getAux_valorvendaminimo() {
		if(this.getValorvendaminimo() != null && this.getValorvendaminimo() > 0){
			return this.getValorvendaminimo();
		}else if(this.getMaterial() != null && this.getMaterial().getValorvendaminimo() != null && this.getMaterial().getValorvendaminimo() > 0){
			return this.getMaterial().getValorvendaminimo();
		}
		return aux_valorvendaminimo;
	}
	@Transient
	public Double getAux_valorvendamaximo() {
		if(this.getValorvendamaximo() != null && this.getValorvendamaximo() > 0){
			return this.getValorvendamaximo();
		}else if(this.getMaterial() != null && this.getMaterial().getValorvendamaximo() != null && this.getMaterial().getValorvendamaximo() > 0){
			return this.getMaterial().getValorvendamaximo();
		}
		return aux_valorvendamaximo;
	}
	public void setAux_valor(Double auxValor) {
		aux_valor = auxValor;
	}
	public void setAux_valorvendaminimo(Double auxValorvendaminimo) {
		aux_valorvendaminimo = auxValorvendaminimo;
	}
	public void setAux_valorvendamaximo(Double auxValorvendamaximo) {
		aux_valorvendamaximo = auxValorvendamaximo;
	}

	@Transient
	public Unidademedida getUnidademedidaAntiga() {
		return unidademedidaAntiga;
	}
	public void setUnidademedidaAntiga(Unidademedida unidademedidaAntiga) {
		this.unidademedidaAntiga = unidademedidaAntiga;
	}
	
	@Transient
	public boolean isUnidadesecundaria(){
		boolean retorno = true;
		if(this.getUnidademedida() == null || this.getUnidademedida().getCdunidademedida() == null){
			retorno =  false;
		}else if(this.getMaterial() != null && this.getMaterial().getUnidademedida() != null && this.getUnidademedida().equals(this.getMaterial().getUnidademedida())){
			retorno = false;
		}
		
		return retorno;
	}
	@Transient
	public Double getFracao() {
		return fracao;
	}
	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
	@Transient
	public Money getPercentualDescontoDiscriminado(){
		if(getMaterialtabelapreco() != null && Boolean.TRUE.equals(getMaterialtabelapreco().getDiscriminardescontovenda())){
			return getPercentualdesconto() != null && Tipocalculo.PERCENTUAL.equals(getMaterialtabelapreco().getTabelaprecotipo()) ? new Money(getPercentualdesconto()) : getMaterialtabelapreco().getDescontopadrao() != null ? new Money(getMaterialtabelapreco().getDescontopadrao()) : null;
		}
		
		return null;
	}
	
	@Transient
	public Boolean getAtualizarValor() {
		return atualizarValor;
	}
	public void setAtualizarValor(Boolean atualizarValor) {
		this.atualizarValor = atualizarValor;
	}

	@Transient
	public Integer getQtdeTabelaEncontrada() {
		return qtdeTabelaEncontrada;
	}
	public void setQtdeTabelaEncontrada(Integer qtdeTabelaEncontrada) {
		this.qtdeTabelaEncontrada = qtdeTabelaEncontrada;
	}
	
	@Transient
	public Comissionamento getComissionamentoItemOuComissionamentoTabela(Comissionamentotipo comissionamentotipo){
		if(getComissionamento() != null && (comissionamentotipo == null || comissionamentotipo.equals(getComissionamento().getComissionamentotipo()))) return getComissionamento();
		if(getMaterialtabelapreco() != null && 
				getMaterialtabelapreco().getComissionamento() != null &&
				(comissionamentotipo == null ||
				comissionamentotipo.equals(getMaterialtabelapreco().getComissionamento().getComissionamentotipo()))) return getMaterialtabelapreco().getComissionamento();
		return null;
	}
}
