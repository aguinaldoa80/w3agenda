package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.ArquivoConfiguravelRetornoDocumentoBean;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivoconfiguravelretornodocumento", sequenceName = "sq_arquivoconfiguravelretornodocumento")
public class Arquivoconfiguravelretornodocumento implements Log {

	private Integer cdarquivoconfiguravelretornodocumento;
	private String nomearquivo;
	private Integer sequencial;
	private String sequencialArquivo;	
	private BancoConfiguracaoRetornoAcaoEnum acao;
	private String codocorrencia;
	private String ocorrencia;
	private String nossonumero;
	private String usoempresa;
	private Date dtvencimento;
	private Date dtpagamento;
	private Date dtocorrencia;
	private Date dtcredito;
	private Money valor;
	private Money valorpago;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	public Arquivoconfiguravelretornodocumento() {}
	
	public Arquivoconfiguravelretornodocumento(String nomearquivo, ArquivoConfiguravelRetornoDocumentoBean outro){
		this.nomearquivo = nomearquivo;
		this.sequencial = outro.getSequencial();
		this.sequencialArquivo = outro.getSequencialArquivo();
		this.acao = outro.getAcao();
		this.codocorrencia = outro.getCodocorrencia();
		this.ocorrencia = outro.getOcorrencia();
		this.nossonumero = outro.getNossonumero();
		this.usoempresa = outro.getUsoempresa();
		this.dtvencimento = outro.getDtvencimento();
		this.dtpagamento = outro.getDtpagamento();
		this.dtocorrencia = outro.getDtocorrencia();
		this.dtcredito = outro.getDtcredito();
		this.valor = outro.getValor();
		this.valorpago = outro.getValorpago();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivoconfiguravelretornodocumento")
	public Integer getCdarquivoconfiguravelretornodocumento() {
		return cdarquivoconfiguravelretornodocumento;
	}
	
	public String getNomearquivo() {
		return nomearquivo;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public String getSequencialArquivo() {
		return sequencialArquivo;
	}

	public BancoConfiguracaoRetornoAcaoEnum getAcao() {
		return acao;
	}

	public String getCodocorrencia() {
		return codocorrencia;
	}

	public String getOcorrencia() {
		return ocorrencia;
	}

	public String getNossonumero() {
		return nossonumero;
	}

	public String getUsoempresa() {
		return usoempresa;
	}

	public Date getDtvencimento() {
		return dtvencimento;
	}

	public Date getDtpagamento() {
		return dtpagamento;
	}

	public Date getDtocorrencia() {
		return dtocorrencia;
	}

	public Date getDtcredito() {
		return dtcredito;
	}

	public Money getValor() {
		return valor;
	}

	public Money getValorpago() {
		return valorpago;
	}

	public void setCdarquivoconfiguravelretornodocumento(Integer cdarquivoconfiguravelretornodocumento) {
		this.cdarquivoconfiguravelretornodocumento = cdarquivoconfiguravelretornodocumento;
	}
	
	public void setNomearquivo(String nomearquivo) {
		this.nomearquivo = nomearquivo;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setSequencialArquivo(String sequencialArquivo) {
		this.sequencialArquivo = sequencialArquivo;
	}

	public void setAcao(BancoConfiguracaoRetornoAcaoEnum acao) {
		this.acao = acao;
	}

	public void setCodocorrencia(String codocorrencia) {
		this.codocorrencia = codocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}

	public void setUsoempresa(String usoempresa) {
		this.usoempresa = usoempresa;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}

	public void setDtocorrencia(Date dtocorrencia) {
		this.dtocorrencia = dtocorrencia;
	}

	public void setDtcredito(Date dtcredito) {
		this.dtcredito = dtcredito;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValorpago(Money valorpago) {
		this.valorpago = valorpago;
	}
	
	//LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}