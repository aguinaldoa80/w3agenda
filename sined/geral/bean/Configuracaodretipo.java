package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Configuracaodreoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Configuracaodreseparacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_configuracaodretipo", sequenceName = "sq_configuracaodretipo")
@DisplayName("Tipo de item na DRE")
public class Configuracaodretipo implements Log {
	
	protected Integer cdconfiguracaodretipo; 
	protected String descricao;
	protected Configuracaodreoperacao configuracaodreoperacao;
	protected Configuracaodreseparacao configuracaodreseparacao;
	protected Integer ordem;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Configuracaodretipo() {
	}
	
	public Configuracaodretipo(Integer cdconfiguracaodretipo) {
		this.cdconfiguracaodretipo = cdconfiguracaodretipo;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaodretipo")
	public Integer getCdconfiguracaodretipo() {
		return cdconfiguracaodretipo;
	}

	@Required
	@DisplayName("Descri��o")
	@MaxLength(200)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Separa��o")
	public Configuracaodreseparacao getConfiguracaodreseparacao() {
		return configuracaodreseparacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	@DisplayName("Opera��o")
	public Configuracaodreoperacao getConfiguracaodreoperacao() {
		return configuracaodreoperacao;
	}

	public void setConfiguracaodreoperacao(
			Configuracaodreoperacao configuracaodreoperacao) {
		this.configuracaodreoperacao = configuracaodreoperacao;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setCdconfiguracaodretipo(Integer cdconfiguracaodretipo) {
		this.cdconfiguracaodretipo = cdconfiguracaodretipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setConfiguracaodreseparacao(
			Configuracaodreseparacao configuracaodreseparacao) {
		this.configuracaodreseparacao = configuracaodreseparacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdconfiguracaodretipo == null) ? 0 : cdconfiguracaodretipo
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuracaodretipo other = (Configuracaodretipo) obj;
		if (cdconfiguracaodretipo == null) {
			if (other.cdconfiguracaodretipo != null)
				return false;
		} else if (!cdconfiguracaodretipo.equals(other.cdconfiguracaodretipo))
			return false;
		return true;
	}
	
}
