package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.beanutils.BeanComparator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_cheque", sequenceName="sq_cheque")
public class Cheque {

	protected Integer cdcheque;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected String numero;
	protected String emitente;
	protected Date dtbompara;
	protected String cpfcnpj;
	protected Empresa empresa;
	protected Boolean proprio;
	protected Conta contabancaria;
	protected Boolean protocoloretiradachequeemitida;
	
	protected List<Movimentacao> listaMovimentacao;
	protected List<Documento> listaDocumento;
	protected List<Vendapagamento> listaVendapagamento;
	protected List<Chequehistorico> listaChequehistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Money valor;
	protected String linhanumerica;
	protected Chequesituacao chequesituacao;
	protected String observacao;
	protected Boolean resgatado;
	protected Boolean isPopup = Boolean.FALSE;
	protected Boolean utilizadoBaixaContapagar;
	protected String whereIn;
	protected String whereInMovimentacao;
	protected Chequedevolucaomotivo chequedevolucaomotivo;
	protected Money valorTotal;
	protected Integer quantidadeNovocheque;
	protected List<Cheque> listaChequesubstituto;
	protected List<Movimentacao> listaMovimentacaoSubstituicao;
	protected Movimentacao movimentacaoNovo;
	protected Boolean gerarcontareceber;
	protected Boolean gerarcontapagar;
	protected String justificativahistorico;
	private String recebidode;
	private String pagoa;
	
	private List<Documento> listaDocumentoNaoBaixado;
	
	public Cheque(){}
	
	public Cheque(Integer cdcheque){
		this.cdcheque = cdcheque;
	}
	
	public Cheque(String numero){
		this.numero = numero;
	}
	
	@Transient
	public String getDescricaoAutocomplete(){
		StringBuilder s = new StringBuilder();
		
		s.append(numero).append("<span><BR>");
		
		if(banco != null) s.append("Banco: ").append(banco).append("<BR>");
		if(agencia != null) s.append("Ag�ncia: ").append(agencia).append("<BR>");
		if(conta != null) s.append("Conta: ").append(conta).append("<BR>");
		if(dtbompara != null) s.append("Bom para: ").append(SinedDateUtils.toString(dtbompara)).append("<BR>");
		if(valor != null) s.append("Valor: ").append(valor.toString()).append("<BR>");
		
		s.append("</span>");
		
		return s.toString();
	}
	
	@Transient
	public String getDescricaoAutocompleteGerarMovimentacao(){
		StringBuilder s = new StringBuilder();
		
		s.append(numero);
		
		if(dtbompara != null) s.append(" - Bom para: ").append(SinedDateUtils.toString(dtbompara));
		
		s.append("<span><BR>");
		
		if(banco != null) s.append("Banco: ").append(banco).append("<BR>");
		if(agencia != null) s.append("Ag�ncia: ").append(agencia).append("<BR>");
		if(conta != null) s.append("Conta: ").append(conta).append("<BR>");
//		if(dtbompara != null) s.append("Bom para: ").append(SinedDateUtils.toString(dtbompara)).append("<BR>");
		if(emitente != null && !"".equals(emitente)) s.append("Emitente: ").append(emitente).append("<BR>");
		if(valor != null) s.append("Valor: ").append("R$ " + valor);
		
		s.append("</span>");
		
		return s.toString();
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cheque")
	public Integer getCdcheque() {
		return cdcheque;
	}
	@MaxLength(9)
	@DisplayName("Banco")
	public Integer getBanco() {
		return banco;
	}
	@MaxLength(9)
	@DisplayName("Ag�ncia")
	public Integer getAgencia() {
		return agencia;
	}
	@MaxLength(9)
	@DisplayName("Conta")
	public Integer getConta() {
		return conta;
	}
	@DescriptionProperty
	@Required
	@DisplayName("N�mero")
	@MaxLength(100)
	public String getNumero() {
		return numero;
	}
	@DisplayName("Emitente")
	public String getEmitente() {
		return emitente;
	}
	@DisplayName("Bom para")
	public Date getDtbompara() {
		return dtbompara;
	}
	@DisplayName("CNPJ/CPF do emitente")
	@MaxLength(14)
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	@OneToMany(mappedBy="cheque")
	public List<Movimentacao> getListaMovimentacao() {
		return listaMovimentacao;
	}
	@OneToMany(mappedBy="cheque")
	public List<Documento> getListaDocumento() {
		return listaDocumento;
	}
	@OneToMany(mappedBy="cheque")
	public List<Vendapagamento> getListaVendapagamento() {
		return listaVendapagamento;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="cheque")	
	public List<Chequehistorico> getListaChequehistorico() {
		return listaChequehistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Cheque pr�prio")
	public Boolean getProprio() {
		return proprio;
	}
	@DisplayName("Conta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontabancaria")	
	public Conta getContabancaria() {
		return contabancaria;
	}

	public void setCdcheque(Integer cdcheque) {
		this.cdcheque = cdcheque;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	public void setDtbompara(Date dtbompara) {
		this.dtbompara = dtbompara;
	}
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}
	public void setProprio(Boolean proprio) {
		this.proprio = proprio;
	}
	public void setContabancaria(Conta contabancaria) {
		this.contabancaria = contabancaria;
	}

	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaMovimentacao(List<Movimentacao> listaMovimentacao) {
		this.listaMovimentacao = listaMovimentacao;
	}
	public void setListaDocumento(List<Documento> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	public void setListaVendapagamento(List<Vendapagamento> listaVendapagamento) {
		this.listaVendapagamento = listaVendapagamento;
	}
	public void setListaChequehistorico(List<Chequehistorico> listaChequehistorico) {
		this.listaChequehistorico = listaChequehistorico;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public Money getValor() {
		return valor;
	}
	public Boolean getResgatado() {
		return resgatado;
	}
	public void setResgatado(Boolean resgatado) {
		this.resgatado = resgatado;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public String getAgenciacontaTrans() {
		String agenciaconta = "";
		
		if(this.getAgencia() != null){
			agenciaconta = this.getAgencia().toString();
		}
		if(!"".equals(agenciaconta) && this.getConta() != null)
			agenciaconta += " / ";
		
		if(this.getConta() != null){
			agenciaconta += this.getConta().toString();
		}
		return agenciaconta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcheque == null) ? 0 : cdcheque.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cheque other = (Cheque) obj;
		if (cdcheque == null) {
			if (other.cdcheque != null)
				return false;
		} else if (!cdcheque.equals(other.cdcheque))
			return false;
		return true;
	}
	
	@Transient
	public Boolean getIsPopup() {
		return isPopup;
	}
	
	public void setIsPopup(Boolean isPopup) {
		this.isPopup = isPopup;
	}

	@DisplayName("Linha num�rica")
	@MaxLength(50)
	public String getLinhanumerica() {
		return linhanumerica;
	}
	@DisplayName("Situa��o")
	public Chequesituacao getChequesituacao() {
		return chequesituacao;
	}
	@DisplayName("Observa��o (Hist�rico)")
	public String getObservacao() {
		return observacao;
	}

	public void setLinhanumerica(String linhanumerica) {
		this.linhanumerica = linhanumerica;
	}
	public void setChequesituacao(Chequesituacao chequesituacao) {
		this.chequesituacao = chequesituacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Transient
	public Boolean getUtilizadoBaixaContapagar() {
		return utilizadoBaixaContapagar;
	}
	@Transient
	@DisplayName("Gerar nova conta a receber")
	public Boolean getGerarcontareceber() {
		return gerarcontareceber;
	}
	@Transient
	@DisplayName("Gerar nova conta a pagar")
	public Boolean getGerarcontapagar() {
		return gerarcontapagar;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	@Transient
	@DisplayName("Motivo")
	public Chequedevolucaomotivo getChequedevolucaomotivo() {
		return chequedevolucaomotivo;
	}

	public void setUtilizadoBaixaContapagar(Boolean utilizadoBaixaContapagar) {
		this.utilizadoBaixaContapagar = utilizadoBaixaContapagar;
	}
	public void setGerarcontareceber(Boolean gerarcontareceber) {
		this.gerarcontareceber = gerarcontareceber;
	}
	public void setGerarcontapagar(Boolean gerarcontapagar) {
		this.gerarcontapagar = gerarcontapagar;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setChequedevolucaomotivo(Chequedevolucaomotivo chequedevolucaomotivo) {
		this.chequedevolucaomotivo = chequedevolucaomotivo;
	}
	
	@Transient
	public Money getValorTotal() {
		return valorTotal;
	}
	@Transient
	@DisplayName("Quantidade de Novos cheques")
	@MaxLength(4)
	public Integer getQuantidadeNovocheque() {
		return quantidadeNovocheque;
	}
	@Transient
	public List<Cheque> getListaChequesubstituto() {
		return listaChequesubstituto;
	}

	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	public void setQuantidadeNovocheque(Integer quantidadeNovocheque) {
		this.quantidadeNovocheque = quantidadeNovocheque;
	}
	public void setListaChequesubstituto(List<Cheque> listaChequesubstituto) {
		this.listaChequesubstituto = listaChequesubstituto;
	}

	@Transient
	public List<Movimentacao> getListaMovimentacaoSubstituicao() {
		return listaMovimentacaoSubstituicao;
	}
	public void setListaMovimentacaoSubstituicao(List<Movimentacao> listaMovimentacaoSubstituicao) {
		this.listaMovimentacaoSubstituicao = listaMovimentacaoSubstituicao;
	}

	@Transient
	public Movimentacao getMovimentacaoNovo() {
		return movimentacaoNovo;
	}
	public void setMovimentacaoNovo(Movimentacao movimentacaoNovo) {
		this.movimentacaoNovo = movimentacaoNovo;
	}

	@Transient
	public String getWhereInMovimentacao() {
		return whereInMovimentacao;
	}
	public void setWhereInMovimentacao(String whereInMovimentacao) {
		this.whereInMovimentacao = whereInMovimentacao;
	}

	@Transient
	public String getJustificativahistorico() {
		return justificativahistorico;
	}

	public void setJustificativahistorico(String justificativahistorico) {
		this.justificativahistorico = justificativahistorico;
	}

	@Transient
	@DisplayName("Recebido de")
	public String getRecebidode() {
		return recebidode;
	}
	@Transient
	@DisplayName("Pago a")
	public String getPagoa() {
		return pagoa;
	}

	public void setRecebidode(String recebidode) {
		this.recebidode = recebidode;
	}
	public void setPagoa(String pagoa) {
		this.pagoa = pagoa;
	}
	
	@Transient
	public String getDescricaoCompleta(){
		StringBuilder s = new StringBuilder();
		final String divisor = " / ";
		
		if (numero!=null && !numero.trim().equals("")){
			s.append("N�mero: ").append(numero).append(divisor);
		}
		
		if (dtbompara!=null){
			s.append(" - Bom para: ").append(SinedDateUtils.toString(dtbompara)).append(divisor);
		}
		
		if (banco!=null){
			s.append("Banco: ").append(banco).append(divisor);
		}
		
		if (agencia!=null){
			s.append("Ag�ncia: ").append(agencia).append(divisor);
		}
		
		if (conta!=null){
			s.append("Conta: ").append(conta).append(divisor);
		}
		
		if (emitente!=null && !emitente.trim().equals("")){
			s.append("Emitente: ").append(emitente).append(divisor);
		}
		
		if (valor!=null){
			s.append("Valor: ").append("R$ " + valor);
		}
		
		return s.toString();
	}
	
	@Transient
	public List<Documento> getListaDocumentoNaoBaixado() {
		if(this.listaDocumentoNaoBaixado == null){
			this.listaDocumentoNaoBaixado = new ArrayList<Documento>();
		}
		return listaDocumentoNaoBaixado;
	}
	public void setListaDocumentoNaoBaixado(
			List<Documento> listaDocumentoNaoBaixado) {
		this.listaDocumentoNaoBaixado = listaDocumentoNaoBaixado;
	}
	
	@Column(insertable=false, updatable=false)
	public Boolean getProtocoloretiradachequeemitida() {
		return protocoloretiradachequeemitida;
	}
	
	public void setProtocoloretiradachequeemitida(Boolean protocoloretiradachequeemitida) {
		this.protocoloretiradachequeemitida = protocoloretiradachequeemitida;
	}
	
	@Transient
	@SuppressWarnings("unchecked")
	public Chequedevolucaomotivo getUltimoChequedevolucaomotivo(){
		if (SinedUtil.isListNotEmpty(listaChequehistorico)){
			List<Chequehistorico> listaChequehistoricoClone = (List<Chequehistorico>) new ArrayList<Chequehistorico>(listaChequehistorico).clone();
			Collections.sort(listaChequehistoricoClone, new BeanComparator("dtaltera"));
			Collections.reverse(listaChequehistoricoClone);
			return listaChequehistoricoClone.get(0).getChequedevolucaomotivo();
		}
		
		return null;
	}
	
	@Transient
	@SuppressWarnings("unchecked")
	public Movimentacao getPrimeiraMovimentacao(){
		if (SinedUtil.isListNotEmpty(listaMovimentacao)){
			List<Movimentacao> listaMovimentacaoClone = (List<Movimentacao>) new ArrayList<Movimentacao>(listaMovimentacao).clone();
			Collections.sort(listaMovimentacaoClone, new BeanComparator("cdmovimentacao"));
			return listaMovimentacaoClone.get(0);
		}
		
		return null;
	}
}