package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;


@Entity
@DisplayName("Tarefa")
@SequenceGenerator(name="sq_tarefaorcamento",sequenceName="sq_tarefaorcamento")
public class Tarefaorcamento implements Log {

	protected Integer cdtarefaorcamento;
	protected Orcamento orcamento;
	protected Indice indice;
	protected String descricao;
	protected Double qtde;
	protected Tarefaorcamento tarefapai;
	protected Integer duracao;
	protected Date dtinicio;
	protected Integer id;
	protected String predecessoras;
	protected Unidademedida unidademedida;
	
	protected Double custototal;
	protected Double percentualacrescimo;
	protected Double custoacrescido;
	
	protected List<Tarefaorcamentorh> listaTarefaorcamentorh = new ListSet<Tarefaorcamentorh>(Tarefaorcamentorh.class);
	protected List<Tarefaorcamentorg> listaTarefaorcamentorg = new ListSet<Tarefaorcamentorg>(Tarefaorcamentorg.class);	
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Tarefaorcamento() {
	}
	
	public Tarefaorcamento(Integer cdtarefaorcamento) {
		this.cdtarefaorcamento = cdtarefaorcamento;
	}
	
	@Id
	@GeneratedValue(generator="sq_tarefaorcamento", strategy=GenerationType.AUTO)	
	public Integer getCdtarefaorcamento() {
		return cdtarefaorcamento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	@DisplayName("Or�amento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	@DisplayName("Composi��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindice")	
	public Indice getIndice() {
		return indice;
	}

	@Required
	@DisplayName("Descri��o")
	@MaxLength(300)
	@DescriptionProperty	
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Quantidade")	
	public Double getQtde() {
		return qtde;
	}

	@DisplayName("Tarefa pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefapai")	
	public Tarefaorcamento getTarefapai() {
		return tarefapai;
	}

	public Integer getDuracao() {
		return duracao;
	}

	public Date getDtinicio() {
		return dtinicio;
	}

	public Integer getId() {
		return id;
	}

	@MaxLength(50)
	public String getPredecessoras() {
		return predecessoras;
	}
	
	@DisplayName("Custo total")
	public Double getCustototal() {
		return custototal;
	}
	
	public Double getPercentualacrescimo() {
		return percentualacrescimo;
	}
	
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")	
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setPercentualacrescimo(Double percentualacrescimo) {
		this.percentualacrescimo = percentualacrescimo;
	}

	public Double getCustoacrescido() {
		return custoacrescido;
	}

	public void setCustoacrescido(Double custoacrescido) {
		this.custoacrescido = custoacrescido;
	}
	
	public void setCustototal(Double custototal) {
		this.custototal = custototal;
	}

	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPredecessoras(String predecessoras) {
		this.predecessoras = predecessoras;
	}

	public void setCdtarefaorcamento(Integer cdtarefaorcamento) {
		this.cdtarefaorcamento = cdtarefaorcamento;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public void setIndice(Indice indice) {
		this.indice = indice;
	}

	// N�O COLOCAR O M�TODO QUE FAZ TRIM NA STRING EM DECRI��O
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setTarefapai(Tarefaorcamento tarefapai) {
		this.tarefapai = tarefapai;
	}
	
	/*************/
	/**** Log ****/
	/*************/
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	/************/
	/** Listas **/
	/************/
	
	@OneToMany(mappedBy="tarefaorcamento")
	@DisplayName("Recursos Gerais")
	public List<Tarefaorcamentorg> getListaTarefaorcamentorg() {
		return listaTarefaorcamentorg;
	}
	
	@OneToMany(mappedBy="tarefaorcamento")
	@DisplayName("Recursos Humanos")
	public List<Tarefaorcamentorh> getListaTarefaorcamentorh() {
		return listaTarefaorcamentorh;
	}
	
	public void setListaTarefaorcamentorg(List<Tarefaorcamentorg> listaTarefaorcamentorg) {
		this.listaTarefaorcamentorg = listaTarefaorcamentorg;
	}
	
	public void setListaTarefaorcamentorh(List<Tarefaorcamentorh> listaTarefaorcamentorh) {
		this.listaTarefaorcamentorh = listaTarefaorcamentorh;
	}
	
	/******************/
	/** Transientes **/
	/****************/
	protected Double idtarefapai;
	protected Date dtfim;
	protected List<Tarefaorcamento> listaTarefaFilha;
	protected Money precoacrescido;
	protected Money precovenda;
	protected Money precocusto;
	protected Money precomo;
	protected Money precomat;
	protected Integer nivelEstrutura = 1;
	protected Double custounitario;
	protected String identificador;
	protected String espacoVazio = "";
	
	@Transient
	public Double getCustounitario() {		
		try{
			return getCustototal() / getQtde();
		} catch (Exception e) {
			return custounitario;
		}
	}
	
	public void setCustounitario(Double custounitario) {
		this.custounitario = custounitario;
	}
	
	@Transient
	public Integer getNivelEstrutura() {
		return nivelEstrutura;
	}
	
	public void setNivelEstrutura(Integer nivelEstrutura) {
		this.nivelEstrutura = nivelEstrutura;
	}
	
	@Transient
	public String getEspacoVazio() {
		return espacoVazio;
	}
	
	public void setEspacoVazio(String espacoVazio) {
		this.espacoVazio = espacoVazio;
	}
	
	@Transient
	public Money getPrecoacrescido() {
		return precoacrescido;
	}

	public void setPrecoacrescido(Money precoacrescido) {
		this.precoacrescido = precoacrescido;
	}

	@Transient
	public Money getPrecomo() {
		return precomo;
	}

	public void setPrecomo(Money precomo) {
		this.precomo = precomo;
	}
	
	@Transient
	public Money getPrecomat() {
		return precomat;
	}

	public void setPrecomat(Money precomat) {
		this.precomat = precomat;
	}

	@Transient
	public Date getDtfim() {
		if(dtfim == null && dtinicio != null && duracao != null){
			Calendario calendario = null;
			if(getOrcamento() != null){
				calendario = Neo.getObject(CalendarioService.class).getByOrcamento(getOrcamento());
			}
			dtfim = SinedDateUtils.incrementaDia(dtinicio, duracao-1, calendario);
		}
		return dtfim;
	}
	
	@Transient
	public List<Tarefaorcamento> getListaTarefaFilha() {
		return listaTarefaFilha;
	}
	
	@Transient
	public Double getIdtarefapai() {
		return idtarefapai;
	}
	
	@Transient
	public Money getPrecocusto() {
		return precocusto;
	}
	
	@Transient
	public Money getPrecovenda() {
		return precovenda;
	}
	
	@Transient
	public String getIdentificador() {
		return identificador;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setPrecocusto(Money precocusto) {
		this.precocusto = precocusto;
	}
	
	public void setPrecovenda(Money precovenda) {
		this.precovenda = precovenda;
	}
	
	public void setIdtarefapai(Double idtarefapai) {
		this.idtarefapai = idtarefapai;
	}
	
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
	public void setListaTarefaFilha(List<Tarefaorcamento> listaTarefaFilha) {
		this.listaTarefaFilha = listaTarefaFilha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdtarefaorcamento == null) ? 0 : cdtarefaorcamento
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Tarefaorcamento other = (Tarefaorcamento) obj;
		if (cdtarefaorcamento == null) {
			if (other.cdtarefaorcamento != null)
				return false;
		} else if (!cdtarefaorcamento.equals(other.cdtarefaorcamento))
			return false;
		return true;
	}

}
