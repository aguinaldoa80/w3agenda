package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.GnreCodigoReceitaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreSituacao;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("GNRE")
@SequenceGenerator(name = "sq_gnre", sequenceName = "sq_gnre")
public class Gnre implements Log {

	protected Integer cdgnre;
	protected Configuracaognre configuracaognre;
	protected Uf uf;
	protected GnreCodigoReceitaEnum codigoreceita;
	protected String codigodetalhamento;
	protected Money valor;
	protected Money valorfcp;
	protected Date dtvencimento;
	protected Date dtpagamento;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Municipio municipioCliente;
	protected Nota nota;
	protected String numerodocumento;
	protected String chaveacesso;
	protected Date dtemissaodocumento;
	protected Date dtsaidadocumento;
	protected String infocomplementardocumento;
	protected GnreSituacao situacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Gnrehistorico> listaGnrehistorico = new ListSet<Gnrehistorico>(Gnrehistorico.class);
	
	public Gnre() {
	}
	
	public Gnre(Integer cdgnre) {
		this.cdgnre = cdgnre;
	}


	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_gnre")
	public Integer getCdgnre() {
		return cdgnre;
	}

	@Required
	@DisplayName("UF favorecida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}

	@Required
	@DisplayName("C�digo da receita")	
	public GnreCodigoReceitaEnum getCodigoreceita() {
		return codigoreceita;
	}

	@DisplayName("Detalhamento da receita")	
	public String getCodigodetalhamento() {
		return codigodetalhamento;
	}

	@Required
	public Money getValor() {
		return valor;
	}

	@DisplayName("Valor FCP")	
	public Money getValorfcp() {
		return valorfcp;
	}

	@Required
	@DisplayName("Data de vencimento")	
	public Date getDtvencimento() {
		return dtvencimento;
	}

	@Required
	@DisplayName("Data de pagamento")	
	public Date getDtpagamento() {
		return dtpagamento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}

	@DisplayName("N�mero")
	public String getNumerodocumento() {
		return numerodocumento;
	}

	@DisplayName("Chave de acesso")
	public String getChaveacesso() {
		return chaveacesso;
	}

	@DisplayName("Data de emiss�o")
	public Date getDtemissaodocumento() {
		return dtemissaodocumento;
	}
	
	@DisplayName("Situa��o")
	public GnreSituacao getSituacao() {
		return situacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaognre")
	public Configuracaognre getConfiguracaognre() {
		return configuracaognre;
	}
	
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipiocliente")
	public Municipio getMunicipioCliente() {
		return municipioCliente;
	}
	
	@DisplayName("Data de sa�da")
	public Date getDtsaidadocumento() {
		return dtsaidadocumento;
	}

	@DisplayName("Informa��es complementar")
	public String getInfocomplementardocumento() {
		return infocomplementardocumento;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="gnre", fetch=FetchType.LAZY)
	public List<Gnrehistorico> getListaGnrehistorico() {
		return listaGnrehistorico;
	}
	
	public void setListaGnrehistorico(List<Gnrehistorico> listaGnrehistorico) {
		this.listaGnrehistorico = listaGnrehistorico;
	}

	public void setDtsaidadocumento(Date dtsaidadocumento) {
		this.dtsaidadocumento = dtsaidadocumento;
	}

	public void setInfocomplementardocumento(String infocomplementardocumento) {
		this.infocomplementardocumento = infocomplementardocumento;
	}

	public void setMunicipioCliente(Municipio municipioCliente) {
		this.municipioCliente = municipioCliente;
	}
	
	public void setConfiguracaognre(Configuracaognre configuracaognre) {
		this.configuracaognre = configuracaognre;
	}
	
	public void setSituacao(GnreSituacao situacao) {
		this.situacao = situacao;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}

	public void setDtemissaodocumento(Date dtemissaodocumento) {
		this.dtemissaodocumento = dtemissaodocumento;
	}

	public void setCdgnre(Integer cdgnre) {
		this.cdgnre = cdgnre;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setCodigoreceita(GnreCodigoReceitaEnum codigoreceita) {
		this.codigoreceita = codigoreceita;
	}

	public void setCodigodetalhamento(String codigodetalhamento) {
		this.codigodetalhamento = codigodetalhamento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValorfcp(Money valorfcp) {
		this.valorfcp = valorfcp;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setDtpagamento(Date dtpagamento) {
		this.dtpagamento = dtpagamento;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
