package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_menumodulo", sequenceName = "sq_menumodulo")
public class Menumodulo implements Log {

	protected Integer cdmenumodulo;
	protected String nome;
	protected String urlprefixo;
	protected Integer ordem;
	protected String eventoclick;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String contexto;
	protected Boolean dashboard;
	
	public Menumodulo() {
	}
	
	public Menumodulo(Integer cdmenumodulo) {
		this.cdmenumodulo = cdmenumodulo;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_menumodulo")
	public Integer getCdmenumodulo() {
		return cdmenumodulo;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(500)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	@MaxLength(50)
	@DisplayName("Prefixo de URL")
	public String getUrlprefixo() {
		return urlprefixo;
	}

	public Integer getOrdem() {
		return ordem;
	}

	@MaxLength(500)
	@DisplayName("Evento onClick")
	public String getEventoclick() {
		return eventoclick;
	}
	
	@MaxLength(50)
	@DisplayName("Contexto")
	public String getContexto() {
		return contexto;
	}
	
	public Boolean getDashboard() {
		return dashboard;
	}
	
	public void setDashboard(Boolean dashboard) {
		this.dashboard = dashboard;
	}
	
	public void setContexto(String contexto) {
		this.contexto = contexto;
	}

	public void setUrlprefixo(String urlprefixo) {
		this.urlprefixo = urlprefixo;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setEventoclick(String eventoclick) {
		this.eventoclick = eventoclick;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCdmenumodulo(
			Integer cdmenumodulo) {
		this.cdmenumodulo = cdmenumodulo;
	}
	
	
//	LOG

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
//	TRANSIENTE
	
	@Transient
	public String getUrlprefixoPermissao(){
		return "/" + this.urlprefixo + "/";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmenumodulo == null) ? 0 : cdmenumodulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menumodulo other = (Menumodulo) obj;
		if (cdmenumodulo == null) {
			if (other.cdmenumodulo != null)
				return false;
		} else if (!cdmenumodulo.equals(other.cdmenumodulo))
			return false;
		return true;
	}
	
}
