package br.com.linkcom.sined.geral.bean;

public enum Tipopessoacrm {

	CLIENTE(0, "Cliente"),
	CONTA(1, "Conta");
	
	private Integer value;
	private String tipo;
	
	private Tipopessoacrm(Integer value, String tipo){
		this.value = value;
		this.tipo = tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	public String getTipo() {
		return tipo;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
}
