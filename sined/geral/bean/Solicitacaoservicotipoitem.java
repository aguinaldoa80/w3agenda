package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_solicitacaoservicotipoitem",sequenceName="sq_solicitacaoservicotipoitem")
public class Solicitacaoservicotipoitem implements Log{
	
	protected Integer cdsolicitacaoservicotipoitem;
	protected Solicitacaoservicotipo solicitacaoservicotipo;
	protected String nome;
	protected Boolean obrigatorio=false;
	
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@DisplayName("N�mero")
	@Id
	@GeneratedValue(generator="sq_solicitacaoservicotipoitem",strategy=GenerationType.AUTO)
	public Integer getCdsolicitacaoservicotipoitem() {
		return cdsolicitacaoservicotipoitem;
	}
	
	@Required
	@DisplayName("Solicita��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaoservicotipo")
	public Solicitacaoservicotipo getSolicitacaoservicotipo() {
		return solicitacaoservicotipo;
	}
	
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Obrigat�rio")
	@Required
	public Boolean getObrigatorio() {
		return obrigatorio;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdsolicitacaoservicotipoitem(Integer cdsolicitacaoservicotipoitem) {
		this.cdsolicitacaoservicotipoitem = cdsolicitacaoservicotipoitem;
	}
	
	public void setSolicitacaoservicotipo(
			Solicitacaoservicotipo solicitacaoservicotipo) {
		this.solicitacaoservicotipo = solicitacaoservicotipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	

}
