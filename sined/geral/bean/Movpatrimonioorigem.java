package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_movpatrimonioorigem",sequenceName="sq_movpatrimonioorigem")
public class Movpatrimonioorigem implements Log{
	
	protected Integer cdmovpatrimonioorigem;
	protected Requisicaomaterial requisicaomaterial;
	protected Entrega entrega;
	protected Usuario usuario;
	protected Romaneio romaneio;
	protected Notafiscalproduto notafiscalproduto;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected Set<Movpatrimonio> listaMovpatrimonio;
	
	public Movpatrimonioorigem(){
	}

	public Movpatrimonioorigem(Romaneio romaneio){
		this.romaneio = romaneio;
	}
	
	@Id
	@GeneratedValue(generator="sq_movpatrimonioorigem",strategy=GenerationType.AUTO)
	public Integer getCdmovpatrimonioorigem() {
		return cdmovpatrimonioorigem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicaomaterial")
	public Requisicaomaterial getRequisicaomaterial() {
		return requisicaomaterial;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega")
	public Entrega getEntrega() {
		return entrega;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneio")
	public Romaneio getRomaneio() {
		return romaneio;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}


	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@OneToMany(mappedBy="movpatrimonioorigem")
	public Set<Movpatrimonio> getListaMovpatrimonio() {
		return listaMovpatrimonio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	
	public void setListaMovpatrimonio(Set<Movpatrimonio> listaMovpatrimonio) {
		this.listaMovpatrimonio = listaMovpatrimonio;
	}

	public void setCdmovpatrimonioorigem(Integer cdmovpatrimonioorigem) {
		this.cdmovpatrimonioorigem = cdmovpatrimonioorigem;
	}


	public void setRequisicaomaterial(Requisicaomaterial requisicaomaterial) {
		this.requisicaomaterial = requisicaomaterial;
	}


	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}


	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setRomaneio(Romaneio romaneio) {
		this.romaneio = romaneio;
	}
	
}
