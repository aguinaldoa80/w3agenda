package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
@SequenceGenerator(name="sq_ecom_pedidovendapagamento", sequenceName="sq_ecom_pedidovendapagamento")
public class Ecom_PedidoVendaPagamento {

	private Integer cdEcom_PedidoVendaPagamento;
	private Integer formaPagamentoId;//"formaPagamentoId": 2188,
	private Integer numeroParcelas;//"numeroParcelas": 1,
	private Double valorParcela;//"valorParcela": 52.41,
	private Double valorDesconto;//"valorDesconto": 0.0,
	private Double valorJuros;//"valorJuros": 0.0,
	private Double valorTotal;//"valorTotal": 52.41,
	private Date dataPagamento;
	private Ecom_PedidoVenda ecomPedidoVenda;
	
	private Documentotipo documentoTipo;
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_pedidovendapagamento", strategy=GenerationType.AUTO)
	public Integer getCdEcom_PedidoVendaPagamento() {
		return cdEcom_PedidoVendaPagamento;
	}
	public void setCdEcom_PedidoVendaPagamento(Integer cdEcom_PedidoVendaPagamento) {
		this.cdEcom_PedidoVendaPagamento = cdEcom_PedidoVendaPagamento;
	}
	public Integer getFormaPagamentoId() {
		return formaPagamentoId;
	}
	public void setFormaPagamentoId(Integer formaPagamentoId) {
		this.formaPagamentoId = formaPagamentoId;
	}
	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}
	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
	public Double getValorParcela() {
		return valorParcela;
	}
	public void setValorParcela(Double valorParcela) {
		this.valorParcela = valorParcela;
	}
	public Double getValorDesconto() {
		return valorDesconto;
	}
	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public Double getValorJuros() {
		return valorJuros;
	}
	public void setValorJuros(Double valorJuros) {
		this.valorJuros = valorJuros;
	}
	public Double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public Date getDataPagamento() {
		return dataPagamento;
	}
	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_pedidovenda")
	public Ecom_PedidoVenda getEcomPedidoVenda() {
		return ecomPedidoVenda;
	}
	public void setEcomPedidoVenda(Ecom_PedidoVenda ecomPedidoVenda) {
		this.ecomPedidoVenda = ecomPedidoVenda;
	}
	@Transient
	public Documentotipo getDocumentoTipo() {
		return documentoTipo;
	}
	public void setDocumentoTipo(Documentotipo documentoTipo) {
		this.documentoTipo = documentoTipo;
	}
}
