package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_entregadocumentoapropriacao", sequenceName="sq_entregadocumentoapropriacao")
public class EntregaDocumentoApropriacao {

	private Integer cdEntregaDocumentoApropriacao;
	private Entregadocumento entregaDocumento;
	
	private Date mesAno;
	private Money valor;
	
	
	//TRANSIENTS
	private String mesAnoTrans;
	
	@Id
	@GeneratedValue(generator="sq_entregadocumentoapropriacao", strategy=GenerationType.AUTO)
	public Integer getCdEntregaDocumentoApropriacao() {
		return cdEntregaDocumentoApropriacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregadocumento")
	public Entregadocumento getEntregaDocumento() {
		return entregaDocumento;
	}
	public Date getMesAno() {
		return mesAno;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@Required
	@Transient
	@DisplayName("M�s/ano")
	public String getMesAnoTrans() {
		if (this.mesAno != null) {
			return new SimpleDateFormat("MM/yyyy").format(this.mesAno);
		}
		
		return null;
	}

	public void setCdEntregaDocumentoApropriacao(
			Integer cdEntregaDocumentoApropriacao) {
		this.cdEntregaDocumentoApropriacao = cdEntregaDocumentoApropriacao;
	}
	public void setEntregaDocumento(Entregadocumento entregaDocumento) {
		this.entregaDocumento = entregaDocumento;
	}
	public void setMesAno(Date mesAno) {
		this.mesAno = mesAno;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setMesAnoTrans(String mesAnoTrans) {
		try {
			this.mesAno = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAnoTrans).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
