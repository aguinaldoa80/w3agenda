package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contratomaterialrede", sequenceName = "sq_contratomaterialrede")
@DisplayName("Rede")
public class Contratomaterialrede implements Log{
	
	protected Integer cdcontratomaterialrede;
	protected Contratomaterial contratomaterial;
	protected String mac;
	protected Servicoservidortipo servicoservidortipo;
	protected Servidorinterface servidorinterfacepreferencial;
	protected Boolean restringirinterface;
	protected Boolean ipautomatico;
	protected Redeip redeip;
	protected String usuario;
	protected String senha;
	protected Boolean ativo = true;
	protected Endereco enderecocliente;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	
	//Transient
	protected Ambienterede ambienterede;
	protected Servidor servidor;
	protected Cliente cliente;
	protected Rede rede;
	
	public Contratomaterialrede() {
	}
	
	public Contratomaterialrede(Integer cdcontratomaterialrede) {
		this.cdcontratomaterialrede = cdcontratomaterialrede;
	}
			
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratomaterialrede")
	public Integer getCdcontratomaterialrede() {
		return cdcontratomaterialrede;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratomaterial")
	@DisplayName("Contrato de material")
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@Required
	@MaxLength(17)
	public String getMac() {
		return mac;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidortipo")
	@DisplayName("Tipo do servi�o")
	public Servicoservidortipo getServicoservidortipo() {
		return servicoservidortipo;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidorinterfacepreferencial")
	@DisplayName("Interface preferencial")
	public Servidorinterface getServidorinterfacepreferencial() {
		return servidorinterfacepreferencial;
	}
	
	@DisplayName("Restringir a esta interface")
	public Boolean getRestringirinterface() {
		return restringirinterface;
	}
	
	@DisplayName("IP autom�tico")
	public Boolean getIpautomatico() {
		return ipautomatico;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdredeip")
	@DisplayName("Endere�o IP")
	public Redeip getRedeip() {
		return redeip;
	}

	@Required
	@MaxLength(50)
	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}

	@Required
	@MaxLength(50)	
	public String getSenha() {
		return senha;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setCdcontratomaterialrede(Integer cdcontratomaterialrede) {
		this.cdcontratomaterialrede = cdcontratomaterialrede;
	}

	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setRedeip(Redeip redeip) {
		this.redeip = redeip;
	}

	public void setServidorinterfacepreferencial(Servidorinterface servidorinterfacepreferencial) {
		this.servidorinterfacepreferencial = servidorinterfacepreferencial;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setMac(String mac) {
		this.mac = mac;
	}

	public void setServicoservidortipo(Servicoservidortipo servicoservidortipo) {
		this.servicoservidortipo = servicoservidortipo;
	}
	
	public void setRestringirinterface(Boolean restringirinterface) {
		this.restringirinterface = restringirinterface;
	}

	public void setIpautomatico(Boolean ipautomatico) {
		this.ipautomatico = ipautomatico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
	
	@DisplayName("Endere�o de instala��o")
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecocliente")
	public Endereco getEnderecocliente() {
		return enderecocliente;
	}
	
	public void setEnderecocliente(Endereco enderecocliente) {
		this.enderecocliente = enderecocliente;
	}
	
	@Transient
	@Required
	public Servidor getServidor() {
		return servidor;
	}	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	
	@Transient
	@Required
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@Required
	@DisplayName("Ambiente")
	public Ambienterede getAmbienterede() {
		return ambienterede;
	}
	public void setAmbienterede(Ambienterede ambienterede) {
		this.ambienterede = ambienterede;
	}

	@Transient
	@Required	
	public Rede getRede() {
		return rede;
	}
	public void setRede(Rede rede) {
		this.rede = rede;
	}
}
