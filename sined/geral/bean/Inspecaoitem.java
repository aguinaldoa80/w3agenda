package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_inspecaoitem", sequenceName = "sq_inspecaoitem")
@DisplayName("Item de inspe��o")
public class Inspecaoitem implements Log {

	protected Integer cdinspecaoitem;
	protected String nome;
	protected String descricao;
	protected Boolean visual;
	protected Inspecaoitemtipo inspecaoitemtipo;
	protected Boolean ativo = Boolean.TRUE;

	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inspecaoitem")
	public Integer getCdinspecaoitem() {
		return cdinspecaoitem;
	}
	@Required
	@DescriptionProperty
	@MaxLength(150)
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@MaxLength(200)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Inspe��o visual")
	public Boolean getVisual() {
		return visual;
	}
	@Required
	@DisplayName("Tipo de item de inspe��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinspecaoitemtipo")
	public Inspecaoitemtipo getInspecaoitemtipo() {
		return inspecaoitemtipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setVisual(Boolean visual) {
		this.visual = visual;
	}
	public void setInspecaoitemtipo(Inspecaoitemtipo inspecaoitemtipo) {
		this.inspecaoitemtipo = inspecaoitemtipo;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setCdinspecaoitem(Integer cdinspecaoitem) {
		this.cdinspecaoitem = cdinspecaoitem;
	}
	public void setNome(String nome) {
		this.nome = nome.trim();
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Veiculoordemservicoitem)
			return ((Veiculoordemservicoitem) obj).getInspecaoitem().getCdinspecaoitem().equals(this.getCdinspecaoitem());
		
		return super.equals(obj);
	}
	
}
