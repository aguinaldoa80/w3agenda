package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_provisao", sequenceName="sq_provisao")
@DisplayName("Provis�o")
public class Provisao implements Log{

	private Integer cdProvisao;
	private Empresa empresa;
	private Colaborador colaborador;
	private EventoPagamento evento;
	private Date dtProvisao;
	private Money valor;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Colaboradordespesaitemtipo tipo;
	private Colaboradordespesa colaboradorDespesaOrigem;
	
	
	
	@Id
	@GeneratedValue(generator="sq_provisao", strategy=GenerationType.AUTO)
	public Integer getCdProvisao() {
		return cdProvisao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEvento() {
		return evento;
	}

	@Required
	@DisplayName("Data")
	public Date getDtProvisao() {
		return dtProvisao;
	}

	@Required
	public Money getValor() {
		return valor;
	}
	
	@Required
	@DisplayName("Tipo de opera��o")
	public Colaboradordespesaitemtipo getTipo() {
		return tipo;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesaorigem")
	public Colaboradordespesa getColaboradorDespesaOrigem() {
		return colaboradorDespesaOrigem;
	}

	public void setCdProvisao(Integer cdProvisao) {
		this.cdProvisao = cdProvisao;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}

	public void setDtProvisao(Date dtProvisao) {
		this.dtProvisao = dtProvisao;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setTipo(Colaboradordespesaitemtipo tipo) {
		this.tipo = tipo;
	}
	
	public void setColaboradorDespesaOrigem(
			Colaboradordespesa colaboradorDespesaOrigem) {
		this.colaboradorDespesaOrigem = colaboradorDespesaOrigem;
	}
}
