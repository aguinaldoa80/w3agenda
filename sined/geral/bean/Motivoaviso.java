package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_motivoaviso", sequenceName = "sq_motivoaviso")
@DisplayName("Motivo Aviso")
public class Motivoaviso implements Log {

	protected Integer cdmotivoaviso;
	protected MotivoavisoEnum motivo;
	protected AvisoOrigem avisoorigem;
	protected String descricao;
	protected Tipoaviso tipoaviso;
	protected Usuario usuario;
	protected Papel papel;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean email;
	protected Boolean emailDiario;
	protected Boolean notificacao;
	protected Boolean popup;
	
	public Motivoaviso() {
	}
	
	public Motivoaviso(Integer cdmotivoaviso) {
		this.cdmotivoaviso = cdmotivoaviso;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_motivoaviso")
	public Integer getCdmotivoaviso() {
		return cdmotivoaviso;
	}
	@Required
	@DisplayName("Motivo do aviso")
	@DescriptionProperty
	public MotivoavisoEnum getMotivo() {
		return motivo;
	}
	@Required
	@DisplayName("Origem")
	public AvisoOrigem getAvisoorigem() {
		return avisoorigem;
	}
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@Required
	@DisplayName("Tipo de aviso")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoaviso")
	public Tipoaviso getTipoaviso() {
		return tipoaviso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	@DisplayName("N�vel")
	public Papel getPapel() {
		return papel;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("E-Mail Di�rio")
	public Boolean getEmailDiario() {
		return emailDiario;
	}
	
	public void setEmailDiario(Boolean emailDiario) {
		this.emailDiario = emailDiario;
	}
	
	public void setCdmotivoaviso(Integer cdmotivoaviso) {
		this.cdmotivoaviso = cdmotivoaviso;
	}
	public void setMotivo(MotivoavisoEnum motivo) {
		this.motivo = motivo;
	}
	public void setAvisoorigem(AvisoOrigem avisoorigem) {
		this.avisoorigem = avisoorigem;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setTipoaviso(Tipoaviso tipoaviso) {
		this.tipoaviso = tipoaviso;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@DisplayName("E-Mail")
	public Boolean getEmail() {
		return email;
	}
	
	public void setEmail(Boolean email) {
		this.email = email;
	}
	
	@DisplayName("Notifica��o")
	public Boolean getNotificacao() {
		return notificacao;
	}
	
	public void setNotificacao(Boolean notificacao) {
		this.notificacao = notificacao;
	}
	
	@DisplayName("Popup")
	public Boolean getPopup() {
		return popup;
	}
	
	public void setPopup(Boolean popup) {
		this.popup = popup;
	}
}
