package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_emporiumvendaitem", sequenceName = "sq_emporiumvendaitem")
public class Emporiumvendaitem {

	protected Integer cdemporiumvendaitem;
	protected Emporiumvenda emporiumvenda;
	protected Integer sequencial;
	protected Integer produto_id;
	protected String produto_desc;
	protected Double quantidade;
	protected Double valorunitario;
	protected Double valortotal;
	protected Double desconto;
	protected String sku_id;
	protected Material material;
	protected String legenda;
	protected Double taxa;
	protected String kit;
	protected String vendedor_id;
	
	// TRANSIENTE
	protected Double desconto_it = 0d;
	protected String numero_ticket;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumvendaitem")
	public Integer getCdemporiumvendaitem() {
		return cdemporiumvendaitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemporiumvenda")
	public Emporiumvenda getEmporiumvenda() {
		return emporiumvenda;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	@DisplayName("ID do Produto")
	public Integer getProduto_id() {
		return produto_id;
	}

	@DisplayName("Produto")
	public String getProduto_desc() {
		return produto_desc;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}

	@DisplayName("Valor total")
	public Double getValortotal() {
		return valortotal;
	}

	@DisplayName("C�digo de barras")
	public String getSku_id() {
		return sku_id;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="produto_id", insertable=false, updatable=false)
	public Material getMaterial() {
		return material;
	}
	
	public Double getDesconto() {
		return desconto;
	}
	
	public String getLegenda() {
		return legenda;
	}
	
	public Double getTaxa() {
		return taxa;
	}
	
	public String getKit() {
		return kit;
	}
	
	public void setKit(String kit) {
		this.kit = kit;
	}
	
	@DisplayName("Vendedor")
	public String getVendedor_id() {
		return vendedor_id;
	}

	public void setVendedor_id(String vendedorId) {
		vendedor_id = vendedorId;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setCdemporiumvendaitem(Integer cdemporiumvendaitem) {
		this.cdemporiumvendaitem = cdemporiumvendaitem;
	}

	public void setEmporiumvenda(Emporiumvenda emporiumvenda) {
		this.emporiumvenda = emporiumvenda;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setProduto_id(Integer produtoId) {
		produto_id = produtoId;
	}

	public void setProduto_desc(String produtoDesc) {
		produto_desc = produtoDesc;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setValortotal(Double valortotal) {
		this.valortotal = valortotal;
	}

	public void setSku_id(String skuId) {
		sku_id = skuId;
	}
	
	@Transient
	public Double getDesconto_it() {
		return desconto_it;
	}
	
	public void setDesconto_it(Double descontoIt) {
		desconto_it = descontoIt;
	}
	
	@Transient
	public Double getValorComDesconto(){
		Double valortotal = this.getValortotal() != null ? this.getValortotal() : 0d;
		Double desconto_it = this.getDesconto_it() != null ? this.getDesconto_it() : 0d;
		return valortotal - desconto_it;
	}
	
	@Transient
	public String getNumero_ticket() {
		return numero_ticket;
	}
	
	public void setNumero_ticket(String numero_ticket) {
		this.numero_ticket = numero_ticket;
	}

}
