package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_pessoacontato", sequenceName = "sq_pessoacontato")
public class PessoaContato {

	protected Integer cdpessoacontato;
	protected Pessoa pessoa;
	protected Contato contato;
	
	//Transient
	private Boolean cliente;
	private Boolean fornecedor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoacontato")
	public Integer getCdpessoacontato() {
		return cdpessoacontato;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	@Required
	public Contato getContato() {
		return contato;
	}
	
	public void setCdpessoacontato(Integer cdpessoacontato) {
		this.cdpessoacontato = cdpessoacontato;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	public void setContato(Contato contato) {
		this.contato = contato;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdpessoacontato == null) ? 0 : cdpessoacontato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaContato other = (PessoaContato) obj;
		if (cdpessoacontato == null) {
			if (other.cdpessoacontato != null)
				return false;
		} else if (!cdpessoacontato.equals(other.cdpessoacontato))
			return false;
		return true;
	}
	
	@Transient
	public Boolean getCliente() {
		return cliente;
	}
	
	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	public Boolean getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Boolean fornecedor) {
		this.fornecedor = fornecedor;
	}
}
