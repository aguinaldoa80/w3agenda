package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tarefasituacao;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name="sq_tarefa",sequenceName="sq_tarefa")
public class Tarefa implements Log, PermissaoProjeto{

	protected Integer cdtarefa;
	protected Planejamento planejamento;
	protected Indice indice;
	protected String descricao;
	protected Double qtde;
	protected Date dtinicio;
	protected Date dtfim;
	protected Integer duracao;
	protected Area area;
	protected Unidademedida unidademedida;
	protected Tarefa tarefapai;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer id;
	protected String predecessoras;
	protected String observacao;
	protected Double custototal;
	protected Tarefasituacao tarefasituacao;
	protected Set<Tarefarecursogeral> listaTarefarecursogeral = new ListSet<Tarefarecursogeral>(Tarefarecursogeral.class);
	protected Set<Tarefarecursohumano> listaTarefarecursohumano = new ListSet<Tarefarecursohumano>(Tarefarecursohumano.class);
	protected Set<Tarefacolaborador> listaTarefacolaborador = new ListSet<Tarefacolaborador>(Tarefacolaborador.class);
	protected Set<Tarefaarquivo> listaTarefaarquivo = new ListSet<Tarefaarquivo>(Tarefaarquivo.class);
	
	protected Set<Atividade> listaAtividade;
	protected Set<Producaodiaria> listaProducaodiaria;
	
	protected Double qtdMensal;
	protected Integer rowPai;
	protected String cdstarefa;
	protected Boolean existeRecursogeralComOrigem;
	
	protected List<Tarefarequisicao> listaTarefarequisicao;
	
	private Integer idPai;
	
	public Tarefa(){}
	
	public Tarefa(Integer cdtarefa){
		this.cdtarefa = cdtarefa;
	}
	
	@Id
	@GeneratedValue(generator="sq_tarefa", strategy=GenerationType.AUTO)
	public Integer getCdtarefa() {
		return cdtarefa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	@DisplayName("Composi��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindice")
	public Indice getIndice() {
		return indice;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(300)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@Required
	@DisplayName("Quantidade")
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Fim")
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Dura��o")
	public Integer getDuracao() {
		return duracao;
	}
	@DisplayName("�rea")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarea")
	public Area getArea() {
		return area;
	}
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@DisplayName("Tarefa pai")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefapai")
	public Tarefa getTarefapai() {
		return tarefapai;
	}
	
	@DisplayName("Situa��o")
	public Tarefasituacao getTarefasituacao() {
		return tarefasituacao;
	}
	
	@OneToMany(mappedBy="tarefa")
	public Set<Atividade> getListaAtividade() {
		return listaAtividade;
	}
	@OneToMany(mappedBy="tarefa")
	public Set<Producaodiaria> getListaProducaodiaria() {
		return listaProducaodiaria;
	}
	@MaxLength(500)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setListaProducaodiaria(Set<Producaodiaria> listaProducaodiaria) {
		this.listaProducaodiaria = listaProducaodiaria;
	}
	public void setListaAtividade(Set<Atividade> listaAtividade) {
		this.listaAtividade = listaAtividade;
	}
	public void setTarefapai(Tarefa tarefapai) {
		this.tarefapai = tarefapai;
	}
	public void setCdtarefa(Integer cdtarefa) {
		this.cdtarefa = cdtarefa;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setIndice(Indice indice) {
		this.indice = indice;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setTarefasituacao(Tarefasituacao tarefasituacao) {
		this.tarefasituacao = tarefasituacao;
	}
	
	
//	LISTAS
	
	@OneToMany(mappedBy="tarefa")
	@DisplayName("Recursos Gerais")
	public Set<Tarefarecursogeral> getListaTarefarecursogeral() {
		return listaTarefarecursogeral;
	}
	
	@OneToMany(mappedBy="tarefa")
	@DisplayName("Recursos Humanos")
	public Set<Tarefarecursohumano> getListaTarefarecursohumano() {
		return listaTarefarecursohumano;
	}
	
	@OneToMany(mappedBy="tarefa")
	@DisplayName("Colaborador")
	public Set<Tarefacolaborador> getListaTarefacolaborador() {
		return listaTarefacolaborador;
	}
	
	@OneToMany(mappedBy="tarefa")
	@DisplayName("Arquivos")
	public Set<Tarefaarquivo> getListaTarefaarquivo() {
		return listaTarefaarquivo;
	}
	
	public void setListaTarefaarquivo(Set<Tarefaarquivo> listaTarefaarquivo) {
		this.listaTarefaarquivo = listaTarefaarquivo;
	}
	public void setListaTarefarecursogeral(Set<Tarefarecursogeral> listaTarefarecursogeral) {
		this.listaTarefarecursogeral = listaTarefarecursogeral;
	}
	public void setListaTarefarecursohumano(Set<Tarefarecursohumano> listaTarefarecursohumano) {
		this.listaTarefarecursohumano = listaTarefarecursohumano;
	}
	public void setListaTarefacolaborador(Set<Tarefacolaborador> listaTarefacolaborador) {
		this.listaTarefacolaborador = listaTarefacolaborador;
	}
	
	
	//	LOG	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}	

	public Integer getId() {
		return id;
	}
	public String getPredecessoras() {
		return predecessoras;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setPredecessoras(String predecessoras) {
		this.predecessoras = predecessoras;
	}
	
	
	/******************/
	/** Transientes **/
	/****************/
	protected Double peso;
	protected List<Tarefa> listaTarefaFilha;	
	protected Integer cdtarefapai;
	protected Integer nivelEstrutura = 1;
	protected Boolean haveCalendario;
	protected String colaboradorResponsavel;
	
	@Transient
	public Boolean getHaveCalendario() {
		return haveCalendario;
	}
	
	public void setHaveCalendario(Boolean haveCalendario) {
		this.haveCalendario = haveCalendario;
	}
	
	@Transient
	public Double getPeso() {
		return peso;
	}
	
	@Transient
	public List<Tarefa> getListaTarefaFilha() {
		return listaTarefaFilha;
	}
	
	@Transient
	public Integer getCdtarefapai() {
		return cdtarefapai;
	}
	
	public void setCdtarefapai(Integer cdtarefapai) {
		this.cdtarefapai = cdtarefapai;
	}
	
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	
	public void setListaTarefaFilha(List<Tarefa> listaTarefaFilha) {
		this.listaTarefaFilha = listaTarefaFilha;
	}
	
	@DisplayName("Custo total")
	public Double getCustototal() {
		return custototal;
	}
	
	public void setCustototal(Double custototal) {
		this.custototal = custototal;
	}
	
	@Transient
	public Integer getNivelEstrutura() {
		return nivelEstrutura;
	}
	
	public void setNivelEstrutura(Integer nivelEstrutura) {
		this.nivelEstrutura = nivelEstrutura;
	}
	
	/***********************/
	/** Equals e HashCode **/
	/***********************/
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Tarefa) {
			Tarefa that = (Tarefa) obj;
			return this.getCdtarefa().equals(that.getCdtarefa());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdtarefa == null) {
			return super.hashCode();
		}
		return cdtarefa.hashCode();
	}
	
	public String subQueryProjeto() {
		return "select tarefasubQueryProjeto.cdtarefa " +
				"from Tarefa tarefasubQueryProjeto " +
				"join tarefasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public Double getQtdMensal() {
		if(this.qtdMensal == null)
			calculaQtdeMensal();
		return qtdMensal;
	}
	
	@Transient
	private void calculaQtdeMensal() {
		this.dtfim = getDtfim();
		if(this.qtde != null && this.dtinicio != null && this.dtfim != null){
			Calendar dtFimAux = Calendar.getInstance();
			Calendar dtInicioAux = Calendar.getInstance();
			
			dtInicioAux.setTimeInMillis(this.dtinicio.getTime());
			dtFimAux.setTimeInMillis(this.dtfim.getTime());
			dtInicioAux.set(Calendar.DAY_OF_MONTH, 1);
			dtFimAux.set(Calendar.DAY_OF_MONTH, 1);
			int qtdeMensalPai = SinedDateUtils.diferencaMeses(new Date(dtFimAux.getTimeInMillis()), new Date(dtInicioAux.getTimeInMillis()))+1;
//			int qtdeMensalPai = (dtFimAux.get(Calendar.YEAR) - dtInicioAux.get(Calendar.YEAR)) * 12 +  dtInicioAux.get(Calendar.MONTH) - dtFimAux.get(Calendar.MONTH)+1;
			this.qtdMensal = SinedUtil.round(this.qtde / qtdeMensalPai, 2);
		}
	}
	
	public void setQtdMensal(Double qtdMensal) {
		this.qtdMensal = qtdMensal;
	}
	
	@Transient
	public Integer getRowPai() {
		return rowPai;
	}
	
	public void setRowPai(Integer rowPai) {
		this.rowPai = rowPai;
	}

	@DisplayName("Ordem de Servi�o")
	@Transient
	public List<Tarefarequisicao> getListaTarefarequisicao() {
		return listaTarefarequisicao;
	}
	public void setListaTarefarequisicao(List<Tarefarequisicao> listaTarefarequisicao) {
		this.listaTarefarequisicao = listaTarefarequisicao;
	}

	@Transient
	public String getColaboradorResponsavel() {
		return colaboradorResponsavel;
	}

	public void setColaboradorResponsavel(String colaboradorResponsavel) {
		this.colaboradorResponsavel = colaboradorResponsavel;
	}
	
	@Transient
	public String getCdstarefa() {
		return cdstarefa;
	}

	public void setCdstarefa(String cdstarefa) {
		this.cdstarefa = cdstarefa;
	}
	
	@Transient
	public Integer getIdPai() {
		return idPai;
	}
	
	public void setIdPai(Integer idPai) {
		this.idPai = idPai;
	}

	@Transient
	public Date getDtfimCalculada() {
		Date dataFim = null; 
		if(dtfim == null && dtinicio != null && duracao != null && (haveCalendario != null && haveCalendario)){
			Calendario calendario = null;
			if(getPlanejamento() != null && getPlanejamento().getProjeto() != null && getPlanejamento().getProjeto().getCdprojeto() != null){
				calendario = Neo.getObject(CalendarioService.class).getByProjeto(getPlanejamento().getProjeto());
			}
			dataFim = SinedDateUtils.incrementaDia(dtinicio, duracao-1, calendario);
		}else if(dtfim == null && dtinicio != null && duracao != null && (haveCalendario == null || !haveCalendario)){
			dataFim = SinedDateUtils.incrementaDia(dtinicio, duracao-1, null);
		}
		return dataFim;
	}

	@Transient
	public Boolean getExisteRecursogeralComOrigem() {
		return existeRecursogeralComOrigem;
	}

	public void setExisteRecursogeralComOrigem(Boolean existeRecursogeralComOrigem) {
		this.existeRecursogeralComOrigem = existeRecursogeralComOrigem;
	}
}