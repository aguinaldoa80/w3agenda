package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servicodominiorede", sequenceName = "sq_servicodominiorede")
public class Servicodominiorede implements Log{
	
	protected Integer cdservicodominiorede;
	protected Servicoservidortipo servicoservidortipo;
	protected String endereco;
	protected Boolean bloqueado;
	protected Redemascara redemascara;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servicodominiorede")
	public Integer getCdservicodominiorede() {
		return cdservicodominiorede;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservicoservidortipo")
	@DisplayName("Servi�o de servidor")
	public Servicoservidortipo getServicoservidortipo() {
		return servicoservidortipo;
	}
	
	@Required
	@MaxLength(25)
	@DisplayName("Endere�o IP")
	public String getEndereco() {
		return endereco;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdredemascara")
	@DisplayName("M�scara")
	public Redemascara getRedemascara() {
		return redemascara;
	}
		
	public void setCdservicodominiorede(Integer cdservicodominiorede) {
		this.cdservicodominiorede = cdservicodominiorede;
	}

	public void setServicoservidortipo(Servicoservidortipo servicoservidortipo) {
		this.servicoservidortipo = servicoservidortipo;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public void setRedemascara(Redemascara redemascara) {
		this.redemascara = redemascara;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
}
