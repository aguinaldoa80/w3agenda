package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
@SequenceGenerator(name = "sq_ordemcompraentregamaterial", sequenceName = "sq_ordemcompraentregamaterial")
public class Ordemcompraentregamaterial {

	private Integer cdordemcompraentregamaterial;
	private Ordemcompramaterial ordemcompramaterial;
	private Entregamaterial entregamaterial;
	private Double qtde;
	
	// TRANSIENTE
	private Double resto;
	private Double aux_qtde;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcompraentregamaterial")
	public Integer getCdordemcompraentregamaterial() {
		return cdordemcompraentregamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompramaterial")
	public Ordemcompramaterial getOrdemcompramaterial() {
		return ordemcompramaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}
	public Double getQtde() {
		return qtde;
	}
	
	public void setCdordemcompraentregamaterial(Integer cdordemcompraentregamaterial) {
		this.cdordemcompraentregamaterial = cdordemcompraentregamaterial;
	}
	public void setOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		this.ordemcompramaterial = ordemcompramaterial;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}	
	
	@Transient
	public Double getResto() {
		return resto;
	}
	
	public void setResto(Double resto) {
		this.resto = resto;
	}

	@Transient
	public Double getAux_qtde() {
		return aux_qtde;
	}
	public void setAux_qtde(Double auxQtde) {
		aux_qtde = auxQtde;
	}
}