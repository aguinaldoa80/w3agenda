package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de item de composição")
public class Tipoitemcomposicao {
	
	public static final Tipoitemcomposicao MATERIAL = new Tipoitemcomposicao(1);
	public static final Tipoitemcomposicao OUTRO = new Tipoitemcomposicao(2);
	
	protected Integer cdtipoitemcomposicao;
	protected String nome;
	
	public Tipoitemcomposicao() {
		
	}
	
	public Tipoitemcomposicao(Integer cdtipoitemcomposicao) {
		this.cdtipoitemcomposicao = cdtipoitemcomposicao;
	}
	
	@Id	
	public Integer getCdtipoitemcomposicao() {
		return cdtipoitemcomposicao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdtipoitemcomposicao(Integer cdtipoitemcomposicao) {
		this.cdtipoitemcomposicao = cdtipoitemcomposicao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tipoitemcomposicao){
			Tipoitemcomposicao that = (Tipoitemcomposicao) obj;
			return this.getCdtipoitemcomposicao().equals(that.getCdtipoitemcomposicao());
		}
		return super.equals(obj);
	}
}
