package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_materialcustoempresa", sequenceName="sq_materialcustoempresa")
@DisplayName("Custo por Empresa")
public class MaterialCustoEmpresa implements Log {
	protected Integer cdMaterialCustoEmpresa;
	protected Material material;
	protected Empresa empresa;
	protected Double quantidadeReferencia;
	protected Double valorUltimaCompra;
	protected Double custoMedio;
	protected Date dtInicio;
	protected Double custoInicial;
	protected ContaContabil contaContabil;
	
	
	// LOG
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialcustoempresa")
	public Integer getCdMaterialCustoEmpresa() {
		return cdMaterialCustoEmpresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Qtde de Refer�ncia")
	public Double getQuantidadeReferencia() {
		return quantidadeReferencia;
	}
	
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	
	@DisplayName("Custo Inicial")
	public Double getCustoInicial() {
		return custoInicial;
	}
	
	@DisplayName("Custo M�dio")
	public Double getCustoMedio() {
		return custoMedio;
	}
	
	@DisplayName("Valor da �ltima Compra")
	public Double getValorUltimaCompra() {
		return valorUltimaCompra;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setCdMaterialCustoEmpresa(Integer cdMaterialCustoEmpresa) {
		this.cdMaterialCustoEmpresa = cdMaterialCustoEmpresa;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setQuantidadeReferencia(Double quantidadeReferencia) {
		this.quantidadeReferencia = quantidadeReferencia;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setCustoInicial(Double custoInicial) {
		this.custoInicial = custoInicial;
	}
	public void setCustoMedio(Double custoMedio) {
		this.custoMedio = custoMedio;
	}
	public void setValorUltimaCompra(Double valorUltimaCompra) {
		this.valorUltimaCompra = valorUltimaCompra;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
}
