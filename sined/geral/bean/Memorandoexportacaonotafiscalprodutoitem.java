package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
@Entity
@SequenceGenerator(name="sq_memorandoexportacaonotafiscalprodutoitem", sequenceName="sq_memorandoexportacaonotafiscalprodutoitem")
public class Memorandoexportacaonotafiscalprodutoitem {
	private Integer cdmemorandoexportacaonotafiscalprodutoitem;
	private Memorandoexportacao memorandoexportacao;
	private Notafiscalprodutoitem notafiscalprodutoitem;
	
	public Memorandoexportacaonotafiscalprodutoitem() {
	}
	
	public Memorandoexportacaonotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem){
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}
	
	@Id
	@GeneratedValue(generator="sq_memorandoexportacaonotafiscalprodutoitem", strategy=GenerationType.AUTO)
	public Integer getCdmemorandoexportacaonotafiscalprodutoitem() {
		return cdmemorandoexportacaonotafiscalprodutoitem;
	}
	public void setCdmemorandoexportacaonotafiscalprodutoitem(
			Integer cdmemorandoexportacaonotafiscalprodutoitem) {
		this.cdmemorandoexportacaonotafiscalprodutoitem = cdmemorandoexportacaonotafiscalprodutoitem;
	}
	@ManyToOne
	@JoinColumn(name="cdmemorandoexportacao")
	public Memorandoexportacao getMemorandoexportacao() {
		return memorandoexportacao;
	}
	public void setMemorandoexportacao(Memorandoexportacao memorandoexportacao) {
		this.memorandoexportacao = memorandoexportacao;
	}
	@ManyToOne
	@JoinColumn(name="cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotafiscalprodutoitem() {
		return notafiscalprodutoitem;
	}
	public void setNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem) {
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}
	
	

}
