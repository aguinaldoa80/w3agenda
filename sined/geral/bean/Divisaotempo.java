package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum Divisaotempo {
	
	NENHUMA(0,""),
	SEMANA(1,"Semana");
	
	private Divisaotempo (Integer codigo){
		this.codigo = codigo;
	}
	
	private Divisaotempo (Integer codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	private Integer codigo;
	private String descricao;
	
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return this.descricao;
	}
	
}
