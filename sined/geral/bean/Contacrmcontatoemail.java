package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@SequenceGenerator(name = "sq_contacrmcontatoemail", sequenceName = "sq_contacrmcontatoemail")
public class Contacrmcontatoemail {
	
	protected Integer cdcontacrmcontatoemail;
	protected String email;
	protected Contacrmcontato contacrmcontato;
	
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contacrmcontatoemail")
	public Integer getCdcontacrmcontatoemail() {
		return cdcontacrmcontatoemail;
	}
	
	@Email
	@MaxLength(80)
	@DisplayName("E-mail")
	public String getEmail() {
		return email;
	}
	
	@DisplayName("Contato")
	@JoinColumn(name="cdcontacrmcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	
	
	public void setCdcontacrmcontatoemail(Integer cdcontacrmcontatoemail) {
		this.cdcontacrmcontatoemail = cdcontacrmcontatoemail;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	
	
	

}
