package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_pedidovendamaterialsincronizacao", sequenceName = "sq_pedidovendamaterialsincronizacao")
public class Pedidovendamaterialsincronizacao {

	protected Integer cdpedidovendamaterialsincronizacao;
	protected Pedidovendasincronizacao pedidovendasincronizacao;
	
	//Material
	protected Material material;
	protected Integer materialId;
	protected Material materialmestre;
	protected Integer materialmestreId;
	protected String materialDescricao;
	protected String materialReferencia;
	protected String materialCodigo;
	protected String materialClasseproduto;
	protected String materialDescricaoEmbalagem;
	protected Integer materialQuantidadeVolume;
	protected Double materialPeso;	
	protected Long materialAltura;
	protected Long materialLargura;
	protected Long materialComprimento;
	protected Integer materialcorId;
	
	//Material Grupo
	protected Materialgrupo materialgrupo;
	protected String materialgrupoNumero;
	protected String materialgrupoNome;
	
	//Pedido Venda Produto
	protected Pedidovendamaterial pedidovendamaterial;
	protected Integer pedidovendaprodutoId;
	protected String pedidovendaprodutoDeposito;
	protected Integer pedidovendaprodutoIdPedidoVenda;
	protected Integer pedidovendaprodutoIdProduto;
	protected Double pedidovendaprodutoQuantidade;
	protected Date pedidovendaprodutoPrevisaoEntrega;
	protected String pedidovendaprodutoTipo;
	protected Integer pedidovendaprodutoIdEnderecoEntrega;
	protected Double pedidovendaprodutoValorUnitario;
	protected String pedidovendaprodutoFlagReenvio;
	protected String pedidovendaprodutoObservacao;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendamaterialsincronizacao")
	public Integer getCdpedidovendamaterialsincronizacao() {
		return cdpedidovendamaterialsincronizacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendasincronizacao")
	public Pedidovendasincronizacao getPedidovendasincronizacao() {
		return pedidovendasincronizacao;
	}
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Material Mestre")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialmestre")
	public Material getMaterialmestre() {
		return materialmestre;
	}
	@DisplayName("Descri��o")
	public String getMaterialDescricao() {
		return materialDescricao;
	}
	@DisplayName("Refer�ncia")
	public String getMaterialReferencia() {
		return materialReferencia;
	}	
	@DisplayName("Descri��o das Embalagem")
	public String getMaterialDescricaoEmbalagem() {
		return materialDescricaoEmbalagem;
	}
	@DisplayName("Quantidade Volume")
	public Integer getMaterialQuantidadeVolume() {
		return materialQuantidadeVolume;
	}
	@DisplayName("Peso")
	public Double getMaterialPeso() {
		return materialPeso;
	}
	@DisplayName("Altura")
	public Long getMaterialAltura() {
		return materialAltura;
	}
	@DisplayName("Largura")
	public Long getMaterialLargura() {
		return materialLargura;
	}
	@DisplayName("Comprimento")
	public Long getMaterialComprimento() {
		return materialComprimento;
	}
	@DisplayName("Id. Cor Material")
	public Integer getMaterialcorId() {
		return materialcorId;
	}
	@DisplayName("Grupo do Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@DisplayName("Nome do Grupo")
	public String getMaterialgrupoNome() {
		return materialgrupoNome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	@DisplayName("Dep�sito")
	public String getPedidovendaprodutoDeposito() {
		return pedidovendaprodutoDeposito;
	}	
	@DisplayName("Quantidade")
	public Double getPedidovendaprodutoQuantidade() {
		return pedidovendaprodutoQuantidade;
	}
	@DisplayName("Previs�o de Entrega")
	public Date getPedidovendaprodutoPrevisaoEntrega() {
		return pedidovendaprodutoPrevisaoEntrega;
	}
	@DisplayName("Tipo")
	public String getPedidovendaprodutoTipo() {
		return pedidovendaprodutoTipo;
	}
	@DisplayName("Endere�o de Entrega")
	public Integer getPedidovendaprodutoIdEnderecoEntrega() {
		return pedidovendaprodutoIdEnderecoEntrega;
	}
	@DisplayName("Valor Unit�rio")
	public Double getPedidovendaprodutoValorUnitario() {
		return pedidovendaprodutoValorUnitario;
	}
	@DisplayName("Flag Reenvio")
	public String getPedidovendaprodutoFlagReenvio() {
		return pedidovendaprodutoFlagReenvio;
	}

	public void setCdpedidovendamaterialsincronizacao(
			Integer cdpedidovendamaterialsincronizacao) {
		this.cdpedidovendamaterialsincronizacao = cdpedidovendamaterialsincronizacao;
	}

	public void setPedidovendasincronizacao(
			Pedidovendasincronizacao pedidovendasincronizacao) {
		this.pedidovendasincronizacao = pedidovendasincronizacao;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}

	public void setMaterialDescricao(String materialDescricao) {
		this.materialDescricao = materialDescricao;
	}

	public void setMaterialReferencia(String materialReferencia) {
		this.materialReferencia = materialReferencia;
	}

	public void setMaterialDescricaoEmbalagem(String materialDescricaoEmbalagem) {
		this.materialDescricaoEmbalagem = materialDescricaoEmbalagem;
	}

	public void setMaterialQuantidadeVolume(Integer materialQuantidadeVolume) {
		this.materialQuantidadeVolume = materialQuantidadeVolume;
	}
	
	public void setMaterialPeso(Double materialPeso) {
		this.materialPeso = materialPeso;
	}

	public void setMaterialAltura(Long materialAltura) {
		this.materialAltura = materialAltura;
	}
	
	public void setMaterialLargura(Long materialLargura) {
		this.materialLargura = materialLargura;
	}
	
	public void setMaterialComprimento(Long materialComprimento) {
		this.materialComprimento = materialComprimento;
	}
	
	public void setMaterialcorId(Integer materialcorId) {
		this.materialcorId = materialcorId;
	}
	
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}

	public void setMaterialgrupoNumero(String materialgrupoNumero) {
		this.materialgrupoNumero = materialgrupoNumero;
	}

	public void setMaterialgrupoNome(String materialgrupoNome) {
		this.materialgrupoNome = materialgrupoNome;
	}

	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}	

	public void setPedidovendaprodutoDeposito(String pedidovendaprodutoDeposito) {
		this.pedidovendaprodutoDeposito = pedidovendaprodutoDeposito;
	}	

	public void setPedidovendaprodutoQuantidade(Double pedidovendaprodutoQuantidade) {
		this.pedidovendaprodutoQuantidade = pedidovendaprodutoQuantidade;
	}

	public void setPedidovendaprodutoPrevisaoEntrega(
			Date pedidovendaprodutoPrevisaoEntrega) {
		this.pedidovendaprodutoPrevisaoEntrega = pedidovendaprodutoPrevisaoEntrega;
	}

	public void setPedidovendaprodutoTipo(String pedidovendaprodutoTipo) {
		this.pedidovendaprodutoTipo = pedidovendaprodutoTipo;
	}

	public void setPedidovendaprodutoIdEnderecoEntrega(
			Integer pedidovendaprodutoIdEnderecoEntrega) {
		this.pedidovendaprodutoIdEnderecoEntrega = pedidovendaprodutoIdEnderecoEntrega;
	}

	public void setPedidovendaprodutoValorUnitario(
			Double pedidovendaprodutoValorUnitario) {
		this.pedidovendaprodutoValorUnitario = pedidovendaprodutoValorUnitario;
	}

	public void setPedidovendaprodutoFlagReenvio(
			String pedidovendaprodutoFlagReenvio) {
		this.pedidovendaprodutoFlagReenvio = pedidovendaprodutoFlagReenvio;
	}

	public String getPedidovendaprodutoObservacao() {
		return pedidovendaprodutoObservacao;
	}
	public void setPedidovendaprodutoObservacao(String pedidovendaprodutoObservacao) {
		this.pedidovendaprodutoObservacao = pedidovendaprodutoObservacao;
	}
	@DisplayName("Id Pedido Venda Material")
	@Transient
	public Integer getMaterialId() {
		if(materialId == null && material != null){
			return material.getCdmaterial();
		}
		return materialId;
	}
	@DisplayName("Id. Material mestre")
	@Transient
	public int getMaterialmestreId() {
		int idRetorno = 0;
		if(materialmestreId != null){
			idRetorno = materialmestreId.intValue();
		}else if(materialmestre != null && materialmestre.getCdmaterial() != null){
			idRetorno = materialmestre.getCdmaterial().intValue();
		}
		return idRetorno;
	}
	@DisplayName("C�digo")
	@Transient
	public String getMaterialCodigo() {
		if((materialCodigo == null || "".equals(materialCodigo)) && material != null && material.getCdmaterial() != null){
			materialCodigo = material.getCdmaterial().toString();
		}
		return materialCodigo;
	}
	
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}

	public void setMaterialmestreId(Integer materialmestreId) {
		this.materialmestreId = materialmestreId;
	}
	public void setMaterialCodigo(String materialCodigo) {
		this.materialCodigo = materialCodigo;
	}

	@DisplayName("Classe")
	@Transient
	public String getMaterialClasseproduto() {
		if((materialClasseproduto == null || "".equals(materialClasseproduto))){
			if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
				return materialgrupo.getCdmaterialgrupo().toString();
			}else if(material != null && material.getMaterialgrupo() != null && material.getMaterialgrupo().getCdmaterialgrupo() != null){
				return material.getMaterialgrupo().getCdmaterialgrupo().toString();
			}
		}
		return materialClasseproduto;
	}
	
	public void setMaterialClasseproduto(String materialClasseproduto) {
		this.materialClasseproduto = materialClasseproduto;
	}

	@DisplayName("N�mero")
	@Transient
	public String getMaterialgrupoNumero() {
		if((materialgrupoNumero == null || "".equals(materialgrupoNumero))){
			if(materialgrupo != null && materialgrupo.getCdmaterialgrupo() != null){
				return materialgrupo.getCdmaterialgrupo().toString();
			}
		}
		return materialgrupoNumero;
	}
	
	@Transient
	public Integer getPedidovendaprodutoId() {
		if(pedidovendaprodutoId == null && pedidovendamaterial != null){
			return pedidovendamaterial.getCdpedidovendamaterial();
		}
		return pedidovendaprodutoId;
	}
	public void setPedidovendaprodutoId(Integer pedidovendaprodutoId) {
		this.pedidovendaprodutoId = pedidovendaprodutoId;
	}
	
	@DisplayName("Id Pedido Venda")
	@Transient
	public Integer getPedidovendaprodutoIdPedidoVenda() {
		if(pedidovendaprodutoIdPedidoVenda == null && pedidovendamaterial != null && pedidovendamaterial.getPedidovenda() != null){
			return pedidovendamaterial.getPedidovenda().getCdpedidovenda();
		}
		return pedidovendaprodutoIdPedidoVenda;
	}

	@DisplayName("Id")
	@Transient
	public Integer getPedidovendaprodutoIdProduto() {
		if(pedidovendaprodutoIdProduto == null && material != null){
			return material.getCdmaterial();
		}
		return pedidovendaprodutoIdProduto;
	}
	
	public void setPedidovendaprodutoIdPedidoVenda(
			Integer pedidovendaprodutoIdPedidoVenda) {
		this.pedidovendaprodutoIdPedidoVenda = pedidovendaprodutoIdPedidoVenda;
	}
	public void setPedidovendaprodutoIdProduto(Integer pedidovendaprodutoIdProduto) {
		this.pedidovendaprodutoIdProduto = pedidovendaprodutoIdProduto;
	}
}
