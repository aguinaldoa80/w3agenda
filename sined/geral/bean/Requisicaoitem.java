package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_requisicaoitem",sequenceName="sq_requisicaoitem")
public class Requisicaoitem {
	protected Integer cdrequisicaoitem;
	protected Requisicao requisicao;
	protected Atividadetipoitem atividadetipoitem;
	protected String valor;
	
	
	@Id
	@GeneratedValue(generator="sq_requisicaoitem",strategy=GenerationType.AUTO)
	public Integer getCdrequisicaoitem() {
		return cdrequisicaoitem;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrequisicao")
	public Requisicao getRequisicao() {
		return requisicao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatividadetipoitem")
	public Atividadetipoitem getAtividadetipoitem() {
		return atividadetipoitem;
	}
	@MaxLength(500)
	public String getValor() {
		return valor;
	}
	
	public void setCdrequisicaoitem(Integer cdrequisicaoitem) {
		this.cdrequisicaoitem = cdrequisicaoitem;
	}
	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}
	public void setAtividadetipoitem(Atividadetipoitem atividadetipoitem) {
		this.atividadetipoitem = atividadetipoitem;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
	
	
}
