package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_contapapel", sequenceName = "sq_contapapel")
public class Contapapel {

	private Integer cdcontapapel;
	private Conta conta;
	private Papel papel;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contapapel")
	@DisplayName("Id")
	public Integer getCdcontapapel() {
		return cdcontapapel;
	}
	public void setCdcontapapel(Integer cdcontapapel) {
		this.cdcontapapel = cdcontapapel;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
}
