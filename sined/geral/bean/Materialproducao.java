package br.com.linkcom.sined.geral.bean;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.ibm.icu.text.SimpleDateFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_materialproducao", sequenceName = "sq_materialproducao")
@DisplayName("Produ��o")
public class Materialproducao {

	protected Integer cdmaterialproducao;
	protected Integer ordemexibicao;
	protected Material materialmestre;
	protected Material material;
	protected Double largura;
	protected Double altura;
	protected Double consumo;
	protected Double preco;
	protected Double valorconsumo;
	protected Producaoetapanome producaoetapanome;
	protected Boolean opcional;
	protected Boolean discriminarnota;
	protected Boolean exibirvenda;
	protected Boolean acompanhabanda;
	protected Boolean carcaca;
	protected List<Materialproducaoformula> listaMaterialproducaoformula = new ListSet<Materialproducaoformula>(Materialproducaoformula.class);
	
//	TRANSIENTES
	protected String index;
	protected Date prazoentrega;
	protected Loteestoque loteestoque;
	protected Boolean producaosemestoque;
	protected Double qtdeprevista;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Material materialanterior;
	protected Boolean naocalcularqtdeprevista;
	protected Integer agitacao;
	protected Integer quantidadepercentual;
	protected Unidademedida unidademedida;
	protected Double fracaounidademedida;
	protected Double qtdereferenciaunidademedida;	
	protected Double vendaaltura;
	protected Double vendalargura;
	protected Double vendacomprimento;
	protected Boolean coleta;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialproducao")
	public Integer getCdmaterialproducao() {
		return cdmaterialproducao;
	}
	@DisplayName("Ordem")
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialmestre")
	@Required
	public Material getMaterialmestre() {
		return materialmestre;
	}
	
	@DisplayName("Material/Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@Required
	public Material getMaterial() {
		return material;
	}

	@MinValue(0)
	public Double getLargura() {
		return largura;
	}

	@MinValue(0)
	public Double getAltura() {
		return altura;
	}

	@Required
	@MinValue(0)
	public Double getConsumo() {
		return consumo;
	}

	@Required
	@DisplayName("Pre�o")	
	public Double getPreco() {
		return preco;
	}

	@DisplayName("Valor consumo")
	public Double getValorconsumo() {
		return valorconsumo;
	}
	
	@DisplayName("Etapa de produ��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoetapanome")
	public Producaoetapanome getProducaoetapanome() {
		return producaoetapanome;
	}
	
	@OneToMany(mappedBy="materialproducao")
	public List<Materialproducaoformula> getListaMaterialproducaoformula() {
		return listaMaterialproducaoformula;
	}
	
	public void setListaMaterialproducaoformula(
			List<Materialproducaoformula> listaMaterialproducaoformula) {
		this.listaMaterialproducaoformula = listaMaterialproducaoformula;
	}

	public void setCdmaterialproducao(Integer cdmaterialproducao) {
		this.cdmaterialproducao = cdmaterialproducao;
	}
	
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	
	public void setMaterialmestre(Material materialmestre) {
		this.materialmestre = materialmestre;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setConsumo(Double consumo) {
		this.consumo = consumo;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public void setValorconsumo(Double valorconsumo) {
		this.valorconsumo = valorconsumo;
	}
	
	public void setProducaoetapanome(Producaoetapanome producaoetapanome) {
		this.producaoetapanome = producaoetapanome;
	}

	
//	TRANSIENTES
	
	@Transient
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	public Boolean getOpcional() {
		return opcional;
	}
	@DisplayName("Discriminar na nota")
	public Boolean getDiscriminarnota() {
		return discriminarnota;
	}
	public void setOpcional(Boolean opcional) {
		this.opcional = opcional;
	}
	public void setDiscriminarnota(Boolean discriminarnota) {
		this.discriminarnota = discriminarnota;
	}
	
	@DisplayName("Exibir na venda")
	public Boolean getExibirvenda() {
		return exibirvenda;
	}
	public void setExibirvenda(Boolean exibirvenda) {
		this.exibirvenda = exibirvenda;
	}
	@Transient
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	@Transient
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	@Transient
	public Double getQtdeprevista() {
		if(qtdeprevista == null){
			return getConsumo();
		}
		return qtdeprevista;
	}
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	
	@Transient
	public String getPrazoentregastr() {
		if(getPrazoentrega() != null){
			return new SimpleDateFormat("dd/MM/yyyy").format(getPrazoentrega());
		}
		return "";
	}
	
	@Transient
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	
	@Transient
	public Material getMaterialanterior() {
		return materialanterior;
	}
	public void setMaterialanterior(Material materialanterior) {
		this.materialanterior = materialanterior;
	}
	
	@Transient
	public Boolean getNaocalcularqtdeprevista() {
		return naocalcularqtdeprevista;
	}
	public void setNaocalcularqtdeprevista(Boolean naocalcularqtdeprevista) {
		this.naocalcularqtdeprevista = naocalcularqtdeprevista;
	}
	
	@Transient
	public Integer getAgitacao() {
		return agitacao;
	}
	public void setAgitacao(Integer agitacao) {
		this.agitacao = agitacao;
	}
	
	@Transient
	public Integer getQuantidadepercentual() {
		return quantidadepercentual;
	}
	public void setQuantidadepercentual(Integer quantidadepercentual) {
		this.quantidadepercentual = quantidadepercentual;
	}
	
	@Transient
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	@Transient
	public Double getFracaounidademedida() {
		return fracaounidademedida;
	}
	
	@Transient
	public Double getQtdereferenciaunidademedida() {
		return qtdereferenciaunidademedida;
	}
	
	public void setFracaounidademedida(Double fracaounidademedida) {
		this.fracaounidademedida = fracaounidademedida;
	}
	
	public void setQtdereferenciaunidademedida(
			Double qtdereferenciaunidademedida) {
		this.qtdereferenciaunidademedida = qtdereferenciaunidademedida;
	}
	
	@Transient
	public Double getVendaaltura() {
		return vendaaltura;
	}
	@Transient
	public Double getVendalargura() {
		return vendalargura;
	}
	@Transient
	public Double getVendacomprimento() {
		return vendacomprimento;
	}
	@Transient
	public Boolean getColeta() {
		return coleta;
	}	
	public void setVendaaltura(Double vendaaltura) {
		this.vendaaltura = vendaaltura;
	}
	public void setVendalargura(Double vendalargura) {
		this.vendalargura = vendalargura;
	}
	public void setVendacomprimento(Double vendacomprimento) {
		this.vendacomprimento = vendacomprimento;
	}
	public void setColeta(Boolean coleta) {
		this.coleta = coleta;
	}
	
	@DisplayName("Acomp. banda")
	public Boolean getAcompanhabanda() {
		return acompanhabanda;
	}
	public void setAcompanhabanda(Boolean acompanhabanda) {
		this.acompanhabanda = acompanhabanda;
	}
	@DisplayName("Carca�a")
	public Boolean getCarcaca() {
		return carcaca;
	}
	public void setCarcaca(Boolean carcaca) {
		this.carcaca = carcaca;
	}
}