package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Composi��o de M�o de Obra")
@SequenceGenerator(name = "sq_composicaomaoobra", sequenceName = "sq_composicaomaoobra")
public class Composicaomaoobra implements Log {

	protected Integer cdcomposicaomaoobra;
	protected Orcamento orcamento;
	protected Cargo cargo;
	protected Money salario;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Composicaomaoobraformula> listaComposicaomaoobraformula = new ListSet<Composicaomaoobraformula>(Composicaomaoobraformula.class);
	
	// TRANSIENTE
	protected Money custoporhora;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_composicaomaoobra")
	public Integer getCdcomposicaomaoobra() {
		return cdcomposicaomaoobra;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}

	@Required
	@DisplayName("Sal�rio")
	public Money getSalario() {
		return salario;
	}
	
	@DisplayName("Encargos")
	@OneToMany(mappedBy="composicaomaoobra")
	public List<Composicaomaoobraformula> getListaComposicaomaoobraformula() {
		return listaComposicaomaoobraformula;
	}
	
	public void setListaComposicaomaoobraformula(
			List<Composicaomaoobraformula> listaComposicaomaoobraformula) {
		this.listaComposicaomaoobraformula = listaComposicaomaoobraformula;
	}
	
	public void setSalario(Money salario) {
		this.salario = salario;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setCdcomposicaomaoobra(Integer cdcomposicaomaoobra) {
		this.cdcomposicaomaoobra = cdcomposicaomaoobra;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Custo por Hora")
	public Money getCustoporhora() {
		return custoporhora;
	}
	
	public void setCustoporhora(Money custoporhora) {
		this.custoporhora = custoporhora;
	}
	
}
