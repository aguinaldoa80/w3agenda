package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
@Entity
@SequenceGenerator(name="sq_grupotributacaonaturezaoperacao",sequenceName="sq_grupotributacaonaturezaoperacao")
public class Grupotributacaonaturezaoperacao{
	
	protected Integer cdgrupotributacaonaturezaoperacao;
	protected Grupotributacao grupotributacao;
	protected Naturezaoperacao naturezaoperacao;
	
	public Grupotributacaonaturezaoperacao(){}
	
	public Grupotributacaonaturezaoperacao(Grupotributacao grupotributacao, Naturezaoperacao naturezaoperacao) {
		this.grupotributacao = grupotributacao;
		this.naturezaoperacao = naturezaoperacao;
	}
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacaonaturezaoperacao")
	public Integer getCdgrupotributacaonaturezaoperacao() {
		return cdgrupotributacaonaturezaoperacao;
	}
	@DisplayName("Grupo tributação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}

	public void setCdgrupotributacaonaturezaoperacao(Integer cdgrupotributacaonaturezaoperacao) {
		this.cdgrupotributacaonaturezaoperacao = cdgrupotributacaonaturezaoperacao;
	}
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
}
