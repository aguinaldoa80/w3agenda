package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Legenda")
@SequenceGenerator(name = "sq_materiallegenda", sequenceName = "sq_materiallegenda")
public class Materiallegenda implements Log {
	
	protected Integer cdmateriallegenda;
	protected Integer ordem;
	protected String legenda;
	protected Arquivo simbolo;
	protected String processo;
	protected Set<Materiallegendamaterialtipo> listaMateriallegendamaterialtipo = new ListSet<Materiallegendamaterialtipo>(Materiallegendamaterialtipo.class);
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materiallegenda")
	public Integer getCdmateriallegenda() {
		return cdmateriallegenda;
	}
	
	@Required
	@MaxLength(5)
	@DescriptionProperty
	public String getLegenda() {
		return legenda;
	}
	
	@Required
	@DisplayName("S�mbolo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdsimbolo")
	public Arquivo getSimbolo() {
		return simbolo;
	}
	
	public String getProcesso() {
		return processo;
	}
	
	@DisplayName("Tipos")
	@OneToMany(fetch=FetchType.LAZY, mappedBy="materiallegenda")
	public Set<Materiallegendamaterialtipo> getListaMateriallegendamaterialtipo() {
		return listaMateriallegendamaterialtipo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setListaMateriallegendamaterialtipo(
			Set<Materiallegendamaterialtipo> listaMateriallegendamaterialtipo) {
		this.listaMateriallegendamaterialtipo = listaMateriallegendamaterialtipo;
	}

	public void setCdmateriallegenda(Integer cdmateriallegenda) {
		this.cdmateriallegenda = cdmateriallegenda;
	}

	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}

	public void setSimbolo(Arquivo simbolo) {
		this.simbolo = simbolo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}
	
}