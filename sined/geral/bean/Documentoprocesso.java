package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentoprocesso",sequenceName="sq_documentoprocesso")
@DisplayName("Documento/processo")
public class Documentoprocesso implements Log{
	
	//Tabela documentoprocesso
	protected Integer cddocumentoprocesso;
	protected String titulo;
	protected Documentoprocessotipo documentoprocessotipo;
	protected Documentoprocessosituacao documentoprocessosituacao;
	protected Usuario responsavel;
	protected Arquivo arquivo;
	protected String aprovadopor;
	protected String descricao;
	protected Timestamp  dtdocumento;
	protected Date vencimento;
	protected Date novovencimento;
	protected List<Documentoprocessoprojeto> listadocumentoprocessoprojeto;
	protected List<Documentoprocessomaterial> listadocumentoprocessomaterial;
	
	//Tabela documentoprocessohistorico		
	
	protected List<Documentoprocessohistorico> listahistorico;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	
	//Transient
	
	protected boolean revisao=false;
	protected Arquivo arquivorevisao;
	protected Boolean ehEditavel=false;
	protected List<Documentoprocesso> listaDocumentoprocesso; 
	
	public Documentoprocesso(){}
	
	public Documentoprocesso(Integer cddocumentoprocesso){
		this.cddocumentoprocesso = cddocumentoprocesso;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Id")
	@Id
	@GeneratedValue(generator="sq_documentoprocesso",strategy=GenerationType.AUTO)
	public Integer getCddocumentoprocesso() {
		return cddocumentoprocesso;
	}
	
	@Required
	@DisplayName("T�tulo")
	@MaxLength(100)
	@DescriptionProperty
	public String getTitulo() {
		return titulo;
	}
	
	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocessotipo")
	public Documentoprocessotipo getDocumentoprocessotipo() {
		return documentoprocessotipo;
	}
	
	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocessosituacao")
	public Documentoprocessosituacao getDocumentoprocessosituacao() {
		return documentoprocessosituacao;
	}
	
	@Required
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Usuario getResponsavel() {
		return responsavel;
	}
	
	@Required	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@DisplayName("Aprovado por")
	@MaxLength(100)
	public String getAprovadopor() {
		return aprovadopor;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(1000)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="documentoprocesso")
	public List<Documentoprocessohistorico> getListahistorico() {
		return listahistorico;
	}
	
	@DisplayName("Disponibilizado em")
	public Timestamp getDtdocumento() {
		return dtdocumento;
	}
	
	@Transient	
	public Arquivo getArquivorevisao() {
		return arquivorevisao;
	}
	
	@Transient
	public boolean getRevisao() {
		return revisao;
	}
	
	@Transient
	public Boolean getEhEditavel() {
		return ehEditavel;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCddocumentoprocesso(Integer cddocumentoprocesso) {
		this.cddocumentoprocesso = cddocumentoprocesso;
	}
	public void setTitulo(String titulo) {
		this.titulo = StringUtils.trimToNull(titulo);
	}
	public void setDocumentoprocessotipo(Documentoprocessotipo documentoprocessotipo) {
		this.documentoprocessotipo = documentoprocessotipo;
	}
	public void setDocumentoprocessosituacao(Documentoprocessosituacao documentoprocessosituacao) {
		this.documentoprocessosituacao = documentoprocessosituacao;
	}
	public void setResponsavel(Usuario responsavel) {
		this.responsavel = responsavel;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setAprovadopor(String aprovadopor) {
		this.aprovadopor = StringUtils.trimToNull(aprovadopor);
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
		
	public void setListahistorico(List<Documentoprocessohistorico> listahistorico) {
		this.listahistorico = listahistorico;
	}
	
	public void setDtdocumento(Timestamp dtdocumento) {
		this.dtdocumento = dtdocumento;
	}
	
	public void setArquivorevisao(Arquivo arquivorevisao) {
		this.arquivorevisao = arquivorevisao;
	}
	
	public void setRevisao(boolean revisao) {
		this.revisao = revisao;
	}
	
	public void setEhEditavel(Boolean ehEditavel) {
		this.ehEditavel = ehEditavel;
	}
	
	public Date getVencimento() {
		return vencimento;
	}
	
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	
	@Transient
	@DisplayName("Novo Vencimento")
	public Date getNovovencimento() {
		return novovencimento;
	}
	
	public void setNovovencimento(Date novovencimento) {
		this.novovencimento = novovencimento;
	}
	
	@OneToMany(mappedBy="documentoprocesso")
	public List<Documentoprocessoprojeto> getListadocumentoprocessoprojeto() {
		return listadocumentoprocessoprojeto;
	}
	public void setListadocumentoprocessoprojeto(
			List<Documentoprocessoprojeto> listadocumentoprocessoprojeto) {
		this.listadocumentoprocessoprojeto = listadocumentoprocessoprojeto;
	}
	
	@Transient
	public List<Documentoprocesso> getListaDocumentoprocesso() {
		return listaDocumentoprocesso;
	}

	public void setListaDocumentoprocesso(
			List<Documentoprocesso> listaDocumentoprocesso) {
		this.listaDocumentoprocesso = listaDocumentoprocesso;
	}

	@OneToMany(mappedBy="documentoprocesso")
	public List<Documentoprocessomaterial> getListadocumentoprocessomaterial() {
		return listadocumentoprocessomaterial;
	}
	public void setListadocumentoprocessomaterial(
			List<Documentoprocessomaterial> listadocumentoprocessomaterial) {
		this.listadocumentoprocessomaterial = listadocumentoprocessomaterial;
	}
}
