package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_notafiscalprodutorestricao", sequenceName = "sq_notafiscalprodutorestricao")
public class NotaFiscalProdutoRestricao {
	
	private Integer cdnotafiscalprodutorestricao;
	private Notafiscalproduto notafiscalproduto;
	private Usuario usuario;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutorestricao")
	public Integer getCdnotafiscalprodutorestricao() {
		return cdnotafiscalprodutorestricao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	@DisplayName("Usu�rio")
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	public void setCdnotafiscalprodutorestricao(Integer cdnotafiscalprodutorestricao) {
		this.cdnotafiscalprodutorestricao = cdnotafiscalprodutorestricao;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
