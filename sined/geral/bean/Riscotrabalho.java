package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_riscotrabalho", sequenceName = "sq_riscotrabalho")
@DisplayName("Risco no trabalho")
public class Riscotrabalho implements Log{

	protected Integer cdriscotrabalho;
	protected String nome;
	protected Riscotrabalhotipo riscotrabalhotipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_riscotrabalho")
	public Integer getCdriscotrabalho() {
		return cdriscotrabalho;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdriscotrabalhotipo")
	@DisplayName("Tipo")
	public Riscotrabalhotipo getRiscotrabalhotipo() {
		return riscotrabalhotipo;
	}
	
	public void setRiscotrabalhotipo(Riscotrabalhotipo riscotrabalhotipo) {
		this.riscotrabalhotipo = riscotrabalhotipo;
	}
	
	public void setCdriscotrabalho(Integer cdriscotrabalho) {
		this.cdriscotrabalho = cdriscotrabalho;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera=cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
