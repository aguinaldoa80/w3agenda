package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;


@Entity
@SequenceGenerator(name = "sq_custeio_processado", sequenceName = "sq_custeio_processado")
@DisplayName("Hist�rico do custeio")
public class CusteioProcessado implements Log{
	
	private Integer cdCusteioProcessado;
	private Empresa empresa;
	private Arquivo arquivo;
	private Date dtInicio;
	private Date dtFim;
	private SituacaoCusteio situacaoCusteio;
	private Date dtConclusao;
	private Usuario usuario;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	private List<CusteioHistorico> listaCusteioHistorico;
	private List<CusteioTipoDocumento> listaCusteioTipoDocumento;
	
	//transient
	private Rateio rateio;
	private Money valor;
	private String observacaohistorico;
	private List<CusteioTipoDocumento> listaTipoPagar = new ArrayList<CusteioTipoDocumento>();
	private List<CusteioTipoDocumento> listaTipoReceber = new ArrayList<CusteioTipoDocumento>();
	private List<CusteioTipoDocumento> listaTipoMovimentacao = new ArrayList<CusteioTipoDocumento>();
	private List<CusteioTipoDocumento> listaTipoEstoque = new ArrayList<CusteioTipoDocumento>();
	
	
	
	//inicio dos GET
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_custeio_processado")
	public Integer getCdCusteioProcessado() {
		return cdCusteioProcessado;
	}
	@DisplayName("Empresa")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Arquivo processado")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@DisplayName("Data de inicio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	public SituacaoCusteio getSituacaoCusteio() {
		return situacaoCusteio;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="custeioProcessado")
	public List<CusteioHistorico> getListaCusteioHistorico() {
		return listaCusteioHistorico;
	}
	@DisplayName("Tipo Documento")
	@OneToMany(mappedBy="custeioProcessado")
	public List<CusteioTipoDocumento> getListaCusteioTipoDocumento() {
		return listaCusteioTipoDocumento;
	}
	@DisplayName("Usu�rio")
	@JoinColumn(name = "cdusuario")
	@ManyToOne(fetch = FetchType.LAZY)
	public Usuario getUsuario() {
		return usuario;
	}
	@DisplayName("Data Conclus�o")
	public Date getDtConclusao() {
		return dtConclusao;
	}
	
	//inicio dos SET
	public void setDtConclusao(Date dtConclusao) {
		this.dtConclusao = dtConclusao;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setListaCusteioHistorico(List<CusteioHistorico> listaCusteioHistorico) {
		this.listaCusteioHistorico = listaCusteioHistorico;
	}
	public void setListaCusteioTipoDocumento(List<CusteioTipoDocumento> listaCusteioTipoDocumento) {
		this.listaCusteioTipoDocumento = listaCusteioTipoDocumento;
	}
	public void setCdCusteioProcessado(Integer cdCusteioProcessado) {
		this.cdCusteioProcessado = cdCusteioProcessado;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
	public void setSituacaoCusteio(SituacaoCusteio situacaoCusteio) {
		this.situacaoCusteio = situacaoCusteio;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	
	//Transient
	
	@Transient
	public String getDtCompleta(){	
		return SinedDateUtils.toString(getDtInicio()) + " at� " + SinedDateUtils.toString(getDtFim());
	}
	
	@Transient
	public Rateio getRateio() {
		return rateio;
	}
	@Transient
	public Money getValor() {
		return valor;
	}
	@Transient
	public String getObservacaohistorico() {
		return observacaohistorico;
	}
	
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}
	@Transient
	@DisplayName("Quantidade documentos")
	public Integer getQuantidadeDocumento() {
		if(listaCusteioTipoDocumento == null){
			return 0;
		}
		return listaCusteioTipoDocumento.size();
	}
	@Transient
	@DisplayName("Conta Pagar")
	public List<CusteioTipoDocumento> getListaTipoPagar() {
		return listaTipoPagar;
	}
	@Transient
	@DisplayName("Conta Receber")
	public List<CusteioTipoDocumento> getListaTipoReceber() {
		return listaTipoReceber;
	}
	@Transient
	@DisplayName("Movimenta��o Estoque")
	public List<CusteioTipoDocumento> getListaTipoEstoque() {
		return listaTipoEstoque;
	}
	@Transient
	@DisplayName("Movimenta��o Financeira")
	public List<CusteioTipoDocumento> getListaTipoMovimentacao() {
		return listaTipoMovimentacao;
	}
	public void setListaTipoReceber(List<CusteioTipoDocumento> listaTipoReceber) {
		this.listaTipoReceber = listaTipoReceber;
	}
	
	public void setListaTipoMovimentacao(
			List<CusteioTipoDocumento> listaTipoMovimentacao) {
		this.listaTipoMovimentacao = listaTipoMovimentacao;
	}
	
	public void setListaTipoEstoque(List<CusteioTipoDocumento> listaTipoEstoque) {
		this.listaTipoEstoque = listaTipoEstoque;
	}
	public void setListaTipoPagar(List<CusteioTipoDocumento> listaTipoPagar) {
		this.listaTipoPagar = listaTipoPagar;
	}
	
	
	
	
}
