package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Entity
public class Veiculousoescoltatipo {

	public static final Veiculousoescoltatipo DEDICADA = new Veiculousoescoltatipo(1, "Dedicada");
	public static final Veiculousoescoltatipo EVENTUAL = new Veiculousoescoltatipo(3, "Eventual");
	public static final Veiculousoescoltatipo COLETA = new Veiculousoescoltatipo(2, "Coleta");
	
	protected Integer cdveiculousoescoltatipo;
	protected String nome;
	
	public Veiculousoescoltatipo(){
	}

	public Veiculousoescoltatipo(Integer cdveiculousoescoltatipo, String nome){
		this.nome = nome;
		this.cdveiculousoescoltatipo = cdveiculousoescoltatipo;
	}
	
	@Id
	public Integer getCdveiculousoescoltatipo() {
		return cdveiculousoescoltatipo;
	}
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdveiculousoescoltatipo(Integer cdveiculousoescoltatipo) {
		this.cdveiculousoescoltatipo = cdveiculousoescoltatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}