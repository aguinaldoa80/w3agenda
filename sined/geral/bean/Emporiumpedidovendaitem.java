package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.util.Util;


@Entity
@SequenceGenerator(name = "sq_emporiumpedidovendaitem", sequenceName = "sq_emporiumpedidovendaitem")
public class Emporiumpedidovendaitem {

	protected Integer cdemporiumpedidovendaitem;
	protected Emporiumpedidovenda emporiumpedidovenda;
	protected Integer sequencial;
	protected Integer produto_id;
	protected String produto_descricao;
	protected Double preco_unitario;
	protected Double quantidade;
	protected Double total;
	protected Double desconto;
	protected Double valorvalecompra;
	protected String taxa_id;
	protected Double subtotal;
	protected String unidademedida;
	
	// TRANSIENTE
	protected Double desconto_it;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumpedidovendaitem")
	public Integer getCdemporiumpedidovendaitem() {
		return cdemporiumpedidovendaitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemporiumpedidovenda")
	public Emporiumpedidovenda getEmporiumpedidovenda() {
		return emporiumpedidovenda;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	@DisplayName("ID do produto")
	public Integer getProduto_id() {
		return produto_id;
	}

	@DisplayName("Descri��o")
	public String getProduto_descricao() {
		return produto_descricao;
	}

	@DisplayName("Valor unit�rio")
	public Double getPreco_unitario() {
		return preco_unitario;
	}

	@DisplayName("Qtde.")
	public Double getQuantidade() {
		return quantidade;
	}

	@DisplayName("Total")
	public Double getTotal() {
		return total;
	}

	@DisplayName("Taxa")
	public String getTaxa_id() {
		return taxa_id;
	}

	@DisplayName("Sub-total")
	public Double getSubtotal() {
		return subtotal;
	}

	@DisplayName("Unidade de medida")
	public String getUnidademedida() {
		return unidademedida;
	}
	
	public Double getDesconto() {
		return desconto;
	}
	
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
	@DisplayName("Vale compra")
	public Double getValorvalecompra() {
		return valorvalecompra;
	}

	public void setValorvalecompra(Double valorvalecompra) {
		this.valorvalecompra = valorvalecompra;
	}

	public void setCdemporiumpedidovendaitem(Integer cdemporiumpedidovendaitem) {
		this.cdemporiumpedidovendaitem = cdemporiumpedidovendaitem;
	}

	public void setEmporiumpedidovenda(Emporiumpedidovenda emporiumpedidovenda) {
		this.emporiumpedidovenda = emporiumpedidovenda;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public void setProduto_id(Integer produtoId) {
		produto_id = produtoId;
	}

	public void setProduto_descricao(String produtoDescricao) {
		produto_descricao = Util.strings.tiraAcento(produtoDescricao);
	}

	public void setPreco_unitario(Double precoUnitario) {
		preco_unitario = precoUnitario;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public void setTaxa_id(String taxaId) {
		taxa_id = Util.strings.tiraAcento(taxaId);
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public void setUnidademedida(String unidademedida) {
		this.unidademedida = Util.strings.tiraAcento(unidademedida);
	}
	
	@Transient
	public Double getDesconto_it() {
		return desconto_it;
	}
	
	public void setDesconto_it(Double descontoIt) {
		desconto_it = descontoIt;
	}

}
