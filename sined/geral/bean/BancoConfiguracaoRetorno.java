package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoCriarMovimentacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoDataMovimentacaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretorno",sequenceName="sq_bancoconfiguracaoretorno")
public class BancoConfiguracaoRetorno implements Log {

	private Integer cdbancoconfiguracaoretorno;
	private String nome;
	private Banco banco;
	private Integer tamanho;
	private BancoConfiguracaoRemessaTipoEnum tipo;
	private BancoConfiguracaoRetornoCriarMovimentacaoEnum criarMovimentacao;
	private BancoConfiguracaoRetornoDataMovimentacaoEnum considerardatamovimentacao;
	private Boolean incluirJurosMultaDescontoTaxasDocumento;
	private Boolean atualizarjurosmulta;
//	private Boolean multisegmentos;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;	
	
	private Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
	private Set<BancoConfiguracaoRetornoOcorrencia> listaBancoConfiguracaoRetornoOcorrencia = new ListSet<BancoConfiguracaoRetornoOcorrencia>(BancoConfiguracaoRetornoOcorrencia.class);
	private Set<BancoConfiguracaoRetornoRejeicao> listaBancoConfiguracaoRetornoRejeicao = new ListSet<BancoConfiguracaoRetornoRejeicao>(BancoConfiguracaoRetornoRejeicao.class);
	private Set<BancoConfiguracaoRetornoIdentificador> listaBancoConfiguracaoRetornoIdentificador = new ListSet<BancoConfiguracaoRetornoIdentificador>(BancoConfiguracaoRetornoIdentificador.class);
	private Set<BancoConfiguracaoRetornoSegmentoDetalhe> listaBancoConfiguracaoRetornoSegmentoDetalhe = new ListSet<BancoConfiguracaoRetornoSegmentoDetalhe>(BancoConfiguracaoRetornoSegmentoDetalhe.class);
	
	//Transient
	private String msgEstrutura;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretorno")
	public Integer getCdbancoconfiguracaoretorno() {
		return cdbancoconfiguracaoretorno;
	}

	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	@Required
	public Banco getBanco() {
		return banco;
	}
	
	@MaxLength(9)
	@Required
	public Integer getTamanho() {
		return tamanho;
	}
	
	@Required
	public BancoConfiguracaoRemessaTipoEnum getTipo() {
		return tipo;
	}
	
	@DisplayName("Criar movimentação")
	public BancoConfiguracaoRetornoCriarMovimentacaoEnum getCriarMovimentacao() {
		return criarMovimentacao;
	}
	
	@Required
	@DisplayName("Considerar na data de movimentação")
	public BancoConfiguracaoRetornoDataMovimentacaoEnum getConsiderardatamovimentacao() {
		return considerardatamovimentacao;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRetorno")
	public Set<BancoConfiguracaoRetornoCampo> getListaBancoConfiguracaoRetornoCampo() {
		return listaBancoConfiguracaoRetornoCampo;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRetorno")
	public Set<BancoConfiguracaoRetornoOcorrencia> getListaBancoConfiguracaoRetornoOcorrencia() {
		return listaBancoConfiguracaoRetornoOcorrencia;
	}
	
	@OneToMany(mappedBy="bancoConfiguracaoRetorno")
	public Set<BancoConfiguracaoRetornoRejeicao> getListaBancoConfiguracaoRetornoRejeicao() {
		return listaBancoConfiguracaoRetornoRejeicao;
	}
	
	public void setCdbancoconfiguracaoretorno(Integer cdbancoconfiguracaoretorno) {
		this.cdbancoconfiguracaoretorno = cdbancoconfiguracaoretorno;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	
	public void setTipo(BancoConfiguracaoRemessaTipoEnum tipo) {
		this.tipo = tipo;
	}
	
	public void setCriarMovimentacao(BancoConfiguracaoRetornoCriarMovimentacaoEnum criarMovimentacao) {
		this.criarMovimentacao = criarMovimentacao;
	}
	
	public void setConsiderardatamovimentacao(BancoConfiguracaoRetornoDataMovimentacaoEnum considerardatamovimentacao) {
		this.considerardatamovimentacao = considerardatamovimentacao;
	}
	
	public void setListaBancoConfiguracaoRetornoCampo(
			Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo) {
		this.listaBancoConfiguracaoRetornoCampo = listaBancoConfiguracaoRetornoCampo;
	}
	
	public void setListaBancoConfiguracaoRetornoOcorrencia(
			Set<BancoConfiguracaoRetornoOcorrencia> listaBancoConfiguracaoRetornoOcorrencia) {
		this.listaBancoConfiguracaoRetornoOcorrencia = listaBancoConfiguracaoRetornoOcorrencia;
	}
	
	public void setListaBancoConfiguracaoRetornoRejeicao(
			Set<BancoConfiguracaoRetornoRejeicao> listaBancoConfiguracaoRetornoRejeicao) {
		this.listaBancoConfiguracaoRetornoRejeicao = listaBancoConfiguracaoRetornoRejeicao;
	}

	
	@OneToMany(mappedBy="bancoConfiguracaoRetorno")
	public Set<BancoConfiguracaoRetornoSegmentoDetalhe> getListaBancoConfiguracaoRetornoSegmentoDetalhe() {
		return listaBancoConfiguracaoRetornoSegmentoDetalhe;
	}

	public void setListaBancoConfiguracaoRetornoSegmentoDetalhe(
			Set<BancoConfiguracaoRetornoSegmentoDetalhe> listaBancoConfiguracaoRetornoSegmentoDetalhe) {
		this.listaBancoConfiguracaoRetornoSegmentoDetalhe = listaBancoConfiguracaoRetornoSegmentoDetalhe;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public BancoConfiguracaoRetornoCampo getCampo(BancoConfiguracaoCampoEnum campo){
		BancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo = null;
		if (listaBancoConfiguracaoRetornoCampo != null){
			for (BancoConfiguracaoRetornoCampo bcrc : listaBancoConfiguracaoRetornoCampo){
				if (bcrc.getCampo() != null && bcrc.getCampo().equals(campo)){
					bancoConfiguracaoRetornoCampo = bcrc;
					break;
				}
			}
		}
		return bancoConfiguracaoRetornoCampo;
	}
	//teste
	public BancoConfiguracaoRetornoIdentificador getCampo2(BancoConfiguracaoCampoEnum campo){
		BancoConfiguracaoRetornoIdentificador bancoConfiguracaoRetornoId = null;
		if (listaBancoConfiguracaoRetornoIdentificador != null){
			for (BancoConfiguracaoRetornoIdentificador bcri : listaBancoConfiguracaoRetornoIdentificador){
				if (bcri.getTipo() != null && bcri.getTipo().equals(campo)){
					bancoConfiguracaoRetornoId = bcri;
					break;
				}
			}
		}
		return bancoConfiguracaoRetornoId;
	}
	
	public BancoConfiguracaoRetornoOcorrencia getOcorrencia(BancoConfiguracaoRetornoAcaoEnum acao, String codocorrencia){
		if(codocorrencia != null && codocorrencia.indexOf(",") != -1){
			codocorrencia = codocorrencia.substring(0, codocorrencia.indexOf(","));
		}
		
		BancoConfiguracaoRetornoOcorrencia bancoConfiguracaoRetornoOcorrencia = null;
		if (listaBancoConfiguracaoRetornoOcorrencia != null){
			for (BancoConfiguracaoRetornoOcorrencia bcro : listaBancoConfiguracaoRetornoOcorrencia){
				if (bcro.getAcao() != null && bcro.getAcao().equals(acao) && 
						(codocorrencia == null || codocorrencia.equals(bcro.getCodigo()))){
					bancoConfiguracaoRetornoOcorrencia = bcro;
					break;
				}
			}
		}
		return bancoConfiguracaoRetornoOcorrencia;
	}
	
	public List<String> getIdentificadoresByTipo(BancoConfiguracaoTipoSegmentoEnum tipo){
		List<String> listaIdentificadores = new ArrayList<String>();
		if (listaBancoConfiguracaoRetornoCampo != null){
			for (BancoConfiguracaoRetornoCampo bcrc : listaBancoConfiguracaoRetornoCampo){
				BancoConfiguracaoRetornoSegmento segmento = bcrc.getBancoConfiguracaoRetornoSegmento();
				if (segmento != null && segmento.getTipo() != null && segmento.getTipo().equals(tipo))
					if (!listaIdentificadores.contains(segmento.getIdentificador()))
						listaIdentificadores.add(segmento.getIdentificador());
			}
		}
		return listaIdentificadores;
	}
	public List<String> getIdentificadoresByTipo2(BancoConfiguracaoTipoSegmentoEnum tipo){
		List<String> listaIdentificadores = new ArrayList<String>();
		if (listaBancoConfiguracaoRetornoSegmentoDetalhe != null){
			for (BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : listaBancoConfiguracaoRetornoSegmentoDetalhe){
				BancoConfiguracaoRetornoSegmento segmento = bcrsd.getBancoConfiguracaoRetornoSegmento();
				if (segmento != null && segmento.getTipo() != null && segmento.getTipo().equals(tipo))
					if (!listaIdentificadores.contains(segmento.getIdentificador()))
						listaIdentificadores.add(segmento.getIdentificador());
			}
		}
		return listaIdentificadores;
	}
	
	public boolean isLinhaContainsIdentificador(String linha, BancoConfiguracaoRetornoSegmento segmento){
		boolean encontrou = false;
		
		if (segmento != null && segmento.getTipo() != null){
			
			BancoConfiguracaoCampoEnum identificador = null;
			if (BancoConfiguracaoTipoSegmentoEnum.HEADER.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER;
			else if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE;
			else if (BancoConfiguracaoTipoSegmentoEnum.TRAILER.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER;			
			
			BancoConfiguracaoRetornoCampo identificadorCampo = getCampo(identificador);
			if (linha != null && identificadorCampo != null && segmento.getIdentificador() != null){
				 if (segmento.getIdentificador().equals(identificadorCampo.getValorString(linha)))
					 encontrou = true;
			}
		}
		return encontrou;
	}
	public boolean isLinhaContainsIdentificador2(String linha, BancoConfiguracaoRetornoSegmento segmento){
		boolean encontrou = false;
		
		if (segmento != null && segmento.getTipo() != null){
			
			BancoConfiguracaoCampoEnum identificador = null;
			if (BancoConfiguracaoTipoSegmentoEnum.HEADER.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER;
			else if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE;
			else if (BancoConfiguracaoTipoSegmentoEnum.TRAILER.equals(segmento.getTipo()))
				identificador = BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER;			
			
			BancoConfiguracaoRetornoIdentificador identificadorCampo = getCampo2(identificador);
			if (linha != null && identificadorCampo != null && segmento.getIdentificador() != null){
				if (segmento.getIdentificador().equals(identificadorCampo.getValorString(linha)))
					encontrou = true;
			}
		}
		return encontrou;
	}
		
	public String getValorCampoLinha(String linha, BancoConfiguracaoCampoEnum campo){
		String valorCampo = null;
		if (linha != null && !linha.isEmpty()){
			BancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo = getCampo(campo);
			if (bancoConfiguracaoRetornoCampo != null && bancoConfiguracaoRetornoCampo.getBancoConfiguracaoRetornoSegmento() != null){				
				if (isLinhaContainsIdentificador(linha, bancoConfiguracaoRetornoCampo.getBancoConfiguracaoRetornoSegmento())){						
					valorCampo = bancoConfiguracaoRetornoCampo.getValorString(linha);
				}
			}
		}
		return valorCampo;
	}
	public String getValorCampoLinha2(String linha, BancoConfiguracaoCampoEnum campo){
		String valorCampo = null;
		if (linha != null && !linha.isEmpty()){
			BancoConfiguracaoRetornoIdentificador bancoConfiguracaoRetornoIdentificador = getCampo2(campo);
			if (bancoConfiguracaoRetornoIdentificador != null && bancoConfiguracaoRetornoIdentificador.getBancoConfiguracaoRetorno()!= null && !bancoConfiguracaoRetornoIdentificador.getBancoConfiguracaoRetorno().getListaBancoConfiguracaoRetornoSegmentoDetalhe().isEmpty()){
				for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : bancoConfiguracaoRetornoIdentificador.getBancoConfiguracaoRetorno().getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
					if (isLinhaContainsIdentificador(linha, bcrsd.getBancoConfiguracaoRetornoSegmento())){						
						valorCampo = bancoConfiguracaoRetornoIdentificador.getValorString(linha);
					}
				}
			}
		}
		return valorCampo;
	}
	
	@Transient
	public String getMsgEstrutura() {
		return msgEstrutura;
	}
	
	public void setMsgEstrutura(String msgEstrutura) {
		this.msgEstrutura = msgEstrutura;
	}
	
	@DisplayName("Incluir juros, multa e desconto do retorno na aba taxas do documento")
	public Boolean getIncluirJurosMultaDescontoTaxasDocumento() {
		return incluirJurosMultaDescontoTaxasDocumento;
	}
	
	public void setIncluirJurosMultaDescontoTaxasDocumento(Boolean incluirJurosMultaDescontoTaxasDocumento) {
		this.incluirJurosMultaDescontoTaxasDocumento = incluirJurosMultaDescontoTaxasDocumento;
	}
	
	@DisplayName("Atualiza juros/multa")
	public Boolean getAtualizarjurosmulta() {
		return atualizarjurosmulta;
	}
	
	public void setAtualizarjurosmulta(Boolean atualizarjurosmulta) {
		this.atualizarjurosmulta = atualizarjurosmulta;
	}

/*	public Boolean getMultisegmentos() {
		return multisegmentos;
	}

	public void setMultisegmentos(Boolean multisegmentos) {
		this.multisegmentos = multisegmentos;
	}*/

	@OneToMany(mappedBy="bancoConfiguracaoRetorno")
	public Set<BancoConfiguracaoRetornoIdentificador> getListaBancoConfiguracaoRetornoIdentificador() {
		return listaBancoConfiguracaoRetornoIdentificador;
	}

	public void setListaBancoConfiguracaoRetornoIdentificador(
			Set<BancoConfiguracaoRetornoIdentificador> listaBancoConfiguracaoRetornoIdentificador) {
		this.listaBancoConfiguracaoRetornoIdentificador = listaBancoConfiguracaoRetornoIdentificador;
	}
	
}