package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_cfop", sequenceName = "sq_cfop")
public class Cfop implements Log{

	protected Integer cdcfop;
	protected Cfoptipo cfoptipo;
	protected Cfopescopo cfopescopo;
	protected String codigo;
	protected String descricaoresumida;
	protected String descricaocompleta;
	protected Boolean incluirvalorfinal;
	protected Boolean atualizarvalor = Boolean.TRUE;
	protected List<CfopRelacionado> listacfoprelacionado = new ArrayList<CfopRelacionado>();
	protected List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = new ArrayList<Naturezaoperacaocfop>();
	protected Boolean naoconsiderarreceita;
	protected Contagerencial contaGerencial;
	
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	public Cfop() {
	}
	
	public Cfop(Integer cdcfop) {
		this.cdcfop = cdcfop;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cfop")
	public Integer getCdcfop() {
		return cdcfop;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfoptipo")
	public Cfoptipo getCfoptipo() {
		return cfoptipo;
	}

	@Required
	@DisplayName("Escopo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcfopescopo")
	public Cfopescopo getCfopescopo() {
		return cfopescopo;
	}

	@Required
	@MaxLength(6)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@Required
	@MaxLength(400)
	@DisplayName("Descri��o resumida")
	public String getDescricaoresumida() {
		return descricaoresumida;
	}

	@Required
	@MaxLength(800)
	@DisplayName("Descri��o completa")
	public String getDescricaocompleta() {
		return descricaocompleta;
	}

	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	
	@DisplayName("Incluir no valor")
	public Boolean getIncluirvalorfinal() {
		return incluirvalorfinal;
	}
	
	@DisplayName("Atualizar valor")
	public Boolean getAtualizarvalor() {
		return atualizarvalor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	@DisplayName("Conta gerencial para estoque")
	@Required
	public Contagerencial getContaGerencial() {
		return contaGerencial;
	}
	
	public void setContaGerencial(Contagerencial contaGerencial) {
		this.contaGerencial = contaGerencial;
	}
	public void setAtualizarvalor(Boolean atualizarvalor) {
		this.atualizarvalor = atualizarvalor;
	}

	public void setIncluirvalorfinal(Boolean incluirvalorfinal) {
		this.incluirvalorfinal = incluirvalorfinal;
	}

	public void setCdcfop(Integer cdcfop) {
		this.cdcfop = cdcfop;
	}

	public void setCfoptipo(Cfoptipo cfoptipo) {
		this.cfoptipo = cfoptipo;
	}

	public void setCfopescopo(Cfopescopo cfopescopo) {
		this.cfopescopo = cfopescopo;
	}

	public void setCodigo(String codigo) {
		if(codigo != null) this.codigo = codigo.trim();
	}

	public void setDescricaoresumida(String descricaoresumida) {
		this.descricaoresumida = descricaoresumida.trim();;
	}

	public void setDescricaocompleta(String descricaocompleta) {
		this.descricaocompleta = descricaocompleta.trim();;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"descricaoresumida", "codigo", "naoconsiderarreceita"})
	public String getDescricaoCodigo(){
		return codigo + " - " + descricaoresumida; 
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdcfop == null) ? 0 : cdcfop.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cfop other = (Cfop) obj;
		if (cdcfop == null) {
			if (other.cdcfop != null)
				return false;
		} else if (!cdcfop.equals(other.cdcfop))
			return false;
		return true;
	}

	@DisplayName("Relacionamento")
	@OneToMany(mappedBy="cfop")
	public List<CfopRelacionado> getListacfoprelacionado() {
		return listacfoprelacionado;
	}

	public void setListacfoprelacionado(List<CfopRelacionado> listacfoprelacionado) {
		this.listacfoprelacionado = listacfoprelacionado;
	}
	
	@OneToMany(mappedBy="cfop", fetch=FetchType.LAZY)
	public List<Naturezaoperacaocfop> getListaNaturezaoperacaocfop() {
		return listaNaturezaoperacaocfop;
	}
	
	public void setListaNaturezaoperacaocfop(List<Naturezaoperacaocfop> listaNaturezaoperacaocfop) {
		this.listaNaturezaoperacaocfop = listaNaturezaoperacaocfop;
	}

	@DisplayName("N�o compor receita de NF")
	public Boolean getNaoconsiderarreceita() {
		return naoconsiderarreceita;
	}
	public void setNaoconsiderarreceita(Boolean naoconsiderarreceita) {
		this.naoconsiderarreceita = naoconsiderarreceita;
	}
	@Transient
	public boolean isCodigoIniciadoCom7(){
		return codigo!=null && codigo.startsWith("7");
	}
}