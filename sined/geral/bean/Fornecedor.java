package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@JoinEmpresa("fornecedor.listaFornecedorempresa.empresa")
public class Fornecedor extends Pessoa implements User, Log{
	
	protected String razaosocial;
	protected Uf ufinscricaoestadual;
	protected String inscricaoestadual;
	protected String inscricaomunicipal;
	
	protected String fax;
	protected String site;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected String login;
	protected String senha;
	protected Boolean acesso;
	protected Boolean transportador;
	protected Boolean correios;
	protected Money valorfrete;
	protected Boolean associacao;
	protected Boolean captador;
	protected Fornecedor captadorassociacao;
	protected Sexo sexo;
	protected Naturezaoperacao naturezaoperacao;
	protected Boolean credenciadora;
	protected Boolean fabricante;
	protected Integer codigoParceiro;
	protected Double valorTicketMedio;
	
	protected Set<Restricao> listaRestricao;
	protected Set<Proposta> listaProposta;
	protected Boolean agenciavenda;
	protected String identificador;
	protected Contagerencial contagerencial;
	protected Contagerencial contagerencialcredito;
	protected ContaContabil contaContabil;
	protected List<Materialfornecedor> listaMaterialfornecedor;
	protected List<Empresarepresentacao> listaEmpresarepresentacao;
	protected List<ColaboradorFornecedor> listaColaboradorFornecedor;
	protected List<Fornecedorempresa> listaFornecedorempresa;
	
	//Transient
	protected Boolean isNotFornecedor;
	protected String listaTelefones;
	protected String cpfcnpj;
	protected String ativosrelatorio;
	protected String telefones;
	protected String tipopessoarel;
	protected Endereco enderecoPrincipal;
	protected String contatos;
	protected Boolean isIdentificadorGerado;
	protected Pessoaquestionario pessoaquestionariotrans;
	protected List<Fornecedorhistorico> historico;
	protected String observacaohistorico;
	private boolean possuiUsuario;
	protected Integer idEcommerce;
	
	public Fornecedor() {}
	public Fornecedor(Integer cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public Fornecedor(String nome) {
		this.nome = nome;
	}
	
	public Fornecedor(Integer cdpessoa, String nome) {
		this.cdpessoa = cdpessoa;
		this.nome = nome;
	}
	
	/**
	 * Sobrescrito para retirar a obrigatoriedade em Cliente
	 * @author Fl�vio Tavares
	 */
	@Override
	@Transient
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return super.getCnpj();
	}
	/**
	 * Sobrescrito para retirar a obrigatoriedade em Cliente
	 * @author Fl�vio Tavares
	 */
	@Override
	@Transient
	@DisplayName("CPF")
	public Cpf getCpf() {
		return super.getCpf();
	}
	

	@Transient
	public Boolean isNotFornecedor() {
		return isNotFornecedor;
	}
	
	@DisplayName("Raz�o social")
	@MaxLength(100)
	public String getRazaosocial() {
		return razaosocial;
	}

	@DisplayName("Inscri��o estadual")
	@MaxLength(20)
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}

	@DisplayName("Inscri��o municipal")
	@MaxLength(20)
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}

	@DisplayName("Fax")
	@MaxLength(20)
	public String getFax(){
		return fax;
	}

	@DisplayName("Site")
	@MaxLength(50)
	public String getSite() {
		return site;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	@OneToMany(mappedBy="pessoa")
	@DisplayName("Restri��es")
	public Set<Restricao> getListaRestricao() {
		return listaRestricao;
	}
	@OneToMany(mappedBy="cliente")
	public Set<Proposta> getListaProposta() {
		return listaProposta;
	}
	
	public void setListaProposta(Set<Proposta> listaProposta) {
		this.listaProposta = listaProposta;
	}
	@DisplayName("Telefones")
	@Transient
	public String getListaTelefones() {
		return listaTelefones;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@MaxLength(50)
	public String getLogin() {
		return login;
	}
	
	@MaxLength(64)
	public String getSenha() {
		return senha;
	}
	
	public Boolean getAcesso() {
		return acesso;
	}
	
	@Transient
	public String getPassword() {
		return getSenha();
	}
	@DisplayName("Transportador")
	public Boolean getTransportador() {
		return transportador;
	}
	@DisplayName("Correios")
	public Boolean getCorreios() {
		return correios;
	}
	@DisplayName("Frete (por Km)")
	public Money getValorfrete() {
		return valorfrete;
	}
	@OneToMany(mappedBy="fornecedor", fetch = FetchType.LAZY)
	@DisplayName("Hist�rico")
	public List<Fornecedorhistorico> getHistorico() {
		return historico;
	}
	@Transient
	@DisplayName("Hist�rico")
	@MaxLength(100)
	public String getObservacaohistorico() {
		return observacaohistorico;
	}	
	public void setValorfrete(Money frete) {
		this.valorfrete = frete;
	}
	public void setTransportador(Boolean transportador) {
		this.transportador = transportador;
	}
	public void setCorreios(Boolean correios) {
		this.correios = correios;
	}
	public void setAcesso(Boolean acesso) {
		this.acesso = acesso;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	@DisplayName("Ag�ncia venda")
	public Boolean getAgenciavenda() {
		return agenciavenda;
	}
	public void setAgenciavenda(Boolean agenciavenda) {
		this.agenciavenda = agenciavenda;
	}
	@MaxLength(11)
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = StringUtils.trimToNull(identificador);
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialcredito")
	@DisplayName("Conta gerencial Cr�dito")
	public Contagerencial getContagerencialcredito() {
		return contagerencialcredito;
	}
	public void setContagerencialcredito(Contagerencial contagerencialcredito) {
		this.contagerencialcredito = contagerencialcredito;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	@DisplayName("Conta cont�bil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	
	@Transient
	public String getTelefones() {
		Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone = getListaTelefone();
		String telefones = "";
		if(listaTelefone != null && !listaTelefone.isEmpty())
			for (br.com.linkcom.sined.geral.bean.Telefone telefone : listaTelefone) {
				telefones += telefone.getTelefone().toString() + (telefone.getTelefonetipo() != null ? "(" + telefone.getTelefonetipo().getNome() + ")" : "") + "\n";
			}
		return telefones;
	}
	
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	
	@Transient
	public String getAtivosrelatorio() {
		return ativosrelatorio;
	}
	
	public void setAtivosrelatorio(String ativosrelatorio) {
		this.ativosrelatorio = ativosrelatorio;
	}
	
	@Transient
	public String getTipopessoarel() {
		return tipopessoarel;
	}
	
	public void setTipopessoarel(String tipopessoarel) {
		this.tipopessoarel = tipopessoarel;
	}
	
	@Transient
	@DisplayName("CNPJ/CPF")
	public String getCpfcnpj() {
		return cpfcnpj;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufinscricaoestadual")
	public Uf getUfinscricaoestadual() {
		return ufinscricaoestadual;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnaturezaoperacao")
	@DisplayName("Natureza da Opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	
	public void setUfinscricaoestadual(Uf ufinscricaoestadual) {
		this.ufinscricaoestadual = ufinscricaoestadual;
	}
	
	public void setCpfcnpj(String cpfcnpj) {
		this.cpfcnpj = cpfcnpj;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = StringUtils.trimToNull(razaosocial);
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setListaTelefones(String listaTelefones) {
		this.listaTelefones = listaTelefones;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setListaRestricao(Set<Restricao> listaRestricao) {
		this.listaRestricao = listaRestricao;
	}
	public void setIsNotFornecedor(Boolean isNotFornecedor) {
		this.isNotFornecedor = isNotFornecedor;
	}
	public void setHistorico(List<Fornecedorhistorico> historico) {
		this.historico = historico;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Fornecedor) {
			Fornecedor fornecedor = (Fornecedor) obj;
			return this.getCdpessoa().equals(fornecedor.getCdpessoa());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdpessoa != null) {
			return cdpessoa.hashCode();
		}
		return super.hashCode();
	}

	@Transient
	public Endereco getEnderecoPrincipal() {
		return enderecoPrincipal;
	}
	public void setEnderecoPrincipal(Endereco enderecoPrincipal) {
		this.enderecoPrincipal = enderecoPrincipal;
	}
	
	@Transient
	public String getContatos() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			StringBuilder sb = new StringBuilder("");
			for (PessoaContato pessoaContato : this.listaContato) 
				if (pessoaContato.getContato() != null) {
					sb.append(pessoaContato.getContato().getNome()).append(", ");
				}
			return sb.substring(0, sb.length()-2).toString();
		}
		return contatos;
	}
	public void setContatos(String contatos) {
		this.contatos = contatos;
	}
	
	@Transient
	public String getFornecedorNomeCpf(){
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}
	
	@Transient
	public String getIdentificadorNomeCpf(){
		StringBuilder s = new StringBuilder();
		if (this.getIdentificador() != null){
			s.append(this.getIdentificador()).append(" - ");
		}
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}
	
	@Transient
	public Boolean getIsIdentificadorGerado() {
		return isIdentificadorGerado;
	}
	public void setIsIdentificadorGerado(Boolean isIdentificadorGerado) {
		this.isIdentificadorGerado = isIdentificadorGerado;
	}
	@Transient
	public String getNomeCpfOuCnpj(){
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCnpj() != null){
			s.append(" - ").append(this.getCnpj());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}
	
	@Transient
	public String getTelefonesHtml() {
		Set<br.com.linkcom.sined.geral.bean.Telefone> listaTelefone = getListaTelefone();
		StringBuilder telefones = new StringBuilder("");
		if (listaTelefone != null && !listaTelefone.isEmpty()) {
			for (br.com.linkcom.sined.geral.bean.Telefone telefone : listaTelefone) {
				if(telefone.getTelefonetipo() != null)
					telefones.append(telefone.getTelefone().toString() + "(" + telefone.getTelefonetipo().getNome()).append(")<BR>");
			}
		}
		return telefones.toString();
	}
	
	@Transient
	public Pessoaquestionario getPessoaquestionariotrans() {
		return pessoaquestionariotrans;
	}
	public void setPessoaquestionariotrans(
			Pessoaquestionario pessoaquestionariotrans) {
		this.pessoaquestionariotrans = pessoaquestionariotrans;
	}
	
	@OneToMany(mappedBy="fornecedor")
	public List<Materialfornecedor> getListaMaterialfornecedor() {
		return listaMaterialfornecedor;
	}
	
	@OneToMany(mappedBy="fornecedor")
	public List<Empresarepresentacao> getListaEmpresarepresentacao() {
		return listaEmpresarepresentacao;
	}
	
	public void setListaMaterialfornecedor(List<Materialfornecedor> listaMaterialfornecedor) {
		this.listaMaterialfornecedor = listaMaterialfornecedor;
	}
	
	public void setListaEmpresarepresentacao(List<Empresarepresentacao> listaEmpresarepresentacao) {
		this.listaEmpresarepresentacao = listaEmpresarepresentacao;
	}
	
	@Transient
	public String getAutocompleteEmpresa(){
		String cdpessoa = this.cdpessoa != null ? this.cdpessoa.toString() : "";
		String nome = this.nome != null ? this.nome.toString() : "";
		return cdpessoa + " - " + nome;
	}
	
	@Transient
	public String getAutocomplete(){
		String nome = this.nome != null ? this.nome.toString() : "";
		return nome;
	}
	
	@Transient
	public boolean isPossuiUsuario() {
		return possuiUsuario;
	}

	public void setPossuiUsuario(boolean possuiUsuario) {
		this.possuiUsuario = possuiUsuario;
	}
	
	@OneToMany(mappedBy="fornecedor")	
	@DisplayName("Colaborador")
	public List<ColaboradorFornecedor> getListaColaboradorFornecedor() {
		return listaColaboradorFornecedor;
	}
	
	public void setListaColaboradorFornecedor(
			List<ColaboradorFornecedor> listaColaboradorFornecedor) {
		this.listaColaboradorFornecedor = listaColaboradorFornecedor;
	}
	
	@OneToMany(mappedBy="fornecedor")	
	@DisplayName("Empresa")
	public List<Fornecedorempresa> getListaFornecedorempresa() {
		return listaFornecedorempresa;
	}
	public void setListaFornecedorempresa(List<Fornecedorempresa> listaFornecedorempresa) {
		this.listaFornecedorempresa = listaFornecedorempresa;
	}
	
	@Transient
	public String getContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getNome() != null){
					return pessoaContato.getPessoa().getNome();
				}
			}
		}
		return "";
	}
	
	@Transient
	public String getEmailContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getEmail() != null){
					return pessoaContato.getPessoa().getEmail();
				}
			}
		}
		return "";
	}
	
	@Transient
	public String getTelefoneContatoEspecifico() {
		if(this.listaContato != null && !this.listaContato.isEmpty()){
			for (PessoaContato pessoaContato : this.listaContato) {
				if(pessoaContato.getPessoa() != null && pessoaContato.getPessoa().getTelefones() != null){
					return pessoaContato.getPessoa().getTelefones();
				}
			}
		}
		return "";
	}
	
	@DisplayName("Associa��o")
	public Boolean getAssociacao() {
		return associacao;
	}
	@DisplayName("Captador")
	public Boolean getCaptador() {
		return captador;
	}
	@DisplayName("Captador da Associa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcaptadorassociacao")
	public Fornecedor getCaptadorassociacao() {
		return captadorassociacao;
	}
	@DisplayName("Sexo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")
	public Sexo getSexo() {
		return sexo;
	}
	
	public void setAssociacao(Boolean associacao) {
		this.associacao = associacao;
	}
	public void setCaptador(Boolean captador) {
		this.captador = captador;
	}
	public void setCaptadorassociacao(Fornecedor captadorassociacao) {
		this.captadorassociacao = captadorassociacao;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	@Transient
	public String getRazaoSocialOrNomeByTipopessoa() {
		if(this.tipopessoa != null){
			if(this.tipopessoa.equals(Tipopessoa.PESSOA_JURIDICA)) 
				return this.razaosocial;
			else 
				return this.nome;
		} else return this.razaosocial;
	}
	
	public Boolean getCredenciadora() {
		return credenciadora;
	}
	
	public void setCredenciadora(Boolean credenciadora) {
		this.credenciadora = credenciadora;
	}
	
	public Boolean getFabricante() {
		return fabricante;
	}
	
	public void setFabricante(Boolean fabricante) {
		this.fabricante = fabricante;
	}
	@DisplayName("C�DIGO PARCEIRO")
	public Integer getCodigoParceiro() {
		return codigoParceiro;
	}
	public void setCodigoParceiro(Integer codigoParceiro) {
		this.codigoParceiro = codigoParceiro;
	}
	
	@DisplayName("TICKET M�DIO POR KG")
	public Double getValorTicketMedio() {
		return valorTicketMedio;
	}
	public void setValorTicketMedio(Double valorTicketMedio) {
		this.valorTicketMedio = valorTicketMedio;
	}
	
	
}