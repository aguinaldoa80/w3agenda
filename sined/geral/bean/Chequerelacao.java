package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_chequerelacao", sequenceName = "sq_chequerelacao")
public class Chequerelacao {
	
	protected Integer cdchequerelacao;
	protected Cheque chequenovo;
	protected Cheque chequeantigo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_chequerelacao")
	public Integer getCdchequerelacao() {
		return cdchequerelacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdchequenovo")
	public Cheque getChequenovo() {
		return chequenovo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdchequeantigo")
	public Cheque getChequeantigo() {
		return chequeantigo;
	}
	
	public void setCdchequerelacao(Integer cdchequerelacao) {
		this.cdchequerelacao = cdchequerelacao;
	}
	public void setChequenovo(Cheque chequenovo) {
		this.chequenovo = chequenovo;
	}
	public void setChequeantigo(Cheque chequeantigo) {
		this.chequeantigo = chequeantigo;
	}
}
