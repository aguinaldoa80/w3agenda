package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdferodoviariocondutor", sequenceName = "sq_mdferodoviariocondutor")
public class MdfeRodoviarioCondutor {

	protected Integer cdMdfeRodoviarioCondutor;
	protected Mdfe mdfe;
	protected String nome;
	protected Cpf cpf;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdferodoviariocondutor")
	public Integer getCdMdfeRodoviarioCondutor() {
		return cdMdfeRodoviarioCondutor;
	}
	public void setCdMdfeRodoviarioCondutor(Integer cdMdfeRodoviarioCondutor) {
		this.cdMdfeRodoviarioCondutor = cdMdfeRodoviarioCondutor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=60)
	@DisplayName("Nome do condutor")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Required
	@DisplayName("CPF do condutor")
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
}
