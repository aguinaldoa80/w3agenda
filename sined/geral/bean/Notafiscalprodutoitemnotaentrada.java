package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutoitemnotaentrada", sequenceName="sq_notafiscalprodutoitemnotaentrada")
public class Notafiscalprodutoitemnotaentrada {
	
	protected Integer cdnotafiscalprodutoitemnotaentrada;
	protected Notafiscalprodutoitem notafiscalprodutoitem;
	protected Entregamaterial entregamaterial;
	protected Double qtdereferencia;
	
	@Id
	@GeneratedValue(generator="sq_notafiscalprodutoitemnotaentrada", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutoitemnotaentrada() {
		return cdnotafiscalprodutoitemnotaentrada;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotafiscalprodutoitem() {
		return notafiscalprodutoitem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}

	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public void setCdnotafiscalprodutoitemnotaentrada(
			Integer cdnotafiscalprodutoitemnotaentrada) {
		this.cdnotafiscalprodutoitemnotaentrada = cdnotafiscalprodutoitemnotaentrada;
	}

	public void setNotafiscalprodutoitem(Notafiscalprodutoitem notafiscalprodutoitem) {
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}

	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
		
}