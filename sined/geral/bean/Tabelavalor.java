package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_tabelavalor", sequenceName = "sq_tabelavalor")
@DisplayName("Tabela de valores")
public class Tabelavalor implements Log{
	
	public static final String CUSTO_OPERACIONAL_ADMINISTRATIVO = "custo_operacional_administrativo";
	public static final String CUSTO_COMERCIAL_ADMINISTRATIVO = "custo_comercial_administrativo";
	public static final String CUSTO_COMERCIAL_PRODUCAO = "custo_comercial_producao";
	
	protected Integer cdtabelavalor;
	protected String identificador;
	protected String descricao;	
	protected Double valor;
	protected List<Tabelavalorhistorico> listaTabelavalorhistorico;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tabelavalor")
	public Integer getCdtabelavalor() {
		return cdtabelavalor;
	}
	@Required
	@MaxLength(40)
	public String getIdentificador() {
		return identificador;
	}
	@Required
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@Required
	public Double getValor() {
		return valor;
	}
	@OneToMany(mappedBy="tabelavalor")
	@DisplayName("Hist�rico")
	public List<Tabelavalorhistorico> getListaTabelavalorhistorico() {
		return listaTabelavalorhistorico;
	}
	
	public void setCdtabelavalor(Integer cdtabelavalor) {
		this.cdtabelavalor = cdtabelavalor;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setListaTabelavalorhistorico(List<Tabelavalorhistorico> listaTabelavalorhistorico) {
		this.listaTabelavalorhistorico = listaTabelavalorhistorico;
	}

	//LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
