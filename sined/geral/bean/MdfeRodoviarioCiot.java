package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_mdferodoviariociot", sequenceName = "sq_mdferodoviariociot")
public class MdfeRodoviarioCiot {

	protected Integer cdMdfeRodoviarioCiot;
	protected Mdfe mdfe;
	protected String ciot;
	protected Tipopessoa tipoPessoa = Tipopessoa.PESSOA_JURIDICA;
	protected Cpf cpf;
	protected Cnpj cnpj;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdferodoviariociot")
	public Integer getCdMdfeRodoviarioCiot() {
		return cdMdfeRodoviarioCiot;
	}
	public void setCdMdfeRodoviarioCiot(Integer cdmdferodoviariociot) {
		this.cdMdfeRodoviarioCiot = cdmdferodoviariociot;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@Required
	@MaxLength(value=12)
	@DisplayName("CIOT")
	public String getCiot() {
		return ciot;
	}
	public void setCiot(String ciot) {
		this.ciot = ciot;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(Tipopessoa tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
}
