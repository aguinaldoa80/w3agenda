package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de rela��o entre recursos")
public class Tiporelacaorecurso {
	
	public static final Tiporelacaorecurso FAIXA_DE_VALORES = new Tiporelacaorecurso(1);
	public static final Tiporelacaorecurso VALOR_UNICO = new Tiporelacaorecurso(2);
	
	protected Integer cdtiporelacaorecurso;
	protected String nome;
	
	public Tiporelacaorecurso() {
		
	}
	
	public Tiporelacaorecurso(Integer cdtiporelacaorecurso) {
		this.cdtiporelacaorecurso = cdtiporelacaorecurso;
	}
	
	@Id	
	public Integer getCdtiporelacaorecurso() {
		return cdtiporelacaorecurso;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdtiporelacaorecurso(Integer cdtiporelacaorecurso) {
		this.cdtiporelacaorecurso = cdtiporelacaorecurso;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tiporelacaorecurso){
			Tiporelacaorecurso that = (Tiporelacaorecurso) obj;
			return this.getCdtiporelacaorecurso().equals(that.getCdtiporelacaorecurso());
		}
		return super.equals(obj);
	}
}
