package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_processoreferenciado", sequenceName = "sq_processoreferenciado")
public class ProcessoReferenciado implements Log {
	
	protected Integer cdProcessoReferenciado;
	protected Empresa empresa;
	protected Integer numeroProcessoJudicial;
	protected Integer idSecaoJudiciaria;
	protected Integer idVara;
	protected AcaoJudicialNatureza acaoJudicialNatureza;
	protected String descricao;
	protected Date dataSentenca;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<ProcessoReferenciadoDocumento> listaProcessoReferenciadoDocumento = new ListSet<ProcessoReferenciadoDocumento>(ProcessoReferenciadoDocumento.class);
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processoreferenciado")
	public Integer getCdProcessoReferenciado() {
		return cdProcessoReferenciado;
	}
	public void setCdProcessoReferenciado(Integer cdProcessoReferenciado) {
		this.cdProcessoReferenciado = cdProcessoReferenciado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@DisplayName("Empresa")
	@Required
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	@Required
	public Integer getNumeroProcessoJudicial() {
		return numeroProcessoJudicial;
	}
	public void setNumeroProcessoJudicial(Integer numeroProcessoJudicial) {
		this.numeroProcessoJudicial = numeroProcessoJudicial;
	}
	@Required
	@DisplayName("Identifica��o da Sess�o Judiciaria ")
	public Integer getIdSecaoJudiciaria() {
		return idSecaoJudiciaria;
	}
	public void setIdSecaoJudiciaria(Integer idSecaoJudiciaria) {
		this.idSecaoJudiciaria = idSecaoJudiciaria;
	}
	@Required
	@DisplayName("Identifica��o da Vara")
	public Integer getIdVara() {
		return idVara;
	}
	public void setIdVara(Integer idVara) {
		this.idVara = idVara;
	}
	
	@Required
	@DisplayName("Natureza da A��o Judicial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdAcaoJudicialNatureza")
	public AcaoJudicialNatureza getAcaoJudicialNatureza() {
		return acaoJudicialNatureza;
	}
	public void setAcaoJudicialNatureza(AcaoJudicialNatureza acaoJudicialNatureza) {
		this.acaoJudicialNatureza = acaoJudicialNatureza;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@DisplayName("Data da Sesnten�a/Decis�o Judicial")
	public Date getDataSentenca() {
		return dataSentenca;
	}
	public void setDataSentenca(Date dataSentenca) {
		this.dataSentenca = dataSentenca;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@OneToMany(mappedBy="processoReferenciado")
	@DisplayName("Documentos Fiscais")
	public List<ProcessoReferenciadoDocumento> getListaProcessoReferenciadoDocumento() {
		return listaProcessoReferenciadoDocumento;
	}
	public void setListaProcessoReferenciadoDocumento(
			List<ProcessoReferenciadoDocumento> listaProcessoReferenciadoDocumento) {
		this.listaProcessoReferenciadoDocumento = listaProcessoReferenciadoDocumento;
	}

}
