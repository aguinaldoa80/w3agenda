package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_empresaconfiguracaonf", sequenceName = "sq_empresaconfiguracaonf")
public class Empresaconfiguracaonf implements Log {
	
	protected Integer cdempresaconfiguracaonf;
	protected String descricao;
	protected Integer linhacorpoinicio;
	protected Integer linhacorpofim;
	protected Integer linhasnotafiscal;
	
	protected List<Empresaconfiguracaonfcampo> listaCampo = new ListSet<Empresaconfiguracaonfcampo>(Empresaconfiguracaonfcampo.class);
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresaconfiguracaonf")
	public Integer getCdempresaconfiguracaonf() {
		return cdempresaconfiguracaonf;
	}

	@DisplayName("Descri��o")
	@Required
	@MaxLength(150)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("Linha de in�cio do corpo")
	public Integer getLinhacorpoinicio() {
		return linhacorpoinicio;
	}

	@Required
	@DisplayName("Linha de fim do corpo")
	public Integer getLinhacorpofim() {
		return linhacorpofim;
	}
	
	@DisplayName("N�mero de linha da nota fiscal")
	@Required
	public Integer getLinhasnotafiscal() {
		return linhasnotafiscal;
	}
	
	public void setLinhasnotafiscal(Integer linhasnotafiscal) {
		this.linhasnotafiscal = linhasnotafiscal;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setLinhacorpoinicio(Integer linhacorpoinicio) {
		this.linhacorpoinicio = linhacorpoinicio;
	}

	public void setLinhacorpofim(Integer linhacorpofim) {
		this.linhacorpofim = linhacorpofim;
	}

	public void setCdempresaconfiguracaonf(Integer cdempresaconfiguracaonf) {
		this.cdempresaconfiguracaonf = cdempresaconfiguracaonf;
	}
	
	
	//LISTAS
	
	@DisplayName("Campos")
	@OneToMany(mappedBy="empresaconfiguracaonf")
	public List<Empresaconfiguracaonfcampo> getListaCampo() {
		return listaCampo;
	}
	
	public void setListaCampo(List<Empresaconfiguracaonfcampo> listaCampo) {
		this.listaCampo = listaCampo;
	}
	
	// LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
}