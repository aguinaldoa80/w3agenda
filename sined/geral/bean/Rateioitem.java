package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_rateioitem",sequenceName="sq_rateioitem")
public class Rateioitem implements Log{

	
	protected Integer cdrateioitem;
	protected Rateio rateio;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Contagerencial contagerencial;
	protected Double percentual;
	protected Money valor;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String observacao; 
	
	//TRANSIENTES
	protected String relatorio;
	protected String nome;
	protected Integer qtdeaux;
	protected String identificador;
	protected Boolean taxabaixaacrescimo;
	
	
	public Rateioitem() {}
	
	public Rateioitem(Rateioitem outro) {
		this.cdrateioitem = outro.cdrateioitem;
		this.cdusuarioaltera = outro.cdusuarioaltera;
		this.centrocusto = outro.centrocusto;
		this.contagerencial = outro.contagerencial;
		this.dtaltera = outro.dtaltera;
		this.percentual = outro.percentual;
		this.projeto = outro.projeto;
		this.rateio = outro.rateio;
		this.valor = outro.valor;
	}
	
	public Rateioitem(Contagerencial contagerencial, Centrocusto centrocusto, Projeto projeto) {
		this.contagerencial = contagerencial;
		this.centrocusto = centrocusto;
		this.projeto = projeto;
	}
	
	public Rateioitem(Contagerencial contagerencial, String identificador, Centrocusto centrocusto, Projeto projeto, Integer qtdaux, Money valor) {
		this.contagerencial = contagerencial;
		this.identificador = identificador;
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.qtdeaux = qtdaux;
		this.valor = valor;
	}
	
	public Rateioitem(Contagerencial contagerencial, Centrocusto centrocusto, Projeto projeto, Money valor) {
		this.contagerencial = contagerencial;
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.valor = valor;
	}
	
	public Rateioitem(Contagerencial contagerencial, Centrocusto centrocusto, Projeto projeto, Money valor, Double percentual) {
		this.contagerencial = contagerencial;
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.valor = valor;
		this.percentual = percentual;
	}
	
	public Rateioitem(Contagerencial contagerencial, Money valor, Double percentual){
		this.contagerencial = contagerencial;
		this.valor = valor;
		this.percentual = percentual;
	}
	
	@Id
	@GeneratedValue(generator="sq_rateioitem",strategy=GenerationType.AUTO)
	public Integer getCdrateioitem() {
		return cdrateioitem;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}
	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}	@Required
	public Double getPercentual() {
		return percentual;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@DisplayName("Observação")
	@MaxLength(200)
	public String getObservacao() {
		return observacao;
	}
	
	@Transient
	public String getNome() {
		return nome;
	}
	@Transient
	public String getRelatorio() {
		return (this.projeto == null ? "" : this.projeto.getNome() + " - ") + (this.contagerencial != null ? this.contagerencial.getNome() : "");
	}
	@Transient
	public Integer getQtdeaux() {
		return qtdeaux;
	}
	@Transient
	public String getIdentificador() {
		return identificador;
	}

	public void setQtdeaux(Integer qtdeaux) {
		this.qtdeaux = qtdeaux;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}
	
	public void setCdrateioitem(Integer cdrateioitem) {
		this.cdrateioitem = cdrateioitem;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Transient
	public Boolean getTaxabaixaacrescimo() {
		return taxabaixaacrescimo;
	}

	public void setTaxabaixaacrescimo(Boolean taxabaixaacrescimo) {
		this.taxabaixaacrescimo = taxabaixaacrescimo;
	}

	// API
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public String toString() {
		return "Valor: "+valor+"\tPercentual: "+percentual;
	}
	
	public String toStringAgendamento(){
		String cg = contagerencial != null && contagerencial.getCdcontagerencial() != null ? contagerencial.getCdcontagerencial().toString() : "";
		String cc = centrocusto != null && centrocusto.getCdcentrocusto() != null ? centrocusto.getCdcentrocusto().toString() : "";
		String pr = projeto != null && projeto.getCdprojeto() != null ? projeto.getCdprojeto().toString() : "";
		
		return "Conta Gerencial: " + cg + "\tCentro de Custo: "+ cc + "\tProjeto: "+ pr;
	}
}
