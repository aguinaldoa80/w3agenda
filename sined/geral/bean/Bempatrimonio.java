package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;


@Entity
public class Bempatrimonio extends Material implements Log {

	protected String nomereduzido;
	protected Money valor;
	protected Money valormercado;
	protected Money valorreferencia;
	protected Integer vidautil;
	protected Money depreciacao;
	protected String observacao;
	
	//Devem pertencer somente a classe m�e:
	//protected Timestamp dtAltera;
	//protected Integer cdUsuarioAltera;
	
	public Bempatrimonio() {
	}
	
	public Bempatrimonio(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	
	public Bempatrimonio(Integer cdmaterial, String nome) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
	}

	public Bempatrimonio(String nome) {
		this.nome = nome;
	}
	
	public Bempatrimonio(Integer cdmaterial, String nomereduzido, Money valor, Money valormercado,
						 Money valorreferencia, Integer vidautil, Money depreciacao, String observacao) {
		this.cdmaterial = cdmaterial;
		this.nomereduzido = nomereduzido;
		this.valor = valor;
		this.valormercado = valormercado;
		this.valorreferencia = valorreferencia;
		this.vidautil = vidautil;
		this.depreciacao = depreciacao;
		this.observacao = observacao;
	}

	public String getNomereduzido() {
		return nomereduzido;
	}

	public Money getValor() {
		return valor;
	}

	public Money getValormercado() {
		return valormercado;
	}

	public Money getValorreferencia() {
		return valorreferencia;
	}
	@MaxLength(10)
	public Integer getVidautil() {
		return vidautil;
	}

	public Money getDepreciacao() {
		return depreciacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setNomereduzido(String nomereduzido) {
		this.nomereduzido = nomereduzido;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setValormercado(Money valormercado) {
		this.valormercado = valormercado;
	}

	public void setValorreferencia(Money valorreferencia) {
		this.valorreferencia = valorreferencia;
	}

	public void setVidautil(Integer vidautil) {
		this.vidautil = vidautil;
	}

	public void setDepreciacao(Money depreciacao) {
		this.depreciacao = depreciacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		return result;
	}
}
