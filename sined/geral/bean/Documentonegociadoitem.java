package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_documentonegociadoitem", sequenceName = "sq_documentonegociadoitem")
public class Documentonegociadoitem{

	protected Integer cddocumentonegociadoitem;
	protected Documentonegociado documentonegociado;
	protected Documento documento;
	
	public Documentonegociadoitem() {}
	
	public Documentonegociadoitem(Integer cddocumentonegociadoitem) {
		this.cddocumentonegociadoitem = cddocumentonegociadoitem;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentonegociadoitem")
	public Integer getCddocumentonegociadoitem() {
		return cddocumentonegociadoitem;
	}
	public void setCddocumentonegociadoitem(Integer cddocumentonegociadoitem) {
		this.cddocumentonegociadoitem = cddocumentonegociadoitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentonegociado")
	public Documentonegociado getDocumentonegociado() {
		return documentonegociado;
	}
	public void setDocumentonegociado(Documentonegociado documentonegociado) {
		this.documentonegociado = documentonegociado;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	

}
