package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@DisplayName ("Material")
@SequenceGenerator(name = "sq_diarioobramaterial", sequenceName = "sq_diarioobramaterial")
public class Diarioobramaterial {

	protected Integer cddiarioobramaterial;
	protected Diarioobra diarioobra;
	protected Material material;
	protected Double quantidade;
	protected Double totalhora;
	protected Unidademedida unidademedida;
	
	@Id
	@DisplayName ("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_diarioobramaterial")
	public Integer getCddiarioobramaterial() {
		return cddiarioobramaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddiarioobra")
	public Diarioobra getDiarioobra() {
		return diarioobra;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@Required
	@DisplayName ("Quantidade")
	public Double getQuantidade() {
		return quantidade;
	}
	
	@DisplayName("Total de horas")
	public Double getTotalhora() {
		return totalhora;
	}
	
	public void setCddiarioobramaterial(Integer cddiarioobramaterial) {
		this.cddiarioobramaterial = cddiarioobramaterial;
	}
	public void setDiarioobra(Diarioobra diarioobra) {
		this.diarioobra = diarioobra;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setTotalhora(Double totalhora) {
		this.totalhora = totalhora;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@DisplayName("Unidade de Medidas")
	@Required
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	} 
}
