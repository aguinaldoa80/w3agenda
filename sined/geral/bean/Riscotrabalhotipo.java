package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;


@Entity
@SequenceGenerator(name = "sq_riscotrabalhotipo", sequenceName = "sq_riscotrabalhotipo")
public class Riscotrabalhotipo{

	protected Integer cdriscotrabalhotipo;
	protected String nome;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_riscotrabalhotipo")
	public Integer getCdriscotrabalhotipo() {
		return cdriscotrabalhotipo;
	}
	
	@DescriptionProperty
	@MaxLength(25)
	public String getNome() {
		return nome;
	}
	
	public void setCdriscotrabalhotipo(Integer cdriscotrabalhotipo) {
		this.cdriscotrabalhotipo = cdriscotrabalhotipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
