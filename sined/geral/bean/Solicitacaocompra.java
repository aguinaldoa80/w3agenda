package br.com.linkcom.sined.geral.bean;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_solicitacaocompra;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_solicitacaocompra", sequenceName = "sq_solicitacaocompra")
@DisplayName("Solicita��o Material")
@JoinEmpresa("solicitacaocompra.empresa")
public class Solicitacaocompra implements Log, PermissaoProjeto {

	public static final Integer PEDIDO = 1;
	public static final Integer ENTREGA = 2;
	public static final Integer ROMANEIO = 3;
	
	protected Integer cdsolicitacaocompra;
	protected Integer identificador; 
	protected Integer cdUsuarioAltera;
	protected Double qtde;
	protected Frequencia frequencia;
	protected Double qtdefrequencia;
	protected Timestamp dtAltera;
	protected String descricao;
	protected String observacao;
	protected Date dtlimite;
	protected Departamento departamento;
	protected Contagerencial contagerencial;
	protected Materialgrupo materialgrupo;
	protected Material material;
	protected Materialclasse materialclasse;
	protected Localarmazenagem localarmazenagem;
	protected Solicitacaocompraorigem solicitacaocompraorigem;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Empresa empresa;
	protected Colaborador colaborador;
	protected Date dtcancelamento;
	protected Date dtbaixa;
	protected Date dtautorizacao;
	protected Date dtsolicitacao;
	protected Date dtnaoautorizacao;
	protected Aux_solicitacaocompra aux_solicitacaocompra;
	protected Boolean faturamentocliente;
	protected Unidademedida unidademedida;
	protected Integer cdpedidovenda;
	protected Integer cdvendaorcamento;
	protected String whereInCdpedidovenda;
	protected String whereInCdvendaorcamento;
	protected Arquivo arquivo;
	protected Double altura;
	protected Double largura;
	protected Double comprimento;
	protected Double pesototal;
	protected Double qtdvolume;
	
	protected String pedido;
	protected String entrega;
	protected String romaneio;
	
	protected Set<Cotacaoorigem> listaCotacaoorigem = new ListSet<Cotacaoorigem>(Cotacaoorigem.class);
	protected Set<Ordemcompraorigem> listaOrdemcompraorigem = new ListSet<Ordemcompraorigem>(Ordemcompraorigem.class);
	protected List<Solicitacaocomprahistorico> listaSolicitacaocomprahistorico = new ListSet<Solicitacaocomprahistorico>(Solicitacaocomprahistorico.class);
	
	//TRANSIENT'S
	@SuppressWarnings("unused")
	private String projetodepartamento;
	private String obrigar_projeto_conta_grupo;
	private String dtentrega;
	private Integer qtdeminima; 
	private Integer cdsolicitacaocompraQuantidadeRestante;
	private Boolean corAmarelo = Boolean.FALSE;
	private Boolean corVermelho = Boolean.FALSE;
	protected Localarmazenagem localunicoentrega;
	protected Empresa empresaunicaentrega;
	protected Boolean copiar;
	
	public Solicitacaocompra(){
	}
	
	public Solicitacaocompra(Integer cdsolicitacaocompra){
		this.cdsolicitacaocompra = cdsolicitacaocompra;
	}
	
	public Solicitacaocompra(String historico, Date dtlimite, Projeto projeto, Departamento departamento, Localarmazenagem localarmazenagem, Centrocusto centrocusto, 
			  Empresa empresa, Colaborador colaborador, Material material, Materialclasse materialclasse, Double qtde, Solicitacaocompraorigem solicitacaocompraorigem, 
			  Integer identificador, Boolean faturamentocliente, String observacao, Frequencia frequencia, Double qtdefrequencia, Integer _identificador, 
			  Double altura, Double largura, Double comprimento, Double pesototal, Double qtdvolume){
		this.descricao = historico;
		this.dtlimite = dtlimite;
		this.projeto = projeto;
		this.departamento = departamento;
		this.localarmazenagem = localarmazenagem;
		this.centrocusto = centrocusto;
		this.empresa = empresa;
		this.colaborador = colaborador;
		this.material = material;
		this.materialclasse = materialclasse;
		this.qtde = qtde;
		this.solicitacaocompraorigem = solicitacaocompraorigem;
		this.identificador = identificador;
		this.faturamentocliente = faturamentocliente;
		this.observacao = observacao;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequencia;
		this.identificador = _identificador;
		this.altura = altura;
		this.largura = largura;
		this.comprimento = comprimento;
		this.pesototal = pesototal;
		this.qtdvolume = qtdvolume;
	}
	
	public Solicitacaocompra(String historico, Date dtlimite, Projeto projeto, Departamento departamento, Localarmazenagem localarmazenagem, Centrocusto centrocusto, 
			  Empresa empresa, Colaborador colaborador, Material material, Materialclasse materialclasse, Double qtde, Solicitacaocompraorigem solicitacaocompraorigem, 
			  Integer identificador, Boolean faturamentocliente, String observacao, Frequencia frequencia, Double qtdefrequencia, Integer _identificador, Unidademedida unidademedida){
		this.descricao = historico;
		this.dtlimite = dtlimite;
		this.projeto = projeto;
		this.departamento = departamento;
		this.localarmazenagem = localarmazenagem;
		this.centrocusto = centrocusto;
		this.empresa = empresa;
		this.colaborador = colaborador;
		this.material = material;
		this.materialclasse = materialclasse;
		this.qtde = qtde;
		this.solicitacaocompraorigem = solicitacaocompraorigem;
		this.identificador = identificador;
		this.faturamentocliente = faturamentocliente;
		this.observacao = observacao;
		this.frequencia = frequencia;
		this.qtdefrequencia = qtdefrequencia;
		this.identificador = _identificador;
		this.unidademedida = unidademedida;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_solicitacaocompra")
	@DisplayName("C�d. solicita��o")
	public Integer getCdsolicitacaocompra() {
		return cdsolicitacaocompra;
	}
	@Required
	@DisplayName("Quantidade")
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("Descri��o")
	@MaxLength(90)
	public String getDescricao() {
		return descricao;
	}
	@Required
	@DisplayName("Data da necessidade")
	public Date getDtlimite() {
		return dtlimite;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@DisplayName ("Conta Gerencial")
	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn (name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@Required
	@DisplayName("Material/Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Necessidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	@Required
	@DisplayName("Local de entrega")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Origem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompraorigem")
	public Solicitacaocompraorigem getSolicitacaocompraorigem() {
		return solicitacaocompraorigem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	public Date getDtbaixa() {
		return dtbaixa;
	}
	@DisplayName("Data autoriza��o")
	public Date getDtautorizacao() {
		return dtautorizacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra", insertable=false, updatable=false)
	public Aux_solicitacaocompra getAux_solicitacaocompra() {
		return aux_solicitacaocompra;
	}
	@MaxLength(9)
	public Integer getIdentificador() {
		return identificador;
	}
	@MaxLength(100)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Faturamento cliente")
	public Boolean getFaturamentocliente() {
		return faturamentocliente;
	}
	public String getPedido() {
		return pedido;
	}
	public String getEntrega() {
		return entrega;
	}
	public String getRomaneio() {
		return romaneio;
	}
	@OneToMany(mappedBy="solicitacaocompra")
	@DisplayName("Hist�rico")
	public List<Solicitacaocomprahistorico> getListaSolicitacaocomprahistorico() {
		return listaSolicitacaocomprahistorico;
	}
	

	@Required
	@DisplayName("Frequ�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}

	@Required
	@DisplayName("Qtde. Freq.")
	public Double getQtdefrequencia() {
		return qtdefrequencia;
	}
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	public Date getDtnaoautorizacao() {
		return dtnaoautorizacao;
	}
	
	public Double getAltura() {
		return altura;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getComprimento() {
		return comprimento;
	}

	@DisplayName("Peso total")
	public Double getPesototal() {
		return pesototal;
	}
	
	@DisplayName("Qtde. Volume(s)")
	public Double getQtdvolume() {
		return qtdvolume;
	}
	
	public void setQtdvolume(Double qtdvolume) {
		this.qtdvolume = qtdvolume;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setPesototal(Double pesototal) {
		this.pesototal = pesototal;
	}

	public void setDtnaoautorizacao(Date dtnaoautorizacao) {
		this.dtnaoautorizacao = dtnaoautorizacao;
	}
	
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	
	public void setQtdefrequencia(Double qtdefrequencia) {
		this.qtdefrequencia = qtdefrequencia;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	public void setRomaneio(String romaneio) {
		this.romaneio = romaneio;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}
	public void setDtautorizacao(Date dtautorizacao) {
		this.dtautorizacao = dtautorizacao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setSolicitacaocompraorigem(
			Solicitacaocompraorigem solicitacaocompraorigem) {
		this.solicitacaocompraorigem = solicitacaocompraorigem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setDtlimite(Date dtlimite) {
		this.dtlimite = dtlimite;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setCdsolicitacaocompra(Integer cdsolicitacaocompra) {
		this.cdsolicitacaocompra = cdsolicitacaocompra;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setAux_solicitacaocompra(Aux_solicitacaocompra aux_solicitacaocompra) {
		this.aux_solicitacaocompra = aux_solicitacaocompra;
	}
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	public void setObservacao(String observacao) {
		this.observacao = StringUtils.trimToNull(observacao);
	}
	
	@OneToMany(mappedBy="solicitacaocompra")
	public Set<Cotacaoorigem> getListaCotacaoorigem() {
		return listaCotacaoorigem;
	}
	
	@OneToMany(mappedBy="solicitacaocompra")
	public Set<Ordemcompraorigem> getListaOrdemcompraorigem() {
		return listaOrdemcompraorigem;
	}
	
	public void setListaOrdemcompraorigem(
			Set<Ordemcompraorigem> listaOrdemcompraorigem) {
		this.listaOrdemcompraorigem = listaOrdemcompraorigem;
	}
	
	public void setListaCotacaoorigem(Set<Cotacaoorigem> listaCotacaoorigem) {
		this.listaCotacaoorigem = listaCotacaoorigem;
	}
	public void setFaturamentocliente(Boolean faturamentocliente) {
		this.faturamentocliente = faturamentocliente;
	}
	public void setListaSolicitacaocomprahistorico(
			List<Solicitacaocomprahistorico> listaSolicitacaocomprahistorico) {
		this.listaSolicitacaocomprahistorico = listaSolicitacaocomprahistorico;
	}
	
	//==================== TRANSIENT'S ===========================
	
	//protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Integer disponivel;
	protected String tempoentrega;
	protected Cotacao cotacaotrans;
	protected String whereIn;
	protected String controller;
//	protected String unidademedida;
	protected List<Solicitacaocompra> solicitacoes = new ListSet<Solicitacaocompra>(Solicitacaocompra.class); 
	protected Orcamentomaterial orcamentomaterial;
	protected Tarefa tarefa;
	protected Integer cdtarefarecursogeral;
	protected Boolean gerado;
	protected Solicitacaocomprahistorico solicitacaocomprahistorico;
	protected Integer cdplanejamentorecursogeral;
	protected Planejamento planejamento;
	protected Frequencia frequenciatrans;
	protected String frequenciastr; 
	protected Boolean urgente;
	protected String unidademedidastr;
	protected Producaoagenda producaoagenda;

	
	
	@DisplayName("Grupo Material")
	@ManyToOne (fetch=FetchType.LAZY)
	@JoinColumn (name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@Transient
	@DisplayName("Tipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	@Transient
	@DisplayName("Dispon�veis")
	public Integer getDisponivel() {
		return disponivel;
	}
	@Transient
	@DisplayName("Prazo de entrega")
	public String getTempoentrega() {
		return tempoentrega;
	}
	@Transient
	public Cotacao getCotacaotrans() {
		return cotacaotrans;
	}
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	@Transient
	public String getController() {
		return controller;
	}
	@Transient
	public List<Solicitacaocompra> getSolicitacoes() {
		return solicitacoes;
	}
	/*@Transient
	@MaxLength(10)
	public String getUnidademedida() {
		return unidademedida;
	}*/
	
	@Transient
	public Orcamentomaterial getOrcamentomaterial() {
		return orcamentomaterial;
	}
	@Transient
	@DisplayName("Compra")
	public String getCompra(){
		String compra = "";
		if(this.pedido != null && !this.pedido.equals("")){
			compra += montaImagem(this.pedido);
			compra += " pedido <BR>";
		} 
		if(this.entrega != null && !this.entrega.equals("")){
			compra += montaImagem(this.entrega);
			compra += " entrega <BR>";
		} 
		if(this.romaneio != null && !this.romaneio.equals("")){
			//compra += this.qtde > new Integer(this.romaneio) ? "<img src=\"/ctx/imagens/suprimentos/icones/flag_red.png\" onmouseover=\"Tip('["+this.romaneio+"]')\"/>" : "<img src=\"/ctx/imagens/suprimentos/icones/flag_geen.png\" onmouseover=\"Tip('["+this.romaneio+"]')\"/>";
			compra += this.qtde > new Double(this.romaneio).intValue() ? "<img src=\"" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "/imagens/suprimentos/icones/flag_red.png\" onmouseover=\"Tip('["+this.romaneio+"]')\"/>" : "<img src=\"" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "/imagens/suprimentos/icones/flag_geen.png\" onmouseover=\"Tip('["+this.romaneio+"]')\"/>";
			compra += " romaneio <BR>";
		} 
		
		return compra;
	}
	
	private String montaImagem(String tipo) {
		String[] vetaux = tipo.split("/");
		String flag = "";
		try{
			if(Double.parseDouble(vetaux[0]) < Double.parseDouble(vetaux[1])){
				flag = "yellow";
			} else{
				flag = "green";
			}
			//return "<img src=\"/ctx/imagens/suprimentos/icones/flag_"+flag+".png\" onmouseover=\"Tip('["+tipo+"]')\"/>";
			return "<img src=\"" + NeoWeb.getRequestContext().getServletRequest().getContextPath() + "/imagens/suprimentos/icones/flag_"+flag+".png\" onmouseover=\"Tip('["+tipo+"]')\"/>";
		} catch (Exception e) {
			return "";
		}
	}
	
	@Transient
	public Integer getCdtarefarecursogeral() {
		return cdtarefarecursogeral;
	}
	
	@Transient
	public Tarefa getTarefa() {
		return tarefa;
	}
	
	@Transient
	public Boolean getGerado() {
		return gerado;
	}
	
	@Transient
	public Solicitacaocomprahistorico getSolicitacaocomprahistorico() {
		return solicitacaocomprahistorico;
	}
	
	@Transient
	public Integer getCdplanejamentorecursogeral() {
		return cdplanejamentorecursogeral;
	}
	
	@Transient
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	
	@Transient
	public Frequencia getFrequenciatrans() {
		return frequenciatrans;
	}
	
	@Transient
	public Boolean getUrgente() {
		return urgente;
	}
	
	@Transient
	public String getUnidademedidastr() {
		if(material != null && material.getUnidademedida() != null && material.getUnidademedida().getSimbolo() != null){
			return material.getUnidademedida().getSimbolo();
		}
		return unidademedidastr;
	}
	
	public void setUnidademedidastr(String unidademedidastr) {
		this.unidademedidastr = unidademedidastr;
	}
	
	public void setUrgente(Boolean urgente) {
		this.urgente = urgente;
	}
	public void setFrequenciatrans(Frequencia frequenciatrans) {
		this.frequenciatrans = frequenciatrans;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCdplanejamentorecursogeral(Integer cdplanejamentorecursogeral) {
		this.cdplanejamentorecursogeral = cdplanejamentorecursogeral;
	}
	public void setGerado(Boolean gerado) {
		this.gerado = gerado;
	}
	public void setCdtarefarecursogeral(Integer cdtarefarecursogeral) {
		this.cdtarefarecursogeral = cdtarefarecursogeral;
	}
	public void setTarefa(Tarefa tarefa) {
		this.tarefa = tarefa;
	}
	public void setOrcamentomaterial(Orcamentomaterial orcamentomaterial) {
		this.orcamentomaterial = orcamentomaterial;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	public void setDisponivel(Integer disponivel) {
		this.disponivel = disponivel;
	}
	public void setTempoentrega(String tempoentrega) {
		this.tempoentrega = tempoentrega;
	}
	public void setCotacaotrans(Cotacao cotacaotrans) {
		this.cotacaotrans = cotacaotrans;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setController(String controller) {
		this.controller = controller;
	}
	public void setSolicitacoes(List<Solicitacaocompra> solicitacoes) {
		this.solicitacoes = solicitacoes;
	}
	/*public void setUnidademedida(String unidademedida) {
		this.unidademedida = unidademedida;
	}*/
	public void setSolicitacaocomprahistorico(
			Solicitacaocomprahistorico solicitacaocomprahistorico) {
		this.solicitacaocomprahistorico = solicitacaocomprahistorico;
	}
	
	@Transient
	public String getFrequenciastr(){
		String string = "";
		Integer cdfrequencia = getFrequencia().getCdfrequencia();
		
		switch (cdfrequencia) {
		case 1: //UNICA
			string = "�nica";
			break;
		case 2: //DI�RIA
			string = getQtdefrequencia() + " dia(s)";
			break;
		case 3: //SEMANAL
			string = getQtdefrequencia() + " semana(s)";
			break;
		case 4: //QUINZENAL
			string = getQtdefrequencia() + " quinzena(s)";
			break;
		case 5: //MENSAL
			string = getQtdefrequencia() + " m�s(es)";
			break;
		case 6: //ANUAL
			string = getQtdefrequencia() + " ano(s)";
			break;
		case 7: //SEMESTRAL
			string = getQtdefrequencia() + " semestre(s)";
			break;
		case 8: //HORA
			string = getQtdefrequencia() + " hora(s)";
			break;
		case 9: //TRIMESTE
			string = getQtdefrequencia() + " trimestre(s)";
			break;

		default:
			break;
		}
		return string;
	}
	
	public void setFrequenciastr(String frequenciastr) {
		this.frequenciastr = frequenciastr;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Solicitacaocompra) {
			Solicitacaocompra bean = (Solicitacaocompra) obj;
			return this.getCdsolicitacaocompra().equals(bean.getCdsolicitacaocompra());
		}
		return super.equals(obj);
	}

	public String subQueryProjeto() {
		return "select solicitacaocomprasubQueryProjeto.cdsolicitacaocompra " +
				"from Solicitacaocompra solicitacaocomprasubQueryProjeto " +
				"left outer join solicitacaocomprasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where (projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") " +
				"or projetosubQueryProjeto.cdprojeto is null)";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	@DisplayName("Projeto/Departamento")
	public String getProjetodepartamento() {
		StringBuilder conc = new StringBuilder("");
		if(this.projeto != null && this.projeto.getNome() != null && !this.projeto.getNome().equals(""))
			conc.append(this.projeto.getNome());
		if(this.departamento != null && this.departamento.getNome() != null && !this.departamento.getNome().equals("")){
			if(conc != null && !conc.toString().equals(""))
				conc.append(" / ");
			conc.append(this.departamento.getNome());
		}
		return conc.toString();
	}
	public void setProjetodepartamento(String projetodepartamento) {
		this.projetodepartamento = projetodepartamento;
	}
	
	@DisplayName("Data solicita��o")
	public Date getDtsolicitacao() {
		return dtsolicitacao;
	}
	
	public void setDtsolicitacao(Date dtsolicitacao) {
		this.dtsolicitacao = dtsolicitacao;
	}

	@Transient
	public String getObrigar_projeto_conta_grupo() {
		return obrigar_projeto_conta_grupo;
	}

	public void setObrigar_projeto_conta_grupo(String obrigar_projeto_conta_grupo) {
		this.obrigar_projeto_conta_grupo = obrigar_projeto_conta_grupo;
	}

	@Transient
	public String getDtentrega() {
		if (dtlimite!= null){
			 Calendar c = Calendar.getInstance();
			 c.setTime(dtlimite);
			if(material.getTempoentrega() != null){
				c.add(Calendar.DATE, material.getTempoentrega());
			}
			SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy");
			dtentrega = dateformatter.format(c.getTime());
		}
		return dtentrega;
	}

	public void setDtentrega(String dtentrega) {
		this.dtentrega = dtentrega;
	}
	
	@Transient
	public Integer getCdpedidovenda() {
		return cdpedidovenda;
	}
	@Transient
	public String getWhereInCdpedidovenda() {
		return whereInCdpedidovenda;
	}
	@Transient
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	@Transient
	public String getWhereInCdvendaorcamento() {
		return whereInCdvendaorcamento;
	}

	public void setWhereInCdpedidovenda(String whereInCdpedidovenda) {
		this.whereInCdpedidovenda = whereInCdpedidovenda;
	}
	public void setCdpedidovenda(Integer cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}
	public void setWhereInCdvendaorcamento(String whereInCdvendaorcamento) {
		this.whereInCdvendaorcamento = whereInCdvendaorcamento;
	}
	
	@Transient
	@DisplayName("Qtde. M�nima")
	public Integer getQtdeminima() {
		return qtdeminima;
	}
	public void setQtdeminima(Integer qtdeminima) {
		this.qtdeminima = qtdeminima;
	}
	
	@Transient
	public Integer getCdsolicitacaocompraQuantidadeRestante() {
		return cdsolicitacaocompraQuantidadeRestante;
	}
	
	public void setCdsolicitacaocompraQuantidadeRestante(
			Integer cdsolicitacaocompraQuantidadeRestante) {
		this.cdsolicitacaocompraQuantidadeRestante = cdsolicitacaocompraQuantidadeRestante;
	}
	
	@Transient
	public Producaoagenda getProducaoagenda() {
		return producaoagenda;
	}
	
	public void setProducaoagenda(Producaoagenda producaoagenda) {
		this.producaoagenda = producaoagenda;
	}

	@Transient
	public Boolean getCorAmarelo() {
		return corAmarelo;
	}

	@Transient
	public Boolean getCorVermelho() {
		return corVermelho;
	}

	public void setCorAmarelo(Boolean corAmarelo) {
		this.corAmarelo = corAmarelo;
	}

	public void setCorVermelho(Boolean corVermelho) {
		this.corVermelho = corVermelho;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	@Transient
	public Date getDataEntrega() {
		Date data = null;
		if (dtlimite!= null){
			 Calendar c = Calendar.getInstance();
			 c.setTime(dtlimite);
			if(material.getTempoentrega() != null){
				c.add(Calendar.DATE, material.getTempoentrega());
			}
			data = new Date(c.getTime().getTime());
		}
		return data;
	}

	@Transient
	public Localarmazenagem getLocalunicoentrega() {
		return localunicoentrega;
	}

	public void setLocalunicoentrega(Localarmazenagem localunicoentrega) {
		this.localunicoentrega = localunicoentrega;
	}
	@Transient
	public Empresa getEmpresaunicaentrega() {
		return empresaunicaentrega;
	}

	public void setEmpresaunicaentrega(Empresa empresaunicaentrega) {
		this.empresaunicaentrega = empresaunicaentrega;
	}
	
	@Transient
	public String getIdentificadorOuCdsolicitacaocompra(){
		return identificador != null ? identificador.toString() : (cdsolicitacaocompra != null ? cdsolicitacaocompra.toString() : "");
	}

	@Transient
	public Boolean getCopiar() {
		return copiar;
	}

	public void setCopiar(Boolean copiar) {
		this.copiar = copiar;
	}
	
	
}
