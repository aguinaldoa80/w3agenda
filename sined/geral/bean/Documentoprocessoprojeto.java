package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_documentoprocessoprojeto",sequenceName="sq_documentoprocessoprojeto")
public class Documentoprocessoprojeto{
	
	protected Integer cddocumentoprocessoprojeto;
	protected Documentoprocesso documentoprocesso;
	protected Projeto projeto;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentoprocessoprojeto")
	public Integer getCddocumentoprocessoprojeto() {
		return cddocumentoprocessoprojeto;
	}
	public void setCddocumentoprocessoprojeto(Integer cddocumentoprocessoprojeto) {
		this.cddocumentoprocessoprojeto = cddocumentoprocessoprojeto;
	}
	
	@DescriptionProperty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocesso")
	public Documentoprocesso getDocumentoprocesso() {
		return documentoprocesso;
	}
	public void setDocumentoprocesso(Documentoprocesso documentoprocesso) {
		this.documentoprocesso = documentoprocesso;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Documentoprocessoprojeto) {
			Documentoprocessoprojeto documentoprocessoprojeto = (Documentoprocessoprojeto) obj;
			return this.getCddocumentoprocessoprojeto().equals(documentoprocessoprojeto.getCddocumentoprocessoprojeto());
		}
		return super.equals(obj);
	}

}
