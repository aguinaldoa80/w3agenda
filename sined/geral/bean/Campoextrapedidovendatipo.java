package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_campoextrapedidovendatipo",sequenceName="sq_campoextrapedidovendatipo")
public class Campoextrapedidovendatipo {

	protected Integer cdcampoextrapedidovendatipo;
	protected Pedidovendatipo pedidovendatipo;
	protected String nome;
	protected Boolean obrigatorio=false;
	protected Integer ordem;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_campoextrapedidovendatipo",strategy=GenerationType.AUTO)
	public Integer getCdcampoextrapedidovendatipo() {
		return cdcampoextrapedidovendatipo;
	}
	
	@Required
	@DisplayName("Tipo de pedido de venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Obrigatório")
	@Required
	public Boolean getObrigatorio() {
		return obrigatorio;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdcampoextrapedidovendatipo(Integer cdcampoextrapedidovenda) {
		this.cdcampoextrapedidovendatipo = cdcampoextrapedidovenda;
	}
	
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	///////////////////////////////////////////
	//  Campos transientes
	///////////////////////////////////////////
	
	private Integer cdvalorcampoextraatual;
	private String valorAtual;

	@Transient
	public Integer getCdvalorcampoextraatual() {
		return cdvalorcampoextraatual;
	}

	public void setCdvalorcampoextraatual(Integer cdvalorcampoextraatual) {
		this.cdvalorcampoextraatual = cdvalorcampoextraatual;
	}

	@Transient
	public String getValorAtual() {
		return valorAtual;
	}

	public void setValorAtual(String valorAtual) {
		this.valorAtual = valorAtual;
	}
	
	
	
}
