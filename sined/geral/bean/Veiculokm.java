package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculokm", sequenceName = "sq_veiculokm")
public class Veiculokm implements Log {

	protected Integer cdveiculokm;
	protected Veiculo veiculo;
	protected Date dtentrada;
	protected String motivo;
	protected Integer kmnovo;
	protected Tipokmajuste tipokmajuste;
	protected Arquivo arquivo;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	protected Veiculokm veiculokmatual;
	protected Date dtentradatrans;
	
	public Veiculokm(Veiculo veiculo, Date dtentrada, Integer kmnovo, Tipokmajuste tipokmajuste){
		this.veiculo = veiculo;
		this.dtentrada = dtentrada;
		this.kmnovo = kmnovo;
		this.tipokmajuste = tipokmajuste;
	}

	public Veiculokm(){
	}
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculokm")
	public Integer getCdveiculokm() {
		return cdveiculokm;
	}
	
	@Required
	@DisplayName("Ve�culo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculo")
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	@Required
	@DisplayName("Tipo ajuste")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipokmajuste")
	public Tipokmajuste getTipokmajuste() {
		return tipokmajuste;
	}

	@DisplayName("Data entrada")
	public Date getDtentrada() {
		return dtentrada;
	}

	@Required
	@MaxLength(50)
	public String getMotivo() {
		return motivo;
	}

	@MaxLength(9)
	@Required
	@DisplayName("Km novo")
	public Integer getKmnovo() {
		return kmnovo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdveiculokm(Integer cdveiculokm) {
		this.cdveiculokm = cdveiculokm;
	}
	
	public void setTipokmajuste(Tipokmajuste tipokmajuste) {
		this.tipokmajuste = tipokmajuste;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	public void setDtentrada(Date dtentrada) {
		this.dtentrada = dtentrada;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public void setKmnovo(Integer kmnovo) {
		this.kmnovo = kmnovo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
	//=================== Transients =============
	
	@DisplayName("Km atual")
	@DescriptionProperty
	@Transient
	public String getDescriptionProperty(){
		return ""+ (this.veiculokmatual.getKmnovo() != null ? this.veiculokmatual.getKmnovo() : "")+" - " 
				 +(this.veiculokmatual.getDtentrada() != null ? DateFormat.getDateInstance(DateFormat.SHORT, new Locale("PT", "BR")).format(this.veiculokmatual.getDtentrada()) : "");
	}
	
	@Transient
	public Veiculokm getVeiculokmatual() {
		return veiculokmatual;
	}
	
	public void setVeiculokmatual(Veiculokm veiculokmatual) {
		this.veiculokmatual = veiculokmatual;
	}

	@Transient
	public Date getDtentradatrans() {
		return getDtentrada();
	}

	public void setDtentradatrans(Date dtentradatrans) {
		this.dtentradatrans = dtentradatrans;
	}
	
	
	
}