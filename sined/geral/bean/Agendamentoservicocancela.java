package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Motivo de cancelamento de agendamento de servi�o")
@SequenceGenerator(name = "sq_agendamentoservicocancela", sequenceName = "sq_agendamentoservicocancela")
public class Agendamentoservicocancela implements Log {

	protected Integer cdagendamentoservicocancela;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendamentoservicocancela")
	public Integer getCdagendamentoservicocancela() {
		return cdagendamentoservicocancela;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(100)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCdagendamentoservicocancela(
			Integer cdagendamentoservicocancela) {
		this.cdagendamentoservicocancela = cdagendamentoservicocancela;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
