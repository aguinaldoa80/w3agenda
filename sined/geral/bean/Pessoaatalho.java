package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_pessoaatalho", sequenceName = "sq_pessoaatalho")
public class Pessoaatalho {

	protected Integer cdpessoaatalho;
	protected Pessoa pessoa;
	protected Tela tela;

	public Pessoaatalho() {
	}
	
	public Pessoaatalho(Pessoa pessoa, Tela tela) {
		this.pessoa = pessoa;
		this.tela = tela;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoaatalho")
	public Integer getCdpessoaatalho() {
		return cdpessoaatalho;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtela")
	public Tela getTela() {
		return tela;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	public void setCdpessoaatalho(Integer cdpessoaatalho) {
		this.cdpessoaatalho = cdpessoaatalho;
	}
	
	public void setTela(Tela tela) {
		this.tela = tela;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
