package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_configuracaofolhaevento", sequenceName="sq_configuracaofolhaevento")
public class ConfiguracaoFolhaEvento {

	private Integer cdConfiguracaoFolhaEvento;
	private ConfiguracaoFolha configuracaoFolha;
	private EventoPagamento evento;
	
	
	@Id
	@GeneratedValue(generator="sq_configuracaofolhaevento", strategy=GenerationType.AUTO)
	public Integer getCdConfiguracaoFolhaEvento() {
		return cdConfiguracaoFolhaEvento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaofolha")
	public ConfiguracaoFolha getConfiguracaoFolha() {
		return configuracaoFolha;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdeventopagamento")
	public EventoPagamento getEvento() {
		return evento;
	}
	public void setCdConfiguracaoFolhaEvento(Integer cdConfiguracaoFolhaEvento) {
		this.cdConfiguracaoFolhaEvento = cdConfiguracaoFolhaEvento;
	}
	public void setConfiguracaoFolha(ConfiguracaoFolha configuracaoFolha) {
		this.configuracaoFolha = configuracaoFolha;
	}
	public void setEvento(EventoPagamento evento) {
		this.evento = evento;
	}
	
	
}
