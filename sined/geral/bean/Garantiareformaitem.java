package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_garantiareformaitem", sequenceName="sq_garantiareformaitem")
@DisplayName("Garantiareformaitem")
public class Garantiareformaitem implements Log {

	private Integer cdgarantiareformaitem;
	private Garantiareforma garantiareforma;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Material material;
	private Pneu pneu;
	private Double residuobanda;
	private Motivodevolucao constatacao;
	private Garantiatipo garantiatipo;
	private Garantiatipopercentual garantiatipopercentual;
	private Producaoordemmaterial producaoordemmaterial;
	private Pedidovendamaterial pedidovendamaterialdestino;
	private Vendamaterial vendamaterialdestino;
	private List<Garantiareformaitemconstatacao> listaGarantiareformaitemconstatacao;
	
	//TRANSIENTS
	private Money valorCreditoGarantia;
	private boolean saveListaGarantiareformaitemconstatacao = false;
	private Motivodevolucao motivodevolucaoConstatacao1;
	private Motivodevolucao motivodevolucaoConstatacao2;
	private Motivodevolucao motivodevolucaoConstatacao3;
	private Motivodevolucao motivodevolucaoConstatacao4;
	private Motivodevolucao motivodevolucaoConstatacao5;
	

	@Id
	@GeneratedValue(generator="sq_garantiareformaitem", strategy=GenerationType.AUTO)
	@DisplayName("Id")
	public Integer getCdgarantiareformaitem() {
		return cdgarantiareformaitem;
	}

	public void setCdgarantiareformaitem(Integer cdgarantiareformaitem) {
		this.cdgarantiareformaitem = cdgarantiareformaitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareforma")
	public Garantiareforma getGarantiareforma() {
		return garantiareforma;
	}

	public void setGarantiareforma(Garantiareforma garantiareforma) {
		this.garantiareforma = garantiareforma;
	}

	@Required
	@DisplayName("Serviço garantido")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Required
	@DisplayName("Pneu")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneu")
	public Pneu getPneu() {
		return pneu;
	}

	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	@DisplayName("Resíduo da Banda")
	public Double getResiduobanda() {
		return residuobanda;
	}

	public void setResiduobanda(Double residuobanda) {
		this.residuobanda = residuobanda;
	}

	@DisplayName("Constatação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconstatacao")
	public Motivodevolucao getConstatacao() {
		return constatacao;
	}

	public void setConstatacao(Motivodevolucao constatacao) {
		this.constatacao = constatacao;
	}

	@Required
	@DisplayName("Tipo de garantia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiatipo")
	public Garantiatipo getGarantiatipo() {
		return garantiatipo;
	}

	public void setGarantiatipo(Garantiatipo garantiatipo) {
		this.garantiatipo = garantiatipo;
	}

	@DisplayName("% de Garantia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiatipopercentual")
	public Garantiatipopercentual getGarantiatipopercentual() {
		return garantiatipopercentual;
	}

	public void setGarantiatipopercentual(
			Garantiatipopercentual garantiatipopercentual) {
		this.garantiatipopercentual = garantiatipopercentual;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordemmaterial", insertable=false, updatable=false)
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}

	@Transient
	@DisplayName("Valor")
	public Money getValorCreditoGarantia() {
		return valorCreditoGarantia;
	}
	public void setValorCreditoGarantia(Money valorCreditoGarantia) {
		this.valorCreditoGarantia = valorCreditoGarantia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterialdestino")
	public Pedidovendamaterial getPedidovendamaterialdestino() {
		return pedidovendamaterialdestino;
	}
	public void setPedidovendamaterialdestino(
			Pedidovendamaterial pedidovendamaterialdestino) {
		this.pedidovendamaterialdestino = pedidovendamaterialdestino;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterialdestino")
	public Vendamaterial getVendamaterialdestino() {
		return vendamaterialdestino;
	}
	public void setVendamaterialdestino(Vendamaterial vendamaterialdestino) {
		this.vendamaterialdestino = vendamaterialdestino;
	}
	
	@OneToMany(mappedBy="garantiareformaitem")
	public List<Garantiareformaitemconstatacao> getListaGarantiareformaitemconstatacao() {
		return listaGarantiareformaitemconstatacao;
	}
	
	public void setListaGarantiareformaitemconstatacao(List<Garantiareformaitemconstatacao> listaGarantiareformaitemconstatacao) {
		this.listaGarantiareformaitemconstatacao = listaGarantiareformaitemconstatacao;
	}
	
	@Transient
	public boolean isSaveListaGarantiareformaitemconstatacao() {
		return saveListaGarantiareformaitemconstatacao;
	}
	
	public void setSaveListaGarantiareformaitemconstatacao(boolean saveListaGarantiareformaitemconstatacao) {
		this.saveListaGarantiareformaitemconstatacao = saveListaGarantiareformaitemconstatacao;
	}
	
	@Transient
	@DisplayName("Constatação")
	public Motivodevolucao getMotivodevolucaoConstatacao1() {
		return motivodevolucaoConstatacao1;
	}

	@Transient
	@DisplayName("Constatação")
	public Motivodevolucao getMotivodevolucaoConstatacao2() {
		return motivodevolucaoConstatacao2;
	}

	@Transient
	@DisplayName("Constatação")
	public Motivodevolucao getMotivodevolucaoConstatacao3() {
		return motivodevolucaoConstatacao3;
	}

	@Transient
	@DisplayName("Constatação")
	public Motivodevolucao getMotivodevolucaoConstatacao4() {
		return motivodevolucaoConstatacao4;
	}

	@Transient
	@DisplayName("Constatação")
	public Motivodevolucao getMotivodevolucaoConstatacao5() {
		return motivodevolucaoConstatacao5;
	}

	public void setMotivodevolucaoConstatacao1(Motivodevolucao motivodevolucaoConstatacao1) {
		this.motivodevolucaoConstatacao1 = motivodevolucaoConstatacao1;
	}

	public void setMotivodevolucaoConstatacao2(Motivodevolucao motivodevolucaoConstatacao2) {
		this.motivodevolucaoConstatacao2 = motivodevolucaoConstatacao2;
	}

	public void setMotivodevolucaoConstatacao3(Motivodevolucao motivodevolucaoConstatacao3) {
		this.motivodevolucaoConstatacao3 = motivodevolucaoConstatacao3;
	}

	public void setMotivodevolucaoConstatacao4(Motivodevolucao motivodevolucaoConstatacao4) {
		this.motivodevolucaoConstatacao4 = motivodevolucaoConstatacao4;
	}

	public void setMotivodevolucaoConstatacao5(Motivodevolucao motivodevolucaoConstatacao5) {
		this.motivodevolucaoConstatacao5 = motivodevolucaoConstatacao5;
	}
	
	@Transient
	public List<Motivodevolucao> getListaMotivodevolucaoConstatacao() {
		List<Motivodevolucao> listaMotivodevolucaoConstatacao = new ArrayList<Motivodevolucao>();
		
		if (motivodevolucaoConstatacao1!=null){
			listaMotivodevolucaoConstatacao.add(motivodevolucaoConstatacao1);
		}
		
		if (motivodevolucaoConstatacao2!=null){
			listaMotivodevolucaoConstatacao.add(motivodevolucaoConstatacao2);
		}
		
		if (motivodevolucaoConstatacao3!=null){
			listaMotivodevolucaoConstatacao.add(motivodevolucaoConstatacao3);
		}
		
		if (motivodevolucaoConstatacao4!=null){
			listaMotivodevolucaoConstatacao.add(motivodevolucaoConstatacao4);
		}
		
		if (motivodevolucaoConstatacao5!=null){
			listaMotivodevolucaoConstatacao.add(motivodevolucaoConstatacao5);
		}
		
		return listaMotivodevolucaoConstatacao;
	}
	
	@Transient
	public String getConstatacoes() {
		if (Hibernate.isInitialized(listaGarantiareformaitemconstatacao) && listaGarantiareformaitemconstatacao!=null){
			return CollectionsUtil.listAndConcatenate(listaGarantiareformaitemconstatacao, "motivodevolucao.descricao", "<br/>");
		}else {
			return "";
		}
			
	}
}
