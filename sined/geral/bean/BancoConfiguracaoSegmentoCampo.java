package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoPreencherEnum;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaosegmentocampo",sequenceName="sq_bancoconfiguracaosegmentocampo")
public class BancoConfiguracaoSegmentoCampo {

	private Integer cdbancoconfiguracaosegmentocampo;
	private BancoConfiguracaoSegmento bancoConfiguracaoSegmento;
	private Integer posInicial;
	private Integer tamanho;
	private String campo;
	private String descricao;
	private boolean completarZero = false;
	private boolean completarDireita = false;
	private BancoConfiguracaoTipoPreencherEnum preencherCom;
	private boolean obrigatorio = false;
	private String formatoData;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaosegmentocampo")
	@Required
	public Integer getCdbancoconfiguracaosegmentocampo() {
		return cdbancoconfiguracaosegmentocampo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaosegmento")
	@Required
	public BancoConfiguracaoSegmento getBancoConfiguracaoSegmento() {
		return bancoConfiguracaoSegmento;
	}
	@DisplayName("Pos. inicial")
	@Required
	public Integer getPosInicial() {
		return posInicial;
	}
	@DisplayName("Tamanho")
	@Required
	public Integer getTamanho() {
		return tamanho;
	}
	@DisplayName("Campo")
	@Required
	public String getCampo() {
		return campo;
	}
	@DisplayName("Descri��o")
	@DescriptionProperty
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Comp. com zeros?")
	public boolean isCompletarZero() {
		return completarZero;
	}
	@DisplayName("� direita?")
	public boolean isCompletarDireita() {
		return completarDireita;
	}
	@DisplayName("Se nulo preencher")
	@Required
	public BancoConfiguracaoTipoPreencherEnum getPreencherCom() {
		return preencherCom;
	}
	@DisplayName("Obrigat�rio")
	public boolean isObrigatorio() {
		return obrigatorio;
	}
	@DisplayName("Formato de data")
	public String getFormatoData() {
		return formatoData;
	}
	public void setCdbancoconfiguracaosegmentocampo(
			Integer cdbancoconfiguracaosegmentocampo) {
		this.cdbancoconfiguracaosegmentocampo = cdbancoconfiguracaosegmentocampo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setPosInicial(Integer posInicial) {
		this.posInicial = posInicial;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	public void setBancoConfiguracaoSegmento(
			BancoConfiguracaoSegmento bancoConfiguracaoSegmento) {
		this.bancoConfiguracaoSegmento = bancoConfiguracaoSegmento;
	}
	public void setCompletarZero(boolean completarZero) {
		this.completarZero = completarZero;
	}
	public void setCompletarDireita(boolean completarDireita) {
		this.completarDireita = completarDireita;
	}
	public void setPreencherCom(BancoConfiguracaoTipoPreencherEnum preencherCom) {
		this.preencherCom = preencherCom;
	}
	public void setObrigatorio(boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	public void setFormatoData(String formatoData) {
		this.formatoData = formatoData;
	}
	
	//Transients
	
	private Integer posFinal;
	
	@Transient
	@DisplayName("Pos. final")
	public Integer getPosFinal() {
		if (this.posInicial != null && this.tamanho != null)
			posFinal = this.posInicial + this.tamanho - 1;
		return posFinal;
	}
	
	public void setPosFinal(Integer posFinal) {
		this.posFinal = posFinal;
	}
}
