package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_campolistagemusuario", sequenceName = "sq_campolistagemusuario")
public class Campolistagemusuario {
	
	private Integer cdcampolistagemusuario;
	private Usuario usuario;
	private Tela tela;
	private String camposordem;
	private String camposexibicao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campolistagemusuario")	
	public Integer getCdcampolistagemusuario() {
		return cdcampolistagemusuario;
	}
	
	@DisplayName("Usu�rio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	public Usuario getUsuario() {
		return usuario;
	}
	
	@DisplayName("Tela")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtela")
	public Tela getTela() {
		return tela;
	}
	
	@DisplayName("Ordem dos Campos")
	public String getCamposordem() {
		return camposordem;
	}
	
	@DisplayName("Campos de Exibi��o")
	public String getCamposexibicao() {
		return camposexibicao;
	}
	
	public void setCdcampolistagemusuario(Integer cdcampolistagemusuario) {
		this.cdcampolistagemusuario = cdcampolistagemusuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setTela(Tela tela) {
		this.tela = tela;
	}
	public void setCamposordem(String camposordem) {
		this.camposordem = camposordem;
	}
	public void setCamposexibicao(String camposexibicao) {
		this.camposexibicao = camposexibicao;
	}
}
