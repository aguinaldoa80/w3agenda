package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@Table(name = "animal")
@SequenceGenerator(name = "sq_animal", sequenceName = "sq_animal")
public class Animal implements Log {

	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer cdanimal;
	protected String nome;
	protected Cliente cliente;
	protected Date dtnascimento;
	protected Date dtobito;
	protected String pedigree;
	protected String chip;
	protected AnimalSituacao animalSituacao;
	protected List<AnimalVacina> vacinas;

	public Animal() {
	}

	public Animal(Integer cdAnimal) {
		this.cdanimal = cdAnimal;
	}

	@Id
	@Column(name = "cdanimal")
	@GeneratedValue(generator = "sq_animal", strategy = GenerationType.AUTO)
	public Integer getCdanimal() {
		return cdanimal;
	}

	@Column(name = "nome")
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcliente")
	@Required
	public Cliente getCliente() {
		return cliente;
	}

	@Column(name = "dtnascimento")
	@Required
	@DisplayName("Data de Nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}

	@Column(name = "dtobito")
	@DisplayName("Data de �bito")
	public Date getDtobito() {
		return dtobito;
	}

	@Column(name = "pedigree")
	@MaxLength(50)
	public String getPedigree() {
		return pedigree;
	}

	@Column(name = "chip")
	public String getChip() {
		return chip;
	}

	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdanimalsituacao")
	@Required
	@DisplayName("Animal Situa��o")
	public AnimalSituacao getAnimalSituacao() {
		return animalSituacao;
	}
	
	@OneToMany(mappedBy="animal")
	public List<AnimalVacina> getVacinas() {
		return vacinas;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdanimal(Integer cdanimal) {
		this.cdanimal = cdanimal;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}

	public void setDtobito(Date dtobito) {
		this.dtobito = dtobito;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public void setAnimalSituacao(AnimalSituacao animalSituacao) {
		this.animalSituacao = animalSituacao;
	}
	
	public void setVacinas(List<AnimalVacina> vacinas) {
		this.vacinas = vacinas;
	}
	

	public boolean equals(Object obj) {
		if(obj instanceof Animal){
			return ((Animal) obj).cdanimal.equals(this.cdanimal);
		}else {
			return false;
		}
	}
}