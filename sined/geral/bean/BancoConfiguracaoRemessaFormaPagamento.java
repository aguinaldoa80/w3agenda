package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessaformapagamento",sequenceName="sq_bancoconfiguracaoremessaformapagamento")
public class BancoConfiguracaoRemessaFormaPagamento {

	private Integer cdbancoconfiguracaoremessaformapagamento;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private BancoFormapagamento bancoFormapagamento;
	private Boolean agrupamentofornecedor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessaformapagamento")
	public Integer getCdbancoconfiguracaoremessaformapagamento() {
		return cdbancoconfiguracaoremessaformapagamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	@Required
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoformapagamento")
	@Required
	public BancoFormapagamento getBancoFormapagamento() {
		return bancoFormapagamento;
	}
	
	@DisplayName("Agrupar documentos com mesmo fornecedor")
	public Boolean getAgrupamentofornecedor() {
		return agrupamentofornecedor;
	}
	
	public void setAgrupamentofornecedor(Boolean agrupamentofornecedor) {
		this.agrupamentofornecedor = agrupamentofornecedor;
	}

	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}
	
	public void setBancoFormapagamento(BancoFormapagamento bancoFormapagamento) {
		this.bancoFormapagamento = bancoFormapagamento;
	}
	
	public void setCdbancoconfiguracaoremessaformapagamento(
			Integer cdbancoconfiguracaoremessaformapagamento) {
		this.cdbancoconfiguracaoremessaformapagamento = cdbancoconfiguracaoremessaformapagamento;
	}	
}
