package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_unidademedida", sequenceName = "sq_unidademedida")
@DisplayName("Unidade de medida")
public class Unidademedida implements Log {

	protected Integer cdunidademedida;
	protected String nome;
	protected String simbolo;
	protected Boolean ativo;
	protected Integer casasdecimaisestoque;
	protected Boolean etiquetaunica;
	
	protected List<Unidademedidaconversao> listaUnidademedidaconversao = new ListSet<Unidademedidaconversao>(Unidademedidaconversao.class);
	protected List<UnidadeMedidaHistorico> listaUnidadeMedidaHistorico = new ListSet<UnidadeMedidaHistorico>(UnidadeMedidaHistorico.class);

	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	//transient
	protected Double qtde;
	protected Boolean prioritariacompra = false;
	protected Double fracao;
	protected Double qtdereferencia;
	
	public Unidademedida(){
		if(this.cdunidademedida == null){
			this.ativo = true;
		}
	}
	
	public Unidademedida(Integer cdunidademedida){
		this.cdunidademedida = cdunidademedida;
	}
	
	public Unidademedida(Integer cdunidademedida,String simbolo){
		this.cdunidademedida = cdunidademedida;
		this.simbolo = simbolo;
	}
	
	public Unidademedida(String simbolo){
		this.simbolo = simbolo;
	}
	
	public Unidademedida(Integer cdunidademedida, String nome, String simbolo) {
		this.cdunidademedida = cdunidademedida;
		this.nome = nome;
		this.simbolo = simbolo;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_unidademedida")
	public Integer getCdunidademedida() {
		return cdunidademedida;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@DisplayName("S�mbolo")
	@MaxLength(10)
	public String getSimbolo() {
		return simbolo;
	}
	
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	
	@Required
	@MaxLength(2)
	@DisplayName("Casas Decimais - Estoque")
	public Integer getCasasdecimaisestoque() {
		return casasdecimaisestoque;
	}
	@DisplayName("Etiqueta �nica do material no recebimento?")
	public Boolean getEtiquetaunica() {
		return etiquetaunica;
	}
	public void setCasasdecimaisestoque(Integer casasdecimaisestoque) {
		this.casasdecimaisestoque = casasdecimaisestoque;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdunidademedida(Integer cdunidademedida) {
		this.cdunidademedida = cdunidademedida;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	public void setEtiquetaunica(Boolean etiquetaunica) {
		this.etiquetaunica = etiquetaunica;
	}
	@DisplayName("Convers�o")
	@OneToMany(mappedBy="unidademedida")
	public List<Unidademedidaconversao> getListaUnidademedidaconversao() {
		return listaUnidademedidaconversao;
	}
	public void setListaUnidademedidaconversao(
			List<Unidademedidaconversao> listaUnidademedidaconversao) {
		this.listaUnidademedidaconversao = listaUnidademedidaconversao;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="unidadeMedida")
	public List<UnidadeMedidaHistorico> getListaUnidadeMedidaHistorico() {
		return listaUnidadeMedidaHistorico;
	}
	
	public void setListaUnidadeMedidaHistorico(
			List<UnidadeMedidaHistorico> listaUnidadeMedidaHistorico) {
		this.listaUnidadeMedidaHistorico = listaUnidadeMedidaHistorico;
	}
	
	@Transient
	public Double getQtde() {
		return qtde;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdunidademedida == null) ? 0 : cdunidademedida.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidademedida other = (Unidademedida) obj;
		if (cdunidademedida == null) {
			if (other.cdunidademedida != null)
				return false;
		} else if (!cdunidademedida.equals(other.cdunidademedida))
			return false;
		return true;
	}

	@Transient
	public Boolean getPrioritariacompra() {
		return prioritariacompra;
	}
	
	public void setPrioritariacompra(Boolean prioritariacompra) {
		this.prioritariacompra = prioritariacompra;
	}
	@Transient
	public Double getFracao() {
		return fracao;
	}
	
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
}
