package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_cargoexameobrigatorio", sequenceName = "sq_cargoexameobrigatorio")
public class Cargoexameobrigatorio {

	protected Integer cdcargoexameobrigatorio;
	protected Exametipo exametipo;
	protected Examenatureza examenatureza;
	protected Frequencia frequencia;
	protected Cargo cargo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargoexameobrigatorio")
	public Integer getCdcargoexameobrigatorio() {
		return cdcargoexameobrigatorio;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdexametipo")
	public Exametipo getExametipo() {
		return exametipo;
	}
	@Required
	@DisplayName("Natureza")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdexamenatureza")
	public Examenatureza getExamenatureza() {
		return examenatureza;
	}
	@Required
	@DisplayName("Periodicidade")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdfrequencia")
	public Frequencia getFrequencia() {
		return frequencia;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	public void setCdcargoexameobrigatorio(Integer cdcargoexameobrigatorio) {
		this.cdcargoexameobrigatorio = cdcargoexameobrigatorio;
	}
	public void setExametipo(Exametipo exametipo) {
		this.exametipo = exametipo;
	}
	public void setExamenatureza(Examenatureza examenatureza) {
		this.examenatureza = examenatureza;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
}
