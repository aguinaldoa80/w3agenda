package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.CategoriaClienteLogin;

@Entity
@SequenceGenerator(name = "sq_categoria", sequenceName = "sq_categoria")
public class Categoria {

	protected Integer cdcategoria;
	protected Integer item;
	protected String nome;
	protected Boolean cliente;
	protected Boolean fornecedor;
	protected Categoria categoriapai;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Vcategoria vcategoria;
	protected CategoriaClienteLogin categoriaClienteLogin;
	protected Set<Pessoacategoria> listaPessoacategoria = new ListSet<Pessoacategoria>(Pessoacategoria.class);
	private String formato;
	
	//Transientes
	private List<Categoria> filhos;

	public Categoria() {

	}

	public Categoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}

	@Id
	@GeneratedValue(generator = "sq_categoria", strategy = GenerationType.AUTO)
	public Integer getCdcategoria() {
		return cdcategoria;
	}
	
	public Integer getItem() {
		return item;
	}

	@Required
	@DescriptionProperty
	@MaxLength(50)
	@DisplayName("Descri��o")
	public String getNome() {
		return nome;
	}
	
	public void setItem(Integer item) {
		this.item = item;
	}

	public Boolean getCliente() {
		return cliente;
	}

	public Boolean getFornecedor() {
		return fornecedor;
	}

	@JoinColumn(name="cdcategoriapai")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Categoria superior")
	public Categoria getCategoriapai() {
		return categoriapai;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoria", insertable=false, updatable=false)
	public Vcategoria getVcategoria() {
		return vcategoria;
	}
	
	@Column(name="categorialogin")
	@DisplayName("Categoria Login")
	public CategoriaClienteLogin getCategoriaClienteLogin() {
		return categoriaClienteLogin;
	}
	
	@OneToMany(mappedBy="categoria")
	public Set<Pessoacategoria> getListaPessoacategoria() {
		return listaPessoacategoria;
	}
	
	@Required
	@DisplayName("Formato (Zeros)")
	public String getFormato() {
		return formato;
	}
	
	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	public void setListaPessoacategoria(
			Set<Pessoacategoria> listaPessoacategoria) {
		this.listaPessoacategoria = listaPessoacategoria;
	}
	
	public void setCategoriaClienteLogin(
			CategoriaClienteLogin categoriaClienteLogin) {
		this.categoriaClienteLogin = categoriaClienteLogin;
	}

	public void setCdcategoria(Integer cdcategoria) {
		this.cdcategoria = cdcategoria;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}
	
	public void setFornecedor(Boolean fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setCategoriapai(Categoria categoriapai) {
		this.categoriapai = categoriapai;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setVcategoria(Vcategoria vcategoria) {
		this.vcategoria = vcategoria;
	}
	
	@Transient
	public String getDescricaoInputPersonalizado() {

		if (getVcategoria() != null) {
			return getVcategoria().getIdentificador()
					+ " - "
					+ getNome()
					+ (getVcategoria().getArvorepai() != null ? " ("
							+ getVcategoria().getArvorepai() + ")" : "");
		} else
			return "";
	}
	

	@Transient
	public List<Categoria> getFilhos() {
		return filhos;
	}
	
	public void setFilhos(List<Categoria> filhos) {
		this.filhos = filhos;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		if (cdcategoria == null) {
			if (other.cdcategoria != null)
				return false;
		} else if (!cdcategoria.equals(other.cdcategoria))
			return false;
		return true;
	}
}
