package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentodigitalusuario",sequenceName="sq_documentodigitalusuario")
public class Documentodigitalusuario implements Log{

	protected Integer cddocumentodigitalusuario;
	protected String email;
	protected String nome;
	protected Cpf cpf;
	protected Date dtnascimento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected String cpfFormatado;
	
	@Id
	@GeneratedValue(generator="sq_documentodigitalusuario",strategy=GenerationType.AUTO)
	public Integer getCddocumentodigitalusuario() {
		return cddocumentodigitalusuario;
	}
	
	@MaxLength(500)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	@Required
	@DescriptionProperty
	@MaxLength(500)
	@DisplayName("E-mail")
	@Email
	public String getEmail() {
		return email;
	}

	@DisplayName("CPF")
	public Cpf getCpf() {
		return cpf;
	}

	@DisplayName("Data de nascimento")
	public Date getDtnascimento() {
		return dtnascimento;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}

	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCddocumentodigitalusuario(Integer cddocumentodigitalusuario) {
		this.cddocumentodigitalusuario = cddocumentodigitalusuario;
	}
	
	
	//Log
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getCpfFormatado() {
		return cpf != null ? cpf.toString() : cpfFormatado;
	}
	
	public void setCpfFormatado(String cpfFormatado) {
		this.cpfFormatado = cpfFormatado;
	}
	
}
