package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@DisplayName("Tipo de ajuste")
public class Tipokmajuste{
	
		public final static Tipokmajuste MARCHARE = new Tipokmajuste(0, "Marcha r�");
		public final static Tipokmajuste TROCAODOMETRO = new Tipokmajuste(1, "Troca de od�metro");
		public final static Tipokmajuste OUTRO = new Tipokmajuste(2, "Outros");
		public final static Tipokmajuste INCIAL = new Tipokmajuste(3, "Inicial");
    
	 	protected Integer cdtipokmajuste;
	    protected String descricao;	    
	    
	    public Tipokmajuste(){
	    	
	    }
	    
	    public Tipokmajuste(Integer cdtipokmajuste, String descricao){
	    	this.cdtipokmajuste = cdtipokmajuste;
	    	this.descricao = descricao;
	    }
	    
	    @Id	   	    	   
	    public Integer getCdtipokmajuste() {
			return cdtipokmajuste;
		}
	    
	    @DescriptionProperty
	    public String getDescricao() {
			return descricao;
		}
	    
	    public void setCdtipokmajuste(Integer cdtipokmajuste) {
			this.cdtipokmajuste = cdtipokmajuste;
		}
	    
	    public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime
					* result
					+ ((cdtipokmajuste == null) ? 0 : cdtipokmajuste.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final Tipokmajuste other = (Tipokmajuste) obj;
			if (cdtipokmajuste == null) {
				if (other.cdtipokmajuste != null)
					return false;
			} else if (!cdtipokmajuste.equals(other.cdtipokmajuste))
				return false;
			return true;
		}
	    
	    
	
}