package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Processojuridicoclienteenvolvido;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohonorario;
import br.com.linkcom.sined.geral.bean.view.Vprocessojuridico;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Processo jur�dico")
@SequenceGenerator(name = "sq_processojuridico", sequenceName = "sq_processojuridico")
public class Processojuridico implements Log, PermissaoClienteEmpresa{
	
	protected Integer cdprocessojuridico;
	protected Date data;
	protected Area area;
	protected Areatipo areatipo;
	protected Colaborador advogado;
	protected String sintese;
	protected Money valorcausa;
	protected Processojuridico processorelacionado;
	protected Tipohonorario tipohonorario;
	protected Money honorario;
	protected Money valorcontrato;
	protected Prazopagamento prazopagamento;
	protected Processojuridicosituacao processojuridicosituacao; 
	
	protected Vprocessojuridico vprocessojuridico;
	protected Projeto projeto;
	
	protected Set<Processojuridicocliente> listaProcessojuridicocliente = new ListSet<Processojuridicocliente>(Processojuridicocliente.class);
	protected List<Processojuridicoinstancia> listaProcessojuridicoinstancia = new ListSet<Processojuridicoinstancia>(Processojuridicoinstancia.class);
	protected Set<Processojuridicoarquivo> listaProcessojuridicoarquivo = new ListSet<Processojuridicoarquivo>(Processojuridicoarquivo.class);
	protected Set<Processojuridicoquestionario> listaProcessojuridicoquestionario = new ListSet<Processojuridicoquestionario>(Processojuridicoquestionario.class);
	protected Set<Processojuridico> listaprocessorelacionado = new ListSet<Processojuridico>(Processojuridico.class);
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridico")
	public Integer getCdprocessojuridico() {
		return cdprocessojuridico;
	}
	@Required
	public Date getData() {
		return data;
	}
	@DisplayName("S�ntese da A��o")
	@MaxLength(1000)
	@Required
	public String getSintese() {
		return sintese;
	}
	@DisplayName("Valor da causa")
	public Money getValorcausa() {
		return valorcausa;
	}
	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridicosituacao")
	public Processojuridicosituacao getProcessojuridicosituacao() {
		return processojuridicosituacao;
	}
	
	@Required
	@DisplayName("Advogado")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdadvogado")
	public Colaborador getAdvogado() {
		return advogado;
	}
	
	@Required
	@DisplayName("�rea")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdarea")
	public Area getArea() {
		return area;
	}
	
	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdareatipo")
	public Areatipo getAreatipo() {
		return areatipo;
	}
	
	@DisplayName("Processo Apenso")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprocessorelacionado")
	public Processojuridico getProcessorelacionado() {
		return processorelacionado;
	}
	
	@DisplayName("Apenso")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="processorelacionado")
	public Set<Processojuridico> getListaprocessorelacionado() {
		return listaprocessorelacionado;
	}
	
	@OneToMany(mappedBy="processojuridico")
	@DisplayName("Envolvidos")
	public Set<Processojuridicocliente> getListaProcessojuridicocliente() {
		return listaProcessojuridicocliente;
	}
	@OneToMany(mappedBy="processojuridico")
	@DisplayName("Inst�ncia")
	public List<Processojuridicoinstancia> getListaProcessojuridicoinstancia() {
		return listaProcessojuridicoinstancia;
	}
	@DisplayName("Arquivos")
	@OneToMany(mappedBy="processojuridico")
	public Set<Processojuridicoarquivo> getListaProcessojuridicoarquivo() {
		return listaProcessojuridicoarquivo;
	}
	@DisplayName("Tipo de honor�rio")
	public Tipohonorario getTipohonorario() {
		return tipohonorario;
	}
	@DisplayName("Honor�rio")
	public Money getHonorario() {
		return honorario;
	}
	@DisplayName("Valor do contrato")
	public Money getValorcontrato() {
		return valorcontrato;
	}
	@DisplayName("Condi��o de pagamento")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@OneToMany(mappedBy="processojuridico")
	public Set<Processojuridicoquestionario> getListaProcessojuridicoquestionario() {
		return listaProcessojuridicoquestionario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridico", insertable=false, updatable=false)
	public Vprocessojuridico getVprocessojuridico() {
		return vprocessojuridico;
	}
	
	@DisplayName("Projeto")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}

	public void setVprocessojuridico(Vprocessojuridico vprocessojuridico) {
		this.vprocessojuridico = vprocessojuridico;
	}
	
	public void setListaProcessojuridicoquestionario(
			Set<Processojuridicoquestionario> listaProcessojuridicoquestionario) {
		this.listaProcessojuridicoquestionario = listaProcessojuridicoquestionario;
	}
	
	public void setTipohonorario(Tipohonorario tipohonorario) {
		this.tipohonorario = tipohonorario;
	}
	public void setHonorario(Money honorario) {
		this.honorario = honorario;
	}
	public void setValorcontrato(Money valorcontrato) {
		this.valorcontrato = valorcontrato;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setCdprocessojuridico(Integer cdprocessojuridico) {
		this.cdprocessojuridico = cdprocessojuridico;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setSintese(String sintese) {
		this.sintese = sintese;
	}
	public void setValorcausa(Money valorcausa) {
		this.valorcausa = valorcausa;
	}
	public void setProcessojuridicosituacao(Processojuridicosituacao processojuridicosituacao) {
		this.processojuridicosituacao = processojuridicosituacao;
	}
	public void setAdvogado(Colaborador advogado) {
		this.advogado = advogado;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public void setAreatipo(Areatipo areatipo) {
		this.areatipo = areatipo;
	}
	public void setProcessorelacionado(Processojuridico processojuridico) {
		this.processorelacionado = processojuridico;
	}
	public void setListaProcessojuridicocliente(Set<Processojuridicocliente> listaProcessojuridicocliente) {
		this.listaProcessojuridicocliente = listaProcessojuridicocliente;
	}
	public void setListaProcessojuridicoinstancia(List<Processojuridicoinstancia> listaProlistaProcessojuridicoinstanciacessojuridicoinstanciaaProcessojuridicoinstanciacessojuridicoinstancia) {
		this.listaProcessojuridicoinstancia = listaProlistaProcessojuridicoinstanciacessojuridicoinstanciaaProcessojuridicoinstanciacessojuridicoinstancia;
	}
	public void setListaProcessojuridicoarquivo(Set<Processojuridicoarquivo> listaProcessojuridicoarquivo) {
		this.listaProcessojuridicoarquivo = listaProcessojuridicoarquivo;
	}
	public void setListaprocessorelacionado(Set<Processojuridico> listaprocessorelacionado) {
		this.listaprocessorelacionado = listaprocessorelacionado;
	}
	
	public String subQueryClienteEmpresa() {
		return "select processojuridicoSubQueryClienteEmpresa.cdprocessojuridico " +
				"from Processojuridico processojuridicoSubQueryClienteEmpresa " +
				"left outer join processojuridicoSubQueryClienteEmpresa.listaProcessojuridicocliente listaProcessojuridicoclienteSubQueryClienteEmpresa " +
				"left outer join listaProcessojuridicoclienteSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	//============ TRANSIENT'S ==================
	protected List<Arquivo> listaArquivos = new ListSet<Arquivo>(Arquivo.class);
	protected String inserirAndamento;
	protected Boolean parcelaContratoAberto = false;
	protected String descAndamentoReport;
	
	@Transient
	public String getInserirAndamento() {
		return inserirAndamento;
	}
	public void setInserirAndamento(String inserirAndamento) {
		this.inserirAndamento = inserirAndamento;
	}
	
	@Transient
	public Boolean getParcelaContratoAberto() {
		return parcelaContratoAberto;
	}
	public void setParcelaContratoAberto(Boolean parcelaContratoAberto) {
		this.parcelaContratoAberto = parcelaContratoAberto;
	}
	@Transient
	public List<Arquivo> getListaArquivos() {
		return listaArquivos;
	}
	public void setListaArquivos(List<Arquivo> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}
	@Transient
	@DisplayName("Processo")
	public String getUltimaInstancia(){
		int maior = 0;
		String numMaior = "";
		for (Processojuridicoinstancia lista : listaProcessojuridicoinstancia) {
			if(lista.getProcessojuridicoinstanciatipo().getCdprocessojuridicoinstanciatipo() > maior){
				maior = lista.getProcessojuridicoinstanciatipo().getCdprocessojuridicoinstanciatipo();
				numMaior = lista.getNumero();
			}
		}
		return numMaior;
	}
	
	@Transient
	@DisplayName("Cliente(s)")
	public String getClientes(){
		String clientes = "";
		
		if(listaProcessojuridicocliente != null){
			for (Processojuridicocliente pc : listaProcessojuridicocliente) {
				if(pc.getEnvolvido().equals(Processojuridicoclienteenvolvido.CLIENTE) && pc.getCliente() != null && pc.getCliente().getNome() != null){
					clientes += pc.getCliente().getNome() + "<BR>";
				}
			}
			if(!clientes.equals("")){
				clientes = clientes.substring(0, clientes.length() - 4);
			}
		}
		
		if(clientes.equals("")) return "-";
		return clientes;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"vprocessojuridico"})
	public String getDescricaoCombo(){
		return getVprocessojuridico() != null && getVprocessojuridico().getDescricao() != null && !getVprocessojuridico().getDescricao().equals("") ? getVprocessojuridico().getDescricao() : (getCdprocessojuridico() != null ? getCdprocessojuridico().toString() : "<ERRO NA DESCRI��O>");
	}
	
	@Transient
	public String getDescAndamentoReport() {
		return descAndamentoReport;
	}
	public void setDescAndamentoReport(String descAndamentoReport) {
		this.descAndamentoReport = descAndamentoReport;
	}
	
	
}
