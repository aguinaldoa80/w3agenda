package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_orcamentomaterialitem", sequenceName = "sq_orcamentomaterialitem")
public class Orcamentomaterialitem implements Log{
	
	protected Integer cdorcamentomaterialitem;
	protected Orcamentomaterial orcamentomaterial;
	protected Integer cdmaterial;
	protected String descricao;
	protected Unidademedida unidademedida;
	protected Integer qtde;
	protected Double ipi;
	protected Boolean ipiincluso;
	protected Double icms;
	protected Money valorunitariocompra;
	protected Boolean faturamentodireto;
	protected Money valorunitariovenda;
	protected Money totalcompra;
	protected Money totalvenda;
	protected Materialclasse materialclasse;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;

	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_orcamentomaterialitem")	
	public Integer getCdorcamentomaterialitem() {
		return cdorcamentomaterialitem;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamentomaterial")	
	public Orcamentomaterial getOrcamentomaterial() {
		return orcamentomaterial;
	}

	public Integer getCdmaterial() {
		return cdmaterial;
	}

	public String getDescricao() {
		return descricao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	public Integer getQtde() {
		return qtde;
	}

	public Double getIpi() {
		return ipi;
	}

	public Boolean getIpiincluso() {
		return ipiincluso;
	}

	public Double getIcms() {
		return icms;
	}

	public Money getValorunitariocompra() {
		return valorunitariocompra;
	}

	public Boolean getFaturamentodireto() {
		return faturamentodireto;
	}

	public Money getValorunitariovenda() {
		return valorunitariovenda;
	}
	
	public Money getTotalcompra() {
		return totalcompra;
	}

	public Money getTotalvenda() {
		return totalvenda;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmaterialclasse")
	public Materialclasse getMaterialclasse() {
		return materialclasse;
	}
	
	public void setMaterialclasse(Materialclasse materialclasse) {
		this.materialclasse = materialclasse;
	}

	public void setTotalcompra(Money totalcompra) {
		this.totalcompra = totalcompra;
	}

	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}

	public void setOrcamentomaterial(Orcamentomaterial orcamentomaterial) {
		this.orcamentomaterial = orcamentomaterial;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}

	public void setIpi(Double ipi) {
		this.ipi = ipi;
	}

	public void setIpiincluso(Boolean ipiincluso) {
		this.ipiincluso = ipiincluso;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setValorunitariocompra(Money valorunitariocompra) {
		this.valorunitariocompra = valorunitariocompra;
	}

	public void setFaturamentodireto(Boolean faturamentodireto) {
		this.faturamentodireto = faturamentodireto;
	}

	public void setValorunitariovenda(Money valorunitariovenda) {
		this.valorunitariovenda = valorunitariovenda;
	}

	public void setCdorcamentomaterialitem(Integer cdorcamentomaterialitem) {
		this.cdorcamentomaterialitem = cdorcamentomaterialitem;
	}
	
//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}