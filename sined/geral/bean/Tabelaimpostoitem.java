package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaimpostoitemtipo;

@Entity
@SequenceGenerator(name = "sq_tabelaimpostoitem", sequenceName = "sq_tabelaimpostoitem")
public class Tabelaimpostoitem {

	protected Integer cdtabelaimpostoitem;
	protected Tabelaimposto tabelaimposto;
	protected String codigo;
	protected String extipi;
	protected Tabelaimpostoitemtipo tipo;
	protected String descricao;
	protected Double nacionalfederal;
	protected Double importadofederal;
	protected Double estadual;
	protected Double municipal;
	
	@Id
	@GeneratedValue(generator = "sq_tabelaimpostoitem", strategy = GenerationType.AUTO)
	public Integer getCdtabelaimpostoitem() {
		return cdtabelaimpostoitem;
	}

	@Required
	@JoinColumn(name = "cdtabelaimposto")
	@ManyToOne(fetch = FetchType.LAZY)
	public Tabelaimposto getTabelaimposto() {
		return tabelaimposto;
	}

	@Required
	@DisplayName("C�digo")
	@MaxLength(50)
	public String getCodigo() {
		return codigo;
	}

	@DisplayName("EX")
	@MaxLength(10)
	public String getExtipi() {
		return extipi;
	}

	@Required
	public Tabelaimpostoitemtipo getTipo() {
		return tipo;
	}

	@Required
	@DisplayName("Descri��o")
	@MaxLength(1000)
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Nacional federal")
	public Double getNacionalfederal() {
		return nacionalfederal;
	}

	@DisplayName("Importado federal")
	public Double getImportadofederal() {
		return importadofederal;
	}

	public Double getEstadual() {
		return estadual;
	}

	public Double getMunicipal() {
		return municipal;
	}

	public void setCdtabelaimpostoitem(Integer cdtabelaimpostoitem) {
		this.cdtabelaimpostoitem = cdtabelaimpostoitem;
	}

	public void setTabelaimposto(Tabelaimposto tabelaimposto) {
		this.tabelaimposto = tabelaimposto;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setExtipi(String extipi) {
		this.extipi = extipi;
	}

	public void setTipo(Tabelaimpostoitemtipo tipo) {
		this.tipo = tipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setNacionalfederal(Double nacionalfederal) {
		this.nacionalfederal = nacionalfederal;
	}

	public void setImportadofederal(Double importadofederal) {
		this.importadofederal = importadofederal;
	}

	public void setEstadual(Double estadual) {
		this.estadual = estadual;
	}

	public void setMunicipal(Double municipal) {
		this.municipal = municipal;
	}

}
