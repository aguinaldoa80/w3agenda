package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_escalacolaborador", sequenceName = "sq_escalacolaborador")
public class Escalacolaborador {

	protected Integer cdescalacolaborador;
	protected Colaborador colaborador;
	protected Escala escala;
	protected Date dtinicio;
	protected Date dtfim;
	protected Boolean exclusivo;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_escalacolaborador")
	public Integer getCdescalacolaborador() {
		return cdescalacolaborador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescala")
	public Escala getEscala() {
		return escala;
	}

	@DisplayName("Data in�cio")
	@Required
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	public Boolean getExclusivo() {
		return exclusivo;
	}

	public void setExclusivo(Boolean exclusivo) {
		this.exclusivo = exclusivo;
	}
	
	public void setCdescalacolaborador(Integer cdescalacolaborador) {
		this.cdescalacolaborador = cdescalacolaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setEscala(Escala escala) {
		this.escala = escala;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	
}
