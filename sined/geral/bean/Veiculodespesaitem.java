package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_veiculodespesaitem", sequenceName = "sq_veiculodespesaitem")
public class Veiculodespesaitem implements Log{

	protected Integer cdveiculodespesaitem;
	protected Veiculodespesa veiculodespesa;
	protected String nome;
	protected Material material;
	protected Float quantidade;
	protected Double valorunitario;
	protected Projeto projeto;
	protected Boolean consumoproprio = Boolean.FALSE;
	protected Localarmazenagem localarmazenagem;
	protected Centrocusto centrocusto;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;

	//Transients
	protected Money valortotal;
	protected Boolean materialoutro;
	protected String qtdedisponivel;
	protected Empresa empresa;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculodespesaitem")
	public Integer getCdveiculodespesaitem() {
		return cdveiculodespesaitem;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Required
	@DisplayName("Despesa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculodespesa")
	public Veiculodespesa getVeiculodespesa() {
		return veiculodespesa;
	}
	@MaxLength(2000)
	public String getNome() {
		return nome;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@MaxLength(9)
	@DisplayName("Qtde.")
	public Float getQuantidade() {
		return quantidade;
	}
	@DisplayName("Valor unit.")
	public Double getValorunitario() {
		return valorunitario;
	}
	@DisplayName("Projeto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Tipo")
	@Transient
	public Boolean getMaterialoutro() {
		return materialoutro;
	}
	
	public void setVeiculodespesa(Veiculodespesa veiculodespesa) {
		this.veiculodespesa = veiculodespesa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQuantidade(Float quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	public void setCdveiculodespesaitem(Integer cdveiculodespesaitem) {
		this.cdveiculodespesaitem = cdveiculodespesaitem;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setMaterialoutro(Boolean materialoutro) {
		this.materialoutro = materialoutro;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Transient
	@DisplayName("Valor total")
	public Money getValortotal() {
		return valortotal;
	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	
	public Money calculaValorTotal(){
		if (valorunitario != null && quantidade != null) {
			valortotal = new Money (valorunitario * quantidade);
		}
		return valortotal != null ? valortotal : new Money(0);
	}
	
	@DisplayName("Consumo pr�prio?")
	public Boolean getConsumoproprio() {
		return consumoproprio;
	}
	@DisplayName("Local de armazenagem")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	
	public void setConsumoproprio(Boolean consumoproprio) {
		this.consumoproprio = consumoproprio;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@Transient
	@DisplayName("Qtde. dispon�vel")
	public String getQtdedisponivel() {
		return qtdedisponivel;
	}
	
	public void setQtdedisponivel(String qtdedisponivel) {
		this.qtdedisponivel = qtdedisponivel;
	}
	
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}