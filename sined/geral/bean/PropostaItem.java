package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_propostaitem",sequenceName="sq_propostaitem")
public class PropostaItem {
	
	protected Integer cdpropostaitem;
	protected Proposta proposta;
	protected PropostaCaixaItem propostacaixaitem;
	protected String valor;
	
	@Id
	@GeneratedValue(generator="sq_propostaitem",strategy=GenerationType.AUTO)
	public Integer getCdpropostaitem() {
		return cdpropostaitem;
	}
	public void setCdpropostaitem(Integer cdpropostaitem) {
		this.cdpropostaitem = cdpropostaitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproposta")
	public Proposta getProposta() {
		return proposta;
	}
	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpropostacaixaitem")
	public PropostaCaixaItem getPropostacaixaitem() {
		return propostacaixaitem;
	}
	public void setPropostacaixaitem(PropostaCaixaItem propostacaixaitem) {
		this.propostacaixaitem = propostacaixaitem;
	}
	
	@MaxLength(500)
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}
