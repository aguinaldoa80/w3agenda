package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_notafiscalprodutoitemdi",sequenceName="sq_notafiscalprodutoitemdi")
public class Notafiscalprodutoitemdi {
	
	protected Integer cdnotafiscalprodutoitemdi;
	protected Notafiscalprodutoitem notafiscalprodutoitem;
	protected Declaracaoimportacao declaracaoimportacao;
	
	@Id
	@GeneratedValue(generator="sq_notafiscalprodutoitemdi", strategy=GenerationType.AUTO)
	public Integer getCdnotafiscalprodutoitemdi() {
		return cdnotafiscalprodutoitemdi;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdnotafiscalprodutoitem")
	public Notafiscalprodutoitem getNotafiscalprodutoitem() {
		return notafiscalprodutoitem;
	}
	
	@Required
	@DisplayName("Declaração de importação")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cddeclaracaoimportacao")
	public Declaracaoimportacao getDeclaracaoimportacao() {
		return declaracaoimportacao;
	}
	
	public void setDeclaracaoimportacao(
			Declaracaoimportacao declaracaoimportacao) {
		this.declaracaoimportacao = declaracaoimportacao;
	}

	public void setCdnotafiscalprodutoitemdi(Integer cdnotafiscalprodutoitemdi) {
		this.cdnotafiscalprodutoitemdi = cdnotafiscalprodutoitemdi;
	}

	public void setNotafiscalprodutoitem(
			Notafiscalprodutoitem notafiscalprodutoitem) {
		this.notafiscalprodutoitem = notafiscalprodutoitem;
	}
		
}