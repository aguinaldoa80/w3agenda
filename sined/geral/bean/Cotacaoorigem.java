package br.com.linkcom.sined.geral.bean;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_cotacaoorigem", sequenceName = "sq_cotacaoorigem")
public class Cotacaoorigem implements Log{

	protected Integer cdcotacaoorigem;
	protected Cotacao cotacao;
	protected Solicitacaocompra solicitacaocompra;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cotacaoorigem")
	public Integer getCdcotacaoorigem() {
		return cdcotacaoorigem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcotacao")
	public Cotacao getCotacao() {
		return cotacao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}


	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}


	public Timestamp getDtaltera() {
		return dtaltera;
	}


	public void setCdcotacaoorigem(Integer cdcotacaoorigem) {
		this.cdcotacaoorigem = cdcotacaoorigem;
	}


	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}


	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}


	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}


	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
