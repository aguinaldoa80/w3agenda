package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivobancariodocumento", sequenceName = "sq_arquivobancariodocumento")
public class Arquivobancariodocumento implements Log{
	
	protected Integer cdarquivobancariodocumento;
	protected Documento documento;
	protected Arquivobancario arquivobancario;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Arquivobancariodocumento(){
	}
	
	public Arquivobancariodocumento(Arquivobancario arquivobancario, Documento documento){
		this.arquivobancario = arquivobancario;
		this.documento = documento;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivobancariodocumento")
	public Integer getCdarquivobancariodocumento() {
		return cdarquivobancariodocumento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivobancario")
	public Arquivobancario getArquivobancario() {
		return arquivobancario;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdarquivobancariodocumento(Integer cdarquivobancariodocumento) {
		this.cdarquivobancariodocumento = cdarquivobancariodocumento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setArquivobancario(Arquivobancario arquivobancario) {
		this.arquivobancario = arquivobancario;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
