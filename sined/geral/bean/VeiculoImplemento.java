package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_veiculoimplemento", sequenceName = "sq_veiculoimplemento")
public class VeiculoImplemento  {

	protected Integer cdveiculoimplemento;
	protected Veiculouso veiculouso;
	protected Veiculo implemento;
	protected Double horimetroinicio;
	protected Double horimetrofim;
	protected Integer kminicio;
	protected Integer kmfim;
	
	// Transients
	protected String kmTotal;
	protected String tempoTotal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_veiculoimplemento")
	public Integer getCdveiculoimplemento() {
		return cdveiculoimplemento;
	}
	public void setCdveiculoimplemento(Integer cdveiculoimplemento) {
		this.cdveiculoimplemento = cdveiculoimplemento;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdveiculouso")
	public Veiculouso getVeiculouso() {
		return veiculouso;
	}
	public void setVeiculouso(Veiculouso veiculouso) {
		this.veiculouso = veiculouso;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="implemento")
	public Veiculo getImplemento() {
		return implemento;
	}
	public void setImplemento(Veiculo implemento) {
		this.implemento = implemento;
	}
	
	@DisplayName("Hor�metro In�cio")
	public Double getHorimetroinicio() {
		return horimetroinicio;
	}
	public void setHorimetroinicio(Double horimetroinicio) {
		this.horimetroinicio = horimetroinicio;
	}
	
	@DisplayName("Hor�metro Fim")
	public Double getHorimetrofim() {
		return horimetrofim;
	}
	public void setHorimetrofim(Double horimetrofim) {
		this.horimetrofim = horimetrofim;
	}
	
	@DisplayName("Km In�cio")
	public Integer getKminicio() {
		return kminicio;
	}
	public void setKminicio(Integer kminicio) {
		this.kminicio = kminicio;
	}
	
	@DisplayName("Km Fim")
	public Integer getKmfim() {
		return kmfim;
	}
	public void setKmfim(Integer kmfim) {
		this.kmfim = kmfim;
	}
	
	@Transient
	@DisplayName("Tempo total(h)")
	public String getTempoTotal(){
		if(this.horimetroinicio != null && this.horimetrofim != null){
			return (this.horimetrofim - this.horimetroinicio) + "";
		}
		return "";
	}
	
	public void setTempoTotal(String tempoTotal) {
		this.tempoTotal = tempoTotal;
	}
	
	@Transient
	@DisplayName("Km Total")
	public String getKmTotal(){
		if(this.kminicio != null && this.kmfim != null)
			return this.kmfim - this.kminicio+"";
		return "";
	}
	public void setKmTotal(String kmTotal) {
		this.kmTotal = kmTotal;
	}
}