package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.types.Money;

@Entity
@SequenceGenerator(name="sq_historicoantecipacao",sequenceName="sq_historicoantecipacao")
public class HistoricoAntecipacao{

	protected Integer cdHistoricoAntecipacao;
	protected Documento documento;				// origem
	protected Documento documentoReferencia;	// destino
	protected String descricao;
	protected Date dataHora = new Date();
	protected Money valor;
	protected Boolean compensacao;
	private Date dtBaixa;

	@Id
	@GeneratedValue(generator="sq_historicoantecipacao",strategy=GenerationType.AUTO)
	public Integer getCdHistoricoAntecipacao() {
		return cdHistoricoAntecipacao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoreferencia")
	public Documento getDocumentoReferencia() {
		return documentoReferencia;
	}

	public String getDescricao() {
		return descricao;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public Money getValor() {
		return valor;
	}

	public Boolean getCompensacao() {
		return compensacao;
	}
	
	public Date getDtBaixa() {
		return dtBaixa;
	}
	
	public void setDtBaixa(Date dtBaixa) {
		this.dtBaixa = dtBaixa;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setDocumentoReferencia(Documento documentoReferencia) {
		this.documentoReferencia = documentoReferencia;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setCdHistoricoAntecipacao(Integer cdHistoricoAntecipacao) {
		this.cdHistoricoAntecipacao = cdHistoricoAntecipacao;
	}
	
	public void setCompensacao(Boolean compensacao) {
		this.compensacao = compensacao;
	}
}