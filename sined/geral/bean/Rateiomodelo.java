package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_rateiomodelo",sequenceName="sq_rateiomodelo")
public class Rateiomodelo implements Log{

	private Integer cdrateiomodelo;
	private String nome;
	private List<Rateiomodeloitem> listaRateiomodeloitem;
	private Tipooperacao tipooperacao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,generator="sq_rateiomodelo")
	public Integer getCdrateiomodelo() {
		return cdrateiomodelo;
	}
	public void setCdrateiomodelo(Integer cdrateiomodelo) {
		this.cdrateiomodelo = cdrateiomodelo;
	}
	
	@Required
	@DisplayName("Nome")
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@DisplayName("Rateio")
	@OneToMany(mappedBy="rateiomodelo")
	public List<Rateiomodeloitem> getListaRateiomodeloitem() {
		return listaRateiomodeloitem;
	}
	public void setListaRateiomodeloitem(
			List<Rateiomodeloitem> listaRateiomodeloitem) {
		this.listaRateiomodeloitem = listaRateiomodeloitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Opera��o")
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
