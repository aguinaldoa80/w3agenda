package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_grupotributacaomaterialtipo", sequenceName="sq_grupotributacaomaterialtipo")
public class GrupotributacaoMaterialTipo {
	protected Integer cdGrupotributacaoMaterialTipo;
	protected Materialtipo materialTipo;
	protected Grupotributacao grupotributacao;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_grupotributacaomaterialtipo")
	public Integer getCdGrupotributacaoMaterialTipo() {
		return cdGrupotributacaoMaterialTipo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmaterialtipo")
	public Materialtipo getMaterialTipo() {
		return materialTipo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	public void setCdGrupotributacaoMaterialTipo(Integer cdGrupotributacaoMaterialTipo) {
		this.cdGrupotributacaoMaterialTipo = cdGrupotributacaoMaterialTipo;
	}
	
	public void setMaterialTipo(Materialtipo materialTipo) {
		this.materialTipo = materialTipo;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
}
