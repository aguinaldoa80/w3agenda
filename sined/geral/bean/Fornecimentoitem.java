package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_fornecimentoitem", sequenceName = "sq_fornecimentoitem")
public class Fornecimentoitem implements Log{
	
	protected Integer cdfornecimentoitem;
	protected Fornecimento fornecimento;
	protected String descricao;
	protected Double qtdeprevista;
	protected Double qtderealizada;
	
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	public Fornecimentoitem(){
	}
	
	public Fornecimentoitem(String descricao, Double qtdeprevista){
		this.descricao = descricao;
		this.qtdeprevista = qtdeprevista;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fornecimentoitem")
	public Integer getCdfornecimentoitem() {
		return cdfornecimentoitem;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimento")
	public Fornecimento getFornecimento() {
		return fornecimento;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(150)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Qtde. prevista")
	@MaxLength(10)
	public Double getQtdeprevista() {
		return qtdeprevista;
	}
	@DisplayName("Qtde. realizada")
	@MaxLength(10)
	public Double getQtderealizada() {
		return qtderealizada;
	}
	
	public void setFornecimento(Fornecimento fornecimento) {
		this.fornecimento = fornecimento;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setQtdeprevista(Double qtdeprevista) {
		this.qtdeprevista = qtdeprevista;
	}
	public void setQtderealizada(Double qtderealizada) {
		this.qtderealizada = qtderealizada;
	}
	public void setCdfornecimentoitem(Integer cdfornecimentoitem) {
		this.cdfornecimentoitem = cdfornecimentoitem;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
}
