package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_arquivoretornocontrato", sequenceName = "sq_arquivoretornocontrato")
public class Arquivoretornocontrato implements Log{
	
	protected Integer cdarquivoretornocontrato;
	protected String numeroremessa;
	protected Arquivo arquivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivoretornocontrato")
	public Integer getCdarquivoretornocontrato() {
		return cdarquivoretornocontrato;
	}
	public String getNumeroremessa() {
		return numeroremessa;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setCdarquivoretornocontrato(Integer cdarquivoretornocontrato) {
		this.cdarquivoretornocontrato = cdarquivoretornocontrato;
	}
	public void setNumeroremessa(String numeroremessa) {
		this.numeroremessa = numeroremessa;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
