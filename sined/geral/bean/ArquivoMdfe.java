package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.ArquivoMdfeSituacao;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_arquivomdfe", sequenceName = "sq_arquivomdfe")
public class ArquivoMdfe implements Log {

	protected Integer cdArquivoMdfe;
	protected Date dtenvio;
	protected Empresa empresa;
	protected Configuracaonfe configuracaonfe;
	protected ArquivoMdfeSituacao arquivoMdfeSituacao;
	protected String numerorecibo;
	protected Timestamp dtrecebimento;
	protected Mdfe mdfe;
	protected Timestamp dtprocessamento;
	protected String protocolomdfe;
	protected String chaveacesso;
	protected Timestamp dtcancelamento;
	protected Timestamp dtencerramento;
	protected Timestamp dtInclusaoCondutor;
	
	// FLAGS
	protected Boolean assinando;
	protected Boolean enviando;
	protected Boolean consultando;
	
	protected Boolean emitindo;
	
	// ARQUIVOS DE ENVIO
	protected Arquivo arquivoxml;
	protected Arquivo arquivoxmlassinado;
	protected Arquivo arquivoxmlconsultasituacao;
	protected Arquivo arquivoxmlconsultalote;
	
	// ARQUIVOS DE RETORNO
	protected Arquivo arquivoretornoenvio;
	protected Arquivo arquivoretornoconsulta;
	protected Arquivo arquivoretornoconsultasituacao;
	
	protected Boolean encerrando;
	protected Arquivo arquivoxmlencerramento;
	protected Boolean encerramentoporevento;
	protected Arquivo arquivoxmlretornoencerramento;
	
	protected Boolean cancelando;
	protected Arquivo arquivoXmlCancelamento;
	protected Boolean cancelamentoPorEvento;
	protected Arquivo arquivoXmlRetornoCancelamento;
	
	protected Boolean incluindoCondutor;
	protected Arquivo arquivoXmlInclusaoCondutor;
	protected Boolean inclusaoCondutorPorEvento;
	protected Arquivo arquivoXmlRetornoInclusaoCondutor;
	
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<ArquivoMdfeErro> listaArquivoMdfeErro;
	
	// TRANSIENTES
	protected String flag;
	protected Arquivo arquivoatualizacao;
	protected String protocoloatualizacao;
	protected Arquivo arquivoatualizacaolote;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivomdfe")
	public Integer getCdArquivoMdfe() {
		return cdArquivoMdfe;
	}
	
	@DisplayName("XML")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}

	@DisplayName("XML assinado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlassinado")
	public Arquivo getArquivoxmlassinado() {
		return arquivoxmlassinado;
	}

	@DisplayName("Data de envio")
	public Date getDtenvio() {
		return dtenvio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaonfe")
	public Configuracaonfe getConfiguracaonfe() {
		return configuracaonfe;
	}

	@DisplayName("Retorno do envio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoenvio")
	public Arquivo getArquivoretornoenvio() {
		return arquivoretornoenvio;
	}

	@DisplayName("Retorno da consulta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoconsulta")
	public Arquivo getArquivoretornoconsulta() {
		return arquivoretornoconsulta;
	}
	
	@DisplayName("Retorno da consulta de situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornoconsultasituacao")
	public Arquivo getArquivoretornoconsultasituacao() {
		return arquivoretornoconsultasituacao;
	}
	
	public Boolean getEncerrando() {
		return encerrando;
	}

	@DisplayName("Xml Encerramento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlencerramento")
	public Arquivo getArquivoxmlencerramento() {
		return arquivoxmlencerramento;
	}
	
	public Boolean getEncerramentoporevento() {
		return encerramentoporevento;
	}
	
	@DisplayName("Xml Retorno Encerramento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlretornoencerramento")
	public Arquivo getArquivoxmlretornoencerramento() {
		return arquivoxmlretornoencerramento;
	}

	@DisplayName("Situa��o")
	public ArquivoMdfeSituacao getArquivoMdfeSituacao() {
		return arquivoMdfeSituacao;
	}
	
	public Boolean getAssinando() {
		return assinando;
	}

	public Boolean getEnviando() {
		return enviando;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("XML de consulta de situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlconsultasituacao")
	public Arquivo getArquivoxmlconsultasituacao() {
		return arquivoxmlconsultasituacao;
	}

	@DisplayName("XML de consulta do lote")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlconsultalote")
	public Arquivo getArquivoxmlconsultalote() {
		return arquivoxmlconsultalote;
	}
	
	public Boolean getConsultando() {
		return consultando;
	}
	
	@DisplayName("Data de recebimento")
	public Timestamp getDtrecebimento() {
		return dtrecebimento;
	}
	
	@DisplayName("N�mero Recibo")
	public String getNumerorecibo() {
		return numerorecibo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}

	public Boolean getEmitindo() {
		return emitindo;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="arquivoMdfe")
	public List<ArquivoMdfeErro> getListaArquivoMdfeErro() {
		return listaArquivoMdfeErro;
	}

	@DisplayName("Data de processamento")
	public Timestamp getDtprocessamento() {
		return dtprocessamento;
	}
	
	@DisplayName("Protocolo")
	public String getProtocolomdfe() {
		return protocolomdfe;
	}
	
	public void setProtocolomdfe(String protocolomdfe) {
		this.protocolomdfe = protocolomdfe;
	}
	
	@DisplayName("Chave de acesso")
	public String getChaveacesso() {
		return chaveacesso;
	}

	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	
	@DisplayName("Data de Cancelamento")
	public Timestamp getDtcancelamento() {
		return dtcancelamento;
	}

	public void setDtcancelamento(Timestamp dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	
	@DisplayName("Data de Encerramento")
	public Timestamp getDtencerramento() {
		return dtencerramento;
	}

	public void setDtencerramento(Timestamp dtencerramento) {
		this.dtencerramento = dtencerramento;
	}
	
	public Timestamp getDtInclusaoCondutor() {
		return dtInclusaoCondutor;
	}
	
	public void setDtInclusaoCondutor(Timestamp dtInclusaoCondutor) {
		this.dtInclusaoCondutor = dtInclusaoCondutor;
	}

	public void setDtprocessamento(Timestamp dtprocessamento) {
		this.dtprocessamento = dtprocessamento;
	}
	
	public void setListaArquivoMdfeErro(List<ArquivoMdfeErro> listaArquivoMdfeErro) {
		this.listaArquivoMdfeErro = listaArquivoMdfeErro;
	}

	public void setEmitindo(Boolean emitindo) {
		this.emitindo = emitindo;
	}

	public void setNumerorecibo(String numerorecibo) {
		this.numerorecibo = numerorecibo;
	}
	
	public void setDtrecebimento(Timestamp dtrecebimento) {
		this.dtrecebimento = dtrecebimento;
	}
	
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	public void setArquivoretornoconsultasituacao(
			Arquivo arquivoretornoconsultasituacao) {
		this.arquivoretornoconsultasituacao = arquivoretornoconsultasituacao;
	}
	
	public void setEncerrando(Boolean encerrando) {
		this.encerrando = encerrando;
	}
	
	public void setArquivoxmlencerramento(Arquivo arquivoxmlencerramento) {
		this.arquivoxmlencerramento = arquivoxmlencerramento;
	}
	
	public void setEncerramentoporevento(Boolean encerramentoporevento) {
		this.encerramentoporevento = encerramentoporevento;
	}
	
	public void setArquivoxmlretornoencerramento(Arquivo arquivoxmlretornoencerramento) {
		this.arquivoxmlretornoencerramento = arquivoxmlretornoencerramento;
	}

	public void setConsultando(Boolean consultando) {
		this.consultando = consultando;
	}

	public void setArquivoxmlconsultasituacao(Arquivo arquivoxmlconsultasituacao) {
		this.arquivoxmlconsultasituacao = arquivoxmlconsultasituacao;
	}

	public void setArquivoxmlconsultalote(Arquivo arquivoxmlconsultalote) {
		this.arquivoxmlconsultalote = arquivoxmlconsultalote;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setEnviando(Boolean enviando) {
		this.enviando = enviando;
	}
	
	public void setAssinando(Boolean assinando) {
		this.assinando = assinando;
	}
	
	public void setArquivoMdfeSituacao(ArquivoMdfeSituacao arquivoMdfeSituacao) {
		this.arquivoMdfeSituacao = arquivoMdfeSituacao;
	}

	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}

	public void setArquivoxmlassinado(Arquivo arquivoxmlassinado) {
		this.arquivoxmlassinado = arquivoxmlassinado;
	}

	public void setDtenvio(Date dtenvio) {
		this.dtenvio = dtenvio;
	}

	public void setConfiguracaonfe(Configuracaonfe configuracaonfe) {
		this.configuracaonfe = configuracaonfe;
	}

	public void setArquivoretornoenvio(Arquivo arquivoretornoenvio) {
		this.arquivoretornoenvio = arquivoretornoenvio;
	}

	public void setArquivoretornoconsulta(Arquivo arquivoretornoconsulta) {
		this.arquivoretornoconsulta = arquivoretornoconsulta;
	}

	public void setCdArquivoMdfe(Integer cdArquivoMdfe) {
		this.cdArquivoMdfe = cdArquivoMdfe;
	}
	
	public Boolean getCancelando() {
		return cancelando;
	}

	public void setCancelando(Boolean cancelando) {
		this.cancelando = cancelando;
	}

	@DisplayName("XML do Cancelamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlcancelamento")
	public Arquivo getArquivoXmlCancelamento() {
		return arquivoXmlCancelamento;
	}

	public void setArquivoXmlCancelamento(Arquivo arquivoXmlCancelamento) {
		this.arquivoXmlCancelamento = arquivoXmlCancelamento;
	}

	public Boolean getCancelamentoPorEvento() {
		return cancelamentoPorEvento;
	}

	public void setCancelamentoPorEvento(Boolean cancelamentoPorEvento) {
		this.cancelamentoPorEvento = cancelamentoPorEvento;
	}

	@DisplayName("XML do Retorno do Cancelamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlretornocancelamento")
	public Arquivo getArquivoXmlRetornoCancelamento() {
		return arquivoXmlRetornoCancelamento;
	}

	public void setArquivoXmlRetornoCancelamento(Arquivo arquivoXmlRetornoCancelamento) {
		this.arquivoXmlRetornoCancelamento = arquivoXmlRetornoCancelamento;
	}
	
	public Boolean getIncluindoCondutor() {
		return incluindoCondutor;
	}

	public void setIncluindoCondutor(Boolean incluindoCondutor) {
		this.incluindoCondutor = incluindoCondutor;
	}

	@DisplayName("XML de Inclus�o de Condutor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlretornoinclusaocondutor")
	public Arquivo getArquivoXmlInclusaoCondutor() {
		return arquivoXmlInclusaoCondutor;
	}

	public void setArquivoXmlInclusaoCondutor(Arquivo arquivoXmlInclusaoCondutor) {
		this.arquivoXmlInclusaoCondutor = arquivoXmlInclusaoCondutor;
	}

	public Boolean getInclusaoCondutorPorEvento() {
		return inclusaoCondutorPorEvento;
	}

	public void setInclusaoCondutorPorEvento(Boolean inclusaoCondutorPorEvento) {
		this.inclusaoCondutorPorEvento = inclusaoCondutorPorEvento;
	}

	@DisplayName("XML de Retorno de Inclus�o de Condutor")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlinclusaocondutor")
	public Arquivo getArquivoXmlRetornoInclusaoCondutor() {
		return arquivoXmlRetornoInclusaoCondutor;
	}

	public void setArquivoXmlRetornoInclusaoCondutor(
			Arquivo arquivoXmlRetornoInclusaoCondutor) {
		this.arquivoXmlRetornoInclusaoCondutor = arquivoXmlRetornoInclusaoCondutor;
	}

//	LOG	
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
//	TRANSIENTES
	
	@Transient
	public String getFlag() {
		return flag;
	}
	
	@Transient
	public Arquivo getArquivoatualizacao() {
		return arquivoatualizacao;
	}
	
	@Transient
	public Arquivo getArquivoatualizacaolote() {
		return arquivoatualizacaolote;
	}

	@Transient
	public String getProtocoloatualizacao() {
		return protocoloatualizacao;
	}
	
	public void setProtocoloatualizacao(String protocoloatualizacao) {
		this.protocoloatualizacao = protocoloatualizacao;
	}
	
	public void setArquivoatualizacao(Arquivo arquivoatualizacao) {
		this.arquivoatualizacao = arquivoatualizacao;
	}
	
	public void setArquivoatualizacaolote(Arquivo arquivoatualizacaolote) {
		this.arquivoatualizacaolote = arquivoatualizacaolote;
	}
	
	public void setFlag(String flag) {
		this.flag = flag;
	}
}
