package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "usuariosincronizacaodashboard")
@SequenceGenerator(name = "sq_usuariosincronizacaodashboard", sequenceName = "sq_usuariosincronizacaodashboard")
public class UsuarioSincronizacaoDashboard {

	private Integer idusuario;
	private String cdpessoa;
	private String token;
	private Date dtSincronizacao;
	private Integer codigoErro;
	private String mensagemErro;
	private Boolean criar;
	private Boolean deletar;
	
	@Id
	public Integer getIdusuario() {
		return idusuario;
	}
	public String getCdpessoa() {
		return cdpessoa;
	}
	public String getToken() {
		return token;
	}
	public Date getDtSincronizacao() {
		return dtSincronizacao;
	}
	public Integer getCodigoErro() {
		return codigoErro;
	}
	public String getMensagemErro() {
		return mensagemErro;
	}
	public Boolean getCriar() {
		return criar;
	}
	public Boolean getDeletar() {
		return deletar;
	}
	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}
	public void setCdpessoa(String cdpessoa) {
		this.cdpessoa = cdpessoa;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setDtSincronizacao(Date dtSincronizacao) {
		this.dtSincronizacao = dtSincronizacao;
	}
	public void setCodigoErro(Integer codigoErro) {
		this.codigoErro = codigoErro;
	}
	public void setMensagemErro(String mensagemErro) {
		this.mensagemErro = mensagemErro;
	}
	public void setCriar(Boolean criar) {
		this.criar = criar;
	}
	public void setDeletar(Boolean deletar) {
		this.deletar = deletar;
	}

}
