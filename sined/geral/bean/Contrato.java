package br.com.linkcom.sined.geral.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.NeoFormater;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_contrato;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.bean.enumeration.Vendedortipo;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FatorajusteService;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratoMaterialRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratoParcelaRTF;
import br.com.linkcom.sined.geral.service.rtf.bean.ContratoRTF;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

@Entity
@SequenceGenerator(name = "sq_contrato", sequenceName = "sq_contrato")
@JoinEmpresa("contrato.empresa")
public class Contrato implements Log, PermissaoProjeto {
	
	protected Integer cdcontrato;
	protected String identificador;
	protected Contratotipo contratotipo;
	protected Documentotipo documentotipo;
	protected Cliente cliente;
	protected Contato contato;
	protected String inscricaoestadual;
	protected Endereco endereco;
	protected Endereco enderecoentrega;
	protected Endereco enderecocobranca;
	protected String descricao;
	protected Date dtinicio = SinedDateUtils.currentDate();
	protected Date dtassinatura;
	protected Date dtfim;
	protected Empresa empresa;
	protected Conta conta;
	protected Contacarteira contacarteira;
	protected Vendedortipo vendedortipo;
	protected Pessoa vendedor;
	protected Fornecedor fornecedor;
	protected Colaborador colaborador;
	protected Colaborador responsavel;
	protected String historico;
	protected String anotacao;
	protected Date dtproximovencimento;
	protected Frequencia frequencia;
	protected Frequencia frequenciarenovacao;
	protected Prazopagamento prazopagamento;
	protected Boolean dataparceladiautil;
	protected Boolean dataparcelaultimodiautilmes;
	protected Mes mes;
	protected Integer ano;
	protected Money valor;
	protected Money valorhoraextra;
	protected Money valoracrescimo;
	protected Boolean acrescimoproximofaturamento;
	protected Indicecorrecao indicecorrecao;
	protected String nomefantasia;
	protected Date dtconfirma;
	protected Date dtcancelamento;
	protected Date dtconclusao;
	protected Comissionamento comissionamento;
	protected Date dtsuspensao;
	protected Double qtde = 1.0;
	protected Fatorajuste fatorajuste;
	protected Money valordedutivocomissionamento;
	protected String urlsistema;
	protected Date dtrenovacao;
	protected Formafaturamento formafaturamento = Formafaturamento.NOTA_ANTES_RECEBIMENTO;
	protected Boolean isento;
	protected Boolean ignoravalormaterial;
	protected Money valortotalfechado;
	protected Double aliquotavalortotalfechado;
	protected Date dtiniciolocacao;
	protected Date dtfimlocacao;
	
	protected Codigocnae codigocnaeBean;
	protected Itemlistaservico itemlistaservicoBean;
	protected Codigotributacao codigotributacao;
	protected ContaContabil contaContabil;
	protected Grupotributacao grupotributacao;
	protected Vendaorcamento vendaorcamento;
	
	protected Aux_contrato aux_contrato;
	
	protected Boolean finalizarcontratofinalizarpagamentos;
	
	//ABAS
	protected Rateio rateio;
	protected Taxa taxa;
	
	//NOTA FISCAL
	protected String anotacaocorpo;
	protected Boolean faturamentolote = false;
	
	//TRIBUTOS
	protected Double ir;
	protected Boolean incideir = false;
	protected Double iss;
	protected Boolean incideiss = false;
	protected Double pis;
	protected Boolean incidepis = false;
	protected Double cofins;
	protected Boolean incidecofins = false;
	protected Double csll;
	protected Boolean incidecsll = false;
	protected Double inss;
	protected Boolean incideinss = false;
	protected Double icms;
	protected Boolean incideicms = false;
	protected Integer codigogps;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	//LISTAS
	protected List<Contratocolaborador> listaContratocolaborador = new ListSet<Contratocolaborador>(Contratocolaborador.class);
	protected List<Contratohistorico> listaContratohistorico = new ListSet<Contratohistorico>(Contratohistorico.class);
	protected List<Contratomaterial> listaContratomaterial = new ListSet<Contratomaterial>(Contratomaterial.class);
	protected List<NotaContrato> listaNotacontrato;
	protected List<Contratoparcela> listaContratoparcela = new ListSet<Contratoparcela>(Contratoparcela.class);
	protected List<Contratodesconto> listaContratodesconto = new ListSet<Contratodesconto>(Contratodesconto.class);
	protected List<Contratoarquivo> listaContratoarquivo = new ListSet<Contratoarquivo>(Contratoarquivo.class);
	protected List<Contratoservico> listaContratoservico = new ListSet<Contratoservico>(Contratoservico.class);
	protected List<Documentoorigem> listaDocumentoOrigem = new ListSet<Documentoorigem>(Documentoorigem.class);
	protected List<Romaneioorigem> listaRomaneioorigem = new ListSet<Romaneioorigem>(Romaneioorigem.class);
	protected List<Romaneioorigem> listaRomaneioorigemFechamento = new ListSet<Romaneioorigem>(Romaneioorigem.class);
	
	//TRANSIENTE
	protected Money valorIr;
	protected Money valorInss;
	protected Money valorIss;
	protected Money valorPis;
	protected Money valorCofins;
	protected Money valorCsll;
	protected Money valorIcms;
	protected Money valorContrato;
	protected Money valorContratoListagem;
	protected Boolean cumulativoCobrado;
	protected Proposta proposta;
	protected Material materialpromocional;
	protected Oportunidade oportunidade;
	protected Integer parcelas;
	protected Faturamentocontrato faturamentocontrato;
	protected Boolean confirmar = Boolean.FALSE;	
	protected Date dtVencimentoAtualiza;
	protected Money valorAtualiza;
	protected Colaborador responsavelAtualiza;
	protected String whereIn;
	protected Boolean entrada;
	protected Money valorContratoAFaturarPorMedicao;
	protected Money valorContratoFaturado;	
	protected String inscricaoMunicipal;
	protected Double percentualValorAtualiza;
	protected Date dtrecalculo1;
	protected Date dtrecalculo2;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected Boolean viaWebService = Boolean.FALSE;
	protected Documento documento;
	protected Boolean atualizarparcelasindiceatual;
	protected Money valortotalcheio;
	protected Boolean identificadorautomatico = Boolean.TRUE;
	protected Boolean finalizarcontrato;
	protected Boolean indenizacao;
	protected String notaremessalocacao;
	protected String dtemissaonotaremessalocacao;
	protected Double saldoromaneio;
	protected String nomeCliente;
	protected Integer mesesFinalContrato;
	protected Integer qtdePromocional = 1;
	
	protected Money faturalocacaoTotallocado;
	protected Money faturalocacaoTotaldevolvido;
	protected Money faturalocacaoTotaldesconto;
	protected Money faturalocacaoTotalacrescimo;
	protected Money faturalocacaoValorfaturar;
	
	protected Contatipo contatipo;
	protected Boolean reajuste;
	protected Double percentualreajuste;
	private Endereco enderecoPrioritario;
	
	public Contrato() {
	}

	public Contrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}

	public Contrato(Integer cdcontrato, String descricao) {
		this.cdcontrato = cdcontrato;
		this.descricao = descricao;
	}
	
	public Contrato(Contrato ct) {
		this.cdcontrato = ct.cdcontrato;
		this.dtproximovencimento = ct.dtproximovencimento;
		this.valor = ct.valor;
		this.cliente = ct.cliente;
		this.descricao = ct.descricao;
		this.ir = ct.ir;
		this.incideir = ct.incideir;
		this.iss = ct.iss;
		this.incideiss = ct.incideiss;
		this.pis = ct.pis;
		this.incidepis = ct.incidepis;
		this.cofins = ct.cofins;
		this.incidecofins = ct.incidecofins;
		this.csll = ct.csll;
		this.incidecsll = ct.incidecsll;
		this.inss = ct.inss;
		this.incideinss = ct.incideinss;
		this.icms = ct.icms;
		this.incideicms = ct.incideicms;
		this.rateio = ct.rateio;
		this.listaContratodesconto = ct.listaContratodesconto;
		this.conta = ct.conta;
		this.qtde = ct.qtde;
		this.qtdePromocional = ct.qtdePromocional;
	}

	public Contrato(
			Integer contrato_cdcontrato, 
			String contrato_descricao,
			java.util.Date contrato_dtproximovencimento, 
			java.util.Date contrato_dtinicio,
			java.util.Date contrato_dtfim,
			String cliente_nome,
			String empresa_nome,
			String empresa_nomefantasia,
			SituacaoContrato aux_contrato_situacao,
			String contrato_identificador,
			Long aux_contrato_identificadornumerico
	){
		cdcontrato = contrato_cdcontrato;
		descricao = contrato_descricao;
		dtproximovencimento = contrato_dtproximovencimento != null ? new Date(contrato_dtproximovencimento.getTime()) : null;
		dtinicio = contrato_dtinicio != null ? new Date(contrato_dtinicio.getTime()) : null;
		dtfim = contrato_dtfim != null ? new Date(contrato_dtfim.getTime()) : null;
		cliente = new Cliente(cliente_nome);
		empresa = new Empresa(empresa_nome);
		empresa.setNomefantasia(empresa_nomefantasia);
		aux_contrato = new Aux_contrato();
		aux_contrato.setSituacao(aux_contrato_situacao);
		identificador = contrato_identificador;
		aux_contrato.setIdentificadornumerico(aux_contrato_identificadornumerico);
		
	}
	public Contrato(
			Integer contrato_cdcontrato, 
			String contrato_descricao,
			java.util.Date contrato_dtproximovencimento, 
			java.util.Date contrato_dtinicio,
			java.util.Date contrato_dtfim,
			String cliente_nome,
			String empresa_nome,
			String empresa_razaosocial,
			String empresa_nomefantasia,
			SituacaoContrato aux_contrato_situacao,
			String contrato_identificador,
			Long aux_contrato_identificadornumerico
	){
		cdcontrato = contrato_cdcontrato;
		descricao = contrato_descricao;
		dtproximovencimento = contrato_dtproximovencimento != null ? new Date(contrato_dtproximovencimento.getTime()) : null;
		dtinicio = contrato_dtinicio != null ? new Date(contrato_dtinicio.getTime()) : null;
		dtfim = contrato_dtfim != null ? new Date(contrato_dtfim.getTime()) : null;
		cliente = new Cliente(cliente_nome);
		empresa = new Empresa(empresa_nome);
		empresa.setRazaosocial(empresa_razaosocial);
		empresa.setNomefantasia(empresa_nomefantasia);
		aux_contrato = new Aux_contrato();
		aux_contrato.setSituacao(aux_contrato_situacao);
		identificador = contrato_identificador;
		aux_contrato.setIdentificadornumerico(aux_contrato_identificadornumerico);
		
	}
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contrato")
	public Integer getCdcontrato() {
		return cdcontrato;
	}
	
	@DisplayName("Identificador")
	@MaxLength(20)
	public String getIdentificador() {
		return identificador;
	}

	@DisplayName("Tipo")
	@JoinColumn(name="cdcontratotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	
	@DisplayName("Tipo de documento")
	@JoinColumn(name="cddocumentotipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}

	@Required
	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	@JoinColumn(name="cdcontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contato getContato() {
		return contato;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(100)
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}

	@Required
	@DisplayName("In�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data de Assinatura")
	public Date getDtassinatura() {
		return dtassinatura;
	}
	@DisplayName("T�rmino")
	public Date getDtfim() {
		return dtfim;
	}

	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Tipo do Vendedor")
	public Vendedortipo getVendedortipo() {
		return vendedortipo;
	}
	
	@JoinColumn(name="cdvendedor")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pessoa getVendedor() {
		return vendedor;
	}

	@Required
	@DisplayName("Respons�vel")
	@JoinColumn(name="cdresponsavel")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getResponsavel() {
		return responsavel;
	}

	@DisplayName("Comiss�o do vendedor")
	@JoinColumn(name="cdcomissionamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Comissionamento getComissionamento() {
		return comissionamento;
	}
	
	public Date getDtsuspensao() {
		return dtsuspensao;
	}
	
	@DisplayName("Hist�rico")
	@MaxLength(255)
	public String getHistorico() {
		return historico;
	}

	@DisplayName("Anota��es")
	@MaxLength(255)
	public String getAnotacao() {
		return anotacao;
	}

	@Required
	@DisplayName("Pr�ximo vencimento")
	public Date getDtproximovencimento() {
		return dtproximovencimento;
	}

	@DisplayName("Periodicidade")
	@JoinColumn(name="cdfrequencia")
	@ManyToOne(fetch=FetchType.LAZY)
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	@DisplayName("Periodicidade de Renova��o")
	@JoinColumn(name="cdfrequenciarenovacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Frequencia getFrequenciarenovacao() {
		return frequenciarenovacao;
	}
	
	@DisplayName("Data da parcela deve ser um dia �til")
	public Boolean getDataparceladiautil() {
		return dataparceladiautil;
	}
	@DisplayName("Vencimento em dia �til dentro do m�s atual.")
	public Boolean getDataparcelaultimodiautilmes() {
		return dataparcelaultimodiautilmes;
	}

	@Required
	public Money getValor() {
		return valor;
	}
	
	@DisplayName("Valor Hora Extra")
	public Money getValorhoraextra() {
		return valorhoraextra;
	}

	@JoinColumn(name="cdrateio")
	@ManyToOne(fetch=FetchType.LAZY)
	public Rateio getRateio() {
		return rateio;
	}

	@JoinColumn(name="cdtaxa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Taxa getTaxa() {
		return taxa;
	}
	
	@MaxLength(4000)
	@DisplayName("Anota��es do corpo da nota")
	public String getAnotacaocorpo() {
		return anotacaocorpo;
	}
	
	@DisplayName("Faturamento em lote")
	public Boolean getFaturamentolote() {
		return faturamentolote;
	}

	@MaxLength(20)
	@DisplayName("IR(%)")
	public Double getIr() {
		return ir;
	}

	@DisplayName("Incide IR")
	public Boolean getIncideir() {
		return incideir;
	}

	@MaxLength(20)
	@DisplayName("ISS(%)")
	public Double getIss() {
		return iss;
	}

	@DisplayName("Incide ISS")
	public Boolean getIncideiss() {
		return incideiss;
	}

	@MaxLength(20)
	@DisplayName("PIS(%)")
	public Double getPis() {
		return pis;
	}

	@DisplayName("Incide PIS")
	public Boolean getIncidepis() {
		return incidepis;
	}

	@MaxLength(20)
	@DisplayName("COFINS(%)")
	public Double getCofins() {
		return cofins;
	}

	@DisplayName("Incide COFINS")
	public Boolean getIncidecofins() {
		return incidecofins;
	}

	@MaxLength(20)
	@DisplayName("CSLL(%)")
	public Double getCsll() {
		return csll;
	}

	@DisplayName("Incide CSLL")
	public Boolean getIncidecsll() {
		return incidecsll;
	}

	@MaxLength(20)
	@DisplayName("INSS(%)")
	public Double getInss() {
		return inss;
	}

	@DisplayName("Incide INSS")
	public Boolean getIncideinss() {
		return incideinss;
	}

	@MaxLength(20)
	@DisplayName("ICMS(%)")
	public Double getIcms() {
		return icms;
	}

	@DisplayName("Incide ICMS")
	public Boolean getIncideicms() {
		return incideicms;
	}

	@MaxLength(9)
	@DisplayName("C�digo da GPS")
	public Integer getCodigogps() {
		return codigogps;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("M�s pr�xima refer�ncia")
	public Mes getMes() {
		return mes;
	}
	
	@DisplayName("V�nculo")
	@JoinColumn(name="cdconta")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getConta() {
		return conta;
	}
	
	@DisplayName("Carteira")
	@JoinColumn(name="cdcontacarteira")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	
	@DisplayName("�ndice corre��o")
	@JoinColumn(name="cdindicecorrecao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	
	@DisplayName("Prazo pagamento")
	@JoinColumn(name="cdprazopagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	@DisplayName("Ano pr�xima refer�ncia")
	@MaxLength(4)
	public Integer getAno() {
		return ano;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato", insertable=false, updatable=false)
	public Aux_contrato getAux_contrato() {
		return aux_contrato;
	}
	
	@DisplayName("Colaborador")
	@OneToMany(mappedBy="contrato")
	public List<Contratocolaborador> getListaContratocolaborador() {
		return listaContratocolaborador;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="contrato")
	public List<Contratohistorico> getListaContratohistorico() {
		return listaContratohistorico;
	}
	
	@DisplayName("Servi�o")
	@OneToMany(mappedBy="contrato")
	public List<Contratomaterial> getListaContratomaterial() {
		return listaContratomaterial;
	}
	
	@DisplayName("Parcelas")
	@OneToMany(mappedBy="contrato")
	public List<Contratoparcela> getListaContratoparcela() {
		return listaContratoparcela;
	}

	@OneToMany(mappedBy="contrato")
	public List<Contratodesconto> getListaContratodesconto() {
		return listaContratodesconto;
	}
	
	@OneToMany(mappedBy="contrato")
	public List<NotaContrato> getListaNotacontrato() {
		return listaNotacontrato;
	}
	
	@MaxLength(100)
	@DisplayName("Nome fantasia")
	public String getNomefantasia() {
		return nomefantasia;
	}
	
	@MaxLength(20)
	@DisplayName("Inscri��o Estadual")
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	
	@DisplayName("Endere�o de Faturamento")
	@JoinColumn(name="cdendereco")
	@ManyToOne(fetch=FetchType.LAZY)
	public Endereco getEndereco() {
		return endereco;
	}
	@DisplayName("Endere�o de Entrega")
	@JoinColumn(name="cdenderecoentrega")
	@ManyToOne(fetch=FetchType.LAZY)	
	public Endereco getEnderecoentrega() {
		return enderecoentrega;
	}
	
	@DisplayName("Endere�o de Cobran�a")
	@JoinColumn(name="cdenderecocobranca")
	@ManyToOne(fetch=FetchType.LAZY)	
	public Endereco getEnderecocobranca() {
		return enderecocobranca;
	}

	public void setEnderecocobranca(Endereco enderecocobranca) {
		this.enderecocobranca = enderecocobranca;
	}

	public Date getDtconfirma() {
		return dtconfirma;
	}
	
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	
	@DisplayName("Data de finaliza��o")
	public Date getDtconclusao() {
		return dtconclusao;
	}
	
	@OneToMany(mappedBy="contrato")
	@DisplayName("Arquivos")
	public List<Contratoarquivo> getListaContratoarquivo() {
		return listaContratoarquivo;
	}
	
	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}
	
	@DisplayName("Fator de ajuste")
	@JoinColumn(name="cdfatorajuste")
	@ManyToOne(fetch=FetchType.LAZY)
	public Fatorajuste getFatorajuste() {
		return fatorajuste;
	}
	
	@DisplayName("Valor dedutivo")
	public Money getValordedutivocomissionamento() {
		return valordedutivocomissionamento;
	}
	
	@OneToMany(mappedBy="contrato")
	@DisplayName("Servi�os Vinculados")
	public List<Contratoservico> getListaContratoservico() {
		return listaContratoservico;
	}
	
	@DisplayName("C�digo CNAE")
	@JoinColumn(name="cdcodigocnae")
	@ManyToOne(fetch=FetchType.LAZY)
	public Codigocnae getCodigocnaeBean() {
		return codigocnaeBean;
	}

	@DisplayName("Item da lista de servi�o")
	@JoinColumn(name="cditemlistaservico")
	@ManyToOne(fetch=FetchType.LAZY)
	public Itemlistaservico getItemlistaservicoBean() {
		return itemlistaservicoBean;
	}

	@DisplayName("C�digo de tributa��o")
	@JoinColumn(name="cdcodigotributacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Codigotributacao getCodigotributacao() {
		return codigotributacao;
	}
	
	@DisplayName("Grupo de tributa��o")
	@JoinColumn(name="cdgrupotributacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	@DisplayName("Renova��o")
	public Date getDtrenovacao() {
		return dtrenovacao;
	}
	
	@Required
	@DisplayName("Forma de faturamento")
	public Formafaturamento getFormafaturamento() {
		return formafaturamento;
	}
	
	public Boolean getIsento() {
		return isento;
	}
	
	@DisplayName("N�o compor valor do contrato")
	public Boolean getIgnoravalormaterial() {
		return ignoravalormaterial;
	}
	
	@DisplayName("Acr�scimo somente no pr�ximo faturamento")
	public Boolean getAcrescimoproximofaturamento() {
		return acrescimoproximofaturamento;
	}
	
	@DisplayName("Valor de acr�scimo")
	public Money getValoracrescimo() {
		return valoracrescimo;
	}
	
	public Date getDtfimlocacao() {
		return dtfimlocacao;
	}
	
	public Date getDtiniciolocacao() {
		return dtiniciolocacao;
	}
	@Required
	@DisplayName("Finalizar contrato ao finalizar pagamentos")
	public Boolean getFinalizarcontratofinalizarpagamentos() {
		return finalizarcontratofinalizarpagamentos;
	}
	
	public void setDtconclusao(Date dtconclusao) {
		this.dtconclusao = dtconclusao;
	}
	
	public void setDtfimlocacao(Date dtfimlocacao) {
		this.dtfimlocacao = dtfimlocacao;
	}
	
	public void setDtiniciolocacao(Date dtiniciolocacao) {
		this.dtiniciolocacao = dtiniciolocacao;
	}
	
	public void setAcrescimoproximofaturamento(
			Boolean acrescimoproximofaturamento) {
		this.acrescimoproximofaturamento = acrescimoproximofaturamento;
	}
	
	public void setValoracrescimo(Money valoracrescimo) {
		this.valoracrescimo = valoracrescimo;
	}
	
	public void setIgnoravalormaterial(Boolean ignoravalormaterial) {
		this.ignoravalormaterial = ignoravalormaterial;
	}
	
	public void setIsento(Boolean isento) {
		this.isento = isento;
	}
	
	public void setFormafaturamento(Formafaturamento formafaturamento) {
		this.formafaturamento = formafaturamento;
	}
	
	public void setDtrenovacao(Date dtrenovacao) {
		this.dtrenovacao = dtrenovacao;
	}

	public void setCodigocnaeBean(Codigocnae codigocnaeBean) {
		this.codigocnaeBean = codigocnaeBean;
	}
	
	public void setFrequenciarenovacao(Frequencia frequenciarenovacao) {
		this.frequenciarenovacao = frequenciarenovacao;
	}

	public void setDataparceladiautil(Boolean dataparceladiautil) {
		this.dataparceladiautil = dataparceladiautil;
	}
	
	public void setDataparcelaultimodiautilmes(Boolean dataparcelaultimodiautilmes) {
		this.dataparcelaultimodiautilmes = dataparcelaultimodiautilmes;
	}

	public void setItemlistaservicoBean(Itemlistaservico itemlistaservicoBean) {
		this.itemlistaservicoBean = itemlistaservicoBean;
	}

	public void setCodigotributacao(Codigotributacao codigotributacao) {
		this.codigotributacao = codigotributacao;
	}
	
	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public void setValordedutivocomissionamento(Money valordedutivocomissionamento) {
		this.valordedutivocomissionamento = valordedutivocomissionamento;
	}

	public void setFatorajuste(Fatorajuste fatorajuste) {
		this.fatorajuste = fatorajuste;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}

	public void setListaContratoarquivo(List<Contratoarquivo> listaContratoarquivo) {
		this.listaContratoarquivo = listaContratoarquivo;
	}
	public void setListaContratoservico(
			List<Contratoservico> listaContratoservico) {
		this.listaContratoservico = listaContratoservico;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	
	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}
	
	public void setDtconfirma(Date dtconfirma) {
		this.dtconfirma = dtconfirma;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	
	public void setListaNotacontrato(List<NotaContrato> listaNotacontrato) {
		this.listaNotacontrato = listaNotacontrato;
	}
	
	public void setListaContratomaterial(
			List<Contratomaterial> listaContratomaterial) {
		this.listaContratomaterial = listaContratomaterial;
	}
	
	public void setListaContratohistorico(
			List<Contratohistorico> listaContratohistorico) {
		this.listaContratohistorico = listaContratohistorico;
	}
	
	public void setListaContratocolaborador(
			List<Contratocolaborador> listaContratocolaborador) {
		this.listaContratocolaborador = listaContratocolaborador;
	}

	public void setAux_contrato(Aux_contrato aux_contrato) {
		this.aux_contrato = aux_contrato;
	}
	
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	
	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public void setCdcontrato(Integer cdcontrato) {
		this.cdcontrato = cdcontrato;
	}	

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtassinatura(Date dtassinatura) {
		this.dtassinatura = dtassinatura;
	}
	
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setVendedortipo(Vendedortipo vendedortipo) {
		this.vendedortipo = vendedortipo;
	}

	public void setVendedor(Pessoa vendedor) {
		this.vendedor = vendedor;
	}

	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}

	public void setComissionamento(Comissionamento comissionamento) {
		this.comissionamento = comissionamento;
	}
	
	public void setDtsuspensao(Date dtsuspensao) {
		this.dtsuspensao = dtsuspensao;
	}
	
	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	public void setDtproximovencimento(Date dtproximovencimento) {
		this.dtproximovencimento = dtproximovencimento;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setValorhoraextra(Money valorhoraextra) {
		this.valorhoraextra = valorhoraextra;
	}

	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}

	public void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}

	public void setAnotacaocorpo(String anotacaocorpo) {
		this.anotacaocorpo = anotacaocorpo;
	}

	public void setIr(Double ir) {
		this.ir = ir;
	}

	public void setIncideir(Boolean incideir) {
		this.incideir = incideir;
	}

	public void setIss(Double iss) {
		this.iss = iss;
	}

	public void setIncideiss(Boolean incideiss) {
		this.incideiss = incideiss;
	}

	public void setPis(Double pis) {
		this.pis = pis;
	}

	public void setIncidepis(Boolean incidepis) {
		this.incidepis = incidepis;
	}

	public void setCofins(Double cofins) {
		this.cofins = cofins;
	}

	public void setIncidecofins(Boolean incidecofins) {
		this.incidecofins = incidecofins;
	}

	public void setCsll(Double csll) {
		this.csll = csll;
	}

	public void setIncidecsll(Boolean incidecsll) {
		this.incidecsll = incidecsll;
	}

	public void setInss(Double inss) {
		this.inss = inss;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void setEnderecoentrega(Endereco enderecoentrega) {
		this.enderecoentrega = enderecoentrega;
	}
	
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	
	public void setIncideinss(Boolean incideinss) {
		this.incideinss = incideinss;
	}

	public void setIcms(Double icms) {
		this.icms = icms;
	}

	public void setIncideicms(Boolean incideicms) {
		this.incideicms = incideicms;
	}

	public void setCodigogps(Integer codigogps) {
		this.codigogps = codigogps;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setFaturamentolote(Boolean faturamentolote) {
		this.faturamentolote = faturamentolote;
	}
	
	@Transient
	@DisplayName("Valor total dos servi�os")
	public Money getValorServicos() {
		return this.getValorServicos(null);
	}
	
	@Transient
	public Money getValorServicos(Date dtboleto){
		Date dtcomparacao = dtboleto != null ? dtboleto : this.dtproximovencimento;
		if(this.listaContratomaterial != null && this.listaContratomaterial.size() > 0 && dtcomparacao != null && (ignoravalormaterial == null || !ignoravalormaterial)){
			Money valor = new Money();
			Money valorLinha;
			for (Contratomaterial cm : this.listaContratomaterial) {
				if(cm != null && cm.getQtde() != null && cm.getValorunitario() != null){
					if(cm.getDtinicio() != null && SinedDateUtils.beforeIgnoreHour(dtcomparacao, cm.getDtinicio())){
						continue;
					}
					if(cm.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(cm.getDtfim(), dtcomparacao)){
						continue;
					}
					valorLinha = new Money(cm.getValorunitario()).multiply(new Money(cm.getQtde()));
					
					Integer periodocobranca = cm.getPeriodocobranca();
					if(periodocobranca != null){
						valorLinha = valorLinha.multiply(new Money(periodocobranca));
					}
					
					if(cm.getValordesconto() != null){
						if(valorLinha.compareTo(cm.getValordesconto()) <= 0){
							valorLinha = valorLinha.subtract(cm.getValordesconto());
						}
					}
					
					valor = valor.add(valorLinha);
				}
			}
			
			return valor;
		}
		return new Money();
	}

	@Transient
	@DisplayName("Valor total das parcelas")
	public Money getValorParcelas() {
		if(this.listaContratoparcela != null && this.listaContratoparcela.size() > 0){
			Money valor = new Money();
			Money valorLinha;
			for (Contratoparcela cp : this.listaContratoparcela) {
				if(cp != null && cp.getValor() != null){
					valorLinha = cp.getValor();
					if(cp.getDesconto() != null){
						if(valorLinha.compareTo(cp.getDesconto()) < 0){
							valorLinha = valorLinha.subtract(cp.getDesconto());
						}
					}
					valor = valor.add(valorLinha);
				}
			}
			
			return valor;
		}
		return new Money();
	}
	
	@Transient
	public Money getValorQtde(){
		Money valorqtde = this.getValor();
		Double qtde = this.getQtde();
		if(valorqtde != null && qtde != null){
			valorqtde = valorqtde.multiply(new Money(qtde));
		}
		return valorqtde;
	}
	
	@Transient
	public Money getValorCofins() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.cofins != null) {
			Money impostoBig = new Money(this.cofins, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorCofins = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorCofins = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_COFINS));
		}
		return valorCofins;
	}
	
	@Transient
	public Money getValorCsll() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.csll != null) {
			Money impostoBig = new Money(this.csll, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorCsll = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorCsll = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_CSLL));
		}
		return valorCsll;
	}
	
	@Transient
	public Money getValorIcms() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.icms != null) {
			Money impostoBig = new Money(this.icms, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorIcms = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorIcms = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_ICMS));
		}
		return valorIcms;
	}
	
	@Transient
	public Money getValorPis() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.pis != null) {
			Money impostoBig = new Money(this.pis, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorPis = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorPis = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_PIS));
		}
		return valorPis;
	}

	@Transient
	public Money getValorIss() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.iss != null) {
			Money impostoBig = new Money(this.iss, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorIss = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorIss = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_ISS));
		}
		return valorIss;
	}
	
	@Transient
	public Money getValorInss() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.inss != null) {
			Money impostoBig = new Money(this.inss, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorInss = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorInss = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_INSS));
		}
		return valorInss;
	}
	
	@Transient
	public Money getValorIr() {
		Money valorTotalServicos = this.getValorQtde();
		if (valorTotalServicos != null && this.ir != null) {
			Money impostoBig = new Money(this.ir, false);
			BigDecimal cemBig = new BigDecimal(100d);
			BigDecimal valorimpostoBig = valorTotalServicos.getValue().multiply(impostoBig.getValue()).divide(cemBig);
			
//			valorIr = new Money(SinedUtil.round(valorimpostoBig, 2));
			valorimpostoBig = valorimpostoBig.setScale(3, BigDecimal.ROUND_HALF_UP);
			valorIr = new Money(SinedUtil.roundImpostoByParametro(valorimpostoBig, Parametrogeral.TIPO_ARREDONDAMENTO_IMPOSTO_IR));
		}
		return valorIr;
	}
	
	@Transient
	public Money getValorTotalDesconto(){
		return this.getValorTotalDesconto(null);
	}
	
	@Transient
	public Money getValorTotalDesconto(Date dtboleto){
		Money desconto = new Money();
		if(this.listaContratodesconto != null && this.listaContratodesconto.size() > 0){
			boolean parcela = this.listaContratoparcela != null && this.listaContratoparcela.size() > 0;
			
			for (Contratodesconto cd : this.listaContratodesconto) {
				if(cd.getValor() != null && cd.getDtinicio() != null){
					if(parcela){
						for (Contratoparcela cp : this.listaContratoparcela) {
							if(cp.getValor() != null && cp.getDtvencimento() != null){
								if(cd.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(cd.getDtfim(), cp.getDtvencimento())){
									continue;
								}
								if(SinedDateUtils.afterIgnoreHour(cd.getDtinicio(), cp.getDtvencimento())){
									continue;
								}
								
								desconto = desconto.add(cd.getValor());
							}
						}
					} else {
						if(dtboleto != null){
							if(cd.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(cd.getDtfim(), dtboleto)){
								continue;
							}
							if(SinedDateUtils.afterIgnoreHour(cd.getDtinicio(), dtboleto)){
								continue;
							}
							
							desconto = desconto.add(cd.getValor());
						} else if(this.dtproximovencimento != null){
							if(cd.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(cd.getDtfim(), this.dtproximovencimento)){
								continue;
							}
							if(SinedDateUtils.afterIgnoreHour(cd.getDtinicio(), this.dtproximovencimento)){
								continue;
							}
							
							desconto = desconto.add(cd.getValor());
						}
					}
				}
			}
		}
		return desconto;
	}
	
	
	@Transient
	public Money getValorTotalImpostos(){
		Money impostos = new Money();
		
		if (this.getValorInss() != null && this.incideinss)
			impostos = impostos.add(this.getValorInss());
		
		if (this.getValorIr() != null && this.incideir)
			impostos = impostos.add(this.getValorIr());
		
		if (this.getValorIss() != null && this.incideiss)
			impostos = impostos.add(this.getValorIss());
		
		if (this.getValorIcms() != null && this.incideicms)
			impostos = impostos.add(this.getValorIcms());
		
		if (this.getValorPis() != null && this.incidepis)
			impostos = impostos.add(this.getValorPis());
		
		if (this.getValorCofins() != null && this.incidecofins)
			impostos = impostos.add(this.getValorCofins());
		
		if (this.getValorCsll() != null && this.incidecsll)
			impostos = impostos.add(this.getValorCsll());
		
		return impostos;
	}
	
	@Transient
	public Money getValorContrato() {
		return this.getValorContrato(null);
	}
	
	@Transient
	public Money getValorContrato(Date dtboleto){
		//se o contrato tiver fator de ajuste retorna o valor do fator de ajuste
		if(this.fatorajuste != null && this.fatorajuste.getCdfatorajuste() != null){
			Double valorDouble = new Double(0.0);
				String dtMesAnoAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
				Fatorajuste f = FatorajusteService.getInstance().carregaFatorajuste(this.fatorajuste);
				if(f != null && f.getListaFatorajustevalor() != null && !f.getListaFatorajustevalor().isEmpty()){
					for(Fatorajustevalor fatorajustevalor : f.getListaFatorajustevalor()){
						if(fatorajustevalor.getMesano() != null && fatorajustevalor.getMesanoAux() != null &&
								fatorajustevalor.getMesanoAux().equals(dtMesAnoAtual) && fatorajustevalor.getValor() != null){
							valorDouble = this.qtde != null ? fatorajustevalor.getValor().getValue().doubleValue() * this.qtde :
														fatorajustevalor.getValor().getValue().doubleValue();	
							
							Money valorImpostos = this.getValorTotalImpostos(); 
							Money valorDescontos = this.getValorTotalDesconto(dtboleto);
							Money valoracrescimo = this.getValoracrescimo();
							
							if(valorImpostos == null) valorImpostos = new Money();
							if(valorDescontos == null) valorDescontos = new Money();	
							if(valoracrescimo == null) valoracrescimo = new Money();
							
							return new Money(valorDouble).subtract(valorImpostos).subtract(valorDescontos).add(valoracrescimo);
						}
					}
				}
			return new Money(valorDouble);
		}else{
			Money impostos = this.getValorTotalImpostos(); 
			Money descontos = this.getValorTotalDesconto(dtboleto);
			Money valor = this.getValorQtde();
			Money valoracrescimo = this.getValoracrescimo();
			if(this.listaContratomaterial != null && this.listaContratomaterial.size() > 0 && dtboleto != null && (ignoravalormaterial == null || !ignoravalormaterial)){
				valor = this.getValorServicos(dtboleto);
			}
			
			if(impostos == null) impostos = new Money();
			if(descontos == null) descontos = new Money();
			if(valor == null) valor = new Money();
			if(valoracrescimo == null) valoracrescimo = new Money();
			
			return valor.subtract(impostos).subtract(descontos).add(valoracrescimo);
		}
	}
	
	@Transient
	public Money getValorContratoListagem(){
		//se o contrato tiver fator de ajuste retorna o valor do fator de ajuste
		if(this.fatorajuste != null && this.fatorajuste.getCdfatorajuste() != null){
			Double valorDouble = new Double(0.0);
				String dtMesAnoAtual = new SimpleDateFormat("MM/yyyy").format(System.currentTimeMillis());
				Fatorajuste f = FatorajusteService.getInstance().carregaFatorajuste(this.fatorajuste);
				if(f != null && f.getListaFatorajustevalor() != null && !f.getListaFatorajustevalor().isEmpty()){
					for(Fatorajustevalor fatorajustevalor : f.getListaFatorajustevalor()){
						if(fatorajustevalor.getMesano() != null && fatorajustevalor.getMesanoAux() != null &&
								fatorajustevalor.getMesanoAux().equals(dtMesAnoAtual) && fatorajustevalor.getValor() != null){
							valorDouble = this.qtde != null ? fatorajustevalor.getValor().getValue().doubleValue() * this.qtde :
														fatorajustevalor.getValor().getValue().doubleValue();	
							
							Money valorImpostos = this.getValorTotalImpostos(); 
							Money valorDescontos = this.getValorTotalDesconto(null);
							Money valoracrescimo = this.getValoracrescimo();
							
							if(valorImpostos == null) valorImpostos = new Money();
							if(valorDescontos == null) valorDescontos = new Money();
							if(valoracrescimo == null) valoracrescimo = new Money();
							
							return new Money(valorDouble).subtract(valorImpostos).subtract(valorDescontos).add(valoracrescimo);
						}
					}
				}
			return new Money(valorDouble);
		}else{
			Money impostos = this.getValorTotalImpostos(); 
			Money descontos = this.getValorTotalDesconto(null);
			Money valor = this.getValorQtde();
			Money valoracrescimo = this.getValoracrescimo();
			if(this.listaContratoparcela != null && !this.listaContratoparcela.isEmpty()){
				valor = this.getValorParcelas();
			}
			
			if(impostos == null) impostos = new Money();
			if(descontos == null) descontos = new Money();
			if(valor == null) valor = new Money();
			if(valoracrescimo == null) valoracrescimo = new Money();
			
			return valor.subtract(impostos).subtract(descontos).add(valoracrescimo);
		}
	}
	
	@Transient
	public Money getValorContratoSemDescontoAba(){
		Money impostos = this.getValorTotalImpostos(); 
		Money valor = this.getValorQtde();
		Money valoracrescimo = this.getValoracrescimo();
		
		if(impostos == null) impostos = new Money();
		if(valor == null) valor = new Money();
		if(valoracrescimo == null) valoracrescimo = new Money();
		
		return valor.subtract(impostos).add(valoracrescimo);
	}
	
	
	@Transient
	public Boolean getCumulativoCobrado() {
		return cumulativoCobrado;
	}
	
	@Transient
	public Proposta getProposta() {
		return proposta;
	}
	
	@Transient
	@DisplayName("Promocional")	
	public Material getMaterialpromocional() {
		return materialpromocional;
	}
	
	@Transient
	public Oportunidade getOportunidade() {
		return oportunidade;
	}
	
	@Transient
	public Integer getParcelas() {
		return parcelas;
	}
	
	@Transient
	public String getNotaremessalocacao() {
		return notaremessalocacao;
	}
	
	@Transient
	public String getDtemissaonotaremessalocacao() {
		return dtemissaonotaremessalocacao;
	}
	
	@Transient
	public Double getSaldoromaneio() {
		return saldoromaneio;
	}

	public void setNotaremessalocacao(String notaremessalocacao) {
		this.notaremessalocacao = notaremessalocacao;
	}
	
	public void setDtemissaonotaremessalocacao(String dtemissaonotaremessalocacao) {
		this.dtemissaonotaremessalocacao = dtemissaonotaremessalocacao;
	}
	
	public void setSaldoromaneio(Double saldoromaneio) {
		this.saldoromaneio = saldoromaneio;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}

	public void setMaterialpromocional(Material materialpromocional) {
		this.materialpromocional = materialpromocional;
	}

	public void setProposta(Proposta proposta) {
		this.proposta = proposta;
	}
	
	public void setCumulativoCobrado(Boolean cumulativoCobrado) {
		this.cumulativoCobrado = cumulativoCobrado;
	}

	public void setValorIr(Money valorIr) {
		this.valorIr = valorIr;
	}

	public void setValorInss(Money valorInss) {
		this.valorInss = valorInss;
	}

	public void setValorIss(Money valorIss) {
		this.valorIss = valorIss;
	}

	public void setValorContrato(Money valorContrato) {
		this.valorContrato = valorContrato;
	}
	
	public void setValorContratoListagem(Money valorContratoListagem) {
		this.valorContratoListagem = valorContratoListagem;
	}

	public void setListaContratoparcela(
			List<Contratoparcela> listaContratoparcela) {
		this.listaContratoparcela = listaContratoparcela;
	}
	
	public void setListaContratodesconto(List<Contratodesconto> listaContratodesconto) {
		this.listaContratodesconto = listaContratodesconto;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Contrato other = (Contrato) obj;
		if (cdcontrato == null) {
			if (other.cdcontrato != null)
				return false;
		} else if (!cdcontrato.equals(other.cdcontrato))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		if(cdcontrato != null)
			return cdcontrato.hashCode();
		return 0;
	}
	
	public String subQueryProjeto() {
		return "select contratosubQueryProjeto.cdcontrato " +
				"from Contrato contratosubQueryProjeto " +
				"join contratosubQueryProjeto.rateio rateiosubQueryProjeto " +
				"join rateiosubQueryProjeto.listaRateioitem listaRateioitemsubQueryProjeto " +
				"join listaRateioitemsubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public String getNome() {
		return getDescricao();
	}
	 
	@Transient
	public ContratoRTF getContratoRTF() {
		
		ContratoRTF contratoRTF = new ContratoRTF();
		
		contratoRTF.setCdcontrato(getCdcontrato() != null ? getCdcontrato().toString() : "");
		contratoRTF.setIdentificador(getIdentificador() != null ? getIdentificador() : "");
		
		//respons�vel legal
		contratoRTF.setContato_contrato(getResponsavel() != null && getResponsavel().getNome() != null ? getResponsavel().getNome() : "");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dateFormatDay = new SimpleDateFormat("dd");
		
		contratoRTF.setCnae_contrato(codigocnaeBean != null && codigocnaeBean.getDescricaocnae() != null ? codigocnaeBean.getDescricaocnae() : ""); 
		
		Cliente cliente = getCliente();
		
		contratoRTF.setNome_cliente(cliente.getNome());
		contratoRTF.setRepresentante(getContato() != null && getContato().getNome() != null ? getContato().getNome() : "");
		
		Endereco endereco = cliente.getEndereco();
		Endereco enderecofaturamento = this.getEndereco();
		Endereco enderecoentrega = this.getEnderecoentrega();
		if(enderecoentrega != null){
			contratoRTF.setEnderecocompleto_entrega_contrato(enderecoentrega.getLogradouroCompletoComCep());
		}
		if(enderecofaturamento != null){
			contratoRTF.setEnderecocompleto_faturamento_contrato(enderecofaturamento.getLogradouroCompletoComCep());
		}
		
		contratoRTF.setEndereco(endereco != null?endereco.getLogradouro():"");
		contratoRTF.setComplemento(endereco != null && endereco.getComplemento() != null ?endereco.getComplemento():"");
		contratoRTF.setNumero(endereco != null && endereco.getNumero() != null ?endereco.getNumero():"");
		contratoRTF.setBairro(endereco != null && endereco.getBairro() != null? endereco.getBairro():"");
		contratoRTF.setMunicipio(endereco != null && endereco.getMunicipio() != null ?endereco.getMunicipio().getNome():"");
		contratoRTF.setUf(endereco != null && endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null?endereco.getMunicipio().getUf().getSigla():"");
		contratoRTF.setCep_cliente(endereco != null && endereco.getCep() != null ? endereco.getCep().toString() : "");
		contratoRTF.setEnderecocompleto(endereco != null ? endereco.getLogradouroCompletoComCep() : "");
		
		contratoRTF.setEnderecoentrega(enderecoentrega != null?enderecoentrega.getLogradouro():"");
		contratoRTF.setComplementoentrega(enderecoentrega != null && enderecoentrega.getComplemento() != null ?enderecoentrega.getComplemento():"");
		contratoRTF.setNumeroentrega(enderecoentrega != null && enderecoentrega.getNumero() != null ?enderecoentrega.getNumero():"");
		contratoRTF.setBairroentrega(enderecoentrega != null && enderecoentrega.getBairro() != null? enderecoentrega.getBairro():"");
		contratoRTF.setMunicipioentrega(enderecoentrega != null && enderecoentrega.getMunicipio() != null ?enderecoentrega.getMunicipio().getNome():"");
		contratoRTF.setUfentrega(enderecoentrega != null && enderecoentrega.getMunicipio() != null && enderecoentrega.getMunicipio().getUf() != null?enderecoentrega.getMunicipio().getUf().getSigla():"");
		contratoRTF.setCep_enderecoentrega(enderecoentrega != null && enderecoentrega.getCep() != null ? enderecoentrega.getCep().toString() : "");
		contratoRTF.setEnderecocompletoentrega(enderecoentrega != null ? enderecoentrega.getLogradouroCompletoComCep() : "");
		
		contratoRTF.setInscricaomunicipal(cliente.getInscricaomunicipal() != null ? cliente.getInscricaomunicipal() : "");
		contratoRTF.setInscricaoestadual(cliente.getInscricaoestadual() != null ? cliente.getInscricaoestadual() : "");
		contratoRTF.setVendedornome(vendedor != null && vendedor.getNome() != null ? vendedor.getNome() : "");
		contratoRTF.setVendedoridentidade(colaborador != null && colaborador.getRg() != null ? colaborador.getRg() : "");
		contratoRTF.setHorainicio(this.getHorainicioContratoRTF());
		
		Set<Endereco> listaEndereco = cliente.getListaEndereco();
		if(listaEndereco != null && listaEndereco.size() > 0){
			for (Endereco end : listaEndereco) {
				if(end.getEnderecotipo() != null){
					if(end.getEnderecotipo().equals(Enderecotipo.COBRANCA)){
						contratoRTF.setEnderecocompleto_cobranca_cliente(end != null ? end.getLogradouroCompletoComCep() : "");
					} else if(end.getEnderecotipo().equals(Enderecotipo.ENTREGA) && contratoRTF.getEnderecocompleto_entrega_contrato() == null){
						contratoRTF.setEnderecocompleto_entrega_cliente(end != null ? end.getLogradouroCompletoComCep() : "");
					} else if(end.getEnderecotipo().equals(Enderecotipo.FATURAMENTO) && contratoRTF.getEnderecocompleto_faturamento_contrato() == null){
						contratoRTF.setEnderecocompleto_faturamento_cliente(end != null ? end.getLogradouroCompletoComCep() : "");
					}
				}
			}
		}
		
		List<PessoaContato> listaPessoaContato = cliente.getListaContato();
		boolean nomecontato = false;
		boolean contatosocio = false;
		
		if(listaPessoaContato != null && !listaPessoaContato.isEmpty()) {
			for (PessoaContato pessoaContato : listaPessoaContato) {
				Contato contato = pessoaContato.getContato();
				
				if(contato.getNome() != null && !nomecontato){
					contratoRTF.setContato_cliente(contato.getNome());
					nomecontato = true;;
				}
				
				if(!contatosocio && contato.getNome() != null && contato.getContatotipo() != null && contato.getContatotipo().getSocio() != null && 
						contato.getContatotipo().getSocio()){
					contratoRTF.setNome_contato_socio(contato.getNome());
					if(contato.getCpf() != null)
						contratoRTF.setCpf_contato_socio(contato.getCpf().toString());
					contatosocio = true;
				}
				
				if(contatosocio && nomecontato) break;
			}
		}
		
		contratoRTF.setCpf(cliente.getCpf() != null?cliente.getCpf().toString():"");
		contratoRTF.setCnpj(cliente.getCnpj() != null?cliente.getCnpj().toString():"");
		contratoRTF.setRazaosocial(cliente.getRazaoOrNomeByTipopessoa()!=null?cliente.getRazaoOrNomeByTipopessoa():"");
		contratoRTF.setEmail_cliente(cliente.getEmail() != null ? cliente.getEmail() : "");
		
		Set<Telefone> listaTelefone = cliente.getListaTelefone();
		
		Set<Telefone> listaCelular = new HashSet<Telefone>();
		Set<Telefone> listaNaoCelular = new HashSet<Telefone>();
		String telefoneprincipal = "";
		
		if(listaTelefone != null){
			for (Telefone telefone : listaTelefone) {
				Telefonetipo telefonetipo = telefone.getTelefonetipo();
				if(telefonetipo != null && telefonetipo.getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL)){
					telefoneprincipal = telefone.getTelefone();
				}
				if(telefonetipo != null && telefonetipo.getCdtelefonetipo().equals(Telefonetipo.CELULAR)){
					listaCelular.add(telefone);
				}else{
					listaNaoCelular.add(telefone);
				}
			}
		}
		
		contratoRTF.setCelular(CollectionsUtil.listAndConcatenate(listaCelular, "telefone", "/"));
		contratoRTF.setTelefone(CollectionsUtil.listAndConcatenate(listaNaoCelular, "telefone", "/"));
		contratoRTF.setTelefoneprincipal(telefoneprincipal);
		contratoRTF.setDescricao_contrato(getDescricao());
		contratoRTF.setData_vencimento_contrato(getDtproximovencimento() != null?simpleDateFormat.format(getDtproximovencimento()):"");
		contratoRTF.setData_inicio_contrato(getDtinicio() != null? simpleDateFormat.format(getDtinicio()):"");
		contratoRTF.setData_fim_contrato(getDtfim() != null ? simpleDateFormat.format(getDtfim()):"");
		contratoRTF.setData_inicio_locacao(getDtiniciolocacao() != null? simpleDateFormat.format(getDtiniciolocacao()):"");
		contratoRTF.setData_fim_locacao(getDtfimlocacao() != null ? simpleDateFormat.format(getDtfimlocacao()):"");
		contratoRTF.setValor_contrato(getValor() != null? getValor().toString():"");
		contratoRTF.setValorporextenso_contrato(getValor() != null? new Extenso(getValor()).toString():"");
		contratoRTF.setData_extenso(SinedDateUtils.dataExtenso(SinedDateUtils.currentDate()));
		contratoRTF.setData_renovacao(this.getDtrenovacao() != null ? simpleDateFormat.format(this.getDtrenovacao()):"");
		contratoRTF.setDia_vencimento(this.getDtproximovencimento() != null ? dateFormatDay.format(this.getDtproximovencimento()) : "");
		contratoRTF.setPeriodicidade(getFrequencia() != null && getFrequencia().getNome() != null ? getFrequencia().getNome() : "");
		contratoRTF.setValor_adicional(getValoracrescimo() != null ? getValoracrescimo().toString() : "");
		contratoRTF.setProfissao(cliente.getClienteprofissao() != null ? cliente.getClienteprofissao().getNome() : "");
		contratoRTF.setEstado_civil(cliente.getEstadocivil() != null ? cliente.getEstadocivil().getNome() : "");
		contratoRTF.setIdentidade(cliente.getRg() != null ? cliente.getRg() : "");
		contratoRTF.setNacionalidade(cliente.getNacionalidade() != null ? cliente.getNacionalidade().getDescricao() : "");
		contratoRTF.setAnotacoes(getAnotacao() != null ? getAnotacao() : "");
		contratoRTF.setHistorico(getHistorico() != null ? getHistorico() : "");
		contratoRTF.setIndicecorrecao(getIndicecorrecao() != null && getIndicecorrecao().getDescricao() != null ? getIndicecorrecao().getDescricao() : "");
		
		ContratoMaterialRTF contratoMaterialRTF;
		List<ContratoMaterialRTF> lista = new ArrayList<ContratoMaterialRTF>();		
		List<Contratomaterial> listaContratomaterial = getListaContratomaterial();
		Money valortotalservicocusto = new Money();
		Money valortotalservico = new Money();
		for (Contratomaterial contratomaterial : listaContratomaterial) {
			
			contratoMaterialRTF = new ContratoMaterialRTF();
			
			contratoMaterialRTF.setNome_material(contratomaterial.getServico() != null && contratomaterial.getServico().getNome() != null ? contratomaterial.getServico().getNome() : "");
			contratoMaterialRTF.setQtd_material(contratomaterial.getQtde() != null ? SinedUtil.descriptionDecimal(contratomaterial.getQtde()) : "");
			contratoMaterialRTF.setValor_unitario_material(contratomaterial.getValorunitario() != null ? new Money(contratomaterial.getValorunitario()).toString() : "");
			contratoMaterialRTF.setValor_custo_material(contratomaterial.getServico() != null && contratomaterial.getServico().getValorcusto() != null ? new Money(contratomaterial.getServico().getValorcusto()).toString() : "");
			contratoMaterialRTF.setValor_total_material(contratomaterial.getValortotal() != null ? contratomaterial.getValortotal().toString() : "");
			contratoMaterialRTF.setValor_total_custo_material(contratomaterial.getValortotalcusto() != null ? contratomaterial.getValortotalcusto().toString() : "");
			contratoMaterialRTF.setHorario_inicio(contratomaterial.getHrinicio() != null ? contratomaterial.getHrinicio().toString() : "");
			contratoMaterialRTF.setValor_indenizacao_material(contratomaterial.getServico() != null && contratomaterial.getServico().getValorindenizacao() != null ? new Money(contratomaterial.getServico().getValorindenizacao()).toString() : "");
			contratoMaterialRTF.setObservacao(contratomaterial.getObservacao() != null ? contratomaterial.getObservacao() : "");
			
			if(contratomaterial.getPatrimonioitem() != null && contratomaterial.getPatrimonioitem().getPlaqueta() != null){
				contratoMaterialRTF.setNome_material(contratoMaterialRTF.getNome_material() + " - " + contratomaterial.getPatrimonioitem().getPlaqueta());
			}
			if(contratomaterial.getValortotal() != null){
				valortotalservico = valortotalservico.add(contratomaterial.getValortotal());
			}
			if(contratomaterial.getValortotalcusto() != null){
				valortotalservicocusto = valortotalservicocusto.add(contratomaterial.getValortotalcusto());
			}
			
			boolean contatomaterialNotNull = contratomaterial!= null && contratomaterial.getServico()!= null && contratomaterial.getServico().getCaracteristicas()!= null;
			contratoMaterialRTF.setCaracteristica_material(contatomaterialNotNull ? contratomaterial.getServico().getCaracteristicas() : "");
			lista.add(contratoMaterialRTF);
		}
		contratoRTF.setServicos(lista);
		
		contratoRTF.setValortotalservicos_contrato(valortotalservico != null ? valortotalservico.toString() : "");
		contratoRTF.setValortotalservicosporextenso_contrato(valortotalservico != null && valortotalservico.getValue().doubleValue() >= 0 ? new Extenso(valortotalservico).toString() : "");
		
		contratoRTF.setValortotalservicoscusto_contrato(valortotalservicocusto != null ? valortotalservicocusto.toString() : "");
		contratoRTF.setValortotalservicosporextensocusto_contrato(valortotalservicocusto != null && valortotalservicocusto.getValue().doubleValue() >= 0 ? new Extenso(valortotalservicocusto).toString() : "");
		
		
		ContratoParcelaRTF contratoParcelaRTF;
		List<ContratoParcelaRTF> listaParcela = new ArrayList<ContratoParcelaRTF>();		
		List<Contratoparcela> listaContratoparcela = getListaContratoparcela();
		for (Contratoparcela contratoparcela : listaContratoparcela) {			
			contratoParcelaRTF = new ContratoParcelaRTF();
			
			contratoParcelaRTF.setValor_parcela(contratoparcela.getValor() != null ? contratoparcela.getValor().toString() : "");
			contratoParcelaRTF.setData_vencimento(contratoparcela.getDtvencimento() != null ? simpleDateFormat.format(contratoparcela.getDtvencimento()) : "");
			
			listaParcela.add(contratoParcelaRTF);
		}
		contratoRTF.setParcelas(listaParcela);
		
		Contratoparcela contratoparcela = this.findParcelaMenorDataNaoCobrada(getListaContratoparcela());
		if(contratoparcela!= null) {
			String dataParcela = simpleDateFormat.format(contratoparcela.getDtvencimento());
			String valorParcela = contratoparcela.getValor().toString();
			contratoRTF.setData_proxima_parcela(dataParcela);
			contratoRTF.setValor_proxima_parcela(valorParcela);
		}else {
			contratoRTF.setData_proxima_parcela("");
			contratoRTF.setValor_proxima_parcela("");
		}
		
		Empresa empresa = getEmpresa();
		if(empresa == null || 
				empresa.getCdpessoa() == null || 
				empresa.getLogomarca() == null || 
				empresa.getLogomarca().getCdarquivo() == null)
			empresa = EmpresaService.getInstance().loadArquivoPrincipal();
		
		if(empresa.getLogomarca() != null && empresa.getLogomarca().getCdarquivo() != null){
			Arquivo logo = empresa.getLogomarca();
			try {
				File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(logo));
				BufferedImage image = ImageIO.read(input);
				
				File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(logo));
				ImageIO.write(image, "png", output);  
				
				contratoRTF.setLogo(new FileInputStream(output));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		contratoRTF.setIr(getIr() != null ? SinedUtil.descriptionDecimal(getIr()) : "");
		contratoRTF.setIss(getIss() != null ? SinedUtil.descriptionDecimal(getIss()) : "");
		contratoRTF.setPis(getPis() != null ? SinedUtil.descriptionDecimal(getPis()) : "");
		contratoRTF.setCofins(getCofins() != null ? SinedUtil.descriptionDecimal(getCofins()) : "");
		contratoRTF.setCsll(getCsll() != null ? SinedUtil.descriptionDecimal(getCsll()) : "");
		contratoRTF.setInss(getInss() != null ? SinedUtil.descriptionDecimal(getInss()) : "");
		contratoRTF.setIcms(getIcms() != null ? SinedUtil.descriptionDecimal(getIcms()) : "");
		
		Money valorIr = getValorIr();
		Money valorIss = getValorIss();
		Money valorPis = getValorPis();
		Money valorCofins = getValorCofins();
		Money valorCsll = getValorCsll();
		Money valorInss = getValorInss();
		Money valorIcms = getValorIcms();
		Money valorAcrescimo = getValoracrescimo();
		
		contratoRTF.setValor_ir(valorIr != null ? valorIr.toString() : "");
		contratoRTF.setValor_iss(valorIss != null ? valorIss.toString() : "");
		contratoRTF.setValor_pis(valorPis != null ? valorPis.toString() : "");
		contratoRTF.setValor_cofins(valorCofins != null ? valorCofins.toString() : "");
		contratoRTF.setValor_csll(valorCsll != null ? valorCsll.toString() : "");
		contratoRTF.setValor_inss(valorInss != null ? valorInss.toString() : "");
		contratoRTF.setValor_icms(valorIcms != null ? valorIcms.toString() : "");
		contratoRTF.setValor_acrescimo(valorAcrescimo != null ? valoracrescimo.toString() : "");
		
		
		contratoRTF.setEmail_cliente(cliente.getEmail() != null ? cliente.getEmail() : "");
		contratoRTF.setEmail_contato(contato != null && contato.getEmailcontato() != null ? contato.getEmailcontato() : "");
		contratoRTF.setRg_representante(contato != null && contato.getIdentidade()!= null ? contato.getIdentidade() : "");
		contratoRTF.setCpf_representante(contato != null && contato.getCpf() != null ? contato.getCpf().toString() : "");
		
		
		Integer quantidadeParcelas = getPrazopagamento() != null && getListaContratoparcela() != null ? getListaContratoparcela().size() : 0;
		contratoRTF.setQuantidadeparcelas(quantidadeParcelas.toString());
		
		
		contratoRTF.setNotaremessalocacao(getNotaremessalocacao());
		contratoRTF.setDtemissaonotaremessalocacao(getDtemissaonotaremessalocacao());
		contratoRTF.setSaldoromaneio(NeoFormater.getInstance().format(getSaldoromaneio()));
		contratoRTF.setPeriodicidadeRenovacao(getFrequenciarenovacao() != null ? getFrequenciarenovacao().getNome(): "");
		return contratoRTF;
		
	}	
	
	private Contratoparcela findParcelaMenorDataNaoCobrada(List<Contratoparcela> parcelas) {
		if(parcelas != null && !parcelas.isEmpty()) {
			Contratoparcela parcelaMenorData = parcelas.get(0);
			for (Contratoparcela contratoparcela : parcelas) {
				if(contratoparcela.getCobrada() != null && !contratoparcela.getCobrada() && contratoparcela.getDtvencimento().before(parcelaMenorData.getDtvencimento())) {
					parcelaMenorData = contratoparcela;
				}
			}
			return parcelaMenorData;
		}
		return null;
	}

	@Transient
	private String getHorainicioContratoRTF() {
		String horainicio = "";
		Timestamp dthistorico = null;
		if(this.getListaContratohistorico() != null && !this.getListaContratohistorico().isEmpty()){
			for(Contratohistorico contratohistorico : this.getListaContratohistorico()){
				if(contratohistorico.getAcao() != null && Contratoacao.CONFIRMADO.equals(contratohistorico.getAcao())){
					dthistorico = contratohistorico.getDthistorico();
					break;
				}
			}
		}
		
		if(dthistorico != null){
			try {
				horainicio = new SimpleDateFormat("HH:mm").format(dthistorico);
			} catch (Exception e) {}
		}
		
		return horainicio;
	}

	@Transient
	public Faturamentocontrato getFaturamentocontrato() {
		return faturamentocontrato;
	}
	
	public void setFaturamentocontrato(Faturamentocontrato faturamentocontrato) {
		this.faturamentocontrato = faturamentocontrato;
	}
	
	@Transient
	@DisplayName("Confirmar?")
	public Boolean getConfirmar() {
		return confirmar;
	}
	
	public void setConfirmar(Boolean confirmar) {
		this.confirmar = confirmar;
	}

	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Transient
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@Transient
	public Date getDtVencimentoAtualiza() {
		return dtVencimentoAtualiza;
	}
	@Transient
	public Money getValorAtualiza() {
		return valorAtualiza;
	}	
	@Transient
	public Colaborador getResponsavelAtualiza() {
		return responsavelAtualiza;
	}
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	@Transient
	public Boolean getEntrada() {
		return entrada;
	}
	
	public void setDtVencimentoAtualiza(Date dtVencimentoAtualiza) {
		this.dtVencimentoAtualiza = dtVencimentoAtualiza;
	}

	public void setValorAtualiza(Money valorAtualiza) {
		this.valorAtualiza = valorAtualiza;
	}

	public void setResponsavelAtualiza(Colaborador responsavelAtualiza) {
		this.responsavelAtualiza = responsavelAtualiza;
	}

	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}
	
	@Transient
	public Money getValorContratoAFaturarPorMedicao() {
		return valorContratoAFaturarPorMedicao;
	}	
	@Transient
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	public void setValorContratoAFaturarPorMedicao(Money valorContratoAFaturarPorMedicao) {
		this.valorContratoAFaturarPorMedicao = valorContratoAFaturarPorMedicao;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	@Transient
	public Money getValorContratoFaturado() {
		return valorContratoFaturado;
	}
	public void setValorContratoFaturado(Money valorContratoFaturado) {
		this.valorContratoFaturado = valorContratoFaturado;
	}

	@DisplayName("URL do Sistema")
	public String getUrlsistema() {
		return urlsistema;
	}

	public void setUrlsistema(String urlsistema) {
		this.urlsistema = urlsistema;
	}

	@OneToMany(mappedBy="contrato")
	public List<Documentoorigem> getListaDocumentoOrigem() {
		return listaDocumentoOrigem;
	}

	public void setListaDocumentoOrigem(List<Documentoorigem> listaDocumentoOrigem) {
		this.listaDocumentoOrigem = listaDocumentoOrigem;
	}
	
	@OneToMany(mappedBy="contrato")
	public List<Romaneioorigem> getListaRomaneioorigem() {
		return listaRomaneioorigem;
	}

	public void setListaRomaneioorigem(List<Romaneioorigem> listaRomaneioorigem) {
		this.listaRomaneioorigem = listaRomaneioorigem;
	}

	@Transient
	public Double getPercentualValorAtualiza() {
		return percentualValorAtualiza;
	}
	
	public void setPercentualValorAtualiza(Double percentualValorAtualiza) {
		this.percentualValorAtualiza = percentualValorAtualiza;
	}
	
	@Transient
	public Date getDtrecalculo1() {
		return dtrecalculo1;
	}
	
	@Transient
	public Date getDtrecalculo2() {
		return dtrecalculo2;
	}
	
	public void setDtrecalculo1(Date dtrecalculo1) {
		this.dtrecalculo1 = dtrecalculo1;
	}
	
	public void setDtrecalculo2(Date dtrecalculo2) {
		this.dtrecalculo2 = dtrecalculo2;
	}
	
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
	@Transient
	public Boolean getViaWebService() {
		return viaWebService;
	}
	
	public void setViaWebService(Boolean viaWebService) {
		this.viaWebService = viaWebService;
	}
	
	@Transient
	public Documento getDocumento() {
		return documento;
	}
	
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	@Transient
	public Boolean getAtualizarparcelasindiceatual() {
		return atualizarparcelasindiceatual;
	}
	public void setAtualizarparcelasindiceatual(Boolean atualizarparcelasindiceatual) {
		this.atualizarparcelasindiceatual = atualizarparcelasindiceatual;
	}

	@DisplayName("Valor Fechado")
	public Money getValortotalfechado() {
		return valortotalfechado;
	}
	@DisplayName("Al�quota")
	public Double getAliquotavalortotalfechado() {
		return aliquotavalortotalfechado;
	}
	public void setValortotalfechado(Money valortotalfechado) {
		this.valortotalfechado = valortotalfechado;
	}
	public void setAliquotavalortotalfechado(Double aliquotavalortotalfechado) {
		this.aliquotavalortotalfechado = aliquotavalortotalfechado;
	}

	@Transient
	@DisplayName("Valor Total Cheio")
	public Money getValortotalcheio() {
		if(this.listaContratomaterial != null && this.listaContratomaterial.size() > 0){
			Money valor = new Money();
			for (Contratomaterial cm : this.listaContratomaterial) {
				if(cm != null && cm.getQtde() != null && cm.getValorunitario() != null){
					valor = valor.add(new Money(cm.getValorunitario()).multiply(new Money(cm.getQtde())));
				}
			}
			
			return valor;
		}
		return new Money();
	}
	public void setValortotalcheio(Money valortotalcheio) {
		this.valortotalcheio = valortotalcheio;
	}
	
	
	@Transient
	@DisplayName("Total Locado")
	public Money getFaturalocacaoTotallocado() {
		return faturalocacaoTotallocado;
	}
	@Transient
	@DisplayName("Total Devolvido")
	public Money getFaturalocacaoTotaldevolvido() {
		return faturalocacaoTotaldevolvido;
	}
	@Transient
	@DisplayName("Desconto")
	public Money getFaturalocacaoTotaldesconto() {
		return faturalocacaoTotaldesconto;
	}
	@Transient
	@DisplayName("Acr�scimo")
	public Money getFaturalocacaoTotalacrescimo() {
		return faturalocacaoTotalacrescimo;
	}

	public void setFaturalocacaoTotallocado(Money faturalocacaoTotallocado) {
		this.faturalocacaoTotallocado = faturalocacaoTotallocado;
	}
	public void setFaturalocacaoTotaldevolvido(Money faturalocacaoTotaldevolvido) {
		this.faturalocacaoTotaldevolvido = faturalocacaoTotaldevolvido;
	}
	public void setFaturalocacaoTotaldesconto(Money faturalocacaoTotaldesconto) {
		this.faturalocacaoTotaldesconto = faturalocacaoTotaldesconto;
	}
	public void setFaturalocacaoTotalacrescimo(Money faturalocacaoTotalacrescimo) {
		this.faturalocacaoTotalacrescimo = faturalocacaoTotalacrescimo;
	}

	@Transient
	@DisplayName("Valor a Faturar")
	public Money getFaturalocacaoValorfaturar() {
		Money valorfaturar = new Money();
		if(this.getFaturalocacaoTotallocado() != null)
			valorfaturar = valorfaturar.add(this.getFaturalocacaoTotallocado());
		if(this.getFaturalocacaoTotaldevolvido() != null)
			valorfaturar = valorfaturar.subtract(this.getFaturalocacaoTotaldevolvido());
		if(this.getFaturalocacaoTotalacrescimo() != null)
			valorfaturar = valorfaturar.add(this.getFaturalocacaoTotalacrescimo());
		if(this.getFaturalocacaoTotaldesconto() != null)
			valorfaturar = valorfaturar.subtract(this.getFaturalocacaoTotaldesconto());
		return valorfaturar;
	}
	public void setFaturalocacaoValorfaturar(Money faturalocacaoValorfaturar) {
		this.faturalocacaoValorfaturar = faturalocacaoValorfaturar;
	}
	
	@Transient
	public Boolean getIdentificadorautomatico() {
		return identificadorautomatico;
	}
	
	public void setIdentificadorautomatico(Boolean identificadorautomatico) {
		this.identificadorautomatico = identificadorautomatico;
	}
	
	@Transient
	public String getDescricaoIdentificador(){
		String str = "";
		
		if(this.getIdentificador() != null && !"".equals(this.getIdentificador())){
			str += this.getIdentificador();
		}else if(this.getCdcontrato() != null){
			str += this.getCdcontrato();
		}
		
		str += " - " + this.getDescricao();
		
		return str;
	}
	
	@Transient
	@DisplayName("Finalizar Contrato")
	public Boolean getFinalizarcontrato() {
		return finalizarcontrato;
	}
	
	public void setFinalizarcontrato(Boolean finalizarcontrato) {
		this.finalizarcontrato = finalizarcontrato;
	}
	
	@Transient
	public String getIdentificadorContratotipo(){
		String str = "";
		
		if(this.getIdentificador() != null && !"".equals(this.getIdentificador())){
			str += this.getIdentificador();
		}else if(this.getCdcontrato() != null){
			str += this.getCdcontrato();
		}
		
		if(getContratotipo() != null && getContratotipo().getNome() != null && !"".equals(getContratotipo().getNome())){
			if(str != null && !"".equals(str)) str += " - ";
			str += getContratotipo().getNome();
		}
		
		return str;
	}

	@Transient
	public Boolean getIndenizacao() {
		return indenizacao;
	}

	public void setIndenizacao(Boolean indenizacao) {
		this.indenizacao = indenizacao;
	}
	
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	} 
	
	@Transient
	public String getNomeCliente() {
		if(getCliente() != null && getCliente().getNome() != null){
			nomeCliente = getCliente().getNome();
		}
		return nomeCliente;
	}
	
	@Transient
	public String getContratoAutocompleteForComissaoVendedor(){
		StringBuilder s = new StringBuilder();
		
		if(this.getCdcontrato() != null){
			s.append(this.getCdcontrato());
			
			if(this.getIdentificador() != null){					
				s.append(" - ").append(this.getIdentificador());
			}
			
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}
	
	@Transient
	public String getIdentificadorOrCdcontrato(){
		if(getIdentificador() != null && !"".equals(getIdentificador())){
			return getIdentificador();
		}else if(getCdcontrato() != null){
			return getCdcontrato().toString();
		}
		return "";
	}
	
	@DisplayName("Venda Or�amento")
	@JoinColumn(name="cdvendaorcamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}
	
	public void setFinalizarcontratofinalizarpagamentos(Boolean finalizarcontratofinalizarpagamentos) {
		this.finalizarcontratofinalizarpagamentos = finalizarcontratofinalizarpagamentos;
	}
	
	@Transient
	public Integer getMesesFinalContrato() {
		return mesesFinalContrato;
	}
	public void setMesesFinalContrato(Integer mesesFinalContrato) {
		this.mesesFinalContrato = mesesFinalContrato;
	}

	@Transient
	public Integer getQtdePromocional() {
		return qtdePromocional;
	}

	public void setQtdePromocional(Integer qtdePromocional) {
		this.qtdePromocional = qtdePromocional;
	}
	
	@Transient
	public Contatipo getContatipo() {
		if(conta != null && contatipo == null){
			contatipo = conta.getContatipo();
		}
		return contatipo;
	}
	
	public void setContatipo(Contatipo contatipo) {
		this.contatipo = contatipo;
	}
	
	@Column(insertable=false, updatable=false)
	public Boolean getReajuste() {
		return reajuste;
	}
	
	public void setReajuste(Boolean reajuste) {
		this.reajuste = reajuste;
	}
	
	@Column(insertable=false, updatable=false)
	public Double getPercentualreajuste() {
		return percentualreajuste;
	}
	
	public void setPercentualreajuste(Double percentualreajuste) {
		this.percentualreajuste = percentualreajuste;
	}
	
	@OneToMany(mappedBy="contratofechamento")
	public List<Romaneioorigem> getListaRomaneioorigemFechamento() {
		return listaRomaneioorigemFechamento;
	}
	public void setListaRomaneioorigemFechamento(
			List<Romaneioorigem> listaRomaneioorigemFechamento) {
		this.listaRomaneioorigemFechamento = listaRomaneioorigemFechamento;
	}
	
	@Transient
	public String getMensagemBoletoReajuste() {
		String msg = "Reajuste previsto em contrato realizado de acordo com o �ndice INPC";
		
		if (percentualreajuste!=null && percentualreajuste>0D){
			msg += " " + new DecimalFormat("###,##0.##").format(percentualreajuste) + "%";
		}
		
		return msg;
	}
	
	@Transient
	public ContaContabil getContagerencialContabilSPED(){
		if (SinedUtil.isListNotEmpty(getListaContratomaterial())){
			for (Contratomaterial contratomaterial: getListaContratomaterial()){
				if (contratomaterial.getServico()!=null && contratomaterial.getServico().getContaContabil()!=null){
					return contratomaterial.getServico().getContaContabil();
				}
			}
		}
		if(getContaContabil() != null){
			return getContaContabil();
		}
		return null;
	}
	
	@Transient
	public Endereco getEnderecoPrioritario() {
		return this.enderecoPrioritario;
	}

	public void setEnderecoPrioritario(Endereco enderecoPrioritario) {
		this.enderecoPrioritario = enderecoPrioritario;
	}
}