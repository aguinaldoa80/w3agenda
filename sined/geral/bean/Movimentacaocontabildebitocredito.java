package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_movimentacaocontabildebitocredito", sequenceName = "sq_movimentacaocontabildebitocredito")
public class Movimentacaocontabildebitocredito implements Cloneable {

	protected Integer cdmovimentacaocontabildebitocredito;
	protected Movimentacaocontabil movimentacaocontabildebito;
	protected Movimentacaocontabil movimentacaocontabilcredito;
	protected ContaContabil contaContabil;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Money valor;
	protected String historico; 
	protected Operacaocontabildebitocredito operacaocontabildebitocredito;
	
	public Movimentacaocontabildebitocredito(){
	}
	
	public Movimentacaocontabildebitocredito(ContaContabil contacontabil, Centrocusto centrocusto, Projeto projeto, Money valor, String historico, Operacaocontabildebitocredito operacaocontabildebitocredito){
		this.contaContabil = contacontabil;
		this.centrocusto = centrocusto;
		this.projeto = projeto;
		this.valor = valor;
		this.historico = historico;
		this.operacaocontabildebitocredito = operacaocontabildebitocredito;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_movimentacaocontabildebitocredito")
	public Integer getCdmovimentacaocontabildebitocredito() {
		return cdmovimentacaocontabildebitocredito;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmovimentacaocontabildebito")
	public Movimentacaocontabil getMovimentacaocontabildebito() {
		return movimentacaocontabildebito;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdmovimentacaocontabilcredito")
	public Movimentacaocontabil getMovimentacaocontabilcredito() {
		return movimentacaocontabilcredito;
	}
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	@DisplayName("Centro de custo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@Required
	@DisplayName("Valor")
	public Money getValor() {
		return valor;
	}
	@DisplayName("Hist�rico Espec�fico")
	@MaxLength(500)
	public String getHistorico() {
		return historico;
	}
	
	@DisplayName("Opera��o cont�bil Cr�dito/D�bito")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdoperacaocontabildebitocredito")
	public Operacaocontabildebitocredito getOperacaocontabildebitocredito() {
		return operacaocontabildebitocredito;
	}
	
	@Transient
	public String getDescricaoGerarLancamentosContabeis(){
		String retorno = getContaContabil().getNome();
		retorno += "<br>";
		
		if (getCentrocusto()!=null){
			retorno += getCentrocusto().getNome();
		}else {
			retorno += "-";
		}
		
		retorno += "<br>";
		
		if (getProjeto()!=null){
			retorno += getProjeto().getNome();
		}else {
			retorno += "-";
		}
		
		retorno += "<br>";
		retorno += getValor();
		
		return retorno;
	}
	@Transient
	public Movimentacaocontabildebitocredito getClone() throws CloneNotSupportedException {
		return (Movimentacaocontabildebitocredito) clone();
	}
	public void setOperacaocontabildebitocredito(
			Operacaocontabildebitocredito operacaocontabildebitocredito) {
		this.operacaocontabildebitocredito = operacaocontabildebitocredito;
	}
	public void setCdmovimentacaocontabildebitocredito(Integer cdmovimentacaocontabildebitocredito) {
		this.cdmovimentacaocontabildebitocredito = cdmovimentacaocontabildebitocredito;
	}
	public void setMovimentacaocontabildebito(Movimentacaocontabil movimentacaocontabildebito) {
		this.movimentacaocontabildebito = movimentacaocontabildebito;
	}
	public void setMovimentacaocontabilcredito(Movimentacaocontabil movimentacaocontabilcredito) {
		this.movimentacaocontabilcredito = movimentacaocontabilcredito;
	}
	public void setContaContabil(ContaContabil contacontabil) {
		this.contaContabil = contacontabil;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
}