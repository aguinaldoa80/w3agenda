package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_documentoclasse", sequenceName = "sq_documentoclasse")
public class Documentoclasse {

	protected Integer cddocumentoclasse;
	protected String nome;
	
	public static final Integer CD_PAGAR = 1;
	public static final Integer CD_RECEBER = 2;
	public static final Documentoclasse OBJ_PAGAR = new Documentoclasse(CD_PAGAR);
	public static final Documentoclasse OBJ_RECEBER = new Documentoclasse(CD_RECEBER);

	public Documentoclasse(){}
	
	public Documentoclasse(Integer cddocumentoclasse){
		this.cddocumentoclasse = cddocumentoclasse;
	}
	
	public Documentoclasse(Integer cddocumentoclasse, String nome) {
		this.cddocumentoclasse = cddocumentoclasse;
		this.nome = nome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentoclasse")
	public Integer getCddocumentoclasse() {
		return cddocumentoclasse;
	}
	public void setCddocumentoclasse(Integer id) {
		this.cddocumentoclasse = id;
	}

	
	@Required
	@MaxLength(10)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Documentoclasse) {
			Documentoclasse documentoclasse = (Documentoclasse) obj;
			return documentoclasse.getCddocumentoclasse().equals(this.cddocumentoclasse);
		}
		return super.equals(obj);
	}

}
