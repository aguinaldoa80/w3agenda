package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("Histograma")
@SequenceGenerator(name = "sq_periodoorcamento", sequenceName = "sq_periodoorcamento")
public class Periodoorcamento {
	
	protected Integer cdperiodoorcamento;
	protected Periodoorcamentocargo periodoorcamentocargo;
	protected Integer numero;
	protected Double qtde;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_periodoorcamento")
	public Integer getCdperiodoorcamento() {
		return cdperiodoorcamento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdperiodoorcamentocargo")
	public Periodoorcamentocargo getPeriodoorcamentocargo() {
		return periodoorcamentocargo;
	}
	public Double getQtde() {
		return qtde;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setCdperiodoorcamento(Integer cdperiodoorcamento) {
		this.cdperiodoorcamento = cdperiodoorcamento;
	}
	public void setPeriodoorcamentocargo(
			Periodoorcamentocargo periodoorcamentocargo) {
		this.periodoorcamentocargo = periodoorcamentocargo;
	}
}
