package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contatotipo", sequenceName = "sq_contatotipo")
@DisplayName("Tipo de contato")
public class Contatotipo implements Log{

	protected Integer cdcontatotipo;
	protected String nome;
	protected Boolean socio;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Contatotipo() {}
	
	public Contatotipo(Integer cdcontatotipo) {
		this.cdcontatotipo = cdcontatotipo;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contatotipo")
	public Integer getCdcontatotipo() {
		return cdcontatotipo;
	}

	@DescriptionProperty
	@MaxLength(20)
	@Required
	public String getNome() {
		return nome;
	}
	
	@DisplayName("S�cio")
	public Boolean getSocio() {
		return socio;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdcontatotipo(Integer cdcontatotipo) {
		this.cdcontatotipo = cdcontatotipo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setSocio(Boolean socio) {
		this.socio = socio;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
