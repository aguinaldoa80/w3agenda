package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum FornecedorTipoEnum {
	
	TODOS(0, "Todos"),
	TRANSPORTADOR(1, "Transportador"),
	FABRICANTE(2, "Fabricante");
	
	private FornecedorTipoEnum (Integer value, String tipo){
		this.tipo = tipo;
		this.value = value;
	}
	
	private Integer value;
	private String tipo;
	
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return this.tipo;
	}

}
