package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contaempresa", sequenceName = "sq_contaempresa")
public class Contaempresa {

	protected Integer cdcontaempresa;
	protected Conta conta;
	protected Empresa empresa;
	
	public Contaempresa() {
	}
	
	public Contaempresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contaempresa")
	@DisplayName("Id")
	public Integer getCdcontaempresa() {
		return cdcontaempresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {	
		return conta;		
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")     
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setCdcontaempresa(Integer cdcontaempresa) {
		this.cdcontaempresa = cdcontaempresa;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
