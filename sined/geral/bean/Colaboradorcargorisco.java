package br.com.linkcom.sined.geral.bean;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorcargorisco", sequenceName = "sq_colaboradorcargorisco")
public class Colaboradorcargorisco{

	protected Integer cdcolaboradorcargorisco;
	protected Colaboradorcargo colaboradorcargo;
	protected Riscotrabalho riscotrabalho;
	protected Exameresponsavel exameresponsavel;
	protected Cargoriscointensidade cargoriscointensidade;
	protected Cargoriscotecnica cargoriscotecnica;
	protected Date dtinicio;
	protected Date dtfim;
	protected Boolean epi;
	protected Boolean epc;
	protected Epi materialepi;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcargorisco")
	public Integer getCdcolaboradorcargorisco() {
		return cdcolaboradorcargorisco;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorcargo")
	public Colaboradorcargo getColaboradorcargo() {
		return colaboradorcargo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdriscotrabalho")
	@DisplayName("Risco")
	@Required
	public Riscotrabalho getRiscotrabalho() {
		return riscotrabalho;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexameresponsavel")
	@DisplayName("Profissional Respons�vel")
	public Exameresponsavel getExameresponsavel() {
		return exameresponsavel;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargoriscointensidade")
	@DisplayName("Intensidade / Concentra��o")
	public Cargoriscointensidade getCargoriscointensidade() {
		return cargoriscointensidade;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargoriscotecnica")
	@DisplayName("T�cnica utilizada")
	@Required
	public Cargoriscotecnica getCargoriscotecnica() {
		return cargoriscotecnica;
	}

	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	
	@Required
	@DisplayName("EPC eficaz")
	public Boolean getEpc() {
		return epc;
	}
	
	@DisplayName("EPI eficaz")
	public Boolean getEpi() {
		return epi;
	}
	
	@DisplayName("Epi")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdepi")
	public Epi getMaterialepi() {
		return materialepi;
	}
	
	public void setMaterialepi(Epi materialepi) {
		this.materialepi = materialepi;
	}
	
	public void setEpc(Boolean epc) {
		this.epc = epc;
	}
	
	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	
	public void setRiscotrabalho(Riscotrabalho riscotrabalho) {
		this.riscotrabalho = riscotrabalho;
	}

	public void setExameresponsavel(Exameresponsavel exameresponsavel) {
		this.exameresponsavel = exameresponsavel;
	}

	public void setCargoriscointensidade(Cargoriscointensidade cargoriscointensidade) {
		this.cargoriscointensidade = cargoriscointensidade;
	}

	public void setCargoriscotecnica(Cargoriscotecnica cargoriscotecnica) {
		this.cargoriscotecnica = cargoriscotecnica;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setCdcolaboradorcargorisco(Integer cdcolaboradorcargorisco) {
		this.cdcolaboradorcargorisco = cdcolaboradorcargorisco;
	}
	
	public void setColaboradorcargo(Colaboradorcargo colaboradorcargo) {
		this.colaboradorcargo = colaboradorcargo;
	}
	
}
