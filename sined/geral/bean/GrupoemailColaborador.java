package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_grupoemailcolaborador", sequenceName="sq_grupoemailcolaborador")
public class GrupoemailColaborador implements Log{

	protected Integer cdgrupoemailcolaborador;
	protected Grupoemail grupoemail;
	protected Colaborador colaborador;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public GrupoemailColaborador() {}
	public GrupoemailColaborador(Grupoemail grupoemail, Colaborador colaborador) {
		this.grupoemail = grupoemail;
		this.colaborador = colaborador;
	}
	
	@Id
	@GeneratedValue(generator="sq_grupoemailcolaborador", strategy=GenerationType.AUTO)
	public Integer getCdgrupoemailcolaborador() {
		return cdgrupoemailcolaborador;
	}
	@JoinColumn(name="cdgrupoemail")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Grupoemail getGrupoemail() {
		return grupoemail;
	}
	@JoinColumn(name="cdcolaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdgrupoemailcolaborador(Integer cdgrupoemailcolaborador) {
		this.cdgrupoemailcolaborador = cdgrupoemailcolaborador;
	}
	public void setGrupoemail(Grupoemail grupoemail) {
		this.grupoemail = grupoemail;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GrupoemailColaborador) {
			GrupoemailColaborador gec = (GrupoemailColaborador) obj;
			return equalsColaborador(gec.colaborador) && equalsGrupoemail(gec.grupoemail);
		}
		return super.equals(obj);
	}
	
	protected boolean equalsColaborador(Colaborador colaborador){
		if(colaborador != null && this.colaborador != null){
			return colaborador.equals(this.colaborador);
		}
		return true;
	}
	
	protected boolean equalsGrupoemail(Grupoemail grupoemail){
		if(grupoemail != null && this.grupoemail != null){
			return grupoemail.equals(this.grupoemail);
		}
		return true;
	}
}
