package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_materialdevolucaohistorico", sequenceName = "sq_materialdevolucaohistorico")
public class Materialdevolucaohistorico {

	protected Integer cdmaterialdevolucaohistorico;
	protected Materialdevolucao materialdevolucao;
	protected String acao;
	protected String observacao;	
	protected Usuario usuario;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialdevolucaohistorico")
	public Integer getCdmaterialdevolucaohistorico() {
		return cdmaterialdevolucaohistorico;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialdevolucao")
	public Materialdevolucao getMaterialdevolucao() {
		return materialdevolucao;
	}
	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@JoinColumn(name="cdusuario")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Respons�vel")
	public Usuario getUsuario() {
		return usuario;
	}
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	
	public void setCdmaterialdevolucaohistorico(Integer cdmaterialdevolucaohistorico) {
		this.cdmaterialdevolucaohistorico = cdmaterialdevolucaohistorico;
	}
	public void setMaterialdevolucao(Materialdevolucao materialdevolucao) {
		this.materialdevolucao = materialdevolucao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
