package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipovalorreceber;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@SequenceGenerator(name = "sq_colaboradordespesa", sequenceName = "sq_colaboradordespesa")
@DisplayName("Despesa")
@Entity
public class Colaboradordespesa implements Log{

	protected Integer cdcolaboradordespesa;
	protected Empresa empresa;
	protected Date dtcancelado;
	protected Date dtinsercao;
	protected Date dtdeposito;
	protected Colaborador colaborador;
	protected Colaboradordespesasituacao situacao;
	protected Money valor;	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected List<Colaboradordespesaitem> listaColaboradordespesaitem;
	protected List<Colaboradordespesahistorico> listaColaboradordespesahistorico;
	protected Colaboradordespesamotivo colaboradordespesamotivo;
	protected Money total;
	protected Tipovalorreceber tipovalorreceber;
	protected Boolean bloqueardebitocredito;
	protected Projeto projeto;
	protected Date dtholeriteinicio;
	protected Date dtholeritefim;
	protected Rateio rateio;
	
//	TRANSIENTE
	protected Integer cdarquivofolha;
	protected String whereIn;
	protected Documentotipo documentotipo;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected List<Documento> contas = new ListSet<Documento>(Documento.class);
	protected Rateiomodelo rateiomodelo;
	protected Money valorRateio;
	protected String idTela;
	protected Date dtVencimentoTrans;
	
	
	@DisplayName ("Total")
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	
	@Required
	@DisplayName ("Tipo de remunera��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordespesamotivo")
	public Colaboradordespesamotivo getColaboradordespesamotivo() {
		return colaboradordespesamotivo;
	}
	
	@DisplayName ("Valor a receber")
	public Tipovalorreceber getTipovalorreceber() {
		return tipovalorreceber;
	}

	@DisplayName ("Hist�rico")
	@OneToMany (mappedBy="colaboradordespesa")
	public List<Colaboradordespesahistorico> getListaColaboradordespesahistorico() {
		return listaColaboradordespesahistorico;
	}

	@DisplayName ("Eventos")
	@OneToMany (mappedBy="colaboradordespesa")
	public List<Colaboradordespesaitem> getListaColaboradordespesaitem() {
		return listaColaboradordespesaitem;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradordespesa")
	public Integer getCdcolaboradordespesa() {
		return cdcolaboradordespesa;
	}	
	
	public Date getDtcancelado() {
		return dtcancelado;
	}
	
	@Required
	@DisplayName("Data de refer�ncia")
	public Date getDtinsercao() {
		return dtinsercao;
	}
	
	
	@DisplayName("Data de pagamento")
	public Date getDtdeposito() {
		return dtdeposito;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@DisplayName("Situa��o")
	public Colaboradordespesasituacao getSituacao() {
		return situacao;
	}	
	
	@DisplayName ("Valor")
	public Money getValor() {
		return valor;
	}	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}	
	
	public Boolean getBloqueardebitocredito() {
		return bloqueardebitocredito;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Date getDtholeriteinicio() {
		return dtholeriteinicio;
	}
	
	public Date getDtholeritefim() {
		return dtholeritefim;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrateio")
	public Rateio getRateio() {
		return rateio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Transient
	public String getIdTela() {
		return idTela;
	}
	
	@Transient
	public Date getDtVencimentoTrans() {
		return dtVencimentoTrans;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setDtholeriteinicio(Date dtholeriteinicio) {
		this.dtholeriteinicio = dtholeriteinicio;
	}
	
	public void setDtholeritefim(Date dtholeritefim) {
		this.dtholeritefim = dtholeritefim;
	}
	
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setBloqueardebitocredito(Boolean bloqueardebitocredito) {
		this.bloqueardebitocredito = bloqueardebitocredito;
	}	
	
	public void setCdcolaboradordespesa(Integer cdcolaboradordespesa) {
		this.cdcolaboradordespesa = cdcolaboradordespesa;
	}	
	
	public void setDtcancelado(Date dtcancelado) {
		this.dtcancelado = dtcancelado;
	}
	
	public void setDtinsercao(Date dtinsercao) {
		this.dtinsercao = dtinsercao;
	}
	
	public void setDtdeposito(Date dtdeposito) {
		this.dtdeposito = dtdeposito;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}	
	
	public void setSituacao(Colaboradordespesasituacao situacao) {
		this.situacao = situacao;
	}
	
	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setListaColaboradordespesaitem(
			List<Colaboradordespesaitem> listaColaboradordespesaitem) {
		this.listaColaboradordespesaitem = listaColaboradordespesaitem;
	}
	

	public void setListaColaboradordespesahistorico(
			List<Colaboradordespesahistorico> listaColaboradordespesahistorico) {
		this.listaColaboradordespesahistorico = listaColaboradordespesahistorico;
	}
	

	public void setColaboradordespesamotivo(
			Colaboradordespesamotivo colaboradordespesamotivo) {
		this.colaboradordespesamotivo = colaboradordespesamotivo;
	}
	
	public void setTipovalorreceber(Tipovalorreceber tipovalorreceber) {
		this.tipovalorreceber = tipovalorreceber;
	}
	
//	TRANSIENTE
	@Transient
	public Integer getCdarquivofolha() {
		return cdarquivofolha;
	}
	
	public void setCdarquivofolha(Integer cdarquivofolha) {
		this.cdarquivofolha = cdarquivofolha;
	}
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	@Transient
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@Transient
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Transient
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@Transient
	public List<Documento> getContas() {
		return contas;
	}
	@Transient
	public String getPeriodoholerite(){
		return SinedUtil.getDescricaoPeriodo(dtholeriteinicio, dtholeritefim);
	}
	@Transient
	public Rateiomodelo getRateiomodelo() {
		return rateiomodelo;
	}
	@Transient
	@DisplayName("Valor de rateio")
	public Money getValorRateio() {
		if(this.total != null){
			valorRateio = new Money(Math.abs(total.getValue().doubleValue()));
		}
		return valorRateio;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn; 
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setContas(List<Documento> contas) {
		this.contas = contas;
	}
	public void setRateiomodelo(Rateiomodelo rateiomodelo) {
		this.rateiomodelo = rateiomodelo;
	}
	public void setValorRateio(Money valorRateio) {
		this.valorRateio = valorRateio;
	}
	public void setIdTela(String idTela) {
		this.idTela = idTela;
	}
	public void setDtVencimentoTrans(Date dtVencimentoTrans) {
		if(dtVencimentoTrans != null){
			dtVencimentoTrans = new Date(SinedDateUtils.javaSqlDateToCalendar(dtVencimentoTrans).getTime().getTime());
		}
		
		this.dtVencimentoTrans = dtVencimentoTrans;
	}
	
	
}
