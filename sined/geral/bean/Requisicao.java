package br.com.linkcom.sined.geral.bean;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@DisplayName("Ordem de Servi�o")
@SequenceGenerator(name="sq_requisicao", sequenceName="sq_requisicao")
@JoinEmpresa("requisicao.empresa")
public class Requisicao implements Log, PermissaoProjeto, PermissaoClienteEmpresa{

	protected Integer cdrequisicao;
	protected Empresa empresa;
	protected Contrato contrato;
	protected Projeto projeto;
	protected Atividadetipo atividadetipo;
	protected Date dtrequisicao = new Date(System.currentTimeMillis());
	protected Date dtprevisao;
	protected Date dtconclusao;
	protected Requisicaoestado requisicaoestado;
	protected Requisicaoprioridade requisicaoprioridade;
	protected String descricao;
	protected String observacao;
	protected Contato contato;
	protected Colaborador colaboradorresponsavel;
	protected Cliente cliente;
	protected Endereco endereco;
	protected String ordemservico;
	protected Meiocontato meiocontato;
	protected Timestamp dthorainicio;
	protected Timestamp dthorafim;
	protected String versaocliente;
	
	protected Contratomaterial contratomaterial;
	protected Requisicao requisicaorelacionada;
	protected Boolean faturar;
	protected Double qtde;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	protected List<Requisicaohistorico> listaRequisicaohistorico = new ListSet<Requisicaohistorico>(Requisicaohistorico.class);
	protected List<Materialrequisicao> listaMateriaisrequisicao = new ListSet<Materialrequisicao>(Materialrequisicao.class);
	protected Set<Requisicaoitem> listaItem = new ListSet<Requisicaoitem>(Requisicaoitem.class);
	protected List<Apontamento> listaApontamento2;
		
	protected String situacao;
	protected String materiais;
	protected String historicos;
	protected Venda venda;
	protected Pedidovenda pedidovenda;
	protected NotaFiscalServico notaFiscalServico;
	protected Boolean observacaoInterna;
	protected Classificacao classificacao;
	protected Agendainteracao agendainteracao;
	
	//TRANSIENT
	protected Integer materialrequisicaoCdrequisicao;
	protected Date materialrequisicaoDtdisponibilizacao;
	protected Date materialrequisicaoDtretirada;
	protected Integer materialrequisicaoQuantidade;
	protected Money materialrequisicaoValortotal;
	protected String materialrequisicaoObs;
	protected Integer materialrequisicaoQtdeIteracao;
	protected Integer materialCdmaterial;
	protected String materialNome;
	protected Integer cdtarefa;
	protected String cdstarefa;
	protected Endereco enderecoEmpresa;
	protected Image logo;
	protected Arquivo arquivohistorico;
	protected List<Apontamento> listaApontamento;
	protected String requisicaoRelacionadaStr;
	protected String horasTrabalhadas;
	protected Localarmazenagem localarmazenagem;
	protected String mensagemAceiteCliente;
	protected Boolean pendenciafinanceira = Boolean.FALSE;
	protected List<Requisicao> listaRequisicoesFaturarPorAutorizacao;
	protected Date vencimento;
	protected String nomeprojeto;
	protected Integer cdcontratotrans;
	protected String informacoesClienteOS;
	
	public Requisicao(){}
	
	public Requisicao(Integer cdrequisicao){
		this.cdrequisicao = cdrequisicao;
	}

	public Requisicao(Integer cdrequisicao, java.sql.Date dtrequisicao, String descricao, Double qtde, Integer cdrequisicaoprioridade, String descricaoprioridade, 
					 Integer cdrequisicaoestado, String descricaoestado, Integer cdpessoa, String nome, Integer cdcontrato, String descricaocontrato, 
					 Integer cdresponsavel, String nomeresponsavel, String situacao){
		this.cdrequisicao = cdrequisicao;
		this.dtrequisicao = dtrequisicao;
		this.descricao = descricao;
		this.qtde = qtde;
		this.requisicaoprioridade = new Requisicaoprioridade(cdrequisicaoprioridade, descricaoprioridade);
		this.requisicaoestado = new Requisicaoestado(cdrequisicaoestado, descricaoestado);
		this.cliente = new Cliente(cdpessoa, nome);
		this.contrato = new Contrato(cdcontrato, descricaocontrato);
		this.situacao = situacao;
		this.colaboradorresponsavel = new Colaborador(cdresponsavel, nomeresponsavel);
	}
	
	public Requisicao(Integer cdrequisicao, java.sql.Date dtrequisicao, String descricao, Double qtde, Integer cdrequisicaoprioridade, String descricaoprioridade, 
				 Integer cdrequisicaoestado, String descricaoestado, Integer cdpessoa, String nome, Integer cdcontrato, String descricaocontrato, 
				 Integer cdresponsavel, String nomeresponsavel, String situacao, Integer materialrequisicaoCdrequisicao, 
				 java.sql.Date  materialrequisicaoDtdisponibilizacao, java.sql.Date materialrequisicaoDtretirada, Integer materialrequisicaoQuantidade, 
				 Double materialrequisicaoValortotal, Integer materialCdmaterial, String materialNome, String atividadetipoNome, String ordemservico,
				 String nomeprojeto, Integer cdprojeto){
		this.cdrequisicao = cdrequisicao;
		this.dtrequisicao = dtrequisicao;
		this.descricao = descricao;
		this.qtde = qtde;
		this.requisicaoprioridade = new Requisicaoprioridade(cdrequisicaoprioridade, descricaoprioridade);
		this.requisicaoestado = new Requisicaoestado(cdrequisicaoestado, descricaoestado);
		this.cliente = new Cliente(cdpessoa, nome);
		this.contrato = new Contrato(cdcontrato, descricaocontrato);
		this.situacao = situacao;
		this.colaboradorresponsavel = new Colaborador(cdresponsavel, nomeresponsavel);
		this.materialrequisicaoCdrequisicao = materialrequisicaoCdrequisicao;
		this.materialrequisicaoDtdisponibilizacao = materialrequisicaoDtdisponibilizacao;
		this.materialrequisicaoDtretirada = materialrequisicaoDtretirada;
		this.materialrequisicaoQuantidade = materialrequisicaoQuantidade;
		this.materialrequisicaoValortotal = new Money(materialrequisicaoValortotal);
		this.materialCdmaterial = materialCdmaterial;
		this.materialNome = materialNome;
		if (atividadetipoNome!=null){
			this.atividadetipo = new Atividadetipo(atividadetipoNome);
		}
		this.ordemservico = ordemservico;
		this.projeto = new Projeto(cdprojeto);
		this.projeto.setNome(nomeprojeto);
	}
	
	
	@Id
	@Required
	@GeneratedValue(generator="sq_requisicao", strategy=GenerationType.AUTO)
	@DisplayName("C�digo")
	public Integer getCdrequisicao() {
		return cdrequisicao;
	}

	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@JoinColumn(name="cdprojeto")
	@ManyToOne(fetch=FetchType.LAZY)	
	public Projeto getProjeto() {
		return projeto;
	}

	@DisplayName("Tipo")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}

	@Required
	@DisplayName("Data OS")
	public Date getDtrequisicao() {
		return dtrequisicao;
	}
	
	@DisplayName("Previs�o")
	public Date getDtprevisao() {
		return dtprevisao;
	}

	@Required
	@JoinColumn(name="cdrequisicaoprioridade")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Prioridade")
	public Requisicaoprioridade getRequisicaoprioridade() {
		return requisicaoprioridade;
	}
	
	@Required
	@JoinColumn(name="cdrequisicaoestado")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Situa��o")
	public Requisicaoestado getRequisicaoestado() {
		return requisicaoestado;
	}

	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Observa��o")
	@Transient
	public String getObservacao() {
		return observacao;
	}

	@JoinColumn(name="cdcontato")
	@ManyToOne(fetch=FetchType.LAZY)	
	public Contato getContato() {
		return contato;
	}

	@Required
	@JoinColumn(name="cdcolaboradorresponsavel")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Respons�vel")
	public Colaborador getColaboradorresponsavel() {
		return colaboradorresponsavel;
	}
	
	@DisplayName("Ordem de servi�o")
	@MaxLength(50)
	public String getOrdemservico() {
		return ordemservico;
	}

	@DisplayName("Servi�o")
	@JoinColumn(name="cdcontratomaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contratomaterial getContratomaterial() {
		return contratomaterial;
	}
	
	@DisplayName("OS relacionada")
	@JoinColumn(name="cdrequisicaorelacionada")
	@ManyToOne(fetch=FetchType.LAZY)
	public Requisicao getRequisicaorelacionada() {
		return requisicaorelacionada;
	}
	
	public Boolean getFaturar() {
		return faturar;
	}
	
	@MaxLength(9)
	@DisplayName("Horas trabalhadas")
	public Double getQtde() {
		return qtde;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="requisicao")
	public List<Requisicaohistorico> getListaRequisicaohistorico() {
		return listaRequisicaohistorico;
	}
	
	@DisplayName("Materiais e Servi�os")
	@OneToMany(mappedBy="requisicao")
	public List<Materialrequisicao> getListaMateriaisrequisicao() {
		return listaMateriaisrequisicao;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="requisicao")
	public Set<Requisicaoitem> getListaItem() {
		return listaItem;
	}
	
	@DisplayName("Endere�o")	
	@JoinColumn(name="cdendereco")
	@ManyToOne(fetch=FetchType.LAZY)
	public Endereco getEndereco() {
		return endereco; 
	}
	
	@Transient
	public Boolean getPendenciafinanceira() {
		return pendenciafinanceira;
	}
	
	@JoinColumn(name="cdnota")
	@ManyToOne(fetch=FetchType.LAZY)
	public NotaFiscalServico getNotaFiscalServico() {
		return notaFiscalServico;
	}
	
	@DisplayName("Considerar como observa��o interna")	
	public Boolean getObservacaoInterna() {
		return observacaoInterna;
	}
	
	@DisplayName("Classifica��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclassificacao")
	public Classificacao getClassificacao() {
		return classificacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendainteracao")
	public Agendainteracao getAgendainteracao() {
		return agendainteracao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}

	public void setObservacaoInterna(Boolean observacaoInterna) {
		this.observacaoInterna = observacaoInterna;
	}

	public void setNotaFiscalServico(NotaFiscalServico notaFiscalServico) {
		this.notaFiscalServico = notaFiscalServico;
	}

	public void setPendenciafinanceira(Boolean pendenciafinanceira) {
		this.pendenciafinanceira = pendenciafinanceira;
	}

	public void setListaMateriaisrequisicao(List<Materialrequisicao> listaMateriaisrequisicao) {
		this.listaMateriaisrequisicao = listaMateriaisrequisicao;
	}
	
	public void setListaItem(Set<Requisicaoitem> listaItem) {
		this.listaItem = listaItem;
	}
	
	public void setListaRequisicaohistorico(
			List<Requisicaohistorico> listaRequisicaohistorico) {
		this.listaRequisicaohistorico = listaRequisicaohistorico;
	}
	
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setFaturar(Boolean faturar) {
		this.faturar = faturar;
	}
	
	public void setRequisicaorelacionada(Requisicao requisicaorelacionada) {
		this.requisicaorelacionada = requisicaorelacionada;
	}
	
	public void setContratomaterial(Contratomaterial contratomaterial) {
		this.contratomaterial = contratomaterial;
	}

	public void setCdrequisicao(Integer cdrequisicao) {
		this.cdrequisicao = cdrequisicao;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	public void setDtprevisao(Date dtprevisao) {
		this.dtprevisao = dtprevisao;
	}
	
	public void setOrdemservico(String ordemservico) {
		this.ordemservico = StringUtils.trimToNull(ordemservico);
	}
	
	public void setDtrequisicao(Date dtrequisicao) {
		this.dtrequisicao = dtrequisicao;
	}

	public void setRequisicaoprioridade(Requisicaoprioridade requisicaoprioridade) {
		this.requisicaoprioridade = requisicaoprioridade;
	}
	
	public void setRequisicaoestado(Requisicaoestado requisicaoestado) {
		this.requisicaoestado = requisicaoestado;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public void setColaboradorresponsavel(Colaborador colaboradorresponsavel) {
		this.colaboradorresponsavel = colaboradorresponsavel;
	}
	public void setAgendainteracao(Agendainteracao agendainteracao) {
		this.agendainteracao = agendainteracao;
	}

	@Required
	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		/*if(contrato != null && contrato.getCliente() != null)
			return contrato.getCliente();*/
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setEndereco(Endereco endereco){
		this.endereco = endereco;
	}	

	@Transient
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@Transient
	@DisplayName("Cliente")
	public String getDescricaoListagem(){
		String string = "";
		if(getCliente() != null && getCliente().getNome() != null){
			string += "<b>" + getCliente().getNome() + "</b>";
		}
		if(string.equals("") && getContrato() != null && getContrato().getCliente() != null && getContrato().getCliente().getNome() != null){
			string += "<b>" + getContrato().getCliente().getNome() + "</b>";
		}
		if(getContrato() != null && getContrato().getDescricao() != null){
			string += "<BR>" + getContrato().getDescricao();
		}
		return string;
	}
	
	@Transient
	public String getMateriais() {
		StringBuilder sb = new StringBuilder("");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		if(this.listaMateriaisrequisicao != null && !this.listaMateriaisrequisicao.isEmpty()){
			for (Materialrequisicao materialrequisicao : this.listaMateriaisrequisicao) {
				if(materialrequisicao.getMaterial() != null && materialrequisicao.getMaterial().getCdmaterial() != null){
					sb.append(materialrequisicao.getMaterial().getNome());
					if(materialrequisicao.getDtdisponibilizacao() != null)
						sb.append(" - Data Dispon.: ").append(dateFormat.format(materialrequisicao.getDtdisponibilizacao()));
					if(materialrequisicao.getDtretirada() != null)
						sb.append("  Data Retirada: ").append(dateFormat.format(materialrequisicao.getDtretirada()));
					if(materialrequisicao.getQuantidade() != null && materialrequisicao.getQuantidade() > 0 && materialrequisicao.getValortotal() != null){
						sb.append("  Valor unit�rio: R$" + new Money(materialrequisicao.getValortotal()/materialrequisicao.getQuantidade()));
						sb.append("  Valor total: R$" + new Money(materialrequisicao.getValortotal()));
					}
					sb.append("\n");
				}
			}
			if(sb.toString().length() > 1)
				sb.delete(sb.length()-1, sb.length());
		}
		return sb.toString();
	}
	
	public void setMateriais(String materiais) {
		this.materiais = materiais;
	}
	
	@Transient
	public String getHistoricos() {
		StringBuilder sb = new StringBuilder("");
		if(this.listaRequisicaohistorico != null && !this.listaRequisicaohistorico.isEmpty()){
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			for (Requisicaohistorico requisicaohistorico : this.listaRequisicaohistorico) {
				if (requisicaohistorico.getDtaltera()!=null){
					sb.append(dateFormat.format(requisicaohistorico.getDtaltera())).append(" - ");
				}
				if (requisicaohistorico.getPessoa()!=null){
					sb.append(requisicaohistorico.getPessoa().getNome());
				}
				sb.append(requisicaohistorico.getObservacao() != null ? " -\n"+requisicaohistorico.getObservacao() : "").append("\n");
			}
		}
		return sb.toString();
	}
	
	public void setHistoricos(String historicos) {
		this.historicos = historicos;
	}
	
	@DisplayName("OS")
	@Transient
	public String getOs(){
		if(getOrdemservico() != null && !getOrdemservico().equals("")){
			return getOrdemservico();
		} else return getCdrequisicao().toString();
	}

	@Transient
	public Integer getMaterialrequisicaoCdrequisicao() {
		return materialrequisicaoCdrequisicao;
	}
	@Transient
	public Date getMaterialrequisicaoDtdisponibilizacao() {
		return materialrequisicaoDtdisponibilizacao;
	}
	@Transient
	public Date getMaterialrequisicaoDtretirada() {
		return materialrequisicaoDtretirada;
	}
	@Transient
	public Integer getMaterialrequisicaoQuantidade() {
		return materialrequisicaoQuantidade;
	}
	@Transient
	public Money getMaterialrequisicaoValortotal() {
		return materialrequisicaoValortotal;
	}
	@Transient
	public String getMaterialrequisicaoObs() {
		return materialrequisicaoObs;
	}
	@Transient
	public Integer getMaterialrequisicaoQtdeIteracao() {
		return materialrequisicaoQtdeIteracao;
	}

	@Transient
	public Integer getMaterialCdmaterial() {
		return materialCdmaterial;
	}
	@Transient
	public String getMaterialNome() {
		return materialNome;
	}

	public void setMaterialrequisicaoCdrequisicao(
			Integer materialrequisicaoCdrequisicao) {
		this.materialrequisicaoCdrequisicao = materialrequisicaoCdrequisicao;
	}

	public void setMaterialrequisicaoDtretirada(
			Date materialrequisicaoDtretirada) {
		this.materialrequisicaoDtretirada = materialrequisicaoDtretirada;
	}

	public void setMaterialrequisicaoQuantidade(Integer materialrequisicaoQuantidade) {
		this.materialrequisicaoQuantidade = materialrequisicaoQuantidade;
	}

	public void setMaterialrequisicaoValortotal(Money materialrequisicaoValortotal) {
		this.materialrequisicaoValortotal = materialrequisicaoValortotal;
	}
	
	public void setMaterialrequisicaoObs(String materialrequisicaoObs) {
		this.materialrequisicaoObs = materialrequisicaoObs;
	}
	
	public void setMaterialrequisicaoQtdeIteracao(Integer materialrequisicaoQtdeIteracao) {
		this.materialrequisicaoQtdeIteracao = materialrequisicaoQtdeIteracao;
	}

	public void setMaterialCdmaterial(Integer materialCdmaterial) {
		this.materialCdmaterial = materialCdmaterial;
	}

	public void setMaterialNome(String materialNome) {
		this.materialNome = materialNome;
	}	

	public void setMaterialrequisicaoDtdisponibilizacao(Date materialrequisicaoDtdisponibilizacao) {
		this.materialrequisicaoDtdisponibilizacao = materialrequisicaoDtdisponibilizacao;
	}
	
	@Transient
	public Integer getCdtarefa() {
		return cdtarefa;
	}
	public void setCdtarefa(Integer cdtarefa) {
		this.cdtarefa = cdtarefa;
	}
	
	@Transient
	public String getCdstarefa() {
		return cdstarefa;
	}

	public void setCdstarefa(String cdstarefa) {
		this.cdstarefa = cdstarefa;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"requisicao.cdrequisicao, requisicao.ordemservico, requisicao.dtrequisicao"})	
	public String getDescription(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String id = this.ordemservico != null && !this.ordemservico.trim().equals("") ? this.ordemservico : this.cdrequisicao + "";
		return id + (this.dtrequisicao != null ? " (" + format.format(this.dtrequisicao) + ")" : "");
	}
	
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public Endereco getEnderecoEmpresa() {
		return enderecoEmpresa;
	}
	public void setEnderecoEmpresa(Endereco enderecoEmpresa) {
		this.enderecoEmpresa = enderecoEmpresa;
	}
	@Transient
	public Image getLogo() {
		return logo;
	}
	public void setLogo(Image logo) {
		this.logo = logo;
	}

	@JoinColumn(name="cdvenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	@JoinColumn(name="cdpedidovenda")
	@ManyToOne(fetch=FetchType.LAZY)
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	@Transient
	public Arquivo getArquivohistorico() {
		return arquivohistorico;
	}
	
	public void setArquivohistorico(Arquivo arquivohistorico) {
		this.arquivohistorico = arquivohistorico;
	}

	@Transient
	@DisplayName("Atividade")
	public List<Apontamento> getListaApontamento() {
		return listaApontamento;
	}

	public void setListaApontamento(List<Apontamento> listaApontamento) {
		this.listaApontamento = listaApontamento;
	}

	@Transient
	public String getRequisicaoRelacionadaStr() {
		requisicaoRelacionadaStr = "OS: " + cdrequisicao.toString();
		if(descricao != null && !descricao.equals(""))
			requisicaoRelacionadaStr += "<span><BR>Descri��o: " + descricao.replaceAll("\n", " ").replaceAll("\"", "''") + "</span>";
		return requisicaoRelacionadaStr;
	}
	
	@Transient
	public String getHorasTrabalhadas() {
		if(qtde == null)
			return "00:00";
		else {
			return SinedDateUtils.doubleToFormatoHora(qtde);
		}
	}
	
	@Transient
	public Money getValorTotalMateriaisServicos(){
		Double valorTotal = 0.00;
		if(this.listaMateriaisrequisicao != null && !this.listaMateriaisrequisicao.isEmpty()){
			for(Materialrequisicao item : this.listaMateriaisrequisicao){
				if(item.getValortotal() != null){
					valorTotal += item.getValortotal();
				}
			}
		}
		return new Money(valorTotal, false);
	}

	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public String subQueryProjeto() {
		return "select requisicaosubQueryProjeto.cdrequisicao " +
				"from Requisicao requisicaosubQueryProjeto " +
				"left outer join requisicaosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select requisicaoSubQueryClienteEmpresa.cdrequisicao " +
				"from Requisicao requisicaoSubQueryClienteEmpresa " +
				"left outer join requisicaoSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public String getMensagemAceiteCliente() {
		return mensagemAceiteCliente;
	}

	public void setMensagemAceiteCliente(String mensagemAceiteCliente) {
		this.mensagemAceiteCliente = mensagemAceiteCliente;
	}

	@DisplayName("Data de Conclus�o")
	public Date getDtconclusao() {
		return dtconclusao;
	}
	
	public void setDtconclusao(Date dtconclusao) {
		this.dtconclusao = dtconclusao;
	}
	
	@Transient
	public String getAutocompleteDescription(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String id = this.ordemservico != null && !this.ordemservico.trim().equals("") ? this.ordemservico : this.cdrequisicao + "";
		return id + (this.dtrequisicao != null ? " (" + format.format(this.dtrequisicao) + ")" : "");
	}
	
	@OneToMany(mappedBy="requisicao")
	public List<Apontamento> getListaApontamento2() {
		return listaApontamento2;
	}
	
	public void setListaApontamento2(List<Apontamento> listaApontamento2) {
		this.listaApontamento2 = listaApontamento2;
	}

	@Transient
	public List<Requisicao> getListaRequisicoesFaturarPorAutorizacao() {
		return listaRequisicoesFaturarPorAutorizacao;
	}

	public void setListaRequisicoesFaturarPorAutorizacao(List<Requisicao> listaRequisicoesFaturarPorAutorizacao) {
		this.listaRequisicoesFaturarPorAutorizacao = listaRequisicoesFaturarPorAutorizacao;
	}
	
	@Transient
	public Date getVencimento() {
		return vencimento;
	}
	
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	
	@DisplayName("Meio de contato")
	@JoinColumn(name="cdmeiocontato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Meiocontato getMeiocontato() {
		return meiocontato;
	}
	@DisplayName("Data/Hora In�cio")
	public Timestamp getDthorainicio() {
		return dthorainicio;
	}
	@DisplayName("Data/Hora Fim")
	public Timestamp getDthorafim() {
		return dthorafim;
	}

	public void setMeiocontato(Meiocontato meiocontato) {
		this.meiocontato = meiocontato;
	}
	public void setDthorainicio(Timestamp dthorainicio) {
		this.dthorainicio = dthorainicio;
	}
	public void setDthorafim(Timestamp dthorafim) {
		this.dthorafim = dthorafim;
	}
	
	@DisplayName("Vers�o W3erp")
	@Column(insertable=false, updatable=false)
	public String getVersaocliente() {
		return versaocliente;
	}
	
	public void setVersaocliente(String versaocliente) {
		this.versaocliente = versaocliente;
	}
	
	@Transient
	public String getNomeprojeto() {
		return nomeprojeto;
	}
	public void setNomeprojeto(String nomeprojeto) {
		this.nomeprojeto = nomeprojeto;
	}
	
	@Transient
	public Integer getCdcontratotrans() {
		return cdcontratotrans;
	}
	public void setCdcontratotrans(Integer cdcontratotrans) {
		this.cdcontratotrans = cdcontratotrans;
	}
	
	@Transient
	@DisplayName("Informa��es do cliente")
	public String getInformacoesClienteOS() {
		return informacoesClienteOS;
	}
	public void setInformacoesClienteOS(String informacoesClienteOS) {
		this.informacoesClienteOS = informacoesClienteOS;
	}
}