package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@DisplayName("Rela��o de depend�ncia entre cargos")
@SequenceGenerator(name = "sq_dependenciacargo", sequenceName = "sq_dependenciacargo")
public class Dependenciacargo {
	
	protected Integer cddependenciacargo;
	protected Relacaocargo relacaocargo;
	protected Cargo cargo;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dependenciacargo")	
	public Integer getCddependenciacargo() {
		return cddependenciacargo;
	}
	
	@DisplayName("Rela��o entre cargos")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdrelacaocargo")
	@Required	
	public Relacaocargo getRelacaocargo() {
		return relacaocargo;
	}
	
	@DisplayName("Cargo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required	
	public Cargo getCargo() {
		return cargo;
	}
	
	public void setCddependenciacargo(Integer cddependenciacargo) {
		this.cddependenciacargo = cddependenciacargo;
	}
	
	public void setRelacaocargo(Relacaocargo relacaocargo) {
		this.relacaocargo = relacaocargo;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
