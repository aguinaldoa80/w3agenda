package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Colaboradorformapagamento{

	public static final Integer DINHEIRO=1;
	public static final Integer CHEQUE=2;
	public static final Integer DEPOSITO_CONTA_CORRENTE=3;
	public static final Integer DEPOSITO_CONTA_POUPANCA=4;
	public static final Integer CARTAO_SALARIO=5;
	
	public Colaboradorformapagamento(){}
	public Colaboradorformapagamento(Integer cdcolaboradorformapagamento){
		this.cdcolaboradorformapagamento = cdcolaboradorformapagamento;
	}
	
	protected Integer cdcolaboradorformapagamento;
	protected String nome;
	
	@Id
	public Integer getCdcolaboradorformapagamento() {
		return cdcolaboradorformapagamento;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setCdcolaboradorformapagamento(Integer cdcolaboradorformapagamento) {
		this.cdcolaboradorformapagamento = cdcolaboradorformapagamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
