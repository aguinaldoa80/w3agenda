package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_conferenciamaterialexpedicao", sequenceName="sq_conferenciamaterialexpedicao")
public class ConferenciaMaterialExpedicao {

	private Integer cdConferenciaMaterialExpedicao;
	private Material material;
	private Unidademedida unidadeMedida;
	private Expedicao expedicao;
	private Expedicaoitem expedicaoitem;
	private Double qtdeVenda;
	private Double qtdeConferida;
	private Vendamaterial vendaMaterial;
	private Expedicaohistorico expedicaoHistorico;
	
	
	@Id
	@GeneratedValue(generator="sq_conferenciamaterialexpedicao", strategy=GenerationType.AUTO)
	public Integer getCdConferenciaMaterialExpedicao() {
		return cdConferenciaMaterialExpedicao;
	}
	public void setCdConferenciaMaterialExpedicao(Integer cdConferenciaMaterialExpedicao) {
		this.cdConferenciaMaterialExpedicao = cdConferenciaMaterialExpedicao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(Unidademedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicaoitem")
	public Expedicaoitem getExpedicaoitem() {
		return expedicaoitem;
	}
	public void setExpedicaoitem(Expedicaoitem expedicaoitem) {
		this.expedicaoitem = expedicaoitem;
	}
	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}
	public Double getQtdeVenda() {
		return qtdeVenda;
	}
	public void setQtdeVenda(Double qtdeVenda) {
		this.qtdeVenda = qtdeVenda;
	}
	public Double getQtdeConferida() {
		return qtdeConferida;
	}
	public void setQtdeConferida(Double qtdeConferida) {
		this.qtdeConferida = qtdeConferida;
	}
	@ManyToOne
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendaMaterial() {
		return vendaMaterial;
	}
	public void setVendaMaterial(Vendamaterial vendaMaterial) {
		this.vendaMaterial = vendaMaterial;
	}
	@ManyToOne
	@JoinColumn(name="cdexpedicaohistorico")
	public Expedicaohistorico getExpedicaoHistorico() {
		return expedicaoHistorico;
	}
	public void setExpedicaoHistorico(Expedicaohistorico expedicaohistorico) {
		this.expedicaoHistorico = expedicaohistorico;
	}
}
