package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoretornosegmento",sequenceName="sq_bancoconfiguracaoretornosegmento")
public class BancoConfiguracaoRetornoSegmento implements Log {

	private Integer cdbancoconfiguracaoretornosegmento;
	private String nome;
	private BancoConfiguracaoTipoSegmentoEnum tipo;
	private String identificador;
	private Banco banco;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;	
	private Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoretornosegmento")
	public Integer getCdbancoconfiguracaoretornosegmento() {
		return cdbancoconfiguracaoretornosegmento;
	}

	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@Required
	public BancoConfiguracaoTipoSegmentoEnum getTipo() {
		return tipo;
	}
		
	@Required
	public String getIdentificador() {
		return identificador;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	@Required
	public Banco getBanco() {
		return banco;
	}
	
	public void setCdbancoconfiguracaoretornosegmento(
			Integer cdbancoconfiguracaoretornosegmento) {
		this.cdbancoconfiguracaoretornosegmento = cdbancoconfiguracaoretornosegmento;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setTipo(BancoConfiguracaoTipoSegmentoEnum tipo) {
		this.tipo = tipo;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@OneToMany(mappedBy="bancoConfiguracaoRetornoSegmento")
	public Set<BancoConfiguracaoRetornoCampo> getListaBancoConfiguracaoRetornoCampo() {
		return listaBancoConfiguracaoRetornoCampo;
	}

	public void setListaBancoConfiguracaoRetornoCampo(
			Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo) {
		this.listaBancoConfiguracaoRetornoCampo = listaBancoConfiguracaoRetornoCampo;
	}
	
	
}
