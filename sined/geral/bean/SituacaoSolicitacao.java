package br.com.linkcom.sined.geral.bean;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum SituacaoSolicitacao {

	EM_ABERTO	(1,	"Em aberto"),
	EM_PROCESSO	(2,	"Em processo de compra"),
	BAIXADA		(3,	"Baixada"),
	CANCELADA	(4,	"Cancelada"); 
	
	private Integer value;
	private String nome;
	
	private SituacaoSolicitacao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
