package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_avisousuario", sequenceName = "sq_avisousuario")
@DisplayName("Aviso usu�rio")
public class Avisousuario implements Log {

	protected Integer cdavisousuario;
	protected Aviso aviso;
	protected Usuario usuario;
	protected Date dtnotificacao;
	protected Date dtleitura;
	protected Date dtEmail;
	protected Boolean preparadoEnvioEmail;
	
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	
	public Avisousuario(){
	}
	
	public Avisousuario(Aviso aviso, Usuario usuario){
		this.aviso = aviso;
		this.usuario = usuario;
		this.dtleitura = new Date(System.currentTimeMillis());
	}
	
	public Avisousuario(Aviso aviso, Usuario usuario, boolean considerarDataAtualDtLeitura){
		this.aviso = aviso;
		this.usuario = usuario;
		if(considerarDataAtualDtLeitura){
			this.dtleitura = new Date(System.currentTimeMillis());
		}
	}
	
	public Avisousuario(Integer cdavisousuario){
		this.cdavisousuario = cdavisousuario;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_avisousuario")
	@DisplayName("Aviso usu�rio")
	public Integer getCdavisousuario() {
		return cdavisousuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdaviso")
	public Aviso getAviso() {
		return aviso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@DisplayName("Usu�rio")
	public Usuario getUsuario() {
		return usuario;
	}
	@DisplayName("Notificado")
	public Date getDtnotificacao() {
		return dtnotificacao;
	}
	@DisplayName("Lido")
	public Date getDtleitura() {
		return dtleitura;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	@DisplayName("E-Mail Enviado")
	public Date getDtEmail() {
		return dtEmail;
	}
	
	public Boolean getPreparadoEnvioEmail() {
		return preparadoEnvioEmail;
	}
	
	public void setPreparadoEnvioEmail(Boolean preparadoEnvioEmail) {
		this.preparadoEnvioEmail = preparadoEnvioEmail;
	}
	
	public void setDtEmail(Date dtEmail) {
		this.dtEmail = dtEmail;
	}
	
	public void setCdavisousuario(Integer cdavisousuario) {
		this.cdavisousuario = cdavisousuario;
	}
	public void setAviso(Aviso aviso) {
		this.aviso = aviso;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setDtnotificacao(Date dtnotificacao) {
		this.dtnotificacao = dtnotificacao;
	}
	public void setDtleitura(Date dtleitura) {
		this.dtleitura = dtleitura;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}

	@Transient
	public boolean getLido(){
		return dtleitura != null;
	}
}
