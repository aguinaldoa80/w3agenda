package br.com.linkcom.sined.geral.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeBean;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeCampoadicionalBean;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeMaterialBean;
import br.com.linkcom.sined.modulo.crm.controller.process.vo.PainelInteracaoVendaInt;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.TelefoneBean;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@SequenceGenerator(name = "sq_oportunidade", sequenceName = "sq_oportunidade")
@JoinEmpresa("oportunidade.empresa")
public class Oportunidade implements Log, PainelInteracaoVendaInt, PermissaoClienteEmpresa{
	
	protected Integer cdoportunidade;
	protected Empresa empresa;
	protected Integer identificador;
	protected String nome;
	protected Tipopessoacrm tipopessoacrm;
	protected Cliente cliente;
	protected Contato contato;
	protected Boolean revendedor;
	protected Contacrm contacrm;
	protected Contacrmcontato contacrmcontato;
	protected Propostacaixa propostacaixa;
	protected Date dtInicio;
	protected Integer ciclo;
	protected Date dtPrevisaotermino;
//	protected Material material;
//	protected Money valor;
//	protected Money valorproducao;
	protected Money valortotal;
	protected Tiporesponsavel tiporesponsavel;
	protected Pessoa responsavel;
	protected Oportunidadefonte oportunidadefonte;
	protected Oportunidadesituacao oportunidadesituacao;
	protected Campanha campanha;
	protected Integer probabilidade = 0;
	protected Date dtretorno;
	protected String proximopasso;
	protected Integer qtdehistorico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Frequencia frequencia;
	
	protected Concorrente concorrente;
	protected String site;
	protected String estrategiaMarketing;
	protected String forcas;
	protected String fraquezas;
		
	protected List<Oportunidadeproposta> listaOportunidadeproposta;
	protected List<Oportunidadehistorico> listoportunidadehistorico;
	protected List<GenericBean> listaTeste = new LinkedList<GenericBean>(new ArrayList<GenericBean>()); 
	protected Set<Oportunidadeitem> listaOportunidadeitem = new ListSet<Oportunidadeitem>(Oportunidadeitem.class);
	protected Set<Oportunidadematerial> listaOportunidadematerial = new ListSet<Oportunidadematerial>(Oportunidadematerial.class);
	
//	Transients
	protected Colaborador colaborador;
	protected Fornecedor agencia;
	protected Arquivo arquivohistorico;
	protected String observacao;
	protected Integer indexlista;
	protected String showProbabilidade;
	protected Boolean redirecionaTela = false;
	protected Boolean usarSequencial = true;
	protected String whereIn;
	protected Boolean entrada;
	protected String ultimoHistorico;
	protected String materiais;
	protected Cpf cpf;
	protected Cnpj cnpj;
	protected String bairro;
	protected String logradouro;
	protected Municipio municipio;
	protected Cep cep;
	protected String numero;
	protected String complementoEndereco;
	protected Boolean permitidoEdicao;
	private Boolean alterado;
	
	public Oportunidade() {
	}
	
	public Oportunidade(Integer cdoportunidade) {
		this.cdoportunidade = cdoportunidade;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_oportunidade")
	public Integer getCdoportunidade() {
		return cdoportunidade;
	}

	@Required	
	@MaxLength(80)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Required	
	@DisplayName("In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("Ciclo")
	public Integer getCiclo() {
		return ciclo;
	}

	@DisplayName("Previs�o T�rmino")
	public Date getDtPrevisaotermino() {
		return dtPrevisaotermino;
	}
	
	@DisplayName("Tipo Respons�vel")
	public Tiporesponsavel getTiporesponsavel() {
		return tiporesponsavel;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Transient
	@DisplayName("Ag�ncia")	
	public Fornecedor getAgencia() {
		return agencia;
	}
	@Transient
	public String getBairro() {
		return bairro;
	}
	@Transient
	public Cep getCep() {
		return cep;
	}
	@Transient
	public Cnpj getCnpj() {
		return cnpj;
	}
	@Transient
	public Cpf getCpf() {
		return cpf;
	}
	@Transient
	public String getLogradouro() {
		return logradouro;
	}
	@Transient
	public Municipio getMunicipio() {
		return municipio;
	}
	@Transient
	public String getNumero() {
		return numero;
	}
	@Transient
	public String getComplementoEndereco() {
		return complementoEndereco;
	}
	
//	@DisplayName("Produto")
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="cdmaterial")
//	public Material getMaterial() {
//		return material;
//	}
			
	@DisplayName("Respons�vel")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdresponsavel")
	public Pessoa getResponsavel() {
		return responsavel;
	}

	@Required	
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidadesituacao")
	public Oportunidadesituacao getOportunidadesituacao() {
		return oportunidadesituacao;
	}

	@DisplayName("Hist�rico")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="oportunidade")
	public List<Oportunidadehistorico> getListoportunidadehistorico() {
		return listoportunidadehistorico;
	}
	
	@DisplayName ("Revis�o de Proposta")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="oportunidade")
	public List<Oportunidadeproposta> getListaOportunidadeproposta() {
		return listaOportunidadeproposta;
	}
	
	@DisplayName("Conta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrm")
	public Contacrm getContacrm() {
		return contacrm;
	}
	
	@DisplayName("Contato")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrmcontato")
	public Contacrmcontato getContacrmcontato() {
		return contacrmcontato;
	}
	
	@DisplayName("Pr�ximo Passo")
	@MaxLength(500)
	public String getProximopasso(){
		return proximopasso;
	}
	@Transient
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Periodicidade")
	@JoinColumn(name="cdfrequencia")
	@ManyToOne(fetch=FetchType.LAZY)
	public Frequencia getFrequencia() {
		return frequencia;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}
	
//	@DisplayName("Valor")
//	public Money getValor(){
//		return valor;
//	}
//	@DisplayName("Valor Produ��o")
//	public Money getValorproducao() {
//		return valorproducao;
//	}
	@DisplayName("Valor Total")
	public Money getValortotal() {
		return valortotal;
	}
	@DisplayName("Campanha")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcampanha")
	public Campanha getCampanha(){
		return campanha;
	}
	
	@Transient
	@MaxLength(5)
	@DisplayName("Prob. %")
	public String getShowProbabilidade() {
		return showProbabilidade;
	}
	@Required
	@MaxLength(5)
	@DisplayName("Prob. Sucesso")
	public Integer getProbabilidade() {
		return probabilidade;
	}
	
	@DisplayName("Data de Retorno")
	public Date getDtretorno() {
		return dtretorno;
	}
	
	@DisplayName("Fonte")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidadefonte")
	public Oportunidadefonte getOportunidadefonte() {
		return oportunidadefonte;
	}
	
	@DisplayName("Hist�ricos")
	public Integer getQtdehistorico() {
		return qtdehistorico;
	}

	@DisplayName("Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}

	@DisplayName("Contato do Cliente")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	public Contato getContato() {
		return contato;
	}
	
	public Boolean getRevendedor() {
		return revendedor;
	}

	@DisplayName("Caixa Proposta")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpropostacaixa")
	public Propostacaixa getPropostacaixa() {
		return propostacaixa;
	}
	
	@Required
	@DisplayName("Tipo Pessoa")
	public Tipopessoacrm getTipopessoacrm() {
		return tipopessoacrm;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="oportunidade")
	public Set<Oportunidadeitem> getListaOportunidadeitem() {
		return listaOportunidadeitem;
	}
	@DisplayName("Produtos e Servi�os")
	@OneToMany(mappedBy="oportunidade")
	public Set<Oportunidadematerial> getListaOportunidadematerial() {
		return listaOportunidadematerial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public Integer getIdentificador() {
		return identificador;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconcorrente")
	public Concorrente getConcorrente() {
		return concorrente;
	}
	
	@DisplayName("Estrat�gia e Marketing")
	@MaxLength(2000)
	public String getEstrategiaMarketing() {
		return estrategiaMarketing;
	}
	
	@DisplayName("For�as")
	@MaxLength(2000)
	public String getForcas() {
		return forcas;
	}
	
	@DisplayName("Fraquezas")
	@MaxLength(2000)
	public String getFraquezas() {
		return fraquezas;
	}
	
	@DisplayName("Site")
	@MaxLength(100)
	public String getSite() {
		return site;
	}
	
	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}
	
	public void setEstrategiaMarketing(String estrategiaMarketing) {
		this.estrategiaMarketing = estrategiaMarketing;
	}
	
	public void setForcas(String forcas) {
		this.forcas = forcas;
	}
	
	public void setFraquezas(String fraquezas) {
		this.fraquezas = fraquezas;
	}
	
	public void setSite(String site) {
		this.site = site;
	}
	
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setListaOportunidadeitem(Set<Oportunidadeitem> listaOportunidadeitem) {
		this.listaOportunidadeitem = listaOportunidadeitem;
	}
	
	public void setListaOportunidadematerial(Set<Oportunidadematerial> listaOportunidadematerial) {
		this.listaOportunidadematerial = listaOportunidadematerial;
	}

	public void setTipopessoacrm(Tipopessoacrm tipopessoacrm) {
		this.tipopessoacrm = tipopessoacrm;
	}

	public void setCdoportunidade(Integer cdoportunidade) {
		this.cdoportunidade = cdoportunidade;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	
	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}
	
	public void setDtPrevisaotermino(Date dtPrevisaotermino) {
		this.dtPrevisaotermino = dtPrevisaotermino;
	}

//	public void setMaterial(Material material) {
//		this.material = material;
//	}

	public void setResponsavel(Pessoa responsavel) {
		this.responsavel = responsavel;
	}

	public void setOportunidadesituacao(Oportunidadesituacao oportunidadesituacao) {
		this.oportunidadesituacao = oportunidadesituacao;
	}

	public void setListoportunidadehistorico(
			List<Oportunidadehistorico> listoportunidadehistorico) {
		this.listoportunidadehistorico = listoportunidadehistorico;
	}
	
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	
	public void setContacrmcontato(Contacrmcontato contacrmcontato) {
		this.contacrmcontato = contacrmcontato;
	}
	public void setProximopasso(String proximopasso){
		this.proximopasso = proximopasso;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	
//	public void setValor(Money valor){
//		this.valor = valor;
//	}
//	public void setValorproducao(Money valorproducao) {
//		this.valorproducao = valorproducao;
//	}
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	public void setCampanha(Campanha campanha){
		this.campanha = campanha;
	}
	
	public void setShowProbabilidade(String showProbabilidade) {
		this.showProbabilidade = showProbabilidade;
	}
	
	public void setProbabilidade(Integer probabilidade) {
		this.probabilidade = probabilidade;
	}
	
	public void setDtretorno(Date dtretorno) {
		this.dtretorno = dtretorno;
	}	

	public void setTiporesponsavel(Tiporesponsavel tiporesponsavel) {
		this.tiporesponsavel = tiporesponsavel;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}


	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setOportunidadefonte(Oportunidadefonte oportunidadefonte) {
		this.oportunidadefonte = oportunidadefonte;
	}
	
	public void setQtdehistorico(Integer qtdehistorico) {
		this.qtdehistorico = qtdehistorico;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select oportunidadeSubQueryClienteEmpresa.cdoportunidade " +
				"from Oportunidade oportunidadeSubQueryClienteEmpresa " +
				"left outer join oportunidadeSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdoportunidade == null) ? 0 : cdoportunidade.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Oportunidade other = (Oportunidade) obj;
		if (cdoportunidade == null) {
			if (other.cdoportunidade != null)
				return false;
		} else if (!cdoportunidade.equals(other.cdoportunidade))
			return false;
		return true;
	}
	
	@Transient
	public Boolean getRedirecionaTela() {
		return redirecionaTela;
	}
	
	public void setRedirecionaTela(Boolean redirecionaTela) {
		this.redirecionaTela = redirecionaTela;
	}

	@Transient
	@DisplayName("Arquivo hist�rico")
	public Arquivo getArquivohistorico() {
		return arquivohistorico;
	}

	public void setArquivohistorico(Arquivo arquivohistorico) {
		this.arquivohistorico = arquivohistorico;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	@Transient
	public Boolean getEntrada() {
		return entrada;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	public void setEntrada(Boolean entrada) {
		this.entrada = entrada;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}
	
	public void setRevendedor(Boolean revendedor) {
		this.revendedor = revendedor;
	}

	public void setPropostacaixa(Propostacaixa propostacaixa) {
		this.propostacaixa = propostacaixa;
	}
	public void setListaOportunidadeproposta(List<Oportunidadeproposta> listaOportunidadeproposta) {
		this.listaOportunidadeproposta = listaOportunidadeproposta;
	}

	@Transient
	public String getUltimoHistorico() {
		StringBuilder historico = new StringBuilder("");
		SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		
		if(this.getListoportunidadehistorico() != null && !this.getListoportunidadehistorico().isEmpty()){
			for(Oportunidadehistorico oportunidadehistorico : this.getListoportunidadehistorico()){
				if(oportunidadehistorico.getDtaltera() != null){
					historico.append(dataFormatada.format(oportunidadehistorico.getDtaltera())).append(" / ");
				}
				historico.append(oportunidadehistorico.getUsuarioaltera() != null ? oportunidadehistorico.getUsuarioaltera() + " / " : "");
				historico.append(oportunidadehistorico.getObservacao() != null ? oportunidadehistorico.getObservacao() : "");
			}
		}
		return historico.toString();
	}
	
	public void setUltimoHistorico(String ultimoHistorico) {
		this.ultimoHistorico = ultimoHistorico;
	}

	@Transient
	public OportunidadeBean getOportunidadeRTF() {

		OportunidadeBean oportunidadeRTF = new OportunidadeBean();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		oportunidadeRTF.setCdoportunidade(getCdoportunidade() != null ? getCdoportunidade().toString() : "");
		oportunidadeRTF.setIdentificador(getIdentificador() != null ? getIdentificador().toString() : "");
		
		if(getCliente() != null ){
			Endereco endereco = getCliente().getEndereco();
			if (endereco != null){
				oportunidadeRTF.setMunicipio_cliente(endereco.getMunicipio() != null ? endereco.getMunicipio().getNome() : "");
			}
			
			oportunidadeRTF.setNome_cliente(getCliente() != null ? getCliente().getNome() : "");
			oportunidadeRTF.setContato_cliente(getContato() != null ? getContato().getNome() : "");
			oportunidadeRTF.setEmail_contato_cliente(getContato() != null && getContato().getEmailcontato() != null ? getContato().getEmailcontato() : "");
			oportunidadeRTF.setRazaosocial_cliente(getCliente().getRazaosocial() != null ? getCliente().getRazaosocial() : "");
			oportunidadeRTF.setCpf_cnpj_cliente(getCliente().getCpfcnpj());
			oportunidadeRTF.setCdcliente_cliente(getCliente().getCdpessoa() != null ? getCliente().getCdpessoa().toString() : "");
			oportunidadeRTF.setIdentificador_cliente(getCliente().getIdentificador() != null ? getCliente().getIdentificador() : "");
			
			if(getCliente() != null && getCliente().getListaTelefone() != null){
				Pessoa resp = new Pessoa();
				resp.setListaTelefone(getCliente().getListaTelefone());
				oportunidadeRTF.setTelefone_cliente(resp.getTelefones());
			}else{
				oportunidadeRTF.setTelefone_cliente("");
			}
		}
		
		if(getEmpresa() != null){
			Endereco enderecoEmpresa = getEmpresa().getEndereco();
			
			oportunidadeRTF.setEmpresa_nome(getEmpresa().getNome() != null ? getEmpresa().getNome() : "");
			oportunidadeRTF.setEmpresa_razaosocial(getEmpresa().getRazaosocial() != null ? getEmpresa().getRazaosocial() : "");
			oportunidadeRTF.setEmpresa_cnpj(getEmpresa().getCnpj() != null ? getEmpresa().getCnpj().toString() : "");
			oportunidadeRTF.setEmpresa_ie(getEmpresa().getInscricaoestadual() != null ? getEmpresa().getInscricaoestadual() : "");
			oportunidadeRTF.setEmpresa_im(getEmpresa().getInscricaomunicipal() != null ? getEmpresa().getInscricaomunicipal() : "");
			oportunidadeRTF.setEmpresa_logradouro(enderecoEmpresa != null ? enderecoEmpresa.getLogradouro() : "");
			oportunidadeRTF.setEmpresa_bairro(enderecoEmpresa != null ? enderecoEmpresa.getBairro() : "");
			oportunidadeRTF.setEmpresa_cidade(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null ? enderecoEmpresa.getMunicipio().getNome() : "");
			oportunidadeRTF.setEmpresa_uf(enderecoEmpresa != null && enderecoEmpresa.getMunicipio() != null && enderecoEmpresa.getMunicipio().getUf() != null ? enderecoEmpresa.getMunicipio().getUf().getSigla() : "" );
			oportunidadeRTF.setEmpresa_cep(enderecoEmpresa != null && enderecoEmpresa.getCep() != null ? enderecoEmpresa.getCep().toString() : "");
			oportunidadeRTF.setEmpresa_telefones(getEmpresa().getTelefones());
			oportunidadeRTF.setEmpresa_email(getEmpresa().getEmail() != null ? getEmpresa().getEmail() : "");
			oportunidadeRTF.setEmpresa_site(getEmpresa().getSite() != null ? getEmpresa().getSite() : "");
		}
		
		oportunidadeRTF.setEmail_representante(getResponsavel() != null && getResponsavel().getEmail() != null ? getResponsavel().getEmail(): "" );
		if(getResponsavel() != null && getResponsavel().getListaTelefone() != null){
			//FIXME resolver problema de bind de classe (Colaborador)
			Pessoa resp = new Pessoa();
			resp.setListaTelefone(getResponsavel().getListaTelefone());
			oportunidadeRTF.setTelefone_representante(resp.getTelefones());
		}else{
			oportunidadeRTF.setTelefone_representante("");
		}
				
		oportunidadeRTF.setTipopessoacrm(getTipopessoacrm() != null ? getTipopessoacrm().name() : "");
		oportunidadeRTF.setNome_contacrm(getContacrm() != null ? getContacrm().getNome() : "");
		oportunidadeRTF.setTelefone(getContacrmcontato() != null ? getContacrmcontato().getTelefones() : "");
		oportunidadeRTF.setNome_contato(getContacrmcontato() != null ? getContacrmcontato().getNome() : "");
		oportunidadeRTF.setEmail_contato(getContacrmcontato() != null ? getContacrmcontato().getListaEmailRTF() : "");
		oportunidadeRTF.setNome(getNome());
		oportunidadeRTF.setTipo_responsavel(getTiporesponsavel() != null ? getTiporesponsavel().getDescricao() : "");
		oportunidadeRTF.setResponsavel(getResponsavel() != null ? getResponsavel().getNome() : "");
		
		LinkedList<TelefoneBean> listaTelefoneBean = new LinkedList<TelefoneBean>();
		if(getResponsavel() != null && Hibernate.isInitialized(getResponsavel().getListaTelefone()) && SinedUtil.isListNotEmpty(responsavel.getListaTelefone())){
			for(Telefone telefone : responsavel.getListaTelefone()){
				listaTelefoneBean.add(new TelefoneBean(telefone));
			}
		}
		oportunidadeRTF.setTelefone_responsavel(listaTelefoneBean);
		
		oportunidadeRTF.setData_inicio(getDtInicio() != null ? dateFormat.format(getDtInicio()) : "");
		oportunidadeRTF.setValortotal(getValortotal() != null ? getValortotal().toString() : "");
		oportunidadeRTF.setProbabilidade(getProbabilidade() != null ? getProbabilidade().toString() : "");
		oportunidadeRTF.setPeriodicidade(getFrequencia() != null ? getFrequencia().getNome() : "");
		oportunidadeRTF.setDataporextenso(SinedDateUtils.dataExtenso(SinedDateUtils.currentDate()));
		oportunidadeRTF.setValorporextenso(getValortotal() != null? new Extenso(getValortotal()).toString():"");
		oportunidadeRTF.setAnoAtual(SinedUtil.datePatternForReport().substring(0, 4));
		
		OportunidadeMaterialBean oportunidadeMaterialRTF;
		LinkedList<OportunidadeMaterialBean> lista = new LinkedList<OportunidadeMaterialBean>();		
		Set<Oportunidadematerial> listaOportunidadematerial = getListaOportunidadematerial();
		Double valortotalprodutos = 0d;
		for (Oportunidadematerial oportunidadematerial : listaOportunidadematerial) {
			
			oportunidadeMaterialRTF = new OportunidadeMaterialBean();
			
			oportunidadeMaterialRTF.setNome_material(oportunidadematerial.getMaterial() != null && oportunidadematerial.getMaterial().getNome() != null ? oportunidadematerial.getMaterial().getNome() : "");
			oportunidadeMaterialRTF.setQtd_material(oportunidadematerial.getQuantidade() != null ? oportunidadematerial.getQuantidade().toString() : "");
			oportunidadeMaterialRTF.setValor_unitario_material(oportunidadematerial.getValorunitario() != null ? new Money(oportunidadematerial.getValorunitario()).toString() : "");
			oportunidadeMaterialRTF.setValor_total_material(oportunidadematerial.getTotal() != null ? new Money(oportunidadematerial.getTotal()).toString() : "");
			oportunidadeMaterialRTF.setPrazo_entrega_material(oportunidadematerial.getPrazoentregamaterial() != null ? oportunidadematerial.getPrazoentregamaterial().toString() : "");
			oportunidadeMaterialRTF.setNcm_material(oportunidadematerial.getMaterial() != null && oportunidadematerial.getMaterial().getNcmcapitulo() != null ? oportunidadematerial.getMaterial().getNcmcapitulo().getDescricao() : "");
			oportunidadeMaterialRTF.setNcmcompleto_material(oportunidadematerial.getMaterial() != null && oportunidadematerial.getMaterial().getNcmcompleto() != null ? oportunidadematerial.getMaterial().getNcmcompleto() : "");
			oportunidadeMaterialRTF.setGarantia_material(oportunidadematerial.getMaterial() != null && oportunidadematerial.getMaterial().getPrazogarantia() != null ? oportunidadematerial.getMaterial().getPrazogarantia().toString() : "");
			
			if(oportunidadematerial.getTotal() != null){
				valortotalprodutos += oportunidadematerial.getTotal();
			}
			boolean oportunidadematerialNotNull = oportunidadematerial!= null && oportunidadematerial.getMaterial()!= null && oportunidadematerial.getMaterial().getCaracteristicas()!= null;
			oportunidadeMaterialRTF.setCaracteristica_material(oportunidadematerialNotNull ? oportunidadematerial.getMaterial().getCaracteristicaMaterial() : "");
			oportunidadeMaterialRTF.setObservacao_material(oportunidadematerial.getObservacao()!=null ? oportunidadematerial.getObservacao().replaceAll("\r\n", SinedUtil.QUEBRA_LINHA_RTF) : "");
			lista.add(oportunidadeMaterialRTF);
		}
		oportunidadeRTF.setProdutos(lista);
		
		StringBuilder observacoes = new StringBuilder();
		if(this.listoportunidadehistorico != null && !this.listoportunidadehistorico.isEmpty()){
			for(Oportunidadehistorico oportunidadehistorico : this.listoportunidadehistorico){
				if(oportunidadehistorico.getObservacao() != null && !"".equals(oportunidadehistorico.getObservacao())){
					if(!"".equals(observacoes.toString())) observacoes.append(". ");
					observacoes.append(oportunidadehistorico.getObservacao());
				}
			}			
		}
		oportunidadeRTF.setObservacao(observacoes.toString());
		
		Empresa empresa = EmpresaService.getInstance().loadArquivoPrincipal();
		
		if(empresa.getLogomarca() != null && empresa.getLogomarca().getCdarquivo() != null){
			Arquivo logo = empresa.getLogomarca();
			try {
				File input = new File(ArquivoDAO.getInstance().getCaminhoArquivoCompleto(logo));
				BufferedImage image = ImageIO.read(input);
				
				File output = new File(ArquivoDAO.getInstance().getCaminhoLogoRtf(logo));
				ImageIO.write(image, "png", output);  
				
				oportunidadeRTF.setLogo(new FileInputStream(output));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		LinkedList<OportunidadeCampoadicionalBean> listaOportunidadeCampoadicionalRTF = new LinkedList<OportunidadeCampoadicionalBean>();	
		Map<String, String> mapCamposAdicionais = new HashMap<String, String>();
		if(getListaOportunidadeitem() != null && getListaOportunidadeitem().size() > 0){
			for (Oportunidadeitem oportunidadeitem : getListaOportunidadeitem()) {
				if(oportunidadeitem.getPropostacaixaitem() != null && 
						oportunidadeitem.getPropostacaixaitem().getNome() != null && 
						oportunidadeitem.getValor() != null){
					mapCamposAdicionais.put(oportunidadeitem.getPropostacaixaitem().getNome(), oportunidadeitem.getValor());
					listaOportunidadeCampoadicionalRTF.add(new OportunidadeCampoadicionalBean(oportunidadeitem.getPropostacaixaitem().getNome(), oportunidadeitem.getValor()));
				}
			}
		}
		oportunidadeRTF.setMapCamposAdicionais(mapCamposAdicionais);
		oportunidadeRTF.setCamposAdicionais(listaOportunidadeCampoadicionalRTF);
		
		if(getCnpj()!=null){
			oportunidadeRTF.setCnpj(getCnpj().toString());
			oportunidadeRTF.setCpf_cnpj(getCnpj().toString());
		}else if(getCpf()!=null){
			oportunidadeRTF.setCpf(getCpf().toString());
			oportunidadeRTF.setCpf_cnpj(getCpf().toString());
		}
		oportunidadeRTF.setBairro(getBairro());
		oportunidadeRTF.setCidade(getMunicipio()!=null ? getMunicipio().getNome() : "");
		oportunidadeRTF.setLogradouro(getLogradouro());
		oportunidadeRTF.setNumero(getNumero());
		oportunidadeRTF.setCep(getCep()!=null ? getCep().toString() : "");
		oportunidadeRTF.setUf(getMunicipio()!=null && getMunicipio().getUf()!=null ? getMunicipio().getUf().getSigla() : "");
		oportunidadeRTF.setComplemento_endereco(getComplementoEndereco());
		
		return oportunidadeRTF;
	}

	@Transient
	public List<GenericBean> getListaTeste() {
		GenericBean gb = new GenericBean();
		gb.setId(150);
		listaTeste.add(gb);
		return listaTeste;
	}

	public void setListaTeste(List<GenericBean> listaTeste) {
		this.listaTeste = listaTeste;
	}
	
	@Transient
	public Boolean getUsarSequencial() {
		return usarSequencial;
	}
	
	public void setUsarSequencial(Boolean usarSequencial) {
		this.usarSequencial = usarSequencial;
	}
	
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	@Transient
	public String getMateriais() {
		if(getListaOportunidadematerial() != null && !getListaOportunidadematerial().isEmpty()){
			StringBuilder s = new StringBuilder();
			if(getListaOportunidadematerial().size() > 5){
				s.append("Diversos");
			}else {
				for(Oportunidadematerial oportunidadematerial : getListaOportunidadematerial()){
					if(oportunidadematerial.getMaterial() != null && oportunidadematerial.getMaterial().getNome() != null &&
							!"".equals(oportunidadematerial.getMaterial().getNome())){
						if(!s.toString().equals("")) s.append(" | ");
						s.append(oportunidadematerial.getMaterial().getNome());
					}
				}
			}
			return s.toString();
		}
		return materiais != null ? materiais : "";
	}

	@Transient
	public String getOportunidadeIdentificadorDtInicio(){
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}		
		if(this.getIdentificador() != null){
			s.append(" - ").append(this.getIdentificador());
		}
		if(this.getDtInicio() != null){
			s.append(" - ").append(new SimpleDateFormat("dd/MM/yyyy").format(this.getDtInicio()));
		}
		return s.toString();
	}
	
	@Transient
	public Boolean getPermitidoEdicao() {
		return permitidoEdicao;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}
	public void setPermitidoEdicao(Boolean permitidoEdicao) {
		this.permitidoEdicao = permitidoEdicao;
	}
	
	// campos Transient 
	@Transient
	public Boolean getAlterado() {
		return alterado;
	}
	
	public void setAlterado(Boolean alterado) {
		this.alterado = alterado;
	}
}
