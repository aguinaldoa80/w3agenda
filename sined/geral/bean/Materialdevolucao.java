package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialdevolucao", sequenceName = "sq_materialdevolucao")
public class Materialdevolucao implements Log {

	protected Integer cdmaterialdevolucao;
	protected Material material;
	protected Double preco;
	protected Double qtdevendida;
	protected Double qtdedevolvida;
	protected String placaveiculo;
	protected String numeronotadevolucao;
	protected Localarmazenagem localarmazenagem;
	protected Unidademedida unidademedida;
	protected Unidademedida unidademedidavenda;
	protected Venda venda;
	protected Vendamaterial vendamaterial;
	protected Pedidovenda pedidovenda;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Movimentacaoestoque movimentacaoestoque;
	protected Date dtcancelamento;
	protected Entregadocumento entregadocumento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	private Boolean valeCompra;
	
	protected List<Materialdevolucaohistorico> listaMaterialdevolucaohistorico;
	
	private Loteestoque loteestoqueTrans;
	
	public Materialdevolucao(){}

	public Materialdevolucao(Integer cdvendamaterial, Integer cdmaterial, Double qtdedevolvida, Integer cdvenda){
		if(cdvendamaterial != null){
			this.vendamaterial = new Vendamaterial(cdvendamaterial);
		}
		if(cdmaterial != null){
			this.material = new Material(cdmaterial);
		}
		this.qtdedevolvida = qtdedevolvida;
		this.venda = new Venda(cdvenda);
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialdevolucao")
	public Integer getCdmaterialdevolucao() {
		return cdmaterialdevolucao;
	}
	@DisplayName("Produto")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Pre�o")
	public Double getPreco() {
		return preco;
	}
	@DisplayName("Quantidade Vendida")
	public Double getQtdevendida() {
		return qtdevendida;
	}
	@DisplayName("Total Devolvido")
	public Double getQtdedevolvida() {
		return qtdedevolvida;
	}
	@DisplayName("Placa do Ve�culo")
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@DisplayName("N�mero da Nota de devolu��o")
	public String getNumeronotadevolucao() {
		return numeronotadevolucao;
	}
	@DisplayName("Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	@DisplayName("Hist�rico")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="materialdevolucao")
	public List<Materialdevolucaohistorico> getListaMaterialdevolucaohistorico() {
		return listaMaterialdevolucaohistorico;
	}
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Unidade de Medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@DisplayName("U.M. Vendida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedidavenda")
	public Unidademedida getUnidademedidavenda() {
		return unidademedidavenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendamaterial")
	public Vendamaterial getVendamaterial() {
		return vendamaterial;
	}
	@DisplayName("Pedido de Venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmovimentacaoestoque")
	public Movimentacaoestoque getMovimentacaoestoque() {
		return movimentacaoestoque;
	}
	@DisplayName("Data de cancelamento")
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	public Boolean getValeCompra() {
		return valeCompra;
	}
	@Transient
	public Boolean isValeCompra() {
		return getValeCompra();
	}
	public void setValeCompra(Boolean valeCompra) {
		this.valeCompra = valeCompra;
	}
	
	public void setVendamaterial(Vendamaterial vendamaterial) {
		this.vendamaterial = vendamaterial;
	}
	public void setCdmaterialdevolucao(Integer cdmaterialdevolucao) {
		this.cdmaterialdevolucao = cdmaterialdevolucao;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setQtdevendida(Double qtdevendida) {
		this.qtdevendida = qtdevendida;
	}
	public void setQtdedevolvida(Double qtdedevolvida) {
		this.qtdedevolvida = qtdedevolvida;
	}
	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setNumeronotadevolucao(String numeronotadevolucao) {
		this.numeronotadevolucao = numeronotadevolucao;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setMovimentacaoestoque(Movimentacaoestoque movimentacaoestoque) {
		this.movimentacaoestoque = movimentacaoestoque;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setUnidademedidavenda(Unidademedida unidademedidavenda) {
		this.unidademedidavenda = unidademedidavenda;
	}
	public void setListaMaterialdevolucaohistorico(
			List<Materialdevolucaohistorico> listaMaterialdevolucaohistorico) {
		this.listaMaterialdevolucaohistorico = listaMaterialdevolucaohistorico;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name ="cdentregadocumento")
	public Entregadocumento getEntregadocumento() {
		return entregadocumento;
	}

	public void setEntregadocumento(Entregadocumento entregadocumento) {
		this.entregadocumento = entregadocumento;
	}

	

	@Transient
	public String getObsHistoricoTrans(){
		StringBuilder s = new StringBuilder();
		if(this.listaMaterialdevolucaohistorico != null && !this.listaMaterialdevolucaohistorico.isEmpty()){
			for(Materialdevolucaohistorico mdh : this.listaMaterialdevolucaohistorico){
				if(mdh.getObservacao() != null && !"".equals(mdh.getObservacao())){
					s.append(mdh.getObservacao()).append(". ");
				}
			}
		}
		return s.toString();
	}
	
	@Transient
	public Boolean getCancelado(){
		if(this.getDtcancelamento()!=null){
			return true;
		}else{
			return false;
		}
	}
	
	@DisplayName("Lote")
	@Transient
	public Loteestoque getLoteestoqueTrans() {
		if(vendamaterial != null){
			loteestoqueTrans = vendamaterial.getLoteestoque();
		}else if(pedidovendamaterial != null){
			loteestoqueTrans = pedidovendamaterial.getLoteestoque();
		}
		return loteestoqueTrans;
	}
	public void setLoteestoqueTrans(Loteestoque loteestoqueTrans) {
		this.loteestoqueTrans = loteestoqueTrans;
	}
}
