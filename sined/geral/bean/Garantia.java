package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_garantia", sequenceName = "sq_garantia")
public class Garantia implements Log{

	protected Integer cdgarantia;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_garantia")
	public Integer getCdgarantia() {
		return cdgarantia;
	}
	
	@Required
	@MaxLength(100)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdgarantia(Integer cdgarantia) {
		this.cdgarantia = cdgarantia;
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	
}
