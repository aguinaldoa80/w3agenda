package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Emporiumvendaacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_emporiumvendahistorico", sequenceName = "sq_emporiumvendahistorico")
public class Emporiumvendahistorico implements Log {

	protected Integer cdemporiumvendahistorico;
	protected Emporiumvenda emporiumvenda;
	protected Emporiumvendaacao acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumvendahistorico")
	public Integer getCdemporiumvendahistorico() {
		return cdemporiumvendahistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdemporiumvenda")
	public Emporiumvenda getEmporiumvenda() {
		return emporiumvenda;
	}
	
	@DisplayName("A��o")
	public Emporiumvendaacao getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setAcao(Emporiumvendaacao acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdemporiumvendahistorico(Integer cdemporiumvendahistorico) {
		this.cdemporiumvendahistorico = cdemporiumvendahistorico;
	}

	public void setEmporiumvenda(Emporiumvenda emporiumvenda) {
		this.emporiumvenda = emporiumvenda;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

}
