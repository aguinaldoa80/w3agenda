package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_garantiatipopercentual", sequenceName="sq_garantiatipopercentual")
@DisplayName("Garantiatipopercentual")
public class Garantiatipopercentual {

	private Integer cdgarantiatipopercentual;
	private Garantiatipo garantiatipo;
	private Double percentual;
	
	public Garantiatipopercentual(){
	}
	
	public Garantiatipopercentual(Integer cdgarantiatipopercentual){
		this.cdgarantiatipopercentual = cdgarantiatipopercentual;
	}
	
	public Garantiatipopercentual(Double percentual){
		this.percentual = percentual;
	}
	
	@Id
	@GeneratedValue(generator="sq_garantiatipopercentual", strategy=GenerationType.AUTO)
	@DisplayName("Id")
	public Integer getCdgarantiatipopercentual() {
		return cdgarantiatipopercentual;
	}
	public void setCdgarantiatipopercentual(Integer cdgarantiatipopercentual) {
		this.cdgarantiatipopercentual = cdgarantiatipopercentual;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiatipo")
	@DisplayName("Tipo de garantia")
	public Garantiatipo getGarantiatipo() {
		return garantiatipo;
	}
	public void setGarantiatipo(Garantiatipo garantiatipo) {
		this.garantiatipo = garantiatipo;
	}
	
	@Required
	@DisplayName("% de Garantia")
	@DescriptionProperty
	@MaxLength(5)
	@MaxValue(100)
	@MinValue(0)
	public Double getPercentual() {
		return percentual;
	}
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}
	
}
