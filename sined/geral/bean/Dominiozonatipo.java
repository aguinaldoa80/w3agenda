package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
public class Dominiozonatipo{

	protected Integer cddominiozonatipo;
	protected String nome;
	
	@Id
	public Integer getCddominiozonatipo() {
		return cddominiozonatipo;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCddominiozonatipo(Integer cddominiozonatipo) {
		this.cddominiozonatipo = cddominiozonatipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
