package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Arquivobancariosituacao {
	
	public static Arquivobancariosituacao CANCELADO = new Arquivobancariosituacao(1);
	public static Arquivobancariosituacao GERADO = new Arquivobancariosituacao(2);
	public static Arquivobancariosituacao PROCESSADO = new Arquivobancariosituacao(3);
	public static Arquivobancariosituacao PROCESSADO_PARCIAL = new Arquivobancariosituacao(4);
	
	protected Integer cdarquivobancariosituacao;
	protected String nome;

	
	public Arquivobancariosituacao(){}
	
	public Arquivobancariosituacao(Integer cdarquivobancariosituacao){
		this.cdarquivobancariosituacao = cdarquivobancariosituacao;
	}
	
	@Id
	public Integer getCdarquivobancariosituacao() {
		return cdarquivobancariosituacao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdarquivobancariosituacao(Integer cdarquivobancariosituacao) {
		this.cdarquivobancariosituacao = cdarquivobancariosituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arquivobancariosituacao) {
			Arquivobancariosituacao arquivobancariosituacao = (Arquivobancariosituacao) obj;
			return arquivobancariosituacao.getCdarquivobancariosituacao().equals(this.getCdarquivobancariosituacao());
		}
		return super.equals(obj);
	}
	
}
