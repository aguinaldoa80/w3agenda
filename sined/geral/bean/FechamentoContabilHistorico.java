package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_fechamentocontabilhistorico", sequenceName="sq_fechamentocontabilhistorico")
public class FechamentoContabilHistorico {
	protected Integer cdFechamentoContabilHistorico;
	protected FechamentoContabil fechamentoContabil;
	protected Usuario usuarioAltera;
	protected String acao;
	protected String observacao;

	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public FechamentoContabilHistorico() {
		this.dtaltera = new Timestamp(System.currentTimeMillis());
	}
	
	public FechamentoContabilHistorico(FechamentoContabil fechamentoContabil, Usuario usuario) {
		this.fechamentoContabil = fechamentoContabil.getCdFechamentoContabil() != null ? fechamentoContabil : null;
		this.usuarioAltera = usuario;
		this.cdusuarioaltera = usuario.getCdpessoa();
		this.dtaltera = new Timestamp(System.currentTimeMillis());
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fechamentocontabilhistorico")
	@DisplayName("C�digo")
	public Integer getCdFechamentoContabilHistorico() {
		return cdFechamentoContabilHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdFechamentoContabil")
	public FechamentoContabil getFechamentoContabil() {
		return fechamentoContabil;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdusuario")
	@DisplayName("Respons�vel")
	public Usuario getUsuarioAltera() {
		return usuarioAltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setCdFechamentoContabilHistorico(
			Integer cdFechamentoContabilHistorico) {
		this.cdFechamentoContabilHistorico = cdFechamentoContabilHistorico;
	}
	public void setFechamentoContabil(FechamentoContabil fechamentoContabil) {
		this.fechamentoContabil = fechamentoContabil;
	}
	public void setUsuarioAltera(Usuario usuarioAltera) {
		this.usuarioAltera = usuarioAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
