package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_projetoitem",sequenceName="sq_projetoitem")
public class Projetoitem {
	
	protected Integer cdprojetoitem;
	protected Projeto projeto;
	protected Projetotipoitem projetotipoitem;
	protected String valor;
	
	@Id
	@GeneratedValue(generator="sq_projetoitem",strategy=GenerationType.AUTO)
	public Integer getCdprojetoitem() {
		return cdprojetoitem;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojetotipoitem")
	public Projetotipoitem getProjetotipoitem() {
		return projetotipoitem;
	}
	
	@MaxLength(500)
	public String getValor() {
		return valor;
	}
	
	public void setProjetotipoitem(Projetotipoitem projetotipoitem) {
		this.projetotipoitem = projetotipoitem;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setCdprojetoitem(Integer cdprojetoitem) {
		this.cdprojetoitem = cdprojetoitem;
	}
}
