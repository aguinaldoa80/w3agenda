package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_arquivomdfeerro", sequenceName = "sq_arquivomdfeerro")
public class ArquivoMdfeErro {
	
	protected Integer cdArquivoMdfeErro;
	protected ArquivoMdfe arquivoMdfe;
	protected Timestamp data;
	protected String mensagem;
	protected String correcao;
	protected String codigo;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivomdfeerro")
	public Integer getCdArquivoMdfeErro() {
		return cdArquivoMdfeErro;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivomdfe")
	public ArquivoMdfe getArquivoMdfe() {
		return arquivoMdfe;
	}

	public String getMensagem() {
		return mensagem;
	}
	
	@DisplayName("Corre��o")
	public String getCorrecao() {
		return correcao;
	}
	
	@DisplayName("Data")
	public Timestamp getData() {
		return data;
	}
	
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	public void setCdArquivoMdfeErro(Integer cdArquivoMdfeErro) {
		this.cdArquivoMdfeErro = cdArquivoMdfeErro;
	}

	public void setArquivoMdfe(ArquivoMdfe arquivoMdfe) {
		this.arquivoMdfe = arquivoMdfe;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setCorrecao(String correcao) {
		this.correcao = correcao;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
