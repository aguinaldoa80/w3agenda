package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_atividadetipoitem",sequenceName="sq_atividadetipoitem")
public class Atividadetipoitem implements Log{
	
	protected Integer cdatividadetipoitem;
	protected Atividadetipo atividadetipo;
	protected String nome;
	protected Boolean obrigatorio=false;
	protected Integer ordem;
	protected Boolean imprimeNota;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_atividadetipoitem",strategy=GenerationType.AUTO)
	public Integer getCdatividadetipoitem() {
		return cdatividadetipoitem;
	}
	
	@Required
	@DisplayName("Tipo de atividade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatividadetipo")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Obrigatório")
	@Required
	public Boolean getObrigatorio() {
		return obrigatorio;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdatividadetipoitem(Integer cdatividadetipoitem) {
		this.cdatividadetipoitem = cdatividadetipoitem;
	}
	
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setObrigatorio(Boolean obrigatorio) {
		this.obrigatorio = obrigatorio;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Required
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@DisplayName("Campo adicional em nota?")
	public Boolean getImprimeNota() {
		return imprimeNota;
	}
	public void setImprimeNota(Boolean imprimeNota) {
		this.imprimeNota = imprimeNota;
	}
	

}
