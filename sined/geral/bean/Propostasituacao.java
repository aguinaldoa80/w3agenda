package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Propostasituacao {
	
	public static Propostasituacao GANHOU = new Propostasituacao(1);
	public static Propostasituacao PERDIDA = new Propostasituacao(2);
	public static Propostasituacao EM_ELABORACAO = new Propostasituacao(3);
	public static Propostasituacao AGUARDANDO_POSICAO_CLIENTE = new Propostasituacao(4);
	public static Propostasituacao SUSPENSA = new Propostasituacao(5);
	public static Propostasituacao DECLINADA = new Propostasituacao(6);
	public static Propostasituacao ORCAMENTO_CONCLUIDO = new Propostasituacao(7);
	
	protected Integer cdpropostasituacao;
	protected String nome;

	
	public Propostasituacao(){}
	
	public Propostasituacao(Integer cdpropostasituacao){
		this.cdpropostasituacao = cdpropostasituacao;
	}
	
	@Id
	public Integer getCdpropostasituacao() {
		return cdpropostasituacao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdpropostasituacao(Integer cdpropostasituacao) {
		this.cdpropostasituacao = cdpropostasituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Propostasituacao) {
			Propostasituacao propostasituacao = (Propostasituacao) obj;
			return propostasituacao.getCdpropostasituacao().equals(this.getCdpropostasituacao());
		}
		return super.equals(obj);
	}
	
}
