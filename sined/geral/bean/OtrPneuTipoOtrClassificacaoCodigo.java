package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_otrpneutipootrclassificacaoCodigo", sequenceName = "sq_otrpneutipootrclassificacaoCodigo")
public class OtrPneuTipoOtrClassificacaoCodigo {
	protected Integer cdOtrPneuTipoOtrClassificacaoCodigo; 
	protected OtrPneuTipo otrPneuTipo;
	protected OtrClassificacaoCodigo otrClassificacaoCodigo;
	protected String desenhoStr;
	protected String tipoServicoStr;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_otrpneutipootrclassificacaoCodigo")
	public Integer getCdOtrPneuTipoOtrClassificacaoCodigo() {
		return cdOtrPneuTipoOtrClassificacaoCodigo;
	}
	public void setCdOtrPneuTipoOtrClassificacaoCodigo(
			Integer cdOtrPneuTipoOtrClassificacaoCodigo) {
		this.cdOtrPneuTipoOtrClassificacaoCodigo = cdOtrPneuTipoOtrClassificacaoCodigo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdOtrPneuTipo")
	public OtrPneuTipo getOtrPneuTipo() {
		return otrPneuTipo;
	}
	public void setOtrPneuTipo(OtrPneuTipo otrPneuTipo) {
		this.otrPneuTipo = otrPneuTipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdOtrClassificacaoCodigo")
	@DescriptionProperty
	public OtrClassificacaoCodigo getOtrClassificacaoCodigo() {
		return otrClassificacaoCodigo;
	}
	public void setOtrClassificacaoCodigo(OtrClassificacaoCodigo otrClassificacaoCodigo) {
		this.otrClassificacaoCodigo = otrClassificacaoCodigo;
	}
	
	@Transient
	@DisplayName("Desenho")
	public String getDesenhoStr() {
		return desenhoStr;
	}
	public void setDesenhoStr(String desenhoStr) {
		this.desenhoStr = desenhoStr;
	}
	@Transient
	@DisplayName("Tipo de Servi�o")
	public String getTipoServicoStr() {
		return tipoServicoStr;
	}
	public void setTipoServicoStr(String tipoServicoStr) {
		this.tipoServicoStr = tipoServicoStr;
	}
	
	
}
