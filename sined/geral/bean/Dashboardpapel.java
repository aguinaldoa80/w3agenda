package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dashboardpapel", sequenceName = "sq_dashboardpapel")
public class Dashboardpapel {

	protected Integer cddashboardpapel;
	protected Integer cddashboard;
	protected Papel papel;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dashboardpapel")
	public Integer getCddashboardpapel() {
		return cddashboardpapel;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	public Papel getPapel() {
		return papel;
	}

	@Required
	public Integer getCddashboard() {
		return cddashboard;
	}

	public void setCddashboardpapel(Integer cddashboardpapel) {
		this.cddashboardpapel = cddashboardpapel;
	}

	public void setCddashboard(Integer cddashboard) {
		this.cddashboard = cddashboard;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}

	
}
