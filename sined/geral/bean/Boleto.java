package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_boleto", sequenceName = "sq_boleto")
public class Boleto implements Log {

	protected Integer cdboleto;
	protected String nome;
	protected String layout;
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	public Boleto(){
	}
	
	public Boleto(Integer cdboleto){
		this.cdboleto = cdboleto;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_boleto")
	public Integer getCdboleto() {
		return cdboleto;
	}
	@DisplayName("Layout boleto")
	public String getLayout() {
		return layout;
	}
	@DescriptionProperty
	@Required
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public void setCdboleto(Integer cdboleto) {
		this.cdboleto = cdboleto;
	}
	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
}
