package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_emporiumpedidovenda", sequenceName = "sq_emporiumpedidovenda")
public class Emporiumpedidovenda implements Log {

	protected Integer cdemporiumpedidovenda;
	protected String loja;
	protected String pdv;
	protected Timestamp datahorafiscal;
	protected Double total;
	protected Boolean enviadoemporium = false;
	protected Boolean processado;
	protected Arquivo arquivo;
	protected Pedidovenda pedidovenda;
	
	protected Integer codigo;
	protected Integer codigo_vendedor;
	protected Integer codigo_cliente;
	protected String cliente_cpf_cnpj;
	protected String cliente_nome;
	protected String cliente_logradouro;
	protected String cliente_bairro;
	protected String cliente_cidade;
	protected String cliente_uf;
	protected Integer cliente_cep; 
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Emporiumpedidovendaitem> listaEmporiumpedidovendaitem = new ListSet<Emporiumpedidovendaitem>(Emporiumpedidovendaitem.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumpedidovenda")
	public Integer getCdemporiumpedidovenda() {
		return cdemporiumpedidovenda;
	}
	public String getLoja() {
		return loja;
	}
	public String getPdv() {
		return pdv;
	}
	@DisplayName("Data/hora fiscal")
	public Timestamp getDatahorafiscal() {
		return datahorafiscal;
	}
	public Double getTotal() {
		return total;
	}
	public Boolean getProcessado() {
		return processado;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	@DisplayName("Itens")
	@OneToMany(mappedBy="emporiumpedidovenda")
	public List<Emporiumpedidovendaitem> getListaEmporiumpedidovendaitem() {
		return listaEmporiumpedidovendaitem;
	}
	public Boolean getEnviadoemporium() {
		return enviadoemporium;
	}
	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	@DisplayName("C�digo do Vendedor")
	public Integer getCodigo_vendedor() {
		return codigo_vendedor;
	}
	@DisplayName("C�digo do Cliente")
	public Integer getCodigo_cliente() {
		return codigo_cliente;
	}
	@DisplayName("CPF/CNPJ do Cliente")
	public String getCliente_cpf_cnpj() {
		return cliente_cpf_cnpj;
	}
	@DisplayName("Nome do Cliente")
	public String getCliente_nome() {
		return cliente_nome;
	}
	@DisplayName("Logradouro do Cliente")
	public String getCliente_logradouro() {
		return cliente_logradouro;
	}
	@DisplayName("Bairro do Cliente")
	public String getCliente_bairro() {
		return cliente_bairro;
	}
	@DisplayName("Cidade do Cliente")
	public String getCliente_cidade() {
		return cliente_cidade;
	}
	@DisplayName("UF do Cliente")
	public String getCliente_uf() {
		return cliente_uf;
	}
	@DisplayName("CEP do Cliente")
	public Integer getCliente_cep() {
		return cliente_cep;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setCodigo_vendedor(Integer codigoVendedor) {
		codigo_vendedor = codigoVendedor;
	}
	public void setCodigo_cliente(Integer codigoCliente) {
		codigo_cliente = codigoCliente;
	}
	public void setCliente_cpf_cnpj(String clienteCpfCnpj) {
		cliente_cpf_cnpj = Util.strings.tiraAcento(clienteCpfCnpj);
	}
	public void setCliente_nome(String clienteNome) {
		cliente_nome = Util.strings.tiraAcento(clienteNome);
	}
	public void setCliente_logradouro(String clienteLogradouro) {
		cliente_logradouro = Util.strings.tiraAcento(clienteLogradouro);
	}
	public void setCliente_bairro(String clienteBairro) {
		cliente_bairro = Util.strings.tiraAcento(clienteBairro);
	}
	public void setCliente_cidade(String clienteCidade) {
		cliente_cidade = Util.strings.tiraAcento(clienteCidade);
	}
	public void setCliente_uf(String clienteUf) {
		cliente_uf = Util.strings.tiraAcento(clienteUf);
	}
	public void setCliente_cep(Integer clienteCep) {
		cliente_cep = clienteCep;
	}
	public void setEnviadoemporium(Boolean enviadoemporium) {
		this.enviadoemporium = enviadoemporium;
	}
	public void setListaEmporiumpedidovendaitem(
			List<Emporiumpedidovendaitem> listaEmporiumpedidovendaitem) {
		this.listaEmporiumpedidovendaitem = listaEmporiumpedidovendaitem;
	}
	public void setCdemporiumpedidovenda(Integer cdemporiumpedidovenda) {
		this.cdemporiumpedidovenda = cdemporiumpedidovenda;
	}
	public void setLoja(String loja) {
		this.loja = Util.strings.tiraAcento(loja);
	}
	public void setPdv(String pdv) {
		this.pdv = Util.strings.tiraAcento(pdv);
	}
	public void setDatahorafiscal(Timestamp datahorafiscal) {
		this.datahorafiscal = datahorafiscal;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public void setProcessado(Boolean processado) {
		this.processado = processado;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
