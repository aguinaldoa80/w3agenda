package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@DisplayName("Calend�rio")
@SequenceGenerator(name = "sq_calendario", sequenceName = "sq_calendario")
public class Calendario implements Log, PermissaoProjeto{

	protected Integer cdcalendario;
	protected String nome;
	protected Boolean sabadoutil;
	protected Boolean domingoutil;
	protected Uf uf;
	protected Municipio municipio;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Calendarioitem> listaCalendarioitem = new ListSet<Calendarioitem>(Calendarioitem.class);
	protected List<Calendarioorcamento> listaCalendarioorcamento = new ListSet<Calendarioorcamento>(Calendarioorcamento.class);
	protected List<Calendarioprojeto> listaCalendarioprojeto = new ListSet<Calendarioprojeto>(Calendarioprojeto.class);
	
//	TRANSIENTES
	protected Projeto projetotrans;
	protected Boolean irListagemDireto = Boolean.FALSE;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_calendario")
	public Integer getCdcalendario() {
		return cdcalendario;
	}
	public void setCdcalendario(Integer id) {
		this.cdcalendario = id;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(40)
	@Required
	public String getNome() {
		return nome;
	}
	
	@DisplayName("S�bado dia �til")	
	public Boolean getSabadoutil() {
		return sabadoutil;
	}
	
	@DisplayName("Domingo dia �til")	
	public Boolean getDomingoutil() {
		return domingoutil;
	}
	
	@DisplayName("Feriados")
	@OneToMany(mappedBy="calendario")
	public List<Calendarioitem> getListaCalendarioitem() {
		return listaCalendarioitem;
	}
	
	@DisplayName("Or�amentos")
	@OneToMany(mappedBy="calendario")
	public List<Calendarioorcamento> getListaCalendarioorcamento() {
		return listaCalendarioorcamento;
	}
	
	@DisplayName("Projetos")
	@OneToMany(mappedBy="calendario")
	public List<Calendarioprojeto> getListaCalendarioprojeto() {
		return listaCalendarioprojeto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Munic�pio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	public void setListaCalendarioorcamento(
			List<Calendarioorcamento> listaCalendarioorcamento) {
		this.listaCalendarioorcamento = listaCalendarioorcamento;
	}
	
	public void setListaCalendarioprojeto(
			List<Calendarioprojeto> listaCalendarioprojeto) {
		this.listaCalendarioprojeto = listaCalendarioprojeto;
	}
	
	public void setListaCalendarioitem(List<Calendarioitem> listaCalendarioitem) {
		this.listaCalendarioitem = listaCalendarioitem;
	}
	
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setSabadoutil(Boolean sabadoutil) {
		this.sabadoutil = sabadoutil;
	}
	
	public void setDomingoutil(Boolean domingoutil) {
		this.domingoutil = domingoutil;
	}	
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
//	TRANSIENTES
	@Transient
	public Projeto getProjetotrans() {
		return projetotrans;
	}
	public void setProjetotrans(Projeto projetotrans) {
		this.projetotrans = projetotrans;
	}
	
	@DisplayName("Projeto(s)")
	@Transient
	public String getProjetosString(){
		StringBuilder projetos = new StringBuilder();;
		if(getListaCalendarioprojeto() != null && getListaCalendarioprojeto().size() > 0){
			for (Calendarioprojeto cp : getListaCalendarioprojeto()) {
				projetos.append(cp.getProjeto().getNome());
				projetos.append(", ");
			}
			return projetos.toString().substring(0, projetos.toString().length() - 2);
		}
		return "";
	}
	
	@DisplayName("Or�amento(s)")
	@Transient
	public String getOrcamentosString(){
		StringBuilder orcamentos = new StringBuilder();;
		if(getListaCalendarioorcamento() != null && getListaCalendarioorcamento().size() > 0){
			for (Calendarioorcamento co : getListaCalendarioorcamento()) {
				orcamentos.append(co.getOrcamento().getNome());
				orcamentos.append(", ");
			}
			return orcamentos.toString().substring(0, orcamentos.toString().length() - 2);
		}
		return "";
	}
	
	public String subQueryProjeto() {
		return "select calendariosubQueryProjeto.cdcalendario " +
				"from Calendario calendariosubQueryProjeto " +
				"join calendariosubQueryProjeto.listaCalendarioprojeto listaCalendarioprojetosubQueryProjeto " +
				"join listaCalendarioprojetosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
}
