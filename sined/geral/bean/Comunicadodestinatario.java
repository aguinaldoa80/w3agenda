package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Email;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_comunicadodestinatario",sequenceName="sq_comunicadodestinatario")
@DisplayName("Destinatários")
public class Comunicadodestinatario implements Log{
	
	protected Integer cdcomunicadodestinatario;
	protected Comunicado comunicado;
	protected Colaborador colaborador;	
	protected String email;
	
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_comunicadodestinatario",strategy=GenerationType.AUTO)
	public Integer getCdcomunicadodestinatario() {
		return cdcomunicadodestinatario;
	}
	
	@Required	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcomunicado")	
	public Comunicado getComunicado() {
		return comunicado;
	}
	
	@Required	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")	
	@DisplayName("Destinatário")	
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("E-mail")
	@Required
	@Email
	public String getEmail() {
		if(this.email == null)
			if(this.colaborador != null)
				return this.colaborador.getEmail();

		return email;
	}
	
	public void setCdcomunicadodestinatario(Integer cdcomunicadodestinatario) {
		this.cdcomunicadodestinatario = cdcomunicadodestinatario;
	}
	
	public void setComunicado(Comunicado comunicado) {
		this.comunicado = comunicado;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	@DescriptionProperty
	public String getNomeColaborador(){
		return colaborador!=null && colaborador.getNome()!=null ? colaborador.getNome() : "";
	}
}
