package br.com.linkcom.sined.geral.bean;



import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.NotaFiscalTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipodesconto;

@Entity
@SequenceGenerator(name = "sq_contratodesconto", sequenceName = "sq_contratodesconto")
public class Contratodesconto {
	protected Integer cdcontratodesconto; 
	protected String motivo; 
	protected Date dtinicio; 
	protected Date dtfim; 
	protected Money valor;
	protected Contrato contrato;
	protected Tipodesconto tipodesconto;
	protected NotaFiscalTipoEnum tiponota;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratodesconto")
	public Integer getCdcontratodesconto() {
		return cdcontratodesconto;
	}
	
	@Required
	@MaxLength(100)
	public String getMotivo() {
		return motivo;
	}
	@Required
	@DisplayName("Data in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data fim")
	public Date getDtfim() {
		return dtfim;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@Required
	@DisplayName("Tipo do desconto")
	public Tipodesconto getTipodesconto() {
		return tipodesconto;
	}
	
	public void setTipodesconto(Tipodesconto tipodesconto) {
		this.tipodesconto = tipodesconto;
	}
	public void setCdcontratodesconto(Integer cdcontratodesconto) {
		this.cdcontratodesconto = cdcontratodesconto;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	@DisplayName("Nota Fiscal de Servi�os")
	public NotaFiscalTipoEnum getTiponota() {
		return tiponota;
	}

	public void setTiponota(NotaFiscalTipoEnum tiponota) {
		this.tiponota = tiponota;
	}
	
	
}
