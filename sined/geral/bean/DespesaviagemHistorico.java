package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Despesaviagemacao;

@Entity
@SequenceGenerator(name="sq_despesaviagemhistorico",sequenceName="sq_despesaviagemhistorico")
public class DespesaviagemHistorico{
	
	protected Integer cddespesaviagemhistorico;
	protected String observacao;
	protected Timestamp dtaltera;
	protected Despesaviagem despesaviagem;
	protected Despesaviagemacao acao;
	protected Usuario usuario;
	
	@Id
	@GeneratedValue(generator = "sq_despesaviagemhistorico", strategy = GenerationType.AUTO)
	public Integer getCddespesaviagemhistorico() {
		return cddespesaviagemhistorico;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddespesaviagem")
	public Despesaviagem getDespesaviagem() {
		return despesaviagem;
	}
	@DisplayName("A��o")
	public Despesaviagemacao getAcao() {
		return acao;
	}
	
	public void setCddespesaviagemhistorico(Integer cddespesaviagemhistorico) {
		this.cddespesaviagemhistorico = cddespesaviagemhistorico;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDespesaviagem(Despesaviagem despesaviagem) {
		this.despesaviagem = despesaviagem;
	}
	public void setAcao(Despesaviagemacao acao) {
		this.acao = acao;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@JoinColumn(name="cdusuarioaltera")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Respons�vel")
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
