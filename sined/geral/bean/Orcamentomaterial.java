package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_orcamentomaterial;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Lista de Material")
@SequenceGenerator(name = "sq_orcamentomaterial", sequenceName = "sq_orcamentomaterial")
public class Orcamentomaterial implements Log{
	
	protected Integer cdorcamentomaterial;
	protected Orcamento orcamento;
	protected String descricao;
	protected Double icmsdestino;
	protected Double fatorfaturamentocompra;
	protected Double fatorfaturamentocliente;
	protected Money totalcompra;
	protected Money totalvenda;
	protected Date dtbaixa;
	protected Date dtcancelamento;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected Aux_orcamentomaterial aux_orcamentomaterial;
	
	protected List<Orcamentomaterialitem> listaOrcamentomaterialitem;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_orcamentomaterial")	
	public Integer getCdorcamentomaterial() {
		return cdorcamentomaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	public String getDescricao() {
		return descricao;
	}

	public Double getIcmsdestino() {
		return icmsdestino;
	}

	public Double getFatorfaturamentocompra() {
		return fatorfaturamentocompra;
	}

	public Double getFatorfaturamentocliente() {
		return fatorfaturamentocliente;
	}

	public Money getTotalcompra() {
		return totalcompra;
	}

	public Money getTotalvenda() {
		return totalvenda;
	}

	public Date getDtbaixa() {
		return dtbaixa;
	}

	public Date getDtcancelamento() {
		return dtcancelamento;
	}

	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}

	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}

	public void setTotalcompra(Money totalcompra) {
		this.totalcompra = totalcompra;
	}

	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}

	public void setCdorcamentomaterial(Integer cdorcamentomaterial) {
		this.cdorcamentomaterial = cdorcamentomaterial;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setIcmsdestino(Double icmsdestino) {
		this.icmsdestino = icmsdestino;
	}

	public void setFatorfaturamentocompra(Double fatorfaturamentocompra) {
		this.fatorfaturamentocompra = fatorfaturamentocompra;
	}

	public void setFatorfaturamentocliente(Double fatorfaturamentocliente) {
		this.fatorfaturamentocliente = fatorfaturamentocliente;
	}
	
//	TABELA AUXILIAR
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamentomaterial", insertable=false, updatable=false)
	public Aux_orcamentomaterial getAux_orcamentomaterial() {
		return aux_orcamentomaterial;
	}
	
	public void setAux_orcamentomaterial(
			Aux_orcamentomaterial aux_orcamentomaterial) {
		this.aux_orcamentomaterial = aux_orcamentomaterial;
	}
	
//	LISTAS
	
	@OneToMany(mappedBy="orcamentomaterial")
	public List<Orcamentomaterialitem> getListaOrcamentomaterialitem() {
		return listaOrcamentomaterialitem;
	}
	
	public void setListaOrcamentomaterialitem(
			List<Orcamentomaterialitem> listaOrcamentomaterialitem) {
		this.listaOrcamentomaterialitem = listaOrcamentomaterialitem;
	}


//	LOG

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
}
