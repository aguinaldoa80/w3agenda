package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_processojuridicoarquivo", sequenceName = "sq_processojuridicoarquivo")
@DisplayName("Arquivo")
public class Processojuridicoarquivo{
	
	protected Integer cdprocessojuridicoarquivo;
	protected String descricao;
	protected Arquivo arquivo;
	protected Processojuridico processojuridico;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_processojuridicoarquivo")
	public Integer getCdprocessojuridicoarquivo() {
		return cdprocessojuridicoarquivo;
	}
	@MaxLength(50)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprocessojuridico")
	public Processojuridico getProcessojuridico() {
		return processojuridico;
	}
	
	public void setCdprocessojuridicoarquivo(Integer cdprocessojuridicoarquivo) {
		this.cdprocessojuridicoarquivo = cdprocessojuridicoarquivo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setProcessojuridico(Processojuridico processojuridico) {
		this.processojuridico = processojuridico;
	}

}
