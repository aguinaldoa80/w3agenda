package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_modelodocumentofiscal", sequenceName = "sq_modelodocumentofiscal")
public class Modelodocumentofiscal implements Log {

	protected Integer cdmodelodocumentofiscal;
	protected String codigo;
	protected String nome;
    protected Boolean enviospedfiscal;
    protected Boolean enviospedpiscofins;
    protected Boolean spedfiscal_c100;
    protected Boolean spedfiscal_c140;
    protected Boolean spedfiscal_d100;
    protected Boolean spedfiscal_d500;
    protected Boolean spedfiscal_c500;
    protected Boolean spedpiscofins_a100;
    protected Boolean spedpiscofins_c100;
    protected Boolean spedpiscofins_f100;
    protected Boolean spedpiscofins_d100;
    protected Boolean spedpiscofins_c500;
    protected Boolean spedpiscofins_d500;
    protected Boolean sintegra_50;
    protected Boolean sintegra_51;
    protected Boolean sintegra_53;
    protected Boolean sintegra_54;
    protected Boolean sintegra_61;
    protected Boolean sintegra_70;
    protected Boolean sintegra_71;
    protected Boolean sintegra_76_77;
    protected Integer codigodominio;
    protected Boolean ctrc;
    protected Boolean apuracaopiscofins;
    protected Boolean apuracaoipi;
    protected Boolean apuracaoicms;
    protected Boolean obrigarchaveacesso;
    protected Boolean obrigartitulo;
    protected Boolean notafiscalprodutoeletronica;
    protected Boolean notafiscalservicoeletronica;
    protected Boolean notafiscalconsumidoreletronica;
    protected Boolean notafiscalaquisicaoenergiaeletricaeletronica;
    protected Boolean conhecimentotransporteeletronico;
    protected Boolean livroentrada;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_modelodocumentofiscal")
	public Integer getCdmodelodocumentofiscal() {
		return cdmodelodocumentofiscal;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getNome() {
		return nome;
	}

	@MaxLength(2)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@DisplayName("Envio ao SPED Fiscal")
	public Boolean getEnviospedfiscal() {
		return enviospedfiscal;
	}

	@DisplayName("Envio ao SPED PIS/COFINS")
	public Boolean getEnviospedpiscofins() {
		return enviospedpiscofins;
	}

	@DisplayName("C100")
	public Boolean getSpedfiscal_c100() {
		return spedfiscal_c100;
	}
	
	@DisplayName("C140")
	public Boolean getSpedfiscal_c140() {
		return spedfiscal_c140;
	}

	@DisplayName("D100")
	public Boolean getSpedfiscal_d100() {
		return spedfiscal_d100;
	}

	@DisplayName("D500")
	public Boolean getSpedfiscal_d500() {
		return spedfiscal_d500;
	}

	@DisplayName("C500")
	public Boolean getSpedfiscal_c500() {
		return spedfiscal_c500;
	}

	@DisplayName("A100")
	public Boolean getSpedpiscofins_a100() {
		return spedpiscofins_a100;
	}

	@DisplayName("C100")
	public Boolean getSpedpiscofins_c100() {
		return spedpiscofins_c100;
	}

	@DisplayName("F100")
	public Boolean getSpedpiscofins_f100() {
		return spedpiscofins_f100;
	}

	@DisplayName("D100")
	public Boolean getSpedpiscofins_d100() {
		return spedpiscofins_d100;
	}

	@DisplayName("C500")
	public Boolean getSpedpiscofins_c500() {
		return spedpiscofins_c500;
	}
	
	@DisplayName("D500")
	public Boolean getSpedpiscofins_d500() {
		return spedpiscofins_d500;
	}

	@DisplayName("Registros 50")
	public Boolean getSintegra_50() {
		return sintegra_50;
	}
	
	@DisplayName("Registros 51")
	public Boolean getSintegra_51() {
		return sintegra_51;
	}

	@DisplayName("Registros 53")
	public Boolean getSintegra_53() {
		return sintegra_53;
	}

	@DisplayName("Registros 54")
	public Boolean getSintegra_54() {
		return sintegra_54;
	}
	
	@DisplayName("Registros 61")
	public Boolean getSintegra_61() {
		return sintegra_61;
	}

	@DisplayName("Registro 70")
	public Boolean getSintegra_70() {
		return sintegra_70;
	}
	
	@DisplayName("Registro 71")
	public Boolean getSintegra_71() {
		return sintegra_71;
	}

	@DisplayName("Registros 76/77")
	public Boolean getSintegra_76_77() {
		return sintegra_76_77;
	}

	@DisplayName("C�digo da Intera��o Dom�nio")
	public Integer getCodigodominio() {
		return codigodominio;
	}

	public Boolean getCtrc() {
		return ctrc;
	}
	
	@DisplayName("Apura��o PIS/COFINS")
	public Boolean getApuracaopiscofins() {
		return apuracaopiscofins;
	}

	@DisplayName("Apura��o IPI")
	public Boolean getApuracaoipi() {
		return apuracaoipi;
	}

	@DisplayName("Apura��o ICMS")
	public Boolean getApuracaoicms() {
		return apuracaoicms;
	}

	@DisplayName("Obrigar Chave de Acesso")
	public Boolean getObrigarchaveacesso() {
		return obrigarchaveacesso;
	}

	@DisplayName("Obrigar T�tulo")
	public Boolean getObrigartitulo() {
		return obrigartitulo;
	}

	@DisplayName("NF-e")
	public Boolean getNotafiscalprodutoeletronica() {
		return notafiscalprodutoeletronica;
	}

	@DisplayName("NFS-e")
	public Boolean getNotafiscalservicoeletronica() {
		return notafiscalservicoeletronica;
	}
	@DisplayName("NFC-e")
	public Boolean getNotafiscalconsumidoreletronica() {
		return notafiscalconsumidoreletronica;
	}
	@DisplayName("NF3-e")
	public Boolean getNotafiscalaquisicaoenergiaeletricaeletronica() {
		return notafiscalaquisicaoenergiaeletricaeletronica;
	}
	@DisplayName("CT-e")
	public Boolean getConhecimentotransporteeletronico() {
		return conhecimentotransporteeletronico;
	}

	@DisplayName("Livro de entrada")
	public Boolean getLivroentrada() {
		return livroentrada;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setLivroentrada(Boolean livroentrada) {
		this.livroentrada = livroentrada;
	}

	public void setCdmodelodocumentofiscal(Integer cdmodelodocumentofiscal) {
		this.cdmodelodocumentofiscal = cdmodelodocumentofiscal;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setEnviospedfiscal(Boolean enviospedfiscal) {
		this.enviospedfiscal = enviospedfiscal;
	}

	public void setEnviospedpiscofins(Boolean enviospedpiscofins) {
		this.enviospedpiscofins = enviospedpiscofins;
	}

	public void setSpedfiscal_c100(Boolean spedfiscalC100) {
		spedfiscal_c100 = spedfiscalC100;
	}

	public void setSpedfiscal_d100(Boolean spedfiscalD100) {
		spedfiscal_d100 = spedfiscalD100;
	}

	public void setSpedfiscal_d500(Boolean spedfiscalD500) {
		spedfiscal_d500 = spedfiscalD500;
	}

	public void setSpedfiscal_c500(Boolean spedfiscalC500) {
		spedfiscal_c500 = spedfiscalC500;
	}

	public void setSpedpiscofins_a100(Boolean spedpiscofinsA100) {
		spedpiscofins_a100 = spedpiscofinsA100;
	}

	public void setSpedpiscofins_c100(Boolean spedpiscofinsC100) {
		spedpiscofins_c100 = spedpiscofinsC100;
	}

	public void setSpedpiscofins_f100(Boolean spedpiscofinsF100) {
		spedpiscofins_f100 = spedpiscofinsF100;
	}

	public void setSpedpiscofins_d100(Boolean spedpiscofinsD100) {
		spedpiscofins_d100 = spedpiscofinsD100;
	}

	public void setSpedpiscofins_c500(Boolean spedpiscofinsC500) {
		spedpiscofins_c500 = spedpiscofinsC500;
	}

	public void setSpedpiscofins_d500(Boolean spedpiscofinsD500) {
		spedpiscofins_d500 = spedpiscofinsD500;
	}

	public void setSintegra_50(Boolean sintegra_50) {
		this.sintegra_50 = sintegra_50;
	}
	
	public void setSintegra_51(Boolean sintegra_51) {
		this.sintegra_51 = sintegra_51;
	}

	public void setSintegra_53(Boolean sintegra_53) {
		this.sintegra_53 = sintegra_53;
	}

	public void setSintegra_54(Boolean sintegra_54) {
		this.sintegra_54 = sintegra_54;
	}
	
	public void setSintegra_61(Boolean sintegra_61) {
		this.sintegra_61 = sintegra_61;
	}

	public void setSintegra_70(Boolean sintegra_70) {
		this.sintegra_70 = sintegra_70;
	}
	
	public void setSintegra_71(Boolean sintegra_71) {
		this.sintegra_71 = sintegra_71;
	}

	public void setSintegra_76_77(Boolean sintegra_76_77) {
		this.sintegra_76_77 = sintegra_76_77;
	}

	public void setCodigodominio(Integer codigodominio) {
		this.codigodominio = codigodominio;
	}

	public void setCtrc(Boolean ctrc) {
		this.ctrc = ctrc;
	}

	public void setApuracaopiscofins(Boolean apuracaopiscofins) {
		this.apuracaopiscofins = apuracaopiscofins;
	}

	public void setApuracaoipi(Boolean apuracaoipi) {
		this.apuracaoipi = apuracaoipi;
	}

	public void setApuracaoicms(Boolean apuracaoicms) {
		this.apuracaoicms = apuracaoicms;
	}

	public void setObrigarchaveacesso(Boolean obrigarchaveacesso) {
		this.obrigarchaveacesso = obrigarchaveacesso;
	}

	public void setObrigartitulo(Boolean obrigartitulo) {
		this.obrigartitulo = obrigartitulo;
	}

	public void setNotafiscalprodutoeletronica(Boolean notafiscalprodutoeletronica) {
		this.notafiscalprodutoeletronica = notafiscalprodutoeletronica;
	}
	
	public void setNotafiscalconsumidoreletronica(Boolean notafiscalconsumidoreletronica) {
		this.notafiscalconsumidoreletronica = notafiscalconsumidoreletronica;
	}

	public void setNotafiscalservicoeletronica(Boolean notafiscalservicoeletronica) {
		this.notafiscalservicoeletronica = notafiscalservicoeletronica;
	}
	
	public void setNotafiscalaquisicaoenergiaeletricaeletronica(Boolean notafiscalaquisicaoenergiaeletricaeletronica) {
		this.notafiscalaquisicaoenergiaeletricaeletronica = notafiscalaquisicaoenergiaeletricaeletronica;
	}
	
	public void setConhecimentotransporteeletronico(Boolean conhecimentotransporteeletronico) {
		this.conhecimentotransporteeletronico = conhecimentotransporteeletronico;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setSpedfiscal_c140(Boolean spedfiscalC140) {
		spedfiscal_c140 = spedfiscalC140;
	}
	
}