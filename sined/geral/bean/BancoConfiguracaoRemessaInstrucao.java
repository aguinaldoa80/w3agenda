package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaInstrucaoEnum;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessainstrucao",sequenceName="sq_bancoconfiguracaoremessainstrucao")
public class BancoConfiguracaoRemessaInstrucao {

	private Integer cdbancoconfiguracaoremessainstrucao;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private Integer codigo;
	private String nome;
	private BancoConfiguracaoRemessaInstrucaoEnum instrucao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessainstrucao")
	public Integer getCdbancoconfiguracaoremessainstrucao() {
		return cdbancoconfiguracaoremessainstrucao;
	}
	
	@DisplayName("C�digo")
	@Required
	public Integer getCodigo() {
		return codigo;
	}
	
	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	@Required
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}
	
	@Required
	public BancoConfiguracaoRemessaInstrucaoEnum getInstrucao() {
		return instrucao;
	}

	public void setCdbancoconfiguracaoremessainstrucao(
			Integer cdbancoconfiguracaoremessainstrucao) {
		this.cdbancoconfiguracaoremessainstrucao = cdbancoconfiguracaoremessainstrucao;
	}

	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setInstrucao(BancoConfiguracaoRemessaInstrucaoEnum instrucao) {
		this.instrucao = instrucao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdbancoconfiguracaoremessainstrucao == null) ? 0
						: cdbancoconfiguracaoremessainstrucao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BancoConfiguracaoRemessaInstrucao other = (BancoConfiguracaoRemessaInstrucao) obj;
		if (cdbancoconfiguracaoremessainstrucao == null) {
			if (other.cdbancoconfiguracaoremessainstrucao != null)
				return false;
		} else if (!cdbancoconfiguracaoremessainstrucao
				.equals(other.cdbancoconfiguracaoremessainstrucao))
			return false;
		return true;
	}
	
}
