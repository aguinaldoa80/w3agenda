package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Calendar;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Documentoreferenciadoimposto;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@SequenceGenerator(name = "sq_documentoreferenciado", sequenceName = "sq_documentoreferenciado")
@JoinEmpresa("documentoreferenciado.empresa")
public class Documentoreferenciado implements Log, PermissaoProjeto{
	
	protected Integer cddocumentoreferenciado;
	protected String nome;
	protected Empresa empresa;
	protected ModelodocumentoEnum modelodocumento;
	protected Documentoreferenciadoimposto imposto;
	protected Integer diavencimento;
	protected Documentotipo documentotipo;
	protected Fornecedor fornecedor;
	protected Contagerencial contagerencial;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENT
	protected String identificadorinternoentregamaterial;
	protected Money valor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentoreferenciado")
	public Integer getCddocumentoreferenciado() {
		return cddocumentoreferenciado;
	}
	@Required
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Modelo do documento")
	public ModelodocumentoEnum getModelodocumento() {
		return modelodocumento;
	}
	@Required
	public Documentoreferenciadoimposto getImposto() {
		return imposto;
	}
	@Required
	@DisplayName("Dia de vencimento")
	@MaxLength(2)
	public Integer getDiavencimento() {
		return diavencimento;
	}
	@Required
	@DisplayName("Tipo de documento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Required
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@Required
	@DisplayName("Centro de custo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}


	public void setCddocumentoreferenciado(Integer cddocumentoreferenciado) {
		this.cddocumentoreferenciado = cddocumentoreferenciado;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setModelodocumento(ModelodocumentoEnum modelodocumento) {
		this.modelodocumento = modelodocumento;
	}
	public void setImposto(Documentoreferenciadoimposto imposto) {
		this.imposto = imposto;
	}
	public void setDiavencimento(Integer diavencimento) {
		this.diavencimento = diavencimento;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public String subQueryProjeto() {
		return "select documentoreferenciadosubQueryProjeto.cddocumentoreferenciado " +
				"from Documentoreferenciado documentoreferenciadosubQueryProjeto " +
				"left outer join documentoreferenciadosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}
	
	public boolean useFunctionProjeto() {
		return true;
	}
	

	//log
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cddocumentoreferenciado == null) ? 0 : cddocumentoreferenciado.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documentoreferenciado other = (Documentoreferenciado) obj;
		if (cddocumentoreferenciado == null) {
			if (other.cddocumentoreferenciado != null)
				return false;
		} else if (!cddocumentoreferenciado.equals(other.cddocumentoreferenciado))
			return false;
		return true;
	}

	@Transient
	public String getDatavencimentoStr(){
		String data = "";
		if(getDiavencimento() != null){
			data = SinedDateUtils.toString(getDatavencimento());
		}
		return data;
	}
	@Transient
	public Date getDatavencimento(){
		if(getDiavencimento() != null){
			try {
				Date data = SinedDateUtils.setDateProperty(new Date(System.currentTimeMillis()), getDiavencimento(), Calendar.DAY_OF_MONTH);
				data = SinedDateUtils.getProximaDataUtilProximoMes(data, empresa, false, true);
				return data;
			} catch (Exception e) {}
		}
		return null;
	}
	@Transient
	public String getImpostoValue(){
		return getImposto() != null ? getImposto().name() : "";
	}
	@Transient
	public String getIdentificadorinternoentregamaterial() {
		return identificadorinternoentregamaterial;
	}
	@Transient
	public Money getValor() {
		return valor;
	}
	
	public void setIdentificadorinternoentregamaterial(String identificadorinternoentregamaterial) {
		this.identificadorinternoentregamaterial = identificadorinternoentregamaterial;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
}
