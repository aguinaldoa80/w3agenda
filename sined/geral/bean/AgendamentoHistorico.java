package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.state.AgendamentoAcao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_agendamentohistorico",sequenceName="sq_agendamentohistorico")
public class AgendamentoHistorico implements Log {

	protected Integer cdAgendamentoHistorico;
	protected Agendamento agendamento;
	protected AgendamentoAcao agendamentoAcao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public AgendamentoHistorico() {}

	public AgendamentoHistorico(Integer cdAgendamentoHistorico, Agendamento agendamento, AgendamentoAcao agendamentoAcao,
								String observacao, Integer cdusuarioaltera, Timestamp dtaltera) {
		this.cdAgendamentoHistorico = cdAgendamentoHistorico;
		this.agendamento = agendamento;
		this.agendamentoAcao = agendamentoAcao;
		this.observacao = observacao;
		this.cdusuarioaltera = cdusuarioaltera;
		this.dtaltera = dtaltera;
	}



	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_agendamentohistorico",strategy=GenerationType.AUTO)
	public Integer getCdAgendamentoHistorico() {
		return cdAgendamentoHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamento")
	public Agendamento getAgendamento() {
		return agendamento;
	}
	
	@DisplayName("A��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagendamentoacao")
	public AgendamentoAcao getAgendamentoAcao() {
		return agendamentoAcao;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	public void setCdAgendamentoHistorico(Integer cdAgendamentoHistorico) {
		this.cdAgendamentoHistorico = cdAgendamentoHistorico;
	}
	
	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}
	
	public void setAgendamentoAcao(AgendamentoAcao agendamentoAcao) {
		this.agendamentoAcao = agendamentoAcao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
