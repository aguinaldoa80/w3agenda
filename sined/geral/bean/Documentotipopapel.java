package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_documentotipopapel", sequenceName = "sq_documentotipopapel")
public class Documentotipopapel {

	private Integer cddocumentotipopapel;
	private Documentotipo documentotipo;
	private Papel papel;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentotipopapel")
	public Integer getCddocumentotipopapel() {
		return cddocumentotipopapel;
	}
	public void setCddocumentotipopapel(Integer cddocumentotipopapel) {
		this.cddocumentotipopapel = cddocumentotipopapel;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentotipo")
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpapel")
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
}
