package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Formula;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Message;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.PneuItemVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizacaoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValoresPassiveisDeRateioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaovendasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.PagamentoTEFSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.bean.enumeration.Producaoagendasituacao;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@SequenceGenerator(name = "sq_venda", sequenceName = "sq_venda")
@DisplayName("Venda")
@JoinEmpresa("venda.empresa")
public class Venda implements Log, PermissaoProjeto, VendaMaterialProducaoInterface, TotalizacaoImpostoInterface, CalculoMarkupInterface, ValoresPassiveisDeRateioInterface{

	protected Integer cdvenda;
	protected Empresa empresa;
	protected Cliente cliente;
	protected Contato contato;
	protected Cliente clienteindicacao;
	protected Fornecedor parceiro;
	protected Money valorusadovalecompra;
	protected Money valoraproximadoimposto;
	protected Endereco endereco;
	protected Colaborador colaborador;
	protected Date dtvenda;
	protected Date dtestorno;
	protected String observacao;
	protected String observacaointerna;
	protected Presencacompradornfe presencacompradornfe;
	protected Vendasituacao vendasituacao;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected ResponsavelFrete frete;
	protected Fornecedor terceiro;
	protected Money valorfrete;
	protected Pedidovenda pedidovenda;
	protected String identificadorexterno;
	protected Integer identificadorcarregamento;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Money saldofinal;
	protected Projeto projeto;
	protected Boolean bonificacao = false;
	protected Boolean comodato = false;
	protected Money taxavenda;
	protected Localarmazenagem localarmazenagem;
	protected Materialtabelapreco materialtabelapreco;
	protected Expedicaovendasituacao expedicaovendasituacao;
	protected Expedicaosituacao expedicaosituacao;
	protected Vendaorcamento vendaorcamento;
	protected Boolean origemwebservice;
	protected Money valordescontorepresentacao;
	protected Money valorbrutorepresentacao;
	protected Boolean criacaofinalizada;
	protected Boolean aprovando;
	protected Boolean limitecreditoexcedido;
	protected Boolean vendaPendenciaNaoNegociada;
	
	protected List<Vendamaterial> listavendamaterial = new ListSet<Vendamaterial>(Vendamaterial.class);
	protected List<Vendapagamento> listavendapagamento = new ListSet<Vendapagamento>(Vendapagamento.class);
	protected List<Documentoorigem> listadocumentoorigem = new ListSet<Documentoorigem>(Documentoorigem.class);
	protected List<Vendahistorico> listavendahistorico = new ListSet<Vendahistorico>(Vendahistorico.class);
	protected Set<NotaVenda> listaNotavenda;
	protected List<Requisicao> listaRequisicao = new ListSet<Requisicao>(Requisicao.class);
	protected Set<Emporiumvenda> listaEmporiumvenda = new ListSet<Emporiumvenda>(Emporiumvenda.class);
	protected List<Vendamaterialmestre> listaVendamaterialmestre = new ListSet<Vendamaterialmestre>(Vendamaterialmestre.class);
	protected List<Vendavalorcampoextra> listaVendavalorcampoextra = new ListSet<Vendavalorcampoextra>(Vendavalorcampoextra.class);
	protected List<Vendapagamentorepresentacao> listavendapagamentorepresentacao = new ListSet<Vendapagamentorepresentacao>(Vendapagamentorepresentacao.class);
	protected List<VendaFornecedorTicketMedio> listaVendaFornecedorTicketMedio = new ListSet<VendaFornecedorTicketMedio>(VendaFornecedorTicketMedio.class);
	
	protected Indicecorrecao indicecorrecao;
	protected Conta contaboleto;
	protected String identificador;
	protected Money taxapedidovenda;
	protected Fornecedor fornecedor;
	protected String index;
	protected Fornecedor fornecedorAgencia;
	protected Money percentualRepresentacao;
	protected Centrocusto centrocusto;
	protected Money valorFreteCIF;
	protected Endereco enderecoFaturamento;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	//transintes
	private List<Producaoordemitemadicional> listaProducaoordemitemadicional = new ListSet<Producaoordemitemadicional>(Producaoordemitemadicional.class);
	private List<Producaoagendaitemadicional> listaProducaoagendaitemadicional = new ListSet<Producaoagendaitemadicional>(Producaoagendaitemadicional.class);
	private String codigo;
	private Material material;
	private Unidademedida unidademedida;
	private Double valor;
	private Double valormaximo;
	private Double valorminimo;
	private Money ultimovalor;
	private Money ultimoValorVenda;
	private Double quantidades = 1D;
	private Double pesomedio;
	private Double comprimentos;
	private Double comprimentosoriginal;
	private Double larguras;
	private Double alturas;
	private Double fatorconversaocomprimento;
	private Double qtdereferenciacomprimento; 
	private Double margemarredondamento;
	private Boolean vendapromocional;
	private Boolean kitflexivel;
	private Boolean existematerialsimilar;
	private Boolean existematerialsimilarColeta;
	private Loteestoque loteestoque;
	private Money ticketMedioCaculado;
	@SuppressWarnings("unused")
	private Money totalvenda = new Money();
	@SuppressWarnings("unused")
	private Money totalipi = new Money();
	@SuppressWarnings("unused")
	private Money totalparcela = new Money();
	@SuppressWarnings("unused")
	private Money totaljurosparcela;
	private Prazopagamento prazopagamento;
	private Documentotipo documentotipo;
	protected Date prazoentrega;
	private Money valorfinal;
	private Money valorfinalMaisImpostos;
	protected String placaveiculo;
	protected String numeronotadevolucao;
	protected String criadopor;
	protected Boolean confirmacaowms;
	protected Boolean valorParcelaRecalculado;
	private Integer qtdestoqueoriginal;
	private Integer qtdestoque;
	private Boolean aprovar;
	protected Boolean prazomedio;
	protected Integer qtdeParcelas;
	protected Boolean trocarvendedor;
	protected Colaborador colaboradoraux;
	protected String vendaMaterialnumeroserie;
	protected String ids;
	protected String observacaohistorico;
	protected List<Sugestaovenda> listaSugestaovenda;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean criarEntradaFiscal = Boolean.FALSE;
	protected Boolean registrarDevolucao = Boolean.FALSE;
	protected List<String> listaMateriaisAtualizacao;
	protected Vendaorcamentoformapagamento vendaformapagamento;
	protected String descricaoCombo;
	protected Boolean considerarvendamultiplos;
	protected Integer qtdeunidade;
	protected Boolean confirmadoparceladiferenteproduto;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Double totalpesobruto;
	protected Double multiplicador;
	protected Boolean metrocubicovalorvenda;
	protected Boolean existdevolucao;
	protected Boolean producaosemestoque;
	protected Boolean validaestoque;
	protected String identificacaomaterial;
	protected Date dtvendaEmissaonota;
	protected Patrimonioitem patrimonioitem;
	protected String whereInOportunidade;
	protected Producaoagendasituacao producaoagendasituacao;
	protected String nomeCliente;
	protected Material materialcoleta;
	protected String produto_anotacoes;
	protected Boolean documentoAposEmissaoNota;
	protected Boolean gerarnovasparcelas;
	protected Integer pagamentobanco;
	protected Integer pagamentoagencia;
	protected Integer pagamentoconta;
	protected String pagamentoprimeironumero;
	protected Money pagamentovalor;
	protected Date pagamentoDataUltimoVencimento;
	protected Boolean boletoEmitido;
	protected String whereInOSV;	
	protected Double totalPesoBruto;
	protected Double totalPesoLiquido;
	protected Integer quantidadevolumes;
	protected Boolean gerarnotaautomatico;
	protected Boolean isNotVerificarestoquePedidovenda = Boolean.FALSE;
	protected Money saldovalecompra;
	protected Money valorvalecompranovo;
	protected Boolean creditovalecompradevolucao = Boolean.FALSE;
	protected Prazopagamento prazopagamentorepresentacao;
	protected Documentotipo documentotiporepresentacao;
	protected Conta contaboletorepresentacao;
	protected Integer qtdevendapagamento;
	protected Integer qtdedocumento;
	protected Boolean replicarmaterial = Boolean.FALSE;
	protected Integer qtdereplicar;
	protected String observacaoFeedApplication;
	protected Boolean sucessoFeedApplication;
	protected Boolean salvarPagamentos;
	protected Boolean salvarItens;
	protected Date prazoentregaMinimo;
	protected Boolean valorSupeiorLimiteCredito;
	protected Date dtestoqueEmissaonota;
	protected Boolean gerouComissionamentoPorFaixas;
	protected java.sql.Date dtSaidaNotaFiscalProduto;
	protected Double qtdeEstoqueMenosReservada;
	protected Boolean origemOtr;
	protected String menorDtVencimentoLote;
	protected Boolean sinalizadoSemReserva = false;
	protected Boolean entregaEcommerceRealizada;
	
	//Transients para Registrar devolu��o
	protected Date devolucaodtchegada;
	protected Date devolucaodtemissao;
	protected Date devolucaodtlancamento;
	protected String devolucaonumero;
	protected String devolucaoplacaveiculo;
	protected String devolucaotipo;
	protected String devolucaotransportadora;
	protected String origemEntrada;
	protected Double limitepercentualdesconto;
	protected Boolean aprovacaopercentualdesconto;
	protected Arquivo arquivoxmlnfe;
	protected String chaveacesso;
	protected Boolean identificadorAutomatico = Boolean.TRUE;
	protected String vendedorprincipal;
	protected Double percentualcomissaoagencia;
	protected Date dtprazoentregamax;
	protected Money valorTotalRateio;
	protected Boolean cancelada;
	protected Boolean isDocumento;
	private Money valorFreteCalcCurvaAbc;
	private Money valorDescontoCalcCurvaAbc;
	private Integer posicaoVendaMaterialCurvaAbc;
	protected Boolean replicarPneu = Boolean.FALSE;
	protected Integer qtdeReplicarPneu;
	protected Boolean exibeIconeEntregaPendente;
	
	// Utilizados para enviar comprovante
	protected List<Contato> listaDestinatarios = new ListSet<Contato>(Contato.class);
	protected List<Arquivo> listaArquivos = new ListSet<Arquivo>(Arquivo.class);
	protected String remetente;
	
	//TRANSIENT para Verificar representantes dos materiais
	protected Money valorMaterialRepresentante;
	@SuppressWarnings("unused")
	private Money totalparcelarepresentacao = new Money();
	private Money valortotalrepresentacao = new Money();
	protected List<Fornecedor> listaFornecedor;
	
	protected Boolean registrarpesomedio;
	protected Double percentualdescontoTabelapreco;
	protected Money descontogarantiareforma;
	protected Garantiareformaitem garantiareformaitem;
	protected Integer ordem;
	protected List<Expedicao> listaExpedicaotrans;
	protected Double qtdereservada;
	protected Documentoacao emporiumDocumentoacao;
	protected List<Message> mensagemAposSalvar;
	protected Boolean fromWebService;
	protected List<Pneu> listaPneu = new ListSet<Pneu>(Pneu.class);
	protected Pneu pneuInclusao;
	protected Pneu pneu;
	protected List<PneuItemVendaInterface> listaItemAvulso;

	protected Money totalIcms;
	protected Money totalDesoneracaoIcms;
	protected Money totalIcmsSt;
	protected Money totalFcp;
	protected Money totalFcpSt;
	protected Money totalImpostos;
	protected Money totalValorSeguro;
	protected Money totalOutrasDespesas;
	protected Money totalDifal;
	protected Boolean incluirEnderecoFaturamento;
	protected Money valorSeguro;
	protected Money outrasdespesas;

	protected Boolean existeServico;
	protected Notafiscalproduto notaFiscalProdutoTrans;
	protected NotaFiscalServico notaFiscalServicoTrans;
	protected PagamentoTEFSituacao pagamentoTEFSituacao;
	protected Pedidovendasituacaoecommerce pedidovendasituacaoecommerce;
	protected String nomeSituacaoEcommerce;
	protected String identificadorOrCdvenda;
	
	private Money valorTotal;
	protected Expedicao expedicao;
	public Venda(){
	}

	public Venda(Integer cdvenda){
		this.cdvenda = cdvenda;
	}
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_venda")
	public Integer getCdvenda() {
		return cdvenda;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	@DisplayName("Contato")
	public Contato getContato() {
		return contato;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteindicacao")
	@DisplayName("Indica��o")
	public Cliente getClienteindicacao() {
		return clienteindicacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdparceiro")
	@DisplayName("Parceiro")
	public Fornecedor getParceiro() {
		return parceiro;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecoentrega")
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	
	@Transient
	@DisplayName("Prazo entrega")
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@DisplayName("Data da venda")
	public Date getDtvenda() {
		return dtvenda;
	}
	@DisplayName("Data do estorno")
	public Date getDtestorno() {
		return dtestorno;
	}
	
	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}
	
	@DisplayName("Observa��es Internas")
	@MaxLength(1000)
	public String getObservacaointerna() {
		return observacaointerna;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdvendasituacao")
	@DisplayName("Situa��o")
	public Vendasituacao getVendasituacao() {
		return vendasituacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdterceiro")
	public Fornecedor getTerceiro() {
		return terceiro;
	}
	
	@DisplayName("Valor do frete")
	public Money getValorfrete() {
		return valorfrete;
	}	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Identifica��o do carregamento")
	public Integer getIdentificadorcarregamento() {
		return identificadorcarregamento;
	}
	
	@Transient
	public String getWhereInOSV() {
		return whereInOSV;
	}

	public void setWhereInOSV(String whereInOSV) {
		this.whereInOSV = whereInOSV;
	}

	public Boolean getBonificacao() {
		return bonificacao;
	}
	
	public Boolean getComodato() {
		return comodato;
	}

	public Expedicaovendasituacao getExpedicaovendasituacao() {
		return expedicaovendasituacao;
	}
	
	public Expedicaosituacao getExpedicaosituacao() {
		return expedicaosituacao;
	}

	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	
	@DisplayName("Origem da opera��o")
	public Presencacompradornfe getPresencacompradornfe() {
		return presencacompradornfe;
	}
	
	public void setPresencacompradornfe(
			Presencacompradornfe presencacompradornfe) {
		this.presencacompradornfe = presencacompradornfe;
	}
	
	
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public void setExpedicaovendasituacao(Expedicaovendasituacao expedicaovendasituacao) {
		this.expedicaovendasituacao = expedicaovendasituacao;
	}
	
	public void setExpedicaosituacao(Expedicaosituacao expedicaosituacao) {
		this.expedicaosituacao = expedicaosituacao;
	}
	
	public void setBonificacao(Boolean bonificacao) {
		this.bonificacao = bonificacao;
	}
	
	public void setComodato(Boolean comodato) {
		this.comodato = comodato;
	}
	
	public void setIdentificadorcarregamento(Integer identificadorcarregamento) {
		this.identificadorcarregamento = identificadorcarregamento;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setTerceiro(Fornecedor terceiro) {
		this.terceiro = terceiro;
	}
	
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	
	public void setCdvenda(Integer cdvenda) {
		this.cdvenda = cdvenda;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public void setClienteindicacao(Cliente clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}	
	public void setParceiro(Fornecedor parceiro) {
		this.parceiro = parceiro;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtvenda(Date dtvenda) {
		this.dtvenda = dtvenda;
	}
	public void setDtestorno(Date dtestorno) {
		this.dtestorno = dtestorno;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}
	public void setVendasituacao(Vendasituacao vendasituacao) {
		this.vendasituacao = vendasituacao;
	}
	
	@OneToMany(mappedBy="venda")
	@DisplayName("Produtos")
	public List<Vendamaterial> getListavendamaterial() {
		return listavendamaterial;
	}
	
	@OneToMany(mappedBy="venda")
	@DisplayName("Pagamento")
	public List<Vendapagamento> getListavendapagamento() {
		return listavendapagamento;
	}

	public void setListavendapagamento(List<Vendapagamento> listavendapagamento) {
		this.listavendapagamento = listavendapagamento;
	}

	@OneToMany(mappedBy="venda")
	@DisplayName("Material Mestre")	
	public List<Vendamaterialmestre> getListaVendamaterialmestre() {
		return listaVendamaterialmestre;
	}

	public void setListaVendamaterialmestre(
			List<Vendamaterialmestre> listaVendamaterialmestre) {
		this.listaVendamaterialmestre = listaVendamaterialmestre;
	}
	
	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="venda")
	public List<Vendavalorcampoextra> getListaVendavalorcampoextra() {
		return listaVendavalorcampoextra;
	}

	@OneToMany(mappedBy="venda")
	@DisplayName("Pagamento")
	public List<Documentoorigem> getListadocumentoorigem() {
		return listadocumentoorigem;
	}

	@OneToMany(mappedBy="venda")
	@DisplayName("Hist�rico")
	public List<Vendahistorico> getListavendahistorico() {
		return listavendahistorico;
	}	
	
	@OneToMany(mappedBy="venda")
	public Set<NotaVenda> getListaNotavenda() {
		return listaNotavenda;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "frete")
	public ResponsavelFrete getFrete() {
		return frete;
	}
	
	public Money getTaxavenda() {
		return taxavenda;
	}
	
	public void setTaxavenda(Money taxavenda) {
		this.taxavenda = taxavenda;
	}
	
	public void setFrete(ResponsavelFrete frete) {
		this.frete = frete;
	}
	
	public void setListaNotavenda(Set<NotaVenda> listaNotavenda) {
		this.listaNotavenda = listaNotavenda;
	}
	
	public void setListaVendavalorcampoextra(List<Vendavalorcampoextra> listaVendavalorcampoextra) {
		this.listaVendavalorcampoextra = listaVendavalorcampoextra;
	}
	
	public void setListadocumentoorigem(
			List<Documentoorigem> listadocumentoorigem) {
		this.listadocumentoorigem = listadocumentoorigem;
	}
	public void setListavendamaterial(List<Vendamaterial> listavendamaterial) {
		this.listavendamaterial = listavendamaterial;
	}
	public void setListavendahistorico(List<Vendahistorico> listavendahistorico) {
		this.listavendahistorico = listavendahistorico;
	}
	
	//LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getItensVendaString(){
		List<Vendamaterial> listavendamaterial2 = getListavendamaterial();
		if (listavendamaterial2 != null && listavendamaterial2.size() > 5)
			return "Diversos";
		String string = "";
		if(listavendamaterial2 != null && listavendamaterial2.size()>0){
			for (Vendamaterial vendamaterial : listavendamaterial2) {
				string += (vendamaterial.getQuantidade()!=null ? new DecimalFormat("#,##0.#########").format(vendamaterial.getQuantidade()) : 0) + " - " + vendamaterial.getMaterial().getNome() + " -  R$ " + SinedUtil.descriptionDecimal(vendamaterial.getPreco(), true) + "<BR>";
			}
		}
		return string;
	}
	
	@Formula("(select max(vi.prazoentrega) from Vendamaterial vi where vi.cdvenda = cdvenda)")
	@Basic(fetch=FetchType.LAZY)
	public Date getDtprazoentregamax() {
		return dtprazoentregamax;
	}
	
	public void setDtprazoentregamax(Date dtprazoentregamax) {
		this.dtprazoentregamax = dtprazoentregamax;
	}
	
	@Transient
	public String getItensVendaStringRelatorio(){
		List<Vendamaterial> listavendamaterial2 = getListavendamaterial();
		if (listavendamaterial2 != null && listavendamaterial2.size() > 5)
			return "Diversos";
		String string = "";
		if(listavendamaterial2 != null && listavendamaterial2.size()>0){
			for (Vendamaterial vendamaterial : listavendamaterial2) {
				string += vendamaterial.getMaterial().getNome() + " -  R$ " + SinedUtil.descriptionDecimal(vendamaterial.getPreco(), true) + "\n";
			}
		}
		return string;
	}
	
	@Transient
	@DisplayName("C�digo")
	@MaxLength(14)
	public String getCodigo() {
		return codigo;
	}

	@Transient
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}

	@Transient
	@DisplayName("Valor")
	public Double getValor() {
		return valor;
	}
	
	@Transient
	@DisplayName("Quantidade")
	public Double getQuantidades() {
		return quantidades;
	}
	
	@Transient
	@DisplayName("Peso m�dio")
	public Double getPesomedio() {
		return pesomedio;
	}
	
	@Transient
	public Double getComprimentos() {
		return comprimentos;
	}
	@Transient
	public Double getComprimentosoriginal() {
		return comprimentosoriginal;
	}

	@Transient
	public Double getLarguras() {
		return larguras;
	}
	@Transient
	public Double getAlturas() {
		return alturas;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setQuantidades(Double quantidades) {
		this.quantidades = quantidades;
	}
	public void setPesomedio(Double pesomedio) {
		this.pesomedio = pesomedio;
	}
	public void setComprimentos(Double comprimentos) {
		this.comprimentos = comprimentos;
	}
	public void setComprimentosoriginal(Double comprimentosoriginal) {
		this.comprimentosoriginal = comprimentosoriginal;
	}	
	public void setAlturas(Double alturas) {
		this.alturas = alturas;
	}
	public void setLarguras(Double larguras) {
		this.larguras = larguras;
	}
	

	@DisplayName ("Total")
	@Transient
	public Money getTotalvenda() {
		Money valor = new Money(0);
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 0 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				Double valorTruncado = SinedUtil.round(BigDecimal.valueOf(vendamaterial.getPreco().doubleValue() * qtde * multiplicador), 2).doubleValue();
				valor = valor.add(new Money(valorTruncado))
							.add(vendamaterial.getOutrasdespesas())
							.add(vendamaterial.getValorSeguro())
							.subtract(vendamaterial.getDesconto());
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? this.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getTotalvendaimpostos() {
		Money valor = getTotalvenda();
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(Boolean.TRUE.equals(item.getAbaterdesoneracaoicms())){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				

				if(Boolean.TRUE.equals(item.getAbaterdesoneracaoicms())){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@Transient
	public Money getTotalvendaMaisImpostosListagem() {
		Money valor = getTotalvendaListagem();
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@DisplayName ("Total")
	@Transient
	public Money getTotalvenda(Boolean truncar, boolean pularQuantidadeZerada) {
		Money valor = new Money(0);
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				if(pularQuantidadeZerada && vendamaterial.getQuantidade() != null && vendamaterial.getQuantidade() == 0){
					continue;
				}
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				Double preco = vendamaterial.getPreco() != null ? vendamaterial.getPreco() : 0d;
				if(truncar){
					Double valorTruncado = SinedUtil.roundFloor(BigDecimal.valueOf(preco.doubleValue() * qtde * multiplicador), 2).doubleValue();
					valor = valor.add(new Money(valorTruncado));
				}else {
					Double v = BigDecimal.valueOf(SinedUtil.round(preco.doubleValue() * qtde * multiplicador, 2)).doubleValue();
					valor = valor.add(new Money(v));
				}
				valor = valor.subtract(vendamaterial.getDesconto())
							.add(vendamaterial.getOutrasdespesas())
							.add(vendamaterial.getValorSeguro());
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? 
				this.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	@DisplayName ("Total")
	@Transient
	public Money getTotalvenda(Boolean truncar) {
		return getTotalvenda(truncar, false);
	}
	
	@Transient
	public Money getTotalvendaListagem() {
		return getTotalvenda(false, true);
	}
	
	@Transient
	public Money getTotalvendaValidacao() {
		Money valor = new Money(0);
		Double valorProdutos = 0d;
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				Double preco = vendamaterial.getPreco() != null ? vendamaterial.getPreco() : 0d;
				Double valorItem = preco.doubleValue() * qtde * multiplicador;
				if(vendamaterial.getDesconto() != null){
					valorItem = valorItem - vendamaterial.getDesconto().getValue().doubleValue();
				}
				valorProdutos += SinedUtil.round(BigDecimal.valueOf(valorItem),2).doubleValue();
			}
		}
		valor = new Money(SinedUtil.roundFloor(BigDecimal.valueOf(valorProdutos), 2));
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? 
				this.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getTotalvendaTruncado() {
		return this.getTotalvenda(true);
	}
	
	@Transient
	public Money getTotalvendaNotTruncado() {
		return this.getTotalvenda(false);
	}
	
	@Transient
	public Money getTotalvendaSemValecompra() {
		Money valor = new Money(0);
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				
				valor = valor.add(new Money(vendamaterial.getPreco().doubleValue() * qtde * multiplicador));
				if(vendamaterial.getDesconto() != null){
					valor = valor.subtract(vendamaterial.getDesconto());
				}
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Double getTotalquantidades(){
		Double total = 0d;
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				total += vendamaterial.getQuantidade();
			}
		}
		return total;
	}
	
	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}
	
	@Transient
	public Money getTotalparcela() {
		Money valor = new Money(0);
		if(this.listavendapagamento != null && !this.listavendapagamento.isEmpty()){
			for (Vendapagamento vendapagamento : this.listavendapagamento) {
				if(vendapagamento.getValororiginal() != null){
					valor = valor.add(new Money(vendapagamento.getValororiginal()));
				}
			}
		}		
		return valor;		
	}
	public void setTotalparcela(Money totalparcela) {
		this.totalparcela = totalparcela;
	}
	@Transient
	public Money getTotaljurosparcela() {
		Money valor = new Money(0);
		if(this.listavendapagamento != null && !this.listavendapagamento.isEmpty()){
			for (Vendapagamento pagamento : this.listavendapagamento) {
				if(pagamento.getValorjuros() != null){
					valor = valor.add(pagamento.getValorjuros());
				}
			}
		}		
		return valor;		
	}
	public void setTotaljurosparcela(Money totaljurosparcela) {
		this.totaljurosparcela = totaljurosparcela;
	}
	
	@DisplayName("Condi��o de Pagamento")
	@JoinColumn(name="cdprazopagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	@Transient
	@DisplayName("Valor m�ximo")
	public Double getValormaximo() {
		return valormaximo;
	}
	@Transient
	@DisplayName("Valor m�nimo")
	public Double getValorminimo() {
		return valorminimo;
	}
	@Transient
	@DisplayName("Sugest�o")
	public Money getUltimovalor() {
		return ultimovalor;
	}
	public void setUltimovalor(Money ultimovalor) {
		this.ultimovalor = ultimovalor;
	}
	@Transient
	@DisplayName("�ltimo Valor")
	public Money getUltimoValorVenda() {
		return ultimoValorVenda;
	}
	public void setUltimoValorVenda(Money ultimoValorVenda) {
		this.ultimoValorVenda = ultimoValorVenda;
	}
	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}
	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}
	@Transient
	@DisplayName("Qtde. dispon�vel")
	public Integer getQtdestoque() {
		return qtdestoque;
	}
	public void setQtdestoque(Integer qtdestoque) {
		this.qtdestoque = qtdestoque;
	}
	
	@DisplayName("Forma de pagamento")
	@JoinColumn(name="cdformapagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotipo() {
		return documentotipo;
	}
	public void setDocumentotipo(Documentotipo documentotipo) {
		this.documentotipo = documentotipo;
	}
	
	@DisplayName("�ndice de corre��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindicecorrecao")
	public Indicecorrecao getIndicecorrecao() {
		return indicecorrecao;
	}
	@JoinColumn(name="cdconta")
	@DisplayName("Conta para Boleto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getContaboleto() {
		return contaboleto;
	}
	
	public void setIndicecorrecao(Indicecorrecao indicecorrecao) {
		this.indicecorrecao = indicecorrecao;
	}
	public void setContaboleto(Conta contaboleto) {
		this.contaboleto = contaboleto;
	}
	
	@Transient
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	@Transient
	public Boolean getAprovar() {
		return aprovar;
	}
	public void setAprovar(Boolean aprovar) {
		this.aprovar = aprovar;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamento")
	public Vendaorcamento getVendaorcamento() {
		return vendaorcamento;
	}
	
	public void setVendaorcamento(Vendaorcamento vendaorcamento) {
		this.vendaorcamento = vendaorcamento;
	}

	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}

	@MaxLength(200)
	@DisplayName("Identificador Externo")
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}

	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}

	@DisplayName("Desconto")
	public Money getDesconto() {
		return desconto;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	@Transient
	@DisplayName("Valor Final")
	public Money getValorfinal() {
		return valorfinal;
	}

	public void setValorfinal(Money valorfinal) {
		this.valorfinal = valorfinal;
	}
	
	public Boolean getPrazomedio() {
		return prazomedio;
	}

	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	
	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}
	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}
	@DisplayName("Valor final")
	public Money getSaldofinal() {
		return saldofinal;
	}
	public void setSaldofinal(Money saldofinal) {
		this.saldofinal = saldofinal;
	}
	@Transient
	@DisplayName("Unidade de medida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	@Transient
	@DisplayName("Venda promocional")
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	
	@Transient
	public Boolean getKitflexivel() {
		return kitflexivel;
	}
	@Transient
	public Boolean getIsDocumento() {
		return isDocumento;
	}
	
	public void setKitflexivel(Boolean kitflexivel) {
		this.kitflexivel = kitflexivel;
	}
	@Transient
	public Money getValorFreteCalcCurvaAbc() {
		return valorFreteCalcCurvaAbc;
	}
	public void setValorFreteCalcCurvaAbc(Money valorFreteCalcCurvaAbc) {
		this.valorFreteCalcCurvaAbc = valorFreteCalcCurvaAbc;
	}
	public void addValorFreteCalcCurvaAbc(Money valorFreteCalcCurvaAbc) {
		if (this.valorFreteCalcCurvaAbc == null) {
			this.valorFreteCalcCurvaAbc = new Money();
		}
		
		this.valorFreteCalcCurvaAbc = this.valorFreteCalcCurvaAbc.add(valorFreteCalcCurvaAbc);
	}
	@Transient
	public Money getValorDescontoCalcCurvaAbc() {
		return valorDescontoCalcCurvaAbc;
	}
	public void setValorDescontoCalcCurvaAbc(Money valorDescontoCalcCurvaAbc) {
		this.valorDescontoCalcCurvaAbc = valorDescontoCalcCurvaAbc;
	}
	public void addValorDescontoCalcCurvaAbc(Money valorDescontoCalcCurvaAbc) {
		if (this.valorDescontoCalcCurvaAbc == null) {
			this.valorDescontoCalcCurvaAbc = new Money();
		}
		
		this.valorDescontoCalcCurvaAbc = this.valorDescontoCalcCurvaAbc.add(valorDescontoCalcCurvaAbc);
	}
	@Transient
	public Integer getPosicaoVendaMaterialCurvaAbc() {
		return posicaoVendaMaterialCurvaAbc;
	}
	
	public void setPosicaoVendaMaterialCurvaAbc(Integer posicaoVendaMaterialCurvaAbc) {
		this.posicaoVendaMaterialCurvaAbc = posicaoVendaMaterialCurvaAbc;
	}
	
	public void addPosicaoVendaMaterialCurvaAbc() {
		if (this.posicaoVendaMaterialCurvaAbc == null) {
			this.posicaoVendaMaterialCurvaAbc = 0;
		}
		
		this.posicaoVendaMaterialCurvaAbc = this.posicaoVendaMaterialCurvaAbc + 1;
	}
	public void setIsDocumento(Boolean isDocumento) {
		this.isDocumento = isDocumento;
	}
	
	public String subQueryProjeto() {
		return "otimizacao_venda_projeto";
//		return "select vendasubQueryProjeto.cdvenda " +
//				"from Venda vendasubQueryProjeto " +
//				"left outer join vendasubQueryProjeto.projeto projetosubQueryProjeto " +
//				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}		

	public boolean useFunctionProjeto() {
		return true;
	}

	@Transient
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@Transient
	public String getNumeronotadevolucao() {
		return numeronotadevolucao;
	}

	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setNumeronotadevolucao(String numeronotadevolucao) {
		this.numeronotadevolucao = numeronotadevolucao;
	}

	@Transient
	public Boolean getTrocarvendedor() {
		return trocarvendedor;
	}
	public void setTrocarvendedor(Boolean trocarvendedor) {
		this.trocarvendedor = trocarvendedor;
	}
	@Transient
	public Colaborador getColaboradoraux() {
		return colaboradoraux;
	}
	public void setColaboradoraux(Colaborador colaboradoraux) {
		this.colaboradoraux = colaboradoraux;
	}
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	@Transient
	@DisplayName("Data de Chegada")
	public Date getDevolucaodtchegada() {
		return devolucaodtchegada;
	}
	@Transient
	@DisplayName("Data de Emiss�o")
	public Date getDevolucaodtemissao() {
		return devolucaodtemissao;
	}
	@Transient
	@DisplayName("Data de Lan�amento")
	public Date getDevolucaodtlancamento() {
		return devolucaodtlancamento;
	}
	@Transient
	@DisplayName("N�mero")
	public String getDevolucaonumero() {
		return devolucaonumero;
	}
	@Transient
	@DisplayName("Placa do Ve�culo")
	public String getDevolucaoplacaveiculo() {
		return devolucaoplacaveiculo;
	}
	@Transient
	@DisplayName("Tipo")
	public String getDevolucaotipo() {
		return devolucaotipo;
	}
	@Transient
	@DisplayName("Transportadora")
	public String getDevolucaotransportadora() {
		return devolucaotransportadora;
	}

	public void setDevolucaodtchegada(Date devolucaodtchegada) {
		this.devolucaodtchegada = devolucaodtchegada;
	}
	public void setDevolucaodtemissao(Date devolucaodtemissao) {
		this.devolucaodtemissao = devolucaodtemissao;
	}
	public void setDevolucaodtlancamento(Date devolucaodtlancamento) {
		this.devolucaodtlancamento = devolucaodtlancamento;
	}
	public void setDevolucaonumero(String devolucaonumero) {
		this.devolucaonumero = devolucaonumero;
	}
	public void setDevolucaoplacaveiculo(String devolucaoplacaveiculo) {
		this.devolucaoplacaveiculo = devolucaoplacaveiculo;
	}
	public void setDevolucaotipo(String devolucaotipo) {
		this.devolucaotipo = devolucaotipo;
	}
	public void setDevolucaotransportadora(String devolucaotransportadora) {
		this.devolucaotransportadora = devolucaotransportadora;
	}

	@Transient
	public String getOrigemEntrada() {
		return origemEntrada;
	}

	public void setOrigemEntrada(String origemEntrada) {
		this.origemEntrada = origemEntrada;
	}

	@DisplayName("Venda")
	@Transient
	public String getVendaMaterialnumeroserie() {
		StringBuilder s = new StringBuilder();
		if(dtvenda != null)
			s.append(new SimpleDateFormat("dd/MM/yyyy").format(dtvenda));
		
		if(cliente != null && cliente.getNome() != null && !"".equals(cliente.getNome())){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(cliente.getNome());
		}
		
		return s.toString();
	}	
	
	@OneToMany(mappedBy="venda")
	public List<Requisicao> getListaRequisicao() {
		return listaRequisicao;
	}
	
	@OneToMany(mappedBy="venda")
	public Set<Emporiumvenda> getListaEmporiumvenda() {
		return listaEmporiumvenda;
	}
	
	public void setListaEmporiumvenda(Set<Emporiumvenda> listaEmporiumvenda) {
		this.listaEmporiumvenda = listaEmporiumvenda;
	}

	public void setListaRequisicao(List<Requisicao> listaRequisicao) {
		this.listaRequisicao = listaRequisicao;
	}

	@Transient
	@DisplayName("Qtde principal dispon�vel")
	public Integer getQtdestoqueoriginal() {
		return qtdestoqueoriginal;
	}

	public void setQtdestoqueoriginal(Integer qtdestoqueoriginal) {
		this.qtdestoqueoriginal = qtdestoqueoriginal;
	}
	
	@Transient
	public String getCriadopor() {
		return criadopor;
	}
	
	public void setCriadopor(String criadopor) {
		this.criadopor = criadopor;
	}
	
	@Transient
	public Boolean getConfirmacaowms() {
		return confirmacaowms;
	}	
	public void setConfirmacaowms(Boolean confirmacaowms) {
		this.confirmacaowms = confirmacaowms;
	}
	
	@Transient
	public Boolean getValorParcelaRecalculado() {
		return valorParcelaRecalculado;
	}

	public void setValorParcelaRecalculado(Boolean valorParcelaRecalculado) {
		this.valorParcelaRecalculado = valorParcelaRecalculado;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	@Transient
	public String getIds() {
		return ids;
	}	
	@Transient
	public String getObservacaohistorico() {
		return observacaohistorico;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}
	
	@DisplayName("Taxa de Pedido de Venda")
	public Money getTaxapedidovenda() {
		return taxapedidovenda;
	}

	public void setTaxapedidovenda(Money taxapedidovenda) {
		this.taxapedidovenda = taxapedidovenda;
	}

	@Transient
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}
	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}
	@Transient
	public Double getFatorconversaocomprimento() {
		return fatorconversaocomprimento;
	}
	public void setFatorconversaocomprimento(Double fatorconversaocomprimento) {
		this.fatorconversaocomprimento = fatorconversaocomprimento;
	}
	
	@DisplayName("Tipo de pedido de venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	@Transient
	public List<Sugestaovenda> getListaSugestaovenda() {
		return listaSugestaovenda;
	}
	
	public void setListaSugestaovenda(List<Sugestaovenda> listaSugestaovenda) {
		this.listaSugestaovenda = listaSugestaovenda;
	}

	@Transient
	public Money getValorMaterialRepresentante() {
		return valorMaterialRepresentante;
	}
	public void setValorMaterialRepresentante(Money valorMaterialRepresentante) {
		this.valorMaterialRepresentante = valorMaterialRepresentante;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	@DisplayName("Tabela de Pre�os")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	
	@Transient
	@DisplayName("Criar Entrada Fiscal?")
	public Boolean getCriarEntradaFiscal() {
		return criarEntradaFiscal;
	}
	
	public void setCriarEntradaFiscal(Boolean criarEntradaFiscal) {
		this.criarEntradaFiscal = criarEntradaFiscal;
	}
	
	@Transient
	public Boolean getRegistrarDevolucao() {
		return registrarDevolucao;
	}
	
	public void setRegistrarDevolucao(Boolean registrarDevolucao) {
		this.registrarDevolucao = registrarDevolucao;
	}
	
	@Transient
	public Double getLimitepercentualdesconto() {
		return limitepercentualdesconto;
	}
	
	public void setLimitepercentualdesconto(Double limitepercentualdesconto) {
		this.limitepercentualdesconto = limitepercentualdesconto;
	}
	
	@Transient
	public Boolean getAprovacaopercentualdesconto() {
		return aprovacaopercentualdesconto;
	}
	
	public void setAprovacaopercentualdesconto(
			Boolean aprovacaopercentualdesconto) {
		this.aprovacaopercentualdesconto = aprovacaopercentualdesconto;
	}

	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	@DisplayName("Lote")
	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	@Transient
	public List<String> getListaMateriaisAtualizacao() {
		return listaMateriaisAtualizacao;
	}
	
	public void setListaMateriaisAtualizacao(
			List<String> listaMateriaisAtualizacao) {
		this.listaMateriaisAtualizacao = listaMateriaisAtualizacao;
	}
	
	@Transient
	public Vendaorcamentoformapagamento getVendaformapagamento() {
		return vendaformapagamento;
	}
	
	public void setVendaformapagamento(
			Vendaorcamentoformapagamento vendaformapagamento) {
		this.vendaformapagamento = vendaformapagamento;
	}
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	
	@DisplayName("Valor aproximado dos impostos (IBPT)")
	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}

	@DisplayName("Venda")
	@Transient
	public String getDescricaoCombo() {
		StringBuilder s = new StringBuilder();
		if(this.cdvenda != null){
			s.append(this.cdvenda);
		}
		if(this.dtvenda != null){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(new SimpleDateFormat("dd/MM/yyyy").format(dtvenda));
		}
		return s.toString();
	}
	public void setDescricaoCombo(String descricaoCombo) {
		this.descricaoCombo = descricaoCombo;
	}

	@Transient
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}
	@Transient
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}
	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}

	@Transient
	public Boolean getConfirmadoparceladiferenteproduto() {
		return confirmadoparceladiferenteproduto;
	}
	public void setConfirmadoparceladiferenteproduto(Boolean confirmadoparceladiferenteproduto) {
		this.confirmadoparceladiferenteproduto = confirmadoparceladiferenteproduto;
	}
	@Transient
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}

	@Transient
	public Double getTotalpesoliquido() {
		Double totalpeso = 0.0;
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : listavendamaterial) {
				if(vendamaterial.getPesoVendaOuMaterial() != null)
					totalpeso +=  vendamaterial.getPesoVendaOuMaterial();
			}
		}
		return SinedUtil.roundByParametro(totalpeso , 2);
	}
	@Transient
	public Double getTotalpesobruto() {
		Double totalpeso = 0.0;
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : listavendamaterial) {
				if(vendamaterial.getMaterial() != null && vendamaterial.getMaterial().getPesobruto() != null)
					totalpeso +=  vendamaterial.getMaterial().getPesobruto();
			}
		}
		return SinedUtil.roundByParametro(totalpeso, 2);
	}
	public void setTotalpesobruto(Double totalpesobruto) {
		this.totalpesobruto = totalpesobruto;
	}
	@Transient
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}

	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	@Transient
	public Double getQtdereferenciacomprimento() {
		return qtdereferenciacomprimento;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setQtdereferenciacomprimento(Double qtdereferenciacomprimento) {
		this.qtdereferenciacomprimento = qtdereferenciacomprimento;
	}

	@Transient
	public Double getMultiplicador() {
		return multiplicador;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}

	@Transient
	public Arquivo getArquivoxmlnfe() {
		return arquivoxmlnfe;
	}
	@Transient
	public String getChaveacesso() {
		return chaveacesso;
	}

	public void setArquivoxmlnfe(Arquivo arquivoxmlnfe) {
		this.arquivoxmlnfe = arquivoxmlnfe;
	}
	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}
	
	@Transient
	public Boolean getMetrocubicovalorvenda() {
		return metrocubicovalorvenda;
	}
	public void setMetrocubicovalorvenda(Boolean metrocubicovalorvenda) {
		this.metrocubicovalorvenda = metrocubicovalorvenda;
	}
	
	@Transient
	public Boolean getProducaosemestoque() {
		return producaosemestoque;
	}
	
	public void setProducaosemestoque(Boolean producaosemestoque) {
		this.producaosemestoque = producaosemestoque;
	}
	
	@Transient
	public Boolean getExistdevolucao() {
		return existdevolucao;
	}
	public void setExistdevolucao(Boolean existdevolucao) {
		this.existdevolucao = existdevolucao;
	}
	
	@Transient
	public Date getDataentregaTrans(){
		java.sql.Date date = null;
		
		if(this.getListavendamaterial() != null && !this.getListavendamaterial().isEmpty()){
			for(Vendamaterial vendamaterial : this.getListavendamaterial()){
				if(vendamaterial.getPrazoentrega() != null){
					if(date == null){
						date = new java.sql.Date(vendamaterial.getPrazoentrega().getTime());
					}else if(SinedDateUtils.beforeIgnoreHour(date, (java.sql.Date)  new java.sql.Date(vendamaterial.getPrazoentrega().getTime()))){
						date = new java.sql.Date(vendamaterial.getPrazoentrega().getTime());
					}
				}
			}
		}
		
		return date;
	}

	@Transient
	public Date getDtvendaEmissaonota() {
		return dtvendaEmissaonota;
	}
	public void setDtvendaEmissaonota(Date dtvendaEmissaonota) {
		this.dtvendaEmissaonota = dtvendaEmissaonota;
	}
	
	@Transient
	public java.sql.Date getDtSaidaNotaFiscalProduto() {
		return dtSaidaNotaFiscalProduto;
	}
	public void setDtSaidaNotaFiscalProduto(java.sql.Date dtSaidaNotaFiscalProduto) {
		this.dtSaidaNotaFiscalProduto = dtSaidaNotaFiscalProduto;
	}
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@Transient
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}

	@Transient
	public String getWhereInOportunidade() {
		return whereInOportunidade;
	}

	public void setWhereInOportunidade(String whereInOportunidade) {
		this.whereInOportunidade = whereInOportunidade;
	}
	@Transient
	public Boolean getValidaestoque() {
		return validaestoque;
	}
	public void setValidaestoque(Boolean validaestoque) {
		this.validaestoque = validaestoque;
	}

	@Transient
	public Money getTotalmaterial() {
		return getValorTotalmaterial(true);
	}
	
	@Transient
	public Money getTotalmaterialSemDesconto() {
		return getValorTotalmaterial(false);
	}
	
	@Transient
	public Money getValorTotalmaterial(Boolean considerarDesconto) {
		Money totalmaterial = new Money();
		if(listavendamaterial != null && !listavendamaterial.isEmpty()){
			for (Vendamaterial vm : listavendamaterial) {
				Double qtde = vm.getQuantidade() == null || vm.getQuantidade() == 0 ? 1 : vm.getQuantidade();
				Double multiplicador = vm.getMultiplicador() != null && vm.getMultiplicador() != 0 ? vm.getMultiplicador() : 1d;
				totalmaterial = totalmaterial.add(new Money(vm.getPreco().doubleValue() * qtde * multiplicador));
				if(considerarDesconto != null && considerarDesconto && vm.getDesconto() != null){
					totalmaterial = totalmaterial.subtract(vm.getDesconto());
				}
			}
		}
		return totalmaterial;
	}
	
	@Transient
	public String getCodigodtvenda(){
		String s = "";
		if(getCdvenda() != null){
			s = getCdvenda().toString();
		}
		if(getDtvenda() != null){
			if(s != null && !"".equals(s)) s += " - ";
			s += new SimpleDateFormat("dd/MM/yyyy").format(getDtvenda());
		}
		return s;
	}

	@Transient
	public Producaoagendasituacao getProducaoagendasituacao() {
		return producaoagendasituacao;
	}

	public void setProducaoagendasituacao(Producaoagendasituacao producaoagendasituacao) {
		this.producaoagendasituacao = producaoagendasituacao;
	}
	
	@Transient
	public String getVendaAutocompleteForExpedicao(){
		StringBuilder s = new StringBuilder();
		
		if(this.getCdvenda() != null){
			s.append(this.getCdvenda());
			
			if(this.getDtvenda() != null){					
				s.append(" - ").append(new SimpleDateFormat("dd/MM/yyyy").format(this.getDtvenda()));
			}
			
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
			
			try {
				if(this.getListaNotavenda() != null && !this.getListaNotavenda().isEmpty()){
					StringBuilder numeroNota = new StringBuilder();
					for(NotaVenda notaVenda : this.getListaNotavenda()){
						if(notaVenda.getNota() != null && notaVenda.getNota().getNumero() != null && 
								!"".equals(notaVenda.getNota().getNumero()) &&
								notaVenda.getNota().getNotaStatus() != null && 
								!notaVenda.getNota().getNotaStatus().equals(NotaStatus.CANCELADA)){
							numeroNota.append(notaVenda.getNota().getNumero()).append(",");
						}
					}
					if(!numeroNota.toString().equals("")){
						s.append(" - Nota: " + numeroNota.toString().substring(0, numeroNota.length()-1));
					}
				}
			} catch (Exception e) {}
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}

	@Transient
	public String getNomeCliente() {
		if(getCliente() != null && getCliente().getNome() != null){
			nomeCliente = getCliente().getNome();
		}
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	} 
	
	@Transient
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	
	@Transient
	public Boolean getExistematerialsimilarColeta() {
		return existematerialsimilarColeta;
	}
	
	public void setExistematerialsimilarColeta(
			Boolean existematerialsimilarColeta) {
		this.existematerialsimilarColeta = existematerialsimilarColeta;
	}
	
	@Transient
	public String getDtvendastr(){
		String datastr = "";
		if(this.dtvenda != null) datastr = new SimpleDateFormat("dd/MM/yyyy").format(this.dtvenda);
		return datastr;
	}
	
	@Transient
	@DisplayName("Anota��es")
	public String getProduto_anotacoes() {
		return produto_anotacoes;
	}

	public void setProduto_anotacoes(String produtoAnotacoes) {
		produto_anotacoes = produtoAnotacoes;
	}
	
	@Transient
	public Money getDescontoWithValecompra(){
		Money valor = new Money();
		if(getDesconto() != null){
			valor = getDesconto();
		}
		if(getValorusadovalecompra() != null){
			valor = valor.add(getValorusadovalecompra());
		}
		return valor;
	}
	
	@Transient
	public String getVendaAutocompleteForComissaoVendedor(){
		StringBuilder s = new StringBuilder();
		
		if(this.getCdvenda() != null){
			s.append(this.getCdvenda());
			
			if(this.getIdentificador() != null){					
				s.append(" - ").append(this.getIdentificador());
			}
			
			if(this.getNomeCliente() != null){
				s.append(" - ").append(this.getNomeCliente());
			}
		}
		
		return s.toString()!=null && !s.toString().isEmpty() ? s.toString() : "";
	}

	@Transient
	public Boolean getDocumentoAposEmissaoNota() {
		return documentoAposEmissaoNota;
	}

	public void setDocumentoAposEmissaoNota(Boolean documentoAposEmissaoNota) {
		this.documentoAposEmissaoNota = documentoAposEmissaoNota;
	}
	
	@Transient
	@DisplayName("Gerar novas parcelas")
	public Boolean getGerarnovasparcelas() {
		return gerarnovasparcelas;
	}

	public void setGerarnovasparcelas(Boolean gerarnovasparcelas) {
		this.gerarnovasparcelas = gerarnovasparcelas;
	}

	@Transient
	@DisplayName("Banco")
	public Integer getPagamentobanco() {
		return pagamentobanco;
	}
	@Transient
	@DisplayName("Ag�ncia")
	public Integer getPagamentoagencia() {
		return pagamentoagencia;
	}
	@Transient
	@DisplayName("Conta")
	public Integer getPagamentoconta() {
		return pagamentoconta;
	}
	@Transient
	@DisplayName("Primeiro N�mero")
	public String getPagamentoprimeironumero() {
		return pagamentoprimeironumero;
	}
	@Transient
	@DisplayName("Valor p/ Gerar Parcelas")
	public Money getPagamentovalor() {
		return pagamentovalor;
	}
	@Transient
	public Date getPagamentoDataUltimoVencimento() {
		return pagamentoDataUltimoVencimento;
	}

	public void setPagamentobanco(Integer pagamentobanco) {
		this.pagamentobanco = pagamentobanco;
	}
	public void setPagamentoagencia(Integer pagamentoagencia) {
		this.pagamentoagencia = pagamentoagencia;
	}
	public void setPagamentoconta(Integer pagamentoconta) {
		this.pagamentoconta = pagamentoconta;
	}
	public void setPagamentoprimeironumero(String pagamentoprimeironumero) {
		this.pagamentoprimeironumero = pagamentoprimeironumero;
	}
	public void setPagamentovalor(Money pagamentovalor) {
		this.pagamentovalor = pagamentovalor;
	}
	public void setPagamentoDataUltimoVencimento(Date pagamentoDataUltimoVencimento) {
		this.pagamentoDataUltimoVencimento = pagamentoDataUltimoVencimento;
	}
	
	@Transient
	public String getIdentificadorOrCdvenda(){
		identificadorOrCdvenda = "";
		if(getIdentificador() != null && !"".equals(getIdentificador())){
			identificadorOrCdvenda = getIdentificador();
		}else if(getCdvenda() != null){
			identificadorOrCdvenda = getCdvenda().toString();
		}
		return identificadorOrCdvenda;
	}
	
	public void setIdentificadorOrCdvenda(String identificadorOrCdvenda) {
		this.identificadorOrCdvenda = identificadorOrCdvenda;
	}
	
	@Transient
	public String getIdentificadorexternoOrCdvenda(){
		String s = "";
		if(getIdentificadorexterno() != null && !"".equals(getIdentificadorexterno())){
			s = getIdentificadorexterno();
		}else if(getCdvenda() != null){
			s = getCdvenda().toString();
		}
		return s;
	}
	
	@Transient
	public String getIdentificadorexternoAndIdentificador(){
		String s = "";
		if(StringUtils.isNotBlank(getIdentificadorexterno())){
			s += getIdentificadorexterno();
		}
		if(StringUtils.isNotBlank(getIdentificador())){
			if(s.length() > 0) s += "/";
			s += getIdentificador();
		}
		return s;
	}

	@Transient
	public Boolean getBoletoEmitido() {
		return boletoEmitido;
	}

	public void setBoletoEmitido(Boolean boletoEmitido) {
		this.boletoEmitido = boletoEmitido;
	}
	
	@Transient
	public Double getTotalPesoBruto() {
		Double totalPesoBruto = 0.0;
		if(SinedUtil.isListNotEmpty(getListavendamaterial())){
			for(Vendamaterial vendamaterial : getListavendamaterial()){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getMaterial().getPesobruto() != null){
					Double pesoBruto = vendamaterial.getMaterial().getPesobruto();
					if(vendamaterial.getUnidademedida() != null && vendamaterial.getMaterial() != null && 
							vendamaterial.getMaterial().getUnidademedida() != null && 
							!vendamaterial.getUnidademedida().equals(vendamaterial.getMaterial().getUnidademedida())){
						pesoBruto = pesoBruto / vendamaterial.getFatorconversaoQtdereferencia();
					}
					totalPesoBruto +=  pesoBruto * qtde;
				}
			}
		}
		return SinedUtil.roundByParametro(totalPesoBruto, 2);
	}
	public void setTotalPesoBruto(Double totalPesoBruto) {
		this.totalPesoBruto = totalPesoBruto;
	}
	
	@Transient
	public Double getTotalPesoLiquido() {
		Double totalPesoLiquido = 0.0;
		if(SinedUtil.isListNotEmpty(getListavendamaterial())){
			for(Vendamaterial vendamaterial : getListavendamaterial()){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getPesoVendaOuMaterial() != null){
					totalPesoLiquido += vendamaterial.getPesoVendaOuMaterial() * qtde;
				}
			}
		}
		return SinedUtil.roundByParametro(totalPesoLiquido, 2);
	}
	public void setTotalPesoLiquido(Double totalPesoLiquido) {
		this.totalPesoLiquido = totalPesoLiquido;
	}
	
	@DisplayName("Quantidade de Volumes")
	public Integer getQuantidadevolumes() {
		return quantidadevolumes;
	}

	public void setQuantidadevolumes(Integer quantidadevolumes) {
		this.quantidadevolumes = quantidadevolumes;
	}

	@Transient
	public Boolean getGerarnotaautomatico() {
		return gerarnotaautomatico;
	}
	public void setGerarnotaautomatico(Boolean gerarnotaautomatico) {
		this.gerarnotaautomatico = gerarnotaautomatico;
	}

	@Transient
	public Boolean getIsNotVerificarestoquePedidovenda() {
		if(isNotVerificarestoquePedidovenda == null)
			return Boolean.FALSE;
		return isNotVerificarestoquePedidovenda;
	}
	public void setIsNotVerificarestoquePedidovenda(Boolean isNotVerificarestoquePedidovenda) {
		this.isNotVerificarestoquePedidovenda = isNotVerificarestoquePedidovenda;
	}
	
	@Transient
	public Boolean getIdentificadorAutomatico() {
		return identificadorAutomatico;
	}
	
	public void setIdentificadorAutomatico(Boolean identificadorAutomatico) {
		this.identificadorAutomatico = identificadorAutomatico;
	}

	@Transient
	@DisplayName("Vendedor Principal")
	public String getVendedorprincipal() {
		return vendedorprincipal;
	}
	
	public void setVendedorprincipal(String vendedorprincipal) {
		this.vendedorprincipal = vendedorprincipal;
	}

	@DisplayName("Origem WebService")
	public Boolean getOrigemwebservice() {
		return origemwebservice;
	}

	public void setOrigemwebservice(Boolean origemwebservice) {
		this.origemwebservice = origemwebservice;
	}
	
	@Transient
	public Money getSaldovalecompra() {
		return saldovalecompra;
	}
	
	public void setSaldovalecompra(Money saldovalecompra) {
		this.saldovalecompra = saldovalecompra;
	}
	
	@Transient
	public Money getValorvalecompranovo() {
		return valorvalecompranovo;
	}
	
	public void setValorvalecompranovo(Money valorvalecompranovo) {
		this.valorvalecompranovo = valorvalecompranovo;
	}
	
	@Transient
	public Boolean getCreditovalecompradevolucao() {
		return creditovalecompradevolucao;
	}
	
	public void setCreditovalecompradevolucao(Boolean creditovalecompradevolucao) {
		this.creditovalecompradevolucao = creditovalecompradevolucao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	@DisplayName("Ag�ncia")
	public Fornecedor getFornecedorAgencia() {
		return fornecedorAgencia;
	}
	public void setFornecedorAgencia(Fornecedor fornecedorAgencia) {
		this.fornecedorAgencia = fornecedorAgencia;
	}
	
	@OneToMany(mappedBy="venda")
	@DisplayName("Comiss�o de representa��o")
	public List<Vendapagamentorepresentacao> getListavendapagamentorepresentacao() {
		return listavendapagamentorepresentacao;
	}

	public void setListavendapagamentorepresentacao(List<Vendapagamentorepresentacao> listavendapagamentorepresentacao) {
		this.listavendapagamentorepresentacao = listavendapagamentorepresentacao;
	}
	
	@OneToMany(mappedBy="venda")
	public List<VendaFornecedorTicketMedio> getListaVendaFornecedorTicketMedio() {
		return listaVendaFornecedorTicketMedio;
	}
	
	public void setListaVendaFornecedorTicketMedio(List<VendaFornecedorTicketMedio> listaVendaFornecedorTicketMedio) {
		this.listaVendaFornecedorTicketMedio = listaVendaFornecedorTicketMedio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	
	@DisplayName("Valor do frete CIF")
	public Money getValorFreteCIF() {
		return valorFreteCIF;
	}
	public void setValorFreteCIF(Money valorFreteCIF) {
		this.valorFreteCIF = valorFreteCIF;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecofaturamento")
	@DisplayName("Endere�o de faturamento")
	public Endereco getEnderecoFaturamento() {
		return enderecoFaturamento;
	}
	public void setEnderecoFaturamento(Endereco enderecoFaturamento) {
		this.enderecoFaturamento = enderecoFaturamento;
	}
	
	@Transient
	public Money getPercentualRepresentacao() {
		return percentualRepresentacao;
	}
	public void setPercentualRepresentacao(Money percentualRepresentacao) {
		this.percentualRepresentacao = percentualRepresentacao;
	}
	
	@Transient
	public List<Contato> getListaDestinatarios() {
		return listaDestinatarios;
	}
	public void setListaDestinatarios(List<Contato> listaDestinatarios) {
		this.listaDestinatarios = listaDestinatarios;
	}

	@Transient
	public List<Arquivo> getListaArquivos() {
		return listaArquivos;
	}
	public void setListaArquivos(List<Arquivo> listaArquivos) {
		this.listaArquivos = listaArquivos;
	}

	@Transient
	public String getRemetente() {
		return remetente;
	}
	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	@DisplayName("Condi��o de Pagamento")
	@JoinColumn(name="cdprazopagamentorepresentacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Prazopagamento getPrazopagamentorepresentacao() {
		return prazopagamentorepresentacao;
	}
	@DisplayName("Forma de pagamento")
	@JoinColumn(name="cdformapagamentorepresentacao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Documentotipo getDocumentotiporepresentacao() {
		return documentotiporepresentacao;
	}
	@JoinColumn(name="cdcontarepresentacao")
	@DisplayName("Conta para Boleto")
	@ManyToOne(fetch=FetchType.LAZY)
	public Conta getContaboletorepresentacao() {
		return contaboletorepresentacao;
	}

	public void setPrazopagamentorepresentacao(Prazopagamento prazopagamentorepresentacao) {
		this.prazopagamentorepresentacao = prazopagamentorepresentacao;
	}
	public void setDocumentotiporepresentacao(Documentotipo documentotiporepresentacao) {
		this.documentotiporepresentacao = documentotiporepresentacao;
	}
	public void setContaboletorepresentacao(Conta contaboletorepresentacao) {
		this.contaboletorepresentacao = contaboletorepresentacao;
	}

	@Transient
	public Money getTotalparcelarepresentacao() {
		Money valor = new Money(0);
		if(Hibernate.isInitialized(this.listavendapagamentorepresentacao) && SinedUtil.isListNotEmpty(this.listavendapagamentorepresentacao)){
			for (Vendapagamentorepresentacao vendapagamentorepresentacao : this.listavendapagamentorepresentacao) {
				valor = valor.add(new Money(vendapagamentorepresentacao.getValororiginal()));
			}
		}		
		return valor;
	}

	public void setTotalparcelarepresentacao(Money totalparcelarepresentacao) {
		this.totalparcelarepresentacao = totalparcelarepresentacao;
	}
	
	@DisplayName("Desconto da comiss�o")
	public Money getValordescontorepresentacao() {
		return valordescontorepresentacao;
	}
	@DisplayName("Valor da comiss�o bruta")
	public Money getValorbrutorepresentacao() {
		return valorbrutorepresentacao;
	}
	@Transient
	@DisplayName("Valor total da comiss�o")
	public Money getValortotalrepresentacao() {
		Money valor = new Money();
		
		if(getValorbrutorepresentacao() != null){
			valor = getValorbrutorepresentacao();
		}
		if(getValordescontorepresentacao() != null && getValordescontorepresentacao().getValue().doubleValue() > 0){
			valor = valor.subtract(getValordescontorepresentacao());
		}
		return valortotalrepresentacao;
	}

	public void setValordescontorepresentacao(Money valordescontorepresentacao) {
		this.valordescontorepresentacao = valordescontorepresentacao;
	}
	public void setValorbrutorepresentacao(Money valorbrutorepresentacao) {
		this.valorbrutorepresentacao = valorbrutorepresentacao;
	}
	public void setValortotalrepresentacao(Money valortotalrepresentacao) {
		this.valortotalrepresentacao = valortotalrepresentacao;
	}
	
	@Transient
	public List<Fornecedor> getListaFornecedor() {
		return listaFornecedor;
	}
	public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}

	@Transient
	public Integer getQtdevendapagamento() {
		return qtdevendapagamento;
	}
	@Transient
	public Integer getQtdedocumento() {
		return qtdedocumento;
	}

	public void setQtdevendapagamento(Integer qtdevendapagamento) {
		this.qtdevendapagamento = qtdevendapagamento;
	}
	public void setQtdedocumento(Integer qtdedocumento) {
		this.qtdedocumento = qtdedocumento;
	}
	
	@Transient
	public Money getPercentualaplicadototal(){
		Double percentualaplicadototal = 0d;
		Double valordescontoitem = 0d;
		Double totalvenda = 0d;
		Double totaltabela = 0d;
		
		if(SinedUtil.isListNotEmpty(getListavendamaterial())){
			for(Vendamaterial vendamaterial : getListavendamaterial()){
				
				if(vendamaterial != null && vendamaterial.getValorvendaRelatorio() != null){
					totaltabela += vendamaterial.getValorvendaRelatorio() * vendamaterial.getQuantidade();
				}
				if(vendamaterial.getConsiderarDesconto() != null && vendamaterial.getConsiderarDesconto() 
						&& vendamaterial.getValordescontocustovendaitem() != null && vendamaterial.getValorvalecomprapropocional() != null
						&& ((vendamaterial.getConsiderarValeCompra() == null || !vendamaterial.getConsiderarValeCompra()) 
							|| (vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra()))){
					
					if(vendamaterial.getUnidademedida() != null && vendamaterial.getUnidademedida().getCasasdecimaisestoque() != null){
						valordescontoitem += SinedUtil.round(vendamaterial.getValordescontocustovendaitem(), vendamaterial.getUnidademedida().getCasasdecimaisestoque());
					}else {
						valordescontoitem += vendamaterial.getValordescontocustovendaitem();
					}
				}
				if((vendamaterial.getConsiderarDesconto() == null || !vendamaterial.getConsiderarDesconto()) 
						&& vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra() 
						&& vendamaterial.getValorvalecomprapropocional() != null){
					
					valordescontoitem += vendamaterial.getValorvalecomprapropocional();
				}			
				if(vendamaterial.getTotalprodutoDescontoReport() != null){
					totalvenda += vendamaterial.getTotalprodutoReport().getValue().doubleValue();
				}
			}
			
			if(totaltabela != null && totaltabela > 0){
				percentualaplicadototal = ( valordescontoitem / totaltabela )* 100;
			}else if(totalvenda != null && totalvenda > 0){
				percentualaplicadototal = valordescontoitem / totalvenda * 100;
			}
		}
		return new Money(percentualaplicadototal);
	}

	@Transient
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}
	
	@Transient
	@DisplayName("Replicar material ao incluir")
	public Boolean getReplicarmaterial() {
		return replicarmaterial;
	}

	@Transient
	@DisplayName("Quantidade de vezes")
	public Integer getQtdereplicar() {
		return qtdereplicar;
	}

	public void setReplicarmaterial(Boolean replicarmaterial) {
		this.replicarmaterial = replicarmaterial;
	}

	public void setQtdereplicar(Integer qtdereplicar) {
		this.qtdereplicar = qtdereplicar;
	}

	@Transient
	public String getIdentificacaomaterial() {
		return identificacaomaterial;
	}

	public void setIdentificacaomaterial(String identificacaomaterial) {
		this.identificacaomaterial = identificacaomaterial;
	}

	@Transient
	public String getObservacaoFeedApplication() {
		return observacaoFeedApplication;
	}
	@Transient
	public Boolean getSucessoFeedApplication() {
		return sucessoFeedApplication;
	}

	public void setObservacaoFeedApplication(String observacaoFeedApplication) {
		this.observacaoFeedApplication = observacaoFeedApplication;
	}
	public void setSucessoFeedApplication(Boolean sucessoFeedApplication) {
		this.sucessoFeedApplication = sucessoFeedApplication;
	}
	
	@Transient
	@DisplayName("Item adicional da Ordem de Produ��o")
	public List<Producaoordemitemadicional> getListaProducaoordemitemadicional() {
		return listaProducaoordemitemadicional;
	}
	
	public void setListaProducaoordemitemadicional(
			List<Producaoordemitemadicional> listaProducaoordemitemadicional) {
		this.listaProducaoordemitemadicional = listaProducaoordemitemadicional;
	}
	
	@Transient
	@DisplayName("Item adicional da Agenda de Produ��o")
	public List<Producaoagendaitemadicional> getListaProducaoagendaitemadicional() {
		return listaProducaoagendaitemadicional;
	}
	
	public void setListaProducaoagendaitemadicional(List<Producaoagendaitemadicional> listaProducaoagendaitemadicional) {
		this.listaProducaoagendaitemadicional = listaProducaoagendaitemadicional;
	}

	@Transient
	@DisplayName("Valor Final com Impostos")
	public Money getValorfinalMaisImpostos() {
		return valorfinalMaisImpostos;
	}

	public void setValorfinalMaisImpostos(Money valorfinalMaisImpostos) {
		this.valorfinalMaisImpostos = valorfinalMaisImpostos;
	}
	
	@DisplayName ("Total IPI")
	@Transient
	public Money getTotalipi() {
		/*Money valor = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				valor = valor.add(item.getValoripi());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				valor = valor.add(item.getValoripi());
			}
		}*/
		return totalipi;
	}
	
	public void setTotalipi(Money totalipi) {
		this.totalipi = totalipi;
	}

	@Transient
	public Boolean getSalvarPagamentos() {
		return salvarPagamentos;
	}

	public void setSalvarPagamentos(Boolean salvarPagamentos) {
		this.salvarPagamentos = salvarPagamentos;
	}

	public Boolean getCriacaofinalizada() {
		return criacaofinalizada;
	}

	public void setCriacaofinalizada(Boolean criacaofinalizada) {
		this.criacaofinalizada = criacaofinalizada;
	}
	
	public Boolean getAprovando() {
		return aprovando;
	}

	public void setAprovando(Boolean aprovando) {
		this.aprovando = aprovando;
	}

	@Transient
	public Boolean getSalvarItens() {
		return salvarItens;
	}

	public void setSalvarItens(Boolean salvarItens) {
		this.salvarItens = salvarItens;
	}
	
	@Transient
	public Date getPrazoentregaMinimo() {
		return prazoentregaMinimo;
	}
	
	public void setPrazoentregaMinimo(Date prazoentregaMinimo) {
		this.prazoentregaMinimo = prazoentregaMinimo;
	}

	public Boolean getLimitecreditoexcedido() {
		return limitecreditoexcedido;
	}

	public void setLimitecreditoexcedido(Boolean limitecreditoexcedido) {
		this.limitecreditoexcedido = limitecreditoexcedido;
	}

	@Transient
	public Boolean getValorSupeiorLimiteCredito() {
		return valorSupeiorLimiteCredito;
	}

	public void setValorSupeiorLimiteCredito(Boolean valorSupeiorLimiteCredito) {
		this.valorSupeiorLimiteCredito = valorSupeiorLimiteCredito;
	}

	@Transient
	public Money getValorTotalRateio() {
		return valorTotalRateio;
	}

	public void setValorTotalRateio(Money valorTotalRateio) {
		this.valorTotalRateio = valorTotalRateio;
	}
	
	@Transient
	public String getNumeroNotaGerarPDF(){
		Set<String> listaNumeroNota = new HashSet<String>();
		
		if (listaNotavenda!=null && Vendasituacao.FATURADA.equals(getVendasituacao())){
			for (NotaVenda notaVenda: listaNotavenda){
				if (notaVenda.getNota()!=null && notaVenda.getNota().getNumero()!=null && !notaVenda.getNota().getNumero().trim().equals("")){
					listaNumeroNota.add(notaVenda.getNota().getNumero());
				}
			}
		}
		
		return CollectionsUtil.concatenate(listaNumeroNota, "/");
	}

	@Transient
	public Boolean getCancelada() {
		return cancelada;
	}

	public void setCancelada(Boolean cancelada) {
		this.cancelada = cancelada;
	}

	@Transient
	public Boolean getRegistrarpesomedio() {
		return registrarpesomedio;
	}
	public void setRegistrarpesomedio(Boolean registrarpesomedio) {
		this.registrarpesomedio = registrarpesomedio;
	}
	
	@Transient
	public Double getPercentualdescontoTabelapreco() {
		return percentualdescontoTabelapreco;
	}
	public void setPercentualdescontoTabelapreco(
			Double percentualdescontoTabelapreco) {
		this.percentualdescontoTabelapreco = percentualdescontoTabelapreco;
	}
	
	@Transient
	public Money getDescontogarantiareforma() {
		return descontogarantiareforma;
	}
	public void setDescontogarantiareforma(Money descontogarantiareforma) {
		this.descontogarantiareforma = descontogarantiareforma;
	}
	
	@Transient
	public Garantiareformaitem getGarantiareformaitem() {
		return garantiareformaitem;
	}
	public void setGarantiareformaitem(Garantiareformaitem garantiareformaitem) {
		this.garantiareformaitem = garantiareformaitem;
	}
	
	@Transient
	public Money getTotalproduto() {
		Money totalproduto = new Money();
		
		if (listavendamaterial!=null){
			for (Vendamaterial vendamaterial: listavendamaterial){
				totalproduto = totalproduto.add(vendamaterial.getTotalproduto());
			}
		}
		
		return totalproduto;
	}
	
	@Transient
	public Money getTotalprodutoValorLiquido() {
		Money totalproduto = this.getTotalproduto();
		
		if (listavendamaterial != null){
			for (Vendamaterial vendamaterial: listavendamaterial){
				if(vendamaterial.getDesconto() != null && vendamaterial.getDesconto().getValue().doubleValue() > 0){
					totalproduto = totalproduto.subtract(vendamaterial.getDesconto());
				}
			}
		}
		
		return totalproduto;
	}
	
	@Transient
	public boolean getExisteGarantia(){
		if(Hibernate.isInitialized(getListavendamaterial()) && SinedUtil.isListNotEmpty(getListavendamaterial())){
			for(Vendamaterial vm : getListavendamaterial()){
				if(vm.getGarantiareformaitem() != null){
					return true;
				}
			}
		}
		return false;
	}

	@Transient
	public Date getDtestoqueEmissaonota() {
		return dtestoqueEmissaonota;
	}

	public void setDtestoqueEmissaonota(Date dtestoqueEmissaonota) {
		this.dtestoqueEmissaonota = dtestoqueEmissaonota;
	}
	
	@Transient
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@Transient
	public List<Expedicao> getListaExpedicaotrans() {
		if(listaExpedicaotrans != null){
			listaExpedicaotrans.clear();
		}else{
			listaExpedicaotrans = new ArrayList<Expedicao>();
			if(this.getListavendamaterial() != null){
				for(Vendamaterial vm: this.getListavendamaterial()){
					if(vm.getListaExpedicaoitem() != null){
						for(Expedicaoitem ei: vm.getListaExpedicaoitem()){
							if(ei.getExpedicao() != null && !listaExpedicaotrans.contains(ei.getExpedicao())){
								listaExpedicaotrans.add(ei.getExpedicao());
							}
						}
					}
				}
			}
		}
		return listaExpedicaotrans;
	}
	public void setListaExpedicaotrans(List<Expedicao> listaExpedicaotrans) {
		this.listaExpedicaotrans = listaExpedicaotrans;
	}
	
	@DisplayName("Qtde reservada")
	@Transient
	public Double getQtdereservada() {
		return qtdereservada;
	}
	public void setQtdereservada(Double qtdereservada) {
		this.qtdereservada = qtdereservada;
	}
	
	@Transient
	public Documentoacao getEmporiumDocumentoacao() {
		return emporiumDocumentoacao;
	}
	public void setEmporiumDocumentoacao(Documentoacao emporiumDocumentoacao) {
		this.emporiumDocumentoacao = emporiumDocumentoacao;
	}
	
	@Transient
	public Boolean getGerouComissionamentoPorFaixas() {
		return gerouComissionamentoPorFaixas;
	}
	public void setGerouComissionamentoPorFaixas(Boolean gerouComissionamentoPorFaixas) {
		this.gerouComissionamentoPorFaixas = gerouComissionamentoPorFaixas;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Venda) {
			Venda venda = (Venda) obj;
			return venda.getCdvenda().equals(this.getCdvenda());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (cdvenda != null) {
			return cdvenda.hashCode();
		}
		return super.hashCode();
	}
	
	@Transient
	public Money getTotalvendaSemArredondamento() {
		Money valor = new Money(0);
		if(this.listavendamaterial != null && !this.listavendamaterial.isEmpty()){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 0 : vendamaterial.getQuantidade();
				Double multiplicador = vendamaterial.getMultiplicador() == null || vendamaterial.getMultiplicador() == 0 ? 1 : vendamaterial.getMultiplicador();
				Double valorTruncado = SinedUtil.round(BigDecimal.valueOf(vendamaterial.getPreco().doubleValue() * qtde * multiplicador), 10).doubleValue();
				valor = valor.add(new Money(valorTruncado));
				if(vendamaterial.getDesconto() != null){
					valor = valor.subtract(vendamaterial.getDesconto());
				}
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.add(this.taxapedidovenda != null ? this.taxapedidovenda : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? 
				this.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	@Transient
	@Override
	public List<VendaMaterialProducaoItemInterface> getItens() {
		List<VendaMaterialProducaoItemInterface> lista = new ArrayList<VendaMaterialProducaoItemInterface>();
		for(Vendamaterial vm: this.getListavendamaterial()){
			lista.add(vm);
		}
		return lista;
	}
	@Transient
	public List<Message> getMensagemAposSalvar() {
		if(mensagemAposSalvar == null){
			mensagemAposSalvar = new ArrayList<Message>();
		}
		return mensagemAposSalvar;
	}
	public void setMensagemAposSalvar(List<Message> mensagemAposSalvar) {
		this.mensagemAposSalvar = mensagemAposSalvar;
	}
	@Transient
	public Boolean getFromWebService() {
		return fromWebService;
	}
	public void setFromWebService(Boolean fromWebService) {
		this.fromWebService = fromWebService;
	}
	@Transient
	public List<Pneu> getListaPneu() {
		if(listaPneu == null){
			listaPneu = new ListSet<Pneu>(Pneu.class);
		}
		return listaPneu;
	}
	public void setListaPneu(List<Pneu> listaPneu) {
		this.listaPneu = listaPneu;
	}
	@Transient
	public Pneu getPneuInclusao() {
		return pneuInclusao;
	}
	public void setPneuInclusao(Pneu pneuInclusao) {
		this.pneuInclusao = pneuInclusao;
	}
	@Transient
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	
	@Transient
	public List<PneuItemVendaInterface> getListaPneuItemInterface(){
		List<PneuItemVendaInterface> lista = new ArrayList<PneuItemVendaInterface>();
		for(Vendamaterial item: listavendamaterial){
			lista.add(item);
		}
		return lista;
	}
	
	@Transient
	@DisplayName ("Qtde dispon�vel")
	public Double getQtdeEstoqueMenosReservada() {
		return qtdeEstoqueMenosReservada;
	}
	public void setQtdeEstoqueMenosReservada(Double qtdeEstoqueMenosReservada) {
		this.qtdeEstoqueMenosReservada = qtdeEstoqueMenosReservada;
	}	
	
	public Boolean getOrigemOtr() {
		return origemOtr;
	}
	public void setOrigemOtr(Boolean origemOtr) {
		this.origemOtr = origemOtr;
	}
	
	@Transient
	public Boolean getReplicarPneu() {
		return replicarPneu;
	}
	public void setReplicarPneu(Boolean replicarPneu) {
		this.replicarPneu = replicarPneu;
	}
	
	@Transient
	public Integer getQtdeReplicarPneu() {
		return qtdeReplicarPneu;
	}
	public void setQtdeReplicarPneu(Integer qtdeReplicarPneu) {
		this.qtdeReplicarPneu = qtdeReplicarPneu;
	}
	
	@Transient
	public Boolean getExibeIconeEntregaPendente() {
		return exibeIconeEntregaPendente;
	}
	public void setExibeIconeEntregaPendente(Boolean exibeIconeEntregaPendente) {
		this.exibeIconeEntregaPendente = exibeIconeEntregaPendente;
	}
	
	@Transient
	public List<PneuItemVendaInterface> getListaItemAvulso() {
		return listaItemAvulso;
	}
	public void setListaItemAvulso(List<PneuItemVendaInterface> listaItemAvulso) {
		this.listaItemAvulso = listaItemAvulso;
	}
	
	@Transient
	public Money getTotalFcp() {
		/*totalFcp = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}*/
		return totalFcp;
	}
	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}
	
	@Transient
	public Money getTotalFcpSt() {
		/*totalFcpSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}*/
		return totalFcpSt;
	}
	public void setTotalFcpSt(Money totalFcpSt) {
		this.totalFcpSt = totalFcpSt;
	}
	
	@Transient
	public Money getTotalDesoneracaoIcms() {
		return totalDesoneracaoIcms;
	}
	
	public void setTotalDesoneracaoIcms(Money totalDesoneracaoIcms) {
		this.totalDesoneracaoIcms = totalDesoneracaoIcms;
	}
	
	@Override
	@Transient
	public Money getTotalIcms() {
		/*totalIcms = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}*/
		return totalIcms;
	}
	public void setTotalIcms(Money totalIcms) {
		this.totalIcms = totalIcms;
	}

	@Transient
	public Money getTotalIcmsSt() {
		/*totalIcmsSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}*/
		return totalIcmsSt;
	}
	public void setTotalIcmsSt(Money totalIcmsSt) {
		this.totalIcmsSt = totalIcmsSt;
	}
	
	@Transient
	public Money getTotalImpostos() {
		totalImpostos = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial item : this.listavendamaterial) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	
	@Transient
	public Money getTotalOutrasDespesas() {
		totalOutrasDespesas = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				totalOutrasDespesas = totalOutrasDespesas.add(vendamaterial.getOutrasdespesas());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalOutrasDespesas = totalOutrasDespesas.add(item.getOutrasdespesas());
			}
		}
		return totalOutrasDespesas;
	}
	public void setTotalOutrasDespesas(Money totalOutrasDespesas) {
		this.totalOutrasDespesas = totalOutrasDespesas;
	}
	
	@Transient
	public Money getTotalValorSeguro() {
		totalValorSeguro = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
			for (Vendamaterial vendamaterial : this.listavendamaterial) {
				totalValorSeguro = totalValorSeguro.add(vendamaterial.getValorSeguro());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
			for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
				totalValorSeguro = totalValorSeguro.add(item.getValorSeguro());
			}
		}
		return totalValorSeguro;
	}
	public void setTotalValorSeguro(Money totalValorSeguro) {
		this.totalValorSeguro = totalValorSeguro;
	}
	
	@Transient
	public Money getTotalDifal() {
		return totalDifal;
	}
	
	@Override
	public void setTotalDifal(Money totalDifal) {
		this.totalDifal = totalDifal;
	}
	
	@Transient
	public Boolean getIncluirEnderecoFaturamento() {
		incluirEnderecoFaturamento = Util.objects.isPersistent(this.getEnderecoFaturamento());
		return incluirEnderecoFaturamento;
	}
	public void setIncluirEnderecoFaturamento(Boolean incluirEnderecoFaturamento) {
		this.incluirEnderecoFaturamento = incluirEnderecoFaturamento;
	}
	@Transient
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	@Transient
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	
	@Transient
	public Boolean getExisteServico() {
		return existeServico;
	}
	public void setExisteServico(Boolean existeServico) {
		this.existeServico = existeServico;
	}
	@Transient
	public Notafiscalproduto getNotaFiscalProdutoTrans() {
		return notaFiscalProdutoTrans;
	}
	public void setNotaFiscalProdutoTrans(
			Notafiscalproduto notaFiscalProdutoTrans) {
		this.notaFiscalProdutoTrans = notaFiscalProdutoTrans;
	}
	@Transient
	public NotaFiscalServico getNotaFiscalServicoTrans() {
		return notaFiscalServicoTrans;
	}
	public void setNotaFiscalServicoTrans(
			NotaFiscalServico notaFiscalServicoTrans) {
		this.notaFiscalServicoTrans = notaFiscalServicoTrans;
	}
	
	@Transient
	public Money getValorTotal() {
		return valorTotal;
	}
	
	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public void addValorTotal(Money valorTotal) {
		if (this.valorTotal == null) {
			this.valorTotal = new Money();
		}
		
		this.valorTotal = this.valorTotal.add(valorTotal);
	}	
	
	@Transient
	public PagamentoTEFSituacao getPagamentoTEFSituacao() {
		return pagamentoTEFSituacao;
	}
	public void setPagamentoTEFSituacao(
			PagamentoTEFSituacao pagamentoTEFSituacao) {
		this.pagamentoTEFSituacao = pagamentoTEFSituacao;
	}
	
    @Transient
    public Money getTotalvendaMaisImpostos() {
        Money valor = getTotalvenda();
        if(SinedUtil.isListNotEmpty(this.listavendamaterial)){
            for (Vendamaterial item : this.listavendamaterial) {
                valor = valor.add(item.getValoripi())
                            .add(item.getValoricmsst())
                            .add(item.getValorfcpst());
                
                if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
            }
        }
        if(SinedUtil.isListNotEmpty(this.listaVendamaterialmestre)){
            for (Vendamaterialmestre item : this.listaVendamaterialmestre) {
                valor = valor.add(item.getValoripi())
                        .add(item.getValoricmsst())
                        .add(item.getValorfcpst());
                
                if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
            }
        }
        return valor;
    }

	public Boolean getVendaPendenciaNaoNegociada() {
		return vendaPendenciaNaoNegociada;
	}
	public void setVendaPendenciaNaoNegociada(Boolean vendaPendenciaNaoNegociada) {
		this.vendaPendenciaNaoNegociada = vendaPendenciaNaoNegociada;
	}
	
	public Boolean getSinalizadoSemReserva() {
		return sinalizadoSemReserva;
	}
	public void setSinalizadoSemReserva(Boolean sinalizadoSemReserva) {
		this.sinalizadoSemReserva = sinalizadoSemReserva;
	}
	
	public Boolean getEntregaEcommerceRealizada() {
		return entregaEcommerceRealizada;
	}
	public void setEntregaEcommerceRealizada(Boolean entregaEcommerceRealizada) {
		this.entregaEcommerceRealizada = entregaEcommerceRealizada;
	}
	
	@Transient
	public String getMenorDtVencimentoLote() {
		return menorDtVencimentoLote;
	}
	
	public void setMenorDtVencimentoLote(String menorDtVencimentoLote) {
		this.menorDtVencimentoLote = menorDtVencimentoLote;
	}
	
	@Transient
	public List<InclusaoLoteVendaInterface> getListaInclusaoLoteVendaInterface(){
		List<InclusaoLoteVendaInterface> lista = new ArrayList<InclusaoLoteVendaInterface>();
		for(Vendamaterial pvm: this.listavendamaterial){
			lista.add(pvm);
		}
		return lista;
	}
	
	@Transient
	public Pedidovendasituacaoecommerce getPedidovendasituacaoecommerce() {
		return pedidovendasituacaoecommerce;
	}
	
	public void setPedidovendasituacaoecommerce(
			Pedidovendasituacaoecommerce pedidovendasituacaoecommerce) {
		this.pedidovendasituacaoecommerce = pedidovendasituacaoecommerce;
	}
	
	@Transient
	public String getNomeSituacaoEcommerce() {
		return nomeSituacaoEcommerce;
	}
	
	public void setNomeSituacaoEcommerce(String nomeSituacaoEcommerce) {
		this.nomeSituacaoEcommerce = nomeSituacaoEcommerce;
	}
	
	@DisplayName ("TICKET M�DIO CALCULADO (POR KG)")
	@Transient
	public Money getTicketMedioCaculado() {
		return ticketMedioCaculado;
	}

	public void setTicketMedioCaculado(Money ticketMedioCaculado) {
		this.ticketMedioCaculado = ticketMedioCaculado;
	}
	
	@Transient
	@DisplayName("Expedi��o")
	public Expedicao getExpedicao() {	
		return expedicao;
	}
	public void setExpedicao(Expedicao expedicao) {	
		this.expedicao=expedicao;
	}
}