package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_empresacodigocnae", sequenceName = "sq_empresacodigocnae")
public class Empresacodigocnae {

	private Integer cdempresacodigocnae;
	private Empresa empresa;
	private Codigocnae codigocnae;
	private Boolean principal;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresacodigocnae")
	public Integer getCdempresacodigocnae() {
		return cdempresacodigocnae;
	}

	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}

	@Required
	@DisplayName("C�digo CNAE")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigocnae")
	public Codigocnae getCodigocnae() {
		return codigocnae;
	}
	
	public Boolean getPrincipal() {
		return principal;
	}
	
	public void setCodigocnae(Codigocnae codigocnae) {
		this.codigocnae = codigocnae;
	}
	
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	public void setCdempresacodigocnae(Integer cdempresacodigocnae) {
		this.cdempresacodigocnae = cdempresacodigocnae;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdempresacodigocnae == null) ? 0 : cdempresacodigocnae.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Empresacodigocnae other = (Empresacodigocnae) obj;
		if (cdempresacodigocnae == null) {
			if (other.cdempresacodigocnae != null)
				return false;
		} else if (!cdempresacodigocnae.equals(other.cdempresacodigocnae))
			return false;
		return true;
	}

}
