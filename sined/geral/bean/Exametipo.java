package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_exametipo", sequenceName = "sq_exametipo")
public class Exametipo implements Log {
	
	protected Integer cdexametipo;
	protected String nome;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public static final Integer OUTROS = 10;

	public Exametipo() {
		super();
	}
	
	public Exametipo(Integer cdexametipo) {
		setCdexametipo(cdexametipo);
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_exametipo")
	public Integer getCdexametipo() {
		return cdexametipo;
	}
	public void setCdexametipo(Integer id) {
		this.cdexametipo = id;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.trim();
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
