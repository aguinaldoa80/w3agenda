package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_declaracaoexportacaonota", sequenceName = "sq_declaracaoexportacaonota")
public class DeclaracaoExportacaoNota implements Log {

	private Integer cdDeclaracaoExportacaoNota;
	private DeclaracaoExportacao declaracaoExportacao;
	private Notafiscalproduto notaFiscalProduto;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_declaracaoexportacaonota")
	public Integer getCdDeclaracaoExportacaoNota() {
		return cdDeclaracaoExportacaoNota;
	}
	@DisplayName("Declaração")
	@JoinColumn(name = "cddeclaracaoexportacao")
	@ManyToOne(fetch = FetchType.LAZY)
	public DeclaracaoExportacao getDeclaracaoExportacao() {
		return declaracaoExportacao;
	}

	@Required
	@JoinColumn(name = "cdnotafiscalproduto")
	@ManyToOne(fetch = FetchType.LAZY)
	public Notafiscalproduto getNotaFiscalProduto() {
		return notaFiscalProduto;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	/////////////////////////////////////////////////////////////////////|
	//  INICIO DOS SET 											/////////|
	/////////////////////////////////////////////////////////////////////|
	
	
	public void setCdDeclaracaoExportacaoNota(Integer cdDeclaracaoExportacaoNota) {
		this.cdDeclaracaoExportacaoNota = cdDeclaracaoExportacaoNota;
	}
	public void setDeclaracaoExportacao(DeclaracaoExportacao declaracaoExportacao) {
		this.declaracaoExportacao = declaracaoExportacao;
	}
	public void setNotaFiscalProduto(Notafiscalproduto notaFiscalProduto) {
		this.notaFiscalProduto = notaFiscalProduto;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}	
	
}
