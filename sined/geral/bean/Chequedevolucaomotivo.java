package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_chequedevolucaomotivo", sequenceName = "sq_chequedevolucaomotivo")
public class Chequedevolucaomotivo {

	protected Integer cdchequedevolucaomotivo;
	protected Integer codigo;
	protected String descricao;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_chequedevolucaomotivo")
	public Integer getCdchequedevolucaomotivo() {
		return cdchequedevolucaomotivo;
	}
	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdchequedevolucaomotivo(Integer cdchequedevolucaomotivo) {
		this.cdchequedevolucaomotivo = cdchequedevolucaomotivo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"codigo","descricao"})
	public String getDescricaocombo(){
		String combo = "";
		if(getCodigo() != null){
			combo = getCodigo().toString();
		}
		if(StringUtils.isNotBlank(getDescricao())){
			combo += (StringUtils.isNotBlank(combo) ?  " - " : "") + getDescricao(); 
		}
		return combo;
	}
}