package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.EmailStatusEnum;

@Entity
@SequenceGenerator(name = "sq_envioemailitem", sequenceName = "sq_envioemailitem")
public class Envioemailitem {

	protected Integer cdenvioemailitem;
	protected Envioemail envioemail;
	protected Pessoa pessoa;
	protected String email;
	protected String nomecontato;
	protected Timestamp dtconfirmacao;
	protected EmailStatusEnum situacaoemail;
	protected List<Envioemailitemhistorico> listaHistorico;
	protected Timestamp ultimaverificacao;
	
	public Envioemailitem() {

	}

	public Envioemailitem(Pessoa pessoa, String email, String nomecontato) {
		super();
		if(pessoa != null && pessoa.getCdpessoa() != null)
			this.pessoa = pessoa;
		this.email = email;
		this.nomecontato = nomecontato;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_envioemailitem")
	@DisplayName("Id")
	public Integer getCdenvioemailitem() {
		return cdenvioemailitem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdenvioemail")
	public Envioemail getEnvioemail() {
		return envioemail;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}

	public String getEmail() {
		if (email != null) {
			return email.toLowerCase();
		}
		return email;
	}

	public String getNomecontato() {
		return nomecontato;
	}

	@DisplayName("Data de Leitura do E-Mail")
	public Timestamp getDtconfirmacao() {
		return dtconfirmacao;
	}
	
	@DisplayName("Situa��o E-mail")
	public EmailStatusEnum getSituacaoemail() {
		return situacaoemail;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="envioemailitem")
	public List<Envioemailitemhistorico> getListaHistorico() {
		return listaHistorico;
	}
	
	@DisplayName("�ltima verifica��o")
	public Timestamp getUltimaverificacao() {
		return ultimaverificacao;
	}

	public void setCdenvioemailitem(Integer cdenvioemailitem) {
		this.cdenvioemailitem = cdenvioemailitem;
	}

	public void setEnvioemail(Envioemail envioemail) {
		this.envioemail = envioemail;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNomecontato(String nomecontato) {
		this.nomecontato = nomecontato;
	}

	public void setDtconfirmacao(Timestamp dtconfirmacao) {
		this.dtconfirmacao = dtconfirmacao;
	}
	
	public void setSituacaoemail(EmailStatusEnum situacaoemail) {
		this.situacaoemail = situacaoemail;
	}
	
	public void setListaHistorico(List<Envioemailitemhistorico> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}
	
	public void setUltimaverificacao(Timestamp ultimaverificacao) {
		this.ultimaverificacao = ultimaverificacao;
	}
}