package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_faixamarkupnome", sequenceName = "sq_faixamarkupnome")
@DisplayName("Faixa de markup")
public class FaixaMarkupNome implements Log{

	private Integer cdFaixaMarkupNome;
	private String nome;
	private String cor;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(generator="sq_faixamarkupnome", strategy=GenerationType.AUTO)
	public Integer getCdFaixaMarkupNome() {
		return cdFaixaMarkupNome;
	}
	
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public String getCor() {
		return cor;
	}
	
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdFaixaMarkupNome(Integer cdFaixaMarkupNome) {
		this.cdFaixaMarkupNome = cdFaixaMarkupNome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FaixaMarkupNome) {
			FaixaMarkupNome faixa = (FaixaMarkupNome) obj;
			return faixa.getCdFaixaMarkupNome().equals(this.getCdFaixaMarkupNome());
		}
		return super.equals(obj);
	}
}
