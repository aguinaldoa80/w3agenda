package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servidorinterfaceip", sequenceName = "sq_servidorinterfaceip")
public class Servidorinterfaceip implements Log{
	
	protected Integer cdservidorinterfaceip;
	protected Servidorinterface servidorinterface;
	protected Redeip redeip;
	protected Boolean gateway;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidorinterfaceip")
	public Integer getCdservidorinterfaceip() {
		return cdservidorinterfaceip;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidorinterface")	
	@DisplayName("Interface")
	public Servidorinterface getServidorinterface() {
		return servidorinterface;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdredeip")
	@DisplayName("IP")
	public Redeip getRedeip() {
		return redeip;
	}
	
	public Boolean getGateway() {
		return gateway;
	}

	public void setCdservidorinterfaceip(Integer cdservidorinterfaceip) {
		this.cdservidorinterfaceip = cdservidorinterfaceip;
	}

	public void setServidorinterface(Servidorinterface servidorinterface) {
		this.servidorinterface = servidorinterface;
	}

	public void setRedeip(Redeip redeip) {
		this.redeip = redeip;
	}

	public void setGateway(Boolean geteway) {
		this.gateway = geteway;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}	
}
