package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_fornecimento", sequenceName = "sq_fornecimento")
public class Fornecimento implements Log, PermissaoProjeto, PermissaoFornecedorEmpresa {
	
	protected Integer cdfornecimento;
	protected Fornecimentocontrato fornecimentocontrato;
	protected String historico;
	protected Date dtcontrato;
	protected Money valor;
	protected String observacao;
	protected Boolean baixado;
	protected Documento documento;
	
	protected Integer cdentregadocumento;
	
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	protected Empresa empresa;
	protected Set<Fornecimentoitem> listaFornecimentoitem = new ListSet<Fornecimentoitem>(Fornecimentoitem.class);
	protected List<Documentoorigem> listaDocumentoorigem;
	
	//Transiente
	protected Boolean fromFornecimentocontrato = false;
	
	public Fornecimento() {}
	
	public Fornecimento(Integer cdfornecimento) {
		this.cdfornecimento = cdfornecimento;
	}
		
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fornecimento")
	public Integer getCdfornecimento() {
		return cdfornecimento;
	}
	@Required
	@DisplayName("Hist�rico")
	@MaxLength(150)
	public String getHistorico() {
		return historico;
	}
	@Required
	@DisplayName("Data de fornecimento")
	public Date getDtcontrato() {
		return dtcontrato;
	}
	@Required
	public Money getValor() {
		return valor;
	}
	@MaxLength(150)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@Required
	public Boolean getBaixado() {
		return baixado;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecimentocontrato")
	@DisplayName("Contrato")
	public Fornecimentocontrato getFornecimentocontrato() {
		return fornecimentocontrato;
	}
	@OneToMany(mappedBy="fornecimento")
	@DisplayName("Itens")
	public Set<Fornecimentoitem> getListaFornecimentoitem() {
		return listaFornecimentoitem;
	}
	@OneToMany(fetch=FetchType.LAZY,mappedBy="fornecimento")
	public List<Documentoorigem> getListaDocumentoorigem() {
		return listaDocumentoorigem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaDocumentoorigem(
			List<Documentoorigem> listaDocumentoorigem) {
		this.listaDocumentoorigem = listaDocumentoorigem;
	}
	public void setFornecimentocontrato(
			Fornecimentocontrato fornecimentocontrato) {
		this.fornecimentocontrato = fornecimentocontrato;
	}
	public void setCdfornecimento(Integer cdfornecimento) {
		this.cdfornecimento = cdfornecimento;
	}
	public void setHistorico(String historico) {
		this.historico = historico;
	}
	public void setDtcontrato(Date dtcontrato) {
		this.dtcontrato = dtcontrato;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setBaixado(Boolean baixado) {
		this.baixado = baixado;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setListaFornecimentoitem(
			Set<Fornecimentoitem> listaFornecimentoitem) {
		this.listaFornecimentoitem = listaFornecimentoitem;
	}
	
	//=========================== TRANSIENT'S =======================
	
	@Transient
	@DisplayName("Baixado")
	public String getIsbaixado(){
		if(this.baixado != null){
			if(this.baixado) return "Sim";
		}
		return "N�o";
	}
	
	@Transient
	@DisplayName("Valor")
	public Money getValortrans(){
		return this.valor;
	}
	@Transient
	public Boolean getFromFornecimentocontrato() {
		return fromFornecimentocontrato;
	}
	public void setFromFornecimentocontrato(Boolean fromFornecimentocontrato) {
		this.fromFornecimentocontrato = fromFornecimentocontrato;
	}
	
	//CAMPO PARA CONTROLE DE GERA��O DA ENTRADA FISCAL
	public Integer getCdentregadocumento() {
		return cdentregadocumento;
	}
	public void setCdentregadocumento(Integer cdentregadocumento) {
		this.cdentregadocumento = cdentregadocumento;
	}

	public String subQueryProjeto() {
		return "select fornecimentosubQueryProjeto.cdfornecimento " +
				"from Fornecimento fornecimentosubQueryProjeto " +
				"join fornecimentosubQueryProjeto.fornecimentocontrato fornecimentocontratosubQueryProjeto " +
				"join fornecimentocontratosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}
	
	public String subQueryFornecedorEmpresa() {
		return "select fornecimentosubQueryFornecedorEmpresa.cdfornecimento " +
				"from Fornecimento fornecimentosubQueryFornecedorEmpresa " +
				"join fornecimentosubQueryFornecedorEmpresa.fornecimentocontrato fornecimentocontratosubQueryFornecedorEmpresa " +
				"join fornecimentocontratosubQueryFornecedorEmpresa.fornecedor fornecedorsubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorsubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}

	public boolean useFunctionProjeto() {
		return false;
	}
	
}
