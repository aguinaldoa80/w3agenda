package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_cotacaofornecedoritemsolicitacaocompra", sequenceName = "sq_cotacaofornecedoritemsolicitacaocompra")
public class Cotacaofornecedoritemsolicitacaocompra {

	protected Integer cdcotacaofornecedoritemsolicitacaocompra;
	protected Cotacaofornecedoritem cotacaofornecedoritem;
	protected Solicitacaocompra solicitacaocompra;
	
	public Cotacaofornecedoritemsolicitacaocompra() {
	}
	
	public Cotacaofornecedoritemsolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_cotacaofornecedoritemsolicitacaocompra")
	public Integer getCdcotacaofornecedoritemsolicitacaocompra() {
		return cdcotacaofornecedoritemsolicitacaocompra;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcotacaofornecedoritem")
	public Cotacaofornecedoritem getCotacaofornecedoritem() {
		return cotacaofornecedoritem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}

	public void setCdcotacaofornecedoritemsolicitacaocompra(
			Integer cdcotacaofornecedoritemsolicitacaocompra) {
		this.cdcotacaofornecedoritemsolicitacaocompra = cdcotacaofornecedoritemsolicitacaocompra;
	}

	public void setCotacaofornecedoritem(
			Cotacaofornecedoritem cotacaofornecedoritem) {
		this.cotacaofornecedoritem = cotacaofornecedoritem;
	}

	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}

}
