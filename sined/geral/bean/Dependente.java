package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_dependente", sequenceName = "sq_dependente")
public class Dependente {

	protected Integer cddependente;
	protected String nome;
	protected Tipodependente tipodependente;
	protected java.sql.Date dtnascimento;
	protected Municipio municipio;
	protected String cartorio;
	protected Integer registro;
	protected String livro;
	protected Integer folha;
	protected java.sql.Date dtcertidao;
	protected Boolean vacina;
	protected Boolean irpf;
	protected Boolean beneficio;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_dependente")
	public Integer getCddependente() {
		return cddependente;
	}
	public void setCddependente(Integer id) {
		this.cddependente = id;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipodependente")
	@Required
	public Tipodependente getTipodependente() {
		return tipodependente;
	}
	
	@Required
	public java.sql.Date getDtnascimento() {
		return dtnascimento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	
	@MaxLength(50)
	public String getCartorio() {
		return cartorio;
	}
	
	public Integer getRegistro() {
		return registro;
	}
	
	@MaxLength(5)
	public String getLivro() {
		return livro;
	}
	
	public Integer getFolha() {
		return folha;
	}
	
	public java.sql.Date getDtcertidao() {
		return dtcertidao;
	}
	
	public Boolean getVacina() {
		return vacina;
	}
	
	public Boolean getIrpf() {
		return irpf;
	}
	
	public Boolean getBeneficio() {
		return beneficio;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setTipodependente(Tipodependente tipodependente) {
		this.tipodependente = tipodependente;
	}
	
	public void setDtnascimento(java.sql.Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	public void setCartorio(String cartorio) {
		this.cartorio = cartorio;
	}
	
	public void setRegistro(Integer registro) {
		this.registro = registro;
	}
	
	public void setLivro(String livro) {
		this.livro = livro;
	}
	
	public void setFolha(Integer folha) {
		this.folha = folha;
	}
	
	public void setDtcertidao(java.sql.Date dtcertidao) {
		this.dtcertidao = dtcertidao;
	}
	
	public void setVacina(Boolean vacina) {
		this.vacina = vacina;
	}
	
	public void setIrpf(Boolean irpf) {
		this.irpf = irpf;
	}
	
	public void setBeneficio(Boolean beneficio) {
		this.beneficio = beneficio;
	}

}
