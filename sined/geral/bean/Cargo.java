package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_cargo", sequenceName = "sq_cargo")
public class Cargo implements Log{

	protected Integer cdcargo;
	protected String nome;
	protected String descricao;	
	protected Cbo cbo;
	protected Integer codigoFolha;
	protected Boolean ativo;
	protected Money custohora;
	protected Contagerencial contagerencial;
	protected Double totalhorasemana;
	protected Tipocargo tipocargo;
	protected Integer cdusuarioaltera;	
	protected Timestamp dtaltera;
	
	protected List<Colaboradorcargo> listaColaboradorcargo;
	protected List<Cargodepartamento> listaCargodepartamento = new ListSet<Cargodepartamento>(Cargodepartamento.class);
	protected Set<Tarefarecursohumano> listaTarefarecursohumano;
	protected Set<Cargomaterialseguranca> listaCargomaterialseguranca = new ListSet<Cargomaterialseguranca>(Cargomaterialseguranca.class);
	protected List<Planejamentorecursohumano> listaPlanejamentorecursohumano;
	protected List<Cargoatividade> listaCargoatividade = new ListSet<Cargoatividade>(Cargoatividade.class);
	protected List<Cargoexameobrigatorio> listaCargoexameobrigatorio = new ListSet<Cargoexameobrigatorio>(Cargoexameobrigatorio.class);
	
	//Trasient
	protected String departamento;
	
	public Cargo() {
	}
	public Cargo(Integer cdcargo) {
		this.cdcargo = cdcargo;
	}

	public Cargo(Integer cdcargo, String nome) {
		this.cdcargo = cdcargo;
		this.nome = nome;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargo")
	public Integer getCdcargo() {
		return cdcargo;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(500)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	
	@DisplayName("CBO")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcbo")	
	public Cbo getCbo() {
		return cbo;
	}

	@OneToMany(mappedBy = "cargo")
	@DisplayName("Departamento")
	public List<Cargodepartamento> getListaCargodepartamento() {
		return listaCargodepartamento;
	}
	
	@OneToMany(mappedBy = "cargo")
	public List<Colaboradorcargo> getListaColaboradorcargo() {
		return listaColaboradorcargo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}	
	
	@DisplayName("C�digo Folha")
	@MaxLength(9)
	@MinValue(0)
	public Integer getCodigoFolha() {
		return codigoFolha;
	}
	
	@Required
	@DisplayName("Tipo de cargo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdtipocargo")
	public Tipocargo getTipocargo() {
		return tipocargo;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@OneToMany(mappedBy="cargo")
	public Set<Tarefarecursohumano> getListaTarefarecursohumano() {
		return listaTarefarecursohumano;
	}
	
	@OneToMany(mappedBy="cargo")
	public List<Planejamentorecursohumano> getListaPlanejamentorecursohumano() {
		return listaPlanejamentorecursohumano;
	}
	
	@OneToMany(mappedBy="cargo")
	@DisplayName("Materiais de Seguran�a")
	public Set<Cargomaterialseguranca> getListaCargomaterialseguranca() {
		return listaCargomaterialseguranca;
	}
	
	@OneToMany(mappedBy="cargo")
	@DisplayName("Atividades")
	public List<Cargoatividade> getListaCargoatividade() {
		return listaCargoatividade;
	}
	
	public void setListaCargoatividade(List<Cargoatividade> listaCargoatividade) {
		this.listaCargoatividade = listaCargoatividade;
	}
	
	@DisplayName("Custo por hora")
	public Money getCustohora() {
		return custohora;
	}
	
	@DisplayName("Conta gerencial")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@DisplayName("Total de horas por semana")
	@MinValue(0)
	@MaxValue(999999999)
	public Double getTotalhorasemana() {
		return totalhorasemana;
	}
	
	@OneToMany(mappedBy="cargo")
	@DisplayName("Exames Obrigat�rios")
	public List<Cargoexameobrigatorio> getListaCargoexameobrigatorio() {
		return listaCargoexameobrigatorio;
	}
	
	public void setListaCargoexameobrigatorio(
			List<Cargoexameobrigatorio> listaCargoexameobrigatorio) {
		this.listaCargoexameobrigatorio = listaCargoexameobrigatorio;
	}
	
	public void setListaCargomaterialseguranca(
			Set<Cargomaterialseguranca> listaCargomaterialseguranca) {
		this.listaCargomaterialseguranca = listaCargomaterialseguranca;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	
	public void setCustohora(Money custohora) {
		this.custohora = custohora;
	}

	public void setCdcargo(Integer cdcargo) {
		this.cdcargo = cdcargo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setCbo(Cbo cbo) {
		this.cbo = cbo;
	}

	public void setCodigoFolha(Integer codigoFolha) {
		this.codigoFolha = codigoFolha;
	}
	
	public void setTipocargo(Tipocargo tipocargo) {
		this.tipocargo = tipocargo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setListaCargodepartamento(
			List<Cargodepartamento> listaCargodepartamento) {
		this.listaCargodepartamento = listaCargodepartamento;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setTotalhorasemana(Double totalhorasemana) {
		this.totalhorasemana = totalhorasemana;
	}
	
	@Transient
	public String getDepartamento() {
		return getListaCargodepartamento() != null ? SinedUtil.listAndConcatenateSort(getListaCargodepartamento(), "departamento.nome", ", ") : "";
	}
	
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public void setListaColaboradorcargo(
			List<Colaboradorcargo> listaColaboradorcargo) {
		this.listaColaboradorcargo = listaColaboradorcargo;
	}
	
	public void setListaTarefarecursohumano(Set<Tarefarecursohumano> listaTarefarecursohumano) {
		this.listaTarefarecursohumano = listaTarefarecursohumano;
	}
	public void setListaPlanejamentorecursohumano(
			List<Planejamentorecursohumano> listaPlanejamentorecursohumano) {
		this.listaPlanejamentorecursohumano = listaPlanejamentorecursohumano;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdcargo == null) ? 0 : cdcargo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (cdcargo == null) {
			if (other.cdcargo != null)
				return false;
		} else if (!cdcargo.equals(other.cdcargo))
			return false;
		return true;
	}
	
	
	
}
