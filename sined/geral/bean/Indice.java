package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@DisplayName("Composi��o")
@SequenceGenerator(name="sq_indice",sequenceName="sq_indice")
public class Indice implements Log {

	protected Integer cdindice;
	protected Unidademedida unidademedida;
	protected String identificador;
	protected String descricao;
	protected Double qtde;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Indicerecursogeral> listaIndicerecursogeral = new ListSet<Indicerecursogeral>(Indicerecursogeral.class);
	protected List<Indicerecursohumano> listaIndicerecursohumano = new ListSet<Indicerecursohumano>(Indicerecursohumano.class);
	
	public Indice() {
	}
	
	public Indice(Integer cdindice) {
		this.cdindice = cdindice;
	}
	
	public Indice(String identificador){
		this.identificador = identificador;
	}
	
	@Id
	@GeneratedValue(generator="sq_indice", strategy=GenerationType.AUTO)
	public Integer getCdindice() {
		return cdindice;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	@Required
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	@DisplayName("Descri��o")
	@MaxLength(300)
	@Required
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@DisplayName("Qtde.")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	
	@MaxLength(10)
	@Required
	@DescriptionProperty()
	public String getIdentificador() {
		return identificador;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	
	public void setCdindice(Integer cdindice) {
		this.cdindice = cdindice;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
//	LISTAS
	
	@OneToMany(mappedBy="indice")
	@DisplayName("Recursos gerais")
	public List<Indicerecursogeral> getListaIndicerecursogeral() {
		return listaIndicerecursogeral;
	}
	
	@OneToMany(mappedBy="indice")
	@DisplayName("Recursos humanos")
	public List<Indicerecursohumano> getListaIndicerecursohumano() {
		return listaIndicerecursohumano;
	}
	
	public void setListaIndicerecursogeral(
			List<Indicerecursogeral> listaIndicerecursogeral) {
		this.listaIndicerecursogeral = listaIndicerecursogeral;
	}
	
	public void setListaIndicerecursohumano(
			List<Indicerecursohumano> listaIndicerecursohumano) {
		this.listaIndicerecursohumano = listaIndicerecursohumano;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdindice == null) ? 0 : cdindice.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Indice))
			return false;
		final Indice other = (Indice) obj;
		if (cdindice == null) {
			if (other.cdindice != null)
				return false;
		} else if (!cdindice.equals(other.cdindice))
			return false;
		return true;
	}
}
