package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_eventosfinaiscorreios", sequenceName = "sq_eventosfinaiscorreios")
@DisplayName("Eventos dos correios")
public class EventosFinaisCorreios implements Log {
	protected Integer cdeventosfinaiscorreios;
	protected String nome;
	protected String corLetra;
	protected String corFundo;
	protected String letra;
	protected Boolean finalizadoSucesso;
	protected Boolean insucesso;
	protected Boolean enviaemailinsucesso;
	protected String email;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	protected List<TipoEventos> listaTipoEventos = new ListSet<TipoEventos>(TipoEventos.class);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_eventosfinaiscorreios")
	public Integer getCdeventosfinaiscorreios() {
		return cdeventosfinaiscorreios;
	}
	public void setCdeventosfinaiscorreios(Integer cdeventosfinaiscorreios) {
		this.cdeventosfinaiscorreios = cdeventosfinaiscorreios;
	}
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Required
	public String getCorLetra() {
		return corLetra;
	}
	public void setCorLetra(String corLetra) {
		this.corLetra = corLetra;
	}
	@Required
	public String getCorFundo() {
		return corFundo;
	}
	public void setCorFundo(String corFundo) {
		this.corFundo = corFundo;
	}
	@Required
	@MaxLength(2)
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}
	@DisplayName("FINALIZADO COM SUCESSO")
	public Boolean getFinalizadoSucesso() {
		return finalizadoSucesso;
	}
	public void setFinalizadoSucesso(Boolean finalizadoSucesso) {
		this.finalizadoSucesso = finalizadoSucesso;
	}
	@DisplayName("Entrega com impedimentos")
	public Boolean getInsucesso() {
		return insucesso;
	}
	public void setInsucesso(Boolean insucesso) {
		this.insucesso = insucesso;
	}
	@DisplayName("ENVIA E-MAIL DE INSUCESSO")
	public Boolean getEnviaemailinsucesso() {
		return enviaemailinsucesso;
	}
	public void setEnviaemailinsucesso(Boolean enviaemailinsucesso) {
		this.enviaemailinsucesso = enviaemailinsucesso;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@DisplayName("Tipo de evento")
	@OneToMany(mappedBy="eventosFinaisCorreios")
	public List<TipoEventos> getListaTipoEventos() {
		return listaTipoEventos;
	}
	public void setListaTipoEventos(List<TipoEventos> listaTipoEventos) {
		this.listaTipoEventos = listaTipoEventos;
	}
}
