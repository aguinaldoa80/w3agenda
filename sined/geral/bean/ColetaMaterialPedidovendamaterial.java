package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_coletamaterialpedidovendamaterial", sequenceName="sq_coletamaterialpedidovendamaterial")
public class ColetaMaterialPedidovendamaterial {
	protected Integer cdColetaMaterialPedidovendamaterial;
	protected ColetaMaterial coletaMaterial;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Boolean producaoRecusada;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator="sq_coletamaterialpedidovendamaterial")
	public Integer getCdColetaMaterialPedidovendamaterial() {
		return cdColetaMaterialPedidovendamaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdcoletamaterial")
	public ColetaMaterial getColetaMaterial() {
		return coletaMaterial;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	
	public void setCdColetaMaterialPedidovendamaterial(
			Integer cdColetaMaterialPedidovendamaterial) {
		this.cdColetaMaterialPedidovendamaterial = cdColetaMaterialPedidovendamaterial;
	}
	
	public void setColetaMaterial(ColetaMaterial coletaMaterial) {
		this.coletaMaterial = coletaMaterial;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public Boolean getProducaoRecusada() {
		return producaoRecusada;
	}
	public void setProducaoRecusada(Boolean producaoRecusada) {
		this.producaoRecusada = producaoRecusada;
	}
}
