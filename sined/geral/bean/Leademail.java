package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_leademail", sequenceName = "sq_leademail")
public class Leademail {
	
	protected Integer cdleademail;
	protected String email;
	protected Lead lead;
	protected String contato;

	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_leademail")
	public Integer getCdleademail() {
		return cdleademail;
	}
	@DisplayName("E-mail")
	@MaxLength(80)
	public String getEmail() {
		return email;
	}
	
	@DisplayName("Lead")
	@JoinColumn(name="cdlead")
	@ManyToOne(fetch=FetchType.LAZY)
	public Lead getLead() {
		return lead;
	}
	
	@Required
	@DisplayName("Contato")
	@MaxLength(80)
	public String getContato() {
		return contato;
	}
	
	
	
	public void setCdleademail(Integer cdleademail) {
		this.cdleademail = cdleademail;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setLead(Lead lead) {
		this.lead = lead;
	}
	
	
	public void setContato(String contato){
		this.contato = contato;
	}
}
