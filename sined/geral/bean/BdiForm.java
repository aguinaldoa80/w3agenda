package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiItemForm;
import br.com.linkcom.sined.modulo.projeto.controller.process.bean.BdiTributoForm;

public class BdiForm {

	protected Integer cdBdi;
	protected Orcamento orcamento;
	protected Money precoCusto;
	protected Double totalTaxas;
	protected Double totalTributos;
	protected Money precoTotalComTaxas;
	protected Double bdiGeralComImpostos;
	protected Double folga;	
	protected Money precoComFolga;
	protected Double bdiGeral;
	protected List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoForm = new ArrayList<BdiComposicaoOrcamentoForm>();
	protected List<BdiItemForm> listaBdiItemForm = new ArrayList<BdiItemForm>();
	protected List<BdiTributoForm> listaBdiTributoForm = new ArrayList<BdiTributoForm>();
	
	public Integer getCdBdi() {
		return cdBdi;
	}
	public Orcamento getOrcamento() {
		return orcamento;
	}
	public Money getPrecoCusto() {
		return precoCusto;
	}
	public Double getTotalTaxas() {
		return totalTaxas;
	}
	public Double getTotalTributos() {
		return totalTributos;
	}
	public Money getPrecoTotalComTaxas() {
		return precoTotalComTaxas;
	}
	public Double getBdiGeralComImpostos() {
		return bdiGeralComImpostos;
	}
	public Double getFolga() {
		return folga;
	}
	public Money getPrecoComFolga() {
		return precoComFolga;
	}
	public Double getBdiGeral() {
		return bdiGeral;
	}
	public List<BdiComposicaoOrcamentoForm> getListaBdiComposicaoOrcamentoForm() {
		return listaBdiComposicaoOrcamentoForm;
	}
	public List<BdiItemForm> getListaBdiItemForm() {
		return listaBdiItemForm;
	}
	public List<BdiTributoForm> getListaBdiTributoForm() {
		return listaBdiTributoForm;
	}
	
	public void setCdBdi(Integer cdBdi) {
		this.cdBdi = cdBdi;
	}
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	public void setPrecoCusto(Money precoCusto) {
		this.precoCusto = precoCusto;
	}
	public void setTotalTaxas(Double totalTaxas) {
		this.totalTaxas = totalTaxas;
	}
	public void setTotalTributos(Double totalTributos) {
		this.totalTributos = totalTributos;
	}
	public void setPrecoTotalComTaxas(Money precoTotalComTaxas) {
		this.precoTotalComTaxas = precoTotalComTaxas;
	}
	public void setBdiGeralComImpostos(Double bdiGeralComImpostos) {
		this.bdiGeralComImpostos = bdiGeralComImpostos;
	}
	public void setFolga(Double folga) {
		this.folga = folga;
	}
	public void setPrecoComFolga(Money precoComFolga) {
		this.precoComFolga = precoComFolga;
	}
	public void setBdiGeral(Double bdiGeral) {
		this.bdiGeral = bdiGeral;
	}
	public void setListaBdiComposicaoOrcamentoForm(List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoForm) {
		this.listaBdiComposicaoOrcamentoForm = listaBdiComposicaoOrcamentoForm;
	}
	public void setListaBdiItemForm(List<BdiItemForm> listaBdiItemForm) {
		this.listaBdiItemForm = listaBdiItemForm;
	}
	public void setListaBdiTributoForm(List<BdiTributoForm> listaBdiTributoForm) {
		this.listaBdiTributoForm = listaBdiTributoForm;
	}
}
