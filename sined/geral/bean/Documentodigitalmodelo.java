package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_documentodigitalmodelo",sequenceName="sq_documentodigitalmodelo")
public class Documentodigitalmodelo implements Log{

	protected Integer cddocumentodigitalmodelo;
	protected String nome;
	protected ReportTemplateBean reporttemplate;
	protected String textotelaaceite;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_documentodigitalmodelo",strategy=GenerationType.AUTO)
	public Integer getCddocumentodigitalmodelo() {
		return cddocumentodigitalmodelo;
	}
	
	@DisplayName("Template de e-mail")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdreporttemplate")
	public ReportTemplateBean getReporttemplate() {
		return reporttemplate;
	}
	
	@Required
	@DescriptionProperty
	@MaxLength(100)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Texto para tela de aceite")
	public String getTextotelaaceite() {
		return textotelaaceite;
	}
	
	public void setTextotelaaceite(String textotelaaceite) {
		this.textotelaaceite = textotelaaceite;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setReporttemplate(ReportTemplateBean reporttemplate) {
		this.reporttemplate = reporttemplate;
	}

	public void setCddocumentodigitalmodelo(Integer cddocumentodigitalmodelo) {
		this.cddocumentodigitalmodelo = cddocumentodigitalmodelo;
	}
	
	
	//Log
	
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
