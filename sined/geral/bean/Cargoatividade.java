package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_cargoatividade", sequenceName = "sq_cargoatividade")
public class Cargoatividade{

	protected Integer cdcargoatividade;
	protected Cargo cargo;
	protected String descricao;
	protected Atividadetipo atividadetipo;
	protected Money valorhoradiferenciada;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_cargoatividade")
	public Integer getCdcargoatividade() {
		return cdcargoatividade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	@DisplayName("Descri��o")
	@MaxLength(400)
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Tipo de Atividade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatividadetipo")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@DisplayName("Valor Hora Diferenciada")
	public Money getValorhoradiferenciada() {
		return valorhoradiferenciada;
	}
	
	public void setCdcargoatividade(Integer cdcargoatividade) {
		this.cdcargoatividade = cdcargoatividade;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setValorhoradiferenciada(Money valorhoradiferenciada) {
		this.valorhoradiferenciada = valorhoradiferenciada;
	}
}
