package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_pedidovendamaterialunidademedidasincronizacao", sequenceName = "sq_pedidovendamaterialunidademedidasincronizacao")
public class Pedidovendamaterialunidademedidasincronizacao {

	protected Integer cdpedidovendamaterialunidademedidasincronizacao;
	protected Pedidovendasincronizacao pedidovendasincronizacao;
	
	//Material
	protected Material material;
	
	//Unidade medida
	protected Unidademedida unidademedida;

	//Materialunidademedida
	protected Materialunidademedida materialunidademedida;
	protected Double fracao;
	protected Double valorunitario;	
	protected Double qtdereferencia;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendamaterialunidademedidasincronizacao")
	public Integer getCdpedidovendamaterialunidademedidasincronizacao() {
		return cdpedidovendamaterialunidademedidasincronizacao;
	}
	public void setCdpedidovendamaterialunidademedidasincronizacao(Integer cdpedidovendamaterialunidademedidasincronizacao) {
		this.cdpedidovendamaterialunidademedidasincronizacao = cdpedidovendamaterialunidademedidasincronizacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendasincronizacao")
	public Pedidovendasincronizacao getPedidovendasincronizacao() {
		return pedidovendasincronizacao;
	}
	public void setPedidovendasincronizacao(Pedidovendasincronizacao pedidovendasincronizacao) {
		this.pedidovendasincronizacao = pedidovendasincronizacao;
	}

	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	@Required
	@DisplayName("Material unidade medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialunidademedida")
	public Materialunidademedida getMaterialunidademedida() {
		return materialunidademedida;
	}
	public void setMaterialunidademedida(Materialunidademedida materialunidademedida) {
		this.materialunidademedida = materialunidademedida;
	}
	
	@DisplayName("Fra��o")
	public Double getFracao() {
		return fracao;
	}
	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	
	@DisplayName("Valor Unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	
	@DisplayName("Qtde. Refer�ncia")
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
}
