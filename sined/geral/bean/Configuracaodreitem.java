package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_configuracaodreitem", sequenceName = "sq_configuracaodreitem")
@DisplayName("Item da DRE")
public class Configuracaodreitem {
	
	protected Integer cdconfiguracaodreitem; 
	protected Configuracaodre configuracaodre;
	protected Configuracaodretipo configuracaodretipo;
	protected ContaContabil contacontabil;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_configuracaodreitem")
	public Integer getCdconfiguracaodreitem() {
		return cdconfiguracaodreitem;
	}

	@Required
	@JoinColumn(name="cdconfiguracaodre")
	@ManyToOne(fetch=FetchType.LAZY)
	public Configuracaodre getConfiguracaodre() {
		return configuracaodre;
	}

	@Required
	@DisplayName("Tipo")
	@JoinColumn(name="cdconfiguracaodretipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Configuracaodretipo getConfiguracaodretipo() {
		return configuracaodretipo;
	}

	@Required
	@DisplayName("Conta cont�bil")
	@JoinColumn(name="cdcontacontabil")
	@ManyToOne(fetch=FetchType.LAZY)
	public ContaContabil getContacontabil() {
		return contacontabil;
	}

	public void setCdconfiguracaodreitem(Integer cdconfiguracaodreitem) {
		this.cdconfiguracaodreitem = cdconfiguracaodreitem;
	}

	public void setConfiguracaodre(Configuracaodre configuracaodre) {
		this.configuracaodre = configuracaodre;
	}

	public void setConfiguracaodretipo(Configuracaodretipo configuracaodretipo) {
		this.configuracaodretipo = configuracaodretipo;
	}

	public void setContacontabil(ContaContabil contacontabil) {
		this.contacontabil = contacontabil;
	}
	
}
	