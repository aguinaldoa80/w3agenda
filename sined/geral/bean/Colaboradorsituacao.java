package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_colaboradorsituacao", sequenceName = "sq_colaboradorsituacao")
public class Colaboradorsituacao {

	protected Integer cdcolaboradorsituacao;
	protected String nome;
	
	public static final Integer NORMAL = 1;
	public static final Integer AFASTADO = 2;
	public static final Integer CONTRATO_EXPERIENCIA = 3;
	public static final Integer AVISO_PREVIO = 4;
	public static final Integer FERIAS = 5;
	public static final Integer DEMITIDO = 6;
	public static final Integer EM_CONTRATACAO = 7;
	public static final Integer PEDIDO_DE_DEMISSAO = 8;

	public Colaboradorsituacao(){}
	
	public Colaboradorsituacao(Integer cdcolaboradorsituacao){
		this.cdcolaboradorsituacao = cdcolaboradorsituacao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorsituacao")
	public Integer getCdcolaboradorsituacao() {
		return cdcolaboradorsituacao;
	}
	public void setCdcolaboradorsituacao(Integer id) {
		this.cdcolaboradorsituacao = id;
	}

	
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}

}
