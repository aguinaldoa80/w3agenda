package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_despesaviagemtipo", sequenceName = "sq_despesaviagemtipo")
public class Despesaviagemtipo implements Log{

	protected Integer cddespesaviagemtipo;
	protected String nome;
	protected Boolean ativo = true;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Contagerencial contagerencial;
	protected Money valorbase;
	
	public Despesaviagemtipo(){
	}
	
	public Despesaviagemtipo(Integer cddespesaviagemtipo){
		this.cddespesaviagemtipo = cddespesaviagemtipo;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_despesaviagemtipo")
	public Integer getCddespesaviagemtipo() {
		return cddespesaviagemtipo;
	}
	
	@Required
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCddespesaviagemtipo(Integer cddespesaviagemtipo) {
		this.cddespesaviagemtipo = cddespesaviagemtipo;
	}
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cddespesaviagemtipo == null) ? 0 : cddespesaviagemtipo
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Despesaviagemtipo other = (Despesaviagemtipo) obj;
		if (cddespesaviagemtipo == null) {
			if (other.cddespesaviagemtipo != null)
				return false;
		} else if (!cddespesaviagemtipo.equals(other.cddespesaviagemtipo))
			return false;
		return true;
	}
	
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	
	@DisplayName("Valor Base")
	public Money getValorbase() {
		return valorbase;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setValorbase(Money valorbase) {
		this.valorbase = valorbase;
	}	
}