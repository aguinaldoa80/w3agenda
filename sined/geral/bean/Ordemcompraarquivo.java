package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_ordemcompraarquivo", sequenceName = "sq_ordemcompraarquivo")
@DisplayName("Arquivo")
public class Ordemcompraarquivo implements Log{
	
	protected Integer cdordemcompraarquivo;
	protected Ordemcompra ordemcompra;
	protected Arquivo arquivo;
	protected Integer cdUsuarioAltera;
	protected Timestamp dtAltera;
	protected Boolean enviarFornecedor;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ordemcompraarquivo")
	public Integer getCdordemcompraarquivo() {
		return cdordemcompraarquivo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompra")
	public Ordemcompra getOrdemcompra() {
		return ordemcompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	@Required
	public Arquivo getArquivo() {
		return arquivo;
	}
	public Integer getCdusuarioaltera() {
		return cdUsuarioAltera;
	}
	public Timestamp getDtaltera() {
		return dtAltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;
	}
	public void setCdordemcompraarquivo(Integer cdordemcompraarquivo) {
		this.cdordemcompraarquivo = cdordemcompraarquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public void setOrdemcompra(Ordemcompra ordemcompra) {
		this.ordemcompra = ordemcompra;
	}
	
	@DisplayName("Enviar ao fornecedor")
	public Boolean getEnviarFornecedor() {
		return enviarFornecedor;
	}
	public void setEnviarFornecedor(Boolean enviarFornecedor) {
		this.enviarFornecedor = enviarFornecedor;
	}

	
}