package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocoluna;

@Entity
@SequenceGenerator(name = "sq_campolistagem", sequenceName = "sq_campolistagem")
public class Campolistagem{
	
	protected Integer cdcampolistagem;
	protected Integer ordemjsp;
	protected String nome;
	protected Boolean exibir;
	protected Integer ordemexibicao;
	protected Tela tela;
	protected Tipocoluna tipocoluna;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_campolistagem")
	public Integer getCdcampolistagem() {
		return cdcampolistagem;
	}
	
	@DisplayName("Ordem no JSP")
	@Required
	public Integer getOrdemjsp() {
		return ordemjsp;
	}

	@Required
	@DisplayName("Campo")
	public String getNome() {
		return nome;
	}
	@Required
	@DisplayName("Exibir")
	public Boolean getExibir() {
		return exibir;
	}
	
	@DisplayName("Ordem de exibi��o")
	@Required
	public Integer getOrdemexibicao() {
		return ordemexibicao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtela")	
	public Tela getTela() {
		return tela;
	}	
	
	@DisplayName("Tipo de coluna")
	@Required
	public Tipocoluna getTipocoluna() {
		return tipocoluna;
	}
	public void setCdcampolistagem(Integer cdcampolistagem) {
		this.cdcampolistagem = cdcampolistagem;
	}
	public void setOrdemjsp(Integer ordemjsp) {
		this.ordemjsp = ordemjsp;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setExibir(Boolean exibir) {
		this.exibir = exibir;
	}
	public void setOrdemexibicao(Integer ordemexibicao) {
		this.ordemexibicao = ordemexibicao;
	}
	public void setTela(Tela tela) {
		this.tela = tela;
	}
	public void setTipocoluna(Tipocoluna tipocoluna) {
		this.tipocoluna = tipocoluna;
	}
}
