package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_ecom_cliente", sequenceName="sq_ecom_cliente")
public class Ecom_Cliente {

	private Integer cdEcom_Cliente;
	private String nome;
	private String razaoSocial;
	private String sobrenome;
	private String tipo;
	private String cpf_cnpj;
	private String rg;
	private String sexo;
	private String email;
	private String nacionalidade;
	private String data_nascimento;
	private String orgao_emissor;
	private String inscricao_estadual;
	private Ecom_Endereco endereco;
	private List<Ecom_Telefone> telefones;
	private List<Ecom_Endereco> listaEnderecos;
	
	
	@Id
	@GeneratedValue(generator="sq_ecom_cliente", strategy=GenerationType.AUTO)
	public Integer getCdEcom_Cliente() {
		return cdEcom_Cliente;
	}
	public void setCdEcom_Cliente(Integer cdEcom_Cliente) {
		this.cdEcom_Cliente = cdEcom_Cliente;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCpf_cnpj() {
		return cpf_cnpj;
	}
	public void setCpf_cnpj(String cpf_cnpj) {
		this.cpf_cnpj = cpf_cnpj;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public String getData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public String getOrgao_emissor() {
		return orgao_emissor;
	}
	public void setOrgao_emissor(String orgao_emissor) {
		this.orgao_emissor = orgao_emissor;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdecom_endereco")
	public Ecom_Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Ecom_Endereco endereco) {
		this.endereco = endereco;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ecomCliente")
	public List<Ecom_Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Ecom_Telefone> telefones) {
		this.telefones = telefones;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="ecomCliente")
	public List<Ecom_Endereco> getListaEnderecos() {
		return listaEnderecos;
	}
	public void setListaEnderecos(List<Ecom_Endereco> listaEnderecos) {
		this.listaEnderecos = listaEnderecos;
	}
	public String getInscricao_estadual() {
		return inscricao_estadual;
	}
	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}
}
