package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "sq_contratofaturalocacaodocumento", sequenceName = "sq_contratofaturalocacaodocumento")
public class Contratofaturalocacaodocumento {

	protected Integer cdcontratofaturalocacaodocumento;
	protected Contratofaturalocacao contratofaturalocacao;
	protected Documento documento;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratofaturalocacaodocumento")
	public Integer getCdcontratofaturalocacaodocumento() {
		return cdcontratofaturalocacaodocumento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratofaturalocacao")
	public Contratofaturalocacao getContratofaturalocacao() {
		return contratofaturalocacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	public void setCdcontratofaturalocacaodocumento(
			Integer cdcontratofaturalocacaodocumento) {
		this.cdcontratofaturalocacaodocumento = cdcontratofaturalocacaodocumento;
	}
	
	public void setContratofaturalocacao(Contratofaturalocacao contratofaturalocacao) {
		this.contratofaturalocacao = contratofaturalocacao;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
}
