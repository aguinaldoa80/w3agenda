package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_segmento",sequenceName="sq_segmento")
public class Segmento implements Log {

	public Integer cdsegmento;
	public String nome;
	public Integer cdusuarioaltera;
	public Timestamp dtaltera;
	private List<CampoExtraSegmento> campoExtraSegmentoList;
	
	//Transient
	private CampoExtraSegmento campoExtraSegmento;
	
	@Override
	public String toString() {
		return this.nome;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_segmento")
	public Integer getCdsegmento() {
		return cdsegmento;
	}

	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Campos Extras")
	@OneToMany(mappedBy="segmento")
	public List<CampoExtraSegmento> getCampoExtraSegmentoList() {
		return campoExtraSegmentoList;
	}

	public void setCdsegmento(Integer cdsegmento) {
		this.cdsegmento = cdsegmento;
	}

	public void setNome(String nome) {
		this.nome = nome.trim();
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCampoExtraSegmentoList(List<CampoExtraSegmento> campoExtraSegmentoList) {
		this.campoExtraSegmentoList = campoExtraSegmentoList;
	}
	
	@Transient
	@DisplayName("Campo Adicional")
	public CampoExtraSegmento getCampoExtraSegmento() {
		return campoExtraSegmento;
	}
	
	public void setCampoExtraSegmento(CampoExtraSegmento campoExtraSegmento) {
		this.campoExtraSegmento = campoExtraSegmento;
	}
}
