package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@DisplayName("Situa��o")
public class Documentoprocessosituacao{
	
	//Atributos
	protected Integer cddocumentoprocessosituacao;
	protected String nome;
	
	//Atributos est�ticos
	public static Documentoprocessosituacao DISPONIVEL = new Documentoprocessosituacao(1, "Dispon�vel");
	public static Documentoprocessosituacao EM_APROVACAO = new Documentoprocessosituacao(2, "Em aprova��o");
	public static Documentoprocessosituacao ARQUIVO_MORTO = new Documentoprocessosituacao(3, "Arquivo morto");
	
	//Construtores
	public Documentoprocessosituacao() {
	}
	
	public Documentoprocessosituacao(Integer cddocumentoprocessosituacao) {
		this.cddocumentoprocessosituacao = cddocumentoprocessosituacao;
	}
	
	
	
	public Documentoprocessosituacao(Integer cddocumentoprocessosituacao, String nome) {
		this.cddocumentoprocessosituacao = cddocumentoprocessosituacao;
		this.nome = nome;
	}

	@Id
	public Integer getCddocumentoprocessosituacao() {
		return cddocumentoprocessosituacao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCddocumentoprocessosituacao(Integer cddocumentoprocessosituacao) {
		this.cddocumentoprocessosituacao = cddocumentoprocessosituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Documentoprocessosituacao){
			Documentoprocessosituacao situacao = 
				(Documentoprocessosituacao)obj;
			return this.getCddocumentoprocessosituacao().
			equals(situacao.getCddocumentoprocessosituacao());
		}
		return super.equals(obj);
	}

}
