package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Orcamentosituacao {
	
	public static Orcamentosituacao EM_ABERTO = new Orcamentosituacao(1);
	public static Orcamentosituacao CONCLUIDO = new Orcamentosituacao(2);
	
	protected Integer cdorcamentosituacao;
	protected String nome;
	
	public Orcamentosituacao(){}
	
	public Orcamentosituacao(Integer cdarquivobancariosituacao){
		this.cdorcamentosituacao = cdarquivobancariosituacao;
	}
	
	@Id
	public Integer getCdorcamentosituacao() {
		return cdorcamentosituacao;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdorcamentosituacao(Integer cdorcamentosituacao) {
		this.cdorcamentosituacao = cdorcamentosituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Orcamentosituacao) {
			Orcamentosituacao orcamentosituacao = (Orcamentosituacao) obj;
			return orcamentosituacao.getCdorcamentosituacao().equals(this.getCdorcamentosituacao());
		}
		return super.equals(obj);
	}
	
}

