package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_vara", sequenceName = "sq_vara")
public class Vara implements Log {
	
	protected Integer cdvara;
	protected String nome;
	protected Municipio municipio;
	protected Uf uf;
	protected Orgaojuridico orgaojuridico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Vara() {	}
	public Vara(Integer cdvara){
		this.cdvara = cdvara;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vara")
	public Integer getCdvara() {
		return cdvara;
	}
	
	@Required
	@MaxLength(100)
	@DescriptionProperty
	@DisplayName("Descri��o")
	public String getNome() {
		return nome;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipio")
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}
	
	@DisplayName("Org�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorgaojuridico")
	public Orgaojuridico getOrgaojuridico() {
		return orgaojuridico;
	}
	
	public void setOrgaojuridico(Orgaojuridico orgaojuridico) {
		this.orgaojuridico = orgaojuridico;
	}
	public void setCdvara(Integer cdvara) {
		this.cdvara = cdvara;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
