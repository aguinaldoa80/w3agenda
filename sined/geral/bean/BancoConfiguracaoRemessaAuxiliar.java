package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessaauxiliar",sequenceName="sq_bancoconfiguracaoremessaauxiliar")
public class BancoConfiguracaoRemessaAuxiliar {

	private Integer cdbancoconfiguracaoremessaauxiliar;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa1;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa2;
	private String codigo;
	private String nome;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessaauxiliar")
	public Integer getCdbancoconfiguracaoremessaauxiliar() {
		return cdbancoconfiguracaoremessaauxiliar;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa1")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa1() {
		return bancoConfiguracaoRemessa1;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa2")
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa2() {
		return bancoConfiguracaoRemessa2;
	}

	@DisplayName("C�digo")
	@Required
	public String getCodigo() {
		return codigo;
	}
	
	@DisplayName("Nome")
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdbancoconfiguracaoremessaauxiliar(
			Integer cdbancoconfiguracaoremessaauxiliar) {
		this.cdbancoconfiguracaoremessaauxiliar = cdbancoconfiguracaoremessaauxiliar;
	}

	public void setBancoConfiguracaoRemessa1(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa1) {
		this.bancoConfiguracaoRemessa1 = bancoConfiguracaoRemessa1;
	}

	public void setBancoConfiguracaoRemessa2(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa2) {
		this.bancoConfiguracaoRemessa2 = bancoConfiguracaoRemessa2;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
