package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_ecommaterial", sequenceName = "sq_ecommaterial")
public class Ecommaterial {

	protected Integer cdecommaterial;
	protected Material material;
	protected String identificador;
	protected String identificadormaterial;
	protected String nome;
	protected Double valorvenda;
	protected Double qtdmin;
	protected Double pesobruto;
	protected Boolean calculofrete;
	protected Double comprimento;
	protected Double largura;
	protected Double altura;
	protected Boolean ativo;
	protected Ecommaterialestoque ecommaterialestoque;
	protected Configuracaoecom configuracaoecom;
	
	//FLAGS
	protected Boolean sincronizado;
	protected Timestamp data;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ecommaterial")
	public Integer getCdecommaterial() {
		return cdecommaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	public String getIdentificador() {
		return identificador;
	}

	public String getNome() {
		return nome;
	}

	public Double getValorvenda() {
		return valorvenda;
	}

	public Double getQtdmin() {
		return qtdmin;
	}

	public Double getPesobruto() {
		return pesobruto;
	}

	public Boolean getCalculofrete() {
		return calculofrete;
	}

	public Double getComprimento() {
		return comprimento;
	}

	public Double getLargura() {
		return largura;
	}

	public Double getAltura() {
		return altura;
	}

	public Boolean getSincronizado() {
		return sincronizado;
	}

	public Timestamp getData() {
		return data;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="cdmaterial", referencedColumnName="cdmaterial", insertable=false, updatable=false),
		@JoinColumn(name="cdconfiguracaoecom", referencedColumnName="cdconfiguracaoecom", insertable=false, updatable=false)
	})
	public Ecommaterialestoque getEcommaterialestoque() {
		return ecommaterialestoque;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaoecom")
	public Configuracaoecom getConfiguracaoecom() {
		return configuracaoecom;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public String getIdentificadormaterial() {
		return identificadormaterial;
	}
	
	public void setIdentificadormaterial(String identificadormaterial) {
		this.identificadormaterial = identificadormaterial;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setConfiguracaoecom(Configuracaoecom configuracaoecom) {
		this.configuracaoecom = configuracaoecom;
	}
	
	public void setEcommaterialestoque(Ecommaterialestoque ecommaterialestoque) {
		this.ecommaterialestoque = ecommaterialestoque;
	}

	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public void setCdecommaterial(Integer cdecommaterial) {
		this.cdecommaterial = cdecommaterial;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}

	public void setQtdmin(Double qtdmin) {
		this.qtdmin = qtdmin;
	}

	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}

	public void setCalculofrete(Boolean calculofrete) {
		this.calculofrete = calculofrete;
	}

	public void setComprimento(Double comprimento) {
		this.comprimento = comprimento;
	}

	public void setLargura(Double largura) {
		this.largura = largura;
	}

	public void setAltura(Double altura) {
		this.altura = altura;
	}
	
}
