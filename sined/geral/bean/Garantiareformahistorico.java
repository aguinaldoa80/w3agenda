package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name="sq_garantiareformahistorico", sequenceName="sq_garantiareformahistorico")
@DisplayName("Hist�rico")
public class Garantiareformahistorico implements Log{

	private Integer cdgarantiareformahistorico;
	private Garantiareforma garantiareforma;
	private Garantiasituacao acao;
	private String observacao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private String selectedItens;
	
	@Id
	@GeneratedValue(generator="sq_garantiareformahistorico", strategy=GenerationType.AUTO)
	public Integer getCdgarantiareformahistorico() {
		return cdgarantiareformahistorico;
	}
	public void setCdgarantiareformahistorico(Integer cdgarantiareformahistorico) {
		this.cdgarantiareformahistorico = cdgarantiareformahistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgarantiareforma")
	public Garantiareforma getGarantiareforma() {
		return garantiareforma;
	}
	public void setGarantiareforma(Garantiareforma garantiareforma) {
		this.garantiareforma = garantiareforma;
	}
	
	@DisplayName("A��o")
	public Garantiasituacao getAcao() {
		return acao;
	}
	public void setAcao(Garantiasituacao acao) {
		this.acao = acao;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return this.cdusuarioaltera;
	}
	@Override
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return this.dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@Transient
	public String getSelectedItens() {
		return selectedItens;
	}
	public void setSelectedItens(String selectedItens) {
		this.selectedItens = selectedItens;
	}
}
