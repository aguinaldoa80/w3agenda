package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
public class Tiponas{

	protected Integer cdtiponas;
	protected String nome;

	@Id
	@DisplayName("Id")
	public Integer getCdtiponas() {
		return cdtiponas;
	}

	@Required
	@MaxLength(20)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	public void setCdtiponas(Integer cdtiponas) {
		this.cdtiponas = cdtiponas;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
