package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoProvisaoCampo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_importacaoprovisaocoluna", sequenceName="sq_importacaoprovisaocoluna")
public class ImportacaoProvisaoColuna implements Log {
	protected Integer cdImportacaoProvisaoColuna;
	protected Integer coluna;
	protected ImportacaoProvisaoCampo tipo;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_importacaoprovisaocoluna")
	@DisplayName("C�digo")
	public Integer getCdImportacaoProvisaoColuna() {
		return cdImportacaoProvisaoColuna;
	}
	
	@Required
	@DisplayName("Coluna")
	public Integer getColuna() {
		return coluna;
	}
	@Required
	@DisplayName("Campo")
	public ImportacaoProvisaoCampo getTipo() {
		return tipo;
	}
	
	public void setCdImportacaoProvisaoColuna(Integer cdImportacaoProvisaoColuna) {
		this.cdImportacaoProvisaoColuna = cdImportacaoProvisaoColuna;
	}
	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}
	public void setTipo(ImportacaoProvisaoCampo tipo) {
		this.tipo = tipo;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
}
