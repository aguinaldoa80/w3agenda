package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Transient;

public class OrdemProducaoSped {
	
	private Integer codigo;
	private Date dtInicio;
	private Date dtConclusao;
	private Integer cdmaterial;
	private Integer situacao;
	private Double quantidadeMaterial;
	protected String identificacao;
	
	public OrdemProducaoSped(Integer codigo, Date dtInicio, Date dtConclusao,
			Integer cdmaterial, String identificacao, Integer situacao, Double quantidadeMaterial) {
		super();
		this.codigo = codigo;
		this.dtInicio = dtInicio;
		this.dtConclusao = dtConclusao;
		this.cdmaterial = cdmaterial;
		this.identificacao = identificacao;
		this.situacao = situacao;
		this.quantidadeMaterial = quantidadeMaterial;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public Date getDtInicio() {
		return dtInicio;
	}
	public Date getDtConclusao() {
		return dtConclusao;
	}
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	public Integer getSituacao() {
		return situacao;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public Double getQuantidadeMaterial() {
		return quantidadeMaterial;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtConclusao(Date dtConclusao) {
		this.dtConclusao = dtConclusao;
	}
	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
	public void setQuantidadeMaterial(Double quantidadeMaterial) {
		this.quantidadeMaterial = quantidadeMaterial;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	
	@Transient
	public String getIdentificacaoOuCdmaterial(){
		String s = "";
		if(this.identificacao != null && !"".equals(this.identificacao)){
			s = this.identificacao;
		}else if(this.cdmaterial != null){
			s = this.cdmaterial.toString();
		}		
		return s;
	}
}