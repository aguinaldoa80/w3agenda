package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;

@Entity
@SequenceGenerator(name = "sq_mdferodoviariocontratante", sequenceName = "sq_mdferodoviariocontratante")
public class MdfeRodoviarioContratante {

	protected Integer cdMdfeRodoviarioContratante;
	protected Mdfe mdfe;
	protected Tipopessoa tipoPessoa = Tipopessoa.PESSOA_JURIDICA;
	protected Cpf cpf;
	protected Cnpj cnpj;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdferodoviariocontratante")
	public Integer getCdMdfeRodoviarioContratante() {
		return cdMdfeRodoviarioContratante;
	}
	public void setCdMdfeRodoviarioContratante(
			Integer cdMdfeRodoviarioContratante) {
		this.cdMdfeRodoviarioContratante = cdMdfeRodoviarioContratante;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}
	
	@DisplayName("Tipo de pessoa")
	public Tipopessoa getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(Tipopessoa tipopessoa) {
		this.tipoPessoa = tipopessoa;
	}
	
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	
	public Cnpj getCnpj() {
		return cnpj;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
}
