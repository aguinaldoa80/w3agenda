package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_valorreferencia", sequenceName = "sq_valorreferencia")
@DisplayName("Valor de refer�ncia")
public class Valorreferencia implements Log, PermissaoProjeto{

	protected Integer cdvalorreferencia;
	protected Planejamento planejamento;
	protected Material material;
	protected Cargo cargo;
	protected Money valor;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
//	TRANSIENTES
	protected Boolean tipo = true;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_valorreferencia")
	public Integer getCdvalorreferencia() {
		return cdvalorreferencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	@Required
	public Planejamento getPlanejamento() {
		return planejamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@DisplayName("Fun��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}

	@Required
	public Money getValor() {
		return valor;
	}
	
	public void setCdvalorreferencia(Integer cdvalorreferencia) {
		this.cdvalorreferencia = cdvalorreferencia;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}

//	LOG
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Valorreferencia) {
			Valorreferencia vr = (Valorreferencia) obj;
			boolean cb,mb;
			Cargo c = vr.getCargo();
			Material m = vr.getMaterial();
			Planejamento p = vr.getPlanejamento();
			
			if (c == null && this.getCargo() == null) {
				cb = true;
			} else if (c != null && this.getCargo() != null) {
				cb = c.equals(this.getCargo());
			} else {
				cb = false;
			}
			
			if (m == null && this.getMaterial() == null) {
				mb = true;
			} else if (m != null && this.getMaterial() != null) {
				mb = m.equals(this.getMaterial());
			} else {
				mb = false;
			}			
			
			return cb && mb && p.equals(this.getPlanejamento());
		}
		return super.equals(obj);
	}
	
	/**
	 * M�todo para pegar da lista o valor de refer�ncia do cargo.
	 *
	 * @param cargo
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Transient
	public static Money getValor(Cargo cargo, List<Valorreferencia> lista){
		if (lista != null && lista.size() > 0) {
			for (Valorreferencia vr : lista) {
				if (cargo.equals(vr.getCargo())) {
					return vr.getValor();
				}
			}
		}
		return null;
	}
	
	/**
	 * M�todo para pegar da lista o valor de refer�ncia do material.
	 *
	 * @param cargo
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	@Transient
	public static Money getValor(Material material, List<Valorreferencia> lista){
		if (lista != null && lista.size() > 0) {
			for (Valorreferencia vr : lista) {
				if (material.equals(vr.getMaterial())) {
					return vr.getValor();
				}
			}
		}
		return null;
	}
	
	@Transient
	public Boolean getTipo() {
		return getMaterial() != null || (getMaterial() == null && getCargo() == null);
	}
	
	public void setTipo(Boolean tipo) {
		this.tipo = tipo;
	}
	
	public String subQueryProjeto() {
		return "select valorreferenciasubQueryProjeto.cdvalorreferencia " +
				"from Valorreferencia valorreferenciasubQueryProjeto " +
				"join valorreferenciasubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
}
