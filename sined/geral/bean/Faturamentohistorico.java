package br.com.linkcom.sined.geral.bean;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Mes;

@Entity
@SequenceGenerator(name = "sq_faturamentohistorico", sequenceName = "sq_faturamentohistorico")
public class Faturamentohistorico {

	protected Integer cdfaturamentohistorico;
	protected Contrato contrato;
	protected Documento documento;
	protected Money valor;
	protected Money descontoincondicionado;
	protected Money descontocondicionado;
	protected Money deducao;
	protected Money outrasretencoes;
	protected Money descontoproduto;
	
	protected Integer numeroparcela;
	protected Integer totalparcelas;
	protected Mes mes;
	protected Integer ano;
	
	protected Set<Faturamentohistoricomaterial> listaFaturamentohistoricomaterial = new ListSet<Faturamentohistoricomaterial>(Faturamentohistoricomaterial.class);

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_faturamentohistorico")
	public Integer getCdfaturamentohistorico() {
		return cdfaturamentohistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	public Money getValor() {
		return valor;
	}

	public Money getDescontoincondicionado() {
		return descontoincondicionado;
	}

	public Money getDescontocondicionado() {
		return descontocondicionado;
	}

	public Money getDeducao() {
		return deducao;
	}

	public Money getOutrasretencoes() {
		return outrasretencoes;
	}

	public Money getDescontoproduto() {
		return descontoproduto;
	}
	
	@OneToMany(mappedBy="faturamentohistorico")
	public Set<Faturamentohistoricomaterial> getListaFaturamentohistoricomaterial() {
		return listaFaturamentohistoricomaterial;
	}
	
	public Integer getNumeroparcela() {
		return numeroparcela;
	}

	public Integer getTotalparcelas() {
		return totalparcelas;
	}
	
	public Mes getMes() {
		return mes;
	}

	public Integer getAno() {
		return ano;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public void setNumeroparcela(Integer numeroparcela) {
		this.numeroparcela = numeroparcela;
	}

	public void setTotalparcelas(Integer totalparcelas) {
		this.totalparcelas = totalparcelas;
	}

	public void setListaFaturamentohistoricomaterial(
			Set<Faturamentohistoricomaterial> listaFaturamentohistoricomaterial) {
		this.listaFaturamentohistoricomaterial = listaFaturamentohistoricomaterial;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}

	public void setDescontoincondicionado(Money descontoincondicionado) {
		this.descontoincondicionado = descontoincondicionado;
	}

	public void setDescontocondicionado(Money descontocondicionado) {
		this.descontocondicionado = descontocondicionado;
	}

	public void setDeducao(Money deducao) {
		this.deducao = deducao;
	}

	public void setOutrasretencoes(Money outrasretencoes) {
		this.outrasretencoes = outrasretencoes;
	}

	public void setDescontoproduto(Money descontoproduto) {
		this.descontoproduto = descontoproduto;
	}

	public void setCdfaturamentohistorico(Integer cdfaturamentohistorico) {
		this.cdfaturamentohistorico = cdfaturamentohistorico;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
}
