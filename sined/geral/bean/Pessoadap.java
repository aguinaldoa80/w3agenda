package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_pessoadap", sequenceName = "sq_pessoadap")
public class Pessoadap {

	protected Integer cdpessoadap;
	protected Pessoa pessoa;
	protected String identificador;
	protected Date dtvencimento;

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pessoadap")
	public Integer getCdpessoadap() {
		return cdpessoadap;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	public Pessoa getPessoa() {
		return pessoa;
	}
	
	@Required
	@MaxLength(30)
	public String getIdentificador() {
		return identificador;
	}

	@Required
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}

	public void setCdpessoadap(Integer cdpessoadap) {
		this.cdpessoadap = cdpessoadap;
	}
	
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
}
