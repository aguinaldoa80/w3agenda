package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_situacaohistorico", sequenceName = "sq_situacaohistorico")
public class Situacaohistorico implements Log{

	protected Integer cdsituacaohistorico;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//CONSTANTES
	public static final Situacaohistorico VENDA = new Situacaohistorico(1, "Venda");
	public static final Situacaohistorico ROTINA = new Situacaohistorico(2, "Rotina");
	public static final Situacaohistorico PERDIDA = new Situacaohistorico(3, "Perdida");
	public static final Situacaohistorico SEM_CONTATO = new Situacaohistorico(4, "Sem contato");
	
	public Situacaohistorico(){
	};
	
	public Situacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}
	
	public Situacaohistorico(Integer cdsituacaohistorico, String descricao) {
		this.cdsituacaohistorico = cdsituacaohistorico;
		this.descricao = descricao;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_situacaohistorico")
	public Integer getCdsituacaohistorico() {
		return cdsituacaohistorico;
	}
	@DisplayName("Descri��o")
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdsituacaohistorico(Integer cdsituacaohistorico) {
		this.cdsituacaohistorico = cdsituacaohistorico;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
