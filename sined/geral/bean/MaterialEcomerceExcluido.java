package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(name="sq_materialecomerceexcluido", sequenceName="sq_materialecomerceexcluido")
public class MaterialEcomerceExcluido {

	private Integer cdMaterialEcomerceExcluido;
	private Material material;
	private Integer idEcommerce;
	
	
	@Id
	@GeneratedValue(generator="sq_materialecomerceexcluido", strategy=GenerationType.AUTO)
	public Integer getCdMaterialEcomerceExcluido() {
		return cdMaterialEcomerceExcluido;
	}
	public void setCdMaterialEcomerceExcluido(Integer cdMaterialEcomerceExcluido) {
		this.cdMaterialEcomerceExcluido = cdMaterialEcomerceExcluido;
	}
	
	@ManyToOne
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public Integer getIdEcommerce() {
		return idEcommerce;
	}
	public void setIdEcommerce(Integer idEcommerce) {
		this.idEcommerce = idEcommerce;
	}
}
