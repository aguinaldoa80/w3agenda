package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@DisplayName("Indicador da origem do processo")
@SequenceGenerator(name = "sq_origemprocessosped", sequenceName = "sq_origemprocessosped")
public class Origemprocessosped {

	protected Integer cdorigemprocessosped;
	protected Integer codigo;
	protected String nome;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_origemprocessosped")
	public Integer getCdorigemprocessosped() {
		return cdorigemprocessosped;
	}
	public Integer getCodigo() {
		return codigo;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdorigemprocessosped(Integer cdorigemprocessosped) {
		this.cdorigemprocessosped = cdorigemprocessosped;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
