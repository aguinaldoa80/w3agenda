package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_valecompra", sequenceName = "sq_valecompra")
public class Valecompra {
	
	protected Integer cdvalecompra;
	protected Cliente cliente;
	protected String identificacao;
	protected Tipooperacao tipooperacao;
	protected Money valor;
	protected Date data;
	
	protected Set<Valecompraorigem> listaValecompraorigem = new ListSet<Valecompraorigem>(Valecompraorigem.class);
	
	public Valecompra() {
	}
	
	public Valecompra(Integer cdvalecompra) {
		this.cdvalecompra = cdvalecompra;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_valecompra")
	public Integer getCdvalecompra() {
		return cdvalecompra;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@Required
	@DisplayName("Identificação")
	public String getIdentificacao() {
		return identificacao;
	}

	@Required
	@DisplayName("Tipo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipooperacao")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}

	@Required
	public Money getValor() {
		return valor;
	}
	
	@OneToMany(mappedBy="valecompra")
	public Set<Valecompraorigem> getListaValecompraorigem() {
		return listaValecompraorigem;
	}
	
	@Required
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public void setListaValecompraorigem(
			Set<Valecompraorigem> listaValecompraorigem) {
		this.listaValecompraorigem = listaValecompraorigem;
	}

	public void setCdvalecompra(Integer cdvalecompra) {
		this.cdvalecompra = cdvalecompra;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
}
