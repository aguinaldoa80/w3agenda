package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_emporiumvenda", sequenceName = "sq_emporiumvenda")
public class Emporiumvenda implements Log{

	protected Integer cdemporiumvenda;
	protected String loja;
	protected String pdv;
	protected String numero_ticket;
	protected Timestamp data_hora;
	protected Integer tipo;
	protected String cliente_id;
	protected String cliente_cpfcnpj;
	protected Double valor;
	protected Double valor_troco;
	protected String vendedor_id;
	protected String operador_id;
	protected Double valor_desconto;
	protected Double valor_acrescimo;
	protected Double valor_juros;
	protected Integer pedidovenda;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Arquivo arquivoXmlNota;
	protected String xml_env;
	protected String xml_ret;
	protected String chv_acs;
	protected String protocolonfe;
	protected Boolean gerar_nota;
	protected Timestamp dtprocessamento;
	
	protected Venda venda;
	
	protected Set<Emporiumvendaarquivo> listaEmporiumvendaarquivo = new ListSet<Emporiumvendaarquivo>(Emporiumvendaarquivo.class);
	protected List<Emporiumvendaitem> listaEmporiumvendaitem = new ListSet<Emporiumvendaitem>(Emporiumvendaitem.class);
	protected List<Emporiumvendafinalizadora> listaEmporiumvendafinalizadora = new ListSet<Emporiumvendafinalizadora>(Emporiumvendafinalizadora.class);
	protected List<Emporiumvendahistorico> listaEmporiumvendahistorico = new ListSet<Emporiumvendahistorico>(Emporiumvendahistorico.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumvenda")
	public Integer getCdemporiumvenda() {
		return cdemporiumvenda;
	}
	@DisplayName("N�mero do ticket")
	public String getNumero_ticket() {
		return numero_ticket;
	}
	@DisplayName("Data/hora")
	public Timestamp getData_hora() {
		return data_hora;
	}
	@DisplayName("Tipo")
	public Integer getTipo() {
		return tipo;
	}
	@DisplayName("Cliente (ID)")
	public String getCliente_id() {
		return cliente_id;
	}
	public Double getValor() {
		return valor;
	}
	@DisplayName("Valor do troco")
	public Double getValor_troco() {
		return valor_troco;
	}
	@DisplayName("Vendedor (ID)")
	public String getVendedor_id() {
		return vendedor_id;
	}
	@DisplayName("Operador (ID)")
	public String getOperador_id() {
		return operador_id;
	}
	@DisplayName("Valor do desconto")
	public Double getValor_desconto() {
		return valor_desconto;
	}
	@DisplayName("Valor do acr�scimo")
	public Double getValor_acrescimo() {
		return valor_acrescimo;
	}
	@DisplayName("Valor do juros")
	public Double getValor_juros() {
		return valor_juros;
	}
	@DisplayName("Finalizadoras")
	@OneToMany(mappedBy="emporiumvenda")
	public List<Emporiumvendafinalizadora> getListaEmporiumvendafinalizadora() {
		return listaEmporiumvendafinalizadora;
	}
	@DisplayName("Itens")
	@OneToMany(mappedBy="emporiumvenda")
	public List<Emporiumvendaitem> getListaEmporiumvendaitem() {
		return listaEmporiumvendaitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvenda")
	public Venda getVenda() {
		return venda;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="emporiumvenda")
	public List<Emporiumvendahistorico> getListaEmporiumvendahistorico() {
		return listaEmporiumvendahistorico;
	}
	public String getLoja() {
		return loja;
	}
	@DisplayName("Pedido")
	public Integer getPedidovenda() {
		return pedidovenda;
	}
	public String getPdv() {
		return pdv;
	}
	@OneToMany(mappedBy="emporiumvenda")
	public Set<Emporiumvendaarquivo> getListaEmporiumvendaarquivo() {
		return listaEmporiumvendaarquivo;
	}
	@DisplayName("Cliente (CPF/CNPJ)")
	public String getCliente_cpfcnpj() {
		return cliente_cpfcnpj;
	}
	public void setCliente_cpfcnpj(String clienteCpfcnpj) {
		cliente_cpfcnpj = clienteCpfcnpj;
	}
	public void setListaEmporiumvendaarquivo(
			Set<Emporiumvendaarquivo> listaEmporiumvendaarquivo) {
		this.listaEmporiumvendaarquivo = listaEmporiumvendaarquivo;
	}
	public void setPdv(String pdv) {
		this.pdv = pdv;
	}
	public void setPedidovenda(Integer pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	public void setListaEmporiumvendahistorico(
			List<Emporiumvendahistorico> listaEmporiumvendahistorico) {
		this.listaEmporiumvendahistorico = listaEmporiumvendahistorico;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public void setListaEmporiumvendafinalizadora(
			List<Emporiumvendafinalizadora> listaEmporiumvendafinalizadora) {
		this.listaEmporiumvendafinalizadora = listaEmporiumvendafinalizadora;
	}
	public void setListaEmporiumvendaitem(
			List<Emporiumvendaitem> listaEmporiumvendaitem) {
		this.listaEmporiumvendaitem = listaEmporiumvendaitem;
	}
	public void setCdemporiumvenda(Integer cdemporiumvenda) {
		this.cdemporiumvenda = cdemporiumvenda;
	}
	public void setNumero_ticket(String numeroTicket) {
		numero_ticket = numeroTicket;
	}
	public void setData_hora(Timestamp dataHora) {
		data_hora = dataHora;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public void setCliente_id(String clienteId) {
		cliente_id = clienteId;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setValor_troco(Double valorTroco) {
		valor_troco = valorTroco;
	}
	public void setVendedor_id(String vendedorId) {
		vendedor_id = vendedorId;
	}
	public void setOperador_id(String operadorId) {
		operador_id = operadorId;
	}
	public void setValor_desconto(Double valorDesconto) {
		valor_desconto = valorDesconto;
	}
	public void setValor_acrescimo(Double valorAcrescimo) {
		valor_acrescimo = valorAcrescimo;
	}
	public void setValor_juros(Double valorJuros) {
		valor_juros = valorJuros;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	@Transient
	public Arquivo getArquivoXmlNota() {
		return arquivoXmlNota;
	}
	public void setArquivoXmlNota(Arquivo arquivoXmlNota) {
		this.arquivoXmlNota = arquivoXmlNota;
	}
	public String getXml_env() {
		return xml_env;
	}
	public void setXml_env(String xml_env) {
		this.xml_env = xml_env;
	}
	public String getXml_ret() {
		return xml_ret;
	}
	public void setXml_ret(String xml_ret) {
		this.xml_ret = xml_ret;
	}
	public String getChv_acs() {
		return chv_acs;
	}
	public void setChv_acs(String chv_acs) {
		this.chv_acs = chv_acs;
	}
	public String getProtocolonfe() {
		return protocolonfe;
	}
	public void setProtocolonfe(String protocolonfe) {
		this.protocolonfe = protocolonfe;
	}
	public Boolean getGerar_nota() {
		return gerar_nota;
	}
	public void setGerar_nota(Boolean gerar_nota) {
		this.gerar_nota = gerar_nota;
	}
	public Timestamp getDtprocessamento() {
		return dtprocessamento;
	}
	public void setDtprocessamento(Timestamp dtprocessamento) {
		this.dtprocessamento = dtprocessamento;
	}
}