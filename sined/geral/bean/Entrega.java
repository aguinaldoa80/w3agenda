package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.Aux_entrega;
import br.com.linkcom.sined.geral.bean.enumeration.RetornoWMSEnum;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;


@Entity
@DisplayName("Recebimento")
@SequenceGenerator(name = "sq_entrega", sequenceName = "sq_entrega")
@JoinEmpresa("entrega.listaEntregadocumento.empresa")
public class Entrega implements Log, PermissaoProjeto, PermissaoFornecedorEmpresa {
	
	protected Integer cdentrega;
	protected Date dtentrega;
	protected Empresa empresa;
	protected Localarmazenagem localarmazenagem;
	protected Aux_entrega aux_entrega;
	protected List<Documentoorigem> listaDocumentoorigem;
	protected RetornoWMSEnum retornowms;
	protected Boolean faturamentocliente;
	protected Arquivo arquivoxml;
	protected Boolean romaneio;
	protected Date dtcancelamento;
	protected Date dtbaixa;
	
	protected List<Entregadocumento> listaEntregadocumento;
	protected Set<Ordemcompraentrega> listaOrdemcompraentrega;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Entregahistorico> listaEntregahistorico = new ListSet<Entregahistorico>(Entregahistorico.class);
	
//	TRANSIENT
	protected String identificadortela;
	protected String controller;
	protected boolean validaQtdeEntrega = true;
	protected boolean fromImportacaoArquivo = false;
	protected List<Solicitacaocompra> listaSolicitacao = new ListSet<Solicitacaocompra>(Solicitacaocompra.class);
	protected Boolean inspecaocompleta;
	protected Ordemcompra ordemcompratrans;
	protected Boolean isOrigemCadastro;
	protected String whereInEntregas;
	protected Money valortotalimposto;
	protected String fornecedorlistagem;
	protected Money valorlistagem;
	protected Boolean irListagemDireto = Boolean.FALSE;
	
	protected String ultimacomprafornecedor;
	protected Date ultimacompradata;
	protected String ultimacompradatastr;
	protected Double ultimacompravalorunitario;
	protected String idsOrdemcompra;
	protected Double precomaximocompra;
	protected List<Pessoaquestionario> listaQuestionario;
	protected Boolean avaliacaoconcluida;
	protected Entregadocumento entregadocumentoTrans;
	
	public Entrega() {
	}
	
	public Entrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entrega")
	public Integer getCdentrega() {
		return cdentrega;
	}
	@Required
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data de recebimento")
	public Date getDtentrega() {
		return dtentrega;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentrega", insertable=false, updatable=false)
	public Aux_entrega getAux_entrega() {
		return aux_entrega;
	}
	@DisplayName("Faturamento cliente")
	public Boolean getFaturamentocliente() {
		return faturamentocliente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	@DisplayName("Romaneio concluido")
	public Boolean getRomaneio() {
		return romaneio;
	}
	@DisplayName("Documentos")
	@OneToMany(fetch=FetchType.LAZY,mappedBy="entrega")
	public List<Entregadocumento> getListaEntregadocumento() {
		return listaEntregadocumento;
	}
	@OneToMany(fetch=FetchType.LAZY,mappedBy="entrega")
	public List<Documentoorigem> getListaDocumentoorigem() {
		return listaDocumentoorigem;
	}
	public Boolean getInspecaocompleta() {
		return inspecaocompleta;
	}	
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	@DisplayName("Data da Baixa")
	public Date getDtbaixa() {
		return dtbaixa;
	}	
	@OneToMany(mappedBy="entrega")
	public Set<Ordemcompraentrega> getListaOrdemcompraentrega() {
		return listaOrdemcompraentrega;
	}
	
	public void setListaOrdemcompraentrega(
			Set<Ordemcompraentrega> listaOrdemcompraentrega) {
		this.listaOrdemcompraentrega = listaOrdemcompraentrega;
	}
	public void setAux_entrega(Aux_entrega aux_entrega) {
		this.aux_entrega = aux_entrega;
	}	
	public void setListaDocumentoorigem(List<Documentoorigem> listaDocumentoorigem) {
		this.listaDocumentoorigem = listaDocumentoorigem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtentrega(Date dtentrega) {
		this.dtentrega = dtentrega;
	}
	public void setCdentrega(Integer cdentrega) {
		this.cdentrega = cdentrega;
	}
	public void setFaturamentocliente(Boolean faturamentocliente) {
		this.faturamentocliente = faturamentocliente;
	}
	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}
	public void setRomaneio(Boolean romaneio) {
		this.romaneio = romaneio;	
	}
	public void setListaEntregadocumento(List<Entregadocumento> listaEntregadocumento) {
		this.listaEntregadocumento = listaEntregadocumento;
	}
	public void setInspecaocompleta(Boolean inspecaocompleta) {
		this.inspecaocompleta = inspecaocompleta;
	}
	public RetornoWMSEnum getRetornowms() {
		return retornowms;
	}
	public void setRetornowms(RetornoWMSEnum retornowms) {
		this.retornowms = retornowms;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setDtbaixa(Date dtbaixa) {
		this.dtbaixa = dtbaixa;
	}

//	LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@OneToMany(mappedBy="entrega")
	@DisplayName("Histórico")
	public List<Entregahistorico> getListaEntregahistorico() {
		return listaEntregahistorico;
	}
	public void setListaEntregahistorico(
			List<Entregahistorico> listaEntregahistorico) {
		this.listaEntregahistorico = listaEntregahistorico;
	}

	//	TRANSIENT
	@Transient
	public String getController() {
		return controller;
	}
	@Transient
	public boolean isFromImportacaoArquivo() {
		return fromImportacaoArquivo;
	}
	@Transient
	public boolean isValidaQtdeEntrega() {
		return validaQtdeEntrega;
	}
	@Transient
	public List<Solicitacaocompra> getListaSolicitacao() {
		return listaSolicitacao;
	}
	@Transient
	public Ordemcompra getOrdemcompratrans() {
		return ordemcompratrans;
	}

	@Transient
	@DisplayName("Valor")
	public Money getValorTotalEntrega(){
		Double valorEntrega = 0.0;
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			Money valor = new Money();
			for(Entregadocumento ed : listaEntregadocumento){
				valor = ed.getValortotaldocumento();
				if(valor != null)
					valorEntrega += valor.getValue().doubleValue();
			}			
		}
		
		return new Money(valorEntrega);
	}
	@Transient
	public Money getValorTotalPagamento(){
		Double valorEntrega = 0.0;
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			Money valor = new Money();
			for(Entregadocumento ed : listaEntregadocumento){
				for(Entregapagamento ep : ed.getListadocumento()){
					valor = ep.getValor();
					if(valor != null)
						valorEntrega += valor.getValue().doubleValue();
				}
			}			
		}
		
		return new Money(valorEntrega);
	}
	@Transient
	public Boolean getIsOrigemCadastro() {
		return isOrigemCadastro;
	}
	@Transient
	public String getWhereInEntregas() {
		return whereInEntregas;
	}
	@Transient
	public Money getValortotalimposto() {
		Money total = new Money();
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				total = total.add(ed.getValortotalimposto());
			}
		}
		return valortotalimposto;
	}	
	@Transient
	@DisplayName("Fornecedor")
	public String getFornecedorlistagem() {
		StringBuilder s = new StringBuilder();
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				if(ed.getFornecedor() != null && ed.getFornecedor().getNome() != null && !"".equals(ed.getFornecedor().getNome())){
					if(!"".equals(s.toString()))
						s.append("<br>");
					s.append(ed.getFornecedor().getNome());
				}
			}
		}
		return s.toString();
	}
	@Transient
	@DisplayName("Valor")
	public Money getValorlistagem() {
		Money valor = new Money();
		if(listaEntregadocumento != null && !listaEntregadocumento.isEmpty()){
			for(Entregadocumento ed : listaEntregadocumento){
				if(ed.getValor() != null){
					valor = valor.add(ed.getValor());
				}
			}
		}
		return valor;
	}	
	@DisplayName("Compra")
	@Transient
	public String getCompraMaterialnumeroserie() {
		StringBuilder s = new StringBuilder();
		if(cdentrega != null){			
			s.append(cdentrega);
		}
		if(dtentrega != null){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(new SimpleDateFormat("dd/MM/yyyy").format(dtentrega));
		}
		return s.length()>0 ? s.toString() : null;
	}

	public void setController(String controller) {
		this.controller = controller;
	}
	public void setValidaQtdeEntrega(boolean validaQtdeEntrega) {
		this.validaQtdeEntrega = validaQtdeEntrega;
	}
	public void setFromImportacaoArquivo(boolean fromImportacaoArquivo) {
		this.fromImportacaoArquivo = fromImportacaoArquivo;
	}	
	public void setListaSolicitacao(List<Solicitacaocompra> listaSolicitacao) {
		this.listaSolicitacao = listaSolicitacao;
	}
	public void setOrdemcompratrans(Ordemcompra ordemcompratrans) {
		this.ordemcompratrans = ordemcompratrans;
	}
	public void setIsOrigemCadastro(Boolean isOrigemCadastro) {
		this.isOrigemCadastro = isOrigemCadastro;
	}
	public void setWhereInEntregas(String whereInEntregas) {
		this.whereInEntregas = whereInEntregas;
	}
	public void setValortotalimposto(Money valortotalimposto) {
		this.valortotalimposto = valortotalimposto;
	}

	public String subQueryProjeto() {
		return "select entregasubQueryProjeto.cdentrega " +
		"from Entrega entregasubQueryProjeto " +
		"join entregasubQueryProjeto.listaEntregadocumento listaEntregadocumentosubQueryProjeto " +
		"join listaEntregadocumentosubQueryProjeto.rateio rateiosubQueryProjeto " +
		"join rateiosubQueryProjeto.listaRateioitem listaRateioitemsubQueryProjeto " +
		"join listaRateioitemsubQueryProjeto.projeto projetosubQueryProjeto " +
		"where entregasubQueryProjeto.cdentrega=entrega.cdentrega and projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}
	
	public String subQueryFornecedorEmpresa() {
		return "select entregaSubQueryFornecedorEmpresa.cdentrega " +
				"from Entrega entregaSubQueryFornecedorEmpresa " +
				"left outer join entregaSubQueryFornecedorEmpresa.listaEntregadocumento listaEntregadocumentoSubQueryFornecedorEmpresa " +
				"left outer join listaEntregadocumentoSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}		

	public boolean useFunctionProjeto() {
		return false;
	}
	
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
	@Transient
	public boolean getHaveOrdemcompra(){
		return this.listaOrdemcompraentrega != null && this.listaOrdemcompraentrega.size() > 0;
	}
	
	@Transient
	public String getUltimacomprafornecedor() {
		return ultimacomprafornecedor;
	}
	@Transient
	public Date getUltimacompradata() {
		return ultimacompradata;
	}
	@Transient
	public Double getUltimacompravalorunitario() {
		return ultimacompravalorunitario;
	}
	@Transient
	public String getUltimacompradatastr() {
		return ultimacompradatastr;
	}
	@Transient
	public Double getPrecomaximocompra() {
		return precomaximocompra;
	}

	public void setUltimacomprafornecedor(String ultimacomprafornecedor) {
		this.ultimacomprafornecedor = ultimacomprafornecedor;
	}
	public void setUltimacompradata(Date ultimacompradata) {
		this.ultimacompradata = ultimacompradata;
	}
	public void setUltimacompravalorunitario(Double ultimacompravalorunitario) {
		this.ultimacompravalorunitario = ultimacompravalorunitario;
	}
	public void setUltimacompradatastr(String ultimacompradatastr) {
		this.ultimacompradatastr = ultimacompradatastr;
	}
	public void setPrecomaximocompra(Double precomaximocompra) {
		this.precomaximocompra = precomaximocompra;
	}
	@Transient
	public String getIdsOrdemcompra() {
		StringBuilder ids = new StringBuilder();
		if(listaOrdemcompraentrega != null && !listaOrdemcompraentrega.isEmpty()){
			for(Ordemcompraentrega ordemcompraentrega : listaOrdemcompraentrega){
				if(ordemcompraentrega.getOrdemcompra() != null && ordemcompraentrega.getOrdemcompra().getCdordemcompra() != null){
					ids.append(!ids.toString().equals("") ? "/" : "").append(ordemcompraentrega.getOrdemcompra().getCdordemcompra());
				}
			}
		}
		return ids.toString();
	}
	public void setIdsOrdemcompra(String idsOrdemcompra) {
		this.idsOrdemcompra = idsOrdemcompra;
	}

	@Transient
	public List<Pessoaquestionario> getListaQuestionario() {
		return listaQuestionario;
	}
	public void setListaQuestionario(List<Pessoaquestionario> listaQuestionario) {
		this.listaQuestionario = listaQuestionario;
	}
	@Transient
	public Boolean getAvaliacaoconcluida() {
		return avaliacaoconcluida;
	}
	public void setAvaliacaoconcluida(Boolean avaliacaoconcluida) {
		this.avaliacaoconcluida = avaliacaoconcluida;
	}
	
	@Transient
	public Entregadocumento getEntregadocumentoTrans() {
		return entregadocumentoTrans;
	}

	public void setEntregadocumentoTrans(Entregadocumento entregadocumentoTrans) {
		this.entregadocumentoTrans = entregadocumentoTrans;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Entrega other = (Entrega) obj;
		if (cdentrega == null) {
			if (other.cdentrega != null)
				return false;
		} else if (!cdentrega.equals(other.cdentrega))
			return false;
		return true;
	}
	
	@Transient
	public String getIdentificadortela() {
		return identificadortela;
	}
	
	public void setIdentificadortela(String identificadortela) {
		this.identificadortela = identificadortela;
	}
	
	@Transient
	public boolean existeEntregadocumentoEntregamaterial(){
		boolean retorno = true;
		try {
			if(SinedUtil.isListEmpty(getListaEntregadocumento())){
				retorno = false;
			}else {
				for(Entregadocumento ed : getListaEntregadocumento()){
					if(SinedUtil.isListEmpty(ed.getListaEntregamaterial())){
						retorno = false;
						break;
					}
				}
			}
		} catch (Exception e) {
		}
		
		return retorno;
	}
}
