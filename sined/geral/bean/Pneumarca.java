package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Marca de Pneu")
@SequenceGenerator(name = "sq_pneumarca", sequenceName = "sq_pneumarca")
public class Pneumarca implements Log {

	protected Integer cdpneumarca;
	protected String nome;
	protected String codigointegracao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Pneumarca(){}
	
	public Pneumarca(Integer cdpneumarca){
		this.cdpneumarca = cdpneumarca;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pneumarca")
	public Integer getCdpneumarca() {
		return cdpneumarca;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(20)
	@DisplayName("C�digo p/ integra��o")
	public String getCodigointegracao() {
		return codigointegracao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdpneumarca(Integer cdpneumarca) {
		this.cdpneumarca = cdpneumarca;
	}

	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}

	public void setCodigointegracao(String codigointegracao) {
		this.codigointegracao = codigointegracao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}