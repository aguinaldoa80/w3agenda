package br.com.linkcom.sined.geral.bean;

public enum SituacaoCusteio {

	SUCESSO		(0,"Processado com sucesso"),
	ERRO		(1,"Processado com ERRO"),
	ESTORNADO	(2,"Cancelado");

	private String nome;
	private Integer valor; 
	
	private SituacaoCusteio(Integer valor, String nome) {
		this.valor = valor;
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return getNome().toUpperCase();
	}
	
}
