package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.GnreConsiderarDataEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreDocumentoOrigemTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnrePeriodoReferenciaEnum;
import br.com.linkcom.sined.geral.bean.enumeration.GnreVersaoEnum;

@Entity
@SequenceGenerator(name="sq_configuracaognreuf", sequenceName="sq_configuracaognreuf")
public class Configuracaognreuf {

	protected Integer cdconfiguracaognreuf;
	protected Configuracaognre configuracaognre;
	protected Uf uf;
	protected GnreVersaoEnum versao;
	protected String codigodetalhamento;
	protected GnreConsiderarDataEnum considerardtvencimento = GnreConsiderarDataEnum.DATA_SAIDA;
	protected Integer diasdtvencimento = 0;
	protected GnreConsiderarDataEnum considerardtpagamento = GnreConsiderarDataEnum.DATA_SAIDA;
	protected Integer diasdtpagamento = 0;
	protected GnrePeriodoReferenciaEnum codigoperiodoreferencia = GnrePeriodoReferenciaEnum.MENSAL;
	protected Boolean chaveacesso;
	protected String codigochaveacesso;
	protected Boolean dtsaida;
	protected String codigodtsaida;
	protected Boolean dtemissao;
	protected String codigodtemissao;
	protected Boolean infocomplementar;
	protected String codigoinfocomplementar;
	protected Boolean documentoorigem;
	protected String codigodocumentoorigem;
	protected GnreDocumentoOrigemTipoEnum documentoorigemtipo;
	protected Boolean destinatario;
	protected Boolean iedestinatario;
	protected Boolean ieremetente;
	protected Boolean infocomplementar2;
	protected String codigoinfocomplementar2;
	protected Integer maximoinfocomplementar;
	protected Integer maximoinfocomplementar2;
	protected Boolean referencia;
	protected Boolean produto;
	protected String convenio;
	
	
	@Id
	@GeneratedValue(generator="sq_configuracaognreuf", strategy=GenerationType.AUTO)
	public Integer getCdconfiguracaognreuf() {
		return cdconfiguracaognreuf;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaognre")
	public Configuracaognre getConfiguracaognre() {
		return configuracaognre;
	}

	@Required
	@DisplayName("UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cduf")
	public Uf getUf() {
		return uf;
	}

	@Required
	@DisplayName("Vers�o")
	public GnreVersaoEnum getVersao() {
		return versao;
	}

	@DisplayName("Chave de acesso")
	public Boolean getChaveacesso() {
		return chaveacesso;
	}

	@DisplayName("C�d. chave de acesso")
	public String getCodigochaveacesso() {
		return codigochaveacesso;
	}

	@DisplayName("Data de sa�da")
	public Boolean getDtsaida() {
		return dtsaida;
	}

	@DisplayName("C�d. data de sa�da")
	public String getCodigodtsaida() {
		return codigodtsaida;
	}

	@DisplayName("Data de emiss�o")
	public Boolean getDtemissao() {
		return dtemissao;
	}

	@DisplayName("C�d. data de emiss�o")
	public String getCodigodtemissao() {
		return codigodtemissao;
	}

	@DisplayName("Info. complementares")
	public Boolean getInfocomplementar() {
		return infocomplementar;
	}

	@DisplayName("C�d. info. complementares")
	public String getCodigoinfocomplementar() {
		return codigoinfocomplementar;
	}
	
	@DisplayName("C�digo de detalhamento de imposto")
	public String getCodigodetalhamento() {
		return codigodetalhamento;
	}

	@Required
	@DisplayName("Considerar para a data de vencimento")
	public GnreConsiderarDataEnum getConsiderardtvencimento() {
		return considerardtvencimento;
	}

	@Required
	@DisplayName("Data de vencimento da guia")
	public Integer getDiasdtvencimento() {
		return diasdtvencimento;
	}
	
	@Required
	@DisplayName("Considerar para a data de pagamento")
	public GnreConsiderarDataEnum getConsiderardtpagamento() {
		return considerardtpagamento;
	}

	@Required
	@DisplayName("Data de pagamento da guia")
	public Integer getDiasdtpagamento() {
		return diasdtpagamento;
	}

	@DisplayName("C�digo do per�odo de refer�ncia")
	public GnrePeriodoReferenciaEnum getCodigoperiodoreferencia() {
		return codigoperiodoreferencia;
	}
	
	@DisplayName("Documento de origem")
	public Boolean getDocumentoorigem() {
		return documentoorigem;
	}

	@DisplayName("C�d. documento de origem")
	public String getCodigodocumentoorigem() {
		return codigodocumentoorigem;
	}

	@DisplayName("Tipo de documento de origem")
	public GnreDocumentoOrigemTipoEnum getDocumentoorigemtipo() {
		return documentoorigemtipo;
	}

	@DisplayName("Destinat�rio")
	public Boolean getDestinatario() {
		return destinatario;
	}

	@DisplayName("IE destinat�rio")
	public Boolean getIedestinatario() {
		return iedestinatario;
	}

	@DisplayName("IE remetente")
	public Boolean getIeremetente() {
		return ieremetente;
	}

	@DisplayName("Info. complementares 2")
	public Boolean getInfocomplementar2() {
		return infocomplementar2;
	}

	@DisplayName("C�d. info. complementares 2")
	public String getCodigoinfocomplementar2() {
		return codigoinfocomplementar2;
	}

	@DisplayName("M�x. info. complementares")
	public Integer getMaximoinfocomplementar() {
		return maximoinfocomplementar;
	}

	@DisplayName("M�x. info. complementares 2")
	public Integer getMaximoinfocomplementar2() {
		return maximoinfocomplementar2;
	}

	@DisplayName("Refer�ncia")
	public Boolean getReferencia() {
		return referencia;
	}

	@DisplayName("Produto")
	public Boolean getProduto() {
		return produto;
	}

	@DisplayName("Conv�nio")
	public String getConvenio() {
		return convenio;
	}

	public void setDocumentoorigem(Boolean documentoorigem) {
		this.documentoorigem = documentoorigem;
	}

	public void setCodigodocumentoorigem(String codigodocumentoorigem) {
		this.codigodocumentoorigem = codigodocumentoorigem;
	}

	public void setDocumentoorigemtipo(
			GnreDocumentoOrigemTipoEnum documentoorigemtipo) {
		this.documentoorigemtipo = documentoorigemtipo;
	}

	public void setDestinatario(Boolean destinatario) {
		this.destinatario = destinatario;
	}

	public void setIedestinatario(Boolean iedestinatario) {
		this.iedestinatario = iedestinatario;
	}

	public void setIeremetente(Boolean ieremetente) {
		this.ieremetente = ieremetente;
	}

	public void setInfocomplementar2(Boolean infocomplementar2) {
		this.infocomplementar2 = infocomplementar2;
	}

	public void setCodigoinfocomplementar2(String codigoinfocomplementar2) {
		this.codigoinfocomplementar2 = codigoinfocomplementar2;
	}

	public void setMaximoinfocomplementar(Integer maximoinfocomplementar) {
		this.maximoinfocomplementar = maximoinfocomplementar;
	}

	public void setMaximoinfocomplementar2(Integer maximoinfocomplementar2) {
		this.maximoinfocomplementar2 = maximoinfocomplementar2;
	}

	public void setReferencia(Boolean referencia) {
		this.referencia = referencia;
	}

	public void setProduto(Boolean produto) {
		this.produto = produto;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public void setCodigodetalhamento(String codigodetalhamento) {
		this.codigodetalhamento = codigodetalhamento;
	}

	public void setConsiderardtvencimento(
			GnreConsiderarDataEnum considerardtvencimento) {
		this.considerardtvencimento = considerardtvencimento;
	}

	public void setDiasdtvencimento(Integer diasdtvencimento) {
		this.diasdtvencimento = diasdtvencimento;
	}

	public void setConsiderardtpagamento(
			GnreConsiderarDataEnum considerardtpagamento) {
		this.considerardtpagamento = considerardtpagamento;
	}

	public void setDiasdtpagamento(Integer diasdtpagamento) {
		this.diasdtpagamento = diasdtpagamento;
	}

	public void setCodigoperiodoreferencia(
			GnrePeriodoReferenciaEnum codigoperiodoreferencia) {
		this.codigoperiodoreferencia = codigoperiodoreferencia;
	}

	public void setChaveacesso(Boolean chaveacesso) {
		this.chaveacesso = chaveacesso;
	}

	public void setCodigochaveacesso(String codigochaveacesso) {
		this.codigochaveacesso = codigochaveacesso;
	}

	public void setDtsaida(Boolean dtsaida) {
		this.dtsaida = dtsaida;
	}

	public void setCodigodtsaida(String codigodtsaida) {
		this.codigodtsaida = codigodtsaida;
	}

	public void setDtemissao(Boolean dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setCodigodtemissao(String codigodtemissao) {
		this.codigodtemissao = codigodtemissao;
	}

	public void setInfocomplementar(Boolean infocomplementar) {
		this.infocomplementar = infocomplementar;
	}

	public void setCodigoinfocomplementar(String codigoinfocomplementar) {
		this.codigoinfocomplementar = codigoinfocomplementar;
	}

	public void setCdconfiguracaognreuf(Integer cdconfiguracaognreuf) {
		this.cdconfiguracaognreuf = cdconfiguracaognreuf;
	}

	public void setConfiguracaognre(Configuracaognre configuracaognre) {
		this.configuracaognre = configuracaognre;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public void setVersao(GnreVersaoEnum versao) {
		this.versao = versao;
	}

	
}
