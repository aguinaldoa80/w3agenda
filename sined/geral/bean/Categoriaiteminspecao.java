package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_categoriaiteminspecao", sequenceName = "sq_categoriaiteminspecao")
public class Categoriaiteminspecao implements Log{

	protected Integer cdcategoriaiteminspecao;
	protected Categoriaveiculo categoriaveiculo;
	protected Inspecaoitem inspecaoitem;
	protected Integer venckm;
	protected Double venchorimetro;
	protected Integer vencperiodo;
	protected Integer margeminspecao;
	protected Double margeminspecaohorimetro;	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer margeminspecaodias;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_categoriaiteminspecao")
	public Integer getCdcategoriaiteminspecao() {
		return cdcategoriaiteminspecao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcategoriaveiculo")
	public Categoriaveiculo getCategoriaveiculo() {
		return categoriaveiculo;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinspecaoitem")
	@DisplayName("Item de inspe��o")
	public Inspecaoitem getInspecaoitem() {
		return inspecaoitem;
	}
	
	@MaxLength(7)
	@DisplayName("Vencimento(KM)")
	public Integer getVenckm() {
		return venckm;
	}
	
	@DisplayName("Vencimento(Horas)")
	public Double getVenchorimetro() {
		return venchorimetro;
	}

	@DisplayName("Vencimento(dias)")
	public Integer getVencperiodo() {
		return vencperiodo;
	}

	@MaxLength(7)
	@DisplayName("Margem de inspe��o(KM)")
	public Integer getMargeminspecao() {
		return margeminspecao;
	}
	
	@DisplayName("Margem de inspe��o(Horas)")
	public Double getMargeminspecaohorimetro() {
		return margeminspecaohorimetro;
	}

	@DisplayName("Margem de inspe��o(Dias)")
	public Integer getMargeminspecaodias() {
		return margeminspecaodias;
	}

	public void setMargeminspecaodias(Integer margeminspecaodias) {
		this.margeminspecaodias = margeminspecaodias;
	}

	public void setCategoriaveiculo(Categoriaveiculo categoriaveiculo) {
		this.categoriaveiculo = categoriaveiculo;
	}

	public void setInspecaoitem(Inspecaoitem inspecaoitem) {
		this.inspecaoitem = inspecaoitem;
	}

	public void setVenckm(Integer venckm) {
		this.venckm = venckm;
	}

	public void setVencperiodo(Integer vencperiodo) {
		this.vencperiodo = vencperiodo;
	}

	public void setMargeminspecao(Integer margeminspecao) {
		this.margeminspecao = margeminspecao;
	}

	public void setCdcategoriaiteminspecao(Integer cdcategoriaiteminspecao) {
		this.cdcategoriaiteminspecao = cdcategoriaiteminspecao;
	}

	public void setVenchorimetro(Double venchorimetro) {
		this.venchorimetro = venchorimetro;
	}

	public void setMargeminspecaohorimetro(Double margeminspecaohorimetro) {
		this.margeminspecaohorimetro = margeminspecaohorimetro;
	}
	
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
