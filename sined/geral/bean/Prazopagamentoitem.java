package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_prazopagamentoitem",sequenceName="sq_prazopagamentoitem")
public class Prazopagamentoitem implements Log {

	protected Integer cdprazopagamentoitem;
	protected Prazopagamento prazopagamento;
	protected Integer parcela;
	protected Integer dias;
	protected Integer meses;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Prazopagamentoitem(){
	}
	
	public Prazopagamentoitem(Integer parcela, Integer dias, Integer meses){
		this.parcela = parcela;
		this.dias = dias;
		this.meses = meses;
	}
	
	@Id
	@GeneratedValue(generator="sq_prazopagamentoitem",strategy=GenerationType.AUTO)
	public Integer getCdprazopagamentoitem() {
		return cdprazopagamentoitem;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprazopagamento")
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	public Integer getParcela() {
		return parcela;
	}
	@MaxLength(5)
	public Integer getDias() {
		return dias;
	}
	@MaxLength(5)
	public Integer getMeses() {
		return meses;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdprazopagamentoitem(Integer cdprazopagamentoitem) {
		this.cdprazopagamentoitem = cdprazopagamentoitem;
	}
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public void setDias(Integer dias) {
		this.dias = dias;
	}
	public void setMeses(Integer meses) {
		this.meses = meses;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

}
