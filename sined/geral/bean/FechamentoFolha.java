package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;

@Entity
@DisplayName("Fechamento Folha")
@SequenceGenerator(name="sq_fechamentofolha", sequenceName="sq_fechamentofolha")
public class FechamentoFolha implements Log{
	
	protected Integer cdFechamentoFolha;
	protected Empresa empresa;
	protected Date dtInicio = SinedDateUtils.firstDateOfMonth(SinedDateUtils.addMesData(SinedDateUtils.currentDate(), -1));
	protected Date dtFim = SinedDateUtils.lastDateOfMonth(SinedDateUtils.addMesData(SinedDateUtils.currentDate(), -1));
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@Required
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_fechamentofolha")
	public Integer getCdFechamentoFolha() {
		return cdFechamentoFolha;
	}
	
	@Required
	@JoinColumn(name="cdempresa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Data Fim")
	public Date getDtFim() {
		return dtFim;
	}
	@Required
	@DisplayName("Data In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	
	public void setCdFechamentoFolha(Integer cdFechamentoFolha) {
		this.cdFechamentoFolha = cdFechamentoFolha;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	//LOG
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
