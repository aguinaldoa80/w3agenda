package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_tarefaorcamentorh",sequenceName="sq_tarefaorcamentorh")
public class Tarefaorcamentorh implements Log{

	protected Integer cdtarefaorcamentorh;
	protected Tarefaorcamento tarefaorcamento;
	protected Cargo cargo;
	protected Double quantidade;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@GeneratedValue(generator="sq_tarefaorcamentorh",strategy=GenerationType.AUTO)
	public Integer getCdtarefaorcamentorh() {
		return cdtarefaorcamentorh;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtarefaorcamento")
	public Tarefaorcamento getTarefaorcamento() {
		return tarefaorcamento;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	public Cargo getCargo() {
		return cargo;
	}
	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdtarefaorcamentorh(Integer cdtarefaorcamentorh) {
		this.cdtarefaorcamentorh = cdtarefaorcamentorh;
	}
	public void setTarefaorcamento(Tarefaorcamento tarefaorcamento) {
		this.tarefaorcamento = tarefaorcamento;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
}
