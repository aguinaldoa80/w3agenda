package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.lkbanco.bank.Bank;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedUtil;


@Entity
@SequenceGenerator(name = "sq_banco", sequenceName = "sq_banco")
public class Banco implements Log,Bank{
	
	public static final String COR_PADRAO_PAINEL_COBRANCA = "000000";
	
	protected Integer cdbanco;
	protected String nome;
	protected Integer numero;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean ativo;
	protected Boolean gerarsispag;
	protected String corPainelCobranca;
	
	protected List<Bancoretorno> listaBancoretorno = new ListSet<Bancoretorno>(Bancoretorno.class);
	protected Set<BancoFormapagamento> listaBancoformapagamento;
	protected Set<BancoTipoPagamento> listaBancotipopagamento;
	protected List<Conta> listaContas;
	
	//Transient
	protected String numeroString;
	
	public Banco(){}
	
	public Banco(Integer cdbanco){
		this.cdbanco = cdbanco;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_banco")
	public Integer getCdbanco() {
		return cdbanco;
	}
	public void setCdbanco(Integer id) {
		this.cdbanco = id;
	}
	
	@DisplayName("Configura��o Retorno")
	@OneToMany(mappedBy="banco")
	public List<Bancoretorno> getListaBancoretorno() {
		return listaBancoretorno;
	}
	
	public void setListaBancoretorno(List<Bancoretorno> listaBancoretorno) {
		this.listaBancoretorno = listaBancoretorno;
	}

	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(3)
	@Required
	@DisplayName("N�mero")
	public Integer getNumero() {
		return numero;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setNumero(Integer numero) {
		this.numero = numero;
		String numeroString1 = convertToString(numero);
		this.numeroString = numeroString1;
	}
	
	/**
	 * Converte um n�mero em string, levando em considera��o se ser�o 1 ou 2 d�gitos
	 * 
	 * @param numero
	 * @return
	 * @author Jo�o Paulo Zica
	 */
	public static String convertToString(Integer numero) {
		String numeroString = null;
		if(numero != null){
			String ns = String.valueOf(numero);
			if(ns.length() < 3){
				if (ns.length()<2) {
					ns = "00"+ns;
				}else{
					ns = "0"+ns; 
				}
			}
			numeroString = ns;
		}
		return numeroString;
	}
	
	@MaxLength(3)
	@DisplayName("N�mero")
	@Transient
	@Required
	public String getNumeroString() {
		return numeroString;
	}
	
	/**
	 * seta o campo numero a partir de uma String passada ao m�todo
	 * 
	 * @param numeroString
	 * @return
	 * @author Jo�o Paulo Zica
	 * @see SinedUtil.validaNumeros(string)
	 */
	public void setNumeroString(String numeroString) {
		this.numeroString = numeroString;
		Integer valor = null;
		if (Util.strings.isNotEmpty(numeroString)) {
			if (SinedUtil.validaNumeros(numeroString)) {
				valor = Integer.parseInt(numeroString);
				this.numero = valor;
			}else
				this.numero = getNumero();
		}		
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
		
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Transient
	public String getName() {
		return getNome();
	}
	@Transient
	public Integer getNumber() {
		return getNumero();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Banco) {
			Banco banco = (Banco) obj;
			return this.getCdbanco().equals(banco.getCdbanco());
		}
		return super.equals(obj);
	}
	
	@DisplayName("FORMA DE PAGAMENTO")
	@OneToMany(mappedBy="banco")
	public Set<BancoFormapagamento> getListaBancoformapagamento() {
		return listaBancoformapagamento;
	}
	public void setListaBancoformapagamento(
			Set<BancoFormapagamento> listaBancoformapagamento) {
		this.listaBancoformapagamento = listaBancoformapagamento;
	}
	
	@DisplayName("TIPO DE PAGAMENTO")
	@OneToMany(mappedBy="banco")
	public Set<BancoTipoPagamento> getListaBancotipopagamento() {
		return listaBancotipopagamento;
	}
	public void setListaBancotipopagamento(
			Set<BancoTipoPagamento> listaBancotipopagamento) {
		this.listaBancotipopagamento = listaBancotipopagamento;
	}
	
	@DisplayName("Gerar SISPAG")
	public Boolean getGerarsispag() {
		return gerarsispag;
	}
	public void setGerarsispag(Boolean gerarsispag) {
		this.gerarsispag = gerarsispag;
	}
	
	@OneToMany(mappedBy="banco")
	public List<Conta> getListaContas() {
		return listaContas;
	}
	public void setListaContas(List<Conta> listaContas) {
		this.listaContas = listaContas;
	}
	
	@DisplayName("Cor (Exibi��o painel de cobran�as)")
	@MaxLength(6)
	public String getCorPainelCobranca() {
		return corPainelCobranca;
	}
	
	public void setCorPainelCobranca(String corPainelCobranca) {
		this.corPainelCobranca = corPainelCobranca;
	}
}