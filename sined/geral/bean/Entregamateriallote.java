package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_entregamateriallote", sequenceName = "sq_entregamateriallote")
public class Entregamateriallote {

	protected Integer cdentregamateriallote;
	protected Entregamaterial entregamaterial;
	protected Loteestoque loteestoque;
	protected Double qtde;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_entregamateriallote")
	public Integer getCdentregamateriallote() {
		return cdentregamateriallote;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdentregamaterial")
	public Entregamaterial getEntregamaterial() {
		return entregamaterial;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdloteestoque")
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}

	@Required
	public Double getQtde() {
		return qtde;
	}
	public void setCdentregamateriallote(Integer cdentregamateriallote) {
		this.cdentregamateriallote = cdentregamateriallote;
	}
	public void setEntregamaterial(Entregamaterial entregamaterial) {
		this.entregamaterial = entregamaterial;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
}