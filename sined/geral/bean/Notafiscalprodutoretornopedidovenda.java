package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_notafiscalprodutoretornopedidovenda", sequenceName = "sq_notafiscalprodutoretornopedidovenda")
public class Notafiscalprodutoretornopedidovenda{

	protected Integer cdnotafiscalprodutoretornopedidovenda;
	protected Pedidovenda pedidovenda;
	protected Notafiscalproduto notafiscalproduto;
	
	public Notafiscalprodutoretornopedidovenda() {}
	
	public Notafiscalprodutoretornopedidovenda(Integer cdpedidovenda, Integer cdnotafiscalproduto) {
		this.pedidovenda = new Pedidovenda(cdpedidovenda);
		this.notafiscalproduto = new Notafiscalproduto(cdnotafiscalproduto);
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notafiscalprodutoretornopedidovenda")
	public Integer getCdnotafiscalprodutoretornopedidovenda() {
		return cdnotafiscalprodutoretornopedidovenda;
	}
	public void setCdnotafiscalprodutoretornopedidovenda(Integer cdnotafiscalprodutoretornopedidovenda) {
		this.cdnotafiscalprodutoretornopedidovenda = cdnotafiscalprodutoretornopedidovenda;
	}	
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovenda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnotafiscalproduto")
	public Notafiscalproduto getNotafiscalproduto() {
		return notafiscalproduto;
	}
	public void setNotafiscalproduto(Notafiscalproduto notafiscalproduto) {
		this.notafiscalproduto = notafiscalproduto;
	}
}