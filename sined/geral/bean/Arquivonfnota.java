package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocontigencia;

@Entity
@SequenceGenerator(name = "sq_arquivonfnota", sequenceName = "sq_arquivonfnota")
public class Arquivonfnota {

	protected Integer cdarquivonfnota;
	protected Arquivonf arquivonf;
	protected Nota nota;
	protected Arquivo arquivoxml;
	
	protected Boolean cancelando;
	protected Arquivo arquivoxmlcancelamento;
	protected Boolean cancelamentoporevento;
	protected Timestamp dtcancelamento;
	protected Arquivo arquivoretornocancelamento;
	protected String motivocancelamento;
	protected Tipocontigencia tipocontigenciacancelamento;

	protected String chaveacesso;
	protected Timestamp dtprocessamento;
	protected String protocolonfe;

	protected String numeronfse;
	protected String codigoverificacao;
	protected Timestamp dtemissao;
	protected Timestamp dtcompetencia;
	
	protected String urlimpressaoparacatu;
	protected Integer numero_filtro;
	
	protected List<Arquivonfnotaerrocancelamento> listaArquivonfnotaerrocancelamento;
	
	protected Boolean consultandonota;
	protected Arquivo arquivoxmlconsulta;
	protected Arquivo arquivoxmlretornoconsulta;
	
	private String qrCode;
	
	// TRANSIENTE
	protected String motivo;
	protected Cliente cliente;
	
	public Arquivonfnota() {
		
	}
	
	public Arquivonfnota(Integer cdarquivonfnota) {
		this.cdarquivonfnota = cdarquivonfnota;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivonfnota")
	public Integer getCdarquivonfnota() {
		return cdarquivonfnota;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivonf")
	public Arquivonf getArquivonf() {
		return arquivonf;
	}
	
	@DisplayName("Nota")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	
	@DisplayName("Chave de acesso (NF-e)")
	public String getChaveacesso() {
		return chaveacesso;
	}

	@DisplayName("Data do processamento (NF-e)")
	public Timestamp getDtprocessamento() {
		return dtprocessamento;
	}
	
	@DisplayName("Protocolo de autoriza��o (NF-e)")
	public String getProtocolonfe() {
		return protocolonfe;
	}

	@DisplayName("N�mero (NFS-e)")
	public String getNumeronfse() {
		return numeronfse;
	}

	@DisplayName("C�digo de verifica��o (NFS-e)")
	public String getCodigoverificacao() {
		return codigoverificacao;
	}

	@DisplayName("Data de emiss�o (NFS-e)")
	public Timestamp getDtemissao() {
		return dtemissao;
	}

	@DisplayName("Data de compet�ncia (NFS-e)")
	public Timestamp getDtcompetencia() {
		return dtcompetencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlcancelamento")
	public Arquivo getArquivoxmlcancelamento() {
		return arquivoxmlcancelamento;
	}
	
	public Boolean getCancelamentoporevento() {
		return cancelamentoporevento;
	}

	@DisplayName("Data de cancelamento")
	public Timestamp getDtcancelamento() {
		return dtcancelamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoretornocancelamento")
	public Arquivo getArquivoretornocancelamento() {
		return arquivoretornocancelamento;
	}
	
	public Boolean getCancelando() {
		return cancelando;
	}
	
	@DisplayName("Motivo do cancelamento")
	public String getMotivocancelamento() {
		return motivocancelamento;
	}
	
	@OneToMany(mappedBy="arquivonfnota")
	public List<Arquivonfnotaerrocancelamento> getListaArquivonfnotaerrocancelamento() {
		return listaArquivonfnotaerrocancelamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxml")
	public Arquivo getArquivoxml() {
		return arquivoxml;
	}
	
	public String getUrlimpressaoparacatu() {
		return urlimpressaoparacatu;
	}
	
	@DisplayName("Consultando")
	public Boolean getConsultandonota() {
		return consultandonota;
	}
	
	public Integer getNumero_filtro() {
		return numero_filtro;
	}
	
	public Tipocontigencia getTipocontigenciacancelamento() {
		return tipocontigenciacancelamento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlconsulta")
	public Arquivo getArquivoxmlconsulta() {
		return arquivoxmlconsulta;
	}
	
	public void setArquivoxmlconsulta(Arquivo arquivoxmlconsulta) {
		this.arquivoxmlconsulta = arquivoxmlconsulta;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivoxmlretornoconsulta")
	public Arquivo getArquivoxmlretornoconsulta() {
		return arquivoxmlretornoconsulta;
	}
	
	public String getQrCode() {
		return qrCode;
	}
	
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	
	public void setArquivoxmlretornoconsulta(Arquivo arquivoxmlretornoconsulta) {
		this.arquivoxmlretornoconsulta = arquivoxmlretornoconsulta;
	}
	
	public void setConsultandonota(Boolean consultandonota) {
		this.consultandonota = consultandonota;
	}
	
	public void setTipocontigenciacancelamento(
			Tipocontigencia tipocontigenciacancelamento) {
		this.tipocontigenciacancelamento = tipocontigenciacancelamento;
	}
	
	public void setNumero_filtro(Integer numeroFiltro) {
		numero_filtro = numeroFiltro;
	}
	
	public void setUrlimpressaoparacatu(String urlimpressaoparacatu) {
		this.urlimpressaoparacatu = urlimpressaoparacatu;
	}
	
	public void setArquivoxml(Arquivo arquivoxml) {
		this.arquivoxml = arquivoxml;
	}
	
	public void setListaArquivonfnotaerrocancelamento(
			List<Arquivonfnotaerrocancelamento> listaArquivonfnotaerrocancelamento) {
		this.listaArquivonfnotaerrocancelamento = listaArquivonfnotaerrocancelamento;
	}
	
	public void setMotivocancelamento(String motivocancelamento) {
		this.motivocancelamento = motivocancelamento;
	}
	
	public void setCancelando(Boolean cancelando) {
		this.cancelando = cancelando;
	}

	public void setArquivoxmlcancelamento(Arquivo arquivoxmlcancelamento) {
		this.arquivoxmlcancelamento = arquivoxmlcancelamento;
	}
	
	public void setCancelamentoporevento(Boolean cancelamentoporevento) {
		this.cancelamentoporevento = cancelamentoporevento;
	}

	public void setDtcancelamento(Timestamp dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}

	public void setArquivoretornocancelamento(Arquivo arquivoretornocancelamento) {
		this.arquivoretornocancelamento = arquivoretornocancelamento;
	}

	public void setCdarquivonfnota(Integer cdarquivonfnota) {
		this.cdarquivonfnota = cdarquivonfnota;
	}

	public void setArquivonf(Arquivonf arquivonf) {
		this.arquivonf = arquivonf;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public void setChaveacesso(String chaveacesso) {
		this.chaveacesso = chaveacesso;
	}

	public void setDtprocessamento(Timestamp dtprocessamento) {
		this.dtprocessamento = dtprocessamento;
	}

	public void setProtocolonfe(String protocolonfe) {
		this.protocolonfe = protocolonfe;
	}

	public void setNumeronfse(String numeronfse) {
		this.numeronfse = numeronfse;
	}

	public void setCodigoverificacao(String codigoverificacao) {
		this.codigoverificacao = codigoverificacao;
	}

	public void setDtemissao(Timestamp dtemissao) {
		this.dtemissao = dtemissao;
	}

	public void setDtcompetencia(Timestamp dtcompetencia) {
		this.dtcompetencia = dtcompetencia;
	}
	
	//TRANSIENTES
	
	@Transient
	public String getErrosCancelamentos(){
		StringBuilder sb = new StringBuilder();
		if(listaArquivonfnotaerrocancelamento != null){
			sb.append("<ul>");
			for (Arquivonfnotaerrocancelamento erro : listaArquivonfnotaerrocancelamento) {
				sb.append("<li>").append(erro.getMensagem()).append("</li>");
			}
			sb.append("</ul>");
		}
		return sb.toString();
	}
	
	@Transient
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdarquivonfnota == null) ? 0 : cdarquivonfnota.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arquivonfnota other = (Arquivonfnota) obj;
		if (cdarquivonfnota == null) {
			if (other.cdarquivonfnota != null)
				return false;
		} else if (!cdarquivonfnota.equals(other.cdarquivonfnota))
			return false;
		return true;
	}
	
}
