package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_diarioobraarquivo", sequenceName = "sq_diarioobraarquivo")
public class Diarioobraarquivo implements Log {

	protected Integer cddiarioobraarquivo;
	protected Arquivo arquivo;
	protected Diarioobra diarioobra;
	protected String descricao;
	protected Date dtarquivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_diarioobraarquivo")
	public Integer getCddiarioobraarquivo() {
		return cddiarioobraarquivo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddiarioobra")
	public Diarioobra getDiarioobra() {
		return diarioobra;
	}
	
	@MaxLength(200)
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Data")
	@Required
	public Date getDtarquivo() {
		return dtarquivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	public void setDtarquivo(Date dtarquivo) {
		this.dtarquivo = dtarquivo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDiarioobra(Diarioobra diarioobra) {
		this.diarioobra = diarioobra;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setCddiarioobraarquivo(Integer cddiarioobraarquivo) {
		this.cddiarioobraarquivo = cddiarioobraarquivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cddiarioobraarquivo == null) ? 0 : cddiarioobraarquivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Diarioobraarquivo other = (Diarioobraarquivo) obj;
		if (cddiarioobraarquivo == null) {
			if (other.cddiarioobraarquivo != null)
				return false;
		} else if (!cddiarioobraarquivo.equals(other.cddiarioobraarquivo))
			return false;
		return true;
	}
	
}
