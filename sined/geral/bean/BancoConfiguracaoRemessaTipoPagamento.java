package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_bancoconfiguracaoremessatipopagamento",sequenceName="sq_bancoconfiguracaoremessatipopagamento")
public class BancoConfiguracaoRemessaTipoPagamento {

	private Integer cdbancoconfiguracaoremessatipopagamento;
	private BancoConfiguracaoRemessa bancoConfiguracaoRemessa;
	private BancoTipoPagamento bancoTipoPagamento;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_bancoconfiguracaoremessatipopagamento")
	public Integer getCdbancoconfiguracaoremessatipopagamento() {
		return cdbancoconfiguracaoremessatipopagamento;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancoconfiguracaoremessa")
	@Required
	public BancoConfiguracaoRemessa getBancoConfiguracaoRemessa() {
		return bancoConfiguracaoRemessa;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbancotipopagamento")
	@Required
	public BancoTipoPagamento getBancoTipoPagamento() {
		return bancoTipoPagamento;
	}

	public void setCdbancoconfiguracaoremessatipopagamento(
			Integer cdbancoconfiguracaoremessatipopagamento) {
		this.cdbancoconfiguracaoremessatipopagamento = cdbancoconfiguracaoremessatipopagamento;
	}

	public void setBancoConfiguracaoRemessa(
			BancoConfiguracaoRemessa bancoConfiguracaoRemessa) {
		this.bancoConfiguracaoRemessa = bancoConfiguracaoRemessa;
	}

	public void setBancoTipoPagamento(BancoTipoPagamento bancoTipoPagamento) {
		this.bancoTipoPagamento = bancoTipoPagamento;
	}
	
}
