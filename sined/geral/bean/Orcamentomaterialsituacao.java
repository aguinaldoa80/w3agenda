package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Orcamentomaterialsituacao {
	
	public static final Orcamentomaterialsituacao EM_ABERTO = new Orcamentomaterialsituacao(0, "EM ABERTO");
	public static final Orcamentomaterialsituacao EM_PROCESSO_DE_COMPRA = new Orcamentomaterialsituacao(1,"EM PROCESSO DE COMPRA");
	public static final Orcamentomaterialsituacao BAIXADA = new Orcamentomaterialsituacao(2,"BAIXADA"); 
	public static final Orcamentomaterialsituacao CANCELADA = new Orcamentomaterialsituacao(3,"CANCELADA"); 
		
	protected Integer cdorcamentomaterialsituacao;
	protected String nome;
		
	public Orcamentomaterialsituacao(){}
	
	public Orcamentomaterialsituacao(Integer cdorcamentomaterialsituacao){
		this.cdorcamentomaterialsituacao = cdorcamentomaterialsituacao;
	}
	
	public Orcamentomaterialsituacao(Integer cdorcamentomaterialsituacao,String nome){
		this.cdorcamentomaterialsituacao = cdorcamentomaterialsituacao;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdorcamentomaterialsituacao() {
		return cdorcamentomaterialsituacao;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setCdorcamentomaterialsituacao(
			Integer cdorcamentomaterialsituacao) {
		this.cdorcamentomaterialsituacao = cdorcamentomaterialsituacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Orcamentomaterialsituacao) {
			Orcamentomaterialsituacao documentoacao = (Orcamentomaterialsituacao) obj;
			return documentoacao.getCdorcamentomaterialsituacao().equals(this.getCdorcamentomaterialsituacao());
		}
		return super.equals(obj);
	}
	
}
