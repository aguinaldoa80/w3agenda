package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_servidor", sequenceName = "sq_servidor")
public class Servidor implements Log{
	
	protected Integer cdservidor;
	protected Ambienterede ambienterede;
	protected Plataforma plataforma;
	protected String nome;
	protected String ip;
	protected Integer porta;
	protected String usuario;
	protected String senha;
	protected Tiponas tiponas;	
	protected Integer portanas;	
	protected String segredonas;
	protected String comunidadenas;
	protected String host;
	protected Boolean briefcase;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;	
	
	protected List<Servicoservidor> listaServicoservidor = new ListSet<Servicoservidor>(Servicoservidor.class);
	protected List<Servidornat> listaServidornat = new ListSet<Servidornat>(Servidornat.class);
	protected List<Servidorinterface> listaServidorinterface = new ListSet<Servidorinterface>(Servidorinterface.class);
	protected List<Servidorfirewall> listaServidorfirewall = new ListSet<Servidorfirewall>(Servidorfirewall.class);
	protected List<Servidorrota> listaServidorrota = new ListSet<Servidorrota>(Servidorrota.class);
		
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidor")
	public Integer getCdservidor() {
		return cdservidor;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdambienterede")
	@DisplayName("Ambiente de rede")
	public Ambienterede getAmbienterede() {
		return ambienterede;
	}
	
	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplataforma")
	@DisplayName("Plataforma")
	public Plataforma getPlataforma() {
		return plataforma;
	}
	
	@Required
	@MaxLength(40)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@MaxLength(25)
	@DisplayName("IP")	
	public String getIp() {
		return ip;
	}
	
	@Required
	public Integer getPorta() {
		return porta;
	}
	
	@Required
	@MaxLength(40)
	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}
	
	@Required
	@MaxLength(40)
	public String getSenha() {
		return senha;
	}
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtiponas")
	@DisplayName("Tipo NAS")
	public Tiponas getTiponas() {
		return tiponas;
	}
	
	@MaxLength(40)
	@DisplayName("Porta NAS")
	public Integer getPortanas() {
		return portanas;
	}

	@MaxLength(40)
	@DisplayName("Segredo NAS")
	public String getSegredonas() {
		return segredonas;
	}	
	
	@MaxLength(40)	
	@DisplayName("Comunidade NAS")
	public String getComunidadenas() {
		return comunidadenas;
	}
	
	@Required
	@MaxLength(80)
	public String getHost() {
		return host;
	}
	
	@DisplayName("BriefCase")
	public Boolean getBriefcase() {
		return briefcase;
	}
	
	@OneToMany(mappedBy="servidor")
	@DisplayName("Servi�o")
	public List<Servicoservidor> getListaServicoservidor() {
		return listaServicoservidor;
	}
	
	@OneToMany(mappedBy="servidor")
	@DisplayName("NAT")
	public List<Servidornat> getListaServidornat() {
		return listaServidornat;
	}
	
	@OneToMany(mappedBy="servidor")
	@DisplayName("Interface")
	public List<Servidorinterface> getListaServidorinterface() {
		return listaServidorinterface;
	}
	
	@OneToMany(mappedBy="servidor")
	@DisplayName("Firewall")
	public List<Servidorfirewall> getListaServidorfirewall() {
		return listaServidorfirewall;
	}
	
	@OneToMany(mappedBy="servidor")
	@DisplayName("Rota")
	public List<Servidorrota> getListaServidorrota() {
		return listaServidorrota;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdservidor(Integer cdservidor) {
		this.cdservidor = cdservidor;
	}

	public void setAmbienterede(Ambienterede ambienterede) {
		this.ambienterede = ambienterede;
	}

	public void setPlataforma(Plataforma plataforma) {
		this.plataforma = plataforma;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setPorta(Integer porta) {
		this.porta = porta;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setTiponas(Tiponas tiponas) {
		this.tiponas = tiponas;
	}

	public void setPortanas(Integer portanas) {
		this.portanas = portanas;
	}

	public void setSegredonas(String segredonas) {
		this.segredonas = segredonas;
	}

	public void setComunidadenas(String comunidadenas) {
		this.comunidadenas = comunidadenas;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public void setBriefcase(Boolean briefcase) {
		this.briefcase = briefcase;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}

	public void setListaServicoservidor(List<Servicoservidor> listaServicoservidor) {
		this.listaServicoservidor = listaServicoservidor;
	}

	public void setListaServidornat(List<Servidornat> listaServidornat) {
		this.listaServidornat = listaServidornat;
	}

	public void setListaServidorinterface(
			List<Servidorinterface> listaServidorinterface) {
		this.listaServidorinterface = listaServidorinterface;
	}

	public void setListaServidorfirewall(
			List<Servidorfirewall> listaServidorfirewall) {
		this.listaServidorfirewall = listaServidorfirewall;
	}

	public void setListaServidorrota(List<Servidorrota> listaServidorrota) {
		this.listaServidorrota = listaServidorrota;
	}
	
}
