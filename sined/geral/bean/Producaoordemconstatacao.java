package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_producaoordemconstatacao", sequenceName = "sq_producaoordemconstatacao")
@DisplayName("Constatações")
public class Producaoordemconstatacao {
	
	private Integer cdproducaoordemconstatacao;
	private Producaoordem producaoordem;
	private Motivodevolucao motivodevolucao;
	private String observacao;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_producaoordemconstatacao")
	public Integer getCdproducaoordemconstatacao() {
		return cdproducaoordemconstatacao;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdproducaoordem")
	public Producaoordem getProducaoordem() {
		return producaoordem;
	}
	
	@Required
	@DisplayName("Constatação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivodevolucao")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	
	@DisplayName("Observação")
	@MaxLength(500)
	public String getObservacao() {
		return observacao;
	}

	public void setCdproducaoordemconstatacao(Integer cdproducaoordemconstatacao) {
		this.cdproducaoordemconstatacao = cdproducaoordemconstatacao;
	}

	public void setProducaoordem(Producaoordem producaoordem) {
		this.producaoordem = producaoordem;
	}

	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
