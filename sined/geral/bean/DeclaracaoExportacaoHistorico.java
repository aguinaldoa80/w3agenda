
package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_declaracaoexportacaohistorico", sequenceName = "sq_declaracaoexportacaohistorico")
public class DeclaracaoExportacaoHistorico implements Log {

	private Integer cddeclaracaoexportacaohistorico;
	private DeclaracaoExportacao declaracaoExportacao;
	private String acao;
	private String observacao;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	/////////////////////////////////////////////////////////////////////|
	//  INICIO DOS GET 											/////////|
	/////////////////////////////////////////////////////////////////////|
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_declaracaoexportacaohistorico")
	public Integer getCddeclaracaoexportacaohistorico() {
		return cddeclaracaoexportacaohistorico;
	}
	@DisplayName("Declaracao")
	@JoinColumn(name = "cddeclaracaoexportacao")
	@ManyToOne(fetch = FetchType.LAZY)
	public DeclaracaoExportacao getDeclaracaoExportacao() {
		return declaracaoExportacao;
	}
	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}
	@DisplayName("Respons�vel")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data de Altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	/////////////////////////////////////////////////////////////////////|
	//  INICIO DOS SET 											/////////|
	/////////////////////////////////////////////////////////////////////|
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCddeclaracaoexportacaohistorico(
			Integer cddeclaracaoexportacaohistorico) {
		this.cddeclaracaoexportacaohistorico = cddeclaracaoexportacaohistorico;
	}
	public void setDeclaracaoExportacao(DeclaracaoExportacao declaracaoExportacao) {
		this.declaracaoExportacao = declaracaoExportacao;
	}
	public void setAcao(String acao) {
		this.acao = acao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	///////////////
	////outros ///
	//////////////
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
