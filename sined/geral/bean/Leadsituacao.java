package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_leadsituacao", sequenceName = "sq_leadsituacao")
public class Leadsituacao implements Log{
	
	protected Integer cdleadsituacao;
	protected String nome;
	protected Boolean defaultListagem;
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_leadsituacao")
	public Integer getCdleadsituacao() {
		return cdleadsituacao;
	}
	
	@Required
	@MaxLength(20)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@DisplayName("Marcado na listagem como padr�o")
	public Boolean getDefaultListagem(){
		return defaultListagem;
	}
	
	public void setDefaultListagem(Boolean defaultListagem) {
		this.defaultListagem = defaultListagem;
	}
	
	public void setCdleadsituacao(Integer cdleadsituacao) {
		this.cdleadsituacao = cdleadsituacao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;		
	}

}
