package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.validator.Length;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.ControleOrcamentoHistoricoEnum;


@SequenceGenerator(name="sq_controleorcamentohistorico", sequenceName = "sq_controleorcamentohistorico")
@DisplayName("Hist�rico")
@Entity
public class ControleOrcamentoHistorico {
	
	protected Integer cdcontroleorcamentohistorico;
	protected Timestamp dataaltera;
	protected ControleOrcamentoHistoricoEnum acaoexecutada;
	protected String responsavel;
	protected Controleorcamento controleorcamento;
	protected String observacao;
	
	//GET
	@Id
	@DisplayName("ID")
	@GeneratedValue(generator="sq_controleorcamentohistorico",strategy=GenerationType.AUTO)
	public Integer getCdcontroleorcamentohistorico() {
		return cdcontroleorcamentohistorico;
	}
	@DisplayName("Data")
	public Timestamp getDataaltera() {
		return dataaltera;
	}
	@DisplayName("A��o")
	public ControleOrcamentoHistoricoEnum getAcaoexecutada() {
		return acaoexecutada;
	}
	@DisplayName("Usu�rio")
	public String getResponsavel() {
		return responsavel;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontroleorcamento")
	public Controleorcamento getControleorcamento() {
		return controleorcamento;
	}
	
	@Length(max = 499)
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	//SET
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdcontroleorcamentohistorico(Integer cdcontroleorcamentohistorico) {
		this.cdcontroleorcamentohistorico = cdcontroleorcamentohistorico;
	}
	public void setDataaltera(Timestamp dataaltera) {
		this.dataaltera = dataaltera;
	}
	public void setAcaoexecutada(ControleOrcamentoHistoricoEnum acaoexecutada) {
		this.acaoexecutada = acaoexecutada;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public void setControleorcamento(Controleorcamento controleorcamento) {
		this.controleorcamento = controleorcamento;
	}
}
