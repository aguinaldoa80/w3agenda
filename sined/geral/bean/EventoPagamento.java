package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Colaboradordespesaitemtipo;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_eventopagamento", sequenceName="sq_eventopagamento")
public class EventoPagamento implements Log{

	private Integer cdEventoPagamento;
	private String nome;
	private Boolean provisionamento;
	private Colaboradordespesaitemtipo tipoEvento;
	private Colaboradordespesaitemtipo colaboradorDespesaItemTipoImportacao;
	private Colaboradordespesaitemtipo colaboradorDespesaItemTipoProcessar;
	private ContaContabil contaContabil;
	private List<EventoPagamentoEmpresa> listaEmpresa;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	private Integer numeroColuna;
	
	
	public EventoPagamento(){
		
	}
	
	public EventoPagamento(Integer cdEventoPagamento){
		this.cdEventoPagamento = cdEventoPagamento;
	}
	
	@Id
	@GeneratedValue(generator="sq_eventopagamento", strategy=GenerationType.AUTO)
	public Integer getCdEventoPagamento() {
		return cdEventoPagamento;
	}
	@Required
	@DisplayName("Evento")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Boolean getProvisionamento() {
		return provisionamento;
	}
	@Required
	@DisplayName("Tipo de opera��o (Cadastro)")
	public Colaboradordespesaitemtipo getTipoEvento() {
		return tipoEvento;
	}
	@DisplayName("Tipo de opera��o (Importa��o)")
	public Colaboradordespesaitemtipo getColaboradorDespesaItemTipoImportacao() {
		return colaboradorDespesaItemTipoImportacao;
	}
	@DisplayName("Tipo de opera��o (Processar)")
	public Colaboradordespesaitemtipo getColaboradorDespesaItemTipoProcessar() {
		return colaboradorDespesaItemTipoProcessar;
	}
	@Required
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="evento")
	public List<EventoPagamentoEmpresa> getListaEmpresa() {
		return listaEmpresa;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("N�mero da coluna")
	public Integer getNumeroColuna() {
		return numeroColuna;
	}
	public void setCdEventoPagamento(Integer cdEventoPagamento) {
		this.cdEventoPagamento = cdEventoPagamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setProvisionamento(Boolean provisionamento) {
		this.provisionamento = provisionamento;
	}
	public void setTipoEvento(Colaboradordespesaitemtipo tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	public void setColaboradorDespesaItemTipoImportacao(Colaboradordespesaitemtipo colaboradorDespesaItemTipoImportacao) {
		this.colaboradorDespesaItemTipoImportacao = colaboradorDespesaItemTipoImportacao;
	}
	public void setColaboradorDespesaItemTipoProcessar(Colaboradordespesaitemtipo colaboradorDespesaItemTipoProcessar) {
		this.colaboradorDespesaItemTipoProcessar = colaboradorDespesaItemTipoProcessar;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
	public void setListaEmpresa(List<EventoPagamentoEmpresa> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setNumeroColuna(Integer numeroColuna) {
		this.numeroColuna = numeroColuna;
	}
	
	
}
