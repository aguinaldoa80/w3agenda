package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_requisicaoprioridade", sequenceName = "sq_requisicaoprioridade")
public class Requisicaoprioridade {

	protected Integer cdrequisicaoprioridade;
	protected String descricao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;

	//constantes
	public static final Integer SEM_PRIORIDADE = 1;
	public static final Integer BAIXA = 2;
	public static final Integer MEDIA = 3;
	public static final Integer ALTA = 4;
	public static final Integer URGENTE = 5;
	

	public Requisicaoprioridade(){}
	
	public Requisicaoprioridade(Integer cdrequisicaoprioridade){
		this.cdrequisicaoprioridade = cdrequisicaoprioridade;
	}

	public Requisicaoprioridade(Integer cdrequisicaoprioridade, String descricao){
		this.cdrequisicaoprioridade = cdrequisicaoprioridade;
		this.descricao = descricao;
	}
	
	@Id
	@Required
	public Integer getCdrequisicaoprioridade() {
		return cdrequisicaoprioridade;
	}
	
	@DescriptionProperty
	@Required
	@MaxLength(20)
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdrequisicaoprioridade(Integer cdrequisicaoprioridade) {
		this.cdrequisicaoprioridade = cdrequisicaoprioridade;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
