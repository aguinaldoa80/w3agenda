package br.com.linkcom.sined.geral.bean;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Arquivo;

@DisplayName("Arquivos para Importação")
public class BancoConfiguracaoSegmentoImportBean {
	
	protected List<Arquivo> listaAquivosImport;

	
	public List<Arquivo> getListaAquivosImport() {
		return listaAquivosImport;
	}

	public void setListaAquivosImport(List<Arquivo> listaAquivosImport) {
		this.listaAquivosImport = listaAquivosImport;
	}
}
