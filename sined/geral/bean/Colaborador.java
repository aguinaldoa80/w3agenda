package br.com.linkcom.sined.geral.bean;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.validation.annotation.ValidationOverride;
import br.com.linkcom.sined.geral.bean.enumeration.Tiposanguineo;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@JoinEmpresa("colaborador.listaColaboradorcargo.empresa")
public class Colaborador extends Pessoa implements Log{
	
	protected Arquivo foto;
	protected Endereco endereco;
	protected String mae;
	protected String pai;
	protected Municipio municipionaturalidade;
	protected Sexo sexo;
	protected Etnia etnia;
	protected Colaboradordeficiencia colaboradordeficiente;
	protected Colaboradortipo colaboradortipo;
	protected String ctps;
	protected Integer seriectps;
	protected Uf ufctps;
	protected Date dtEmissaoCtps;
	protected String rg;
	protected Date dtemissaorg;
	protected String orgaoemissorrg;
	protected String cnh;
	protected Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh;
	protected Uf ufcnh;
	protected Date dtemissaocnh;
	protected Date dtvalidadecnh;
	protected Tipoinscricao tipoinscricao;
	protected String numeroinscricao;
	protected Tipocarteiratrabalho tipocarteiratrabalho;
	protected String carteiratrabalho;
	protected Date dtemissaocarteiratrabalho;
	protected String orgaoemissorcarteiratrabalho;
	protected String tituloeleitoral;
	protected String zonaeleitoral;
	protected String secaoeleitoral;
	protected Tipodocumentomilitar tipodocumentomilitar;
	protected String documentomilitar;
	protected Grauinstrucao grauinstrucao;
	protected Estadocivil estadocivil;
	protected String nomeconjuge;
	protected String perfilprofissional;
	protected Boolean recontratar;
	protected Colaboradorformapagamento colaboradorformapagamento;
	protected Set<Colaboradorcargo> listaColaboradorcargo;
	private Set<Materialcolaborador> listaMaterialColaborador;
	protected Set<Colaboradorarea> listaColaboradorarea;
	protected Set<Colaboradordependente> listaColaboradordependente;
	protected Set<Turmaparticipante> listaTurmaparticipante;
	protected Set<Contratocolaborador> listaContratocolaborador;
//	protected Set<ProjetoColaborador> listaProjetoColaborador;
	protected List<Colaboradorequipamento> listaColaboradorequipamento = new ListSet<Colaboradorequipamento>(Colaboradorequipamento.class);
	protected List<Escalacolaborador> listaEscalacolaborador = new ListSet<Escalacolaborador>(Escalacolaborador.class);
	protected Colaboradorsituacao colaboradorsituacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Dirf dirf;
	protected Date dtadmissao;
	protected Colaboradorstatusfolha colaboradorstatusfolha;
	protected Boolean alterado = Boolean.TRUE;
	protected Tiposanguineo tiposanguineo;
	protected Boolean indiceprevidencia;
	protected Boolean fgtsopcao;
	protected Date fgtsdata;
	protected ContaContabil contaContabil;
	protected String codigoemporium;
	protected Fornecedor colaboradorcontabilista;
	protected String crccontabilista;
	
	protected Boolean brpdh;
	
	protected Set<ColaboradorOcorrencia> listaColaboradorocorrencia = new ListSet<ColaboradorOcorrencia>(ColaboradorOcorrencia.class);
	protected Set<Colaboradoracidente> listaColaboradoracidente = new ListSet<Colaboradoracidente>(Colaboradoracidente.class);
	protected Set<Colaboradorexame> listaColaboradorexame = new ListSet<Colaboradorexame>(Colaboradorexame.class);
	protected Set<Comunicadodestinatario> listaComunicadodestinatario = new ListSet<Comunicadodestinatario>(Comunicadodestinatario.class);
	protected Set<ColaboradorArquivo> listaColaboradorArquivo = new ListSet<ColaboradorArquivo>(ColaboradorArquivo.class);
	protected List<Colaboradorbeneficio> listaColaboradorbeneficio;
	
	//Transiente
	protected Set<Categoriacnh> listaCategoriacnh;
	protected Integer cdcandidato;
	protected String telefones;
	protected String nitformatado;
	protected String brPdhString;
	protected String indiceprevidenciaString;
	protected String fgtsopcaoString;
	protected Image fotoImagem;
	protected Integer categCnhA = 0;
	protected Integer categCnhB = 0;
	protected Integer categCnhC = 0;
	protected Integer categCnhD = 0;
	protected Integer categCnhE = 0;
	protected String agencia;
	protected String conta;
	protected java.sql.Date dtdemissao;
	protected Long matricula;	
	protected boolean possuiUsuario = false;
	protected Integer cdcargo = 0;
	protected String listaExameTipoNatureza;
	protected boolean vendedorprincipalTrans = false;
	protected String apelido;
	protected Boolean apagaVencimento;
	protected Beneficio beneficio;
	protected Boolean criarColaboradorCargo;
	
	// DADOS DO COLABORADORCARGO
	protected Integer cdcolaboradorcargo;
	protected Money salario;
	protected Departamento departamentotransient;
	protected Cargo cargotransient;
	protected Projeto projetotransient;
	protected Sindicato sindicato;
	protected Regimecontratacao regimecontratacao;
	protected Date dtinicio;
	protected Date dtfim;
	protected Empresa empresa;
	protected String observacao;
	protected Boolean inserirHistorico;
	protected String usuarioaltera;
	protected Date vencimentosituacao;
	
	protected Cpf verificaCpf;
	protected Integer cdpessoaaux;
	protected Boolean alterardtinicio;
	protected String validaaux;
	
	protected Cargo cargoColaboradorcargo;
	protected Regimecontratacao regimecontratacaoColaboradorcargo;
	protected Departamento departamentoColaboradorcargo;
	protected Date dtinicioColaboradorcargo;
	protected Date dtfimColaboradorcargo;
	protected String empresaColaboradorcargo;
	protected String observacaoColaboradorcargo;
	protected Money salarioColaboradorcargo;
	
	protected String validaColaboradorcargo;
	
	protected Cbo cboAux;
	
	protected Date dttroca;
	
//	protected Money valorvaletransporte;
//	protected Valetransporte valetransporte;
	
	private Fornecedor fornecedor;
	
	public Colaborador(){}

	public Colaborador(Integer cdpessoa){
		this.cdpessoa = cdpessoa;
	}
	
	public Colaborador(Integer cdpessoa, String nome){
		this.cdpessoa = cdpessoa;
		this.nome = nome;
	}
	
	public Colaborador(Integer cdpessoa, String nome, String email){
		this.cdpessoa = cdpessoa;
		this.nome = nome;
		this.email = email;
	}
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfoto")
	@DisplayName("Foto")
	public Arquivo getFoto() {
		return foto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	@JoinColumn(name="cdendereco")
	@DisplayName("Endere�o")
	@ValidationOverride(field="endereco.municipio",required=@Required)
	public Endereco getEndereco() {
		return endereco;
	}
	
	@Required
	@MaxLength(50)
	@DisplayName("Nome da m�e")
	public String getMae() {
		return mae;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddirf")
	@DisplayName("DIRF")
	public Dirf getDirf() {
		return dirf;
	}

	public void setDirf(Dirf dirf) {
		this.dirf = dirf;
	}
	
	
	@DisplayName("Status Folha")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorstatusfolha")
	public Colaboradorstatusfolha getColaboradorstatusfolha() {
		return colaboradorstatusfolha;
	}
	
	public void setColaboradorstatusfolha(Colaboradorstatusfolha colaboradorstatusfolha) {
		this.colaboradorstatusfolha = colaboradorstatusfolha;
	}

	@MaxLength(50)
	@DisplayName("Nome do Pai")
	public String getPai() {
		return pai;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmunicipionaturalidade")
	@DisplayName("Munic�pio de Naturalidade")
	@ValidationOverride(field="municipionaturalidade.uf")
	public Municipio getMunicipionaturalidade() {
		return municipionaturalidade;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsexo")
	@Required
	public Sexo getSexo() {
		return sexo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdetnia")
	@Required
	@DisplayName("Etnia")
	public Etnia getEtnia() {
		return etnia;
	}
	
	@DisplayName("FGTS Op��o")
	public Boolean getFgtsopcao() {
		return fgtsopcao;
	}
	
	@DisplayName("FGTS Data")
	public Date getFgtsdata() {
		return fgtsdata;
	}
	
	public void setFgtsdata(Date fgtsdata) {
		this.fgtsdata = fgtsdata;
	}
	
	public void setFgtsopcao(Boolean fgtsopcao) {
		this.fgtsopcao = fgtsopcao;
	}
	
	@DisplayName("Defici�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradordeficiencia")
	public Colaboradordeficiencia getColaboradordeficiente() {
		return colaboradordeficiente;
	}
	
	@DisplayName("Tipo de empregado")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradortipo")
	@Required
	public Colaboradortipo getColaboradortipo() {
		return colaboradortipo;
	}
	
	@MaxLength(8)
	@DisplayName("CTPS")
	public String getCtps() {
		return ctps;
	}
	
	@DisplayName("CTPS - UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufctps")
	public Uf getUfctps() {
		return ufctps;
	}
	
	@MaxLength(20)
	@DisplayName("RG")
	@Required
	public String getRg() {
		return rg;
	}
	
	@MaxLength(5)
	@DisplayName("CTPS - S�rie")
	public Integer getSeriectps() {
		return seriectps;
	}

	@DisplayName("CTPS - Data de emiss�o")
	public Date getDtEmissaoCtps() {
		return dtEmissaoCtps;
	}

	public void setSeriectps(Integer seriectps) {
		this.seriectps = seriectps;
	}

	public void setDtEmissaoCtps(Date dtEmissaoCtps) {
		this.dtEmissaoCtps = dtEmissaoCtps;
	}

	@DisplayName("RG - Data de emiss�o")
	@Required
	public java.sql.Date getDtemissaorg() {
		return dtemissaorg;
	}
	
	@MaxLength(50)
	@DisplayName("RG - �rg�o emissor")
	@Required
	public String getOrgaoemissorrg() {
		return orgaoemissorrg;
	}
	
	@MaxLength(15)
	@DisplayName("Cnh")
	public String getCnh() {
		return cnh;
	}
	
	@DisplayName("Cnh - Data de emiss�o")
	public java.sql.Date getDtemissaocnh() {
		return dtemissaocnh;
	}
	
	@MaxLength(14)
	@DisplayName("N�mero de inscri��o")
	public String getNumeroinscricao() {
		return numeroinscricao;
	}
	
	@DisplayName("Tipo de inscri��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipoinscricao")
	public Tipoinscricao getTipoinscricao() {
		return tipoinscricao;
	}
	
	@DisplayName("�ndice previd�ncia")
	public Boolean getIndiceprevidencia() {
		return indiceprevidencia;
	}
	
	public void setIndiceprevidencia(Boolean indiceprevidencia) {
		this.indiceprevidencia = indiceprevidencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipocarteiratrabalho")
	@DisplayName("Classifica��o da carteira profissional")
	public Tipocarteiratrabalho getTipocarteiratrabalho() {
		return tipocarteiratrabalho;
	}
	
	@MaxLength(10)
	@DisplayName("Carteira profissional")
	public String getCarteiratrabalho() {
		return carteiratrabalho;
	}
	
	@DisplayName("carteira profissional - Data de emiss�o")
	public java.sql.Date getDtemissaocarteiratrabalho() {
		return dtemissaocarteiratrabalho;
	}
	
	@DisplayName("carteira profissional - �rg�o emissor")
	@MaxLength(10)
	public String getOrgaoemissorcarteiratrabalho() {
		return orgaoemissorcarteiratrabalho;
	}
	
	@MaxLength(15)
	@DisplayName("T�tulo eleitoral")
	public String getTituloeleitoral() {
		return tituloeleitoral;
	}
	
	@MaxLength(10)
	@DisplayName("Zona")
	public String getZonaeleitoral() {
		return zonaeleitoral;
	}
	
	@MaxLength(10)
	@DisplayName("Se��o")
	public String getSecaoeleitoral() {
		return secaoeleitoral;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtipodocumentomilitar")
	@DisplayName("Categoria documento militar")
	public Tipodocumentomilitar getTipodocumentomilitar() {
		return tipodocumentomilitar;
	}
	
	@MaxLength(20)
	@DisplayName("Documento militar")
	public String getDocumentomilitar() {
		return documentomilitar;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrauinstrucao")
	@Required
	@DisplayName("Escolaridade")
	public Grauinstrucao getGrauinstrucao() {
		return grauinstrucao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdestadocivil")
	@Required
	@DisplayName("Estado civil")
	public Estadocivil getEstadocivil() {
		return estadocivil;
	}
	
	@DisplayName("Nome C�njuge")
	public String getNomeconjuge() {
		return nomeconjuge;
	}
	
	@MaxLength(1000)
	@DisplayName("Observa��o")
	public String getPerfilprofissional() {
		return perfilprofissional;
	}
	
	@DisplayName("Recontratar")
	public Boolean getRecontratar() {
		return recontratar;
	}
	
	@DisplayName("Forma de pagamento")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorformapagamento")
	@Required
	public Colaboradorformapagamento getColaboradorformapagamento() {
		return colaboradorformapagamento;
	}
	@DisplayName("Data de troca do cargo")
	@Transient
	public Date getDttroca() {
		return dttroca;
	}
	
	@DisplayName("Tipo sangu�neo")
	public Tiposanguineo getTiposanguineo() {
		return tiposanguineo;
	}
	
//	@DisplayName("Valor vale-transporte")
//	public Money getValorvaletransporte() {
//		return valorvaletransporte;
//	}
//
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="cdvaletransporte")
//	@DisplayName("Tipo de vale-transporte")
//	public Valetransporte getValetransporte() {
//		return valetransporte;
//	}	
	
	@Transient
	public Integer getCdcargo() {
		return cdcargo;
	}
	
	@Transient
	public String getApelido() {
		return apelido;
	}

	@Transient
	public Boolean getApagaVencimento() {
		return apagaVencimento;
	}

	public void setApagaVencimento(Boolean apagaVencimento) {
		this.apagaVencimento = apagaVencimento;
	}

	public void setCdcargo(Integer cdcargo) {
		this.cdcargo = cdcargo;
	}

//	public void setValorvaletransporte(Money valorvaletransporte) {
//		this.valorvaletransporte = valorvaletransporte;
//	}
//
//		public void setValetransporte(Valetransporte valetransporte) {
//		this.valetransporte = valetransporte;
//	}

	public void setTiposanguineo(Tiposanguineo tiposanguineo) {
		this.tiposanguineo = tiposanguineo;
	}

	public void setDttroca(Date dttroca) {
		this.dttroca = dttroca;
	}
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public void setMae(String mae) {
		this.mae = mae;
	}
	
	public void setPai(String pai) {
		this.pai = pai;
	}
	
	public void setMunicipionaturalidade(Municipio municipionaturalidade) {
		this.municipionaturalidade = municipionaturalidade;
	}
	
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	
	public void setEtnia(Etnia etnia) {
		this.etnia = etnia;
	}
	
	public void setColaboradordeficiente(Colaboradordeficiencia colaboradordeficiente) {
		this.colaboradordeficiente = colaboradordeficiente;
	}
	
	public void setCtps(String ctps) {
		this.ctps = ctps;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public void setDtemissaorg(java.sql.Date dtemissaorg) {
		this.dtemissaorg = dtemissaorg;
	}
	
	public void setOrgaoemissorrg(String orgaoemissorrg) {
		this.orgaoemissorrg = orgaoemissorrg;
	}
	
	public void setCnh(String cnh) {
		this.cnh = cnh;
	}
	
	public void setDtemissaocnh(java.sql.Date dtemissaocnh) {
		this.dtemissaocnh = dtemissaocnh;
	}
	
	public void setNumeroinscricao(String numeroinscricao) {
		this.numeroinscricao = numeroinscricao;
	}
	
	public void setTipocarteiratrabalho(Tipocarteiratrabalho tipocarteiratrabalho) {
		this.tipocarteiratrabalho = tipocarteiratrabalho;
	}
	
	public void setCarteiratrabalho(String carteiraprofissional) {
		this.carteiratrabalho = carteiraprofissional;
	}
	
	public void setDtemissaocarteiratrabalho(java.sql.Date dtemissaocarteiraprofissional) {
		this.dtemissaocarteiratrabalho = dtemissaocarteiraprofissional;
	}

	public void setTituloeleitoral(String tituloeleitoral) {
		this.tituloeleitoral = tituloeleitoral;
	}
	
	public void setZonaeleitoral(String zonaeleitoral) {
		this.zonaeleitoral = zonaeleitoral;
	}
	
	public void setSecaoeleitoral(String secaoeleitoral) {
		this.secaoeleitoral = secaoeleitoral;
	}
	
	public void setTipodocumentomilitar(Tipodocumentomilitar tipodocumentomilitar) {
		this.tipodocumentomilitar = tipodocumentomilitar;
	}
	
	public void setDocumentomilitar(String documentomilitar) {
		this.documentomilitar = documentomilitar;
	}
	
	public void setGrauinstrucao(Grauinstrucao grauinstrucao) {
		this.grauinstrucao = grauinstrucao;
	}
	
	public void setEstadocivil(Estadocivil estadocivil) {
		this.estadocivil = estadocivil;
	}
	
	public void setNomeconjuge(String nomeconjuge) {
		this.nomeconjuge = nomeconjuge;
	}
	
	public void setPerfilprofissional(String perfilprofissional) {
		this.perfilprofissional = perfilprofissional;
	}
	
	public void setRecontratar(Boolean recontratar) {
		this.recontratar = recontratar;
	}
	
	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorsituacao")
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	
	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	
	@OneToMany(mappedBy="colaborador")
	public Set<Colaboradorcategoriacnh> getListaColaboradorcategoriacnh() {
		return listaColaboradorcategoriacnh;
	}
	
	@DisplayName("Escalas")
	@OneToMany(mappedBy="colaborador")
	public List<Escalacolaborador> getListaEscalacolaborador() {
		return listaEscalacolaborador;
	}

	@DisplayName("Escalas")
	@OneToMany(mappedBy="colaborador")
	public void setListaEscalacolaborador(List<Escalacolaborador> listaEscalacolaborador) {
		this.listaEscalacolaborador = listaEscalacolaborador;
	}
	
	@OneToMany(mappedBy="colaborador")
	public Set<Comunicadodestinatario> getListaComunicadodestinatario() {
		return listaComunicadodestinatario;
	}

	public void setListaComunicadodestinatario(
			Set<Comunicadodestinatario> listaComunicadodestinatario) {
		this.listaComunicadodestinatario = listaComunicadodestinatario;
	}

	@DisplayName("Acidentes")
	@OneToMany(mappedBy="colaborador")
	public Set<Colaboradoracidente> getListaColaboradoracidente() {
		return listaColaboradoracidente;
	}
	
	@DisplayName("Exames cl�nicos")
	@OneToMany(mappedBy="colaborador")
	public Set<Colaboradorexame> getListaColaboradorexame() {
		return listaColaboradorexame;
	}
	
	@OneToMany(mappedBy="colaborador")
	@DisplayName("Hist�rico profissional")
	public Set<Colaboradorcargo> getListaColaboradorcargo() {
		return listaColaboradorcargo;
	}
	
	@OneToMany(mappedBy = "colaborador")
	public Set<Materialcolaborador> getListaMaterialColaborador() {
		return listaMaterialColaborador;
	}
	
	@OneToMany(mappedBy="colaborador")
	@DisplayName("�rea")
	public Set<Colaboradorarea> getListaColaboradorarea() {
		return listaColaboradorarea;
	}
	public void setListaColaboradorarea(
			Set<Colaboradorarea> listaColaboradorarea) {
		this.listaColaboradorarea = listaColaboradorarea;
	}

	@OneToMany(mappedBy="colaborador")
	@DisplayName("Dependente")
	public Set<Colaboradordependente> getListaColaboradordependente() {
		return listaColaboradordependente;
	}
	@OneToMany(mappedBy="colaborador")
	@DisplayName("Turma")
	public Set<Turmaparticipante> getListaTurmaparticipante() {
		return listaTurmaparticipante;
	}
	@DisplayName("Contratos")
	@OneToMany(mappedBy="colaborador")
	public Set<Contratocolaborador> getListaContratocolaborador() {
		return listaContratocolaborador;
	}
	@DisplayName("Equipamento")
	@OneToMany(mappedBy="colaborador")
	public List<Colaboradorequipamento> getListaColaboradorequipamento() {
		return listaColaboradorequipamento;
	}
	@DisplayName("BR/PDH")
	public Boolean getBrpdh() {
		return brpdh;
	}
	public void setBrpdh(Boolean brpdh) {
		this.brpdh = brpdh;
	}
	public void setListaColaboradorequipamento(
			List<Colaboradorequipamento> listaColaboradorequipamento) {
		this.listaColaboradorequipamento = listaColaboradorequipamento;
	}
	public void setListaContratocolaborador(
			Set<Contratocolaborador> listaContratocolaborador) {
		this.listaContratocolaborador = listaContratocolaborador;
	}
	public void setListaColaboradorcategoriacnh(Set<Colaboradorcategoriacnh> listaColaboradorcategoriacnh) {
		this.listaColaboradorcategoriacnh = listaColaboradorcategoriacnh;
	}
	
	public void setListaColaboradorcargo(Set<Colaboradorcargo> listaColaboradorcargo) {
		this.listaColaboradorcargo = listaColaboradorcargo;
	}
	
	public void setListaColaboradordependente(Set<Colaboradordependente> listaColaboradordependente) {
		this.listaColaboradordependente = listaColaboradordependente;
	}
	public void setListaTurmaparticipante(Set<Turmaparticipante> listaTurmaparticipante) {
		this.listaTurmaparticipante = listaTurmaparticipante;
	}
	public void setListaColaboradoracidente(
			Set<Colaboradoracidente> listaColaboradoracidente) {
		this.listaColaboradoracidente = listaColaboradoracidente;
	}
	public void setListaColaboradorexame(
			Set<Colaboradorexame> listaColaboradorexame) {
		this.listaColaboradorexame = listaColaboradorexame;
	}
	@Transient
	public Set<Categoriacnh> getListaCategoriacnh() {
		return listaCategoriacnh;
	}
	@Transient
	@Required
	@DisplayName("Cargo")
	public Cargo getCargotransient() {
		return cargotransient;
	}
	@Transient
	public Sindicato getSindicato() {
		return sindicato;
	}
	@Transient
	@DisplayName("Sal�rio")
	public Money getSalario() {
		return salario;
	}
	
	public void setSalario(Money salario) {
		this.salario = salario;
	}
	
	@Transient
	@Required
	@DisplayName("Regime de contrata��o")
	public Regimecontratacao getRegimecontratacao() {
		return regimecontratacao;
	}
	
	@Transient
	@Required
	@DisplayName("Departamento")
	public Departamento getDepartamentotransient() {
		return departamentotransient;
	}
	
	@Transient
	@Required
	@DisplayName("Data de admiss�o")
	public Date getDtinicio() {
		return dtinicio;
	}
	@Transient
	@DisplayName("Data final")
	public Date getDtfim() {
		return dtfim;
	}
		
	@Transient
	@DisplayName("Digite o CPF a ser cadastrado")
	public Cpf getVerificaCpf() {
		return verificaCpf;
	}
	
	@Transient
	@DisplayName("CD pessoa")
	public Integer getCdpessoaaux() {
		return cdpessoaaux;
	}
	
	@Transient
	@DisplayName("ALTERAR DATA ADMISS�O")
	public Boolean getAlterardtinicio() {
		return alterardtinicio;
	}
	
	@Transient
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Transient
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	@Transient
	@DisplayName("Inserir no hist�rico?")
	public Boolean getInserirHistorico() {
		return inserirHistorico;
	}
	@Transient
	public Integer getCdcolaboradorcargo() {
		return cdcolaboradorcargo;
	}
	
	@DisplayName("C�digo de Integra��o ECF")
	public String getCodigoemporium() {
		return codigoemporium;
	}
	
	public void setCodigoemporium(String codigoemporium) {
		this.codigoemporium = codigoemporium;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setListaCategoriacnh(Set<Categoriacnh> listaCategoriacnh) {
		this.listaCategoriacnh = listaCategoriacnh;
	}
	
	public void setCargotransient(Cargo cargo) {
		this.cargotransient = cargo;
	}
	public void setSindicato(Sindicato sindicato) {
		this.sindicato = sindicato;
	}
	public void setRegimecontratacao(Regimecontratacao regimecontratacao) {
		this.regimecontratacao = regimecontratacao;
	}
	
	public void setDepartamentotransient(Departamento departamento) {
		this.departamentotransient = departamento;
	}
	
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	@DisplayName("Cnh - UF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdufcnh")
	public Uf getUfcnh() {
		return ufcnh;
	}

	@DisplayName("Cnh - Data de validade")
	public Date getDtvalidadecnh() {
		return dtvalidadecnh;
	}


	public void setUfcnh(Uf ufcnh) {
		this.ufcnh = ufcnh;
	}

	public void setDtvalidadecnh(Date dtvalidadecnh) {
		this.dtvalidadecnh = dtvalidadecnh;
	}

	public void setVerificaCpf(Cpf verificaCpf) {
		this.verificaCpf = verificaCpf;
	}
	
	public void setCdpessoaaux(Integer cdpessoaaux) {
		this.cdpessoaaux = cdpessoaaux;
	}
	
	public void setAlterardtinicio(Boolean alterardtinicio) {
		this.alterardtinicio = alterardtinicio;
	}

	public void setTipoinscricao(Tipoinscricao tipoinscricao) {
		this.tipoinscricao = tipoinscricao;
	}

	
	public void setOrgaoemissorcarteiratrabalho(String orgaoemissorcarteiratrabalho) {
		this.orgaoemissorcarteiratrabalho = orgaoemissorcarteiratrabalho;
	}

	
	public void setColaboradortipo(Colaboradortipo colaboradortipo) {
		this.colaboradortipo = colaboradortipo;
	}

	public void setUfctps(Uf ufctps) {
		this.ufctps = ufctps;
	}

	public void setColaboradorformapagamento(Colaboradorformapagamento colaboradorformapagamento) {
		this.colaboradorformapagamento = colaboradorformapagamento;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	
	public void setInserirHistorico(Boolean inserirHistorico) {
		this.inserirHistorico = inserirHistorico;
	}
	public void setCdcolaboradorcargo(Integer cdcolaboradorcargo) {
		this.cdcolaboradorcargo = cdcolaboradorcargo;
	}
	
	@DisplayName("Data de admiss�o")
	@Required
	public Date getDtadmissao() {
		return dtadmissao;
	}
	public Boolean getAlterado() {
		return alterado;
	}
	public void setDtadmissao(Date dtadmissao) {
		this.dtadmissao = dtadmissao;
	}
	public void setAlterado(Boolean alterado) {
		this.alterado = alterado;
	}
	
	@Transient
	@DisplayName("Valida��o auxiliar")
	public String getValidaaux() {
		return validaaux;
	}
	
	public void setValidaaux(String validaaux) {
		this.validaaux = validaaux;
	}

	@Transient
	@DisplayName("Cargo")
	public Cargo getCargoColaboradorcargo() {
		return cargoColaboradorcargo;
	}

	@Transient
	@DisplayName("Regime de Contrata��o")
	public Regimecontratacao getRegimecontratacaoColaboradorcargo() {
		return regimecontratacaoColaboradorcargo;
	}

	@Transient
	@DisplayName("Departamento")
	public Departamento getDepartamentoColaboradorcargo() {
		return departamentoColaboradorcargo;
	}

	@Transient
	@DisplayName("Data inicial")
	public Date getDtinicioColaboradorcargo() {
		return dtinicioColaboradorcargo;
	}

	@Transient
	@DisplayName("Data de encerramento")
	public Date getDtfimColaboradorcargo() {
		return dtfimColaboradorcargo;
	}

	@Transient
	@DisplayName("Empresa")
	public String getEmpresaColaboradorcargo() {
		return empresaColaboradorcargo;
	}

	@Transient
	@DisplayName("Outras informa��es")
	public String getObservacaoColaboradorcargo() {
		return observacaoColaboradorcargo;
	}

	@Transient
	@DisplayName("Sal�rio")
	public Money getSalarioColaboradorcargo() {
		return salarioColaboradorcargo;
	}

	public void setCargoColaboradorcargo(Cargo cargoColaboradorcargo) {
		this.cargoColaboradorcargo = cargoColaboradorcargo;
	}

	public void setRegimecontratacaoColaboradorcargo(
			Regimecontratacao regimecontratacaoColaboradorcargo) {
		this.regimecontratacaoColaboradorcargo = regimecontratacaoColaboradorcargo;
	}

	public void setDepartamentoColaboradorcargo(
			Departamento departamentoColaboradorcargo) {
		this.departamentoColaboradorcargo = departamentoColaboradorcargo;
	}

	public void setDtinicioColaboradorcargo(Date dtinicioColaboradorcargo) {
		this.dtinicioColaboradorcargo = dtinicioColaboradorcargo;
	}

	public void setDtfimColaboradorcargo(Date dtfimColaboradorcargo) {
		this.dtfimColaboradorcargo = dtfimColaboradorcargo;
	}

	public void setEmpresaColaboradorcargo(String empresaColaboradorcargo) {
		this.empresaColaboradorcargo = empresaColaboradorcargo;
	}

	public void setObservacaoColaboradorcargo(String observacaoColaboradorcargo) {
		this.observacaoColaboradorcargo = observacaoColaboradorcargo;
	}

	public void setSalarioColaboradorcargo(Money salarioColaboradorcargo) {
		this.salarioColaboradorcargo = salarioColaboradorcargo;
	}
	
	@Transient
	public String getValidaColaboradorcargo() {
		return validaColaboradorcargo;
	}
	
	public void setValidaColaboradorcargo(String validaColaboradorcargo) {
		this.validaColaboradorcargo = validaColaboradorcargo;
	}
	
	@Transient
	@DisplayName("Cbo auxiliar")
	public Cbo getCboAux() {
		return cboAux;
	}

	public void setCboAux(Cbo cboAux) {
		this.cboAux = cboAux;
	}
	
	@Transient
	public String getUsuarioaltera() {
		return usuarioaltera;
	}
	
	public void setUsuarioaltera(String usuarioaltera) {
		this.usuarioaltera = usuarioaltera;
	}
	
	@Transient
	public Integer getCdcandidato() {
		return cdcandidato;
	}
	
	public void setCdcandidato(Integer cdcandidato) {
		this.cdcandidato = cdcandidato;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Colaborador) {
			Colaborador c = (Colaborador) obj;
			return c.getCdpessoa().equals(this.getCdpessoa());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if(cdpessoa != null)
			return cdpessoa.hashCode();
		return 0;
	}
	
	@Transient
	public String getTelefoneColaborador(){
		Set<Telefone> listaTel = getListaTelefone();
		if(listaTel != null){
			for (Telefone telefone : listaTel) {
				if(telefone != null && telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.PRINCIPAL)){
					return telefone.getTelefone();
				}
			}
		}
		return null;
	} 
	
	@Transient
	public String getCelularColaborador(){
		Set<Telefone> listaTel = getListaTelefone();
		if(listaTel != null){
			for (Telefone telefone : listaTel) {
				if(telefone != null && telefone.getTelefonetipo() != null && telefone.getTelefonetipo().getCdtelefonetipo() != null && telefone.getTelefonetipo().getCdtelefonetipo().equals(Telefonetipo.CELULAR)){
					return telefone.getTelefone();
				}
			}
		}
		return null;
	}
	
	@Transient
	public String getNitformatado() {
		if(this.numeroinscricao != null && this.numeroinscricao.length() == 11)
			return this.numeroinscricao.substring(0, 3)+"."+this.numeroinscricao.substring(3, 8)+"."+this.numeroinscricao.substring(8, 10)+"-"+this.numeroinscricao.substring(10, 11);
		else 
			return this.numeroinscricao;
	}
	
	@Transient
	public String getTelefones() {
		return telefones;
	}
	
	@Transient
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	@Transient
	public String getBrPdhString() {
		return brPdhString;
	}
	public void setBrPdhString(String brPdhString) {
		this.brPdhString = brPdhString;
	}
	
	@Transient
	public String getIndiceprevidenciaString() {
		return indiceprevidenciaString;
	}
	public void setIndiceprevidenciaString(String indiceprevidenciaString) {
		this.indiceprevidenciaString = indiceprevidenciaString;
	}
	
	@Transient
	public String getFgtsopcaoString() {
		return fgtsopcaoString;
	}
	public void setFgtsopcaoString(String fgtsopcaoString) {
		this.fgtsopcaoString = fgtsopcaoString;
	}
	@Transient
	public Image getFotoImagem() {
		return fotoImagem;
	}
	public void setFotoImagem(Image fotoImagem) {
		this.fotoImagem = fotoImagem;
	}
	@Transient
	public Integer getCategCnhA() {
		return categCnhA;
	}
	
	@Transient
	public Integer getCategCnhB() {
		return categCnhB;
	}
	@Transient
	public Integer getCategCnhC() {
		return categCnhC;
	}
	@Transient
	public Integer getCategCnhD() {
		return categCnhD;
	}
	@Transient
	public Integer getCategCnhE() {
		return categCnhE;
	}

	public void setCategCnhA(Integer categCnhA) {
		this.categCnhA = categCnhA;
	}

	public void setCategCnhB(Integer categCnhB) {
		this.categCnhB = categCnhB;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	
	public void setCategCnhC(Integer categCnhC) {
		this.categCnhC = categCnhC;
	}

	public void setCategCnhD(Integer categCnhD) {
		this.categCnhD = categCnhD;
	}

	public void setCategCnhE(Integer categCnhE) {
		this.categCnhE = categCnhE;
	}
	@Transient
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	@Transient
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	@Transient
	@Required
	@DisplayName("Data de demiss�o")
	public java.sql.Date getDtdemissao() {
		return dtdemissao;
	}
	public void setDtdemissao(java.sql.Date dtdemissao) {
		this.dtdemissao = dtdemissao;
	}
	
	@Transient
	public Long getMatricula() {
		return matricula;
	}
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}
	
	@Transient
	public Escala getEscala(){
		List<Escalacolaborador> lista = getListaEscalacolaborador();
		if(lista != null && lista.size() > 0){
			Escalacolaborador maior = null;
			for (Escalacolaborador ec : lista) {
				if(maior == null)
					maior = ec;
				else if(maior.getCdescalacolaborador() < ec.getCdescalacolaborador())
					maior = ec;
			}
			return maior.getEscala();
		} else
			return null;
	}
	
	@Transient
	public String getDescricaoAutocomplete(){
		return this.nome;
	}

	@Transient	
	public String getNomeCpfColaborador() {
		StringBuilder s = new StringBuilder();
		if(this.getNome() != null){
			s.append(this.getNome());
		}
		if(this.getCpf() != null){
			s.append(" - ").append(this.getCpf());
		}
		return s.toString();
	}

	@Transient
	public boolean isPossuiUsuario() {
		return possuiUsuario;
	}

	public void setPossuiUsuario(boolean possuiUsuario) {
		this.possuiUsuario = possuiUsuario;
	}
	
	@DisplayName("TIPO (NATUREZA)")
	@Transient
	public String getListaExameTipoNatureza() {
		return this.listaExameTipoNatureza;
	}
	
	public void setListaExameTipoNatureza(){
		if(this.listaColaboradorexame == null || this.listaColaboradorexame.isEmpty()){
			this.listaExameTipoNatureza = "";
		}
		StringBuilder sb = new StringBuilder();
		int newLine = 0;
		for(Colaboradorexame colaboradorexame : this.listaColaboradorexame){
			if(colaboradorexame.getDtproximoexame() != null 
					&& colaboradorexame.getDtproximoexame().before(SinedDateUtils.addDiasData(SinedDateUtils.currentDate(), 8))){
				
				if(newLine < 2){
					sb.append(colaboradorexame.getExametipo().getNome()+" ("+colaboradorexame.getExamenatureza().getNome()+"), ");
					newLine++;
				}
				else{
					sb.append(colaboradorexame.getExametipo().getNome()+" ("+colaboradorexame.getExamenatureza().getNome()+"),<BR>");
					newLine = 0;
				}
			}
		}
		if(sb.length() > 0){
			int indexUltimaVirgula = sb.lastIndexOf(",");
			sb.setCharAt(indexUltimaVirgula, '.');
		}
		
		this.listaExameTipoNatureza = sb.toString();
	}
	
	public void setListaExameTipoNatureza(String listaExameTipoNatureza) {
		this.listaExameTipoNatureza = listaExameTipoNatureza;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	@DisplayName("Conta cont�bil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}

	@Transient
	public Projeto getProjetotransient() {
		return projetotransient;
	}
	public void setProjetotransient(Projeto projetotransient) {
		this.projetotransient = projetotransient;
	}

	@Transient
	public Cargo getCargoAtualTrans() {
		Cargo cargo = null;
		if(this.getListaColaboradorcargo() != null && !this.getListaColaboradorcargo().isEmpty()){
			for(Colaboradorcargo colaboradorcargo : this.listaColaboradorcargo){
				if(colaboradorcargo.getDtfim() == null){
					cargo = colaboradorcargo.getCargo();
					break;
				}
			}
		}
		return cargo;
	}
	
	@Transient
	public boolean isVendedorprincipalTrans() {
		return vendedorprincipalTrans;
	}
	public void setVendedorprincipalTrans(boolean vendedorprincipalTrans) {
		this.vendedorprincipalTrans = vendedorprincipalTrans;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	@OneToMany(fetch=FetchType.LAZY, mappedBy="colaborador")
	@DisplayName("Ocorr�ncias")
	public Set<ColaboradorOcorrencia> getListaColaboradorocorrencia() {
		return listaColaboradorocorrencia;
	}

	public void setListaColaboradorocorrencia(
			Set<ColaboradorOcorrencia> listaColaboradorocorrencia) {
		this.listaColaboradorocorrencia = listaColaboradorocorrencia;
	}

	@DisplayName("Arquivos")
	@OneToMany(mappedBy="colaborador")
	public Set<ColaboradorArquivo> getListaColaboradorArquivo() {
		return listaColaboradorArquivo;
	}

	public void setListaColaboradorArquivo(Set<ColaboradorArquivo> listaColaboradorArquivo) {
		this.listaColaboradorArquivo = listaColaboradorArquivo;
	}
	
	@Transient
	public Date getVencimentosituacao() {
		return vencimentosituacao;
	}
	
	public void setVencimentosituacao(Date vencimentosituacao) {
		this.vencimentosituacao = vencimentosituacao;
	}
	
	@DisplayName("Benef�cios")
	@OneToMany(mappedBy="colaborador")
	public List<Colaboradorbeneficio> getListaColaboradorbeneficio() {
		return listaColaboradorbeneficio;
	}
	
	public void setListaColaboradorbeneficio(List<Colaboradorbeneficio> listaColaboradorbeneficio) {
		this.listaColaboradorbeneficio = listaColaboradorbeneficio;
	}
	
	@Transient
	public Beneficio getBeneficio() {
		return beneficio;
	}
	
	public void setBeneficio(Beneficio beneficio) {
		this.beneficio = beneficio;
	}
	
	@Transient
	public Boolean getCriarColaboradorCargo() {
		return criarColaboradorCargo;
	}

	public void setCriarColaboradorCargo(Boolean criarColaboradorCargo) {
		this.criarColaboradorCargo = criarColaboradorCargo;
	}

	@Transient
	public Colaboradorcargo getColaboradorCargoAtualTrans() {
		if(this.getListaColaboradorcargo() != null && !this.getListaColaboradorcargo().isEmpty()){
			for(Colaboradorcargo colaboradorcargo : this.listaColaboradorcargo){
				if(colaboradorcargo.getDtfim() == null){
					return colaboradorcargo;
				}
			}
		}
		return null;
	}

	public void setListaMaterialColaborador(Set<Materialcolaborador> listaMaterialColaborador) {
		this.listaMaterialColaborador = listaMaterialColaborador;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcontabilista")
	@DisplayName("Contabilista")
	public Fornecedor getColaboradorcontabilista() {
		return colaboradorcontabilista;
	}
	
	public void setColaboradorcontabilista(Fornecedor colaboradorcontabilista) {
		this.colaboradorcontabilista = colaboradorcontabilista;
	}
	
	@DisplayName("CRC")
	@MaxLength(15)
	public String getCrccontabilista() {
		return crccontabilista;
	}
	
	public void setCrccontabilista(String crccontabilista) {
		this.crccontabilista = crccontabilista;
	}
	
}