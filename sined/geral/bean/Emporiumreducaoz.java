package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_emporiumreducaoz", sequenceName = "sq_emporiumreducaoz")
public class Emporiumreducaoz implements Log {
	
	protected Integer cdemporiumreducaoz;
	protected Date data;
	protected Integer cro;
	protected Integer crz;
	protected Integer numerocoofinal;
	protected Double gtfinal;
	protected Double valorcancelados;
	protected Double valordesconto;
	protected Double valoracrescimo;
	protected Double valorvendabruta;
	protected Emporiumpdv emporiumpdv;
	
	// TRANSIENTE
	protected Integer crzUltimo;
	protected Double gtfinalUltimo;
	protected Date dataUltimo;
	protected Double totalizadores;
	protected Double valorreducao;
	protected Double valorvendaw3;
	protected String whereInDataEntre;
	protected Double diferencatotalizadores;
	protected Double diferencavendaw3;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_emporiumreducaoz")
	public Integer getCdemporiumreducaoz() {
		return cdemporiumreducaoz;
	}
	
	public Date getData() {
		return data;
	}

	public Integer getCro() {
		return cro;
	}

	public Integer getCrz() {
		return crz;
	}

	@DisplayName("N�mero COO Final")
	public Integer getNumerocoofinal() {
		return numerocoofinal;
	}

	@DisplayName("GT Final")
	public Double getGtfinal() {
		return gtfinal;
	}

	@DisplayName("Valor de venda bruta")
	public Double getValorvendabruta() {
		return valorvendabruta;
	}

	@DisplayName("PDV")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdemporiumpdv")
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}
	
	@DisplayName("Valor de cancelados")
	public Double getValorcancelados() {
		return valorcancelados;
	}

	@DisplayName("Valor de desconto")
	public Double getValordesconto() {
		return valordesconto;
	}

	@DisplayName("Valor de acr�scimo")
	public Double getValoracrescimo() {
		return valoracrescimo;
	}

	public void setValorcancelados(Double valorcancelados) {
		this.valorcancelados = valorcancelados;
	}

	public void setValordesconto(Double valordesconto) {
		this.valordesconto = valordesconto;
	}

	public void setValoracrescimo(Double valoracrescimo) {
		this.valoracrescimo = valoracrescimo;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setCro(Integer cro) {
		this.cro = cro;
	}

	public void setCrz(Integer crz) {
		this.crz = crz;
	}

	public void setNumerocoofinal(Integer numerocoofinal) {
		this.numerocoofinal = numerocoofinal;
	}

	public void setGtfinal(Double gtfinal) {
		this.gtfinal = gtfinal;
	}

	public void setValorvendabruta(Double valorvendabruta) {
		this.valorvendabruta = valorvendabruta;
	}

	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}

	public void setCdemporiumreducaoz(Integer cdemporiumreducaoz) {
		this.cdemporiumreducaoz = cdemporiumreducaoz;
	}

	@Override
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}

	@Override
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
	}

	@Transient
	public Integer getCrzUltimo() {
		return crzUltimo;
	}

	@Transient
	public Double getGtfinalUltimo() {
		return gtfinalUltimo;
	}
	
	@Transient
	public Date getDataUltimo() {
		return dataUltimo;
	}
	
	@Transient
	public Double getTotalizadores() {
		return totalizadores;
	}
	
	@Transient
	public Double getValorreducao() {
		return valorreducao;
	}
	
	@Transient
	public String getWhereInDataEntre() {
		return whereInDataEntre;
	}
	
	@Transient
	public Double getValorvendaw3() {
		return valorvendaw3;
	}
	
	@Transient
	public Double getDiferencatotalizadores() {
		return diferencatotalizadores;
	}

	@Transient
	public Double getDiferencavendaw3() {
		return diferencavendaw3;
	}

	public void setDiferencatotalizadores(Double diferencatotalizadores) {
		this.diferencatotalizadores = diferencatotalizadores;
	}

	public void setDiferencavendaw3(Double diferencavendaw3) {
		this.diferencavendaw3 = diferencavendaw3;
	}

	public void setValorvendaw3(Double valorvendaw3) {
		this.valorvendaw3 = valorvendaw3;
	}
	
	public void setWhereInDataEntre(String whereInDataEntre) {
		this.whereInDataEntre = whereInDataEntre;
	}
	
	public void setValorreducao(Double valorreducao) {
		this.valorreducao = valorreducao;
	}
	
	public void setTotalizadores(Double totalizadores) {
		this.totalizadores = totalizadores;
	}
	
	public void setDataUltimo(Date dataUltimo) {
		this.dataUltimo = dataUltimo;
	}

	public void setCrzUltimo(Integer crzUltimo) {
		this.crzUltimo = crzUltimo;
	}

	public void setGtfinalUltimo(Double gtfinalUltimo) {
		this.gtfinalUltimo = gtfinalUltimo;
	}
	
	

}
