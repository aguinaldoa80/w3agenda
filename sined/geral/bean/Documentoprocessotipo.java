package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_documentoprocessotipo", sequenceName = "sq_documentoprocessotipo")
@DisplayName("Tipo de Documento/Processo")
public class Documentoprocessotipo implements Log{
	
	//Tabela documentoprocessotipo
	protected Integer cddocumentoprocessotipo;
	protected String nome;
	protected Boolean ativo= true;
	//Log
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@Id
	@GeneratedValue(generator="sq_documentoprocessotipo",strategy=GenerationType.AUTO)
	public Integer getCddocumentoprocessotipo() {
		return cddocumentoprocessotipo;
	}
	
	@Required
	@MaxLength(50)	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	public void setCddocumentoprocessotipo(Integer cddocumentoprocessotipo) {
		this.cddocumentoprocessotipo = cddocumentoprocessotipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

}
