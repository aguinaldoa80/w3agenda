package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_vendasincronizacaoflex", sequenceName = "sq_vendasincronizacaoflex")
@Table(name="vendasincronizacaoflex")
public class VendaSincronizacaoFlex {
	
	protected Integer cdvendasincronizacaoflex;
	protected Venda venda;
	protected Date dtsincronizacao;
	protected String mensagem;
	protected String nomeProcesso;
	protected String numeroInscricao; 
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendasincronizacaoflex")
	public Integer getCdvendasincronizacaoflex() {
		return cdvendasincronizacaoflex;
	}

	@DisplayName("Venda")
	@JoinColumn(name="cdvenda")
	@ManyToOne(fetch=FetchType.LAZY)
	@Required
	public Venda getVenda() {
		return venda;
	}

	public String getNomeProcesso() {
		return nomeProcesso;
	}

	public String getNumeroInscricao() {
		return numeroInscricao;
	}

	public void setCdvendasincronizacaoflex(Integer cdvendasincronizacaoflex) {
		this.cdvendasincronizacaoflex = cdvendasincronizacaoflex;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public void setNomeProcesso(String nomeProcesso) {
		this.nomeProcesso = nomeProcesso;
	}

	public void setNumeroInscricao(String numeroInscricao) {
		this.numeroInscricao = numeroInscricao;
	}

	public Date getDtsincronizacao() {
		return dtsincronizacao;
	}
	public void setDtsincronizacao(Date dtsincronizacao) {
		this.dtsincronizacao = dtsincronizacao;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdvendasincronizacaoflex == null) ? 0
						: cdvendasincronizacaoflex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final VendaSincronizacaoFlex other = (VendaSincronizacaoFlex) obj;
		if (cdvendasincronizacaoflex == null) {
			if (other.cdvendasincronizacaoflex != null)
				return false;
		} else if (!cdvendasincronizacaoflex
				.equals(other.cdvendasincronizacaoflex))
			return false;
		return true;
	}
}
