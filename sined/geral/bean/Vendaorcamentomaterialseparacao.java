package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_vendaorcamentomaterialseparacao", sequenceName = "sq_vendaorcamentomaterialseparacao")
public class Vendaorcamentomaterialseparacao {

	protected Integer cdvendaorcamentomaterialseparacao;
	protected Vendaorcamentomaterial vendaorcamentomaterial;
	protected Unidademedida unidademedida;
	protected Double quantidade;
	
	//Transientes
	protected Double fracao;
	protected Double qtdereferencia;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendaorcamentomaterialseparacao")
	public Integer getCdvendaorcamentomaterialseparacao() {
		return cdvendaorcamentomaterialseparacao;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamentomaterial")
	public Vendaorcamentomaterial getVendaorcamentomaterial() {
		return vendaorcamentomaterial;
	}
	
	public Double getQuantidade() {
		return quantidade;
	}

	public void setCdvendaorcamentomaterialseparacao(
			Integer cdvendaorcamentomaterialseparacao) {
		this.cdvendaorcamentomaterialseparacao = cdvendaorcamentomaterialseparacao;
	}

	public void setVendaorcamentomaterial(
			Vendaorcamentomaterial vendaorcamentomaterial) {
		this.vendaorcamentomaterial = vendaorcamentomaterial;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
//Transientes
	
	@Transient
	public Double getFracao() {
		return fracao;
	}

	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}

	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}

	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	
}
