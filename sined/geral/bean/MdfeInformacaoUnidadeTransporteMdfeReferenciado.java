package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.MdfeTipoTransporte;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_mdfeinformacaounidadetransportemdfereferenciado", sequenceName = "sq_mdfeinformacaounidadetransportemdfereferenciado")
public class MdfeInformacaoUnidadeTransporteMdfeReferenciado {

	protected Integer cdMdfeInformacaoUnidadeTransporteMdfeReferenciado;
	protected Mdfe mdfe;
	protected String idMdfeReferenciado;
	protected String idUnidadeTransporte;
	protected MdfeTipoTransporte tipoTransporte;
	protected String identificacaoTransporte;
	protected Money quantidadeRateada;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_mdfeinformacaounidadetransportemdfereferenciado")
	public Integer getCdMdfeInformacaoUnidadeTransporteMdfeReferenciado() {
		return cdMdfeInformacaoUnidadeTransporteMdfeReferenciado;
	}
	public void setCdMdfeInformacaoUnidadeTransporteMdfeReferenciado(
			Integer cdMdfeInformacaoUnidadeTransporte) {
		this.cdMdfeInformacaoUnidadeTransporteMdfeReferenciado = cdMdfeInformacaoUnidadeTransporte;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmdfe")
	public Mdfe getMdfe() {
		return mdfe;
	}
	public void setMdfe(Mdfe mdfe) {
		this.mdfe = mdfe;
	}

	@DisplayName("ID do MDFe referenciado")
	public String getIdMdfeReferenciado() {
		return idMdfeReferenciado;
	}
	public void setIdMdfeReferenciado(String idMdfeReferenciado) {
		this.idMdfeReferenciado = idMdfeReferenciado;
	}
	
	@DisplayName("ID da unidade de transporte")
	public String getIdUnidadeTransporte() {
		return idUnidadeTransporte;
	}
	public void setIdUnidadeTransporte(String idUnidadeTransporte) {
		this.idUnidadeTransporte = idUnidadeTransporte;
	}
	
	@Required
	@DisplayName("Tipo de transporte")
	public MdfeTipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}
	public void setTipoTransporte(MdfeTipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	@Required
	@MaxLength(value=20)
	@DisplayName("Identificação do transporte")
	public String getIdentificacaoTransporte() {
		return identificacaoTransporte;
	}
	public void setIdentificacaoTransporte(String identificacaoTransporte) {
		this.identificacaoTransporte = identificacaoTransporte;
	}

	@DisplayName("Quantidade rateada")
	public Money getQuantidadeRateada() {
		return quantidadeRateada;
	}
	public void setQuantidadeRateada(Money quantidadeRateada) {
		this.quantidadeRateada = quantidadeRateada;
	}
	
	@Transient
	public List<MdfeLacreUnidadeTransporteMdfeReferenciado> getListaLacreUnidadeTransporteMdfeReferenciado(MdfeInformacaoUnidadeTransporteMdfeReferenciado infUnTransMdfeReferenciado, Mdfe mdfe) {
		List<MdfeLacreUnidadeTransporteMdfeReferenciado> list = new ArrayList<MdfeLacreUnidadeTransporteMdfeReferenciado>();
		if(SinedUtil.isListNotEmpty(mdfe.getListaLacreUnidadeTransporteMdfeReferenciado())){
			for(MdfeLacreUnidadeTransporteMdfeReferenciado item : mdfe.getListaLacreUnidadeTransporteMdfeReferenciado()){
				if(item.getIdUnidadeTransporte().equals(infUnTransMdfeReferenciado.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
	
	@Transient
	public List<MdfeInformacaoUnidadeCargaMdfeReferenciado> getListaInformacaoUnidadeCargaMdfeReferenciado(MdfeInformacaoUnidadeTransporteMdfeReferenciado infUnTransMdfeReferenciado, Mdfe mdfe) {
		List<MdfeInformacaoUnidadeCargaMdfeReferenciado> list = new ArrayList<MdfeInformacaoUnidadeCargaMdfeReferenciado>(); 
		if(SinedUtil.isListNotEmpty(mdfe.getListaInformacaoUnidadeCargaMdfeReferenciado())){
			for(MdfeInformacaoUnidadeCargaMdfeReferenciado item : mdfe.getListaInformacaoUnidadeCargaMdfeReferenciado()){
				if(item.getIdUnidadeTransporte().equals(infUnTransMdfeReferenciado.getIdUnidadeTransporte())){
					list.add(item);
				}
			}
		}
		return list;
	}
}
