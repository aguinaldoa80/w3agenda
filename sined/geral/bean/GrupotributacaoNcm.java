package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_grupotributacaoncm",sequenceName="sq_grupotributacaoncm")
public class GrupotributacaoNcm {

	private Integer cdgrupotributacaoNcm;
	private Grupotributacao grupotributacao;
	private String ncm;

	public GrupotributacaoNcm() {}
	
	public GrupotributacaoNcm(Grupotributacao grupotributacao, String ncm) {
		this.grupotributacao = grupotributacao;
		this.ncm = ncm;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_grupotributacaoncm")
	public Integer getCdgrupotributacaoNcm() {
		return cdgrupotributacaoNcm;
	}

	public void setCdgrupotributacaoNcm(Integer cdgrupotributacaoNcm) {
		this.cdgrupotributacaoNcm = cdgrupotributacaoNcm;
	}
	
	@DisplayName("Grupo tributação")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}

	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}

	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}
}
