package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;


@Entity
public class Radiushistorico implements Log{
	
	protected Integer cdradiushistorico;
	protected String usuario;
	protected String framedipaddress;
	protected String nascodigoporta;
	protected String estacaocliente;
	protected String estacaoservidor;
	protected Timestamp dtinicio;
	protected Timestamp dtfim;
	protected Integer temposessao;
	protected Long dadoentrada;
	protected Long dadosaida;
	protected String nasip;
	
	//Transient
	protected String duracao;
	protected Double dadoentradaMb;
	protected Double dadosaidaMb;

	@Id	
	public Integer getCdradiushistorico() {
		return cdradiushistorico;
	}
	
	@DisplayName("Cliente")
	public String getUsuario() {
		return usuario;
	}
	@DisplayName("Endere�o Ip")
	public String getFramedipaddress() {
		return framedipaddress;
	}
	@DisplayName("Porta")
	public String getNascodigoporta() {
		return nascodigoporta;
	}
	@DisplayName("Mac")
	public String getEstacaocliente() {
		return estacaocliente;
	}
	@DisplayName("In�cio")
	public Timestamp getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Fim")
	public Timestamp getDtfim() {
		return dtfim;
	}
	@DisplayName("Dura��o")
	public Integer getTemposessao() {
		return temposessao;
	}
	@DisplayName("Entrada")
	public Long getDadoentrada() {
		return dadoentrada;
	}
	@DisplayName("Sa�da")
	public Long getDadosaida() {
		return dadosaida;
	}
	@DisplayName("Ip da torre")
	public String getNasip() {
		return nasip;
	}
	@DisplayName("Servidor")
	public String getEstacaoservidor() {
		return estacaoservidor;
	}
	public void setDadoentrada(Long dadoentrada) {
		this.dadoentrada = dadoentrada;
	}
	public void setDadosaida(Long dadosaida) {
		this.dadosaida = dadosaida;
	}
	public void setCdradiushistorico(Integer cdradiushistorico) {
		this.cdradiushistorico = cdradiushistorico;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setFramedipaddress(String framedipaddress) {
		this.framedipaddress = framedipaddress;
	}
	public void setNascodigoporta(String nascodigoporta) {
		this.nascodigoporta = nascodigoporta;
	}
	public void setEstacaocliente(String estacaocliente) {
		this.estacaocliente = estacaocliente;
	}
	public void setDtinicio(Timestamp dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Timestamp dtfim) {
		this.dtfim = dtfim;
	}
	public void setTemposessao(Integer temposessao) {
		this.temposessao = temposessao;
	}
	public void setNasip(String nasip) {
		this.nasip = nasip;
	}
	public void setEstacaoservidor(String estacaoservidor) {
		this.estacaoservidor = estacaoservidor;
	}
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}
	public void setDtaltera(Timestamp dtaltera) {
	}
	
	@Transient
	@DisplayName("Dura��o (h:m:s)")
	public String getDuracao() {		
		
		Date hoje = new java.util.Date();
		
		long segundos = (hoje.getTime() - dtinicio.getTime())/1000;
		if (segundos < 0 )  
			return "";
		int hora = (int)(segundos / 3600);
		segundos -= hora * 3600;
		int minutos = (int)(segundos / 60);
		segundos -= minutos * 60;
		
		return hora + ":" + (minutos < 10 ? "0" : "") + minutos + ":" + (segundos < 10 ? "0" : "") + segundos;
	};
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	@Transient
	@DisplayName("Upload")
	public Double getDadoentradaMb() {
		return dadoentradaMb;
	}
	@Transient
	@DisplayName("Download")
	public Double getDadosaidaMb() {
		return dadosaidaMb;
	}

	public void setDadoentradaMb(Double dadoentradaMb) {
		this.dadoentradaMb = dadoentradaMb;
	}

	public void setDadosaidaMb(Double dadosaidaMb) {
		this.dadosaidaMb = dadosaidaMb;
	}
	
	
}
