package br.com.linkcom.sined.geral.bean;

import java.awt.Image;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.LazyInitializationException;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.ExibirMatrizAtributosEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.FaturamentoVeterinaria;
import br.com.linkcom.sined.geral.bean.enumeration.FreteGratisEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacaonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Produtodiferenciado;
import br.com.linkcom.sined.geral.bean.enumeration.Retencao;
import br.com.linkcom.sined.geral.bean.enumeration.StatusUsoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoitemsped;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterial;
import br.com.linkcom.sined.geral.bean.view.Vgerenciarmaterialsoma;
import br.com.linkcom.sined.modulo.producao.controller.crud.bean.Materiaprima;
import br.com.linkcom.sined.modulo.suprimentos.controller.crud.bean.enumeration.FormaTributacaoEnum;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoFornecedorEmpresa;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@SequenceGenerator(name = "sq_material", sequenceName = "sq_material")
@DisplayName("Material")
@JoinEmpresa("material.listaMaterialempresa.empresa")
public class Material implements Log, Cloneable, PermissaoFornecedorEmpresa {
	
	
//	COLUNA 1
	protected Integer cdmaterial;
	protected String nome;
	protected String nomenf;
	protected String codigofabricante;
	protected Unidademedida unidademedida;
	protected Boolean considerarvendamultiplos;
	protected Centrocusto centrocustovenda;
	protected ContaContabil contaContabil;
	protected Contagerencial contagerencial;
	protected Contagerencial contagerencialvenda;
	protected Materialgrupo materialgrupo;
	protected Materialtipo materialtipo;
	protected Materialcategoria materialcategoria;
	protected Materialcor materialcor;
	protected Localizacaoestoque localizacaoestoque;
	protected Double valorcusto;
	protected Money valorcustomateriaprima;
	protected Money frete;
	protected Money valorfrete;
	protected Double valorvenda;
	protected Boolean naoexibirminmaxvenda;
	protected Double valorvendaminimo;
	protected Double valorvendamaximo;
	protected Integer qtdeunidade;
	protected Boolean vendapromocional;
	protected Boolean kit;
	protected Boolean kitflexivel;
	protected Boolean exibiritenskitvenda;
	protected Boolean exibiritenskitflexivelvenda;
	protected Boolean producao;
	protected Producaoetapa producaoetapa;
	protected Double qtdereferencia;
	protected Material materialcoleta;
	protected String identificacao;
	protected String identificacaoordenacao;
	protected String referencia;
	protected Material materialmestregrade;
	protected Boolean freteseparadonota;
	protected Boolean ativo = Boolean.TRUE;
	protected Double peso;
	protected Double pesobruto;
	protected Codigoanp codigoanp;
	protected Boolean vendaecf;
	protected Double percentualimpostoecf;
	protected Boolean metrocubicovalorvenda;
	protected Boolean pesoliquidovalorvenda;
	protected Double valorindenizacao;
	protected Date dtcustoinicial;
	protected Double qtdecustoinicial;
	protected Double valorcustoinicial;
	protected Produtodiferenciado produtodiferenciado;
	protected Boolean tributacaoipi;
	protected Integer prazogarantia;
	protected Boolean definirqtdeminimaporlocal;
	protected Boolean tributacaomunicipal;
	protected Pneumedida pneumedida;
	protected Double profundidadesulco;
	protected Boolean itemGradePadraoMaterialMestre;
	protected String idVinculoExterno;
	
//	CADASTRAR DADOS
	protected Boolean cadastrarCaracteristica;
	protected Boolean cadastrarItemInspecao;
	protected Boolean cadastrarColaboradoresPermissao;
	protected Boolean cadastrarConversaoUnidades;
	protected Boolean cadastrarMateriaisSimilares;
	protected Boolean cadastrarSugestaoVenda;
	protected Boolean cadastrarNumeroSerie;
	
//	COLUNA 2
	protected Boolean radcaracteristica;
	protected Boolean radimposto;
	protected Boolean radfornecedor;
	protected Integer tempoentrega;
	protected Double metaprazomediocompra;
	protected Double toleranciaprazocompra;
	protected Boolean radempresa;
	protected Boolean radinspecao;
	protected Boolean radcolaborador;
	protected Empresamodelocontrato empresamodelocontrato;
	protected Boolean materialcte;
	protected Boolean obrigarlote;
	protected Material materialacompanhabanda;
	protected Boolean cadastrarFaixaMarkup;
	protected Boolean ecommerce;
	protected Boolean embalagem;
	protected Boolean marketplace;
	protected Boolean somenteParceiros;
	protected String urlVideo;
	
	protected Set<Materialrelacionado> listaMaterialrelacionado = new ListSet<Materialrelacionado>(Materialrelacionado.class);
	protected Set<Materialkitflexivel> listaMaterialkitflexivel = new ListSet<Materialkitflexivel>(Materialkitflexivel.class);
	protected Set<Materialsimilar> listaMaterialsimilar = new ListSet<Materialsimilar>(Materialsimilar.class);
	protected Set<Materialcaracteristica> listaCaracteristica = new ListSet<Materialcaracteristica>(Materialcaracteristica.class);
	protected Set<Materialfornecedor> listaFornecedor = new ListSet<Materialfornecedor>(Materialfornecedor.class);
	protected Set<Materialproducao> listaProducao = new ListSet<Materialproducao>(Materialproducao.class);
	protected Set<Materialempresa> listaMaterialempresa = new ListSet<Materialempresa>(Materialempresa.class);
	protected Set<Materialinspecaoitem> listaMaterialinspecaoitem = new ListSet<Materialinspecaoitem>(Materialinspecaoitem.class);
	protected Set<Contratomaterial> listaContratomaterial = new ListSet<Contratomaterial>(Contratomaterial.class);
	protected Set<Materialunidademedida> listaMaterialunidademedida = new ListSet<Materialunidademedida>(Materialunidademedida.class);
	protected Set<Materialnumeroserie> listaMaterialnumeroserie = new ListSet<Materialnumeroserie>(Materialnumeroserie.class); 
	protected Set<Materialvenda> listaMaterialvenda = new ListSet<Materialvenda>(Materialvenda.class);
	protected Set<Materialcolaborador> listaMaterialcolaborador = new ListSet<Materialcolaborador>(Materialcolaborador.class);
	protected Set<MaterialCustoEmpresa> listaMaterialCustoEmpresa = new ListSet<MaterialCustoEmpresa>(MaterialCustoEmpresa.class);
	protected List<Materialhistorico> listaMaterialhistorico = new ListSet<Materialhistorico>(Materialhistorico.class);
	protected List<Materialformulapeso> listaMaterialformulapeso = new ListSet<Materialformulapeso>(Materialformulapeso.class);
	protected List<Materialformulavalorvenda> listaMaterialformulavalorvenda = new ListSet<Materialformulavalorvenda>(Materialformulavalorvenda.class);
	protected List<Materialformulacusto> listaMaterialformulacusto = new ListSet<Materialformulacusto>(Materialformulacusto.class);
	protected List<Materialformulavendaminimo> listaMaterialformulavendaminimo = new ListSet<Materialformulavendaminimo>(Materialformulavendaminimo.class);
	protected List<Materialformulavendamaximo> listaMaterialformulavendamaximo = new ListSet<Materialformulavendamaximo>(Materialformulavendamaximo.class);
	protected List<Materialformulamargemcontribuicao> listaMaterialformulamargemcontribuicao = new ListSet<Materialformulamargemcontribuicao>(Materialformulamargemcontribuicao.class);
	protected Set<Materialimposto> listaMaterialimposto = new ListSet<Materialimposto>(Materialimposto.class);
	protected List<Materiallocalarmazenagem> listaMateriallocalarmazenagem = new ListSet<Materiallocalarmazenagem>(Materiallocalarmazenagem.class);
	protected List<Patrimonioitem> listaPatrimonioitem;
	protected List<Materialpneumodelo> listaMaterialpneumodelo = new ListSet<Materialpneumodelo>(Materialpneumodelo.class);
	protected List<MaterialFaixaMarkup> listaMaterialFaixaMarkup = new ListSet<MaterialFaixaMarkup>(MaterialFaixaMarkup.class);
	protected List<Embalagem> listaEmbalagem = new ListSet<Embalagem>(Embalagem.class);
	
	protected MaterialRateioEstoque materialRateioEstoque;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	protected Boolean produto;
	protected Boolean patrimonio;
	protected Boolean servico;
	protected Boolean epi;
	protected Arquivo arquivo;
	protected Image image;
	
//	SPED
	protected Tipoitemsped tipoitemsped;
	protected Ncmcapitulo ncmcapitulo;
	protected String codigobarras;
	protected Boolean desconsiderarcodigobarras;
	protected String ncmcompleto;
	protected String extipi;
	protected String codlistaservico;
	protected Boolean tributacaoestadual;
	protected Double basecalculost;
	protected Double valorst;
	protected Origemproduto origemproduto;
	protected String nve;
	protected String codigonbs;
	protected String cest;
	
	
//  TRIBUTA��O
	protected Tributacaoecf tributacaoecf;
	protected Grupotributacao grupotributacao;
	protected FormaTributacaoEnum formaTributacao;
	
//	VETERIN�RIA
	protected FaturamentoVeterinaria faturamentoVeterinaria;
	protected Double qtdeunidadeveterinaria;
	
//  E-COMMERCE GERAL
	protected Integer idEcommerce;
	protected String maisInformacoes;
	protected String detalhesTecnicos;
	protected String itensInclusos;
	protected String conexoes;
	protected StatusUsoEnum statusUso;
	protected ModeloDisponibilidade modeloDisponibilidade;
	protected Money precoVendaFantasia;
	protected Integer descontoAVista;
	protected Boolean precoSobConsulta;
	protected Boolean permiteVendaSite = Boolean.TRUE;	
	protected Double ecommerce_largura;
	protected Double ecommerce_comprimento;
	protected Double ecommerce_altura;	
	protected Double qtdeCarrinho;
	protected Double qtdeEstoqueMinimoEcommerce;
	
//  E-COMMERCE ECOMPLETO
	protected List<ImagensEcommerce> listaImagensEcommerce = new ListSet<ImagensEcommerce>(ImagensEcommerce.class);
	protected List<MaterialAtributosMaterial> listaMaterialAtributosMaterial ;
	
//  E-COMMERCE TRAYCORP
	protected Integer qtdeMaximaCarrinho;
	protected ExibirMatrizAtributosEnum exibirMatrizAtributos;
	protected Boolean contraProposta;
	protected FreteGratisEnum freteGratis;
	protected Integer idPaiExterno;
	protected Boolean enviarInformacoesEcommerce;;
	
//	TRANSIENTES
	protected String nomescape;
	protected List<Empresa> listaEmpresa;
	protected String caracteristicas;
	protected Integer cdmaterialclasse;
	protected Integer disponivel;
	protected String classestring;
	protected Integer cdmaterialtrans;
	protected Integer cdmaterialproducao;
	protected Integer cdmaterialrelacionado;
	protected Integer cdmaterialkitflexivel;
	protected Money valorUltimaOrdemCompra;
	protected String nomeFornecedor;
	protected java.util.Date dtUltimaCompra;	
	protected List<Material> listaMaterialitemgrade;
	protected Date prazoentrega;
	protected Double preco;
	protected Loteestoque loteestoque;
	private Double materiaisDisponiveisEmEstoque;
	protected Integer cdlocalarmazenagem;
	protected Integer cdempresa;
	protected Integer cdcliente;
	protected Integer cdpedidovendamaterial;
	protected Integer cdvendamaterial;
	protected Integer cdvendaorcamentomaterial;
	protected Integer itemindex;
	protected Integer cdmaterialtabelapreco;
	protected Double multiplicador;
	protected Double comprimentos;
	protected Double comprimentosoriginal;
	protected Double larguras;
	protected Double alturas;
	protected Double fatorconversaocomprimento;
	protected Double margemarredondamento;
	protected Boolean exibirDimensoes;
	protected String descricaocomplementar;
	protected Integer quantidadeitemgrade = 1;
	protected Double qtdvolume;
	protected String indexEntregamaterial;
	protected Integer cdordemcompramaterial;
	protected String whereInMaterialItenGrade;
	protected Grupotributacao grupotributacaoNota;
	protected Ordemcompra ordemcompratrans;
	protected Boolean existReserva;
	protected Boolean isValorvendaFormula;
	protected Boolean isValorvendaminimoFormula;
	protected Boolean isValorvendamaximoFormula;
	protected String whereIn;
	protected Boolean tabelaprecoComPrazopagamento;
//	protected String localarmazenagem;
	protected Localarmazenagem localarmazenagem;
	protected Boolean revendedor;
	protected Double quantidadeOportunidade;
	protected Projeto projeto;
	protected Materialunidademedida materialunidademedida;
	protected Integer qtdMaterialNota = 1;
	protected String plaquetaSerie;
	protected Double consumoMaterialAcompanhaBanda;
	
	// Transiente
	protected Integer idEcommerceTrans;
	protected String unidademedidaString;
	protected Boolean bloqueado;
	protected Integer cdcontratomaterial;
	protected Integer cdrestricaocontratomaterial;
	protected Contrato contrato;
	protected String nomenfdescricaonota;
	protected Double qtdenfp;
	protected String alturaLarguraComprimentoConcatenados;
	protected String codigoalternativo;
	protected Boolean irListagemDireto = Boolean.FALSE;
	protected Integer qtdeagendaproducao;
	protected String idDocumentoRegistrarEntrada;
	protected String idFornecedorRegistrarEntrada;
	protected Boolean fromVeiculo;
	protected Double fracao;
	protected Double valorunitario;
	protected Double valordiatrans;
	protected Boolean unidadesecundariaTabelapreco;
	protected Patrimonioitem patrimonioitem;
	protected Vendamaterialmestre vendamaterialmestre;
	protected Double produto_qtde;
	protected Producaoagenda producaoagendatrans;
	protected Producaoordem producaoordemtrans;
	protected Producaoetapaitem producaoetapaitemtrans;
	protected Materialformulapeso materialformulapesoTrans;
	protected Boolean fromEntrafiscalMontarGrade;
	protected Boolean fromEntrafiscalProcessMontarGrade;
	protected Integer casasdecimaisestoqueTrans;
	protected Double vendaaltura;
	protected Double vendalargura;
	protected Double vendacomprimento;
	protected Unidademedida vendaunidademedida;
	protected String vendasimbolounidademedida;
	protected Double vendafrete;
	protected Double valorcustoByCalculoProducao;
	protected String descricaoInconsistenciaLancamento;
	protected Boolean considerarValorvendaCalculado;
	protected Boolean checado;
	protected Integer numerocopiasimpressaoetiqueta;
	protected Boolean modelodocumentofiscalICMS;
	protected String nomealternativo;
	protected String whereInSolicitacaocompra;
	protected Pneu pneu;
	protected Boolean inverterconversao;
	protected Material banda;
	protected Comissionamento comissionamentoTabelapreco;
	protected Double valorvendasemdesconto;
	protected Boolean temTabelaPrecoVigenteMaterial;
	protected Boolean exibirItensKitFlexivelTrans = true;
	protected Double qtdeDisponivelEmEstoque;
	protected Localdestinonfe localDestinoNfe;
	protected Money valorCustoEcommerce;
	protected Money valorVendaEcommerce;
	protected Double qtdeEstoqueEcommerce;
	
	
	protected String codigoBarrasEmbalagem;

//	PRODUTO
	protected Produto materialproduto;
	protected String produto_nomereduzido;
	protected Integer produto_qtdmin;
	protected Boolean produto_gerar;
	protected String produto_anotacoes;
	protected Double produto_largura;
	protected Double produto_comprimento;
	protected Double produto_altura;
	protected Boolean produto_materialcorte;
	protected Double produto_fatorconversao;
	protected Boolean produto_arredondamentocomprimento;	
	protected Double produto_margemarredondamento;	
	protected Double produto_fatorconversaoproducao;
	
//	PATRIM�NIO
	protected String patrimonio_nomereduzido;
	protected Money patrimonio_valor;
	protected Money patrimonio_valormercado;
	protected Money patrimonio_valorref;
	protected Integer patrimonio_vida;
	protected Money patrimonio_depreciacao;
	protected String patrimonio_anotacoes;
	
//	EPI
	protected String epi_nomereduzido;
	protected Integer epi_duracao;
	protected String epi_anotacoes;
	protected Integer epi_caepi;
	
//	SERVI�O
	protected String servico_nomereduzido;
	protected Integer servico_qtdmin;
	protected Boolean servico_gerar;
	protected String servico_anotacoes;
	private String codigoAnvisa;
	private Boolean isento;
	private String codigoIsencao;
	
	protected Vgerenciarmaterial vgerenciarmaterial;
	protected Vgerenciarmaterialsoma vgerenciarmaterialsoma;
	
	// PARA JOINS
	protected Set<Tarefarecursogeral> listaTarefarecursogeral;
	protected List<Planejamentorecursogeral> listaPlanejamentorecursogeral;
	
	protected String autocompleteDescription;
	
	//Al�quotas para serem calculadas de acordo com o grupo de tributa��o do material
	protected Money aliquotaicms;
	protected Money aliquotaicmsst;
	protected Money aliquotaipi;
	protected Money aliquotapis;
	protected Money aliquotacofins;
	protected Money aliquotaiss;
	protected Double aliquotamva;
	protected Money aliquotacsll;
	protected Money aliquotair;
	protected Money aliquotainss;
	protected Money aliquotaFCP;
	protected Money aliquotaFCPST;
	
	protected Double percentualBcicms;
	protected Double percentualBcipi;
	protected Double percentualBcpis;
	protected Double percentualBccofins;
	protected Double percentualBciss;
	protected Double percentualBccsll;
	protected Double percentualBcir;
	protected Double percentualBcinss;
	protected Double percentualBcfcp;
	protected Double percentualBcfcpst;
	
	protected Retencao retencaoIpi;
	
	//Informa��es referentes ao estado de tributa��o de origem e destino
	protected Empresa empresaTributacaoOrigem;
	protected Endereco enderecoTributacaoOrigem;
	protected Endereco enderecoTributacaoDestino;
	protected Endereco endereconotaDestino;
	protected Boolean enderecoavulso;
	protected Naturezaoperacao naturezaoperacao;
	protected Operacaonfe operacaonfe;
	
	protected String index;
	protected Double ultimovalorcompra;
	protected Double ultimovalorvenda;
	protected Double qtdeemexpedicao;
	protected Double qtdeemcompra;
	protected Double qtdeemreserva;
	protected Materialtabelapreco materialtabelapreco;
	protected Cliente cliente;
	protected Empresa empresatrans;
	protected Double quantidade;
	protected Double qtdeTotalLote;
	protected Double qtdePedido;
	protected Double qtdeTotaItens;
	protected Prazopagamento prazopagamento;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean exibiritensproducaovenda;
	protected List<Materiaprima> listaMateriaprima;
	protected Boolean pesoliquidocalculadopelaformula;
	protected Double percentualdescontoTabela;
	protected Double valorCustoForRecalculoCustoMedio;
	
	//AUXILIARES PARA GERA��O DO RELAT�RIO DE SUGEST�O DE COMPRA
	protected Double sugestaocompraestoqueminimo;
	protected Double sugestaocompravendas;
	protected Double sugestaocompraestoqueatual;
	protected Double sugestaocompraciclos;
	protected Double sugestaocomprasugestao;
	protected Double sugestaocompratotalpeso;
	protected Double sugestaocompratotalpesobruto;
	
	protected Materialrelacionado materialrelacionadoForCurvaabc;
	
	public Material() {}
	
	public Material(Integer cdmaterial){
		this.cdmaterial = cdmaterial;
	}
	
	public Material(Integer cdmaterial, String nome){
		this.cdmaterial = cdmaterial;
		this.nome = nome;
	}
	
	public Material(Integer cdmaterial, String nome, String identificacao) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.identificacao = identificacao;
	}
	
	public Material(Integer cdmaterial, String nome, Double qtdeemexpedicao, Double qtdeemcompra) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.qtdeemexpedicao = qtdeemexpedicao;
		this.qtdeemcompra = qtdeemcompra;
	}
	
	public Material(Integer cdmaterial, String nome, Double qtdeemreserva) {
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.qtdeemreserva = qtdeemreserva;
	}

	public Material(Integer cdmaterial, String nome, Boolean produto, Boolean epi, Boolean patrimonio, Boolean servico, 
					String gruponome, String tiponome, String categoriadescricao, String contagerencialnome, Double valorUltimaOrdemCompra, Double _valorCusto, Double _valorVenda, 
					Double _valorVendaMaximo, Double _valorVendaMinimo, Date dtUltimaCompra, String _identificador, 
					String _referencia, String _codbarras, String codigofabricante, String nomenf, Unidademedida unidademedida, Materialcor materialcor,
					Integer qtdeunidade, Localizacaoestoque localizacaoestoque, Empresamodelocontrato empresamodelocontrato, 
					Double qtdereferencia, Integer tempoentrega, Money frete, Money valorfrete ,Grupotributacao grupotributacao,
					String contagerencialvenda, String centrocustovenda, Tipoitemsped tipoitemsped, Ncmcapitulo ncmcapitulo, 
					String ncmcompleto, String cest, String extipi, String codlistaservico , Double fracao, Double valorunitario, Double pesobruto, Set<Materialunidademedida> listaMaterialunidademedida){
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.produto = produto;
		this.epi = epi;
		this.patrimonio = patrimonio;
		this.servico = servico;
		this.materialgrupo = new Materialgrupo(gruponome);
		this.materialtipo = new Materialtipo(tiponome);
		this.materialcategoria = new Materialcategoria(categoriadescricao);
		this.contagerencial = new Contagerencial(contagerencialnome);
		this.valorUltimaOrdemCompra = new Money(valorUltimaOrdemCompra);
		this.valorcusto = _valorCusto;
		this.valorvenda = _valorVenda;
		this.valorvendamaximo = _valorVendaMaximo;
		this.valorvendaminimo = _valorVendaMinimo;
		this.dtUltimaCompra = dtUltimaCompra;
		this.identificacao = _identificador;
		this.referencia = _referencia;
		this.codigobarras = _codbarras;
		this.codigofabricante = codigofabricante;
		this.nomenf = nomenf;
		this.unidademedida = unidademedida;
		this.materialcor = materialcor;
		this.qtdeunidade =  qtdeunidade;
		this.localizacaoestoque = localizacaoestoque;
		this.empresamodelocontrato = empresamodelocontrato;
		this.qtdereferencia = qtdereferencia;
		this.tempoentrega = tempoentrega;
//		this.icms = icms;
//		this.ipi = ipi;
//		this.lucro = lucro;
//		this.valorlucro = lucro;
		this.frete = frete;
		this.valorfrete = valorfrete;
		this.grupotributacao = grupotributacao;
		this.contagerencialvenda = new Contagerencial(contagerencialvenda);
		this.centrocustovenda = new Centrocusto(centrocustovenda);
		this.tipoitemsped = tipoitemsped;
		this.ncmcapitulo = ncmcapitulo;
		this.ncmcompleto = ncmcompleto;
		this.cest = cest;
		this.extipi = extipi;
		this.codlistaservico = codlistaservico;
		this.fracao = fracao;
		this.valorunitario = valorunitario;
		this.pesobruto = pesobruto;
		this.listaMaterialunidademedida = listaMaterialunidademedida;
	}
	
	public Material(String nome){
		this.nome = nome;
	}
	
	public Material(String nome, Double estoqueminimo, Double vendas, Double estoqueatual, Double ciclos, 
			Double sugestaocompra, String simbolo, Double peso, Double pesobruto){
		this.nome = nome;
		this.sugestaocompraestoqueminimo = estoqueminimo;
		this.sugestaocompravendas = vendas != null ? vendas : 0.0;
		this.sugestaocompraestoqueatual = estoqueatual != null ? estoqueatual : 0.0;
		this.sugestaocompraciclos = ciclos != null ? ciclos : 0.0;
		this.sugestaocomprasugestao = sugestaocompra != null  && sugestaocompra > 0 ? sugestaocompra : 0.0;
		this.unidademedida = new Unidademedida(simbolo);
		this.peso = peso != null ? SinedUtil.roundByParametro(peso) : null;
		this.pesobruto = pesobruto != null ? SinedUtil.roundByParametro(pesobruto) : null;
	}
	
	public Material(Integer cdmaterial, String nome, String identificacao, Double estoqueminimo, Double vendas, String simbolo, Integer casasdecimaisestoque, Double peso, Double pesobruto){
		this.cdmaterial = cdmaterial;
		this.nome = nome;
		this.identificacao = identificacao;
		this.sugestaocompraestoqueminimo = estoqueminimo;
		this.sugestaocompravendas = vendas != null ? vendas : 0.0;
		this.unidademedida = new Unidademedida(simbolo);
		this.unidademedida.setCasasdecimaisestoque(casasdecimaisestoque);
		this.peso = peso != null ? SinedUtil.roundByParametro(peso) : null;
		this.pesobruto = pesobruto != null ? SinedUtil.roundByParametro(pesobruto) : null;
	}
	
	public Material(Integer cdmaterial, Double qtdecustoinicial) {
		this.cdmaterial = cdmaterial;
		this.qtdecustoinicial = qtdecustoinicial;
	}
	
	public Material(Integer cdmaterial, String referencia,
			String identificacao, String nome, Double peso,
			Double pesobruto, Unidademedida unidademedida) {
		this.cdmaterial = cdmaterial;
		this.referencia = referencia;
		this.identificacao = identificacao;
		this.nome = nome;
		this.peso = peso;
		this.pesobruto = pesobruto;
		this.unidademedida = unidademedida;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_material")
	public Integer getCdmaterial() {
		return cdmaterial;
	}
	
	@MaxLength(2000)
	@Required   
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Nome para NF")
	@MaxLength(100)
	public String getNomenf() {
		return nomenf;
	}
	
	@DisplayName("C�digo do fabricante")
	@MaxLength(25)
	public String getCodigofabricante() {
		return codigofabricante;
	}
	
	@Required
	@DisplayName("Unidade de medida")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	
	@DisplayName("Considerar Venda em M�ltiplos")
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}

	@DisplayName("Conta gerencial compra")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	@DisplayName("Conta gerencial venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencialvenda")
	public Contagerencial getContagerencialvenda() {
		return contagerencialvenda;
	}
	
	@DisplayName("Conta cont�bil")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacontabil")
	public ContaContabil getContaContabil() {
		return contaContabil;
	}

	@DisplayName("Centro de custo venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocustovenda")
	public Centrocusto getCentrocustovenda() {
		return centrocustovenda;
	}
	@DisplayName("C�digo ANP")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcodigoanp")
	public Codigoanp getCodigoanp() {
		return codigoanp;
	}
	@Required
	@DisplayName("Grupo do material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}

	@DisplayName("Tipo do material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtipo")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	
	@DisplayName("Cor do material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialcor")
	public Materialcor getMaterialcor() {
		return materialcor;
	}
	
	@MaxLength(10)
	@DisplayName("Tempo de entrega")
	public Integer getTempoentrega() {
		return tempoentrega;
	}
	
	@DisplayName("Meta de prazo m�dio de Compras")	
	public Double getMetaprazomediocompra() {
		return metaprazomediocompra;
	}

	@DisplayName("Toler�ncia para prazo de compra")
	public Double getToleranciaprazocompra() {
		return toleranciaprazocompra;
	}

	@Column(name="caracteristica")
	@DisplayName("Caracter�stica")
	public Boolean getRadcaracteristica() {
		return radcaracteristica;
	}
	
	@Column(name="empresa")
	@DisplayName("Empresa")
	public Boolean getRadempresa() {
		return radempresa;
	}
	
	@Column(name="inspecao")
	@DisplayName("Inspe��o")
	public Boolean getRadinspecao() {
		return radinspecao;
	}
	
	@Column(name="colaborador")
	@DisplayName("Colaborador/Comiss�o")
	public Boolean getRadcolaborador() {
		return radcolaborador;
	}
	
	@DisplayName("Peso L�quido no Valor de Venda")
	public Boolean getPesoliquidovalorvenda() {
		return pesoliquidovalorvenda;
	}
	
	@DisplayName("METRO C�BICO DA QTDE DA VENDA")
	public Boolean getMetrocubicovalorvenda() {
		return metrocubicovalorvenda;
	}
	
	@DisplayName("Produto diferenciado")
	public Produtodiferenciado getProdutodiferenciado() {
		return produtodiferenciado;
	}
	
	@DisplayName("Tributa��o municipal")
	public Boolean getTributacaomunicipal() {
		return tributacaomunicipal;
	}
	
	@DisplayName("Medida de Pneu Padr�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneumedida")
	public Pneumedida getPneumedida() {
		return pneumedida;
	}
	
	public void setPneumedida(Pneumedida pneumedida) {
		this.pneumedida = pneumedida;
	}
	
	@MaxLength(6)
	@MinValue(0)
	@DisplayName("Profundidade do Sulco (mm)")
	public Double getProfundidadesulco() {
		return profundidadesulco;
	}
	
	@DisplayName("Caracter�stica")
	public Boolean getCadastrarCaracteristica() {
		return cadastrarCaracteristica;
	}
	
	@DisplayName("Colaboradores/Comiss�o")
	public Boolean getCadastrarColaboradoresPermissao() {
		return cadastrarColaboradoresPermissao;
	}
	
	@DisplayName("Convers�o de Unidades")
	public Boolean getCadastrarConversaoUnidades() {
		return cadastrarConversaoUnidades;
	}
	
	@DisplayName("Item Inspe��o")
	public Boolean getCadastrarItemInspecao() {
		return cadastrarItemInspecao;
	}
	
	@DisplayName("Materiais Similares")
	public Boolean getCadastrarMateriaisSimilares() {
		return cadastrarMateriaisSimilares;
	}
	
	@DisplayName("N�meros de S�rie")
	public Boolean getCadastrarNumeroSerie() {
		return cadastrarNumeroSerie;
	}
	
	@DisplayName("Sugest�o na Venda")
	public Boolean getCadastrarSugestaoVenda() {
		return cadastrarSugestaoVenda;
	}
	
	@OneToOne(fetch=FetchType.LAZY,cascade = {CascadeType.ALL})
	@JoinColumn(name="cdmaterialrateioestoque")
	@DisplayName("Rateio Estoque")
	public MaterialRateioEstoque getMaterialRateioEstoque() {
		return materialRateioEstoque;
	}
	
	public void setMaterialRateioEstoque(MaterialRateioEstoque materialRateioEstoque) {
		this.materialRateioEstoque = materialRateioEstoque;
	}
	public void setProfundidadesulco(Double profundidadesulco) {
		this.profundidadesulco = profundidadesulco;
	}
	
	public void setCadastrarCaracteristica(Boolean cadastrarCaracteristica) {
		this.cadastrarCaracteristica = cadastrarCaracteristica;
	}
	
	public void setCadastrarColaboradoresPermissao(
			Boolean cadastrarColaboradoresPermissao) {
		this.cadastrarColaboradoresPermissao = cadastrarColaboradoresPermissao;
	}
	
	public void setCadastrarConversaoUnidades(Boolean cadastrarConversaoUnidades) {
		this.cadastrarConversaoUnidades = cadastrarConversaoUnidades;
	}
	
	public void setCadastrarItemInspecao(Boolean cadastrarItemInspecao) {
		this.cadastrarItemInspecao = cadastrarItemInspecao;
	}
	
	public void setCadastrarMateriaisSimilares(
			Boolean cadastrarMateriaisSimilares) {
		this.cadastrarMateriaisSimilares = cadastrarMateriaisSimilares;
	}
	
	public void setCadastrarNumeroSerie(Boolean cadastrarNumeroSerie) {
		this.cadastrarNumeroSerie = cadastrarNumeroSerie;
	}
	
	public void setCadastrarSugestaoVenda(Boolean cadastrarSugestaoVenda) {
		this.cadastrarSugestaoVenda = cadastrarSugestaoVenda;
	}
	
	public void setTributacaomunicipal(Boolean tributacaomunicipal) {
		this.tributacaomunicipal = tributacaomunicipal;
	}
	
	public void setProdutodiferenciado(Produtodiferenciado produtodiferenciado) {
		this.produtodiferenciado = produtodiferenciado;
	}
	
	public void setMetrocubicovalorvenda(Boolean metrocubicovalorvenda) {
		this.metrocubicovalorvenda = metrocubicovalorvenda;
	}
	
	public void setPesoliquidovalorvenda(Boolean pesoliquidovalorvenda) {
		this.pesoliquidovalorvenda = pesoliquidovalorvenda;
	}
	
	public void setRadcolaborador(Boolean radcolaborador) {
		this.radcolaborador = radcolaborador;
	}
	
	public void setCodigoanp(Codigoanp codigoanp) {
		this.codigoanp = codigoanp;
	}
	
	public void setRadinspecao(Boolean radinspecao) {
		this.radinspecao = radinspecao;
	}
	
	@DisplayName("Modelo de contrato em RTF")
	@JoinColumn(name="cdempresamodelocontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Empresamodelocontrato getEmpresamodelocontrato() {
		return empresamodelocontrato;
	}
	
	@DisplayName("Material para importa��o de cte")
	public Boolean getMaterialcte() {
		return materialcte;
	}
	public void setMaterialcte(Boolean materialcte) {
		this.materialcte = materialcte;
	}

	@DisplayName("Produto com controle de lote")
	public Boolean getObrigarlote() {
		return obrigarlote;
	}
	public void setObrigarlote(Boolean obrigarlote) {
		this.obrigarlote = obrigarlote;
	}

	@DisplayName("NVE")
	@MaxLength(6)
	public String getNve() {
		return nve;
	}
	
	@MaxLength(9)
	@DisplayName("C�digo NBS")
	public String getCodigonbs() {
		return codigonbs;
	}
	
	public void setCodigonbs(String codigonbs) {
		this.codigonbs = codigonbs;
	}
	
	public void setNve(String nve) {
		this.nve = nve;
	}
	
	public void setEmpresamodelocontrato(Empresamodelocontrato empresamodelocontrato) {
		this.empresamodelocontrato = empresamodelocontrato;
	}
	
	@Column(name="fornecedor")
	@DisplayName("Fornecedor")
	public Boolean getRadfornecedor() {
		return radfornecedor;
	}
	
	@Column(name="imposto")
	@DisplayName("Imposto")
	public Boolean getRadimposto() {
		return radimposto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial", insertable=false, updatable=false)
	public Vgerenciarmaterial getVgerenciarmaterial() {
		return vgerenciarmaterial;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial", insertable=false, updatable=false)
	public Vgerenciarmaterialsoma getVgerenciarmaterialsoma() {
		return vgerenciarmaterialsoma;
	}
	
	@DisplayName("Pre�o de custo")
	public Double getValorcusto() {
		return valorcusto;
	}
	
	@DisplayName("Custo mat�ria-prima")
	public Money getValorcustomateriaprima() {
		return valorcustomateriaprima;
	}

	@DisplayName("Frete(%)")
	public Money getFrete() {
		return frete;
	}

	@DisplayName("Pre�o de venda")
	public Double getValorvenda() {
		return valorvenda;
	}
	
	@DisplayName("Valor de indeniza��o")
	public Double getValorindenizacao() {
		return valorindenizacao;
	}
	
	@DisplayName("Pre�o m�nimo de venda")
	public Double getValorvendaminimo() {
		return valorvendaminimo;
	}

	@DisplayName("Pre�o m�ximo de venda")
	public Double getValorvendamaximo() {
		return valorvendamaximo;
	}
	
	@MaxLength(8)
	@DisplayName("Quantidade por unidade")
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}

	@DisplayName("Kit")
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	
	@DisplayName("Produ��o")
	public Boolean getProducao() {
		return producao;
	}
	
	@DisplayName("Ciclo de Produ��o Padr�o")
	@JoinColumn(name="cdproducaoetapa")
	@ManyToOne(fetch=FetchType.LAZY)
	public Producaoetapa getProducaoetapa() {
		return producaoetapa;
	}
	
	@DisplayName("Tipo de item")
	public Tipoitemsped getTipoitemsped() {
		return tipoitemsped;
	}

	@DisplayName("G�nero do NCM")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdncmcapitulo")
	public Ncmcapitulo getNcmcapitulo() {
		return ncmcapitulo;
	}

	@DisplayName("C�digo de barras")
	@MaxLength(14)
	public String getCodigobarras() {
		return codigobarras;
	}
	
	@DisplayName("Desconsiderar C�digo de Barras")
	public Boolean getDesconsiderarcodigobarras() {
		return desconsiderarcodigobarras;
	}

	@DisplayName("NCM completo")
	@MaxLength(8)
	public String getNcmcompleto() {
		return ncmcompleto;
	}

	@DisplayName("EX TIPI")
	@MaxLength(3)
	public String getExtipi() {
		return extipi;
	}

	@DisplayName("C�digo da Lista de Itens de Servi�o")
	@MaxLength(9)
	public String getCodlistaservico() {
		return codlistaservico;
	}
	
	@DisplayName("Tributa��o estadual")
	public Boolean getTributacaoestadual() {
		return tributacaoestadual;
	}
	
	@DisplayName("Base de C�lculo ST")
	public Double getBasecalculost() {
		return basecalculost;
	}
	
	@DisplayName("Valor ST")
	public Double getValorst() {
		return valorst;
	}
	
	@DisplayName("Grupo de tributa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgrupotributacao")
	public Grupotributacao getGrupotributacao() {
		return grupotributacao;
	}
	
	@DisplayName("Forma de tributa��o")
	public FormaTributacaoEnum getFormaTributacao() {
		return formaTributacao;
	}
	
	@DisplayName("Tributa��o ECF")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdtributacaoecf")
	public Tributacaoecf getTributacaoecf() {
		return tributacaoecf;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public String getIdentificacaoordenacao() {
		return identificacaoordenacao;
	}
	
	@DisplayName("Integra��o com ECF")
	public Boolean getVendaecf() {
		return vendaecf;
	}
	
	@DisplayName("% imposto")
	public Double getPercentualimpostoecf() {
		return percentualimpostoecf;
	}
	
	@DisplayName("Categoria de material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialcategoria")
	public Materialcategoria getMaterialcategoria() {
		return materialcategoria;
	}

	public void setMaterialcategoria(Materialcategoria materialcategoria) {
		this.materialcategoria = materialcategoria;
	}

	public void setPercentualimpostoecf(Double percentualimpostoecf) {
		this.percentualimpostoecf = percentualimpostoecf;
	}
	
	public void setVendaecf(Boolean vendaecf) {
		this.vendaecf = vendaecf;
	}
	
	public void setIdentificacaoordenacao(String identificacaoordenacao) {
		this.identificacaoordenacao = identificacaoordenacao;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setTributacaoestadual(Boolean tributacaoestadual) {
		this.tributacaoestadual = tributacaoestadual;
	}
	
	public void setBasecalculost(Double basecalculost) {
		this.basecalculost = basecalculost;
	}

	public void setValorindenizacao(Double valorindenizacao) {
		this.valorindenizacao = valorindenizacao;
	}
	
	public void setValorst(Double valorst) {
		this.valorst = valorst;
	}

	public void setGrupotributacao(Grupotributacao grupotributacao) {
		this.grupotributacao = grupotributacao;
	}
	
	public void setFormaTributacao(FormaTributacaoEnum formaTributacao) {
		this.formaTributacao = formaTributacao;
	}
	
	public void setTributacaoecf(Tributacaoecf tributacaoecf) {
		this.tributacaoecf = tributacaoecf;
	}

	public void setTipoitemsped(Tipoitemsped tipoitemsped) {
		this.tipoitemsped = tipoitemsped;
	}

	public void setNcmcapitulo(Ncmcapitulo ncmcapitulo) {
		this.ncmcapitulo = ncmcapitulo;
	}

	public void setCodigobarras(String codigobarras) {
		this.codigobarras = codigobarras;
	}

	public void setDesconsiderarcodigobarras(Boolean desconsiderarcodigobarras) {
		this.desconsiderarcodigobarras = desconsiderarcodigobarras;
	}
	
	public void setNcmcompleto(String ncmcompleto) {
		this.ncmcompleto = ncmcompleto;
	}

	public void setExtipi(String extipi) {
		this.extipi = extipi;
	}

	public void setCodlistaservico(String codlistaservico) {
		this.codlistaservico = codlistaservico;
	}

	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}

	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}

	public void setProducao(Boolean producao) {
		this.producao = producao;
	}
	
	public void setProducaoetapa(Producaoetapa producaoetapa) {
		this.producaoetapa = producaoetapa;
	}
	
	public void setValorcusto(Double valorcusto) {
		this.valorcusto = valorcusto;
	}

	public void setValorcustomateriaprima(Money valorcustomateriaprima) {
		this.valorcustomateriaprima = valorcustomateriaprima;
	}
	
	public void setFrete(Money frete) {
		this.frete = frete;
	}
	
	public void setValorvenda(Double valorvenda) {
		this.valorvenda = valorvenda;
	}
	
	public void setValorvendaminimo(Double valorvendaminimo) {
		this.valorvendaminimo = valorvendaminimo;
	}
	
	public void setCentrocustovenda(Centrocusto centrocustovenda) {
		this.centrocustovenda = centrocustovenda;
	}

	public void setValorvendamaximo(Double valorvendamaximo) {
		this.valorvendamaximo = valorvendamaximo;
	}

	public void setVgerenciarmaterial(Vgerenciarmaterial vgerenciarmaterial) {
		this.vgerenciarmaterial = vgerenciarmaterial;
	}
	
	public void setVgerenciarmaterialsoma(
			Vgerenciarmaterialsoma vgerenciarmaterialsoma) {
		this.vgerenciarmaterialsoma = vgerenciarmaterialsoma;
	}
	
	public void setNome(String nome) {
		this.nome = StringUtils.trimToNull(nome);
	}
	
	public void setNomenf(String nomenf) {
		this.nomenf = StringUtils.trimToNull(nomenf);
	}

	public void setCodigofabricante(String codigofabricante) {
		this.codigofabricante = codigofabricante;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}

	public void setMaterialcor(Materialcor materialcor) {
		this.materialcor = materialcor;
	}
	
	public void setTempoentrega(Integer tempoentrega) {
		this.tempoentrega = tempoentrega;
	}
	
	public void setMetaprazomediocompra(Double metaprazomediocompra) {
		this.metaprazomediocompra = metaprazomediocompra;
	}

	public void setToleranciaprazocompra(Double toleranciaprazocompra) {
		this.toleranciaprazocompra = toleranciaprazocompra;
	}

	public void setRadcaracteristica(Boolean radcaracteristica) {
		this.radcaracteristica = radcaracteristica;
	}
	
	public void setRadempresa(Boolean radempresa) {
		this.radempresa = radempresa;
	}
	
	public void setRadfornecedor(Boolean radfornecedor) {
		this.radfornecedor = radfornecedor;
	}
	
	public void setRadimposto(Boolean radimposto) {
		this.radimposto = radimposto;
	}

	public void setCdmaterial(Integer cdmaterial) {
		this.cdmaterial = cdmaterial;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}
	
	@DisplayName("Composi��o do Kit")
	@OneToMany(mappedBy="material")
	public Set<Materialrelacionado> getListaMaterialrelacionado() {
		return listaMaterialrelacionado;
	}
	@DisplayName("Composi��o do Kit Flex�vel")
	@OneToMany(mappedBy="material")
	public Set<Materialkitflexivel> getListaMaterialkitflexivel() {
		return listaMaterialkitflexivel;
	}
	@DisplayName("F�rmula para calculo do peso")
	@OneToMany(mappedBy="material")
	public List<Materialformulapeso> getListaMaterialformulapeso() {
		return listaMaterialformulapeso;
	}
	@DisplayName("Custo por Empresa")
	@OneToMany(mappedBy="material")
	public Set<MaterialCustoEmpresa> getListaMaterialCustoEmpresa() {
		return listaMaterialCustoEmpresa;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="material")
	public List<Materialhistorico> getListaMaterialhistorico() {
		return listaMaterialhistorico;
	}
	@DisplayName("Materiais Similares")
	@OneToMany(mappedBy="material")
	public Set<Materialsimilar> getListaMaterialsimilar() {
		return listaMaterialsimilar;
	}
	@DisplayName("Sugest�o na Venda")
	@OneToMany(mappedBy="material")
	public Set<Materialvenda> getListaMaterialvenda() {
		return listaMaterialvenda;
	}
	@DisplayName("F�rmula para calculo do custo")
	@OneToMany(mappedBy="material")
	public List<Materialformulacusto> getListaMaterialformulacusto() {
		return listaMaterialformulacusto;
	}
	@DisplayName("F�rmula para calculo do valor m�nimo de venda")
	@OneToMany(mappedBy="material")
	public List<Materialformulavendaminimo> getListaMaterialformulavendaminimo() {
		return listaMaterialformulavendaminimo;
	}
	@DisplayName("F�rmula para calculo do valor m�ximo de venda")
	@OneToMany(mappedBy="material")
	public List<Materialformulavendamaximo> getListaMaterialformulavendamaximo() {
		return listaMaterialformulavendamaximo;
	}
	@DisplayName("F�rmula para calculo da margem de contrib. R$")
	@OneToMany(mappedBy="material")
	public List<Materialformulamargemcontribuicao> getListaMaterialformulamargemcontribuicao() {
		return listaMaterialformulamargemcontribuicao;
	}

	@DisplayName("Colaboradores/Comiss�o")
	@OneToMany(mappedBy="material")
	public Set<Materialcolaborador> getListaMaterialcolaborador() {
		return listaMaterialcolaborador;
	}
	
	@DisplayName("Quantidade m�nima")
	@OneToMany(mappedBy="material")
	public List<Materiallocalarmazenagem> getListaMateriallocalarmazenagem() {
		return listaMateriallocalarmazenagem;
	}
	
	@DisplayName("Modelos Vinculados")
	@OneToMany(mappedBy="material")
	public List<Materialpneumodelo> getListaMaterialpneumodelo() {
		return listaMaterialpneumodelo;
	}
	
	@DisplayName("Faixa de Markup")
	@OneToMany(mappedBy="material")
	public List<MaterialFaixaMarkup> getListaMaterialFaixaMarkup() {
		return listaMaterialFaixaMarkup;
	}

	@DisplayName("Tributa��o Cupom Fiscal")
	@OneToMany(mappedBy="material")
	public Set<Materialimposto> getListaMaterialimposto() {
		return listaMaterialimposto;
	}
	
	public void setListaMaterialpneumodelo(
			List<Materialpneumodelo> listaMaterialpneumodelo) {
		this.listaMaterialpneumodelo = listaMaterialpneumodelo;
	}
	
	public void setListaMaterialFaixaMarkup(
			List<MaterialFaixaMarkup> listaMaterialFaixaMarkup) {
		this.listaMaterialFaixaMarkup = listaMaterialFaixaMarkup;
	}
	
	public void setListaMaterialimposto(
			Set<Materialimposto> listaMaterialimposto) {
		this.listaMaterialimposto = listaMaterialimposto;
	}
	
	public void setListaMaterialhistorico(
			List<Materialhistorico> listaMaterialhistorico) {
		this.listaMaterialhistorico = listaMaterialhistorico;
	}
	public void setListaMaterialrelacionado(
			Set<Materialrelacionado> listaMaterialrelacionado) {
		this.listaMaterialrelacionado = listaMaterialrelacionado;
	}
	public void setListaMaterialkitflexivel(
			Set<Materialkitflexivel> listaMaterialkitflexivel) {
		this.listaMaterialkitflexivel = listaMaterialkitflexivel;
	}
	public void setListaMaterialformulapeso(
			List<Materialformulapeso> listaMaterialformulapeso) {
		this.listaMaterialformulapeso = listaMaterialformulapeso;
	}
	public void setListaMaterialCustoEmpresa(
			Set<MaterialCustoEmpresa> listaMaterialCustoEmpresa) {
		this.listaMaterialCustoEmpresa = listaMaterialCustoEmpresa;
	}
	public void setListaMaterialsimilar(Set<Materialsimilar> listaMaterialsimilar) {
		this.listaMaterialsimilar = listaMaterialsimilar;
	}
	public void setListaMaterialvenda(Set<Materialvenda> listaMaterialvenda) {
		this.listaMaterialvenda = listaMaterialvenda;
	}
	
	public void setListaMaterialcolaborador(
			Set<Materialcolaborador> listaMaterialcolaborador) {
		this.listaMaterialcolaborador = listaMaterialcolaborador;
	}
	public void setListaMaterialformulacusto(List<Materialformulacusto> listaMaterialformulacusto) {
		this.listaMaterialformulacusto = listaMaterialformulacusto;
	}
	public void setListaMaterialformulavendaminimo(List<Materialformulavendaminimo> listaMaterialformulavendaminimo) {
		this.listaMaterialformulavendaminimo = listaMaterialformulavendaminimo;
	}
	public void setListaMaterialformulavendamaximo(List<Materialformulavendamaximo> listaMaterialformulavendamaximo) {
		this.listaMaterialformulavendamaximo = listaMaterialformulavendamaximo;
	}
	public void setListaMaterialformulamargemcontribuicao(List<Materialformulamargemcontribuicao> listaMaterialformulamargemcontribuicao) {
		this.listaMaterialformulamargemcontribuicao = listaMaterialformulamargemcontribuicao;
	}
	public void setListaMateriallocalarmazenagem(List<Materiallocalarmazenagem> listaMateriallocalarmazenagem) {
		this.listaMateriallocalarmazenagem = listaMateriallocalarmazenagem;
	}
	
	@DisplayName("Caracter�stica")
	@OneToMany(mappedBy="material")
	public Set<Materialcaracteristica> getListaCaracteristica() {
		return listaCaracteristica;
	}
	
	@DisplayName("Fornecedor")
	@OneToMany(mappedBy="material")
	public Set<Materialfornecedor> getListaFornecedor() {
		return listaFornecedor;
	}

	@DisplayName("Mat�rias-primas")
	@OneToMany(mappedBy="materialmestre")
	public Set<Materialproducao> getListaProducao() {
		return listaProducao;
	}
	
	@DisplayName("Empresa")
	@OneToMany(mappedBy="material")
	public Set<Materialempresa> getListaMaterialempresa() {
		return listaMaterialempresa;
	}
	
	@DisplayName("Item inspe��o")
	@OneToMany(mappedBy="material")
	public Set<Materialinspecaoitem> getListaMaterialinspecaoitem() {
		return listaMaterialinspecaoitem;
	}

	public void setListaMaterialinspecaoitem(
			Set<Materialinspecaoitem> listaMaterialinspecaoitem) {
		this.listaMaterialinspecaoitem = listaMaterialinspecaoitem;
	}

	@OneToMany(mappedBy="material")
	public Set<Tarefarecursogeral> getListaTarefarecursogeral() {
		return listaTarefarecursogeral;
	}
	@OneToMany(mappedBy="material")
	public List<Planejamentorecursogeral> getListaPlanejamentorecursogeral() {
		return listaPlanejamentorecursogeral;
	}
	
	public void setListaMaterialempresa(
			Set<Materialempresa> listaMaterialempresa) {
		this.listaMaterialempresa = listaMaterialempresa;
	}
	
	public void setListaFornecedor(Set<Materialfornecedor> listaFornecedor) {
		this.listaFornecedor = listaFornecedor;
	}
	
	public void setListaProducao(Set<Materialproducao> listaProducao) {
		this.listaProducao = listaProducao;
	}
	
	public void setListaCaracteristica(
			Set<Materialcaracteristica> listaCaracteristica) {
		this.listaCaracteristica = listaCaracteristica;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	@DisplayName("Imagem")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public Boolean getProduto() {
		return produto;
	}

	public Boolean getPatrimonio() {
		return patrimonio;
	}

	public Boolean getServico() {
		return servico;
	}

	public Boolean getEpi() {
		return epi;
	}
	
	@DisplayName("C�DIGO ANVISA")
	public String getCodigoAnvisa() {
		return codigoAnvisa;
	}
	
	@DisplayName("ISENTO")
	public Boolean getIsento() {
		return isento;
	}
	
	@MaxLength(255)
	@DisplayName("C�DIGO ISEN��O")
	public String getCodigoIsencao() {
		return codigoIsencao;
	}
	
	public void setCodigoIsencao(String codigoIsencao) {
		this.codigoIsencao = codigoIsencao;
	}
	
	public void setIsento(Boolean isento) {
		this.isento = isento;
	}
	
	public void setCodigoAnvisa(String codigoAnvisa) {
		this.codigoAnvisa = codigoAnvisa;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setProduto(Boolean produto) {
		this.produto = produto;
	}

	public void setPatrimonio(Boolean patrimonio) {
		this.patrimonio = patrimonio;
	}

	public void setServico(Boolean servico) {
		this.servico = servico;
	}

	public void setEpi(Boolean epi) {
		this.epi = epi;
	}
	public void setListaTarefarecursogeral(Set<Tarefarecursogeral> listaTarefarecursogeral) {
		this.listaTarefarecursogeral = listaTarefarecursogeral;
	}
	public void setListaPlanejamentorecursogeral(
			List<Planejamentorecursogeral> listaPlanejamentorecursogeral) {
		this.listaPlanejamentorecursogeral = listaPlanejamentorecursogeral;
	}
	public void setContagerencialvenda(Contagerencial contagerencialvenda) {
		this.contagerencialvenda = contagerencialvenda;
	}
	public void setContaContabil(ContaContabil contaContabil) {
		this.contaContabil = contaContabil;
	}
//	#########################################################
//	#################	TRANSIENTES	   ######################
//	#########################################################
	
	@Transient
	@DisplayName("Empresa")
	public List<Empresa> getListaEmpresa() {
		return listaEmpresa;
	}
	
	@Transient
	public Money getValorUltimaOrdemCompra() {
		return valorUltimaOrdemCompra;
	}
	
	public void setListaEmpresa(List<Empresa> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}
	
	@Transient
	public String getCaracteristicas() {
		if(this.getListaCaracteristica() != null && this.getListaCaracteristica().size() > 0){
			StringBuilder caracteristica = new StringBuilder();
			for (Materialcaracteristica materialcaracteristica  : this.getListaCaracteristica()) {
				caracteristica.append("* " + materialcaracteristica.getNome());
				caracteristica.append(materialcaracteristica.getValor() != null ? " - "+materialcaracteristica.getValor()+"\n" : "\n");
			}
			if(caracteristica.length() > 0) caracteristica.delete(caracteristica.length()-1, caracteristica.length());
			return caracteristica.toString();
		}
		
		return "";
	}
	
	@Transient
	public String getCaracteristicaMaterial(){
		if(this.getListaCaracteristica() != null && this.getListaCaracteristica().size() > 0){
			StringBuilder caracteristica = new StringBuilder();
			for (Materialcaracteristica materialcaracteristica  : this.getListaCaracteristica()) {
				caracteristica.append(" " + materialcaracteristica.getNome());
				caracteristica.append(materialcaracteristica.getValor() != null ? " - "+materialcaracteristica.getValor()+"\n" : "\n");
			}
			if(caracteristica.length() > 0) caracteristica.delete(caracteristica.length()-1, caracteristica.length());
			return caracteristica.toString();
		}
		
		return "";
	}
	
	@Transient
	public Integer getDisponivel() {
		return disponivel;
	}
	
	@Transient
	@Required
	@DisplayName("N�mero de c�pias")
	public Integer getNumerocopiasimpressaoetiqueta() {
		return numerocopiasimpressaoetiqueta;
	}
	
	public void setDisponivel(Integer disponivel) {
		this.disponivel = disponivel;
	}
	
	public void setValorUltimaOrdemCompra(Money valorUltimaOrdemCompra) {
		this.valorUltimaOrdemCompra = valorUltimaOrdemCompra;
	}
	@Transient
	public Double getFracao() {
		return fracao;
	}

	public void setFracao(Double fracao) {
		this.fracao = fracao;
	}
	@Transient
	public Double getValorunitario() {
		return valorunitario;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	
	public void setNumerocopiasimpressaoetiqueta(
			Integer numerocopiasimpressaoetiqueta) {
		this.numerocopiasimpressaoetiqueta = numerocopiasimpressaoetiqueta;
	}
	
//	#########################################################
//	####################### PRODUTO #########################
//	#########################################################
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial", insertable=false, updatable=false)
	public Produto getMaterialproduto() {
		return materialproduto;
	}

	@Transient
	@DisplayName("Nome reduzido")
	@MaxLength(40)
	public String getProduto_nomereduzido() {
		return produto_nomereduzido;
	}

	@Transient
	@DisplayName("Qtde. M�nima")
	public Integer getProduto_qtdmin() {
		return produto_qtdmin;
	}

	@Transient
	public Boolean getProduto_gerar() {
		return produto_gerar;
	}

	@Transient
	@MaxLength(250)
	public String getProduto_anotacoes() {
		return produto_anotacoes;
	}

	@Transient
	@DisplayName("Largura (mm)")
	@MinValue(0)
	public Double getProduto_largura() {
		return produto_largura;
	}
	
	@Transient
	@DisplayName("Comprimento (mm)")
	@MinValue(0)
	public Double getProduto_comprimento() {
		return produto_comprimento;
	}
	
	@Transient
	@DisplayName("Altura (mm)")
	@MinValue(0)
	public Double getProduto_altura() {
		return produto_altura;
	}
	@Transient
	@Required
	@DisplayName("Material para corte")
	public Boolean getProduto_materialcorte() {
		return produto_materialcorte;
	}
	@Transient
	@DisplayName("Convers�o para Venda")
	public Double getProduto_fatorconversao() {
		return produto_fatorconversao;
	}
	@Transient
	@DisplayName("Arredondamento comprimento")
	public Boolean getProduto_arredondamentocomprimento() {
		return produto_arredondamentocomprimento;
	}
	@Transient
	@DisplayName("Margem de Arredondamento (mm)")
	public Double getProduto_margemarredondamento() {
		return produto_margemarredondamento;
	}	
	@Transient
	@DisplayName("Convers�o para Produ��o")
	public Double getProduto_fatorconversaoproducao() {
		return produto_fatorconversaoproducao;
	}

	public void setProduto_altura(Double produtoAltura) {
		produto_altura = produtoAltura;
	}

	public void setProduto_materialcorte(Boolean produtoMaterialcorte) {
		produto_materialcorte = produtoMaterialcorte;
	}

	public void setMaterialproduto(Produto materialproduto) {
		this.materialproduto = materialproduto;
	}
	
	public void setProduto_nomereduzido(String produto_nomereduzido) {
		this.produto_nomereduzido = produto_nomereduzido;
	}

	public void setProduto_qtdmin(Integer produto_qtdmin) {
		this.produto_qtdmin = produto_qtdmin;
	}

	public void setProduto_gerar(Boolean produto_gerar) {
		this.produto_gerar = produto_gerar;
	}

	public void setProduto_anotacoes(String produto_anotacoes) {
		this.produto_anotacoes = produto_anotacoes;
	}
	
	public void setProduto_largura(Double produto_largura) {
		this.produto_largura = produto_largura;
	}
	
	public void setProduto_comprimento(Double produto_comprimento) {
		this.produto_comprimento = produto_comprimento;
	}
	
	public void setProduto_fatorconversao(Double produtoFatorconversao) {
		produto_fatorconversao = produtoFatorconversao;
	}
	
	public void setProduto_arredondamentocomprimento(Boolean produtoArredondamentocomprimento) {
		produto_arredondamentocomprimento = produtoArredondamentocomprimento;
	}
	
	public void setProduto_margemarredondamento(Double produtoMargemarredondamento) {
		produto_margemarredondamento = produtoMargemarredondamento;
	}

	public void setProduto_fatorconversaoproducao(Double produtoFatorconversaoproducao) {
		produto_fatorconversaoproducao = produtoFatorconversaoproducao;
	}
	
	public void setProdutoObj(Produto produto) {
		setProduto_nomereduzido(produto.getNomereduzido()); 
		setProduto_qtdmin(produto.getQtdeminima());
		setProduto_gerar(null);
		setProduto_anotacoes(produto.getObservacao());
		setProduto_largura(produto.getLargura());
		setProduto_comprimento(produto.getComprimento());
		setProduto_altura(produto.getAltura());
		setProduto_materialcorte(produto.getMaterialcorte());
		setProduto_arredondamentocomprimento(produto.getArredondamentocomprimento());
		setProduto_fatorconversao(produto.getFatorconversao());
		setProduto_margemarredondamento(produto.getMargemarredondamento());
		setProduto_fatorconversaoproducao(produto.getFatorconversaoproducao());
	}
	
//	#########################################################
//	####################### PATRIM�NIO ######################
//	#########################################################
	
	@Transient
	@DisplayName("Nome reduzido")
	@MaxLength(40)
	public String getPatrimonio_nomereduzido() {
		return patrimonio_nomereduzido;
	}
	
	@Transient
	@DisplayName("Valor")
	public Money getPatrimonio_valor() {
		return patrimonio_valor;
	}
	
	@Transient
	@DisplayName("Valor mercado")
	public Money getPatrimonio_valormercado() {
		return patrimonio_valormercado;
	}
	
	@Transient
	@DisplayName("Valor refer�ncia")
	public Money getPatrimonio_valorref() {
		return patrimonio_valorref;
	}
	
	@Transient
	@DisplayName("Vida �til")
	public Integer getPatrimonio_vida() {
		return patrimonio_vida;
	}
	
	@Transient
	@DisplayName("Deprecia��o")
	public Money getPatrimonio_depreciacao() {
		return patrimonio_depreciacao;
	}
	
	@Transient
	@MaxLength(250)
	public String getPatrimonio_anotacoes() {
		return patrimonio_anotacoes;
	}
	
	@Transient
	public Integer getCdmaterialtrans() {
		return cdmaterialtrans;
	}
	
	@Transient
	public Integer getCdmaterialproducao() {
		return cdmaterialproducao;
	}
	@Transient	
	public Integer getCdmaterialrelacionado() {
		return cdmaterialrelacionado;
	}
	@Transient
	public Integer getCdmaterialkitflexivel() {
		return cdmaterialkitflexivel;
	}

	public void setCdmaterialkitflexivel(Integer cdmaterialkitflexivel) {
		this.cdmaterialkitflexivel = cdmaterialkitflexivel;
	}

	public void setCdmaterialrelacionado(Integer cdmaterialrelacionado) {
		this.cdmaterialrelacionado = cdmaterialrelacionado;
	}
	public void setCdmaterialproducao(Integer cdmaterialproducao) {
		this.cdmaterialproducao = cdmaterialproducao;
	}

	public void setPatrimonio_nomereduzido(String patrimonio_nomereduzido) {
		this.patrimonio_nomereduzido = patrimonio_nomereduzido;
	}

	public void setPatrimonio_valor(Money patrimonio_valor) {
		this.patrimonio_valor = patrimonio_valor;
	}

	public void setPatrimonio_valormercado(Money patrimonio_valormercado) {
		this.patrimonio_valormercado = patrimonio_valormercado;
	}

	public void setPatrimonio_valorref(Money patrimonio_valorref) {
		this.patrimonio_valorref = patrimonio_valorref;
	}

	public void setPatrimonio_vida(Integer patrimonio_vida) {
		this.patrimonio_vida = patrimonio_vida;
	}

	public void setPatrimonio_depreciacao(Money patrimonio_depreciacao) {
		this.patrimonio_depreciacao = patrimonio_depreciacao;
	}

	public void setPatrimonio_anotacoes(String patrimonio_anotacoes) {
		this.patrimonio_anotacoes = patrimonio_anotacoes;
	}
	
	public void setCdmaterialtrans(Integer cdmaterialtrans) {
		this.cdmaterialtrans = cdmaterialtrans;
	}
	
	public void setPatrimonioObj(Bempatrimonio bem){
		setPatrimonio_nomereduzido(bem.getNomereduzido());
		setPatrimonio_valor(bem.getValor());
		setPatrimonio_valormercado(bem.getValormercado());
		setPatrimonio_valorref(bem.getValorreferencia());
		setPatrimonio_vida(bem.getVidautil());
		setPatrimonio_depreciacao(bem.getDepreciacao());
		setPatrimonio_anotacoes(bem.getObservacao());
	}
	
//	#########################################################
//	####################### EPI #############################
//	#########################################################
	
	@Transient
	@DisplayName("Nome reduzido")
	@MaxLength(40)
	public String getEpi_nomereduzido() {
		return epi_nomereduzido;
	}
	
	@Transient
	@MaxLength(250)
	public String getEpi_anotacoes() {
		return epi_anotacoes;
	}
	@Transient
	@MaxLength(10)
	@DisplayName("CA")
	public Integer getEpi_caepi() {
		return epi_caepi;
	}
	public void setEpi_caepi(Integer epi_caepi) {
		this.epi_caepi = epi_caepi;
	}
	
	@Transient
	@DisplayName("Dura��o (Semanas)")
	public Integer getEpi_duracao() {
		return epi_duracao;
	}
	
	public void setEpi_duracao(Integer epi_duracao) {
		this.epi_duracao = epi_duracao;
	}

	public void setEpi_nomereduzido(String epi_nomereduzido) {
		this.epi_nomereduzido = epi_nomereduzido;
	}

	public void setEpi_anotacoes(String epi_anotacoes) {
		this.epi_anotacoes = epi_anotacoes;
	}
	
	public void setEpiObj(Epi epi){
		setEpi_nomereduzido(epi.getNomereduzido());
		setEpi_duracao(epi.getDuracao());
		setEpi_anotacoes(epi.getObservacao());
		setEpi_caepi(epi.getCaepi());
	}
	
//	#########################################################
//	####################### SERVI�O #########################
//	#########################################################
	
	@Transient
	@DisplayName("Nome reduzido")
	@MaxLength(40)
	public String getServico_nomereduzido() {
		return servico_nomereduzido;
	}

	@Transient
	@DisplayName("Qtd. m�nima")
	public Integer getServico_qtdmin() {
		return servico_qtdmin;
	}
	@Transient
	public Boolean getServico_gerar() {
		return servico_gerar;
	}

	@Transient
	@MaxLength(250)
	public String getServico_anotacoes() {
		return servico_anotacoes;
	}
	
	public void setServico_nomereduzido(String servico_nomereduzido) {
		this.servico_nomereduzido = servico_nomereduzido;
	}

	public void setServico_qtdmin(Integer servico_qtdmin) {
		this.servico_qtdmin = servico_qtdmin;
	}

	public void setServico_gerar(Boolean servico_gerar) {
		this.servico_gerar = servico_gerar;
	}

	public void setServico_anotacoes(String servico_anotacoes) {
		this.servico_anotacoes = servico_anotacoes;
	}
	
	public void setServicoObj(Servico servico){
		setServico_nomereduzido(servico.getNomereduzido());
		setServico_qtdmin(servico.getQtdeminima());
		setServico_anotacoes(servico.getObservacao());
	}
	
	@Transient
	public Integer getCdmaterialclasse() {
		return cdmaterialclasse;
	}
	
	public void setCdmaterialclasse(Integer cdmaterialclasse) {
		this.cdmaterialclasse = cdmaterialclasse;
	}
	
	@Transient
	public String getClassestring() {
		String string = "";
		if(epi != null && epi){
			string += "EPI<BR>";
		}
		if(patrimonio != null && patrimonio){
			string += "Patrim�nio<BR>";
		}
		if(produto != null && produto){
			string += "Produto<BR>";
		}
		if(servico != null && servico){
			string += "Servi�o<BR>";
		}
		return string;
	}
	
	public void setClassestring(String classestring) {
		this.classestring = classestring;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdmaterial == null) ? 0 : cdmaterial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Material) {
			Material material = (Material) obj;
			return material.getCdmaterial().equals(this.getCdmaterial());
		}
		return super.equals(obj);
	}

	public String subQueryFornecedorEmpresa() {
		return "select materialSubQueryFornecedorEmpresa.cdmaterial " +
				"from Material materialSubQueryFornecedorEmpresa " +
				"left outer join materialSubQueryFornecedorEmpresa.listaFornecedor listaFornecedorSubQueryFornecedorEmpresa " +
				"left outer join listaFornecedorSubQueryFornecedorEmpresa.fornecedor fornecedorSubQueryFornecedorEmpresa " +
				"where true = permissao_fornecedorempresa(fornecedorSubQueryFornecedorEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	@Transient
	public Image getImage() {
		return image;
	}
	
	public void setImage(Image image) {
		this.image = image;
	}

	@MaxLength(9)
	@DisplayName("Qtde. Refer�ncia")
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
		
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}

	@Transient
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	@Transient
	public java.util.Date getDtUltimaCompra() {
		return dtUltimaCompra;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public void setDtUltimaCompra(java.util.Date dtUltimaCompra) {
		this.dtUltimaCompra = dtUltimaCompra;
	}

	@OneToMany(mappedBy="servico")
	public Set<Contratomaterial> getListaContratomaterial() {
		return listaContratomaterial;
	}

	public void setListaContratomaterial(Set<Contratomaterial> listaContratomaterial) {
		this.listaContratomaterial = listaContratomaterial;
	}
	
	@Transient
	public String getUnidademedidaString() {
		return unidademedidaString;
	}
	public void setUnidademedidaString(String unidademedidaString) {
		this.unidademedidaString = unidademedidaString;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"identificacao", "nome"})
	public String getAutocompleteDescription(){
		if(this.autocompleteDescription != null) {
			return this.autocompleteDescription;
		}
		return this.getIdentificacaoListagem() != null && !this.getIdentificacaoListagem().equals("") ? this.getIdentificacaoListagem() + " - " + this.nome : this.nome;		
	}
	
	public void setAutocompleteDescription(String autocompleteDescription) {
		this.autocompleteDescription = autocompleteDescription;
	}
	
	@Transient
	public String getAutocompleteDescriptionTruncate(){
		String str = getAutocompleteDescription();
		return str != null && str.length() > 60 ? str.substring(0, 60) + " ..." : str;		
	}
	
	@Transient
	public Boolean getBloqueado() {
		return bloqueado;
	}
	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	@Transient
	public Integer getCdcontratomaterial() {
		return cdcontratomaterial;
	}
	public void setCdcontratomaterial(Integer cdcontratomaterial) {
		this.cdcontratomaterial = cdcontratomaterial;
	}
	@Transient
	public Integer getCdrestricaocontratomaterial() {
		return cdrestricaocontratomaterial;
	}
	public void setCdrestricaocontratomaterial(Integer cdrestricaocontratomaterial) {
		this.cdrestricaocontratomaterial = cdrestricaocontratomaterial;
	}
	@Transient
	public Contrato getContrato() {
		return contrato;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	@Transient
	public String getNomescape() {
		return nomescape;
	}
	public void setNomescape(String nomescape) {
		this.nomescape = nomescape;
	}

	@MaxLength(20)
	@DisplayName("Identificador")
	public String getIdentificacao() {
		return identificacao;
	}
	@Transient
	@DisplayName("Identificador")
	public String getIdentificacaoListagem() {
		String identificacaoListagem = "";
		if(getMaterialmestregrade() != null && getMaterialmestregrade().getIdentificacao() != null && 
				!"".equals(getMaterialmestregrade().getIdentificacao())){
			identificacaoListagem = getMaterialmestregrade().getIdentificacao();
		}
		if(getIdentificacao() != null && !"".equals(getIdentificacao())){
			if(identificacaoListagem != null && !"".equals(identificacaoListagem)) 
				identificacaoListagem += ".";
			
			identificacaoListagem += getIdentificacao();
		}
		
		return identificacaoListagem;
	}

	@MaxLength(20)
	@DisplayName("Refer�ncia")
	public String getReferencia() {
		return referencia;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	@Transient
	public String getCodigoalternativo() {
		int count = 0;
		StringBuilder codigos = new StringBuilder("");
		if(this.listaFornecedor != null && !this.listaFornecedor.isEmpty())
			for (Materialfornecedor materialfornecedor : this.listaFornecedor) 
				if(materialfornecedor.getCodigo() != null && !materialfornecedor.getCodigo().equals("")){
					count++;
					
					if(count == 5) return "Diversos";
					codigos.append(materialfornecedor.getCodigo()).append(", ");
				}
		
		return !codigos.toString().equals("") ? codigos.delete(codigos.length()-2, codigos.length()).toString() : "";
	}
	
	@Transient
	public Endereco getEnderecoTributacaoOrigem() {
		return enderecoTributacaoOrigem;
	}
	@Transient
	public Endereco getEnderecoTributacaoDestino() {
		return enderecoTributacaoDestino;
	}
	@Transient
	public Empresa getEmpresaTributacaoOrigem() {
		return empresaTributacaoOrigem;
	}
	@Transient
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	@Transient
	public Operacaonfe getOperacaonfe() {
		return operacaonfe;
	}

	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setEnderecoTributacaoDestino(Endereco enderecoTributacaoDestino) {
		this.enderecoTributacaoDestino = enderecoTributacaoDestino;
	}
	public void setEnderecoTributacaoOrigem(Endereco enderecoTributacaoOrigem) {
		this.enderecoTributacaoOrigem = enderecoTributacaoOrigem;
	}
	public void setEmpresaTributacaoOrigem(Empresa empresaTributacaoOrigem) {
		this.empresaTributacaoOrigem = empresaTributacaoOrigem;
	}
	public void setOperacaonfe(Operacaonfe operacaonfe) {
		this.operacaonfe = operacaonfe;
	}
	
	public void setCodigoalternativo(String codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}
	
	@DisplayName("Localiza��o Estoque")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalizacaoestoque")
	public Localizacaoestoque getLocalizacaoestoque() {
		return localizacaoestoque;
	}
	public void setLocalizacaoestoque(Localizacaoestoque localizacaoestoque) {
		this.localizacaoestoque = localizacaoestoque;
	}

	@Transient
	public String getMaterialComLarguraEComprimento(){
		String retorno = getAutocompleteDescription();
		if (this.getProduto_largura() != null){
			retorno += " - " + this.getProduto_largura();
		}
		if (this.getProduto_comprimento() != null){
			retorno += " x " + this.getProduto_comprimento();
		}
		return retorno;
		
	}
	
	@Transient
	public String getDescriptionAutocompelteVenda() {
		StringBuilder s = new StringBuilder();
		
		s.append(nome)
		 .append("<span><BR>")
		 .append("C�digo: " + cdmaterial + "<BR>");
		
		if(getIdentificacaoListagem() != null && !"".equals(getIdentificacaoListagem())){
			s.append("Identifica��o: " + getIdentificacaoListagem() + "<BR>");
		}
		
		s.append(materialgrupo != null ? "Grupo: " +  materialgrupo.nome + "<BR>" : "");
		
		if(materialtipo != null)
			s.append("Tipo: " + materialtipo.nome + "<BR>");
		
		if (valorvenda!=null && valorvenda>0){
			s.append("Valor de venda: R$ " + new DecimalFormat("#,##0.00").format(valorvenda));
		}
		
		s.append("</span>");
		
		return s.toString();
	}
	
	@Transient
	@DisplayName("Identificador")
	public String getDescriptionWithIdentificacaomestre() {
		String descricao = "";
		if(getMaterialmestregrade() != null && getMaterialmestregrade().getIdentificacao() != null && 
				!"".equals(getMaterialmestregrade().getIdentificacao())){
			descricao = getMaterialmestregrade().getIdentificacao();
		}
		if(getIdentificacao() != null && !"".equals(getIdentificacao())){
			if(descricao != null && !"".equals(descricao)) 
				descricao += ".";
			
			descricao += getIdentificacao();
		}
		if(getNome() != null){
			descricao += (!"".equals(descricao) ? " - " : "") + getNome(); 
		}
		
		return descricao;
	}

	@Transient
	public String getAutocompleteDescriptionMovimentacaoEstoque(){
		StringBuilder description = new StringBuilder();
		description.append(getIdentificacaoListagem() != null && !getIdentificacaoListagem().equals("") ? getIdentificacaoListagem() + " - " + this.nome : this.nome);
		description.append(this.codigobarras != null && !this.codigobarras.equals("") ? " - " + this.codigobarras : ""); 
		return description.toString(); 		
	}
	
//	@DisplayName("Outros(R$)")
//	public Money getValorlucro() {
//		return valorlucro;
//	}
	@DisplayName("Frete(R$)")
	public Money getValorfrete() {
		return valorfrete;
	}
	@DisplayName("Separar o frete na nota")
	public Boolean getFreteseparadonota() {
		return freteseparadonota;
	}

//	public void setValorlucro(Money valorlucro) {
//		this.valorlucro = valorlucro;
//	}

	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}

	public void setFreteseparadonota(Boolean freteseparadonota) {
		this.freteseparadonota = freteseparadonota;
	}
	
	@Transient
	public String getDescriptionAutocomplete(){
		return this.nome;		
	}

	@Transient
	public Money getAliquotaicms() {
		return aliquotaicms;
	}

	@Transient
	public Money getAliquotaicmsst() {
		return aliquotaicmsst;
	}

	@Transient
	public Money getAliquotaipi() {
		return aliquotaipi;
	}

	@Transient
	public Money getAliquotapis() {
		return aliquotapis;
	}

	@Transient
	public Money getAliquotacofins() {
		return aliquotacofins;
	}
	
	@Transient
	public Money getAliquotaiss() {
		return aliquotaiss;
	}

	@Transient
	public Double getAliquotamva() {
		return aliquotamva;
	}

	@Transient
	public Money getAliquotacsll() {
		return aliquotacsll;
	}

	@Transient
	public Money getAliquotair() {
		return aliquotair;
	}
	
	@Transient
	public Money getAliquotainss() {
		return aliquotainss;
	}
	@Transient
	public Money getAliquotaFCP() {
		return aliquotaFCP;
	}
	@Transient
	public Money getAliquotaFCPST() {
		return aliquotaFCPST;
	}

	public void setAliquotacsll(Money aliquotacsll) {
		this.aliquotacsll = aliquotacsll;
	}

	public void setAliquotair(Money aliquotair) {
		this.aliquotair = aliquotair;
	}

	public void setAliquotainss(Money aliquotainss) {
		this.aliquotainss = aliquotainss;
	}

	public void setAliquotaicms(Money aliquotaicms) {
		this.aliquotaicms = aliquotaicms;
	}

	public void setAliquotaicmsst(Money aliquotaicmsst) {
		this.aliquotaicmsst = aliquotaicmsst;
	}

	public void setAliquotaipi(Money aliquotaipi) {
		this.aliquotaipi = aliquotaipi;
	}

	public void setAliquotapis(Money aliquotapis) {
		this.aliquotapis = aliquotapis;
	}

	public void setAliquotacofins(Money aliquotacofins) {
		this.aliquotacofins = aliquotacofins;
	}

	public void setAliquotaiss(Money aliquotaiss) {
		this.aliquotaiss = aliquotaiss;
	}

	public void setAliquotamva(Double aliquotamva) {
		this.aliquotamva = aliquotamva;
	}
	
	public void setAliquotaFCP(Money aliquotaFCP) {
		this.aliquotaFCP = aliquotaFCP;
	}
	
	public void setAliquotaFCPST(Money aliquotaFCPST) {
		this.aliquotaFCPST = aliquotaFCPST;
	}
	
	@Transient
	public Double getPercentualBcicms() {
		return percentualBcicms;
	}
	@Transient
	public Double getPercentualBcipi() {
		return percentualBcipi;
	}
	@Transient
	public Double getPercentualBcpis() {
		return percentualBcpis;
	}
	@Transient
	public Double getPercentualBccofins() {
		return percentualBccofins;
	}
	@Transient
	public Double getPercentualBciss() {
		return percentualBciss;
	}
	@Transient
	public Double getPercentualBccsll() {
		return percentualBccsll;
	}
	@Transient
	public Double getPercentualBcir() {
		return percentualBcir;
	}
	@Transient
	public Double getPercentualBcinss() {
		return percentualBcinss;
	}
	@Transient
	public Double getPercentualBcfcp() {
		return percentualBcfcp;
	}
	@Transient
	public Double getPercentualBcfcpst() {
		return percentualBcfcpst;
	}
	
	@Transient
	public Retencao getRetencaoIpi() {
		return retencaoIpi;
	}

	public void setRetencaoIpi(Retencao retencaoIpi) {
		this.retencaoIpi = retencaoIpi;
	}

	public void setPercentualBcicms(Double percentualBcicms) {
		this.percentualBcicms = percentualBcicms;
	}

	public void setPercentualBcipi(Double percentualBcipi) {
		this.percentualBcipi = percentualBcipi;
	}

	public void setPercentualBcpis(Double percentualBcpis) {
		this.percentualBcpis = percentualBcpis;
	}

	public void setPercentualBccofins(Double percentualBccofins) {
		this.percentualBccofins = percentualBccofins;
	}

	public void setPercentualBciss(Double percentualBciss) {
		this.percentualBciss = percentualBciss;
	}
	
	public void setPercentualBccsll(Double percentualBccsll) {
		this.percentualBccsll = percentualBccsll;
	}

	public void setPercentualBcir(Double percentualBcir) {
		this.percentualBcir = percentualBcir;
	}

	public void setPercentualBcinss(Double percentualBcinss) {
		this.percentualBcinss = percentualBcinss;
	}
	
	public void setPercentualBcfcp(Double percentualBcfcp) {
		this.percentualBcfcp = percentualBcfcp;
	}
	
	public void setPercentualBcfcpst(Double percentualBcfcpst) {
		this.percentualBcfcpst = percentualBcfcpst;
	}

	@DisplayName("Convers�o de Unidades")
	@OneToMany(mappedBy="material")
	public Set<Materialunidademedida> getListaMaterialunidademedida() {
		return listaMaterialunidademedida;
	}

	public void setListaMaterialunidademedida(Set<Materialunidademedida> listaMaterialunidademedida) {
		this.listaMaterialunidademedida = listaMaterialunidademedida;
	}
	@DisplayName("N�meros de S�rie ")
	@OneToMany(mappedBy="material")
	public Set<Materialnumeroserie> getListaMaterialnumeroserie() {
		return listaMaterialnumeroserie;
	}

	public void setListaMaterialnumeroserie(Set<Materialnumeroserie> listaMaterialnumeroserie) {
		this.listaMaterialnumeroserie = listaMaterialnumeroserie;
	}
	
	@Transient
	public String getAutocompleteDescriptionNomenf(){
		return this.identificacao != null && !this.identificacao.equals("") ? this.identificacao + " - " + 
				(this.nomenf != null && !"".equals(this.nomenf) ? this.nomenf : this.nome) : (this.nomenf != null && !"".equals(this.nomenf) ?
						this.nomenf : this.nome);		
	}
	
	@Transient
	public String getAutocompleteDescriptionNomealternativoOuNomenf(){
		String s = StringUtils.isNotEmpty(this.identificacao) ? this.identificacao + " - " : "";
		
		if(StringUtils.isNotEmpty(this.nomealternativo)){
			s += this.nomealternativo;
		}else if(StringUtils.isNotEmpty(this.nomenf)){
			s += this.nomenf;
		}else if(StringUtils.isNotEmpty(this.nome)){
			s += this.nome;
		}
		return s;
	}

	@Transient
	public String getNomenfdescricaonota() {
		if (nomenfdescricaonota != null && !nomenfdescricaonota.isEmpty()){
			return nomenfdescricaonota;
		} else {
			String nomedescricaonota = "";
			if (this.identificacao != null && !this.identificacao.equals("")) {
				nomedescricaonota = this.identificacao;
			} else if(this.cdmaterial != null){
				nomedescricaonota = this.cdmaterial.toString();
			}
			
			nomedescricaonota += " - ";
			
			if (this.nomenf != null && !"".equals(this.nomenf)) {
				nomedescricaonota += this.nomenf;
			} else {
				nomedescricaonota += this.nome;
			}
					
			if(this.getAlturaLarguraComprimentoConcatenados() != null && !"".equals(this.getAlturaLarguraComprimentoConcatenados())){
				nomedescricaonota += " " +  this.getAlturaLarguraComprimentoConcatenados();
			}
			
			return nomedescricaonota;
		}
	}

	public void setNomenfdescricaonota(String nomenfdescricaonota) {
		this.nomenfdescricaonota = nomenfdescricaonota;
	}
	
	@Transient
	public String getNomenfOuNome() {
		return this.nomenf != null && !"".equals(this.nomenf) ? this.nomenf : this.nome;
	}
	
	@Transient
	public String getIdentificacaoOuCdmaterial(){
		String s = "";
		if(this.identificacao != null && !"".equals(this.identificacao)){
			s = this.identificacao;
		}else if(this.cdmaterial != null){
			s = this.cdmaterial.toString();
		}		
		return s;
	}

	@DisplayName("N�o exibir M�nimo/M�ximo na Venda")
	public Boolean getNaoexibirminmaxvenda() {
		return naoexibirminmaxvenda;
	}
	public void setNaoexibirminmaxvenda(Boolean naoexibirminmaxvenda) {
		this.naoexibirminmaxvenda = naoexibirminmaxvenda;
	}

	@Transient
	public Double getQtdenfp() {
		return qtdenfp;
	}
	public void setQtdenfp(Double qtdenfp) {
		this.qtdenfp = qtdenfp;
	}	
	
	@Transient
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}

	@Transient
	public String getAlturaLarguraComprimentoConcatenados() {
		return alturaLarguraComprimentoConcatenados;
	}

	public void setAlturaLarguraComprimentoConcatenados(String alturaLarguraComprimentoConcatenados) {
		this.alturaLarguraComprimentoConcatenados = alturaLarguraComprimentoConcatenados;
	}
	
	@Transient
	public Boolean getIrListagemDireto() {
		return irListagemDireto;
	}
	
	public void setIrListagemDireto(Boolean irListagemDireto) {
		this.irListagemDireto = irListagemDireto;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Material material = new Material();
		
		material.setCdmaterial(this.getCdmaterial());
		material.setNome(this.getNome());
		material.setIdentificacao(this.getIdentificacao());
		material.setProducao(this.getProducao());
		material.setProduto(this.getProduto());
		material.setServico(this.getServico());
		material.setExtipi(this.getExtipi());
		material.setNcmcompleto(this.getNcmcompleto());
		material.setCodigobarras(this.getCodigobarras());
		material.setUnidademedida(this.getUnidademedida());
		material.setPatrimonio(this.getPatrimonio());
		material.setEpi(this.getEpi());
		material.setMaterialgrupo(this.getMaterialgrupo());
		material.setMaterialtipo(this.getMaterialtipo());
		material.setValorvendaminimo(this.getValorvendaminimo());
		material.setValorvendamaximo(this.getValorvendamaximo());
		material.setValorcusto(this.getValorcusto());
		material.setValorvenda(this.getValorvenda());
		material.setPeso(this.getPeso());
		material.setPesobruto(this.getPesobruto());
		material.setObrigarlote(this.obrigarlote);
		material.setTributacaoipi(this.tributacaoipi);
		material.setPesoliquidovalorvenda(this.pesoliquidovalorvenda);
		material.setValorvendasemdesconto(this.valorvendasemdesconto);
		
		return material;
	}
	
	public Material copiaMaterial(){
		Material material = new Material();
		try {
			material = (Material) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return material;
	}

	@MaxLength(20)
	@DisplayName("Peso L�quido")
	public Double getPeso() {
		return peso;
	}

	@MaxLength(20)
	@DisplayName("Peso Bruto")
	public Double getPesobruto() {
		return pesobruto;
	}
	
	public void setPesobruto(Double pesobruto) {
		this.pesobruto = pesobruto;
	}
	
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	
	
	@Transient
	public Double getSugestaocompraestoqueminimo() {
		return sugestaocompraestoqueminimo;
	}
	@Transient
	public Double getSugestaocompravendas() {
		return sugestaocompravendas;
	}
	@Transient
	public Double getSugestaocompraestoqueatual() {
		return sugestaocompraestoqueatual;
	}
	@Transient
	public Double getSugestaocompraciclos() {
		return sugestaocompraciclos;
	}
	@Transient
	public Double getSugestaocomprasugestao() {
		return sugestaocomprasugestao;
	}

	public void setSugestaocompraestoqueminimo(Double sugestaocompraestoqueminimo) {
		this.sugestaocompraestoqueminimo = sugestaocompraestoqueminimo;
	}
	public void setSugestaocompravendas(Double sugestaocompravendas) {
		this.sugestaocompravendas = sugestaocompravendas;
	}
	public void setSugestaocompraestoqueatual(Double sugestaocompraestoqueatual) {
		this.sugestaocompraestoqueatual = sugestaocompraestoqueatual;
	}
	public void setSugestaocompraciclos(Double sugestaocompraciclos) {
		this.sugestaocompraciclos = sugestaocompraciclos;
	}
	public void setSugestaocomprasugestao(Double sugestaocomprasugestao) {
		this.sugestaocomprasugestao = sugestaocomprasugestao;
	}
	
	@Transient
	public Double getUltimovalorcompra() {
		return ultimovalorcompra;
	}
	@Transient
	public Double getUltimovalorvenda() {
		return ultimovalorvenda;
	}

	public void setUltimovalorcompra(Double ultimovalorcompra) {
		this.ultimovalorcompra = ultimovalorcompra;
	}
	public void setUltimovalorvenda(Double ultimovalorvenda) {
		this.ultimovalorvenda = ultimovalorvenda;
	}

	@Transient
	public Double getQtdeemexpedicao() {
		return qtdeemexpedicao;
	}
	@Transient
	public Double getQtdeemcompra() {
		return qtdeemcompra;
	}

	public void setQtdeemexpedicao(Double qtdeemexpedicao) {
		this.qtdeemexpedicao = qtdeemexpedicao;
	}
	public void setQtdeemcompra(Double qtdeemcompra) {
		this.qtdeemcompra = qtdeemcompra;
	}
	
	@Transient
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	
	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	
	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	@Transient
	public Empresa getEmpresatrans() {
		return empresatrans;
	}

	public void setEmpresatrans(Empresa empresatrans) {
		this.empresatrans = empresatrans;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@Transient
	public Integer getQtdeagendaproducao() {
		return qtdeagendaproducao;
	}
	public void setQtdeagendaproducao(Integer qtdeagendaproducao) {
		this.qtdeagendaproducao = qtdeagendaproducao;
	}

	@Transient
	public String getIdDocumentoRegistrarEntrada() {
		return idDocumentoRegistrarEntrada;
	}
	@Transient
	public String getIdFornecedorRegistrarEntrada() {
		return idFornecedorRegistrarEntrada;
	}
	public void setIdFornecedorRegistrarEntrada(String idFornecedorRegistrarEntrada) {
		this.idFornecedorRegistrarEntrada = idFornecedorRegistrarEntrada;
	}
	public void setIdDocumentoRegistrarEntrada(String idDocumentoRegistrarEntrada) {
		this.idDocumentoRegistrarEntrada = idDocumentoRegistrarEntrada;
	}

	@Transient
	public Double getQtdeemreserva() {
		return qtdeemreserva;
	}
	public void setQtdeemreserva(Double qtdeemreserva) {
		this.qtdeemreserva = qtdeemreserva;
	}
	
	@Transient
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	@Transient
	public Boolean getFromVeiculo() {
		return fromVeiculo;
	}

	public void setFromVeiculo(Boolean fromVeiculo) {
		this.fromVeiculo = fromVeiculo;
	}

	@Transient
	public Double getValordiatrans() {
		return valordiatrans;
	}
	public void setValordiatrans(Double valordiatrans) {
		this.valordiatrans = valordiatrans;
	}
	
	@Transient
	public String getCodigonaturezareceitaTrans(Faixaimpostocontrole faixaimpostocontrole){
		if(this.getGrupotributacao() != null && this.getGrupotributacao().getListaGrupotributacaoimposto() != null &&
				!this.getGrupotributacao().getListaGrupotributacaoimposto().isEmpty()){
			for(Grupotributacaoimposto grupotributacaoimposto : this.getGrupotributacao().getListaGrupotributacaoimposto()){
				if(grupotributacaoimposto.getControle() != null && faixaimpostocontrole.equals(grupotributacaoimposto.getControle())){
					return grupotributacaoimposto.getCodigonaturezareceita();
				}
			}
		}
		return "999";
	}
	
	@Transient
	public Double getFatorconversaoQtdereferencia(){
		Double valor = 1.0;
		if(this.fracao != null)
			valor = (this.fracao / (this.qtdereferencia != null ? this.qtdereferencia : 1.0));
		return valor;
	}

	@Transient
	public Boolean getUnidadesecundariaTabelapreco() {
		return unidadesecundariaTabelapreco;
	}
	public void setUnidadesecundariaTabelapreco(Boolean unidadesecundariaTabelapreco) {
		this.unidadesecundariaTabelapreco = unidadesecundariaTabelapreco;
	}

	@Transient
	public Double getSugestaocompratotalpeso() {
		if(this.getSugestaocompravendas() != null && this.getPeso() != null){
			return SinedUtil.round(this.getSugestaocompravendas() * this.getPeso(), 3);
		}
		return sugestaocompratotalpeso;
	}
	@Transient
	public Double getSugestaocompratotalpesobruto() {
		if(this.getSugestaocompravendas() != null && this.getPesobruto() != null){
			return SinedUtil.round(this.getSugestaocompravendas() * this.getPesobruto(), 3);
		}
		return sugestaocompratotalpesobruto;
	}

	public void setSugestaocompratotalpeso(Double sugestaocompratotalpeso) {
		this.sugestaocompratotalpeso = sugestaocompratotalpeso;
	}

	public void setSugestaocompratotalpesobruto(Double sugestaocompratotalpesobruto) {
		this.sugestaocompratotalpesobruto = sugestaocompratotalpesobruto;
	}

	@Transient
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	@Transient
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}

	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}
	
	@Transient
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}
	
	@Transient
	public Vendamaterialmestre getVendamaterialmestre() {
		return vendamaterialmestre;
	}

	public void setVendamaterialmestre(Vendamaterialmestre vendamaterialmestre) {
		this.vendamaterialmestre = vendamaterialmestre;
	}

	@Transient
	public Endereco getEndereconotaDestino() {
		return endereconotaDestino;
	}
	@Transient
	public Boolean getEnderecoavulso() {
		return enderecoavulso;
	}

	public Boolean getKit() {
		return kit;
	}
	
	@DisplayName("Kit Flex�vel")
	public Boolean getKitflexivel() {
		return kitflexivel;
	}

	@DisplayName("Material de Coleta")
	@JoinColumn(name="cdmaterialcoleta")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterialcoleta() {
		return materialcoleta;
	}
	@DisplayName("Material Mestre da Grade")
	@JoinColumn(name="cdmaterialmestregrade")
	@ManyToOne(fetch=FetchType.EAGER)
	public Material getMaterialmestregrade() {
		return materialmestregrade;
	}
	
	public void setKit(Boolean kit) {
		this.kit = kit;
	}
	public void setKitflexivel(Boolean kitflexivel) {
		this.kitflexivel = kitflexivel;
	}
	public void setMaterialcoleta(Material materialcoleta) {
		this.materialcoleta = materialcoleta;
	}
	public void setMaterialmestregrade(Material materialmestregrade) {
		this.materialmestregrade = materialmestregrade;
	}
	public void setEndereconotaDestino(Endereco endereconotaDestino) {
		this.endereconotaDestino = endereconotaDestino;
	}
	public void setEnderecoavulso(Boolean enderecoavulso) {
		this.enderecoavulso = enderecoavulso;
	}

	@Transient
	public Double getProduto_qtde() {
		return produto_qtde;
	}
	@Transient
	public Producaoagenda getProducaoagendatrans() {
		return producaoagendatrans;
	}
	@Transient
	public Producaoordem getProducaoordemtrans() {
		return producaoordemtrans;
	}

	public void setProduto_qtde(Double produtoQtde) {
		produto_qtde = produtoQtde;
	}
	public void setProducaoagendatrans(Producaoagenda producaoagendatrans) {
		this.producaoagendatrans = producaoagendatrans;
	}
	public void setProducaoordemtrans(Producaoordem producaoordemtrans) {
		this.producaoordemtrans = producaoordemtrans;
	}

	@Transient
	public List<Material> getListaMaterialitemgrade() {
		return listaMaterialitemgrade;
	}
	@Transient
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	@Transient
	public Double getPreco() {
		if(preco == null){
			return getValorvenda();
		}
		return preco;
	}
	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	@Transient
	public Integer getCdlocalarmazenagem() {
		return cdlocalarmazenagem;
	}
	@Transient
	public Integer getCdempresa() {
		return cdempresa;
	}
	@Transient
	public Integer getCdcliente() {
		return cdcliente;
	}
	@Transient
	public Integer getCdpedidovendamaterial() {
		return cdpedidovendamaterial;
	}
	@Transient
	public Integer getCdvendamaterial() {
		return cdvendamaterial;
	}
	@Transient
	public Integer getCdvendaorcamentomaterial() {
		return cdvendaorcamentomaterial;
	}
	@Transient
	public Integer getItemindex() {
		return itemindex;
	}
	@Transient
	public Integer getCdmaterialtabelapreco() {
		return cdmaterialtabelapreco;
	}
	@Transient
	public Double getComprimentos() {
		return comprimentos;
	}
	@Transient
	public Double getLarguras() {
		return larguras;
	}
	@Transient
	public Double getAlturas() {
		return alturas;
	}
	@Transient
	public Double getFatorconversaocomprimento() {
		return fatorconversaocomprimento;
	}
	@Transient
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}
	@Transient
	public Double getMultiplicador() {
		return multiplicador;
	}
	@Transient
	public Double getComprimentosoriginal() {
		return comprimentosoriginal;
	}
	@Transient
	public Boolean getExibirDimensoes() {
		return exibirDimensoes;
	}

	public void setListaMaterialitemgrade(List<Material> listaMaterialitemgrade) {
		this.listaMaterialitemgrade = listaMaterialitemgrade;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	public void setCdlocalarmazenagem(Integer cdlocalarmazenagem) {
		this.cdlocalarmazenagem = cdlocalarmazenagem;
	}
	public void setCdcliente(Integer cdcliente) {
		this.cdcliente = cdcliente;
	}
	public void setCdempresa(Integer cdempresa) {
		this.cdempresa = cdempresa;
	}
	public void setCdpedidovendamaterial(Integer cdpedidovendamaterial) {
		this.cdpedidovendamaterial = cdpedidovendamaterial;
	}
	public void setCdvendamaterial(Integer cdvendamaterial) {
		this.cdvendamaterial = cdvendamaterial;
	}
	public void setCdvendaorcamentomaterial(Integer cdvendaorcamentomaterial) {
		this.cdvendaorcamentomaterial = cdvendaorcamentomaterial;
	}
	public void setItemindex(Integer itemindex) {
		this.itemindex = itemindex;
	}
	public void setCdmaterialtabelapreco(Integer cdmaterialtabelapreco) {
		this.cdmaterialtabelapreco = cdmaterialtabelapreco;
	}
	public void setComprimentos(Double comprimentos) {
		this.comprimentos = comprimentos;
	}
	public void setLarguras(Double larguras) {
		this.larguras = larguras;
	}
	public void setAlturas(Double alturas) {
		this.alturas = alturas;
	}
	public void setFatorconversaocomprimento(Double fatorconversaocomprimento) {
		this.fatorconversaocomprimento = fatorconversaocomprimento;
	}
	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	public void setComprimentosoriginal(Double comprimentosoriginal) {
		this.comprimentosoriginal = comprimentosoriginal;
	}
	public void setExibirDimensoes(Boolean exibirDimensoes) {
		this.exibirDimensoes = exibirDimensoes;
	}

	@Transient
	public String getDescricaocomplementar() {
		return descricaocomplementar;
	}

	public void setDescricaocomplementar(String descricaocomplementar) {
		this.descricaocomplementar = descricaocomplementar;
	}
	@Transient
	public String getIndexEntregamaterial() {
		return indexEntregamaterial;
	}
	@Transient
	public Integer getCdordemcompramaterial() {
		return cdordemcompramaterial;
	}

	public void setIndexEntregamaterial(String indexEntregamaterial) {
		this.indexEntregamaterial = indexEntregamaterial;
	}

	public void setCdordemcompramaterial(Integer cdordemcompramaterial) {
		this.cdordemcompramaterial = cdordemcompramaterial;
	}

	@Transient
	public Materialformulapeso getMaterialformulapesoTrans() {
		return materialformulapesoTrans;
	}

	public void setMaterialformulapesoTrans(Materialformulapeso materialformulapesoTrans) {
		this.materialformulapesoTrans = materialformulapesoTrans;
	}
	
	@Transient
	public Double getQtdvolume() {
		return qtdvolume;
	}
	
	public void setQtdvolume(Double qtdvolume) {
		this.qtdvolume = qtdvolume;
	}

	@Transient
	public Integer getQuantidadeitemgrade() {
		return quantidadeitemgrade;
	}

	public void setQuantidadeitemgrade(Integer quantidadeitemgrade) {
		this.quantidadeitemgrade = quantidadeitemgrade;
	}
	
	@Transient
	public Boolean getFromEntrafiscalMontarGrade() {
		return fromEntrafiscalMontarGrade;
	}

	public void setFromEntrafiscalMontarGrade(Boolean fromEntrafiscalMontarGrade) {
		this.fromEntrafiscalMontarGrade = fromEntrafiscalMontarGrade;
	}
	@Transient
	public Boolean getFromEntrafiscalProcessMontarGrade() {
		return fromEntrafiscalProcessMontarGrade;
	}

	public void setFromEntrafiscalProcessMontarGrade(Boolean fromEntrafiscalProcessMontarGrade) {
		this.fromEntrafiscalProcessMontarGrade = fromEntrafiscalProcessMontarGrade;
	}
	@Transient
	public Grupotributacao getGrupotributacaoNota() {
		return grupotributacaoNota;
	}

	public void setGrupotributacaoNota(Grupotributacao grupotributacaoNota) {
		this.grupotributacaoNota = grupotributacaoNota;
	}
	@Transient
	public String getWhereInMaterialItenGrade() {
		return whereInMaterialItenGrade;
	}

	public void setWhereInMaterialItenGrade(String whereInMaterialItenGrade) {
		this.whereInMaterialItenGrade = whereInMaterialItenGrade;
	}
	@Transient
	public Double getQtdeTotalLote() {
		return qtdeTotalLote;
	}

	public void setQtdeTotalLote(Double qtdeTotalLote) {
		this.qtdeTotalLote = qtdeTotalLote;
	}
	
	@Transient
	public Double getQtdePedido() {
		return qtdePedido;
	}
	public void setQtdePedido(Double qtdePedido) {
		this.qtdePedido = qtdePedido;
	}
	
	@Transient
	public Double getQtdeTotaItens() {
		return qtdeTotaItens;
	}

	public void setQtdeTotaItens(Double qtdeTotaItens) {
		this.qtdeTotaItens = qtdeTotaItens;
	}
	@Transient
	public Grupotributacao getGrupotributacaoForNota(Empresa empresa, Grupotributacao grupotributacao){
		if(grupotributacao != null && grupotributacao.getCdgrupotributacao() != null)
			return grupotributacao;
		
		try {
			if(empresa != null && getListaMaterialempresa() != null && !getListaMaterialempresa().isEmpty()){
				for(Materialempresa materialempresa : getListaMaterialempresa()){
					if(materialempresa.getEmpresa() != null && materialempresa.getEmpresa().equals(empresa)){
						if(materialempresa.getGrupotributacao() != null)
							return materialempresa.getGrupotributacao();
						break;
					}
				}
			}
		} catch (Exception e) {}
		
		return getGrupotributacao();
	}
	
	@Transient
	public Boolean getExibiritensproducaovenda() {
		return exibiritensproducaovenda;
	}
	public void setExibiritensproducaovenda(Boolean exibiritensproducaovenda) {
		this.exibiritensproducaovenda = exibiritensproducaovenda;
	}
	
	@Transient
	public Integer getCasasdecimaisestoqueTrans() {
		return casasdecimaisestoqueTrans;
	}
	public void setCasasdecimaisestoqueTrans(Integer casasdecimaisestoqueTrans) {
		this.casasdecimaisestoqueTrans = casasdecimaisestoqueTrans;
	}
	
	@Transient
	public Ordemcompra getOrdemcompratrans() {
		return ordemcompratrans;
	}
	public void setOrdemcompratrans(Ordemcompra ordemcompratrans) {
		this.ordemcompratrans = ordemcompratrans;
	}
	
	@Transient
	public Boolean getExistReserva() {
		return existReserva;
	}
	public void setExistReserva(Boolean existReserva) {
		this.existReserva = existReserva;
	}
	
	@DisplayName("Exibir itens na venda")
	public Boolean getExibiritenskitvenda() {
		return exibiritenskitvenda;
	}

	public void setExibiritenskitvenda(Boolean exibiritenskitvenda) {
		this.exibiritenskitvenda = exibiritenskitvenda;
	}
	
	@DisplayName("Exibir itens na nota")
	public Boolean getExibiritenskitflexivelvenda() {
		return exibiritenskitflexivelvenda;
	}

	public void setExibiritenskitflexivelvenda(Boolean exibiritenskitflexivelvenda) {
		this.exibiritenskitflexivelvenda = exibiritenskitflexivelvenda;
	}

	@DisplayName("F�rmula para calculo do valor de venda")
	@OneToMany(mappedBy="material")
	public List<Materialformulavalorvenda> getListaMaterialformulavalorvenda() {
		return listaMaterialformulavalorvenda;
	}

	public void setListaMaterialformulavalorvenda(List<Materialformulavalorvenda> listaMaterialformulavalorvenda) {
		this.listaMaterialformulavalorvenda = listaMaterialformulavalorvenda;
	}

	@Transient
	public Double getVendaaltura() {
		return vendaaltura;
	}
	@Transient
	public Double getVendalargura() {
		return vendalargura;
	}
	@Transient
	public Double getVendacomprimento() {
		return vendacomprimento;
	}
	@Transient
	public Unidademedida getVendaunidademedida() {
		return vendaunidademedida;
	}
	@Transient
	public String getVendasimbolounidademedida() {
		return vendasimbolounidademedida;
	}
	@Transient
	public Double getVendafrete() {
		return vendafrete;
	}

	public void setVendaaltura(Double vendaaltura) {
		this.vendaaltura = vendaaltura;
	}
	public void setVendalargura(Double vendalargura) {
		this.vendalargura = vendalargura;
	}
	public void setVendacomprimento(Double vendacomprimento) {
		this.vendacomprimento = vendacomprimento;
	}
	public void setVendaunidademedida(Unidademedida vendaunidademedida) {
		this.vendaunidademedida = vendaunidademedida;
	}
	public void setVendasimbolounidademedida(String vendasimbolounidademedida) {
		this.vendasimbolounidademedida = vendasimbolounidademedida;
	}
	public void setVendafrete(Double vendafrete) {
		this.vendafrete = vendafrete;
	}

	@Transient
	public Boolean getIsValorvendaFormula() {
		return isValorvendaFormula;
	}
	public void setIsValorvendaFormula(Boolean isValorvendaFormula) {
		this.isValorvendaFormula = isValorvendaFormula;
	}
	
	@Transient
	public Boolean getIsValorvendamaximoFormula() {
		return isValorvendamaximoFormula;
	}

	public void setIsValorvendamaximoFormula(Boolean isValorvendamaximoFormula) {
		this.isValorvendamaximoFormula = isValorvendamaximoFormula;
	}
	
	@Transient
	public Boolean getIsValorvendaminimoFormula() {
		return isValorvendaminimoFormula;
	}

	public void setIsValorvendaminimoFormula(Boolean isValorvendaminimoFormula) {
		this.isValorvendaminimoFormula = isValorvendaminimoFormula;
	}

	@DisplayName("Origem do Produto")
	public Origemproduto getOrigemproduto() {
		return origemproduto;
	}
	public void setOrigemproduto(Origemproduto origemproduto) {
		this.origemproduto = origemproduto;
	}

	@Transient
	public Double getValorcustoByCalculoProducao() {
		return valorcustoByCalculoProducao;
	}
	public void setValorcustoByCalculoProducao(Double valorcustoByCalculoProducao) {
		this.valorcustoByCalculoProducao = valorcustoByCalculoProducao;
	}
	
	@Transient
	public String getDescricaoInconsistenciaLancamento() {
		return descricaoInconsistenciaLancamento;
	}
	public void setDescricaoInconsistenciaLancamento(String descricaoInconsistenciaLancamento) {
		this.descricaoInconsistenciaLancamento = descricaoInconsistenciaLancamento;
	}

	@DisplayName("Data do custo inicial")
	public Date getDtcustoinicial() {
		return dtcustoinicial;
	}
	@DisplayName("Qtde. de refer�ncia")
	public Double getQtdecustoinicial() {
		return qtdecustoinicial;
	}
	@DisplayName("Valor do custo inicial")
	public Double getValorcustoinicial() {
		return valorcustoinicial;
	}

	public void setDtcustoinicial(Date dtcustoinicial) {
		this.dtcustoinicial = dtcustoinicial;
	}
	public void setQtdecustoinicial(Double qtdecustoinicial) {
		this.qtdecustoinicial = qtdecustoinicial;
	}
	public void setValorcustoinicial(Double valorcustoinicial) {
		this.valorcustoinicial = valorcustoinicial;
	}

	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

	@Transient
	public Boolean getTabelaprecoComPrazopagamento() {
		return tabelaprecoComPrazopagamento;
	}

	public void setTabelaprecoComPrazopagamento(Boolean tabelaprecoComPrazopagamento) {
		this.tabelaprecoComPrazopagamento = tabelaprecoComPrazopagamento;
	}

	@Transient
	public Boolean getConsiderarValorvendaCalculado() {
		return considerarValorvendaCalculado;
	}
	public void setConsiderarValorvendaCalculado(
			Boolean considerarValorvendaCalculado) {
		this.considerarValorvendaCalculado = considerarValorvendaCalculado;
	}

	@Transient
	public Boolean getChecado() {
		return checado;
	}

	public void setChecado(Boolean checado) {
		this.checado = checado;
	}

	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
//	public String getLocalarmazenagem() {
//		return localarmazenagem;
//	}
//
//	public void setLocalarmazenagem(String localarmazenagem) {
//		this.localarmazenagem = localarmazenagem;
//	}
	
	@Transient
	public Boolean getModelodocumentofiscalICMS() {
		return modelodocumentofiscalICMS;
	}

	public void setModelodocumentofiscalICMS(Boolean modelodocumentofiscalICMS) {
		this.modelodocumentofiscalICMS = modelodocumentofiscalICMS;
	}

	@Transient
	public List<Materiaprima> getListaMateriaprima() {
		return listaMateriaprima;
	}

	public void setListaMateriaprima(List<Materiaprima> listaMateriaprima) {
		this.listaMateriaprima = listaMateriaprima;
	}
	
	@Transient
	public Boolean getPesoliquidocalculadopelaformula() {
		return pesoliquidocalculadopelaformula;
	}

	public void setPesoliquidocalculadopelaformula(Boolean pesoliquidocalculadopelaformula) {
		this.pesoliquidocalculadopelaformula = pesoliquidocalculadopelaformula;
	}

	@Transient
	public String getNomealternativo() {
		return nomealternativo;
	}

	public void setNomealternativo(String nomealternativo) {
		this.nomealternativo = nomealternativo;
	}
	
	@Transient
	public String getWhereInSolicitacaocompra() {
		return whereInSolicitacaocompra;
	}
	
	public void setWhereInSolicitacaocompra(String whereInSolicitacaocompra) {
		this.whereInSolicitacaocompra = whereInSolicitacaocompra;
	}

	@Transient
	public Boolean getRevendedor() {
		return revendedor;
	}
	@Transient
	public Double getQuantidadeOportunidade() {
		return quantidadeOportunidade;
	}

	public void setRevendedor(Boolean revendedor) {
		this.revendedor = revendedor;
	}
	public void setQuantidadeOportunidade(Double quantidadeOportunidade) {
		this.quantidadeOportunidade = quantidadeOportunidade;
	}

	@Transient
	public Pneu getPneu() {
		return pneu;
	}
	
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}

	@DisplayName("Tributa��o de impostos")
	public Boolean getTributacaoipi() {
		return tributacaoipi;
	}

	public void setTributacaoipi(Boolean tributacaoipi) {
		this.tributacaoipi = tributacaoipi;
	}
	
	@Transient
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Transient
	public Materialunidademedida getMaterialunidademedida() {
		return materialunidademedida;
	}

	public void setMaterialunidademedida(Materialunidademedida materialunidademedida) {
		this.materialunidademedida = materialunidademedida;
	}
	
	@Transient
	public Integer getQtdMaterialNota() {
		return qtdMaterialNota;
	}
	public void setQtdMaterialNota(Integer qtdMaterialNota) {
		this.qtdMaterialNota = qtdMaterialNota;
	}

	@MaxLength(7)
	public String getCest() {
		return cest;
	}

	public void setCest(String cest) {
		this.cest = cest;
	}

	@Transient
	public Boolean getInverterconversao() {
		return inverterconversao;
	}

	public void setInverterconversao(Boolean inverterconversao) {
		this.inverterconversao = inverterconversao;
	}

	@Transient
	public Producaoetapaitem getProducaoetapaitemtrans() {
		return producaoetapaitemtrans;
	}

	public void setProducaoetapaitemtrans(Producaoetapaitem producaoetapaitemtrans) {
		this.producaoetapaitemtrans = producaoetapaitemtrans;
	}

	@Transient
	public Material getBanda() {
		return banda;
	}

	public void setBanda(Material banda) {
		this.banda = banda;
	}

	@DisplayName("Prazo de Garantia (Dias)")
	public Integer getPrazogarantia() {
		return prazogarantia;
	}

	public void setPrazogarantia(Integer prazogarantia) {
		this.prazogarantia = prazogarantia;
	}

	@DisplayName("Definir por local")
	public Boolean getDefinirqtdeminimaporlocal() {
		return definirqtdeminimaporlocal;
	}

	public void setDefinirqtdeminimaporlocal(Boolean definirqtdeminimaporlocal) {
		this.definirqtdeminimaporlocal = definirqtdeminimaporlocal;
	}
	
	@OneToMany(mappedBy="bempatrimonio")	
	public List<Patrimonioitem> getListaPatrimonioitem() {
		return listaPatrimonioitem;
	}
	
	public void setListaPatrimonioitem(List<Patrimonioitem> listaPatrimonioitem) {
		this.listaPatrimonioitem = listaPatrimonioitem;
	}
	
	@Transient
	public String getPlaquetaSerie() {
		return plaquetaSerie;
	}
	
	public void setPlaquetaSerie(String plaquetaSerie) {
		this.plaquetaSerie = plaquetaSerie;
	}
	
	@Transient
	public String getSerie() {
		try {
			if (listaMaterialnumeroserie!=null && !listaMaterialnumeroserie.isEmpty()){
				return CollectionsUtil.listAndConcatenate(listaMaterialnumeroserie, "numero", " / ");
			}else {
				return null;
			}
		} catch (LazyInitializationException e) {
			return null;
		}
	}
	
	@Transient
	public String getSeriePatrimonio(Materialnumeroserie materialnumeroserie) {
		try {
			if (materialnumeroserie != null && materialnumeroserie.getCdmaterialnumeroserie() != null && listaMaterialnumeroserie!=null && !listaMaterialnumeroserie.isEmpty()){
				if(SinedUtil.isListNotEmpty(listaMaterialnumeroserie)){
					for(Materialnumeroserie mns : listaMaterialnumeroserie){
						if(materialnumeroserie.getCdmaterialnumeroserie().equals(mns.getCdmaterialnumeroserie())){
							return mns.getNumero();
						}
					}
				}
				return CollectionsUtil.listAndConcatenate(listaMaterialnumeroserie, "numero", " / ");
			}else {
				return null;
			}
		} catch (LazyInitializationException e) {
			return null;
		}
	}
	
	@Transient
	public String getIdentificacaoNomeSerie() {
		String retorno = "";
		
		if (identificacao!=null && !identificacao.trim().equals("")){
			retorno += identificacao + " - ";
		}
		
		retorno += nome;
		
		String serie = getSerie();
		
		if (serie!=null){
			retorno += " - " + serie;
		}
		
		return retorno;
	}
	
	@Transient
	public String getDescriptionAutocompleteRomaneio() {
		String retorno = "";
		
		if (identificacao!=null && !identificacao.trim().equals("")){
			retorno += identificacao + " - ";
		}
		
		retorno += nome;
		
		if (Boolean.TRUE.equals(getPatrimonio())){
			String serie = getSerie();
			
			if (serie!=null){
				retorno += " - " + serie;
			}
		}		
		
		return retorno;
	}
	
	@DisplayName("Material acompanha banda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialacompanhabanda")
	public Material getMaterialacompanhabanda() {
		return materialacompanhabanda;
	}
	
	public void setMaterialacompanhabanda(Material materialacompanhabanda) {
		this.materialacompanhabanda = materialacompanhabanda;
	}
	
	@DisplayName("Faixa de Markup")
	public Boolean getCadastrarFaixaMarkup() {
		return cadastrarFaixaMarkup;
	}
	
	public void setCadastrarFaixaMarkup(Boolean cadastrarFaixaMarkup) {
		this.cadastrarFaixaMarkup = cadastrarFaixaMarkup;
	}
	
	@Transient
	public Double getPercentualdescontoTabela() {
		return percentualdescontoTabela;
	}
	public void setPercentualdescontoTabela(Double percentualdescontoTabela) {
		this.percentualdescontoTabela = percentualdescontoTabela;
	}
	
	@Transient
	public Double getValorCustoForRecalculoCustoMedio() {
		return valorCustoForRecalculoCustoMedio;
	}
	public void setValorCustoForRecalculoCustoMedio(Double valorCustoForRecalculoCustoMedio) {
		this.valorCustoForRecalculoCustoMedio = valorCustoForRecalculoCustoMedio;
	}
	
	@Transient
	public Comissionamento getComissionamentoTabelapreco() {
		return comissionamentoTabelapreco;
	}

	public void setComissionamentoTabelapreco(Comissionamento comissionamentoTabelapreco) {
		this.comissionamentoTabelapreco = comissionamentoTabelapreco;
	}
	
	@Transient
	public Double getValorvendasemdesconto() {
		return valorvendasemdesconto;
	}

	public void setValorvendasemdesconto(Double valorvendasemdesconto) {
		this.valorvendasemdesconto = valorvendasemdesconto;
	}
	
	@Transient
	public Boolean getTemTabelaPrecoVigenteMaterial() {
		return temTabelaPrecoVigenteMaterial;
	}
	
	public void setTemTabelaPrecoVigenteMaterial(Boolean temTabelaPrecoVigenteMaterial) {
		this.temTabelaPrecoVigenteMaterial = temTabelaPrecoVigenteMaterial;
	}
	
	@Transient
	public Boolean getExibirItensKitFlexivelTrans() {
		return exibirItensKitFlexivelTrans;
	}
	
	@Transient
	@DisplayName("Qtde dispon�vel em estoque")
	public Double getQtdeDisponivelEmEstoque() {
		return qtdeDisponivelEmEstoque;
	}
	
	public void setExibirItensKitFlexivelTrans(
			Boolean exibirItensKitFlexivelTrans) {
		this.exibirItensKitFlexivelTrans = exibirItensKitFlexivelTrans;
	}
	
	public void setQtdeDisponivelEmEstoque(Double qtdeDisponivelEmEstoque) {
		this.qtdeDisponivelEmEstoque = qtdeDisponivelEmEstoque;
	}
	
	@Transient
	public Double getValorvendaSemDescontoOuValorvenda(){
		return valorvendasemdesconto != null ? valorvendasemdesconto : valorvenda; 
	}

	@Transient
	public String getNomeBanda(){
		String retorno = "";
		
		if (Hibernate.isInitialized(listaProducao) && listaProducao!=null){
			for (Materialproducao materialproducao: listaProducao){
				if (Boolean.TRUE.equals(materialproducao.getExibirvenda()) && materialproducao.getMaterial()!=null){
					if (materialproducao.getMaterial().getIdentificacao()!=null && !materialproducao.getMaterial().getIdentificacao().trim().equals("")){
						retorno = materialproducao.getMaterial().getIdentificacao() + " - ";
					}
					retorno += materialproducao.getMaterial().getNome();
					break;
				}
			}
		}
		
		return retorno;
	}
	
	@Transient
	public String getProfundidadesulcoBanda(){
		if (Hibernate.isInitialized(listaProducao) && listaProducao!=null){
			for (Materialproducao materialproducao: listaProducao){
				if (Boolean.TRUE.equals(materialproducao.getExibirvenda()) && materialproducao.getMaterial()!=null && materialproducao.getMaterial().getProfundidadesulco()!=null){
					return new DecimalFormat("###,##0.##").format(materialproducao.getMaterial().getProfundidadesulco());
				}
			}
		}
		
		return null;
	}
	
	@Transient
	public Materialrelacionado getMaterialrelacionadoForCurvaabc() {
		return materialrelacionadoForCurvaabc;
	}
	public void setMaterialrelacionadoForCurvaabc(
			Materialrelacionado materialrelacionadoForCurvaabc) {
		this.materialrelacionadoForCurvaabc = materialrelacionadoForCurvaabc;
	}
	
	@Required
	@DisplayName("Faturamento por")
	public FaturamentoVeterinaria getFaturamentoVeterinaria() {
		return faturamentoVeterinaria;
	}
	
	@DisplayName("Qtde. por unidade")
	@MaxLength(9)
	public Double getQtdeunidadeveterinaria() {
		return qtdeunidadeveterinaria;
	}

	public void setFaturamentoVeterinaria(FaturamentoVeterinaria faturamentoVeterinaria) {
		this.faturamentoVeterinaria = faturamentoVeterinaria;
	}

	public void setQtdeunidadeveterinaria(Double qtdeunidadeveterinaria) {
		this.qtdeunidadeveterinaria = qtdeunidadeveterinaria;
	}

	@Transient
	public Double getMateriaisDisponiveisEmEstoque() {
		return materiaisDisponiveisEmEstoque;
	}

	public void setMateriaisDisponiveisEmEstoque(
			Double materiaisDisponiveisEmEstoque) {
		this.materiaisDisponiveisEmEstoque = materiaisDisponiveisEmEstoque;
	}

	@Transient
	public Double getConsumoMaterialAcompanhaBanda() {
		return consumoMaterialAcompanhaBanda;
	}

	public void setConsumoMaterialAcompanhaBanda(Double consumoMaterialAcompanhaBanda) {
		this.consumoMaterialAcompanhaBanda = consumoMaterialAcompanhaBanda;
	}
	
	@Transient
	public Localdestinonfe getLocalDestinoNfe() {
		return localDestinoNfe;
	}
	
	public void setLocalDestinoNfe(Localdestinonfe localDestinoNfe) {
		this.localDestinoNfe = localDestinoNfe;
	}
	
	@DisplayName("Item de grade padr�o para material mestre")
	public Boolean getItemGradePadraoMaterialMestre() {
		return itemGradePadraoMaterialMestre;
	}
	
	public void setItemGradePadraoMaterialMestre(
			Boolean itemGradePadraoMaterialMestre) {
		this.itemGradePadraoMaterialMestre = itemGradePadraoMaterialMestre;
	}
	
	public String getIdVinculoExterno() {
		return idVinculoExterno;
	}
	
	public void setIdVinculoExterno(String idVinculoExterno) {
		this.idVinculoExterno = idVinculoExterno;
	}
	
	@DisplayName("E-commerce")
	public Boolean getEcommerce() {
		return ecommerce;
	}
	
	public void setEcommerce(Boolean ecommerce) {
		this.ecommerce = ecommerce;
	}
	
	public Boolean getMarketplace() {
		return marketplace;
	}
	
	public void setMarketplace(Boolean marketplace) {
		this.marketplace = marketplace;
	}
	
	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}
	
	public Boolean getSomenteParceiros() {
		return somenteParceiros;
	}
	public void setSomenteParceiros(Boolean somenteParceiros) {
		this.somenteParceiros = somenteParceiros;
	}
	
	@DisplayName("Id de e-commerce")
	public Integer getIdEcommerce() {
		return idEcommerce;
	}
	
	@DisplayName("Mais informa��es")
	public String getMaisInformacoes() {
		return maisInformacoes;
	}
	
	@DisplayName("Detalhes t�cnicos")
	public String getDetalhesTecnicos() {
		return detalhesTecnicos;
	}
	
	@DisplayName("Itens inclusos")
	public String getItensInclusos() {
		return itensInclusos;
	}
	
	@DisplayName("Conex�es")
	public String getConexoes() {
		return conexoes;
	}
	
	@DisplayName("Status do Produto")
	public StatusUsoEnum getStatusUso() {
		return statusUso;
	}
	
	@DisplayName("Modelo de disponibilidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmodelodisponibilidade")
	public ModeloDisponibilidade getModeloDisponibilidade() {
		return modeloDisponibilidade;
	}
	
	@DisplayName("Pre�o de venda fantasia")
	public Money getPrecoVendaFantasia() {
		return precoVendaFantasia;
	}
	
	@DisplayName("(%) Desconto � vista")
	public Integer getDescontoAVista() {
		return descontoAVista;
	}
	
	@DisplayName("Quantidade m�nima carrinho")
	public Double getQtdeCarrinho() {
		return qtdeCarrinho;
	}
	
	@DisplayName("Pre�o sob consulta")
	public Boolean getPrecoSobConsulta() {
		return precoSobConsulta;
	}
	
	@DisplayName("Permitir venda no site")
	public Boolean getPermiteVendaSite() {
		return permiteVendaSite;
	}

	public void setIdEcommerce(Integer idEcommerce) {
		this.idEcommerce = idEcommerce;
	}

	public void setMaisInformacoes(String maisInformacoes) {
		this.maisInformacoes = maisInformacoes;
	}

	public void setDetalhesTecnicos(String detalhesTecnicos) {
		this.detalhesTecnicos = detalhesTecnicos;
	}

	public void setItensInclusos(String itensInclusos) {
		this.itensInclusos = itensInclusos;
	}

	public void setConexoes(String conexoes) {
		this.conexoes = conexoes;
	}
	
	public void setStatusUso(StatusUsoEnum statusUso) {
		this.statusUso = statusUso;
	}
	
	public void setModeloDisponibilidade(ModeloDisponibilidade modeloDisponibilidade) {
		this.modeloDisponibilidade = modeloDisponibilidade;
	}

	public void setPrecoVendaFantasia(Money precoVendaFantasia) {
		this.precoVendaFantasia = precoVendaFantasia;
	}

	public void setDescontoAVista(Integer descontoAVista) {
		this.descontoAVista = descontoAVista;
	}
	
	public void setQtdeCarrinho(Double qtdeCarrinho) {
		this.qtdeCarrinho = qtdeCarrinho;
	}

	public void setPrecoSobConsulta(Boolean precoSobConsulta) {
		this.precoSobConsulta = precoSobConsulta;
	}

	public void setPermiteVendaSite(Boolean permiteVendaSite) {
		this.permiteVendaSite = permiteVendaSite;
	}
	
	@Transient
	public Money getValorCustoEcommerce() {
		return valorCustoEcommerce;
	}
	@Transient
	public Money getValorVendaEcommerce() {
		return valorVendaEcommerce;
	}
	@Transient
	public Double getQtdeEstoqueEcommerce() {
		return qtdeEstoqueEcommerce;
	}
	public void setValorCustoEcommerce(Money valorCustoEcommerce) {
		this.valorCustoEcommerce = valorCustoEcommerce;
	}
	public void setValorVendaEcommerce(Money valorVendaEcommerce) {
		this.valorVendaEcommerce = valorVendaEcommerce;
	}
	public void setQtdeEstoqueEcommerce(Double qtdeEstoqueEcommerce) {
		this.qtdeEstoqueEcommerce = qtdeEstoqueEcommerce;
	}
	
	@DisplayName("Largura (cm)")
	public Double getEcommerce_largura() {
		return ecommerce_largura;
	}
	@DisplayName("Comprimento (cm)")
	public Double getEcommerce_comprimento() {
		return ecommerce_comprimento;
	}
	@DisplayName("Altura (cm)")
	public Double getEcommerce_altura() {
		return ecommerce_altura;
	}

	public void setEcommerce_largura(Double ecommerce_largura) {
		this.ecommerce_largura = ecommerce_largura;
	}

	public void setEcommerce_comprimento(Double ecommerce_comprimento) {
		this.ecommerce_comprimento = ecommerce_comprimento;
	}

	public void setEcommerce_altura(Double ecommerce_altura) {
		this.ecommerce_altura = ecommerce_altura;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="material")
	public List<ImagensEcommerce> getListaImagensEcommerce() {
		return listaImagensEcommerce;
	}
	
	public void setListaImagensEcommerce(
			List<ImagensEcommerce> listaImagensEcommerce) {
		this.listaImagensEcommerce = listaImagensEcommerce;
	}
	
	public Boolean getEmbalagem() {
		return embalagem;
	}
	
	public void setEmbalagem(Boolean embalagem) {
		this.embalagem = embalagem;
	}
	
	@DisplayName("Embalagem")
	@OneToMany(mappedBy="material")
	public List<Embalagem> getListaEmbalagem() {
		return listaEmbalagem;
	}
	
	public void setListaEmbalagem(List<Embalagem> listaEmbalagem) {
		this.listaEmbalagem = listaEmbalagem;
	}
	
	@Transient
	public String getNomeFornecedorFabricante(){
		if(SinedUtil.isListNotEmptyAndInitialized(getListaFornecedor())){
			for(Materialfornecedor materialfornecedor : getListaFornecedor()){
				if(materialfornecedor.getFornecedor() != null && Boolean.TRUE.equals(materialfornecedor.getFabricante())){
					return materialfornecedor.getFornecedor().getNome();
				}
			}
		}
		return "";
	}
	
	@DisplayName("Quantidade m�xima para o carrinho")
	public Integer getQtdeMaximaCarrinho() {
		return qtdeMaximaCarrinho;
	}

	public void setQtdeMaximaCarrinho(Integer qtdeMaximaCarrinho) {
		this.qtdeMaximaCarrinho = qtdeMaximaCarrinho;
	}

	@DisplayName("Exibir mapa de matriz de atributos")
	public ExibirMatrizAtributosEnum getExibirMatrizAtributos() {
		return exibirMatrizAtributos;
	}

	public void setExibirMatrizAtributos(ExibirMatrizAtributosEnum exibirMatrizAtributos) {
		this.exibirMatrizAtributos = exibirMatrizAtributos;
	}

	@DisplayName("Contra proposta")
	public Boolean getContraProposta() {
		return contraProposta;
	}

	public void setContraProposta(Boolean contraProposta) {
		this.contraProposta = contraProposta;
	}

	@DisplayName("Frete Gr�tis")
	public FreteGratisEnum getFreteGratis() {
		return freteGratis;
	}

	public void setFreteGratis(FreteGratisEnum freteGratis) {
		this.freteGratis = freteGratis;
	}
	
	public Integer getIdPaiExterno() {
		return idPaiExterno;
	}
	public void setIdPaiExterno(Integer idPaiExterno) {
		this.idPaiExterno = idPaiExterno;
	}
	
	@DisplayName("Enviar para o e-commerce?")
	public Boolean getEnviarInformacoesEcommerce() {
		return enviarInformacoesEcommerce;
	}
	public void setEnviarInformacoesEcommerce(Boolean enviarInformacoesEcommerce) {
		this.enviarInformacoesEcommerce = enviarInformacoesEcommerce;
	}
	
	@DisplayName("Atributos")
	@OneToMany(mappedBy="material")
	public List<MaterialAtributosMaterial> getListaMaterialAtributosMaterial() {
		return listaMaterialAtributosMaterial;
	}

	public void setListaMaterialAtributosMaterial(
			List<MaterialAtributosMaterial> listaMaterialAtributosMaterial) {
		this.listaMaterialAtributosMaterial = listaMaterialAtributosMaterial;
	}
	
    @DisplayName("Estoque m�nimo")
    public Double getQtdeEstoqueMinimoEcommerce() {
        return qtdeEstoqueMinimoEcommerce;
    }
    public void setQtdeEstoqueMinimoEcommerce(Double qtdeEstoqueMinimoEcommerce) {
        this.qtdeEstoqueMinimoEcommerce = qtdeEstoqueMinimoEcommerce;
    }
	
	@Transient
	@DisplayName("ID do e-commerce")
	public Integer getIdEcommerceTrans() {
		return idEcommerceTrans;
	}
	public void setIdEcommerceTrans(Integer idEcommerceTrans) {
		this.idEcommerceTrans = idEcommerceTrans;
	}
}