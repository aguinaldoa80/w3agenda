package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Entity
public class Solicitacaocompraacao {

	public static final Solicitacaocompraacao CRIADA = new Solicitacaocompraacao(1, "Criada");
	public static final Solicitacaocompraacao AUTORIZADA = new Solicitacaocompraacao(2, "Autorizada");
	public static final Solicitacaocompraacao EM_PROCESSO_COMPRA = new Solicitacaocompraacao(3, "Em processo de compra");
	public static final Solicitacaocompraacao BAIXADA = new Solicitacaocompraacao(4, "Baixada");
	public static final Solicitacaocompraacao CANCELADA = new Solicitacaocompraacao(5, "Cancelada");
	public static final Solicitacaocompraacao ESTORNADA = new Solicitacaocompraacao(6, "Estornada");
	public static final Solicitacaocompraacao NAO_AUTORIZADA = new Solicitacaocompraacao(7, "N�o autorizada");
	public static final Solicitacaocompraacao EXCLUSAO_COTACAO = new Solicitacaocompraacao(8, "Exclus�o da Cota��o");
	
	protected Integer cdsolicitacaocompraacao;
	protected String nome;
	
	public Solicitacaocompraacao(){
	}
	
	public Solicitacaocompraacao(Integer cdsolicitacaocompraacao, String nome){
		this.cdsolicitacaocompraacao = cdsolicitacaocompraacao;
		this.nome = nome;
	}
	
	@Id
	public Integer getCdsolicitacaocompraacao() {
		return cdsolicitacaocompraacao;
	}
	@MaxLength(30)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setCdsolicitacaocompraacao(Integer cdsolicitacaocompraacao) {
		this.cdsolicitacaocompraacao = cdsolicitacaocompraacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
