package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

public class BdiComposicaoOrcamentoForm {

	protected Integer cdBdiComposicaoOrcamento;
	protected Composicaoorcamento composicaoOrcamento;
	protected String nome;
	protected Double valor;	
	protected Boolean marcado;
	protected List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoFormFilho = new ArrayList<BdiComposicaoOrcamentoForm>();
	
	public Integer getCdBdiComposicaoOrcamento() {
		return cdBdiComposicaoOrcamento;
	}
	public Composicaoorcamento getComposicaoOrcamento() {
		return composicaoOrcamento;
	}
	public String getNome() {
		return nome;
	}
	public Double getValor() {
		return valor;
	}
	public Boolean getMarcado() {
		return marcado;
	}
	public List<BdiComposicaoOrcamentoForm> getListaBdiComposicaoOrcamentoFormFilho() {
		return listaBdiComposicaoOrcamentoFormFilho;
	}
	public void setCdBdiComposicaoOrcamento(Integer cdBdiComposicaoOrcamento) {
		this.cdBdiComposicaoOrcamento = cdBdiComposicaoOrcamento;
	}
	public void setComposicaoOrcamento(Composicaoorcamento composicaoOrcamento) {
		this.composicaoOrcamento = composicaoOrcamento;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public void setMarcado(Boolean marcado) {
		this.marcado = marcado;
	}
	public void setListaBdiComposicaoOrcamentoFormFilho(
			List<BdiComposicaoOrcamentoForm> listaBdiComposicaoOrcamentoFormFilho) {
		this.listaBdiComposicaoOrcamentoFormFilho = listaBdiComposicaoOrcamentoFormFilho;
	}	
}
