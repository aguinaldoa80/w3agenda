package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Motivoinventario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@DisplayName("Invent�rio")
@SequenceGenerator(name = "sq_inventario", sequenceName = "sq_inventario")
public class Inventario implements Log{
	
	protected Integer cdinventario;
	protected Date dtinventario;
	protected Localarmazenagem localarmazenagem;
	protected Projeto projeto;
	protected Motivoinventario motivoinventario;
	protected String codigocontaanalitica;
	protected List<Inventariomaterial> listaInventariomaterial;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	protected Boolean correcao;
	protected Inventario inventarioOrigem;
	protected InventarioTipo inventarioTipo;
	protected InventarioSituacao inventarioSituacao;
	protected List<InventarioHistorico> listaInventarioHistorico;
	protected Empresa empresa;

	public Inventario() {
	}
	
	public Inventario(Integer cdinventario) {
		this.cdinventario = cdinventario;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inventario")
	public Integer getCdinventario() {
		return cdinventario;
	}
	@DisplayName("Data do invent�rio")
	public Date getDtinventario() {
		return dtinventario;
	}
	@OneToMany(mappedBy="inventario")
	public List<Inventariomaterial> getListaInventariomaterial() {
		return listaInventariomaterial;
	}
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="inventario")
	public List<InventarioHistorico> getListaInventarioHistorico() {
		return listaInventarioHistorico;
	}
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	@DisplayName("Situa��o")
	public InventarioSituacao getInventarioSituacao() {
		return inventarioSituacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setListaInventarioHistorico(
			List<InventarioHistorico> listaInventarioHistorico) {
		this.listaInventarioHistorico = listaInventarioHistorico;
	}
	public void setInventarioSituacao(InventarioSituacao inventarioSituacao) {
		this.inventarioSituacao = inventarioSituacao;
	}
	public void setCdinventario(Integer cdinventario) {
		this.cdinventario = cdinventario;
	}
	public void setDtinventario(Date dtinventario) {
		this.dtinventario = dtinventario;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	public void setListaInventariomaterial(List<Inventariomaterial> listaInventariomaterial) {
		this.listaInventariomaterial = listaInventariomaterial;
	}
	
	@Transient
	@DescriptionProperty(usingFields={"cdinventario", "dtinventario"})
	public String getDescricaoAutocomplete(){
		return this.cdinventario + " - " +this.getMesanoAux();
	}
	
	@DisplayName("M�s/ano")
	@Transient
	public String getMesanoAux() {
		if(this.dtinventario != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtinventario);
		return "";
	}
	
	public void setMesanoAux(String mesanoAux) {
		try {
			if(mesanoAux != null){
				this.dtinventario = new Date(new SimpleDateFormat("MM/yyyy").parse(mesanoAux).getTime());
				this.dtinventario = SinedDateUtils.lastDateOfMonth(this.dtinventario);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	@DisplayName("Motivo do Invent�rio")
	public Motivoinventario getMotivoinventario() {
		return motivoinventario;
	}
	public void setMotivoinventario(Motivoinventario motivoinventario) {
		this.motivoinventario = motivoinventario;
	}
	
	@DisplayName("Conta anal�tica cont�bil")
	public String getCodigocontaanalitica() {
		return codigocontaanalitica;
	}
	public void setCodigocontaanalitica(String codigocontaanalitica) {
		this.codigocontaanalitica = codigocontaanalitica;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
	@DisplayName("")
	public Boolean getCorrecao() {
		return correcao;
	}
	public void setCorrecao(Boolean correcao) {
		this.correcao = correcao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinventarioorigem")
	public Inventario getInventarioOrigem() {
		return inventarioOrigem;
	}
	public void setInventarioOrigem(Inventario inventarioOrigem) {
		this.inventarioOrigem = inventarioOrigem;
	}
	
	public Inventario ajustaBeanToLoad(){
		if(Hibernate.isInitialized(getListaInventariomaterial()) && SinedUtil.isListNotEmpty(getListaInventariomaterial())){
			for(Inventariomaterial im : getListaInventariomaterial()){
				im.ajustaBeanToLoad();
			}
		}
		return this;
	}
	
	@DisplayName("Tipo de Invent�rio")
	@Column(name="inventariotipo")
	public InventarioTipo getInventarioTipo() {
		return inventarioTipo;
	}
	public void setInventarioTipo(InventarioTipo inventarioTipo) {
		this.inventarioTipo = inventarioTipo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdinventario == null) ? 0 : cdinventario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventario other = (Inventario) obj;
		if (cdinventario == null) {
			if (other.cdinventario != null)
				return false;
		} else if (!cdinventario.equals(other.cdinventario))
			return false;
		return true;
	}
	
	
}
