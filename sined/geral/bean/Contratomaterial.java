package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contratomaterial", sequenceName = "sq_contratomaterial")
@DisplayName("Produtos e Servi�os")
public class Contratomaterial implements Log {

	protected Integer cdcontratomaterial;
	protected Contrato contrato;
	protected Material materialpromocional;
	protected Material servico;
	protected Patrimonioitem patrimonioitem;
	protected Date dtinicio;
	protected Date dtfim;
	protected Hora hrinicio;
	protected Hora hrfim;
	protected Double valorunitario;
	protected Double qtde;
	protected Integer periodocobranca;
	protected Money valordesconto;
	protected Boolean detalhamento;
	protected Indice indice;
	protected Double valorfechado;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String observacao;
	
	//TRASIENTES
	protected Money valortotal;
	protected String descricao;
	protected Integer cdservicoservidortipo;
	protected String descricaotip;
	protected String imagedescricao;
	protected Double qtdeauxfaturalocacao;
	protected Money valortotalfaturalocacao;
	protected Material materialinclusao;
	protected Double qtdematerialinclusao;
	protected Cargo cargoinclusao;
	protected Double qtdecargoinclusao;
	protected Localarmazenagem localarmazenagem;
	protected Cliente cliente;
	protected Empresa empresa;
	protected Frequencia frequencia;
	protected Boolean considerarDescricaoTipoLocacaoComMaterial;
	protected Double qtderomaneio;
	protected Money valorbrutoForCriacaoNota;
	protected Boolean considerarQtdeTotalItem;
	protected Double valorfechadoCalculado;
	protected String codigo;
	
	//LISTAS
	protected List<Contratomaterialrede> listaContratomaterialrede = new ListSet<Contratomaterialrede>(Contratomaterialrede.class);
	protected List<Contratomaterialpessoa> listaContratomaterialpessoa = new ListSet<Contratomaterialpessoa>(Contratomaterialpessoa.class);
	protected List<Contratomaterialitem> listaContratomaterialitem = new ListSet<Contratomaterialitem>(Contratomaterialitem.class);
	protected List<Contratomateriallocacao> listaContratomateriallocacao = new ListSet<Contratomateriallocacao>(Contratomateriallocacao.class);
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratomaterial")
	public Integer getCdcontratomaterial() {
		return cdcontratomaterial;
	}
	
	@Required
	@DisplayName("Produto/Servi�o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservico")	
	public Material getServico() {
		return servico;
	}
	
	@DisplayName("Promocional")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialpromocional")
	public Material getMaterialpromocional() {
		return materialpromocional;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")	
	public Contrato getContrato() {
		return contrato;
	}

	@Required
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}

	@DisplayName("Data de fim")
	public Date getDtfim() {
		return dtfim;
	}

	@Required
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}

	@Required
	@DisplayName("Qtde.")
	public Double getQtde() {
		return qtde;
	}
	
	@DisplayName("Desconto")
	public Money getValordesconto() {
		return valordesconto;
	}
	
	@Required
	@DisplayName("Per�odo para cobran�a")
	public Integer getPeriodocobranca() {
		return periodocobranca;
	}

	@DisplayName("Hora in�cio")
	public Hora getHrinicio() {
		return hrinicio;
	}

	@DisplayName("Hora fim")
	public Hora getHrfim() {
		return hrfim;
	}
	
	public Boolean getDetalhamento() {
		return detalhamento;
	}

	@DisplayName("Composi��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindice")	
	public Indice getIndice() {
		return indice;
	}
	
	@DisplayName("Patrim�nio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpatrimonioitem")
	public Patrimonioitem getPatrimonioitem() {
		return patrimonioitem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@OneToMany(mappedBy="contratomaterial")
	public List<Contratomaterialitem> getListaContratomaterialitem() {
		return listaContratomaterialitem;
	}
	
	@OneToMany(mappedBy="contratomaterial")
	public List<Contratomaterialpessoa> getListaContratomaterialpessoa() {
		return listaContratomaterialpessoa;
	}
	
	@OneToMany(mappedBy="contratomaterial")
	public List<Contratomaterialrede> getListaContratomaterialrede() {
		return listaContratomaterialrede;
	}
	
	@OneToMany(mappedBy="contratomaterial")
	public List<Contratomateriallocacao> getListaContratomateriallocacao() {
		return listaContratomateriallocacao;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(1000)
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setPatrimonioitem(Patrimonioitem patrimonioitem) {
		this.patrimonioitem = patrimonioitem;
	}

	public void setListaContratomateriallocacao(List<Contratomateriallocacao> listaContratomateriallocacao) {
		this.listaContratomateriallocacao = listaContratomateriallocacao;
	}

	public void setListaContratomaterialrede(List<Contratomaterialrede> listaContratomaterialrede) {
		this.listaContratomaterialrede = listaContratomaterialrede;
	}
	
	public void setListaContratomaterialitem(
			List<Contratomaterialitem> listaContratomaterialitem) {
		this.listaContratomaterialitem = listaContratomaterialitem;
	}
	
	public void setListaContratomaterialpessoa(
			List<Contratomaterialpessoa> listaContratomaterialpessoa) {
		this.listaContratomaterialpessoa = listaContratomaterialpessoa;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setDetalhamento(Boolean detalhamento) {
		this.detalhamento = detalhamento;
	}

	public void setIndice(Indice indice) {
		this.indice = indice;
	}

	public void setHrinicio(Hora hrinicio) {
		this.hrinicio = hrinicio;
	}

	public void setHrfim(Hora hrfim) {
		this.hrfim = hrfim;
	}

	public void setPeriodocobranca(Integer periodocobranca) {
		this.periodocobranca = periodocobranca;
	}
	
	public void setCdcontratomaterial(Integer cdcontratomaterial) {
		this.cdcontratomaterial = cdcontratomaterial;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setMaterialpromocional(Material materialpromocional) {
		this.materialpromocional = materialpromocional;
	}

	public void setServico(Material servico) {
		this.servico = servico;
	}

	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}

	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}

	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}

	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}	
	
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	
	// TRANSIENTE
	@Transient
	@DisplayName("Valor total")
	public Money getValortotal() {
		if(this.qtde != null && this.valorunitario != null){
			Money valortotal = new Money(this.valorunitario).multiply(new Money(this.qtde));
			if(this.periodocobranca != null){
				valortotal = valortotal.multiply(new Money(this.periodocobranca));
			}
			if(this.valordesconto != null){
				valortotal = valortotal.subtract(this.valordesconto);
			}
			return valortotal;
		}
		return valortotal;
	}
	
	public void setValortotal(Money valortotal) {
		this.valortotal = valortotal;
	}
	
	@Transient
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Transient
	public Integer getCdservicoservidortipo() {
		return cdservicoservidortipo;
	}
	public void setCdservicoservidortipo(Integer cdservicoservidortipo) {
		this.cdservicoservidortipo = cdservicoservidortipo;
	}
	
	@Transient
	public String getDescricaotip() {
		return descricaotip;
	}
	
	public void setDescricaotip(String descricaotip) {
		this.descricaotip = descricaotip;
	}

	@Transient
	public String getImagedescricao() {
		return imagedescricao;
	}

	public void setImagedescricao(String imagedescricao) {
		this.imagedescricao = imagedescricao;
	}

	@Transient
	public Double getQtdeauxfaturalocacao() {
		return qtdeauxfaturalocacao;
	}
	public void setQtdeauxfaturalocacao(Double qtdeauxfaturalocacao) {
		this.qtdeauxfaturalocacao = qtdeauxfaturalocacao;
	}

	@Transient
	public Money getValortotalfaturalocacao() {
		if((this.qtde != null || this.qtdeauxfaturalocacao != null) && this.valorunitario != null){
			Money valortotal = new Money(this.valorunitario).multiply(new Money(this.qtde != null ? this.qtde : this.qtdeauxfaturalocacao));
			if(this.periodocobranca != null){
				valortotal = valortotal.multiply(new Money(this.periodocobranca));
			}
			if(this.valordesconto != null){
				valortotal = valortotal.subtract(this.valordesconto);
			}
			return valortotal;
		}
		return valortotalfaturalocacao;
	}
	public void setValortotalfaturalocacao(Money valortotalfaturalocacao) {
		this.valortotalfaturalocacao = valortotalfaturalocacao;
	}
	@Transient
	public Money getValortotalcusto(){
		if((this.qtde != null || this.getServico() != null) && this.getServico().getValorcusto() != null){
			Money valortotal = new Money(this.getServico().getValorcusto() * this.qtde);
			if(this.periodocobranca != null){
				valortotal = valortotal.multiply(new Money(this.periodocobranca));
			}
			return valortotal;
		}
		return null;
	}

	@Transient
	@DisplayName("Material")
	public Material getMaterialinclusao() {
		return materialinclusao;
	}

	@Transient
	@DisplayName("Qtde.")
	public Double getQtdematerialinclusao() {
		return qtdematerialinclusao;
	}

	@Transient
	@DisplayName("Cargo")
	public Cargo getCargoinclusao() {
		return cargoinclusao;
	}

	@Transient
	@DisplayName("Qtde.")
	public Double getQtdecargoinclusao() {
		return qtdecargoinclusao;
	}

	public void setMaterialinclusao(Material materialinclusao) {
		this.materialinclusao = materialinclusao;
	}

	public void setQtdematerialinclusao(Double qtdematerialinclusao) {
		this.qtdematerialinclusao = qtdematerialinclusao;
	}

	public void setCargoinclusao(Cargo cargoinclusao) {
		this.cargoinclusao = cargoinclusao;
	}

	public void setQtdecargoinclusao(Double qtdecargoinclusao) {
		this.qtdecargoinclusao = qtdecargoinclusao;
	}

	@DisplayName("Pre�o Fechado")
	public Double getValorfechado() {
		return valorfechado;
	}
	public void setValorfechado(Double valorfechado) {
		this.valorfechado = valorfechado;
	}
	
	@Transient
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	@Transient
	public Cliente getCliente() {
		return cliente;
	}
	@Transient
	public Empresa getEmpresa() {
		return empresa;
	}
	@Transient
	public Frequencia getFrequencia() {
		return frequencia;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}
	
	@Transient
	public Double getValorfechadoOrValorunitario() {
		if(getValorfechadoCalculado() != null && getValorfechadoCalculado() > 0){
			return getValorfechadoCalculado();
		}else if(getValorfechado() != null && getValorfechado() > 0){
			return getValorfechado();
		}else {
			return getValorunitario();
		}
	} 
	
	@Transient
	public Boolean getConsiderarDescricaoTipoLocacaoComMaterial() {
		return considerarDescricaoTipoLocacaoComMaterial;
	}
	public void setConsiderarDescricaoTipoLocacaoComMaterial(Boolean considerarDescricaoTipoLocacaoComMaterial) {
		this.considerarDescricaoTipoLocacaoComMaterial = considerarDescricaoTipoLocacaoComMaterial;
	}

	@Transient
	public Double getQtderomaneio() {
		return qtderomaneio;
	}
	
	public void setQtderomaneio(Double qtderomaneio) {
		this.qtderomaneio = qtderomaneio;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcontratomaterial == null) ? 0 : cdcontratomaterial
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contratomaterial other = (Contratomaterial) obj;
		if (cdcontratomaterial == null) {
			if (other.cdcontratomaterial != null)
				return false;
		} else if (!cdcontratomaterial.equals(other.cdcontratomaterial))
			return false;
		return true;
	}
	
	@Transient
	public Money getValorbrutoForCriacaoNota() {
		if(this.getValorfechadoOrValorunitario() != null && this.getQtde() != null){
			valorbrutoForCriacaoNota = new Money(this.getValorfechadoOrValorunitario()).multiply(new Money(this.getQtde()));
		}
		return valorbrutoForCriacaoNota;
	}
	public void setValorbrutoForCriacaoNota(Money valorbrutoForCriacaoNota) {
		this.valorbrutoForCriacaoNota = valorbrutoForCriacaoNota;
	}

	@Transient
	public Boolean getConsiderarQtdeTotalItem() {
		return considerarQtdeTotalItem;
	}

	public void setConsiderarQtdeTotalItem(Boolean considerarQtdeTotalItem) {
		this.considerarQtdeTotalItem = considerarQtdeTotalItem;
	}

	@Transient
	public Double getValorfechadoCalculado() {
		return valorfechadoCalculado;
	}

	public void setValorfechadoCalculado(Double valorfechadoCalculado) {
		this.valorfechadoCalculado = valorfechadoCalculado;
	}
	
	@Transient
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}