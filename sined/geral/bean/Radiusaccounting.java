package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
public class Radiusaccounting implements Log{
	
	protected Integer radacctid;
	protected String username;
	protected String framedipaddress;
	protected String nasportid;
	protected String callingstationid;
	protected String calledstationid;
	protected Timestamp acctstarttime;
	protected Timestamp acctstoptime;
	protected Integer acctsessiontime;
	protected Long acctinputoctets;
	protected Long acctoutputoctets;
	protected String nasipaddress;
	
	//Transient
	protected String duracao;
	protected Servidor servidor;

	@Id	
	public Integer getRadacctid() {
		return radacctid;
	}
	
	@DisplayName("Cliente")
	public String getUsername() {
		return username;
	}
	@DisplayName("Endere�o Ip")
	public String getFramedipaddress() {
		return framedipaddress;
	}
	@DisplayName("Porta")
	public String getNasportid() {
		return nasportid;
	}
	@DisplayName("Mac")
	public String getCallingstationid() {
		return callingstationid;
	}
	@DisplayName("In�cio")
	public Timestamp getAcctstarttime() {
		return acctstarttime;
	}
	@DisplayName("Fim")
	public Timestamp getAcctstoptime() {
		return acctstoptime;
	}
	@DisplayName("Dura��o")
	public Integer getAcctsessiontime() {
		return acctsessiontime;
	}
	@DisplayName("Entrada")
	public Long getAcctinputoctets() {
		return acctinputoctets;
	}
	@DisplayName("Sa�da")
	public Long getAcctoutputoctets() {
		return acctoutputoctets;
	}
	@DisplayName("Ip da torre")
	public String getNasipaddress() {
		return nasipaddress;
	}
	@DisplayName("Servidor")
	public String getCalledstationid() {
		return calledstationid;
	}
	public void setAcctinputoctets(Long acctinputoctets) {
		this.acctinputoctets = acctinputoctets;
	}
	public void setAcctoutputoctets(Long acctoutputoctets) {
		this.acctoutputoctets = acctoutputoctets;
	}
	public void setRadacctid(Integer radacctid) {
		this.radacctid = radacctid;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setFramedipaddress(String framedipaddress) {
		this.framedipaddress = framedipaddress;
	}
	public void setNasportid(String nasportid) {
		this.nasportid = nasportid;
	}
	public void setCallingstationid(String callingstationid) {
		this.callingstationid = callingstationid;
	}
	public void setAcctstarttime(Timestamp acctstarttime) {
		this.acctstarttime = acctstarttime;
	}
	public void setAcctstoptime(Timestamp acctstoptime) {
		this.acctstoptime = acctstoptime;
	}
	public void setAcctsessiontime(Integer acctsessiontime) {
		this.acctsessiontime = acctsessiontime;
	}
	public void setNasipaddress(String nasipaddress) {
		this.nasipaddress = nasipaddress;
	}
	public void setCalledstationid(String calledstationid) {
		this.calledstationid = calledstationid;
	}
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}
	public void setDtaltera(Timestamp dtaltera) {
	}
	
	@Transient
	@DisplayName("Dura��o (h:m:s)")
	public String getDuracao() {		
		
		Date hoje = new java.util.Date();
		
		long segundos = (hoje.getTime() - acctstarttime.getTime())/1000;
		if (segundos < 0 )  
			return "";
		int hora = (int)(segundos / 3600);
		segundos -= hora * 3600;
		int minutos = (int)(segundos / 60);
		segundos -= minutos * 60;
		
		return hora + ":" + (minutos < 10 ? "0" : "") + minutos + ":" + (segundos < 10 ? "0" : "") + segundos;
	};
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	
	@Transient
	public Servidor getServidor() {
		return servidor;
	}
	
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
}