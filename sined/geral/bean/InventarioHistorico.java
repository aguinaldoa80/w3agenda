package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.InventarioAcao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_inventariohistorico", sequenceName = "sq_inventariohistorico")
public class InventarioHistorico implements Log {

	protected Integer cdInventarioHistorico;
	protected Inventario inventario;
	protected InventarioAcao inventarioAcao;
	protected String observacao;
	
	// LOG
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//TRANSIENTE
	protected String whereIn;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_inventariohistorico")
	public Integer getCdInventarioHistorico() {
		return cdInventarioHistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdinventario")	
	public Inventario getInventario() {
		return inventario;
	}

	@DisplayName("A��o")
	public InventarioAcao getInventarioAcao() {
		return inventarioAcao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdInventarioHistorico(Integer cdInventarioHistorico) {
		this.cdInventarioHistorico = cdInventarioHistorico;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	public void setInventarioAcao(InventarioAcao inventarioAcao) {
		this.inventarioAcao = inventarioAcao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}

}
