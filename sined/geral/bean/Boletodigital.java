package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.sined.geral.bean.enumeration.W3erpBankStatusIntegracao;

@Entity
@SequenceGenerator(name = "sq_boletodigital", sequenceName = "sq_boletodigital")
public class Boletodigital {

	protected Integer cdboletodigital;
	protected Documento documento;
	protected String id;
	protected String status;
	protected String nossonumero;
	protected String codigodebarras;
	protected String linhadigitavel;
	protected String cliente_id;
	protected Boolean registrado;
	protected Double valorliquidado;
	protected Double valorpago;
	protected String carteira;
	protected Date pagamento;
	protected Date cancelamento;
	protected W3erpBankStatusIntegracao statusintegracao = W3erpBankStatusIntegracao.SINCRONIZAR;
	protected String msgintegracao;
	protected Date vencimentooriginal;
	protected Date vencimento;
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sq_boletodigital")
	public Integer getCdboletodigital() {
		return cdboletodigital;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	public String getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public String getNossonumero() {
		return nossonumero;
	}

	public String getCodigodebarras() {
		return codigodebarras;
	}

	public String getLinhadigitavel() {
		return linhadigitavel;
	}

	public String getCliente_id() {
		return cliente_id;
	}

	public Boolean getRegistrado() {
		return registrado;
	}

	public Double getValorliquidado() {
		return valorliquidado;
	}

	public Double getValorpago() {
		return valorpago;
	}

	public String getCarteira() {
		return carteira;
	}

	public Date getPagamento() {
		return pagamento;
	}

	public Date getCancelamento() {
		return cancelamento;
	}

	public W3erpBankStatusIntegracao getStatusintegracao() {
		return statusintegracao;
	}

	public String getMsgintegracao() {
		return msgintegracao;
	}

	public Date getVencimentooriginal() {
		return vencimentooriginal;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimentooriginal(Date vencimentooriginal) {
		this.vencimentooriginal = vencimentooriginal;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public void setCdboletodigital(Integer cdboletodigital) {
		this.cdboletodigital = cdboletodigital;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}

	public void setCodigodebarras(String codigodebarras) {
		this.codigodebarras = codigodebarras;
	}

	public void setLinhadigitavel(String linhadigitavel) {
		this.linhadigitavel = linhadigitavel;
	}

	public void setCliente_id(String cliente_id) {
		this.cliente_id = cliente_id;
	}

	public void setRegistrado(Boolean registrado) {
		this.registrado = registrado;
	}

	public void setValorliquidado(Double valorliquidado) {
		this.valorliquidado = valorliquidado;
	}

	public void setValorpago(Double valorpago) {
		this.valorpago = valorpago;
	}

	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	public void setPagamento(Date pagamento) {
		this.pagamento = pagamento;
	}

	public void setCancelamento(Date cancelamento) {
		this.cancelamento = cancelamento;
	}

	public void setStatusintegracao(W3erpBankStatusIntegracao statusintegracao) {
		this.statusintegracao = statusintegracao;
	}

	public void setMsgintegracao(String msgintegracao) {
		this.msgintegracao = msgintegracao;
	}
	
	
}
