package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.ColaboradorescalaSituacao;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.SinedDateUtils;


@Entity
@DisplayName("Escala do colaborador")
@SequenceGenerator(name = "sq_colaboradorescala", sequenceName = "sq_colaboradorescala")
public class Colaboradorescala implements Log {

	protected Integer cdcolaboradorescala;
	protected Colaborador colaborador;
	protected Date dtescala;
	protected Contrato contrato;
	protected Hora horainicio;
	protected Hora horafim;
	protected Double horarealizada;
	protected ColaboradorescalaSituacao situacao;
	protected Colaboradorescala escalasubstituida;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Colaboradorescala> listaSubstituto = new ListSet<Colaboradorescala>(Colaboradorescala.class);
	
	
//	TRANSIENTE
	protected Double horaprevista;
	protected DiaSemana diaSemana;
	protected Boolean escolheSubtituto;
	protected Colaborador colaboradorTrans;
	protected Date dtescalaTrans;
	protected Contrato contratoTrans;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorescala")
	public Integer getCdcolaboradorescala() {
		return cdcolaboradorescala;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}

	@Required
	@DisplayName("Data")
	public Date getDtescala() {
		return dtescala;
	}

	@Required
	@DisplayName("Hor�rio inicial")
	public Hora getHorainicio() {
		return horainicio;
	}

	@Required
	@DisplayName("Hor�rio final")
	public Hora getHorafim() {
		return horafim;
	}

	

	@DisplayName("Horas realizadas")
	public Double getHorarealizada() {
		return horarealizada;
	}
	
	@Required
	@DisplayName("Situa��o")
	public ColaboradorescalaSituacao getSituacao() {
		return situacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdescalasubstituida")
	public Colaboradorescala getEscalasubstituida() {
		return escalasubstituida;
	}
	
	@DisplayName("Substitutos") 
	@OneToMany(mappedBy="escalasubstituida")
	public List<Colaboradorescala> getListaSubstituto() {
		return listaSubstituto;
	}
	
	public void setListaSubstituto(List<Colaboradorescala> listaSubstituto) {
		this.listaSubstituto = listaSubstituto;
	}
	
	public void setEscalasubstituida(Colaboradorescala escalasubstituida) {
		this.escalasubstituida = escalasubstituida;
	}
	
	public void setSituacao(ColaboradorescalaSituacao situacao) {
		this.situacao = situacao;
	}

	public void setCdcolaboradorescala(Integer cdcolaboradorescala) {
		this.cdcolaboradorescala = cdcolaboradorescala;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setDtescala(Date dtescala) {
		this.dtescala = dtescala;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public void setHorainicio(Hora horainicio) {
		this.horainicio = horainicio;
	}

	public void setHorafim(Hora horafim) {
		this.horafim = horafim;
	}

	public void setHorarealizada(Double horarealizada) {
		this.horarealizada = horarealizada;
	}

	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	
	@Transient
	@DisplayName("Dia da semana")
	public DiaSemana getDiaSemana() {
		if(getDtescala() != null){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getDtescala());
			
			return DiaSemana.values()[calendar.get(Calendar.DAY_OF_WEEK) - 1];
		} else return null;
		
	}
	
	@Transient
	@DisplayName("Horas previstas")
	public Double getHoraprevista() {
		if(getHorainicio() != null && getHorafim() != null){
			return SinedDateUtils.calculaDiferencaHoras(getHorainicio().toString(), getHorafim().toString());
		}
		return 0.0;
	}
	
	public void setDiaSemana(DiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}
	
	public void setHoraprevista(Double horaprevista) {
		this.horaprevista = horaprevista;
	}
	
	@Transient
	@DisplayName("Hor�rio inicial")
	public String getHoraInicioFormat() {
		if(getHorainicio() != null){
			return getHorainicio().toString();
		} else return null;
	}
	
	@Transient
	@DisplayName("Hor�rio final")
	public String getHoraFimFormat() {
		if(getHorafim() != null){
			return getHorafim().toString();
		} else return null;
	}

	@Transient
	@DisplayName("Escolher substitutos")
	public Boolean getEscolheSubtituto() {
		return escolheSubtituto;
	}

	public void setEscolheSubtituto(Boolean escolheSubtituto) {
		this.escolheSubtituto = escolheSubtituto;
	}

	@Transient
	@DisplayName("Colaborador")
	public Colaborador getColaboradorTrans() {
		return getColaborador();
	}

	@Transient
	@DisplayName("Data")
	public Date getDtescalaTrans() {
		return getDtescala();
	}

	@Transient
	@DisplayName("Contrato")
	public Contrato getContratoTrans() {
		return getContrato();
	}

	public void setColaboradorTrans(Colaborador colaboradorTrans) {
		this.colaboradorTrans = colaboradorTrans;
	}

	public void setDtescalaTrans(Date dtescalaTrans) {
		this.dtescalaTrans = dtescalaTrans;
	}

	public void setContratoTrans(Contrato contratoTrans) {
		this.contratoTrans = contratoTrans;
	}
	
	
	
	
	
}
