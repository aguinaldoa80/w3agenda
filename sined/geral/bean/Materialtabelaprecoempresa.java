package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialtabelaprecoempresa", sequenceName = "sq_materialtabelaprecoempresa")
public class Materialtabelaprecoempresa {

	protected Integer cdmaterialtabelaprecoempresa;
	protected Materialtabelapreco materialtabelapreco;
	protected Empresa empresa;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialtabelaprecoempresa")
	public Integer getCdmaterialtabelaprecoempresa() {
		return cdmaterialtabelaprecoempresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setCdmaterialtabelaprecoempresa(Integer cdmaterialtabelaprecoempresa) {
		this.cdmaterialtabelaprecoempresa = cdmaterialtabelaprecoempresa;
	}

	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
