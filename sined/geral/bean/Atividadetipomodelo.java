package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_atividadetipomodelo", sequenceName="sq_atividadetipomodelo")
public class Atividadetipomodelo {
	
	private Integer cdatividadetipomodelo;
	private Atividadetipo atividadetipo;
	private ReportTemplateBean reportTemplate;
	
	public Atividadetipomodelo(){
	}
	
	public Atividadetipomodelo(Integer cdatividadetipomodelo){
		this.cdatividadetipomodelo = cdatividadetipomodelo;
	}
	
	@Id
	@GeneratedValue(generator="sq_atividadetipomodelo", strategy=GenerationType.AUTO)
	public Integer getCdatividadetipomodelo() {
		return cdatividadetipomodelo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdatividadetipo")	
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdreporttemplate")
	@DisplayName("Modelo")
	public ReportTemplateBean getReportTemplate() {
		return reportTemplate;
	}
	
	public void setCdatividadetipomodelo(Integer cdatividadetipomodelo) {
		this.cdatividadetipomodelo = cdatividadetipomodelo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setReportTemplate(ReportTemplateBean reportTemplate) {
		this.reportTemplate = reportTemplate;
	}
}