package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_custeio_historico", sequenceName = "sq_custeio_historico")
@DisplayName("Hist�rico")
public class CusteioHistorico implements Log{
	

	private Integer cdcusteiohistorico;
	private SituacaoCusteio situacaoCusteio;
	private String observacao;
	private Timestamp dtaltera;
	private Integer cdusuarioaltera;
	private CusteioProcessado custeioProcessado;
	// get
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_custeio_historico")
	public Integer getCdcusteiohistorico() {
		return cdcusteiohistorico;
	}
	@DisplayName("Situa��o")
	public SituacaoCusteio getSituacaoCusteio() {
		return situacaoCusteio;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Usu�rio")
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Custeio")
	@JoinColumn(name = "cdcusteioprocessado")
	@ManyToOne(fetch = FetchType.LAZY)
	public CusteioProcessado getCusteioProcessado() {
		return custeioProcessado;
	}
	// set 
	public void setCdcusteiohistorico(Integer cdcusteiohistorico) {
		this.cdcusteiohistorico = cdcusteiohistorico;
	}
	public void setSituacaoCusteio(SituacaoCusteio situacaoCusteio) {
		this.situacaoCusteio = situacaoCusteio;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setCusteioProcessado(CusteioProcessado custeioProcessado) {
		this.custeioProcessado = custeioProcessado;
	}
	
	
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	
	
}
