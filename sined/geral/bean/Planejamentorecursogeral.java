package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name="sq_planejamentorecursogeral",sequenceName="sq_planejamentorecursogeral")
public class Planejamentorecursogeral implements Log, PermissaoProjeto {
	
	protected Integer cdplanejamentorecursogeral;
	protected Material material;
	protected Double qtde;
	protected Date dtinicio;
	protected Date dtfim;
	protected Planejamento planejamento;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<Solicitacaocompraorigem> listaSolicitacaocompraorigem;
	protected List<Ordemcompramaterial> listaOrdemcompramaterial;
	protected List<Requisicaoorigem> listaRequisicaoorigem;
	
	public Planejamentorecursogeral() {
	}
	
	public Planejamentorecursogeral(Integer cdplanejamentorecursogeral) {
		this.cdplanejamentorecursogeral = cdplanejamentorecursogeral;
	}
	
	@Id
	@GeneratedValue(generator="sq_planejamentorecursogeral",strategy=GenerationType.AUTO)
	public Integer getCdplanejamentorecursogeral() {
		return cdplanejamentorecursogeral;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@Required
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Qtde.")
	@MaxLength(9)
	public Double getQtde() {
		return qtde;
	}
	@DisplayName("Data de in�cio")
	public Date getDtinicio() {
		return dtinicio;
	}
	@DisplayName("Data de fim")
	public Date getDtfim() {
		return dtfim;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdplanejamento")
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdplanejamentorecursogeral(Integer cdplanejamentorecursogeral) {
		this.cdplanejamentorecursogeral = cdplanejamentorecursogeral;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@OneToMany(mappedBy="planejamentorecursogeral")
	public List<Solicitacaocompraorigem> getListaSolicitacaocompraorigem() {
		return listaSolicitacaocompraorigem;
	}
	@OneToMany(mappedBy="planejamentorecursogeral")
	public List<Ordemcompramaterial> getListaOrdemcompramaterial() {
		return listaOrdemcompramaterial;
	}
	@OneToMany(mappedBy="planejamentorecursogeral")
	public List<Requisicaoorigem> getListaRequisicaoorigem() {
		return listaRequisicaoorigem;
	}
	
	public void setListaSolicitacaocompraorigem(List<Solicitacaocompraorigem> listaSolicitacaocompraorigem) {
		this.listaSolicitacaocompraorigem = listaSolicitacaocompraorigem;
	}
	public void setListaOrdemcompramaterial(List<Ordemcompramaterial> listaOrdemcompramaterial) {
		this.listaOrdemcompramaterial = listaOrdemcompramaterial;
	}
	public void setListaRequisicaoorigem(List<Requisicaoorigem> listaRequisicaoorigem) {
		this.listaRequisicaoorigem = listaRequisicaoorigem;
	}

	public String subQueryProjeto() {
		return "select planejamentorecursogeralsubQueryProjeto.cdplanejamentorecursogeral " +
				"from Planejamentorecursogeral planejamentorecursogeralsubQueryProjeto " +
				"join planejamentorecursogeralsubQueryProjeto.planejamento planejamentosubQueryProjeto " +
				"join planejamentosubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ")";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}
}
