package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_empresaproprietario", sequenceName = "sq_empresaproprietario")
public class Empresaproprietario {

	protected Integer cdempresaproprietario;
	protected Colaborador colaborador;
	protected Empresa empresa;
	protected Boolean principal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_empresaproprietario")
	public Integer getCdempresaproprietario() {
		return cdempresaproprietario;
	}
	public void setCdempresaproprietario(Integer cdempresaproprietario) {
		this.cdempresaproprietario = cdempresaproprietario;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Colaborador")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public Boolean getPrincipal() {
		return principal;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
}
