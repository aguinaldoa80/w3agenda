package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaorelacionado;
import br.com.linkcom.sined.geral.bean.enumeration.Agendainteracaosituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoagendainteracao;
import br.com.linkcom.sined.modulo.suprimentos.controller.process.bean.Pessoaquestionario;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoClienteEmpresa;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_agendainteracao", sequenceName = "sq_agendainteracao")
@DisplayName("Agenda de intera��o")
public class Agendainteracao implements Log, PermissaoClienteEmpresa{
	
	protected Integer cdagendainteracao; 
	protected Cliente cliente;
	protected Contacrm contacrm;
	protected Material material;
	protected Contrato contrato;
	protected Atividadetipo atividadetipo;
	protected Animal animal;
	protected java.sql.Date dtproximainteracao;
	protected Date dtcancelamento;
	protected Date dtconclusao;
	protected Colaborador responsavel;
	protected Frequencia periodicidade;
	protected Integer qtdefrequencia;
	protected Agendainteracaorelacionado agendainteracaorelacionado;  
	protected Agendainteracaosituacao agendainteracaosituacao;  
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Tipoagendainteracao tipoagendainteracao = Tipoagendainteracao.CLIENTE;
	protected String descricao;
	protected Empresa empresahistorico;
	protected String contato;
	
	protected List<Agendainteracaohistorico> listaAgendainteracaohistorico;
	protected Set<Pessoaquestionario> listaQuestionarioServicos;
	

	//TRANSIENTE
	private Integer situacao;
	
	public Agendainteracao(Integer cdagendainteracao) {
		this.cdagendainteracao = cdagendainteracao;
	}
	
	public Agendainteracao() {}
	
	public Agendainteracao(Integer cdcliente, Date dtproximainteracao) {
		this.cliente = new Cliente(cdcliente);
		this.dtproximainteracao = dtproximainteracao != null ? new java.sql.Date(dtproximainteracao.getTime()) : null;
	}

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendainteracao")
	public Integer getCdagendainteracao() {
		return cdagendainteracao;
	}

	@JoinColumn(name="cdcliente")
	@ManyToOne(fetch=FetchType.LAZY)
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Conta CRM")
	@JoinColumn(name="cdcontacrm")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contacrm getContacrm() {
		return contacrm;
	}

	@DisplayName("Material/Servi�o")
	@JoinColumn(name="cdmaterial")
	@ManyToOne(fetch=FetchType.LAZY)
	public Material getMaterial() {
		return material;
	}

	@JoinColumn(name="cdcontrato")
	@ManyToOne(fetch=FetchType.LAZY)
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}

	@Required
	@DisplayName("Pr�xima intera��o")
	public java.sql.Date getDtproximainteracao() {
		return dtproximainteracao;
	}
	
	public Date getDtcancelamento() {
		return dtcancelamento;
	}
	
	public Date getDtconclusao() {
		return dtconclusao;
	}
	
	@Required
	@DisplayName("Respons�vel")
	@JoinColumn(name="cdresponsavel")
	@ManyToOne(fetch=FetchType.LAZY)
	public Colaborador getResponsavel() {
		return responsavel;
	}
	
	@Required
	@JoinColumn(name="cdfrequencia")
	@ManyToOne(fetch=FetchType.LAZY)
	public Frequencia getPeriodicidade() {
		return periodicidade;
	}
	
	@DisplayName("Situa��o")
	@Transient
	public Agendainteracaosituacao getAgendainteracaosituacao() {
		if(this.getDtcancelamento() != null){
			agendainteracaosituacao = Agendainteracaosituacao.CANCELADO;
		}else if(this.getDtconclusao() != null){
			agendainteracaosituacao = Agendainteracaosituacao.CONCLUIDO;
		}else{
			java.sql.Date dataAtual = new java.sql.Date(System.currentTimeMillis());
			java.sql.Date proximaInteracao = this.getDtproximainteracao();
			Integer diferencaData = SinedDateUtils.diferencaDias(proximaInteracao, dataAtual);
			
			if(diferencaData > 5){
				agendainteracaosituacao = Agendainteracaosituacao.NORMAL;
			}else if(diferencaData <= 5 && diferencaData >= 0){
				agendainteracaosituacao = Agendainteracaosituacao.ATENCAO;
			}else if(diferencaData < 0){
				agendainteracaosituacao = Agendainteracaosituacao.ATRASADO;
			}else{
				agendainteracaosituacao = Agendainteracaosituacao.NORMAL;
			}
		}
		return agendainteracaosituacao;
	}
	
	public void setAgendainteracaosituacao(
			Agendainteracaosituacao agendainteracaosituacao) {
		this.agendainteracaosituacao = agendainteracaosituacao;
	}
	
	@DisplayName("Relacionado a")
	@Column(name="relacionado")
	@Required
	public Agendainteracaorelacionado getAgendainteracaorelacionado() {
		return agendainteracaorelacionado;
	}
	
	@DisplayName("Frequ�ncia")
	public Integer getQtdefrequencia() {
		return qtdefrequencia;
	}
	@Required
	@DisplayName("Tipo")
	public Tipoagendainteracao getTipoagendainteracao() {
		return tipoagendainteracao;
	}
	@DisplayName("Descri��o")
	@MaxLength(2000)
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="agendainteracao")
	public List<Agendainteracaohistorico> getListaAgendainteracaohistorico() {
		return listaAgendainteracaohistorico;
	}
	
	@Required
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresahistorico")
	public Empresa getEmpresahistorico() {
		return empresahistorico;
	}

	public void setListaAgendainteracaohistorico(
			List<Agendainteracaohistorico> listaAgendainteracaohistorico) {
		this.listaAgendainteracaohistorico = listaAgendainteracaohistorico;
	}
	
	public void setDtconclusao(Date dtconclusao) {
		this.dtconclusao = dtconclusao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTipoagendainteracao(Tipoagendainteracao tipoagendainteracao) {
		this.tipoagendainteracao = tipoagendainteracao;
	}

	public void setQtdefrequencia(Integer qtdefrequencia) {
		this.qtdefrequencia = qtdefrequencia;
	}

	public void setCdagendainteracao(Integer cdagendainteracao) {
		this.cdagendainteracao = cdagendainteracao;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	public void setDtproximainteracao(java.sql.Date dtproximainteracao) {
		this.dtproximainteracao = dtproximainteracao;
	}
	public void setDtcancelamento(Date dtcancelamento) {
		this.dtcancelamento = dtcancelamento;
	}
	public void setResponsavel(Colaborador responsavel) {
		this.responsavel = responsavel;
	}
	public void setPeriodicidade(Frequencia periodicidade) {
		this.periodicidade = periodicidade;
	}

	public void setAgendainteracaorelacionado(Agendainteracaorelacionado agendainteracaorelacionado) {
		this.agendainteracaorelacionado = agendainteracaorelacionado;
	}
	
	public String subQueryClienteEmpresa(){
		return  "select agendainteracaoSubQueryClienteEmpresa.cdagendainteracao " +
				"from Agendainteracao agendainteracaoSubQueryClienteEmpresa " +
				"left outer join agendainteracaoSubQueryClienteEmpresa.cliente clienteSubQueryClienteEmpresa " +
				"where true = permissao_clienteempresa(clienteSubQueryClienteEmpresa.cdpessoa, '" + new SinedUtil().getListaEmpresa() + "') ";
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Animal")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdanimal")
	public Animal getAnimal() {
		return animal;
	}
	
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	public void setEmpresahistorico(Empresa empresahistorico) {
		this.empresahistorico = empresahistorico;
	}
	
	@Transient
	public Integer getSituacao() {
		return situacao;
	}
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
	
	@Transient
	@DisplayName("Contato")
	public String getContato() {
		if(tipoagendainteracao==Tipoagendainteracao.CLIENTE){
			contato = cliente != null && cliente.getContatoResponsavel() != null? cliente.getContatoResponsavel().getNome()+" - "+cliente.getContatoResponsavel().getTelefones(): "";
		}else{
			contato = contacrm != null ? contacrm.getContatoAndTelefone() : "";
		}
		return contato;
	}
	public void setContato(String contato) {
		this.contato = contato;
	}
	
	@Transient
	@DisplayName("Question�rio de servi�os")
	public Set<Pessoaquestionario> getListaQuestionarioServicos() {
		return listaQuestionarioServicos;
	}
	public void setListaQuestionarioServicos(
			Set<Pessoaquestionario> listaQuestionarioServicos) {
		this.listaQuestionarioServicos = listaQuestionarioServicos;
	}
	
}
