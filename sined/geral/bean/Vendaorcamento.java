package br.com.linkcom.sined.geral.bean;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.auxiliar.CalculoMarkupInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.InclusaoLoteVendaInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.TotalizacaoImpostoInterface;
import br.com.linkcom.sined.geral.bean.auxiliar.ValoresPassiveisDeRateioInterface;
import br.com.linkcom.sined.geral.bean.enumeration.Situacaopedidosolicitacaocompra;
import br.com.linkcom.sined.geral.bean.enumeration.TipoPessoaVendaEnum;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.Sugestaovenda;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.PermissaoProjeto;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.annotation.JoinEmpresa;

import com.ibm.icu.text.SimpleDateFormat;

@Entity
@SequenceGenerator(name = "sq_vendaorcamento", sequenceName = "sq_vendaorcamento")
@DisplayName("Or�amento")
@JoinEmpresa("vendaorcamento.empresa")
public class Vendaorcamento implements Log, PermissaoProjeto, TotalizacaoImpostoInterface, CalculoMarkupInterface, ValoresPassiveisDeRateioInterface{

	protected Integer cdvendaorcamento;
	protected Empresa empresa;
	protected TipoPessoaVendaEnum tipopessoa = TipoPessoaVendaEnum.CLIENTE;
	protected Cliente cliente;
	protected Contacrm contacrm;
	protected Contato contato;
	protected Cliente clienteindicacao;
	protected Fornecedor parceiro;
	protected Money valorusadovalecompra;
	protected Money valoraproximadoimposto;
	protected Endereco endereco;
	protected Colaborador colaborador;
	protected Date dtorcamento;
	protected String observacao;
	protected String observacaointerna;
	protected Vendaorcamentosituacao vendaorcamentosituacao;
	protected Double fatorconversao;
	protected Double qtdereferencia;
	protected ResponsavelFrete frete;
	protected Fornecedor terceiro;
	protected Money valorfrete;
	protected String identificadorexterno;
	protected Money desconto;
	protected Double percentualdesconto;
	protected Money saldofinal;
	protected Projeto projeto;
	protected Localarmazenagem localarmazenagem;
	protected Materialtabelapreco materialtabelapreco;
	protected String identificador;
	protected Date dtvalidade;
	protected Fornecedor fornecedor;
	protected Fornecedor agencia;
	protected Oportunidade oportunidade;
	protected String justificativareprovar;
	protected MotivoReprovacaoFaturamento motivoreprovacaofaturamento;
	protected Money valorFreteCIF;
	protected Endereco enderecoFaturamento;
	// protected Vendasituacao vendasituacao;
	
	protected List<Vendaorcamentomaterial> listavendaorcamentomaterial = new ListSet<Vendaorcamentomaterial>(Vendaorcamentomaterial.class);
	protected List<Vendaorcamentohistorico> listavendaorcamentohistorico = new ListSet<Vendaorcamentohistorico>(Vendaorcamentohistorico.class);
	protected Set<Vendaorcamentoformapagamento> listavendaorcamentoformapagamento = new ListSet<Vendaorcamentoformapagamento>(Vendaorcamentoformapagamento.class);
	protected List<Vendaorcamentomaterialmestre> listavendaorcamentomaterialmestre = new ListSet<Vendaorcamentomaterialmestre>(Vendaorcamentomaterialmestre.class);
	protected List<Orcamentovalorcampoextra> listaOrcamentovalorcampoextra = new ListSet<Orcamentovalorcampoextra>(Orcamentovalorcampoextra.class);
	protected List<VendaOrcamentoFornecedorTicketMedio> listaVendaOrcamentoFornecedorTicketMedio = new ListSet<VendaOrcamentoFornecedorTicketMedio>(VendaOrcamentoFornecedorTicketMedio.class);

	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	
	//Transients
	private Long codigo;
	private Material material;
	private Unidademedida unidademedida;
	private Double valor;
	private Double valormaximo;
	private Double valorminimo;
	private Money ultimovalor;
	private Money ultimoValorVenda;
	private Date ultimodtvenda;
	private Double quantidades = 1D;
	private Double comprimentos;
	private Double comprimentosoriginal;
	private Double larguras;
	private Double alturas;
	private Double fatorconversaocomprimento;
	private Double qtdereferenciacomprimento;
	private Double margemarredondamento;
	private Boolean vendapromocional;
	private Boolean kitflexivel;
	private Boolean existematerialsimilar;
	private Loteestoque loteestoque;
	@SuppressWarnings("unused")
	private Money totalvenda = new Money();
	@SuppressWarnings("unused")
	private Money totalipi = new Money();
	private Money ticketMedioCaculado;
	protected Date prazoentrega;
	private Money valorfinal;
	private Money valorfinalMaisImpostos;
	protected String placaveiculo;
	protected String numeronotadevolucao;
	protected String criadopor;
	protected Vendaorcamentoformapagamento vendaorcamentoformapagamento;
	protected Boolean confirmacaowms;
	private Integer qtdestoqueoriginal;
	private Integer qtdestoque;
	private Boolean aprovar;
	protected Boolean prazomedio;
	protected Integer qtdeParcelas;
	protected Boolean trocarvendedor;
	protected Colaborador colaboradoraux;
	protected String vendaMaterialnumeroserie;
	protected String ids;
	protected String observacaohistorico;
	protected List<Sugestaovenda> listaSugestaovenda;
	protected Pedidovendatipo pedidovendatipo;
	protected Boolean criarEntradaFiscal = Boolean.FALSE;
	protected Boolean registrarDevolucao = Boolean.FALSE;
	protected List<String> listaMateriaisAtualizacao;
	protected Prazopagamento prazopagamento;
	protected Boolean considerarvendamultiplos;
	protected Integer qtdeunidade;
	protected Double valorcustomaterial;
	protected Double valorvendamaterial;
	protected Double multiplicador;
	protected Boolean metrocubicovalorvenda;
	protected Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra;
	protected String produto_anotacoes;
	protected Double totalPesoBruto;
	protected Double totalPesoLiquido;
	protected Boolean identificadorAutomatico = Boolean.TRUE;
	protected Money saldovalecompra;
	protected Double percentualcomissaoagencia;
	protected String identificacaomaterial;
	protected Date prazoentregaMinimo;
	protected Integer cdultimovendaorcamento;
	protected String vendedorprincipal;
	protected Double qtdereservada;
	protected Double qtdeEstoqueMenosReservada;
	protected String menorDtVencimentoLote;
	
	//Transients para Registrar devolu��o
	protected Date devolucaodtchegada;
	protected Date devolucaodtemissao;
	protected Date devolucaodtlancamento;
	protected String devolucaonumero;
	protected String devolucaoplacaveiculo;
	protected String devolucaotipo;
	protected String devolucaotransportadora;
	protected String origemEntrada;
	protected Double limitepercentualdesconto;
	protected Boolean aprovacaopercentualdesconto;
	
	//TRANSIENT para Verificar representantes dos materiais
	protected Money valorMaterialRepresentante;
	
	protected String dtorcamentoClasse;
	protected Double percentualdescontoTabelapreco;
	protected Integer ordem;
	protected Money totalIcms;
	protected Money totalDesoneracaoIcms;
	protected Money totalIcmsSt;
	protected Money totalFcp;
	protected Money totalFcpSt;
	protected Money totalDifal;
	protected Money totalImpostos;
	protected Money totalValorSeguro;
	protected Money totalOutrasDespesas;
	protected Boolean incluirEnderecoFaturamento;
	protected Money valorSeguro;
	protected Money outrasdespesas;
	
	public Vendaorcamento(){
	}

	public Vendaorcamento(Integer cdvendaorcamento){
		this.cdvendaorcamento = cdvendaorcamento;
	}
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_vendaorcamento")
	public Integer getCdvendaorcamento() {
		return cdvendaorcamento;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@Required
	@DisplayName("Tipo de pessoa")
	public TipoPessoaVendaEnum getTipopessoa() {
		return tipopessoa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontacrm")
	@DisplayName("Conta")
	public Contacrm getContacrm() {
		return contacrm;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontato")
	@DisplayName("Contato")
	public Contato getContato() {
		return contato;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdclienteindicacao")
	@DisplayName("Indica��o")
	public Cliente getClienteindicacao() {
		return clienteindicacao;
	}
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdparceiro")
	@DisplayName("Parcerio")
	public Fornecedor getParceiro() {
		return parceiro;
	}
		
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecoentrega")
	@DisplayName("Local de entrega")
	public Endereco getEndereco() {
		return endereco;
	}
	/*
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cdvendasituacao")
	@DisplayName("Situa��o")
	public Vendasituacao getVendasituacao() {
		return vendasituacao;
	}
	
	public void setVendasituacao(Vendasituacao vendasituacao) {
		this.vendasituacao = vendasituacao;
	}
	*/
	@Transient
	@DisplayName("Prazo de entrega")
	public Date getPrazoentrega() {
		return prazoentrega;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaborador")
	@DisplayName("Respons�vel")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@Required
	@DisplayName("Data do or�amento")
	public Date getDtorcamento() {
		return dtorcamento;
	}
	
	@DisplayName("Observa��es")
	public String getObservacao() {
		return observacao;
	}
	@DisplayName("Observa��es Internas")
	@MaxLength(1000)
	public String getObservacaointerna() {
		return observacaointerna;
	}	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdvendaorcamentosituacao")
	@DisplayName("Situa��o")
	public Vendaorcamentosituacao getVendaorcamentosituacao() {
		return vendaorcamentosituacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdterceiro")
	public Fornecedor getTerceiro() {
		return terceiro;
	}
	
	@DisplayName("Valor do frete")
	public Money getValorfrete() {
		return valorfrete;
	}	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public Double getPercentualdesconto() {
		return percentualdesconto;
	}
	
	@DisplayName("Validade")
	public Date getDtvalidade() {
		return dtvalidade;
	}
	
	@DisplayName("Ag�ncia")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdagencia")
	public Fornecedor getAgencia() {
		return agencia;
	}	
	
	public String getJustificativareprovar() {
		return justificativareprovar;
	}
	
	public void setJustificativareprovar(String justificativareprovar) {
		this.justificativareprovar = justificativareprovar;
	}
	
	public void setAgencia(Fornecedor agencia) {
		this.agencia = agencia;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}

	public void setDtvalidade(Date dtvalidade) {
		this.dtvalidade = dtvalidade;
	}
	
	public void setPercentualdesconto(Double percentualdesconto) {
		this.percentualdesconto = percentualdesconto;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setTerceiro(Fornecedor terceiro) {
		this.terceiro = terceiro;
	}
	
	public void setValorfrete(Money valorfrete) {
		this.valorfrete = valorfrete;
	}
	
	public void setCdvendaorcamento(Integer cdvendaorcamento) {
		this.cdvendaorcamento = cdvendaorcamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setTipopessoa(TipoPessoaVendaEnum tipopessoa) {
		this.tipopessoa = tipopessoa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setContacrm(Contacrm contacrm) {
		this.contacrm = contacrm;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}	
	public void setParceiro(Fornecedor parceiro) {
		this.parceiro = parceiro;
	}
	public void setClienteindicacao(Cliente clienteindicacao) {
		this.clienteindicacao = clienteindicacao;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public void setPrazoentrega(Date prazoentrega) {
		this.prazoentrega = prazoentrega;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtorcamento(Date dtorcamento) {
		this.dtorcamento = dtorcamento;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setObservacaointerna(String observacaointerna) {
		this.observacaointerna = observacaointerna;
	}
	public void setVendaorcamentosituacao(
			Vendaorcamentosituacao vendaorcamentosituacao) {
		this.vendaorcamentosituacao = vendaorcamentosituacao;
	}
	
	@OneToMany(mappedBy="vendaorcamento")
	@DisplayName("Produtos")
	public List<Vendaorcamentomaterial> getListavendaorcamentomaterial() {
		return listavendaorcamentomaterial;
	}
	
	@OneToMany(mappedBy="vendaorcamento")
	@DisplayName("Forma(s) de pagamento")
	public Set<Vendaorcamentoformapagamento> getListavendaorcamentoformapagamento() {
		return listavendaorcamentoformapagamento;
	}
	
	public void setListavendaorcamentoformapagamento(
			Set<Vendaorcamentoformapagamento> listavendaorcamentoformapagamento) {
		this.listavendaorcamentoformapagamento = listavendaorcamentoformapagamento;
	}
	
	@OneToMany(mappedBy="vendaorcamento")
	@DisplayName("Hist�rico")
	public List<Vendaorcamentohistorico> getListavendaorcamentohistorico() {
		return listavendaorcamentohistorico;
	}
	
	@OneToMany(mappedBy="vendaorcamento")
	public List<Vendaorcamentomaterialmestre> getListavendaorcamentomaterialmestre() {
		return listavendaorcamentomaterialmestre;
	}

	@DisplayName("Campos adicionais")
	@OneToMany(mappedBy="vendaorcamento")
	public List<Orcamentovalorcampoextra> getListaOrcamentovalorcampoextra() {
		return listaOrcamentovalorcampoextra;
	}
	
	@OneToMany(mappedBy="vendaorcamento")
	public List<VendaOrcamentoFornecedorTicketMedio> getListaVendaOrcamentoFornecedorTicketMedio() {
		return listaVendaOrcamentoFornecedorTicketMedio;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "frete")
	public ResponsavelFrete getFrete() {
		return frete;
	}
	
	public void setFrete(ResponsavelFrete frete) {
		this.frete = frete;
	}
	
	public void setListavendaorcamentohistorico(
			List<Vendaorcamentohistorico> listavendaorcamentohistorico) {
		this.listavendaorcamentohistorico = listavendaorcamentohistorico;
	}
	
	public void setListavendaorcamentomaterial(
			List<Vendaorcamentomaterial> listavendaorcamentomaterial) {
		this.listavendaorcamentomaterial = listavendaorcamentomaterial;
	}
	
	public void setListavendaorcamentomaterialmestre(List<Vendaorcamentomaterialmestre> listavendaorcamentomaterialmestre) {
		this.listavendaorcamentomaterialmestre = listavendaorcamentomaterialmestre;
	}

	public void setListaOrcamentovalorcampoextra(List<Orcamentovalorcampoextra> listaOrcamentovalorcampoextra) {
		this.listaOrcamentovalorcampoextra = listaOrcamentovalorcampoextra;
	}
	
	public void setListaVendaOrcamentoFornecedorTicketMedio(List<VendaOrcamentoFornecedorTicketMedio> listaVendaOrcamentoFornecedorTicketMedio) {
		this.listaVendaOrcamentoFornecedorTicketMedio = listaVendaOrcamentoFornecedorTicketMedio;
	}
	
	//LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getItensVendaString(){
		List<Vendaorcamentomaterial> listavendaorcamentomaterial2 = getListavendaorcamentomaterial();
		if (listavendaorcamentomaterial2 != null && listavendaorcamentomaterial2.size() > 5)
			return "Diversos";
		String string = "";
		if(listavendaorcamentomaterial2 != null && listavendaorcamentomaterial2.size()>0){
			for (Vendaorcamentomaterial vendaorcamentomaterial : listavendaorcamentomaterial2) {
				string += (vendaorcamentomaterial.getQuantidade()!=null ? new DecimalFormat("#,##0.#########").format(vendaorcamentomaterial.getQuantidade()) : 0) + 
						" -  " +  
						vendaorcamentomaterial.getMaterial().getNome() + 
						" -  R$ " + 
						SinedUtil.descriptionDecimal(vendaorcamentomaterial.getPreco(), true) + 
						"<BR>";
			}
		}
		return string;
	}
	
	@Transient
	public String getItensVendaStringRelatorio(){
		List<Vendaorcamentomaterial> listavendaorcamentomaterial2 = getListavendaorcamentomaterial();
		if (listavendaorcamentomaterial2 != null && listavendaorcamentomaterial2.size() > 5)
			return "Diversos";
		String string = "";
		if(listavendaorcamentomaterial2 != null && listavendaorcamentomaterial2.size()>0){
			for (Vendaorcamentomaterial vendaorcamentomaterial : listavendaorcamentomaterial2) {
				string += vendaorcamentomaterial.getMaterial().getNome() + 
				" -  R$ " + 
				SinedUtil.descriptionDecimal(vendaorcamentomaterial.getPreco(), true) + 
				"\n";
			}
		}
		return string;
	}
	
	@Transient
	@DisplayName("C�digo")
	@MaxLength(14)
	public Long getCodigo() {
		return codigo;
	}

	@Transient
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}

	@Transient
	@DisplayName("Valor")
	public Double getValor() {
		return valor;
	}
	
	@Transient
	@DisplayName("Quantidade")
	public Double getQuantidades() {
		return quantidades;
	}
	@Transient
	public Double getComprimentos() {
		return comprimentos;
	}
	@Transient
	public Double getComprimentosoriginal() {
		return comprimentosoriginal;
	}

	@Transient
	public Double getLarguras() {
		return larguras;
	}
	@Transient
	public Double getAlturas() {
		return alturas;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setQuantidades(Double quantidades) {
		this.quantidades = quantidades;
	}
	public void setComprimentos(Double comprimentos) {
		this.comprimentos = comprimentos;
	}
	public void setComprimentosoriginal(Double comprimentosoriginal) {
		this.comprimentosoriginal = comprimentosoriginal;
	}	
	public void setAlturas(Double alturas) {
		this.alturas = alturas;
	}
	public void setLarguras(Double larguras) {
		this.larguras = larguras;
	}
	

	@DisplayName ("Total")
	@Transient
	public Money getTotalvenda() {
		Money valor = new Money(0);
		if(this.listavendaorcamentomaterial != null && !this.listavendaorcamentomaterial.isEmpty()){
			for (Vendaorcamentomaterial vendaorcamentomaterial : this.listavendaorcamentomaterial) {
				Double qtde = vendaorcamentomaterial.getQuantidade() == null || vendaorcamentomaterial.getQuantidade() == 0 ? 1 : vendaorcamentomaterial.getQuantidade();
				Double multiplicador = vendaorcamentomaterial.getMultiplicador() == null || vendaorcamentomaterial.getMultiplicador() == 0 ? 1 : vendaorcamentomaterial.getMultiplicador();
				Double valorTruncado = SinedUtil.round(BigDecimal.valueOf(vendaorcamentomaterial.getPreco().doubleValue() * qtde * multiplicador), 2).doubleValue();
				
				valor = valor.add(new Money(valorTruncado))
							.subtract(vendaorcamentomaterial.getDesconto())
							.add(vendaorcamentomaterial.getValorSeguro())
							.add(vendaorcamentomaterial.getOutrasdespesas());
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? this.getValorusadovalecompra() : new Money(0.0));
		totalvenda = valor;
		return totalvenda;
	}
	
	@Transient
	public Money getTotalvendaSemArredondamento() {
		Money valor = new Money(0);
		if(this.listavendaorcamentomaterial != null && !this.listavendaorcamentomaterial.isEmpty()){
			for (Vendaorcamentomaterial vendaorcamentomaterial : this.listavendaorcamentomaterial) {
				Double qtde = vendaorcamentomaterial.getQuantidade() == null || vendaorcamentomaterial.getQuantidade() == 0 ? 1 : vendaorcamentomaterial.getQuantidade();
				Double multiplicador = vendaorcamentomaterial.getMultiplicador() == null || vendaorcamentomaterial.getMultiplicador() == 0 ? 1 : vendaorcamentomaterial.getMultiplicador();
				Double valorTruncado = SinedUtil.round(BigDecimal.valueOf(vendaorcamentomaterial.getPreco().doubleValue() * qtde * multiplicador), 10).doubleValue();
				
				valor = valor.add(new Money(valorTruncado));
				if(vendaorcamentomaterial.getDesconto() != null){
					valor = valor.subtract(vendaorcamentomaterial.getDesconto())
							.add(vendaorcamentomaterial.getValorSeguro())
							.add(vendaorcamentomaterial.getOutrasdespesas());
				}
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		valor = valor.subtract(this.getValorusadovalecompra() != null ? this.getValorusadovalecompra() : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Money getTotalvendaMaisImpostos() {
		Money valor = getTotalvenda();
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				valor = valor.add(item.getValoripi())
						.add(item.getValoricmsst())
						.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				valor = valor.add(item.getValoripi())
						.add(item.getValoricmsst())
						.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@Transient
	public Money getTotalvendaSemValecompra() {
		Money valor = new Money(0);
		if(this.listavendaorcamentomaterial != null && !this.listavendaorcamentomaterial.isEmpty()){
			for (Vendaorcamentomaterial vendaorcamentomaterial : this.listavendaorcamentomaterial) {
				Double qtde = vendaorcamentomaterial.getQuantidade() == null || vendaorcamentomaterial.getQuantidade() == 0 ? 1 : vendaorcamentomaterial.getQuantidade();
				Double multiplicador = vendaorcamentomaterial.getMultiplicador() == null || vendaorcamentomaterial.getMultiplicador() == 0 ? 1 : vendaorcamentomaterial.getMultiplicador();
				
				valor = valor.add(new Money(vendaorcamentomaterial.getPreco().doubleValue() * qtde * multiplicador))
							.subtract(vendaorcamentomaterial.getDesconto());
				
			}
		}
		valor = valor.add(this.valorfrete != null ? this.valorfrete : new Money(0.0));
		valor = valor.subtract(this.desconto != null ? this.desconto : new Money(0.0));
		return valor;
	}
	
	@Transient
	public Double getTotalquantidades(){
		Double total = 0d;
		if(this.listavendaorcamentomaterial != null && !this.listavendaorcamentomaterial.isEmpty()){
			for (Vendaorcamentomaterial vendaorcamentomaterial : this.listavendaorcamentomaterial) {
				total += vendaorcamentomaterial.getQuantidade() != null ? vendaorcamentomaterial.getQuantidade() : 0.0;
			}
		}
		return total;
	}
	
	public void setTotalvenda(Money totalvenda) {
		this.totalvenda = totalvenda;
	}
	
	@Transient
	@DisplayName("Valor m�ximo")
	public Double getValormaximo() {
		return valormaximo;
	}
	@Transient
	@DisplayName("Valor m�nimo")
	public Double getValorminimo() {
		return valorminimo;
	}
	@Transient
	@DisplayName("Sugest�o")
	public Money getUltimovalor() {
		return ultimovalor;
	}
	public void setUltimovalor(Money ultimovalor) {
		this.ultimovalor = ultimovalor;
	}
	
	@Transient
	@DisplayName("�ltimo Valor")
	public Money getUltimoValorVenda() {
		return ultimoValorVenda;
	}
	public void setUltimoValorVenda(Money ultimoValorVenda) {
		this.ultimoValorVenda = ultimoValorVenda;
	}
	
	@Transient
	public Date getUltimodtvenda() {
		return ultimodtvenda;
	}

	public void setUltimodtvenda(Date ultimodtvenda) {
		this.ultimodtvenda = ultimodtvenda;
	}

	public void setValormaximo(Double valormaximo) {
		this.valormaximo = valormaximo;
	}
	public void setValorminimo(Double valorminimo) {
		this.valorminimo = valorminimo;
	}
	@Transient
	@DisplayName("Qtde. dispon�vel")
	public Integer getQtdestoque() {
		return qtdestoque;
	}
	public void setQtdestoque(Integer qtdestoque) {
		this.qtdestoque = qtdestoque;
	}
	
	@Transient
	public Double getFatorconversao() {
		return fatorconversao;
	}
	public void setFatorconversao(Double fatorconversao) {
		this.fatorconversao = fatorconversao;
	}
	@Transient
	public Boolean getAprovar() {
		return aprovar;
	}
	public void setAprovar(Boolean aprovar) {
		this.aprovar = aprovar;
	}

	@MaxLength(200)
	@DisplayName("Identificador Externo")
	public String getIdentificadorexterno() {
		return identificadorexterno;
	}

	public void setIdentificadorexterno(String identificadorexterno) {
		this.identificadorexterno = identificadorexterno;
	}

	@DisplayName("Desconto")
	public Money getDesconto() {
		return desconto;
	}

	public void setDesconto(Money desconto) {
		this.desconto = desconto;
	}

	@Transient
	@DisplayName("Valor Final")
	public Money getValorfinal() {
		return valorfinal;
	}

	public void setValorfinal(Money valorfinal) {
		this.valorfinal = valorfinal;
	}
	
	@Transient
	public Boolean getPrazomedio() {
		return prazomedio;
	}

	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	
	@Transient
	public Integer getQtdeParcelas() {
		return qtdeParcelas;
	}
	public void setQtdeParcelas(Integer qtdeParcelas) {
		this.qtdeParcelas = qtdeParcelas;
	}
	@DisplayName("Valor final")
	public Money getSaldofinal() {
		return saldofinal;
	}
	public void setSaldofinal(Money saldofinal) {
		this.saldofinal = saldofinal;
	}
	@Transient
	@DisplayName("Unidade de medida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	@Transient
	@DisplayName("Venda promocional")
	public Boolean getVendapromocional() {
		return vendapromocional;
	}
	public void setVendapromocional(Boolean vendapromocional) {
		this.vendapromocional = vendapromocional;
	}
	
	@Transient
	public Boolean getKitflexivel() {
		return kitflexivel;
	}

	public void setKitflexivel(Boolean kitflexivel) {
		this.kitflexivel = kitflexivel;
	}
	
	public String subQueryProjeto() {
		return "select vendasubQueryProjeto.cdvendaorcamento " +
				"from Vendaorcamento vendasubQueryProjeto " +
				"left outer join vendasubQueryProjeto.projeto projetosubQueryProjeto " +
				"where projetosubQueryProjeto.cdprojeto in (" + SinedUtil.getListaProjeto() + ") ";
	}	

	public boolean useFunctionProjeto() {
		return false;
	}

	@Transient
	public String getPlacaveiculo() {
		return placaveiculo;
	}
	@Transient
	public String getNumeronotadevolucao() {
		return numeronotadevolucao;
	}

	public void setPlacaveiculo(String placaveiculo) {
		this.placaveiculo = placaveiculo;
	}
	public void setNumeronotadevolucao(String numeronotadevolucao) {
		this.numeronotadevolucao = numeronotadevolucao;
	}

	@Transient
	public Boolean getTrocarvendedor() {
		return trocarvendedor;
	}
	public void setTrocarvendedor(Boolean trocarvendedor) {
		this.trocarvendedor = trocarvendedor;
	}
	@Transient
	public Colaborador getColaboradoraux() {
		return colaboradoraux;
	}
	public void setColaboradoraux(Colaborador colaboradoraux) {
		this.colaboradoraux = colaboradoraux;
	}
	@DisplayName("Local")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}

	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}

	@Transient
	@DisplayName("Data de Chegada")
	public Date getDevolucaodtchegada() {
		return devolucaodtchegada;
	}
	@Transient
	@DisplayName("Data de Emiss�o")
	public Date getDevolucaodtemissao() {
		return devolucaodtemissao;
	}
	@Transient
	@DisplayName("Data de Lan�amento")
	public Date getDevolucaodtlancamento() {
		return devolucaodtlancamento;
	}
	@Transient
	@DisplayName("N�mero")
	public String getDevolucaonumero() {
		return devolucaonumero;
	}
	@Transient
	@DisplayName("Placa do Ve�culo")
	public String getDevolucaoplacaveiculo() {
		return devolucaoplacaveiculo;
	}
	@Transient
	@DisplayName("Tipo")
	public String getDevolucaotipo() {
		return devolucaotipo;
	}
	@Transient
	@DisplayName("Transportadora")
	public String getDevolucaotransportadora() {
		return devolucaotransportadora;
	}

	public void setDevolucaodtchegada(Date devolucaodtchegada) {
		this.devolucaodtchegada = devolucaodtchegada;
	}
	public void setDevolucaodtemissao(Date devolucaodtemissao) {
		this.devolucaodtemissao = devolucaodtemissao;
	}
	public void setDevolucaodtlancamento(Date devolucaodtlancamento) {
		this.devolucaodtlancamento = devolucaodtlancamento;
	}
	public void setDevolucaonumero(String devolucaonumero) {
		this.devolucaonumero = devolucaonumero;
	}
	public void setDevolucaoplacaveiculo(String devolucaoplacaveiculo) {
		this.devolucaoplacaveiculo = devolucaoplacaveiculo;
	}
	public void setDevolucaotipo(String devolucaotipo) {
		this.devolucaotipo = devolucaotipo;
	}
	public void setDevolucaotransportadora(String devolucaotransportadora) {
		this.devolucaotransportadora = devolucaotransportadora;
	}

	@Transient
	public String getOrigemEntrada() {
		return origemEntrada;
	}

	public void setOrigemEntrada(String origemEntrada) {
		this.origemEntrada = origemEntrada;
	}

	@DisplayName("Venda")
	@Transient
	public String getVendaMaterialnumeroserie() {
		StringBuilder s = new StringBuilder();
		if(dtorcamento != null)
			s.append(new SimpleDateFormat("dd/MM/yyyy").format(dtorcamento));
		
		if(cliente != null && cliente.getNome() != null && !"".equals(cliente.getNome())){
			if(!"".equals(s.toString())) s.append(" - ");
			s.append(cliente.getNome());
		}
		
		return s.toString();
	}	

	@Transient
	@DisplayName("Qtde principal dispon�vel")
	public Integer getQtdestoqueoriginal() {
		return qtdestoqueoriginal;
	}

	public void setQtdestoqueoriginal(Integer qtdestoqueoriginal) {
		this.qtdestoqueoriginal = qtdestoqueoriginal;
	}
	
	@Transient
	public String getCriadopor() {
		return criadopor;
	}
	
	public void setCriadopor(String criadopor) {
		this.criadopor = criadopor;
	}
	
	@Transient
	public Vendaorcamentoformapagamento getVendaorcamentoformapagamento() {
		return vendaorcamentoformapagamento;
	}
	
	public void setVendaorcamentoformapagamento(Vendaorcamentoformapagamento vendaorcamentoformapagamento) {
		this.vendaorcamentoformapagamento = vendaorcamentoformapagamento;
	}
	
	@Transient
	public Boolean getConfirmacaowms() {
		return confirmacaowms;
	}	
	public void setConfirmacaowms(Boolean confirmacaowms) {
		this.confirmacaowms = confirmacaowms;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	@Transient
	public String getIds() {
		return ids;
	}	
	@Transient
	public String getObservacaohistorico() {
		return observacaohistorico;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public void setObservacaohistorico(String observacaohistorico) {
		this.observacaohistorico = observacaohistorico;
	}

	@Transient
	public Double getMargemarredondamento() {
		return margemarredondamento;
	}
	public void setMargemarredondamento(Double margemarredondamento) {
		this.margemarredondamento = margemarredondamento;
	}
	@Transient
	public Double getFatorconversaocomprimento() {
		return fatorconversaocomprimento;
	}
	public void setFatorconversaocomprimento(Double fatorconversaocomprimento) {
		this.fatorconversaocomprimento = fatorconversaocomprimento;
	}
	
	@DisplayName("Tipo de pedido de venda")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendatipo")
	public Pedidovendatipo getPedidovendatipo() {
		return pedidovendatipo;
	}
	
	public void setPedidovendatipo(Pedidovendatipo pedidovendatipo) {
		this.pedidovendatipo = pedidovendatipo;
	}

	@Transient
	public List<Sugestaovenda> getListaSugestaovenda() {
		return listaSugestaovenda;
	}
	
	public void setListaSugestaovenda(List<Sugestaovenda> listaSugestaovenda) {
		this.listaSugestaovenda = listaSugestaovenda;
	}

	@Transient
	public Money getValorMaterialRepresentante() {
		return valorMaterialRepresentante;
	}
	public void setValorMaterialRepresentante(Money valorMaterialRepresentante) {
		this.valorMaterialRepresentante = valorMaterialRepresentante;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialtabelapreco")
	@DisplayName("Tabela de Pre�os")
	public Materialtabelapreco getMaterialtabelapreco() {
		return materialtabelapreco;
	}
	public void setMaterialtabelapreco(Materialtabelapreco materialtabelapreco) {
		this.materialtabelapreco = materialtabelapreco;
	}
	
	@Transient
	@DisplayName("Criar Entrada Fiscal?")
	public Boolean getCriarEntradaFiscal() {
		return criarEntradaFiscal;
	}
	
	public void setCriarEntradaFiscal(Boolean criarEntradaFiscal) {
		this.criarEntradaFiscal = criarEntradaFiscal;
	}
	
	@Transient
	public Boolean getRegistrarDevolucao() {
		return registrarDevolucao;
	}
	
	public void setRegistrarDevolucao(Boolean registrarDevolucao) {
		this.registrarDevolucao = registrarDevolucao;
	}
	
	@Transient
	public Double getLimitepercentualdesconto() {
		return limitepercentualdesconto;
	}
	
	public void setLimitepercentualdesconto(Double limitepercentualdesconto) {
		this.limitepercentualdesconto = limitepercentualdesconto;
	}
	
	@Transient
	public Boolean getAprovacaopercentualdesconto() {
		return aprovacaopercentualdesconto;
	}
	
	public void setAprovacaopercentualdesconto(
			Boolean aprovacaopercentualdesconto) {
		this.aprovacaopercentualdesconto = aprovacaopercentualdesconto;
	}

	@Transient
	public Boolean getExistematerialsimilar() {
		return existematerialsimilar;
	}
	public void setExistematerialsimilar(Boolean existematerialsimilar) {
		this.existematerialsimilar = existematerialsimilar;
	}
	@DisplayName("Lote")
	@Transient
	public Loteestoque getLoteestoque() {
		return loteestoque;
	}
	public void setLoteestoque(Loteestoque loteestoque) {
		this.loteestoque = loteestoque;
	}
	
	@Transient
	public List<String> getListaMateriaisAtualizacao() {
		return listaMateriaisAtualizacao;
	}
	
	public void setListaMateriaisAtualizacao(
			List<String> listaMateriaisAtualizacao) {
		this.listaMateriaisAtualizacao = listaMateriaisAtualizacao;
	}
	
	@Transient
	public Prazopagamento getPrazopagamento() {
		return prazopagamento;
	}
	
	public void setPrazopagamento(Prazopagamento prazopagamento) {
		this.prazopagamento = prazopagamento;
	}
	
	public Money getValorusadovalecompra() {
		return valorusadovalecompra;
	}
	
	public void setValorusadovalecompra(Money valorusadovalecompra) {
		this.valorusadovalecompra = valorusadovalecompra;
	}
	
	@DisplayName("Valor aproximado dos impostos (IBPT)")
	public Money getValoraproximadoimposto() {
		return valoraproximadoimposto;
	}
	public void setValoraproximadoimposto(Money valoraproximadoimposto) {
		this.valoraproximadoimposto = valoraproximadoimposto;
	}
	
	@DisplayName("Motivo")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdmotivoreprovacaofaturamento")
	public MotivoReprovacaoFaturamento getMotivoreprovacaofaturamento() {
		return motivoreprovacaofaturamento;
	}

	@DisplayName("Valor do frete CIF")
	public Money getValorFreteCIF() {
		return valorFreteCIF;
	}
	
	public void setMotivoreprovacaofaturamento(
			MotivoReprovacaoFaturamento motivoreprovacaofaturamento) {
		this.motivoreprovacaofaturamento = motivoreprovacaofaturamento;
	}
	
	public void setValorFreteCIF(Money valorFreteCIF) {
		this.valorFreteCIF = valorFreteCIF;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdenderecofaturamento")
	@DisplayName("Endere�o de faturamento")
	public Endereco getEnderecoFaturamento() {
		return enderecoFaturamento;
	}
	public void setEnderecoFaturamento(Endereco enderecoFaturamento) {
		this.enderecoFaturamento = enderecoFaturamento;
	}

	@Transient
	public Boolean getConsiderarvendamultiplos() {
		return considerarvendamultiplos;
	}
	@Transient
	public Integer getQtdeunidade() {
		return qtdeunidade;
	}
	public void setConsiderarvendamultiplos(Boolean considerarvendamultiplos) {
		this.considerarvendamultiplos = considerarvendamultiplos;
	}
	public void setQtdeunidade(Integer qtdeunidade) {
		this.qtdeunidade = qtdeunidade;
	}
	
	@Transient
	public Double getValorcustomaterial() {
		return valorcustomaterial;
	}
	public void setValorcustomaterial(Double valorcustomaterial) {
		this.valorcustomaterial = valorcustomaterial;
	}
	@Transient
	public Double getValorvendamaterial() {
		return valorvendamaterial;
	}
	public void setValorvendamaterial(Double valorvendamaterial) {
		this.valorvendamaterial = valorvendamaterial;
	}
	
	@Transient
	public Double getQtdereferencia() {
		return qtdereferencia;
	}
	@Transient
	public Double getQtdereferenciacomprimento() {
		return qtdereferenciacomprimento;
	}
	public void setQtdereferencia(Double qtdereferencia) {
		this.qtdereferencia = qtdereferencia;
	}
	public void setQtdereferenciacomprimento(Double qtdereferenciacomprimento) {
		this.qtdereferenciacomprimento = qtdereferenciacomprimento;
	}
	
	@Transient
	public Double getMultiplicador() {
		return multiplicador;
	}
	
	public void setMultiplicador(Double multiplicador) {
		this.multiplicador = multiplicador;
	}
	
	@Transient
	public Boolean getMetrocubicovalorvenda() {
		return metrocubicovalorvenda;
	}
	
	public void setMetrocubicovalorvenda(Boolean metrocubicovalorvenda) {
		this.metrocubicovalorvenda = metrocubicovalorvenda;
	}
	
	@Transient
	public Date getDataentregaTrans(){
		java.sql.Date date = null;
		
		if(this.getListavendaorcamentomaterial() != null && !this.getListavendaorcamentomaterial().isEmpty()){
			for(Vendaorcamentomaterial vendaorcamentomaterial : this.getListavendaorcamentomaterial()){
				if(vendaorcamentomaterial.getPrazoentrega() != null){
					if(date == null){
						date = new java.sql.Date(vendaorcamentomaterial.getPrazoentrega().getTime());
					}else if(SinedDateUtils.beforeIgnoreHour(date, (java.sql.Date)  new java.sql.Date(vendaorcamentomaterial.getPrazoentrega().getTime()))){
						date = new java.sql.Date(vendaorcamentomaterial.getPrazoentrega().getTime());
					}
				}
			}
		}
		
		return date;
	}
	
	@Transient
	public Situacaopedidosolicitacaocompra getSituacaopedidosolicitacaocompra() {
		return situacaopedidosolicitacaocompra;
	}
	public void setSituacaopedidosolicitacaocompra(Situacaopedidosolicitacaocompra situacaopedidosolicitacaocompra) {
		this.situacaopedidosolicitacaocompra = situacaopedidosolicitacaocompra;
	}
	
	@Transient
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public String getDtorcamentoClasse() {
		return dtorcamentoClasse;
	}
	
	public void setDtorcamentoClasse(String dtorcamentoClasse) {
		this.dtorcamentoClasse = dtorcamentoClasse;
	}
	
	@Transient
	@DisplayName("Anota��es")
	public String getProduto_anotacoes() {
		return produto_anotacoes;
	}

	public void setProduto_anotacoes(String produtoAnotacoes) {
		produto_anotacoes = produtoAnotacoes;
	}
	
	@Transient
	public Double getTotalPesoBruto() {
		Double totalPesoBruto = 0.0;
		if(this.listavendaorcamentomaterial!= null && !this.listavendaorcamentomaterial.isEmpty()){
			for(Vendaorcamentomaterial vendamaterial : this.listavendaorcamentomaterial){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getMaterial().getPesobruto() != null){
					totalPesoBruto += vendamaterial.getMaterial().getPesobruto() * qtde;
				}
			}
		}
		return SinedUtil.roundByParametro(totalPesoBruto, 2);
	}
	public void setTotalPesoBruto(Double totalPesoBruto) {
		this.totalPesoBruto = totalPesoBruto;
	}
	
	@Transient
	public Double getTotalPesoLiquido() {
		Double totalPesoLiquido = 0.0;
		if(this.listavendaorcamentomaterial!= null && !this.listavendaorcamentomaterial.isEmpty()){
			for(Vendaorcamentomaterial vendamaterial : this.listavendaorcamentomaterial){
				Double qtde = vendamaterial.getQuantidade() == null || vendamaterial.getQuantidade() == 0 ? 1 : vendamaterial.getQuantidade();
				if(vendamaterial.getPesoVendaOuMaterial() != null){
					totalPesoLiquido += vendamaterial.getPesoVendaOuMaterial() * qtde;
				}
			}
		}
		return SinedUtil.roundByParametro(totalPesoLiquido, 2);
	}
	public void setTotalPesoLiquido(Double totalPesoLiquido) {
		this.totalPesoLiquido = totalPesoLiquido;
	}
	
	@Transient
	public Boolean getIdentificadorAutomatico() {
		return identificadorAutomatico;
	}
	
	public void setIdentificadorAutomatico(Boolean identificadorAutomatico) {
		this.identificadorAutomatico = identificadorAutomatico;
	}
	
	@Transient
	public Money getSaldovalecompra() {
		return saldovalecompra;
	}
	
	public void setSaldovalecompra(Money saldovalecompra) {
		this.saldovalecompra = saldovalecompra;
	}

	@Transient
	public Double getPercentualcomissaoagencia() {
		return percentualcomissaoagencia;
	}

	public void setPercentualcomissaoagencia(Double percentualcomissaoagencia) {
		this.percentualcomissaoagencia = percentualcomissaoagencia;
	}

	@Transient
	public String getIdentificacaomaterial() {
		return identificacaomaterial;
	}

	public void setIdentificacaomaterial(String identificacaomaterial) {
		this.identificacaomaterial = identificacaomaterial;
	}

	@Transient
	@DisplayName("Valor Final + Impostos")
	public Money getValorfinalMaisImpostos() {
		return valorfinalMaisImpostos;
	}

	public void setValorfinalMaisImpostos(Money valorfinalMaisImpostos) {
		this.valorfinalMaisImpostos = valorfinalMaisImpostos;
	}
	
	@DisplayName ("Total IPI")
	@Transient
	public Money getTotalipi() {
		/*Money valor = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				if(item.getValoripi() != null){
					valor = valor.add(item.getValoripi());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				if(item.getValoripi() != null){
					valor = valor.add(item.getValoripi());
				}
			}
		}*/
		return totalipi;
	}
	
	public void setTotalipi(Money totalipi) {
		this.totalipi = totalipi;
	}
	
	@Transient
	public Date getPrazoentregaMinimo() {
		return prazoentregaMinimo;
	}
	
	public void setPrazoentregaMinimo(Date prazoentregaMinimo) {
		this.prazoentregaMinimo = prazoentregaMinimo;
	}

	@Transient
	public Integer getCdultimovendaorcamento() {
		return cdultimovendaorcamento;
	}

	public void setCdultimovendaorcamento(Integer cdultimovendaorcamento) {
		this.cdultimovendaorcamento = cdultimovendaorcamento;
	}
	
	@Transient
	@DisplayName("Vendedor principal")
	public String getVendedorprincipal() {
		return vendedorprincipal;
	}
	public void setVendedorprincipal(String vendedorprincipal) {
		this.vendedorprincipal = vendedorprincipal;
	}
	
	@Transient
	public Double getPercentualdescontoTabelapreco() {
		return percentualdescontoTabelapreco;
	}
	public void setPercentualdescontoTabelapreco(
			Double percentualdescontoTabelapreco) {
		this.percentualdescontoTabelapreco = percentualdescontoTabelapreco;
	}
	
	@Transient
	public Integer getOrdem() {
		return ordem;
	}
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	@Transient
	public Money getTotalFcp() {
		/*totalFcp = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalFcp = totalFcp.add(item.getValorfcp());
			}
		}*/
		return totalFcp;
	}
	public void setTotalFcp(Money totalFcp) {
		this.totalFcp = totalFcp;
	}
	
	@Transient
	public Money getTotalFcpSt() {
		/*totalFcpSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalFcpSt = totalFcpSt.add(item.getValorfcpst());
			}
		}*/
		return totalFcpSt;
	}
	public void setTotalFcpSt(Money totalFcpSt) {
		this.totalFcpSt = totalFcpSt;
	}
	
	@Transient
	public Money getTotalDesoneracaoIcms() {
		return totalDesoneracaoIcms;
	}
	
	public void setTotalDesoneracaoIcms(Money totalDesoneracaoIcms) {
		this.totalDesoneracaoIcms = totalDesoneracaoIcms;
	}
	
	@Transient
	public Money getTotalIcms() {
		/*totalIcms = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalIcms = totalIcms.add(item.getValoricms());
			}
		}*/
		return totalIcms;
	}
	public void setTotalIcms(Money totalIcms) {
		this.totalIcms = totalIcms;
	}
	
	@Transient
	public Money getTotalIcmsSt() {
		/*totalIcmsSt = new Money(0);
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalIcmsSt = totalIcmsSt.add(item.getValoricmsst());
			}
		}*/
		return totalIcmsSt;
	}
	public void setTotalIcmsSt(Money totalIcmsSt) {
		this.totalIcmsSt = totalIcmsSt;
	}
	
	@Transient
	public Money getTotalImpostos() {
		totalImpostos = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalImpostos = totalImpostos.add(item.getValoripi())
											.add(item.getValoricms())
											.add(item.getValoricmsst())
											.add(item.getValorfcp())
											.add(item.getValorfcpst())
											.add(item.getValordifal());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					totalImpostos = totalImpostos.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return totalImpostos;
	}
	public void setTotalImpostos(Money totalImpostos) {
		this.totalImpostos = totalImpostos;
	}
	
	@Transient
	public Money getTotalOutrasDespesas() {
		totalOutrasDespesas = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial vendamaterial : this.listavendaorcamentomaterial) {
				totalOutrasDespesas = totalOutrasDespesas.add(vendamaterial.getOutrasdespesas());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalOutrasDespesas = totalOutrasDespesas.add(item.getOutrasdespesas());
			}
		}
		return totalOutrasDespesas;
	}
	public void setTotalOutrasDespesas(Money totalOutrasDespesas) {
		this.totalOutrasDespesas = totalOutrasDespesas;
	}
	
	@Transient
	public Money getTotalValorSeguro() {
		totalValorSeguro = new Money();
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial vendamaterial : this.listavendaorcamentomaterial) {
				totalValorSeguro = totalValorSeguro.add(vendamaterial.getValorSeguro());
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				totalValorSeguro = totalValorSeguro.add(item.getValorSeguro());
			}
		}
		return totalValorSeguro;
	}
	public void setTotalValorSeguro(Money totalValorSeguro) {
		this.totalValorSeguro = totalValorSeguro;
	}
	
	@Transient
	public Boolean getIncluirEnderecoFaturamento() {
		incluirEnderecoFaturamento = Util.objects.isPersistent(this.getEnderecoFaturamento());
		return incluirEnderecoFaturamento;
	}
	public void setIncluirEnderecoFaturamento(Boolean incluirEnderecoFaturamento) {
		this.incluirEnderecoFaturamento = incluirEnderecoFaturamento;
	}
	@Transient
	public Money getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(Money valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	@Transient
	public Money getOutrasdespesas() {
		return outrasdespesas;
	}
	public void setOutrasdespesas(Money outrasdespesas) {
		this.outrasdespesas = outrasdespesas;
	}
	
	@DisplayName("Qtde Reservada")
	@Transient
	public Double getQtdereservada() {
		return qtdereservada;
	}
	public void setQtdereservada(Double qtdereservada) {
		this.qtdereservada = qtdereservada;
	}
	
	@Transient
	@DisplayName ("Qtde dispon�vel")
	public Double getQtdeEstoqueMenosReservada() {
		return qtdeEstoqueMenosReservada;
	}
	public void setQtdeEstoqueMenosReservada(Double qtdeEstoqueMenosReservada) {
		this.qtdeEstoqueMenosReservada = qtdeEstoqueMenosReservada;
	}
	
	@Transient
	public Money getTotalDifal() {
		return totalDifal;
	}
	
	@Override
	public void setTotalDifal(Money totalDifal) {
		this.totalDifal = totalDifal;
	}
	
	@Transient
	public Money getTotalvendaimpostos() {
		Money valor = getTotalvenda();
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterial)){
			for (Vendaorcamentomaterial item : this.listavendaorcamentomaterial) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		if(SinedUtil.isListNotEmpty(this.listavendaorcamentomaterialmestre)){
			for (Vendaorcamentomaterialmestre item : this.listavendaorcamentomaterialmestre) {
				valor = valor.add(item.getValoripi())
							.add(item.getValoricmsst())
							.add(item.getValorfcpst());
				
				if(item.getAbaterdesoneracaoicms() != null && item.getAbaterdesoneracaoicms()){
					valor = valor.subtract(item.getValordesoneracaoicms());
				}
			}
		}
		return valor;
	}
	
	@Transient
	public String getMenorDtVencimentoLote() {
		return menorDtVencimentoLote;
	}
	
	public void setMenorDtVencimentoLote(String menorDtVencimentoLote) {
		this.menorDtVencimentoLote = menorDtVencimentoLote;
	}
	
	@Transient
	public List<InclusaoLoteVendaInterface> getListaInclusaoLoteVendaInterface(){
		List<InclusaoLoteVendaInterface> lista = new ArrayList<InclusaoLoteVendaInterface>();
		for(Vendaorcamentomaterial pvm: this.listavendaorcamentomaterial){
			lista.add(pvm);
		}
		return lista;
	}

	@DisplayName ("TICKET M�DIO CALCULADO (POR KG)")
	@Transient
	public Money getTicketMedioCaculado() {
		return ticketMedioCaculado;
	}

	public void setTicketMedioCaculado(Money ticketMedioCaculado) {
		this.ticketMedioCaculado = ticketMedioCaculado;
	}
	
	@Transient
	public Money getPercentualaplicadototal(){
		Double percentualaplicadototal = 0d;
		Double valordescontoitem = 0d;
		Double totalvenda = 0d;
		Double totaltabela = 0d;
		
		if(SinedUtil.isListNotEmpty(getListavendaorcamentomaterial())){
			for(Vendaorcamentomaterial vendamaterial : getListavendaorcamentomaterial()){
				
				if(vendamaterial != null && vendamaterial.getValorvendaRelatorio() != null){
					totaltabela += vendamaterial.getValorvendaRelatorio() * vendamaterial.getQuantidade();
				}
				if(vendamaterial.getConsiderarDesconto() != null && vendamaterial.getConsiderarDesconto() 
						&& vendamaterial.getValordescontocustovendaitem() != null && vendamaterial.getValorvalecomprapropocional() != null
						&& ((vendamaterial.getConsiderarValeCompra() == null || !vendamaterial.getConsiderarValeCompra()) 
							|| (vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra()))){
					
					if(vendamaterial.getUnidademedida() != null && vendamaterial.getUnidademedida().getCasasdecimaisestoque() != null){
						valordescontoitem += SinedUtil.round(vendamaterial.getValordescontocustovendaitem(), vendamaterial.getUnidademedida().getCasasdecimaisestoque());
					}else {
						valordescontoitem += vendamaterial.getValordescontocustovendaitem();
					}
				}
				if((vendamaterial.getConsiderarDesconto() == null || !vendamaterial.getConsiderarDesconto()) 
						&& vendamaterial.getConsiderarValeCompra() != null && vendamaterial.getConsiderarValeCompra() 
						&& vendamaterial.getValorvalecomprapropocional() != null){
					
					valordescontoitem += vendamaterial.getValorvalecomprapropocional();
				}			
				if(vendamaterial.getTotalprodutoDescontoReport() != null){
					totalvenda += vendamaterial.getTotalprodutoReport().getValue().doubleValue();
				}
			}
			
			if(totaltabela != null && totaltabela > 0){
				percentualaplicadototal = ( valordescontoitem / totaltabela )* 100;
			}else if(totalvenda != null && totalvenda > 0){
				percentualaplicadototal = valordescontoitem / totalvenda * 100;
			}
		}
		return new Money(percentualaplicadototal);
	}
}