package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;


@Entity
@SequenceGenerator(name = "sq_materialgrupotributacao", sequenceName = "sq_materialgrupotributacao")
public class Materialgrupotributacao {

	protected Integer cdmaterialgrupotributacao;
	protected Materialgrupo materialgrupo;
	
	protected Empresa empresa;
	protected Cfop cfop;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialgrupotributacao")
	public Integer getCdmaterialgrupotributacao() {
		return cdmaterialgrupotributacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialgrupo")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdcfop")
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setCdmaterialgrupotributacao(Integer cdmaterialgrupotributacao) {
		this.cdmaterialgrupotributacao = cdmaterialgrupotributacao;
	}
}
