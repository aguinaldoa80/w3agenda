package br.com.linkcom.sined.geral.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsOperacao;
import br.com.linkcom.sined.geral.bean.enumeration.ModeloDocumentoFiscalEnum;

@Entity
@SequenceGenerator(name = "sq_ajusteipidocumento", sequenceName = "sq_ajusteipidocumento")
@DisplayName("Documentos")
public class AjusteIpiDocumento {

	private Integer cdAjusteIpiDocumento;
	private AjusteIpi ajusteIpi;
	private String numero;
	private String serie;
	private String subserie;
	private ModeloDocumentoFiscalEnum modeloDocumento;
	private String chaveAcesso;
	protected AjusteIcmsOperacao tipo;
	private Pessoa pessoa;
	private Date dtEmissao;
	private Material material;
	private Money valor;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_ajusteipidocumento")
	public Integer getCdAjusteIpiDocumento() {
		return cdAjusteIpiDocumento;
	}
	
	public void setCdAjusteIpiDocumento(Integer cdAjusteIpiDocumento) {
		this.cdAjusteIpiDocumento = cdAjusteIpiDocumento;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdajusteipi")
	@DisplayName("Ajuste")
	public AjusteIpi getAjusteIpi() {
		return ajusteIpi;
	}

	public void setAjusteIpi(AjusteIpi ajusteIpi) {
		this.ajusteIpi = ajusteIpi;
	}

	@Required
	@DisplayName("N�mero")
	@MaxLength(9)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@DisplayName("S�rie")
	@MaxLength(4)
	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@MaxLength(3)
	@DisplayName("Subs�rie")
	public String getSubserie() {
		return subserie;
	}

	public void setSubserie(String subserie) {
		this.subserie = subserie;
	}

	@Required
	public ModeloDocumentoFiscalEnum getModeloDocumento() {
		return modeloDocumento;
	}
	
	public void setModeloDocumento(ModeloDocumentoFiscalEnum modeloDocumento) {
		this.modeloDocumento = modeloDocumento;
	}

	@DisplayName("Chave de Acesso")
	@MaxLength(44)
	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	@Required
	public AjusteIcmsOperacao getTipo() {
		return tipo;
	}
	
	public void setTipo(AjusteIcmsOperacao tipo) {
		this.tipo = tipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@DisplayName("Cliente/Fornecedor")
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Required
	@DisplayName("Data de Emiss�o")
	public Date getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	@DisplayName("Material")
	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Required
	@DisplayName("Valor do ajuste")
	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@Transient
	public Fornecedor getFornecedor(){
		if(this.pessoa != null){
			if(this.pessoa instanceof Fornecedor){
				return (Fornecedor)this.pessoa;
			}
			Fornecedor fornecedor = new Fornecedor(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			return fornecedor;
		}
		return null;
	}
	
	public void setFornecedor(Fornecedor fornecedor){
		if(fornecedor != null){
			this.pessoa = fornecedor;
		}
	}
	
	@Transient
	public Cliente getCliente(){
		if(this.pessoa != null){
			if(this.pessoa instanceof Cliente){
				return (Cliente)this.pessoa;
			}
			Cliente cliente = new Cliente(this.pessoa.getCdpessoa(), this.pessoa.getNome());
			return cliente;
		}
		return null;
	}
	
	public void setCliente(Cliente cliente){
		if(cliente != null){
			this.pessoa = cliente;
		}
	}
}
