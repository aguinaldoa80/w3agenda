package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_sindicato", sequenceName = "sq_sindicato")
public class Sindicato implements Log{
	
	protected Integer cdsindicato;
	protected String nome;
	protected String descricao;
	protected Integer codigofolha;
	protected Boolean ativo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Set<Colaboradorcargo> listaColaboradorcargo;
	
	public Sindicato() {
	}
	
	
	/**
	 * Quando sadjaksd jaklas
	 * 
	 * @param cdsindicato
	 * @param nome
	 * @param codigofolha
	 * @param ativo
	 */
	public Sindicato(Integer cdsindicato, String nome, String codigofolha,Boolean ativo) {
		super();
		this.cdsindicato = cdsindicato;
		this.nome = nome;
		this.codigofolha = Integer.parseInt(codigofolha);
		this.ativo = ativo;
	}



	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_sindicato")
	@Required
	public Integer getCdsindicato() {
		return cdsindicato;
	}
	
	public void setCdsindicato(Integer cdsindicato) {
		this.cdsindicato = cdsindicato;
	}

	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@MaxLength(9)
	@DisplayName("C�digo Folha")
	@Required
	public Integer getCodigofolha() {
		return codigofolha;
	}

	public void setCodigofolha(Integer codigofolha) {
		this.codigofolha = codigofolha;
	}

	@Required
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@DisplayName("Colaborador")
	@OneToMany(mappedBy="sindicato")
	public Set<Colaboradorcargo> getListaColaboradorcargo() {
		return listaColaboradorcargo;
	}

	public void setListaColaboradorcargo(Set<Colaboradorcargo> listaColaboradorcargo) {
		this.listaColaboradorcargo = listaColaboradorcargo;
	}

	
}
