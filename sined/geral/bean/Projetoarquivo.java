package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_projetoarquivo", sequenceName = "sq_projetoarquivo")
public class Projetoarquivo implements Log {

	protected Integer cdprojetoarquivo;
	protected Arquivo arquivo;
	protected Projeto projeto;
	protected String descricao;
	protected Date dtarquivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_projetoarquivo")
	public Integer getCdprojetoarquivo() {
		return cdprojetoarquivo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@MaxLength(50)
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Data")
	@Required
	public Date getDtarquivo() {
		return dtarquivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	public void setDtarquivo(Date dtarquivo) {
		this.dtarquivo = dtarquivo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setCdprojetoarquivo(Integer cdprojetoarquivo) {
		this.cdprojetoarquivo = cdprojetoarquivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdprojetoarquivo == null) ? 0 : cdprojetoarquivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Projetoarquivo other = (Projetoarquivo) obj;
		if (cdprojetoarquivo == null) {
			if (other.cdprojetoarquivo != null)
				return false;
		} else if (!cdprojetoarquivo.equals(other.cdprojetoarquivo))
			return false;
		return true;
	}
	
}
