package br.com.linkcom.sined.geral.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialkitflexivel", sequenceName = "sq_materialkitflexivel")
@DisplayName("Composi��o do Kit Flex�vel")
public class Materialkitflexivel {

	protected Integer cdmaterialkitflexivel;
	protected Material material;
	protected Material materialkit;
	protected Double quantidade = 1d;
	protected Money valorvenda;
	protected List<Materialkitflexivelformula> listaMaterialkitflexivelformula = new ListSet<Materialkitflexivelformula>(Materialkitflexivelformula.class);
	
	protected String index;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialkitflexivel")
	public Integer getCdmaterialkitflexivel() {
		return cdmaterialkitflexivel;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	@Required
	@DisplayName("Material")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterialkit")
	public Material getMaterialkit() {
		return materialkit;
	}
	public Double getQuantidade() {
		return quantidade;
	}
	@Required
	@DisplayName("Valor venda")
	public Money getValorvenda() {
		return valorvenda;
	}
	@OneToMany(mappedBy="materialkitflexivel")
	public List<Materialkitflexivelformula> getListaMaterialkitflexivelformula() {
		return listaMaterialkitflexivelformula;
	}
	
	public void setCdmaterialkitflexivel(Integer cdmaterialkitflexivel) {
		this.cdmaterialkitflexivel = cdmaterialkitflexivel;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setMaterialkit(Material materialkit) {
		this.materialkit = materialkit;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	public void setValorvenda(Money valorvenda) {
		this.valorvenda = valorvenda;
	}
	public void setListaMaterialkitflexivelformula(List<Materialkitflexivelformula> listaMaterialkitflexivelformula) {
		this.listaMaterialkitflexivelformula = listaMaterialkitflexivelformula;
	}
	
	@Transient
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
}
