package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;


@Entity
@SequenceGenerator(name = "sq_expedicaohistorico", sequenceName = "sq_expedicaohistorico")
public class Expedicaohistorico implements Log {

	protected Integer cdexpedicaohistorico;
	protected Expedicao expedicao;
	protected Expedicaoacao expedicaoacao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	protected List<ConferenciaMaterialExpedicao> listaConferencia;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_expedicaohistorico")
	public Integer getCdexpedicaohistorico() {
		return cdexpedicaohistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdexpedicao")
	public Expedicao getExpedicao() {
		return expedicao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	@DisplayName("A��o")
	public Expedicaoacao getExpedicaoacao() {
		return expedicaoacao;
	}

	public void setExpedicaoacao(Expedicaoacao expedicaoacao) {
		this.expedicaoacao = expedicaoacao;
	}

	public void setCdexpedicaohistorico(Integer cdexpedicaohistorico) {
		this.cdexpedicaohistorico = cdexpedicaohistorico;
	}

	public void setExpedicao(Expedicao expedicao) {
		this.expedicao = expedicao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}

	@Transient
	public List<ConferenciaMaterialExpedicao> getListaConferencia() {
		return listaConferencia;
	}
	public void setListaConferencia(List<ConferenciaMaterialExpedicao> listaConferencia) {
		this.listaConferencia = listaConferencia;
	}
}
