package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialfornecedor", sequenceName = "sq_materialfornecedor")
@DisplayName("Fornecedor")
public class Materialfornecedor implements Log {

	protected Integer cdmaterialfornecedor;
	protected Material material;
	protected Fornecedor fornecedor;
	protected Integer tempoentrega;
	protected String codigo;
	protected Boolean fabricante;
	protected Boolean principal;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialfornecedor")
	public Integer getCdmaterialfornecedor() {
		return cdmaterialfornecedor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	@DisplayName("Tempo de entrega")
	public Integer getTempoentrega() {
		return tempoentrega;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	@DisplayName("C�digo alternativo do fornecedor")
	@MaxLength(60)
	public String getCodigo() {
		return codigo;
	}
	@Column(name="fabricante")
	public Boolean getFabricante() {
		return fabricante;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setFabricante(Boolean fabricante) {
		this.fabricante = fabricante;
	}
	public void setCdmaterialfornecedor(Integer cdmaterialfornecedor) {
		this.cdmaterialfornecedor = cdmaterialfornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setTempoentrega(Integer tempoentrega) {
		this.tempoentrega = tempoentrega;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}	

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	public Boolean getPrincipal() {
		return principal;
	}
	
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
}
