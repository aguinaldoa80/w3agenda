package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_faixaimpostoempresa",sequenceName="sq_faixaimpostoempresa")
public class Faixaimpostoempresa {
	
	protected Integer cdfaixaimpostoempresa;
	protected Faixaimposto faixaimposto;
	protected Empresa empresa;
	
	@Id
	@GeneratedValue(generator="sq_faixaimpostoempresa", strategy=GenerationType.AUTO)
	public Integer getCdfaixaimpostoempresa() {
		return cdfaixaimpostoempresa;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfaixaimposto")
	public Faixaimposto getFaixaimposto() {
		return faixaimposto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setCdfaixaimpostoempresa(Integer cdfaixaimpostoempresa) {
		this.cdfaixaimpostoempresa = cdfaixaimpostoempresa;
	}

	public void setFaixaimposto(Faixaimposto faixaimposto) {
		this.faixaimposto = faixaimposto;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
