package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name="sq_configuracaotefadquirente", sequenceName="sq_configuracaotefadquirente")
public class Configuracaotefadquirente {

	protected Integer cdconfiguracaotefadquirente;
	protected Configuracaotef configuracaotef;
	protected String adquirente;
	protected Fornecedor fornecedor;
	
	@Id
	@GeneratedValue(generator="sq_configuracaotefadquirente", strategy=GenerationType.AUTO)
	public Integer getCdconfiguracaotefadquirente() {
		return cdconfiguracaotefadquirente;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconfiguracaotef")
	public Configuracaotef getConfiguracaotef() {
		return configuracaotef;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	public String getAdquirente() {
		return adquirente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setCdconfiguracaotefadquirente(Integer cdconfiguracaotefadquirente) {
		this.cdconfiguracaotefadquirente = cdconfiguracaotefadquirente;
	}

	public void setConfiguracaotef(Configuracaotef configuracaotef) {
		this.configuracaotef = configuracaotef;
	}

	public void setAdquirente(String adquirente) {
		this.adquirente = adquirente;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdconfiguracaotefadquirente == null) ? 0
						: cdconfiguracaotefadquirente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuracaotefadquirente other = (Configuracaotefadquirente) obj;
		if (cdconfiguracaotefadquirente == null) {
			if (other.cdconfiguracaotefadquirente != null)
				return false;
		} else if (!cdconfiguracaotefadquirente
				.equals(other.cdconfiguracaotefadquirente))
			return false;
		return true;
	}
	
}
