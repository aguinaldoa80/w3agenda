package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitalsituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Documentodigitaltipo;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_documentodigital", sequenceName = "sq_documentodigital")
public class Documentodigital implements Log{

	protected Integer cddocumentodigital;
	protected Empresa empresa;
	protected Documentodigitalmodelo documentodigitalmodelo;
	protected Arquivo arquivo;
	protected Documentodigitalsituacao situacao = Documentodigitalsituacao.EM_ESPERA;
	protected Documentodigitaltipo tipo;
	protected Oportunidade oportunidade;
	protected Cliente cliente;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected String hash;
	
	protected List<Documentodigitaldestinatario> listaDocumentodigitaldestinatario = new ListSet<Documentodigitaldestinatario>(Documentodigitaldestinatario.class);
	protected Set<Documentodigitalobservador> listaDocumentodigitalobservador = new ListSet<Documentodigitalobservador>(Documentodigitalobservador.class);

	protected String email;
	
	@Id
	@DisplayName("C�digo")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentodigital")
	public Integer getCddocumentodigital() {
		return cddocumentodigital;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}

	@DisplayName("Situa��o")
	public Documentodigitalsituacao getSituacao() {
		return situacao;
	}

	@Required
	@DisplayName("Tipo")
	public Documentodigitaltipo getTipo() {
		return tipo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdoportunidade")
	public Oportunidade getOportunidade() {
		return oportunidade;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcliente")
	public Cliente getCliente() {
		return cliente;
	}
	
	@DisplayName("Destinat�rios")
	@OneToMany(mappedBy="documentodigital", fetch=FetchType.LAZY)
	public List<Documentodigitaldestinatario> getListaDocumentodigitaldestinatario() {
		return listaDocumentodigitaldestinatario;
	}
	
	@DisplayName("Observadores")
	@OneToMany(mappedBy="documentodigital", fetch=FetchType.LAZY)
	public Set<Documentodigitalobservador> getListaDocumentodigitalobservador() {
		return listaDocumentodigitalobservador;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Modelo")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentodigitalmodelo")
	public Documentodigitalmodelo getDocumentodigitalmodelo() {
		return documentodigitalmodelo;
	}
	
	@DisplayName("Hash (MD5)")
	public String getHash() {
		return hash;
	}
	
	public void setListaDocumentodigitalobservador(
			Set<Documentodigitalobservador> listaDocumentodigitalobservador) {
		this.listaDocumentodigitalobservador = listaDocumentodigitalobservador;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public void setDocumentodigitalmodelo(
			Documentodigitalmodelo documentodigitalmodelo) {
		this.documentodigitalmodelo = documentodigitalmodelo;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setListaDocumentodigitaldestinatario(
			List<Documentodigitaldestinatario> listaDocumentodigitaldestinatario) {
		this.listaDocumentodigitaldestinatario = listaDocumentodigitaldestinatario;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public void setSituacao(Documentodigitalsituacao situacao) {
		this.situacao = situacao;
	}

	public void setTipo(Documentodigitaltipo tipo) {
		this.tipo = tipo;
	}

	public void setOportunidade(Oportunidade oportunidade) {
		this.oportunidade = oportunidade;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setCddocumentodigital(Integer cddocumentodigital) {
		this.cddocumentodigital = cddocumentodigital;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Cliente / Oportunidade")
	public String getDescricaotipo(){
		if(this.tipo != null && this.tipo.equals(Documentodigitaltipo.CLIENTE) && this.cliente != null) return this.cliente.getNome();
		if(this.tipo != null && this.tipo.equals(Documentodigitaltipo.OPORTUNIDADE) && this.oportunidade != null) return this.oportunidade.getNome();
		return "";
	}
	
	@Transient
	@DisplayName("Adicionar e-mail")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
}
