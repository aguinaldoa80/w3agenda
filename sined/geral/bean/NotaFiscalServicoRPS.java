package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_NotaFiscalServicoRPS",sequenceName="sq_NotaFiscalServicoRPS")
public class NotaFiscalServicoRPS {

	protected Integer cdNotaFiscalServicoRPS;
	protected NotaFiscalServico notaFiscalServico;
	protected Integer numero;
	protected Timestamp dataemissao;
	protected NotaFiscalServicoLoteRPS notaFiscalServicoLoteRPS;
	
	@Id
	@GeneratedValue(generator="sq_NotaFiscalServicoRPS",strategy=GenerationType.AUTO)
	public Integer getCdNotaFiscalServicoRPS() {
		return cdNotaFiscalServicoRPS;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdNotaFiscalServico")
	public NotaFiscalServico getNotaFiscalServico() {
		return notaFiscalServico;
	}

	@DisplayName("N�mero do RPS")
	public Integer getNumero() {
		return numero;
	}

	@DisplayName("Data de emiss�o")
	public Timestamp getDataemissao() {
		return dataemissao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdNotaFiscalServicoLoteRPS")
	public NotaFiscalServicoLoteRPS getNotaFiscalServicoLoteRPS() {
		return notaFiscalServicoLoteRPS;
	}
	
	public void setNotaFiscalServicoLoteRPS(
			NotaFiscalServicoLoteRPS notaFiscalServicoLoteRPS) {
		this.notaFiscalServicoLoteRPS = notaFiscalServicoLoteRPS;
	}

	public void setCdNotaFiscalServicoRPS(Integer cdNotaFiscalServicoRPS) {
		this.cdNotaFiscalServicoRPS = cdNotaFiscalServicoRPS;
	}

	public void setNotaFiscalServico(NotaFiscalServico notaFiscalServico) {
		this.notaFiscalServico = notaFiscalServico;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public void setDataemissao(Timestamp dataemissao) {
		this.dataemissao = dataemissao;
	}

}