package br.com.linkcom.sined.geral.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MaxValue;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_servidorfirewall", sequenceName = "sq_servidorfirewall")
public class Servidorfirewall{
	
	protected Integer cdservidorfirewall;
	protected Servidor servidor;
	protected Integer ordem;
	protected String acao;
	protected String protocolo;
	protected String iporigem;
	protected String portaorigem;
	protected String ipdestino;
	protected String portadestino;
	protected Servidorfirewallsentido servidorfirewallsentido;
	protected String campo_interface;
	protected String opcionais;
	protected Boolean ativo = true;
		
	//Transient's
	protected Integer indexlista;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_servidorfirewall")
	public Integer getCdservidorfirewall() {
		return cdservidorfirewall;
	}

	@Required
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidor")	
	public Servidor getServidor() {
		return servidor;
	}
	
	@Required
	@MaxLength(5)
	@MinValue(1)
	@MaxValue(65534)
	public Integer getOrdem() {
		return ordem;
	}
	
	@Required
	@MaxLength(100)
	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}
	
	@Required
	@MaxLength(20)
	public String getProtocolo() {
		return protocolo;
	}
	
	@Required
	@MaxLength(2000)
	@DisplayName("IP de origem")
	public String getIporigem() {
		return iporigem;
	}
	
	@DisplayName("Porta de origem")
	@MaxLength(2000)
	public String getPortaorigem() {
		return portaorigem;
	}
	
	@Required
	@MaxLength(2000)
	@DisplayName("IP de destino")
	public String getIpdestino() {
		return ipdestino;
	}
	
	@DisplayName("Porta de destino")
	@MaxLength(2000)
	public String getPortadestino() {
		return portadestino;
	}
		
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdservidorfirewallsentido")
	@DisplayName("Sentido")
	public Servidorfirewallsentido getServidorfirewallsentido() {
		return servidorfirewallsentido;
	}
	
	@Column(name="interface")
	@DisplayName("Interface")
	@MaxLength(50)
	public String getCampo_interface() {
		return campo_interface;
	}
	
	@MaxLength(300)
	public String getOpcionais() {
		return opcionais;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	@Transient
	public Integer getIndexlista() {
		return indexlista;
	}

	public void setCdservidorfirewall(Integer cdservidorfirewall) {
		this.cdservidorfirewall = cdservidorfirewall;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public void setIporigem(String iporigem) {
		this.iporigem = iporigem;
	}

	public void setPortaorigem(String portaorigem) {
		this.portaorigem = portaorigem;
	}

	public void setIpdestino(String ipdestino) {
		this.ipdestino = ipdestino;
	}

	public void setPortadestino(String portadestino) {
		this.portadestino = portadestino;
	}

	public void setServidorfirewallsentido(Servidorfirewallsentido servidorfirewallsentido) {
		this.servidorfirewallsentido = servidorfirewallsentido;
	}

	public void setCampo_interface(String campo_interface) {
		this.campo_interface = campo_interface;
	}

	public void setOpcionais(String opcionais) {
		this.opcionais = opcionais;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
	
	public void setIndexlista(Integer indexlista) {
		this.indexlista = indexlista;
	}
	
}
