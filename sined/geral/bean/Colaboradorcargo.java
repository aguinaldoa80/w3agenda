package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.CAGED;
import br.com.linkcom.sined.geral.bean.enumeration.Tiposalario;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Cargos do colaborador")
@SequenceGenerator(name = "sq_colaboradorcargo", sequenceName = "sq_colaboradorcargo")
public class Colaboradorcargo implements Log{

	protected Integer cdcolaboradorcargo;
	protected Colaborador colaborador;
	protected Cargo cargo;
	protected Regimecontratacao regimecontratacao;
	protected Colaboradorsituacao colaboradorsituacao;
	protected Date vencimentosituacao;
	protected Departamento departamento;
	protected java.sql.Date dtinicio;
	protected java.sql.Date dtfim;
	protected Empresa empresa;
	protected String observacao;
	protected Money salario;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Sindicato sindicato;
	protected CAGED caged;
	protected Tiposalario tiposalario;
	protected Projeto projeto;
	protected Long matricula;
	protected Centrocusto centroCusto;
	
	protected Integer codigocfip;
	protected Boolean implementacaomedidas;
	protected Boolean usoepiiniterrupto;
	protected Boolean prazovalidade;
	protected Boolean trocaprograma;
	protected Boolean higienizacao;

	private Fornecedor fornecedor;
	
	protected List<Colaboradorcargoprofissiografia> listaColaboradorcargoprofissiografia = new ListSet<Colaboradorcargoprofissiografia>(Colaboradorcargoprofissiografia.class);
	protected List<Colaboradorcargorisco> listaColaboradorcargorisco = new ListSet<Colaboradorcargorisco>(Colaboradorcargorisco.class);
	
	protected List<Colaboradorcargohistorico> listcolaboradorcargohistorico;
	
	
	//transient
	protected Boolean inserirHistorico;
	protected java.sql.Date dttroca;
	protected String idColaboradores;
	protected Boolean pendencia;
	protected Boolean matriculaautomatico = Boolean.TRUE;
	protected Formularh formularh;
	
	public Colaboradorcargo(){}
	public Colaboradorcargo(Integer cdcolaboradorcargo){
		this.cdcolaboradorcargo = cdcolaboradorcargo;
	}
	public Colaboradorcargo(Colaboradorcargo colaboradorcargo, Colaborador colaborador){
		this.colaborador = colaborador;
		this.cargo = colaboradorcargo.getCargo();
		this.projeto = colaboradorcargo.getProjeto();
		this.regimecontratacao = colaboradorcargo.getRegimecontratacao();
		this.departamento = colaboradorcargo.getDepartamento();
		this.dtinicio = colaboradorcargo.getDtinicio();
		this.dtfim = colaboradorcargo.getDtfim();
		this.empresa = colaboradorcargo.getEmpresa();
		this.observacao = colaboradorcargo.getObservacao();
		this.salario = colaboradorcargo.getSalario();
		this.cdusuarioaltera = colaboradorcargo.getCdusuarioaltera();
		this.dtaltera = colaboradorcargo.getDtaltera();
		this.sindicato = colaboradorcargo.getSindicato();
		this.caged = colaboradorcargo.getCaged();
		this.tiposalario = colaboradorcargo.getTiposalario();
		this.codigocfip = colaboradorcargo.getCodigocfip();
		this.implementacaomedidas = colaboradorcargo.getImplementacaomedidas();
		this.usoepiiniterrupto = colaboradorcargo.getUsoepiiniterrupto();
		this.prazovalidade = colaboradorcargo.getPrazovalidade();
		this.trocaprograma = colaboradorcargo.getTrocaprograma();
		this.higienizacao = colaboradorcargo.getHigienizacao();
		this.listaColaboradorcargoprofissiografia = new ArrayList<Colaboradorcargoprofissiografia>();
		this.vencimentosituacao = colaboradorcargo.getVencimentosituacao();
		this.colaboradorsituacao = colaboradorcargo.getColaboradorsituacao();
		
		for (Colaboradorcargoprofissiografia itemccp : colaboradorcargo.getListaColaboradorcargoprofissiografia()) {
			Colaboradorcargoprofissiografia novoItemccp = new Colaboradorcargoprofissiografia();
			novoItemccp.setCargoatividade(itemccp.getCargoatividade()); 
			novoItemccp.setColaboradorcargo(itemccp.getColaboradorcargo());
			novoItemccp.setDtinicio(itemccp.getDtinicio());
			novoItemccp.setDtfim(itemccp.getDtfim());
			this.listaColaboradorcargoprofissiografia.add(novoItemccp);
		}
		this.listaColaboradorcargorisco = new ArrayList<Colaboradorcargorisco>();
		for (Colaboradorcargorisco itemccr : colaboradorcargo.getListaColaboradorcargorisco()) {
			Colaboradorcargorisco novoItemccr = new Colaboradorcargorisco();
			novoItemccr.setColaboradorcargo(itemccr.getColaboradorcargo());
			novoItemccr.setRiscotrabalho(itemccr.getRiscotrabalho());
			novoItemccr.setExameresponsavel(itemccr.getExameresponsavel());
			novoItemccr.setCargoriscointensidade(itemccr.getCargoriscointensidade());
			novoItemccr.setCargoriscotecnica(itemccr.getCargoriscotecnica());
			novoItemccr.setDtinicio(itemccr.getDtinicio());
			novoItemccr.setDtfim(itemccr.getDtfim());
			novoItemccr.setEpi(itemccr.getEpi());
			novoItemccr.setEpc(itemccr.getEpc());
			novoItemccr.setMaterialepi(itemccr.getMaterialepi());
			this.listaColaboradorcargorisco.add(novoItemccr);
		}
	}
	
	@Transient
	@DisplayName("Inserir no hist�rico")
	public Boolean getInserirHistorico() {
		return inserirHistorico;
	}
	public void setInserirHistorico(Boolean inserirHistorico) {
		this.inserirHistorico = inserirHistorico;
	}
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_colaboradorcargo")
	public Integer getCdcolaboradorcargo() {
		return cdcolaboradorcargo;
	}
	public void setCdcolaboradorcargo(Integer id) {
		this.cdcolaboradorcargo = id;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsindicato")
	public Sindicato getSindicato() {
		return sindicato;
	}

	public void setSindicato(Sindicato sindicato) {
		this.sindicato = sindicato;
	}
	
	@MaxLength(9)
	@DisplayName("Matr�cula")
	public Long getMatricula() {
		return matricula;
	}
	
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcentrocusto")
	@DisplayName("Centro de custo")
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcargo")
	@Required
	@DisplayName("Cargo")
	public Cargo getCargo() {
		return cargo;
	}
	
	@OneToMany(mappedBy = "colaboradorcargo")
	@DisplayName("Profissiografia")
	public List<Colaboradorcargoprofissiografia> getListaColaboradorcargoprofissiografia() {
		return listaColaboradorcargoprofissiografia;
	}
	
	@OneToMany(mappedBy = "colaboradorcargo")
	@DisplayName("Exposi��o a Riscos")
	public List<Colaboradorcargorisco> getListaColaboradorcargorisco() {
		return listaColaboradorcargorisco;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdregimecontratacao")
	@DisplayName("Regime de contrata��o")
	public Regimecontratacao getRegimecontratacao() {
		return regimecontratacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddepartamento")
	@DisplayName("Departamento")
	@Required
	public Departamento getDepartamento() {
		return departamento;
	}
	
	@DisplayName("Empresa")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@Required
	@DisplayName("Data in�cio")
	public java.sql.Date getDtinicio() {
		return dtinicio;
	}
	
	@MaxLength(1000)
	@DisplayName("Outras informa��es")
	public String getObservacao() {
		return observacao;
	}

	@DisplayName("CAGED")
	@Type(type="br.com.linkcom.sined.util.CAGEDType")
	public CAGED getCaged() {
		return caged;
	}
	
	@DisplayName("Tipo sal�rio")
	@Type(type="br.com.linkcom.sined.util.TiposalarioType")
	public Tiposalario getTiposalario() {
		return tiposalario;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Hist�rico")
	@OneToMany(fetch = FetchType.LAZY, mappedBy="colaboradorcargo")
	public List<Colaboradorcargohistorico> getListcolaboradorcargohistorico() {
		return listcolaboradorcargohistorico;
	}
	
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public void setTiposalario(Tiposalario tiposalario) {
		this.tiposalario = tiposalario;
	}
	
	public void setCaged(CAGED caged) {
		this.caged = caged;
	}
	
	@DisplayName("Data fim")
	public java.sql.Date getDtfim() {
		return dtfim;
	}
	
	@DisplayName("Data de troca do cargo")
	@Required
	@Transient
	public java.sql.Date getDttroca() {
		return dttroca;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	@DisplayName("C�digo GFIP")
	@MaxLength(2)
	public Integer getCodigocfip() {
		return codigocfip;
	}
	public Boolean getImplementacaomedidas() {
		return implementacaomedidas;
	}
	public Boolean getUsoepiiniterrupto() {
		return usoepiiniterrupto;
	}
	public Boolean getPrazovalidade() {
		return prazovalidade;
	}
	public Boolean getTrocaprograma() {
		return trocaprograma;
	}
	public Boolean getHigienizacao() {
		return higienizacao;
	}
	@Transient
	public Boolean getMatriculaautomatico() {
		return matriculaautomatico;
	}
	public void setCodigocfip(Integer codigocfip) {
		this.codigocfip = codigocfip;
	}
	public void setImplementacaomedidas(Boolean implementacaomedidas) {
		this.implementacaomedidas = implementacaomedidas;
	}
	public void setUsoepiiniterrupto(Boolean usoepiiniterrupto) {
		this.usoepiiniterrupto = usoepiiniterrupto;
	}
	public void setPrazovalidade(Boolean prazovalidade) {
		this.prazovalidade = prazovalidade;
	}
	public void setTrocaprograma(Boolean trocaprograma) {
		this.trocaprograma = trocaprograma;
	}
	public void setHigienizacao(Boolean higienizacao) {
		this.higienizacao = higienizacao;
	}
	public void setRegimecontratacao(Regimecontratacao regimecontratacao) {
		this.regimecontratacao = regimecontratacao;
	}
	public void setListaColaboradorcargoprofissiografia(
			List<Colaboradorcargoprofissiografia> listaColaboradorcargoprofissiografia) {
		this.listaColaboradorcargoprofissiografia = listaColaboradorcargoprofissiografia;
	}
	public void setListaColaboradorcargorisco(
			List<Colaboradorcargorisco> listaColaboradorcargorisco) {
		this.listaColaboradorcargorisco = listaColaboradorcargorisco;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setDtinicio(java.sql.Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setDtfim(java.sql.Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Sal�rio")
	public Money getSalario() {
		return salario;
	}
	
	public void setSalario(Money salario) {
		this.salario = salario;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setDttroca(java.sql.Date dttroca) {
		this.dttroca = dttroca;
	}
	@Transient
	public String getIdColaboradores() {
		return idColaboradores;
	}
	public void setIdColaboradores(String idColaboradores) {
		this.idColaboradores = idColaboradores;
	}
	public void setMatriculaautomatico(Boolean matriculaautomatico) {
		this.matriculaautomatico = matriculaautomatico;
	}
	@Transient
	public String getDescricaoAutocomplete(){
		if(this.matricula != null && this.colaborador != null && this.colaborador.nome != null){
			return this.matricula + " - " + this.colaborador.nome;
		} else if(this.colaborador != null && this.colaborador.nome != null){
			return this.colaborador.nome;
		} else if(this.matricula != null){
			return this.matricula.toString();
		} else {
			return "";
		}
	}
	
	@Transient
	public String getNomeColaborador(){
		return getColaborador() != null ? getColaborador().getNome() : "";
	}
	
	public void setNomeColaborador(String nome){
		if(getColaborador() != null){
			getColaborador().setNome(nome);
		} else {
			setColaborador(new Colaborador());
			getColaborador().setNome(nome);
		}
	}
	public void setListcolaboradorcargohistorico(List<Colaboradorcargohistorico> listcolaboradorcargohistorico) {
		this.listcolaboradorcargohistorico = listcolaboradorcargohistorico;
	}
	
	@Transient
	public Boolean getPendencia() {
		return pendencia;
	}
	
	public void setPendencia(Boolean pendencia) {
		this.pendencia = pendencia;
	}

	@Required
	@DisplayName("Situa��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcolaboradorsituacao")
	public Colaboradorsituacao getColaboradorsituacao() {
		return colaboradorsituacao;
	}
	@DisplayName("Vencimento da situa��o")
	public Date getVencimentosituacao() {
		return vencimentosituacao;
	}
	public void setColaboradorsituacao(Colaboradorsituacao colaboradorsituacao) {
		this.colaboradorsituacao = colaboradorsituacao;
	}
	public void setVencimentosituacao(Date vencimentosituacao) {
		this.vencimentosituacao = vencimentosituacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfornecedor")
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	@Transient
	public Formularh getFormularh() {
		return formularh;
	}
	
	public void setFormularh(Formularh formularh) {
		this.formularh = formularh;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcolaboradorcargo == null) ? 0 : cdcolaboradorcargo
						.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colaboradorcargo other = (Colaboradorcargo) obj;
		if (cdcolaboradorcargo == null) {
			if (other.cdcolaboradorcargo != null)
				return false;
		} else if (!cdcolaboradorcargo.equals(other.cdcolaboradorcargo))
			return false;
		return true;
	}
	
	
}
