package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_conselhoclasseprofissional", sequenceName="sq_conselhoclasseprofissional")
public class Conselhoclasseprofissional implements Log {
	
	private Integer cdconselhoclasseprofissional;
	private String descricao;
	private String sigla;
	private Boolean ativo = Boolean.TRUE;
	private Integer cdusuarioaltera;
	private Timestamp dtaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_conselhoclasseprofissional")
	public Integer getCdconselhoclasseprofissional() {
		return cdconselhoclasseprofissional;
	}
	@Required
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@Required
	@DisplayName("Sigla")
	@MaxLength(20)
	public String getSigla() {
		return sigla;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Transient
	@DescriptionProperty(usingFields={"descricao", "sigla"})
	public String getDescricaoSigla() {
		return sigla + " - " + descricao;
	}
	
	public void setCdconselhoclasseprofissional(Integer cdconselhoclasseprofissional) {
		this.cdconselhoclasseprofissional = cdconselhoclasseprofissional;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}