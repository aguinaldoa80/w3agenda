package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@DisplayName("Item de Custo com M�o de Obra")
@SequenceGenerator(name = "sq_customaoobraitem", sequenceName = "sq_customaoobraitem")
public class Customaoobraitem implements Log {

	protected Integer cdcustomaoobraitem;
	protected String identificador;
	protected Orcamento orcamento;
	protected String descricao;
	protected Customaoobraitemgrupo customaoobraitemgrupo;
	protected Contagerencial contagerencial;
	protected Unidademedida unidademedida;
	protected Money custounitario;
	protected Double quantidade;
	protected Double percentualvenda;
	protected Money custovenda;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean todoscargos;
	
	protected List<Customaoobraitemcargo> listaCustomaoobraitemcargo = new ListSet<Customaoobraitemcargo>(Customaoobraitemcargo.class);
	
	// TRANSIENTE
	protected String whereIn;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_customaoobraitem")
	public Integer getCdcustomaoobraitem() {
		return cdcustomaoobraitem;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdorcamento")
	public Orcamento getOrcamento() {
		return orcamento;
	}

	@Required
	@MaxLength(200)
	@DescriptionProperty
	@DisplayName("Item de Custo MO")
	public String getDescricao() {
		return descricao;
	}
	
	@Required
	@MaxLength(50)
	public String getIdentificador() {
		return identificador;
	}

	@Required
	@DisplayName("Grupo de Item de Custo MO")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcustomaoobraitemgrupo")
	public Customaoobraitemgrupo getCustomaoobraitemgrupo() {
		return customaoobraitemgrupo;
	}
	
	@DisplayName("Conta Gerencial")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontagerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	@Required
	@DisplayName("Unidade")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdunidademedida")
	public Unidademedida getUnidademedida() {
		return unidademedida;
	}

	@Required
	@DisplayName("Custo unit�rio")
	public Money getCustounitario() {
		return custounitario;
	}

	@Required
	public Double getQuantidade() {
		return quantidade;
	}

	@DisplayName("% Lucro")
	public Double getPercentualvenda() {
		return percentualvenda;
	}

	@DisplayName("Custo venda")
	public Money getCustovenda() {
		return custovenda;
	}
	
	@DisplayName("V�lido para todos os cargos")
	public Boolean getTodoscargos() {
		return todoscargos;
	}

	@DisplayName("Cargos")
	@OneToMany(mappedBy="customaoobraitem")
	public List<Customaoobraitemcargo> getListaCustomaoobraitemcargo() {
		return listaCustomaoobraitemcargo;
	}
	
	public void setListaCustomaoobraitemcargo(
			List<Customaoobraitemcargo> listaCustomaoobraitemcargo) {
		this.listaCustomaoobraitemcargo = listaCustomaoobraitemcargo;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public void setCustomaoobraitemgrupo(Customaoobraitemgrupo customaoobraitemgrupo) {
		this.customaoobraitemgrupo = customaoobraitemgrupo;
	}
	
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setUnidademedida(Unidademedida unidademedida) {
		this.unidademedida = unidademedida;
	}

	public void setCustounitario(Money custounitario) {
		this.custounitario = custounitario;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public void setPercentualvenda(Double percentualvenda) {
		this.percentualvenda = percentualvenda;
	}

	public void setCustovenda(Money custovenda) {
		this.custovenda = custovenda;
	}
	
	public void setTodoscargos(Boolean todoscargos) {
		this.todoscargos = todoscargos;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}
	
	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setCdcustomaoobraitem(Integer cdcustomaoobraitem) {
		this.cdcustomaoobraitem = cdcustomaoobraitem;
	}
	
//	LOG
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	public String getWhereIn() {
		return whereIn;
	}
	
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	
}
