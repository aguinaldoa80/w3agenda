package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_contaprojeto", sequenceName = "sq_contaprojeto")
public class Contaprojeto {

	protected Integer cdcontaprojeto;
	protected Conta conta;
	protected Projeto projeto;
	
	public Contaprojeto() {
	}
	
	public Contaprojeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contaprojeto")
	@DisplayName("Id")
	public Integer getCdcontaprojeto() {
		return cdcontaprojeto;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdconta")
	public Conta getConta() {	
		return conta;		
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdprojeto")     
	public Projeto getProjeto() {
		return projeto;
	}

	public void setCdcontaprojeto(Integer cdcontaprojeto) {
		this.cdcontaprojeto = cdcontaprojeto;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
}
