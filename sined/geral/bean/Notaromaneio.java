package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@SequenceGenerator(name = "sq_notaromaneio", sequenceName = "sq_notaromaneio")
public class Notaromaneio {

	protected Integer cdnotaromaneio;
	protected Nota nota;
	protected Romaneio romaneio;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_notaromaneio")
	public Integer getCdnotaromaneio() {
		return cdnotaromaneio;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdnota")
	public Nota getNota() {
		return nota;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdromaneio")
	public Romaneio getRomaneio() {
		return romaneio;
	}

	public void setCdnotaromaneio(Integer cdnotaromaneio) {
		this.cdnotaromaneio = cdnotaromaneio;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	public void setRomaneio(Romaneio romaneio) {
		this.romaneio = romaneio;
	}
	
}
