package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_agendainteracaohistorico", sequenceName = "sq_agendainteracaohistorico")
public class Agendainteracaohistorico implements Log{

	protected Integer cdagendainteracaohistorico;
	protected Agendainteracao agendainteracao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Boolean interacaoapp = Boolean.FALSE;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendainteracaohistorico")
	public Integer getCdagendainteracaohistorico() {
		return cdagendainteracaohistorico;
	}
	@DisplayName("Agenda de Intera��o")
	@JoinColumn(name="cdagendainteracao")
	@ManyToOne(fetch=FetchType.LAZY)
	public Agendainteracao getAgendainteracao() {
		return agendainteracao;
	}
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@DisplayName("Data")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@DisplayName("Intera��o pelo app")
	public Boolean getInteracaoapp() {
		return interacaoapp;
	}
	
	public void setCdagendainteracaohistorico(Integer cdagendainteracaohistorico) {
		this.cdagendainteracaohistorico = cdagendainteracaohistorico;
	}
	public void setAgendainteracao(Agendainteracao agendainteracao) {
		this.agendainteracao = agendainteracao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	public void setInteracaoapp(Boolean interacaoapp) {
		this.interacaoapp = interacaoapp;
	}
	
	@Transient
	public String getUsuarioaltera(){
		if(getCdusuarioaltera() != null){
			return TagFunctions.findUserByCd(getCdusuarioaltera());
		} else return null;
	}
	
}
