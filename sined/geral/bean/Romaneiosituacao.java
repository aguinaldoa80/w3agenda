package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_romaneiosituacao", sequenceName = "sq_romaneiosituacao")
public class Romaneiosituacao {

	public static final Romaneiosituacao EM_ABERTO = new Romaneiosituacao(1, "Em aberto");
	public static final Romaneiosituacao BAIXADA = new Romaneiosituacao(2, "Baixado");
	public static final Romaneiosituacao CANCELADA = new Romaneiosituacao(3, "Cancelado");
	
	protected Integer cdromaneiosituacao;
	protected String descricao;
	
	public Romaneiosituacao(){
	}
	
	public Romaneiosituacao(Integer cdromaneiosituacao, String descricao){
		this.cdromaneiosituacao = cdromaneiosituacao;
		this.descricao = descricao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_romaneiosituacao")
	public Integer getCdromaneiosituacao() {
		return cdromaneiosituacao;
	}
	@DescriptionProperty
	@MaxLength(20)
	@Required
	@DisplayName("Situa��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setCdromaneiosituacao(Integer cdromaneiosituacao) {
		this.cdromaneiosituacao = cdromaneiosituacao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdromaneiosituacao == null) ? 0 : cdromaneiosituacao
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Romaneiosituacao other = (Romaneiosituacao) obj;
		if (cdromaneiosituacao == null) {
			if (other.cdromaneiosituacao != null)
				return false;
		} else if (!cdromaneiosituacao.equals(other.cdromaneiosituacao))
			return false;
		return true;
	}
	
}
