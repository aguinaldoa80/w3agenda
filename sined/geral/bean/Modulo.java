
package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name = "sq_modulo", sequenceName = "sq_modulo")
public class Modulo {

	protected Integer cdmodulo;
	protected String nome;

	@Override
	public String toString() {
		return this.nome;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_modulo")
	public Integer getCdmodulo() {
		return cdmodulo;
	}
	public void setCdmodulo(Integer id) {
		this.cdmodulo = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setDescricao(String descricao) {
	}

}
