package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.enumeration.Contratoacao;
import br.com.linkcom.sined.geral.bean.enumeration.Formafaturamento;

@Entity
@SequenceGenerator(name="sq_contratohistorico",sequenceName="sq_contratohistorico")
@DisplayName("Hist�rico")
public class Contratohistorico{

	protected Integer cdcontratohistorico;
	protected Contrato contrato;
	protected Timestamp dthistorico;
	protected Contratoacao acao;
	protected Usuario usuario;
	protected String observacao;
	protected MotivoCancelamentoFaturamento motivoCancelamentoFaturamento;
	protected Money valor;
	protected Date dtproximovencimento;
	protected Formafaturamento formafaturamento;
	protected String historico;
	protected String anotacao;
	protected Boolean visualizarContratoHistorico;
	protected String rateio;
	protected String parcelas;
	
	protected String responsavel;
	protected Double quantidade;
	protected Fatorajuste fatorAjuste;
	protected Indicecorrecao indiceCorrecao;
	protected Date dtrenovacao;
	
	// TRANSIENTE
	protected Boolean cancelarRomaneio = Boolean.FALSE;
	protected List<String> listaRateio;
	protected List<String> listaParcela;
	
	public Contratohistorico(){
	}

	public Contratohistorico(Contrato contrato, Timestamp dthistorico, Contratoacao acao, Usuario usuario){
		this.contrato = contrato;
		this.dthistorico = dthistorico;
		this.acao = acao;
		this.usuario = usuario;
	}
	
	public Contratohistorico(Contrato contrato, Timestamp dthistorico, Contratoacao acao, Usuario usuario, String observacao){
		this.contrato = contrato;
		this.dthistorico = dthistorico;
		this.acao = acao;
		this.usuario = usuario;
		this.observacao = observacao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratohistorico")
	public Integer getCdcontratohistorico() {
		return cdcontratohistorico;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@DisplayName("Data")
	public Timestamp getDthistorico() {
		return dthistorico;
	}
	
	@DisplayName("A��o")
	public Contratoacao getAcao() {
		return acao;
	}
	
	@JoinColumn(name="cdusuario")
	@ManyToOne(fetch=FetchType.LAZY)
	@DisplayName("Respons�vel")
	public Usuario getUsuario() {
		return usuario;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivocancelamentofaturamento")
	@DisplayName("Motivo de Cancelamento/Suspens�o")
	public MotivoCancelamentoFaturamento getMotivoCancelamentoFaturamento() {
		return motivoCancelamentoFaturamento;
	}
	
	public void setCdcontratohistorico(Integer cdcontratohistorico) {
		this.cdcontratohistorico = cdcontratohistorico;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public void setDthistorico(Timestamp dthistorico) {
		this.dthistorico = dthistorico;
	}
	public void setAcao(Contratoacao acao) {
		this.acao = acao;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public void setMotivoCancelamentoFaturamento(
			MotivoCancelamentoFaturamento motivoCancelamentoFaturamento) {
		this.motivoCancelamentoFaturamento = motivoCancelamentoFaturamento;
	}
	
	@Transient
	@DisplayName("Deseja cancelar os romaneios e as movimenta��es de estoque relacionadas?")
	public Boolean getCancelarRomaneio() {
		return cancelarRomaneio;
	}
	public void setCancelarRomaneio(Boolean cancelarRomaneio) {
		this.cancelarRomaneio = cancelarRomaneio;
	}
	
	public Boolean getVisualizarContratoHistorico() {
		return visualizarContratoHistorico;
	}
	
	public void setVisualizarContratoHistorico(Boolean visualizarContratoHistorico) {
		this.visualizarContratoHistorico = visualizarContratoHistorico;
	}

	public Money getValor() {
		return valor;
	}

	public void setValor(Money valor) {
		this.valor = valor;
	}
	
	@DisplayName("Pr�ximo vencimento")
	public Date getDtproximovencimento() {
		return dtproximovencimento;
	}
	
	public void setDtproximovencimento(Date dtproximovencimento) {
		this.dtproximovencimento = dtproximovencimento;
	}
	
	@DisplayName("Forma de faturamento")
	public Formafaturamento getFormafaturamento() {
		return formafaturamento;
	}

	public void setFormafaturamento(Formafaturamento formafaturamento) {
		this.formafaturamento = formafaturamento;
	}
	
	@DisplayName("Hist�rico")
	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}
	
	@DisplayName("Anota��es")
	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	public String getRateio() {
		return rateio;
	}

	public void setRateio(String rateio) {
		this.rateio = rateio;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	@Transient
	public List<String> getListaRateio() {
		return listaRateio;
	}

	public void setListaRateio(List<String> listaRateio) {
		this.listaRateio = listaRateio;
	}

	@Transient
	public List<String> getListaParcela() {
		return listaParcela;
	}

	public void setListaParcela(List<String> listaParcela) {
		this.listaParcela = listaParcela;
	}
	
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		return responsavel;
	}
	
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	@DisplayName("Quantidade")
	public Double getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	@DisplayName("Data de renova��o")
	public Date getDtrenovacao() {
		return dtrenovacao;
	}
	
	public void setDtrenovacao(Date dtrenovacao) {
		this.dtrenovacao = dtrenovacao;
	}
	
	@DisplayName("Fator de reajuste")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdfatorajuste")
	public Fatorajuste getFatorAjuste() {
		return fatorAjuste;
	}
	
	public void setFatorAjuste(Fatorajuste fatorAjuste) {
		this.fatorAjuste = fatorAjuste;
	}
	
	@DisplayName("�ndice de corre��o")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdindicecorrecao")
	public Indicecorrecao getIndiceCorrecao() {
		return indiceCorrecao;
	}
	
	public void setIndiceCorrecao(Indicecorrecao indiceCorrecao) {
		this.indiceCorrecao = indiceCorrecao;
	}
}
