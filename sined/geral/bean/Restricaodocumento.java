package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_restricaodocumento",sequenceName="sq_restricaodocumento")
public class Restricaodocumento{
	
	protected Integer cdrestricaodocumento;
	protected Restricao restricao;
	protected Documento documento;

	@Id
	@GeneratedValue(generator="sq_restricaodocumento",strategy=GenerationType.AUTO)
	public Integer getCdrestricaodocumento() {
		return cdrestricaodocumento;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdrestricao")
	public Restricao getRestricao() {
		return restricao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumento")
	public Documento getDocumento() {
		return documento;
	}

	public void setCdrestricaodocumento(Integer cdrestricaodocumento) {
		this.cdrestricaodocumento = cdrestricaodocumento;
	}

	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
}
