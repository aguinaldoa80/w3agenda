package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_documentoprocessomaterial",sequenceName="sq_documentoprocessomaterial")
public class Documentoprocessomaterial{
	
	protected Integer cddocumentoprocessomaterial;
	protected Documentoprocesso documentoprocesso;
	protected Material material;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_documentoprocessomaterial")
	public Integer getCddocumentoprocessomaterial() {
		return cddocumentoprocessomaterial;
	}
	@DescriptionProperty
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cddocumentoprocesso")
	public Documentoprocesso getDocumentoprocesso() {
		return documentoprocesso;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	public void setCddocumentoprocessomaterial(Integer cddocumentoprocessomaterial) {
		this.cddocumentoprocessomaterial = cddocumentoprocessomaterial;
	}
	public void setDocumentoprocesso(Documentoprocesso documentoprocesso) {
		this.documentoprocesso = documentoprocesso;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Documentoprocessomaterial) {
			Documentoprocessomaterial documentoprocessomaterial = (Documentoprocessomaterial) obj;
			return this.getCddocumentoprocessomaterial().equals(documentoprocessomaterial.getCddocumentoprocessomaterial());
		}
		return super.equals(obj);
	}
}
