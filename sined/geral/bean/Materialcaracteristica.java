package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_materialcaracteristica", sequenceName = "sq_materialcaracteristica")
@DisplayName("Característica")
public class Materialcaracteristica implements Log {

	protected Integer cdmaterialcaracteristica;
	protected Material material;
	protected String nome;
	protected String valor;
	
	protected Timestamp dtAltera;
	protected Integer cdUsuarioAltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialcaracteristica")
	public Integer getCdmaterialcaracteristica() {
		return cdmaterialcaracteristica;
	}
	
	@MaxLength(500)
	@DisplayName("Descrição")
	@Required
	public String getNome() {
		return nome;
	}
	
	@MaxLength(50)
	public String getValor() {
		return valor;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	
	public void setCdmaterialcaracteristica(Integer cdmaterialcaracteristica) {
		this.cdmaterialcaracteristica = cdmaterialcaracteristica;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}	

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCdusuarioaltera() {		
		return this.cdUsuarioAltera;
	}

	public Timestamp getDtaltera() {	
		return this.dtAltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdUsuarioAltera = cdusuarioaltera;		
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtAltera = dtaltera;		
	}

	
}
