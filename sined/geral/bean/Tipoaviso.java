package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@SequenceGenerator(name = "sq_tipoaviso", sequenceName = "sq_tipoaviso")
public class Tipoaviso {

	public static final Tipoaviso GERAL = new Tipoaviso(1, "Geral");
	public static final Tipoaviso NIVEL = new Tipoaviso(2, "N�vel");
	public static final Tipoaviso USUARIO =  new Tipoaviso(3, "Usu�rio");
	public static final Tipoaviso CLIENTE =  new Tipoaviso(4, "Cliente");
	
	protected Integer cdtipoaviso;
	protected String descricao;
	
	public Tipoaviso(){
	}

	public Tipoaviso(Integer cdtipoaviso, String descricao){
		this.cdtipoaviso = cdtipoaviso;
		this.descricao = descricao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_tipoaviso")
	public Integer getCdtipoaviso() {
		return cdtipoaviso;
	}
	public void setCdtipoaviso(Integer cdtipoaviso) {
		this.cdtipoaviso = cdtipoaviso;
	}
	@DescriptionProperty
	@DisplayName("Descri��o")
	@MaxLength(15)
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
