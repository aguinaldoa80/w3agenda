package br.com.linkcom.sined.geral.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_contratoarquivo", sequenceName = "sq_contratoarquivo")
public class Contratoarquivo implements Log {

	protected Integer cdcontratoarquivo;
	protected Arquivo arquivo;
	protected Contrato contrato;
	protected String descricao;
	protected Date dtarquivo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	

	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_contratoarquivo")
	public Integer getCdcontratoarquivo() {
		return cdcontratoarquivo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontrato")
	public Contrato getContrato() {
		return contrato;
	}
	
	@MaxLength(50)
	@DisplayName("Descri��o")
	@Required
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Data")
	@Required
	public Date getDtarquivo() {
		return dtarquivo;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	public void setDtarquivo(Date dtarquivo) {
		this.dtarquivo = dtarquivo;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	public void setCdcontratoarquivo(Integer cdcontratoarquivo) {
		this.cdcontratoarquivo = cdcontratoarquivo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdcontratoarquivo == null) ? 0 : cdcontratoarquivo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Contratoarquivo other = (Contratoarquivo) obj;
		if (cdcontratoarquivo == null) {
			if (other.cdcontratoarquivo != null)
				return false;
		} else if (!cdcontratoarquivo.equals(other.cdcontratoarquivo))
			return false;
		return true;
	}
	
}
