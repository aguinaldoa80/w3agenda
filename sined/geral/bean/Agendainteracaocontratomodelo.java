package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.enumeration.AgendainteracaocontratomodeloPessoatipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoDataReferenciaContrato;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name="sq_agendainteracaocontratomodelo", sequenceName="sq_agendainteracaocontratomodelo")
public class Agendainteracaocontratomodelo implements Log{
	
	protected Integer cdagendainteracaocontratomodelo;
	protected AgendainteracaocontratomodeloPessoatipo pessoatipo;
	protected Frequencia periodicidade;
	protected TipoDataReferenciaContrato referencia;
	protected Integer diasparacomecar;
	protected Integer frequencia;
	protected Atividadetipo atividadetipo;
	protected String descricao;
	protected Contratotipo contratotipo;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_agendainteracaocontratomodelo")
	public Integer getCdagendainteracaocontratomodelo() {
		return cdagendainteracaocontratomodelo;
	}
	public void setCdagendainteracaocontratomodelo(
			Integer cdagendainteracaocontratomodelo) {
		this.cdagendainteracaocontratomodelo = cdagendainteracaocontratomodelo;
	}
	
	@Required
	@DisplayName("Respons�vel")
	public AgendainteracaocontratomodeloPessoatipo getPessoatipo() {
		return pessoatipo;
	}
	public void setPessoatipo(AgendainteracaocontratomodeloPessoatipo pessoatipo) {
		this.pessoatipo = pessoatipo;
	}
	
	@Required
	@DisplayName("Periodicidade")
	@JoinColumn(name="cdperiodicidade")
	@ManyToOne(fetch=FetchType.LAZY)
	public Frequencia getPeriodicidade() {
		return periodicidade;
	}
	public void setPeriodicidade(Frequencia periodicidade) {
		this.periodicidade = periodicidade;
	}
	
	@Required
	@DisplayName("Data de refer�ncia")
	public TipoDataReferenciaContrato getReferencia() {
		return referencia;
	}
	public void setReferencia(TipoDataReferenciaContrato referencia) {
		this.referencia = referencia;
	}
	
	@Required
	@DisplayName("Dias para come�ar")
	public Integer getDiasparacomecar() {
		return diasparacomecar;
	}
	public void setDiasparacomecar(Integer diasparacomecar) {
		this.diasparacomecar = diasparacomecar;
	}
	
	@DisplayName("Frequ�ncia")
	public Integer getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(Integer frequencia) {
		this.frequencia = frequencia;
	}
	
	@DisplayName("Atividade")
	@JoinColumn(name="cdatividadetipo")
	@ManyToOne(fetch=FetchType.LAZY)
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcontratotipo")
	public Contratotipo getContratotipo() {
		return contratotipo;
	}
	public void setContratotipo(Contratotipo contratotipo) {
		this.contratotipo = contratotipo;
	}
	
	@MaxLength(2000)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	@Override
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
		
	}
	@Override
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
