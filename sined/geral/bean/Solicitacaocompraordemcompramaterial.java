package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

@Entity
@SequenceGenerator(name = "sq_solicitacaocompraordemcompramaterial", sequenceName = "sq_solicitacaocompraordemcompramaterial")
public class Solicitacaocompraordemcompramaterial {

	private Integer cdsolicitacaocompraordemcompramaterial;
	private Solicitacaocompra solicitacaocompra;
	private Ordemcompramaterial ordemcompramaterial;
	private Double qtde;
	
	// TRANSIENTE
	private Double resto;
	private Double aux_qtde;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_solicitacaocompraordemcompramaterial")
	public Integer getCdsolicitacaocompraordemcompramaterial() {
		return cdsolicitacaocompraordemcompramaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdsolicitacaocompra")
	public Solicitacaocompra getSolicitacaocompra() {
		return solicitacaocompra;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdordemcompramaterial")
	public Ordemcompramaterial getOrdemcompramaterial() {
		return ordemcompramaterial;
	}
	public Double getQtde() {
		return qtde;
	}
	
	public void setCdsolicitacaocompraordemcompramaterial(Integer cdsolicitacaocompraordemcompramaterial) {
		this.cdsolicitacaocompraordemcompramaterial = cdsolicitacaocompraordemcompramaterial;
	}
	public void setSolicitacaocompra(Solicitacaocompra solicitacaocompra) {
		this.solicitacaocompra = solicitacaocompra;
	}
	public void setOrdemcompramaterial(Ordemcompramaterial ordemcompramaterial) {
		this.ordemcompramaterial = ordemcompramaterial;
	}
	public void setQtde(Double qtde) {
		this.qtde = qtde;
	}	
	
	@Transient
	public Double getResto() {
		return resto;
	}
	
	public void setResto(Double resto) {
		this.resto = resto;
	}

	@Transient
	public Double getAux_qtde() {
		return aux_qtde;
	}
	public void setAux_qtde(Double auxQtde) {
		aux_qtde = auxQtde;
	}
}