package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_arquivotefhistorico", sequenceName = "sq_arquivotefhistorico")
@DisplayName("Hist�rico")
public class Arquivotefhistorico implements Log {

	protected Integer cdarquivotefhistorico;
	protected Arquivotef arquivotef;
	protected String acao;
	protected String observacao;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_arquivotefhistorico")
	public Integer getCdarquivotefhistorico() {
		return cdarquivotefhistorico;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivotef")
	public Arquivotef getArquivotef() {
		return arquivotef;
	}

	@DisplayName("A��o")
	public String getAcao() {
		return acao;
	}

	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	@DisplayName("Data/Hora")
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public void setCdarquivotefhistorico(Integer cdarquivotefhistorico) {
		this.cdarquivotefhistorico = cdarquivotefhistorico;
	}

	public void setArquivotef(Arquivotef arquivotef) {
		this.arquivotef = arquivotef;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
}
