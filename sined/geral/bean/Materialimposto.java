package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;


@Entity
@SequenceGenerator(name = "sq_materialimposto", sequenceName = "sq_materialimposto")
public class Materialimposto {

	protected Integer cdmaterialimposto;
	protected Material material;
	protected Empresa empresa;
	protected Double nacionalfederal;
	protected Double importadofederal;
	protected Double estadual;
	protected Double municipal;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_materialimposto")
	public Integer getCdmaterialimposto() {
		return cdmaterialimposto;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}

	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdempresa")
	public Empresa getEmpresa() {
		return empresa;
	}

	@DisplayName("Nacional Federal")
	public Double getNacionalfederal() {
		return nacionalfederal;
	}

	@DisplayName("Importados Federal")
	public Double getImportadofederal() {
		return importadofederal;
	}

	public Double getEstadual() {
		return estadual;
	}

	public Double getMunicipal() {
		return municipal;
	}

	public void setCdmaterialimposto(Integer cdmaterialimposto) {
		this.cdmaterialimposto = cdmaterialimposto;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setNacionalfederal(Double nacionalfederal) {
		this.nacionalfederal = nacionalfederal;
	}

	public void setImportadofederal(Double importadofederal) {
		this.importadofederal = importadofederal;
	}

	public void setEstadual(Double estadual) {
		this.estadual = estadual;
	}

	public void setMunicipal(Double municipal) {
		this.municipal = municipal;
	}
	
}
