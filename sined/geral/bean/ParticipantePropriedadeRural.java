package br.com.linkcom.sined.geral.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;

@Entity
@SequenceGenerator(name="sq_participantepropriedaderural", sequenceName="sq_participantepropriedaderural")
public class ParticipantePropriedadeRural {
	private Integer cdParticipantePropriedadeRural;
	private PropriedadeRural propriedadeRural;
	private Colaborador colaborador;
	private Double percentual;
	
	public ParticipantePropriedadeRural() {}
	
	public ParticipantePropriedadeRural(Integer cdParticipantePropriedadeRural) {
		this.cdParticipantePropriedadeRural = cdParticipantePropriedadeRural;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_participantepropriedaderural")
	public Integer getCdParticipantePropriedadeRural() {
		return cdParticipantePropriedadeRural;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdPropriedadeRural")
	public PropriedadeRural getPropriedadeRural() {
		return propriedadeRural;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpessoa")
	@DisplayName("Nome do Participante")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	@DisplayName("Percentual de Participação")
	public Double getPercentual() {
		return percentual;
	}
	
	public void setCdParticipantePropriedadeRural(
			Integer cdParticipantePropriedadeRural) {
		this.cdParticipantePropriedadeRural = cdParticipantePropriedadeRural;
	}
	
	public void setPropriedadeRural(PropriedadeRural propriedadeRural) {
		this.propriedadeRural = propriedadeRural;
	}
	
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
	public void setPercentual(Double percentual) {
		this.percentual = percentual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdParticipantePropriedadeRural == null) ? 0
						: cdParticipantePropriedadeRural.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipantePropriedadeRural other = (ParticipantePropriedadeRural) obj;
		if (cdParticipantePropriedadeRural == null) {
			if (other.cdParticipantePropriedadeRural != null)
				return false;
		} else if (!cdParticipantePropriedadeRural
				.equals(other.cdParticipantePropriedadeRural))
			return false;
		return true;
	}
}
