package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;


@Entity
@SequenceGenerator(name = "sq_motivodevolucao", sequenceName = "sq_motivodevolucao")
public class Motivodevolucao implements Log {

	protected Integer cdmotivodevolucao;
	protected String descricao;
	protected Boolean constatacao;
	protected Boolean motivodevolucao;
	
	//LOG
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	public Motivodevolucao(){
	}
	
	public Motivodevolucao(Integer cdmotivodevolucao){
		this.cdmotivodevolucao = cdmotivodevolucao;
	}
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_motivodevolucao")
	public Integer getCdmotivodevolucao() {
		return cdmotivodevolucao;
	}
	
	@Required
	@DescriptionProperty
	@DisplayName("Descrição")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Constatação")
	public Boolean getConstatacao() {
		return constatacao;
	}
	
	@DisplayName("Motivo de Devolução")
	public Boolean getMotivodevolucao() {
		return motivodevolucao;
	}

	public void setCdmotivodevolucao(Integer cdmotivodevolucao) {
		this.cdmotivodevolucao = cdmotivodevolucao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setConstatacao(Boolean constatacao) {
		this.constatacao = constatacao;
	}
	public void setMotivodevolucao(Boolean motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	
	//LOG
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
