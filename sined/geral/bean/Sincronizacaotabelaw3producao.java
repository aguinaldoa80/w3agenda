package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaw3producao;

@Entity
@SequenceGenerator(name = "sq_sincronizacaotabelaw3producao", sequenceName = "sq_sincronizacaotabelaw3producao")
public class Sincronizacaotabelaw3producao {

	protected Integer cdsincronizacaotabelaw3producao;
	protected Tabelaw3producao tabelaw3producao;
	protected Integer tabelaid;
	protected Timestamp dtalteracao;
	protected Boolean excluido;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_sincronizacaotabelaw3producao")
	public Integer getCdsincronizacaotabelaw3producao() {
		return cdsincronizacaotabelaw3producao;
	}

	public Tabelaw3producao getTabelaw3producao() {
		return tabelaw3producao;
	}

	public Integer getTabelaid() {
		return tabelaid;
	}

	public Timestamp getDtalteracao() {
		return dtalteracao;
	}

	public Boolean getExcluido() {
		return excluido;
	}

	public void setCdsincronizacaotabelaw3producao(Integer cdsincronizacaotabelaw3producao) {
		this.cdsincronizacaotabelaw3producao = cdsincronizacaotabelaw3producao;
	}

	public void setTabelaw3producao(Tabelaw3producao tabelaw3producao) {
		this.tabelaw3producao = tabelaw3producao;
	}

	public void setTabelaid(Integer tabelaid) {
		this.tabelaid = tabelaid;
	}

	public void setDtalteracao(Timestamp dtalteracao) {
		this.dtalteracao = dtalteracao;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

}
