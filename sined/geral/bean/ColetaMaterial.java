package br.com.linkcom.sined.geral.bean;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import org.hibernate.Hibernate;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.SinedUtil;

@Entity
@SequenceGenerator(name = "sq_coletamaterial", sequenceName = "sq_coletamaterial")
public class ColetaMaterial{
	
	protected Integer cdcoletamaterial;
	protected Coleta coleta;
	protected Material material;
	protected Pneu pneu;
	protected Double valorunitario;
	protected Double quantidade;
	protected Double quantidadedevolvida;
	protected Double quantidadedevolvidanota;
	protected Double quantidadecomprada;
	protected String observacao;
	protected Localarmazenagem localarmazenagem;
	protected List<ColetaMaterialDevolucao> listaColetaMaterialDevolucao;
	protected Pedidovendamaterial pedidovendamaterial;
	protected Motivodevolucao motivodevolucao;
	private List<Coletamaterialmotivodevolucao> listaColetamaterialmotivodevolucao;
	
	protected List<ColetaMaterialPedidovendamaterial> listaColetaMaterialPedidovendamaterial;
		
	//Transientes
	protected Empresa empresa;
	protected Cliente cliente;
	protected Double quantidaderestante;
	protected Double quantidadeestorno;
	protected Producaoordemmaterial producaoordemmaterial;
	protected Boolean utilizado;
	protected Double qtdeDevolvidaEstonar;
	protected String whereInPedidovendamaterial;
	
	public ColetaMaterial(){}
	public ColetaMaterial(Integer cdcoletamaterial) {
		this.cdcoletamaterial = cdcoletamaterial;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_coletamaterial")
	public Integer getCdcoletamaterial() {
		return cdcoletamaterial;
	}
	public void setCdcoletamaterial(Integer cdcoletamaterial) {
		this.cdcoletamaterial = cdcoletamaterial;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdcoleta")
	public Coleta getColeta() {
		return coleta;
	}
	public void setColeta(Coleta coleta) {
		this.coleta = coleta;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmaterial")
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpneu")
	public Pneu getPneu() {
		return pneu;
	}
	public void setPneu(Pneu pneu) {
		this.pneu = pneu;
	}
	
	@Required
	public Double getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}
	
	@DisplayName("Valor unit�rio")
	public Double getValorunitario() {
		return valorunitario;
	}
	public void setValorunitario(Double valorunitario) {
		this.valorunitario = valorunitario;
	}
	
	@DisplayName("Quantidade Devolvida")
	public Double getQuantidadedevolvida() {
		return quantidadedevolvida;
	}
	public void setQuantidadedevolvida(Double quantidadedevolvida) {
		this.quantidadedevolvida = quantidadedevolvida;
	}
	
	@DisplayName("Quantidade Devolvida com nota")
	public Double getQuantidadedevolvidanota() {
		return quantidadedevolvidanota;
	}
	public void setQuantidadedevolvidanota(Double quantidadedevolvidanota) {
		this.quantidadedevolvidanota = quantidadedevolvidanota;
	}
	
	@DisplayName("Quantidade Comprada")
	public Double getQuantidadecomprada() {
		return quantidadecomprada;
	}
	public void setQuantidadecomprada(Double quantidadecomprada) {
		this.quantidadecomprada = quantidadecomprada;
	}
	
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdlocalarmazenagem")
	@DisplayName("Local de Entrada")
	public Localarmazenagem getLocalarmazenagem() {
		return localarmazenagem;
	}
	public void setLocalarmazenagem(Localarmazenagem localarmazenagem) {
		this.localarmazenagem = localarmazenagem;
	}
	
	@OneToMany(mappedBy="coletamaterial")
	public List<ColetaMaterialDevolucao> getListaColetaMaterialDevolucao() {
		return listaColetaMaterialDevolucao;
	}
	public void setListaColetaMaterialDevolucao(List<ColetaMaterialDevolucao> listaColetaMaterialDevolucao) {
		this.listaColetaMaterialDevolucao = listaColetaMaterialDevolucao;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdpedidovendamaterial")
	@DisplayName("Material do pedido de venda")
	public Pedidovendamaterial getPedidovendamaterial() {
		return pedidovendamaterial;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdmotivodevolucao")
	@DisplayName("Motivo da Devolu��o")
	public Motivodevolucao getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public void setPedidovendamaterial(Pedidovendamaterial pedidovendamaterial) {
		this.pedidovendamaterial = pedidovendamaterial;
	}
	public void setMotivodevolucao(Motivodevolucao motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
	
	@Transient
	public Empresa getEmpresa() {
		if(empresa == null && coleta != null && coleta.getPedidovenda() != null && coleta.getPedidovenda().getCdpedidovenda() != null){
			empresa = coleta.getPedidovenda().getEmpresa();
		}
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@Transient
	public Cliente getCliente() {
		if(cliente == null && coleta != null){
			if(coleta.getCliente() != null){
				cliente = coleta.getCliente();
			} else if(coleta.getPedidovenda() != null){				
				cliente = coleta.getPedidovenda().getCliente();
			}
		}
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@Transient
	@DisplayName("Total")
	public Double getValortotal(){
		if(getValorunitario() != null && getQuantidade() != null){
			Double qtde = getQuantidadedevolvida() != null ? getQuantidade() - getQuantidadedevolvida() : getQuantidade(); 
			return getValorunitario() * qtde;
		} else return null;
	}
	
	@Transient
	public Double getQuantidaderestante() {
		return quantidaderestante;
	}
	
	public void setQuantidaderestante(Double quantidaderestante) {
		this.quantidaderestante = quantidaderestante;
	}
	
	@Transient
	public Double getQuantidadeestorno() {
		return quantidadeestorno;
	}
	
	public void setQuantidadeestorno(Double quantidadeestorno) {
		this.quantidadeestorno = quantidadeestorno;
	}
	
	@Transient
	public Producaoordemmaterial getProducaoordemmaterial() {
		return producaoordemmaterial;
	}
	
	public void setProducaoordemmaterial(Producaoordemmaterial producaoordemmaterial) {
		this.producaoordemmaterial = producaoordemmaterial;
	}
	
	@Transient
	public Double getQuantidadedevolvidaSemNota() {
		Double qtde = 0d; 
		if(getQuantidadedevolvida() != null){
			qtde += getQuantidadedevolvida();
		}
		if(getQuantidadedevolvidanota() != null){
			qtde -= getQuantidadedevolvidanota();
		}
		return qtde;
	}

	@Transient
	public Boolean getUtilizado() {
		return utilizado;
	}

	public void setUtilizado(Boolean utilizado) {
		this.utilizado = utilizado;
	}
	
	@OneToMany(mappedBy="coletaMaterial")
	public List<Coletamaterialmotivodevolucao> getListaColetamaterialmotivodevolucao() {
		return listaColetamaterialmotivodevolucao;
	}
	
	public void setListaColetamaterialmotivodevolucao(List<Coletamaterialmotivodevolucao> listaColetamaterialmotivodevolucao) {
		this.listaColetamaterialmotivodevolucao = listaColetamaterialmotivodevolucao;
	}
	
	@Transient
	public List<Motivodevolucao> getListaMotivodevolucao(){
		List<Motivodevolucao> listaMotivodevolucao = new ArrayList<Motivodevolucao>();
		
		if (Hibernate.isInitialized(getListaColetamaterialmotivodevolucao()) && SinedUtil.isListNotEmpty(getListaColetamaterialmotivodevolucao())){
			for (Coletamaterialmotivodevolucao coletamaterialmotivodevolucao: getListaColetamaterialmotivodevolucao()){
				if (coletamaterialmotivodevolucao.getMotivodevolucao()!=null){
					listaMotivodevolucao.add(coletamaterialmotivodevolucao.getMotivodevolucao());
				}
			}
		}
		
		return listaMotivodevolucao;
	}
	
	@Transient
	public Double getQtdeDevolvidaEstonar() {
		return qtdeDevolvidaEstonar;
	}
	public void setQtdeDevolvidaEstonar(Double qtdeDevolvidaEstonar) {
		this.qtdeDevolvidaEstonar = qtdeDevolvidaEstonar;
	}
	
	@OneToMany(mappedBy = "coletaMaterial")
	public List<ColetaMaterialPedidovendamaterial> getListaColetaMaterialPedidovendamaterial() {
		return listaColetaMaterialPedidovendamaterial;
	}
	public void setListaColetaMaterialPedidovendamaterial(List<ColetaMaterialPedidovendamaterial> listaColetaMaterialPedidovendamaterial) {
		this.listaColetaMaterialPedidovendamaterial = listaColetaMaterialPedidovendamaterial;
	}
	
	@Transient
	public String getWhereInPedidovendamaterial() {
		if(this.listaColetaMaterialPedidovendamaterial != null && !this.listaColetaMaterialPedidovendamaterial.isEmpty()) {
			return SinedUtil.listAndConcatenate(this.listaColetaMaterialPedidovendamaterial, "pedidovendamaterial.cdpedidovendamaterial", ", ");
		}
		return "";
	}
	public void setWhereInPedidovendamaterial(String whereInPedidovendamaterial) {
		this.whereInPedidovendamaterial = whereInPedidovendamaterial;
	}
}
