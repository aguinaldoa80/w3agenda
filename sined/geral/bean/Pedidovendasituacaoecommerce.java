package br.com.linkcom.sined.geral.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.util.Log;

@Entity
@SequenceGenerator(name = "sq_pedidovendasituacaoecommerce", sequenceName = "sq_pedidovendasituacaoecommerce")
public class Pedidovendasituacaoecommerce implements Log {
	
	protected Integer cdpedidovendasituacaoecommerce;
	protected String nome;
	protected String corletra;
	protected String corfundo;
	protected String letra;
	protected String idsecommerce;
	protected Integer ordem;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	@Id
	@DisplayName("Id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_pedidovendasituacaoecommerce")
	public Integer getCdpedidovendasituacaoecommerce() {
		return cdpedidovendasituacaoecommerce;
	}
	
	@Required	
	@MaxLength(20)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}	
	
	@Required	
	@MaxLength(20)
	@DisplayName("Cor da letra de representação")
	public String getCorletra() {
		return corletra;
	}
	
	@Required	
	@MaxLength(20)
	@DisplayName("Cor do fundo de representação")
	public String getCorfundo() {
		return corfundo;
	}
	
	@Required	
	@MaxLength(2)
	@DisplayName("Letra de representação")
	public String getLetra() {
		return letra;
	}

	@Required	
	@DisplayName("Ids da situação no e-commerce")
	public String getIdsecommerce() {
		return idsecommerce;
	}
	
	public Integer getOrdem() {
		return ordem;
	}
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}

	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public void setCdpedidovendasituacaoecommerce(
			Integer cdpedidovendasituacaoecommerce) {
		this.cdpedidovendasituacaoecommerce = cdpedidovendasituacaoecommerce;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCorletra(String corletra) {
		this.corletra = corletra;
	}

	public void setCorfundo(String corfundo) {
		this.corfundo = corfundo;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public void setIdsecommerce(String idsecommerce) {
		this.idsecommerce = idsecommerce;
	}

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}

	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cdpedidovendasituacaoecommerce == null) ? 0
						: cdpedidovendasituacaoecommerce.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Pedidovendasituacaoecommerce other = (Pedidovendasituacaoecommerce) obj;
		if (cdpedidovendasituacaoecommerce == null) {
			if (other.cdpedidovendasituacaoecommerce != null)
				return false;
		} else if (!cdpedidovendasituacaoecommerce.equals(other.cdpedidovendasituacaoecommerce))
			return false;
		return true;
	}

}
